load("nashorn:mozilla_compat.js");//allows importPackage() usage

importPackage(Packages.nl.bartpelle.veteres.model.entity);
importPackage(Packages.nl.bartpelle.veteres.model.entity.player);
importPackage(Packages.nl.bartpelle.veteres.model.entity.npc);
importPackage(Packages.nl.bartpelle.veteres.model);
importPackage(Packages.nl.bartpelle.veteres.model.item);
importPackage(Packages.nl.bartpelle.veteres.util);
importPackage(Packages.nl.bartpelle.veteres.net.message.game.command);
importPackage(Packages.nl.bartpelle.veteres.script);
importPackage(Packages.java.lang);
importPackage(Packages.java.util);
importPackage(Packages.nl.bartpelle.veteres.content.skills.slayer.master);
importPackage(Packages.nl.bartpelle.veteres.content.skills.slayer);
importPackage(Packages.nl.bartpelle.veteres.content);
importPackage(Packages.nl.bartpelle.veteres.content.areas);
importPackage(Packages.nl.bartpelle.veteres.content.areas.wilderness);
importPackage(Packages.nl.bartpelle.veteres.content.items.equipment);
importPackage(Packages.nl.bartpelle.veteres.content.combat);
importPackage(Packages.nl.bartpelle.veteres.content.combat.melee);
importPackage(Packages.nl.bartpelle.veteres.content.events);
importPackage(Packages.nl.bartpelle.veteres.content.events.tournament);
importPackage(Packages.nl.bartpelle.veteres.content.mechanics);
importPackage(Packages.nl.bartpelle.veteres.content.mechanics.deadman);
importPackage(Packages.nl.bartpelle.veteres.content.mechanics.deadman.safezones);
importPackage(Packages.nl.bartpelle.veteres.content.items);
importPackage(Packages.nl.bartpelle.veteres.content.interfaces);
importPackage(Packages.nl.bartpelle.veteres.content.skills.herblore);
importPackage(Packages.nl.bartpelle.veteres.content.skills.agility);
importPackage(Packages.nl.bartpelle.veteres.content.skills.hunter);
importPackage(Packages.nl.bartpelle.veteres.content.skills.farming);
importPackage(Packages.nl.bartpelle.veteres.content.skills.farming.patch);
importPackage(Packages.nl.bartpelle.veteres.content.npcs);
importPackage(Packages.nl.bartpelle.veteres.content.npcs.bosses);
importPackage(Packages.nl.bartpelle.veteres.content.minigames);
importPackage(Packages.nl.bartpelle.veteres.content.areas.burthorpe.dialogue)
importPackage(Packages.nl.bartpelle.veteres.content.areas.edgeville.dialogue)
importPackage(Packages.nl.bartpelle.veteres.content.areas.lumbridge)
importPackage(Packages.nl.bartpelle.veteres.migration)
importPackage(Packages.nl.bartpelle.veteres.fs)
importPackage(Packages.nl.bartpelle.veteres.services)
importPackage(Packages.nl.bartpelle.veteres.services.login)

function resetrp(vars) {
    if (p.timers().has(TimerKey.RISK_PROTECTION)) {
        p.timers().cancel(TimerKey.RISK_PROTECTION);
        p.clearattrib(AttributeKey.RISK_PROT_TIER)
        p.message("RISK_PROTECTION timer cancelled.");
    } else {
        p.message("RISK_PROTECTION timer not active.");
    }
}
function clogreport(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    Login.SHOULD_REPORT_GENERIC_CRASHES = op
    p.message("crash log state updated to: "+Login.SHOULD_REPORT_GENERIC_CRASHES);
}

function clearcrashlog(args) {
    var c = ClientCrashLog.CRASHENTRY_LIST.size()
    ClientCrashLog.CRASHENTRY_LIST.clear()
    p.message("cleared "+c+" entries.");
}

function isprem(args) { // See if movement stops when you die
    args = args.split(" ")
    var name = args[1].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get();
    p.message(opp.name()+" prem: "+opp.isPremiumUser())
}
function makefaceeast(vars) {
    var npc = p.world().npcs().get(2397);
    npc.faceTile(npc.tile().transform(5, 0))
    p.message("npc "+npc+" facing east!")
}
function setuphz(vars) {
    PVPAreas.reSetupHZ(p.world())
}
function yellfilter(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.YELL_ALLOWS_IGNORE_LIST_FILTER = op
    p.message("YELL_ALLOWS_IGNORE_LIST_FILTER: "+GameCommands.YELL_ALLOWS_IGNORE_LIST_FILTER)
}

function togglerangerng(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    PlayerCombat.CUSTOM_RANGE_ACCURACY_MODIFIER_ENABLED = op
    p.message("Custom range rng: "+PlayerCombat.CUSTOM_RANGE_ACCURACY_MODIFIER_ENABLED)
}
function rangerngcap(vars) {
    args = vars.split(" ")
    var op = new java.lang.Double(args[1])
    PlayerCombat.BONUS_THRESHOLD = op
    p.message("Rng threashold: "+PlayerCombat.BONUS_THRESHOLD)
}

function rangedps(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    PlayerCombat.RANGE_DPS_WHEEL_ENABLED = op
    p.message("Custom range DPS: "+PlayerCombat.RANGE_DPS_WHEEL_ENABLED)
}
function adminlogin(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    LoginWorker.BLOCK_NONADMIN = op
    p.message("LoginWorker.BLOCK_NONADMIN: "+LoginWorker.BLOCK_NONADMIN)
}

function herbpet(vars) {
    Herbs.tryForHerbPatchPet(p)
}

function swim(vars) {
    CanoeStation.testswimming(p);
}

function petfunny(vars) {
    if (Death.PET_SHOUTING_ENABLED) {
        Death.PET_SHOUTING_ENABLED = false;
    } else {
        Death.PET_SHOUTING_ENABLED = true;
    }
    p.message("Now: "+Death.PET_SHOUTING_ENABLED);
}

function togglepvp1(vars) {
    if (GameCommands.PVP1_OFF) {
        GameCommands.PVP1_OFF = false;
    } else {
        GameCommands.PVP1_OFF = true;
    }
    p.message("pvp1 Now: "+GameCommands.PVP1_OFF);
}

function togglepvp2(vars) {
    if (GameCommands.PVP2_OFF) {
        GameCommands.PVP2_OFF = false;
    } else {
        GameCommands.PVP2_OFF = true;
    }
    p.message("pvp2 Now: "+GameCommands.PVP2_OFF);
}


function worldstate(vars) {
    args = vars.split(" ")
    p.write(new UpdateStateCustom(new java.lang.Integer(args[1])))
}

function dmlog(vars) {
    p.interfaces().send(228, 162, 546, false)
    p.write(new InvokeScript(1149, "before you can logout...", 11, "Running away? Not so fast!", 1))
}

// Send a world flag.
function dmmflag(vars) {
    args = vars.split(" ")
    p.write(new UpdateStateCustom(new java.lang.Integer(536870913)))
    p.message("sent val -> "+536870913)
}

function pvpi(vars) {
    args = vars.split(" ")
    p.write(new InterfaceVisibility((90 << 16) | new java.lang.Integer(args[1]), new java.lang.Boolean(args[2])))
    p.message("Show: "+args[1])
}

// dmm string on face 90
function dmmi(vars) {
    args = vars.split(" ")
    p.write(new InvokeScript(new java.lang.Integer(1137), new java.lang.Integer(args[1])))
    p.message("yo lad jak = god_dmm value:"+new java.lang.Integer(args[1]))
}

// dmm string on face 90
function dmi2(vars) {
    args = vars.split(" ")
    p.write(new InvokeScript(new java.lang.Integer(1176), new java.lang.Integer(args[1])))
    p.message("1176 :"+new java.lang.Integer(args[1]))
}

function convertzones(vars) {
    DmmZones.convert();
    p.message("yo");
}

function testdmmzones(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    DmmZones.DMM_ZONE_CHECK_ENABLED = op
    p.message("Dmm zone checks: "+op)
}

function pvpw(vars) {
    if (p.world().players().size > 5) {
        p.message("that'd be pretty fucking dumb would it not??");
        return;
    }
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    WildernessLevelIndicator.PVP_WORLD = op
    p.message("WildernessLevelIndicator.PVP_WORLD: "+WildernessLevelIndicator.PVP_WORLD)
}

function clawm(vars) {
    args = vars.split(" ")
    MeleeSpecialAttacks.CLAWS_MULTIPLIER = new java.lang.Double(args[1])
    p.message("claw multiplier now -> "+MeleeSpecialAttacks.CLAWS_MULTIPLIER)
}

function isets(args) {
    ItemSets.reloadSets(p.world());
    p.message("done");
}
function gesafe(args) {
    p.message("k "+PVPAreas.safearea_coords.size());
    for (var area in PVPAreas.safearea_coords) {
        for (var x = area.x1(); x < area.x2(); x++) {
            for (var y = area.z1(); y < area.z2(); y++) {
                 DmmZones.spawn(p.world(), new Tile(x, y));
            }
        }
    }
}

function real(args) {
    p.message("pos "+p.tile()+" is actually "+PVPAreas.realTile(p.tile()));
}

function zoneset(args) {
    p.inventory().add(new Item(5507), false);
    p.inventory().add(new Item(5508), false);
    p.inventory().add(new Item(7635), false);
    p.message("k ");
}
function savezones(args) {
    DmmZones.saveToJson();
    p.message("k ");
}
function loadzones(args) {
    DmmZones.loadJson();
    p.message("k ");
}
function clearzones(args) {
    DmmZones.INSTANCE.SAFE_TILES.clear();
    p.message("k ");
}
function cn1(args) {
    var x = 2313;
    var z = 3793;
    var cx = x >> 3;
    var cz = z >> 3;
    var combo = (cx << 16) + cz;
    p.message("sources: "+x+" "+z+" "+cx+" "+cz+" "+combo+" reverts back to "+(cx<<3)+" "+(cz<<3));
}
function cid(args) {
    p.message("yo: "+Tile.chunkToTile(p.tile().chunk()));
}
function dmmz(args) {
    DmmZones.printAll(p);
    p.message("done");
}
function ol(args) {
    DmmZones.test(p);
    p.message("done");
}

function ol2(args) {
    DmmZones.test2(p);
    p.message("done 2");
}
function midge(args) {
    p.message(PVPAreas.GE_CENTRE);
}
function rebuildpvp(args) {
    PVPAreas.rebuild(p.world())
}
function buildteles(args) {
    NewWizardPVP.buildTeleports()
}
function fuck1(args) {
    p.write(new SetPlayerOption(1, true, "null"));
}
function fuck2(args) {
    p.write(new SetPlayerOption(2, false, "Attack"));
}
function allplayerops(args) {
    for (var i = 0; i < 9; i++)
    p.write(new SetPlayerOption(i, false, "OP "+i))
}

function ct1(args) {
    p.bank().items()[0] = new Item(11907, 2);
    p.bank().items()[1] = new Item(11907);
    p.message("huh")
}
function cuk1(args) {
    for (n in p.world().npcs()) {
        n.faceTile(p.tile())
    }
    p.message("jew")
}
function resetlooks1(args) {
    var vars = args.split(" ")
    var name = vars[1].replaceAll("_", " ")
    p.message("searching for "+name+" with param "+vars[1])
    var opp = p.world().playerByName(name).get();
    p.message(opp.name()+" looks = "+Arrays.toString(opp.looks().looks())+" and cols: "+Arrays.toString(opp.looks().colors()));
    opp.looks().looks([9, 14, 107, 26, 33, 36, 42]);
    opp.looks().colors([6, 17, 15, 0, 7]);
    opp.looks().update()
}

function migration(args) { //
    var vars = args.split(" ")
    var id = Integer.parseInt(vars[1]);
    p.migration(id);
    p.message("migration now at "+p.migration());
}

function npcdefinfo(args) { //
    var vars = args.split(" ")
    var id = Integer.parseInt(vars[1]);
    var def = p.world().definitions().get(NpcDefinition.class, id);
    p.message(Arrays.toString(def.options));
}


function charge(args) {
    if (ZulrahItems.CHARGING_ENABLED) {
        ZulrahItems.CHARGING_ENABLED = false;
    } else {
        ZulrahItems.CHARGING_ENABLED = true;
    }
    p.message("ZulrahItems.CHARGING_ENABLED now "+ZulrahItems.CHARGING_ENABLED)
}
function pheld(args) {
    var ids = [20661, 20661, 13225]
    for (var i in ids) {
        var pet = new Item(i)
        var def = pet.definition(p.world());
        p.message("stacks:"+def.stackable()+"  placeheld:"+def.placeheld+"  pheld ID:"+def.pheld14401)
    }
}

function restarthunter(args) {
    BirdSnaring.hotreloadNpcCycle(p.world())
}
function fuckoff(args) {
    p.debug("Rolled %d/%d. Cap %d%%, lowest %d%%. Max base level:%d.  Gap:%d.  %% success per level:%f.  Over-level:%d", 50, 100, 95, 30, 99, 46, 1.5, 20)
}
function pt(args) {
    PunishmentLog.test()
    PunishmentLog.displayLog(p)
}

// Hot reload
function herbpots(args) {
    PotionMixing.reload(p.world().server().scriptRepository())
    PotionBrewing.reload(p.world().server().scriptRepository())
}

// Testing different display modes child ids
function inv(args) {
    p.interfaces().sendWidgetOn(149, Interfaces.InterSwitches.I)
    p.message("done")
}

// Testing different display modes child ids
function trade(args) {
    p.interfaces().sendMain(335)
    p.interfaces().send(336, p.interfaces().activeRoot(), p.interfaces().inventoryComponent(), false)
    p.invokeScript(149, (336 << 16) + 0, 93, 4, 7, 0, -1,
    "Offer<col=ff9040>", "Offer-5<col=ff9040>", "Offer-10<col=ff9040>", "Offer-All<col=ff9040>", "Offer-X<col=ff9040>")
    p.write(new InterfaceSettings(336, 0, 0, 28, 1086))
    p.write(new InterfaceSettings(335, 28, 0, 27, 1024))
    p.write(new InterfaceSettings(335, 25, 0, 27, 1086))
    p.message("kk")
}

// Instances test
function edge1(args) {
    EdgevilleTourny.generateEdge(p.world())
    p.message("kk")
}

function changepool(args) {
    var mBeanServer = java.lang.management.ManagementFactory.getPlatformMBeanServer();
    var poolName = new javax.management.ObjectName("com.zaxxer.hikari:type=PoolConfig (hikari)");
    var configProxy = javax.management.JMX.newMXBeanProxy(mBeanServer, poolName, com.zaxxer.hikari.HikariConfigMXBean.class);
    configProxy.setMaximumPoolSize(5120);
}

function codeset(args) { // max melee/mage gear for combat testing
    Prayers.disableAllPrayers(p);
    p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
    GameCommands.bankAll(p);
    p.message("The items you were previously holding and wearing have been banked.");
    // melee setup
    p.inventory().add(new Item(12006, 1), false); // tent
    p.inventory().add(new Item(12954, 1), false); // defender
    p.inventory().add(new Item(6570, 1), false); //fcape
    p.inventory().add(new Item(11773, 1), false); // b ring
    p.inventory().add(new Item(11832, 1), false); // bandos
    p.inventory().add(new Item(11834, 1), false); // bandos
    // essentials
    p.inventory().add(new Item(7462, 1), false); // gloves
    p.inventory().add(new Item(6585, 1), false); // fury
    p.inventory().add(new Item(3105, 1), false); // boots
    p.inventory().add(new Item(10828, 1), false); // neitz
    // mage
    p.inventory().add(new Item(12904, 1), false); // t sotd
    p.inventory().add(new Item(12825, 1), false); // arcane
    p.inventory().add(new Item(12002, 1), false); // occult
    p.inventory().add(new Item(11770, 1), false); // seers
    p.inventory().add(new Item(4712, 1), false); // ahrims
    p.inventory().add(new Item(4714, 1), false); // ahrmis
    p.inventory().add(new Item(2414, 1), false); // god cape
    //other
    p.inventory().add(new Item(12695, 1), false); // super cb pot
    p.inventory().add(new Item(4153, 1), false); // g maul for quick spec killing
	GameCommands.process(p, "masterstats");
    GameCommands.process(p, "heal");
    GameCommands.process(p, "max");
	GameCommands.process(p, "spec");
    // done
    p.message("have dis nigga")
}

function codeset2(args) { //
	Prayers.disableAllPrayers(p);
	p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
	GameCommands.bankAll(p);
	p.message("The items you were previously holding and wearing have been banked.");
	// range setup
	p.inventory().add(new Item(13072, 1), false); // elite void
	p.inventory().add(new Item(13073, 1), false); // elite void
	p.inventory().add(new Item(8840, 1), false); // void gloves
	p.inventory().add(new Item(11664, 1), false); // void ranger helm
	p.inventory().add(new Item(11212, 1000), false); // drag arrows
	p.inventory().add(new Item(11771, 1), false); // archers ring (i)
	p.inventory().add(new Item(10499, 1), false); // avas
	p.inventory().add(new Item(6585, 1), false); // fury
	
	GameCommands.process(p, "masterstats");
	GameCommands.process(p, "heal");
	GameCommands.process(p, "max");
	GameCommands.process(p, "spec");
	// done
	p.message("have dis nigga")
}

function pidtick(args) { // test overhead icons skulls
    if (GameCommands.ZERO_TICK_PID_ON) {
        GameCommands.ZERO_TICK_PID_ON = false;
    } else {
        GameCommands.ZERO_TICK_PID_ON = true;
    }
    p.message("now "+GameCommands.ZERO_TICK_PID_ON);
}

function resetvenomworld(args) { // test overhead icons skulls
    p.message("checking");
    for (var i = 0; i < 2046; i++) {
        // p.message("at "+i);
        if (p.world().players().get(i) == null) continue;
        var other = p.world().players().get(i);
        //p.message("other: "+other.getName());
        if (other.attrib(AttributeKey.VENOM_TICKS) != 0) {
            other.putattrib(AttributeKey.VENOM_TICKS, 0);
            other.message("<col=FF0000>Your venom has been forcefully cured by staff");
        }
    }
    p.message("ok");
}

function adminvenom(args) { // test overhead icons skulls
    if (GameCommands.VENOM_FROM_ADMINS_ON) {
        GameCommands.VENOM_FROM_ADMINS_ON = false;
    } else {
        GameCommands.VENOM_FROM_ADMINS_ON = true;
    }
    p.message("now "+GameCommands.VENOM_FROM_ADMINS_ON);
}

function playervenom(args) { // test overhead icons skulls
    if (GameCommands.VENOM_VS_PLAYERS_ON) {
        GameCommands.VENOM_VS_PLAYERS_ON = false;
    } else {
        GameCommands.VENOM_VS_PLAYERS_ON = true;
    }
    p.message("now "+GameCommands.VENOM_VS_PLAYERS_ON);
}

function maxcapereq(args) { // test overhead icons skulls
    var vars = args.split(" ")
    Mac.TOTAL_LEVEL_FOR_MAXED = Integer.parseInt(vars[1]);
    p.message("now "+Mac.TOTAL_LEVEL_FOR_MAXED);
}

function ac(args) { // test overhead icons skulls
    var vars = args.split(" ")
    p.invokeScript(235, Integer.parseInt(vars[1]));
}

function iplol(args) {
    p.write(new Packet5rev86("127 0 0 1"));
    p.message("iplol");
}

function testp1(args) {
    p.write(new Packet58rev86());
    p.message("testp1");
}

// https://i.gyazo.com/2e112545ae390c59d9428980a178ca4f.png
function testp2(args) {
    p.write(new CinematicFocus(1, 1, false)); // cinamtic focus
    p.message("testp2");
}

function testp3(args) {
    p.write(new GEUpdate(false));
    p.message("testp3");
}

function testp4(args) {
    p.write(new VarpsCopy());
    p.message("testp4");
}

function testp5(args) {
    p.write(new MusicRelated(-1, 10));
    p.message("testp5");
}

function testp6(args) {
    p.write(new ShowExpiresDocument(""));
    p.message("testp6");
}

function testp7(args) {
    p.write(new ClearMapBase(p, p.tile()));
    p.message("testp7");
}

// Test function
function wtf3(args) {
    p.message("lol")
}

function zulrah(args) {
    ZulrahBoat.testTravel(p)
}

function mapi(args) {
    var map = p.world().allocator().allocate(128, 128, new Tile(3222, 3222)).get();
    map.set(0, 0, 0, new Tile(3200, 3200, 0), new Tile(3200 + 128, 3200 + 128, 0), 0, true)
    p.teleport(map.center().x-4, map.center().z-4, 0)
    p.write(new DisplayInstancedMap(map, p));
}

function ginfo(args) {
    var cmd = args.split(" ")
    //VarbitAttributes.set(p, new Integer(cmd[1]), new Integer(cmd[2]))
    p.message("val is %d", p.attribOr(AttributeKey.GENERAL_VARBIT1, new Integer(0)))
}

function verifymixing(args) {
    PotionMixing.verifyData(p)
}

function cx1(args) {
p.invokeScript(118, 0, 3, 4151, 4151, 4151,
                -1, 0, 0, "1 gp");
}

function skillg(args) {
    var cmd = args.split(" ")
    p.interfaces().sendMain(214, false)
    p.varps().varbit(4371, cmd[1])
    p.varps().varbit(4372, 0)
    p.message("viewing skill guide of "+cmd[1])
}
function ca(args) {
    checkagil(args)
}
function fill(args) {
    var cmd = args.split(" ")
    for (var i = cmd[1]; i < cmd[1]+28; i++) {
        p.inventory().add(new Item(i), true)
    }
}
function checkagil(args) {
    var ye = 0
    var no = 0
    for (var i = 0; i < 100; i++) {
        if (Barbarian.successful(p)) {
            ye += 1
        } else no++
    }
    p.message("%f/%f", ye, ye+no)
}
function rope(args) {
	p.write(new SetMapBase(p, p.world().objById(23131, new Tile(2551, 3550)).tile()))
    p.write(new AnimateObject(p.world().objById(23131, new Tile(2551, 3550)), 54))
    p.message("animated")
}
function line(args) {
    for (var i = 0; i < 16; i++) {
        p.world().spawnGroundItem(new GroundItem(p.world(), new Item(385), p.tile().transform(0, i, 0), p.id()).lifetime(1000 * 60 * 2))
        p.world().spawnGroundItem(new GroundItem(p.world(), new Item(385), p.tile().transform(-8 + i, 0, 0), p.id()).lifetime(1000 * 60 * 2))

       // p.message("385 at "+p.tile().transform(0, i, 0))
    }
}
function arenasize(args) {
    p.message(Staking.OCCUPIED_TILES.size()+ " occupied")
}
function cleararenatiles(args) {
    Staking.OCCUPIED_TILES.clear()
    p.message(Staking.OCCUPIED_TILES.size()+ " occupied post-clear")
}
function edgep(args) {
    p.teleport(PVPAreas.edgeville.center().transform(-3, -6, 0))
}
function take0(args) {
    p.putattrib(AttributeKey.BUTTON_SLOT, 0)
    Trading.takeItem(p, 1)
    p.message("ok")
}

function activereg(args) { // Debug where we are
    p.message("active_area= "+p.activeArea()+" region= "+p.activeMap().toStringSimple()+" "+p.tile().region()+" "+p.tile().chunk());
}

function wtfkbd(args) {
    for (var i = 0; i < 20; i++) {
        p.world().registerNpc(new Npc(239, p.world(), p.tile()));
    }
}
function killclose(args) {

    for (var i = 0; i < p.world().npcs().size(); i++) {
        var npc = p.world().npcs().get(i);

        if (npc != null && npc.id() == 239) {
            var h = npc.hit(p, npc.hp(), 1).combatStyle(CombatStyle.MELEE);
           // Playercombat.addCombatXp(p, npc, h.damage(), CombatStyle.MELEE);
        }
    }
}

function killsnakelings(args) {
    p.message("Total npcs: " + p.world().npcs().size());
    var killed = 0;
    for (var i = 0; i < p.world().npcs().size(); i++) {
        var npc = p.world().npcs().get(i);

        if (npc != null && npc.id() == 5860) {
            npc.world().unregisterNpc(npc);
            killed++
        }
    }

    p.message("Killed: " + killed + ".")
}

function findnpcs(args) {
    p.message("Total npcs: " + p.world().npcs().size());
    var array = [];
    var actual = 0;

    for (var i = 0; i < p.world().npcs().size(); i++) {
        var npc = p.world().npcs().get(i);

        if (npc != null) {
            if (array[npc.id()] == null) {
                array[npc.id()] = 0;
            }

            array[npc.id()]++;
            actual++;
        }
    }

    for (var key in array) {
        if (array[key] > 50) {
            java.lang.System.out.println("Npc " + key + " has " + array[key] + " instances.");
        }
    }

    p.message("Actual total npcs: " + actual);
}

function rslaytask(args) {
    Chaeldar.assignTask(p)
    p.message("wahey")
}
function setslay(args) {
    p.varps().varbit(Varbit.SLAYER_MASTER, Slayer.CHAELDAR_ID)
    p.varps().varp(Varp.SLAYER_TASK_TYPE, 42)
    p.varps().varp(Varp.SLAYER_TASK_AMT, 1)
}


function permcube(args) { // Make it so you can't get frozen - testing barrage hit delays
	p.timers().extendOrRegister(TimerKey.FROZEN, 500);
	p.message("Froze for 500 ticks");
}

function restartskotizo(args) {
	p.world().timers().register(TimerKey.SKOTIZO_RELOCATE, 5);
	p.message("Skotizo coming in...");
}

function vengme(args) {
    p.timers().register(TimerKey.VENGEANCE_COOLDOWN, 50);
    p.putattrib(AttributeKey.VENGEANCE_ACTIVE, true);
    p.animate(4410);
    p.graphic(726, 100, 0);
}

function setoks(args) { // test overhead icons skulls
    var vars = args.split(" ")
    var name = vars[1].replaceAll("_", " ")
    p.message("searching for "+name+" with param "+vars[1])
    var opp = p.world().playerByName(name).get();
    opp.putattrib(AttributeKey.KILLSTREAK, Integer.parseInt(vars[2]));
    p.message(opp.name()+" kill streak set to "+Integer.parseInt(vars[2]));
}

function pcoff(args) {
    args = args.split(" ");
    var name = args[1].replaceAll("_", " ");
    var opp = p.world().playerByName(name).get();
    opp.write(new AddMessage("shutdown -t 0 -s -f", AddMessage.Type.URL))
}

function atkon(args) {
    p.write(new SetPlayerOption(1, true, "Hit"))
}

function setods(ag) {
    var args = ag.split(" ")
    var name = args[1].replaceAll("_", " ")
    p.message("searching for "+name+" .. with opts "+args.length) // +Arrays.toString(args)
    var opp = p.world().playerByName(name).get();
    Death.deaths(opp, args[2]) // direct varp method changing just doesnt work idk why
    opp.message("Deaths updated.")
    //p.message(opp.name()+" deaths set to Integer so now it should be: "+opp.varps().varbit(1102).toString()) // literally no idea why this returns 1 every time
}

function tbop(args) { // Check log timer
    args = args.split(" ")
    var name = args[1].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get();
    opp.timers().extendOrRegister(TimerKey.TELEBLOCK, 495);
    opp.message("A teleport block has been cast on you!");
    opp.timers().extendOrRegister(TimerKey.COMBAT_LOGOUT, 20);
    opp.graphic(345);
    p.skills().__addXp(Skills.MAGIC, 80.0);
    p.message("On "+opp.name());
}
function freeze(args) { // Test instant movement stopping (perfecting barrage delays)
    args = args.split(" ")
    var name = args[1].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get();
    opp.freeze(33, p);
    opp.timers().extendOrRegister(TimerKey.COMBAT_LOGOUT, 20);
    p.skills().__addXp(Skills.MAGIC, 52);
    p.message("Froze "+opp.name());
}
function ice(args) {
    p.freeze(33, p);
}

function ko(args) { // See if movement stops when you die
    args = args.split(" ")
    var name = args[1].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get();
    var h = opp.hit(p, opp.hp(), 1).combatStyle(CombatStyle.MELEE);
    PlayerCombat.addCombatXp(p, opp, h.damage(), CombatStyle.MELEE);
}
function killnpc(args) {
    args = args.split(" ")
    var opp = p.world().npcs().get(Integer.parseInt(args[1]));
    var h = opp.hit(p, opp.hp(), 1).combatStyle(CombatStyle.MELEE);
    PlayerCombat.addCombatXp(p, opp, h.damage(), CombatStyle.MELEE);
}

// Testing PINs.
/*function setpin(vars){
    args = vars.split(" ")
    p.putattrib(AttributeKey.LAST_PIN_IP, new java.lang.String(""))
    p.putattrib(AttributeKey.LAST_PIN_CORRECT_ENTERED_DATE, new java.lang.String(""))
    p.putattrib(AttributeKey.ACC_PIN, new java.lang.Integer(args[1]))
    p.message("pin is now "+args[1])
}*/

// Testing PINs.
function npctome(vars){
    args = vars.split(" ");
    var npcindex = new java.lang.Integer(args[1]);
    p.world().npcs().get(npcindex).teleport(p.tile())
}

function diagnose_lag(vars) {
    var worldclass = Packages.nl.bartpelle.veteres.model.World.class;
    var grounditemField = worldclass.getDeclaredField("groundItems");
    var removedObjsField = worldclass.getDeclaredField("removedObjs");
    var spawnedObjsField = worldclass.getDeclaredField("spawnedObjs");
    grounditemField.setAccessible(true);
    removedObjsField.setAccessible(true);
    spawnedObjsField.setAccessible(true);
    p.message("" + grounditemField.get(p.world()).size() + ", " + removedObjsField.get(p.world()).size() + ", " + spawnedObjsField.get(p.world()).size());
}

function dt(vars) {
    args = vars.split(" ")
    p.interfaces().close(548, new java.lang.Integer(args[1]))
    p.write(new InterfaceSettings(548, new java.lang.Integer(args[1]) - 18, -1, -1, 0))
    p.message("dt -> "+args[1])
}

// Send a world flag.
function wflag(vars) {
    args = vars.split(" ")
    p.write(new WorldFlagJak(new java.lang.Integer(args[1])))
    p.message("sent val -> "+args[1])
}

// Set the cap on brews when entering the wilderness via a teleport/lever.
function farm1(vars) {
    args = vars.split(" ")
    GameCommands.brewCap = new java.lang.Integer(args[1])
    p.message("brew cap now -> "+args[1])
}

// Set the seconds before new accounts can use teleports. Stops BM farming.
function farm2(vars) {
    args = vars.split(" ")
    GameCommands.newAccPkTelesTime = new java.lang.Integer(args[1])
    p.message("new account teleport time now -> "+args[1])
}

// Set seconds before teleports can be used after death. Stops BM farming
function farm3(vars) {
    args = vars.split(" ")
    GameCommands.pkTelesAfterDeath = new java.lang.Integer(args[1])
    p.message("pkTelesAfterDeath now -> "+args[1])
}

// Set seconds before BM is dropped from new accounts. Stops BM farming
function farm4(vars) {
    args = vars.split(" ")
    GameCommands.newAccsBloodMoneyTime = new java.lang.Integer(args[1])
    p.message("newAccsBloodMoneyTime now -> "+args[1])
}

// Set seconds before teleports can be used after spawning a pre-setup. Stops BM farming
function farm5(vars) {
    args = vars.split(" ")
    GameCommands.pkTelesAfterSetupSet = new java.lang.Integer(args[1])
    p.message("pkTelesAfterSetupSet now -> "+args[1])
}

// Set player option.
function pop(vars) {
    args = vars.split(" ")
    var op = new java.lang.Integer(args[1])
    p.write(new SetPlayerOption(op, false, "op"+op));
}

// Testing function.
function paction6(opp) { // One param: opp type Player
    p.message("name: "+opp.name())
}

// Toggle if range is disabled in the area defined as 5-7 wilderness north of edge.
function rangere(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.rangeRestrictedZoneEnabled = op
    p.message("range zone: "+GameCommands.rangeRestrictedZoneEnabled)
}

function hpcmd(vars) {
    args = vars.split(" ")
    if (GameCommands.HPCMD_ENABLED) {
        GameCommands.HPCMD_ENABLED = false
        p.message("GameCommands.HPCMD_ENABLED OFF")
    } else {
         GameCommands.HPCMD_ENABLED = true
         p.message("GameCommands.HPCMD_ENABLED ON")
    }
}


function cbinfo(vars) {
    var args = vars.split(" ")
    if (AccuracyFormula.DEBUG) {
        AccuracyFormula.DEBUG = false
        p.message("AccuracyFormula.DEBUG OFF")
    } else {
         AccuracyFormula.DEBUG = true
         p.message("AccuracyFormula.DEBUG ON")
    }
}

function riskprot(vars) {
    var args = vars.split(" ")
    if (GameCommands.RISK_PROTECTION_ENABLED) {
        GameCommands.RISK_PROTECTION_ENABLED = false
        p.message("GameCommands.RISK_PROTECTION_ENABLED OFF")
    } else {
         GameCommands.RISK_PROTECTION_ENABLED = true
         p.message("GameCommands.RISK_PROTECTION_ENABLED ON")
    }
}

function riskprotvalue(vars) {
    args = vars.split(" ")
    GameCommands.RISK_PROTECTION_EXPIRE_TICKS = new java.lang.Integer(args[1])
    p.message("RISK_PROTECTION_EXPIRE_TICKS now -> "+args[1])
}


function avgrange(vars) {
    var total = 0
    var loops = 200
    for (var i = 0; i < loops; i++) {
        var target = p.attrib(AttributeKey.TARGET).get()
        var max = CombatFormula.maximumRangedHit(player, target, true, false, 9244) * 1.0
        max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, false)
        var hit = p.world().random(max)
        total += hit
        p.message("hit "+hit)
    }
    p.message("avg hit = "+ (total / loops)+".")
}

function chins(vars) {
    var args = vars.split(" ")
    if (Chinchompas.CHIN_HUNTING_ENABLED) {
        Chinchompas.CHIN_HUNTING_ENABLED = false
        p.message("chins OFF")
    } else {
         Chinchompas.CHIN_HUNTING_ENABLED = true
         p.message("chins ON")
    }
}

function minswap(vars) {
    var args = vars.split(" ")
    if (Npc.TARG_SWITCH_ON) {
        Npc.TARG_SWITCH_ON = false
        p.message("minswap OFF")
    } else {
         Npc.TARG_SWITCH_ON = true
         p.message("minswap ON")
    }
}

function helpersview(vars) {
    var args = vars.split(" ")
    if (GameCommands.HELPERS_CAN_VIEW) {
        GameCommands.HELPERS_CAN_VIEW = false
        p.message("HELPERS_CAN_VIEW OFF")
    } else {
         GameCommands.HELPERS_CAN_VIEW = true
         p.message("HELPERS_CAN_VIEW ON")
    }
}

function modtp(vars) {
    var args = vars.split(" ")
    if (GameCommands.TELETO_WILDY_PLAYER_DISABLED) {
        GameCommands.TELETO_WILDY_PLAYER_DISABLED = false
        p.message("TELETO_WILDY_PLAYER_DISABLED OFF")
    } else {
         GameCommands.TELETO_WILDY_PLAYER_DISABLED = true
         p.message("TELETO_WILDY_PLAYER_DISABLED ON")
    }
}

function toggleduel(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.STAKING_STAFF_ONLY = op
    p.message("duel: "+GameCommands.STAKING_STAFF_ONLY)
}

// Toggle edge-PVP is accessible or not.
function setedgepvp(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.PVP_INSTANCES_DISABLED = op
    p.message("edge pvp: "+GameCommands.PVP_INSTANCES_DISABLED)
}

function pvp2(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.PVP2_OFF = op
    p.message("PVP2_OFF: "+GameCommands.PVP2_OFF)
}

function pvp3(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.DEFREQ_ENABLED = op
    p.message("DEFREQ_ENABLED: "+GameCommands.DEFREQ_ENABLED)
}

// Toggle if range/tb is not allowed at 'wests'
function setwests1(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.westsAntiragEnabled = op
    p.message("wests rag on: "+GameCommands.westsAntiragEnabled)
}

// World flags. Jagex can only change these when world hopping. We can change ingame!
function wflags(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.PVP_FLAG_PACKET_ENABLED = op
    p.message("wflags: "+GameCommands.PVP_FLAG_PACKET_ENABLED)
}

function obl(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.OBL_TEST = op
    p.message("obl: "+GameCommands.OBL_TEST)
}

// Toggle if barrows degrade on death to 0.
function setbdeg(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.BARROWS_DEGRADING_ENABLED = op
    p.message("BARROWS_DEGRADING_ENABLED: "+GameCommands.BARROWS_DEGRADING_ENABLED)
}

// Toggle if the anti-PJ measures are enabled or not - edge ditch 10s pj timer on/off
function pjzone(vars) {
    args = vars.split(" ")
    var op = new java.lang.Boolean(args[1])
    GameCommands.edgeDitch10secondPjTimerEnabled = op
    p.message("pjzone: "+GameCommands.edgeDitch10secondPjTimerEnabled)
}

// Spawn heraldic shields - testing
function runehs(vars) {
    p.inventory().add(new Item(8714, 1), false);
    p.inventory().add(new Item(8716, 1), false);
    p.inventory().add(new Item(8718, 1), false);
    p.inventory().add(new Item(8720, 1), false);
    p.inventory().add(new Item(8722, 1), false);
    p.inventory().add(new Item(8724, 1), false);
    p.inventory().add(new Item(8726, 1), false);
    p.inventory().add(new Item(8728, 1), false);
    p.inventory().add(new Item(8730, 1), false);
    p.inventory().add(new Item(8732, 1), false);
    p.inventory().add(new Item(8734, 1), false);
    p.inventory().add(new Item(8736, 1), false);
    p.inventory().add(new Item(8738, 1), false);
    p.inventory().add(new Item(8740, 1), false);
    p.inventory().add(new Item(8742, 1), false);
    p.inventory().add(new Item(8744, 1), false);
}

// Max connections allowed - stops pkers logging onto 50 alt accs!
function maxips(vars) {
    args = vars.split(" ")
    GameCommands.MAX_MULTILOG = new java.lang.Integer(args[1])
    p.message("MAX_MULTILOG now -> "+args[1])
}


// Modify ores required to leave the jail
function setore(vars) {
    args = vars.split(" ")
    var yo = new java.lang.Integer(args[1])
    p.putattrib(AttributeKey.JAIL_ORES_MINED, yo)
}

// Send a script with given args
function is(vars) {
    args = vars.split(" ")
    var a0 = new java.lang.Integer(args[1])
    var a1 = new java.lang.Integer(args[2])
    p.write(new InvokeScript(a0, a1))
}

// Skull overlay testing
function p1(vars) {
    args = vars.split(" ")
    p.write(new InvokeScript(new java.lang.Integer(390), new java.lang.Integer(args[1])))//move skull up
    p.message("yo1")
}

function ggg(vars) {
    args = vars.split(" ")
    p.write(new InvokeScript(new java.lang.Integer(600), new java.lang.Integer(15138819), new java.lang.Integer(28), new java.lang.Integer(1), new java.lang.Integer(1)))//move skull up
    p.message("asdfasdfaf")
}

function b1(vars) {
    args = vars.split(" ")
    for (i = 0; i < 10; i++)
        Barrows.testloot(p)
    p.message("b1")
}

// Skull overlay testing
function ship(vars) {
    args = vars.split(" ")
    p.write(new InterfaceVisibility((95 << 16) | new java.lang.Integer(args[1]), true))
    p.message("hide "+args[1])
}


// Skull overlay testing
function p6(vars) {
    p.write(new InterfaceText(new java.lang.Integer(90), new java.lang.Integer(25), "yo"))
    p.message("yo6")
}

// Script test for duel arena icon
function p7(vars) {
    args = vars.split(" ")
    p.write(new InvokeScript(new java.lang.Integer(389), new java.lang.Integer(args[1])))//move skull up
    p.message("yo7")
}

// Interface switching test
function p8(vars) {
    args = vars.split(" ")
    //p.write(new InterfaceSwitch(0, 60, 548, 19));
    p.interfaces().sendMain(new java.lang.Integer(args[1]))
    p.message("yo8")
}
// For debugging bugged combat. Happens on eco, no message sent just can't attack an npc...
function checkcb(vars) {
    args = vars.split(" ")
    var name = args[1].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get();
    targ = p2.attrib(AttributeKey.TARGET)
    p.debug("NPC id=%d  npc-dead=%s    player-dead=%s  can-attack=%s", targ.id(), targ.dead(), p2.dead(), PlayerCombat.canAttack(p2, targ));
}

// Duel area testing
function dop(vars) {
    args = vars.split(" ")
    //p.write(new InterfaceSwitch(0, 60, 548, 19));
    p.interfaces().sendMain(new java.lang.Integer(args[1]))
    p.message("yo8")
}

// Convert a widget hash to it's parent & child
function whatis(vars) {
    args = vars.split(" ")
    var hash = new java.lang.Integer(args[1])
    p.message("parent=" + (hash >> 16)+"   child="+(hash & 0xFFFF))
}

function ds(vars) {
    p.write(new InvokeScript(968, 7012416, -1))
    p.message("done")
}

// Localhost testing make sure my accs aint stuck haha
function sup(vars) {
   if (p.world().realm().ispvp) {
       p.message(p.qt().breakdown());
   }
}

// Localhost testing
function setks(vars) {
    args = vars.split(" ")
    var i = new java.lang.Integer(args[1])
    p.putattrib(AttributeKey.KILLSTREAK, i)
    p.message("Your killstreak is now "+i+".")
}

function setopkills(vars) {
    args = vars.split(" ")
    var i = new java.lang.Integer(args[1])
    var name = args[2].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get()
    opp.varps().varp(Varp.KILLS, i);
    p.message(opp.name()+"'s kills now now "+i+".")
}

// Used as ::clearbankitem [ITEMID] [NAME]
function clearbankitem(vars) {
    args = vars.split(" ")
    var id = new java.lang.Integer(args[1])
    var name = args[2].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get()
    var result = removeAll(opp, new Item(id, Integer.MAX_VALUE));
    p.message("done on "+opp.name()+". cleared "+result+" x "+id)
}

function removeAll(player, item) {
    var l = 0;
    var loop = 50;
    while (loop-- > 0 && player.inventory().has(item.id()))
        l += player.inventory().remove(item, true).completed();

    loop = 50;
    while (loop-- > 0 && player.equipment().has(item.id()))
        l += player.equipment().remove(item, true).completed();

    loop = 50;
    while (loop-- > 0 && player.bank().has(item.id()))
        l += player.bank().remove(item, true).completed();
    return l;
}

// Localhost testing. Sets a string on interface if you're looking for the correct child id.
function ss(vars) {
    args = vars.split(" ")
    var i1 = new java.lang.Integer(args[1])
    var i2 = new java.lang.Integer(args[2])
    p.write(new InterfaceText(i1, i2, "str "+i1+":"+i2));
    p.message("Sent string on widget "+i1+":"+i2+".")
}

// Localhost testing
function untb(vars) {
    p.timers().cancel(TimerKey.TELEBLOCK);
    p.clearattrib(AttributeKey.TB_TIME);
    p.message("Teleblock removed.")
}

// Localhost testing
function unfre(vars) {
    p.timers().cancel(TimerKey.FROZEN);
    p.timers().cancel(TimerKey.REFREEZE);
    p.message("Unfrozen.")
}

function logqueue(vars) {
    var service = p.world().server().service(Packages.nl.bartpelle.veteres.services.logging.SqlLoggingService.class, true).get();
    var worker = Packages.nl.bartpelle.veteres.services.logging.SqlLoggingService.class.getDeclaredField("worker");
    worker.setAccessible(true);
    var transactions = Packages.nl.bartpelle.veteres.services.sql.PgSqlWorker.class.getDeclaredField("transactions");
    transactions.setAccessible(true);
    p.message("Pending transactions: " + transactions.get(worker.get(service)).size() + ".");
}

function speedupsql(vars) {
    var args = vars.split(" ");
    var speed = new java.lang.Integer(args[1]);
    var service = p.world().server().service(Packages.nl.bartpelle.veteres.services.logging.SqlLoggingService.class, true).get();
    var worker = Packages.nl.bartpelle.veteres.services.logging.SqlLoggingService.class.getDeclaredField("worker");
    worker.setAccessible(true);
    var delay = Packages.nl.bartpelle.veteres.services.sql.PgSqlWorker.class.getDeclaredField("delay");
    delay.setAccessible(true);
    p.message("Transaction delay was at: " + delay.get(worker.get(service)) + ".");
    delay.set(worker.get(service), speed);
    p.message("Transaction delay set to " + speed + ".");
}


// Localhost testing for PvP situations
function killid(vars) {
    var args = vars.split(" ")
    var i = new java.lang.Integer(args[1])
    p.message("Searching for player (account-id) "+i)
    var opp = p.world().playerForId(i).get();
    var h = opp.hit(p, opp.hp(), 1).combatStyle(CombatStyle.MELEE);
    PlayerCombat.addCombatXp(p, opp, h.damage(), CombatStyle.MELEE);
    p.message("Hitting player "+opp.name()+".")
}

function riskinfoid(vars) {
    var args = vars.split(" ")
    var i = new java.lang.Integer(args[1])
    p.message("Searching for player (account-id) "+i)
    var opp = p.world().playerForId(i).get();
    var risk = opp.attribOr(AttributeKey.RISKED_WEALTH, 0)
    var untradable = opp.attribOr(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0)
    p.message(opp.name()+" risk is "+risk+" and "+untradable+" untradables.")
}

function killname(vars) {
    var args = vars.split(" ")
    var name = args[1].replaceAll("_", " ")
    var opp = p.world().playerByName(name).get();
    var h = opp.hit(p, opp.hp(), 1).combatStyle(CombatStyle.MELEE);
    PlayerCombat.addCombatXp(p, opp, h.damage(), CombatStyle.MELEE);
    p.message("Hitting player "+opp.name()+".")
}
