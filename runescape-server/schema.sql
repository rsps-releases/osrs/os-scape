create extension if not exists citext;

create table if not exists accounts
(
	id serial primary key,
	username citext not null
		constraint accounts_username_key
			unique,
	password varchar(60) not null,
	rights smallint default 0,
	email varchar(127),
	muted_until timestamp,
	banned_until timestamp,
	displayname varchar(12),
	premium_until timestamp,
	lastname varchar(12)
		constraint accounts_lastname_key
			unique,
	forum_id integer
		constraint accounts_forum_id_key
			unique,
	premium_points integer default 0,
	premium_rank integer default 0,
	clanname varchar(12) default NULL::character varying,
	joinreq smallint default 0,
	talkreq smallint default 0,
	kickreq smallint default 6,
	logged boolean default false,
	default_icon integer default 0,
	twofactor_key text,
	helper boolean default false,
	registered_at timestamp default now(),
	email_verification text,
	email_verified boolean default false,
	last_ip varchar(16),
	last_uuid text,
	last_hwid text,
	last_online timestamp with time zone,
	extra_usd double precision default 0,
	deadman_participant boolean default false,
	seniormod boolean default false,
	shadowmuted boolean default false
);

create index if not exists accounts_displayname_index
	on accounts (displayname);

create index if not exists accounts_lower_idx
	on accounts (lower(displayname::text));

create index if not exists accounts_lower_idx1
	on accounts (lower(email::text));

create index if not exists accounts_forum_id_idx
	on accounts (forum_id);

create table if not exists cart_items
(
	owner_id integer,
	added_on timestamp default now(),
	product_id integer,
	quantity integer default 1
);

create index if not exists cart_items_owner_id_index
	on cart_items (owner_id);

create table if not exists characters
(
	id serial primary key,
	account_id integer not null
		constraint characters_account_id_fkey
			references accounts,
	service_id integer default 0,
	x integer default 3094,
	z integer default 3107,
	level smallint default 0,
	inventory jsonb default '{"items": []}'::jsonb,
	equipment jsonb default '{"items": []}'::jsonb,
	bank jsonb default '{"items": []}'::jsonb,
	skills jsonb default '{"xp": [0.0, 0.0, 0.0, 1154.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], "level": [1, 1, 1, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]}'::jsonb,
	pkp integer default 0,
	varps jsonb default '[{"id": 18, "val": 1}, {"id": 166, "val": 2}, {"id": 447, "val": -1}, {"id": 486, "val": 1073741824}, {"id": 520, "val": 1}, {"id": 553, "val": -2147483648}, {"id": 788, "val": 128}, {"id": 810, "val": 33554432}, {"id": 849, "val": -1}, {"id": 850, "val": -1}, {"id": 851, "val": -1}, {"id": 852, "val": -1}, {"id": 853, "val": -1}, {"id": 854, "val": -1}, {"id": 855, "val": -1}, {"id": 856, "val": -1}, {"id": 904, "val": 246}, {"id": 913, "val": 4194304}, {"id": 1010, "val": 2048}, {"id": 1017, "val": 8192}, {"id": 1050, "val": 4096}, {"id": 1065, "val": -1}, {"id": 1067, "val": -1683488768}, {"id": 1075, "val": -1}, {"id": 1151, "val": -1}, {"id": 1224, "val": 172395585}, {"id": 1225, "val": 379887846}, {"id": 1226, "val": 12}]'::jsonb,
	playtime integer default 0,
	ironmode integer default 0,
	attribs jsonb default '[]'::jsonb,
	gamemode integer default 1,
	migration integer default 0,
	gender smallint default 0,
	looks integer[] default '{0,10,18,26,33,36,42}'::integer[],
	colors integer[] default '{3,16,16,0,0}'::integer[],
	pmstatus integer default 0,
	constraint account_service
		unique (account_id, service_id)
);

create table if not exists command_log
(
	account_id integer
		constraint command_log_account_id_fkey
			references accounts,
	world_id integer,
	x integer,
	z integer,
	level integer,
	command citext,
	ip varchar(16),
	time timestamp default now(),
	command_privilege integer
);

create index if not exists command_log_account_id_index
	on command_log (account_id);

create table if not exists cp_access_log
(
	id serial primary key,
	account_id integer,
	url text,
	time timestamp,
	ip text,
	panel_version integer default 1
);

create index if not exists cp_access_log_account_id_index
	on cp_access_log (account_id);

create table if not exists credit_transactions
(
	id serial primary key,
	time timestamp default now(),
	ip varchar(16),
	account_id integer,
	cost integer default 0,
	item integer,
	amount integer,
	acquired boolean default false,
	service_id integer,
	product_id integer,
	price_usd double precision default 0,
	invoice_id integer default 0
);

create index if not exists credit_transactions_account_id_index
	on credit_transactions (account_id);

create index if not exists credit_transactions_item_index
	on credit_transactions (item);

create index if not exists credit_transactions_time_index
	on credit_transactions (time);

create table if not exists shop_products
(
	id integer not null
		constraint shop_products_pkey
			primary key,
	name text,
	description text,
	price integer,
	image text,
	item_id integer,
	item_amount integer,
	service_id integer,
	sort_order double precision default 1,
	usd_price double precision default 1.00,
	disabled boolean default false,
	discount_price double precision default 0.0,
	original_price integer default 0
);

create table if not exists drop_logs
(
	taker_id integer,
	dropper_id integer,
	taker_ip varchar(16),
	dropper_ip varchar(16),
	id integer,
	amount integer,
	value bigint,
	x integer,
	z integer,
	level integer,
	world integer,
	time timestamp with time zone default now(),
	dropper_uuid text,
	taker_uuid text,
	dropper_hwid text,
	taker_hwid text
);

create index if not exists drop_logs_dropper_id_index
	on drop_logs (dropper_id);

create index if not exists drop_logs_taker_id_index
	on drop_logs (taker_id);

create table if not exists dupewatch_triggers
(
	id serial primary key,
	account_id integer,
	old_inventory bigint,
	old_equipment bigint,
	old_bank bigint,
	new_inventory bigint,
	new_equipment bigint,
	new_bank bigint,
	time timestamp default now(),
	world integer,
	x integer,
	z integer,
	level integer,
	processed_by integer,
	ip varchar(16)
);

create index if not exists dupewatch_triggers_account_id_index
	on dupewatch_triggers (account_id);

create table if not exists eco_highscores
(
	account_id integer not null,
	skill integer not null,
	character_id integer,
	xp integer,
	last_updated timestamp default now(),
	game_mode integer,
	iron_mode integer,
	lvl integer,
	service_id integer default 1
);

create index if not exists eco_highscores_account_id_skill_index
	on eco_highscores (account_id, skill);

create index if not exists eco_highscores_game_mode_index
	on eco_highscores (game_mode);

create index if not exists eco_highscores_iron_mode_index
	on eco_highscores (iron_mode);

create index if not exists eco_highscores_service_id_index
	on eco_highscores (service_id);

create table if not exists exchange_offers
(
	id bigserial primary key,
	account_id integer,
	service_id integer,
	slot smallint,
	is_sell boolean,
	item_id integer,
	price_per integer,
	requested integer,
	completed integer,
	spent integer,
	pending_coins integer,
	pending_items integer,
	cancelled boolean default false,
	processed boolean default false,
	created_at timestamp with time zone default now(),
	updated_at timestamp with time zone default now()
);

create index if not exists exchange_offers_account_id_idx
	on exchange_offers (account_id);

create index if not exists exchange_offers_item_id_is_sell_idx
	on exchange_offers (item_id, is_sell);

create index if not exists exchange_offers_processed_idx
	on exchange_offers (processed);

create index if not exists exchange_offers_service_id_idx
	on exchange_offers (service_id);

create table if not exists friends
(
	account_id integer
		constraint friends_account_id_fkey
			references accounts
				on delete cascade,
	friend_id integer
		constraint friends_friend_id_fkey
			references accounts
				on delete cascade,
	clanrank integer default 0
);

create unique index if not exists friends_account_id_friend_id_uindex
	on friends (account_id, friend_id);

create index if not exists friends_friend_id_index
	on friends (friend_id);

create table if not exists gametime_statistics
(
	id bigserial primary key,
	account_id integer,
	time timestamp default now(),
	gametime bigint
);

create index if not exists gametime_statistics_account_id_idx
	on gametime_statistics (account_id);

create index if not exists gametime_statistics_time_index
	on gametime_statistics (time);

create table if not exists homepage_news
(
	id serial primary key,
	title text,
	body text,
	time timestamp with time zone,
	by text,
	url text
);

create table if not exists hwid_bans
(
	hwid citext not null
		constraint hwid_bans_pkey
			primary key,
	time timestamp default now(),
	account_id integer,
	by_account_id integer
);

create index if not exists hwid_bans_hwid_index
	on hwid_bans (hwid);

create table if not exists ignores
(
	account_id integer
		constraint ignores_account_id_fkey
			references accounts
				on delete cascade,
	friend_id integer
		constraint ignores_friend_id_fkey
			references accounts
				on delete cascade
);

create unique index if not exists ignores_account_id_friend_id_uindex
	on ignores (account_id, friend_id);

create index if not exists ignores_friend_id_index
	on ignores (friend_id);

create table if not exists invoice_items
(
	id serial primary key,
	account_id integer,
	quantity integer,
	added_on timestamp default now(),
	invoice_id integer,
	product_id integer
);

create index if not exists invoice_items_account_id_index
	on invoice_items (account_id);

create index if not exists invoice_items_invoice_id_index
	on invoice_items (invoice_id);

create table if not exists invoices
(
	id serial primary key,
	account_id integer,
	time timestamp default now(),
	total_price double precision,
	ip varchar(16),
	total_items integer,
	paid boolean default false,
	payment_method varchar(64),
	paid_on timestamp,
	synched boolean default false,
	ignore_price boolean default false
);

create index if not exists invoices_account_id_index
	on invoices (account_id);

create index if not exists invoices_ip_index
	on invoices (ip);

create index if not exists invoices_paid_index
	on invoices (paid);

create index if not exists invoices_time_index
	on invoices (time);

create table if not exists item_definitions
(
	id integer not null
		constraint item_definitions_id_pk
			primary key,
	name text,
	noted boolean default false,
	cost integer,
	stackable boolean,
	placeholder boolean default false,
	bm_value integer default 0,
	exchange boolean default false not null,
	exchange_price integer default 0,
	exchange_price_w3 integer default 1
);

create index if not exists item_definitions_name_index
	on item_definitions (name);

create unique index if not exists item_definitions_id_uindex
	on item_definitions (id);

create table if not exists logins
(
	account_id integer,
	time timestamp default now(),
	ip varchar(16),
	world integer,
	uuid text,
	hwid text
);

create index if not exists logins_account_id_index
	on logins (account_id);

create index if not exists logins_ip_index
	on logins (ip);

create index if not exists logins_time_index
	on logins (time desc);

create table if not exists lottery
(
	world_id integer not null
		constraint lottery_world_id_pk
			primary key,
	currency_id integer not null,
	deposit_modifier numeric,
	deposit_modifier_expire timestamp,
	withdraw_modifier numeric,
	withdraw_modifier_expire timestamp,
	entrants jsonb,
	enddate bigint default '-1'::integer not null
);

create unique index if not exists lottery_world_id_uindex
	on lottery (world_id);

create table if not exists lottery_log
(
	account_id integer
		constraint lottery_log_account_id_fkey
			references accounts,
	world_id integer not null,
	amt bigint,
	previous_amount bigint,
	ip varchar(16),
	time timestamp default now(),
	currency integer,
	type varchar(16)
);

create table if not exists lottery_winners
(
	account_id integer
		constraint lottery_winners_account_id_fkey
			references accounts,
	world_id integer,
	amt bigint,
	time timestamp default now()
);

create table if not exists member_notes
(
	id serial primary key,
	note text,
	time timestamp default now(),
	account_id integer,
	by_id integer
);

create index if not exists member_notes_account_id_index
	on member_notes (account_id);

create table if not exists modcp_notifications
(
	id serial primary key,
	account_id integer,
	title text,
	message text,
	icon text,
	icon_col text,
	read boolean default false,
	time timestamp default now(),
	path text
);

create index if not exists modcp_notifications_account_id_index
	on modcp_notifications (account_id);

create table if not exists mollie_log
(
	id serial,
	invoice_id int primary key,
	jsondata text
);

create index if not exists mollie_log_invoice_id_index
	on mollie_log (invoice_id);

create table if not exists name_reservations
(
	id serial primary key,
	account_id integer,
	name text,
	time timestamp default now(),
	ip varchar(16),
	expires timestamp default (now() + '7 days'::interval)
);

create index if not exists name_reservations_account_id_index
	on name_reservations (account_id);

create index if not exists name_reservations_name_index
	on name_reservations (name);

create table if not exists newsletter_emails
(
	email text not null
		constraint newsletter_emails_pkey
			primary key,
	unregister_key text default lower(md5((random())::text)),
	unregistered boolean default false,
	bounced boolean default false,
	delivered boolean default false,
	username text,
	id bigserial,
	opened boolean default false,
	clicked boolean default false
);

create index if not exists newsletter_emails_id_index
	on newsletter_emails (id);

create table if not exists newsletter_jobs
(
	id serial primary key,
	subject text,
	html_body text,
	text_body text,
	created_on timestamp with time zone default now(),
	opens integer default 0
);

create table if not exists newsletter_mail_queue
(
	job_id integer,
	email_id bigint,
	completed boolean default false,
	id bigserial primary key
);

create table if not exists newsletter_subscriptions
(
	email text not null
		constraint newsletter_subscriptions_pkey
			primary key,
	registered_at timestamp with time zone default now()
);

create table if not exists npc_kills
(
	character_id integer
		constraint npc_kills_characters_id_fk
			references characters,
	npc_id integer,
	kills integer default 0
);

create index if not exists npc_kills_character_id_npc_id_index
	on npc_kills (character_id, npc_id);

create table if not exists online_characters
(
	account_id integer not null
		constraint online_characters_account_id_key
			unique
		constraint online_characters_account_id_fkey
			references accounts,
	character_id integer not null,
	service_id integer default 0,
	world_id integer not null,
	since timestamp default now(),
	ip varchar(16),
	clanchat integer default 0,
	uuid text
);

create index if not exists online_characters_clanchat_idx
	on online_characters (clanchat);

create table if not exists online_statistics
(
	id serial primary key,
	time timestamp default now(),
	world integer,
	players integer
);

create index if not exists online_statistics_date_trunc_idx
	on online_statistics (date_trunc('day'::text, "time"));

create index if not exists online_statistics_players_index
	on online_statistics (players);

create index if not exists online_statistics_time_idx
	on online_statistics (time);

create index if not exists online_statistics_time_idx1
	on online_statistics (time);

create index if not exists online_statistics_world_index
	on online_statistics (world);

create table if not exists packet_logs
(
	id serial primary key,
	account_id integer,
	ip varchar(16),
	uuid text,
	packet_id integer,
	packet_size integer,
	description text,
	time timestamp default now(),
	system_time bigint,
	x integer,
	z integer,
	level integer
);

create index if not exists packet_logs_account_id_idx
	on packet_logs (account_id);

create index if not exists packet_logs_time_idx
	on packet_logs (time);

create index if not exists packet_logs_uuid_idx
	on packet_logs (uuid);

create table if not exists password_requests
(
	id serial primary key,
	account_id integer,
	requested_by integer,
	completed_by integer,
	password text,
	time timestamp,
	completed_on timestamp,
	reason text
);

create table if not exists play_evolutions
(
	id integer not null
		constraint play_evolutions_pkey
			primary key,
	hash varchar(255) not null,
	applied_at timestamp not null,
	apply_script text,
	revert_script text,
	state varchar(255),
	last_problem text
);

create table if not exists preset_items
(
	preset_id integer,
	account_id integer,
	item_id smallint,
	amount integer,
	slot smallint,
	equipment boolean
);

create index if not exists preset_items_account_id_index
	on preset_items (account_id);

create index if not exists preset_items_preset_id_index
	on preset_items (preset_id);

create table if not exists presets
(
	id serial primary key,
	account_id integer,
	name text
);

create index if not exists presets_account_id_index
	on presets (account_id);

create index if not exists presets_id_index
	on presets (id);

create table if not exists private_messages
(
	from_account_id integer
		constraint private_messages_accounts_id_fk
			references accounts,
	to_account_id integer
		constraint private_messages_accounts_id_fk2
			references accounts,
	from_x integer,
	from_z integer,
	from_level integer,
	from_world integer,
	to_world integer,
	message citext,
	from_ip varchar(16),
	time timestamp default now()
);

create index if not exists private_messages_from_account_id_to_account_id_index
	on private_messages (from_account_id, to_account_id);

create index if not exists private_messages_time_idx
	on private_messages (time desc);

create index if not exists private_messages_to_account_id_from_account_id_index
	on private_messages (to_account_id, from_account_id);

create table if not exists public_chats
(
	account_id integer
		constraint public_chats_accounts_id_fk
			references accounts,
	world_id integer,
	x integer,
	z integer,
	level integer,
	message citext,
	ip varchar(16),
	time timestamp default now()
);

create index if not exists public_chats_account_id_index
	on public_chats (account_id);

create table if not exists purchases
(
	id serial primary key,
	bmt_id integer default 0,
	time timestamp default now(),
	ip varchar(16),
	account_id integer,
	price text,
	product integer,
	quantity integer,
	email text,
	processed boolean default false,
	twocheckout_id bigint default 0
);

create index if not exists purchases_account_id_index
	on purchases (account_id);

create table if not exists pvp_highscores
(
	account_id integer not null
		constraint pvp_highscores_pkey
			primary key,
	kills integer,
	deaths integer,
	kdr double precision,
	shutdown integer,
	updated_on timestamp default now(),
	killstreak integer,
	killstreak_top integer
);

create unique index if not exists pvp_highscores_account_id_uindex
	on pvp_highscores (account_id);

create table if not exists stake_items
(
	stake_id integer,
	account_id integer,
	item_id integer,
	amount integer
);

create index if not exists stake_items__index
	on stake_items (item_id);

create index if not exists stake_items_stake_id_index
	on stake_items (stake_id);

create table if not exists stakes
(
	id serial primary key,
	winner_id integer,
	loser_id integer,
	winner_value bigint,
	loser_value bigint,
	winner_ip text,
	loser_ip text,
	world integer,
	time timestamp default now(),
	winner_uuid text,
	loser_uuid text
);

create index if not exists stakes_loser_id_index
	on stakes (loser_id);

create index if not exists stakes_time_index
	on stakes (time);

create index if not exists stakes_winner_id_index
	on stakes (winner_id);

create table if not exists timers
(
	id serial primary key,
	character_id integer,
	timer integer,
	ticks integer,
	persisted_on bigint
);

create table if not exists trades
(
	id serial primary key,
	trader1_account_id integer,
	trader2_account_id integer,
	trader1_value bigint,
	trader2_value bigint,
	trader1_ip varchar(16),
	trader2_ip varchar(16),
	time timestamp default now(),
	world integer,
	x integer,
	z integer,
	level integer
);

create index if not exists trades_time
	on trades (time);

create index if not exists trades_trader_account2
	on trades (trader2_account_id);

create index if not exists trades_trader_accounts
	on trades (trader1_account_id, trader2_account_id);

create index if not exists trades_trader_ip1
	on trades (trader1_ip);

create index if not exists trades_trader_ip2
	on trades (trader2_ip);

create table if not exists trade_items
(
	trade_id integer
		constraint trade_items_trade_id_fkey
			references trades,
	account_id integer,
	item_id integer,
	amount bigint
);

create index if not exists trade_items_item_id
	on trade_items (item_id);

create index if not exists trade_items_trade_id
	on trade_items (trade_id);

create table if not exists twofactor_invites
(
	id text not null
		constraint twofactor_invites_pkey
			primary key,
	code text,
	generated_on timestamp default now(),
	expires_on timestamp default (now() + '24:00:00'::interval),
	player_name text,
	player_name_url text
);

create index if not exists twofactor_invites_id_index
	on twofactor_invites (id);

create table if not exists twofactor_whitelist
(
	id serial primary key,
	account_id integer,
	ip varchar(16),
	added_on timestamp,
	last_used timestamp
);

create index if not exists twofactor_whitelist_account_id_index
	on twofactor_whitelist (account_id);

create index if not exists twofactor_whitelist_ip_index
	on twofactor_whitelist (ip);

create table if not exists uuid_bans
(
	uuid text not null
		constraint uuid_bans_pkey
			primary key,
	time timestamp default now(),
	account_id integer,
	by_account_id integer
);

create table if not exists web_settings
(
	key varchar(32) not null
		constraint web_settings_pkey
			primary key,
	string_value text,
	int_value integer,
	boolean_value boolean,
	double_value double precision
);

create index if not exists web_settings_key_index
	on web_settings (key);

create table if not exists worlds
(
	id integer not null
		constraint worlds_pkey
			primary key,
	name text,
	boost_multiplier double precision default 1.0,
	ip text,
	public boolean default false,
	sort_order integer default 1,
	flags integer default 0
);

create unique index if not exists worlds_id_uindex
	on worlds (id);

create table if not exists stripe_log
(
	id serial primary key,
	invoice_id integer,
	jsondata text,
	time timestamp default now()
);

create index if not exists stripe_log_invoice_id_index
	on stripe_log (invoice_id);

create table if not exists itemattribs
(
	character_id integer,
	container varchar(30),
	slot integer,
	attribkey integer,
	attribvalue integer
);

create table if not exists item_prices
(
	itemid integer not null,
	price integer not null,
	serviceid integer not null
);

create index if not exists item_prices_itemid_index
	on item_prices (itemid);

create table if not exists exchange_logs
(
	buyer_id integer,
	seller_id integer,
	itemid integer,
	amount integer,
	cost_per integer,
	serviceid integer,
	date timestamp with time zone default now()
);

create table if not exists price_history
(
	serviceid integer,
	itemid integer,
	old_price integer,
	new_price integer,
	date timestamp with time zone default now()
);

create table if not exists settings
(
	key varchar(64),
	value varchar(256)
);

create table if not exists preset_levels
(
	preset_id integer,
	account_id integer,
	id smallint,
	level smallint
);

create index if not exists preset_levels_preset_id_index
	on preset_levels (preset_id);

create index if not exists preset_levels_account_id_index
	on preset_levels (account_id);

create table if not exists pk_items
(
	kill_id integer,
	item_id integer,
	item_amount integer
);

create table if not exists name_history
(
	id serial primary key,
	old text,
	new text,
	time timestamp with time zone,
	account_id integer,
	price integer default 0
);

create type container_type as enum ('inventory', 'equipment', 'bank', 'lootingbag');

create type packet_log_target as enum ('uuid', 'ip');

create type punish_reason as enum ('ddos', 'hacking', 'rwt', 'botting', 'scamming', 'boosting', 'advertising', 'offensive', 'swapping', 'privacy', 'threats', 'evasion', 'other', 'unspecified');

create type punish_type as enum ('ban', 'mute', 'unban', 'unmute', 'ipban', 'ipmute', 'unipban', 'unipmute', 'jail', 'unjail', 'uuidban', 'uuidunban', 'unshadowmute', 'shadowmute');

create table if not exists items
(
	uid bigserial primary key,
	character_id integer,
	id integer,
	amount integer,
	container container_type,
	slot integer
);

create index if not exists items_character_id_index
	on items (character_id);

create index if not exists items_id_index
	on items (id);

create table if not exists packet_log_targets
(
	id serial primary key,
	target text,
	target_type packet_log_target
);

create index if not exists packet_log_targets_target_idx
	on packet_log_targets (target);

create table if not exists punish_log
(
	id serial primary key,
	account_id integer,
	mod_id integer,
	reason punish_reason default 'unspecified'::punish_reason,
	time timestamp with time zone default now(),
	comment text default 'None provided'::text,
	from_cpanel boolean default false,
	type punish_type default 'mute'::punish_type
);

create index if not exists punish_log_account_id_idx
	on punish_log (account_id);

create index if not exists punish_log_mod_id_idx
	on punish_log (mod_id);

create index if not exists punish_log_reason_idx
	on punish_log (reason);

create index if not exists punish_log_time_idx
	on punish_log (time);

create table if not exists player_kills
(
	id serial primary key,
	killer_id integer,
	killed_id integer,
	time timestamp default now(),
	killer_ip varchar(16),
	killed_ip varchar(16),
	world integer,
	total_value bigint default 0,
	x integer,
	z integer,
	level integer
);

create unique index if not exists player_kills_id_uindex
	on player_kills (id);

create index if not exists player_kills_killer_id_idx
	on player_kills (killer_id);

create index if not exists player_kills_killed_id_idx
	on player_kills (killed_id);

create index if not exists player_kills_time_idx
	on player_kills (time desc);

create index if not exists player_kills_killer_ip_idx
	on player_kills (killer_ip);

create index if not exists player_kills_killed_ip_idx
	on player_kills (killed_ip);

create index if not exists player_kills_total_value_idx
	on player_kills (total_value);

create table if not exists refunds
(
	id serial primary key,
	username citext,
	password text,
	old_account_id integer,
	claimed_by integer,
	total_usd double precision,
	total_credits integer,
	credits_remaining integer,
	credits_per_chunk integer,
	last_claimed timestamp
);

create unique index if not exists refunds_username_idx
	on refunds (username);

create index if not exists refunds_claimed_by_idx
	on refunds (claimed_by);

create index if not exists refunds_old_account_id_idx
	on refunds (old_account_id);

create table if not exists refund_log
(
	uid serial primary key,
	account_id integer,
	credits integer,
	new_balance integer,
	from_account_id integer,
	time timestamp with time zone default now()
);

