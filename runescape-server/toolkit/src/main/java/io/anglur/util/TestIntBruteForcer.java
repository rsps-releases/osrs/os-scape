package io.anglur.util;

import io.anglur.cache.RSBuffer;
import io.anglur.map.MapUtils;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by Jonathan on 7/8/2015.
 */
public final class TestIntBruteForcer {
	
	
	public static void main(String[] args) {
		MapUtils.init();
		
		RSBuffer buffer = new RSBuffer(ByteBuffer.allocate(Short.MAX_VALUE));
		
		buffer.writeShort(100);
		
		
		buffer.writeInt(853711784);
		buffer.writeInt(241148978);
		buffer.writeInt(378704137);
		buffer.writeInt(781770156);
		
		buffer.writeInt(-1235220076);
		buffer.writeInt(-1734863631);
		buffer.writeInt(1661386917);
		buffer.writeInt(-967682427);
		
		buffer.writeInt(-1037938149);
		buffer.writeInt(435828222);
		buffer.writeInt(434919754);
		buffer.writeInt(610004394);
		
		buffer.writeInt(1439706299);
		buffer.writeInt(1142795502);
		buffer.writeInt(-1411116527);
		buffer.writeInt(-802607581);
		buffer.writeShortA(55);
		buffer.writeLEShortA(52);
		buffer.writeLEShort(415);
		System.out.println("Pos: " + buffer.position());
		buffer.writeShort(406);
		System.out.println("Pos: " + buffer.position());
		buffer.writeByte(3);
		
		parseMapRegionPacket(buffer);
	}
	
	public static void parseMapRegionPacket(RSBuffer b) {
		RSBuffer buffer = new RSBuffer(b.get().array());
		
		int size = buffer.readUShort();//Never changes
		if (size < 9) {
			return;
		}
		
		final int startPos = buffer.position();
		int currentPos = startPos;
		
		int[] xteas = new int[4];
		int[] indexs = new int[4];
		
		while (true) {
			try {
				//System.out.println(indexs[0] + ", " + indexs[1] + ", " + indexs[2] + ", " + indexs[3]);
				xteas[0] = readIntForIndex(buffer, indexs[0]);
				xteas[1] = readIntForIndex(buffer, indexs[1]);
				xteas[2] = readIntForIndex(buffer, indexs[2]);
				xteas[3] = readIntForIndex(buffer, indexs[3]);
				if (indexs[3] == 3) {
					indexs[0]++;
					indexs[1] = 0;
					indexs[2] = 0;
					indexs[3] = 0;
				} else if (indexs[2] == 3) {
					indexs[3]++;
				} else if (indexs[1] == 3) {
					indexs[2]++;
				} else {
					indexs[1]++;
				}
				
				
				System.out.println("Trying: " + Arrays.toString(xteas));
				int response = MapUtils.findMap(xteas);
				
				//System.out.println("Response nigger: " + response);
				if (response != -1) {
					System.out.println("Found xteas for region " + response + ", " + Arrays.toString(xteas));
					//receivedMaps.put(response, xteas);
					Arrays.fill(indexs, 0);
					continue;
				}
				
				buffer.rewind().position(currentPos);
				if (indexs[0] > 3) {
					buffer.rewind().position(currentPos++);
					//System.out.println("XTEA's not found, moving up another index!");
					Arrays.fill(indexs, 0);
				}
			} catch (BufferUnderflowException e) {
				break;
			}
		}
	}
	
	public static int readIntForIndex(RSBuffer buffer, int keyIdx) {
		switch (keyIdx) {
			case 1:
				return buffer.readIntegerV1();
			case 2:
				return buffer.readIntegerV2();
			case 3:
				return buffer.readIntegerLE();
			default:
				return buffer.readInt();
		}
	}
	
}
