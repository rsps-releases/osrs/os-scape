package io.anglur;

import io.anglur.cache.def.MapDefinition;
import io.anglur.cache.def.OverlayDefinitions;
import io.anglur.cache.def.TextureLoader;
import io.anglur.cache.def.UnderlayDefinitions;
import io.anglur.gui.MapViewer;
import io.anglur.map.MapUtils;
import io.anglur.map.RegionChunk;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jonathan on 6/18/2015.
 */
public final class MapDrawer {
	
	public static void main(String[] args) throws IOException {
		System.out.println("Starting... This may take a few mins.");
		drawMap();
		System.out.println("Finished!");
	}
	
	public static MapViewer drawMap() {
		BufferedImage mapImage = new BufferedImage(Constants.IMAGE_SIZE.width, Constants.IMAGE_SIZE.height, BufferedImage.TYPE_INT_ARGB);
		Map<Integer, RegionChunk> regions = new HashMap<>();
		
		MapUtils.init();
		UnderlayDefinitions.init();
		OverlayDefinitions.init();
		TextureLoader.loadTextures();
		
		int working = 0;
		int total = 0;
		for (int xArea = 0; xArea < 256; xArea++) {
			for (int yArea = 0; yArea < 256; yArea++) {
				if (Constants.CACHE.getIndex(5).getContainerByName("l" + xArea + "_" + yArea) != null) {
					int regionId = (xArea << 8) + yArea;
					MapDefinition def = MapDefinition.get(regionId);
					if (def.underlays == null) {
						continue;
					}
					RegionChunk chunk = new RegionChunk(regionId, def, new Point(xArea << 6, yArea << 6));
					chunk.drawOn(mapImage);
					if (chunk.stage() == RegionChunk.GOOD || chunk.stage() == RegionChunk.UNREACHABLE) {
						working++;
					}
					total++;
					
					regions.put(regionId, chunk);
				}
			}
		}
		
		try {
			ImageIO.write(mapImage, "PNG", new File("map_" + Constants.REVISION + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return MapViewer.open(mapImage, total, working, regions);
	}
}