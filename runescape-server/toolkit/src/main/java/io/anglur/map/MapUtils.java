package io.anglur.map;

import io.anglur.Constants;
import io.anglur.cache.def.MapDefinition;
import nl.bartpelle.dawnguard.DataStore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Jonathan on 6/13/2015.
 */
public final class MapUtils {
	
	private static final ConcurrentHashMap<Integer, int[]> XTEAS = new ConcurrentHashMap<>();
	
	public static ConcurrentHashMap<Integer, int[]> xteas() {
		return XTEAS;
	}
	
	public static void loadMaps() {
		for (File f : new File(Constants.XTEA_PATH).listFiles()) {
			if (!f.getName().endsWith(".txt")) {
				continue;
			}
			int regionId = Integer.parseInt(f.getName().substring(0, f.getName().length() - 4));
			if (XTEAS.containsKey(regionId)) {
				continue;
			}
			List<String> lines = null;
			try {
				lines = Files.readAllLines(f.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			int[] xteas = new int[4];
			for (int i = 0; i < xteas.length; i++) {
				xteas[i] = Integer.parseInt(lines.get(i));
			}
			XTEAS.put(regionId, xteas);
		}
	}
	
	public static void init() {
		Constants.CACHE = new DataStore(Constants.CACHE_PATH);
		loadMaps();
		System.out.println("Loaded: " + XTEAS.size() + " xteas!");
	}
	
	public static int[] get(int region) {
		return XTEAS.getOrDefault(region, null);
	}
	
	public static int findMap(int[] xteas) {
		Set<Integer> regions = new HashSet<>(XTEAS.keySet());
		for (int region : regions) {
			if (Arrays.equals(xteas, XTEAS.get(region))) {
				return region;
			}
		}
		
		int[] originalXteas;
		for (int region : regions) {
			originalXteas = XTEAS.getOrDefault(region, new int[]{
					0, 0, 0, 0,
			});
			XTEAS.put(region, xteas);
			int response = MapDefinition.get(region, false).firstStage();
			if (response == RegionChunk.GOOD) {
				System.err.println("We found the xteas for region " + region);
				return region;
			} else {
				XTEAS.put(region, originalXteas);
			}
		}
		return -1;
	}
	
	public static void removeInvalidMap(int regionId) {
		File file = new File(Constants.XTEA_PATH + regionId + ".txt");
		if (file.exists()) {
			//System.out.println("Deleting map: " + regionId);
			//file.delete();
		}
	}
	
	public static void writeZeros(int regionId) {
		File file = new File(Constants.XTEA_PATH + regionId + ".txt");
		if (file.exists()) {
			//file.delete();
		}
		int[] keys = new int[4];
		try {
			Files.write(file.toPath(), ((keys[0] + "\n") + (keys[1] + "\n") + (keys[2] + "\n") + (keys[3])).getBytes(), StandardOpenOption.CREATE_NEW);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("Writing zeros map: " + regionId);
	}
	
}
