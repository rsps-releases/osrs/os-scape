package io.anglur.map;

import io.anglur.Constants;
import io.anglur.cache.def.MapDefinition;
import io.anglur.cache.def.OverlayDefinitions;
import io.anglur.cache.def.TextureLoader;
import io.anglur.cache.def.UnderlayDefinitions;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * Created by Jonathan on 6/18/2015.
 */
public final class RegionChunk {
	
	private final int regionId;
	private MapDefinition mapDef;
	private int stage;
	private Point location;
	
	public RegionChunk(int regionId, MapDefinition def, Point location) {
		this.regionId = regionId;
		this.mapDef = def;
		this.stage = def.firstStage();
		this.location = location;
	}
	
	public void drawOn(BufferedImage image) {
		BufferedImage regionImage = createGraphics();
		for (int x = 0; x < 65; x++) {
			for (int y = 0; y < 65; y++) {
				image.setRGB((x + (location.x)), Constants.IMAGE_SIZE.height - (y + (location.y)), regionImage.getRGB(x, y));
			}
		}
	}
	
	private BufferedImage createGraphics() {
		String region = String.valueOf(regionId);
		BufferedImage img = new BufferedImage(65, 65, BufferedImage.TYPE_INT_RGB);
		
		Graphics2D g2 = img.createGraphics();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		for (int i = 0; i < Constants.HEIGHTS_TO_DRAW; i++) {
			for (int x = 0; x < 64; x++) {
				for (int y = 0; y < 64; y++) {
					try {
						int index = mapDef.underlays[i][x][y] & 0xff;
						if (index > 0) {
							UnderlayDefinitions uDef = UnderlayDefinitions.get(index);
							if (uDef != null) {
								img.setRGB(x, y, uDef.getRGB());
							}
						}
						
						index = mapDef.overlays[i][x][y] & 0xff;
						if (index > 0) {
							OverlayDefinitions oDef = OverlayDefinitions.get(index);
							if (oDef.materialId != -1) {
								img.setRGB(x, y, TextureLoader.getColorFor(oDef.materialId));
							} else {
								if (oDef.getRGB() > 0) {
									img.setRGB(x, y, oDef.getRGB());
								}
								if (oDef.getRGB2() > 0) {
									img.setRGB(x, y, oDef.getRGB2());
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.exit(-1);
					}
				}
			}
		}
		if (stage != GOOD && color() != null) {
			g2.setComposite(AlphaComposite.SrcOver.derive(0.5f));
			g2.setColor(color());
			g2.fillRect(0, 0, img.getWidth(), img.getHeight());
		}
		g2.setComposite(AlphaComposite.SrcOver);
		
		g2.drawImage(getText(region), 0, 0, null);
		
		g2.setColor(Color.ORANGE);
		g2.setStroke(new BasicStroke(1));
		g2.drawLine(0, 0, 0, img.getHeight()); //Top
		g2.drawLine(0, 0, img.getWidth(), 0);//Bottom
		g2.drawLine(0, img.getHeight() - 1, img.getWidth(), img.getHeight() - 1);//Left
		g2.drawLine(img.getWidth() - 1, img.getHeight() - 1, img.getWidth() - 1, 0);//Right
		g2.dispose();
		return img;
	}
	
	private BufferedImage getText(String region) {
		BufferedImage text = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) text.getGraphics();
		g.setFont(new Font("Helvetica", 1, 13));
		Font font = g.getFont();
		AffineTransform fontAT = new AffineTransform();
		fontAT.rotate(Math.toRadians(180));
		g.setFont(font.deriveFont(fontAT));
		g.setColor(Color.GREEN);
		
		String suffix = "";
		switch (stage()) {
			case MISSING:
				suffix = "MISSING";
				break;
			case UNREACHABLE:
				suffix = "DISTANT";
				break;
			case ACTIVE:
				suffix = "ACTIVE";
				break;
		}
		drawString(g, "\n\n" + region + "\n\n" + suffix, text.getWidth(), text.getHeight());
		g.dispose();
		
		BufferedImage flipped = new BufferedImage(text.getWidth(), text.getHeight(), text.getType());
		AffineTransform tran = AffineTransform.getTranslateInstance(text.getWidth(), 0);
		AffineTransform flip = AffineTransform.getScaleInstance(-1d, 1d);
		tran.concatenate(flip);
		
		g = flipped.createGraphics();
		g.setColor(Color.ORANGE);
		g.setTransform(tran);
		g.drawImage(text, 0, 0, null);
		g.dispose();
		return flipped;
	}
	
	private void drawString(Graphics g, String text, int width, int height) {
		FontMetrics fm = g.getFontMetrics();
		int y = ((height - fm.getHeight())) + fm.getAscent();
		for (String line : text.split("\n")) {
			int x = (width - fm.stringWidth(line)) / 2;
			g.drawString(line, x, y += g.getFontMetrics().getHeight());
		}
	}
	
	public int stage() {
		return stage;
	}
	
	public void stage(int stage) {
		this.stage = stage;
	}
	
	private static final Color[] COLORS = {Color.GREEN, Color.RED, Color.MAGENTA, Color.CYAN};
	
	private Color color() {
		return COLORS[stage];
	}
	
	public static final int GOOD = 0;
	public static final int MISSING = 1;
	public static final int UNREACHABLE = 2;
	public static final int ACTIVE = 3;
	
}
