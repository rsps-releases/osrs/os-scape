package io.anglur;


import nl.bartpelle.dawnguard.DataStore;

import java.awt.*;

/**
 * Created by Jonathan on 6/13/2015.
 */
public final class Constants {
	
	public static DataStore CACHE;
	
	public static final Dimension IMAGE_SIZE = new Dimension(6000, 11000);
	
	public static final int HEIGHTS_TO_DRAW = 4;
	
	public static boolean MOVE_WORKING = false;
	
	public static final int REVISION = 144;
	
	public static final String CACHE_PATH = "./data/filestore";
	
	//Make sure this ends with a file separator
	public static final String XTEA_PATH = "./temp/";
	
}
