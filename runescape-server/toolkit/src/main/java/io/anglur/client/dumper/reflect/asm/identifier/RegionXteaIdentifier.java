package io.anglur.client.dumper.reflect.asm.identifier;

import io.anglur.client.dumper.reflect.asm.Hook;
import io.anglur.client.dumper.reflect.asm.Identifier;
import io.anglur.client.dumper.reflect.asm.bytecode.Container;
import io.anglur.client.dumper.reflect.asm.bytecode.element.ClassElement;
import io.anglur.client.dumper.reflect.asm.bytecode.element.MethodElement;
import io.anglur.client.dumper.reflect.asm.bytecode.insn.InsnMatcher;
import io.anglur.client.dumper.reflect.asm.service.DumperService;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.FieldInsnNode;

import java.util.Iterator;


public class RegionXteaIdentifier extends Identifier {
	
	public RegionXteaIdentifier(Container container) {
		super(container);
	}
	
	@Override
	public void identify() {
		for (ClassElement element : container().elements()) {
			for (MethodElement method : element.methods()) {
				tInc(1);
				
				if (method.desc().equals("(Z)V"))
					continue;
				
				InsnMatcher matcher = new InsnMatcher(method.insn());
				Iterator<AbstractInsnNode[]> it = matcher.match("GETSTATIC ILOAD ILOAD IASTORE");
				while (it.hasNext()) {
					AbstractInsnNode[] nodes = it.next();
					FieldInsnNode regions = (FieldInsnNode) nodes[0];
					Iterator<AbstractInsnNode[]> xIt = matcher.match("ILOAD ICONST_4 MULTIANEWARRAY PUTSTATIC");
					
					if (!xIt.hasNext())
						continue;
					
					FieldInsnNode keys = (FieldInsnNode) xIt.next()[3];
					
					DumperService.set(new Hook(regions), new Hook(keys));
					pInc(0);
				}
			}
		}
	}
	
	@Override
	public void log() {
		System.out.println("Injected " + pointer(0) + " xtea print(s) out of " + total(1) + " method(s)");
	}
	
}
