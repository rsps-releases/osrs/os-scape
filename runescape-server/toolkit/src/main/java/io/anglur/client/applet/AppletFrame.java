package io.anglur.client.applet;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;

public class AppletFrame extends JFrame {
	
	public AppletFrame(Applet applet) {
		init(applet);
	}
	
	private void init(Applet applet) {
		setTitle("Old School RuneScape");
		setSize(new Dimension(783, 545));
		add(applet);
	}
}
