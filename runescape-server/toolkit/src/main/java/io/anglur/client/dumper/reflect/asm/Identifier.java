package io.anglur.client.dumper.reflect.asm;

import io.anglur.client.dumper.reflect.asm.bytecode.Container;

import java.text.DecimalFormat;


public abstract class Identifier {
	
	private Container container;
	
	private DecimalFormat form = new DecimalFormat("###,###.##");
	
	private int[] pointers = new int[6];
	private int[] totals = new int[6];
	
	public Identifier(Container container) {
		this.container = container;
	}
	
	public abstract void identify();
	
	public abstract void log();
	
	public Container container() {
		return container;
	}
	
	public void pInc(int index) {
		pointers[index]++;
		totals[index]++;
	}
	
	public void tInc(int index) {
		totals[index]++;
	}
	
	public String pointer(int index) {
		return form.format(pointers[index]);
	}
	
	public String total(int index) {
		return form.format(totals[index]);
	}
}
