package io.anglur.client.dumper.packet;

import io.anglur.Constants;
import io.anglur.cache.RSBuffer;

import java.util.Arrays;

/**
 * Created by Jonathan on 7/7/2015.
 */
public final class PacketUtils {
	
	
	public static void parseMapRegionPacket(RSBuffer buffer) {
		int size = buffer.readUShort();
		
		int centerY = 0, centerX = 0, localX = 0, localY = 0, height = 0, mapCount = 0;
		int[][] xteas = null;
		switch (Constants.REVISION) {
			case 79:
				centerY = buffer.readLEShort();
				mapCount = ((size - 2) / 16);
				xteas = new int[mapCount][4];
				for (int mapIdx = 0; mapIdx < mapCount; mapIdx++) {
					for (int h = 0; h < 4; h++) {
						xteas[mapIdx][h] = buffer.readInt();
					}
				}
				localY = buffer.readLEShort();
				height = buffer.readUByteN();
				localX = buffer.readUShort();
				centerX = buffer.readUShort();
				break;
			case 80:
				height = buffer.readUByte();
				centerY = buffer.readLEShortA();
				localX = buffer.readUShort();
				centerX = buffer.readShortA();
				localY = buffer.readLEShortA();
				
				
				mapCount = ((size - 9) / 16);
				xteas = new int[mapCount][4];
				for (int mapIdx = 0; mapIdx < mapCount; mapIdx++) {
					for (int h = 0; h < 4; h++) {
						xteas[mapIdx][h] = buffer.readIntegerV2();
					}
				}
				break;
			case 83:
				localX = buffer.readLEShortA();
				mapCount = ((size - 2) / 16);
				xteas = new int[mapCount][4];
				for (int mapIdx = 0; mapIdx < mapCount; mapIdx++) {
					for (int h = 0; h < 4; h++) {
						xteas[mapIdx][h] = buffer.readInt();
					}
				}
				localY = buffer.readLEShort();
				height = buffer.readUByteN();
				centerX = buffer.readShortA();
				centerY = buffer.readLEShortA();
				break;
			case 85:
				
				break;
			case 91:
				mapCount = ((size - 2) / 16);
				xteas = new int[mapCount][4];
				for (int mapIdx = 0; mapIdx < mapCount; mapIdx++) {
					for (int h = 0; h < 4; h++) {
						xteas[mapIdx][h] = buffer.readIntegerV1();
					}
				}
				
				centerY = buffer.readUShort();
				centerX = buffer.readShortA();
				localY = buffer.readLEShort();
				height = buffer.readUByteS();
				localX = buffer.readLEShortA();
				System.out.println(height + ", " + localX + ", " + localY);
				
				System.out.println("Hello? " + centerX + ", " + centerY + ", " + mapCount + ", " + Arrays.deepToString(xteas));
				break;
			case 132:
				//readLEShortA readLEShort readShortA readLEShortA
				centerX = buffer.readLEShortA();
				centerY = buffer.readUShort();
				
				mapCount = buffer.readLEShort();
				
				
				height = buffer.readUByteN();
				System.out.println(height + ", " + buffer.readUShort());
				
				System.out.println("Hello? " + centerX + ", " + centerY + ", " + mapCount + ", " + Arrays.deepToString(xteas));
				
				break;
		}
		if (mapCount < 1 || mapCount > 10) {
			return;
		}
		
		if (localY < 40 || localY > 64) {
			return;
		}
		
		if (height < 0 || height > 4) {
			return;
		}
		
		if (localX < 40 || localX > 64) {
			return;
		}
		
		boolean encrypted = !((centerX / 8 == 48 || centerX / 8 == 49) && centerY / 8 == 48) || (centerX / 8 == 48 && centerY / 8 == 148);
		mapCount = 0;
		
		for (int regionX = (centerX - 6) / 8; regionX <= (centerX + 6) / 8; ++regionX) {
			for (int regionY = (centerY - 6) / 8; regionY <= (centerY + 6) / 8; regionY++) {
				int regionId = regionY + (regionX << 8);
				if (encrypted || regionY != 49 && regionY != 149 && regionY != 147 && regionX != 50 && (regionX != 49 || regionY != 47)) {
					//Dumper.dump(regionId, xteas[mapCount]);
				}
				mapCount++;
			}
		}
	}
	
}
