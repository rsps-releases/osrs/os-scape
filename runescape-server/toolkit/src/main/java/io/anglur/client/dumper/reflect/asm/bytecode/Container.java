package io.anglur.client.dumper.reflect.asm.bytecode;

import io.anglur.client.dumper.reflect.asm.bytecode.element.ClassElement;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class Container {
	
	private Map<String, ClassElement> elements;
	
	public Container(File file) throws IOException {
		this.elements = new HashMap<>();
		
		JarFile jar = new JarFile(file);
		
		Enumeration<JarEntry> enumeration = jar.entries();
		while (enumeration.hasMoreElements()) {
			JarEntry next = enumeration.nextElement();
			if (next.getName().endsWith(".class")) {
				ClassReader reader = new ClassReader(jar.getInputStream(next));
				ClassNode node = new ClassNode();
				reader.accept(node, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
				ClassElement element = new ClassElement(node);
				
				elements.put(element.name(), element);
			}
		}
		jar.close();
		System.out.println("Loaded " + elements.size() + " classes.");
	}
	
	public Collection<ClassElement> elements() {
		return elements.values();
	}
	
}
