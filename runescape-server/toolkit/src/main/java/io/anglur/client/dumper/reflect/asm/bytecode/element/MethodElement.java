package io.anglur.client.dumper.reflect.asm.bytecode.element;

import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodNode;

public class MethodElement {
	
	private ClassElement parent;
	private MethodNode node;
	
	public MethodElement(ClassElement parent, MethodNode node) {
		this.parent = parent;
		this.node = node;
	}
	
	public ClassElement parent() {
		return parent;
	}
	
	public MethodNode node() {
		return node;
	}
	
	public String name() {
		return node.name;
	}
	
	public String desc() {
		return node.desc;
	}
	
	public InsnList insn() {
		return node.instructions;
	}
}
