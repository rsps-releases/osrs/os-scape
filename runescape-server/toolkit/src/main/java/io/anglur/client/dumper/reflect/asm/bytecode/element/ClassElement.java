package io.anglur.client.dumper.reflect.asm.bytecode.element;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.util.ArrayList;
import java.util.List;

public class ClassElement {
	
	private ClassNode node;
	private List<MethodElement> methods;
	
	public ClassElement(ClassNode node) {
		this.node = node;
		this.methods = new ArrayList<MethodElement>();
		
		for (Object m : node.methods) {
			methods.add(new MethodElement(this, (MethodNode) m));
		}
	}
	
	public ClassNode node() {
		return node;
	}
	
	public String name() {
		return node.name;
	}
	
	public List<MethodElement> methods() {
		return methods;
	}
}
