/**
 * Copyright (c) 2016 Kyle Fricilone
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.anglur.client.dumper.reflect.asm;

import org.objectweb.asm.tree.FieldInsnNode;

/**
 * @author Kyle Fricilone
 * @date Jul 1, 2016
 */
public class Hook {
	
	private final String owner;
	
	private final String name;
	
	private final String desc;
	
	public Hook(final String o, final String n, final String d) {
		this.owner = o;
		this.name = n;
		this.desc = d;
	}
	
	public Hook(final FieldInsnNode f) {
		this.owner = f.owner;
		this.name = f.name;
		this.desc = f.desc;
	}
	
	public final String getOwner() {
		return owner;
	}
	
	public final String getName() {
		return name;
	}
	
	public final String getDesc() {
		return desc;
	}
	
	@Override
	public String toString() {
		return "Hook: { " + owner + "." + name + " (" + desc + ") }";
	}
	
}
