package io.anglur.client;

import io.anglur.client.applet.AppletBackend;
import io.anglur.client.applet.AppletFrame;
import io.anglur.client.dumper.reflect.asm.Identifier;
import io.anglur.client.dumper.reflect.asm.bytecode.Container;
import io.anglur.client.dumper.reflect.asm.identifier.RegionXteaIdentifier;
import io.anglur.client.dumper.reflect.asm.service.DumperService;

import java.applet.Applet;
import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameClient {
	
	private static final int WORLD = 46;
	private static final String url = "http://oldschool" + WORLD + ".runescape.com/j1";
	
	private final Map<String, String> parameters = new HashMap<String, String>();
	private String archive;
	private String code;
	private Applet applet;
	
	public GameClient() {
		try {
			parseParameters();
			initApplet();
			display();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void parseParameters() throws IOException {
		String source = readPageSource(url);
		Matcher matcher = Pattern.compile("<param name=\"([^\\s]+)\"\\s+value=\"([^>]*)\">").matcher(source);
		while (matcher.find()) {
			parameters.put(matcher.group(1), matcher.group(2));
		}
		matcher = Pattern.compile("archive=gamepack(.*?)\\.jar").matcher(source);
		archive = matcher.find() ? matcher.group(1) : "";
		
		matcher = Pattern.compile("code=(.*?)\\.class").matcher(source);
		code = matcher.find() ? matcher.group(1) : "";
	}
	
	private void initApplet() throws Exception {
		System.out.println("Downloading gamepack....");
		URL url = new URL("http://oldschool" + WORLD + ".runescape.com/gamepack" + this.archive + ".jar");
		File out = new File("./gamepack" + this.archive + ".jar");
		
		try {
			URLConnection conn = url.openConnection();
			InputStream input = conn.getInputStream();
			FileOutputStream fos = new FileOutputStream(out);
			byte[] buffer = new byte[1024];
			int read;
			while ((read = input.read(buffer, 0, buffer.length)) != -1) {
				fos.write(buffer, 0, read);
			}
			fos.close();
			out.deleteOnExit();
		} catch (IOException ioex) {
			ioex.printStackTrace();
		}
		System.out.println("Finished downloading gamepack");
		
		Container container = new Container(out);
		List<Identifier> identifiers = Collections.synchronizedList(new ArrayList<Identifier>());
		
		synchronized (identifiers) {
			identifiers.add(new RegionXteaIdentifier(container));
		}
		
		synchronized (identifiers) {
			for (Identifier identifier : identifiers) {
				identifier.identify();
				identifier.log();
			}
		}
		
		URLClassLoader loader = (URLClassLoader) ClassLoader.getSystemClassLoader();
		Class<?> urlClass = URLClassLoader.class;
		Method method = urlClass.getDeclaredMethod("addURL", new Class[]{URL.class});
		method.setAccessible(true);
		method.invoke(loader, new Object[]{out.toURI().toURL()});
		
		Class<?> clnt = Class.forName(code);
		applet = (Applet) clnt.getConstructor().newInstance();
		applet.setSize(770, 530);
		
		DumperService.start();
	}
	
	public static String readPageSource(String page) throws IOException {
		URLConnection uc = new URL(page).openConnection();
		uc.setRequestProperty("User-Agent", "Mozilla/4.0");
		uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		byte[] data = new byte[uc.getContentLength()];
		new DataInputStream(uc.getInputStream()).readFully(data);
		return new String(data);
	}
	
	private void display() throws Exception {
		applet.setStub(new AppletBackend(new URL(url), parameters));
		AppletFrame frame = new AppletFrame(applet);
		applet.init();
		frame.setDefaultCloseOperation(3);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		applet.start();
	}
}
