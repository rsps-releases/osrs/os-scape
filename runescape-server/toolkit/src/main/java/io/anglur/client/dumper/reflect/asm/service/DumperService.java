package io.anglur.client.dumper.reflect.asm.service;

import com.google.common.util.concurrent.AbstractScheduledService;
import io.anglur.Constants;
import io.anglur.XTEADumper;
import io.anglur.client.dumper.reflect.asm.Hook;
import io.anglur.map.MapUtils;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class DumperService extends AbstractScheduledService {
	
	private final static DumperService service = new DumperService();
	
	private final Lookup lookup = MethodHandles.lookup();
	
	private Hook idHook;
	private Hook xteaHook;
	private int idHash;
	private int xteaHash;
	private MethodHandle idHandle;
	private MethodHandle xteaHandle;
	
	@Override
	protected void runOneIteration() throws Exception {
		try {
			if (idHandle == null) {
				Class<?> clazz = Class.forName(idHook.getOwner());
				Field field = clazz.getDeclaredField(idHook.getName());
				field.setAccessible(true);
				
				idHandle = lookup.unreflectGetter(field);
			}
			
			if (xteaHandle == null) {
				Class<?> clazz = Class.forName(xteaHook.getOwner());
				Field field = clazz.getDeclaredField(xteaHook.getName());
				field.setAccessible(true);
				
				xteaHandle = lookup.unreflectGetter(field);
			}
			
			int[] regionIds = (int[]) idHandle.invoke();
			
			int[][] regionXTEAs = (int[][]) xteaHandle.invoke();
			
			if ((regionIds != null && regionXTEAs != null)
					&& (idHash != regionIds.hashCode())
					&& (xteaHash != regionXTEAs.hashCode())) {
				
				idHash = regionIds.hashCode();
				xteaHash = regionXTEAs.hashCode();
				
				dumpMap(regionIds, regionXTEAs);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected Scheduler scheduler() {
		return Scheduler.newFixedRateSchedule(1, 1, TimeUnit.SECONDS);
	}
	
	private static long lastClear = -1;
	
	public void dumpMap(int[] regions, int[][] xteas) {
		for (int i = 0; i < regions.length; i++) {
			int region = regions[i];
			int[] keys = xteas[i];
			
			if ((System.currentTimeMillis() - lastClear) > 250) {
				XTEADumper.mapViewer().activeRegions().forEach(XTEADumper.mapViewer().previousRegions()::add);
				XTEADumper.mapViewer().activeRegions().clear();
				System.out.println();
			}
			
			lastClear = System.currentTimeMillis();
			XTEADumper.mapViewer().activeRegions().add(region);
			System.out.println("Region: " + region + " (" + (region >> 8) + "," + (region & 0xFF) + "), Coords: (" + ((region >> 8) << 6) + ", "
					+ ((region & 0xFF) << 6) + ") " + Arrays.toString(keys));
			
			int[] existingKey = MapUtils.get(region);
			if (!Arrays.equals(existingKey, keys)) {
				XTEADumper.mapViewer().map().valid++;
				
				File file = new File(Constants.XTEA_PATH + region + ".txt");
				if (file.exists())
					file.delete();
				
				try {
					Files.write(file.toPath(), ((keys[0] + "\n") + (keys[1] + "\n") + (keys[2] + "\n") + (keys[3])).getBytes(), StandardOpenOption.CREATE_NEW);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void set(final Hook id, final Hook xtea) {
		service.idHook = id;
		service.xteaHook = xtea;
	}
	
	public static void start() {
		service.startAsync();
	}
	
}
