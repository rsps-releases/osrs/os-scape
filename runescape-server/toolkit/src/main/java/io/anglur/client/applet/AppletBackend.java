package io.anglur.client.applet;

import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.applet.AudioClip;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AppletBackend implements AppletContext, AppletStub {
	
	private static final Logger logger = Logger.getLogger(AppletBackend.class.getName());
	
	private final URL base;
	private final Map<String, String> parameters;
	
	public AppletBackend(URL base, Map<String, String> parameters) {
		this.base = base;
		this.parameters = parameters;
	}
	
	@Override
	public AudioClip getAudioClip(URL url) {
		return null;
	}
	
	@Override
	public Image getImage(URL url) {
		return null;
	}
	
	@Override
	public Applet getApplet(String name) {
		return null;
	}
	
	@Override
	public Enumeration<Applet> getApplets() {
		return null;
	}
	
	@Override
	public void showDocument(URL url) {
		logger.log(Level.INFO, "showDocument", url);
	}
	
	@Override
	public void showDocument(URL url, String target) {
		
	}
	
	@Override
	public void showStatus(String status) {
	}
	
	@Override
	public void setStream(String key, InputStream stream) throws IOException {
	}
	
	@Override
	public InputStream getStream(String key) {
		return null;
	}
	
	@Override
	public Iterator<String> getStreamKeys() {
		return null;
	}
	
	@Override
	public boolean isActive() {
		return false;
	}
	
	@Override
	public URL getDocumentBase() {
		return base;
	}
	
	@Override
	public URL getCodeBase() {
		return base;
	}
	
	@Override
	public String getParameter(String name) {
		String value = parameters.get(name);
		if (value == null) {
			value = "";
		}
		return value;
	}
	
	@Override
	public AppletContext getAppletContext() {
		return null;
	}
	
	@Override
	public void appletResize(int width, int height) {
	}
}
