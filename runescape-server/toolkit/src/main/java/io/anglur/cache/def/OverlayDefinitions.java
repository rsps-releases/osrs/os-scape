package io.anglur.cache.def;

import io.anglur.Constants;
import io.anglur.cache.RSBuffer;
import nl.bartpelle.dawnguard.DataStore;


public class OverlayDefinitions {
	
	public int rgb1 = 0;
	public boolean aBool6416;
	public int color;
	public int materialId = -1;
	public int id;
	
	public static OverlayDefinitions[] overlayDefinitions = new OverlayDefinitions[1000];
	
	public static OverlayDefinitions get(int index) {
		return overlayDefinitions[index - 1];
	}
	
	public static void init() {
		for (int id = 0; id < 1000; id++) {
			try {
				overlayDefinitions[id] = new OverlayDefinitions(Constants.CACHE, id);
			} catch (Exception e) {
			}
		}
	}
	
	void decode(RSBuffer buffer, int i) {
		if (Constants.REVISION > 500) {
			if (i == 1) {
				rgb1 = ((buffer.readUShort()) << 8) | (buffer.readUByte());
			} else if (2 == i) {
				materialId = buffer.readUByte();
			} else if (3 == i) {
				materialId = buffer.readUShort();
				if (65535 == materialId) {
					materialId = -1;
				}
			} else if (i == 5) {
				aBool6416 = false;
			} else if (7 == i) {
				color = ((buffer.readUShort()) << 8) | (buffer.readUByte());
			}
			if (9 == i) {
				int anInt6419 = (buffer.readUShort()) << 2;
			} else if (i == 10) {
				boolean aBool6417 = false;
			} else if (i == 11) {
				int anInt6421 = buffer.readUByte();
			} else if (12 == i) {
				boolean aBool6422 = true;
			} else if (i == 13) {
				int anInt6423 = (buffer.readUShort()) | (buffer.readUByte());
			} else if (14 == i) {
				int anInt6427 = (buffer.readUByte()) << 2;
			} else if (16 == i) {
				int anInt6425 = buffer.readUByte();
			} else if (20 == i) {
				int anInt6426 = buffer.readUShort();
			} else if (i == 21) {
				int anInt6428 = buffer.readUByte();
			} else if (i == 22) {
				int anInt6414 = buffer.readUShort();
			}
		} else {
			if (i == 1) {
				rgb1 = buffer.readTriByte();
			} else if (i == 2) {
				materialId = buffer.readUByte();
			} else if (i == 5) {
				aBool6416 = false;
			} else if (i == 7) {
				color = buffer.readTriByte();
			} else {
				System.err.println("Overlay Unhandled opcode: " + i);
			}
		}
	}
	
	void decode(RSBuffer buffer) {
		for (; ; ) {
			int opcode = buffer.readUByte();
			if (opcode == 0) {
				break;
			}
			decode(buffer, opcode);
		}
	}
	
	public OverlayDefinitions(DataStore store, int id) {
		this.id = id;
		byte[] data = store.getFileDirect(2, 4, id);
		RSBuffer buffer = new RSBuffer(data);
		decode(buffer);
	}
	
	public int getRGB() {
		if (rgb1 == 0xFF00FF) {
			return 0;
		}
		return rgb1;
	}
	
	public int getRGB2() {
		if (color == 0xFF00FF) {
			return 0;
		}
		return color;
	}
	
}
