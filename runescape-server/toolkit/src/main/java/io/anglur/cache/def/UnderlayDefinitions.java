package io.anglur.cache.def;

import io.anglur.Constants;
import io.anglur.cache.RSBuffer;
import nl.bartpelle.dawnguard.DataStore;

public class UnderlayDefinitions {
	
	private int rgb;
	
	public static UnderlayDefinitions[] underlayDefinitions = new UnderlayDefinitions[1000];
	private int material;
	private int anInt4700;
	private boolean aBool4698;
	private boolean occlude;
	
	public static UnderlayDefinitions get(int index) {
		return underlayDefinitions[index - 1];
	}
	
	public static void init() {
		for (int id = 0; id < 500; id++) {
			try {
				underlayDefinitions[id] = new UnderlayDefinitions(Constants.CACHE, id);
			} catch (Exception e) {
			}
		}
	}
	
	public UnderlayDefinitions(DataStore store, int id) {
		byte[] data = store.getFileDirect(2, 1, id);
		
		RSBuffer buffer = new RSBuffer(data);
		
		while (true) {
			byte op = buffer.readByte();
			if (op == 0) {
				break;
			}
			
			decode(buffer, op);
		}
	}
	
	private void decode(RSBuffer buffer, int opcode) {
		if (Constants.REVISION > 500) {
			if (opcode == 1) {
				rgb = buffer.readUShort();
				rgb <<= 8;
				rgb |= buffer.readByte();
			} else if (opcode == 2) {
				material = buffer.readUShort();
				if (material == 65535) {
					material = -1;
				}
			} else if (opcode == 3) {
				anInt4700 = (buffer.readUShort()) << 2;
			} else if (opcode == 4) {
				aBool4698 = false;
			} else if (opcode == 5) {
				occlude = false;
			} else {
				System.err.println("Underlay Unhandled opcode: " + opcode);
			}
		} else {
			if (opcode == 1) {
				rgb = buffer.readTriByte();
			} else {
				System.err.println("Underlay Unhandled opcode: " + opcode);
			}
		}
	}
	
	public int getRGB() {
		return rgb;
	}
	
}
