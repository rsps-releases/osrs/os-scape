package io.anglur.cache.def;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.PixelGrabber;
import java.io.File;

/**
 * Created by Jonathan on 6/14/2015.
 */
public final class TextureLoader {
	
	private static int[] textureAverageColors;
	private static Image[] textures;
	
	public static int getColorFor(int textureId) {
		return textureAverageColors[textureId];
	}
	
	public static void loadTextures() {
		try {
			textures = new Image[1000];
			textureAverageColors = new int[1000];
			
			for (int i = 0; i < textures.length; i++) {
				File f2 = new File("./data/textures/" + i + ".png");
				
				if (!f2.exists()) {
					continue;
				}
				
				Image img = ImageIO.read(f2);
				textures[i] = img;
				int width = img.getWidth(null);
				int height = img.getHeight(null);
				int[] pixels = new int[width * height];
				PixelGrabber pixelgrabber = new PixelGrabber(img, 0, 0, width, height, pixels, 0, width);
				pixelgrabber.grabPixels();
				textureAverageColors[i] = averageColorForPixels(pixels);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static int averageColorForPixels(int[] pixels) {
		int redTotal = 0;
		int greenTotal = 0;
		int blueTotal = 0;
		int totalPixels = pixels.length;
		
		for (int i = 0; i < totalPixels; i++) {
			if (pixels[i] == 0xff00ff) {
				totalPixels--;
				continue;
			}
			
			redTotal += pixels[i] >> 16 & 0xff;
			greenTotal += pixels[i] >> 8 & 0xff;
			blueTotal += pixels[i] & 0xff;
		}
		
		int averageRGB = (redTotal / totalPixels << 16) + (greenTotal / totalPixels << 8) + blueTotal / totalPixels;
		
		if (averageRGB == 0) {
			averageRGB = 1;
		}
		
		return averageRGB;
	}
	
}
