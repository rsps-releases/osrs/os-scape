package io.anglur.cache;

import java.nio.ByteBuffer;

/**
 * Created by Bart Pelle on 8/22/2014.
 */
public class RSBuffer {
	
	private ByteBuffer backing;
	
	public RSBuffer(ByteBuffer backing) {
		this.backing = backing;
	}
	
	public RSBuffer(byte[] data) {
		backing = ByteBuffer.wrap(data);
	}
	
	public ByteBuffer get() {
		return backing;
	}
	
	public RSBuffer writeByte(int v) {
		backing.put((byte) v);
		return this;
	}
	
	public byte readByte() {
		return backing.get();
	}
	
	public byte readByteN() {
		return (byte) -backing.get();
	}
	
	public int readUByteN() {
		return (0 - readByte() & 0xff);
	}
	
	public int readUByte() {
		return backing.get() & 0xFF;
	}
	
	public byte readByteS() {
		return (byte) (128 - backing.get());
	}
	
	public short readUByteS() {
		return (short) (128 - readUByte());
	}
	
	public RSBuffer writeByteA(int v) {
		backing.put((byte) (v + 128));
		return this;
	}
	
	public RSBuffer writeByteS(int v) {
		backing.put((byte) (128 - v));
		return this;
	}
	
	public RSBuffer writeByteN(int v) {
		backing.put((byte) -v);
		return this;
	}
	
	public RSBuffer writeBytes(byte[] data, int start, int end) {
		backing.put(data, start, end);
		return this;
	}
	
	public RSBuffer writeShort(int v) {
		backing.putShort((short) v);
		return this;
	}
	
	public RSBuffer writeLEShortA(int v) {
		backing.put((byte) (v + 128));
		backing.put((byte) (v >> 8));
		return this;
	}
	
	public RSBuffer writeShortA(int v) {
		backing.put((byte) (v >> 8));
		backing.put((byte) (v + 128));
		return this;
	}
	
	public RSBuffer writeLEShort(int v) {
		backing.put((byte) v);
		backing.put((byte) (v >> 8));
		return this;
	}
	
	
	public short readShort() {
		return backing.getShort();
	}
	
	public int readUShort() {
		return backing.getShort() & 0xFFFF;
	}
	
	public int readLEShort() {
		return readUByte() | (readUByte() << 8);
	}
	
	public int readLEShortA() {
		return ((readByte() - 128) & 0xFF) | (readUByte() << 8);
	}
	
	public int readShortA() {
		return (readUByte() << 8) | ((readByte() - 128) & 0xFF);
	}
	
	public RSBuffer writeInt(int v) {
		backing.putInt(v);
		return this;
	}
	
	public RSBuffer writeLEInt(int v) {
		backing.put((byte) v);
		backing.put((byte) (v >> 8));
		backing.put((byte) (v >> 16));
		backing.put((byte) (v >> 24));
		return this;
	}
	
	public RSBuffer writeIntV1(int v) {
		backing.put((byte) (v >> 8));
		backing.put((byte) v);
		backing.put((byte) (v >> 24));
		backing.put((byte) (v >> 16));
		return this;
	}
	
	public RSBuffer writeIntV2(int v) {
		backing.put((byte) (v >> 16));
		backing.put((byte) (v >> 24));
		backing.put((byte) v);
		backing.put((byte) (v >> 8));
		return this;
	}
	
	public int readInt() {
		return backing.getInt();
	}
	
	public int readIntegerV2() {
		return (readUByte() << 16) | (readUByte() << 24) | (readUByte()) | (readUByte() << 8);
	}
	
	public int readIntegerV1() {
		return (readUByte() << 8) | (readUByte() << 0) | (readUByte() << 24) | (readUByte() << 16);
	}
	
	public int readIntegerLE() {
		return (readUByte()) | (readUByte() << 8) | (readUByte() << 16) | (readUByte() << 24);
	}
	
	public long readLong() {
		long l = readInt() & 0xffffffffL;
		long l_24_ = readInt() & 0xffffffffL;
		return l_24_ + (l << 32);
	}
	
	public RSBuffer writeCompact(int v) {
		if (v >= 0x80) {
			writeShort(v + 0x8000);
		} else {
			writeByte(v);
		}
		return this;
	}
	
	public RSBuffer writeString(String str) {
		if (str == null) {
			str = "";
		}
		
		backing.put(str.getBytes()).put((byte) 0);
		return this;
	}
	
	public String readString() {
		String s = "";
		int i;
		while ((i = readByte()) != 0) {
			s += (char) i;
		}
		return s;
	}
	
	public int readTriByte() {
		return ((readUByte()) << 16) | ((readUByte()) << 8) | (readUByte());
	}
	
	public RSBuffer position(int size) {
		backing.position(size);
		return this;
	}
	
	public int position() {
		return backing.position();
	}
	
	public RSBuffer readBytes(byte[] data) {
		backing.get(data);
		return this;
	}
	
	public RSBuffer reset() {
		backing.reset();
		return this;
	}
	
	public RSBuffer rewind() {
		backing.rewind();
		return this;
	}
	
}
