package io.anglur.cache.def;

/**
 * Created by Jonathan on 6/13/2015.
 */

import io.anglur.Constants;
import io.anglur.map.MapUtils;
import io.anglur.map.RegionChunk;

import java.nio.ByteBuffer;

public class MapDefinition {
	
	private static final ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 1024);
	
	public static double TOTAL_REGIONS = 912;
	
	private int initialStage = -1;
	
	private boolean deleteInvalid;
	public short[][][] overlays;
	public short[][][] underlays;
	
	public static MapDefinition get(int regionId) {
		return new MapDefinition(regionId, true);
	}
	
	public static MapDefinition get(int regionId, boolean deleteInvalid) {
		return new MapDefinition(regionId, deleteInvalid);
	}
	
	private MapDefinition(int regionId, boolean deleteInvalid) {
		this.deleteInvalid = deleteInvalid;
		
		int[] xteas = MapUtils.get(regionId);
		int regionX = (regionId >> 8) * 64;
		int regionY = (regionId & 0xff) * 64;
		if (xteas == null) {
			initialStage = RegionChunk.MISSING;
		}
		int landArchiveId;
		try {
			landArchiveId = Constants.CACHE.getIndex(5).getContainerByName("l" + (regionX >> 3) / 8 + "_" + (regionY >> 3) / 8).getId();
		} catch (Exception e) {
			landArchiveId = -1;
		}
		byte[] landContainerData;
		try {
			try {
				landContainerData = landArchiveId == -1 ? null : Constants.CACHE.getEncryptedFileDirect(5, landArchiveId, 0, xteas);
			} catch (Exception e) {
				landContainerData = null;
			}
			if (!deleteInvalid && landContainerData == null) {
				initialStage = RegionChunk.MISSING;
				return;
			}
			if (landContainerData != null) {
				initialStage = RegionChunk.GOOD;
			} else {
				try {
					landContainerData = landArchiveId == -1 ? null : Constants.CACHE.getEncryptedFileDirect(5, landArchiveId, 0, new int[]{0, 0, 0, 0});
				} catch (Exception e) {
					landContainerData = null;
				}
				if (landContainerData != null) {
					//System.out.println(regionId + " can be fixed by sending all 0's");
					MapUtils.writeZeros(regionId);
					initialStage = RegionChunk.GOOD;
				} else {
					try {
						landContainerData = landArchiveId == -1 ? null : Constants.CACHE.getEncryptedFileDirect(5, landArchiveId, 0, null);
					} catch (Exception e) {
						landContainerData = null;
					}
					if (landContainerData != null) {
						//System.out.println(regionId + " can be fixed by sending null");
						MapUtils.removeInvalidMap(regionId);
						initialStage = RegionChunk.GOOD;
					}
				}
			}
		} catch (Exception e) {
			initialStage = RegionChunk.MISSING;
			e.printStackTrace();
		}
		
		
		int mapArchiveId;
		try {
			mapArchiveId = Constants.CACHE.getIndex(5).getContainerByName("m" + (regionX >> 3) / 8 + "_" + (regionY >> 3) / 8).getId();
		} catch (Exception e) {
			mapArchiveId = -1;
		}
		byte[] d = mapArchiveId == -1 ? null : Constants.CACHE.getFileDirect(5, mapArchiveId, 0);
		
		if (d == null || d.length < 1) {
			return;
		}
		
		buffer.clear();
		buffer.put(d);
		buffer.flip();
		
		overlays = new short[4][64][64];
		underlays = new short[4][64][64];
		
		boolean reachable = false;
		for (int plane = 0; plane < 4; plane++) {
			for (int x = 0; x < 64; x++) {
				for (int y = 0; y < 64; y++) {
					while (true) {
						int opcode = buffer.get() & 0xFF;
						if (opcode == 0) {
							break;
						}
						if (opcode == 1) {
							buffer.get();
							break;
						} else if (opcode <= 49) {
							overlays[plane][x][y] = buffer.get();
						} else if (opcode > 81) {
							underlays[plane][x][y] = (short) (opcode - 81);
							reachable = true;
						}
					}
				}
			}
		}
		if (initialStage != RegionChunk.GOOD) {
			initialStage = reachable && canReach(regionId) ? RegionChunk.MISSING : RegionChunk.UNREACHABLE;
			
			if (deleteInvalid) {
				MapUtils.removeInvalidMap(regionId);
			}
		}
	}
	
	public int firstStage() {
		return initialStage;
	}
	
	private static int[] unreachableRegions = {
			7486, 8250, 8249, 8505, 11048, 11304, 11560, 11816, 12072, 12073, 12074, 12075
	};
	
	private static boolean canReach(int regionId) {
		for (int r : unreachableRegions) {
			if (r == regionId) {
				return false;
			}
		}
		return true;
	}
}
