package io.anglur.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MapViewerOld extends JPanel {
	
	public static final String ZOOM_LEVEL_CHANGED_PROPERTY = "zoomLevel";
	
	public static final String IMAGE_CHANGED_PROPERTY = "image";
	
	private static final double HIGH_QUALITY_RENDERING_SCALE_THRESHOLD = 1.0;
	private static final Object INTERPOLATION_TYPE = RenderingHints.VALUE_INTERPOLATION_BILINEAR;
	
	private double zoomIncrement = 0.2;
	private double zoomFactor = 1.0 + zoomIncrement;
	private BufferedImage image;
	private double initialScale = 0.0;
	private double scale = 0.0;
	private int originX = 0;
	private int originY = 0;
	private Point mousePosition;
	private Dimension previousPanelSize;
	private boolean highQualityRenderingEnabled = true;
	
	private class Coords {
		public double x;
		public double y;
		
		public Coords(double x, double y) {
			this.x = x;
			this.y = y;
		}
		
		public int getIntX() {
			return (int) Math.round(x);
		}
		
		public int getIntY() {
			return (int) Math.round(y);
		}
		
		public String toString() {
			return "[Coords: x=" + x + ",y=" + y + "]";
		}
	}
	
	public MapViewerOld() {
		setOpaque(false);
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				if (scale > 0.0) {
					if (isFullImageInPanel()) {
						centerImage();
					} else if (isImageEdgeInPanel()) {
						scaleOrigin();
					}
					repaint();
				}
				previousPanelSize = getSize();
			}
		});
		
		addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)) {
					Point p = e.getPoint();
					moveImage(p);
				}
			}
			
			public void mouseMoved(MouseEvent e) {
				mousePosition = e.getPoint();//
				repaint();
			}
		});
		
		addMouseWheelListener(e -> {
			Point p = e.getPoint();
			boolean zoomIn = (e.getWheelRotation() < 0);
			if (isInImage(p)) {
				if (zoomIn) {
					zoomFactor = 1.0 + zoomIncrement;
				} else {
					zoomFactor = 1.0 - zoomIncrement;
				}
				zoomImage();
			}
		});
	}
	
	private void initializeParams() {
		double xScale = (double) getWidth() / image.getWidth();
		double yScale = (double) getHeight() / image.getHeight();
		initialScale = Math.min(xScale, yScale);
		scale = initialScale;
		
		centerImage();
	}
	
	private void centerImage() {
		originX = (getWidth() - getScreenImageWidth()) / 2;
		originY = (getHeight() - getScreenImageHeight()) / 2;
	}
	
	public void setImage(BufferedImage image) {
		BufferedImage oldImage = this.image;
		this.image = image;
		firePropertyChange(IMAGE_CHANGED_PROPERTY, oldImage, image);
		repaint();
	}
	
	private Coords panelToImageCoords(Point p) {
		return new Coords((p.x - originX) / scale, (p.y - originY) / scale);
	}
	
	private Coords imageToPanelCoords(Coords p) {
		return new Coords((p.x * scale) + originX, (p.y * scale) + originY);
	}
	
	
	private boolean isInImage(Point p) {
		Coords coords = panelToImageCoords(p);
		int x = coords.getIntX();
		int y = coords.getIntY();
		return (x >= 0 && x < image.getWidth() && y >= 0 && y < image.getHeight());
	}
	
	private boolean isImageEdgeInPanel() {
		if (previousPanelSize == null) {
			return false;
		}
		
		return (originX > 0 && originX < previousPanelSize.width || originY > 0 && originY < previousPanelSize.height);
	}
	
	private boolean isFullImageInPanel() {
		return (originX >= 0 && (originX + getScreenImageWidth()) < getWidth() && originY >= 0 && (originY + getScreenImageHeight()) < getHeight());
	}
	
	private boolean isHighQualityRendering() {
		return (highQualityRenderingEnabled && scale > HIGH_QUALITY_RENDERING_SCALE_THRESHOLD);
	}
	
	private void scaleOrigin() {
		originX = originX * getWidth() / previousPanelSize.width;
		originY = originY * getHeight() / previousPanelSize.height;
		repaint();
	}
	
	public double getZoom() {
		return scale / initialScale;
	}
	
	private void zoomImage() {
		Coords imageP = panelToImageCoords(mousePosition);
		double oldZoom = getZoom();
		scale *= zoomFactor;
		Coords panelP = imageToPanelCoords(imageP);
		
		originX += (mousePosition.x - (int) panelP.x);
		originY += (mousePosition.y - (int) panelP.y);
		
		firePropertyChange(ZOOM_LEVEL_CHANGED_PROPERTY, new Double(oldZoom), new Double(getZoom()));
		
		repaint();
	}
	
	private void moveImage(Point p) {
		int xDelta = p.x - mousePosition.x;
		int yDelta = p.y - mousePosition.y;
		originX += xDelta;
		originY += yDelta;
		mousePosition = p;
		repaint();
	}
	
	private Rectangle getImageClipBounds() {
		Coords startCoords = panelToImageCoords(new Point(0, 0));
		Coords endCoords = panelToImageCoords(new Point(getWidth() - 1, getHeight() - 1));
		int panelX1 = startCoords.getIntX();
		int panelY1 = startCoords.getIntY();
		int panelX2 = endCoords.getIntX();
		int panelY2 = endCoords.getIntY();
		
		if (panelX1 >= image.getWidth() || panelX2 < 0 || panelY1 >= image.getHeight() || panelY2 < 0) {
			return null;
		}
		
		int x1 = (panelX1 < 0) ? 0 : panelX1;
		int y1 = (panelY1 < 0) ? 0 : panelY1;
		int x2 = (panelX2 >= image.getWidth()) ? image.getWidth() - 1 : panelX2;
		int y2 = (panelY2 >= image.getHeight()) ? image.getHeight() - 1 : panelY2;
		return new Rectangle(x1, y1, x2 - x1 + 1, y2 - y1 + 1);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); // Paints the background
		
		if (image == null) {
			return;
		}
		
		if (scale == 0.0) {
			initializeParams();
		}
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, getWidth(), getHeight());
		
		if (isHighQualityRendering()) {
			Rectangle rect = getImageClipBounds();
			if (rect == null || rect.width == 0 || rect.height == 0) { // no part of image is displayed in the panel
				return;
			}
			
			BufferedImage subimage = image.getSubimage(rect.x, rect.y, rect.width, rect.height);
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, INTERPOLATION_TYPE);
			g2.drawImage(subimage, Math.max(0, originX), Math.max(0, originY), Math.min((int) (subimage.getWidth() * scale), getWidth()), Math.min((int) (subimage.getHeight() * scale), getHeight()), null);
		} else {
			g.drawImage(image, originX, originY, getScreenImageWidth(), getScreenImageHeight(), null);
		}
		
		g.setColor(Color.YELLOW);
		Insets insets = getInsets();
		int x = 0;
		int y = 0;
		if (mousePosition != null) {
			Coords p = panelToImageCoords(mousePosition);
			x = p.getIntX() - (insets.left + insets.right) + 4;
			y = p.getIntY() - (insets.top + insets.bottom) - 4560;
		}
		g.drawString("Zoom: " + String.format("%.2f", scale * 100.0) + "%", 10, getHeight() - 60);
		g.drawString(String.format("Mouse Coords: [x=%d, y=%d]", x, y), 10, getHeight() - 40);
		
		double perc = (good / (double) total) * 100.0;
		g.drawString("XTEA information: Total=" + String.format("%.0f", perc) + "% (" + good + "/" + total + ") [160 missing, 100 wrong keys]", 10, getHeight() - 20);
	}
	
	private int getScreenImageWidth() {
		return (int) (scale * image.getWidth());
	}
	
	private int getScreenImageHeight() {
		return (int) (scale * image.getHeight());
	}
	
	private static int good, total;
	
	public static MapViewerOld open(BufferedImage image, int g, int t) {
		final JFrame frame = new JFrame("OSRS Map Viewer - Jonathan Beaudoin");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MapViewerOld panel = new MapViewerOld();
		panel.setImage(image);
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Rectangle bounds = ge.getMaximumWindowBounds();
		frame.setSize(new Dimension(bounds.width, bounds.height));
		frame.setVisible(true);
		good = g;
		total = t;
		return panel;
	}
	
	public static void main(String[] args) {
		try {
			final BufferedImage image1 = ImageIO.read(new File("map_91.png"));
			open(image1, -1, -1);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
	
}