package io.anglur.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;

public class MapPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private static final double ZOOM_MODIFIER = .20; //Add 20% speed
	
	public final MapViewer context;
	public BufferedImage img;
	
	private int zoom = 100;
	
	int centerX = 3200;
	
	int centerY = 3200;
	
	private final int total;
	public int valid = 0;
	
	MapPanel(MapViewer mapViewer, BufferedImage img, int total, int valid) {
		this.context = mapViewer;
		this.img = img;
		this.total = total;
		this.valid = valid;
		
		MouseAdapter ma = new MapPanelMouseAdapter();
		addMouseMotionListener(ma);
		addMouseWheelListener(ma);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		g.clearRect(0, 0, getWidth(), getHeight());
		g.drawImage(img, 0, 0, getWidth(), getHeight(), centerX - getHorizontalViewSize(), img.getHeight() - (centerY + getVerticalViewSize()), centerX + getHorizontalViewSize(), img.getHeight() - (centerY - getVerticalViewSize()), null);
		Graphics2D g2d = (Graphics2D) g;
		
		new Selection(new Area(context.drag().zoomTo)).draw(this, g2d);
		
		g2d.setColor(Color.DARK_GRAY);
		g2d.fillRect(0, getHeight() - 70, 270, 100);
		g2d.setColor(Color.WHITE);
		g2d.drawString("Zoom: " + zoom + "%", 10, getHeight() - 50);
		
		if (currentMousePos != null) {
			g2d.drawString(String.format("Mouse Coords: [x=%d, y=%d]", getMapX(currentMousePos.x), getMapY(currentMousePos.y)), 10, getHeight() - 30);
		}
		
		double perc = (valid / ((double) total)) * 100.0;
		g2d.drawString("XTEA's : " + String.format("%.0f", perc) + "% (" + valid + "/" + total + ") Missing=" + (total - valid), 10, getHeight() - 10);
	}
	
	void setView(Rectangle rect) {
		if (rect.getWidth() == 0 && rect.getHeight() == 0) {
			return;
		}
		int zoomX = (int) (context.map().getWidth() * 100 / rect.getWidth());
		int zoomY = (int) (context.map().getHeight() * 100 / rect.getHeight());
		context.map().centerX = (int) rect.getCenterX();
		context.map().centerY = (int) rect.getCenterY();
		context.map().setZoom(Math.min(zoomX, zoomY));
		repaint();
	}
	
	private int getHorizontalViewSize() {
		return getWidth() * 100 / zoom / 2;
	}
	
	private int getVerticalViewSize() {
		return getHeight() * 100 / zoom / 2;
	}
	
	int getMapX(int panelX) {
		return centerX - getHorizontalViewSize() + (2 * getHorizontalViewSize() * panelX / getWidth());
	}
	
	int getMapY(int panelY) {
		return (int) ((centerY - getVerticalViewSize()) + getVerticalViewSize() / (getHeight() / 2D) * (getHeight() - panelY));
	}
	
	int getPanelX(double mapX) {
		return ((int) mapX - centerX + getHorizontalViewSize()) * getWidth() / 2 / getHorizontalViewSize();
	}
	
	int getPanelY(double mapY) {
		return -(((int) mapY - centerY - getVerticalViewSize()) * getHeight() / 2 / getVerticalViewSize());
	}
	
	private void setZoom(int z) {
		zoom = Math.min(Math.max(z, 1), 5000); //Cuz we cant / by zero
		
		if (getMapX(getWidth()) < 0) {
			centerX = 0;
		} else if (getMapX(0) > 6400) {
			centerX = 6400;
		}
		if (getMapY(0) < 1000) {
			centerY = 1000;
		} else if (getMapY(getHeight()) > 11000) {
			centerY = 11000;
		}
	}
	
	private Point currentMousePos;
	
	public class MapPanelMouseAdapter extends MouseAdapter {
		
		@Override
		public void mouseMoved(MouseEvent e) {
			currentMousePos = e.getPoint();
			repaint();
		}
		
		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			boolean zoomIn = e.getWheelRotation() < 0;
			int newZoom = zoom - e.getWheelRotation();
			if (zoomIn) newZoom += (newZoom * ZOOM_MODIFIER);
			else newZoom -= (newZoom * ZOOM_MODIFIER);
			
			setZoom(newZoom);
			repaint();
		}
	}
	
}