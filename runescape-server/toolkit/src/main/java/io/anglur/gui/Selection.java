package io.anglur.gui;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

public class Selection {
	
	private Area area;
	
	public Selection(Area area) {
		this.area = area;
	}
	
	public Area translate(MapPanel context) {
		GeneralPath onScreen = new GeneralPath(Path2D.WIND_EVEN_ODD);
		PathIterator it = area.getPathIterator(null);
		while (!it.isDone()) {
			double[] coords = new double[6];
			switch (it.currentSegment(coords)) {
				case PathIterator.SEG_LINETO:
					onScreen.lineTo(context.getPanelX(coords[0]), context.getPanelY(coords[1]));
					break;
				case PathIterator.SEG_MOVETO:
					onScreen.moveTo(context.getPanelX(coords[0]), context.getPanelY(coords[1]));
					break;
				case PathIterator.SEG_CLOSE:
					onScreen.closePath();
					break;
				default:
					System.err.println("path translate unhandled op!! " + it.currentSegment(coords) + " " + coords[0] + " " + coords[1]);
					break;
			}
			it.next();
		}
		return new Area(onScreen);
	}
	
	public void draw(MapPanel context, Graphics2D g) {
		Area screenArea = translate(context);
		g.setColor(new Color(0x44000000 | (Color.MAGENTA.getRGB() & 0xFFFFFF), true));
		g.fill(screenArea);
		g.setColor(new Color(0xAA000000 | (Color.MAGENTA.getRGB() & 0xFFFFFF), true));
		g.fill(new BasicStroke(3).createStrokedShape(screenArea));
	}
	
}
