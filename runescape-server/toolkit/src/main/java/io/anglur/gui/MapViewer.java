package io.anglur.gui;

import io.anglur.map.RegionChunk;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class MapViewer extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	public static void main(String[] args) throws IOException {
		open(ImageIO.read(new java.io.File("./map_103.png")), 0, 0, null);
	}
	
	private MapPanel map;
	private DragTool drag;
	private Map<Integer, RegionChunk> regions;
	private CopyOnWriteArrayList<Integer> activeRegions = new CopyOnWriteArrayList<>();
	private CopyOnWriteArrayList<Integer> previousRegions;
	
	public static MapViewer open(BufferedImage image, int total, int valid, Map<Integer, RegionChunk> regions) {
		MapViewer frame = new MapViewer();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.initComponents(image, total, valid, regions);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		return frame;
	}
	
	private void initComponents(BufferedImage image, int total, int valid, Map<Integer, RegionChunk> regions) {
		this.map = new MapPanel(this, image, total, valid);
		this.drag = new DragTool(this);
		this.regions = regions;
		this.activeRegions = new CopyOnWriteArrayList<>();
		this.previousRegions = new CopyOnWriteArrayList<>();
		
		map.addMouseListener(drag);
		map.addMouseMotionListener(drag);
		
		getContentPane().add(BorderLayout.CENTER, map);
		setTitle("Realtime OSRS Worldmap");
		setSize(1000, 800);
	}
	
	public MapPanel map() {
		return map;
	}
	
	public DragTool drag() {
		return drag;
	}
	
	public Map<Integer, RegionChunk> regions() {
		return regions;
	}
	
	public CopyOnWriteArrayList<Integer> activeRegions() {
		return activeRegions;
	}
	
	public CopyOnWriteArrayList<Integer> previousRegions() {
		return previousRegions;
	}
	
	
}
