package io.anglur.gui;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DragTool extends MouseAdapter {
	
	private MapViewer context;
	private int lastDragX;
	private int lastDragY;
	private boolean isLeftDrag = false;
	
	private int baseX;
	private int baseY;
	public Rectangle zoomTo = new Rectangle();
	
	public DragTool(MapViewer ctx) {
		context = ctx;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			// DRAG TO MOVE
			isLeftDrag = true;
			lastDragX = context.map().getMapX(e.getX());
			lastDragY = context.map().getMapY(e.getY());
			
			String s = "tele " + lastDragX + " " + lastDragY;
			StringSelection select = new StringSelection(s);
			Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
			clip.setContents(select, null);
			System.out.println("Coords: " + lastDragX + ", " + lastDragY);
			System.out.println("Copied - " + s + " - to clipboard.");
		} else {
			// ZOOM TO SELECTION
			isLeftDrag = false;
			baseX = context.map().getMapX(e.getX());
			baseY = context.map().getMapY(e.getY());
		}
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if (isLeftDrag) {
			// Calculate amount of tiles that the cursor moved
			int ox = context.map().getMapX(e.getX()) - lastDragX;
			int oy = context.map().getMapY(e.getY()) - lastDragY;
			if (ox != 0 || oy != 0) { // check if the tile changed
				// center around the new tile
				context.map().centerX -= context.map().getMapX(e.getX()) - lastDragX;
				context.map().centerY -= context.map().getMapY(e.getY()) - lastDragY;
				lastDragX = context.map().getMapX(e.getX());
				lastDragY = context.map().getMapY(e.getY());
				context.map().repaint();
			}
		} else {
			// DRAW SELECTION ZOOM RECTANGLE
			int currX = context.map().getMapX(e.getX());
			int currY = context.map().getMapY(e.getY());
			// swap start and end coordinates if needed (height and width must
			// be positive)
			int x = Math.min(baseX, currX);
			int y = Math.min(baseY, currY);
			int w = Math.max(baseX, currX) - x;
			int h = Math.max(baseY, currY) - y;
			zoomTo = new Rectangle(x, y, w, h);
			context.map().repaint();
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.getButton() != MouseEvent.BUTTON1) {
			context.map().setView(zoomTo);
			zoomTo = new Rectangle();
		}
	}
	
}
