package io.anglur;

import io.anglur.client.GameClient;
import io.anglur.gui.MapViewer;
import io.anglur.map.RegionChunk;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by Jonathan on 6/13/2015.
 */
public final class XTEADumper {
	
	public static Mode mode = Mode.REFLECTION;
	
	private static MapViewer mapViewer;
	
	public static void main(String[] args) throws IOException {
		mapViewer = MapDrawer.drawMap();
		
		new Timer(1000, e -> {
			for (int region : mapViewer.previousRegions()) {
				RegionChunk chunk = mapViewer.regions().get(region);
				if (chunk != null) {
					chunk.stage(RegionChunk.GOOD);
					chunk.drawOn(mapViewer.map().img);
				}
			}
			mapViewer.previousRegions().clear();
			for (int region : mapViewer.activeRegions()) {
				RegionChunk chunk = mapViewer.regions().get(region);
				if (chunk != null) {
					chunk.stage(RegionChunk.ACTIVE);
					chunk.drawOn(mapViewer.map().img);
				}
			}
			
			mapViewer.map().repaint();
		}).start();
		
		mode.start();
	}
	
	public static MapViewer mapViewer() {
		return mapViewer;
	}
	
	private enum Mode {
		REFLECTION {
			@Override
			void start() {
				new GameClient();
			}
		},;
		
		abstract void start();
	}
	
}
