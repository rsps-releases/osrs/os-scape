package nl.bartpelle.veteres;

import nl.bartpelle.dawnguard.DataStore;

/**
 * Created by Bart on 1/21/2016.
 */
public class PackDwh {
	
	
	public static void main(String[] args) {
		DataStore dwh = new DataStore("C:\\Users\\Bart\\Documents\\osrs-server\\data\\filestore-dwh", true);
		DataStore ours = new DataStore("C:\\Users\\Bart\\Documents\\osrs-server\\data\\filestore", true);
		
		
		pack(dwh, ours);
		dwh.getDescriptorIndex().write(5, ours.getDescriptorIndex().getArchive(5));
		dwh.getIndex(2).getContainer(6).addFile(0, ours.getIndex(2).getContainer(6).getFileData(0), true);
	}
	
	
	public static boolean pack(DataStore to, DataStore from) {
		try {
			int e = from.getIndex(5).getLastArchiveId();
			
			for (int c = 0; c <= e; ++c) {
				byte[] d = from.getIndex(5).getArchive(c);
				if (d != null)
					to.getIndex(5).write(c, d);
			}
			
			return true;
		} catch (Exception var4) {
			var4.printStackTrace();
			return false;
		}
	}
	
}
