package nl.bartpelle.veteres.toolkit.interfacearchitect.widgets;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import net.openrs.cache.sprite.Sprite;
import nl.bartpelle.veteres.toolkit.interfacearchitect.BaseWidget;
import nl.bartpelle.veteres.toolkit.interfacearchitect.InterfaceArchitect;
import nl.bartpelle.veteres.toolkit.interfacearchitect.WidgetType;

import java.nio.ByteBuffer;

public class SpriteWidget extends BaseWidget {
	
	private static final Image MISSING_IMAGE = new Image("/interfacebuilder/img/image-not-found.png");
	
	private Image image;
	private boolean imageNotFound = true;
	
	public SpriteWidget(InterfaceArchitect app, int id) {
		super(app);
		
		try {
			Sprite decoded = Sprite.decode(ByteBuffer.wrap(application.fs().getFileDirect(8, id, 0)));
			image = SwingFXUtils.toFXImage(decoded.getFrame(0), null);
			imageNotFound = false;
		} catch (Exception e) {
			System.err.println("Cannot load image: " + id);
			e.printStackTrace();
		}
	}
	
	@Override
	public WidgetType getType() {
		return WidgetType.SPRITE;
	}
	
	@Override
	public void render(GraphicsContext g, long now, boolean selected) {
		if (imageNotFound) {
			g.drawImage(MISSING_IMAGE, x, y, width, height);
		} else {
			g.drawImage(image, x, y);
		}
		
		super.render(g, now, selected);
	}
	
}
