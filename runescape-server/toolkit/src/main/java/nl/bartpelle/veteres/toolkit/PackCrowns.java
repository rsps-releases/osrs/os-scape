package nl.bartpelle.veteres.toolkit;

import net.openrs.cache.Container;
import net.openrs.cache.FileStore;
import net.openrs.cache.ReferenceTable;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.zip.CRC32;

/**
 * Created by Bart on 4/16/2016.
 */
public class PackCrowns {
	
	public static void main(String[] args) throws Exception {
		FileStore store_us = FileStore.open("data/filestore");
		
		// Store icons
		byte[] crowndata = Files.readAllBytes(new File("crown_icons.bin").toPath());
		store_us.write(8, 423, ByteBuffer.wrap(crowndata));
		
		Container our_rt = Container.decode(store_us.read(255, 8));
		ReferenceTable rt = ReferenceTable.decode(our_rt.getData());
		rt.setVersion(rt.getVersion() + 1); // Increase old version
		CRC32 crc32 = new CRC32();
		crc32.update(crowndata, 0, crowndata.length - 2);
		rt.getEntry(423).setCrc((int) crc32.getValue());
		rt.getEntry(423).setVersion(rt.getEntry(423).getVersion() + 1);
		our_rt = new Container(our_rt.getType(), rt.encode());
		store_us.write(255, 8, our_rt.encode());
	}
}
