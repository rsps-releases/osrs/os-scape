package nl.bartpelle.veteres.toolkit.interfacearchitect;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class BaseWidget {
	
	protected InterfaceArchitect application;
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	
	public BaseWidget(InterfaceArchitect application) {
		this.application = application;
	}
	
	public BaseWidget bounds(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		return this;
	}
	
	public abstract WidgetType getType();
	
	public void render(GraphicsContext g, long now, boolean selected) {
		if (selected) {
			g.setStroke(Color.YELLOW);
			g.strokeRect(x, y, width, height);
		}
	}
	
	protected boolean draggable() {
		return true;
	}
	
}
