package nl.bartpelle.veteres.toolkit;

import nl.bartpelle.dawnguard.DataStore;

/**
 * Created by Jonathan on 6/5/2017.
 */
public class PackItemsNpcsMapsObjects {
	
	public static void main(String[] args) {
		DataStore latestCache = new DataStore("C:\\Storage\\Sync\\Projects\\Java\\RuneScape\\RSPS Junk\\Tools\\Cache Downloader OSRS\\archive\\rev143\\");
		DataStore ours = new DataStore("./data/filestore/");
		
/*		System.out.println("Packed items:" + ours.getIndex(2).write(10, latestCache.getIndex(2).getArchive(10)));
		System.out.println("Packed items:" + ours.getIndex(2).write(10, latestCache.getIndex(2).getArchive(10)));
		System.out.println("Packed npcs:" + ours.getIndex(2).write(9, latestCache.getIndex(2).getArchive(9)));*/
		//System.out.println("Packed models:" + ours.getIndex(7).pack(latestCache));
		System.out.println("Packed models:" + pack(2, ours, latestCache));
		//System.out.println("Packed objects:" + ours.getIndex(2).write(6, latestCache.getIndex(2).getArchive(6)));
		//System.out.println("Packed maps:" + ours.getIndex(5).pack(latestCache));
		
	}
	
	public static boolean pack(int index, DataStore to, DataStore from) {
		try {
			int lastContainer = from.getIndex(index).getLastArchiveId();
			
			for (int c = 0; c <= lastContainer; ++c) {
				write(c, to, from.getIndex(index).getArchive(c));
			}
			
			return true;
		} catch (Exception var4) {
			var4.printStackTrace();
			return false;
		}
	}
	
	public static boolean write(int id, DataStore to, byte[] data) {
		if (data == null) {
			return true;
		}
		boolean b = to.getDescriptorIndex().writeArchive(id, data, data == null ? 0 : data.length, true);
		return b || to.getDescriptorIndex().writeArchive(id, data, data == null ? 0 : data.length, false);
	}
	
}
