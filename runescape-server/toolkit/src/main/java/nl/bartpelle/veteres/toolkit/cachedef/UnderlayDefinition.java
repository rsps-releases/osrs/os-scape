package nl.bartpelle.veteres.toolkit.cachedef;

import nl.bartpelle.veteres.fs.Definition;
import nl.bartpelle.veteres.io.RSBuffer;

public class UnderlayDefinition implements Definition {
	
	public int rgb = 0;
	public int saturation;
	public int brightness;
	public int hueDivisor;
	public int hue;
	
	void decode(RSBuffer var1, int code) {
		if (code == 1) {
			rgb = var1.readTriByte();
		}
		postCalc(rgb);
	}
	
	void postCalc(int rgb) {
		double var3 = (double) (rgb >> 16 & 255) / 256.0D;
		double var11 = (double) (rgb >> 8 & 255) / 256.0D;
		double var5 = (double) (rgb & 255) / 256.0D;
		double var7 = var3;
		if (var11 < var3) {
			var7 = var11;
		}
		
		if (var5 < var7) {
			var7 = var5;
		}
		
		double var9 = var3;
		if (var11 > var3) {
			var9 = var11;
		}
		
		if (var5 > var9) {
			var9 = var5;
		}
		
		double var17 = 0.0D;
		double var15 = 0.0D;
		double var13 = (var7 + var9) / 2.0D;
		if (var7 != var9) {
			if (var13 < 0.5D) {
				var15 = (var9 - var7) / (var9 + var7);
			}
			
			if (var13 >= 0.5D) {
				var15 = (var9 - var7) / (2.0D - var9 - var7);
			}
			
			if (var9 == var3) {
				var17 = (var11 - var5) / (var9 - var7);
			} else if (var11 == var9) {
				var17 = (var5 - var3) / (var9 - var7) + 2.0D;
			} else if (var5 == var9) {
				var17 = (var3 - var11) / (var9 - var7) + 4.0D;
			}
		}
		
		var17 /= 6.0D;
		saturation = (int) (var15 * 256.0D);
		brightness = (int) (var13 * 256.0D);
		if (saturation < 0) {
			saturation = 0;
		} else if (saturation > 255) {
			saturation = 255;
		}
		
		if (brightness < 0) {
			brightness = 0;
		} else if (brightness > 255) {
			brightness = 255;
		}
		
		if (var13 > 0.5D) {
			hueDivisor = (int) (512.0D * var15 * (1.0D - var13));
		} else {
			hueDivisor = (int) (var13 * var15 * 512.0D);
		}
		
		if (hueDivisor < 1) {
			hueDivisor = 1;
		}
		
		hue = (int) ((double) (hueDivisor) * var17);
	}
	
	public void decode(RSBuffer buffer) {
		while (true) {
			int code = buffer.readUByte();
			if (code == 0) {
				return;
			}
			
			decode(buffer, code);
		}
	}
	
}
