package nl.bartpelle.veteres.toolkit.interfacearchitect.loader;

import nl.bartpelle.veteres.io.RSBuffer;

public class WidgetDefinition {
	
	public static final int TYPE_CONTAINER = 0;
	public static final int TYPE_LABEL = 4;
	public static final int TYPE_IMAGE = 5;
	public static final int TYPE_MEDIA = 6;
	
	public Object[] objectArray3;
	public Object[] objectArray20;
	public Object[] objectArray22;
	public Object[] objectArray23;
	public Object[] objectArray24;
	public Object[] objectArray25;
	public Object[] objectArray26;
	public Object[] objectArray27;
	public Object[] objectArray28;
	public Object[] objectArray29;
	public WidgetDefinition[] children;
	public boolean cs2Inter = false;
	public int childHash = -1;
	public int int438 = -1;
	public int actionType = 0;
	public int interactType = 0;
	public int int458 = 0;
	public int int500 = 0;
	public int int442 = 0;
	public int int443 = 0;
	public int x = 0;
	public int y = 0;
	public int width = 0;
	public int height = 0;
	public int int448 = 0;
	public int int447 = 0;
	public int int472 = 0;
	public int int466 = 0;
	public int int493 = 1;
	public int int452 = 1;
	public int hoverPopup = -1;
	public boolean hidden = false;
	public int int453 = 0;
	public int int435 = 0;
	public int int454 = 0;
	public int int455 = 0;
	public int color = 0;
	public int activeColor = 0;
	public int hoverColor = 0;
	public int hoverActiveColor = 0;
	public boolean filled = false;
	public int alpha;
	public int int460;
	public int int461;
	public boolean bool41;
	public int spriteId;
	public int int463;
	public int int436;
	public boolean bool42;
	public int int470;
	public int int491;
	public int mediaType;
	public int mediaID;
	int activeMediaType;
	int activeMediaID;
	public int animation;
	public int activeAnimation;
	public int int457;
	public int int474;
	public int rotationX;
	public int rotationY;
	public int int477;
	public int zoom;
	public int int464;
	public int int479;
	public boolean bool45;
	public int int480;
	public int fontId;
	public String componentString;
	public String activeComponentString;
	public int int451;
	public int textAlign;
	public int int483;
	public boolean hasShadow;
	public int itemPaddingX;
	public int itemPaddingY;
	public int defaultSettings;
	public String string29;
	public WidgetDefinition widget3;
	public int int489;
	public int int490;
	public boolean bool47;
	public String selectedActionName;
	public boolean bool48;
	public int int441;
	public String spellName;
	public String tooltip;
	public int int496;
	public int int465;
	public int int498;
	public int int445;
	public boolean bool39;
	public boolean bool50;
	public int int501;
	public int int495;
	public int int502;
	public int int497;
	public int int504;
	public int int473;
	public boolean bool40;
	public boolean bool51;
	public int widgetType;
	public int[] conditionTypes;
	public int[] conditionValues;
	public int[][] cs1Opcodes;
	public int[] items;
	public int[] itemAmounts;
	public int[] spritesX;
	public int[] spritesY;
	public int[] sprites;
	public String[] interfaceOptions;
	public boolean flipHorizontal;
	public String[] regularOptions;
	public boolean flipVertical;
	public Object[] objectArray6;
	public Object[] objectArray10;
	public Object[] objectArray21;
	public Object[] objectArray15;
	public Object[] objectArray14;
	public Object[] onConfigTrigger;
	public Object[] onItemUpdateTrigger;
	public Object[] onSkillUpdateTrigger;
	public Object[] objectArray17;
	public Object[] objectArray18;
	public Object[] objectArray11;
	public Object[] objectArray7;
	public Object[] objectArray8;
	public Object[] objectArray5;
	public Object[] objectArray9;
	public Object[] objectArray30;
	public Object[] objectArray13;
	public Object[] objectArray19;
	public int[] mouseWheelTrigger;
	public int[] configChangeTriggers;
	public int[] itemUpdateTriggers;
	
	public WidgetDefinition() {
		this.alpha = 0;
		this.int460 = 0;
		this.int461 = 1;
		this.bool41 = false;
		this.spriteId = -1;
		this.int463 = -1;
		this.int436 = 0;
		this.bool42 = false;
		this.int470 = 0;
		this.int491 = 0;
		this.mediaType = 1;
		this.mediaID = -1;
		this.activeMediaType = 1;
		this.activeMediaID = -1;
		this.animation = -1;
		this.activeAnimation = -1;
		this.int457 = 0;
		this.int474 = 0;
		this.rotationX = 0;
		this.rotationY = 0;
		this.int477 = 0;
		this.zoom = 100;
		this.int464 = 0;
		this.int479 = 0;
		this.bool45 = false;
		this.int480 = 2;
		this.fontId = -1;
		this.componentString = "";
		this.activeComponentString = "";
		this.int451 = 0;
		this.textAlign = 0;
		this.int483 = 0;
		this.hasShadow = false;
		this.itemPaddingX = 0;
		this.itemPaddingY = 0;
		this.defaultSettings = 0;
		this.string29 = "";
		this.widget3 = null;
		this.int489 = 0;
		this.int490 = 0;
		this.bool47 = false;
		this.selectedActionName = "";
		this.bool48 = false;
		this.int441 = -1;
		this.spellName = "";
		this.tooltip = "Ok";
		this.int496 = -1;
		this.int465 = 0;
		this.int498 = 0;
		this.int445 = 0;
		this.bool39 = false;
		this.bool50 = false;
		this.int501 = -1;
		this.int495 = 0;
		this.int502 = 0;
		this.int497 = 0;
		this.int504 = -1;
		this.int473 = -1;
		this.bool40 = false;
		this.bool51 = false;
	}
	
	public void load(byte[] bytes) {
		if (bytes[0] == -1) {
			unpackNew(new RSBuffer(bytes));
		} else {
			unpackOld(new RSBuffer(bytes));
		}
	}
	
	private void unpackOld(RSBuffer buffer) {
		this.cs2Inter = false;
		this.widgetType = buffer.readUByte();
		this.actionType = buffer.readUByte();
		this.interactType = buffer.readUShort();
		this.x = buffer.readShort();
		this.y = buffer.readShort();
		this.width = buffer.readUShort();
		this.height = buffer.readUShort();
		this.alpha = buffer.readUByte();
		this.hoverPopup = buffer.readUShort();
		if (this.hoverPopup == 65535) {
			this.hoverPopup = -1;
		} else {
			this.hoverPopup += this.childHash & -65536;
		}
		
		this.int441 = buffer.readUShort();
		if (this.int441 == 65535) {
			this.int441 = -1;
		}
		
		int conditionCount = buffer.readUByte();
		int opcodeCount;
		if (conditionCount > 0) {
			this.conditionTypes = new int[conditionCount];
			this.conditionValues = new int[conditionCount];
			
			for (opcodeCount = 0; opcodeCount < conditionCount; opcodeCount++) {
				this.conditionTypes[opcodeCount] = buffer.readUByte();
				this.conditionValues[opcodeCount] = buffer.readUShort();
			}
		}
		
		opcodeCount = buffer.readUByte();
		int actionIndex;
		int subOpcode;
		int opcode;
		if (opcodeCount > 0) {
			this.cs1Opcodes = new int[opcodeCount][];
			
			for (actionIndex = 0; actionIndex < opcodeCount; actionIndex++) {
				subOpcode = buffer.readUShort();
				this.cs1Opcodes[actionIndex] = new int[subOpcode];
				
				for (opcode = 0; opcode < subOpcode; opcode++) {
					this.cs1Opcodes[actionIndex][opcode] = buffer.readUShort();
					if (this.cs1Opcodes[actionIndex][opcode] == 65535) {
						this.cs1Opcodes[actionIndex][opcode] = -1;
					}
				}
			}
		}
		
		if (this.widgetType == 0) {
			this.int455 = buffer.readUShort();
			this.hidden = buffer.readUByte() == 1;
		}
		
		if (this.widgetType == 1) {
			buffer.readUShort();
			buffer.readUByte();
		}
		
		if (this.widgetType == 2) {
			this.items = new int[this.width * this.height];
			this.itemAmounts = new int[this.height * this.width];
			actionIndex = buffer.readUByte();
			if (actionIndex == 1) {
				this.defaultSettings |= 268435456;
			}
			
			subOpcode = buffer.readUByte();
			if (subOpcode == 1) {
				this.defaultSettings |= 1073741824;
			}
			
			opcode = buffer.readUByte();
			if (opcode == 1) {
				this.defaultSettings |= Integer.MIN_VALUE;
			}
			
			int i_8 = buffer.readUByte();
			if (i_8 == 1) {
				this.defaultSettings |= 536870912;
			}
			
			this.itemPaddingX = buffer.readUByte();
			this.itemPaddingY = buffer.readUByte();
			this.spritesX = new int[20];
			this.spritesY = new int[20];
			this.sprites = new int[20];
			
			int index;
			for (index = 0; index < 20; index++) {
				int i_10 = buffer.readUByte();
				if (i_10 == 1) {
					this.spritesX[index] = buffer.readShort();
					this.spritesY[index] = buffer.readShort();
					this.sprites[index] = buffer.readInt();
				} else {
					this.sprites[index] = -1;
				}
			}
			
			this.interfaceOptions = new String[5];
			
			for (index = 0; index < 5; index++) {
				String string_12 = buffer.readString();
				if (string_12.length() > 0) {
					this.interfaceOptions[index] = string_12;
					this.defaultSettings |= 1 << index + 23;
				}
			}
		}
		
		if (this.widgetType == 3) {
			this.filled = buffer.readUByte() == 1;
		}
		
		if (this.widgetType == 4 || this.widgetType == 1) {
			this.textAlign = buffer.readUByte();
			this.int483 = buffer.readUByte();
			this.int451 = buffer.readUByte();
			this.fontId = buffer.readUShort();
			if (this.fontId == 65535) {
				this.fontId = -1;
			}
			
			this.hasShadow = buffer.readUByte() == 1;
		}
		
		if (this.widgetType == 4) {
			this.componentString = buffer.readString();
			this.activeComponentString = buffer.readString();
		}
		
		if (this.widgetType == 1 || this.widgetType == 3 || this.widgetType == 4) {
			this.color = buffer.readInt();
		}
		
		if (this.widgetType == 3 || this.widgetType == 4) {
			this.activeColor = buffer.readInt();
			this.hoverColor = buffer.readInt();
			this.hoverActiveColor = buffer.readInt();
		}
		
		if (this.widgetType == 5) {
			this.spriteId = buffer.readInt();
			this.int463 = buffer.readInt();
		}
		
		if (this.widgetType == 6) {
			this.mediaType = 1;
			this.mediaID = buffer.readUShort();
			if (this.mediaID == 65535) {
				this.mediaID = -1;
			}
			
			this.activeMediaType = 1;
			this.activeMediaID = buffer.readUShort();
			if (this.activeMediaID == 65535) {
				this.activeMediaID = -1;
			}
			
			this.animation = buffer.readUShort();
			if (this.animation == 65535) {
				this.animation = -1;
			}
			
			this.activeAnimation = buffer.readUShort();
			if (this.activeAnimation == 65535) {
				this.activeAnimation = -1;
			}
			
			this.zoom = buffer.readUShort();
			this.rotationX = buffer.readUShort();
			this.rotationY = buffer.readUShort();
		}
		
		if (this.widgetType == 7) {
			this.items = new int[this.height * this.width];
			this.itemAmounts = new int[this.height * this.width];
			this.textAlign = buffer.readUByte();
			this.fontId = buffer.readUShort();
			if (this.fontId == 65535) {
				this.fontId = -1;
			}
			
			this.hasShadow = buffer.readUByte() == 1;
			this.color = buffer.readInt();
			this.itemPaddingX = buffer.readShort();
			this.itemPaddingY = buffer.readShort();
			actionIndex = buffer.readUByte();
			if (actionIndex == 1) {
				this.defaultSettings |= 1073741824;
			}
			
			this.interfaceOptions = new String[5];
			
			for (subOpcode = 0; subOpcode < 5; subOpcode++) {
				String string_11 = buffer.readString();
				if (string_11.length() > 0) {
					this.interfaceOptions[subOpcode] = string_11;
					this.defaultSettings |= 1 << subOpcode + 23;
				}
			}
		}
		
		if (this.widgetType == 8) {
			this.componentString = buffer.readString();
		}
		
		if (this.actionType == 2 || this.widgetType == 2) {
			this.selectedActionName = buffer.readString();
			this.spellName = buffer.readString();
			actionIndex = buffer.readUShort() & 63;
			this.defaultSettings |= actionIndex << 11;
		}
		
		if (this.actionType == 1 || this.actionType == 4 || this.actionType == 5 || this.actionType == 6) {
			this.tooltip = buffer.readString();
			if (this.tooltip.length() == 0) {
				if (this.actionType == 1) {
					this.tooltip = "Ok";
				}
				
				if (this.actionType == 4) {
					this.tooltip = "Select";
				}
				
				if (this.actionType == 5) {
					this.tooltip = "Select";
				}
				
				if (this.actionType == 6) {
					this.tooltip = "Continue";
				}
			}
		}
		
		if (this.actionType == 1 || this.actionType == 4 || this.actionType == 5) {
			this.defaultSettings |= 4194304;
		}
		
		if (this.actionType == 6) {
			this.defaultSettings |= 1;
		}
		
	}
	
	private void unpackNew(RSBuffer buffer) {
		buffer.readUByte();
		this.cs2Inter = true;
		this.widgetType = buffer.readUByte();
		this.interactType = buffer.readUShort();
		this.x = buffer.readShort();
		this.y = buffer.readShort();
		this.width = buffer.readUShort();
		if (this.widgetType == 9) {
			this.height = buffer.readShort();
		} else {
			this.height = buffer.readUShort();
		}
		
		this.int442 = buffer.readByte();
		this.int443 = buffer.readByte();
		this.int458 = buffer.readByte();
		this.int500 = buffer.readByte();
		
		this.hoverPopup = buffer.readUShort();
		if (this.hoverPopup == 65535) {
			this.hoverPopup = -1;
		} else {
			this.hoverPopup += this.childHash & -65536;
		}
		
		this.hidden = buffer.readUByte() == 1;
		if (this.widgetType == 0) {
			this.int454 = buffer.readUShort();
			this.int455 = buffer.readUShort();
			this.bool40 = buffer.readUByte() == 1;
		}
		
		if (this.widgetType == 5) {
			this.spriteId = buffer.readInt();
			this.int436 = buffer.readUShort();
			this.bool42 = buffer.readUByte() == 1;
			this.alpha = buffer.readUByte();
			this.int470 = buffer.readUByte();
			this.int491 = buffer.readInt();
			this.flipHorizontal = buffer.readUByte() == 1;
			this.flipVertical = buffer.readUByte() == 1;
		}
		
		if (this.widgetType == 6) {
			this.mediaType = 1;
			this.mediaID = buffer.readUShort();
			if (this.mediaID == 65535) {
				this.mediaID = -1;
			}
			
			this.int457 = buffer.readShort();
			this.int474 = buffer.readShort();
			this.rotationX = buffer.readUShort();
			this.rotationY = buffer.readUShort();
			this.int477 = buffer.readUShort();
			this.zoom = buffer.readUShort();
			this.animation = buffer.readUShort();
			if (this.animation == 65535) {
				this.animation = -1;
			}
			
			this.bool45 = buffer.readUByte() == 1;
			buffer.readUShort();
			if (this.int442 != 0) {
				this.int464 = buffer.readUShort();
			}
			
			if (this.int443 != 0) {
				buffer.readUShort();
			}
		}
		
		if (this.widgetType == 4) {
			this.fontId = buffer.readUShort();
			if (this.fontId == 65535) {
				this.fontId = -1;
			}
			
			this.componentString = buffer.readString();
			this.int451 = buffer.readUByte();
			this.textAlign = buffer.readUByte();
			this.int483 = buffer.readUByte();
			this.hasShadow = buffer.readUByte() == 1;
			this.color = buffer.readInt();
		}
		
		if (this.widgetType == 3) {
			this.color = buffer.readInt();
			this.filled = buffer.readUByte() == 1;
			this.alpha = buffer.readUByte();
		}
		
		if (this.widgetType == 9) {
			this.int461 = buffer.readUByte();
			this.color = buffer.readInt();
			this.bool41 = buffer.readUByte() == 1;
		}
		
		this.defaultSettings = buffer.readTriByte();
		this.string29 = buffer.readString();
		int i_3 = buffer.readUByte();
		if (i_3 > 0) {
			this.regularOptions = new String[i_3];
			
			for (int i_4 = 0; i_4 < i_3; i_4++) {
				this.regularOptions[i_4] = buffer.readString();
			}
		}
		
		this.int489 = buffer.readUByte();
		this.int490 = buffer.readUByte();
		this.bool47 = buffer.readUByte() == 1;
		this.selectedActionName = buffer.readString();
		
		this.objectArray6 = this.method596(buffer);
		this.objectArray10 = this.method596(buffer);
		this.objectArray21 = this.method596(buffer);
		this.objectArray15 = this.method596(buffer);
		this.objectArray14 = this.method596(buffer);
		this.onConfigTrigger = this.method596(buffer);
		this.onItemUpdateTrigger = this.method596(buffer);
		this.onSkillUpdateTrigger = this.method596(buffer);
		this.objectArray17 = this.method596(buffer);
		this.objectArray18 = this.method596(buffer);
		this.objectArray11 = this.method596(buffer);
		this.objectArray7 = this.method596(buffer);
		this.objectArray8 = this.method596(buffer);
		this.objectArray5 = this.method596(buffer);
		this.objectArray9 = this.method596(buffer);
		this.objectArray30 = this.method596(buffer);
		this.objectArray13 = this.method596(buffer);
		this.objectArray19 = this.method596(buffer);
		this.mouseWheelTrigger = this.method597(buffer);
		this.configChangeTriggers = this.method597(buffer);
		this.itemUpdateTriggers = this.method597(buffer);
	}
	
	Object[] method596(RSBuffer stream_1) {
		int i_3 = stream_1.readUByte();
		if (i_3 == 0) {
			return null;
		} else {
			Object[] arr = new Object[i_3];
			
			for (int i_5 = 0; i_5 < i_3; i_5++) {
				int i_6 = stream_1.readUByte();
				if (i_6 == 0) {
					arr[i_5] = stream_1.readInt();
				} else if (i_6 == 1) {
					arr[i_5] = stream_1.readString();
				}
			}
			
			this.bool48 = true;
			return arr;
		}
	}
	
	int[] method597(RSBuffer stream_1) {
		int i_3 = stream_1.readUByte();
		if (i_3 == 0) {
			return null;
		} else {
			int[] ints = new int[i_3];
			
			for (int i_5 = 0; i_5 < i_3; i_5++) {
				ints[i_5] = stream_1.readInt();
			}
			
			return ints;
		}
	}
	
}
