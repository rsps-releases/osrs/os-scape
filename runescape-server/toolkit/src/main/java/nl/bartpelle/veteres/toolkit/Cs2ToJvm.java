package nl.bartpelle.veteres.toolkit;

import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.dawnguard.util.Compression;
import org.objectweb.asm.*;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.objectweb.asm.Opcodes.*;

/**
 * Created by Bart on 11/24/2015.
 */
public class Cs2ToJvm {
	
	public static void translate(Map<Integer, InstrInfo> info, CS2Script script, DataStore ds) {
		//System.out.println(script.numIntParams + ", " + script.numStringParams + ", " + script.numIntLocals + ", " + script.numStringLocals);
		
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		String cname = "Script" + script.id;
		writer.visit(V1_7, ACC_PUBLIC, cname, null, "java/lang/Object", new String[0]);
		
		// Calculate parameter signature
		StringBuilder signature = new StringBuilder("(");
		for (int i = 0; i < script.numIntParams; i++) {
			signature.append("I");
		}
		for (int i = 0; i < script.numStringParams; i++) {
			signature.append("Ljava/lang/String;");
		}
		signature.append(")Ljava/lang/Object;");
		
		// Labels
		Label start = new Label();
		Label end = new Label();
		
		MethodVisitor mv = writer.visitMethod(ACC_PUBLIC, "run", signature.toString(), null, null);
		mv.visitCode();
		mv.visitJumpInsn(Opcodes.GOTO, start);
		mv.visitLabel(start);
		
		// A map holding our addresses with Labels we want to spit out when time is nigh.
		Map<Integer, Label> queuedLabels = new HashMap<>();
		
		for (int addr = 0; addr < script.numInstructions; addr++) {
			int instr = script.instructions[addr];
			int iop = script.integerOperands[addr];
			int next = addr + 1 >= script.numInstructions ? -1 : script.instructions[addr + 1];
			String strop = script.stringOperands[addr];
			
			// Check for a queued label.
			Label pending = queuedLabels.remove(addr);
			if (pending != null) {
				mv.visitLabel(pending);
			}
			
			if (instr >= 2000 && instr < 3000) { // Interface load
				mv.visitMethodInsn(INVOKESTATIC, cname, "getInterface", "(I)LRSInterface;", false);
				instr -= 1000;
			}
			
			if (instr == 33) { // Load local int
				mv.visitVarInsn(ILOAD, iop + 1); // + 1 because 0 is 'this'
			} else if (instr == 34) { // Set local int
				mv.visitVarInsn(ISTORE, iop + 1); // + 1 because 0 is 'this'
			} else if (instr == 0) { // Push int
				mv.visitLdcInsn(iop);
			} else if (instr == 4012) { // Math.pow
				new MethodInsnNode(INVOKESTATIC, "java/lang/Math", "pow", "(II)I", false).accept(mv);
			} else if (instr == 4000) { // Add ints
				mv.visitInsn(IADD);
			} else if (instr == 4001) { // Subtract ints
				mv.visitInsn(ISUB);
			} else if (instr == 4002) { // Multiply ints
				mv.visitInsn(IMUL);
			} else if (instr == 4003) { // Divide ints
				mv.visitInsn(IDIV);
			} else if (instr == 4011) { // Remainder of division of ints
				mv.visitInsn(IREM);
			} else if (instr == 21) { // Return.. tricky one. Needs work TODO
				System.out.println("Return not properly supported (stack consists of: ");
				//mv.visitMethodInsn(INVOKESTATIC, "java/util/Arrays", "asList", "(II)Ljava/util/Array;", false);
				
				mv.visitInsn(RETURN);
			} else if (instr == 3324) {
				mv.visitMethodInsn(INVOKESTATIC, cname, "worldSettings", "()I", false);
			} else if (instr == 4010) {
				mv.visitMethodInsn(INVOKESTATIC, cname, "checkBit", "(II)I", false);
			} else if (instr == 8) { // This is where it gets harder. Jumps. JMP_EQ
				Label jmpTarg = new Label();
				mv.visitJumpInsn(IF_ICMPEQ, jmpTarg);
				queuedLabels.put(addr + iop + 1, jmpTarg);
			} else if (instr == 6) { // Unconditional jump
				Label jmpTarg = new Label();
				mv.visitJumpInsn(GOTO, jmpTarg);
				queuedLabels.put(addr + iop + 1, jmpTarg);
			} else if (instr == 31) { // This is where it gets harder. Jumps. JMP_EQ
				Label jmpTarg = new Label();
				mv.visitJumpInsn(IF_ICMPLE, jmpTarg);
				queuedLabels.put(addr + iop + 1, jmpTarg);
			} else if (instr == 32) { // This is where it gets harder. Jumps. JMP_EQ
				Label jmpTarg = new Label();
				mv.visitJumpInsn(IF_ICMPGE, jmpTarg);
				queuedLabels.put(addr + iop + 1, jmpTarg);
			} else if (instr == 40) { // HARD AS FUCK LOL jk
				int sid = iop;
				
				// Load that script's defs
				CS2Script toRun = CS2Script.decode(sid, Compression.decompressArchive(ds.getIndex(12).getArchive(sid)));
				
				// Calculate parameter signature
				StringBuilder sig = new StringBuilder("(");
				for (int i = 0; i < toRun.numIntParams; i++) {
					sig.append("I");
				}
				for (int i = 0; i < toRun.numStringParams; i++) {
					sig.append("Ljava/lang/String;");
				}
				sig.append(")V");
				
				mv.visitMethodInsn(INVOKESTATIC, "Script" + sid, "run", sig.toString(), false);
			} else if (instr == 1112) {
				mv.visitInsn(Opcodes.SWAP);
				mv.visitMethodInsn(INVOKEVIRTUAL, "RSInterface", "setText", "(Ljava/lang/String;)V", false);
			} else if (instr == 3100) {
				mv.visitMethodInsn(INVOKESTATIC, cname, "sendMessage", "(Ljava/lang/String;)V", false);
			} else if (instr == 1504) {
				mv.visitMethodInsn(INVOKEVIRTUAL, "RSInterface", "isVisible", "()I", false);
			} else if (instr == 3) {
				mv.visitLdcInsn(strop);
			} else if (instr == 37) { // str cat
				// Initialize the string builder
				mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
				mv.visitInsn(DUP);
				mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
				
				// Add all the strings..
				for (int i = 0; i < iop; i++) {
					mv.visitInsn(Opcodes.SWAP);
					mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
				}
				
				// Finish up the string builder
				mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
			} else if (instr == 4106) { // Int to string
				mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "toString", "(I)Ljava/lang/String;", false);
			} else if (instr == 3300) {
				mv.visitMethodInsn(INVOKESTATIC, cname, "currentCycle", "()I", false);
			} else if (instr == 3408) {
				mv.visitMethodInsn(INVOKESTATIC, cname, "getEnumValue", "(IIII)I", false);
			} else if (instr == 25) {
				mv.visitLdcInsn(iop);
				mv.visitMethodInsn(INVOKESTATIC, cname, "getVarbit", "(I)I", false);
			} else if (instr == 42) {
				mv.visitLdcInsn(iop);
				mv.visitMethodInsn(INVOKESTATIC, cname, "getVarc", "(I)I", false);
			} else if (instr == 43) {
				mv.visitLdcInsn(iop);
				mv.visitInsn(Opcodes.SWAP);
				mv.visitMethodInsn(INVOKESTATIC, cname, "setVarc", "(II)V", false);
			} else if (instr == 47) {
				mv.visitLdcInsn(iop);
				mv.visitMethodInsn(INVOKESTATIC, cname, "getVarcstr", "(I)Ljava/lang/String;", false);
			} else if (instr == 48) {
				mv.visitLdcInsn(iop);
				mv.visitInsn(Opcodes.SWAP);
				mv.visitMethodInsn(INVOKESTATIC, cname, "setVarcstr", "(ILjava/lang/String;)V", false);
			} else if (instr == 35) { // Local string load
				mv.visitVarInsn(ALOAD, iop + 1); // + 1 because 0 is 'this'
			} else if (instr == 36) { // Local string store
				mv.visitVarInsn(ASTORE, iop + 1); // + 1 because 0 is 'this'
			} else {
				System.err.println("Missing instruction " + instr + " at address " + addr);
				
				// Try to guess it if we have info available
				if (info.containsKey(instr)) {
					InstrInfo guess = info.get(instr);
					if (guess.accurate) {
						String sig = "()V";
						
						if (guess.i == -1 && guess.s == 0) {
							sig = "(I)V";
						} else if (guess.i == 1 && guess.s == 0) {
							sig = "()I";
						} else if (guess.i == 0 && guess.s == 1) {
							sig = "()Ljava/lang/String;";
						} else if (guess.i == 0 && guess.s == -1) {
							sig = "(Ljava/lang/String;)V";
						}
						
						mv.visitMethodInsn(INVOKESTATIC, cname, "opcode_" + instr, sig, false);
					}
				}
			}
			
			System.out.println(instr + " " + iop + " " + strop);
		}
		
		mv.visitMaxs(-1, -1);
		mv.visitLabel(end);
		mv.visitEnd();
		writer.visitEnd();
		
		byte[] data = writer.toByteArray();
		ClassReader reader = new ClassReader(data);
		reader.accept(new TraceClassVisitor(new PrintWriter(System.out)), ClassReader.SKIP_DEBUG);
		
		try {
			Files.write(Paths.get("jvmscripts", "Script" + script.id + ".class"), data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(queuedLabels);
		
		reader = new ClassReader(data);
		CheckClassAdapter.verify(reader, false, new PrintWriter(System.err));
	}
	
}
