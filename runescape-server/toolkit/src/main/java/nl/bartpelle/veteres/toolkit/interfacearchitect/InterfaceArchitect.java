package nl.bartpelle.veteres.toolkit.interfacearchitect;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import net.openrs.cache.sprite.Sprite;
import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.veteres.toolkit.interfacearchitect.loader.WidgetDefinition;
import nl.bartpelle.veteres.toolkit.interfacearchitect.widgets.SpriteWidget;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Optional;

@SuppressWarnings("unchecked")
public class InterfaceArchitect extends Application {
	
	private static final Logger logger = LogManager.getLogger(InterfaceArchitect.class);
	private static final Color CANVAS_BG_COLOR = Color.web("#676767");
	
	private DataStore dataStore;
	private RootInterface activeInterface = new RootInterface(this, 500, 400);
	private Canvas canvas;
	private ScrollPane canvasPane;
	private TreeView<Node> hierarchyTree;
	
	@Override
	public void start(Stage stage) throws Exception {
		logger.info("Loading file store...");
		dataStore = new DataStore("data/filestore145");
		logger.info("Loaded file store.");
		
		setUserAgentStylesheet(STYLESHEET_MODENA);
		
		Parent root = FXMLLoader.load(getClass().getResource("/interfacebuilder/interfacearchitect.fxml"));

		Scene scene = new Scene(root, 1280, 800);
		stage.getIcons().add(new Image(getClass().getResourceAsStream("/interfacebuilder/img/icon.png")));

		stage.setTitle("Interface Architect");
		stage.setScene(scene);
		stage.show();
	
		int widgetId = 218;
		loadInterface(widgetId);
		
		canvas = (Canvas) root.lookup("#canvas");
		canvas.setWidth(activeInterface.widthProperty().get());
		canvas.setHeight(activeInterface.heightProperty().get());
		
		// Expand basic components list
		TitledPane basicComponentsPane = (TitledPane) root.lookup("#basicComponentsPane");
		basicComponentsPane.setExpanded(true);
		
		// Expand properties pane
		TitledPane propertiesPane = (TitledPane) root.lookup("#propertiesPane");
		propertiesPane.setExpanded(true);
		
		// Add listeners to resize canvas
		activeInterface.widthProperty().addListener((observable, oldValue, newValue) -> canvas.setWidth(newValue.intValue()));
		activeInterface.heightProperty().addListener((observable, oldValue, newValue) -> canvas.setHeight(newValue.intValue()));
		
		// Add open button
		MenuBar open = (MenuBar) root.lookup("#menuBar");
		for (Menu menu : open.getMenus()) {
			if ("fileMenu".equals(menu.getId())) {
				for (MenuItem menuItem : menu.getItems()) {
					if (menuItem.getId() != null && menuItem.getId().equals("menuOpen")) {
						menuItem.setOnAction(event -> {
							TextInputDialog dialog = new TextInputDialog("walter");
							dialog.setTitle("Open interface");
							dialog.setHeaderText(null);
							dialog.setContentText("Enter an interface ID:");

							Optional<String> s = dialog.showAndWait();
							int id = Integer.parseInt(s.get());
							loadInterface(id);
						});
					}
				}
			}
		}

		// Populate sprite list
		ListView<Node> spriteList = (ListView<Node>) root.lookup("#spritelist");
		System.out.println(spriteList);

		for (int i = 0; i < 20_000; i++) {
			try {
				Sprite decoded = Sprite.decode(ByteBuffer.wrap(fs().getFileDirect(8, i, 0)));
				WritableImage image = SwingFXUtils.toFXImage(decoded.getFrame(0), null);
				ImageView imgv = new ImageView(image);
				Label label = new Label("" + i);
				label.setAlignment(Pos.CENTER_LEFT);
				label.setTextAlignment(TextAlignment.CENTER);

				HBox hbox = new HBox(5, imgv, label);
				hbox.setAlignment(Pos.CENTER_LEFT);
				spriteList.getItems().add(hbox);
			} catch (Exception e) {
			}
		}
		
		ListView<Node> basicComponents = (ListView<Node>) root.lookup("#basicComponents");
		ObservableList<Node> items = basicComponents.getItems();
		items.add(newComponentNode("component_sprite.png", "Sprite"));
		
		AnimationTimer loop = new AnimationTimer() {
			@Override
			public void handle(long now) {
				if (activeInterface.dirty()) {
					GraphicsContext g = canvas.getGraphicsContext2D();
					g.setFill(CANVAS_BG_COLOR);
					g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
					
					activeInterface.render(g, now);
				}
			}
		};
		loop.start();
	}
	
	private Node newComponentNode(String imageResource, String description) throws IOException {
		Node elem = FXMLLoader.load(getClass().getResource("/interfacebuilder/fragment-component.fxml"));
		((Label) elem.lookup("#description")).setText(description);
		ImageView imageView = (ImageView) elem.lookup("#image");
		imageView.setFitHeight(18);
		imageView.setFitWidth(18);
		imageView.setImage(new Image("/interfacebuilder/img/" + imageResource, 18, 18, true, false));
		return elem;
	}
	
	public void loadInterface(int widgetId) {
		WidgetDefinition[] loaded = new WidgetDefinition[dataStore.getFilecount(3, widgetId)];
		for (int i = 0; i < loaded.length; i++) {
			loaded[i] = new WidgetDefinition();
			
			byte[] widgetData = dataStore.getFileDirect(3, widgetId, i);
			loaded[i].load(widgetData);
		}
		activeInterface.load(loaded);
	}
	
	public DataStore fs() {
		return dataStore;
	}
	
}
