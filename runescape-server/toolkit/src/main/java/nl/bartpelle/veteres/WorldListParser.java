package nl.bartpelle.veteres;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


/**
 * Created by Situations on 2016-11-04.
 */

public class WorldListParser {
	
	private byte[] bytes = new byte[5000];
	private int offset;
	
	public static void main(String[] args) {
		WorldListParser parser = new WorldListParser();
		World[] worlds = parser.getWorlds();
		if (worlds != null) {
			for (World world : worlds) {
				System.out.println(world.toString());
			}
		}
	}
	
	public World[] getWorlds() {
		offset = 0;
		try {
			URL url = new URL("http://www.runescape.com/slr.ws?order=WMLPA");
			InputStream is = url.openStream();
			int success = is.read(bytes);
			if (success != 5000) {
				System.err.println("Invalid size");
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int length = bytes[5];
		World[] worlds = new World[length];
		offset = 6;
		for (int i = 0; i < length; i++) {
			World world = new World();
			world.id = readShort();
			world.flags = readInt();
			world.domain = readString();
			world.activity = readString();
			world.location = readByte();
			world.playerCount = readShort();
			world.index = i;
			worlds[i] = world;
		}
		return worlds;
	}
	
	private int readByte() {
		offset += 2;
		return bytes[offset - 1] & 0xF;
	}
	
	private int readShort() {
		offset += 2;
		return ((bytes[offset - 2] & 0xFF) << 8) + (bytes[offset - 1] & 0xFF);
	}
	
	private int readInt() {
		offset += 4;
		return ((bytes[offset - 4] & 0xFF) << 24) + ((bytes[offset - 3] & 0xFF) << 16)
				+ ((bytes[offset - 2] & 0xFF) << 8) + (bytes[offset - 1] & 0xFF);
	}
	
	private String readString() {
		String s = "";
		if (bytes[offset] == 0) {
			offset++;
		}
		while (bytes[offset] != 0) {
			s += (char) bytes[offset];
			offset++;
		}
		return s;
	}
	
}

class World {
	public String activity, domain;
	public int id, index, location, flags, playerCount;
	
	@Override
	public String toString() {
		return String.format("World[index:%d id:%d players:%d location:%d flags:%s domain:'%s' activity:'%s']",
				index, id, playerCount, location, flags, domain, activity);
	}
}
