package nl.bartpelle.veteres;

import java.io.File;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Bart on 12/2/2015.
 */
public class BlockDdos {
	
	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(new File("C:\\users\\bart\\desktop\\debug.log"));
		Set<String> ips = new HashSet<>();
		
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if (line.contains("excess:") && line.contains("POST /community/index.php? HTTP/1.1")) {
				String ip = line.substring(line.indexOf("client: ") + 8);
				ip = ip.substring(0, ip.indexOf(","));
				ips.add(ip);
			}
		}
		
		for (String s : ips) {
			System.out.println("sudo iptables -A INPUT -p tcp -s " + s + " -j DROP");
		}
		//System.out.println(ips);
	}
	
}
