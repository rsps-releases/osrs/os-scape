package nl.bartpelle.veteres;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import nl.bartpelle.veteres.util.Tuple;

import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Bart on 7/30/2015.
 */
public class Flooder {
	
	public static void main(String[] args) throws Exception {
		List<Socket> sockets = new LinkedList<>();
		for (int i = 0; i < 600; i++) {
			Socket socket = new Socket("localhost", 43594);
			
			Tuple<Integer, byte[]> msg = prepareMessage(i);
			socket.getOutputStream().write(msg.second(), 0, msg.first());
			//System.out.println(socket.getInputStream().read());
			
			Thread.sleep(3);
			sockets.add(socket);
			
			System.out.println("Total clients: " + i);
		}
		
		while (true) {
			sockets.forEach(socket -> {
				try {
					socket.getOutputStream().write(25);
					socket.getOutputStream().flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			Thread.sleep(5000);
		}
	}
	
	private static Tuple<Integer, byte[]> prepareMessage(int ui) {
		ByteBuf buffer = Unpooled.buffer(512);
		
		buffer.writeByte(16); //login
		buffer.writeShort(0); // Size
		
		buffer.writeInt(91); // rev
		buffer.writeByte(0);
		buffer.writeByte(0);
		
		for (int i = 0; i < 4; i++)
			buffer.writeInt(0);
		
		buffer.writeZero(3); // auth code
		buffer.writeZero(5); // unused
		
		buffer.writeBytes("kfctastesgr8".getBytes()).writeByte(0);
		buffer.writeBytes(("bartflood" + ui).getBytes()).writeByte(0);
		
		buffer.writeZero(5); // Unused
		buffer.writeZero(24); // random.dat
		buffer.writeByte(0); // null-term string
		buffer.writeZero(4); // Unused
		buffer.writeZero(16); // UUID
		buffer.writeZero(18); // unused
		
		for (int i = 0; i < 4; i++) {
			buffer.writeByte(0); // 0-term str
			buffer.writeByte(0); // 0-term str
		}
		
		buffer.writeZero(3); // unused
		
		buffer.writeByte(0); // 0-term str
		buffer.writeByte(0); // 0-term str
		buffer.writeByte(0); // 0-term str
		buffer.writeByte(0); // 0-term str
		
		buffer.writeZero(2 + 4 * 3 + 4);
		for (int i : new int[]{-300048816, -368924009, 401132249, 880296741, -495863972, -4342373, 1746818960, -754609647, 345791584, 1014997305, -50026467, -903793899, 1362879558, 126773822, 728850419, -1192080095}) {
			buffer.writeInt(i);
		}
		
		buffer.writeZero(20); // hwid
		
		buffer.setShort(1, buffer.writerIndex() - 3);
		return new Tuple<>(buffer.writerIndex(), buffer.array());
	}
	
}
