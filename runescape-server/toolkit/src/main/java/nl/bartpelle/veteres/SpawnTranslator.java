package nl.bartpelle.veteres;

import com.google.gson.Gson;
import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.veteres.fs.DefinitionRepository;
import nl.bartpelle.veteres.fs.NpcDefinition;
import nl.bartpelle.veteres.util.NpcSpawn;

import java.io.File;
import java.io.FileReader;

/**
 * Created by Bart on 2/25/2016.
 */
public class SpawnTranslator {
	
	public static void main(String[] args) throws Exception {
		File in = new File(args[0]);
		DataStore ds = new DataStore("data/filestore", true);
		DefinitionRepository repo = new DefinitionRepository(ds, true);
		
		Gson gson = new Gson();
		NpcSpawn[] s = gson.fromJson(new FileReader(in), NpcSpawn[].class);
		for (NpcSpawn sp : s) {
			if (sp == null)
				continue;
			
			NpcDefinition def = repo.get(NpcDefinition.class, sp.id);
			if (def != null && def.models != null) {
				System.out.printf("%s;%d;%d;%d;%d;%d;%d%n", def.name, def.models[0], def.combatlevel, sp.x, sp.z, sp.level, sp.radius);
			}
		}
	}
	
}
