package nl.bartpelle.veteres.toolkit.interfacearchitect;

public enum WidgetType {
	
	SPRITE(5),
	LABEL(4);
	
	private int internalTypeId;
	
	WidgetType(int type) {
		internalTypeId = type;
	}
	
}
