package nl.bartpelle.veteres.toolkit.cachedef;

import nl.bartpelle.veteres.fs.Definition;
import nl.bartpelle.veteres.io.RSBuffer;

public class OverlayDefinition implements Definition {
	
	public int anInt2330;
	public int anInt2323;
	public int anInt2335;
	public int anInt2325 = -1;
	public int anInt2326 = -1;
	public boolean aBool2327 = true;
	public int anInt2328 = 0;
	public int anInt2332;
	public int anInt2329;
	public int anInt2337;
	
	public void decode(RSBuffer buffer) {
		while (true) {
			int code = buffer.readUByte();
			if (code == 0) {
				postDecode();
				return;
			}
			
			decode(buffer, code);
		}
	}
	
	void method2491(int var1, byte var2) {
		double var3 = (double) (var1 >> 16 & 255) / 256.0D;
		double var9 = (double) (var1 >> 8 & 255) / 256.0D;
		double var5 = (double) (var1 & 255) / 256.0D;
		double var7 = var3;
		if (var9 < var3) {
			var7 = var9;
		}
		
		if (var5 < var7) {
			var7 = var5;
		}
		
		double var11 = var3;
		if (var9 > var3) {
			var11 = var9;
		}
		
		if (var5 > var11) {
			var11 = var5;
		}
		
		double var17 = 0.0D;
		double var15 = 0.0D;
		double var13 = (var7 + var11) / 2.0D;
		if (var7 != var11) {
			if (var13 < 0.5D) {
				var15 = (var11 - var7) / (var11 + var7);
			}
			
			if (var13 >= 0.5D) {
				var15 = (var11 - var7) / (2.0D - var11 - var7);
			}
			
			if (var11 == var3) {
				var17 = (var9 - var5) / (var11 - var7);
			} else if (var11 == var9) {
				var17 = (var5 - var3) / (var11 - var7) + 2.0D;
			} else if (var11 == var5) {
				var17 = 4.0D + (var3 - var9) / (var11 - var7);
			}
		}
		
		var17 /= 6.0D;
		anInt2330 = (int) (256.0D * var17);
		anInt2323 = (int) (var15 * 256.0D);
		anInt2335 = (int) (var13 * 256.0D);
		if (anInt2323 < 0) {
			anInt2323 = 0;
		} else if (anInt2323 > 255) {
			anInt2323 = 255;
		}
		
		if (anInt2335 < 0) {
			anInt2335 = 0;
		} else if (anInt2335 > 255) {
			anInt2335 = 255;
		}
		
	}
	
	public void postDecode() {
		if (anInt2328 != -1) {
			method2491(anInt2328, (byte) 100);
			anInt2332 = anInt2330;
			anInt2329 = anInt2323;
			anInt2337 = anInt2335;
		}
		
		method2491(anInt2325, (byte) 61);
	}
	
	void decode(RSBuffer var1, int code) {
		if (code == 1) {
			anInt2325 = var1.readTriByte();
		} else if (code == 2) {
			anInt2326 = var1.readUByte();
		} else if (code == 5) {
			aBool2327 = false;
		} else if (code == 7) {
			anInt2328 = var1.readTriByte();
		} else if (code == 8) {
			;
		}
	}
	
}
