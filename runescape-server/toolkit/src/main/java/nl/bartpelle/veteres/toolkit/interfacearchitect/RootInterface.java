package nl.bartpelle.veteres.toolkit.interfacearchitect;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.TextAlignment;
import nl.bartpelle.veteres.toolkit.interfacearchitect.loader.WidgetDefinition;
import nl.bartpelle.veteres.toolkit.interfacearchitect.widgets.LabelWidget;
import nl.bartpelle.veteres.toolkit.interfacearchitect.widgets.SpriteWidget;

import java.util.LinkedList;
import java.util.List;

public class RootInterface {
	
	private final InterfaceArchitect app;
	
	private SimpleIntegerProperty width = new SimpleIntegerProperty();
	private SimpleIntegerProperty height = new SimpleIntegerProperty();
	
	private List<BaseWidget> widgets = new LinkedList<>();
	private boolean dirty;
	
	public RootInterface(InterfaceArchitect app, int width, int height) {
		this.app = app;
		this.width.set(width);
		this.height.set(height);
	}
	
	public IntegerProperty widthProperty() {
		return width;
	}
	
	public IntegerProperty heightProperty() {
		return height;
	}
	
	public List<BaseWidget> widgets() {
		return widgets;
	}
	
	public boolean dirty() {
		return dirty;
	}
	
	public void render(GraphicsContext g, long now) {
		for (BaseWidget w : widgets) {
			w.render(g, now, true);
		}
	}
	
	public void load(WidgetDefinition children[]) {
		int width = 0;
		int height = 0;
		widgets.clear();
		
		for (WidgetDefinition w : children) {
			int type = w.widgetType;
			
			BaseWidget widget = null;
			
			if (type == WidgetDefinition.TYPE_IMAGE) {
				SpriteWidget spriteWidget = new SpriteWidget(app, w.spriteId);
				widget = spriteWidget;
			} else if (type == WidgetDefinition.TYPE_LABEL) {
				TextAlignment align = w.textAlign == 1 ? TextAlignment.CENTER :
						w.textAlign == 0 ? TextAlignment.LEFT : TextAlignment.RIGHT;
				LabelWidget labelWidget = new LabelWidget(app, w.componentString, w.color, align);
				widget = labelWidget;
			} else {
				System.out.println("Unknown type: " + type);
			}
			
			// Set shared settings
			if (widget != null) {
				widget.bounds(w.x, w.y, w.width, w.height);
				System.out.println("Added! " + w.x + ", " + w.y + ", " + w.width + ", " + w.height);
				
				if (w.x + w.width > width) {
					width = w.x + w.width;
				}
				if (w.y + w.height > height) {
					height = w.y + w.height;
				}
				
				widgets.add(widget);
			}
		}
		
		this.width.set(width);
		this.height.set(height);
		dirty = true;
	}
	
}
