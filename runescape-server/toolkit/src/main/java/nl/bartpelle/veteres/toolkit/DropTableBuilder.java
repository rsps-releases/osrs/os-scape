package nl.bartpelle.veteres.toolkit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.veteres.fs.DefinitionRepository;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bart on 10/10/2015.
 */
public class DropTableBuilder extends Application {
	
	private static final String NUMBER_PATTERN = "\\d+";
	private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
	
	private static final Pattern PATTERN = Pattern.compile("(?<SEMICOLON>" + NUMBER_PATTERN + ")" + "|(?<STRING>" + STRING_PATTERN + ")");
	
	private TreeItem root;
	private CodeArea codeArea;
	private DataStore store;
	private DefinitionRepository repo;
	
	@Override
	public void start(Stage stage) throws Exception {
		System.out.println(DropTableBuilder.class.getResource("/droptable.fxml"));
		store = new DataStore("data/filestore", true);
		repo = new DefinitionRepository(store, true);
		
		Label label = new Label("Drop table for Guard");
		TreeView treeView = new TreeView(new TreeItem(label));
		root = (TreeItem) treeView.rootProperty().get();
		
		MenuItem add = new MenuItem("Add child");
		add.setOnAction(event -> {
			root.getChildren().add(makeTreeItem());
			root.setExpanded(true);
		});
		ContextMenu contextMenu = new ContextMenu(add);
		label.setOnMousePressed(e -> contextMenu.show(label, Side.BOTTOM, e.getX(), e.getY()));
		
		root.getChildren().addAll(makeTreeItem());
		
		// Make the editor
		codeArea = new CodeArea();
		codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));
		
		codeArea.richChanges().subscribe(change -> codeArea.setStyleSpans(0, computeHighlighting(codeArea.getText())));
		codeArea.replaceText(0, 0, new GsonBuilder().setPrettyPrinting().create().toJson(new Object[]{"hey", "hello", "numbers", new int[]{1, 2, 3}}));
		
		SplitPane splitPane = new SplitPane(treeView, codeArea);
		splitPane.setOrientation(Orientation.HORIZONTAL);
		
		MenuItem save = new MenuItem("Save");
		save.setOnAction(event -> buildJson());
		BorderPane borderPane = new BorderPane(splitPane, new MenuBar(new Menu("File", null, save)), null, null, null);
		
		Scene scene = new Scene(borderPane, 720, 420);
		scene.getStylesheets().add(this.getClass().getResource("/jsonsyntax.css").toExternalForm());
		
		stage.setTitle("Drop table editor");
		stage.setScene(scene);
		stage.show();
	}
	
	private void buildJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		DropTable table = new DropTable();
		
		recursiveJson(root, table);
		codeArea.replaceText(gson.toJson(table));
		recursiveCalcChances(root, 1, 1);
		
	}
	
	private void recursiveJson(TreeItem item, DropTable table) {
		item.getChildren().forEach(c -> {
			TreeItem ti = ((TreeItem) c);
			AnchorPane ap = (AnchorPane) ti.getValue();
			TextField itemid = (TextField) ap.lookup("#itemid");
			TextField minamt = (TextField) ap.lookup("#minamt");
			TextField maxamt = (TextField) ap.lookup("#maxamt");
			Spinner diesides = (Spinner) ap.lookup("#diesides");
			
			try {
				int itemId = Integer.parseInt(itemid.getText());
				int minAmt = Integer.parseInt(minamt.getText());
				int maxAmt = maxamt.getText().length() == 0 ? -1 : Integer.parseInt(maxamt.getText());
				
				Item newItem = new Item();
				if (minAmt == maxAmt || maxAmt == -1) {
					newItem.amount = minAmt;
				} else {
					newItem.min = minAmt;
					newItem.max = maxAmt;
				}
				
				newItem.id = itemId;
				newItem.points = (int) diesides.getValue();
				
				table.items.add(newItem);
			} catch (Exception e) {
				DropTable child = new DropTable();
				child.name = itemid.getText();
				child.points = (int) diesides.getValue();
				
				table.tables.add(child);
				
				recursiveJson(ti, child);
			}
		});
	}
	
	private void recursiveCalcChances(TreeItem item, int num, int denom) {
		// Compute total first
		final int[] total = {0};
		item.getChildren().forEach(c -> {
			TreeItem ti = ((TreeItem) c);
			AnchorPane ap = (AnchorPane) ti.getValue();
			Spinner diesides = (Spinner) ap.lookup("#diesides");
			int die = (int) diesides.getValue();
			total[0] += die;
		});
		
		// Apply rates
		int computed = denom * total[0];
		System.out.println("Total: " + computed);
		
		DecimalFormat format = new DecimalFormat("###.##%");
		item.getChildren().forEach(c -> {
			TreeItem ti = ((TreeItem) c);
			AnchorPane ap = (AnchorPane) ti.getValue();
			Spinner diesides = (Spinner) ap.lookup("#diesides");
			Label cplabel = (Label) ap.lookup("#computed");
			int die = (int) diesides.getValue();
			TextField id = (TextField) ap.lookup("#itemid");
			
			String name = "";
			try {
				int i = Integer.parseInt(id.getText());
				name = " (" + new nl.bartpelle.veteres.model.item.Item(i).unnote(repo).definition(repo).name + ")";
			} catch (Exception e) {
			}
			
			double chance = (double) (die * num) / (double) computed;
			double outOf = 1.0 / chance;
			
			cplabel.setText("/ " + total[0] + " (O " + (die * num) + "/" + computed + ", ~" + format.format(chance) + ", ~1/" + (int) outOf + ") " + name);
			
			recursiveCalcChances(ti, num * die, computed);
		});
	}
	
	private TreeItem makeTreeItem() {
		try {
			AnchorPane layout = FXMLLoader.load(DropTableBuilder.class.getResource("/droptable.fxml"));
			TreeItem item = new TreeItem<>(layout);
			
			MenuItem add = new MenuItem("Add child");
			add.setOnAction(event -> {
				item.getChildren().add(makeTreeItem());
				item.setExpanded(true);
			});
			
			MenuItem remove = new MenuItem("Remove");
			remove.setOnAction(event -> {
				item.getParent().getChildren().remove(item);
			});
			
			Spinner spinner = (Spinner) layout.lookup("#diesides");
			spinner.setOnScroll(e -> {
				e.consume();
				if (e.getDeltaY() > 0)
					spinner.increment();
				else if (e.getDeltaY() < 0)
					spinner.decrement();
			});
			spinner.valueProperty().addListener((observable, oldValue, newValue) -> {
				buildJson();
			});
			
			final ContextMenu contextMenu = new ContextMenu(add, remove);
			layout.setOnMousePressed(e -> contextMenu.show(layout, Side.BOTTOM, e.getX(), e.getY()));
			return item;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static StyleSpans<Collection<String>> computeHighlighting(String text) {
		Matcher matcher = PATTERN.matcher(text);
		int lastKwEnd = 0;
		StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
		while (matcher.find()) {
			String styleClass = matcher.group("SEMICOLON") != null ? "semicolon" : matcher.group("STRING") != null ? "string" : matcher.group("COMMENT") != null ? "comment" : null; /* never happens */
			assert styleClass != null;
			spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
			spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
			lastKwEnd = matcher.end();
		}
		spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
		return spansBuilder.create();
	}
	
	public static void main(String args[]) {
		launch(args);
	}
	
	static class DropTable {
		String name;
		int points;
		java.util.List<Item> items = new LinkedList<>();
		java.util.List<DropTable> tables = new LinkedList<>();
	}
	
	static class Item {
		Integer id;
		Integer min;
		Integer max;
		Integer amount;
		Integer points;
	}
}