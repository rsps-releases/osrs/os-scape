package nl.bartpelle.veteres.toolkit;

import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.dawnguard.util.Compression;

import java.io.File;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Bart on 2/14/2015.
 */
public class CS2Dasm {
	
	public static void main(String[] args) throws Exception {
		DataStore ds = new DataStore("data/filestore", true);
		
		Map<Integer, InstrInfo> infos = new HashMap<>();
		Scanner scanner = new Scanner(new File("toolkit/instrdefs.conf"));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] bits = line.split(" ");
			InstrInfo info = new InstrInfo();
			info.i = Integer.parseInt(bits[1]);
			info.s = Integer.parseInt(bits[2]);
			info.accurate = Boolean.parseBoolean(bits[3]);
			infos.put(Integer.parseInt(bits[0]), info);
		}
		
		//System.out.println(mnemonics);
		for (int i = 0; i < ds.getIndex(12).getLastArchiveId(); i++) {
			try {
				//CS2Script test = CS2Script.decode(i, Compression.decompressArchive(ds.getIndex(12).getArchive(i)));
				//Cs2ToJvm.translate(infos, test, ds);
			} catch (Exception e) {
			}
		}
		
		int id = 73;
		CS2Script test = CS2Script.decode(id, Compression.decompressArchive(ds.getIndex(12).getArchive(id)));
		//decompile(test);
		//Cs2ToJvm.translate(infos, test, ds);
		
		System.out.println("Num " + ds.getIndex(12).getLastArchiveId());
		new File("scriptdasm").mkdirs();
		int x = 364;
		for (int i = 1; i < ds.getIndex(12).getLastArchiveId(); i++) {
			if (i == 1495) continue;
			try {
				System.err.println(i);
				CS2Script scr = CS2Script.decode(i, Compression.decompressArchive(ds.getIndex(12).getArchive(i)));
				PrintStream writer = new PrintStream(new File("scriptdasm/" + i + ".txt"));
				scr.dasm(writer);
				//FlowGraph graph = new FlowGraph(scr);
				//Files.write(new File("scriptdump/" + i + ".java").toPath(), graph.decompile().getBytes());
			} catch (Exception e) {
				System.err.println("Can't do anything with " + i);
				e.printStackTrace();
			}
		}

//		CS2Script test = CS2Script.decode(15, Compression.decompressArchive(ds.getIndex(12).getArchive(15)));
//		Cs2ToJvm.translate(test);
	}
	
	private static void decompile(CS2Script script, Map<Integer, InstrInfo> infos) throws Exception {
		FlowGraph graph = new FlowGraph(script, infos);
		Files.write(new File("scriptdump/" + script.id + ".java").toPath(), graph.decompile().getBytes());
		graph.blocks().forEach((block) -> {
			System.out.println(block.toGraphString());
			;
		});
	}
	
}
