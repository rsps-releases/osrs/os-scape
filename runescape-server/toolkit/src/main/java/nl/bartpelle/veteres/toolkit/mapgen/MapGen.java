package nl.bartpelle.veteres.toolkit.mapgen;

import io.netty.buffer.Unpooled;
import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.veteres.fs.DefinitionRepository;
import nl.bartpelle.veteres.fs.MapDefinition;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.toolkit.cachedef.OverlayDefinition;
import nl.bartpelle.veteres.toolkit.cachedef.UnderlayDefinition;

import java.awt.image.BufferedImage;

/**
 * Created by Bart on 12/6/2015.
 */
public class MapGen {
	
	public static void main(String[] args) {
		DataStore ds = new DataStore("data/filestore", true);
		DefinitionRepository repo = new DefinitionRepository(ds, true);
		
		// Load underlays
		UnderlayDefinition[] underlays = new UnderlayDefinition[256];
		for (int i = 0; i < 256; i++) {
			try {
				UnderlayDefinition def = new UnderlayDefinition();
				def.decode(new RSBuffer(Unpooled.wrappedBuffer(ds.getFileDirectUncached(2, 1, i))));
				underlays[i] = def;
			} catch (Exception e) {
			}
		}
		
		// Load overlays
		OverlayDefinition[] overlays = new OverlayDefinition[256];
		for (int i = 0; i < 256; i++) {
			try {
				OverlayDefinition def = new OverlayDefinition();
				def.decode(new RSBuffer(Unpooled.wrappedBuffer(ds.getFileDirectUncached(2, 4, i))));
				overlays[i] = def;
				System.out.println(i);
			} catch (Exception e) {
			}
		}
		
		// Generate map
		
		for (int x = 0; x < 80; x++) {
			System.out.println(x);
			for (int y = 0; y < 190; y++) {
				try {
					int mapId = ds.getIndex(5).getDescriptor().getArchiveID("m" + x + "_" + y);
					MapDefinition def = new MapDefinition(x, y);
					def.load(repo, ds.getFileDirect(5, mapId, 0), new byte[100], true);
					//ImageIO.write(def.generateImage(underlays, overlays), "png", new File("mapout/"+((x<<8)|y)+".png"));
				} catch (Exception e) {
					//e.printStackTrace();
				}
			}
			System.gc();
		}
		
	}
	
	private static void paintSq(BufferedImage img, int x, int y, int col) {
		for (int dx = x * 4; dx < x * 4 + 4; dx++) {
			for (int dy = y * 4; dy < y * 4 + 4; dy++) {
				img.setRGB(dx, dy, col);
			}
		}
	}
	
}
