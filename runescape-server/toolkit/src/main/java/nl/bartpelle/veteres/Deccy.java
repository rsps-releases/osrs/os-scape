package nl.bartpelle.veteres;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Bart on 4/24/2017.
 */
public class Deccy {
	
	public static void main(String[] args) throws Exception {
		byte[] data = Files.readAllBytes(Paths.get("extracted.dat"));
		byte[] res = decrypt(data);
		Files.write(Paths.get("raw_extracted.txt"), res);
		System.out.println("Done.");
	}
	
	public static byte[] decrypt(byte[] plainText) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
		SecretKeySpec key = new SecretKeySpec("98M89NR*T&Neqn22".getBytes("UTF-8"), "AES");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec("a92j3t!@)(j4y8(*".getBytes("UTF-8")));
		return cipher.doFinal(plainText);
	}
	
}
