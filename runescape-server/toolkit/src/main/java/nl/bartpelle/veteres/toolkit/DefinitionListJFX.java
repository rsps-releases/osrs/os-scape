package nl.bartpelle.veteres.toolkit;

import com.typesafe.config.*;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.fs.NpcDefinition;
import nl.bartpelle.veteres.fs.ObjectDefinition;
import nl.bartpelle.veteres.model.item.ItemPriceRepository;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class DefinitionListJFX extends Application {
	
	private static final Logger logger = LogManager.getLogger(DefinitionListJFX.class);
	
	private static final long serialVersionUID = -1799567686109870502L;
	private JList<String> itemList = new JList<String>();
	private JList<String> npcList = new JList<String>();
	private static ItemDefinition[] items = new ItemDefinition[40000];
	private static NpcDefinition[] npcs = new NpcDefinition[40000];
	private static ObjectDefinition[] objects = new ObjectDefinition[40000];
	private static final String[] itemsx = new String[40000];
	private static final String[] npcsx = new String[30000];
	private static final String[] objectsx = new String[120000];
	private static ObservableList<String> itemListModel = FXCollections.observableArrayList(Arrays.asList(itemsx));
	private static ObservableList<String> npcListModel = FXCollections.observableArrayList(Arrays.asList(npcsx));
	private static ObservableList<String> objectListMOdel = FXCollections.observableArrayList(Arrays.asList(objectsx));
	private JTextField itemFilter = new JTextField();
	private JTextField npcFilter = new JTextField();
	private static DataStore ds;
	private static boolean first;
	private static int lastItem, lastNPC, lastObject;
	
	public static void main(String[] args) {
		logger.info("Loading cache...");
		
		Config config = ConfigFactory.systemProperties().withFallback(ConfigFactory.parseFileAnySyntax(new File("server.conf")));
		PgSqlService sqlService = null;
		ConfigList serviceDefinitions = config.getList("services");
		
		for (ConfigValue serv : serviceDefinitions) {
			Class<? extends Service> serviceClass = null;
			Config object = ((ConfigObject) serv).toConfig();
			
			try {
				serviceClass = Class.forName(object.getString("class")).asSubclass(Service.class);
			} catch (Exception e) {
				logger.error("Unable to cast '{}' to subtype of Service.", object.getString("class"));
			}
			
			if (serviceClass != null) {
				try {
					Service service = serviceClass.newInstance();
					if (service instanceof PgSqlService) {
						sqlService = (PgSqlService) service;
						service.setup(null, object);
						logger.info("Loaded quickstart service '{}'.", service.getClass().getSimpleName());
					}
				} catch (Exception e) {
					logger.error("Unable to instantiate '{}'.", object.getString("class"), e);
				}
			}
		}
		
		if (sqlService == null) {
			System.out.println("Could not not PGSqlService.");
			return;
		}
		
		ItemPriceRepository repo = new ItemPriceRepository(sqlService, null);
		sqlService.stop();
		
		DataStore ds = new DataStore("data/filestore");
		logger.info("Loading items...");
		StringBuilder b = new StringBuilder();
		b.append("INSERT INTO public.item_definitions (id, name, noted, cost, stackable, placeholder, bm_value, exchange, exchange_price)\n");
		b.append("VALUES ");
		for (int i = 0; i < 40000; i++) {
			try {
				byte[] data = ds.getIndex(2).getContainer(10).getFileData(i, true, true);
				ItemDefinition def = new ItemDefinition(i, data);
				if (data != null && data.length > 0) {
					items[i] = def;
					lastItem = i;
					String name = def.name;
					int realId = i;
					int realCost = def.cost;
					if (def.noted()) {
						ItemDefinition noteDef = new ItemDefinition(def.notelink, ds.getIndex(2).getContainer(10).getFileData(def.notelink, true, true));
						name = noteDef.name + " (noted)";
						realCost = noteDef.cost;
						realId = def.notelink;
					} else if ((def.pheld14401 > 0)) {
						ItemDefinition noteDef = new ItemDefinition(def.placeheld, ds.getIndex(2).getContainer(10).getFileData(def.placeheld, true, true));
						name = noteDef.name + " (placeholder)";
						realCost = noteDef.cost;
						realId = def.placeheld;
					}
					name = name.replace("'", "''");
					
					if (first) {
						b.append(",\n");
					}
					
					b.append("(" + i + ", '" + name + "', " + def.noted() + ", " + repo.getOrElse(realId, realCost, true) + ", " + def.stackable() + ", " + (def.pheld14401 > 0) + ", " + repo.bloodyMoney(realId) + ", " + def.grandexchange + ", " + repo.coins(realId) + ")");
					
					first = true;
				}
			} catch (Exception ignored) {
				ignored.printStackTrace();
			}
		}
		
		b.append("\nON CONFLICT (id) DO NOTHING;");
		
		
		logger.info("Loading npcs...");
		for (int i = 0; i < 40000; i++) {
			try {
				byte[] data = ds.getIndex(2).getContainer(9).getFileData(i, true, true);
				NpcDefinition def = new NpcDefinition(i, data);
				if (data != null && data.length > 0) {
					npcs[i] = def;
					lastNPC = i;
				}
			} catch (Exception ignored) {
			}
		}
		
		logger.info("Loading objects...");
		for (int i = 0; i < 40000; i++) {
			try {
				byte[] data = ds.getIndex(2).getContainer(6).getFileData(i, true, true);
				ObjectDefinition def = new ObjectDefinition(i, data);
				if (data != null && data.length > 0) {
					objects[i] = def;
					lastObject = i;
				}
			} catch (Exception ignored) {
			}
		}
		
		try {
			Files.write(Paths.get("sql_definitions.sql"), b.toString().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		ds.clearMemory();
		System.gc();
		
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		setUserAgentStylesheet(STYLESHEET_MODENA);
		primaryStage.setTitle("OSRS Item List");
		StackPane root = new StackPane();
		TabPane tabPane = new TabPane();
		
		// Create items tab
		Tab items = new Tab("Items");
		items.setClosable(false);
		tabPane.getTabs().add(items);
		
		// Create NPCs tab
		Tab npcs = new Tab("NPCs");
		npcs.setClosable(false);
		tabPane.getTabs().add(npcs);
		
		// Create items tab
		Tab objects = new Tab("Objects");
		objects.setClosable(false);
		tabPane.getTabs().add(objects);
		
		// Items pane contents
		BorderPane pane = new BorderPane();
		ListView<String> list = new ListView<>();
		list.setItems(itemListModel);
		list.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				// Copy id
				StringSelection sel = new StringSelection(list.getSelectionModel().getSelectedItem().split(": ")[0]);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel, null);
			}
		});
		pane.setCenter(list);
		TextField textField = new TextField();
		textField.textProperty().addListener((observable, oldValue, newValue) -> {
			itemListModel.clear();
			for (int i = 0; i <= lastItem; i++) {
				try {
					if (DefinitionListJFX.items[i] != null && DefinitionListJFX.items[i].name.toLowerCase().contains(newValue) || String.valueOf(i).contains(newValue)) {
						itemListModel.add(i + ": " + DefinitionListJFX.items[i].name);
					}
				} catch (Exception e) {
				}
			}
		});
		pane.setBottom(textField);
		items.setContent(pane);
		
		// NPC pane contents
		BorderPane npcPane = new BorderPane();
		ListView<String> npcLIst = new ListView<>();
		npcLIst.setItems(npcListModel);
		npcLIst.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				// Copy id
				StringSelection sel = new StringSelection(npcLIst.getSelectionModel().getSelectedItem().split(": ")[0]);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel, null);
			}
		});
		npcPane.setCenter(npcLIst);
		TextField npcTextField = new TextField();
		npcTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			npcListModel.clear();
			for (int i = 0; i <= lastNPC; i++) {
				try {
					if (DefinitionListJFX.npcs[i].name.toLowerCase().contains(newValue) || String.valueOf(i).contains(newValue)) {
						npcListModel.add(i + ": " + DefinitionListJFX.npcs[i].name);
					}
				} catch (Exception e) {
				}
			}
		});
		npcPane.setBottom(npcTextField);
		npcs.setContent(npcPane);
		
		// Objects pane contenst
		BorderPane objpane = new BorderPane();
		ListView<String> objlist = new ListView<>();
		objlist.setItems(objectListMOdel);
		objlist.setOnMouseClicked(event -> {
			if (event.getClickCount() == 2) {
				// Copy id
				StringSelection sel = new StringSelection(objlist.getSelectionModel().getSelectedItem().split(": ")[0]);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel, null);
			}
		});
		objpane.setCenter(objlist);
		TextField objfilter = new TextField();
		objfilter.textProperty().addListener((observable, oldValue, newValue) -> {
			objectListMOdel.clear();
			for (int i = 0; i <= lastObject; i++) {
				try {
					if (DefinitionListJFX.objects[i].name.toLowerCase().contains(newValue) || String.valueOf(i).contains(newValue)) {
						objectListMOdel.add(i + ": " + DefinitionListJFX.objects[i].name);
					}
				} catch (Exception e) {
				}
			}
		});
		objpane.setBottom(objfilter);
		objects.setContent(objpane);
		
		root.getChildren().add(tabPane);
		primaryStage.setScene(new Scene(root, 250, 500));
		primaryStage.show();
		
		loadItems();
		loadNPCs();
		loadObjects();
	}
	
	private void loadItems() {
		itemListModel.clear();
		for (int i = 0; i < items.length; i++) {
			try {
				if (items[i] != null && !items[i].name.isEmpty()) {
					itemListModel.add(i + ": " + items[i].name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void loadNPCs() {
		npcListModel.clear();
		for (int i = 0; i < npcs.length; i++) {
			try {
				if (npcs[i] != null && !npcs[i].name.isEmpty()) {
					npcListModel.add(i + ": " + npcs[i].name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void loadObjects() {
		objectListMOdel.clear();
		for (int i = 0; i < objects.length; i++) {
			try {
				if (objects[i] != null && !objects[i].name.isEmpty()) {
					objectListMOdel.add(i + ": " + objects[i].name);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
