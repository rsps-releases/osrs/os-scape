package nl.bartpelle.veteres.toolkit.interfacearchitect.widgets;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import nl.bartpelle.veteres.toolkit.interfacearchitect.BaseWidget;
import nl.bartpelle.veteres.toolkit.interfacearchitect.InterfaceArchitect;
import nl.bartpelle.veteres.toolkit.interfacearchitect.WidgetType;

public class LabelWidget extends BaseWidget {
	
	private String text;
	private Color color;
	private TextAlignment alignment;
	
	public LabelWidget(InterfaceArchitect app, String text, int color, TextAlignment alignment) {
		super(app);
		this.text = text;
		this.alignment = alignment;
		this.color = Color.web("#" + Integer.toHexString(color));
	}
	
	@Override
	public WidgetType getType() {
		return WidgetType.LABEL;
	}
	
	@Override
	public void render(GraphicsContext g, long now, boolean selected) {
		g.setFill(color);
		g.setTextAlign(alignment);
		g.setTextBaseline(VPos.TOP);
		g.fillText(text, x + (alignment == TextAlignment.CENTER ? (width/2) : 0), y, width);
		
		super.render(g, now, selected);
	}
	
}
