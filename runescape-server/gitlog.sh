git log --no-merges --pretty=format:"%cn, %cd: %s" \
| sed 's/Malia Hale/\[member=Carl\]/g' \
| sed 's/Bart Pelle/Bart/g' \
| sed 's/#\([0-9]\{7\}\)/\1/g' \
| sed 's/\([0-9]\{7\}\)/[url=https:\/\/www.catanai.com\/bugs\/view.php?id=\1]#\1[\/url]/g' \
| sed 's/bitbucket.org:Velocity_\/catanai/repository/g' \
| sed 's/https:\/\/bitbucket.org\/Velocity_\/catanai/repository/g' \
| sed 's/$/\r/' \
> gitlog.txt