package com.osscape.shadowall;

import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.RS2Widget;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Created by Bart on 11/19/2015.
 // Mossy rock (24838)
 24838 - 1406 5732 0 s 0
 */
@ScriptManifest(name = "Shadowall", author = "Bart Pelle @ OS-Scape", info = "Internal script. cos yolo lol", version = 1.0, logo = "")
public class Shadowall extends Script {
	
	private int lastAnim = -1;
	private Map<Integer, BoundCapture> captures = new HashMap<>();
	private Map<Integer, AnimCapture> anims = new HashMap<>();
	
	@Override
	public void onStart() throws InterruptedException {
		for (RS2Widget rs2Widget : widgets.getWidgets(548)) {
			log(rs2Widget.getRootId() + ", " + rs2Widget.getSecondLevelId() + ", " + rs2Widget.getThirdLevelId());
		}
	}
	
	@Override
	public void onExit() throws InterruptedException {
	}
	
	@Override
	public int onLoop() throws InterruptedException {
		if (myPlayer().getAnimation() != lastAnim) {
			lastAnim = myPlayer().getAnimation();
			log("Player animation: " + lastAnim + ", " + myPlayer().getAnimationDelay());
		}
		
		npcs.getAll().stream().filter(npc -> npc != null).forEach(npc -> {
			BoundCapture capt = captures.computeIfAbsent(npc.getIndex(), integer -> new BoundCapture(npc));
			capt.update(npc.getX(), npc.getY());
			
			// Capture animations :)
			if (npc.isAnimating()) {
				anims.computeIfAbsent(npc.getId(), integer -> new AnimCapture(npc.getName()));
				anims.get(npc.getId()).anims.add(npc.getAnimation());
				anims.get(npc.getId()).anims.add(npc.getAnimation2());
			}
		});
		
		try {
			FileWriter fw = new FileWriter("C:\\Users\\Bart\\OSBot\\Data\\npcdump.json");
			fw.append("[\n");
			captures.forEach((k, c) -> {
				try {
					fw.append(c.toJsonEntry());
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			fw.append("]");
			fw.flush();
			fw.close();
		} catch (Exception e) {
			log("Fuck " + e);
			e.printStackTrace();
		}
		
		try {
			FileWriter fw = new FileWriter("C:\\Users\\Bart\\OSBot\\Data\\npcanims.json");
			fw.append("[\n");
			anims.forEach((k, c) -> {
				try {
					fw.append(String.format("%s (%d): %s%n", c.name, k, c.anims));
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			fw.append("]");
			fw.flush();
			fw.close();
		} catch (Exception e) {
			log("Fuck " + e);
			e.printStackTrace();
		}
		
		return 500;
	}
	
	@Override
	public void onPaint(Graphics2D g) {
		g.setFont(g.getFont().deriveFont(12f));
		try {
			for (RS2Object o : objects.getAll()) {
				if (o != null && o.getName() != null && !o.getName().isEmpty() && !o.getName().equalsIgnoreCase("null")) {
					Polygon poly = o.getPosition().getPolygon(bot);
					if (poly.contains(mouse.getPosition().x, mouse.getPosition().y)) {
						g.drawString(o.getName() + " (" + o.getId() + ") x: " + o.getX() + ", y: " + o.getY() + ", rot: " + o.getOrientation() + ", type: " + o.getType(), (int) poly.getBounds().getCenterX(), (int) poly.getBounds().getCenterY() + 10);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			Field declaredField = Class.forName("fp").getDeclaredField("bc");
			log(declaredField);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		// Hovering any animated inter?
		for (RS2Widget rs2Widget : widgets.getAll()) {
			if (rs2Widget != null) {
				//rs2Widget.getContentType()
			}
		}
		
	}
	
	@Override
	public void onPlayAudio(int var) {
		super.onPlayAudio(var);
		System.out.println("audio? " + var);
	}
	
	
	@Override
	public void onConfig(int id, int v) throws InterruptedException {
		//log("Varp[" + id + "]: " + v);
	}
	
	static class AnimCapture {
		public Set<Integer> anims = new HashSet<>();
		public String name;
		
		public AnimCapture(String name) {
			this.name = name;
		}
	}
	
	static class BoundCapture {
		
		private int npcId;
		private String name;
		private int minX;
		private int minZ;
		private int maxX;
		private int maxZ;
		private int centerX;
		private int centerZ;
		private int radius;
		private int level;
		
		public BoundCapture(NPC npc) {
			this.npcId = npc.getId();
			this.name = npc.getName();
			this.level = npc.getZ();
		}
		
		public void update(int x, int z) {
			// No zeroes ty
			if (maxX == 0) maxX = x;
			if (minX == 0) minX = x;
			if (maxZ == 0) maxZ = z;
			if (minZ == 0) minZ = z;
			
			minX = Math.min(x, minX);
			minZ = Math.min(z, minZ);
			maxX = Math.max(x, maxX);
			maxZ = Math.max(z, maxZ);
			
			int wx = maxX - minX; // width X
			int wz = maxZ - minZ; // width Z
			int hx = wx / 2; // half across X
			int hz = wz / 2; // half across Z
			
			centerX = minX + hx;
			centerZ = minZ + hz;
			radius = Math.max(hx, hz);
		}
		
		public String toJsonEntry() {
			if (level == 0) {
				return "  {\"id\": " + npcId + ", \"x\": " + centerX + ", \"z\": " + centerZ + ", \"radius\": " + radius + "}, // " + name + "\n";
			} else {
				return "  {\"id\": " + npcId + ", \"x\": " + centerX + ", \"z\": " + centerZ + ", \"radius\": " + radius + ", \"level\": " + level + "}, // " + name + "\n";
			}
		}
	}
	
}
