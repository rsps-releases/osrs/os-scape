package nl.bartpelle.skript;

/**
 * Created by Bart on 1/7/2017.
 */
public interface Condition {
	
	public boolean check(Script s);
	
}
