package nl.bartpelle.skript;

import co.paralleluniverse.fibers.Fiber;
import co.paralleluniverse.fibers.SuspendExecution;
import co.paralleluniverse.fibers.Suspendable;
import kotlin.Pair;
import kotlin.jvm.functions.Function1;
import nl.bartpelle.veteres.content.npcs.NpcFacingPlayerPolicy;
import nl.bartpelle.veteres.fs.NpcDefinition;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.*;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Bart on 1/6/2017.
 */
public class Script {
	
	private boolean started;
	private boolean interrupted;
	private Fiber fiber;
	private Object context;
	private WaitReason waitReason;
	private Object waitParam;
	private int lastTick;
	private Object waitReturnVal;
	public Function1<Script, ?> interruptCall;
	private Function1<Script, ?> executedFunction;
	
	public final boolean hasStarted() {
		return this.started;
	}
	
	public final void setStarted(boolean x) {
		this.started = x;
	}
	
	public final boolean interrupted() {
		return this.interrupted;
	}
	
	public final void interrupt() {
		if (interruptCall != null) {
			interruptCall.invoke(this);
		}
		interrupted = true;
	}
	
	
	public final <T> T ctx() {
		return (T) this.context;
	}
	
	public final void onInterrupt(Function1<Script, ?> func) {
		this.interruptCall = func;
	}
	
	public int hashCode() {
		return super.hashCode();
	}
	
	public final Fiber getFiber() {
		return this.fiber;
	}
	
	public final void setFiber(Fiber x) {
		this.fiber = x;
	}
	
	public final Object getContext() {
		return this.context;
	}
	
	public final void setContext(Object x) {
		this.context = x;
	}
	
	public final WaitReason getWaitReason() {
		return this.waitReason;
	}
	
	public final void setWaitReason(WaitReason x) {
		this.waitReason = x;
	}
	
	public final <T> T getWaitParam() {
		return (T) this.waitParam;
	}
	
	public final void setWaitParam(Object x) {
		this.waitParam = x;
	}
	
	public final int getLastTick() {
		return this.lastTick;
	}
	
	public final void setLastTick(int x) {
		this.lastTick = x;
	}
	
	public final Object getWaitReturnVal() {
		return this.waitReturnVal;
	}
	
	public final void setWaitReturnVal(Object x) {
		this.waitReturnVal = x;
	}
	
	public final void setInterruptCall(Function1 x) {
		this.interruptCall = x;
	}
	
	public Script(Fiber fiber, Object context, WaitReason waitReason, Object waitParam, int lastTick, Object waitReturnVal, Function1 interruptCall, Function1<Script, ?> executedFunction) {
		super();
		this.fiber = fiber;
		this.context = context;
		this.waitReason = waitReason;
		this.waitParam = waitParam;
		this.lastTick = lastTick;
		this.waitReturnVal = waitReturnVal;
		this.executedFunction = executedFunction;
		this.interruptCall = interruptCall;
	}
	
	public final Fiber component1() {
		return this.fiber;
	}
	
	public final Object component2() {
		return this.context;
	}
	
	public final WaitReason component3() {
		return this.waitReason;
	}
	
	public final Object component4() {
		return this.waitParam;
	}
	
	public final int component5() {
		return this.lastTick;
	}
	
	public final Object component6() {
		return this.waitReturnVal;
	}
	
	public final Function1 component7() {
		return this.interruptCall;
	}
	
	public void setExecutedFunction(Function1<Script, ?> fn) {
		executedFunction = fn;
	}
	
	public Function1<Script, ?> getExecutedFunction() {
		return executedFunction;
	}
	
	// Server-specific functions
	public void message(String msg) {
		((Player) context).message(msg);
	}
	
	public void message(String msg, Object... args) {
		((Player) context).message(msg, args);
	}
	
	public Entity entity() {
		return (Entity) context;
	}
	
	public Player player() {
		return (Player) context;
	}
	
	public Npc npc() {
		return (Npc) context;
	}
	
	public Entity target() {
		WeakReference<Entity> holder = entity().attrib(AttributeKey.TARGET);
		
		if (holder != null)
			return holder.get();
		
		return null;
	}
	
	public Npc targetNpc() {
		return (Npc) entity().<WeakReference<Entity>>attrib(AttributeKey.TARGET).get();
	}
	
	private static final Function1<Script, ?> dialogueInterruptCall = (Script s) -> {
		s.player().interfaces().close(162, 550);
		s.player().invokeScript(101);
		return null;
	};
	
	private static final Function1<Script, ?> inputInterruptCall = (Script s) -> {
		s.player().write(new CloseInputDialogue());
		s.player().invokeScript(101);
		return null;
	};
	
	@Suspendable
	public void messagebox(String message) throws SuspendExecution {
		Player player = (Player) context;
		player.interfaces().send(229, 162, 550, false);
		player.write(new InterfaceText(229, 0, message));
		player.write(new  InterfaceText(229, 1, "Click here to continue"));
		player.write(new InvokeScript(600, 1, 1, 31, 15007744));
		player.write(new InterfaceSettings(229, 1, -1, -1, 1));
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();

		player.interfaces().close(162, 550);
	}
	
	@Suspendable
	public int inputInteger(String msg) throws SuspendExecution {
		player().invokeScript(108, msg);
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = inputInterruptCall;
		Fiber.park();
		
		return (waitReturnVal == null || !(waitReturnVal instanceof Integer)) ? 0 : (int) waitReturnVal;
	}
	
	@Suspendable
	public String inputString(String msg) throws SuspendExecution {
		player().invokeScript(110, msg);
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = inputInterruptCall;
		Fiber.park();
		
		return (waitReturnVal == null || !(waitReturnVal instanceof String)) ? "" : (String) waitReturnVal;
	}
	
	@Suspendable
	public Pair<Integer, Integer> displayDialogueInterface(int id, int target, int child, boolean walk) throws SuspendExecution {
		player().interfaces().send(id, target, child, walk);
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
		int ret = (int) waitReturnVal;
		return new Pair<>(ret >> 16, ret & 0xFFFF);
	}
	
	@Suspendable
	public void chatPlayer(String msg) throws SuspendExecution {
		chatPlayer(msg, 588);
	}
	
	@Suspendable
	public void chatPlayer(String msg, int anim) throws SuspendExecution {
		player().interfaces().send(217, 162, 550, false);
		player().write(new InterfaceText(217, 1, player().name()));
		player().write(new InterfaceText(217, 2, "Click here to continue"));
		player().write(new InterfaceText(217, 3, msg));
		player().write(new PlayerOnInterface(217, 0));
		player().write(new AnimateInterface(217, 0, anim));
		player().write(new InvokeScript(600, 1, 1, 16, 14221315));
		player().write(new InterfaceSettings(217, 2, -1, -1, 1));
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
	}
	
	
	// Cut out need to specify npc id.. assume you're talking to one.
// Wouldn't work maybe during a cutscene when you face different things or your Target attribute is cancelled.
	
	@Suspendable
	public void chatNpc2(String msg) throws SuspendExecution {
		chatNpc2(msg, 588);
	}
	
	@Suspendable
	public void chatNpc2(String msg, int anim) throws SuspendExecution {
		if (context instanceof Player) {
			if (targetNpc() != null) {
				chatNpc(msg, targetNpc().id(), anim);
			}
		}
	}
	
	@Suspendable
	public void chatNpc(String msg, int npc) throws SuspendExecution {
		chatNpc(msg, npc, 588);
	}
	
	@Suspendable
	public void chatNpc(String msg, int npc, int anim) throws SuspendExecution {
		NpcDefinition def = player().world().definitions().get(NpcDefinition.class, npc);
		
		player().interfaces().send(231, 162, 550, false);
		player().write(new InterfaceText(231, 1, def.name));
		player().write(new InterfaceText(231, 2, "Click here to continue"));
		player().write(new InterfaceText(231, 3, msg));
		player().write(new NpcOnInterface(231, 0, npc));
		player().write(new AnimateInterface(231, 0, anim));
		player().write(new InvokeScript(600, 1, 1, 16, 15138819));
		player().write(new InterfaceSettings(231, 2, -1, -1, 1));
		
		// The default Npc facing player policy. Npcs only face the player when their chat option appears.
		// Not when you interact with them.
		
		Entity target = target();
		if (target != null && target.isNpc()) {
			target.face(player());

			// Npcs after ~8 seconds will reset their face direction if still facing the player.
			// If a player runs off and we're still lookin' at them it's a bit freakyy.
			target.world().server().scriptExecutor().executeScript(target, new NpcFacingPlayerPolicy(player()));
		}

		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
	}
	
	@Suspendable
	public int options(String... options) throws SuspendExecution {
		return optionsTitled("Select an Option", options);
	}
	
	@Suspendable
	public int optionsTitled(String title, String... options) throws SuspendExecution {
		player().interfaces().send(219, 162, 550, false);
		player().write(new InvokeScript(58, title, Arrays.stream(options).collect(Collectors.joining("|"))));
		player().write(new InterfaceSettings(219, 0, 1, 5, 1));
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
		// TODO investigate how spamming numbers or spacebar on keyboard can make this return null. Trips over itself/interrupts?
		return (waitReturnVal == null || !(waitReturnVal instanceof Integer)) ? -1 : (int) waitReturnVal;
	}
	
	// Copy of options but vararg options can be null, and those null are filtered out. Allow for variable paramater/ease of use in code
	@Suspendable
	public int optionsNullable(String title, String... options) throws SuspendExecution {
		player().interfaces().send(219, 162, 550, false);
		player().write(new InvokeScript(58, title, Arrays.stream(options).filter(s -> s != null && !s.isEmpty()).collect(Collectors.joining("|"))));
		player().write(new InterfaceSettings(219, 0, 1, 5, 1));
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
		// TODO investigate how spamming numbers or spacebar on keyboard can make this return null. Trips over itself/interrupts?
		return (waitReturnVal == null || !(waitReturnVal instanceof Integer)) ? -1 : (int) waitReturnVal;
	}
	
	@Suspendable
	public String optionsNullableS(String title, String... options) throws SuspendExecution {
		int result = optionsNullable(title, options) - 1;
		Object[] proper = Arrays.stream(options).filter(s -> s != null && !s.isEmpty()).toArray();
		return (result >= 0 && result < proper.length) ? (String) proper[result] : "";
	}
	
	@Suspendable
	public String optionsS(String title, String... options) throws SuspendExecution {
		int result = optionsTitled(title, options) - 1;
		return (result >= 0 && result < options.length) ? options[result] : "";
	}
	
	@Suspendable
	public int customOptions(String title, String... options) throws SuspendExecution {
		player().interfaces().send(219, 162, 550, false);
		player().write(new InvokeScript(58, title, Arrays.stream(options).collect(Collectors.joining("|"))));
		player().write(new InterfaceSettings(219, 0, 1, 5, 1));
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
		return (waitReturnVal == null || !(waitReturnVal instanceof Integer)) ? -1 : (int) waitReturnVal;
	}
	
	/**
	 * Shows text with no player head/npc head etc and just click to continue.
	 */
	@Suspendable
	public void messagebox2(String message) throws SuspendExecution {
		player().interfaces().send(229, 162, 550, false);
		player().write(new InterfaceText(229, 0, message));
		player().write(new InterfaceText(229, 1, "Click here to continue"));
		player().write(new InvokeScript(600, 1, 1, 17, 15007744));
		player().write(new InterfaceSettings(229, 1, -1, -1, 1));
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
	}
	
	@Suspendable
	public void messagebox3(String message) throws SuspendExecution {
		player().interfaces().send(229, 162, 550, false);
		player().write(new InterfaceText(229, 0, message));
		player().write(new InterfaceText(229, 1, "Click here to continue"));
		player().write(new InvokeScript(600, 1, 1, 24, 15007744));
		player().write(new InterfaceSettings(229, 1, -1, -1, 1));
		
		waitReason = WaitReason.DIALOGUE;
		interruptCall = dialogueInterruptCall;
		Fiber.park();
		
		player().interfaces().close(162, 550);
	}
	
	public String toString() {
		return "Script(fiber=" + this.fiber + ", context=" + this.context + ", waitReason=" + this.waitReason + ", waitParam=" + this.waitParam + ", lastTick=" + this.lastTick + ", waitReturnVal=" + this.waitReturnVal + ", interruptCall=" + this.interruptCall + ")";
	}
	
	@Suspendable
	public final void delay(int time) throws SuspendExecution {
		waitReason = WaitReason.DELAYED;
		waitParam = time;
		Fiber.park();
	}
	
}
