package nl.bartpelle.skript;

import co.paralleluniverse.fibers.Fiber;
import co.paralleluniverse.fibers.Suspendable;

/**
 * Created by Bart on 1/7/2017.
 */
public class FunctionFiber extends Fiber<Object> {
	
	private Script script;
	
	public FunctionFiber(Script script) {
		this.script = script;
	}
	
	@Suspendable
	public final Object run() {
		script.getExecutedFunction().invoke(script);
		return null;
	}
	
}
