package nl.bartpelle.skript;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Bart on 1/6/2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ScriptMain {
}
