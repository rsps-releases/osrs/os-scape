package nl.bartpelle.skript;

/**
 * Created by Bart on 1/6/2017.
 */
public enum WaitReason {
	NONE,
	DIALOGUE,
	DELAYED;
}