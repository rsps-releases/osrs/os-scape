package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart on 8/11/2015.
 */
public class AddPrivateMessage extends Command {
	
	private String sender;
	private String text;
	
	public AddPrivateMessage(String sender, String text) {
		this.sender = sender;
		this.text = text.replace('\0', ' ');
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(text.length())).packet(42).writeSize(RSBuffer.SizeType.SHORT);
		
		buffer.writeString(sender);
		
		byte[] huffmandata = new byte[512];
		int len = player.world().server().huffman().encode(text, huffmandata);
		
		buffer.writeCompact(text.length());
		buffer.get().writeBytes(huffmandata, 0, len);
		
		return buffer;
	}
	
}
