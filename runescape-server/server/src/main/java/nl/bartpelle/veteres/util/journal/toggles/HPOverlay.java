package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.Varbit;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class HPOverlay extends JournalEntry {

    @Override
    public void send(Player player) {
        boolean showOverlay = player.varps().varbit(Varbit.HP_OVERLAY_TOGGLED) == 1;
        if(showOverlay)
            send(player, "<img=58> HP Overlay", "On", Color.GREEN);
        else
            send(player, "<img=58> HP Overlay", "Off", Color.RED);
    }

    @Override
    public void select(Player player) {
        boolean showOverlay = player.varps().varbit(Varbit.HP_OVERLAY_TOGGLED) == 1;
        player.varps().varbit(Varbit.HP_OVERLAY_TOGGLED, (showOverlay ? 0 : 1));
        send(player);
    }

}