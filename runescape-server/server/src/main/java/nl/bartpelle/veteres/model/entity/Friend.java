package nl.bartpelle.veteres.model.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by Bart on 11/15/2015.
 */
public class Friend {
	
	public int id;
	public String name;
	public String prevname;
	public int world;
	public int clanrank;
	public int rights;
	
	public Friend(int id, String name, String prevname, int world, int clanrank, int rights) {
		this.id = id;
		this.name = name;
		this.prevname = prevname;
		this.world = world;
		this.clanrank = clanrank;
		this.rights = rights;
	}
	
	public int id() {
		return id;
	}
	
	public String name() {
		return name;
	}
	
	public String prevname() {
		return prevname;
	}
	
	public int world() {
		return world;
	}
	
	public boolean online() {
		return world > 0;
	}
	
	public int rights() {
		return rights;
	}
	
	public int clanrank() {
		return clanrank;
	}
	
	@Override
	public int hashCode() {
		return id; // Unique enough imo
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.DEFAULT_STYLE, false, false);
	}
	
}
