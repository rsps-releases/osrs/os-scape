package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.GroundItemAction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 8/23/2015.
 * <p>
 * The pick-up option on ground items.
 */
@PacketInfo(size = 7)
public class TakeGroundItem implements Action {
	
	private int x;
	private int z;
	private int id;
	private boolean run;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		x = buf.readUShort();
		id = buf.readUShortA();
		z = buf.readULEShort();
		run = buf.readByteN() == 1;
		
		log(player, opcode, size, "id=%d x=%d z=%d", id, x, z);
	}
	
	@Override
	public void process(Player player) {
		GroundItem item = player.world().getGroundItem(player, x, z, player.tile().level, id);
		if (player.attribOr(AttributeKey.DEBUG, false))
			player.debug("Gitem op3 - id: %d at %d,%d,%d", x, z, id, player.tile().level);
		// Even if the item is null (been picked up) we would run to the location anyway.
		if (!player.locked() && !player.dead()) {
			player.stopActions(true);
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, item);
			player.putattrib(AttributeKey.INTERACTION_OPTION, 3);
			player.world().server().scriptExecutor().executeLater(player, GroundItemAction.script);
		}
	}
	
}
