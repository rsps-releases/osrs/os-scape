package nl.bartpelle.veteres.model.entity.player.content.exchange;

import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;

import java.sql.Timestamp;

/**
 * Created by Bart on 10/28/2016.
 */
public final class ExchangeOffer {
	
	private long id;
	private final int owner;
	private final int slot;
	private final ExchangeType type;
	private final int item;
	private final int requested;
	private final int price;
	private Timestamp created;
	private Timestamp updated;
	private boolean cancelled;
	private boolean processed;
	private int coins;
	private int spent;
	private int items;
	private int completed;
	
	private boolean dirty;
	private boolean changed;
	
	private ItemContainer collectItems;
	private final World world;
	
	public ExchangeOffer(int owner, int slot, ExchangeType type, int pricePerItem, int itemId, int itemAmount, int amountCompleted,
	                     boolean cancelled, int coins, int spent, int items, World world, boolean processed, Timestamp created, Timestamp updated) {
		this(-1, owner, slot, type, pricePerItem, itemId, itemAmount, amountCompleted, cancelled, coins, spent, items, world, processed, created, updated);
	}
	
	public ExchangeOffer(long id, int owner, int slot, ExchangeType type, int pricePerItem, int itemId, int itemAmount, int amountCompleted,
	                     boolean cancelled, int coins, int spent, int items, World world, boolean processed, Timestamp created, Timestamp updated) {
		this.id = id;
		this.owner = owner;
		this.slot = slot;
		this.type = type;
		this.item = itemId;
		this.requested = itemAmount;
		this.completed = amountCompleted;
		this.cancelled = cancelled;
		this.coins = coins;
		this.spent = spent;
		this.items = items;
		this.price = pricePerItem;
		this.world = world;
		this.processed = processed;
		this.created = created;
		this.updated = updated;
		
		makeDirty();
	}
	
	public int owner() {
		return owner;
	}
	
	public int slot() {
		return slot;
	}
	
	public ExchangeType type() {
		return type;
	}
	
	public int item() {
		return item;
	}
	
	public int requested() {
		return requested;
	}
	
	public int completed() {
		return completed;
	}
	
	public void completed(int completed) {
		if (this.completed != completed) {
			makeDirty();
			changed = true;
		}
		
		this.completed = completed;
	}
	
	public int remaining() {
		return requested - completed;
	}
	
	public boolean cancelled() {
		return cancelled;
	}
	
	public boolean processed() {
		return processed;
	}
	
	public void processed(boolean processed) {
		this.processed = processed;
	}
	
	public int coins() {
		return coins;
	}
	
	public void coins(int coins) {
		if (this.coins != coins) {
			changed = true;
		}
		this.coins = coins;
	}
	
	public int spent() {
		return spent;
	}
	
	public void spent(int spent) {
		this.spent = spent;
	}
	
	public int items() {
		return items;
	}
	
	public void items(int items) {
		if (this.items != items) {
			changed = true;
		}
		this.items = items;
	}
	
	public int pricePer() {
		return price;
	}
	
	public void cancel() {
		if (requested == completed || cancelled)
			return;
		
		this.cancelled = true;
		
		collectItems = new ItemContainer(world, 2, ItemContainer.Type.FULL_STACKING);
		
		int remaining = type() == ExchangeType.SELL ? remaining() + items : items;
		if (remaining > 0)
			collectItems.add(new Item(item(), remaining), true);
		items = remaining;
		
		remaining = type() == ExchangeType.BUY ? (price * remaining()) + coins : coins;
		if (remaining > 0)
			collectItems.add(new Item(world.currency(), remaining), true);
		coins = remaining;
		
		changed = true;
	}
	
	public ItemContainer collectItems() {
		if (isDirty()) {
			collectItems = new ItemContainer(world, 2, ItemContainer.Type.FULL_STACKING);
			collectItems.set(0, items > 0 ? new Item(item(), items) : null);
			
			//Coins can take the first slot in selling
			collectItems.set(type == ExchangeType.SELL && items <= 0 ? 0 : 1, coins > 0 ? new Item(world.currency(), coins) : null);
			
			clean();
		}
		return collectItems;
	}
	
	public long id() {
		return id;
	}
	
	public void id(long id) {
		if (this.id != id) {
			changed = true;
		}
		this.id = id;
	}
	
	public int computeType() {
		if (type == ExchangeType.NULL) {
			return 0;
		}
		
		int base = type == ExchangeType.SELL ? 8 : 0;
		
		// Both have the same state :)
		if (cancelled || completed == requested) {
			return base | 5;
		}
		
		return base | 1;
	}
	
	public boolean isDirty() {
		return dirty;
	}
	
	public void makeDirty() {
		this.dirty = true;
	}
	
	public void clean() {
		this.dirty = false;
	}
	
	public boolean finished() {
		return completed == requested;
	}
	
	public boolean changed() {
		return changed;
	}
	
	public void changed(boolean changed) {
		this.changed = changed;
	}
	
	public Timestamp getCreated() {
		return created;
	}
	
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
	public Timestamp getUpdated() {
		return updated;
	}
	
	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}
	
	@Override
	public String toString() {
		return "ExchangeOffer{" +
				"id=" + id +
				", owner=" + owner +
				", slot=" + slot +
				", type=" + type +
				", item=" + item +
				", requested=" + requested +
				", completed=" + completed +
				", cancelled=" + cancelled +
				", coins=" + coins +
				", items=" + items +
				", price=" + price +
				", collectItems=" + collectItems +
				", world=" + world +
				", dirty=" + dirty +
				'}';
	}
}
