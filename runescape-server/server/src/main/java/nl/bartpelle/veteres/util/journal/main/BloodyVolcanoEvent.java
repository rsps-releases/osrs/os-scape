package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.events.BloodyVolcano;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class BloodyVolcanoEvent {

    public static final class Entry extends JournalEntry {

        public static final Entry INSTANCE = new Entry();

        @Override
        public void send(Player player) {
            int remaining = BloodyVolcano.INSTANCE.getWildernessKillsRequired();
            if(remaining > 0)
                send(player, "<img=58> Bloody Volcano", remaining + " kills", Color.GREEN);
            else
                send(player, "<img=58> Bloody Volcano", "Active", Color.GREEN);
        }

        @Override
        public void select(Player player) {
            int killsRemaining = BloodyVolcano.INSTANCE.getWildernessKillsRequired();
            if(killsRemaining > 0) {
                player.message("<col=6a1a18><img=15> There hasn't been enough bloodshed. " + killsRemaining + " more "
                        + (killsRemaining == 1 ? "player" : "players") + " need to die for the Volcano to become active.");
            } else {
                String despawnTimer = player.world().timers().asMinutesAndSecondsLeft(TimerKey.ACTIVE_VOLCANO_DESPAWN);
                player.message("<col=6a1a18><img=15> The Volcano in level 53 wilderness has become active due to the amount of bloodshed! Quickly help subdue it before it erupts in " + despawnTimer + "!");
            }
        }

    }
}
