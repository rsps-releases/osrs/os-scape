package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.services.intercom.ClanChatService;
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer;
import nl.bartpelle.veteres.services.sql.SqlTransaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Optional;

/**
 * Created by Bart on 12/11/2015.
 */
@PacketInfo(size = -1)
public class ChangeClanRank implements Action {
	
	private int rank;
	private String name;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		rank = Math.min(7, Math.max(0, (buf.readByteA())));
		name = buf.readString();
	}
	
	@Override
	public void process(Player player) {
		for (Friend friend : player.social().friends()) {
			if (friend.name.equalsIgnoreCase(name)) {
				player.message("Changes will take effect on your clan in the next 60 seconds.");
				
				player.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
					s.transact(new SqlTransaction() {
						@Override
						public void execute(Connection connection) throws Exception {
							PreparedStatement stmt = connection.prepareStatement("UPDATE friends SET clanrank=? WHERE account_id=? AND friend_id=?;");
							stmt.setInt(1, rank);
							stmt.setInt(2, (Integer) player.id());
							stmt.setInt(3, friend.id);
							stmt.executeUpdate();
							connection.commit();
							
							ClanChat chat = player.world().chats().get((Integer) player.id());
							Optional<ClanChatService> clanChatService = player.world().server().service(ClanChatService.class, true);
							if (clanChatService.isPresent() && chat != null) {
								clanChatService.get().command(chat, "refresh");
							}
						}
					});
				});
				break;
			}
		}
		//System.out.println("Settng rank of " + name + " to " + rank);
	}
	
}
