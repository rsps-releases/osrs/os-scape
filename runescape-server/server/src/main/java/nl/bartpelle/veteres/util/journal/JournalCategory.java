package nl.bartpelle.veteres.util.journal;

public class JournalCategory {

    public final String name;

    public int count;

    JournalCategory(String name) {
        this.name = name;
    }

}