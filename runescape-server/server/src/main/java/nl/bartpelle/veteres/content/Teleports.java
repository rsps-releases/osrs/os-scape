package nl.bartpelle.veteres.content;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import nl.bartpelle.veteres.util.JGson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

public class Teleports {

    public static Category[] CATEGORIES;

    public static void loadTeleports() {
        Gson gson = JGson.get();
        try {
            Map<String, Subcategory[]> categories = gson.fromJson(new FileReader("data/teleports.json"), new TypeToken<Map<String, Subcategory[]>>() {
            }.getType());
            CATEGORIES = new Category[categories.size()];
            int i = 0;
            for (Map.Entry<String, Subcategory[]> entry : categories.entrySet()) {
                Category category = new Category();
                category.name = entry.getKey();
                category.subcategories = entry.getValue();
                CATEGORIES[i++] = category;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static final class Category {
        public String name;
        public Subcategory[] subcategories;
    }

    public static final class Subcategory {
        @Expose public String name;
        @Expose public Teleport[] teleports;
    }

    public static final class Teleport {
        @Expose public String name;
        @Expose public int x, y, z;
        @Expose public int price;
    }
}
