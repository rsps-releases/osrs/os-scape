package nl.bartpelle.veteres.services.advertisement;

import com.everythingrs.vote.Vote;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.services.Service;

/**
 * Created by Jonathan on 12/30/2017.
 */
public class VoteRewardService implements Service {
	
	private static final String SECRET = "kizyukacjw1qzfe08wo7xpqfr636tq9b6jq8jno2i0os7fd2t992v48926nx16rn5pr2cpu8fr";
	
	public static int TOTAL_VOTES_CLAIMED;
	private static int VOTING_REWARD_TOKEN = 5020;
	
	private GameServer server;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
	}
	
	public void claim(Player player) {
		// Why would you lookup for someone not online? It's going to fail later.
		if (!server.world().playerForId(player.id()).isPresent())
			return;
		
		Vote.service.execute(() -> {
			try {
				Vote reward = Vote.reward(SECRET, player.name(), "1", "all")[0];
				if (reward.message != null) {
					player.message(reward.message);
					return;
				}
				
				giveReward(player, reward.give_amount);
			} catch (Exception e) {
				player.message("Api Services are currently offline. Please check back shortly");
				e.printStackTrace();
			}
		});
	}
	
	private void giveReward(Player player, int amount) {
		boolean inventory = player.inventory().full();
		
		// Increment vote count. Used to broadcast every X votes, to make people aware you can vote.
		TOTAL_VOTES_CLAIMED++;
		
		if (!inventory && player.inventory().freeSlots() >= 1) {
			player.message("<img=23><col=f9fe11><shad> Thank you for voting! As a reward, a voting reward token has been deposited into your inventory.");
			player.inventory().add(new Item(VOTING_REWARD_TOKEN, amount), true);
		} else {
			player.message("<img=23><col=f9fe11><shad> Thank you for voting! As a reward, a voting reward token has been deposited into your bank.");
			player.bank().add(new Item(VOTING_REWARD_TOKEN, amount), true);
		}
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return false;
	}
	
	@Override
	public boolean isAlive() {
		return false;
	}
	
}
