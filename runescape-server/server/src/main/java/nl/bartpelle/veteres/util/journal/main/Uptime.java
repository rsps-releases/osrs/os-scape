package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.quests.QuestGuide;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class Uptime extends JournalEntry {

    public static final Uptime INSTANCE = new Uptime();

    @Override
    public void send(Player player) {
        send(player, "<img=11> Uptime",  player.world().getTime(true), Color.GREEN);
    }

    @Override
    public void select(Player player) {
        player.stopActions(false);
        QuestGuide.clear(player);

        player.interfaces().text(275, 2, "<img=1> Server Information");
        player.interfaces().text(275, 4, "<col=800000>Notice:</col> If there's something you'd like to see on");
        player.interfaces().text(275, 5, "this list, please request it on our forums.");
        player.interfaces().text(275, 7, "<col=800000>Server uptime:</col> " + player.world().getTime(true));

        QuestGuide.open(player);
    }

}