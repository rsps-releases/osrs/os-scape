package nl.bartpelle.veteres.util;

/**
 * Created by Bart on 1/1/2016.
 * <p>
 * Tear out his spline, take out your gold pieces and smear the blood
 * of your opponent on the money. Yeah, that's what it's like being a killer.
 */

import java.util.HashMap;
import java.util.Map;

/**
 * !!!!! VERY IMPORTANT !!!!!
 * <p>
 * When adding an item unobtainable through the shops to our HashMap, ensure you
 * add it into the UNPURCHASABLE intArrayOf Inside:
 * <p>
 * server\src\main\kotlin\nl\bartpelle\veteres\content\interfaces\TournamentSupplies.kt
 * <p>
 * If you don't do this, somebody can modify the client and obtain the items through our
 * interface.
 */

public class BloodMoney {
	
	//Hello future coder, we no longer use this as all prices have moved to the database.
	//If you want to modify a price for an item make sure you get the latest price by checking the database for current price
	
	//See ItemPriceRepository.java
	//To get prices use world.prices().get(itemid, price, forceCoins)
	//the force coins is used for only world 2 if for some reason you want the coin price and not the blood money price :)
	/*
	    SELECT * FROM public.itemdefinitions WHERE id = YOURITEMID;
        
        
        then update it through the UI or
        
        UPDATE public.itemdefinitions
        SET cost = COINSCOST, bm_value = BLOODYMONEYCOST
        WHERE id = YOURITEMID;
        
     */
      public static final Map<Integer, Integer> CATALOG = new HashMap<Integer, Integer>() {
        {
            put(11808, 6_000); // Zamorak godsword
            put(20374, 6_000); // zgs (or)
            put(11806, 6_000); // Saradomin godsword
            put(20372, 6_000); // sgs (or)
            put(11804, 10_000); // Bandos godsword
            put(20370, 10_000); // bgs (or)
            put(11802, 30_000); // Armadyl godsword
            put(20368, 30_000); // ags (or)
            put(12851, 25_000); // Amulet of the damned
            put(12853, 25_000); // Amulet of the damned
            put(11832, 5_000); // Bandos chestplate
            put(11834, 7_500); // Bandos tassets
            put(11826, 4_000); // Armadyl helmet
            put(11828, 7_000); // Armadyl chestplate
            put(11830, 7_000); // Armadyl chainskirt
            put(6918, 500); // Infinity hat
            put(6916, 750); // Infinity top
            put(6924, 750); // Infinity bottoms
            put(6920, 750); // Infinity boots
            put(6922, 500); // Infinity gloves
            put(12419, 5_500); // Light infinity hat (ornament)
            put(12420, 5_750); // Light infinity top (orn)
            put(12421, 5_750); // Light infinity bottoms (orn)
            put(12457, 5_500); // Dark infinity hat (orn)
            put(12458, 5_750); // Dark infinity top (orn)
            put(12459, 5_750); // Dark infinity bottoms (orn)
            put(11791, 5_000); // Staff of the dead
            put(12902, 7_000); // Uncharged Toxic staff of the dead
            put(12904, 7_000); // Toxic staff of the dead
            put(11283, 9_000); // Dragonfire shield
            put(13265, 13_000); // Abyssal dagger normal
            put(13267, 13_000); // Abyssal dagger p
            put(13269, 13_000); // Abyssal dagger p+
            put(13271, 13_000); // Abyssal dagger p++
            put(6585, 3_500); // Amulet of fury
            put(12436, 3_500); // Amulet of fury (or)
            put(12929, 7_000); // Serpentine helm uncharged
            put(12931, 7_000); // Serpentine helm
            put(13196, 7_000); // Tanz helm uncharged
            put(13197, 7_000); // Tanz helm
            put(13198, 7_000); // Magma helm uncharged
            put(13199, 7_000); // Magma helm
            put(11924, 7_000); // Malediction ward
            put(11926, 7_000); // Odium ward
            put(12806, 13_000); // Malediction (or)
            put(12807, 13_000); // Odium (or)
            put(4224, 6_500); // New crystal shield
            put(4212, 10_000); // New crystal bow
            put(4225, 6_500); // Crystal shield (full)
            put(4214, 10_000); // Crystal bow (full)
            put(4207, 3_000); // Crystal seed. Can be enchanted for BM into other items.
            put(13080, 9_000); // Crystal halberd (i) (new)
            put(13081, 9_000); // Crystal halberd (i) (full)
            put(13091, 9_000); // Crystal halberd (new)
            put(13092, 9_000); // Crystal halberd  (full)
            put(12002, 3_000); // Occult necklace
            put(19720, 3_000); // Occult necklace (or)
            put(11785, 15_000); // Armadyl crossbow
            put(12829, 600); // Spirit shield
            put(12831, 4500); // Blessed spirit shield
            put(12817, 150_000); // Elysian spirit shield
            put(12825, 70_000); // Arcane spirit shield
            put(12821, 70_000); // Spectral spirit shield
            put(4151, 750); // Abyssal whip
            put(11838, 3200); // Saradomin sword
            put(12006, 4_000); // Abyssal tentacle
            put(12808, 4_000); // Blessed sara sword (full)
            put(12809, 4_000); // Blessed sara sword
            put(11235, 750); // Dark bow
            put(11840, 550); // Dragon boots
            put(2577, 1_000); // Ranger boots
            put(13227, 2_500); // Eternal crystal
            put(13229, 2_500); // Pegasian crystal
            put(13231, 2_500); // Primordial crystal
            put(6889, 4_000); // Mage's book
            put(6914, 2_000); // Master wand
            put(11905, 4_000); // Trident
            put(11907, 4_000); // Trident
            put(11908, 4_000); // Trident
            put(12899, 4_000); // Trident
            put(12900, 4_000); // Trident
            put(6524, 400); // Obby shield (defensive rune defender) aka Toktz-ket-xil
            put(13235, 3_250); // Eternal boots. Combined from infinity & eternal crystal.
            put(13237, 3_500); // Pegasian boots. Rangers & peg crystal.
            put(13239, 3_050); // Primordial boots. Dragon boots & prim crystal.
            put(12773, 1750); // Ice whip
            put(12774, 1750); // Lava whip
            put(12765, 1750); // Dark bow coloured
            put(12766, 1750); // Dark bow coloured
            put(12767, 1750); // Dark bow coloured
            put(12768, 1750); // Dark bow coloured
            put(1038, 80_000); // Red Partyhat
            put(1040, 90_000); // Yellow Partyhat
            put(1042, 100_000); // Blue Partyhat
            put(1044, 75_000); // Green Partyhat
            put(1046, 50_000); // Purple Partyhat
            put(1048, 70_000); // White Partyhat
            put(11862, 120_000); // Black Partyhat
            put(11863, 120_000); // Rainbow Partyhat
            put(12399, 120_000); // Partyhat & specs
            put(1053, 40_000); // Green halloween mask
            put(1055, 40_000); // Blue halloween mask
            put(1057, 40_000); // Red halloween mask
            put(11847, 45_000); // Black halloween mask
            put(1050, 40_000); // Santa hat
            put(13343, 40_000); // Black santa hat
            put(13344, 40_000); // Inverted santa hat
            put(12845, 5_000); // Grim reaper hood
            put(1419, 5_000); // Scythe
            put(9920, 7_500); // Jack lantern mask
            put(9925, 7_500); // Skeleton mask
            put(9924, 7_500); // Skeleton shirt
            put(9923, 7_500); // Skeleton leggings
            put(9922, 3_500); // Skeleton gloves
            put(9921, 3_500); // Skeleton boots
            put(13283, 7_500); // Gravedigger mask
            put(13284, 7_500); // Gravedigger top
            put(13285, 7_500); // Gravedigger leggings
            put(13286, 3_500); // Gravedigger gloves
            put(13287, 3_500); // Gravedigger boots
            put(6665, 7_500); // Mudskipper hat
            put(6666, 7_500); // Flippers
            put(4566, 5_000); // Rubber chicken
            put(11919, 7_500); // Cow mask
            put(12956, 7_500); // Cow top
            put(12957, 7_500); // Cow leggings
            put(12958, 3_500); // Cow gloves
            put(12959, 3_500); // Cow boots
            put(1037, 5_000); // Bunny ears
            put(13182, 5_000); // Bunny feet
            put(4565, 5_000); // Easter basket
            put(12887, 7_500); // Santa mask
            put(12888, 7_500); // Santa top
            put(12889, 7_500); // Santa leggings
            put(12890, 3_500); // Santa gloves
            put(12891, 3_500); // Santa boots
            put(12353, 5_000); // Monocle
            put(12434, 5_000); // Top hat & monocle
            put(12337, 7_500); // Sagacious spectacles
            put(12892, 7_500); // Anti-santa mask
            put(12893, 7_500); // Anti-santa top
            put(12894, 7_500); // Anti-santa leggings
            put(12895, 3_500); // Anti-santa gloves
            put(12896, 3_500); // Anti-santa boots
            put(7537, 5_000); // Crab claw
            put(8929, 5_000); // Crab claw hook
            put(2997, 5_000); // Pirate's hook
            put(8924, 500); // Bandana eyepatch
            put(8925, 500); // Bandana eyepatch
            put(8926, 500); // Bandana eyepatch
            put(8927, 500); // Bandana eyepatch
            put(8950, 1_500); // Pirate's hat
            put(12412, 2_500); // Piate's hat & patch
            put(12355, 2_000); // Big pirate's hat
            put(2631, 600); // Highwayman mask
            put(2639, 400); // Tan cavalier
            put(2641, 400); // Dark cavalier
            put(12325, 600); // Navy cavalier
            put(12323, 600); // Red cavalier
            put(12321, 800); // White cavalier
            put(2643, 800); // Black cavalier
            put(11280, 800); // Cavalier mask
            put(12319, 1_500); // Crier hat
            put(12249, 2_500); // Imp mask
            put(12251, 2_500); // Goblin mask
            put(12361, 2_500); // Cat mask
            put(12428, 2_500); // Penguin mask
            put(12245, 3_000); // Beanie
            put(12430, 3_000); // Afro
            put(12359, 2_500); // Leprechaun hat
            put(12540, 2_500); // Deerstalker
            put(2645, 400); // Red headband
            put(2647, 400); // Black headband
            put(2649, 400); // Brown headband
            put(12299, 400); // White headband
            put(12301, 400); // Blue headband
            put(12303, 400); // Gold headband
            put(12305, 400); // Pink headband
            put(12307, 400); // Green headband
            put(10316, 300); // Bob's red shirt
            put(10318, 300); // Bob's blue shirt
            put(10322, 300); // Bob's black shirt4
            put(10324, 300); // Bob's purple shirt
            put(12375, 500); // Black cane
            put(12377, 1_500); // Adamant cane
            put(12379, 3_000); // Rune cane
            put(12373, 5_000); // Dragon cane
            put(12363, 2_500); // Bronze dragon mask
            put(12365, 750); // Iron dragon mask
            put(12367, 1_000); // Steel dragon mask
            put(12369, 1_250); // Mithril dragon mask
            put(12371, 2_500); // Lava dragon mask
            put(12518, 1_500); // Green dragon mask
            put(12522, 1_500); // Red dragon mask
            put(12524, 1_500); // Black dragon mask
            put(12526, 5_000); // Fury ornament kit
            put(12528, 5_000); // Dark infinity colour kit
            put(12530, 5_000); // Light infinity colour kit
            put(12532, 5_000); // Dragon sq shield ornament kit
            put(12534, 5_000); // Dragon chainbody ornament kit
            put(12536, 5_000); // Dragon plate/skirt ornament kit
            put(12528, 5_000); // Dragon full helm ornament kit
            put(12849, 5_000); // Granite clamp (kit)
            put(12848, 6_000); // Granite maul (ornament)
            put(12757, 1_000); // Blue dark bow paint
            put(12759, 1_000); // Green dark bow paint
            put(12761, 1_000); // Yellow dark bow paint
            put(12763, 1_000); // White dark bow paint
            put(12769, 1_000); // Frozen whip mix
            put(12771, 1_000); // Volcanic whip mix
            put(12774, 5_750); // Frozen Abyssal whip (not listed)
            put(12773, 5_750); // Volcanic Abyssal whip (not listed)
            put(12766, 5_750); // Colored dbow (not listed)
            put(12765, 5_750); // Colored dbow (not listed)
            put(12767, 5_750); // Colored dbow (not listed)
            put(12768, 5_750); // Colored dbow (not listed)
            put(12802, 5_000); // Ward upgrade kit
            put(12798, 5_000); // Steam staff upgrade kit
            put(12796, 5_000); // Steam staff (ornament)
            put(8845, 100); // Bronze defender
            put(8846, 200); // Iron defender
            put(8847, 300); // Steel defender
            put(8848, 400); // Black defender
            put(8849, 500); // Mithril defender
            put(8850, 600); // Addy defender
            put(8850, 700); // Rune defender
            put(8850, 700); // Rune defender
            put(12954, 800); // Dragon defender
            put(6570, 3_000); // Fire cape
            put(13280, 4_000); // Max cape
            put(13342, 4_000); // Max cape with right-click equip options
            put(13329, 5_000); // Fire max cape
            put(13331, 5_000); // Saradomin max cape
            put(13333, 5_000); // Zamorak max cape
            put(13335, 5_000); // Guthix max cape
            put(13337, 5_000); // Ava's max cape
            put(13124, 3_000); // Ardougne cloak 4
            put(20760, 5_000); // Ardougne max cape.
            put(11663, 1_500); // Mage helm
            put(11664, 1_500); // Range helm
            put(11665, 1_500); // Melee helm
            put(13072, 3_500); // Elite void top
            put(8839, 2_500); // Void top
            put(13073, 3_500); // Elite void robe
            put(8840, 2_500); // Void robe
            put(8842, 2_000); // Void gloves
            put(10547, 1_000); // Healing hat
            put(10548, 1_000); // Fighter hat
            put(10549, 1_000); // Runner hat
            put(10550, 1_000); // Ranger hat
            put(10551, 2_000); // Fighter torso
            put(10552, 1_000); // Runner boots
            put(10555, 1_000); // Penance skirt
            put(4708, 500); // Ahrims hood
            put(4716, 500); // Dharoks helm
            put(4724, 500); // Guthans helm
            put(4732, 500); // Karils coif
            put(4745, 500); // Torags helm
            put(4753, 500); // Veracs helm
            put(4712, 750); // Ahrims robetop
            put(4720, 500); // Dharoks platebody
            put(4728, 500); // Guthans platebody
            put(4736, 750); // Karils leathertop
            put(4749, 500); // Torags platebody
            put(4757, 500); // Veracs brassard
            put(4714, 750); // Ahrims robeskirt
            put(4722, 500); // Dharoks platelegs
            put(4730, 500); // Guthans skirt
            put(4738, 500); // Karils leatherskirt
            put(4751, 500); // Torags platelegs
            put(4759, 500); // Veracs plateskirt
            put(4710, 500); // Ahrims Staff
            put(4718, 500); // Dharoks axe
            put(4726, 500); // Guthans warpsear
            put(4734, 500); // Karils crossbow
            put(4747, 500); // Torags hammers
            put(4755, 500); // Veracs flail
            put(12695, 10); // Super combat potion (4)
            put(12907, 25); // Anti-venom(4)
            put(12909, 18); // Anti-venom(3)
            put(12911, 12); // Anti-venom(2)
            put(12905, 6); // Anti-venom(1)
            put(2446, 0); // Anti-poison
            put(12625, 0); // Stamina potion KEEP THIS AS 0. PEOPLE HAVE BOUGHT OUT 5K BY MISTAKE. CANNOT use migration tool to give back value=dupe!
            put(11936, 2); // Dark crab
            put(3144, 0); // Cooked Karambwuan SAME AS STAMINA. Id was noted instead of non-noted, people bought 100k out in 10 minutes. Can't refund.
            put(385, 0); //Shark
            put(11212, 10); // Dragon arrows
            put(8013, 0); // Home teles
            put(9244, 0); // Spawnable d bolts (e)
            put(9243, 0); // Spawnable diamond bolts (e)
            put(892, 0); // Spawnable rune arrows
            put(4153, 1000); // Granite maul
            put(1725, 0); // Str amulet
            put(2550, 2); // Recoil rings
            put(12642, 2); // Lumberyard teleport scroll
            put(6731, 2500); // Seers ring
            put(6733, 4000); // Archers ring
            put(6735, 800); // Warriors ring
            put(6737, 4250); // Berserker ring
            put(11770, 4000); // Seers ring (i)
            put(11771, 7500); // Archer ring (i)
            put(11772, 1400); // Warriors ring (i)
            put(11773, 7750); // Berserker ring (i)
            put(2412, 0); // God cape
            put(2413, 0); // God cape
            put(2414, 0); // God cape
            put(2581, 15_000); // Robin hood hat
            put(11128, 1000); // Obby neck
            put(6528, 200); // Obby maul
            put(6525, 180); // Obby dagger/knife
            put(12788, 150); // Msb (i)
            put(12596, 25_000); // Ranger's tunic
            put(11898, 500); // Decorative hat
            put(11896, 500); // Decorative top
            put(11897, 500); // Decorative bottom
            put(11899, 500); // Decorative range top
            put(11900, 500); // Decorative range bottom
            put(12637, 500); // Sara halo
            put(12638, 500); // Zam halo
            put(12639, 500); // Guth halo
            put(10156, 100); // Hunter's crossbow
            put(13028, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13030, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13032, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13034, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13052, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13054, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13056, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13058, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13060, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13062, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13040, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13042, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13044, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13046, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13048, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(13050, 35); // Rune armour set -- could be a god set, (t) or (g) (sk) or (pl)
            put(12453, 600); // Black wiz (g) hat
            put(12449, 1000); // Black wiz (g) top
            put(12445, 800); // Black wiz (g) bottom
            put(12455, 600); // Black wiz (t) hat
            put(12451, 1000); // Black wiz (t) top
            put(12447, 800); // Black wiz (t) bottom
            put(10458, 300); // Vestment robes
            put(10464, 300); // Vestment robes
            put(10460, 300); // Vestment robes
            put(10468, 300); // Vestment robes
            put(10462, 300); // Vestment robes
            put(10466, 300); // Vestment robes
            put(12193, 300); // Vestment robes
            put(12195, 300); // Vestment robes
            put(12253, 300); // Vestment robes
            put(12255, 300); // Vestment robes
            put(12265, 300); // Vestment robes
            put(12267, 300); // Vestment robes
            put(13307, 1); // Blood money is worth 1 bm duh
            put(13576, 35_000); // Dragon warhammer: NOT IN STORES. Only through webstore (but protection price..)
            put(1249, 250); // D spear. Also from pvm in limited supply (dont fuck prices that much)
            put(1187, 900); // D sq
            put(12418, 900); // d sq (g)
            put(4585, 380); // D plateskirt
            put(12416, 380); // d skirt (g)
            put(4087, 380); // d legs
            put(12415, 380); // d legs (g)
            put(3140, 450); // d chain
            put(12414, 450); // d chain (g)
            put(11335, 5000); // d full helm
            put(12417, 5000); // d full helm (g)
            put(6809, 150); // Granite platelegs
            put(3122, 200); // Granite shield
            put(9069, 15); // Moonclan armour peice
            put(9070, 15); // Moonclan armour peice
            put(9071, 15); // Moonclan armour peice
            put(9072, 15); // Moonclan armour peice
            put(9073, 15); // Moonclan armour peice
            put(9074, 15); // Moonclan armour peice
            put(13652, 40_000); // Dragon claws
            put(12924, 20_000); // Toxic blowpipe (empty)
            put(12926, 20_000); // Toxic blowpipe
            put(1523, 0); // Lockpick
            put(2572, 5_000); // Ring of wealth
            put(1727, 0); // Amulet of Magic
            put(1729, 0); // Amulet of defence
            put(1731, 0); // Amulet of power
            put(1478, 0); // Amulet of accuracy
            put(11978, 0); // Amulet of glory
            put(11090, 0); // Pheonix necklace
            put(7458, 0); // Mithril gloves
            put(7459, 0); // Addy gloves
            put(7460, 0); // Rune gloves
            put(7461, 0); // Dragon gloves
            put(7462, 0); // Barrows gloves
            put(2446, 0); // Antipoison(4)
            put(3840, 0); // Holy book
            put(3842, 0); // Unholy book
            put(3844, 0); // Book of balance
            put(12608, 0); // Book of war
            put(12610, 0); // Book of law
            put(12612, 0); // Book of darkness
            put(2415, 0); // Saradomin staff
            put(2416, 0); // Guthix staff
            put(2417, 0); // Zamorak staff
            put(861, 0); // Magic shortbow
            put(6328, 0); // Snakeskin boots
            put(4740, 0); // Bolt rack
            put(868, 0); // Rune knife
            put(811, 0); // Rune dart
            put(10034, 0); // Chincompa

            // THIRD AGE
            put(10330, 32000); // range top
            put(10332, 32000); // range legs
            put(10334, 32000); // range coif
            put(10336, 32000); // vambs
            put(10338, 32000); // ropbe top
            put(10340, 32000); // robe bottom
            put(10342, 32000); // mage hat
            put(10344, 32000); // ammy
            put(10346, 32000); // platelegs
            put(10348, 32000); // platebody
            put(10350, 32000); // full helm
            put(10352, 32000); // kiteshield
            put(12422, 32000); // wand
            put(12424, 32000); // bow
            put(12426, 32000); // longsword
            put(12437, 32000); // cloak
            put(12020, 1000); // dice bag

            put(19478, 10_000); // Light ballista
            put(19481, 31_000); // Heavy ballista
            put(19484, 500); // Dragon Javelin
            put(19553, 20_000); // Amulet of torture- above ags, spectral, under arc, ballista
            put(20366, 20_000); // torture (or)
            put(22249, 15_000); //anguish (or)
            put(19547, 15_000); // Necklace of anguish- above ags, spectral, under arc, ballista
            put(19550, 25_000); // Ring of suffering - defensive ring
            put(19710, 25_000); // Ring of suffering (i)
            put(20655, 25_000); // Ring of suffering (r)
            put(20657, 25_000); // Ring of suffering (ri)
            put(19544, 30_500); // Tormented bracelet - above ags, spectral, under arc, ballista

            put(4860, 100); // Barrows 0
            put(4866, 100); // Barrows 0
            put(4872, 100); // Barrows 0
            put(4878, 100); // Barrows 0
            put(4884, 100); // Barrows 0
            put(4890, 100); // Barrows 0
            put(4896, 100); // Barrows 0
            put(4902, 100); // Barrows 0
            put(4908, 100); // Barrows 0
            put(4914, 100); // Barrows 0
            put(4920, 100); // Barrows 0
            put(4926, 100); // Barrows 0
            put(4932, 100); // Barrows 0
            put(4938, 100); // Barrows 0
            put(4944, 100); // Barrows 0
            put(4950, 100); // Barrows 0
            put(4956, 100); // Barrows 0
            put(4962, 100); // Barrows 0
            put(4968, 100); // Barrows 0
            put(4974, 100); // Barrows 0
            put(4980, 100); // Barrows 0
            put(4986, 100); // Barrows 0
            put(4992, 100); // Barrows 0
            put(4998, 100); // Barrows 0
            put(299, 1); // Mithril seeds.
            put(11824, 5000); // Zammy spear
            put(11889, 5000); // Zammy hasta
            put(19566, 3000); // Skotizo's key, to make it tradable.
            put(20000, 20000); // Dragon scimitar (g)
            put(20002, 20000); // Dragon scimitar ornament kit
            put(20143, 10000); // Dragon defender ornament kit
            put(19722, 10800); // Dragon defender (ornament)
            put(20062, 12500); // Amulet of torture ornament kit
            put(20065, 12500); // Occult ornament kit
            put(20068, 15000); // Armadyl godsword ornament kit
            put(20071, 15000); // Bandos godsword ornament kit
            put(20074, 15000); // Saradomin godsword ornament kit
            put(20077, 15000); // Zamorak godsword ornament kit
            put(12802, 10000); // Ward update kit
            put(7158, 3000); //Dragon 2H
            put(12800, 5000); // Dragon pickaxe upgrade kit
            put(12797, 5000); // Dragon pickaxe (orn)
            put(11920, 250); // Dragon pickaxe

            put(21003, 40_000); // Elder maul
            put(21006, 60_000); // Kodai wand
            put(21018, 60_000); // Ancestral hat
            put(21021, 76_000); // Ancestral robe top
            put(21024, 67_000); // Ancestral robe bottom
            put(20997, 135_000); // Twisted bow
            put(20849, 1_600); // Dragon thrownaxe
            put(21000, 50_000); // Twisted buckler
            put(21028, 1_600); // Dragon harpoon
            put(21012, 37_000); // Dragon hunter crossbow
            put(21009, 7_000); // Dragon sword
            put(21015, 40_000); // Dinh's Bulwark
            put(13215, 1_000); //Blood token
            put(12791, 5_000); // Rune pouch

            put(22242, 5_000); // Dragon platebody (or)
            put(22244, 5_000); // Dragon kiteshield (or)
            put(22234, 5_000); // Dragon boots (or)
            put(22296, 9_000); // Staff of light
            put(13190, 1_500); // Bond (OS-Scape credit)
            put(1505, 20_000); // Obelisk redirection scroll
        }
    };
	
}
