package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;
import nl.bartpelle.veteres.model.item.Item;

/**
 * Created by Bart on 11/03/2017.
 */
public class ResetPvpSkills implements Migration {
	
	private static final int[] SKILLS_TO_RESET = {
			Skills.FISHING, Skills.HUNTER, Skills.MINING,
			Skills.WOODCUTTING, Skills.SLAYER, Skills.THIEVING
	};
	
	@Override
	public int id() {
		return 17;
	}
	
	@Override
	public boolean apply(Player player) {
		if (player.world().realm().isPVP()) {
			for (int i : SKILLS_TO_RESET) {
				player.skills().setLevel(i, 1);
				player.skills().setXp(i, 0);
			}
		}
		
		return true;
	}
}
