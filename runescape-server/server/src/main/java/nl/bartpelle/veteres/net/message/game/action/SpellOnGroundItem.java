package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.SpellOnEntity;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Jak on 10/05/2016.
 */
@PacketInfo(size = 13)
public class SpellOnGroundItem implements Action {
	
	private static final Logger logger = LogManager.getLogger(SpellOnGroundItem.class);
	
	private int parent, child, x, y, itemid, unknown, varp82;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		varp82 = buf.readByte();
		x = buf.readULEShortA();
		unknown = buf.readShort();
		y = buf.readULEShortA();
		itemid = buf.readUShort();
		int hash = buf.readLEInt();
		parent = hash >> 16;
		child = hash & 0xFF;
		
		player.debug("%d %d %d %d %d %d %d", parent, child, x, y, itemid, varp82, unknown);
	}
	
	@Override
	public void process(Player player) {
		
		if (!player.locked() && !player.dead()) {
			player.stopActions(false);
			GroundItem gitem = player.world().getGroundItem(player, x, y, player.tile().level, itemid);
			if (gitem == null) {
				return;
			}
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, gitem);
			player.putattrib(AttributeKey.INTERACTION_OPTION, 4);
			player.putattrib(AttributeKey.INTERACTED_WIDGET_INFO, new int[]{parent, child});
			player.world().server().scriptExecutor().executeLater(player, SpellOnEntity.script);
		}
	}
}
