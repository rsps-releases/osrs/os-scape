package nl.bartpelle.veteres.content.mechanics;

import nl.bartpelle.skript.ScriptMain;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.script.ScriptRepository;
import nl.bartpelle.veteres.script.TimerKey;

/**
 * Created by Bart on 8/16/2016.
 */
public class AntifirePotionJava {

	@ScriptMain
	public static void register(ScriptRepository r) {
		// Register on login
		r.onLogin(script -> {
			((Player) script.ctx()).timers().register(TimerKey.ANTIFIRE_POTION, 10);
			return null;
		});
		
		// On antifire timer
		r.onTimer(TimerKey.ANTIFIRE_POTION, script -> {
			Player player = script.ctx();
			int ticks = player.attribOr(AttributeKey.ANTIFIRE_POTION, 0);
			if (ticks == 0) {
				// Not active.
				player.timers().register(TimerKey.ANTIFIRE_POTION, 19);
				return null;
			}
			ticks -= 1;

			if (ticks == 0 || ticks == 1000) {
				player.message("<col=8F4808>Your antifire potion has expired.");
				player.clearattrib(AttributeKey.ANTIFIRE_POTION);
			} else {
				player.putattrib(AttributeKey.ANTIFIRE_POTION, ticks);
				if (ticks == 3 || ticks == 1003) {
					player.message("<col=8F4808>Your antifire potion is about to expire.");
				}
			}
			player.timers().register(TimerKey.ANTIFIRE_POTION, 19);
			return null;
		});
	}
	
}
