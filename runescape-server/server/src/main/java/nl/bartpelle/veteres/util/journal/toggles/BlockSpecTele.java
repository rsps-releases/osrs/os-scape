package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class BlockSpecTele extends JournalEntry {

    public static void update(Player player) {
        boolean block = VarbitAttributes.varbiton(player, VarbitAttributes.VarbitInfo.BLOCK_SPEC_AND_TELE.getVarbitid());
        player.write(UpdateStateCustom.skullToggle(!block));
    }

    @Override
    public void send(Player player) {
        boolean block = VarbitAttributes.varbiton(player, VarbitAttributes.VarbitInfo.BLOCK_SPEC_AND_TELE.getVarbitid());
        if(block)
            send(player, "<img=58> Block Spec Tele", "On", Color.GREEN);
        else
            send(player, "<img=58> Block Spec Tele", "Off", Color.RED);
        update(player);
    }

    @Override
    public void select(Player player) {
        VarbitAttributes.toggle(player, VarbitAttributes.VarbitInfo.BLOCK_SPEC_AND_TELE.getVarbitid());
        boolean block = VarbitAttributes.varbiton(player, VarbitAttributes.VarbitInfo.BLOCK_SPEC_AND_TELE.getVarbitid());
        if(block)
            player.message(Color.COOL_BLUE.wrap("Players will no longer be able to special attack and instantly teleport against you."));
        else
            player.message(Color.COOL_BLUE.wrap("Players will now be able to special attack and instantly teleport against you."));
        send(player);
    }

}