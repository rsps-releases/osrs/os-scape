package nl.bartpelle.veteres.model.entity.player;

/**
 * Created by Bart on 11/30/2015.
 */
public enum PremiumTier {
	NONE, SILVER, GOLD, PLATINUM, DIAMOND
}
