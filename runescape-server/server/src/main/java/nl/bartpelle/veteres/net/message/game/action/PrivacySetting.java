package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.net.message.game.command.UpdateFriendsStatus;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer;

/**
 * Created by Jak on 09/07/2016.
 */
@PacketInfo(size = 3)
public class PrivacySetting implements Action {
	
	// 2=off, 1=friends, 0=on
	private int publicChat, privateChat, tradeStatus;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		publicChat = buf.readByte();
		privateChat = buf.readByte();
		tradeStatus = buf.readByte();
	}
	
	@Override
	public void process(Player player) {
		player.debug("Privacy now %d %d %d", publicChat, privateChat, tradeStatus);
		if (VarbitAttributes.varbit(player, VarbitAttributes.VarbitInfo.PUBLIC_CHAT_STATUS.getVarbitid()) != publicChat) {
			VarbitAttributes.set(player, VarbitAttributes.VarbitInfo.PUBLIC_CHAT_STATUS.getVarbitid(), publicChat);
		}
		if (VarbitAttributes.varbit(player, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) != privateChat) {
			VarbitAttributes.set(player, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid(), privateChat);
			player.message("Changes will take effect on your clan in the next 60 seconds.");
			player.write(new UpdateFriendsStatus(privateChat)); // Force update client, probably redundant.
			
			player.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
				sql.pmStatus(player, privateChat);
				
				if (privateChat == 0 || privateChat == 1) { // On/Friends
					player.world().server().service(PmService.class, true).ifPresent(pmService -> {
						pmService.onUserOnline(player);
					});
				} else if (privateChat == 2) { // Off
					player.world().server().service(PmService.class, true).ifPresent(s -> {
						s.onUserOffline(player);
					});
				}
			});
		}
		if (VarbitAttributes.varbit(player, VarbitAttributes.VarbitInfo.TRADE_STATUS.getVarbitid()) != tradeStatus) {
			VarbitAttributes.set(player, VarbitAttributes.VarbitInfo.TRADE_STATUS.getVarbitid(), tradeStatus);
		}
	}
}
