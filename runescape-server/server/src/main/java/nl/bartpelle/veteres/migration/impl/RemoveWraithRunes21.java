package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.content.items.equipment.RunePouch;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;

/**
 * Created by Bart on 11/03/2017.
 */
public class RemoveWraithRunes21 implements Migration {
	
	@Override
	public int id() {
		return 21;
	}
	
	@Override
	public boolean apply(Player player) {
		player.discardWrites(true);
		
		if (player.world().realm().isPVP()) {
			for (int i = 0; i <= 2; i++) {
				RunePouch.INSTANCE.setRuneAt(player, i, null);
			}
		}
		
		player.discardWrites(false);
		return true;
	}
}
