package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class HighestKillstreak extends JournalEntry {

    @Override
    public void send(Player player) {
        int killstreakTop = player.attribOr(AttributeKey.KILLSTREAK_RECORD, 0);
        send(player, "<img=31> Highest Killstreak", killstreakTop, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int killstreakTop = player.attribOr(AttributeKey.KILLSTREAK_RECORD, 0);
        player.sync().shout("<img=31> My highest kill streak is " + killstreakTop + ".");
    }

}