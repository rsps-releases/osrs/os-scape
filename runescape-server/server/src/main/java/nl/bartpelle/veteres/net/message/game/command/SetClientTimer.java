package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * @author Mack
 */
public class SetClientTimer extends Command {

    private int id;
    private int ticks;

    public SetClientTimer(int id, int ticks) {
        this.id = id;
        this.ticks = ticks;
    }

    @Override
    protected RSBuffer encode(Player player) {
        RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(1 + 4 + 1)).packet(-1);

        buffer.writeByte(id);
        buffer.writeInt(ticks);

        return buffer;
    }
}
