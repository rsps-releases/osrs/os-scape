package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.SpellOnEntity;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.map.MapObj;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Jak on 10/05/2016.
 */
@PacketInfo(size = 13)
public class SpellOnObject implements Action {
	
	private static final Logger logger = LogManager.getLogger(SpellOnObject.class);
	
	private int objectId, x, y, varp82, parent, child, child2;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		child2 = buf.readUShortA();
		int hash = buf.readIntV1();
		parent = hash >> 16;
		child = hash & 0xFF;
		y = buf.readUShortA();
		objectId = buf.readULEShortA();
		x = buf.readUShortA();
		varp82 = buf.readByteS();
		
		player.debug("%d %d %d %d %d %d %d", objectId, x, y, varp82, parent, child, child2);
	}
	
	@Override
	public void process(Player player) {
		
		if (!player.locked() && !player.dead()) {
			player.stopActions(false);
			MapObj obj = player.world().objById(objectId, x, y, player.tile().level);
			if (obj == null) return;
			obj = new MapObj(new Tile(x, y, player.tile().level), obj.id(), obj.type(), obj.rot()).cloneattribs(obj);
			player.putattrib(AttributeKey.INTERACTION_OBJECT, obj);
			player.putattrib(AttributeKey.INTERACTION_OPTION, 3);
			player.putattrib(AttributeKey.INTERACTED_WIDGET_INFO, new int[]{parent, child});
			player.world().server().scriptExecutor().executeLater(player, SpellOnEntity.script);
		}
	}
}
