package nl.bartpelle.veteres.model.entity.player;

import nl.bartpelle.veteres.content.areas.clanwars.ClanWars;
import nl.bartpelle.veteres.content.minigames.raids.RaidExtensionsKt;
import nl.bartpelle.veteres.content.minigames.raids.RaidParty;
import nl.bartpelle.veteres.content.minigames.raids.RaidsResources;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.UpdateClanChat;
import nl.bartpelle.veteres.services.intercom.ClanChatService;
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.RSFormatter;
import nl.bartpelle.veteres.util.UsernameUtilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by Bart on 12/10/2015.
 */
public class ClanChat {
	
	private World world;
	private Set<Friend> entrants = Collections.newSetFromMap(new ConcurrentHashMap<>());
	private Integer owner;
	private String name;
	private String ownerName;
	private Map<Integer, Integer> ranks = new ConcurrentHashMap<>();
	private HashSet<Integer> bannedList = new HashSet<>();
	private HashSet<Integer> ignores = new HashSet<>();
	private HashSet<RaidParty> raidParties = new HashSet<>();
	
	private int talkreq;
	private int kickreq;
	private int joinreq;
	
	public ClanChat(World world, int owner, String name, String ownerName) {
		this.world = world;
		this.owner = owner;
		this.name = name;
		this.ownerName = ownerName;
	}
	
	public void putranks(Map<Integer, Integer> ranks) {
		this.ranks.putAll(ranks);
	}
	
	public void ranks(int kick, int join, int talk) {
		kickreq = kick;
		joinreq = join;
		talkreq = talk;
	}
	
	public int rankFor(int playerId) {
		if (owner.equals(playerId))
			return 127;
		return ranks.getOrDefault(playerId, 0);
	}
	
	public int rawRankFor(int playerId) {
		return ranks.getOrDefault(playerId, 0);
	}
	
	public int kickreq() {
		return kickreq;
	}
	
	public int joinreq() {
		return joinreq;
	}
	
	public int talkreq() {
		return talkreq;
	}
	
	public boolean canKick(int player) {
		return isOwner(player) || rankFor(player) >= kickreq || (kickreq == 100 && ranks.containsKey(player));
	}
	
	public boolean canTalk(int player) {
		return isOwner(player) || rankFor(player) >= talkreq || (talkreq == 100 && ranks.containsKey(player));
	}
	
	public boolean canJoin(int player) {
		return isOwner(player) || rankFor(player) >= joinreq || (joinreq == 100 && ranks.containsKey(player));
	}
	
	public boolean isOwner(int id) {
		return owner.equals(id);
	}
	
	public Set<Friend> members() {
		return entrants;
	}

	public ArrayList<Player> toPlayerTypedArray() {
	    ArrayList<Player> array = new ArrayList<>(members().size());
	    for (Friend friend : members()) {
	        Optional<Player> player = world.playerForId(friend.id);
            if (!player.isPresent()) {
                continue;
            }
            array.add(player.get());
        }

	    return array;
    }
	
	public void cleanup() {
		boolean dirty = false;
		
		for (Iterator<Friend> it = entrants.iterator(); it.hasNext(); ) {
			Friend entrant = it.next();
			if (entrant.world == world.id() && !world.playerForId(entrant).isPresent()) {
				it.remove();
				dirty = true;
			}
		}
		
		// Did we make changes? Push
		if (dirty) {
			refreshAll(true);
			broadcastChannel();
		}
	}
	
	/**
	 * Broadcasts the channel and settings to all the worlds alive through redis.
	 */
	public void broadcastChannel() {
		
	}
	
	public void rename(String name) {
		this.name = name;
	}
	
	public static void broadcastRename(Player owner, String name) {
		World world = owner.world();
		name = name.trim();
		if (name.length() > 12)
			name = name.substring(0, 12);
		name = RSFormatter.formatDisplayname(UsernameUtilities.decodeUsername(UsernameUtilities.encodeUsername(name)).replaceAll("_", " ")); // Magic trixxx
		
		final String finalName = name;
		world.server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("UPDATE accounts SET clanname=? WHERE id=?;");
					s.setString(1, finalName);
					s.setInt(2, (Integer) owner.id());
					s.executeUpdate();
					connection.commit();
					
					world.server().service(ClanChatService.class, true).ifPresent(serv -> {
						serv.command((Integer) owner.id(), "reloadinfo", 0);
					});
				}
			});
		});
	}
	
	;
	
	public void clearTempBannedList() {
		bannedList.clear();
	}
	
	public int tempBanListSize() {
		return bannedList.size();
	}
	
	public void disable() {
		bannedList.clear();
		
		world.server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("UPDATE accounts SET clanname=NULL WHERE id=?;");
					s.setInt(1, owner);
					s.executeUpdate();
					connection.commit();
					
					broadcastKickAll();
				}
			});
		});
	}
	
	public String name() {
		return name;
	}
	
	public String ownerName() {
		return ownerName;
	}
	
	public int ownerId() {
		return owner;
	}
	
	public void refreshAll(boolean members) {
		for (Friend friend : entrants) {
			if (friend != null) {
				world.playerForId(friend.id).ifPresent(player -> {
					refresh(player, members);
				});
			}
		}
	}
	
	public void refresh(Player player, boolean members) {
		// Check if we need to be kicked by rule policies
		if (!canJoin((Integer) player.id())) {
			kick((Integer) player.id(), false);
			return;
		}
		
		player.write(new UpdateClanChat(this, members));
	}
	
	public static void clearChat(Player player) {
		player.write(new UpdateClanChat(null, false));
	}
	
	public void kickall() {
		entrants.stream().map(f -> f.id).collect(Collectors.toList()).forEach(id -> kick(id, false));
	}
	
	public void kick(Integer id, boolean banned) {
		if (banned)
			bannedList.add(id);
		
		world.playerForId(id).ifPresent(p -> leave(p, false));
	}
	
	public void leave(Player player, boolean rejoinOnLogin) {
		entrants.removeIf(p -> player.id().equals(p.id));
		player.write(new UpdateClanChat(null, false));
		player.message("You have left the channel.");

		RaidParty currentParty = RaidsResources.partyById(player.attribOr(AttributeKey.RAID_PARTY, -1));
		if (currentParty != null) {
		    RaidExtensionsKt.removeMember(currentParty, player);
        }
		
		if (ClanWars.inInstance(player)) {
			ClanWars.endFor(player, "You can't stay in the arena if you're not on either team.", false);
		}
		
		// If we don't want to rejoin on login (eg. manual leaving), unset the attribute.
		if (!rejoinOnLogin) {
			player.clearattrib(AttributeKey.CLAN_CHANNEL);
		}
		
		// Remove us from the clan
		world.server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("UPDATE online_characters SET clanchat=0 WHERE account_id=?;");
					s.setInt(1, (Integer) player.id());
					s.executeUpdate();
					connection.commit();
					
					broadcastUserChange();
				}
			});
		});
	}
	
	public boolean join(Player player) {
		// Can we join?
		if (ignores.contains((Integer) player.id())) {
			player.message("You are not allowed to join this user's clan channel.");
			return false;
		}
		
		// Does the rank setting allow us to join?
		if (!canJoin((Integer) player.id())) {
			player.message("You do not have a high enough rank to join this clan channel.");
			return false;
		}
		
		//Has our player been temporarily banned?
		if (bannedList != null && bannedList.contains(player.id())) {
			player.message("You are temporarily banned from this clan channel.");
			return false;
		}
		
		//Has our player been temporarily banned?
		if (bannedList != null && bannedList.contains(player.id())) {
			player.message("You are temporarily banned from this clan channel.");
			return false;
		}
		
		// Full?
		if (entrants.size() >= 98) {
			player.message("This channel is full.");
			return false;
		}

		//Join a waiting raid party, if available.
		if (!raidParties.isEmpty()) {
            for (RaidParty party : raidParties) {
                if (party.status() == RaidsResources.RaidPartyStatus.LOBBY.value()) {
                    RaidExtensionsKt.addMember(party, player);
                    break;
                }
            }
        }
		
		player.putattrib(AttributeKey.CLAN_CHANNEL, owner);
		player.message("Now talking in clan channel " + name);
		player.message("To talk, start each line of chat with the / symbol.");
		
		// 'Oss Staking' shared staff account for ranking middlemen/trusted players.
		if (owner == 51470) {
			player.message("<col=FF0000>Find gamble rules on our forums. Hit 55 on 55x2 is a Player win, not a reroll! (Feb 11)");
		}
		
		refresh(player, true);
		
		// Put us in the clan
		world.server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("UPDATE online_characters SET clanchat=? WHERE account_id=?;");
					s.setInt(1, owner);
					s.setInt(2, (Integer) player.id());
					s.executeUpdate();
					connection.commit();
					
					broadcastUserChange();
				}
			});
		});

		return true;
	}
	
	public static void changeReq(Player player, String req, int required) {
		player.message("Changes will take effect on your clan in the next 60 seconds.");
		
		if (req.equals("joinreq"))
			player.putattrib(AttributeKey.CLAN_ENTER, required);
		else if (req.equals("talkreq"))
			player.putattrib(AttributeKey.CLAN_TALK, required);
		else if (req.equals("kickreq"))
			player.putattrib(AttributeKey.CLAN_KICK, required);
		
		nl.bartpelle.veteres.content.interfaces.ClanChat.writeChannelReqs(player);
		
		player.world().server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("UPDATE accounts SET " + req + "=? WHERE id=?;");
					s.setInt(1, required);
					s.setInt(2, (Integer) player.id());
					s.executeUpdate();
					connection.commit();
					
					player.world().server().service(ClanChatService.class, true).ifPresent(serv -> {
						serv.command((Integer) player.id(), "reloadinfo", 0);
					});
				}
			});
		});
	}
	
	public void updateIgnores() {
		world.server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
			s.transact(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement accounts = connection.prepareStatement(
							"SELECT ignores.account_id, friend_id, displayname, lastname, rights\n" +
									"FROM ignores\n" +
									"LEFT JOIN accounts\n" +
									"ON ignores.friend_id = accounts.id\n" +
									"WHERE ignores.account_id = ?");
					
					accounts.setInt(1, ownerId());
					
					ResultSet online = accounts.executeQuery();
					
					ignores.clear();
					
					while (online.next()) {
						ignores.add(online.getInt("friend_id"));
					}
					
					connection.commit();
				}
			});
			
		});
	}
	
	public void updateInformation() {
		world.server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
			s.transact(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("SELECT id,displayname,clanname,joinreq,talkreq,kickreq " +
							"FROM accounts " +
							"WHERE id = ?;");
					stmt.setInt(1, owner);
					ResultSet set = stmt.executeQuery();
					if (set.next()) {
						name = set.getString("clanname");
						ownerName = set.getString("displayname");
						joinreq = set.getInt("joinreq");
						talkreq = set.getInt("talkreq");
						kickreq = set.getInt("kickreq");
						refreshAll(false);
					}
					
					connection.commit();
				}
			});
			
		});
	}
	
	public static void join(Player player, String channel) {
		player.message("Attempting to join channel...");
		
		player.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
			s.transact(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("SELECT id,displayname,clanname,joinreq,talkreq,kickreq " +
							"FROM accounts " +
							"WHERE displayname ILIKE ? AND clanname != '';");
					stmt.setString(1, channel);
					ResultSet set = stmt.executeQuery();
					if (set.next()) {
						int id = set.getInt("id");
						String name = set.getString("clanname");
						String dispname = set.getString("displayname");
						int joinreq = set.getInt("joinreq");
						int talkreq = set.getInt("talkreq");
						int kickreq = set.getInt("kickreq");
						
						if (player.world().chats().containsKey(id)) {
							player.world().chats().get(id).join(player);
						} else {
							// Get ranks!
							stmt = connection.prepareStatement("SELECT friend_id,clanrank FROM friends WHERE friends.account_id = ?;");
							
							stmt.setInt(1, id);
							set = stmt.executeQuery();
							Map<Integer, Integer> ranks = new HashMap<>();
							
							while (set.next()) {
								ranks.put(set.getInt("friend_id"), set.getInt("clanrank"));
							}
							
							ClanChat chat = new ClanChat(player.world(), id, name, dispname);
							chat.putranks(ranks);
							chat.ranks(kickreq, joinreq, talkreq);
							
							// Can we join?
							if (chat.ignores.contains((Integer) player.id())) {
								player.message("You are not allowed to join this user's clan channel.");
								connection.commit();
								return;
							}
							
							// Can we join?
							if (!chat.canJoin((Integer) player.id())) {
								player.message("Your rank is not high enough to join this clan chat.");
								connection.commit();
								return;
							}
							
							//Has our player been kicked?
							if (chat.bannedList != null && chat.bannedList.contains(player.id())) {
								player.message("You are temporarily banned from this clan channel.");
								return;
							}
							
							if (chat.entrants.size() >= 98) {
								player.message("This channel is full.");
								connection.commit();
								return;
							}
							
							player.world().chats().put(id, chat);
							chat.join(player);
						}
					} else {
						player.message("The channel you tried to join does not exist.");
					}
					
					connection.commit();
				}
			});
		});
	}
	
	// TODO this is not dry bart go kys
	public static void join(Player player, int channel) {
		player.message("Attempting to join channel...");
		
		player.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
			s.transact(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("SELECT id,displayname,clanname,joinreq,talkreq,kickreq " +
							"FROM accounts " +
							"WHERE id = ? AND clanname != '';");
					stmt.setInt(1, channel);
					ResultSet set = stmt.executeQuery();
					if (set.next()) {
						int id = set.getInt("id");
						String name = set.getString("clanname");
						String dispname = set.getString("displayname");
						int joinreq = set.getInt("joinreq");
						int talkreq = set.getInt("talkreq");
						int kickreq = set.getInt("kickreq");
						
						if (player.world().chats().containsKey(id)) {
							player.world().chats().get(id).join(player);
						} else {
							// Get ranks!
							stmt = connection.prepareStatement("SELECT friend_id,clanrank FROM friends WHERE friends.account_id = ?;");
							
							stmt.setInt(1, id);
							set = stmt.executeQuery();
							Map<Integer, Integer> ranks = new HashMap<>();
							
							while (set.next()) {
								ranks.put(set.getInt("friend_id"), set.getInt("clanrank"));
							}
							
							ClanChat chat = new ClanChat(player.world(), id, name, dispname);
							chat.putranks(ranks);
							chat.ranks(kickreq, joinreq, talkreq);
							
							// Can we join?
							if (chat.ignores.contains((Integer) player.id())) {
								player.message("You are not allowed to join this user's clan channel.");
								connection.commit();
								return;
							}
							
							// Can we join?
							if (!chat.canJoin((Integer) player.id())) {
								player.message("You do not have a high enough rank to join this clan channel.");
								connection.commit();
								return;
							}
							
							//Has our player been kicked?
							if (chat.bannedList != null && chat.bannedList.contains(player.id())) {
								player.message("You are temporarily banned from this clan channel.");
								return;
							}
							
							if (chat.entrants.size() >= 98) {
								player.message("This channel is full.");
								connection.commit();
								return;
							}
							
							player.world().chats().put(id, chat);
							chat.join(player);
						}
					} else {
						player.message("The channel you tried to join does not exist.");
					}
					
					connection.commit();
				}
			});
		});
	}
	
	public void broadcastUserChange() {
		world.server().service(ClanChatService.class, true).ifPresent(serv -> {
			serv.command(this, "refreshusers");
		});
	}
	
	public void broadcastKickAll() {
		world.server().service(ClanChatService.class, true).ifPresent(serv -> {
			serv.command(this, "kickall");
		});
	}
	
	public void refreshOnline() {
		world.server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement(
							"SELECT id,displayname,world_id,rights " +
									"FROM online_characters " +
									"LEFT JOIN accounts ON online_characters.account_id = accounts.id " +
									"WHERE clanchat = ?;");
					
					s.setInt(1, owner);
					
					ResultSet set = s.executeQuery();
					entrants.clear();
					while (set.next()) {
						Friend friend = new Friend(set.getInt("id"), set.getString("displayname"), null,
								set.getInt("world_id"), rankFor(set.getInt("id")), set.getInt("rights"));
						entrants.add(friend);
					}
					
					connection.commit();
					refreshAll(true);
				}
			});
		});
	}
	
	public void refreshRanks() {
		world.server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("SELECT friend_id,clanrank " +
							"FROM friends WHERE friends.account_id = ?;");
					
					stmt.setInt(1, owner);
					ResultSet set = stmt.executeQuery();
					Map<Integer, Integer> ranks = new HashMap<>();
					
					while (set.next()) {
						ranks.put(set.getInt("friend_id"), set.getInt("clanrank"));
					}
					connection.commit();
					
					ClanChat.this.ranks = ranks;
					
					// Update the list of ranks in friends list
					for (Friend f : entrants) {
						f.clanrank = rankFor(f.id);
					}
					
					refreshAll(true);
				}
			});
		});
	}
	
	public static void leaveCurrent(Player player) {
		
	}
	
	public void chat(Player player, String msg) {
		chat(player, msg, null);
	}
	
	public void chat(Player player, String msg, String chatNameOverride) {
		if (!canTalk((Integer) player.id())) {
			player.message("You are not allowed to talk in this clan channel.");
			return;
		}
		
		Optional<ClanChatService> clanChatService = world.server().service(ClanChatService.class, true);
		
		if (clanChatService.isPresent()) {
			clanChatService.get().broadcast(player, this, msg, chatNameOverride);
		}
	}
	
	public static Optional<ClanChat> current(Player player) {
		int chan = player.attribOr(AttributeKey.CLAN_CHANNEL, -1);
		if (chan >= 0) {
			ClanChat chat = player.world().chats().get(chan);
			return Optional.ofNullable(chat);
		}
		
		return Optional.empty();
	}
	
	public static int currentId(Player player) {
		return player.attribOr(AttributeKey.CLAN_CHANNEL, -1);
	}
	
	public static Optional<ClanChat> own(Player player) {
		ClanChat chat = player.world().chats().get((Integer) player.id());
		return Optional.ofNullable(chat);
	}

	public HashSet<RaidParty> raidParties() {
	    return raidParties;
    }
}
