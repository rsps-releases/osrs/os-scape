package nl.bartpelle.veteres;

import co.paralleluniverse.strands.Strand;
import io.netty.handler.traffic.TrafficCounter;
import it.unimi.dsi.fastutil.objects.Object2IntArrayMap;
import it.unimi.dsi.fastutil.objects.Object2LongArrayMap;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.task.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Bart Pelle on 8/23/2014.
 * <p>
 * Represents the 'heart' of the game server, or the 'main thread' so to speak.
 * The ServerProcessor has a (linked) list of tasks it runs in sequence. If any of
 * the tasks in the sequence fails that tick, the other tasks will still run. The
 * failed task will simply be executed again the next cycle.
 */
public class ServerProcessor extends Thread {
	
	private static final Logger logger = LogManager.getLogger(ServerProcessor.class);
	
	/**
	 * A reference to the server to which this processor belongs.
	 */
	private GameServer server;
	
	/**
	 * A reference to our game world.
	 */
	private World world;
	
	/**
	 * A list of tasks which are run in order every 600ms 'tick'. This list is of type {@link java.util.LinkedList}.
	 */
	private List<Task> tasks = new LinkedList<>();
	
	/**
	 * As long as this boolean is true, we process every 600ms.
	 */
	private boolean running = true;
	
	/**
	 * Ticks until we print some information about the server.
	 */
	private int infotick = 10;
	
	/**
	 * The executor that will nicely execute all our async tasks.
	 */
	private ExecutorService taskExecutor;
	
	/**
	 * Small runnable jobs list to execute on logic thread (before all other tasks)
	 */
	private ConcurrentLinkedQueue<Runnable> logicJobs;
	
	/**
	 * When true, it'll print all information to stdout at the end of the cycle regarding usage.
	 */
	public static boolean forceLog;
	
	/**
	 * Constructs a new ServerProcessor which automatically starts and proceeds
	 * to run the tasks it has registered.
	 *
	 * @param server The server whom we process for.
			*/
	public ServerProcessor(GameServer server) {
		super("ServerProcessor"); // Set thread name
		
		this.server = server;
		this.world = server.world();
		taskExecutor = Executors.newWorkStealingPool();
		logicJobs = new ConcurrentLinkedQueue<>();
		
		// Add tasks
		tasks.add(new PacketProcessingTask());
		tasks.add(new ScriptProcessingTask());
		tasks.add(new PlayerProcessingTask());
		tasks.add(new NpcProcessingTask());
		
		// Sync map region
		tasks.add(new PlayerPreSyncTask());
		//tasks.add(new NpcPreSyncTask());
		tasks.add(new NpcViewportTask());
		tasks.add(new PlayerSyncTask());
		tasks.add(new NpcSyncTask());
		tasks.add(new PlayerPostSyncTask());
		tasks.add(new NpcPostSyncTask());
		
		tasks.add(new NetworkFlushTask());
		
		// Trigger world init (this is basically what we consider that yes)
		server.scriptRepository().triggerWorldInit(world);
		
		start();
	}
	
	public void submitLogic(Runnable r) {
		logicJobs.add(r);
	}
	
	@Override
	public void run() {
		while (running) {
			process();
		}
	}
	
	public static Object2IntArrayMap<Class> times = new Object2IntArrayMap<>();
	public static Object2LongArrayMap<String> computeTimes = new Object2LongArrayMap<>();
	
	private void process() {
		computeTimes.clear();
		long start = System.currentTimeMillis();
		
		// Execute logic jobs
		logicJobs.forEach((runnable) -> {
			try {
				runnable.run();
			} catch (Exception e) {
				logger.error("Error executing logic job!", e);
			}
		});
		logicJobs.clear();
		
		
		times.clear();
		for (Task t : tasks) {
			long l = System.currentTimeMillis();
			try {
				if (t.isAsyncSafe()) { // Is this job distributable across multiple workers?
					Collection<SubTask> jobs = t.createJobs(world);
					List<Future<Object>> futures = taskExecutor.invokeAll(jobs);
					
					while (futures.stream().anyMatch(f -> !f.isDone())) {
						Strand.sleep(1); // Sleep a small millisecond to go easy on the cpu.
					}
				} else { // Simple non-thread safe job that must execute all by itself.
					t.execute(world);
				}
			} catch (Throwable throwable) {
				logger.error("An exception occurred when executing " + t.getClass().getSimpleName() + ".", throwable);
			}
			times.put(t.getClass(), (int) (System.currentTimeMillis() - l));
		}
		
		long delay = 600 - (System.currentTimeMillis() - start);
		
		if (infotick-- == 0) {
			infotick = 10;
			
			long totalMem = Runtime.getRuntime().totalMemory();
			long freeMem = Runtime.getRuntime().freeMemory();
			long maxMem = Runtime.getRuntime().maxMemory();
			TrafficCounter traffic = server.initializer().trafficStats();
			if (traffic != null && server.isVerbose())
				logger.info("Cycle time: {}ms, players: {}, npcs: {}. Memory usage: {}MB/{}MB. Reserved: {}MB. Rx: {}KB/s, Tx: {}KB/s. Scripts: {}.",
						System.currentTimeMillis() - start, world.players().size(), world.npcs().size(), (totalMem - freeMem) / 1024 / 1024,
						totalMem / 1024 / 1024, maxMem / 1024 / 1024, traffic.lastReadThroughput() / 1024, traffic.lastWriteThroughput() / 1024,
						server.scriptExecutor().getScheduled().size());
		}
		
		if (forceLog) {
			logger.error(times);
			logger.error(computeTimes);
			logger.error(server.scriptRepository().getTimerProfiler());
			forceLog = false;
		}
		
		if (delay < 0) {
			logger.error("Server cannot keep up! Cycle overdue: {}ms.", -delay);
			logger.error(times);
			logger.error(computeTimes);
		} else {
			try {
				Strand.sleep(delay);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Stops the processor in a graceful way, interrupting nothing but only avoiding execution of a next 600ms-cycle.
	 */
	public void terminate() {
		running = false;
	}
	
}
