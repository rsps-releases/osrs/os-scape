package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.GroundItemAction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 8/23/2015.
 * <p>
 * Typically options like "light" on logs on the ground, or "lay" on a box trap (hunter)
 */

@PacketInfo(size = 7)
public class GroundItemAction2 implements Action {
	
	private int x;
	private int z;
	private int id;
	private boolean run;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		z = buf.readUShortA();
		id = buf.readUShortA();
		x = buf.readULEShort();
		run = buf.readByteA() == 1;
		
		log(player, opcode, size, "id=%d x=%d z=%d", id, x, z);
	}
	
	@Override
	public void process(Player player) {
		GroundItem item = player.world().getGroundItem(player, x, z, player.tile().level, id);
		player.debug("Gitem op2 - x: %d, z: %d, id: %d, my-h: %d", x, z, id, player.tile().level);
		
		if (!player.locked() || !player.dead()) {
			player.stopActions(true);
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, item);
			player.putattrib(AttributeKey.INTERACTION_OPTION, 2);
			player.world().server().scriptExecutor().executeLater(player, GroundItemAction.script);
		}
	}
}