package nl.bartpelle.veteres.task;

import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Npc;

import java.util.Collection;

/**
 * Created by Bart Pelle on 8/10/2015.
 */
public class NpcPreSyncTask implements Task {
	
	@Override
	public void execute(World world) {
		world.npcs().forEach(this::preUpdate);
	}
	
	private void preUpdate(Npc npc) {
		// bye
	}
	
	@Override
	public Collection<SubTask> createJobs(World world) {
		return null;
	}
	
	@Override
	public boolean isAsyncSafe() {
		return false;
	}
	
}
