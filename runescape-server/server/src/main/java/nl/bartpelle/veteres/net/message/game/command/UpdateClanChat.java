package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.net.message.game.Command;
import nl.bartpelle.veteres.util.UsernameUtilities;

/**
 * Created by Bart on 12/10/2015.
 */
public class UpdateClanChat extends Command {
	
	private ClanChat chat;
	private boolean updateMembers;
	
	public UpdateClanChat(ClanChat chat, boolean updateMembers) {
		this.chat = chat;
		this.updateMembers = updateMembers;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(128)).packet(64).writeSize(RSBuffer.SizeType.SHORT);
		
		if (chat != null) {
			buffer.writeString(chat.ownerName());
			buffer.writeLong(UsernameUtilities.encodeUsername(chat.name()));
			buffer.writeByte(chat.kickreq()); // ?? only used in cs2 at 1 instr 3616
			
			if (updateMembers) {
				buffer.writeByte(chat.members().size()); // Membercount
				
				int num = 0;
				for (Friend f : chat.members()) {
					buffer.writeString(f.name);
					buffer.writeShort(f.world);
					buffer.writeByte(f.rights == 2 ? 127 : f.clanrank); // 127 = admin
					buffer.writeString(f.prevname);
					num++;
					
					if (num == 100)
						break;
				}
			} else {
				buffer.writeByte(255);
			}
		}
		
		return buffer;
	}
	
}
