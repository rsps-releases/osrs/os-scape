package nl.bartpelle.veteres.util;

import com.google.common.base.Preconditions;

/**
 * Created by Jason MacKeigan on 2016-09-29 at 10:31 AM
 * <p>
 * A basic implementation of the {@link CycleParser} interface that allows us to easily convert cycles, that is
 * server game pulses that happen every 600ms, to some form of time.
 */
public class BasicCycleParser implements CycleParser {
	
	private final int cycles;
	
	private final int hours;
	
	private final int minutes;
	
	private final int seconds;
	
	public BasicCycleParser(int cycles) {
		Preconditions.checkArgument(cycles > 0, "Cycles argument must be greater than 0, it is %d.", cycles);
		this.cycles = cycles;
		this.seconds = (int) (cycles * .6);
		this.minutes = cycles % 100;
		this.hours = cycles % 60_000;
	}
	
	public final int getCycles() {
		return cycles;
	}
	
	/**
	 * It should be noted that this function returns an inaccurate result due to cycles being impossible to
	 * divide evenly into seconds.
	 *
	 * @return the cycles converted to seconds but you already knew that.
	 */
	@Override
	public int toSeconds() {
		return seconds;
	}
	
	@Override
	public int toMinutes() {
		return minutes;
	}
	
	@Override
	public int toHours() {
		return hours;
	}
}
