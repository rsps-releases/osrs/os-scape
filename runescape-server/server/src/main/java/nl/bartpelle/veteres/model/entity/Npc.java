package nl.bartpelle.veteres.model.entity;

import nl.bartpelle.veteres.content.areas.instances.PVPAreas;
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.combat.EntityCombat;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.items.equipment.BraceletOfEthereum;
import nl.bartpelle.veteres.content.mechanics.NpcDeath;
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext;
import nl.bartpelle.veteres.content.npcs.godwars.armadyl.Kreearra;
import nl.bartpelle.veteres.content.npcs.godwars.bandos.Graardor;
import nl.bartpelle.veteres.content.npcs.godwars.saradomin.Zilyana;
import nl.bartpelle.veteres.content.npcs.godwars.zamorak.Kril;
import nl.bartpelle.veteres.content.skills.hunter.Chinchompas;
import nl.bartpelle.veteres.fs.NpcDefinition;
import nl.bartpelle.veteres.model.*;
import nl.bartpelle.veteres.model.entity.npc.NpcCombatInfo;
import nl.bartpelle.veteres.model.entity.npc.NpcMovementSync;
import nl.bartpelle.veteres.model.entity.player.EquipSlot;
import nl.bartpelle.veteres.model.entity.player.NpcSyncInfo;
import nl.bartpelle.veteres.model.entity.player.PlayerDamageTracker;
import nl.bartpelle.veteres.model.instance.InstancedMap;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.map.steroids.RangeStepSupplier;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.util.Tuple;
import nl.bartpelle.veteres.util.Varbit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Bart on 8/10/2015.
 */
public class Npc extends Entity {
	
	private static final Logger logger = LogManager.getLogger(Entity.class);
	
	public static boolean TARG_SWITCH_ON = true;
	
	public String spawnStack = "";
	
	private int id;
	private final Tile spawnTile;
	private int walkRadius;
	private int spawnDirection;
	// If a player can see this npc. if not, what's the point in processing it?
	private boolean inViewport = true;
	private NpcDefinition def;
	private int hp;
	private NpcCombatInfo combatInfo;
	private boolean hidden;
	private boolean respawns = true;
	private boolean venomImmune;
	private boolean poisonImmune;
	private Area spawnArea;

	
	// A list of npc-ids such as Bosses that are immune to venom.
	public static final int[] immunes = new int[]{2668, 3127, 494, 2265, 2266, 2267, 7144, 7145, 7146, 7147, 7148, 7149, 6611, 6612, 2042, 2043, 2044, 2668};
	public static final int[] poiimmunes = new int[]{2668};
	
	public Npc(int id, World world, Tile tile) {
		super(world, tile);
		this.id = id;
		sync = new NpcSyncInfo(this);
		spawnTile = tile;
		def = world.definitions().get(NpcDefinition.class, id);
		combatInfo = world.combatInfo(id);
		hp = combatInfo == null ? 50 : combatInfo.stats.hitpoints;
		spawnArea = new Area(spawnTile, walkRadius);
		putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 12);
		
		for (int types : immunes) {
			if (id == types) {
				setVenomImmune(true);
			}
		}
		for (int types : poiimmunes) {
			if (id == types) {
				setPoisonImmune(true);
			}
		}
	}
	
	public void inViewport(boolean b) {
		inViewport = b;
	}
	
	public boolean inViewport() {
		return inViewport;
	}
	
	public void walkRadius(int r) {
		if (walkRadius != r) {
			spawnArea = new Area(spawnTile, r);
		}
		walkRadius = r;
	}
	
	public int walkRadius() {
		return walkRadius;
	}
	
	public Npc spawnDirection(int d) {
		spawnDirection = d;
		return this;
	}
	
	public int spawnDirection() {
		return spawnDirection;
	}
	
	public Tile spawnTile() {
		return spawnTile;
	}
	
	public int id() {
		return id;
	}
	
	public NpcSyncInfo sync() {
		return (NpcSyncInfo) sync;
	}
	
	@Override
	public void animate(int id) {
		// Attack sounds..
		if (attackAnimation() == id && combatInfo != null && combatInfo.sounds != null
				&& combatInfo.sounds.attack != null && combatInfo.sounds.attack.length > 0) {
			Entity target = EntityCombat.INSTANCE.getTarget(this);
			if (target != null && target instanceof Player) {
				((Player) target).sound(world.random(combatInfo.sounds.attack));
			}
		}
		
		super.animate(id);
	}
	
	public NpcDefinition def() {
		return def;
	}
	
	public void def(NpcDefinition d) {
		this.def = d;
	}
	
	public NpcCombatInfo combatInfo() {
		return combatInfo;
	}
	
	public void combatInfo(NpcCombatInfo info) {
		combatInfo = info;
	}
	
	public void hidden(boolean b) {
		hidden = b;
	}
	
	public boolean hidden() {
		return hidden;
	}
	
	public Npc respawns(boolean b) {
		respawns = b;
		return this;
	}
	
	public boolean respawns() {
		return respawns;
	}
	
	// Keep in mem instead of create every cycle
	static final Area[] rooms = new Area[]{Graardor.getBANDOS_AREA(), Zilyana.getENCAMPMENT(), Kril.getENCAMPMENT(), Kreearra.getENCAMPMENT()};
	
	@Override
	public void cycle() {
		
		// Then timers
		super.cycle();
		
		// Fire timers
		fire_timers();
		
		// Then scripts after we have checked damage that may kill us. Decided to actually do npc scripts in the same place as contextless scripts.. after the PacketTask Job.
		//world.server().scriptExecutor().cycle(Conditions.context(this));
		
		// After much research (06/04/2017) any damage appearing on an npc this cycle does NOT stop combat.
		// Notably, if an npc 0t kills a player, the player's combat only stops if another PID player also hits them (doesn't need to deal any damage)
		// otherwise the dead players attack will actually happen even though they're dead. I've got vid proof of this but replicating is pretty complex, decided
		// it wasn't worth investigating further.. no harm done.
		cycle_hits(true);
		cycle_hits(false);
		
		
		WeakReference<Entity> wrTarget = attrib(AttributeKey.TARGET);
		Entity target;
		// TODO May only do this when not in combat and not interacted with. That's going to be tricky :(.
		if (inViewport && walkRadius > 0 && !locked() && !frozen() && !timers.has(TimerKey.IN_COMBAT) && pathQueue.empty() && !(this.id() >= 3116 && this.id() <= 3128)) {
			if (!spawnArea.contains(tile) || world.random(9) == 0) {
				if (!world.performance()) { // When on critical performance mode, don't move randomly.
					if ((int) attribOr(AttributeKey.HUNTER_INFO, 0) == 0) { // More specifics checks. Don't walk when pathing towards a trap.
						//if (target != null)
						//	target.message("fucking cunt ");
						animate(-1);
						int rndX = world.random(walkRadius * 2 + 1) - walkRadius;
						int rndZ = world.random(walkRadius * 2 + 1) - walkRadius;
						walkTo(spawnTile.transform(rndX, rndZ, 0), PathQueue.StepType.REGULAR);
						
						// Make sure we don't walk too many tiles!
						if (spawnArea.contains(tile)) { // Trim outside ONLY if we're inside, otherwise walking back becomes broken
							pathQueue.trimToSize(4);
							pathQueue.removeOutside(spawnArea);
						}
					}
				}
			}
		}
		
		boolean inferno = id >= 7677 && id <= 7710;
		boolean roomBoss = PlayerCombat.gwdRoomNpc(this) || (def != null && def.name != null && (id >= 2042 && id <= 2044 || inferno));
		
		if (wrTarget != null && (target = wrTarget.get()) != null) {
			// Target is no longer valid. Clear it.
			// Interesting fact, the NPC will path to it's set target and only reset when in distance. So a melee NPC will get within distance
			// (1 tile) then reset combat if unavailable.
			
			// Now we need to be clever and check what sort of combat style this npc uses
			int dist = 1;
			if (combatInfo() != null && combatInfo().scripts != null && combatInfo().scripts.combat_ != null) { // This will have to do
				// Custom script could still be melee in which case it'll use the wrong distance.
				dist = 10;
			}
			RangeStepSupplier supplier = new RangeStepSupplier(this, target, dist);
			boolean reached = supplier.reached(world, target);
			// Fuck this is not a perfect solution! canAttack always worked but other circumstances such as area/distance checks need to be included too.
			if (!(this.id() >= 3116 && this.id() <= 3128) && this.id() != 5886 && id != 239 && !inferno) {
				
				if (!EntityCombat.targetOk(this, target, roomBoss ? 50 : 16) || !PlayerCombat.canAttack(this, target) && (reached || (dist == 1 && this.touches(target)))) {
					//target.message(def.name+" doesnt fucking like you hey?");
					this.clearattrib(AttributeKey.TARGET); // Clear it.
					this.sync().faceEntity(null); // Reset face
				}
			}
			
			//If our NPC is a reanimated monster, after 60 seconds remove it!
			boolean isReanimated = attribOr(AttributeKey.IS_REANIMATED_MONSTER, false);
			if (isReanimated) {
				if (!timers.has(TimerKey.REANIMATED_MONSTER_DESPAWN)) {
					target.clearattrib(AttributeKey.HAS_REANIMATED_MONSTER);
					world().unregisterNpc(this);
				}
			}
			
			// Changing target aggression. Gwd boss room minions.
			if (hp() > 0 && Npc.TARG_SWITCH_ON && !locked() && world.cycleCount() - (int) attribOr(AttributeKey.LAST_AGRO_SWITCH, 0) >= 2) {
				// Set anyway, don't check for 2t. Less lag xd
				putattrib(AttributeKey.LAST_AGRO_SWITCH, world.cycleCount());
				
				Entity lastagro = null;
				if (Graardor.isMinion(this) && Graardor.getBANDOS_AREA().contains(this)) {
					lastagro = Graardor.getLastBossDamager();
				} else if (Kril.isMinion(this) && Kril.getENCAMPMENT().contains(this)) {
					lastagro = Kril.getLastBossDamager();
				} else if (Zilyana.isMinion(this) && Zilyana.getENCAMPMENT().contains(this)) {
					lastagro = Zilyana.getLastBossDamager();
				} else if (Kreearra.isMinion(this) && Kreearra.getENCAMPMENT().contains(this)) {
					lastagro = Kreearra.getLastBossDamager();
				}
				if (lastagro != null && lastagro != target) { // Change target to a new one
					if (EntityCombat.targetOk(this, lastagro, 50) && PlayerCombat.canAttack(this, lastagro)) {
						//target.message("target changed to "+lastagro.tile().toStringSimple());
						putattrib(AttributeKey.TARGET, new WeakReference<>(lastagro));
						face(lastagro);
					}
				}
			}
		}
		
		// Aggression
		if (hp() > 0 && ((wrTarget == null || (target = wrTarget.get()) == null) || !timers.has(TimerKey.IN_COMBAT)) && !locked() && (inViewport || roomBoss) && !timers.has(TimerKey.COMBAT_ATTACK)) {
			boolean wilderness = (WildernessLevelIndicator.wildernessLevel(tile) >= 1 || PVPAreas.inPVPArea(this)) && !WildernessLevelIndicator.inside_rouges_castle(tile) && !Chinchompas.hunterNpc(id);

			if (combatInfo != null && (combatInfo.aggressive || wilderness)) {
				
				// Find players around us
				boolean[] done = {false};
				world.players().forEachInArea(bounds(combatInfo != null ? combatInfo.aggroradius : 1), p -> {
					RangeStepSupplier supplier = new RangeStepSupplier(this, p, 16);
					
					if (!done[0] && !p.looks().hidden() && (bounds().contains(p) || supplier.reached(world, p) || roomBoss)) {
						boolean mayAggress = false;
						
						// When in the wilderness, all monsters attack you. When not, it depends on your combat level.
						if (wilderness || p.skills().combatLevel() <= def.combatlevel * 2) {
							mayAggress = true;
						}
						
						// Some NPC's have custom scripts for determining if we attack or not. Example is Scorpia's offspring.
						if (combatInfo != null && combatInfo.scripts != null && combatInfo.scripts.aggression_ != null) {
							mayAggress = combatInfo.scripts.aggression_.invoke(p);
						}

						// Revenants aggression check
						Item bracelet = p.equipment().get(EquipSlot.HANDS);
						if (bracelet != null && bracelet.id() == BraceletOfEthereum.CHARGED) {
							mayAggress = false;
						}

						//Remove NPC aggression from guards in varrock PVP
						if(id == 3010 || id == 3011 && PVPAreas.inPVPArea(this)) {
							mayAggress = false;
						}

						// Since custom agro script doesn't support/give access to this agroing npc we can't check coords.
						// Hardcode time :(
						
						if (!PlayerCombat.bothInFixedRoom(this, p)) {
							mayAggress = false;
						}
						
						// if npc is still not agressive check if player is in an instanced map and if so, see if entities
						// are aggressive in this map. This is a fairly expensive operation, thus the mapAgress check.
						if (!mayAggress && combatInfo.aggressive) {
							Optional<InstancedMap> instancedMap = p.world().allocator().active(p.tile());
							mayAggress = instancedMap.isPresent() && instancedMap.get().areEntitiesAgressiveInArea();
						}
						if (mayAggress) {
							long lastTime = System.currentTimeMillis() - (long) p.attribOr(AttributeKey.LAST_WAS_ATTACKED_TIME, 0L);
							Entity lastAttacker = p.attrib(AttributeKey.LAST_DAMAGER);
							
							if (lastTime > 5000L || lastAttacker == this || (lastAttacker != null && (lastAttacker.dead() || lastAttacker.finished())) || p.varps().varbit(Varbit.MULTIWAY_AREA) == 1) {
								if (PlayerCombat.canAttack(this, p)) {
									
									//if (target != null)
									//	target.message("agroed "+p.name());
									attack(p);
									//String ss = this.def.name+" v "+p.name()+" : "+ PlayerCombat.canAttack(this, p);
									//System.out.println(ss);
									//this.sync().shout(ss);
									done[0] = true;
								}
							}
						}
					}
				});
				
			}
		}
		int maxDistanceFromSpawn = InfernoContext.isInfernoNpc(this) ? 200 : attribOr(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 12);
		
		// Prevent being too far from spawn - unless you're in a boss room. Free reign!
		if (spawnTile.distance(tile) > maxDistanceFromSpawn && walkRadius != -1) {
			stopActions(false);
			clearattrib(AttributeKey.TARGET); // Otherwise we'll forever be stuck with a target yo!
			//if (target != null)
			//	target.message("abandoned - out of range "+def.name);
		}
	}
	
	private void fire_timers() {
		try {
			timerloop:
			for (Iterator<nl.bartpelle.veteres.script.Timer> it = timers.timers().iterator(); it.hasNext(); ) {
				nl.bartpelle.veteres.script.Timer entry = it.next();
				if (entry != null && entry.ticks() < 1) {
					TimerKey key = entry.key();
					int oldTicks, attemptsLeft = 10; // We cap attempts to 10 to avoid bugs. Lower for npcs because support for this is brand new as of 16/8/16 - unsure of performance effects
					
					while ((oldTicks = entry.ticks()) <= 0 && attemptsLeft-- >= 0) {
						// Did it not get re-fired, or was the timer removed?
						boolean cont = false;
						if (entry == null || entry.ticks() == oldTicks || !timers.has(key)) {
							timers.cancel(key);
							cont = true;
						}
						
						world.server().scriptRepository().triggerTimer(this, key); // Fire event
						
						if (cont) {
							continue timerloop; // Stop firing more events.
						}
					}
					
					if (attemptsLeft < 0) {
						timers.cancel(key);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error processing timers for {}.", this, e);
		}
	}
	
	@Override
	public int hp() {
		return hp;
	}
	
	@Override
	public int maxHp() {
		return combatInfo == null ? 50 : combatInfo.stats.hitpoints;
	}
	
	@Override
	public void blockHit(Hit hit) {
		// Handle block animation
		if (!sync.hasFlag(NpcSyncInfo.Flag.ANIMATION.value)) {
			if (combatInfo != null) {
				if (combatInfo.animations != null) {
					if (combatInfo.animations.block > 0)
						animate(combatInfo.animations.block);
				}
			} else {
				super.blockHit(hit);
			}
		}
		
		// Handle block sound
		if (hit != null && hit.origin() instanceof Player && combatInfo != null && combatInfo.sounds != null
				&& combatInfo.sounds.block != null && combatInfo.sounds.block.length > 0) {
			((Player) hit.origin()).sound(world.random(combatInfo.sounds.block));
		}
	}
	
	@Override
	public Optional<Integer> killer() {
		// If we don't even have any killers, then return an empty optional.
		if (damagers().isEmpty())
			return Optional.empty();
		
		// Obtain a stream set of all information tracked.
		Set<Map.Entry<Integer, PlayerDamageTracker>> trackset = damagers().entrySet()
				.stream()
				.filter(info -> System.currentTimeMillis() - getDamagerLastTime().get(info.getKey()) <= 300000)
				.collect(Collectors.toSet());
		
		
		// Try to find the killer based on hitters
		Comparator<Map.Entry<Integer, PlayerDamageTracker>> comparator = (e1, e2) -> e2.getValue().damage().compareTo(e1.getValue().damage());
		
		// Identify the result
		Map.Entry<Integer, PlayerDamageTracker> result = trackset.stream().sorted(comparator).findFirst().orElse(null);
		
		// An anti-farming mechanic on W2 - checks if you ate or just suicided. Why suicide?
		int[] totalDamageTaken = new int[1];
		trackset.forEach(e -> totalDamageTaken[0] += e.getValue().damage());
		
		int account_id = result != null ? result.getKey() : -1;
		if (account_id != -1) {
			putattrib(AttributeKey.MOST_DAM_TRACKER, new Tuple<>(totalDamageTaken[0], result.getValue()));
			return Optional.ofNullable(account_id); // And find the highest damager :)
		} else {
			clearattrib(AttributeKey.MOST_DAM_TRACKER);
			return Optional.empty();
		}
	}
	
	@Override
	public void autoRetaliate(Entity attacker) {
		
		// If the bosses' current target has not attacked us back for at least 10, we change target to whoever attacked us last.
		if ((id == 2215 && target_fleeing(Graardor.getBANDOS_AREA(), attacker))
				|| (id == 3162 && target_fleeing(Kreearra.getENCAMPMENT(), attacker))
				|| (id == 2205 && target_fleeing(Zilyana.getENCAMPMENT(), attacker))
				|| (id == 3129 && target_fleeing(Kril.getENCAMPMENT(), attacker))
				|| (id == 7709)
				|| (id == 7710)
				|| (id == 7707)) {
			return;
		}
		if (combatInfo != null && !combatInfo.retaliates) {
			return;
		}
		super.autoRetaliate(attacker);
	}
	
	private boolean target_fleeing(Area room, Entity attacker) {
		WeakReference<Entity> wrTarget = attrib(AttributeKey.TARGET);
		Entity target;
		if (wrTarget != null && (target = wrTarget.get()) != null && room != null) {
			Map<Entity, Long> last_attacked_map = attribOr(AttributeKey.LAST_ATTACKED_MAP, new HashMap<>());
			List<Entity> invalid = new ArrayList<>();
			
			// Identify when our current focused target attacked us.
			long[] last_time = new long[1];
			
			// Identify invalid entries and our current targets last attack time
			if (last_attacked_map.size() > 0) {
				last_attacked_map.forEach((p, t) -> {
					if (target == p) // Our current target hasn't attacked for 10s. Fuck that guy, change!
						last_time[0] = t;
					if (!room.contains(p)) {
						invalid.add(p);
					}
					//System.out.println(p.index()+" vs "+target.index());
				});
			}
			// Remove invalid entries
			invalid.forEach(last_attacked_map::remove);
			invalid.clear();
			
			// 0L = never attacked in the first place. otherwise 10s check
			if (last_time[0] == 0L || System.currentTimeMillis() - last_time[0] >= 8000) {
				if (last_attacked_map.size() > 0) {
					// Retaliate to a random person who has recently attacked us in this room.
					super.autoRetaliate(last_attacked_map.keySet().toArray(new Entity[0])[world().random(last_attacked_map.size() - 1)]);
				} else {
					// Fall back to whoever actually hit us
					super.autoRetaliate(attacker);
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void hp(int hp, int exceed) {
		this.hp = Math.min(maxHp() + exceed, hp);
	}
	
	@Override
	public int size() {
		return def.size;
	}
	
	@Override
	public boolean isPlayer() {
		return false;
	}
	
	@Override
	public boolean isNpc() {
		return true;
	}
	
	@Override
	public boolean dead() {
		return hp == 0;
	}
	
	@Override
	protected void die() {
		world.server().scriptExecutor().executeScript(this, NpcDeath.script);
	}
	
	@Override
	public int attackAnimation() {
		if (combatInfo != null && combatInfo.animations != null) {
			return combatInfo.animations.attack;
		}
		
		return 422;
	}
	
	@Override
	public Area bounds() {
		return new Area(tile.x, tile.z, tile.x + size() - 1, tile.z + size() - 1);
	}
	
	public Area bounds(int enlargedBy) {
		return new Area(tile.x - enlargedBy, tile.z - enlargedBy, (tile.x + size() - 1) + enlargedBy, (tile.z + size() - 1) + enlargedBy);
	}
	
	@Override
	public Area pathbounds() {
		Tile t = pathQueue.peekLastTile();
		return new Area(t.x, t.z, t.x + size() - 1, t.z + size() - 1);
	}
	
	@Override
	public Area basebounds(Tile t) {
		return new Area(t.x, t.z, t.x + size() - 1, t.z + size() - 1);
	}
	
	@Override
	public void post_cycle_movement() {
		NpcMovementSync.npc_post_cycle_movement(this);
	}
	
	public boolean isVenomImmune() {
		return venomImmune;
	}
	
	public void setVenomImmune(boolean venomImmune) {
		this.venomImmune = venomImmune;
	}
	
	public boolean isPoisonImmune() {
		return poisonImmune;
	}
	
	public void setPoisonImmune(boolean poisonImmune) {
		this.poisonImmune = poisonImmune;
	}
	
	public void clone_damage(@NotNull Npc npc) {
		damagers = npc.damagers;
		damagerLastTime = npc.damagerLastTime;
	}
}
