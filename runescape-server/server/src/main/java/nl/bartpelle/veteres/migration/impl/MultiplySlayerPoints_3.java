package nl.bartpelle.veteres.migration.impl;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.GameMode;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.util.Varbit;

/**
 * Created by Bart on 3/10/2016.
 * <p>
 * Migration to make slayer points get multiplied appropriately since the change
 * that introduced increased points from slayer.
 */
public class MultiplySlayerPoints_3 implements Migration {
	
	@Override
	public boolean apply(Player player) {
		if (!player.world().realm().isPVP() && player.mode() != GameMode.LAID_BACK) {
			player.discardWrites(true); // Avoid crashing when sending the varp
			
			int oldPoints = player.varps().varbit(Varbit.SLAYER_POINTS);
			int newPoints = Math.min(0xFFFF, oldPoints * player.mode().slayerPointMultiplier());
			player.varps().varbit(Varbit.SLAYER_POINTS, newPoints);
			
			// Add an action to inform the player once they're through the login process
			player.pendingActions().add(new Action() {
				@Override
				public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
				}
				
				@Override
				public void process(Player player) {
					if (oldPoints != newPoints) {
						player.message("Your Slayer points have been increased from %d to %d as per a recent change.", oldPoints, newPoints);
					}
				}
			});
			
			// Re-enable writing messages
			player.discardWrites(false);
		}
		
		return true;
	}
	
	@Override
	public int id() {
		return 3;
	}
	
}
