package nl.bartpelle.veteres.services.logging;

import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.util.ItemsOnDeath;

/**
 * Created by Bart on 11/25/2015.
 */
public interface LoggingService extends Service {
	
	void logNpcKill(Integer characterId, int npc, int kills);
	
	void logPublicChat(Integer accountId, int world, int x, int z, int level, String message);
	
	void logPrivateMessage(Integer accountId, Integer toAccountId, int fromWorld, Tile tile, String message);
	
	void logTrade(Player trader1, Player trader2, ItemContainer offer1, ItemContainer offer2);
	
	void logDropTrade(Player player, GroundItem gitem);
	
	void logStake(Player winner, Player loser, ItemContainer winnerItems, ItemContainer loserItems);
	
	void logLogin(Player player);
	
	void logKill(Player killer, Player killed, long value, ItemsOnDeath.KillersLoot lootTable);
	
	void logPunishment(Player punisher, String account, String type);
	
	void logPacket(Player player, int id, int size, String desc);
	
	void logPvPHighscore(Player player);
	
	void logEcoHighscore(Player player);
	
	void logCommand(Integer accountId, int world, int x, int z, int level, String command, int privilege);
	
	void logLottery(Integer accountId, int world, String type, long amt, long previous_amt, String ip, int currency);
	
	void logExchange(Integer buyerId, Integer sellerId, int itemid, int amount, int cost_per, int serviceId);

	void logGrounditemExpired(GroundItem gitem, World world);
}
