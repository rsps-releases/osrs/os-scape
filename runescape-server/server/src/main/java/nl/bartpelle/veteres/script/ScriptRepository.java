package nl.bartpelle.veteres.script;


import it.unimi.dsi.fastutil.objects.Object2LongArrayMap;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import nl.bartpelle.skript.Script;
import nl.bartpelle.skript.ScriptExecutor;
import nl.bartpelle.skript.ScriptMain;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.content.items.RottenPotato;
import nl.bartpelle.veteres.model.*;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.map.MapObj;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by Bart on 7/9/2015.
 * <p>
 * The man of the triggers.
 */
public class ScriptRepository {
	
	private static final Logger logger = LogManager.getLogger(ScriptRepository.class);
	
	private List<Function1<Script, Unit>> loginTriggers = new LinkedList<>();
	private List<Function1<Script, Unit>> worldInitTriggers = new LinkedList<>();
	private Map<Integer, Function1<Script, Unit>> buttonTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> npcSpawnTriggers = new HashMap<>();
	private List<Function1<Script, Unit>> npcCreateTriggers = new LinkedList<>();
	private Map<Integer, Function1<Script, Unit>> objectTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> itemOnObjectTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> spellOnEntityTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> itemOnPlayer = new HashMap<>();
	private Map<Integer, Set<Function1<Script, Unit>>> regionEnterTriggers = new HashMap<>();
	private Map<Integer, Set<Function1<Script, Unit>>> regionExitTriggers = new HashMap<>();
	private Map<Integer, Set<Function1<Script, Unit>>> chunkEnterTriggers = new HashMap<>();
	private Map<Integer, Set<Function1<Script, Unit>>> chunkExitTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> interfaceCloseTriggers = new HashMap<>();
	private Map<TimerKey, Function1<Script, Unit>> timerTriggers = new EnumMap<>(TimerKey.class);
	private Map<TimerKey, Function1<Script, Unit>> worldTimerTriggers = new EnumMap<>(TimerKey.class);
	private Set<Integer> remoteInteractableObjects = new HashSet<>();
	
	private Map<Integer, Function1<Script, Unit>> item1Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> item2Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> item3Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> item4Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> item5Triggers = new HashMap<>();
	private Map<Long, Function1<Script, Unit>> itemOnItemTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> grounditem1Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> grounditem2Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> grounditem3Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> invitemOnGrounditem = new HashMap<>();
	
	private Map<Integer, Function1<Script, Unit>> npc1Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> npc2Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> npc3Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> npc4Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> itemOnNpcTriggers = new HashMap<>();
	
	private Map<Integer, Function1<Script, Unit>> itemEquipTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> itemUnequipTriggers = new HashMap<>();
	
	private Map<Integer, Function1<Script, Unit>> equipment1Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> equipment2Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> equipment3Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> equipment4Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> equipment5Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> equipment6Triggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> equipment7Triggers = new HashMap<>();
	
	private Map<Integer, Function1<Script, Unit>> spellOnItemTriggers = new HashMap<>();
	
	private List<Function1<Script, Unit>> player1Triggers = new LinkedList<>();
	private List<Function1<Script, Unit>> player2Triggers = new LinkedList<>();
	private List<Function1<Script, Unit>> player3Triggers = new LinkedList<>();
	private List<Function1<Script, Unit>> player4Triggers = new LinkedList<>();
	private List<Function1<Script, Unit>> player5Triggers = new LinkedList<>();
	private List<Function1<Script, Unit>> player6Triggers = new LinkedList<>();
	private List<Function1<Script, Unit>> player7Triggers = new LinkedList<>();
	private List<Function1<Script, Unit>> player8Triggers = new LinkedList<>();
	
	private Map<Integer, Function1<Script, Unit>> spellOnGroundItemTriggers = new HashMap<>();
	private Map<Integer, Function1<Script, Unit>> spellOnObjectTriggers = new HashMap<>();
	
	private Object2LongArrayMap<TimerKey> timerProfiler = new Object2LongArrayMap<>();
	
	private Map<Tile, Function1<Script, Unit>> walkDestinationTriggers = new HashMap<>();
	
	
	private ScriptExecutor executor;
	private GameServer server;
	
	public ScriptRepository(GameServer server, ScriptExecutor executor) {
		this.server = server;
		this.executor = executor;
	}
	
	public GameServer server() {
		return server;
	}
	
	public Object2LongArrayMap<TimerKey> getTimerProfiler() {
		return timerProfiler;
	}
	
	public void load() {
		// Clear all existing script containers
		loginTriggers.clear();
		worldInitTriggers.clear();
		buttonTriggers.clear();
		npcSpawnTriggers.clear();
		npcCreateTriggers.clear();
		objectTriggers.clear();
		itemOnObjectTriggers.clear();
		spellOnEntityTriggers.clear();
		itemOnPlayer.clear();
		regionEnterTriggers.clear();
		regionExitTriggers.clear();
		chunkEnterTriggers.clear();
		chunkExitTriggers.clear();
		interfaceCloseTriggers.clear();
		timerTriggers.clear();
		worldTimerTriggers.clear();
		npc1Triggers.clear();
		npc2Triggers.clear();
		npc3Triggers.clear();
		npc4Triggers.clear();
		player1Triggers.clear();
		player2Triggers.clear();
		player3Triggers.clear();
		player4Triggers.clear();
		player5Triggers.clear();
		player6Triggers.clear();
		player7Triggers.clear();
		player8Triggers.clear();
		itemOnNpcTriggers.clear();
		item1Triggers.clear();
		item2Triggers.clear();
		item3Triggers.clear();
		item4Triggers.clear();
		item5Triggers.clear();
		itemOnItemTriggers.clear();
		grounditem1Triggers.clear();
		grounditem2Triggers.clear();
		grounditem3Triggers.clear();
		invitemOnGrounditem.clear();
		equipment1Triggers.clear();
		equipment2Triggers.clear();
		equipment3Triggers.clear();
		equipment4Triggers.clear();
		equipment5Triggers.clear();
		equipment6Triggers.clear();
		equipment7Triggers.clear();
		spellOnItemTriggers.clear();
		remoteInteractableObjects.clear();
		spellOnGroundItemTriggers.clear();
		spellOnObjectTriggers.clear();
		walkDestinationTriggers.clear();
		Set<Method> methods = new Reflections("nl.bartpelle.veteres.content", new SubTypesScanner(false), new MethodAnnotationsScanner()).getMethodsAnnotatedWith(ScriptMain.class);
		methods.forEach(m -> {
			if (!m.getDeclaringClass().getName().contains("$") && !m.getDeclaringClass().getName().endsWith("Package")) {
				try {
					m.invoke(null, this);
				} catch (Exception e) {
					logger.error("Error loading script {}. Could not invoke method.", m.getDeclaringClass(), e);
				}
			}
		});
	}
	
	public ScriptExecutor executor() {
		return executor;
	}
	
	private <K extends Tile, V extends Function1<Script, Unit>> void checkForExisting(Map<K, V> triggers, K tile) {
		if (triggers.containsKey(tile)) {
			logger.log(Level.FATAL, "trigger for mapping already exists: " + tile.toStringSimple());
			System.exit(0);
		}
	}
	
	public void onWalkDestination(Tile tile, Function1<Script, Unit> script) {
		checkForExisting(walkDestinationTriggers, tile);
		walkDestinationTriggers.put(tile, script);
	}
	
	public void onLogin(Function1<Script, Unit> script) {
		loginTriggers.add(script);
	}
	
	public void onWorldInit(Function1<Script, Unit> script) {
		worldInitTriggers.add(script);
	}
	
	public void onButton(int parentWidgetId, int widgetId, Function1<Script, Unit> script) {
		if (buttonTriggers.containsKey((parentWidgetId << 16) | widgetId)) {
			logger.log(Level.WARN, "WARNING: duplicate 'on button' trigger - " + parentWidgetId +", "+ widgetId);
		}
		buttonTriggers.put((parentWidgetId << 16) | widgetId, script);
	}
	
	public void onSpellOnItem(int parent, int childId, Function1<Script, Unit> script) {
		spellOnItemTriggers.put((parent << 16) | childId, script);
	}
	
	public boolean isRemoteObject(int id) {
		return remoteInteractableObjects.contains(id);
	}
	
	public void makeRemoteObject(int id) {
		remoteInteractableObjects.add(id);
	}
	
	public void onObject(int id, Function1<Script, Unit> script) {
		if (objectTriggers.containsKey(id)) {
			logger.log(Level.WARN, "WARNING: duplicate 'on object' trigger - " + id);
		}
		objectTriggers.put(id, script);
	}
	
	public void onItemOnObject(int objectId, Function1<Script, Unit> script) {
		if (itemOnObjectTriggers.containsKey(objectId)) {
			System.out.println("WARNING: duplicate 'item on object' trigger - " + objectId);
		}
		itemOnObjectTriggers.put(objectId, script);
	}
	
	public void onSpellOnEntity(int parentId, int childId, Function1<Script, Unit> script) {
		if (spellOnEntityTriggers.containsKey(((parentId << 16) | childId))) {
			System.out.println("WARNING: duplicate 'spell on entity' trigger - " + ((parentId << 16) | childId));
		}
		spellOnEntityTriggers.put((parentId << 16) | childId, script);
	}
	
	public void onItemOnPlayer(int item, Function1<Script, Unit> script) {
		if (itemOnPlayer.containsKey(item)) {
			System.out.println("WARNING: duplicate 'item on player' trigger - " + item);
		}
		itemOnPlayer.put(item, script);
	}
	
	public void onNpcSpawn(int npcType, Function1<Script, Unit> script) {
		if (npcSpawnTriggers.containsKey(npcType)) {
			System.out.println("WARNING: duplicate 'on npc spawn' trigger - " + npcType);
		}
		npcSpawnTriggers.put(npcType, script);
	}
	
	public void onNpcCreate(Function1<Script, Unit> script) {
		npcCreateTriggers.add(script);
	}
	
	public void onRegionEnter(int region, Function1<Script, Unit> script) {
		regionEnterTriggers.compute(region, (k, v) -> {
			if (v == null)
				v = new HashSet<>();
			v.add(script);
			return v;
		});
	}
	
	public void onRegionExit(int region, Function1<Script, Unit> script) {
		regionExitTriggers.compute(region, (k, v) -> {
			if (v == null)
				v = new HashSet<>();
			v.add(script);
			return v;
		});
	}
	
	public void onChunkEnter(int region, Function1<Script, Unit> script) {
		chunkEnterTriggers.compute(region, (k, v) -> {
			if (v == null)
				v = new HashSet<>();
			v.add(script);
			return v;
		});
	}
	
	public void onChunkExit(int region, Function1<Script, Unit> script) {
		chunkExitTriggers.compute(region, (k, v) -> {
			if (v == null)
				v = new HashSet<>();
			v.add(script);
			return v;
		});
	}
	
	public void onInterfaceClose(int id, Function1<Script, Unit> script) {
		if (interfaceCloseTriggers.containsKey(id)) {
			System.out.println("WARNING: duplicate 'interface close' trigger - " + id);
		}
		interfaceCloseTriggers.put(id, script);
	}
	
	public void onItemOption1(int item, Function1<Script, Unit> script) {
		item1Triggers.put(item, script);
	}
	
	public void onItemOption2(int item, Function1<Script, Unit> script) {
		item2Triggers.put(item, script);
	}
	
	public void onGroundItemOption1(int item, Function1<Script, Unit> script) {
		grounditem1Triggers.put(item, script);
	}
	
	public void onGroundItemOption2(int item, Function1<Script, Unit> script) {
		grounditem2Triggers.put(item, script);
	}
	
	public void onGroundItemOption3(int item, Function1<Script, Unit> script) {
		grounditem3Triggers.put(item, script);
	}
	
	public void onInvitemOnGrounditem(int item, Function1<Script, Unit> script) {
		invitemOnGrounditem.put(item, script);
	}
	
	public void onNpcOption1(int npc, Function1<Script, Unit> script) {
		npc1Triggers.put(npc, script);
	}
	
	public void onItemOnNpc(int npc, Function1<Script, Unit> script) {
		itemOnNpcTriggers.put(npc, script);
	}
	
	public void onNpcOption2(int npcId, Function1<Script, Unit> script) {
		npc2Triggers.put(npcId, script);
	}
	
	public void onNpcOption3(int npcId, Function1<Script, Unit> script) {
		npc3Triggers.put(npcId, script);
	}
	
	public void onNpcOption4(int npcId, Function1<Script, Unit> script) {
		npc4Triggers.put(npcId, script);
	}
	
	public void onItemOption4(int item, Function1<Script, Unit> script) {
		item4Triggers.put(item, script);
	}
	
	public void onItemOption5(int item, Function1<Script, Unit> script) {
		item5Triggers.put(item, script);
	}
	
	public void onItemOption3(int item, Function1<Script, Unit> script) {
		item3Triggers.put(item, script);
	}
	
	public void onItemOnItem(long item1, long item2, Function1<Script, Unit> script) {
		itemOnItemTriggers.put((Math.max(item1, item2) << 32L) | Math.min(item1, item2), script);
	}
	
	public void onItemOnItem(int item1, int item2, Function1<Script, Unit> script) {
		onItemOnItem((long) item1, (long) item2, script);
	}
	
	public void onEquipmentOption(int opt, int item, Function1<Script, Unit> script) {
		switch (opt) {
			case 1:
				equipment1Triggers.put(item, script);
				break;
			case 2:
				equipment2Triggers.put(item, script);
				break;
			case 3:
				equipment3Triggers.put(item, script);
				break;
			case 4:
				equipment4Triggers.put(item, script);
				break;
			case 5:
				equipment5Triggers.put(item, script);
				break;
			case 6:
				equipment6Triggers.put(item, script);
				break;
			case 7:
				equipment7Triggers.put(item, script);
				break;
		}
	}
	
	public void triggerWalkDestination(Player player) {
		if (walkDestinationTriggers.containsKey(player.tile())) {
			executor.executeScript(player, walkDestinationTriggers.get(player.tile()));
		}
	}
	
	public void onTimer(TimerKey timer, Function1<Script, Unit> script) {
		timerTriggers.put(timer, script);
	}
	
	public void onWorldTimer(TimerKey timer, Function1<Script, Unit> script) {
		worldTimerTriggers.put(timer, script);
	}
	
	public void triggerLogin(Player player) {
		loginTriggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void triggerWorldInit(World world) {
		worldInitTriggers.forEach(t -> executor.executeScript(world, t));
	}
	
	public void triggerButton(Player player, int parentId, int childId, int slot, int action, int item) {
		player.putattrib(AttributeKey.BUTTON_SLOT, slot);
		player.putattrib(AttributeKey.BUTTON_ACTION, action);
		player.putattrib(AttributeKey.ITEM_ID, item);
		player.putattrib(AttributeKey.CHILD_ID, childId);
		if (buttonTriggers.containsKey((parentId << 16) | childId)) {
			executor.executeScript(player, buttonTriggers.get((parentId << 16) | childId));
		}
	}
	
	public void triggerSpellOnItem(Player player, int parent, int child, int slot) {
		player.putattrib(AttributeKey.BUTTON_SLOT, slot);
		if (spellOnItemTriggers.containsKey((parent << 16) | child)) {
			executor.executeScript(player, spellOnItemTriggers.get((parent << 16) | child));
		}
	}
	
	public void triggerNpcSpawn(Npc npc) {
		if (npcSpawnTriggers.containsKey(npc.id())) {
			executor.executeScript(npc, npcSpawnTriggers.get(npc.id()));
		}
	}
	
	public void triggerNpcCreate(Npc npc) {
		npcCreateTriggers.forEach(n -> executor.executeScript(npc, n));
	}
	
	public boolean triggerObject(Player player, MapObj obj, int action) {
		player.putattrib(AttributeKey.INTERACTION_OBJECT, obj);
		player.putattrib(AttributeKey.INTERACTION_OPTION, action);
		if (objectTriggers.containsKey(obj.id())) {
			executor.executeScript(player, objectTriggers.get(obj.id()));
			return true;
		} else {
			return false;
		}
	}
	
	public boolean triggerItemOnObject(Player player, MapObj obj) {
		if (itemOnObjectTriggers.containsKey(obj.id())) {
			executor.executeScript(player, itemOnObjectTriggers.get(obj.id()));
			return true;
		} else {
			return false;
		}
	}
	
	public void triggerSpellOnEntity(Player player, int i, int c) {
		if (spellOnEntityTriggers.containsKey((i << 16) | c)) {
			executor.executeScript(player, spellOnEntityTriggers.get((i << 16) | c));
		}
	}
	
	public void triggerItemOnPlayer(Player player, int item) {
		if (itemOnPlayer.containsKey(item)) {
			executor.executeScript(player, itemOnPlayer.get(item));
		}
	}
	
	public void triggerRegionEnter(Player player, int id) {
		if (regionEnterTriggers.containsKey(id)) {
			for (Function1<Script, Unit> s : regionEnterTriggers.get(id)) {
				executor.executeScript(player, s);
			}
		}
	}
	
	public void triggerRegionExit(Player player, int id) {
		if (regionExitTriggers.containsKey(id)) {
			for (Function1<Script, Unit> s : regionExitTriggers.get(id)) {
				executor.executeScript(player, s);
			}
		}
	}
	
	public void triggerChunkEnter(Player player, int id) {
		if (chunkEnterTriggers.containsKey(id)) {
			for (Function1<Script, Unit> s : chunkEnterTriggers.get(id)) {
				executor.executeScript(player, s);
			}
		}
	}
	
	public void triggerChunkExit(Player player, int id) {
		if (chunkExitTriggers.containsKey(id)) {
			for (Function1<Script, Unit> s : chunkExitTriggers.get(id)) {
				executor.executeScript(player, s);
			}
		}
	}
	
	public void triggerInterfaceClose(Player player, int id) {
		if (interfaceCloseTriggers.containsKey(id)) {
			executor.executeScript(player, interfaceCloseTriggers.get(id));
		}
	}
	
	public void triggerTimer(Entity entity, TimerKey timer) {
		long start = System.nanoTime();
		if (timerTriggers.containsKey(timer)) {
			executor.executeScript(entity, timerTriggers.get(timer));
		}
		
		long delta = System.nanoTime() - start;
		timerProfiler.put(timer, timerProfiler.getOrDefault(timer, 0L) + delta);
	}
	
	public void triggerWorldTimer(World world, TimerKey timer) {
		long start = System.nanoTime();
		if (worldTimerTriggers.containsKey(timer)) {
			executor.executeScript(world, worldTimerTriggers.get(timer));
		}
		
		long delta = System.nanoTime() - start;
		timerProfiler.put(timer, timerProfiler.getOrDefault(timer, 0L) + delta);
	}
	
	public boolean triggerNpcOption1(Player player, Npc npc) {
		if (npc1Triggers.containsKey(npc.id())) {
			if (!npc.locked()) player.world().server().scriptExecutor().interruptFor(npc);
			player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(npc));
			executor.executeScript(player, npc1Triggers.get(npc.id()));
			return true;
		}
		return false;
	}
	
	public boolean triggerNpcOption2(Player player, Npc npc) {
		if (npc2Triggers.containsKey(npc.id())) {
			if (!npc.locked()) player.world().server().scriptExecutor().interruptFor(npc);
			player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(npc));
			executor.executeScript(player, npc2Triggers.get(npc.id()));
			return true;
		}
		return false;
	}
	
	public boolean triggerNpcOption3(Player player, Npc npc) {
		if (npc3Triggers.containsKey(npc.id())) {
			if (!npc.locked()) player.world().server().scriptExecutor().interruptFor(npc);
			player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(npc));
			executor.executeScript(player, npc3Triggers.get(npc.id()));
			return true;
		}
		return false;
	}
	
	public boolean triggerNpcOption4(Player player, Npc npc) {
		if (npc4Triggers.containsKey(npc.id())) {
			if (!npc.locked()) player.world().server().scriptExecutor().interruptFor(npc);
			player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(npc));
			executor.executeScript(player, npc4Triggers.get(npc.id()));
			return true;
		}
		return false;
	}
	
	public boolean triggerItemOnNpc(Player player, Npc npc) {
		// Rotten potato applies to all NPCs.
		Integer itemId = player.attrib(AttributeKey.ITEM_ID);
		if (player.privilege().eligibleTo(Privilege.ADMIN) && itemId != null && itemId == RottenPotato.ROTTEN_POTATO) {
			player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(npc));
			executor.executeScript(player, RottenPotato.potatoOnNpc);
			return true;
		}
		
		if (itemOnNpcTriggers.containsKey(npc.id())) {
			if (!npc.locked()) player.world().server().scriptExecutor().interruptFor(npc);
			player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(npc));
			executor.executeScript(player, itemOnNpcTriggers.get(npc.id()));
			return true;
		}
		return false;
	}
	
	public void triggerItemOption1(Player player, int item, int slot) {
		if (item1Triggers.containsKey(item)) {
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			player.putattrib(AttributeKey.FROM_ITEM, player.inventory().get(slot));
			player.putattrib(AttributeKey.ITEM_ID, player.inventory().get(slot).id());
			executor.executeScript(player, item1Triggers.get(item));
		}
	}
	
	public void triggerItemOption2(Player player, int item, int slot) {
		if (item2Triggers.containsKey(item)) {
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			player.putattrib(AttributeKey.FROM_ITEM, player.inventory().get(slot));
			player.putattrib(AttributeKey.ITEM_ID, player.inventory().get(slot).id());
			executor.executeScript(player, item2Triggers.get(item));
		}
	}
	
	public void triggerItemOption3(Player player, int item, int slot) {
		if (item3Triggers.containsKey(item)) {
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			player.putattrib(AttributeKey.FROM_ITEM, player.inventory().get(slot));
			player.putattrib(AttributeKey.ITEM_ID, player.inventory().get(slot).id());
			executor.executeScript(player, item3Triggers.get(item));
		}
	}
	
	public void triggerItemOption4(Player player, int item, int slot) {
		if (item4Triggers.containsKey(item)) {
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			player.putattrib(AttributeKey.FROM_ITEM, player.inventory().get(slot));
			player.putattrib(AttributeKey.ITEM_ID, player.inventory().get(slot).id());
			executor.executeScript(player, item4Triggers.get(item));
		}
	}
	
	public boolean triggerItemOption5(Player player, int item, int slot, int hash) { // Boolean since dropping is the default action.
		if (item5Triggers.containsKey(item)) {
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			if (hash >> 16 == 149) {
				player.putattrib(AttributeKey.FROM_ITEM, player.inventory().get(slot));
				player.putattrib(AttributeKey.ITEM_ID, player.inventory().get(slot).id());
			}
			player.putattrib(AttributeKey.CHILD_ID, hash & 0xffff);
			player.putattrib(AttributeKey.INTERACTED_WIDGET_INFO, hash);
			executor.executeScript(player, item5Triggers.get(item));
			return true;
		}
		return false;
	}
	
	public void triggerItemOnItemOption(Player player, Item from, Item to, int fromSlot, int toSlot) {
		long hash = ((long) Math.max(from.id(), to.id()) << 32L) | (long) Math.min(from.id(), to.id());
		long hash2 = ((long) Integer.MAX_VALUE << 32L) | from.id();
		long hash3 = ((long) Integer.MAX_VALUE << 32L) | to.id();
		if (itemOnItemTriggers.containsKey(hash)
				|| itemOnItemTriggers.containsKey(hash2)
				|| itemOnItemTriggers.containsKey(hash3)) {
			player.putattrib(AttributeKey.ITEM_SLOT, fromSlot);
			player.putattrib(AttributeKey.ALT_ITEM_SLOT, toSlot);
			player.putattrib(AttributeKey.FROM_ITEM, from);
			player.putattrib(AttributeKey.TO_ITEM, to);
			player.putattrib(AttributeKey.ITEM_ID, from.id());
			player.putattrib(AttributeKey.ALT_ITEM_ID, to.id());
			
			if (itemOnItemTriggers.containsKey(hash)) {
				executor.executeScript(player, itemOnItemTriggers.get(hash));
			} else if (itemOnItemTriggers.containsKey(hash2)) {
				executor.executeScript(player, itemOnItemTriggers.get(hash2));
			} else if (itemOnItemTriggers.containsKey(hash3)) {
				executor.executeScript(player, itemOnItemTriggers.get(hash3));
			}
		} else {
			player.message("Nothing interesting happens.");
		}
	}
	
	public void triggerGroundItemOption1(Player player, int item, GroundItem groundItem) {
		if (grounditem1Triggers.containsKey(item)) {
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, groundItem);
			executor.executeScript(player, grounditem1Triggers.get(item));
		}
	}
	
	public void triggerGroundItemOption2(Player player, GroundItem groundItem) {
		if (grounditem2Triggers.containsKey(groundItem.item().id())) {
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, groundItem);
			executor.executeScript(player, grounditem2Triggers.get(groundItem.item().id()));
		}
	}
	
	public void triggerGroundItemOption3(Player player, int item, GroundItem groundItem) {
		if (grounditem3Triggers.containsKey(item)) {
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, groundItem);
			executor.executeScript(player, grounditem3Triggers.get(item));
		}
	}
	
	public void triggerInvItemOnGroundItem(Player player, GroundItem groundItem) {
		int id = player.inventory().get(player.attrib(AttributeKey.ITEM_SLOT)).id();
		if (invitemOnGrounditem.containsKey(id)) {
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, groundItem);
			executor.executeScript(player, invitemOnGrounditem.get(id));
		}
	}
	
	public void triggerEquipmentOption(Player player, int item, int slot, int opt) {
		Function1<Script, Unit> fnc = null;
		switch (opt) {
			case 1:
				fnc = equipment1Triggers.get(item);
				break;
			case 2:
				fnc = equipment2Triggers.get(item);
				break;
			case 3:
				fnc = equipment3Triggers.get(item);
				break;
			case 4:
				fnc = equipment4Triggers.get(item);
				break;
			case 5:
				fnc = equipment5Triggers.get(item);
				break;
			case 6:
				fnc = equipment6Triggers.get(item);
				break;
			case 7:
				fnc = equipment7Triggers.get(item);
				break;
		}
		
		if (fnc != null) {
			player.putattrib(AttributeKey.FROM_ITEM, player.equipment().get(slot));
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			executor.executeScript(player, fnc);
		}
	}
	
	public void onPlayerOption1(Function1<Script, Unit> script) {
		player1Triggers.add(script);
	}
	
	public void triggerPlayerOption1(Player player, Player other) {
		player1Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void onPlayerOption2(Function1<Script, Unit> script) {
		player2Triggers.add(script);
	}
	
	public void triggerPlayerOption2(Player player, Player other) {
		player2Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void onPlayerOption3(Function1<Script, Unit> script) {
		player3Triggers.add(script);
	}
	
	public void triggerPlayerOption3(Player player, Player other) {
		player3Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void onPlayerOption4(Function1<Script, Unit> script) {
		player4Triggers.add(script);
	}
	
	public void triggerPlayerOption4(Player player, Player other) {
		player4Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void onPlayerOption5(Function1<Script, Unit> script) {
		player5Triggers.add(script);
	}
	
	public void triggerPlayerOption5(Player player, Player other) {
		player5Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void onPlayerOption6(Function1<Script, Unit> script) {
		player6Triggers.add(script);
	}
	
	public void triggerPlayerOption6(Player player, Player other) {
		player6Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void onPlayerOption7(Function1<Script, Unit> script) {
		player7Triggers.add(script);
	}
	
	public void triggerPlayerOption7(Player player, Player other) {
		player7Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	public void onPlayerOption8(Function1<Script, Unit> script) {
		player8Triggers.add(script);
	}
	
	public void triggerPlayerOption8(Player player, Player other) {
		player8Triggers.forEach(t -> executor.executeScript(player, t));
	}
	
	
	public void onSpellOnGroundItem(int parent, int childId, Function1<Script, Unit> script) {
		spellOnGroundItemTriggers.put((parent << 16) | childId, script);
	}
	
	public void triggerSpellOnGroundItem(Player player, int parent, int child, GroundItem gitem) {
		player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, gitem);
		if (spellOnGroundItemTriggers.containsKey((parent << 16) | child)) {
			executor.executeScript(player, spellOnGroundItemTriggers.get((parent << 16) | child));
		}
	}
	
	public void onItemEquip(int item, Function1<Script, Unit> script) {
		itemEquipTriggers.put(item, script);
	}
	
	public void triggerItemEquip(Player player, int item) {
		if (itemEquipTriggers.containsKey(item)) {
			executor.executeScript(player, itemEquipTriggers.get(item));
		}
	}
	
	public void onItemUnequip(int item, Function1<Script, Unit> script) {
		itemUnequipTriggers.put(item, script);
	}
	
	public void triggerItemUnequip(Player player, int item) {
		if (itemUnequipTriggers.containsKey(item)) {
			executor.executeScript(player, itemUnequipTriggers.get(item));
		}
	}
	
	public void onSpellOnObject(int parent, int childId, Function1<Script, Unit> script) {
		spellOnObjectTriggers.put((parent << 16) | childId, script);
	}
	
	public void triggerSpellOnObject(Player player, int parent, int child, MapObj obj) {
		player.putattrib(AttributeKey.INTERACTION_OBJECT, obj);
		if (spellOnObjectTriggers.containsKey((parent << 16) | child)) {
			executor.executeScript(player, spellOnObjectTriggers.get((parent << 16) | child));
		}
	}

}
