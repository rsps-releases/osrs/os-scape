package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.ObjectInteraction;
import nl.bartpelle.veteres.fs.ObjectDefinition;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.map.MapObj;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 8/23/2015.
 */

@PacketInfo(size = 7)
public class ObjectAction5 implements Action {
	
	private int id;
	private int x;
	private int z;
	private boolean run;
	
	private int opcode;
	private int size;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		run = buf.readByteA() == 1;
		z = buf.readULEShortA();
		id = buf.readUShort();
		x = buf.readUShortA();
		
		this.opcode = opcode;
		this.size = size;
	}
	
	@Override
	public void process(Player player) {
		MapObj obj = player.world().objById(id, x, z, player.tile().level);
		
		if (obj == null)
			return;
		
		MapObj original = player.world().objByType(obj.type(), x, z, player.tile().level);
		if (original == null || original.id() != obj.id())
			return;
		
		MapObj over = new MapObj(new Tile(x, z, player.tile().level), original.id(), original.type(), original.rot()).cloneattribs(original);
		log(player, opcode, size, "id=%d x=%d z=%d", over.id(), over.tile().x, over.tile().z);
		
		if (player.attribOr(AttributeKey.DEBUG, false)) {
			ObjectDefinition objdef = player.world().definitions().get(ObjectDefinition.class, id);
			player.message("Click:%d id=%d at %d,%d,%d rot:%d vb:%d, v:%d lx=%d lz=%d", 5, id, x, z, over.tile().level, over.rot(), objdef.varbit, objdef.varp, x & 63, z & 63);
		}
		
		if (!player.locked() && !player.dead()) {
			player.stopActions(true);
			player.putattrib(AttributeKey.ORIGINAL_INTERACTION_OBJECT, original);
			player.putattrib(AttributeKey.INTERACTION_OBJECT, over);
			player.putattrib(AttributeKey.INTERACTION_OPTION, 5);
			player.world().server().scriptExecutor().executeLater(player, ObjectInteraction.script);
		}
	}
	
}
