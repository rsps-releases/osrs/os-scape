package nl.bartpelle.veteres.net.message.game.action;

import droptables.ScalarLootTable;
import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.fs.NpcDefinition;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.net.message.game.command.DropTableViewer;
import nl.bartpelle.veteres.util.BloodMoney;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Situations on 12/7/2015.
 */
@PacketInfo(size = 2)
public class ExamineNpc implements Action {
	
	private int npcId;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		npcId = buf.readULEShort();
	}
	
	@Override
	public void process(Player player) {
		NpcDefinition def = player.world().definitions().get(NpcDefinition.class, npcId);
		ScalarLootTable dropTable = ScalarLootTable.forNPC(npcId);
		if(dropTable == null) {
			player.message("<img=108>" + Color.DARK_GREEN.tag() + "Bestiary: " + Color.OLIVE.tag() + def.name + " has no drops.");
			return;
		}
		int petId, petAverage;

		petId = dropTable.petItem == 0 ? -1 : dropTable.petItem;
		petAverage = dropTable.petRarity;

		List<Integer[]> drops = new ArrayList<>();
		double totalTablesWeight = dropTable.ptsTotal();
		if(dropTable.guaranteed != null) {
			for(ScalarLootTable.TableItem item : dropTable.guaranteed) {
				Integer[] drop = new Integer[5];
				drop[0] = item.id;
				drop[1] = BloodMoney.CATALOG.containsKey(item.id) ? 2 : -1;
				drop[2] = item.min;
				drop[3] = item.max;
				drop[4] = 1; //average 1/1
				drops.add(drop);
			}
		}
		if(dropTable.tables != null) {
			for(ScalarLootTable table : dropTable.tables) {
				if(table != null) {
					double tableChance = table.points / totalTablesWeight;
					if(table.items.length == 0) {
						//Nothing!
						//nothingPercentage = tableChance * 100D;
					} else {
						for(ScalarLootTable.TableItem item : table.items) {
							Integer[] drop = new Integer[5];
							drop[0] = item.id;
							drop[1] = BloodMoney.CATALOG.containsKey(item.id) ? 2 : -1;
							drop[2] = item.min;
							drop[3] = item.max;
							if(item.points == 0)
								drop[4] = (int) (1D / tableChance);
							else
								drop[4] = (int) (1D / (item.computedFraction.doubleValue()));

							drops.add(drop);
						}
					}
				}
			}
		}
		//todo - some how generate this string in the constructor! ^^^ :)
		player.interfaces().sendMain(383);
		player.write(new DropTableViewer(def.name, petId, petAverage, drops));

		//if (player.attribOr(AttributeKey.DEBUG, false))
		//	player.message(player.world().examineRepository().npc(npcId) + ", walkAnim=" + player.world().definitions().get(NpcDefinition.class, npcId).idleAnimation + " (" + npcId + ") ");
		//else
		//	player.message(player.world().examineRepository().npc(npcId));

	}
	
	public String color(Double fraction) {
		if (fraction < 0.01)
			return "<col=990000>";
		if (fraction < 0.02)
			return "<col=cc5200>";
		if (fraction < 0.03)
			return "<col=e6e600>";
		
		return "<col=008000>";
	}
	
	public String showAmount(Integer min, Integer max, Integer amt) {
		if (min > 0)
			return "" + min + "-" + max;
		if (amt > 1)
			return "x" + amt;
		return "";
	}
}
