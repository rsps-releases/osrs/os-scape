package nl.bartpelle.veteres.services.serializers.pg.part;

import com.google.gson.*;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemAttrib;
import nl.bartpelle.veteres.util.JGson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Bart on 8/10/2015.
 */
public class InventoryPart implements PgJsonPart {
	
	private JsonParser parser = new JsonParser();
	private Gson gson = JGson.get();
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		JsonObject inventory = parser.parse(resultSet.getString("inventory")).getAsJsonObject();
		JsonArray itemarray = inventory.getAsJsonArray("items");
		for (JsonElement item_ : itemarray) {
			JsonObject item = item_.getAsJsonObject();
			
			Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
			JsonArray attribs = inventory.getAsJsonArray("properties");
			for (JsonElement attrib : itemarray) {
				JsonObject attr = attrib.getAsJsonObject();
				int pkey = attr.get("id").getAsInt();
				Optional<ItemAttrib> iattr = ItemAttrib.forPersistencyKey(pkey);
				iattr.ifPresent(itemAttrib -> itemobj.property(itemAttrib, attr.get("value").getAsInt()));
			}
			player.inventory().set(item.get("slot").getAsInt(), itemobj);
		}
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection connection, boolean removeOnline) throws SQLException {
		JsonArray itemarray = new JsonArray();
		for (int i = 0; i < 28; i++) {
			Item item = player.inventory().get(i);
			if (item != null) {
				JsonObject obj = new JsonObject();
				obj.add("slot", new JsonPrimitive(i));
				obj.add("id", new JsonPrimitive(item.id()));
				obj.add("amount", new JsonPrimitive(item.amount()));
				
				if (item.hasProperties()) {
					JsonArray attribsarray = new JsonArray();
					for (Map.Entry<ItemAttrib, Integer> e : item.propset()) {
						// Ignore non-persistent pairs
						if (!e.getKey().isPersistent())
							continue;
						JsonObject attrib = new JsonObject();
						attrib.add("id", new JsonPrimitive(e.getKey().getPersistencyKey()));
						attrib.add("value", new JsonPrimitive(e.getValue()));
					}
					obj.add("properties", attribsarray);
				}
				itemarray.add(obj);
			}
		}
		
		JsonObject itemobj = new JsonObject();
		itemobj.add("items", itemarray);
		
		characterUpdateStatement.setString(4, gson.toJson(itemobj));
	}
	
}
