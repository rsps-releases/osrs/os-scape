package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;

/**
 * Created by Bart on 11/03/2017.
 */
public class ResetPvpSkillsV2 implements Migration {
	
	private static final int[] SKILLS_TO_RESET = {
			Skills.FISHING, Skills.HUNTER, Skills.MINING,
			Skills.WOODCUTTING, Skills.SLAYER, Skills.THIEVING
	};
	
	@Override
	public int id() {
		return 18;
	}
	
	@Override
	public boolean apply(Player player) {
		if (player.world().realm().isPVP()) {
			for (int i : SKILLS_TO_RESET) {
				player.skills().xp()[i] = 0;
				player.skills().setLevel(i, 1);
			}
		}
		
		return true;
	}
}
