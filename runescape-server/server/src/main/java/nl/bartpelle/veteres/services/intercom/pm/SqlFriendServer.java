package nl.bartpelle.veteres.services.intercom.pm;

import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.services.intercom.ClanChatService;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.PgSqlWorker;
import nl.bartpelle.veteres.services.sql.SqlTransaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Bart on 11/15/2015.
 */
public class SqlFriendServer {
	
	private PgSqlWorker worker;
	private PgSqlService sqlService;
	private RedisPmService master;
	
	public SqlFriendServer(RedisPmService master) {
		this.master = master;
		
		sqlService = master.server().service(PgSqlService.class, false).get();
		worker = new PgSqlWorker(sqlService);
		
		new Thread(worker).start();
	}
	
	public void updateSocialFor(Player player, Runnable r) {
		if (player.bot())
			return;
		
		worker.submit(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				PreparedStatement accounts = connection.prepareStatement(
						"SELECT * FROM getfriendsfor(?)");
				
				accounts.setInt(1, (Integer) player.id());
				
				ResultSet online = accounts.executeQuery();
				List<Friend> friends = new LinkedList<>();
				
				while (online.next()) {
					friends.add(new Friend(online.getInt("friend_id"), online.getString("displayname"),
							online.getString("lastname"), online.getInt("world_id"), online.getInt("clanrank"), online.getInt("rights")));
				}
				
				connection.commit();
				player.social().friends(friends);
				player.social().pushFriends();
				
				r.run();
			}
		});
		
		worker.submit(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				PreparedStatement accounts = connection.prepareStatement(
						"SELECT ignores.account_id, friend_id, displayname, lastname, rights\n" +
								"FROM ignores\n" +
								"LEFT JOIN accounts\n" +
								"ON ignores.friend_id = accounts.id\n" +
								"WHERE ignores.account_id = ?");
				
				accounts.setInt(1, (Integer) player.id());
				
				ResultSet online = accounts.executeQuery();
				List<Friend> ignores = new LinkedList<>();
				
				while (online.next()) {
					ignores.add(new Friend(online.getInt("friend_id"), online.getString("displayname"),
							online.getString("lastname"), 0, 0, online.getInt("rights")));
				}
				
				connection.commit();
				player.social().ignores(ignores);
				player.social().pushIgnores();
				
				player.world().server().service(ClanChatService.class, true).ifPresent(serv -> {
					serv.command((Integer) player.id(), "reloadignores", 0);
				});
				
				r.run();
			}
		});
	}
	
	private void updateClanChat(int id) {
		ClanChat chat = master.server().world().chats().get(id);
		Optional<ClanChatService> clanChatService = master.server().service(ClanChatService.class, true);
		if (clanChatService.isPresent() && chat != null) {
			clanChatService.get().command(id, "refresh", 0);
		}
	}
	
	public void addFriend(Player player, String name) {
		if (player.bot())
			return;
		
		worker.submit(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement accounts = connection.prepareStatement("SELECT id,displayname,lastname,rights FROM accounts WHERE displayname ILIKE ?");
					accounts.setString(1, name);
					
					// Did we resolve him?
					ResultSet accdata = accounts.executeQuery();
					if (accdata.next()) {
						int accid = accdata.getInt("id");
						String name = accdata.getString("displayname");
						String last = accdata.getString("lastname");
						int rights = accdata.getInt("rights");
						player.social().friends().add(new Friend(accid, name, last, 0, 0, rights));
						player.social().updateFriend(accid, 0);
						
						// Insert into the table
						PreparedStatement insertion = connection.prepareStatement("INSERT INTO friends (account_id, friend_id) VALUES (?, ?)");
						insertion.setInt(1, (Integer) player.id());
						insertion.setInt(2, accid);
						insertion.executeUpdate();
						
						// Now check his online status
						PreparedStatement online = connection.prepareStatement("SELECT world_id FROM online_characters WHERE account_id=?");
						online.setInt(1, accid);
						
						ResultSet onlinedata = online.executeQuery();
						if (onlinedata.next()) {
							player.world().server().service(PmService.class, true).ifPresent(pmService -> {
								pmService.onUserOnline(player);
							});
						}
						
						player.world().server().service(ClanChatService.class, true).ifPresent(serv -> {
							serv.command((Integer) player.id(), "refresh", 0);
						});
					} else {
						player.message("Unable to add friend - unknown player.");
					}
				} catch (Exception ignored) {
					
				}
				
				connection.commit();
			}
		});
	}
	
	public void addIgnore(Player player, String name) {
		if (player.bot())
			return;
		
		worker.submit(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement accounts = connection.prepareStatement("SELECT id,displayname,lastname, rights FROM accounts WHERE displayname ILIKE ?");
					accounts.setString(1, name);
					
					// Did we resolve him?
					ResultSet accdata = accounts.executeQuery();
					if (accdata.next()) {
						int accid = accdata.getInt("id");
						String name = accdata.getString("displayname");
						String lastname = accdata.getString("lastname");
						int rights = accdata.getInt("rights");
						player.social().ignores().add(new Friend(accid, name, lastname, 0, 0, rights));
						player.social().pushIgnores();
						
						// Insert into the table
						PreparedStatement insertion = connection.prepareStatement("INSERT INTO ignores (account_id, friend_id) VALUES (?, ?)");
						insertion.setInt(1, (Integer) player.id());
						insertion.setInt(2, accid);
						insertion.executeUpdate();
						
						player.world().server().service(ClanChatService.class, true).ifPresent(serv -> {
							serv.command((Integer) player.id(), "reloadignores", 0);
						});
					} else {
						player.message("Unable to add name - unknown player.");
					}
				} catch (Exception ignored) {
					
				}
				
				connection.commit();
			}
		});
	}
	
	public void removeFriend(Player player, int id) {
		if (player.bot())
			return;
		
		worker.submit(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				PreparedStatement accounts = connection.prepareStatement("DELETE FROM friends WHERE account_id = ? AND friend_id = ?");
				accounts.setInt(1, (Integer) player.id());
				accounts.setInt(2, id);
				accounts.executeUpdate();
				connection.commit();
				
				player.world().server().service(ClanChatService.class, true).ifPresent(serv -> {
					serv.command((Integer) player.id(), "refresh", 0);
				});
				
				player.world().server().service(PmService.class, true).ifPresent(pmService -> {
					pmService.onUserOnline(player);
				});
			}
		});
	}
	
	public void removeAllFriends(Player player) {
		if (player.bot())
			return;
		
		worker.submit(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				PreparedStatement accounts = connection.prepareStatement("DELETE FROM friends WHERE account_id = ?");
				accounts.setInt(1, (Integer) player.id());
				accounts.executeUpdate();
				connection.commit();
				
				player.world().server().service(ClanChatService.class, true).ifPresent(serv -> {
					serv.command((Integer) player.id(), "refresh", 0);
				});
				
				player.world().server().service(PmService.class, true).ifPresent(pmService -> {
					pmService.onUserOnline(player);
				});
			}
		});
	}
	
	public void removeIgnore(Player player, int id) {
		if (player.bot())
			return;
		
		worker.submit(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				PreparedStatement accounts = connection.prepareStatement("DELETE FROM ignores WHERE account_id = ? AND friend_id = ?");
				accounts.setInt(1, (Integer) player.id());
				accounts.setInt(2, id);
				accounts.executeUpdate();
				connection.commit();
				
				player.world().server().service(ClanChatService.class, true).ifPresent(serv -> {
					serv.command((Integer) player.id(), "reloadignores", 0);
				});
			}
		});
	}
	
}
