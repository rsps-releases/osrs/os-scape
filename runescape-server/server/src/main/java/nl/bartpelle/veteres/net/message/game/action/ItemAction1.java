package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 5-2-2015.
 */
@PacketInfo(size = 8)
public class ItemAction1 extends ItemAction {
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		slot = buf.readULEShortA();
		item = buf.readUShortA();
		hash = buf.readInt();
		
		log(player, opcode, size, "inter=%d child=%d item=%d slot=%d", hash >> 16, hash & 0xffff, item, slot);
	}
	
	@Override
	protected int option() {
		return 0;
	}
	
	@Override
	public void process(Player player) {
		super.process(player);
		if (slot < 0 || slot > 27) return;
		Item item = player.inventory().get(slot);
		if (item != null && item.id() == this.item && !player.locked() && !player.dead()) {
			player.stopActions(false);
			player.putattrib(AttributeKey.FROM_ITEM, item);
			player.world().server().scriptRepository().triggerItemOption1(player, item.id(), slot);
		}
	}
}
