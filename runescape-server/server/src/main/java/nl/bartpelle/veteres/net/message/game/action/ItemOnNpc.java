package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.NpcInteraction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

import java.lang.ref.WeakReference;

/**
 * Created by Bart on 11/10/2015.
 */
@PacketInfo(size = 11)
public class ItemOnNpc implements Action {
	
	private int hash;
	private int itemSlot;
	private int npcIdx, itemId;
	private boolean run;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		itemSlot = buf.readUShort();
		run = buf.readByteS() != 0;
		npcIdx = buf.readULEShort();
		itemId = buf.readUShortA();
		hash = buf.readIntV2();
		
		log(player, opcode, size, "slot=%d idx=%d inter=[%d:%d] item=%d run=%b", itemSlot, npcIdx, hash >> 16, hash & 0xffff, itemId, run);
	}
	
	@Override
	public void process(Player player) {
		player.debug("[%d:%d], %d, %d, %d%n", hash >> 16, hash & 0xFFFF, itemSlot, npcIdx, itemId);
		
		int inter = hash >> 16;
		int child = hash & 0xFFFF;
		
		// Only process inventory spells for now
		if (inter == 149 && child == 0) {
			Npc targ = player.world().npcs().get(npcIdx);
			if (targ == null || player.locked() || player.dead()) {
				return;
			}
			player.stopActions(true);
			player.face(targ);
			
			if (targ.dead()) {
				return;
			}
			
			Item item = player.inventory().get(itemSlot);
			if (item == null || item.id() != itemId)
				return;
			
			// Store attribs
			player.putattrib(AttributeKey.ITEM_SLOT, itemSlot);
			player.putattrib(AttributeKey.INTERACTION_OPTION, -1); // secret key
			player.putattrib(AttributeKey.ITEM_ID, itemId);
			player.putattrib(AttributeKey.FROM_ITEM, item);
			player.putattrib(AttributeKey.TARGET, new WeakReference<>(targ));
			
			player.world().server().scriptExecutor().executeLater(player, NpcInteraction.script);
		}
	}
}
