package nl.bartpelle.veteres.util;

import co.paralleluniverse.fibers.Suspendable;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.zaxxer.hikari.HikariPoolMXBean;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import kotlin.ranges.IntRange;
import nl.bartpelle.skript.Script;
import nl.bartpelle.veteres.ServerProcessor;
import nl.bartpelle.veteres.content.BuyCredits;
import nl.bartpelle.veteres.content.Help;
import nl.bartpelle.veteres.content.TestVarbits;
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars;
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.RemoveObjects;
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.sigmund.SigmundExchangeConstants;
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance;
import nl.bartpelle.veteres.content.areas.instances.PVPAreas;
import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Hans;
import nl.bartpelle.veteres.content.areas.tzhaar.InfernoPool;
import nl.bartpelle.veteres.content.areas.wilderness.CommandTeleportPrompt;
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.combat.magic.MagicCombat;
import nl.bartpelle.veteres.content.combat.melee.MeleeSpecialAttacks;
import nl.bartpelle.veteres.content.events.BloodyVolcano;
import nl.bartpelle.veteres.content.interfaces.Bank;
import nl.bartpelle.veteres.content.interfaces.SpellSelect;
import nl.bartpelle.veteres.content.interfaces.magicbook.TeleportNoReqs;
import nl.bartpelle.veteres.content.items.equipment.RunePouch;
import nl.bartpelle.veteres.content.lottery.Lottery;
import nl.bartpelle.veteres.content.mechanics.*;
import nl.bartpelle.veteres.content.mechanics.deadman.BankKey;
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanContainers;
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics;
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup;
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent;
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking;
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext;
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession;
import nl.bartpelle.veteres.content.minigames.pest_control.*;
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveGame;
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveMonsters;
import nl.bartpelle.veteres.content.minigames.wintertodt.WintertodtGame;
import nl.bartpelle.veteres.content.npcs.pestcontrol.Splatter;
import nl.bartpelle.veteres.content.npcs.pets.Pet;
import nl.bartpelle.veteres.content.quests.QuestGuide;
import nl.bartpelle.veteres.content.skills.fishing.Fishing;
import nl.bartpelle.veteres.content.skills.magic.CombinationRunes;
import nl.bartpelle.veteres.content.tournament.Tournament;
import nl.bartpelle.veteres.content.tournament.TournamentConfig;
import nl.bartpelle.veteres.content.tournament.TournamentInscription;
import nl.bartpelle.veteres.crypto.IsaacRand;
import nl.bartpelle.veteres.fs.*;
import nl.bartpelle.veteres.migration.impl.Zulrah_uncharge_all_14;
import nl.bartpelle.veteres.model.*;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.*;
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeHistory;
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeType;
import nl.bartpelle.veteres.model.entity.player.content.exchange.GrandExchange;
import nl.bartpelle.veteres.model.instance.InstancedMap;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.model.item.Shop;
import nl.bartpelle.veteres.model.item.W2tradables;
import nl.bartpelle.veteres.model.map.MapObj;
import nl.bartpelle.veteres.model.map.steroids.Direction;
import nl.bartpelle.veteres.net.codec.game.ActionDecoder;
import nl.bartpelle.veteres.net.message.game.command.*;
import nl.bartpelle.veteres.script.JSScripts;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.services.advertisement.VoteRewardService;
import nl.bartpelle.veteres.services.financial.BMTPostback;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.services.login.LoginWorker;
import nl.bartpelle.veteres.services.lottery.LottoService;
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.journal.toggles.RiskProtectionState;
import nl.bartpelle.veteres.util.map.MapDecryptionKeys;
import org.mindrot.jbcrypt.BCrypt;

import javax.management.JMX;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static nl.bartpelle.veteres.content.interfaces.magicbook.Teleports.basicTeleport;
import static nl.bartpelle.veteres.content.interfaces.magicbook.Teleports.canTeleport;

/**
 * Created by Bart Pelle on 8/23/2014.
 */
public final class GameCommands {
	
	public static final String[] ADMIN_EXCEPTIONS = new String[]{"Heaven", "Airo", "Bart"};
	/**
	 * Map containing the registered commands.
	 */
	private static Map<String, Command> commands = setup();
	
	private GameCommands() {
		
	}
	
	@Suspendable
	private static Map<String, Command> setup() {
		nondesccomnnds = 0;
		desccomnnds = 0;
		
		commands = new HashMap<>();
		buildCommandsList();


		put(Privilege.PLAYER, "damplist", (p, args) -> {
			// Show words
			StringBuilder sb1 = new StringBuilder();
			Censor.getDamperedWords().forEach(w -> sb1.append(String.format("%s, ", w)));
			String dampers = sb1.toString();
			p.message("Repeated <col=FF0000>dampered words</col> ::adddamp / ::deldamp to adjust.");
			p.message(dampers.substring(0, dampers.length() - 2)); // remove last \", \"")
		});
		put(Privilege.PLAYER, "adddamp", (p, args) -> {
			p.executeKtScript(PunishmentLog.addDamper);
		});
		put(Privilege.PLAYER, "deldamp", (p, args) -> {
			p.executeKtScript(PunishmentLog.deleteDamper);
		});

		/* Player commands */
		put(Privilege.ADMIN, "dt", (p, args) -> {
			MapObj o = new MapObj(p.tile(), Integer.parseInt(args[0]), 10, 1);
			p.message("Flip of " + o.id() + " is " + o.definition(p.world()).vflip);
		}, "TODO Usage ::dt objectid");
		
		put(Privilege.ADMIN, "doorlist", (p, args) -> {
			Doors.getDOORSET().forEach(door -> System.out.println("Door: " + door.getId() + " to " + door.getToId() + " open: " + door.getOpen()));
		}, "Prints out the currently handled doors. ::doorlist");
		
		put(Privilege.ADMIN, "goto", (p, args) -> FastTravel.process(p, args[0]));
		put(Privilege.ADMIN, "ft", (p, args) -> FastTravel.process(p, args[0]));
		
		put(Privilege.ADMIN, "printpos", (p, args) -> System.out.println(p.tile()));
		put(Privilege.ADMIN, "sigmund", (p, args) -> {
			SigmundExchangeConstants.load();
			p.message("Reloaded Sigmund prices.");
		});

		put(Privilege.PLAYER, "jointournament", (p, args) -> Tournament.INSTANCE.join(p, true));

		put(Privilege.ADMIN, "openinscriptions", (p, args) -> TournamentInscription.INSTANCE.open(p.world()));

		put(Privilege.ADMIN, "closeinscriptions", (p, args) -> TournamentInscription.INSTANCE.close(p.world()));

		put(Privilege.ADMIN, "tournamentrewards", (p, args) -> p.putattrib(AttributeKey.UNCLAIMED_TOURNAMENT_BM, Integer.valueOf(args[0])));

		put(Privilege.ADMIN, "pvptournamentinstance", (p, args) -> p.teleport(TournamentConfig.PVP_TOURNAMENT_AREA.randomTile()));

		put(Privilege.PLAYER, "youtuber", (p, args) -> {
		    if (p.getDefaultIcon() < PlayerIcon.YOUTUBER_BRONZE || p.getDefaultIcon() > PlayerIcon.YOUTUBER_GOLD) {
				return;
			}

		    long current = System.currentTimeMillis();
		    long lastRedeem = p.attribOr(AttributeKey.BLOOD_MONEY_CLAIM, 0L);
		    long days = TimeUnit.MILLISECONDS.toDays(current - lastRedeem);
		    if (lastRedeem > 0 && days < 7) {
		        p.message("<col=FF0000>You've already claimed this week's rewards. You may redeem again in another "+ (7 - days) +" days.");
		        return;
            }

            int bloodMoneyCount = p.getDefaultIcon() == PlayerIcon.YOUTUBER_GOLD ? 300_000 : p.getDefaultIcon() == PlayerIcon.YOUTUBER_SILVER ? 150_000 : 50_000;

            if (p.inventory().add(new Item(13307, bloodMoneyCount)).failed()) {
                p.message("<col=FF0000>Due to insufficient space in your inventory your blood money reward has been placed in your bank.");
                p.bank().add(new Item(13307, bloodMoneyCount));
            } else {
                p.message("<col=FF0000>You've successfully claimed your blood money reward. Thanks for playing Os-Scape!");
            }

            p.putattrib(AttributeKey.BLOOD_MONEY_CLAIM, current);
        });
		
		put(Privilege.ADMIN, "killself", (p, args) -> {
			p.hit(p, 99);
			p.message("You've successfully necked yourself.");
		});
		for (String s : new String[]{"helperzone", "hz", "visithz", "telehz"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
					if (PVPAreas.getHELPERS_INSTANCE() != null) {
						if (PlayerCombat.inCombat(p)) {
							p.message("You can't use this teleport when in combat.");
							return;
						}
						if (Staking.in_duel(p)) {
							p.message("You can't teleport out of a duel.");
							return;
						}
						if (ClanWars.inInstance(p)) {
							p.message("You can't teleport out of a clan war.");
							return;
						}
						if (p.helper() && inWildBypassable(p, "You can't tele hz when you're in the Wilderness.", true)) {
							return;
						}
						p.world().tileGraphic(110, p.tile(), 110, 15);
						basicTeleport(p, PVPAreas.getHZ_ENTER_COORDS());
					}
				}
			}, "Teleports you to the helper's zone. ::hz");
		}
		
		put(Privilege.PLAYER, "sendhz", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				Optional<Player> otherp = p.world().playerByName(glue(args));
				if (otherp.isPresent()) {
					Player other = otherp.get();
					if (Staking.in_duel(other)) {
						p.message("You can't send hz to someone during a duel.");
					} else if (ClanWars.inInstance(other)) {
						p.message("You can't send hz to someone during a clan war.");
					} else if (other.helper() || other.privilege().eligibleTo(Privilege.MODERATOR)) {
						p.message("You can't send hz other staff members.");
					} else {
						if (p.helper() && inDangerous(other)) {
							p.message("You can't sendhz someone when they're in a dangerous area.");
							return;
						}
						if (PlayerCombat.inCombat(other)) {
							p.message("You can't sendhz someone who is in combat. ");
							return;
						}
						if (PVPAreas.getHELPERS_INSTANCE() != null) {
							other.stopActions(true);
							other.world().tileGraphic(110, other.tile(), 110, 15);
							other.world().server().scriptExecutor().executeScript(other, new TeleportNoReqs(PVPAreas.getHZ_ENTER_COORDS()));
							p.message("You've sent <col=FF0000>%s</col> to the Helper's zone.", other.name());
							other.message("<col=FF0000>%s</col> teleported you to the Helpers' zone.", p.name());
						}
					}
				} else {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				}
			}
		}, "Teleports the specified player to the helper's zone. ::sendhz playername");
		
		put(Privilege.MODERATOR, "togglearena", (p, args) -> {
			GameCommands.STAKING_STAFF_ONLY = !GameCommands.STAKING_STAFF_ONLY;
			p.message("Duel arena staff only : " + GameCommands.STAKING_STAFF_ONLY);
		}, "Turns the duel arena on/off. ::togglearena");
		
		put(Privilege.ADMIN, "facetile", (p, args) -> {
			p.sync().facetile(p.tile().transform(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
		});

        put(Privilege.ADMIN, "cwars", (p, args) -> {

            InstancedMap allocated = p.world().allocator().allocate(1, new Tile(3232, 3232)).get();

            allocated.setDangerous(false);
            allocated.setDeallocateOnDeath(false);
            allocated.setDeallocatesOnLogout(false);
            allocated.setMulti(true);
            allocated.set(0, 0, new Tile(3264, 4928), new Tile(3264, 4928).transform(64, 64), 0, true);

            p.teleport(allocated.x1 + 52, allocated.z1 + 20);
        });

		put(Privilege.ADMIN, "mapoffset", (p, args) -> {
			Optional<InstancedMap> map = p.world().allocator().active(p.tile());
			
			if (!map.isPresent()) {
				p.message("You are not in a instanced map.");
			} else {
				map.ifPresent(m -> {
					p.teleport(m.center().transform(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
				});
			}
		}, "Teleports you inside an instanced map. ::mapoffset x y");
		
		for (String s : new String[]{"objsonme", "objsat", "objat"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.world().spawnedObjs().stream().filter(o -> o.tile().equals(p.tile().x, p.tile().z)).forEach(
						o -> p.message("spawned: " + o.id()));
				
				p.world().removedObjs().stream().filter(o -> o.tile().equals(p.tile().x, p.tile().z)).forEach(
						o -> p.message("removed: " + o.id()));
				
				MapDefinition mapdef = p.world().definitions().get(MapDefinition.class, p.tile().region());
				
				List<MapObj> objects = mapdef.objs(0, p.tile().x & 63, p.tile().z & 63);
				if (objects != null && objects.size() > 0) {
					objects.stream().filter(o -> o != null).forEach(o -> {
						String info = String.format("mapdef has object: ID: %d ROT: %d TYPE: %d (%s) at %s.", o.id(), o.rot(), o.type(), o.definition(p.world()).name, p.tile().toStringSimple());
						p.message(info);
						System.out.println(info);
					});
				} else {
					p.message("There are no mapobjects belonging to this region at your position %s", p.tile().toStringSimple());
				}
			}, "Finds the object information at your current location. ::objsonme");
		}
		
		put(Privilege.ADMIN, "mymapoffset", (p, args) -> {
			Optional<InstancedMap> map = p.world().allocator().active(p.tile());
			
			if (!map.isPresent()) {
				p.message("You are not in a instanced map.");
			} else {
				map.ifPresent(m -> {
					Tile myTile = p.tile();
					Tile center = m.center();
					Tile botLeft = m.bottomLeft();
					
					Tile offset = new Tile(myTile.x - center.x, myTile.z - center.z);
					Tile offset2 = new Tile(myTile.x - botLeft.x, myTile.z - botLeft.z);
					
					p.message("Offset to center: " + offset);
					p.message("Offset to base: " + offset2);
				});
			}
		}, "Finds your map offset in an instanced map. ::mymapoffset");
		
		put(Privilege.ADMIN, "tentcopy", (p, args) -> {
			Tile base = new Tile(2790, 3656);
			Tile dest = new Tile(p.tile().x, p.tile().z);
			for (int l = 0; l < 2; l++) {
				for (int x = 0; x < 40; x++) {
					for (int z = 0; z < 40; z++) {
						MapDefinition def = p.world().definitions().get(MapDefinition.class, Tile.coordsToRegion(base.x + x, base.z + z));
						List<MapObj> objs = def.objs(0, (base.x + x) & 63, (base.z + z) & 63);
						if (objs != null) {
							for (MapObj o : objs) {
								p.world().spawnObj(new MapObj(new Tile(dest.x + x, dest.z + z), o.id(), o.type(), o.rot()), true);
							}
						}
					}
				}
			}
		}, "TODO Usage ::tentcopy");
		
		for (String s : new String[]{"lockxp", "xplock", "noxp"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isPVP() || p.world().realm().isDeadman() || p.world().server().isDevServer()) {
					VarbitAttributes.toggle(p, Varbit.XPLOCKED);
					p.message("Your experience is %s.", VarbitAttributes.varbiton(p, Varbit.XPLOCKED) ? "<col=5858FA>LOCKED</col>" : "<col=AC58FA>UNLOCKED</col>");
				}
			}, "Locks/Unlocks your xp rate. ::lockxp");
		}
		
		put(Privilege.ADMIN, "lpos", (p, args) -> {
			int x = p.tile().x;
			int z = p.tile().z;
			
			int base_x = x / 8;
			int base_z = z / 8;
			
			int botleft_x = (base_x - 6) * 8;
			int botleft_z = (base_z - 6) * 8;
			
			x = base_x;
			z = base_z;
			int localX = x - botleft_x;
			int localZ = z - botleft_z;
			p.message("Location %s, %s, %s, %s, %s", x, z, localX, localZ, p.tile().x >> 3);
		}, "TODO Usage ::tentcopy");
		
		put(Privilege.ADMIN, "rpos", (p, args) -> {
			Tile region = p.tile().regionCorner();
			int z = p.tile().z - region.z;
			int x = p.tile().x - region.x;
			p.message("Region Location %s, %s, %s", x, z, p.tile().region());
		}, "TODO Usage ::tentcopy");
		
		put(Privilege.ADMIN, "cpos", (p, args) -> {
			Tile region = p.tile().regionCorner();
			int z = p.tile().z - region.z;
			int x = p.tile().x - region.x;
			p.message("Chunk Location %s, %s, %s", x / 8 * 8, z / 8 * 8, p.tile().region());
		}, "TODO Usage ::tentcopy");
		
		put(Privilege.ADMIN, "hikari", (p, args) -> {
			try {
				MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
				ObjectName poolName = new ObjectName("com.zaxxer.hikari:type=Pool (hikari)");
				HikariPoolMXBean poolProxy = JMX.newMXBeanProxy(mBeanServer, poolName, HikariPoolMXBean.class);
				
				p.message("Idle connections: %d", poolProxy.getIdleConnections());
				p.message("Active connections: %d", poolProxy.getActiveConnections());
				p.message("Total connections: %d", poolProxy.getTotalConnections());
				p.message("Awaiting threads: %d", poolProxy.getThreadsAwaitingConnection());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, "Shows information about current connections. ::hikari");
		
		put(Privilege.PLAYER, "commands", (p, args) -> {
			if (p.world().realm().isPVP() || p.world().realm().isDeadman()) {
				QuestGuide.clear(p);
				
				for (int i = 2; i < 2 + rawCommandsListText.length; i++) {
					if (!p.privilege().eligibleTo(Privilege.ADMIN) && i >= 53) break;
					if (!p.privilege().eligibleTo(Privilege.MODERATOR) && i >= 28) break;
					if (!p.privilege().eligibleTo(Privilege.MODERATOR) && !p.helper() && i >= 21) break;
					p.interfaces().text(275, i, rawCommandsListText[i - 2]);
				}
				
				QuestGuide.open(p);
			}
		}, "Opens the commands interface. ::commands");
		
		put(Privilege.ADMIN, "ss", (p, args) -> {
			p.interfaces().text(Integer.parseInt(args[0]), Integer.parseInt(args[1]), args[2]);
		}, "Sets string on an interface. ::ss interface child string");
		
		put(Privilege.ADMIN, "showstrings", (p, args) -> {
			for (int i = 0; i < 1000; i++) {
				p.interfaces().text(Integer.parseInt(args[0]), i, "" + i);
			}
		}, "Sets string on an interface. ::ss interface child string");
		
		put(Privilege.ADMIN, "booth", (p, args) -> {
			PollBooth.INSTANCE.showPolls(p);
		}, "");
		
		put(Privilege.ADMIN, "resetcamera", (p, args) -> {
			p.write(new ShakeReset());
		}, "Turns off camera shake. ::resetcamera");

		put(Privilege.ADMIN, "test", (p, args) -> {
			p.timers().cancel(TimerKey.RISK_PROTECTION);
			RiskProtectionState.monitorRiskProtection(p);
		}, "");

		put(Privilege.ADMIN, "creditdiscount", (p, args) -> {
			int discountPercent = Integer.valueOf(args[0]);
			BuyCredits.setDiscount(discountPercent);
			p.message("Credits are now " + discountPercent + "% off.");
		}, "");

		put(Privilege.ADMIN, "creditmessage", (p, args) -> {
			String message = glue(args);
			BuyCredits.setMessage(message);
			p.message("Credit message changed to: " + message);
		}, "");

		put(Privilege.ADMIN, "pulllotto", (p, args) -> {
			Lottery.pull();
			p.message("OSScape Lottery has been synced with the database.");
		}, "Force the lottery to pull from the database. ::pulllotto");
		
		put(Privilege.ADMIN, "pushlotto", (p, args) -> {
			Lottery.push();
			p.message("The Lottery database has been synced with OSScape Lottery.");
		}, "Force the lottery to push to the database. ::pushlotto");
		
		put(Privilege.MODERATOR, "togglelotto", (p, args) -> {
			p.message("OSScape Lottery has been " + (!Lottery.enabled() ? "enabled" : "disabled") + ".");
			p.world().server().service(LottoService.class, true).ifPresent(LottoService::toggle);
		}, "Turns the lottery on/off. Effects all worlds. ::togglelotto");
		
		put(Privilege.MODERATOR, "togglege", (p, args) -> {
			p.message("Grand Exchange has been " + (!GrandExchange.enabled ? "enabled" : "disabled") + ".");
			GrandExchange.enabled = !GrandExchange.enabled;
		}, "Turns the grand exchange on/off. Must be done one each world. ::togglege");
		
		put(Privilege.MODERATOR, "ffs", (p, args) -> {
			p.grandExchange().loadOffers();
		}, "Turns the grand exchange on/off. Must be done one each world. ::togglege");
		
		put(Privilege.ADMIN, "endlotto", (p, args) -> {
			Lottery.rewardAndReset();
		}, "Forces lottery to end. ::endlotto");
		
		put(Privilege.ADMIN, "reloadprices", (p, args) -> {
			p.world().prices().reload();
		}, "Reload all item prices from the database. ::reloadprices");
		
		put(Privilege.PLAYER, "players", (p, args) -> {
			int size = Misc.playerCount(p.world());
			p.message("There %s %d player%s online.", size == 1 ? "is" : "are", size, size == 1 ? "" : "s");
		}, "Shows the current player count. ::players");
		
		put(Privilege.PLAYER, "uptime", (p, args) -> {
			p.message("Uptime: %s", p.world().getTime());
		}, "Shows the current uptime of the server. ::uptime");
		
		put(Privilege.ADMIN, "rdealloc", (p, args) -> {
			p.world().allocator().cleanup();
		}, "Cleanup regions/other garbage. ::rdealloc");
		
		put(Privilege.ADMIN, "energy", (p, args) -> {
			int amt = Integer.parseInt(args[0]);
			p.setRunningEnergy(amt, true);
		}, "Set your run energy to the specified amount. ::energy amount");
		
		/* Supervisor commands */
		for (String s : new String[]{"reload", "reloadcommands", "reloadcmd", "reloadcmds"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				commands = setup();
				p.message("Commands reinitalized.");
			}, "Reloads the commands. ::reloadcommands");
		}

		put(Privilege.ADMIN, "disableyell", (p, args) -> {
			String playerName = glue(args);
			if (playerName.isEmpty()) {
				p.message("Player name must not be empty! Syntax ::disableyell playername");
				return;
			}

			Optional<Player> targetPlayer = p.world().playerByName(playerName);

			if (!targetPlayer.isPresent()) {
				p.message("Player is offline.");
				return;
			}

			if (targetPlayer.get().privilege() == Privilege.MODERATOR || targetPlayer.get().privilege() == Privilege.ADMIN) {
				p.message("You cannot revoke a staff member's ability to yell.");
				return;
			}

			targetPlayer.get().putattrib(AttributeKey.YELL_DISABLED, true);
			p.message("You have disabled "+ targetPlayer.get().name() +"'s ability to yell.");
			targetPlayer.get().message("Your yell ability has been disabled by "+ p.name() +".");

		}, "Disable another player's yell ability. ::disableyell playername");
		
		put(Privilege.ADMIN, "refreshlooks", (p, args) -> p.looks().update(), "Force refresh of your player looks. ::refreshlooks");
		put(Privilege.MODERATOR, "resetlooks", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				other.looks().looks(Looks.GOOD_LOOKS);
				other.looks().update();
			});
		}, "Reset looks of another player. ::resetlooks playername");
		
		put(Privilege.ADMIN, "logout", (p, args) -> p.logout(), "Forces you to logout. ::logout");
		
		for (String s : new String[]{"coords", "loc", "pos"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
			    p.message("Your coordinates are [%d, %d, %d]. Region %d, RelX=%d, RelZ=%d, Chunk-id %d.", p.tile().x, p.tile().z, p.tile().level, p.tile().region(), p.tile().regionX(), p.tile().regionZ(), p.tile().chunk());
            }, "Gets your current position. ::coords");
		}
		
		for (String s : new String[]{"corner"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.message("Region %d has corner coords %d, %d.", p.tile().region(), p.tile().regionCorner().x, p.tile().regionCorner().z);
			}, "Gets your current region's corner positions. ::corner");
		}
		
		put(Privilege.ADMIN, "loop", (p, args) -> {
			final String type = args[0];
			
			final int start = Integer.parseInt(args[1]);
			
			final int amount = Integer.parseInt(args[2]);
			
			if (type.equals("anim")) {
				AnimationIteration iteration = new AnimationIteration(p, start, amount);
				p.world().server().scriptExecutor().executeScript(p, iteration.getScript());
			} else if (type.equals("gfx")) {
				GraphicIteration iteration = new GraphicIteration(p, start, amount);
				p.world().server().scriptExecutor().executeScript(p, iteration.getScript());
			} else if (type.equals("interface")) {
				InterfaceIteration iteration = new InterfaceIteration(p, start, amount);
				p.world().server().scriptExecutor().executeScript(p, iteration.getScript());
			} else if (type.equals("varbit")) {
			    p.world().server().scriptExecutor().executeScript(p, new VarbitIteration(p, start, amount).getScript());
			} else {
				p.message("You need to loop either anim or gfx.");
			}
		}, "Loop a specified amount of anim/gfx/interfaces. ::loop anim/gfx/interface start end");
		
		put(Privilege.ADMIN, "posof", (p, args) -> {
			int r = Integer.parseInt(args[0]);
			int x = (r >> 8) << 6;
			int y = (r & 0xFF) << 6;
			p.message("Region " + r + " is located at " + x + ", " + y + ".");
		}, "Get the specified region x/y. ::posof regionid");
		
		put(Privilege.ADMIN, "shop", (p, args) -> {
			int shopId = Integer.parseInt(args[0]);
			p.world().shop(shopId).display(p);
			p.message("Viewing shop: " + shopId);
		}, "Opens the specified shop. ::shop shopid");
		
		put(Privilege.MODERATOR, "tele", (p, args) -> {
			if (p.privilege() == Privilege.ADMIN || p.seniorModerator()) {
				p.interfaces().closeMain();
				if (args[0].contains(",")) { // Ctrl-shift click
					String[] params = args[0].split(",");
					int level = Integer.parseInt(params[0]);
					int rx = Integer.parseInt(params[1]);
					int rz = Integer.parseInt(params[2]);
					int lx = Integer.parseInt(params[3]);
					int lz = Integer.parseInt(params[4]);
					Optional<InstancedMap> before = p.world().allocator().active(p.tile());
					p.teleport(rx * 64 + lx, rz * 64 + lz, level);
					Optional<InstancedMap> after = p.world().allocator().active(p.tile());
					// Usually only triggers (sync) when change <16 | >88 size last region change. This forces it for small distance teleports.
					if (after.isPresent() && (!before.isPresent() || (before.isPresent() && before.get() != after.get()))) { // Instanced map changed or is now present
						p.message("Force syncing new instance map");
						//p.write(new DisplayInstancedMap(after.get(), p));
					}
				} else {
					p.teleport(Math.max(0, Math.min(16000, Integer.parseInt(args[0]))),
							Math.max(0, Math.min(16000, Integer.parseInt(args[1]))), args.length > 2 ? Integer.parseInt(args[2]) : 0);
				}
			}
		}, "Teleports to an x/y/z location. ::tele x y height");
		
		put(Privilege.ADMIN, "region", (p, args) -> {
			p.interfaces().closeMain();
			String[] params = args[0].split(",");
			int id = Integer.parseInt(params[0]);
			p.teleport(new Tile((id >> 8) << 6, (id & 0xFF) << 6));
		}, "Teleport to a specific region. ::region regionid");
		
		put(Privilege.ADMIN, "chunk", (p, args) -> {
			p.interfaces().closeMain();
			String[] params = args[0].split(",");
			int id = Integer.parseInt(params[0]);
			p.teleport(Tile.chunkToTile(id));
			p.message("Teleported to chunk %d corner %s", id, Tile.chunkToTile(id));
		}, "Teleport to a specified chunk. ::chunk cunkid");
		
		put(Privilege.ADMIN, "ralloc", (p, args) -> {
			InstancedMap.test(p);
		}, "Test instanced maps. ::realloc");
		
		put(Privilege.ADMIN, "sync", (p, args) -> {
			p.animate(Integer.parseInt(args[0]));
			p.graphic(Integer.parseInt(args[1]));
		}, "Preform animation and graphics at the same time. ::sync animid graphicsid");
		
		put(Privilege.ADMIN, "anim", (p, args) -> p.animate(Integer.parseInt(args[0])), "Preforms the specified animation. ::anim animid");
		
		put(Privilege.ADMIN, "gfx", (p, args) -> p.graphic(Integer.parseInt(args[0]), args.length > 1 ? Integer.parseInt(args[1]) : 0, 0), "Preforms the specified graphics. ::gfx graphicsid *delay*");
		
		put(Privilege.ADMIN, "worldgfx", (p, args) -> p.world().tileGraphic(Integer.parseInt(args[0]), p.tile(), 0, 0), "Preforms the specified graphics on your tile. ::worldgfx graphicsid");
		
		put(Privilege.ADMIN, "pc", (player, arguments) -> {
			if (PestControl.INSTANCE.getEnabled()) {
				Tile tile = LanderType.NOVICE.getOutside();
				
				player.teleport(tile);
			}
		}, "Teleports you to pest control. ::pc");
		put(Privilege.ADMIN, "pcquick", (player, arguments) -> {
			if (PestControl.INSTANCE.getEnabled()) {
				PestControl.INSTANCE.getLanders().get(PestControlDifficulty.NOVICE).setWaitCycles(10);
				player.message("Novice boat leaving in 10 ticks.");
			}
		}, "Makes the Novice lander start in 10 ticks. ::pcquick");
		
		put(Privilege.ADMIN, "pctoggle", (player, arguments) -> {
			boolean enabled = PestControl.INSTANCE.getEnabled();
			
			PestControl.INSTANCE.setEnabled(!enabled);
			player.message("Pest control mini-game is now " + (enabled ? "disabled" : "enabled") + ".");
		}, "Toggle pest control mini-game. ::pctoggle");
		
		put(Privilege.ADMIN, "pcpoints", (player, arguments) -> {
			player.putattrib(AttributeKey.PEST_CONTROL_POINTS, 1000);
		}, "Sets your pest control points to 1000. ::pcpoints");
		
		put(Privilege.PLAYER, "fillbank", (player, arguments) -> {
			player.bank().add(new Item(4587, 20000)); // Scim
			player.bank().add(new Item(1215, 20000)); // Dagger
			player.bank().add(new Item(4089, 20000)); // Mystic
			player.bank().add(new Item(4109, 20000)); // Mystic
			player.bank().add(new Item(4099, 20000)); // Mystic
			player.bank().add(new Item(7400, 20000)); // Enchanted
			player.bank().add(new Item(3755, 20000)); // farseer helm
			player.bank().add(new Item(1163, 20000)); // rune full helm
			player.bank().add(new Item(1305, 20000)); // d long
			player.bank().add(new Item(4675, 20000)); // ancient staff
			player.bank().add(new Item(4091, 20000)); // Mystic
			player.bank().add(new Item(4111, 20000)); // Mystic
			player.bank().add(new Item(4101, 20000)); // Mystic
			player.bank().add(new Item(7399, 20000)); // enchanted
			player.bank().add(new Item(3751, 20000)); // hat
			player.bank().add(new Item(1127, 20000)); // rune
			player.bank().add(new Item(1434, 20000)); // mace
			player.bank().add(new Item(9185, 20000)); // crossbow
			player.bank().add(new Item(4093, 20000)); // Mystic
			player.bank().add(new Item(4113, 20000)); // Mystic
			player.bank().add(new Item(4103, 20000)); // Mystic
			player.bank().add(new Item(7398, 20000)); // enchanted
			player.bank().add(new Item(3753, 20000)); // helm
			player.bank().add(new Item(1079, 20000)); // rune
			player.bank().add(new Item(5698, 20000)); // dagger
			player.bank().add(new Item(10499, 20000)); // avas
			player.bank().add(new Item(4097, 20000)); // Mystic
			player.bank().add(new Item(4117, 20000)); // Mystic
			player.bank().add(new Item(4107, 20000)); // Mystic
			player.bank().add(new Item(2579, 20000)); // wiz boots
			player.bank().add(new Item(3749, 20000)); // helm
			player.bank().add(new Item(4131, 20000)); // rune boots
			player.bank().add(new Item(2503, 20000)); // hides
			player.bank().add(new Item(2497, 20000)); // hides
			player.bank().add(new Item(12492, 20000)); // hides
			player.bank().add(new Item(12508, 20000)); // hides
			player.bank().add(new Item(12500, 20000)); // hides
			player.bank().add(new Item(3105, 20000)); // climbers
			player.bank().add(new Item(1093, 20000)); // rune
			player.bank().add(new Item(1201, 20000)); // rune
			player.bank().add(new Item(3842, 20000)); // god book
			player.bank().add(new Item(12612, 20000)); // god book
			player.bank().add(new Item(12494, 20000)); // hides
			player.bank().add(new Item(12510, 20000)); // hides
			player.bank().add(new Item(12502, 20000)); // hides
			player.bank().add(new Item(6108, 20000)); // ghostly
			player.bank().add(new Item(6107, 20000)); // ghostly
			player.bank().add(new Item(6109, 20000)); // ghostly
			player.bank().add(new Item(2436, 20000)); // pots
			player.bank().add(new Item(2440, 20000)); // pots
			player.bank().add(new Item(2442, 20000)); // pots
			player.bank().add(new Item(2444, 20000)); // pots
			player.bank().add(new Item(3040, 20000)); // pots
			player.bank().add(new Item(10925, 20000)); // pots
			player.bank().add(new Item(3024, 20000)); // pots
			player.bank().add(new Item(6685, 20000)); // pots
			player.bank().add(new Item(145, 20000)); // pots
			player.bank().add(new Item(157, 20000)); // pots
			player.bank().add(new Item(163, 20000)); // pots
			player.bank().add(new Item(169, 20000)); // pots
			player.bank().add(new Item(3042, 20000)); // pots
			player.bank().add(new Item(10927, 20000)); // pots
			player.bank().add(new Item(3026, 20000)); // pots
			player.bank().add(new Item(6689, 20000)); // pots
			player.bank().add(new Item(147, 20000)); // pots
			player.bank().add(new Item(159, 20000)); // pots
			player.bank().add(new Item(165, 20000)); // pots
			player.bank().add(new Item(171, 20000)); // pots
			player.bank().add(new Item(3044, 20000)); // pots
			player.bank().add(new Item(10929, 20000)); // pots
			player.bank().add(new Item(3028, 20000)); // pots
			player.bank().add(new Item(6687, 20000)); // pots
			player.bank().add(new Item(149, 20000)); // pots
			player.bank().add(new Item(161, 20000)); // pots
			player.bank().add(new Item(167, 20000)); // pots
			player.bank().add(new Item(173, 20000)); // pots
			player.bank().add(new Item(3046, 20000)); // pots
			player.bank().add(new Item(10931, 20000)); // pots
			player.bank().add(new Item(3030, 20000)); // pots
			player.bank().add(new Item(6691, 20000)); // pots
			player.bank().add(new Item(385, 20000)); // sharks
			player.bank().add(new Item(3144, 20000)); // karambwan
			player.bank().add(new Item(560, 20000000)); // runes
			player.bank().add(new Item(565, 20000000)); // runes
			player.bank().add(new Item(555, 20000000)); // runes
			player.bank().add(new Item(562, 20000000)); // runes
			player.bank().add(new Item(557, 20000000)); // runes
			player.bank().add(new Item(559, 20000000)); // runes
			player.bank().add(new Item(564, 20000000)); // runes
			player.bank().add(new Item(554, 20000000)); // runes
			player.bank().add(new Item(9075, 20000000)); // runes
			player.bank().add(new Item(556, 20000000)); // runes
			player.bank().add(new Item(563, 20000000)); // runes
			player.bank().add(new Item(559, 20000000)); // runes
			player.bank().add(new Item(566, 20000000)); // runes
			player.bank().add(new Item(561, 20000000)); // runes
			player.bank().add(new Item(9241, 20000)); // bolts
			player.bank().add(new Item(9244, 20000)); // bolts
			player.bank().add(new Item(9245, 20000)); // bolts
			player.bank().add(new Item(9243, 20000)); // bolts
			player.bank().add(new Item(9242, 20000)); // bolts
			player.bank().add(new Item(892, 20000)); // rune arrows
			player.bank().add(new Item(10828, 20000)); // neit helm
			player.bank().add(new Item(2412, 20000)); // sara god cape
			player.bank().add(new Item(8013, 20000)); // home teleport tab
			player.bank().add(new Item(7458, 20000)); // mithril gloves for pures
			player.bank().add(new Item(7462, 20000)); // gloves
			player.bank().add(new Item(11978, 20000)); // glory (6)
		}, "Fills your bank with starting items ::fillbank");

		put(Privilege.ADMIN, "icon", (p, args) -> {
		    int icon = Integer.parseInt(args[0]);
		    p.message("default_icon: to "+ icon);
		    p.defaultIcon(icon);
		    p.looks().update();
        });
		
		put(Privilege.PLAYER, "yell", (p, args) -> {
			int icon = calculateYellIcon(p.calculateBaseIcon());
			int timer = calculateYellTimer(p);
			if ((int) p.id() == 141462) timer = 1; // Test yelling on normal accs
			DonationTier rank = p.donationTier();
			
			//Is our player muted?
			if (p.muted()) {
				p.message("You can't use this command while muted.");
				return;
			}

			if ((Boolean) p.attribOr(AttributeKey.YELL_DISABLED, false)) {
				p.message("Your yelling privilege has been revoked by staff. Contact staff for further details.");
				return;
			}
			
			//we don't want to base the donator check off of a players icon, since their base icon may infact be something
			//other than a donator icon (iron man, staff, etc)
			//if(p.privilege() == Privilege.PLAYER && (p.donationTier() == DonationTier.CUSTOM || !(icon >= 4 && icon <= 8))) {
			boolean hasYellableIcon = p.calculateBaseIcon() == 10 || (p.calculateBaseIcon() >= 15 && p.calculateBaseIcon() <= 21);
			if (p.privilege() == Privilege.PLAYER && rank == DonationTier.NONE && !p.helper() && !hasYellableIcon && !p.name().equalsIgnoreCase("mack")) {
				p.message("You need to be a donator to use this feature.");
				return;
			}
			
			//Else we must be a privileged user so we..
			if ((p.privilege() == Privilege.PLAYER && !p.helper()) && p.timers().has(TimerKey.YELL) && !p.name().equalsIgnoreCase("mack")) {
				p.message("You need to wait %s seconds before doing this again!", ((int) (0.6 * p.timers().left(TimerKey.YELL))));
				return;
			}
			
			if (p.attribOr(AttributeKey.YELL_CHANNEL_DISABLED, false) == Boolean.TRUE) {
				p.message("You can't submit a yell with the 'yell channel' disabled.");
				return;
			}
			
			String yellmsg = p.privilege().eligibleTo(Privilege.ADMIN) ? glue(args) : L10n.replaceTags(glue(args));
			String msg = String.format("<col=0000ff>[<img=%s> %s]</col> %s", icon == 9 ? 17 : icon == 3 ? 2 : icon == 4 ? 3 : p.helper() ? 36 : icon, p.name(), yellmsg);
			
			boolean checkIgnores = YELL_ALLOWS_IGNORE_LIST_FILTER && p.privilege() == Privilege.PLAYER && !p.helper();
			
			boolean[] ignoreYell = new boolean[1];
			p.world().players().forEach(p2 -> {
				if (p.shadowMuted() && p2.id() != p.id()) {
					ignoreYell[0] = true;
				}
				if (checkIgnores) {
					p2.social().ignores().forEach(ignore -> {
						if (ignore != null && ignore.id == (int) p.id()) {
							ignoreYell[0] = true;
						}
					});
				}
				if (!ignoreYell[0]) {
					if (p2.attribOr(AttributeKey.YELL_CHANNEL_DISABLED, false) == Boolean.FALSE) {
						p2.filterableMessage(msg);
					}
				} else {
					ignoreYell[0] = false;
				}
			});
			p.timers().addOrSet(TimerKey.YELL, timer);
		}, "Sends a message to the entire server. ::yell message");
		
		put(Privilege.ADMIN, "crownlist", (p, args) -> {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 30; i++)
				sb.append("<img=" + i + "> ");
			StringBuilder sb2 = new StringBuilder();
			for (int i = 28; i < 60; i++)
				sb2.append("<img=" + i + "> ");
			if (p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
				p.world().broadcast(sb.toString());
				p.world().broadcast(sb2.toString());
			} else {
				p.message(sb.toString());
				p.message(sb2.toString());
			}
		}, "Lists the current support crowns. ::crownlist");
		
		put(Privilege.ADMIN, "runscript", (p, args) -> p.write(new InvokeScript(Integer.parseInt(args[0]), (Object[]) Arrays.copyOfRange(args, 1, args.length)))
				, "Tests a client script. ::runscript scriptid args");
		
		put(Privilege.ADMIN, "up", (p, args) -> p.teleport(p.tile().x, p.tile().z, Math.min(3, p.tile().level + 1)), "Increments your player height by 1. ::up");
		
		put(Privilege.ADMIN, "down", (p, args) -> p.teleport(p.tile().x, p.tile().z, Math.max(0, p.tile().level - 1)), "Decrements your player height by 1. ::down");
		
		put(Privilege.ADMIN, "scripts", (p, args) -> new Thread(() -> {
			long l = System.currentTimeMillis();
			p.world().server().scriptRepository().load();
			p.message("Took %d to reload scripts.", System.currentTimeMillis() - l);
		}).start(), "Reload all the scripts. ::scripts");

		put(Privilege.ADMIN, "disablerefunds", (p, args) -> {
			RefundHandlerNpc.setEnabled(false);
			p.message("Refunds are now disabled.");
		});
		put(Privilege.ADMIN, "enablerefunds", (p, args) -> {
			RefundHandlerNpc.setEnabled(true);
			p.message("Refunds are now disabled.");
		});

		put(Privilege.ADMIN, "reloadhelp", (p, args) -> {
			Help.loadHelp();
			p.message("Reloaded help configuration.");
		});
		
		put(Privilege.ADMIN, "dz", (p, args) -> p.teleport(1678, 3495, 1), "Teleports you to the donator zone. ::dz");
		
		put(Privilege.ADMIN, "clipinfo", (p, args) -> p.message("Current clip: %s", Arrays.deepToString(p.world().clipSquare(p.tile(), 5))), "Gets the clip information at your current tile. ::clipinfo");
		
		put(Privilege.ADMIN, "worldtimernow", (p, args) -> {
			for (TimerKey t : TimerKey.values()) {
				if (t.name().equalsIgnoreCase(args[0])) {
					int oldTicks = p.world().timers().left(t);
					p.world().timers().register(t, 10);
					p.message("Set timer to 10 ticks. Was at " + oldTicks + " ticks.");
					return;
				}
			}
			
			p.message("Cannot find timer.");
		});
		
		for (String s : new String[]{"interface", "face"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				int id = Integer.parseInt(args[0]);
				if (id == -1) {
					p.message("Closing "+p.interfaces().activeMain());
					p.interfaces().closeMain();
					return;
				}
				
				if (args.length > 1) {
					int child = Integer.parseInt(args[1]);
					boolean visible = Boolean.parseBoolean(args[2]);
					
					p.interfaces().sendWidgetOn(id, Interfaces.InterSwitches.T);
					p.write(new InterfaceVisibility(id, child, visible));
					p.message("Sending %s id and child %s", id, child);
				} else {
					p.interfaces().sendMain(Integer.parseInt(args[0]), false);
					p.message("Sending %s id", id);
				}
			}, "Show the specified interface. ::interface interfaceid *child* *visible*");
		}
		for (String s : new String[]{"over"}) { // A click through interface so you can move while it changes.. like wildy level.
			put(Privilege.ADMIN, s, (p, args) -> {
				int id = Integer.parseInt(args[0]);
				p.interfaces().send(Integer.parseInt(args[0]), p.interfaces().activeRoot(), p.interfaces().mainComponent(), true);
				p.message("Sending %s id", id);
			}, "Show a specified interface overlay. ::over overlayid");
		}
		
		put(Privilege.ADMIN, "setrealm", (p, args) -> p.write(new SetRealm(Integer.parseInt(args[0]))), "Set your specified realm id. ::setrealm realmid");
		
		put(Privilege.ADMIN, "rootwindow", (p, args) -> p.interfaces().sendRoot(Integer.parseInt(args[0])), "Sets the root window. ::rootwindow windowid");
		
		put(Privilege.ADMIN, "close", (p, args) -> p.interfaces().closeMain(), "Closes your main interface. ::close");
		
		put(Privilege.ADMIN, "lastchild", (p, args) -> p.message("Last child of %s is %d.", args[0], p.world().server().store().getIndex(3).getDescriptor().getLastFileId(Integer.parseInt(args[0]))), "Gets the last child of the specified interfaceid. ::lastchild interfaceid");
		put(Privilege.ADMIN, "lastinter", (p, args) -> p.message("Last interface is %d.", p.world().server().store().getIndex(3).getDescriptor().getLastArchiveId()), "Gets the last interface id.");

		put(Privilege.ADMIN, "music", (p, args) -> p.write(new PlayMusic(Integer.parseInt(args[0]))), "Plays the specified song. ::music songid");
		
		put(Privilege.ADMIN, "item", (p, args) -> {
			if (!p.privilege().eligibleTo(Privilege.ADMIN)) {
				if (!p.world().realm().isDeadman()) {
					// No feedback. go away.
					return;
				} else if (!p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
					// Not a dmm test server.
					return;
				}
			}
			if (!p.name().equalsIgnoreCase("maple leaf") && (p.privilege().eligibleTo(Privilege.ADMIN) || p.world().realm().isDeadman())) { // Check for admin too for security
				int itemId = Integer.parseInt(args[0]);
				int amount = 1;
				ItemDefinition def = p.world().definitions().get(ItemDefinition.class, itemId);
				if (args.length > 1) {
					if (args[1].equalsIgnoreCase("max")) {
						amount = Integer.MAX_VALUE;
					} else if (args[1].endsWith("k")) {
						amount = Integer.parseInt(args[1].substring(0, args[1].length() - 1)) * 1000;
					} else if (args[1].endsWith("m")) {
						amount = Integer.parseInt(args[1].substring(0, args[1].length() - 1)) * 1000000;
					} else if (args[1].endsWith("b")) {
						amount = Integer.parseInt(args[1].substring(0, args[1].length() - 1)) * 1000000000;
					} else {
						amount = Integer.parseInt(args[1]);
					}
				}
				int MAX = p.privilege().eligibleTo(Privilege.ADMIN) ? Integer.MAX_VALUE : 1000; // Cap max stackables to 1k for players
				Item spawnedItem = new Item(itemId, Math.min(MAX, amount));
				p.inventory().add(spawnedItem, true);
				p.message("Added item "+ itemId +" to inventory. [tradable="+ spawnedItem.tradable(p.world()) +", rawtrade="+ (!spawnedItem.rawtradable(p.world()) +", lostUntrade="+ (!spawnedItem.lostUntradable(p.world()))) +"]");
				WildernessLevelIndicator.updateCarriedWealthProtection(p);
			}
		}, "Spawns the specified item. ::item id *amount*");
		
		put(Privilege.ADMIN, "varp", (p, args) -> {
			p.varps().varp(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
			p.message("Varp %d value now %d", Integer.parseInt(args[0]), p.varps().varp(Integer.parseInt(args[0])));
		}, "Set the specified varp value. ::varp id value");

		put(Privilege.ADMIN, "setrights", (p, args) -> {
			String playerName = args[0];
			Optional<Player> targetAvailable = p.world().playerByName(playerName);
			if (Objects.isNull(targetAvailable) || !targetAvailable.isPresent()) {
				p.message("Error! Player by the name of '"+ playerName +"' is offline or does not exist.");
				return;
			}
			int rights = Integer.parseInt(args[1]);
			Player target = targetAvailable.get();
			switch (rights) {
				case 0:
					target.privilege(Privilege.PLAYER);
					target.looks().update();
					p.message("You have successfully set '"+ playerName +"' rights to Player.");
					break;
				case 1:
					target.ironMode(IronMode.REGULAR);
					target.looks().update();
					p.message("You have successfully set '"+ playerName +"' rights to Ironman.");
					break;
				case 2:
					target.ironMode(IronMode.ULTIMATE);
					target.looks().update();
					p.message("You have successfully set '"+ playerName +"' rights to Ultimate Ironman.");
					break;
				case 3:
					target.ironMode(IronMode.HARDCORE);
					target.looks().update();
					p.message("You have successfully set '"+ playerName +"' rights to Hardcore Ironman.");
					break;
			}
		}, "Looks up a player by their username and sets the specified rights. Usage: playername-rights");
		
		put(Privilege.ADMIN, "varbit", (p, args) -> {
			int id = Integer.parseInt(args[0]);
			int val = Integer.parseInt(args[1]);
			if (id == 2168 && val != 0) {
				p.message("THIS IS A DUMMY JAGEX SPASTIC RETARD VARBIT! DONT USE! USE 2172/5363");
				return;
			}
			p.varps().varbit(id, val);
			p.message("Varbit %d value now %d", id, p.varps().varbit(Integer.parseInt(args[0])));
		}, "Set the specified varp value. ::varp id value");
		
		put(Privilege.ADMIN, "enums", (p, args) -> {
			int enumId = Integer.parseInt(args[0]);
			int v2 = args.length >= 2 ? Integer.parseInt(args[1]) : 0;
			EnumDefinition e = p.world().definitions().get(EnumDefinition.class, enumId);
			e.enums().values().forEach(ee -> {
				p.message("Enum: %s %s", ee, e.getInt(v2));
			});

			if (p.name().equalsIgnoreCase("simplemind")) {
			    System.out.println(p.world().definitions().get(EnumDefinition.class, enumId).enums().toString());
            }
		}, "Shows the specified enum definition. ::enums id *value*");
		
		put(Privilege.ADMIN, "varpof", (p, args) -> {
			int varpbit = Integer.parseInt(args[0]);
			p.message("Varbit " + varpbit + " belongs to varp " + p.world().definitions().get(VarbitDefinition.class, varpbit).varp + ".");
		}, "Find parent varp. ::varpof varp")
		;
		put(Privilege.ADMIN, "getvarbit", (p, args) -> p.message("Varbit %s value: %d.", args[0], p.varps().varbit(Integer.parseInt(args[0]))), "Get varbit value. ::getvarbit id");
		put(Privilege.ADMIN, "getvarp", (p, args) -> p.message("Varp %s value: %d.", args[0], p.varps().varp(Integer.parseInt(args[0]))), "Get varp value. ::getvarp id");
		
		put(Privilege.ADMIN, "varpsfor", (p, args) -> {
			StringBuilder sb = new StringBuilder();
			int varp = Integer.parseInt(args[0]);
			int found = 0;
			for (int i = 0; i < p.world().definitions().total(VarbitDefinition.class); i++) {
				VarbitDefinition def = p.world().definitions().get(VarbitDefinition.class, i);
				if (def.varp == varp) {
					int coverage = Math.max(1, def.endbit-def.startbit);
					int maxvalue = coverage == 1 ? 1 : (int)(Math.pow(2, coverage));
					sb.append(String.format("Varbit <col=0B610B>%s</col> is %s to %s .. for %s bits with max value <col=0B610B>%s</col>", i, def.startbit, def.endbit, coverage, maxvalue)+"%n");
					found++;
				}
			}
			p.message("<col=FF0000>Found %d varbits for varp %d", found, varp);
			String res = sb.toString();
			for (String s : res.split("%n")) {
				p.message(s);
			}
		}, "Finds all varbits linked to the given varp ID. ::varpsfor varpid");
		
		put(Privilege.MODERATOR, "hp", (p, args) -> {
			if (p.privilege().eligibleTo(Privilege.ADMIN) || (!p.privilege().eligibleTo(Privilege.ADMIN) && p.world().realm().isPVP() && GameCommands.HPCMD_ENABLED)) {
				p.world().tileGraphic(110, p.tile(), 110, 0);
				p.heal(99);
			} else {
				p.message("Nope.");
			}
		}, "Heals your player. ::hp");
		
		for (String s : new String[]{"hide", "show", "vis", "invis"}) {
			put(Privilege.MODERATOR, s, (p, args) -> {
				if (p.privilege() == Privilege.ADMIN || p.seniorModerator()) {
					p.looks().hide(Boolean.parseBoolean(args[0]));
					p.looks().update();
					p.message("You are now %s.", p.looks().hidden() ? "hidden" : "exposed");
				} else {
					p.message("You do not have permission to use this command.");
				}
			}, "Hides your player. ::hide true/false");
		}
		for (String s : new String[] {"grounditems", "clearfloor", "wipefloor", "emptyfloor"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				new ArrayList<>(p.world().groundItems()).forEach(g -> p.world().removeGroundItem(g));
				p.world().loadItemSpawns(new File("data/map/items"));
				p.message("Cleared floor.");
			}, "Reload ground item spawns. ::grounditems");
		}
		put(Privilege.ADMIN, "clearrespawns", (p, args) -> {
			final int[] count = new int[1];
			new ArrayList<>(p.world().groundItems()).stream().filter(g -> g.respawns()).forEach(g -> {if (p.world().removeGroundItem(g)) count[0]++; });
			p.world().loadItemSpawns(new File("data/map/items"));
			p.message("Respawned %s respawnables", count[0]);
		}, "Reloads grounditems which are world respawnables only ::clearrespawns");
		
		put(Privilege.ADMIN, "moneyspent", (p, args) -> {
			if (p.name().equalsIgnoreCase("Simplemind")) {
				int amount = Integer.parseInt(args[0]);
				p.totalSpent(amount);
				p.message("You have set your money spent to "+ amount +".");
			}
		}, "Changes your donator status to the type entered.");
		
		put(Privilege.ADMIN, "give", (p, args) -> {
			if (!p.name().equalsIgnoreCase("maple leaf")) {
				if (p.privilege() != Privilege.ADMIN && p.tile().z > 3520 && p.tile().z < 3972) {
					p.message("You cannot spawn items while standing in the wilderness.");
					return;
				}
				
				String name = args[0];
				boolean noted = name.startsWith("noted_");
				name = name.replaceAll("noted_", "");
				for (int i = 0; i < p.world().definitions().total(ItemDefinition.class); i++) {
					ItemDefinition def = p.world().definitions().get(ItemDefinition.class, i);
					if (def != null) {
						String n = def.name;
						n = n.replaceAll(" ", "_");
						n = n.replaceAll("\\(", "");
						n = n.replaceAll("\\)", "");
						if (n.equalsIgnoreCase(name)) {
							Item item = new Item(i, args.length > 1 ? Integer.parseInt(args[1]) : 1);
							if (noted) item = item.note(p.world());
							p.inventory().add(item, true);
							break;
						}
					}
				}
			}
		}, "Gives yourself item. ::give itemname *amount*");
		
		put(Privilege.ADMIN, "gc", (p, args) -> System.gc(), "Force garbage collection. ::gc");
		
		put(Privilege.ADMIN, "npc", (p, args) -> {
			if (!p.name().equalsIgnoreCase("maple leaf") && ((p.world().realm().isDeadman() && p.world().server().config().hasPathOrNull("deadman.jaktestserver"))
					|| p.privilege().eligibleTo(Privilege.ADMIN))) {
				Npc npc = new Npc(Integer.parseInt(args[0]), p.world(), p.tile()).respawns(false);
				int walkrad = args.length >= 2 ? Integer.parseInt(args[1]) : 0;
				npc.walkRadius(walkrad);
				p.world().registerNpc(npc);
				p.message("Spawned %s index %s type %s", npc.def().name, npc.index(), npc.id());
				
				if (Pest.SPLATTER.getIds().contains(Integer.parseInt(args[0]))) {
					Optional<InstancedMap> activemap = p.world().allocator().active(npc.tile());
					
					if (activemap.isPresent()) {
						npc.world().server().scriptExecutor().executeScript(npc, script -> {
							Optional<Map.Entry<PestControlDifficulty, Optional<PestControlGame>>> game =
									PestControl.INSTANCE.getGames().entrySet().stream().filter(it ->
											it.getValue().isPresent()).filter(it -> it.getValue().get()
											.getMembers().contains(p)).findAny();
							
							game.ifPresent(g -> {
								Splatter.INSTANCE.search(g.getValue().get().getKnight(), script, activemap.get().center());
							});
							return script;
						});
					}
				}
			}
		}, "spawns a npc. ::npc id *walkradius*");
		
		put(Privilege.ADMIN, "showgrounditems", (p, args) -> {
			p.world().groundItems().forEach(i -> i.forceBroadcast(true));
		}, "Forces all ground items to show globally. ::showgrounditems");
		
		put(Privilege.ADMIN, "debugnpcs", (p, args) -> {
			HashMap<String, Integer> counts = new HashMap<>();
			p.world().npcs().forEach(npc -> {
				Integer count = counts.getOrDefault(npc.spawnStack, 0);
				counts.put(npc.spawnStack, (count + 1));
			});
			Misc.sortByValue(counts, true);
			
			System.out.println("NPC STACKS:");
			counts.forEach((stack, count) -> System.out.println("    " + stack + ": " + count));
		}, "TODO. ::debugnpcs");
		
		put(Privilege.ADMIN, "tstack", (p, args) -> {
			for (int i = 0; i < 5; i++) {
				Npc n = new Npc(FightCaveMonsters.INSTANCE.getKET_ZEK(), p.world(), p.tile().transform(0, 5));
				p.world().registerNpc(n);
			}
		}, "Test npc stacking.");
		put(Privilege.ADMIN, "tstack2", (p, args) -> {
			for (int i = 0; i < 5; i++) {
				Npc n = new Npc(3078, p.world(), p.tile().transform(0, 5 + (5 - 3)));
				n.combatInfo().stats.hitpoints = 100;
				n.hp(100, 100);
				n.combatInfo().unstacked = true;
				p.world().registerNpc(n);
			}
		}, "Test npc stacking.");
		
		put(Privilege.ADMIN, "musicbyname", (p, args) -> {
			String name = glue(args).toLowerCase();
			int id = p.world().server().store().getIndex(6).getContainerByName(name).getId();
			p.message("%s resolves to %d.", name, id);
			p.write(new PlayMusic(id));
		}, "Plays music by name. ::musicbyname songname");
		
		put(Privilege.ADMIN, "alwayshit", (p, args) -> {
			tryOrUsage(p, "Usage: ::alwayshit value", () -> {
				int hit = Integer.parseInt(args[0]);
				if (hit > 0) {
					p.putattrib(AttributeKey.ALWAYS_HIT, hit);
					p.message("You will always hit " + hit + " damage.");
				} else {
					p.clearattrib(AttributeKey.ALWAYS_HIT);
					p.message("You will no longer hit constant damage.");
				}
			});
		}, "Will make all your damage hit a specific value. ::alwayshit 99");

		put(Privilege.ADMIN, "ipillhp", (p, args) -> {
			InfernoSession session = p.attrib(AttributeKey.INFERNO_SESSION);
			session.getActivePillars().forEach((k, v) -> {
				k.hp(5, 0);
			});
		}, "Fast start inferno");
		put(Privilege.ADMIN, "fastinf", (p, args) -> {
			InfernoSession session = new InfernoSession(Integer.parseInt(args[0]), p, p.world());
			session.start(Integer.parseInt(args[0]), false);
		}, "::fastinf [wave].");
		for (String s : new String[]{"on", "debugon"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.putattrib(AttributeKey.DEBUG, true);
				p.message("Debugging information is now on.");
			}, "Turns on debug mode. ::debugon");
		}

		for (String s : new String[]{"off", "debugoff"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.putattrib(AttributeKey.DEBUG, false);
				p.message("Debugging information is now off.");
			}, "Turns off debug mode. ::debugoff");
		}
		
		put(Privilege.PLAYER, "char", (p, args) -> {
			if (p.privilege().eligibleTo(Privilege.ADMIN) || p.world().realm().isDeadman())
				p.interfaces().sendMain(269); // makeover mage character setup
		}, "Open makeover mage interface. ::char");
		
		put(Privilege.PLAYER, "randomchar", (p, args) -> {
			p.looks().randomize();
		}, "Open makeover mage interface. ::char");
		
		put(Privilege.ADMIN, "pets", (p, args) -> {
			Pet[] pets = Pet.values();
			for (int i = 0; i < pets.length; i++) {
				p.inventory().add(new Item(pets[i].getItem()), false);
			}
		}, "Gives you all the pets. ::pets");
		
		put(Privilege.ADMIN, "pets2", (p, args) -> {
			Pet[] pets = Pet.values();
			for (int i = 28; i < pets.length; i++) {
				p.inventory().add(new Item(pets[i].getItem()), false);
			}
		}, "Gives you all the pets. ::pets2");
		
		put(Privilege.ADMIN, "idef", (p, args) -> {
			int id = Integer.parseInt(args[0]);
			ItemDefinition def = new Item(id).definition(p.world());
			String opts = def.options == null ? "" : Arrays.toString(def.options);
			String iopts = def.ioptions == null ? "" : Arrays.toString(def.ioptions);
			p.message("Def %d options: %s %s", id, opts, iopts);
		}, "Shows interface definitions. ::idef interfaceid");

		put(Privilege.ADMIN, "resetxptrack", (p, args) -> {
			for (int i = 1252; i < 1276; i++)
				p.varps().varp(i, 0);
			for (int i = 1228; i < 1256; i++)
				p.varps().varp(i, 0);
		});
		put(Privilege.ADMIN, "tr", (p, args) -> {
			CombinationRunes.findComboRuneUses(p, new Item[] {new Item(554, 5), new Item(556, 7), new Item(555, 3), new Item(560, 5)});
		});
		put(Privilege.ADMIN, "tr2", (p, args) -> {
			long start = System.currentTimeMillis();
			GameCommands.process(p, "empty");
			for (Item i : new Item[] {new Item(560, 1000), new Item(554, 1000)})
				p.inventory().add(i);
			GameCommands.process(p, "combos");
			p.message("----------- TEST 1 -----------");
			// Fire blast 554x5 fire, 556x4 air, 560x1 death
			// Inventory: 1k FIRES + 1k of all combo runes + 1k deaths
			MagicCombat.has(p, new Item[]{new Item(554, 5), new Item(556, 4), new Item(560, 1)}, true);
			// expected combos used: smoke x4 (4 fire + 4 water)
			// afterwards remaining: 1 fire, 1 death to remove -> both deleted as normal

			// TEST 2
			GameCommands.process(p, "empty");
			for (Item i : new Item[] {new Item(560, 1000)})
				p.inventory().add(i);
			GameCommands.process(p, "combos");
			p.message("");
			p.message("----------- TEST 2 -----------");
			long sub2 = System.nanoTime();
			MagicCombat.has(p, new Item[]{new Item(554, 5), new Item(556, 4), new Item(560, 1)}, true);
			// EXPECTED:
			// 4xsmoke deleted, 1 fire+death remaining -> death deleted -> x1 steam rune deleted (first combo with FIRE in found in inv cos no fire runes)
			p.message("Tests took "+(System.currentTimeMillis() - start)+"ms and test 2 took "+(System.nanoTime() - sub2)+"ns");
		});
		put(Privilege.ADMIN, "combos", (p, args) -> {
			for (int i = 4694; i <= 4699; i++)
				p.inventory().add(new Item(i, 1000));
		});
		put(Privilege.ADMIN, "testtrades", (p, args) -> {
			W2tradables.verify(p.world());
		});
		put(Privilege.ADMIN, "testant", (p, args) -> {
			p.putattrib(AttributeKey.ANTIFIRE_POTION, 4);
			p.message("done");
		});
		put(Privilege.ADMIN, "heal", (p, args) -> {
			p.skills().resetStats();
			p.setRunningEnergy(100, true);
			p.message("Your stats have been reset, your run replenished and your poison cured.");
			Poison.cure(p);
			Venom.cure(2, p);
		}, "Heals your player and cures poison. ::heal");

		put(Privilege.ADMIN, "spear", (p, args) -> {
			Optional<Player> op = p.world().playerByName(glue(args));
			if (!op.isPresent()) {
				p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
			} else {
				Player targ = op.get();
				// First time
				targ.pathQueue().clear();
				targ.lockDelayDamage();
				targ.graphic(254, 100, 0);
				PlayerCombat.dragonSpearMovement(Direction.East, targ);
				targ.timers().extendOrRegister(TimerKey.SPEAR, 5);

				// Now accumulate
				Queue<Direction> moves = new LinkedList<Direction>(Arrays.asList(Direction.East, Direction.East, Direction.East));
				targ.putattrib(AttributeKey.SPEAR_MOVES, moves);
				p.message("Done on "+targ.name());
			}
		});

		put(Privilege.ADMIN, "gojad", (p, args) -> {
			p.putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, Integer.parseInt(args[0]));
			p.executeScript(s -> {
				FightCaveGame.initiateFightCaves(s, false);
				return s;
			});
		}, "Inserts you into the Fight Caves mingame on wave x");
		
		put(Privilege.ADMIN, "pt", (p, args) -> {
			p.message(Hans.getTimeDHS(p));
		}, "Playtime in short");
		
		put(Privilege.ADMIN, "looters", (p, args) -> {
			PkBots.initGlobalLooterBots(p.world());
		}, "Reinit looter bots");
		
		put(Privilege.ADMIN, "pvpspawns", (p, args) -> {
			PkBots.spawnGitems(p.world());
		}, "Spawns respawnable gitems in pvp");
		
		put(Privilege.ADMIN, "fbots", (p, args) -> {
			int id = Integer.parseInt(args[0]);
			switch (id) {
				case 1:
					PkBots.initFollowerBots(p.world());
					break;
				case 2:
					PkBots.initFollowerBots(p.world(), PVPAreas.getVarrockSafe(), 7, PVPAreas.getVarrock_map());
					break;
				case 3:
					PkBots.initFollowerBots(p.world(), PVPAreas.getEnter_tile_canifis(), 7, PVPAreas.getCanifis_map());
					break;
				case 4:
					PkBots.initFollowerBots(p.world(), PVPAreas.getCAMELOT_PVP_ENTER_TILE(), 7, PVPAreas.getCAMELOT_INSTANCE());
					break;
				case 5:
					PkBots.initFollowerBots(p.world(), PVPAreas.getGE_ENTER_TILE(), 7, PVPAreas.getGE_MAP());
					break;
			}
		}, "Init pvp follower bots");
		
		put(Privilege.ADMIN, "purebots", (p, args) -> {
			int id = Integer.parseInt(args[0]);
			switch (id) {
				case 1:
					PkBots.initPkers(p.world());
					break;
				case 2:
					PkBots.initPurePker(p.world(), PkBots.getPURE_PKERS_VARROCK(), PVPAreas.getVarrockSafe(), PVPAreas.getVarrock_map());
					break;
			}
		}, "Init pvp follower bots");
		
		put(Privilege.ADMIN, "grabbots", (p, args) -> {
			PkBots.pvpFollowersInZone(p).ifPresent(fb -> fb.setTarget(p));
		}, "Makes current pvp instances bots follow you");
		
		put(Privilege.ADMIN, "removelocalbots", (p, args) -> {
			PkBots.pvpFollowersInZone(p).ifPresent(fb -> fb.getBots().forEach(b -> b.logout()));
		}, "Despawns pvp bots in the current pvp instance");
		
		put(Privilege.ADMIN, "mypvpfolos", (p, args) -> {
			PkBots.FollowerBots fb = PkBots.initFollowerBots(p.world(), p.tile(), 7, null);
			fb.setTarget(p);
			fb.getBots().forEach(bot -> PkBots.info(bot).setPermFollower(true));
		}, "Spawns a squad that will follow you until wilderness removed.");
		
		put(Privilege.ADMIN, "lpvp", (p, args) -> {
			PkBots.initGlobalLooterBots(p.world());
			PkBots.spawnGitems(p.world());
			PkBots.initFollowerBots(p.world());
			PkBots.initPkers(p.world());
		}, "Reinit looter bots");
		
		put(Privilege.ADMIN, "removepkbots", (p, args) -> {
			tryOrUsage(p, "give me errors", () -> {
				PkBots.getLOOTER_BOTS().forEach(player -> {
					if (player != null && player.bot()) {
						player.logout();
					}
				});
				PkBots.getLOOTER_BOTS().clear();
				PkBots.getFOLLOWER_BOTS().forEach(fb -> {
					Arrays.stream(fb.getBots().toArray(new Player[fb.getBots().size()])).forEach(player -> {
						if (player != null && player.bot()) {
							player.logout();
						}
					});
					fb.getBots().clear();
				});
				PkBots.getFOLLOWER_BOTS().clear();
				PkBots.getPKER_BOTS().forEach(player -> {
					if (player != null && player.bot()) {
						player.logout();
					}
				});
				PkBots.getPKER_BOTS().clear();
			});
		},"Remove looter & follower bots");

		
		put(Privilege.ADMIN, "tsyo", (p, args) -> {
			p.sync().sufixPrefixName(new String[] {"kek", "1", "2"});
		}, "Testing new gpi flag");
		put(Privilege.ADMIN, "seticonname", (p, args) -> {
			Optional<Player> otherp = p.world().playerByName(args[0]);
			if (otherp.isPresent()) {
				p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
					s.updateDefaultIcon((int) otherp.get().id(), Integer.parseInt(args[1]));
				});
			}
			p.message("Rank: " + args[1]);
		}, "Changes a online players override rank (donor etc) ::seticonname [dmm gun] [11]");
		
		put(Privilege.ADMIN, "iconof", (p, args) -> {
			Optional<Player> otherp = p.world().playerByName(args[0].replace("_", " "));
			if (otherp.isPresent()) {
				p.message("Rank: " + otherp.get().getDefaultIcon());
			}
		}, "Tells you their default rank value ::iconof [dmm gun]");
		
		put(Privilege.ADMIN, "seticonid", (p, args) -> {
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
				s.updateDefaultIcon(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
			});
			p.message("Rank: " + args[1]);
		}, "Changes a players override rank (donor etc) by their acc ID ::seticonid [acc id check CP] [11]");
		
		put(Privilege.ADMIN, "emptybankimsure", (p, args) -> {
			int items = p.bank().size() - p.bank().occupiedSlots();
			p.bank().empty();
			p.bank().clean();
			p.message("Bank cleared of " + items + " items.");
		}, "Emptys your bank. ::emptybankimsure");
		
		put(Privilege.ADMIN, "setopiron", (p, args) -> {
			Optional<Player> otherp = p.world().playerByName(args[0]);
			if (otherp.isPresent()) {
				Player other = otherp.get();
				int mode = Integer.parseInt(args[1]);
				other.ironMode(IronMode.values()[mode]);
				p.message("Set %s mode to %d.", other.name(), mode);
			} else {
				p.message("Unable to find that player.");
			}
		}, "Change players ironman mode. ::setopiron playername ironmanmode");
		
		put(Privilege.ADMIN, "poison", (p, args) -> {
			int amt = 6;
			if (args != null && args.length >= 1)
				amt = Integer.parseInt(args[0]);
			p.poison(amt);
		}, "Poisons you. Testing. Optional start dmg ::poison <6>");
		
		put(Privilege.ADMIN, "gwdkc", (p, args) -> {
			p.message("All GWD kcs set to 80.");
			p.varps().varbit(Varbit.GWD_BANDOS_KC, 80);
			p.varps().varbit(Varbit.GWD_ZAMORAK_KC, 80);
			p.varps().varbit(Varbit.GWD_SARADOMIN_KC, 80);
			p.varps().varbit(Varbit.GWD_ARMADYL_KC, 80);
		}, "Sets all your GWD kcs to 80. ::gwdkc");
		
		put(Privilege.PLAYER, "copy", (p, args) -> {
			if (!p.privilege().eligibleTo(Privilege.ADMIN) && !p.world().realm().isDeadman()) return;
			Optional<Player> otherp = p.world().playerByName(glue(args));
			if (otherp.isPresent()) {
				Player other = otherp.get();
				p.equipment().items(other.equipment().items().clone());
				p.inventory().items(other.inventory().items().clone());
				p.inventory().makeDirty();
				p.equipment().makeDirty();
				p.message("Copied %s's inventory and equipment.", other.name());
			} else {
				p.message("Unable to find that player.");
			}
		}, "Copies player equipment and inventory. ::copy playername");
		
		for (String s : new String[]{"toggleskulls", "toggleskull"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				VarbitAttributes.toggle(p, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.getVarbitid());
				boolean hide = VarbitAttributes.varbiton(p, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.getVarbitid());
				p.message("Kill streak <img=26> skulls have now been " + (hide ? "hidden." : "activated."));
				p.write(UpdateStateCustom.skullToggle(!hide));
			}, "Turns skull on or off. ::toggleskull");
		}
		
		put(Privilege.ADMIN, "worldid", (p, args) -> {
			
			int worldId = Integer.parseInt(args[0]);
			
			if (worldId < 0 || worldId > 200) {
				p.message("World ID out of bounds. Try 1-100");
				return;
			}
			
			p.write(new UpdateStateCustom(worldId));
			p.message("World ID successfully updated to "+ worldId +".");
		});
		put(Privilege.PLAYER, "clearfl", (p, args) -> {
			if (p.social().friends().size() > 0) {
				p.world().server().service(PmService.class, true).ifPresent(pmService -> {
					pmService.removeAllFriends(p);
					p.social().friends().clear();
				});
				
				p.message("<col=FF0000>Removed all friends. Relog for this to update your friends list.");
			}
		}, "Clears your friends list. ::clearfl");
		
		put(Privilege.ADMIN, "maxhit", (p, args) -> {
			double meleeMax = CombatFormula.maximumMeleeHit(p) * (args.length == 0 ? 1.0D : Double.parseDouble(glue(args)));
			meleeMax *= MeleeSpecialAttacks.multi(p, ((WeakReference<Entity>) p.attribOr(AttributeKey.TARGET, new WeakReference<Entity>(null))).get(), CombatStyle.MELEE, false);
			p.debug("Melee max: %f    Range-max: %d (%d with ammo range-str, %d with current bolt special attack)",
					meleeMax,
					CombatFormula.maximumRangedHit(p, null, false, false, -1),
					CombatFormula.maximumRangedHit(p, null, true, false, -1),
					CombatFormula.maximumRangedHit(p, null, true, true, p.equipment().hasAt(EquipSlot.AMMO) ? p.equipment().get(EquipSlot.AMMO).id() : -1)
			);
		}, "Calculates max melee hit. ::maxhit bonus");
		
		put(Privilege.ADMIN, "magicmax", (p, args) -> {
			int rawmax = Integer.parseInt(args[0]);
			int max = CombatFormula.modifyMagicDamage(p, rawmax, args.length >= 2 ? args[1] : "");
			p.message("Magic max with base max " + rawmax + " is " + max);
		}, "Calculates max magic hit. ::magicmax max *bonus*");
		
		for (String s : new String[]{"dummykeys", "dummykey"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isDeadman() && p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
					int keycount = args.length == 0 ? 1 : Integer.parseInt(args[0]);
					DeadmanContainers dmminfo = DeadmanMechanics.infoForPlayer(p);
					if (dmminfo == null) return;
					for (int i = p.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED); i < Math.min(5, keycount); i++) {
						ItemContainer inv = new ItemContainer(p.world(), 10, ItemContainer.Type.FULL_STACKING);
						inv.add(new Item(i == 0 ? 4151 : i == 1 ? 995 : i == 2 ? 5698 : i == 3 ? 10828 : 3105), true);
						dmminfo.keys[i] = new BankKey(inv);
						if (p.inventory().add(new Item(13302 + i), false).success())
							p.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, Math.min(5, p.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED) + 1));
					}
					p.looks().update();
					p.message("Created dummy keys.");
				} else {
					p.message("This is a Deadman Beta world feature only.");
				}
			}, "Creates dummykeys. ::dummykeys");
		}
		
		for (String s : new String[]{"killkey", "killkeys"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isDeadman() && p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
					DeadmanContainers dmminfo = DeadmanMechanics.infoForPlayer(p);
					if (dmminfo == null) return;
					int held = p.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED);
					p.varps().varbit(Varbit.DEADMAN_SKULL_TIMER, 0);
					for (int i = 0; i < held; i++) {
						dmminfo.keys[i] = null;
						if (p.inventory().remove(new Item(13302 + i), false).success())
							p.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, Math.min(5, p.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED) - 1));
					}
					p.looks().update();
					p.message("Destroyed carried deadman keys.");
				}
			}, "Kills carried deadman keys. ::killkey");
		}
		
		for (String s : new String[]{"diekeys"}) { // Force remove keys when the system breaks
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isDeadman() && p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
					p.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, 0);
					p.varps().varbit(Varbit.DEADMAN_SKULL_TIMER, 0);
					DeadmanContainers dmminfo = DeadmanMechanics.infoForPlayer(p);
					if (dmminfo == null) return;
					for (int i = 0; i < 5; i++) {
						dmminfo.keys[i] = null;
						p.inventory().remove(new Item(13302 + i), false);
					}
					p.looks().update();
					p.message("Force destroyed carried deadman keys.");
				}
			}, "Kills carried deadman keys. ::killkey");
		}
		
		put(Privilege.ADMIN, "killnpc", (p, args) -> {
			Npc opp = p.world().npcs().get(Integer.parseInt(glue(args)));
			Hit h = opp.hit(p, opp.hp(), 1).combatStyle(CombatStyle.MELEE).addXp();
		}, "Kills a npc. ::killnpc index");
		
		put(Privilege.ADMIN, "fixnpc", (p, args) -> {
			Npc opp = p.world().npcs().get(Integer.parseInt(glue(args)));
			opp.world().server().scriptExecutor().executeScript(opp, NpcDeath.script);
		}, "Fixes npc. ::fixnpc index");
		
		put(Privilege.ADMIN, "max", (p, args) -> {
			// Boost stats instead of having to waste time potting when testing combat functions.
			p.skills().setLevel(Skills.ATTACK, 118);
			p.skills().setLevel(Skills.STRENGTH, 118);
			p.skills().setLevel(Skills.DEFENCE, 118);
			p.skills().setLevel(Skills.RANGED, 112);
			p.skills().setLevel(Skills.MAGIC, 104);
		}, "Boosts your combat stats. ::max");
		
		put(Privilege.ADMIN, "spec", (p, args) -> {
			p.varps().varp(Varp.SPECIAL_ENERGY, 1000);
			p.message("Special energy has been restored to full.");
			//acceptLogins
		}, "Restores special energy. ::spec");
		
		put(Privilege.ADMIN, "acceptlogins", (p, args) -> {
			LoginWorker.acceptLogins = !LoginWorker.acceptLogins;
			p.message("Accept logins is now " + (LoginWorker.acceptLogins ? "enabled" : "disabled") + ".");
		}, "NONO");
		
		put(Privilege.PLAYER, "infhp", (p, args) -> {//all incoming damage won't reduce HP.
			if (p.privilege() == Privilege.ADMIN || p.seniorModerator() || p.world().realm().isDeadman()) {
				VarbitAttributes.toggle(p, Varbit.INFHP);
				p.message("Infinite HP is now " + (VarbitAttributes.varbiton(p, Varbit.INFHP) ? "enabled" : "disabled") + ".");
			} else {
				p.message("You do not have permission to use that command.");
			}
		}, "Toggles infinite hp. ::infhp");
		
		put(Privilege.ADMIN, "rejs", (p, args) -> {
			p.message("Reloading...");
			JSScripts.reload(p);
			p.message("Done.");
		}, "Reload JS scripts. ::rejs");
		
		put(Privilege.ADMIN, "jss", (p, args) -> {
			boolean on = (int) p.attribOr(AttributeKey.JSS_USE, 0) == 1;
			p.putattrib(AttributeKey.JSS_USE, (!on) ? 1 : 0);
			p.message("javascripts: " + (((int) p.attrib(AttributeKey.JSS_USE) == 1) ? "on" : "off"));
		}, "Toggles JS scripts. ::jss");
		
		put(Privilege.ADMIN, "infpray", (p, args) -> {
			VarbitAttributes.toggle(p, Varbit.INFPRAY);
			p.message("infpray: " + (VarbitAttributes.varbiton(p, Varbit.INFPRAY) ? "on" : "off"));
		}, "Toggles infinite prayer. ::infpray");
		
		put(Privilege.PLAYER, "infspec", (p, args) -> {
			if (p.privilege().eligibleTo(Privilege.ADMIN) || p.world().realm().isDeadman()) {
				GameCommands.process(p, "spec");
				VarbitAttributes.toggle(p, Varbit.INFSPEC);
				p.message("infspec: " + (VarbitAttributes.varbiton(p, Varbit.INFSPEC) ? "on" : "off"));
			}
		}, "Toggles infinite special. ::infspec");
		for (String s : new String[] {"masterstats", "masterall"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				if (p.privilege() == Privilege.ADMIN || p.world().realm().isPVP()) {
					if (inWildBypassable(p, "You cannot do this while in the wilderness.", true)) {
						return;
					}
					for (int i = 0; i < Skills.SKILL_COUNT; i++) {
						boolean exception = Arrays.stream(ADMIN_EXCEPTIONS).anyMatch(name -> p.name().equalsIgnoreCase(name));
						if (!exception) {
							if (i == Skills.MINING || i == Skills.FISHING || i == Skills.SLAYER || i == Skills.HUNTER || i == Skills.THIEVING || i == Skills.WOODCUTTING) {
								continue;
							}
						}
						p.skills().setXp(i, Skills.levelToXp(99));
					}
				}
			}, "Sets all your stats to 99. ::masterstats");
		}
		put(Privilege.ADMIN, "infernowave", (p, args) -> {
			if (p.attribOr(AttributeKey.INFERNO_SESSION, null) == null) {
				if (inWildBypassable(p, "You can't do this while being inside The Inferno.", true)) {
					return;
				}
				int wave = Integer.parseInt(args[0]);
				if (wave < 1 || wave > 69) {
					p.message("Don't be silly.");
					return;
				}
				InfernoPool.INSTANCE.setInfernoWaveOffset(wave);
				p.message("Your Inferno session will start at wave <col=FF0000>"+ wave);
			}
		}, "Sets the starting wave on The Inferno.");
		
		
		put(Privilege.PLAYER, "master", (p, args) -> {
			if (p.world().realm().isDeadman()) {
				if (!p.privilege().eligibleTo(Privilege.ADMIN) && VarbitAttributes.varbit(p, 15) == 1) {
					p.message("This command can only be used in a Guarded zone on Deadman worlds.");
					return;
				}
				if (p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
					for (int i = 0; i < 7; i++) {
						if (i != 5)
							p.skills().setXp(i, Skills.levelToXp(94));
					}
					p.skills().setXp(5, Skills.levelToXp(70));
				}
			} else if (p.privilege() == Privilege.ADMIN || p.world().realm().isPVP()) {
				if (inWildBypassable(p, "You cannot do this while in the wilderness.", true)) {
					return;
				}
				for (int i = 0; i < Skills.SKILL_COUNT; i++) {
					if (!p.privilege().eligibleTo(Privilege.ADMIN) && (i == Skills.MINING || i == Skills.FISHING || i == Skills.SLAYER || i == Skills.HUNTER || i == Skills.THIEVING || i == Skills.WOODCUTTING)) {
						continue;
					}
					p.skills().setXp(i, Skills.levelToXp(99));
				}
			}
		}, "Sets all your stats to 99. ::master");
		
		put(Privilege.ADMIN, "statreset", (p, args) -> {
			for (int i = 0; i < Skills.SKILL_COUNT; i++)
				p.skills().setXp(i, Skills.levelToXp(i == 3 ? 10 : 0));
		}, "Sets all your stats to 1. ::statreset");
		
		put(Privilege.ADMIN, "addxp", (p, args) -> p.skills().__addXp(Integer.valueOf(args[0]), Integer.valueOf(args[1])), "Sets xp rate of a skill. ::addxp stat xp");
		
		put(Privilege.ADMIN, "hitme", (p, args) -> p.hit(p, Integer.valueOf(args[0])), "Hits your current player. ::hitme damage");
		
		put(Privilege.PLAYER, "empty", (p, args) -> {
			if (p.privilege().eligibleTo(Privilege.ADMIN)) {
				// Admins can straight up empty.
				p.inventory().empty();
			} else if (p.world().realm().isPVP() || p.world().realm().isDeadman() || p.privilege().eligibleTo(Privilege.ADMIN)) {
				p.world().server().scriptExecutor().executeScript(p, EmptyCommandConfirm.script);
			}
		}, "Empties your inventory. ::empty");
		
		for (String s : new String[]{"store", "donate", "donator", "premium", "donating", "donation", "webstore"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				p.message("Opening http://store.os-scape.com... One moment, please.");
				
				try {
					String concat = p.id() + ":gVFBUEHj:TqkvPAY6:" + p.id();
					MessageDigest md = MessageDigest.getInstance("SHA-256");
					
					md.update(concat.getBytes("UTF-8")); // Change this to "UTF-16" if needed
					byte[] digest = md.digest();
					String auth = String.format("%064x", new java.math.BigInteger(1, digest));
					String url = "https://www.os-scape.com/store/login?id=" + p.id() + "&auth=" + auth;
					p.write(new AddMessage(url, AddMessage.Type.URL));
				} catch (Exception e) {
					e.printStackTrace();
					p.write(new AddMessage("https://www.os-scape.com/store/login", AddMessage.Type.URL));
				}
			}, "Opens the store in a web browser. ::store");
		}
		put(Privilege.ADMIN, "bypassgametime", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				other.gameTime(30000);
				p.message("Gametime set to 30 minutes.");
				other.message("You can now trade.");
			});
		}, "Bypasses player game time. ::bypassgametime playername");
		
		put(Privilege.PLAYER, "wiki", (p, args) -> {
			p.message("Opening http://wiki.os-scape.com... One moment, please.");
			p.write(new AddMessage("http://wiki.os-scape.com/", AddMessage.Type.URL));
		}, "Opens the OSScape wiki in a web browser. ::wiki");
		
		put(Privilege.PLAYER, "vote", (p, args) -> p.write(new AddMessage("http://os-scape.com/vote/", AddMessage.Type.URL)), "Opens the OSScape vote page in a web browser. ::vote");
		
		put(Privilege.PLAYER, "setreason", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(Integer.parseInt(glue(args))));
			}
		}, "Setreason for a punishment id. ::setreason id");
		
		put(Privilege.PLAYER, "resetlooks", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				Optional<Player> op = p.world().playerByName(glue(args));
				if (!op.isPresent()) {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				} else {
					Player targ = op.get();
					p.message(targ.name() + "'s looks = " + Arrays.toString(targ.looks().looks()) + " and colors = " + Arrays.toString(targ.looks().colors()));
					p.message("They've now been reset.");
					targ.looks().looks(new int[]{9, 14, 107, 26, 33, 36, 42});
					targ.looks().colors(new int[]{6, 17, 15, 0, 7});
					targ.looks().update();
				}
			}
		}, "Resets a players looks. ::resetlooks playername");
		
		put(Privilege.PLAYER, "teleto", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				if (p.privilege() != Privilege.ADMIN && WildernessLevelIndicator.inAttackableArea(p)) {
					p.message("You're unable to use this command in the wilderness.");
					return;
				}
				Optional<Player> op = p.world().playerByName(glue(args));
				if (!op.isPresent()) {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				} else {
					Player targ = op.get();
					if (InfernoContext.inSession(targ)) {
						p.message("You can't teleport to a player whilst they are involved in this game!");
						return;
					}
					if ((Integer) p.attribOr(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, 0) > 0) {
						p.message("A magical for prevents you from teleporting.");
						return;
					}
					if (p.helper() && targ.privilege().eligibleTo(Privilege.MODERATOR)) {
						p.message("You can't teleport to other staff members. Ask %s to TP you to them.", targ.name());
						return;
					}
					if (p.privilege() != Privilege.ADMIN && targ.privilege().eligibleTo(Privilege.ADMIN)) {
						p.message("You can't teleport to Administrators. Ask %s to TP you to them.", targ.name());
						return;
					}
					if (p.privilege() != Privilege.ADMIN && WildernessLevelIndicator.inAttackableArea(targ) && GameCommands.TELETO_WILDY_PLAYER_DISABLED) {
						p.message("You can't teleport to a player who is inside the wilderness!");
						return;
					}
					if (WildernessLevelIndicator.inWilderness(targ.tile()) || WildernessLevelIndicator.inPvpInstanceAttackable(targ)) {
						p.timers().extendOrRegister(TimerKey.COMBAT_IMMUNITY, 50); // 30 S
						p.timers().extendOrRegister(TimerKey.COMBAT_LOGOUT, 25); // Can't logout for 25s stop abusers insta logging
						p.world().server().scriptExecutor().executeScript(p, new ChatboxOptionResult(targ, 1));
					} else {
						p.teleport(targ.tile());
						if (p.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) != targ.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY)) {
							p.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, targ.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY));
						}
					}
				}
			}
		}, "Teleports you to the specified player. ::teleto username");
		
		put(Privilege.MODERATOR, "teletome", (p, args) -> {
			Optional<Player> op = p.world().playerByName(glue(args));
			if (!op.isPresent()) {
				p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
			} else {
				Player targ = op.get();
				if (!p.privilege().eligibleTo(Privilege.ADMIN)) {
					if (WildernessLevelIndicator.inAttackableArea(p)) {
						p.message("You're unable to use this command in the wilderness.");
						return;
					}
					if (Staking.in_duel(targ)) {
						p.message("You can't teleport somebody out of a duel. Kick them or speak to an admin.");
						return;
					}
					if (ClanWars.inInstance(targ)) {
						p.message("You can't teleport somebody out of a clan war. Kick them or speak to an admin.");
						return;
					}
				}
				
				if ((p.privilege() == Privilege.MODERATOR || p.helper()) && WildernessLevelIndicator.inAttackableArea(p)) {
					p.message("You cannot teleport somebody to you while you're inside the wilderness.");
					return;
				}
				
				if ((p.privilege() == Privilege.MODERATOR || p.helper()) && WildernessLevelIndicator.inAttackableArea(targ)) {
					p.message("You cannot teleport to sombody while they're inside the wilderness.");
					return;
				}
				
				if ((Integer) p.attribOr(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, 0) > 0) {
					FightCaveGame.INSTANCE.leaveFightCave(p, false);
				}
				
				if (InfernoContext.get(p) != null) {
					InfernoContext.get(p).end(false);
				}
				
				targ.teleport(p.tile());
				targ.timers().extendOrRegister(TimerKey.COMBAT_IMMUNITY, 16); // 10 S
				if (targ.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) != p.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY)) {
					targ.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, p.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY));
				}
				p.world().server().scriptExecutor().executeScript(p, new ChatboxOptionResult(targ, 2));
			}
		}, "Teleports the specified player to you. ::teletome username");
		
		put(Privilege.MODERATOR, "cctome", (p, args) -> { // used for tournys in future
			if (p.privilege() == Privilege.MODERATOR) {
				p.message("In loving memory of Badder, 1/8/2017 8:13pm Amsterdam time.");
				return;
			}
			
			if (args.length < 2 || !args[1].equalsIgnoreCase("mynameisnotbadder")) {
				p.message("Usage: ::cctome mynameisnotbadder");
				return;
			}
			
			ClanChat.current(p).ifPresent(cc -> {
				for (Friend friend : cc.members()) {
					Player player = p.world().playerByName(friend.name()).get();
					if (player != null) {
						player.teleport(p.tile());
						p.message("done");
					} else {
						p.message(glue(args) + " not found.");
					}
				}
			});
		}, "Teleports your entire clan to you. ::cctome");
		
		for (String s1 : new String[]{"finditem", "cycle", "itemname", "fi"}) {
			put(Privilege.PLAYER, s1, (p, args) -> {
				if (!p.privilege().eligibleTo(Privilege.ADMIN) && !p.world().realm().isDeadman()) {
					// Silent not allowed
					return;
				}
				String s = glue(args);
				new Thread(() -> {
					int found = 0;
					
					for (int i = 0; i < p.world().definitions().total(ItemDefinition.class); i++) {
						if (found > 249) {
							p.message("<col=FF0000>Too many results (> 250). Please narrow down.");
							break;
						}
						ItemDefinition def = p.world().definitions().get(ItemDefinition.class, i);
						if (def != null && def.name.toLowerCase().contains(s)) {
							String result_string = "Result: " + i + " - " + def.name + " (price: " + p.world().prices().get(i, true) + ")";
							p.message(result_string);
							if (p.world().players().size() < 10) { // Show in cmd for more results
								System.out.println(result_string);
							}
							found++;
						}
					}
					p.message("<col=FF0000>Done searching. Found " + found + " results for '" + s + "'.");
				}).start();
			}, "Finds item id by name. ::finditem itemname");
		}
		
		for (String s1 : new String[]{"findnpc", "searchnpc", "npcname", "fn"}) {
			put(Privilege.ADMIN, s1, (p, args) -> {
				//int[] guards = new int[] {6574, 6575, 6576, 6577, 6578, 6579, 6580, 6581, 6582, 6583, 6698, 6699, 6700, 6701, 6702};
				String s = glue(args);
				new Thread(() -> {
					int found = 0;
					for (int i = 0; i < p.world().definitions().total(NpcDefinition.class); i++) {
						if (found > 249 && !p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
							p.message("Too many results (> 250). Please narrow down.");
							break;
						}
						NpcDefinition def = p.world().definitions().get(NpcDefinition.class, i);
						if (def != null && def.name.toLowerCase().contains(s)) {
							String result_string = "Result: " + i + " - " + def.name + " (cb " + def.combatlevel + ", alts: " + Arrays.toString(def.altForms) + ", renders: " + def.idleAnimation + ", " + def.walkAnimation + ")";
							p.message(result_string);
							if (p.world().players().size() < 10) { // Show in cmd for more results
								System.out.println(result_string);
							}
							found++;
						}
						/*if (def.altForms != null) {
							for (int alt : def.altForms)
								for (int guardId : guards)
									if (alt == guardId)
										System.out.println("alt guard found! "+i);
						}*/
					}
					p.message("Done searching. Found " + found + " results for '" + s + "'.");
				}).start();
			}, "Finds npc id by name. ::findnpc npcname");
		}

		put(Privilege.ADMIN, "dumpnpcs", (p, args) -> {
			try {
				FileWriter fw = new FileWriter("npc_list.txt");
				for (int i = 0; i < 10000; i++) {
					NpcDefinition def = p.world().definitions().get(NpcDefinition.class, i);
					if (def != null) {
						fw.append(String.format("%d: %s (cb: %d, renders: %d, %d)\n", i, def.name, def.combatlevel, def.idleAnimation, def.walkAnimation));
					}
				}
				p.message("Finished dumping NPC list.");
				fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
		put(Privilege.ADMIN, "findobj", (p, args) -> {
			String s = glue(args);
			new Thread(() -> {
				int found = 0;
				for (int i = 0; i < p.world().definitions().total(ObjectDefinition.class); i++) {
					if (found > 249) {
						p.message("Too many results (> 250). Please narrow down.");
						break;
					}
					ObjectDefinition def = p.world().definitions().get(ObjectDefinition.class, i);
					if (def != null && def.name.toLowerCase().contains(s)) {
						String result_string = "Result: " + i + " - " + def.name + " ";
						p.message(result_string);
						if (p.world().players().size() < 10) { // Show in cmd for more results
							System.out.println(result_string);
						}
						found++;
					}
				}
				p.message("Done searching. Found " + found + " results for '" + s + "'.");
			}).start();
		}, "Fines objectid by name. ::findobj objectname");
		
		put(Privilege.ADMIN, "givebox", (p, args) -> {
			Player player = p.world().playerByName(glue(args)).get();
			
			if (player != null) {
				if (player.inventory().add(new Item(7966), true).success()) {
					player.message("An Administrator has given you a gift!");
					p.message("You've successfully given a gift to " + glue(args) + ".");
				} else {
					p.message("That player doesn't have a free inventory slot.");
				}
			} else {
				p.message(glue(args) + " was not found.");
			}
		}, "Gives player a mystery box. ::givebox username");
		
		for (String s : new String[]{"ancients", "ancient"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.varps().varbit(Varbit.SPELLBOOK, 1);
				SpellSelect.reset(p, true, true);
			}, "Switches your spellbook to ancients magic. ::ancients");
		}
		
		for (String s : new String[]{"modern", "moderns", "norm", "normals"}) { //#lazy #downsyndrome
			put(Privilege.ADMIN, s, (p, args) -> {
				p.varps().varbit(Varbit.SPELLBOOK, 0);
				SpellSelect.reset(p, true, true);
			}, "Switches your spellbook to modern magic. ::modern");
		}
		
		for (String s : new String[]{"lunar", "lunars"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.varps().varbit(Varbit.SPELLBOOK, 2);
				SpellSelect.reset(p, true, true);
			}, "Switches your spellbook to lunar magic. ::lunar");
		}
		
		for (String s : new String[]{"switch", "spellbook", "book"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.varps().varbit(Varbit.SPELLBOOK, Integer.parseInt(args[0]));
				SpellSelect.reset(p, true, true);
			}, "Switches to the specific spellbook. ::spellbook id");
		}
		
		put(Privilege.ADMIN, "kickall", (Player p, String[] args) -> {
			p.world().players().forEach(Player::logout);
			p.message("All players have been logged out.");
		}, "Forces all players to logout. ::kickall");
		
		put(Privilege.ADMIN, "infrunes", (p, args) -> {
			boolean on = p.varps().varp(Varp.INFINITY_RUNES) >> 1 == 1;//check if on
			p.varps().varp(Varp.INFINITY_RUNES, (on ? 0 : 1) << 1);//switch
			p.message("infrunes: " + ((p.varps().varp(Varp.INFINITY_RUNES) >> 1) == 1));//tell user if changed val is now on
		}, "Toggles infinite runes. ::infrunes");
		
		for (String s : new String[]{"ob", "openbank"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.donationTier() == DonationTier.ULTIMATE_DONATOR || p.privilege() == Privilege.ADMIN || p.seniorModerator()) {
					if((p.donationTier() == DonationTier.ULTIMATE_DONATOR || p.seniorModerator()) && WildernessLevelIndicator.inWilderness(p.tile())) {
						p.message("You can't use this command inside the wilderness.");
						return;
					}

					if (p.donationTier() == DonationTier.ULTIMATE_DONATOR) {
						if (p.timers().has(TimerKey.IN_COMBAT)) {
							p.message("You can't do this whilst being in combat.");
							return;
						}
						if (WildernessLevelIndicator.inAttackableArea(p)) {
							p.message("You can't do this here.");
							return;
						}
					}

					Bank.open(p, null);
				}
			});
		}

		put(Privilege.PLAYER, "startvolcano", (p, args) -> {
			if (p.privilege() == Privilege.ADMIN || p.seniorModerator()) {
				if(BloodyVolcano.INSTANCE.getBoulder() == null) {
					BloodyVolcano.INSTANCE.setWildernessKillsRequired(0);
				} else {
					p.message("A volcano is already active!");
				}
			}
		});

		put(Privilege.ADMIN, "sound", (p, args) -> {
			p.write(new PlaySound(Integer.parseInt(args[0]), 0));
			p.message("Playing sound id: " + args[0]);
		}, "Plays a sound. ::sound id");
		
		put(Privilege.ADMIN, "removenpcs", (p, args) -> {
			int removed = p.world().npcs().size();
			p.world().npcs().forEach(n -> {
				n.stopActions(true);
				p.world().npcs().remove(n);
			});
			p.message("Removed %d npcs.", removed);
		}, "Removes all npcs. ::removenpcs");
		
		put(Privilege.ADMIN, "reloadnpcs", (p, args) -> {
			p.world().npcs().forEach(n -> p.world().npcs().remove(n));
			p.world().loadNpcSpawns(new File("data/map/npcs"));
			Fishing.respawnAllSpots(p.world());
			p.message("Reloaded %d npcs. <col=FF0000>Warning: Npcs in Instances will not be respawned. Must be done manually.", p.world().npcs().size());
		}, "Reloads all npcs. ::reloadnpcs");
		
		for (String s : new String[]{"combatdefs", "npcinfo"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.world().loadNpcCombatInfo();
				
				// Reload npcs
				p.world().npcs().forEach(n -> {
					if (n != null) {
						n.combatInfo(p.world().combatInfo(n.id()));
					}
				});
				
				p.message("Reloaded NPC combat definitions.");
			}, "Reloads npc combat definitions. ::combatdefs");
		}
		put(Privilege.ADMIN, "amtdonated", (p, args) -> p.totalSpent = Double.valueOf(args[0]), "Sets your amount donated. ::amtdonated");
		
		put(Privilege.ADMIN, "killstreak", (p, args) -> {
			p.putattrib(AttributeKey.KILLSTREAK, Integer.valueOf(args[0]));
			p.looks().update();
		});

		for (String s : new String[]{"slayerpoints", "sp"}) {
			put(Privilege.ADMIN, s, (p, args) -> p.varps().varbit(Varbit.SLAYER_POINTS, p.varps().varbit(Varbit.SLAYER_POINTS) + Integer.valueOf(args[0])));
		}
		
		put(Privilege.ADMIN, "slayhelmlock", (p, args) -> p.varps().varbit(Varbit.SLAYER_UNLOCKED_HELM, 1));
		
		put(Privilege.ADMIN, "drops", (p, args) -> {
			p.world().loadDrops();
			p.message("Reloaded NPC drops.");
		});
		
		put(Privilege.PLAYER, "topic", (p, args) -> {
			try {
				int id = Integer.parseInt(args[0]);
				p.write(new AddMessage("https://forum.os-scape.com/index.php?threads/topic." + id, AddMessage.Type.URL));
			} catch (Exception ignored) {
				p.message("That is not a valid topic.");
			}
		});
		
		for (String s : new String[]{"crayon"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isDeadman()) {
					p.looks().transmog(p.looks().trans() > 0 ? -1 : 9999);
					p.message("Transmog swapped.");
				}
			});
		}
		
		for (String s : new String[]{"transmog", "pnpc"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				int id = Integer.parseInt(args[0]);
				p.looks().transmog(id);
				if (id == -1)
					p.looks().resetRender();
				else
					p.looks().render(p.world().definitions().get(NpcDefinition.class, id).renderpairs());
				p.message("Transmogged player into %s.", args[0]);
			});
		}
		
		put(Privilege.ADMIN, "copycoords", (p, args) -> {
			Toolkit.getDefaultToolkit().getSystemClipboard()
					.setContents(new StringSelection(String.format("\"x\": %d, \"z\": %d", p.tile().x, p.tile().z)), (clipboard, contents) -> {
					});
			p.message("Coordinates copied to system clipboard.");
		});
		
		put(Privilege.ADMIN, "clearkilled", (p, args) -> {
			p.clearattrib(AttributeKey.KILLED_PLAYERS);
			p.clearattrib(AttributeKey.FIRST_KILL_OF_THE_DAY);
			p.message("Kill history has been cleared.");
		});
		
		for (String s : new String[]{"shops", "reloadshops"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.world().loadShops();
				p.message("A total of %d shops have been reloaded.", p.world().shops().size());
			});
		}
		
		put(Privilege.PLAYER, "resettask", (p, args) -> {
			if (p.privilege() == Privilege.ADMIN || p.seniorModerator()) {
				p.world().playerByName(glue(args)).ifPresent(other -> {
					other.putattrib(AttributeKey.SLAYER_TASK_ID, 0);
					other.putattrib(AttributeKey.SLAYER_TASK_AMT, 0);
					p.message("Slayer task reset.");
				});
			}
		});
		
		put(Privilege.ADMIN, "resetregion", (p, args) -> {
			p.world().definitions().unset(MapDefinition.class, p.tile().region());
			p.message("Region has been reverted to stock.");
		});
		
		put(Privilege.ADMIN, "hardresetregion", (p, args) -> {
			int region = p.tile().region();
			p.world().spawnedObjs().removeIf(object -> object == null || object.tile().region() == region);
			p.world().removedObjs().removeIf(object -> object == null || object.tile().region() == region);
			p.world().definitions().unset(MapDefinition.class, region);
			
			p.message("Region has been reverted to stock and spawns have been cleared.");
		});
		
		for (String s : new String[]{"mode", "gamemode", "mymode"}) {
			put(Privilege.PLAYER, s, (p, args) -> shoutMode(p));
		}
		
		put(Privilege.ADMIN, "equipinfo", (p, args) -> {
			p.world().loadEquipmentInfo();
			p.message("Reloaded equip info");
			RemoveObjects.adjustEdgevilleRealms_PVP_OSR(p.world());
			
		});
		
		put(Privilege.ADMIN, "doublecrates", (p, args) -> {
			tryOrUsage(p, "Usage: ::doublecrates <true/false>", () -> {
				boolean on = Boolean.parseBoolean(args[0]);
				WintertodtGame.setDoubleCrates(on);
			});
		});
		
		for (String s : new String[]{"obj", "object"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				int inputId = Integer.parseInt(args[0]);
				if (inputId == -1) { // remove and unclip whatever is on this tile, TYPE 10 (10 is assumed by that command)
					GameCommands.process(p, "delobj");
					return;
				}
				int id = Math.max(0, Math.min(p.world().definitions().total(ObjectDefinition.class), inputId));
				int rot = args.length >= 2 ? Math.max(0, Math.min(3, Integer.parseInt(args[1]))) : 0;
				int type = args.length >= 3 ? Math.max(0, Math.min(22, Integer.parseInt(args[2]))) : 10;
				boolean addclip = args.length < 4 || Boolean.parseBoolean(args[3]);
				MapObj obj = new MapObj(p.tile(), id, type, rot);
				p.message("Spawned %d (%s) at %d,%d,%d  [type:%d rot:%d].", id, obj.definition(p.world()).name, p.tile().x, p.tile().z, p.tile().level, type, rot);
				p.world().spawnObj(obj, addclip);
			});
		}
		
		for (String s : new String[]{"removeobj", "removeobject", "removeo", "delobj"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				MapObj obj = p.world().objByType(10, p.tile());
				boolean unclip = args.length < 1 || Boolean.parseBoolean(args[0]);
				p.world().removeObj(obj, unclip);
				p.message("Attempting to remove unclip %s - found: %s", unclip, obj);
			});
		}
		
		for (String s : new String[]{"delobj", "removeobj", "removeobject"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				int type = args.length >= 2 ? Math.max(0, Math.min(22, Integer.parseInt(args[1]))) : 10;
				boolean unclip = args.length < 3 || Boolean.parseBoolean(args[2]);
				MapObj obj = p.world().objByType(type, p.tile());
				if (obj == null) {
					p.message("Unable to find a mapobj type %d at your position of %s.", type, p.tile().toStringSimple());
				} else {
					p.world().removeObj(obj, unclip);
					p.message("Removed mapobj type %d. Clipping %s.", type, unclip ? "<col=FF0000>also removed</col>" : "<col=FF0000>not</col> removed");
				}
			});
		}
		
		for (String s : new String[]{"delobjs", "removeobjs", "removeobjects"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				boolean unclip = args.length < 2 || Boolean.parseBoolean(args[1]);
				List<MapObj> objs = p.world().objByTile(p.tile());
				if (objs == null || objs.size() == 0) {
					p.message("Unable to find a mapobj(s) at your position of %s.", p.tile().toStringSimple());
				} else {
					objs.forEach(obj -> {
						p.world().removeObj(obj, unclip);
						p.message("Removed mapobj(s) type %d. Clipping %s.", obj.type(), unclip ? "<col=FF0000>also removed</col>" : "<col=FF0000>not</col> removed");
					});
				}
			});
		}
		
		put(Privilege.PLAYER, "jailores", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				int id = Integer.parseInt(args[0]);
				if (id > 0) {
					GameCommands.jailOres = id;
					p.message("Ores required to exit now: " + GameCommands.jailOres + ".");
				}
			}
		});
		
		for (String s : new String[]{"staff", "staffonline"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				QuestGuide.clear(p);
				List<Player> staff = p.world().players().stream().filter(px -> px != null && (px.privilege() != Privilege.PLAYER || px.helper())).collect(Collectors.toList());
				int pointer = 5;
				int admins = 0, mods = 0, helpers = 0;
				p.interfaces().text(275, 4, "<col=800000><img=1> Administrators:");
				for (Player staffmember : staff) {
					if (staffmember.privilege() == Privilege.ADMIN) {
						if (ignorename(staffmember.name())) continue;
						if (VarbitAttributes.varbit(staffmember, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) == 2) // Pm OFF
							continue;
						p.interfaces().text(275, pointer++, " - " + staffmember.name());
						admins++;
					}
				}
				
				if (admins == 0) {
					p.interfaces().text(275, pointer++, " - Nobody");
				}
				
				pointer++;
				
				p.interfaces().text(275, pointer++, "<col=800000><img=0> Moderators:");
				for (Player staffmember : staff) {
					if (VarbitAttributes.varbit(staffmember, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) == 2) // Pm OFF
						continue;
					if (staffmember.privilege() == Privilege.MODERATOR) {
						p.interfaces().text(275, pointer++, " - " + staffmember.name());
						mods++;
					}
				}
				
				if (mods == 0) {
					p.interfaces().text(275, pointer++, " - Nobody");
				}
				
				pointer++;
				
				p.interfaces().text(275, pointer++, "<col=800000><img=36> Helpers:");
				for (Player staffmember : staff) {
					if (VarbitAttributes.varbit(staffmember, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) == 2) // Pm OFF
						continue;
					if (staffmember.helper()) {
						p.interfaces().text(275, pointer++, " - " + staffmember.name());
						helpers++;
					}
				}
				
				if (helpers == 0) {
					p.interfaces().text(275, pointer++, " - Nobody");
				}
				String admin_s = admins != 1 ? "s" : "";
				String mod_s = mods != 1 ? "s" : "";
				String hepl_s = helpers != 1 ? "s" : "";
				int total = admins + helpers + mods;
				String breakdown = total == 0 ? "None" : total + " (" + admins + " admin" + admin_s + ", " + mods + " mod" + mod_s + ", " + helpers + " helper" + hepl_s + ")";
				p.interfaces().text(275, 2, "<img=1> Staff Online: " + breakdown);
				QuestGuide.open(p);
			});
		}
		
		put(Privilege.ADMIN, "shoptest", (p, args) -> {
			
			int interfaceId = Integer.parseInt(args[0]);
			int shopId = Integer.parseInt(args[1]);
			
			if (shopId < 0) {
				p.message("Shop argument must be positive!");
				return;
			}
			
			Shop shop = p.world().shop(shopId);
			
			p.write(new UpdateCustomShop(shop.name(), interfaceId, shop.stock()));
			p.interfaces().sendMain(interfaceId);
//			shop.display(p);
		});
		
		for (String s : new String[]{"staffzone", "sz", "sz1"}) {
			put(Privilege.MODERATOR, s, (p, args) -> {
				if (!p.privilege().eligibleTo(Privilege.ADMIN)) {
					if (PlayerCombat.inCombat(p)) {
						p.message("You can't use this teleport when in combat. Use modop (no punishment) to become immune, then teleport.");
						return;
					}
					if (Staking.in_duel(p)) {
						p.message("You can't teleport out of a duel.");
						return;
					}
					if (ClanWars.inInstance(p)) {
						p.message("You can't teleport out of a clan war");
						return;
					}
					if (WildernessLevelIndicator.inWilderness(p.tile()) && PlayerCombat.inCombat(p)) {
						p.message("You can't use this teleport when under attack in the Wilderness.");
						return;
					}
				}
				p.world().tileGraphic(110, p.tile(), 110, 15);
				p.teleport(2666, 10396, 0);
				p.graphic(110, 110, 0);
			});
		}
		
		put(Privilege.MODERATOR, "broadcast", (p, args) -> {
			if (!p.privilege().eligibleTo(Privilege.ADMIN) && !p.seniorModerator()) {
				return;
			}
			String cast = glue(args);
			p.world().players().forEach(other -> {
				other.write(new AddMessage(cast, AddMessage.Type.BROADCAST));
			});
		});
		
		for (String s : new String[]{"sz2", "testzone"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.teleport(0, 0, 0);
			});
		}
		
		put(Privilege.ADMIN, "setblessing", (p, args) -> {
			String blessing = glue(args);
			boolean found = false;
			
			for (BlessingGroup blessingGroup : BlessingGroup.values()) {
				if (blessingGroup.getTitle().equalsIgnoreCase(blessing)) {
					BonusContent.activeBlessing = blessingGroup;
					found = true;
					break;
				}
			}
			
			if (found) {
				p.message("Blessing changed to '%s'.", BonusContent.activeBlessing.getTitle());
			} else {
				p.message("Could not find blessing with name '%s'.", blessing);
			}
		});
		
		put(Privilege.ADMIN, "removebots", (p, args) -> {
			p.world().players().forEach(player -> {
				if (player != null && player.bot()) {
					player.logout();
				}
			});
		});
		
		put(Privilege.ADMIN, "addbots", (p, args) -> {
			int x = -5;
			int y = -5;
			int amount = 1500;
			try {
				amount = Integer.parseInt(args[0]);
			} catch (Exception ignored) {
			}
			for (int i = 0; i < amount; i++) {
				Player fake = new Player(null, Misc.randomString(12), p.world(), p.tile().transform(x, y), new IsaacRand(), new IsaacRand());
				p.world().registerPlayer(fake);
				if (x == 10) {
					y++;
					x = -5;
				} else {
					x++;
				}
			}
		});
		
		put(Privilege.PLAYER, "modop", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				
				if (p.attribOr(AttributeKey.GIFT_PRAYERS_PLAYER_OP, false)) // Also in slot 6
					p.clearattrib(AttributeKey.GIFT_PRAYERS_PLAYER_OP);
				
				if (p.attribOr(AttributeKey.MODOPS, false)) {
					p.write(new SetPlayerOption(6, false, "null"));
					p.clearattrib(AttributeKey.MODOPS);
				} else {
					p.putattrib(AttributeKey.MODOPS, true);
					p.write(new SetPlayerOption(6, false, "Moderate"));
				}
				p.message("Mod options toggled.");
			}
		});
		put(Privilege.ADMIN, "giftprayer", (p, args) -> {
			if (!(p.world().realm().isPVP() || p.world().server().isDevServer())) {
				p.message("This feature is only available on World 2.");
				return;
			}
			if (p.tile().region() != 12342) {
				p.message("This feature can only be used in Edgeville.");
				return;
			}
			if (p.attribOr(AttributeKey.MODOPS, false)) // Also in slot 6
				p.clearattrib(AttributeKey.MODOPS);
			
			if (p.attribOr(AttributeKey.GIFT_PRAYERS_PLAYER_OP, false)) {
				p.write(new SetPlayerOption(6, false, "null"));
				p.clearattrib(AttributeKey.GIFT_PRAYERS_PLAYER_OP);
			} else {
				p.putattrib(AttributeKey.GIFT_PRAYERS_PLAYER_OP, true);
				p.write(new SetPlayerOption(6, false, "Gift prayer"));
			}
			p.message("The 'Gift prayer' option of players has been toggled.");
		});
		
		put(Privilege.PLAYER, "modpk", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				if (p.timers().has(TimerKey.COMBAT_IMMUNITY)) {
					p.timers().cancel(TimerKey.COMBAT_IMMUNITY);
					p.message("You are <col=FF0000>no longer</col> immune to combat.");
				} else {
					p.message("You're not immune right now.");
				}
			}
		});
		
		put(Privilege.MODERATOR, "gethwid", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				p.message("Hardware ID of %s is: <col=ff0000>%s</col>.", glue(args), other.hwid());
			});
		});
		
		put(Privilege.MODERATOR, "getuuid", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				p.message("UUID of %s is: <col=ff0000>%s</col>.", glue(args), other.uuid());
			});
		});
		
		for (String s : new String[]{"visitjail", "gojail", "telejail"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
					if (pkTeleportOk(p, 3290, 3017, false)) {
						p.interfaces().closeMain(); // Could have bank open lol
						p.teleport(new Tile(3290, 3017));
						p.timers().cancel(TimerKey.FROZEN);
						p.timers().cancel(TimerKey.REFREEZE);
						p.write(new SendWidgetTimer(WidgetTimer.BARRAGE, 0));
					}
				}
			});
		}
		
		put(Privilege.ADMIN, "setvarbits", (p, args) ->
				p.world().server().scriptExecutor().executeScript(p, new TestVarbits(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]))));
		
		put(Privilege.PLAYER, "jail", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				String opname = glue(args);
				int jailOres = GameCommands.jailOres;
				if (args != null && args.length > 1) {
					try {
						int amt = Integer.parseInt(args[args.length - 1]);
						opname = args[0].replace("_", " ");
					} catch (NumberFormatException e) {
						p.message("<col=7F00FF>No ore count specified, will set to 167. You can specify by ::jail [name_with_underscores] [ores to mine]");
						p.debug("%s", e.getMessage());
					}
				}
				Optional<Player> otherp = p.world().playerByName(opname);
				if (otherp.isPresent()) {
					Player other = otherp.get();
					if (Staking.in_duel(other)) {
						p.message("You can't jail to someone during a duel.");
					} else if (ClanWars.inInstance(other)) {
						p.message("You can't jail to someone during a clan war");
					} else if (other.helper() || other.privilege().eligibleTo(Privilege.MODERATOR)) {
						p.message("You can't jail other staff members.");
					} else {
						
						if (p.attribOr(AttributeKey.INFERNO_SESSION, null) != null) {
							InfernoContext.get(p).end(false);
						}
						
						other.stopActions(true);
						other.world().tileGraphic(110, other.tile(), 110, 0);
						other.world().server().scriptExecutor().executeScript(other, new TeleportNoReqs(new Tile(3290, 3017)));
						other.putattrib(AttributeKey.JAILED, 1);
						other.putattrib(AttributeKey.JAIL_ORES_TO_ESCAPE, jailOres);
						other.message("You have been jailed. Mine ores and give to Irena to escape.");
						other.putattrib(AttributeKey.JAIL_ORES_MINED, 0);
						other.putattrib(AttributeKey.LOC_BEFORE_JAIL, other.tile());
						Poison.cure(other);
						int logId = PunishmentLog.append(glue(args), p, "-", "jailed");
						p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "jail"));
						if (!PlayerCombat.inCombat(p)) {
							p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
							p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
						}
					}
				} else {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				}
			}
		});
		put(Privilege.PLAYER, "unjail", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				Optional<Player> otherp = p.world().playerByName(glue(args));
				if (otherp.isPresent()) {
					Player other = otherp.get();
					if (other.jailed()) {
						other.stopActions(true);
						other.putattrib(AttributeKey.JAILED, 0);
						Tile end = new Tile(3092, 3500);
						if (!other.world().realm().isPVP()) {
							end = other.attribOr(AttributeKey.LOC_BEFORE_JAIL, new Tile(3092, 3500));
						}
						other.world().tileGraphic(110, other.tile(), 110, 0);
						other.world().server().scriptExecutor().executeScript(other, new TeleportNoReqs(end));
						other.putattrib(AttributeKey.JAIL_ORES_MINED, 0);
						for (Item i : other.inventory().items()) { // Clear blurite ores.
							if (i == null) continue;
							if (i.id() == 668) {
								other.inventory().remove(i, true);
							}
						}
						p.message("Un-jailed " + glue(args) + ".");
						int logId = PunishmentLog.append(glue(args), p, "-", "un-jailed");
						p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "unjail"));
						if (!PlayerCombat.inCombat(p)) {
							p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
							p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
						}
					} else {
						p.message(glue(args) + " is not jailed.");
					}
				} else {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				}
			}
		});
		put(Privilege.MODERATOR, "ban", (p, args) -> {
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
				s.banPlayer(glue(args));
				p.world().playerByName(glue(args)).ifPresent(Player::logout);
				p.message("Banned user: <col=7F00FF>" + glue(args) + "</col> for <col=7F00FF>1 year</col>. Use ::setban to adjust.");
				p.world().server().service(LoggingService.class, true).ifPresent(ser ->
						ser.logPunishment(p, glue(args), "ban")
				);
				int logId = PunishmentLog.append(glue(args), p, "-", "banned");
				if (!PlayerCombat.inCombat(p)) {
					p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
					p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
				}
			});
		});
		put(Privilege.MODERATOR, "unban", (p, args) -> {
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
				s.unbanPlayer(glue(args));
				p.message("Unbanned: <col=7F00FF>" + glue(args) + ".");
				p.world().server().service(LoggingService.class, true).ifPresent(ser ->
						ser.logPunishment(p, glue(args), "unban")
				);
				int logId = PunishmentLog.append(glue(args), p, "-", "un-banned");
				if (!PlayerCombat.inCombat(p)) {
					p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
					p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
				}
			});
		});
		put(Privilege.ADMIN, "macban", (p, args) -> {
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
				p.world().playerByName(glue(args)).ifPresent(other -> {
					s.uuidPlayerBan(other, (Integer) p.id());
					s.banPlayer(glue(args));
					other.logout();
					p.message("Player UUID ban request on " + glue(args) + " acknowledged.");
					p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "uuidban"));
					return;
				});
				p.message(glue(args) + " is offline. Use ::idban [uuid] [account-id] to macban when victim is offline.");
			});
		});
		put(Privilege.ADMIN, "idban", (p, args) -> {
			if (args == null || args.length != 2) {
				p.message("Usage is ::idban [uuid] [acc-id]   Example: -D34JA88DJ29KGM489F and 37420");
				return;
			}
			String uuid = args[0];
			int targetid = Integer.parseInt(args[1]);
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
				s.uuidBan(args[0], targetid, (Integer) p.id());
				p.message("UUID banned account #<col=7F00FF>[" + targetid + "]</col> with uuid <col=7F00FF>[" + uuid + "].");
				// username needed to log
				//p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "uuidban"));
			});
		});
		for (String str : new String[]{"unmacban", "unmac", "unuuid", "unidban"}) {
			put(Privilege.MODERATOR, str, (p, args) -> {
				p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
					s.uuidUnban(glue(args));
					p.message("Player UUID unban request on <col=7F00FF>" + glue(args) + "</col> acknowledged.");
					p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "uuidunban"));
				});
			});
		}
		for (String str : new String[]{"modlog", "modlogs"}) {
			put(Privilege.PLAYER, str, (p, args) -> {
				if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
					PunishmentLog.displayLog(p);
				}
			});
		}
		put(Privilege.ADMIN, "crashlog", (p, args) -> {
			ClientCrashLog.displayLog(p);
		});
		put(Privilege.MODERATOR, "log", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				other.logged(true);
				p.message("Player packet logging started for session length on " + glue(args) + ".");
				p.message("To set up permanent logging, visit the management panel.");
			});
		});
		put(Privilege.MODERATOR, "unlog", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				other.logged(false);
				p.message("Player packet logging stopped on " + glue(args) + ".");
			});
		});
		put(Privilege.PLAYER, "shadowmute", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
					s.shadowMutePlayer(glue(args));
					p.world().playerByName(glue(args)).ifPresent(other -> other.shadowMuted(true));
					p.message("Player shadow mute request on <col=7F00FF>" + glue(args) + "</col> acknowledged.");
					
					p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "shadowmute"));
					int logId = PunishmentLog.append(glue(args), p, "-", "shadow muted");
					if (!PlayerCombat.inCombat(p)) {
						p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
						p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
					}
				});
			}
		});
		put(Privilege.PLAYER, "unshadowmute", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
					s.unShadowMutePlayer(glue(args));
					p.world().playerByName(glue(args)).ifPresent(other -> other.shadowMuted(false));
					p.message("Player unshadowmute request on <col=7F00FF>" + glue(args) + "</col> acknowledged.");
					
					p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "unshadowmute"));
					int logId = PunishmentLog.append(glue(args), p, "-", "un-shadow-muted");
					if (!PlayerCombat.inCombat(p)) {
						p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
						p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
					}
				});
			}
		});
		put(Privilege.PLAYER, "mute", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
					s.mutePlayer(glue(args));
					p.world().playerByName(glue(args)).ifPresent(other -> {
						other.muted(new Timestamp(System.currentTimeMillis() + 3600000));
					});
					p.message("Muted <col=7F00FF>" + glue(args) + "</col> for <col=7F00FF>1 hour</col>. Use ::setmute to adjust.");
					
					p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "mute"));
					int logId = PunishmentLog.append(glue(args), p, "-", "muted");
					if (!PlayerCombat.inCombat(p)) {
						p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
						p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
					}
				});
			}
		});
		put(Privilege.PLAYER, "unmute", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(s -> {
					s.unmutePlayer(glue(args));
					p.world().playerByName(glue(args)).ifPresent(other -> {
						other.muted(null);
						other.message("<col=FF0000>You've been unmuted.");
					});
					p.message("Player unmute request on " + glue(args) + " acknowledged.");
					
					p.world().server().service(LoggingService.class, true).ifPresent(ser -> ser.logPunishment(p, glue(args), "unmute"));
					int logId = PunishmentLog.append(glue(args), p, "-", "un-muted");
					if (!PlayerCombat.inCombat(p)) {
						p.timers().addOrSet(TimerKey.COMBAT_IMMUNITY, 50);
						p.world().server().scriptExecutor().executeScript(p, new SetPunishmentReason(logId));
					}
				});
			}
		});
		for (String s : new String[]{"resetcheckview", "rcv", "rview"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
					p.inventory().makeDirty();
					p.equipment().makeDirty();
					p.bank().makeDirty();
					
					p.message("Restored inventory/equip/bank to your own.");
				}
			});
		}
		put(Privilege.ADMIN, "wall", (p, args) -> {
//			TzKalZuk.spawnWall(p);
		});
		put(Privilege.ADMIN, "npcemote", (p, args) -> {
			tryOrUsage(p, "Usage: ::npcemote id", () -> {
				int anim = Integer.parseInt(args[0]);
				System.out.println("Hello " + anim);
				p.world().npcs().forEach(n -> n.animate(anim));
			});
		});
		put(Privilege.ADMIN, "takeitem", (p, args) -> {
			tryOrUsage(p, "Usage: ::takeitem [from_name] [inv/worn] [id] [amount=1]- make sure to replace spaces in usernames with underscores ('_')", () -> {
				Optional<Player> from = p.world().playerByName(args[0].replace("_", " "));
				if (!from.isPresent()) {
					p.message("%s is offline.", args[0]);
				} else {
					ItemContainer c = args[1].equalsIgnoreCase("inv") ? from.get().inventory() :
							args[1].equalsIgnoreCase("worn") || args[1].equalsIgnoreCase("equip") ? from.get().equipment() : null;
					if (c == null) {
						p.message("Unknown container: %s", args[1]);
					} else {
						int id = Integer.parseInt(args[2]);
						int amt = args.length >= 4 ? Integer.parseInt(args[3]) : 1;
						Item take = new Item(id, amt);
						if (c.findAll(id).stream().filter(i -> i.second().hasProperties()).findFirst().isPresent()) {
							p.message("Items with properties such as scales, darts or charges cannot be removed.");
						} else if (c.count(id) > 0) {
							ItemContainer.Result r = c.remove(take, true);
							if (r.success()) {
								if (p.inventory().add(take, false).success()) {
									p.message("%s x %s was added to your inventory from %s.", take.name(p.world()), take.amount(), from.get().name());
								} else {
									p.message("%s x %s could not be added to your inventory but WAS STILL taken from %s.", take.name(p.world()), take.amount(), from.get().name());
								}
							} else {
								p.message("Only managed to take %s/%s x %s to your inventory from %s.", r.completed(), take.amount(), take.name(p.world()), from.get().name());
							}
						} else {
							p.message("%s does not have any %s.", from.get().name(), take.name(p.world()));
						}
					}
				}
			});
		});
		put(Privilege.MODERATOR, "xpray", (p, args) -> {
			tryOrUsage(p, "Usage: ::xpray [from_name] [to_name] [preserve/rigour/augury] - make sure to replace spaces in usernames with underscores ('_')", () -> {
				Optional<Player> from = p.world().playerByName(args[0].replace("_", " "));
				Optional<Player> to = p.world().playerByName(args[1].replace("_", " "));
				if (!from.isPresent()) {
					p.message("%s is offline.", args[0]);
				} else if (!to.isPresent()) {
					p.message("%s is offline.", args[1]);
				} else {
					String prayer = args[2];
					int varbit = prayer.equalsIgnoreCase("preserve") ? Varbit.UNLOCK_PRESERVE :
							prayer.equalsIgnoreCase("augury") ? Varbit.UNLOCK_AUGURY :
									prayer.equalsIgnoreCase("rigour") ? Varbit.UNLOCK_RIGOUR : -1;
					if (varbit == -1) {
						p.message("Unrecognised prayer: %s", prayer);
					} else if (!from.get().varps().varbitBool(varbit)) {
						p.message("%s doesn't have %s unlocked.", from.get().name(), prayer);
					} else if (to.get().varps().varbitBool(varbit)) {
						p.message("%s already has %s unlocked.", to.get().name(), prayer);
					} else {
						from.get().varps().varbit(varbit, 0);
						to.get().varps().varbit(varbit, 1);
						p.message("The %s prayer was transfered from %s to %s.", prayer, from.get().name(), to.get().name());
						from.get().message("The %s prayer was transfered to %s by %s", prayer, to.get().name(),
								p.name());
						from.get().message("You've recieved the %s prayer from %s by %s", prayer, from.get().name(),
								p.name());
					}
				}
			});
		}, "Transfers prayers between accounts ::xpray [from username] [to name] [prayer name]");
		
		put(Privilege.PLAYER, "geh", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				
				Optional<Player> otherp = p.world().playerByName(glue(args));
				if (!otherp.isPresent()) {
					p.message("%s is offline.", args[0]);
				} else {
					Player other = otherp.get();
					
					p.invokeScript(InvokeScript.SETVARCS, -1, -1);
					p.interfaces().send(149, 548, 66, true);
					p.interfaces().sendMain(383);
					p.interfaces().setting(548, 48, -1, -1, 2);
					p.interfaces().setting(383, 3, 0, 114, 1026);
					p.invokeScript(299, 1, 1); // Closes the chatbox
					
					p.invokeScript(1644);
					
					for (int i = 0; i < other.grandExchange().getHistory().size(); i++) {
						if (i > other.grandExchange().getHistory().size()) {
							break;
						}
						ExchangeHistory h = other.grandExchange().getHistory().get(i);
						if (h != null) {
							p.invokeScript(1645, i, h.id(), h.type() == ExchangeType.SELL ? 1 : 2, h.amount(), h.price());
						}
					}
					p.invokeScript(1646);
				}
			}
		}, "Shows most 10 recent GE history transactions ::geh [name]");
		put(Privilege.PLAYER, "view", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				if (p.helper() && !GameCommands.HELPERS_CAN_VIEW) {
					p.message("This command is unavailable for helpers.");
					return;
				}
				Optional<Player> otherp = p.world().playerByName(glue(args));
				if (otherp.isPresent()) {
					Player other = otherp.get();
					if (WildernessLevelIndicator.inWilderness(other.tile()) && p.privilege().equals(Privilege.MODERATOR)) {
						p.message("You can't view someone while they're in the Wilderness.");
						return;
					}
					
					p.interfaces().sendMain(149, false); // Show inventory
					p.write(new SetItems(93, 149, 0, other.inventory()));
					if (Arrays.stream(other.inventory().items()).filter(o -> o != null && o.id() == 12791).findFirst().isPresent()) {
						p.message(p.name() + "'s rune pouch contains: " + Arrays.stream(RunePouch.INSTANCE.allRunesOf(other))
								.map(r -> r.name(p.world()))
								.collect(Collectors.toList()));
					}
					
					p.interfaces().sendMain(12, false); // Show bank interface
					p.write(new SetItems(95, other.bank().copy()));
					
					p.write(new SetItems(94, other.equipment()));
					p.message("Viewing <col=FF0000>" + glue(args) + "</col>.  Use ::rview to restore your items.");
				} else {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				}
			}
		});
		for (String s : new String[]{"checkbank", "viewbank"}) {
			put(Privilege.MODERATOR, s, (p, args) -> {
				Optional<Player> otherp = p.world().playerByName(glue(args));
				if (otherp.isPresent()) {
					Player other = otherp.get();
					if (WildernessLevelIndicator.inWilderness(other.tile()) && p.privilege().equals(Privilege.MODERATOR)) {
						p.message("You can't view someone's bank while they're in the Wilderness.");
						return;
					}
					p.interfaces().sendMain(12, false);
					p.interfaces().sendInventory(15);
					p.write(new SetItems(95, other.bank().copy()));
					p.message("Viewing bank of <col=FF0000>" + glue(args) + "</col>.  Use ::rview to restore your items.");
				} else {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				}
			});
		}
		for (String str : new String[]{"checkinventory", "checkinv", "viewinv"}) {
			put(Privilege.MODERATOR, str, (p, args) -> {
				Optional<Player> otherp = p.world().playerByName(glue(args));
				if (otherp.isPresent()) {
					Player other = otherp.get();
					if (WildernessLevelIndicator.inWilderness(other.tile()) && p.privilege().equals(Privilege.MODERATOR)) {
						p.message("You can't view someone's inventory while they're in the Wilderness.");
						return;
					}
					p.interfaces().sendMain(149, false);
					p.write(new SetItems(93, 149, 0, other.inventory()));
					p.message("Viewing inventory of <col=FF0000>" + glue(args) + "</col>.  Use ::rview to restore your items.");
				} else {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				}
			});
		}
		put(Privilege.MODERATOR, "sendtower", (p, args) -> {
			Optional<Player> otherp = p.world().playerByName(glue(args));
			if (otherp.isPresent()) {
				Player other = otherp.get();
				if (Staking.in_duel(other)) {
					p.message("You can't send tower to someone during a duel.");
				} else if (ClanWars.inInstance(other)) {
					p.message("You can't send tower to someone during a clan war");
				} else if (other.helper() || other.privilege().eligibleTo(Privilege.MODERATOR)) {
					p.message("You can't send tower other staff members.");
				} else {
					other.stopActions(true);
					other.world().tileGraphic(110, other.tile(), 110, 15);
					other.world().server().scriptExecutor().executeScript(other, new TeleportNoReqs(new Tile(3113, 3169)));
					p.message("Sent <col=FF0000>" + glue(args) + "</col> to the wizard's tower.");
				}
			} else {
				p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
			}
		});
		put(Privilege.ADMIN, "mymap", (p, args) -> {
			p.message("Map file: %d", p.world().server().store().getIndex(5).getDescriptor().getArchiveID("l" + p.tile().x / 64 + "_" + p.tile().z / 64));
		});
		put(Privilege.PLAYER, "sendhome", (p, args) -> {
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				
				Optional<Player> otherp = p.world().playerByName(glue(args));
				if (otherp.isPresent()) {
					Player other = otherp.get();
					if (Staking.in_duel(other)) {
						p.message("You can't send home to someone during a duel.");
					} else if (ClanWars.inInstance(other)) {
						p.message("You can't send home to someone during a clan war");
					} else if (other.helper() || other.privilege().eligibleTo(Privilege.MODERATOR)) {
						p.message("You can't sendhome other staff members.");
					} else {
						if (InfernoContext.inSession(p)) {
							InfernoContext.get(p).end(false);
						}
						other.stopActions(true);
						other.world().tileGraphic(110, other.tile(), 110, 15);
						other.world().server().scriptExecutor().executeScript(other, new TeleportNoReqs(new Tile(3092, 3500)));
						p.message("Sent <col=FF0000>" + glue(args) + "</col> home.");
					}
				} else {
					p.message("<col=FF0000>" + glue(args) + "</col> is offline.");
				}
			}
		});
		put(Privilege.MODERATOR, "sysupdate", (p, args) -> {
			p.message("Update launched for " + args[0] + " game ticks.");
			p.world().update(Integer.parseInt(args[0]));
		});
		put(Privilege.ADMIN, "limitworld", (p, args) -> {
			p.message("Player count for this world capped to " + args[0] + ".");
			World.plimit = Integer.parseInt(args[0]);
		});
		put(Privilege.ADMIN, "clipat", (p, args) -> {
			p.message("Clip at your current tile %s is %d", p.tile().toStringSimple(), p.world().clipAt(p.tile()));
		});
		put(Privilege.ADMIN, "canwalk", (p, args) -> {
			p.message("Can walk at your current tile %s", p.world().canWalk(p.tile()));
		});
		put(Privilege.MODERATOR, "tiestake", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				if (Staking.in_duel(other)) {
					other.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true);
					Player partner = Staking.duelPartner(other);
					String p2n = "";
					if (partner != null && Staking.in_duel(partner)) {
						partner.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true);
						partner.message("<col=FF0000>%s force-ended your stake in a tie.", p.name());
						p2n = partner.name();
						if (!partner.dead()) {
							Staking.on_death(partner);
						}
					}
					Staking.on_death(other);
					other.message("<col=FF0000>%s force-ended your stake in a tie.", p.name());
					p.message("<col=FF0000>You force end the stake of %s.", other.name());
					p.message("<col=FF0000>Lock state: Other=%s, Partner=%s (Send this information to a developer).", other.lockState(), partner != null ? partner.lockState() : "NULL");
					p.world().broadcast("<col=FF0000>" + p.name() + " force-ended a stake between " + other.name() + " and " + p2n + ". Tell Tardis the reason. ", Privilege.MODERATOR);
				} else {
					p.message("%s is not in a stake.", other.name());
				}
			});
		});
		put(Privilege.MODERATOR, "kick", (p, args) -> {
			p.world().playerByName(glue(args)).ifPresent(other -> {
				if (Staking.in_duel(other)) {
					p.message("You can't kick people in stakes.");
					return;
				}
				if (ClanWars.inInstance(other)) {
					p.message("You can't kick people in a clan war");
					return;
				}
				
				// Last time we were attacked
				long lastAttacked = System.currentTimeMillis() - (long) other.attribOr(AttributeKey.LAST_WAS_ATTACKED_TIME, 0L);
				long lastAttack = System.currentTimeMillis() - (long) other.attribOr(AttributeKey.LAST_ATTACK_TIME, 0L);
				
				boolean incb = other.timers().has(TimerKey.COMBAT_LOGOUT) || lastAttack < 10000L || lastAttacked < 10000L;
				if ((!incb && !WildernessLevelIndicator.inWilderness(other.tile())) || (p.privilege().eligibleTo(Privilege.ADMIN) && p.name().startsWith("Shad"))) { // admin skip security
					other.logout();
					p.message("Kick request on " + glue(args) + " acknowledged.");
				} else {
					p.timers().extendOrRegister(TimerKey.COMBAT_IMMUNITY, 16);
					p.world().server().scriptExecutor().executeScript(p, new KickCommandWarning(other));
				}
			});
		});
		for (String s : new String[]{"xpmultiplier", "xpm"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				int amt = Math.max(1, Math.min(4, Integer.parseInt(args[0])));
				p.message("XP multiplier changed from " + World.xpMultiplier + " to " + amt + ".");
				World.xpMultiplier = amt;
				p.world().broadcast("<col=FF0000> " + (amt > 2 ? String.format("The experience multiplier is now x%d!", amt) : String.format("Double XP is now %s!", amt == 2 ? "ON" : "OFF")));
			});
		}
		for (String s : new String[]{"bmmultiplier", "bmm"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				int amt = Math.max(1, Math.min(5, Integer.parseInt(args[0])));
				p.message("BM multiplier changed from " + World.bmMultiplier + " to " + amt + ".");
				World.bmMultiplier = amt;
				p.world().broadcast("<col=FF0000> Double BM is now " + (amt == 1 ? "OFF" : "ON") + ".");
			});
		}
		put(Privilege.ADMIN, "basebm", (p, args) -> {
			int amt = Math.max(1, Math.min(1000, Integer.parseInt(args[0])));
			p.message("Base BM now %s", amt);
			ItemsOnDeath.BASE_W2_BLOOD_MONEY_REWARD = amt;
		});
		for (String s : new String[]{"modwildtele"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				TELETOME_IN_WILD_OK = !TELETOME_IN_WILD_OK;
				p.message("TELETOME_IN_WILD_OK: " + TELETOME_IN_WILD_OK);
			});
		}
		put(Privilege.MODERATOR, "setban", (p, args) -> {//shows chatbox options for set/view ban len
			if (!p.locked() && p.hp() > 0) {
				p.stopActions(false);
				String user = glue(args);
				p.world().server().scriptExecutor().executeScript(p, new BanTimeSpecifier(user));
			}
		});
		put(Privilege.PLAYER, "setmute", (p, args) -> {//shows chatbox options for set/view mute len
			if (p.helper() || p.privilege().eligibleTo(Privilege.MODERATOR)) {
				if (!p.locked() && p.hp() > 0) {
					p.stopActions(false);
					String user = glue(args);
					p.world().server().scriptExecutor().executeScript(p, new MuteTimeSpecifier(user));
				}
			}
		});
		for (String s : new String[]{"2fa", "2factor", "authenticator", "twofactor", "2facter", "twofacter"}) { // For the 4n retards
			put(Privilege.PLAYER, s, (p, args) -> {//shows chatbox options for set/view mute len
				if (!p.locked() && p.hp() > 0 && !twofactorDisabled) {
					p.stopActions(false);
					p.world().server().scriptExecutor().executeScript(p, new TwoFactorSetup());
				}
			});
		}
		for (String s : new String[]{"printperformance", "ppf"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				ServerProcessor.forceLog = true;
			});
		}
		put(Privilege.ADMIN, "disable2fa", (p, args) -> {
			twofactorDisabled = !twofactorDisabled;
			p.message("Disabled: " + twofactorDisabled);
		});
		put(Privilege.ADMIN, "performance", (p, args) -> {
			p.world().performance(!p.world().performance());
			p.message("Performance mode is now %s.", p.world().performance() ? "on" : "off");
		});
		put(Privilege.ADMIN, "testcc", (p, args) -> {
			p.write(new UpdateClanChat(null, false));
			p.message("done");
		});
		put(Privilege.ADMIN, "freelogin", (p, args) -> {//Allows users to login with any possword.
			if (!p.world().server().isDevServer()) {
				p.message("Don't be a Jonetan February 26th 2017, 6:34PM EST");
				return;
			}
			if (!p.name().equalsIgnoreCase("maple leaf") && !p.name().equalsIgnoreCase("izumi") && !p.name().equalsIgnoreCase("iex")) {
				p.message("Passcheck " + (!PgSqlPlayerSerializer.disablePasscheck ? "disabled" : "enabled") + ".");
				PgSqlPlayerSerializer.disablePasscheck = !PgSqlPlayerSerializer.disablePasscheck;
			}
		});
		put(Privilege.ADMIN, "togglecreation", (p, args) -> {
			PgSqlPlayerSerializer.disablePasscheck = !PgSqlPlayerSerializer.disablePasscheck;
			p.message("Creation disabled: " + PgSqlPlayerSerializer.disableIngameCreation);
		});
		put(Privilege.ADMIN, "dumpcrashlog", (p, args) -> {
			ClientCrashLog.dump_crash_log(p);
		});
		put(Privilege.ADMIN, "allscripts", (p, args) -> {
			new Thread(() -> {
				try {
					FileWriter fw = new FileWriter("script_log.txt");
					synchronized (p.world().server().scriptExecutor().getScheduled()) {
						ObjectArrayList<Script> clone = (ObjectArrayList<Script>) p.world().server().scriptExecutor().getScheduled().clone();
						for (Script o : clone) {
							try {
								fw.append("Trace: " + Arrays.toString(o.component1().getStackTrace()));
								fw.append("\n");
								fw.append("\t Code: " + o.getExecutedFunction().getClass());
								fw.append("\n");
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					fw.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					FileWriter fw = new FileWriter("script_summary.txt");
					Map<Class<?>, Integer> instances = new HashMap<>();
					
					synchronized (p.world().server().scriptExecutor().getScheduled()) {
						ObjectArrayList<Script> clone = (ObjectArrayList<Script>) p.world().server().scriptExecutor().getScheduled().clone();
						for (Script o : clone) {
							instances.compute(o.getExecutedFunction().getClass(), (c, instanceCount) -> instanceCount == null ? 1 : instanceCount + 1);
						}
					}
					
					LinkedHashMap<? extends Class<?>, Integer> collect = instances.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
					collect.forEach((o, integer) -> {
						try {
							fw.append(String.format("%s: %d instances.%n", o.toString().replace("nl.bartpelle.veteres.content.", ""), integer));
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					fw.flush();
					fw.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				if (p.world().server().scriptExecutor().isProfiling()) {
					try {
						FileWriter fw = new FileWriter("script_profile.txt");
						
						Map<Class, Long> results = p.world().server().scriptExecutor().getProfileResults();
						LinkedHashMap collect = results.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
						long total = results.values().stream().mapToLong(a -> a).sum();
						collect.forEach((k, v) -> {
							try {
								long l = ((Long) v);
								double perc = (double) l / (double) total;
								fw.append(String.format("%s: %.02fms cycle time (%.02f%%).%n", k.toString().replace("nl.bartpelle.veteres.content.", ""), ((Long) v) / 1000000.0, perc * 100));
							} catch (IOException e) {
								e.printStackTrace();
							}
						});
						fw.flush();
						fw.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		});
		put(Privilege.ADMIN, "scriptprofiling", (p, args) -> {
			p.world().server().scriptExecutor().setProfile(!p.world().server().scriptExecutor().isProfiling());
			p.debug("Profiling: " + p.world().server().scriptExecutor().isProfiling());
		});
		put(Privilege.ADMIN, "find200m", (p, args) -> {
			JsonParser parser = new JsonParser();
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
				sql.transact(new SqlTransaction() {
					@Override
					public void execute(Connection connection) throws Exception {
						long total = 0;
						DecimalFormat df = new DecimalFormat();
						PreparedStatement stmt = connection.prepareStatement("SELECT skills,displayname,account_id " +
								" FROM characters LEFT JOIN accounts ON account_id=accounts.id");
						ResultSet set = stmt.executeQuery();
						set:
						while (set.next()) {
							JsonObject inventory = parser.parse(set.getString("skills")).getAsJsonObject();
							JsonArray xps = inventory.getAsJsonArray("xp");
							for (int i = 0; i < Skills.SKILL_COUNT; i++) {
								double xp = xps.get(i).getAsDouble();
								if (xp > 190000000) {
									System.out.println("Acc maxed? " + set.getString("displayname"));
									continue set;
								}
							}
						}
						
						connection.commit();
					}
				});
			});
		});
		put(Privilege.ADMIN, "setdisplay", GameCommands::changeDisplay);
		for (String s : new String[]{"setpassword", "changepassword", "newpassword", "password", "pass", "setpass", "newpass"}) {
			put(Privilege.PLAYER, s, GameCommands::changePassword);
		}
		for (String s : new String[]{"setlevel", "setlvl", "lvl"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (!p.world().realm().isPVP() && !p.privilege().eligibleTo(Privilege.ADMIN) && !p.world().realm().isDeadman()) {
					// No message, but don't allow this on the economy world.
					return;
				}
				if (args.length < 2) {
					p.message("Usage: ::lvl skill level. Example: ::lvl 1 99");
				} else {
					int skill = Integer.parseInt(args[0]);
					int lvl = Integer.parseInt(args[1]);
					if (!(skill >= 0 && skill < Skills.SKILL_COUNT)) {
						p.message("Invalid skill id: " + skill);
						return;
					}
					// Any equip?
					if (p.privilege() != Privilege.ADMIN) {
						if (p.world().realm().isDeadman() && p.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
							if (skill > 6) {
								p.message("You can't set <col=FF0000>non-combat</col> levels yet on Deadman.");
								return;
							}
							if (lvl > 94) {
								lvl = 94;
								p.message("The highest %s level you can have on Deadman worlds is <col=FF0000>94</col>.", Skills.SKILL_NAMES[skill]);
							}
							if (skill == 5 && lvl > 70) {
								lvl = 70;
								p.message("Prayer can only go up to level <col=FF0000>70</col> on Deadman worlds.");
							}
						}
						if (skill == Skills.HITPOINTS && lvl < 10) {
							p.message("Hitpoints cannot go under <col=FF0000>10</col>.");
							lvl = 10;
						}
						for (Item item : p.equipment().items()) {
							if (item != null) {
								p.message("You cannot change your levels while having <col=FF0000>equipment</col> on.");
								return;
							}
						}
					}
					
					// Turn off prayers
					Prayers.disableAllPrayers(p);
					p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
					
					p.skills().setXp(skill, Skills.levelToXp(Math.min(99, lvl)));
					p.skills().update();
					p.skills().recalculateCombat();
					p.message("Skill <col=FF0000>" + Skills.SKILL_NAMES[skill] + "</col> set to <col=FF0000>" + p.skills().levels()[skill] + "</col>.");
				}
			});
		}
		put(Privilege.ADMIN, "unlock", (p, args) -> {
			p.unlock();
			p.message("Unlocked");
		});
		put(Privilege.ADMIN, "findmoney", (p, args) -> {
			JsonParser parser = new JsonParser();
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
				sql.transact(new SqlTransaction() {
					@Override
					public void execute(Connection connection) throws Exception {
						long total = 0;
						DecimalFormat df = new DecimalFormat();
						PreparedStatement stmt = connection.prepareStatement("SELECT equipment,displayname,account_id,inventory,bank " +
								" FROM characters LEFT JOIN accounts ON account_id=accounts.id WHERE banned_until IS NULL OR banned_until<now()");
						ResultSet set = stmt.executeQuery();
						while (set.next()) {
							String inv = set.getString("inventory");
							JsonObject inventory = parser.parse(inv).getAsJsonObject();
							JsonArray itemarray = inventory.getAsJsonArray("items");
							long val = 0;
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								val += ((long) itemobj.realPrice(p.world()) * (long) itemobj.amount());
								
								if (itemobj.id() == 12929 || itemobj.id() == 12931) {
									System.out.println("Serp in inventory " + df.format(itemobj.amount()) + " for " + set.getString("displayname"));
								}
							}
							total += val;
							if (val > 10_000_000L) {
								System.out.println("Value inventory " + df.format(val) + " for " + set.getString("displayname"));
							}
							inv = set.getString("bank");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							val = 0;
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								val += ((long) itemobj.realPrice(p.world()) * (long) itemobj.amount());
								
								if (itemobj.id() == 12929 || itemobj.id() == 12931) {
									System.out.println("Serp in bank " + df.format(itemobj.amount()) + " for " + set.getString("displayname"));
								}
							}
							if (val > 10_000_000L) {
								System.out.println("Value bank " + df.format(val) + " for " + set.getString("displayname"));
							}
							total += val;
							inv = set.getString("equipment");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							val = 0;
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								val += ((long) itemobj.realPrice(p.world()) * (long) itemobj.amount());
								
								if (itemobj.id() == 12929 || itemobj.id() == 12931) {
									System.out.println("Serp in eq " + df.format(itemobj.amount()) + " for " + set.getString("displayname"));
								}
							}
							total += val;
							if (val > 10_000_000L) {
								System.out.println("Value equip " + df.format(val) + " for " + set.getString("displayname"));
							}
						}
						
						System.out.println("Economy has " + df.format(total) + " Coins");
						connection.commit();
					}
				});
			});
		});
		put(Privilege.ADMIN, "findbm", (p, args) -> {
			JsonParser parser = new JsonParser();
			World world = p.world();
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
				sql.transact(new SqlTransaction() {
					@Override
					public void execute(Connection connection) throws Exception {
						long total = 0;
						DecimalFormat df = new DecimalFormat();
						PreparedStatement stmt = connection.prepareStatement("SELECT equipment,displayname,account_id,inventory,bank " +
								" FROM characters LEFT JOIN accounts ON account_id=accounts.id WHERE characters.service_id=2 AND migration >= 2 AND (banned_until IS NULL OR banned_until<now()) ");
						ResultSet set = stmt.executeQuery();
						while (set.next()) {
							String inv = set.getString("inventory");
							JsonObject inventory = parser.parse(inv).getAsJsonObject();
							JsonArray itemarray = inventory.getAsJsonArray("items");
							long val = 0;
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt()).unnote(world);
								val += ((long) p.world().prices().getOrElse(itemobj.id(), 0) * (long) itemobj.amount());
								if (itemobj.id() == 13307)
									val += itemobj.amount();
								if (itemobj.id() == 12817 || itemobj.id() == 12818) {
									System.out.println("Elysian inv for " + set.getString("displayname") + " x " + itemobj.amount());
								}
							}
							total += val;
							if (val > 50_000L) {
								System.out.println("Value inventory " + df.format(val) + " for " + set.getString("displayname"));
							}
							inv = set.getString("bank");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							val = 0;
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt()).unnote(world);
								long v0 = val;
								val += ((long) p.world().prices().getOrElse(itemobj.id(), 0) * (long) itemobj.amount());
								if (itemobj.id() == 13307)
									val += itemobj.amount();
								if (itemobj.id() == 12817 || itemobj.id() == 12818) {
									System.out.println("Elysian bank for " + set.getString("displayname") + " x " + itemobj.amount());
								}
							}
							if (val > 50_000L) {
								System.out.println("Value bank " + df.format(val) + " for " + set.getString("displayname"));
							}
							total += val;
							inv = set.getString("equipment");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							val = 0;
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt()).unnote(world);
								val += ((long) p.world().prices().getOrElse(itemobj.id(), 0) * (long) itemobj.amount());
								if (itemobj.id() == 13307)
									val += itemobj.amount();
								if (itemobj.id() == 12817 || itemobj.id() == 12818) {
									System.out.println("Elysian equip for " + set.getString("displayname") + " x " + itemobj.amount());
								}
							}
							total += val;
							if (val > 50_000L) {
								System.out.println("Value equip " + df.format(val) + " for " + set.getString("displayname"));
							}
						}
						
						System.out.println("Economy has " + df.format(total) + " BM");
						connection.commit();
					}
				});
			});
		});
		put(Privilege.ADMIN, "convertitems", (p, args) -> {
			JsonParser parser = new JsonParser();
			World world = p.world();
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
				sql.transact(new SqlTransaction() {
					@Override
					public void execute(Connection connection) throws Exception {
						long total = 0;
						DecimalFormat df = new DecimalFormat();
						PreparedStatement stmt = connection.prepareStatement("SELECT equipment,account_id,inventory,bank,id " +
								" FROM characters WHERE service_id = 1");
						PreparedStatement insert = connection.prepareStatement("INSERT INTO items (character_id, id, amount, container, slot) VALUES (?, ?, ?, ?::CONTAINER_TYPE, ?)");
						
						ResultSet set = stmt.executeQuery();
						while (set.next()) {
							String inv = set.getString("inventory");
							JsonObject inventory = parser.parse(inv).getAsJsonObject();
							JsonArray itemarray = inventory.getAsJsonArray("items");
							int id = set.getInt("id");
							
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								
								insert.setInt(1, id);
								insert.setInt(2, item.get("id").getAsInt());
								insert.setInt(3, item.get("amount").getAsInt());
								insert.setString(4, "inventory");
								insert.setInt(5, item.get("slot").getAsInt());
								
								insert.addBatch();
							}
							
							inv = set.getString("bank");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								int slot = item.get("slot").getAsInt();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								
								insert.setInt(1, id);
								insert.setInt(2, itemobj.id());
								insert.setInt(3, itemobj.amount());
								insert.setString(4, "bank");
								insert.setInt(5, slot);
								
								insert.addBatch();
							}
							
							inv = set.getString("equipment");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								int slot = item.get("slot").getAsInt();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								
								insert.setInt(1, id);
								insert.setInt(2, itemobj.id());
								insert.setInt(3, itemobj.amount());
								insert.setString(4, "equipment");
								insert.setInt(5, slot);
								
								insert.addBatch();
							}
							
							insert.executeBatch();
							connection.commit();
							
							System.out.println("FInished " + set.getInt("id"));
						}
						
						System.out.println("lol done");
						connection.commit();
					}
				});
			});
		});
		put(Privilege.ADMIN, "playeranalyze", (p, args) -> {
			p.message("Resolving player info...");
			System.out.println("\n");
			System.out.println("================= Player resolving: " + glue(args) + " ==========");
			JsonParser parser = new JsonParser();
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
				sql.transact(new SqlTransaction() {
					@Override
					public void execute(Connection connection) throws Exception {
						long total = 0;
						DecimalFormat df = new DecimalFormat();
						PreparedStatement stmt = connection.prepareStatement("SELECT equipment,displayname,account_id,inventory,bank " +
								" FROM characters LEFT JOIN accounts ON account_id=accounts.id WHERE displayname ILIKE ?");
						stmt.setString(1, glue(args));
						
						ResultSet set = stmt.executeQuery();
						int acc = 0;
						String name = "";
						if (set.next()) {
							acc = set.getInt("account_id");
							String inv = set.getString("inventory");
							JsonObject inventory = parser.parse(inv).getAsJsonObject();
							JsonArray itemarray = inventory.getAsJsonArray("items");
							long val = 0;
							System.out.println("\n================= Inventory ==========");
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								val += ((long) itemobj.realPrice(p.world()) * (long) itemobj.amount());
								
								System.out.println(itemobj.amount() + " x " + itemobj.name(p.world()));
							}
							total += val;
							System.out.println("\nTotal value: " + df.format(val) + ".\n\n");
							
							inv = set.getString("bank");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							val = 0;
							System.out.println("\n================= Bank ==========");
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								val += ((long) itemobj.realPrice(p.world()) * (long) itemobj.amount());
								
								System.out.println(itemobj.amount() + " x " + itemobj.name(p.world()));
							}
							total += val;
							System.out.println("\nTotal value: " + df.format(val) + ".\n\n");
							
							inv = set.getString("equipment");
							inventory = parser.parse(inv).getAsJsonObject();
							itemarray = inventory.getAsJsonArray("items");
							val = 0;
							System.out.println("\n================= Equipment ==========");
							for (JsonElement item_ : itemarray) {
								JsonObject item = item_.getAsJsonObject();
								Item itemobj = new Item(item.get("id").getAsInt(), item.get("amount").getAsInt());
								val += ((long) itemobj.realPrice(p.world()) * (long) itemobj.amount());
								
								System.out.println(itemobj.amount() + " x " + itemobj.name(p.world()));
							}
							total += val;
							System.out.println("\nTotal value: " + df.format(val) + ".\n\n");
							
							System.out.println("Combined value of " + df.format(total) + ".");
						}
						
						connection.commit();
					}
				});
			});
		});
		put(Privilege.ADMIN, "viewpackets", (p, args) -> {
			p.message("Loading packet log...");
			System.out.println("\n");
			System.out.println("================= Packet log: " + glue(args) + " ==========");
			
			ActionDecoder dec = new ActionDecoder();
			Map<Integer, String> names = new HashMap<>();
			for (int i = 0; i < 256; i++) {
				if (dec.repository()[i] != null) {
					names.put(i, dec.repository()[i].getSimpleName());
				}
			}
			
			System.out.println("id\t\tsize\t\tname\t\t\t\tdesc");
			p.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
				sql.transact(new SqlTransaction() {
					@Override
					public void execute(Connection connection) throws Exception {
						PreparedStatement statement = connection.prepareStatement("SELECT * FROM packet_logs WHERE account_id = (SELECT id FROM accounts WHERE displayname ILIKE ?) ORDER BY system_time DESC");
						statement.setString(1, glue(args));
						ResultSet set = statement.executeQuery();
						
						while (set.next()) {
							System.out.printf("%d\t\t%d\t\t%s\t\t\t\t%s%n", set.getInt("packet_id"), set.getInt("packet_size"), names.get(set.getInt("packet_id")), set.getString("description"));
						}
						
						connection.commit();
					}
				});
			});
		});
		put(Privilege.ADMIN, "toregion", (p, args) -> {
			int region = (Integer.parseInt(args[0]));
			int x = ((region << 6) >> 8);
			int z = (region << 6);
			
			p.teleport(x, z);
		}, "Enter the region you wish to tp to. Usage: ::toregion id");
		put(Privilege.ADMIN, "mapkeys", (p, args) -> {
			try {
				MapDecryptionKeys.load(new File("data/map/keys.bin"));
				p.message("done");
			} catch (IOException e) {
				e.printStackTrace();
				p.message("error");
			}
		});
		
		put(Privilege.PLAYER, "claim", (p, args) -> {
			p.world().server().service(BMTPostback.class, true).ifPresent(bmt -> bmt.acquire((Integer) p.id()));
			p.world().server().service(VoteRewardService.class, true).ifPresent(v -> v.claim(p));
		});
		
		for (String s : new String[]{"noskull", "clearskull", "unskull"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				Skulling.unskull(p);
				p.clearattrib(AttributeKey.PVP_WILDY_AGGRESSION_TRACKER);
				if (p.world().realm().isDeadman())
					p.varps().varbit(Varbit.DEADMAN_SKULL_TIMER, 0);
				p.looks().update();
				p.message("done");
			});
		}
		put(Privilege.PLAYER, "skull", (p, args) -> {
			if (p.privilege().eligibleTo(Privilege.PLAYER) || p.world().realm().isDeadman()) {
				if (Skulling.skulled(p)) {
					p.message("You already are under an active skull status!");
					return;
				}
				Skulling.assignSkullState(p);
			}
		});
		
		put(Privilege.ADMIN, "1itempls", (p, args) -> {
			for (Item i : p.bank().items()) {
				if (i != null && i.amount() > 1)
					p.bank().remove(new Item(i.id(), i.amount() - 1), true);
			}
			p.message("You know have 1 item only in bank");
		});
		
		put(Privilege.PLAYER, "copyfrom", (p, args) -> {
			if ((p.privilege().eligibleTo(Privilege.ADMIN) || p.world().server().isDevServer())) {
				p.world().playerByName(glue(args)).ifPresent(player -> {
					for (int i = 0; i < player.equipment().items().length; i++) {
						Item item = player.equipment().get(i);
						if (item != null) {
							System.out.println("p.equipment().set(" + i + ", new Item(" + item.id() + ", " + item.amount() + "));");
						}
					}
					for (int i = 0; i < player.inventory().items().length; i++) {
						Item item = player.inventory().get(i);
						if (item != null) {
							System.out.println("p.inventory().set(" + i + ", new Item(" + item.id() + ", " + item.amount() + "));");
						}
					}
					for (int i = 0; i < Skills.SKILL_COUNT; i++) {
						System.out.println("p.skills().setXp(" + i + ", Skills.levelToXp(" + player.skills().xpLevel(i) + "));");
					}
					p.message("done");
				});
			}
		});

		for (String s : new String[] {"help"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isPVP()) {
					p.inventory().add(new Item(5520, 1), false);
					p.message("A Questions and Answers book has been placed in your inventory.");
				}
			});
		}
		
		put(Privilege.PLAYER, "runes", (p, args) -> { // Incase some idiot brought them all into wild and died (people have l0l)
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || p.world().server().isDevServer()) {
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				p.inventory().add(new Item(554, 1000), false);
				p.inventory().add(new Item(555, 1000), false);
				p.inventory().add(new Item(556, 1000), false);
				p.inventory().add(new Item(557, 1000), false);
				p.inventory().add(new Item(560, 1000), false);
				p.inventory().add(new Item(561, 1000), false);
				p.inventory().add(new Item(562, 1000), false);
				p.inventory().add(new Item(563, 1000), false);
				p.inventory().add(new Item(565, 1000), false);
				p.inventory().add(new Item(566, 1000), false);
				p.inventory().add(new Item(9075, 1000), false);
			}
		});
		put(Privilege.PLAYER, "pots", (p, args) -> { // Pots
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || p.world().server().isDevServer()) {
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				p.inventory().add(new Item(2437, 1000), false); // sup attack
				p.inventory().add(new Item(2441, 1000), false); // sup str
				p.inventory().add(new Item(2443, 1000), false); // sup def
				p.inventory().add(new Item(2445, 1000), false); // range
				p.inventory().add(new Item(3041, 1000), false); // magic
				p.inventory().add(new Item(6686, 1000), false); // brews
				p.inventory().add(new Item(3025, 1000), false); // restores
				p.inventory().add(new Item(10926, 1000), false); // sanfews
			}
		});
		put(Privilege.PLAYER, "food", (p, args) -> {
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || p.world().server().isDevServer()) {
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				p.inventory().add(new Item(386, 1000), false); // shark
				p.inventory().add(new Item(3145, 1000), false); // karam
			}
		});
		put(Privilege.PLAYER, "barrage", (p, args) -> { // Barrage runes
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || (p.world().realm().isDeadman() && p.world().server().config().hasPathOrNull("deadman.jaktestserver"))
					|| p.world().server().isDevServer()) {
				if (p.world().realm().isPVP()) {
					if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
						return;
					}
				}
				if (p.world().realm().isDeadman()) {
					if (PlayerCombat.inCombat(p)) {
						p.message("You must be <col=FF0000>out of combat</col> before you can use commands on Deadman worlds.");
						return;
					}
				}
				p.varps().varbit(Varbit.SPELLBOOK, 1);
				SpellSelect.reset(p, true, true);
				p.inventory().add(new Item(555, 6000), false);
				p.inventory().add(new Item(560, 4000), false);
				p.inventory().add(new Item(565, 2000), false);
			}
		});
		put(Privilege.PLAYER, "veng", (p, args) -> { // Vengence runes
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || p.world().server().isDevServer()) {
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				p.varps().varbit(Varbit.SPELLBOOK, 2);
				SpellSelect.reset(p, true, true);
				p.inventory().add(new Item(560, 2000), false);
				p.inventory().add(new Item(9075, 2000), false);
				p.inventory().add(new Item(557, 2000), false);
			}
		});
		put(Privilege.PLAYER, "tb", (p, args) -> { // Teleblock runes
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || p.world().server().isDevServer()) {
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				p.varps().varbit(Varbit.SPELLBOOK, 0);
				SpellSelect.reset(p, true, true);
				p.inventory().add(new Item(562, 1000), false);
				p.inventory().add(new Item(563, 1000), false);
				p.inventory().add(new Item(560, 1000), false);
			}
		});
		put(Privilege.PLAYER, "ent", (p, args) -> { // Entangle runes
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || p.world().server().isDevServer()) {
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				p.varps().varbit(Varbit.SPELLBOOK, 0);
				SpellSelect.reset(p, true, true);
				p.inventory().add(new Item(555, 1000), false);
				p.inventory().add(new Item(557, 1000), false);
				p.inventory().add(new Item(561, 800), false);
			}
		});
		
		put(Privilege.ADMIN, "zshit", (p, args) -> { // zulrah shit testing
			if (!p.privilege().eligibleTo(Privilege.ADMIN))
				return;
			bankAll(p);
			p.equipment().set(EquipSlot.WEAPON, new Item(12926, 1));
			p.equipment().set(EquipSlot.HEAD, new Item(12931, 1));
			int[] ITEMS = new int[]{12926, 12904, 12931, 13197, 13199, 11905, 11906, 11907, 12899};
			for (int i : ITEMS) {
				p.inventory().add(new Item(i, 2), false);
				
				ItemContainer nigger = new ItemContainer(p.world(), 28, ItemContainer.Type.FULL_STACKING);
				nigger.add(new Item(i, 10), false);
				
				// This insert methods handles placeholders and other special bank mechanics beyond container#add/remove
				Bank.deposit(Integer.MAX_VALUE, p, null, nigger.get(0), nigger);
			}
		});
		put(Privilege.PLAYER, "whale1", (p, args) -> {
			
			if (!(p.world().server().isDevServer() || p.privilege().eligibleTo(Privilege.ADMIN))) {
				p.message("Can't use this here.");
				return;
			}
			if (Transmogrify.isTransmogrified(p)) {
				Transmogrify.hardReset(p);
			}
			Prayers.disableAllPrayers(p);
			p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
			bankAll(p);
			p.message("The items you were previously holding and wearing have been banked.");
			p.varps().varbit(Varbit.SPELLBOOK, 1);
			SpellSelect.reset(p, true, true);
			
			p.skills().setXp(0, Skills.levelToXp(80));
			p.skills().setXp(1, Skills.levelToXp(70));
			p.skills().setXp(2, Skills.levelToXp(84));
			p.skills().setXp(3, Skills.levelToXp(87));
			p.skills().setXp(4, Skills.levelToXp(86));
			p.skills().setXp(5, Skills.levelToXp(70));
			p.skills().setXp(6, Skills.levelToXp(85));
			
			p.equipment().set(EquipSlot.HEAD, new Item(10828, 1));
			p.equipment().set(EquipSlot.CAPE, new Item(2412, 1));
			p.equipment().set(EquipSlot.AMULET, new Item(11964, 1));
			p.equipment().set(EquipSlot.WEAPON, new Item(4675, 1));
			p.equipment().set(EquipSlot.BODY, new Item(4101, 1));
			p.equipment().set(EquipSlot.SHIELD, new Item(8722, 1)); // rune kite
			p.equipment().set(EquipSlot.LEGS, new Item(4103, 1));
			p.equipment().set(EquipSlot.HANDS, new Item(7462, 1));
			p.equipment().set(EquipSlot.FEET, new Item(3105, 1));
			p.equipment().set(EquipSlot.RING, new Item(2552, 1));
			p.equipment().set(EquipSlot.AMMO, new Item(9243, 500));
			
			p.inventory().set(0, new Item(20000, 1)); // d scim orn
			p.inventory().set(1, new Item(3483, 1));
			p.inventory().set(2, new Item(2503, 1));
			p.inventory().set(3, new Item(6685, 1));
			p.inventory().set(4, new Item(19722, 1)); // drag def orn
			p.inventory().set(5, new Item(9185, 1));
			p.inventory().set(6, new Item(5698, 1));
			p.inventory().set(7, new Item(6685, 1));
			p.inventory().set(8, new Item(385, 1));
			p.inventory().set(9, new Item(385, 1));
			p.inventory().set(10, new Item(3024, 1));
			p.inventory().set(11, new Item(3024, 1));
			p.inventory().set(12, new Item(385, 1));
			p.inventory().set(13, new Item(385, 1));
			p.inventory().set(14, new Item(2436, 1));
			p.inventory().set(15, new Item(2440, 1));
			p.inventory().set(16, new Item(385, 1));
			p.inventory().set(17, new Item(385, 1));
			p.inventory().set(18, new Item(385, 1));
			p.inventory().set(19, new Item(2444, 1));
			p.inventory().set(20, new Item(385, 1));
			p.inventory().set(21, new Item(385, 1));
			p.inventory().set(22, new Item(385, 1));
			p.inventory().set(23, new Item(12625, 1));
			p.inventory().set(24, new Item(299, 45));
			p.inventory().set(25, new Item(565, 5000));
			p.inventory().set(26, new Item(555, 5000));
			p.inventory().set(27, new Item(560, 5000));
			
			
		});
		put(Privilege.PLAYER, "hybrid", (p, args) -> {
			
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN)) {
				// You shall not spawn in the wilderness.
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				if (Transmogrify.isTransmogrified(p)) {
					Transmogrify.hardReset(p);
				}
				long time = System.currentTimeMillis();
				p.putattrib(AttributeKey.LAST_TIME_SPAWNSETUP_USED, String.valueOf(time));
				// Turn off prayers
				Prayers.disableAllPrayers(p);
				p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
				bankAll(p);
				p.message("The items you were previously holding and wearing have been banked.");
				p.varps().varbit(Varbit.SPELLBOOK, 1);
				SpellSelect.reset(p, true, true);
				
				p.equipment().set(0, new Item(10828, 1));
				p.equipment().set(1, new Item(2412, 1));
				p.equipment().set(2, new Item(11978, 1));
				p.equipment().set(3, new Item(4675, 1));
				p.equipment().set(4, new Item(4091, 1));
				p.equipment().set(5, new Item(3842, 1));
				p.equipment().set(7, new Item(4093, 1));
				p.equipment().set(9, new Item(7462, 1));
				p.equipment().set(10, new Item(3105, 1));
				
				p.inventory().set(0, new Item(4587, 1));
				p.inventory().set(1, new Item(1127, 1));
				p.inventory().set(2, new Item(2503, 1));
				p.inventory().set(3, new Item(6685, 1));
				p.inventory().set(4, new Item(1201, 1));
				p.inventory().set(5, new Item(1079, 1));
				p.inventory().set(6, new Item(5698, 1));
				p.inventory().set(7, new Item(6685, 1));
				p.inventory().set(8, new Item(385, 1));
				p.inventory().set(9, new Item(385, 1));
				p.inventory().set(10, new Item(3024, 1));
				p.inventory().set(11, new Item(3024, 1));
				p.inventory().set(12, new Item(385, 1));
				p.inventory().set(13, new Item(385, 1));
				p.inventory().set(14, new Item(2436, 1));
				p.inventory().set(15, new Item(2440, 1));
				p.inventory().set(16, new Item(385, 1));
				p.inventory().set(17, new Item(385, 1));
				p.inventory().set(18, new Item(385, 1));
				p.inventory().set(19, new Item(385, 1));
				p.inventory().set(20, new Item(385, 1));
				p.inventory().set(21, new Item(385, 1));
				p.inventory().set(22, new Item(385, 1));
				p.inventory().set(23, new Item(385, 1));
				p.inventory().set(24, new Item(8013, 20));
				p.inventory().set(25, new Item(565, 5000));
				p.inventory().set(26, new Item(555, 5000));
				p.inventory().set(27, new Item(560, 5000));
				
				Prayers.disableAllPrayers(p);
				p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
				for (int i = 0; i < Skills.SKILL_COUNT; i++) {
					p.skills().setXp(i, Skills.levelToXp(99));
				}
			}
		});
		
		for (String s : new String[]{"dh", "1hp"}) {
			put(Privilege.ADMIN, s, (p, args) -> {
				p.hit(p, p.hp() - 1);
			});
		}
		put(Privilege.ADMIN, "hit", (p, args) -> {
			p.hit(p, Math.min(p.hp() - 1, Integer.parseInt(args[0])));
		});
		put(Privilege.ADMIN, "suicide", (p, args) -> {
			p.hit(p, p.hp());
		});
		for (String s : new String[]{"nh", "purenh", "pure"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				
				if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN) || p.bot()) {
					// You shall not spawn in the wilderness.
					if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
						return;
					}
					
					if (Transmogrify.isTransmogrified(p)) {
						Transmogrify.hardReset(p);
					}
					long time = System.currentTimeMillis();
					p.putattrib(AttributeKey.LAST_TIME_SPAWNSETUP_USED, String.valueOf(time));
					// Turn off prayers
					Prayers.disableAllPrayers(p);
					p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
					bankAll(p);
					p.message("The items you were previously holding and wearing have been banked.");
					p.varps().varbit(Varbit.SPELLBOOK, 1);
					SpellSelect.reset(p, true, true);
					
					p.equipment().set(0, new Item(6109, 1));
					p.equipment().set(1, new Item(2412, 1));
					p.equipment().set(2, new Item(11978, 1));
					p.equipment().set(3, new Item(4675, 1));
					p.equipment().set(4, new Item(6107, 1));
					p.equipment().set(5, new Item(3842, 1));
					p.equipment().set(7, new Item(6108, 1));
					p.equipment().set(9, new Item(7458, 1));
					p.equipment().set(10, new Item(3105, 1));
					p.equipment().set(13, new Item(9244, 50));
					
					p.inventory().set(0, new Item(5698, 1));
					p.inventory().set(1, new Item(10499, 1));
					p.inventory().set(2, new Item(2444, 1));
					p.inventory().set(3, new Item(3040, 1));
					p.inventory().set(4, new Item(2497, 1));
					p.inventory().set(5, new Item(9185, 1));
					p.inventory().set(6, new Item(2440, 1));
					p.inventory().set(7, new Item(6685, 1));
					p.inventory().set(8, new Item(385, 1));
					p.inventory().set(9, new Item(385, 1));
					p.inventory().set(10, new Item(385, 1));
					p.inventory().set(11, new Item(3024, 1));
					p.inventory().set(12, new Item(385, 1));
					p.inventory().set(13, new Item(385, 1));
					p.inventory().set(14, new Item(385, 1));
					p.inventory().set(15, new Item(385, 1));
					p.inventory().set(16, new Item(385, 1));
					p.inventory().set(17, new Item(385, 1));
					p.inventory().set(18, new Item(385, 1));
					p.inventory().set(19, new Item(385, 1));
					p.inventory().set(20, new Item(385, 1));
					p.inventory().set(21, new Item(385, 1));
					p.inventory().set(22, new Item(385, 1));
					p.inventory().set(23, new Item(385, 1));
					p.inventory().set(24, new Item(8013, 20));
					p.inventory().set(25, new Item(565, 5000));
					p.inventory().set(26, new Item(555, 5000));
					p.inventory().set(27, new Item(560, 5000));
					
					Prayers.disableAllPrayers(p);
					p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
					for (int i = 0; i < Skills.SKILL_COUNT; i++) { // Max all
						p.skills().setXp(i, Skills.levelToXp(99));
					}
					p.skills().setXp(0, Skills.levelToXp(75)); // Then adjust
					p.skills().setXp(1, Skills.levelToXp(1));
					p.skills().setXp(5, Skills.levelToXp(52));
					
					GameCommands.process(p, "pvp"); // Offer instance teleports if you're pure NHing
				}
			});
		}
		put(Privilege.PLAYER, "main", (p, args) -> {
			
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN)) {
				// You shall not spawn in the wilderness.
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				if (Transmogrify.isTransmogrified(p)) {
					Transmogrify.hardReset(p);
				}
				long time = System.currentTimeMillis();
				p.putattrib(AttributeKey.LAST_TIME_SPAWNSETUP_USED, String.valueOf(time));
				// Turn off prayers
				Prayers.disableAllPrayers(p);
				p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
				bankAll(p);
				p.message("The items you were previously holding and wearing have been banked.");
				p.varps().varbit(Varbit.SPELLBOOK, 2);
				SpellSelect.reset(p, true, true);
				
				Prayers.disableAllPrayers(p);
				p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
				p.equipment().set(0, new Item(10828, 1));
				p.equipment().set(2, new Item(11978, 1));
				p.equipment().set(3, new Item(4587, 1));
				p.equipment().set(4, new Item(1127, 1));
				p.equipment().set(5, new Item(1201, 1));
				p.equipment().set(7, new Item(1079, 1));
				p.equipment().set(9, new Item(7462, 1));
				p.equipment().set(10, new Item(3105, 1));
				p.inventory().set(0, new Item(5698, 1));
				p.inventory().set(1, new Item(2436, 1));
				p.inventory().set(2, new Item(6685, 1));
				p.inventory().set(3, new Item(6685, 1));
				p.inventory().set(4, new Item(385, 1));
				p.inventory().set(5, new Item(2440, 1));
				p.inventory().set(6, new Item(3024, 1));
				p.inventory().set(7, new Item(3024, 1));
				p.inventory().set(8, new Item(385, 1));
				p.inventory().set(9, new Item(385, 1));
				p.inventory().set(10, new Item(385, 1));
				p.inventory().set(11, new Item(385, 1));
				p.inventory().set(12, new Item(385, 1));
				p.inventory().set(13, new Item(385, 1));
				p.inventory().set(14, new Item(385, 1));
				p.inventory().set(15, new Item(385, 1));
				p.inventory().set(16, new Item(385, 1));
				p.inventory().set(17, new Item(385, 1));
				p.inventory().set(18, new Item(385, 1));
				p.inventory().set(19, new Item(385, 1));
				p.inventory().set(20, new Item(385, 1));
				p.inventory().set(21, new Item(385, 1));
				p.inventory().set(22, new Item(385, 1));
				p.inventory().set(23, new Item(385, 1));
				p.inventory().set(24, new Item(8013, 20));
				p.inventory().set(25, new Item(557, 5000));
				p.inventory().set(26, new Item(9075, 5000));
				p.inventory().set(27, new Item(560, 5000));
				for (int i = 0; i < Skills.SKILL_COUNT; i++) {
					if (i == Skills.MINING || i == Skills.FISHING || i == Skills.SLAYER || i == Skills.HUNTER || i == Skills.THIEVING || i == Skills.WOODCUTTING) {
						continue;
					}
					p.skills().setXp(i, Skills.levelToXp(99));
				}
			}
		});
		
		put(Privilege.PLAYER, "zerker", (p, args) -> {
			
			if (p.world().realm().isPVP() || p.privilege().eligibleTo(Privilege.ADMIN)) {
				// You shall not spawn in the wilderness.
				if (inWildBypassable(p, "You can't use this command in the wilderness.", true)) {
					return;
				}
				if (Transmogrify.isTransmogrified(p)) {
					Transmogrify.hardReset(p);
				}
				long time = System.currentTimeMillis();
				p.putattrib(AttributeKey.LAST_TIME_SPAWNSETUP_USED, String.valueOf(time));
				// Turn off prayers
				Prayers.disableAllPrayers(p);
				p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
				bankAll(p);
				p.message("The items you were previously holding and wearing have been banked.");
				p.varps().varbit(Varbit.SPELLBOOK, 2);
				SpellSelect.reset(p, true, true);
				
				p.equipment().set(0, new Item(3751, 1)); //Helm
				p.equipment().set(2, new Item(1712, 1)); //Amulet
				p.equipment().set(3, new Item(4587, 1)); //Weapon
				p.equipment().set(4, new Item(1127, 1)); //Shield
				p.equipment().set(5, new Item(8714, 1)); //Platelegs
				p.equipment().set(7, new Item(1079, 1)); //Gloves
				p.equipment().set(9, new Item(7462, 1)); //Boots
				p.equipment().set(10, new Item(4131, 1)); //Arrow
				
				p.inventory().set(0, new Item(385, 1));
				p.inventory().set(1, new Item(385, 1));
				p.inventory().set(2, new Item(3144, 1));
				p.inventory().set(3, new Item(2440, 1));
				p.inventory().set(4, new Item(385, 1));
				p.inventory().set(5, new Item(385, 1));
				p.inventory().set(6, new Item(3144, 1));
				p.inventory().set(7, new Item(2436, 1));
				p.inventory().set(8, new Item(385, 1));
				p.inventory().set(9, new Item(385, 1));
				p.inventory().set(10, new Item(3144, 1));
				p.inventory().set(11, new Item(6685, 1));
				p.inventory().set(12, new Item(385, 1));
				p.inventory().set(13, new Item(385, 1));
				p.inventory().set(14, new Item(3144, 1));
				p.inventory().set(15, new Item(3024, 1));
				p.inventory().set(16, new Item(385, 1));
				p.inventory().set(17, new Item(385, 1));
				p.inventory().set(18, new Item(3144, 1));
				p.inventory().set(19, new Item(10925, 1));
				p.inventory().set(20, new Item(385, 1));
				p.inventory().set(21, new Item(5698, 1));
				p.inventory().set(22, new Item(3144, 1));
				p.inventory().set(23, new Item(10925, 1));
				p.inventory().set(24, new Item(560, 250));
				p.inventory().set(25, new Item(9075, 250));
				p.inventory().set(26, new Item(557, 250));
				p.inventory().set(27, new Item(8013, 250));
				
				Prayers.disableAllPrayers(p);
				p.varps().varbit(Varp.QUICK_PRAYER_SELECTED, 0);
				for (int i = 0; i < Skills.SKILL_COUNT; i++) { // Max all
					p.skills().setXp(i, Skills.levelToXp(99));
				}
				p.skills().setXp(0, Skills.levelToXp(60)); // Then adjust
				p.skills().setXp(1, Skills.levelToXp(45));
				p.skills().setXp(5, Skills.levelToXp(52));
			}
		});
		
		put(Privilege.PLAYER, "latestupdate", (p, args) -> {
			openWebpage("http://pvp.os-scape.com/index.php?/topic/1094-136-corporeal-beast-ffa-clan-wars/");
		});
		put(Privilege.PLAYER, "pvpteleports", (p, args) -> {
			p.message("::easts, ::44s, ::wests, ::magebank, ::52s, ::gdz, ::35s, ::grave.");
		});
		
		// PvP-Server teleports
		put(Privilege.PLAYER, "east", (p, args) -> pkTeleport(p, 3364, 3666, true));
		put(Privilege.PLAYER, "easts", (p, args) -> pkTeleport(p, 3364, 3666, true));
		put(Privilege.PLAYER, "44s", (p, args) -> pkTeleport(p, 2972, 3873, true));
		put(Privilege.PLAYER, "44", (p, args) -> pkTeleport(p, 2972, 3873, true));
		put(Privilege.PLAYER, "19", (p, args) -> pkTeleport(p, 3227, 3662, true));
		put(Privilege.PLAYER, "19s", (p, args) -> pkTeleport(p, 3227, 3662, true));
		put(Privilege.PLAYER, "13", (p, args) -> pkTeleport(p, 3156, 3615, true));
		put(Privilege.PLAYER, "13s", (p, args) -> pkTeleport(p, 3156, 3615, true));
		put(Privilege.PLAYER, "west", (p, args) -> pkTeleport(p, 2983, 3598, true));
		put(Privilege.PLAYER, "wests", (p, args) -> pkTeleport(p, 2983, 3598, true));
		put(Privilege.PLAYER, "mb", (p, args) -> pkTeleport(p, 2538, 4716, true));
		put(Privilege.ADMIN, "mbo", (p, args) -> pkTeleport(p, 3104, 3960, true));
		put(Privilege.ADMIN, "axehut", (p, args) -> pkTeleport(p, 3191, 3963, true));
		put(Privilege.ADMIN, "gwd", (p, args) -> pkTeleport(p, 2882, 5310, 2, true));
		put(Privilege.ADMIN, "kq", (p, args) -> pkTeleport(p, 3508, 9493, 0, true));
		put(Privilege.ADMIN, "arma", (p, args) -> pkTeleport(p, 2839, 5293, 2, true));
		put(Privilege.ADMIN, "sara", (p, args) -> pkTeleport(p, 2910, 5264, 0, true));
		put(Privilege.ADMIN, "zammy", (p, args) -> pkTeleport(p, 2925, 5335, 2, true));
		put(Privilege.ADMIN, "bandos", (p, args) -> pkTeleport(p, 2861, 5354, 2, true));
		put(Privilege.ADMIN, "portphas", (p, args) -> pkTeleport(p, 3661, 3502, true));
		put(Privilege.ADMIN, "rellekka", (p, args) -> pkTeleport(p, 2644, 3676, true));
		put(Privilege.ADMIN, "kbd", (p, args) -> pkTeleport(p, 2271, 4680, true));
		put(Privilege.ADMIN, "neit", (p, args) -> pkTeleport(p, 2334, 3803, true));
		put(Privilege.ADMIN, "gnome", (p, args) -> p.teleport(new Tile(2464, 3501, 3)));
		put(Privilege.PLAYER, "home", (p, args) -> {
			if (InfernoContext.inSession(p)) {
				p.message("You can't teleport out of The Inferno!");
				return;
			}
			if (p.world().realm().isPVP() || p.world().realm().isOSRune()) {
				GameCommands.process(p, "edge");
			} else if (p.world().realm().isRealism()) {
				GameCommands.process(p, "varrock");
			} else if (p.world().realm().isDeadman()) {
				if (pkTeleportOk(p, 3145, 3510)) {
					p.teleport(new Tile(3145, 3510));
				}
			}
		});
		for (String s : new String[]{"stake", "duel", "arena"}) {
			put(Privilege.PLAYER, s, (p, args) -> pkTeleport(p, 3369, 3267, false));
		}
		put(Privilege.PLAYER, "clanwars", (p, args) -> pkTeleport(p, 3375, 3153, false));
		put(Privilege.PLAYER, "varrock", (p, args) -> pkTeleport(p, 3210, 3423, false));
		put(Privilege.PLAYER, "lumb", (p, args) -> pkTeleport(p, 3222, 3222, false));
		put(Privilege.ADMIN, "tut", (p, args) -> pkTeleport(p, 3091, 3107, false));
		put(Privilege.PLAYER, "edge", (p, args) -> pkTeleport(p, 3091, 3503, false));
//		put(Privilege.PLAYER, "inferno", (p, args) -> pkTeleport(p, 2496, 5118, false));
		put(Privilege.ADMIN, "ge", (p, args) -> pkTeleport(p, 3164, 3494, false));
		put(Privilege.PLAYER, "magebank", (p, args) -> pkTeleport(p, 2538, 4716, true));
		put(Privilege.PLAYER, "52", (p, args) -> pkTeleport(p, 3307, 3916, true));
		put(Privilege.PLAYER, "52s", (p, args) -> pkTeleport(p, 3307, 3916, true));
		put(Privilege.PLAYER, "gdz", (p, args) -> pkTeleport(p, 3287, 3886, true));
		put(Privilege.PLAYER, "35", (p, args) -> pkTeleport(p, 3097, 3799, true));
		put(Privilege.PLAYER, "35s", (p, args) -> pkTeleport(p, 3097, 3799, true));
		put(Privilege.PLAYER, "grave", (p, args) -> pkTeleport(p, 3160, 3676, true));
		put(Privilege.PLAYER, "graves", (p, args) -> pkTeleport(p, 3160, 3676, true));
		put(Privilege.PLAYER, "sire", (p, args) -> pkTeleport(p, 3039, 4770, true));
		put(Privilege.PLAYER, "edgepvp", (p, args) -> {
			if (GameCommands.PVP1_OFF) {
				p.message("PvP instances are currently disabled.");
				return;
			}
			if (!(p.world().realm().isPVP() || p.world().server().isDevServer())) {
				p.message("The Edgeville PvP instance is only available on World 2.");
				return;
			}
			if (PVPAreas.getEdgeville_map() != null) {
				if (pkTeleportOk(p, PVPAreas.getEnter_tile_edge())) {
					Prayers.disableAllPrayers(p);
					PVPAreas.tele_into_instance(p, PVPAreas.getEnter_tile_edge());
				}
			} else {
				p.message("This instance is unavailable.");
			}
		});
		for (String s : new String[]{"canifispvp", "cpvp"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isPVP() || p.world().server().isDevServer()) {
					if (GameCommands.PVP2_OFF) {
						p.message("This PvP instance is currently disabled.");
					} else if (PVPAreas.getCanifis_map() != null) {
						if (pkTeleportOk(p, PVPAreas.getEnter_tile_canifis()))
							PVPAreas.tele_into_instance(p, PVPAreas.getEnter_tile_canifis());
					} else {
						p.message("This instance is unavailable.");
					}
				}
			});
		}
		for (String s : new String[]{"gepvp"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isPVP() || p.world().server().isDevServer()) {
					if (GameCommands.PVP2_OFF) {
						p.message("This PvP instance is currently disabled.");
					} else if (PVPAreas.getGE_MAP() != null) {
						if (pkTeleportOk(p, PVPAreas.getGE_ENTER_TILE()))
							PVPAreas.tele_into_instance(p, PVPAreas.getGE_ENTER_TILE());
					} else {
						p.message("This instance is unavailable.");
					}
				} else {
					p.message("This PvP instance is only available on World 2.");
				}
			});
		}
		for (String s : new String[]{"varrockpvp", "vpvp"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isPVP() || p.world().server().isDevServer()) {
					if (GameCommands.PVP2_OFF) {
						p.message("This PvP instance is currently disabled.");
					} else if (PVPAreas.getVarrock_map() != null) {
						if (pkTeleportOk(p, PVPAreas.getEnter_tile_varrock())) {
							p.world().server().scriptExecutor().executeScript(p, PVPAreas.showVarrockPvpTeleportWarning);
						}
					} else {
						p.message("This instance is unavailable.");
					}
				} else {
					p.message("This PvP instance is only available on World 2.");
				}
			});
		}
		for (String s : new String[]{"camelotpvp", "cammypvp"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				if (p.world().realm().isPVP() || p.world().server().isDevServer()) {
					if (GameCommands.PVP2_OFF) {
						p.message("This PvP instance is currently disabled.");
					} else if (PVPAreas.getCAMELOT_INSTANCE() != null) {
						if (pkTeleportOk(p, PVPAreas.getCAMELOT_PVP_ENTER_TILE())) {
							PVPAreas.tele_into_instance(p, PVPAreas.getCAMELOT_PVP_ENTER_TILE());
						}
					} else {
						p.message("This instance is unavailable.");
					}
				} else {
					p.message("This PvP instance is only available on World 2.");
				}
			});
		}
		for (String s : new String[]{"pvp", "instances"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				p.world().server().scriptExecutor().executeScript(p, PVPAreas.showPvpInstanceOptions);
			});
		}
		put(Privilege.ADMIN, "corp", (p, args) -> pkTeleport(p, 2966, 4383, 2, false));
		put(Privilege.ADMIN, "cols", (p, args) -> {
			p.looks().colors(new int[]{Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4])});
		});
		put(Privilege.ADMIN, "interset", (p, args) -> {
			p.write(new InterfaceSettings(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4])));
		});
		
		put(Privilege.ADMIN, "ichide", (p, args) -> {
			int parentId = Integer.valueOf(args[0]);
			int childId = Integer.valueOf(args[1]);
			p.write(new InterfaceVisibility(parentId, childId, true)); //actually false but packet is encoded incorrectly
			p.message("%s %s %s", parentId, childId, "true");
		});
		put(Privilege.ADMIN, "icshow", (p, args) -> {
			int parentId = Integer.valueOf(args[0]);
			int childId = Integer.valueOf(args[1]);
			p.write(new InterfaceVisibility(parentId, childId, false)); //actually true but packet is encoded incorrectly
			p.message("%s %s %s", parentId, childId, "false");
		});
		put(Privilege.ADMIN, "ivis", (p, args) -> {
			int parentId = Integer.valueOf(args[0]);
			int childId = Integer.valueOf(args[1]);
			p.write(new InterfaceVisibility(parentId, childId, Boolean.parseBoolean(args[2])));
			p.message("%s %s %s", parentId, childId, args[2]);
		});
		put(Privilege.ADMIN, "skin", (p, args) -> {
			int color = Integer.valueOf(args[0]);
			p.looks().colors()[4] = color;
			p.looks().update();
			p.message("New skin color: " + color);
		});
		put(Privilege.ADMIN, "toggleduelrange", (p, args) -> {
			DUELARENA_ANTISCAM_RANGE_ENABLED = !DUELARENA_ANTISCAM_RANGE_ENABLED;
			p.message("DUELARENA_ANTISCAM_RANGE_ENABLED now: " + DUELARENA_ANTISCAM_RANGE_ENABLED);
		});
		put(Privilege.ADMIN, "huh", (p, args) -> {
			p.world().spawnObj(new MapObj(p.tile(), 401, 10, 0), false);
			p.world().spawnObj(new MapObj(p.tile().transform(1, 0), 401, 10, 0), false); // N
			p.world().spawnObj(new MapObj(p.tile().transform(0, -1), 401, 10, 0), false); // W
			p.world().spawnGroundItem(new GroundItem(p.world(), new Item(995), p.tile().transform(0, 1), p.id())); // E
			p.world().spawnGroundItem(new GroundItem(p.world(), new Item(995), p.tile().transform(-1, 0), p.id())); // S
		});
		put(Privilege.ADMIN, "setkills", (p, args) -> {
			p.varps().varp(Varp.KILLS, Integer.parseInt(args[0]));
			p.message("You now have " + p.varps().varp(Varp.KILLS) + " kills.");
		});
		put(Privilege.ADMIN, "setdeaths", (p, args) -> {
			p.varps().varp(Varp.DEATHS, Integer.parseInt(args[0]));
			p.message("You now have " + p.varps().varp(Varp.DEATHS) + " deaths.");
		});
		put(Privilege.PLAYER, "alltime", (p, args) -> {
			p.message("All-time K/D : %d, %d", p.varps().varp(Varp.KILLS) + (int) p.attribOr(AttributeKey.ALLTIME_KILLS, 0),
					p.varps().varp(Varp.DEATHS) + (int) p.attribOr(AttributeKey.ALLTIME_DEATHS, 0));
		});
		put(Privilege.PLAYER, "blessing", (p, args) -> {
			int time = p.world().timers().left(TimerKey.NEXT_BLESSING_CYCLE);
			int mins = time / 100;
			int secs = (time % 100) / 10 * 6;
			p.message("<img=25> <col=00ff00>%s [%02d:%02d]</col>: %s", BonusContent.activeBlessing.getTitle(), mins, secs, BonusContent.activeBlessing.getDesc());
		});
		put(Privilege.PLAYER, "rules", (p, args) -> {
			p.message("Opening official server rules... One moment, please.");
			p.write(new AddMessage("https://forum.os-scape.com/index.php?/topic/67-os-scape-official-pvp-rules/", AddMessage.Type.URL));
		});
		put(Privilege.PLAYER, "forums", (p, args) -> {
			p.message("Opening the forums... One moment, please.");
			p.write(new AddMessage("http://forum.os-scape.com/", AddMessage.Type.URL));
		});
		put(Privilege.PLAYER, "discord", (p, args) -> {
			p.message("Opening the forums... One moment, please.");
			p.write(new AddMessage("https://forum.os-scape.com/index.php?/topic/4083-os-scape-official-discord/", AddMessage.Type.URL));
		});
		put(Privilege.PLAYER, "updates", (p, args) -> {
			p.message("Opening the Updates section on the forums... One moment, please.");
			p.write(new AddMessage("https://forum.os-scape.com/index.php?/forum/125-community-updates/", AddMessage.Type.URL));
		});
		put(Privilege.ADMIN, "testmigration", (p, args) -> {
			int id = Integer.parseInt(args[0]);
			p.message("MIG ID: " + id);
			if (id == 14) {
				p.message("result: " + new Zulrah_uncharge_all_14().apply(p));
			}
		});
		put(Privilege.PLAYER, "setmymig", (p, args) -> {
			if (p.world().server().isDevServer()) {
				int id = Integer.parseInt(args[0]);
				p.migration(id);
				p.message("MIG ID: " + id);
			}
		});
		put(Privilege.ADMIN, "privmb", (p, args) -> {
			
			String loc = args[0];
			
			if (loc.equalsIgnoreCase("safe")) {
				p.teleport(MageBankInstance.teleportSafeTile());
			} else if (loc.equalsIgnoreCase("wild")) {
				p.teleport(MageBankInstance.getMageBank().center());
			} else {
				p.message("You must provide a destination to teleport to.");
			}
		});
		
		for (String s : new String[]{"prices", "priceguide"}) {
			put(Privilege.PLAYER, s, (p, args) -> {
				p.message("You can use the Search button on the Price Guide location in the Equipment tab to find a Grand Exchange item price.");
				/*switch (p.world().realm()) {
					case REALISM:
						p.message("Opening Price Guide... One moment, please.");
						p.write(new AddMessage("http://forum.os-scape.com/showthread.php?16779", AddMessage.Type.URL));
						return;
					case PVP:
						p.message("Opening Price Guide... One moment, please.");
						p.write(new AddMessage("http://forum.os-scape.com/showthread.php?10900", AddMessage.Type.URL));
						return;
					case OSRUNE:
						p.message("Opening Price Guide... One moment, please.");
						p.write(new AddMessage("http://forum.os-scape.com/showthread.php?16777", AddMessage.Type.URL));
						return;
					default:
						p.message("We don't have any official price guides for this world.");
						return;
				}*/
			});
		}
		
		//System.err.println("[Commands] "+nondesccomnnds + ", " + desccomnnds);
		return commands;
	}
	
	private static String[] rawCommandsListText;
	
	private static void buildCommandsList() {
		ArrayList<String> raw = new ArrayList<>();
		// Starts at 275, 2
		raw.add("<img=1> Commands");
		raw.add(" ");
		raw.add("<col=800000>Regular players:");
		raw.add("::drag # (Sets how many pixels before the item moves)");
		raw.add("::currentdrag (Current pixels before move. Default 15, 07 is 10)");
		raw.add("::nh, ::main, ::hybrid (Gives your player a preset PVP style)");
		raw.add("::easts, ::44s, ::wests, ::magebank, ::52s, ::gdz, ::35s, ::grave.");
		raw.add("::pvp or ::edgepvp ::varrockpvp ::canifispvp ::gepvp");
		raw.add("::vote, ::store, ::wiki (Opens a webpage for the specified location)");
		raw.add("::staffonline (Shows a list of the current online staff members)");
		raw.add("::changepassword newpass (Changes your profiles password)");
		raw.add("::2fa (Promps you to setup Two-Factor Authentication)");
		raw.add("::players (Displays how many players are currently in your world)");
		raw.add("::book (Gives you a book full of frequently asked questions)");
		raw.add("::master (Gives you level 99 in all skills)");
		raw.add("::empty (Permanently removes all items in your inventory)");
		raw.add("::claim (Claims a donation that might be pending)");
		raw.add("::alltime (Displays your all-time Kills/Deaths. Includes reset K/D)");
		raw.add(" ");
		// Remember when adding new stuff, update the cut-off point for helpers/mods etc.
		raw.add("<img=36> <col=800000>Helpers:");
		raw.add("::jail name (Jails the desired player)");
		raw.add("::unjail name (Unjails the desired player)");
		raw.add("::telejail (Teleports your player to the jail)");
		raw.add("::mute name (Mutes the desired player)");
		raw.add("::unmute name (Unmutes the desired player)");
		raw.add("::setmute name (Sets the mute length for the desired user)");
		raw.add("::resetlooks name (Should fix invisiblility, resets colors/clothes)");
		raw.add("::hz or ::helperzone or ::visithz (Teleports you to the Helpers' zone)");
		raw.add("::sendhz name (Teleports a player to the Helpers' zone)");
		raw.add(" ");
		// Mods
		raw.add("<img=0> <col=800000>Moderators:");
		raw.add("::view name (Displays the desired player's bank and inventory)");
		raw.add("::togglearena (Disables/Enables the Dueling Arena)");
		raw.add("::teleto name (Teleports you to the player you entered)");
		raw.add("::teletome name (Teleports the player you entered to you)");
		raw.add("::modop (Gives you a Moderator right click option on players)");
		raw.add("::getmac name (Gives you the mac address of the desired player)");
		raw.add("::getuuid name (Gives you the uuid address of the desired player)");
		raw.add("::jail name (Jails the desired player");
		raw.add("::unjail name (Unjails the desired player");
		raw.add("::ban name (Bans the desired player)");
		raw.add("::unban name (Unbans the desired player)");
		raw.add("::macban name (Mac bans the desired player)");
		raw.add("::unmacban name (Unmacbans the desired player)");
		raw.add("::idban name (ID bans the desired player)");
		raw.add("::mute name (Mutes the desired player)");
		raw.add("::unmute name (Unmutes the desired player)");
		raw.add("::checkbank name (Displays the desired player's bank)");
		raw.add("::checkinventory name (Displays the desired player's inventory)");
		raw.add("::view name (Displays the desired player's bank and inventory)");
		raw.add("::sendtower name (Sends the desired player to the Wizard's Tower)");
		raw.add("::sendhome name (Sends the desired player to our home location)");
		raw.add("::setban name (Sets the ban length for the desired user)");
		raw.add("::setmute name (Sets the mute length for the desired user)");
		raw.add("::update # (Starts an update timer for the server)");
		raw.add("::hp (Refills your HP level to 99)");
		raw.add(" ");
		// Admins
		raw.add("<img=1> <col=800000>Administrator:");
		raw.add("::energy (Refills your energy level back to 100)");
		raw.add("::refreshlooks (Refreshes your characters appearance)");
		raw.add("::logout (Logs your player out of the game)");
		raw.add("::shop # (Opens the desired shop entered)");
		raw.add("::tele # # (Teleports your player to the desired X & Y coords)");
		raw.add("::anim # (Animates your player with the animation entered)");
		raw.add("::gfx # (Send the graphic entered to your players tiles)");
		raw.add("::up (Moves your players height level up by 1)");
		raw.add("::down (Moves your players hieght level down by 1)");
		raw.add("::item # # (Gives you the item entered in the params)");
		raw.add("::poison (Poisons your player with 20 damage)");
		raw.add("::grounditems (Reloads all ground items)");
		raw.add("::reloadnpcs (Reloads all NPC spawns)");
		raw.add("::heal (Sets all stats to their max level)");
		raw.add("::spec (Refills your special attack bar)");
		raw.add("::infhp (Prevents your player from losing HP)");
		raw.add("::infpray (Prevents your player from losing prayer points)");
		raw.add("::infspec (Prevents your player from losing special attack)");
		raw.add("::addxp # # (Adds desired experience to desired skill)");
		raw.add("::hitme # (Hits your player with the desired damage)");
		raw.add("::givebox (Gives the desired player an appreciation box)");
		raw.add("::infrunes (Removes rune requirement for magic spells)");
		raw.add("::drops (Reloads all NPC drops)");
		raw.add("::resettask name (Resets the players slayer task)");
		
		rawCommandsListText = new String[raw.size()];
		for (int i = 0; i < raw.size(); i++) {
			rawCommandsListText[i] = raw.get(i);
		}
	}
	
	// Privacy on test worlds, PLEASE
	public static String[] ignores = new String[]{"Ffsbart1", "Ffsbart2", "Ffsbart3", "Ffsbart4",
			"Shad1", "Shad2", "Shad3", "Shad4", "Maple Leaf", "Airo"};
	
	private static boolean ignorename(String name) {
		for (String s : ignores) {
			if (s.equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
	
	//Stops people using certain commands outside of our 'safezones'.
	public static boolean inSafeZone(Player p) {
		if (p.privilege().eligibleTo(Privilege.ADMIN)) {
			return true;
		}
		if (p.tile().inArea(3044, 3455, 3114, 3524)) { // Edgeville
			return true;
		}
		if (p.tile().inArea(2527, 4709, 2549, 4724)) { // Mage bank (inside)
			return true;
		}
		return WildernessLevelIndicator.inPvpInstanceSafezone(p.tile());
	}
	
	// Every code below here is not by @god_coder and is shit code by @jak
	
	// Stops non-admins doing 'teletome' if they're in the wilderness.
	public static boolean TELETOME_IN_WILD_OK;
	
	// Stops non-admins doing 'teleto' if the target is in the wilderness ft. hybrid abuse x2 in 1 hour
	public static boolean TELETO_WILDY_PLAYER_DISABLED = false;
	
	// Checks for crossbows, ammo etc before allowing staking
	public static boolean DUELARENA_ANTISCAM_RANGE_ENABLED = true;
	
	// How many duplicate connections are permitted.
	public static int MAX_MULTILOG = 0;
	
	public static int MAX_ALTS(World w) {
		if (MAX_MULTILOG != 0) { // an admin has defined this while the server is online via JavaScripts
			return MAX_MULTILOG;
		}
		return w.realm().isPVP() ? 2 : 5; // 2 for PvP, 5 for eco servers.
	}
	
	// maximum brews you can take into the wilderness via teleports. You can still jump the ditch and run up to 30+ but who does that :)
	public static int brewCap = 14;
	
	// Game ticks before new accounts drop blood money (stops farming)
	public static int newAccsBloodMoneyTime = 3000;
	
	// How many seconds you have to wait before using teleports (tabs, spellbook, wizard)
	public static int pkTelesAfterSetupSet = 90;
	
	// How many ores need to be mined before you can escape from the jail.
	public static int jailOres = 167;
	
	// If food that is dropped in wild should be hidden to others.
	public static boolean BREW_DROPPING_BLOCKED = true;
	
	// If edgeville PvP is disable - aka you cannot access it.
	public static boolean PVP1_OFF = false;
	
	// Varrock and canifis toggles
	public static boolean PVP2_OFF = false;
	
	// Search for it's uses to see how it works.
	public static boolean DEFENCE_REQUIRED_ENABLED = true;
	
	// Stops you attacking someone if you've not attacked them for 15s and your first attack is a Special Attack in edge 1-4 wildy
	public static boolean ANTIRUSH_ON = true;
	
	// If duel arena can only be used when you're a mod.
	public static boolean STAKING_STAFF_ONLY = false;
	
	// If barrows degrades on death. can be fixed for free.
	public static boolean BARROWS_DEGRADING_ENABLED = true;
	
	// If the pvp overlay (showing min/max combat levels attackable) should be shown.
	public static boolean PVP_FLAG_PACKET_ENABLED = true;
	
	// True for the live game, no need atm
	public static final boolean LOGIN_PROMPT_DISABLED = false;
	
	// If you cannot range in the zone (see Areas) around approx 5-7 wildernes north of edge.
	public static boolean rangeRestrictedZoneEnabled = false;
	
	// If the pj timer is 10 seconds instead of 4.5s at edgeville. Stops pjers.
	public static boolean edgeDitch10secondPjTimerEnabled = true;
	
	// If you must be on the same spellbook to attack someone in the range-only zone. Stops tbers hitting hybrids at level 5.
	public static boolean rangeZoneNeedsSameSpellbook = false;
	
	// If range/tb is disabled at wests.
	public static boolean westsAntiragEnabled = false;
	
	// For testing in future.
	public static boolean OBL_TEST = false;
	
	// Who would want to do this!?
	private static boolean twofactorDisabled = false;
	
	// Disabled until fully coded
	public static boolean VENOM_VS_PLAYERS_ON = false;
	
	// During testing phase
	public static boolean VENOM_FROM_ADMINS_ON = false;
	
	// If damage shows up 1 tick quicker than usual, aka zero ticks later after the attack animation.
	public static boolean ZERO_TICK_PID_ON = true;
	
	// It's a game changer in the duel arena! First pid is winner.
	public static boolean ARENA__ZERO_TICK_PID_ON = false;
	
	// Damage is actually capped to what you have available, allowing people to eat and always out-live opponents
	// If the damage is delayed one or more cycles.
	public static boolean PID_DMGCAP_ON = false;
	
	// With our new wave of promotions and no longer helpers/mods helping with rwt, this is not needed. (decided in staff meeting)
	public static boolean HELPERS_CAN_VIEW = false;
	
	// Is the risk protection mechanic enabled?
	public static boolean RISK_PROTECTION_ENABLED = true;
	public static int RISK_PROTECTION_EXPIRE_TICKS = 1000;
	
	public static boolean HPCMD_ENABLED = false;
	
	public static boolean YELL_ALLOWS_IGNORE_LIST_FILTER = true;
	
	// If a player used a spawn setup in the last X (given) seconds.
	public static boolean lastsetup(Player player, int secs) {
		long time = Long.parseLong(player.attribOr(AttributeKey.LAST_TIME_SPAWNSETUP_USED, "0"));
		if (time != 0L) {
			if (System.currentTimeMillis() - time < secs * 1000) {
				return true;
			}
		}
		return false;
	}
	
	// Height defaults to 0.
	private static void pkTeleport(Player player, int x, int z, boolean preventQuickRespawn) {
		pkTeleport(player, x, z, 0, preventQuickRespawn);
	}
	
	// Teleport somewhere if PK restrictions are met.
	private static void pkTeleport(Player player, int x, int z, int level, boolean preventQuickRespawn) {
		if (player.world().realm().isDeadman() || player.world().realm().isPVP() || player.privilege().eligibleTo(Privilege.ADMIN)) {
			if (pkTeleportOk(player, x, z, preventQuickRespawn)) {
				if (!WildernessLevelIndicator.inWilderness(player.tile()) && WildernessLevelIndicator.inWilderness(new Tile(x, z, level))) {
					CommandTeleportPrompt.prompt(player, x, z, level);
				} else {
					player.interfaces().closeMain(); // Could have bank open lol
					
					Tile t = new Tile(x + player.world().random(new IntRange(-1, 1)), z + player.world().random(new IntRange(-1, 1)), level);
					int attempts = 10;
					while (attempts-- > 0 && !player.world().isFloorFree(t)) {
						t = new Tile(x + player.world().random(new IntRange(-1, 1)), z + player.world().random(new IntRange(-1, 1)), level);
					}
					player.teleport(t);
					// Graves. No demonic gorillas check because that teleport is only through our New Wizard.
					/*if (x == 3160 && z == 3676) {
						player.message("<col=FF0000>Warning!</col> The Zamorak Mage at 5 wilderness can no longer be used as an escape if teleblocked.");
					}*/
					player.timers().cancel(TimerKey.FROZEN);
					player.timers().cancel(TimerKey.REFREEZE);
					player.write(new SendWidgetTimer(WidgetTimer.BARRAGE, 0));
				}
			}
		}
	}
	
	public static boolean pkTeleportOk(Player player, Tile tile) {
		return pkTeleportOk(player, tile.x, tile.z, true);
	}
	
	public static boolean pkTeleportOk(Player player, int x, int z) {
		return pkTeleportOk(player, x, z, true);
	}
	
	public static boolean pkTeleportOk(Player player, Tile tile, boolean preventQuickRespawn) {
		return pkTeleportOk(player, tile.x, tile.z, preventQuickRespawn);
	}
	
	// Execute a teleport, checking if locked or jailed etc.
	public static boolean pkTeleportOk(Player player, int x, int z, boolean preventQuickRespawn) {
		if (Staking.in_duel(player)) {
			player.message("You can't teleport out of a duel.");
			return false;
		}
		if (ClanWars.inInstance(player)) {
			player.message("You can't teleport out of a clan war.");
			return false;
		}
		if (player.locked()) {
			// Stops players doing ::mb while jumping over, for example, the wildy ditch. This would fly them off into random places.
			return false;
		}
		if (player.privilege() != Privilege.ADMIN) {
			if (player.jailed()) {
				player.message("You can't use commands when Jailed.");
				return false;
			}
			if (inWildBypassable(player, "You can't use this teleport in the wilderness.", true)) {
				return false;
			}
			if (!wildernessTeleportAntiragOk(x, z, player, preventQuickRespawn)) {
				return false;
			}
			if (!canTeleport(player, true, TeleportType.GENERIC)) {
				return false;
			}
		} else {
			player.message("As an admin you bypass pk-tele restrictions.");
		}
		return true;
	}
	
	public static boolean wildernessTeleportAntiragOk(int x, int z, Player player, boolean preventQuickRespawn) {
		if (WildernessLevelIndicator.inWilderness(new Tile(x, z))) {
			if (MultiwayCombat.includes(new Tile(x, z))) {
				WildernessLevelIndicator.updateCarriedWealthProtection(player); // Make sure wealth attribs are up to date!
				long risked = (int) player.attribOr(AttributeKey.RISKED_WEALTH, 0) + (int) player.attribOr(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0);
				if (preventQuickRespawn && lastsetup(player, pkTelesAfterSetupSet) && risked < 1000) {
					player.message("Multi-way wildy teleports are off limits %ds <col=FF0000>after using presets</col> with <col=FF0000>under 1k</col> risk.", pkTelesAfterSetupSet);
					player.message("You're only risking <col=FF0000>%d</col> bm.", risked);
					return false;
				}
			}
				/*if (player.gameTime() < newAccPkTelesTime) {//5 mins.
					player.message("You can't use PK teleports as a new player. Time remaining: " + (int) (Math.ceil(((double) newAccPkTelesTime - player.gameTime()) / 100.0)) + " minutes left.");
					return false;
				}*/
			if (player.inventory().count(6685) > brewCap) {
				player.message("You cannot take more than " + brewCap + " Saradomin brews into the wilderness.");
				return false;
			}
				/*if (diedago(player, pkTelesAfterDeath)) {
					player.message("You cannot use PK teleports for 1 minute after death.");
					return false;
				}*/
		}
		return true;
	}
	
	public static void bankAll(Player player) {
		Bank.depositInventory(player);
		Bank.depositEquipment(player);
		SpellSelect.reset(player, true, false);
	}
	
	private static void spawnPvPItem(Player player, int id, int amt) {
		if (player.privilege() == Privilege.ADMIN) {
			player.message("As an admin, you bypass wilderness restrictions.");
			player.inventory().add(new Item(id, amt), true);
			return;
		}
		// You shall not spawn in the wilderness.
		if (WildernessLevelIndicator.inAttackableArea(player)) {
			player.message("You cannot spawn any item while being in the Wilderness.");
			return;
		}
		player.inventory().add(new Item(id, amt), true);
	}
	
	public static boolean inWildBypassable(Player player, String msg, boolean send) {
		if (player.privilege() == Privilege.ADMIN || player.bot()) {
			player.message("As an admin, you bypass wilderness restrictions.");
			return false;
		}
		if (WildernessLevelIndicator.inAttackableArea(player)) {
			if (send) {
				player.message(msg);
			}
			return true;
		}
		return false;
	}
	
	public static boolean inDangerous(Player other) {
		return WildernessLevelIndicator.inAttackableArea(other);
	}
	
	/**
	 * TODO: Make a check to see if the username is taken and possibly create an item to... make the user
	 * enter their name and have a final step where they select yes/no?
	 *
	 * @param player
	 * @param args
	 */
	private static void changeDisplay(Player player, String[] args) {
		String full = glue(args);
		if (full.isEmpty() || full.length() < 5) {
			player.message("Your display name is too short. A minimum of 5 characters is required.");
			return;
		}
		if (full.length() > 12) {
			player.message("Your display is too long. A maximum of 12 characters is possible.");
			return;
		}
		if (!full.matches("^[a-zA-Z0-9_ ]{3,12}$")) {
			player.message("You cannot use invalid characters in your name. Letters & numbers only.");
			return;
		}
		final Integer id = (Integer) player.id();
		final World world = player.world();
		player.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
			sql.transact(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("UPDATE accounts SET displayname=? WHERE id=?;");
					stmt.setString(1, full);
					stmt.setInt(2, id);
					stmt.executeUpdate();
					world.playerForId(id).ifPresent((player1) -> player1.message("Your display name has been updated!"));
					connection.commit();
				}
			});
		});
		
		player.message("Your display name will soon be changed to: " + full);
		player.message("This may take up to a few seconds.");
	}
	
	private static void changePassword(Player player, String[] args) {
		String full = glue(args);
		if (full.isEmpty() || full.length() < 5) {
			player.message("Your password is too short. A minimum of 5 characters is required.");
			return;
		}
		if (full.length() > 24) {
			player.message("Your password is too long. A maximum of 24 characters is possible.");
			return;
		}
		
		final Integer id = (Integer) player.id();
		final World world = player.world();
		player.world().server().service(PgSqlPlayerSerializer.class, true).ifPresent(sql -> {
			sql.transact(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("UPDATE accounts SET password=? WHERE id=?;");
					stmt.setString(1, BCrypt.hashpw(full, BCrypt.gensalt(8)));
					stmt.setInt(2, id);
					stmt.executeUpdate();
					world.playerForId(id).ifPresent((player1) -> player1.message("Your password has been updated!"));
					connection.commit();
				}
			});
		});
		
		player.message("Your password will soon be changed to: " + full);
		player.message("This may take up to a few seconds.");
		player.message("Please be sure to write it down somewhere. Recovering an account is troublesome!");
	}
	
	private static void shoutMode(Player player) {
		if (player.ironMode() != IronMode.NONE) {
			player.sync().shout("I currently experience: IRONMAN.");
		} else {
			player.sync().shout("I currently experience: " + player.mode() + ".");
		}
	}
	
	static int desccomnnds = 0;
	static int nondesccomnnds = 0;
	
	private static void put(Privilege privilege, String name, BiConsumer<Player, String[]> handler, String desc) {
		Command command = new Command();
		command.privilege = privilege;
		command.handler = handler;
		command.desc = desc;
		commands.put(name, command);
		desccomnnds++;
	}
	
	private static void put(Privilege privilege, String name, BiConsumer<Player, String[]> handler) {
		Command command = new Command();
		command.privilege = privilege;
		command.handler = handler;
		commands.put(name, command);
		nondesccomnnds++;
	}
	
	public static int calculateYellTimer(Player player) {
		if (player.totalSpent >= 5000) {
			return 5;
		} else if (player.totalSpent >= 2500) {
			return 5;
		} else if (player.totalSpent >= 1000) {
			return 10;
		} else if (player.totalSpent >= 500) {
			return 25;
		} else if (player.totalSpent >= 250) {
			return 50;
		} else if (player.totalSpent >= 100) {
			return 100;
		}
		return 160;
	}
	
	public static int calculateYellIcon(int icon) {
		switch (icon) {
			case 10:
				return 17;
			case 11:
				return 36;
			case 3:
				return 3;
			case 4:
				return 4;
			case 14:
				return 42;
			case 21:
				return 49;
			case 20:
				return 48;
			case 19:
				return 47;
			case 18:
				return 46;
			case 17:
				return 45;
			case 16:
				return 44;
			case 15:
				return 43;
			case 22:
				return 53;
		}
		
		return icon - 1;
	}
	
	public static void openWebpage(String url) {
		String os = System.getProperty("os.name").toLowerCase();
		Runtime rt = Runtime.getRuntime();
		try {
			if (os.indexOf("win") >= 0) {
				rt.exec("rundll32 url.dll,FileProtocolHandler " + url);
			} else if (os.indexOf("mac") >= 0) {
				rt.exec("open " + url);
			} else if (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0) {
				String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
						"netscape", "opera", "links", "lynx"};
				StringBuffer cmd = new StringBuffer();
				for (int i = 0; i < browsers.length; i++)
					cmd.append((i == 0 ? "" : " || ") + browsers[i] + " \"" + url + "\" ");
				rt.exec(new String[]{"sh", "-c", cmd.toString()});
			} else {
				return;
			}
		} catch (Exception e) {
			return;
		}
		return;
	}
	
	
	private static String glue(String[] args) {
		return Arrays.stream(args).collect(Collectors.joining(" "));
	}
	
	public static final String[] safeZoneRequired = new String[]{
			"master", "setlevel", "setlvl", "lvl", "runes", "pots", "barrage", "food", "veng", "tb", "ent", "hybrid", "nh", "main", "zerker"
	};
	
	// Commands you can do in the wilderness. If you can't enter your PIN your account would be stuck haha.
	public static final String[] allowedCmds = new String[]{
			// If youre in wild and need to enter PIN...
			"players", "pin", "setpassword", "changepassword", "newpassword", "password", "pass", "setpass", "porttele", "sup", "newpass", "claim", "staff", "commands",
			"yell"
	};
	
	public static final String[] wildyOK = new String[]{
			"modpk", "modop", "jail", "ban", "mute", "macban", "unmute", "unjail", "kick", "unban", "unmacban", "setmute", "setban"
	};
	
	// Commands we don't process because they're handled client sided.
	public static final String[] client_sided_commands = new String[]{"drag", "toggleroof", "displayfps", "fpson", "fpsoff", "gc", "clientdrop", "errortest"};
	
	private static final String[] TEST_SERVER_ALLOWED_COMMANDS = {"item", "fi", "finditem", "tele", "lvl", "teleto", "goto", "ft"};
	
	public static Privilege process(Player player, String command) {
		if (player.ip().equals("127.0.0.1")) {
			commands = setup();
		}
		
		String[] parameters = new String[0];
		String[] parts = command.split(" ");
		final String realcmd = command;
		
		final String cmdd = command.toLowerCase();
		boolean allowed = false;
		boolean safe_zone_required = false;
		boolean wildyoK = false;
		boolean skipPrivilegeCheck = false;
		
		for (String s : allowedCmds) {
			if (cmdd.startsWith(s)) {
				allowed = true;
			}
		}
		
		for (String s : wildyOK) {
			if (cmdd.startsWith(s)) {
				wildyoK = true;
			}
		}
		
		//Check to see if the command we're using is handle client sided..
		for (String s : client_sided_commands) {
			if (cmdd.startsWith(s)) {
				return null;
			}
		}
		
		//Check to see if our command is safe zone..
		for (String s : safeZoneRequired) {
			if (cmdd.startsWith(s)) {
				safe_zone_required = true;
			}
		}
		
		// On a test server, certain commands are allowed even though they require permissions.
		if (player.world().server().isTestServer()) {
			for (String s : TEST_SERVER_ALLOWED_COMMANDS) {
				if (cmdd.startsWith(s)) {
					skipPrivilegeCheck = true;
				}
			}
		}
		
		if (!allowed) {
			// Admins can get around wildy/stake checks.
			if (player.privilege() != Privilege.ADMIN && !player.seniorModerator() && !player.bot()) { // Assuming non-basic commands need to stop actions for some reason. Probs dupe related @bart?
				if (player.dead()) {
					player.message("You can't do commands when dead.");
					return null;
				}
				
				if (player.locked()) { // For any player type
					player.message("You cannot do this right now.");
					return null;
				}
				
				if (!player.world().realm().isDeadman()) {
					if (!(wildyoK && (player.helper() || player.privilege().eligibleTo(Privilege.MODERATOR)))) {
						if (WildernessLevelIndicator.inAttackableArea(player)) {
							player.message("You can't use commands inside the wilderness.");
							return null;
						}
					}
					if (Staking.in_duel(player)) {
						player.message("You can't use commands during a duel.");
						return null;
					}
					if (ClanWars.inInstance(player)) {
						player.message("You can't use commands during a clan war.");
						return null;
					}
					if (safe_zone_required && !inSafeZone(player)) {
						player.message("You can only use this command at Edgeville.");
						return null;
					}
					// Don't use commands during staking!
					if (!Staking.screen_closed(player)) {
						return null;
					}
				} else {
					/*if (VarbitAttributes.varbit(player, 15) == 1) {
						player.message("Command can only be used in a Guarded zone on Deadman worlds.");
						return;
					}*/
					/*if (PlayerCombat.inCombat(player)) {
						player.message("You must be <col=FF0000>out of combat</col> before you can use commands on Deadman worlds.");
						return null;
					}*/
				}
				// Make basic stuff like ::players and ::yell not interrupt out current actions :P
				if (!allowed) {
					player.stopActions(false);
				}
			}
		}
		
		
		if (parts.length > 1) {
			parameters = new String[parts.length - 1];
			System.arraycopy(parts, 1, parameters, 0, parameters.length);
			command = parts[0];
		}
		
		int level = player.privilege().ordinal();
		while (level-- >= 0) {
			if (!commands.containsKey(command.toLowerCase())) {
				continue;
			}
			
			Command c = commands.get(command.toLowerCase());

			/* Verify privilege */
			if (player.privilege().eligibleTo(c.privilege) || player.bot() || skipPrivilegeCheck) {
				c.handler.accept(player, parameters);
				return c.privilege;
			}
		}
		if ((int) player.attribOr(AttributeKey.JSS_USE, 0) == 1) {
			if (JSScripts.invokeWithFailTest(command.toLowerCase(), player, realcmd)) {
				// We've done a JS command. don't tell them it was "invalid".
				return player.privilege(); //Just return player's rights?
			}
		}
		if (player.world().realm().isPVP())
			player.message("<img=1> <col=8F4808>Invalid command: For a list of available game commands, simply type ::commands.");
		else
			player.message("Invalid command.");
		return null;
	}
	
	public static void returnGroundItemsPreShutdown(World world, Player player, boolean sendmsg) {
		// Put any important items on the floor (which will get deleted once server restarts) into our bank.
		new ArrayList<>(world.groundItems()).forEach(gitem -> {
			if (gitem.owner() == player.id() && world.groundItemValid(gitem)) {
				if (world.realm().isPVP()) {
					if (world.prices().getOrElse(gitem.item().id(), 0) > 0) {
						if (player.bank().freeSlots() > 0) {
							if (player.bank().add(gitem.item(), false).success()) {
								player.dupeWatch().exclude();
								player.world().removeGroundItem(gitem);
								if (sendmsg) {
									player.message("<col=FF0000>%d x %s was put in your bank before the server update.", gitem.item().amount(), gitem.item().name(world));
								}
							}
						}
					}
				} else if (player.bank().freeSlots() > 0) {
					if (player.bank().add(gitem.item(), false).success()) {
						player.dupeWatch().exclude();
						player.world().removeGroundItem(gitem);
						if (sendmsg) {
							player.message("<col=FF0000>%d x %s was put in your bank before the server update.", gitem.item().amount(), gitem.item().name(world));
						}
					}
				}
			}
		});
	}
	
	private static void tryOrUsage(Player player, String usage, Runnable function) {
		try {
			function.run();
		} catch (Throwable t) {
			player.message(usage);
			if (player.world().server().isDevServer() || player.world().server().isTestServer())
				t.printStackTrace();
		}
	}
	
	static class Command {
		Privilege privilege;
		BiConsumer<Player, String[]> handler;
		String desc;
	}
	
}
