package nl.bartpelle.veteres.util;

/**
 * Created by Jonathan on 5/5/2017.
 */
public class MutableTuple<F, S> {
	
	private F first;
	private S second;
	
	public MutableTuple(F first, S second) {
		this.first = first;
		this.second = second;
	}
	
	public F first() {
		return first;
	}
	
	public F first(F first) {
		this.first = first;
		return this.first;
	}
	
	public S second() {
		return second;
	}
	
	public S second(S second) {
		this.second = second;
		return this.second;
	}
	
}
