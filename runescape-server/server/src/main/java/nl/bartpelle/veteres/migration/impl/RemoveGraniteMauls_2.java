package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;

/**
 * Created by Bart on 1/9/2016.
 */
public class RemoveGraniteMauls_2 implements Migration {
	
	public static final Item ALL_GMAULS = new Item(4153, Integer.MAX_VALUE);
	public static final Item ALL_GMAULS2 = new Item(4154, Integer.MAX_VALUE);
	
	@Override
	public int id() {
		return 2;
	}
	
	@Override
	public boolean apply(Player player) {
		// Reason: probably a price change
		player.bank().remove(ALL_GMAULS, true);
		player.inventory().remove(ALL_GMAULS, true);
		player.equipment().remove(ALL_GMAULS, true);
		player.bank().remove(ALL_GMAULS2, true);
		player.inventory().remove(ALL_GMAULS2, true);
		player.equipment().remove(ALL_GMAULS2, true);
		return true;
	}
	
}
