package nl.bartpelle.veteres.services.serializers.pg.part;

import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.script.Timer;
import nl.bartpelle.veteres.script.TimerKey;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Created by Bart on 4/23/2016.
 * <p>
 * Saves and loads timers. Pretty timers.
 */
public class TimersPart implements PgJsonPart {
	
	@Override
	public void decode(Player player, ResultSet rs, int characterId, Connection con) throws SQLException {
		PreparedStatement statement = con.prepareStatement("SELECT * FROM timers WHERE character_id = ?");
		statement.setInt(1, characterId);
		
		// Fetch timers one by one, scheduling them based on time passed on the triggers
		ResultSet timers = statement.executeQuery();
		while (timers.next()) {
			int key = timers.getInt("timer");
			int ticks = timers.getInt("ticks");
			long storedOn = timers.getLong("persisted_on");
			
			// Load timer based on stored key, or ignore if missing (backward compatibility).
			Optional<TimerKey> keyobj = TimerKey.forPersistencyKey(key);
			if (keyobj.isPresent()) {
				// If the timer key ticks offline, we need to compute the ticks elapsed and subtract them.
				// If they're negative, the logic will fire them multiple times in one tick to catch up.
				TimerKey timerKey = keyobj.get();
				if (timerKey.ticksOffline()) {
					long elapsed = System.currentTimeMillis() - storedOn;
					ticks -= (int) (elapsed / 600L);
				}
				
				// Store timer with the new ticks. Gets processed in player's logic processing.
				player.timers().register(timerKey, ticks);
			}
		}
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection con, boolean removeOnline) throws SQLException {
		// Prune timers from database..
		PreparedStatement remove = con.prepareStatement("DELETE FROM timers WHERE character_id = ?");
		remove.setInt(1, characterId);
		remove.executeUpdate();
		
		// Persist new timers!
		PreparedStatement insert = con.prepareStatement("INSERT INTO timers(character_id, timer, ticks, persisted_on) VALUES (?, ?, ?, ?)");
		for (Timer timer : player.timers().timers()) {
			if (timer != null && timer.key().persistent() && timer.ticks() > 0) {
				try {
					insert.setInt(1, characterId);
					insert.setInt(2, timer.key().persistencyKey());
					insert.setInt(3, timer.ticks());
					insert.setLong(4, System.currentTimeMillis());
					insert.addBatch();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		// Insert :)
		insert.executeBatch();
	}
	
}
