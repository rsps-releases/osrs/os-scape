package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.ObjectInteraction;
import nl.bartpelle.veteres.fs.ObjectDefinition;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.map.MapObj;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 11/18/2015.
 */
@PacketInfo(size = 15)
public class ItemOnObject implements Action {
	
	private int slot;
	private int hash;
	private int objid;
	private boolean run;
	private int z;
	private int itemid;
	private int x;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		itemid = buf.readULEShortA();
		run = buf.readByte() == 1;
		hash = buf.readIntV1();
		x = buf.readULEShort();
		z = buf.readUShort();
		objid = buf.readUShortA();
		slot = buf.readUShort();
		
		log(player, opcode, size, "slot=%d obj=%d inter=[%d:%d] item=%d run=%b x=%d z=%d", slot, objid, hash >> 16, hash & 0xffff, itemid, run, x, z);
	}
	
	@Override
	public void process(Player player) {
		MapObj obj = player.world().objById(objid, x, z, player.tile().level);
		
		if (obj == null) {
			player.debug("Cannot find object %d on [%d, %d].", objid, x, z);
			return;
		}
		
		obj = new MapObj(new Tile(x, z, player.tile().level), obj.id(), obj.type(), obj.rot()).cloneattribs(obj);
		
		if (player.attribOr(AttributeKey.DEBUG, false)) {
			ObjectDefinition objdef = player.world().definitions().get(ObjectDefinition.class, objid);
			player.message("Item on object %d at [%d, %d] varbit: %d, varp: %d (item %d)", objid, x, z, objdef.varbit, objdef.varp, itemid);
		}
		
		// Check item
		Item item = player.inventory().get(slot);
		if (item == null || item.id() != itemid) {
			return;
		}
		
		if (!player.locked() && !player.dead()) {
			player.stopActions(true);
			player.putattrib(AttributeKey.INTERACTION_OBJECT, obj);
			player.putattrib(AttributeKey.INTERACTION_OPTION, -1); // Special code
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			player.putattrib(AttributeKey.ITEM_ID, itemid);
			player.putattrib(AttributeKey.FROM_ITEM, item);
			player.world().server().scriptExecutor().executeLater(player, ObjectInteraction.script);
		}
	}
	
}
