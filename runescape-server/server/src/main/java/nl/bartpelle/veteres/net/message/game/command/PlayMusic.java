package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart Pelle on 8/23/2014.
 */
public class PlayMusic extends Command {
	
	private int track;
	
	public PlayMusic(int track) {
		this.track = track;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(3));
		buffer.packet(29);
		
		buffer.writeShortA(track);
		
		return buffer;
	}
}
