package nl.bartpelle.veteres.services.dupewatch;

import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Hans;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.net.message.game.command.AddReceivedPrivateMessage;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.NumberFormat;

/**
 * Created by Bart on 12/7/2015.
 */
public class DupeWatch {
	
	private static final Logger logger = LogManager.getLogger(DupeWatch.class);
	
	private static final int RAW_INC_THRESHOLD = 750_000; // 500k of alch value is a LOT.
	private static final int RAW_INC_CRITICAL = 5_000_000; // 5 million!? of alch value is a HUUUGE LOT.
	
	private long inv = -1;
	private long equip = -1;
	private long bank = -1;
	private long total = -1;
	private int ticksUntil = 5;
	private boolean exclude;
	
	public void update(Player player) {
		// Don't log admins as they can spawn
		if (player.privilege() == Privilege.ADMIN)
			return;
		
		if (ticksUntil-- > 0) return;
		
		World world = player.world();
		long newinv = totalPriceOf(world, player.inventory());
		long newbank = totalPriceOf(world, player.equipment());
		long newequip = totalPriceOf(world, player.bank());
		long newtotal = newinv + newbank + newequip;
		
		if (!exclude) {
			if (total != -1 && criticalIncrease(world, total, newtotal)) {
				reportCritical(player, newinv, newbank, newequip, newtotal);
			} else if (total != -1 && significantIncrease(world, total, newtotal)) {
				report(player, newinv, newbank, newequip, newtotal);
			}
		}
		
		total = newtotal;
		inv = newinv;
		equip = newequip;
		bank = newbank;
		
		exclude = false;
		ticksUntil = 5;
	}
	
	public void exclude() {
		exclude = true;
	}
	
	private void report(Player player, long inv, long bank, long equip, long total) {
		logger.warn("DupeWatch spotted significant increase of wealth for {}. {}/{}/{} ({}) => {}/{}/{} ({}), increase of {}/{}/{} ({}).", player.name(), this.inv, this.equip, this.bank, this.total, inv, equip, bank, total, signed(inv - this.inv), signed(equip - this.equip), signed(bank - this.bank), signed(total - this.total));
	}
	
	private void reportCritical(Player player, long inv, long bank, long equip, long total) {
		logger.warn("DupeWatch spotted CRITICAL increase of wealth for {}! {}/{}/{} ({}) => {}/{}/{} ({}), increase of {}/{}/{} ({}).", player.name(), this.inv, this.equip, this.bank, this.total, inv, equip, bank, total, signed(inv - this.inv), signed(equip - this.equip), signed(bank - this.bank), signed(total - this.total));
		
		// Notify moderators
		if (!player.world().realm().isDeadman() && !player.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
			player.world().players().forEach(p -> {
				if (p != null && p.privilege().eligibleTo(Privilege.MODERATOR)) {
					p.write(new AddReceivedPrivateMessage("DupeWatch", 2, 0, "WARNING: apparent user duping: " + player.name() + "; see chatbox!"));
					p.message("<col=ff0000>[DUPEWATCH] CRITICAL WARNING!");
					p.message("<col=ff0000>'%s' (age: %s) Wealth %s => %s (%s)!", player.name(), Hans.getTimeDHS(player), NumberFormat.getInstance().format(this.total), NumberFormat.getInstance().format(total), signed(total - this.total));
				}
			});
		}
		
		long oldinv = this.inv;
		long oldequip = this.equip;
		long oldbank = this.bank;
		
		// Insert into database
		player.world().server().service(PgSqlService.class, true).ifPresent(psql -> {
			psql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("INSERT INTO dupewatch_triggers(account_id, old_inventory, old_equipment, old_bank, new_inventory, new_equipment, new_bank, world, x, z, level) VALUES (?,?,?,?,?,?,?,?,?,?,?);");
					
					s.setInt(1, (Integer) player.id());
					s.setLong(2, oldinv);
					s.setLong(3, oldequip);
					s.setLong(4, oldbank);
					s.setLong(5, inv);
					s.setLong(6, equip);
					s.setLong(7, bank);
					s.setInt(8, player.world().id());
					s.setInt(9, player.tile().x);
					s.setInt(10, player.tile().z);
					s.setInt(11, player.tile().level);
					
					s.executeUpdate();
					connection.commit();
				}
			});
		});
	}
	
	private String signed(long num) {
		if (num < 0) {
			return NumberFormat.getInstance().format(num);
		} else {
			return "+" + NumberFormat.getInstance().format(num);
		}
	}
	
	private static long totalPriceOf(World world, ItemContainer container) {
		long l = 0;
		for (Item item : container.items()) {
			if (item != null) {
				if (world.realm().isPVP()) {
					l += (long) world.prices().getOrElse(item.unnote(world).id(), 0) * item.amount();
				} else {
					l += (long) item.realPrice(container.world()) * item.amount();
				}
			}
		}
		return l;
	}
	
	// TODO make this algorithm a bit more complex and more analytical
	private static boolean significantIncrease(World world, long old, long current) {
		return current - old > (world.realm().isPVP() ? (RAW_INC_THRESHOLD / 10) : RAW_INC_THRESHOLD);
	}
	
	// TODO make this algorithm a bit more complex and more analytical
	private static boolean criticalIncrease(World world, long old, long current) {
		return current - old > (world.realm().isPVP() ? (RAW_INC_CRITICAL / 10) : RAW_INC_CRITICAL);
	}
	
}
