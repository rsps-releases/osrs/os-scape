package nl.bartpelle.veteres.services.http;

import com.google.common.net.HttpHeaders;
import com.google.gson.Gson;
import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariPoolMXBean;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.util.HttpString;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.script.JSScripts;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.util.JGson;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.management.JMX;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bart on 5/28/2016.
 */
public class LiveData implements Service {
	
	private static final Logger logger = LogManager.getLogger(LiveData.class);
	
	private GameServer server;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
	}
	
	@Override
	public boolean start() {
		if (!server.serviceAlive(UndertowService.class)) {
			logger.error("Cannot start the world list server without Undertow being ready.");
			return false;
		}
		
		server.service(UndertowService.class, true).ifPresent(undertow -> {
			// Register item resolver
			undertow.pathHandler().remove("/players/{player}/items.json");
			undertow.pathHandler().add("/players/{player}/items.json", undertow.authHandler(exchange -> {
				String pparam = exchange.getQueryParameters().get("player").getFirst();
				int pid = Integer.parseInt(pparam);
				
				exchange.setStatusCode(404); // In case we don't find it
				
				server.world().playerForId(pid).ifPresent(player -> {
					ItemInfo info = new ItemInfo();
					info.name = player.name();
					info.rights = player.privilege().ordinal();
					info.tile = player.tile();
					info.value = totalValueOf(player);
					info.ip = player.ip();
					info.uuid = player.uuid();
					info.hwid = player.hwid();
					info.character = player.characterId();
					
					info.inventory = new ItemContainerInfo(player.inventory(), player.world());
					info.equipment = new ItemContainerInfo(player.equipment(), player.world());
					info.bank = new ItemContainerInfo(player.bank(), player.world());
					
					exchange.setStatusCode(200);
					exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "application/json");
					exchange.getResponseSender().send(JGson.get().toJson(info));
				});
			}));
			
			// Register player summary handler
			undertow.pathHandler().remove("/players/summary.json");
			undertow.pathHandler().add("/players/summary.json", undertow.authHandler(exchange -> {
				Map<Integer, PlayerInfo> infos = new HashMap<>();
				server.world().players().forEach(player -> {
					PlayerInfo info = new PlayerInfo();
					info.name = player.name();
					info.rights = player.privilege().ordinal();
					info.tile = player.tile();
					info.value = totalValueOf(player);
					info.voteTickets = totalVoteTickets(player);
					info.ip = player.ip();
					info.uuid = player.uuid();
					info.hwid = player.hwid();
					info.character = player.characterId();
					infos.put((Integer) player.id(), info);
				});
				
				exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "application/json");
				exchange.getResponseSender().send(JGson.get().toJson(infos));
			}));
			
			// HikariCP information endpoint
			undertow.pathHandler().remove("/system/database.json");
			undertow.pathHandler().add("/system/database.json", undertow.authHandler(exchange -> {
				exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "application/json");
				exchange.getResponseSender().send(JGson.get().toJson(new HikariInformation()));
			}));
			
			// Error logs
			undertow.pathHandler().remove("/system/error.log");
			undertow.pathHandler().add("/system/error.log", undertow.authHandler(exchange -> {
				exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "application/octet-stream");
				exchange.getResponseSender().send(ByteBuffer.wrap(Files.readAllBytes(Paths.get("error.log"))));
			}));
			
			// Error log size
			undertow.pathHandler().remove("/system/errorsize");
			undertow.pathHandler().add("/system/errorsize", undertow.authHandler(exchange -> {
				exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "text/plain");
				exchange.getResponseSender().send(String.valueOf(Files.size(Paths.get("error.log"))));
			}));
			
			// Dynamic JS
			undertow.pathHandler().remove("/system/js/execute");
			undertow.pathHandler().remove("/system/js/update");
			undertow.pathHandler().remove("/system/js/fetch");
			
			// Execute the given script post data
			undertow.pathHandler().add("/system/js/execute", undertow.authHandler(new BlockingHandler(exchange -> {
				exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "text/plain");
				byte[] data = IOUtils.toByteArray(exchange.getInputStream());
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(baos);
				
				try {
					ScriptEngineManager mgr = new ScriptEngineManager();
					ScriptEngine engine = mgr.getEngineByName("JavaScript");
					engine.put("server", server);
					engine.put("world", server.world());
					engine.put("undertow", undertow);
					engine.put("logger", logger);
					engine.put("out", ps);
					
					engine.eval("load(\"nashorn:mozilla_compat.js\");");
					engine.eval(new String(data));
				} catch (Throwable t) {
					t.printStackTrace(ps);
					t.printStackTrace();
				}
				
				exchange.getResponseSender().send(ByteBuffer.wrap(baos.toByteArray()));
			})));
			
			// Updates the main script file
			undertow.pathHandler().add("/system/js/update", undertow.authHandler(new BlockingHandler(exchange -> {
				exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "text/plain");
				byte[] data = IOUtils.toByteArray(exchange.getInputStream());
				Files.write(Paths.get("data/javascripts/script.js"), data);
				JSScripts.reload(null);
				
				exchange.getResponseSender().send("OK");
			})));
			
			// Grabs the main script file
			undertow.pathHandler().add("/system/js/fetch.js", undertow.authHandler(new BlockingHandler(exchange -> {
				exchange.getResponseHeaders().put(HttpString.tryFromString(HttpHeaders.CONTENT_TYPE), "application/javascript");
				exchange.getResponseSender().send(ByteBuffer.wrap(Files.readAllBytes(Paths.get("data/javascripts/script.js"))));
			})));
		});
		
		return true;
	}
	
	private static long totalValueOf(Player player) {
		World w = player.world();
		return totalValueOf(player.inventory(), w) + totalValueOf(player.equipment(), w) + totalValueOf(player.bank(), w);
	}
	
	private static int totalVoteTickets(Player player) {
		return player.inventory().count(10943) + player.bank().count(10943);
	}
	
	private static long totalValueOf(ItemContainer container, World world) {
		boolean pvp = world.realm().isPVP();
		long l = 0;
		for (Item i : container) {
			if (i != null) {
				if (pvp) {
					l += world.prices().getOrElse(i.id(), 0) * i.amount();
				} else {
					l += i.realPrice(world) * i.amount();
				}
			}
		}
		return l;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
	static class PlayerInfo {
		public String name;
		public long value;
		public int voteTickets;
		public Tile tile;
		public int rights;
		public int character;
		public String ip;
		public String uuid;
		public String hwid;
	}
	
	static class ItemInfo {
		public String name;
		public long value;
		public Tile tile;
		public int rights;
		public int character;
		public String ip;
		public String uuid;
		public String hwid;
		
		public ItemContainerInfo inventory;
		public ItemContainerInfo equipment;
		public ItemContainerInfo bank;
	}
	
	static class ItemContainerInfo {
		
		public long value;
		public int[] ids;
		public int[] amounts;
		
		public ItemContainerInfo(ItemContainer container, World world) {
			value = totalValueOf(container, world);
			
			ids = new int[container.size()];
			amounts = new int[container.size()];
			
			for (int i = 0; i < ids.length; i++) {
				Item item = container.get(i);
				
				if (item != null) {
					ids[i] = item.id();
					amounts[i] = item.amount();
				}
			}
		}
	}
	
	static class HikariInformation {
		
		public int activeConnections;
		public int idleConnections;
		public int totalConnections;
		public int awaitingThreads;
		
		HikariInformation() {
			try {
				MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
				ObjectName poolName = new ObjectName("com.zaxxer.hikari:type=Pool (hikari)");
				HikariPoolMXBean poolProxy = JMX.newMXBeanProxy(mBeanServer, poolName, HikariPoolMXBean.class);
				
				activeConnections = poolProxy.getActiveConnections();
				idleConnections = poolProxy.getIdleConnections();
				totalConnections = poolProxy.getTotalConnections();
				awaitingThreads = poolProxy.getThreadsAwaitingConnection();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
