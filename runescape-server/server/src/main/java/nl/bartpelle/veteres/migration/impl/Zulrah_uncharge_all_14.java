package nl.bartpelle.veteres.migration.impl;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jak on 15/04/2017.
 * <p>
 * With the release of venom, the following items which could previously not be charged will be converted to their uncharged forms.
 * They will now require charging with scales.
 */
public class Zulrah_uncharge_all_14 implements Migration {
	
	@Override
	public int id() {
		return 14;
	}
	
	// weapon id : equip slot : replacement id
	private static final int[][] EQUIP_REPLACEMENTS = new int[][]{
			{12926, 3, 12924}, // used blowpipe
			
			{11905, 3, 11908}, // trident, full
			{11907, 3, 11908}, // trident used
			{12899, 3, 12900}, // toxic trident used
			
			{12904, 3, 12902}, // toxic sotd used
			
			{12931, 0, 12929}, // serp charged
			{13197, 0, 13196}, // tanz helm charged
			{13199, 0, 13198}, // magma helm charged
	};
	
	@Override
	public boolean apply(Player player) {
		// Don't push this migration yet.
		/*boolean test = player.world().realm().isDeadman() && player.world().server().config().hasPathOrNull("deadman.jaktestserver");
	    if (!test) {
            return false;
        }*/
		// These messages are to give feedback on what was replaced.
		List<String> messages = new ArrayList<>();
		
		// World 2 had antivenoms in the store for 25bm a 4 dose potion.
		if (player.world().realm().isPVP() /*|| test*/) {
			long doses = 0L;
			doses += 4 * removeAll(player, new Item(12905, Integer.MAX_VALUE));
			doses += 4 * removeAll(player, new Item(12906, Integer.MAX_VALUE));
			doses += 3 * removeAll(player, new Item(12907, Integer.MAX_VALUE));
			doses += 3 * removeAll(player, new Item(12908, Integer.MAX_VALUE));
			doses += 2 * removeAll(player, new Item(12909, Integer.MAX_VALUE));
			doses += 2 * removeAll(player, new Item(12910, Integer.MAX_VALUE));
			doses += removeAll(player, new Item(12911, Integer.MAX_VALUE));
			doses += removeAll(player, new Item(12912, Integer.MAX_VALUE));
			long potions = doses / 4;
			int refund = (int) (18 * potions); // sell back to store price
			if (refund > 0)
				messages.add("<col=FF0000>[UPDATE]</col> You were refunded <col=7F00FF>" + refund + " blood money</col> to your bank for " + doses + " doses of anti-venom potion.");
		}
		// Convert equipment.. people get a little confused when it's randomly disappeared. None of these items stack in the Equipment container so this is safe.
		for (int[] info : EQUIP_REPLACEMENTS) {
			Item worn = player.equipment().get(info[1]);
			if (worn == null)
				continue;
			if (worn.id() == info[0]) {
				player.equipment().set(info[1], new Item(info[2], worn.amount()));
				messages.add("<col=FF0000>[UPDATE]</col> Your equipment: <col=7F00FF>" + worn.name(player.world()) + "</col> was changed to: <col=7F00FF>" + (new Item(info[2]).name(player.world())) + "</col>. ");
			}
		}
		
		// Convert inventory & bank... stackables.
		replaceIntoBank(player, 12931, 12929, messages); // normal serp
		replaceIntoBank(player, 13197, 13196, messages); // tanz
		replaceIntoBank(player, 13199, 13198, messages); // magma
		
		replaceIntoBank(player, 12904, 12902, messages); // toxic sotd
		
		replaceIntoBank(player, 11905, 11908, messages); // trident (full) - shouldn't be any ingame but oh well.
		replaceIntoBank(player, 11906, 11908, messages); // trident (full) (noted, tradeable) - shouldn't be any ingame but oh well.
		replaceIntoBank(player, 11907, 11908, messages); // trident (used) - these should be the only trident type in game.
		replaceIntoBank(player, 12899, 12900, messages); // toxic trident (used) - the only type of toxic trident
		
		replaceIntoBank(player, 12926, 12924, messages); // blowpipe - the only blowpipe type in the game.
		
		// Feedback on changes
		messages.forEach(m -> inform(player, m));
		if (messages.size() > 0) {
			inform(player, "<col=FF0000>There has been a game update that has changed some of your items.</col> See above! And check out <col=7F00FF>::updates</col>");
		}
		messages.clear();
		return true;
	}
	
	private void replaceIntoBank(Player player, int original, int replacement, List<String> messages) {
		Item old = new Item(original, Integer.MAX_VALUE);
		int replaced = (int) removeAll(player, old);
		if (replaced == 0)
			return;
		Item r = new Item(replacement, replaced);
		String oldn = old.name(player.world());
		
		// Correct name for feedback.
		if (old.definition(player.world()).noted())
			oldn = old.unnote(player.world()).name(player.world());
		
		if (player.bank().add(r, false).failed()) {
			player.world().spawnGroundItem(new GroundItem(player.world(), r, player.tile(), player.id()).lifetime(1000 * 60 * 30));
			if (messages != null)
				messages.add("<col=FF0000>[UPDATE]</col> Item: <col=7F00FF>" + replaced + " x " + oldn + "</col> was changed to: <col=7F00FF>" + (r.name(player.world())) + "</col>. Your bank is full, it was dropped under you.");
		} else {
			if (messages != null)
				messages.add("<col=FF0000>[UPDATE]</col> Item: <col=7F00FF>" + replaced + " x " + oldn + "</col> was changed to: <col=7F00FF>" + (r.name(player.world())) + "</col>. It was banked.");
		}
	}
	
	private long removeAll(Player player, int... ids) {
		long removed = 0L;
		for (int id : ids)
			removed += removeAll(player, new Item(id, Integer.MAX_VALUE));
		return removed;
	}
	
	public static void inform(Player player, String m) {
		//System.out.println("Informed "+player.name()+": "+m);
		if (player.index() > 0) {
			player.message(m);
		} else {
			// Not logged in.
			player.pendingActions().add(new Action() {
				@Override
				public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
				}
				
				@Override
				public void process(Player player) {
					player.message(m);
				}
			});
		}
	}
}
