package nl.bartpelle.veteres.util;

/**
 * @author Simplemind
 */
public class PlayerIcon {

    public static final int YOUTUBER_BRONZE = 58;
    public static final int YOUTUBER_SILVER = 57;
    public static final int YOUTUBER_GOLD = 56;
}
