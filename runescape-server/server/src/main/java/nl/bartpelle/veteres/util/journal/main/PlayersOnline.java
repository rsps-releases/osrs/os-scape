package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.Misc;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class PlayersOnline extends JournalEntry {

    public static final PlayersOnline INSTANCE = new PlayersOnline();

    @Override
    public void send(Player player) {
        send(player, "<img=12> Players Online",  Misc.playerCount(player.world()), Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int count = Misc.playerCount(player.world());
        player.message("There " + (count == 1 ? "is" : " are") + " currently " + count + " player" + (count == 1 ? "" : "s") + " online.");
    }

}