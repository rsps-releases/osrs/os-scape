package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Situations on 12/31/2015.
 */
public class SetWeight extends Command {
	
	private double weight = 0;
	
	public SetWeight(double weight) {
		this.weight = weight;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(3)).packet(53);
		
		buffer.writeShort((int) weight);
		
		return buffer;
	}
	
}