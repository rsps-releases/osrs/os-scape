package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.content.npcs.pets.Pet;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Jak on 26/07/2016.
 */
public class ReplaceInventoryPholderPets_12 implements Migration {
	
	private static final Logger logger = LogManager.getLogger(ReplaceInventoryPholderPets_12.class);
	
	@Override
	public int id() {
		return 12;
	}
	
	@Override
	public boolean apply(Player player) {
		Item[] items = player.inventory().items();
		for (int i = 0; i < player.inventory().size(); i++) {
			if (i < items.length && items[i] != null) {
				for (Pet pet : Pet.values()) {
					if (pet.getPhold() == items[i].id()) {
						player.inventory().remove(items[i], true, i);
						player.inventory().add(new Item(pet.getItem()), true, i);
						logger.info("Updated a broken pet pholder in " + player.name() + "'s inventory to correct ID: " + pet.toString());
					}
				}
			}
		}
		return true;
	}
}
