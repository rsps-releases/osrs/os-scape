package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;

public class PruneAntiVenomPlusW2 implements Migration {
    @Override
    public int id() {
        return 19;
    }

    @Override
    public boolean apply(Player player) {
        long removed = 0L;
        for (int i = 12913; i <= 12920; i++) {
            removed += removeAll(player, new Item(i, Integer.MAX_VALUE));
        }
        if (removed > 0)
            pendingMessage(player, String.format("<col=8F4808>%s anti-venom potions were pruned from your bank. They can now be bought from the Supplies shop.", removed));
        return true;
    }
}
