package nl.bartpelle.veteres.util.journal.presets;

import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.DonationTier;
import nl.bartpelle.veteres.model.Preset;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

import java.util.ArrayList;
import java.util.Comparator;

public class CustomPresets extends JournalEntry {

    private void sendEmpty(Player player) {
	    if (checkLocked(player, true)) {
		    return;
	    }

	    send(player, "Empty #" + id, Color.BRONZE);
    }

    private void sendCustom(Player player, Preset preset) {
    	if (checkLocked(player, true)) {
    		return;
	    }

        send(player, preset.name(), Color.BRONZE);
    }

    private boolean checkLocked(Player player, boolean write) {
	    if (id >= 17 && player.donationTier().compareTo(DonationTier.ULTIMATE_DONATOR) < 0) {
	    	if (write) {
			    send(player, "<img=49> Ultimate Donator Req.", Color.RED);
		    }

		    return true;
	    } else if (id >= 15 && player.donationTier().compareTo(DonationTier.GRAND_MASTER_DONATOR) < 0) {
            if (write) {
                send(player, "<img=48> Grandmaster Donator Req.", Color.RED);
            }

            return true;
        } else if (id >= 13 && player.donationTier().compareTo(DonationTier.MASTER_DONATOR) < 0) {
            if (write) {
                send(player, "<img=47> Master Donator Req.", Color.RED);
            }

            return true;
        } else if (id >= 11 && player.donationTier().compareTo(DonationTier.LEGENDARY_DONATOR) < 0) {
            if (write) {
                send(player, "<img=46> Legendary Donator Req.", Color.RED);
            }

            return true;
	    } else if (id >= 9 && player.donationTier().compareTo(DonationTier.EXTREME_DONATOR) < 0) {
            if (write) {
                send(player, "<img=45> Extreme Donator Req.", Color.RED);
            }

            return true;
        } else if (id >= 7 && player.donationTier().compareTo(DonationTier.SUPER_DONATOR) < 0) {
            if (write) {
                send(player, "<img=44> Super Donator Req.", Color.RED);
            }

            return true;
	    } else if (id >= 5 && player.donationTier().compareTo(DonationTier.DONATOR) < 0) {
	    	if (write) {
			    send(player, "<img=43> Donator Req.", Color.RED);
		    }

		    return true;
	    }

    	return false;
    }

    @Override
    public void send(Player player) {
        ArrayList<Preset> presets = new ArrayList<>(player.presetRepository().presets().values());
        if(id >= presets.size()) {
            sendEmpty(player);
            return;
        }

        // Sort based on ID to make newest show up last.
        presets.sort(Comparator.comparingInt(Preset::id));

        Preset preset = presets.get(id);
        if(preset == null) {
            sendEmpty(player);
        } else
            sendCustom(player, preset);
    }

    private int id;

    public CustomPresets(int id) {
        this.id = id;
    }

    @Override
    public void select(Player player) {
        ArrayList<Preset> presets = new ArrayList<>(player.presetRepository().presets().values());
        if(id >= presets.size()) {
	        player.world().server().scriptExecutor().executeLater(player, new QuestTab.PresetEmptyOptions(player));
            sendEmpty(player);
            return;
        }

	    // Sort based on ID to make newest show up last.
	    presets.sort(Comparator.comparingInt(Preset::id));

        if (checkLocked(player, false)) {
        	player.message("This preset is currently locked.");
        	return;
        }

        Preset preset = presets.get(id);
        if (preset != null)
            player.world().server().scriptExecutor().executeLater(player, new QuestTab.PresetOptions(player, preset));
        else
            player.message("Something went wrong.. please message an administrator for assistance!");
    }

}