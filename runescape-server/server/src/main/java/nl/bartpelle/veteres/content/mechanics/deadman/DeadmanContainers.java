package nl.bartpelle.veteres.content.mechanics.deadman;

import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by Jak on 04/11/2016.
 * <p>
 * Contains information about multiple Item Containers used in Deadman Mode for each player.
 */
public class DeadmanContainers {
	
	public Player player;
	
	public DeadmanContainers(Player player) {
		this.player = player;
		protectedItems = new BankKey(player.world());
		bankItemsKey = new BankKey(player.world());
		keys = new BankKey[5];
		for (int i = 0; i < keys.length; i++)
			keys[i] = new BankKey(player.world());
	}
	
	/**
	 * Our protected items container and bank key items container.
	 * Our protected items are never lost. Our bank key is stolen and the items removed from our real bank container
	 * when we die.
	 */
	public BankKey protectedItems, bankItemsKey;
	
	/**
	 * The keys we are carrying, maximum 5.
	 */
	public BankKey[] keys;
	
	// Updates the BankKey representing our bank should we die. Only call this once when you die.
	public DeadmanContainers updateBankKey() {
		bankItemsKey = generateBankKeyFromContainer(player.world(), player.bank());
		return this;
	}
	
	/**
	 * Generates an instance of BankKey from the given source.
	 * Will take the 10 most valuable items from the source container.
	 * NOTE: items are NOT removed from the source. That is gone later, if needed.
	 */
	public static BankKey generateBankKeyFromContainer(World world, ItemContainer source) {
		
		LinkedList<Item> items = new LinkedList<>();
		Collections.addAll(items, source.copy());
		
		// Keep untradables if NOT on the PVP world!
		if (!world.realm().isPVP())
			items.removeIf(item -> item == null || item.definition(world) == null || !item.rawtradable(world));
		else
			items.removeIf(item -> item == null || item.definition(world) == null);
		//player.debug("Possible losses: "+items.size());
		
		Collections.sort(items, (o1, o2) -> {
			o1 = o1.unnote(world);
			o2 = o2.unnote(world);
			ItemDefinition def1 = o1.definition(world);
			ItemDefinition def2 = o2.definition(world);
			// TODO Once GE is released, we will go off street prices.. long as price are not manipulated.
			// Best to use highest of street vs alch price.
			int v1 = def1 == null ? 0 : world.realm().isPVP() ? world.prices().getOrElse(o1.id(), 0) : o1.realPrice(world);
			int v2 = def2 == null ? 0 : world.realm().isPVP() ? world.prices().getOrElse(o2.id(), 0) : o2.realPrice(world);
			return Integer.compare(v2, v1);
		});
		
		ItemContainer lostitems = new ItemContainer(world, 10, ItemContainer.Type.FULL_STACKING);
		int left = 10;
		while (left-- > 0 && !items.isEmpty()) {
			Item lost = items.peek();
			if (lost == null) {
				left++;
				items.poll();
				continue;
			}
			lostitems.add(items.poll(), true);
		}
		long value = 0L;
		for (Item lost : lostitems.items()) {
			if (lost == null) continue;
			value += lost.realPrice(world);
		}
		//System.out.printf("BankKey generated. Contents count:%d, value:%d %n", (lostitems.size() - lostitems.freeSlots()), value);
		return new BankKey(world, lostitems, value);
	}
	
	public static BankKey[] filteredKeys(BankKey[] keys) {
		LinkedList<BankKey> keylist = new LinkedList<>();
		for (BankKey key : keys)
			if (key != null)
				Collections.addAll(keylist, key);
		return filteredKeys(keylist);
	}
	
	public static BankKey[] filteredKeys(ArrayList<BankKey> keys) {
		LinkedList<BankKey> keylist = new LinkedList<>();
		for (BankKey key : keys)
			if (key != null)
				Collections.addAll(keylist, key);
		return filteredKeys(keylist);
	}
	
	public static BankKey[] filteredKeys(LinkedList<BankKey> keylist) {
		Collections.sort(keylist, (o1, o2) -> {
			return Long.compare(o2.value, o1.value);
		});
		return keylist.toArray(new BankKey[keylist.size()]);
	}
	
	
}
