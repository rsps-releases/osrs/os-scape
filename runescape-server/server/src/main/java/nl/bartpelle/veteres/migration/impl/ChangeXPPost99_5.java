package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;

/**
 * Created by Bart on 4/3/2016.
 */
public class ChangeXPPost99_5 implements Migration {
	
	private static final int[] AFFECTED_SKILLS = {Skills.ATTACK, Skills.STRENGTH, Skills.DEFENCE, Skills.RANGED,
			Skills.MAGIC, Skills.HITPOINTS};
	private static final double XP_TO_99 = 13034431;
	
	@Override
	public int id() {
		return 5;
	}
	
	@Override
	public boolean apply(Player player) {
		for (int skill : AFFECTED_SKILLS) {
			if (player.skills().xp()[skill] > XP_TO_99) {
				double delta = player.skills().xp()[skill] - XP_TO_99;
				double addition = delta / 50.0;
				player.skills().xp()[skill] = XP_TO_99 + addition;
			}
		}
		
		return true;
	}
	
}
