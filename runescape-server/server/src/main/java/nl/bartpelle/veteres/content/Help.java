package nl.bartpelle.veteres.content;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import nl.bartpelle.veteres.util.JGson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Help {

    public static List<String> SCROLL_OPTIONS = new ArrayList<>();
    public static Map<String, String[]> SCROLL_MAP = new LinkedHashMap<>();

    public static void loadHelp() {
        Gson gson = JGson.get();
        try {
            List<Entry> entries = gson.fromJson(new FileReader("data/help.json"), new TypeToken<List<Entry>>() {
            }.getType());

            SCROLL_MAP.clear();
            SCROLL_OPTIONS.clear();

            for(int i = 0; i < entries.size(); i++) {
                Entry entry = entries.get(i);
                SCROLL_MAP.put(entry.title, entry.message);
                SCROLL_OPTIONS.add("<col=735a28>" + (i + 1) + ".</col> " + entry.title);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static final class Entry {
        @Expose public String key;
        @Expose public String title;
        @Expose public String[] message;
    }

}
