package nl.bartpelle.veteres.util;

/**
 * Created by Bart on 11/22/2015.
 */
public enum AttackMode {
	
	ATTACK, STRENGTH, DEFENCE, RANGED, MAGIC, SHARED
	
}
