package nl.bartpelle.veteres.services.serializers.pg.part;

import com.google.gson.*;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.JGson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Bart on 8/25/2015.
 */
public class VarpsPart implements PgJsonPart {
	
	private JsonParser parser = new JsonParser();
	private Gson gson = JGson.get();
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		JsonArray varparray = parser.parse(resultSet.getString("varps")).getAsJsonArray();
		for (JsonElement varp : varparray) {
			JsonObject item = varp.getAsJsonObject();
			player.varps().raw()[item.get("id").getAsInt()] = item.get("val").getAsInt();
		}
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection connection, boolean removeOnline) throws SQLException {
		if (removeOnline)
			player.varps().presave();
		
		JsonArray varparray = new JsonArray();
		
		int[] v = player.varps().raw();
		for (int i = 0; i < 2000; i++) {
			if (v[i] != 0) {
				JsonObject obj = new JsonObject();
				obj.add("id", new JsonPrimitive(i));
				obj.add("val", new JsonPrimitive(v[i]));
				varparray.add(obj);
			}
		}
		
		characterUpdateStatement.setString(5, gson.toJson(varparray));
	}
	
}
