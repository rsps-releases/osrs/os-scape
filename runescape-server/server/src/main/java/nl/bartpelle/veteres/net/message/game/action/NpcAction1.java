package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.NpcInteraction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

import java.lang.ref.WeakReference;

/**
 * Created by Bart on 8/12/2015.
 */
@PacketInfo(size = 3)
public class NpcAction1 implements Action {
	
	private boolean run;
	private int index;
	
	private int opcode;
	private int size;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		index = buf.readUShortA();
		run = buf.readByteN() == 1;
		
		this.opcode = opcode;
		this.size = size;
	}
	
	@Override
	public void process(Player player) {
		Npc other = player.world().npcs().get(index);
		if (other != null) {
			log(player, opcode, size, "id=%d index=%d", other.id(), index);
			
			if (!player.locked() && !player.dead()) {
				player.stopActions(true);
				player.face(other);
				
				if (!other.dead()) {
					player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(other));
					player.putattrib(AttributeKey.INTERACTION_OPTION, 1);
					player.world().server().scriptExecutor().executeLater(player, NpcInteraction.script);
				}
			}
		}
	}
	
}
