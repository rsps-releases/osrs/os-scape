package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.content.npcs.pets.Pet;
import nl.bartpelle.veteres.content.npcs.pets.PetAI;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;

/**
 * Created by Jak on 26/07/2016.
 */
public class RemoveFreePets_13 implements Migration {
	
	@Override
	public int id() {
		return 13;
	}
	
	@Override
	public boolean apply(Player player) {
		// Reason: you could obtain via a bug in Probita shop without actually having it unlocked via content.
		if (!PetAI.hasUnlocked(player, Pet.PHOENIX)) {
			removeAll(player, new Item(Pet.PHOENIX.getItem(), Integer.MAX_VALUE));
		}
		if (!PetAI.hasUnlocked(player, Pet.ROCKY)) {
			removeAll(player, new Item(Pet.ROCKY.getItem(), Integer.MAX_VALUE));
		}
		return true;
	}
}
