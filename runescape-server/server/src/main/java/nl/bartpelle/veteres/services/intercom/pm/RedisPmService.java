package nl.bartpelle.veteres.services.intercom.pm;

import com.google.gson.Gson;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.AddReceivedPrivateMessage;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.redis.RedisService;
import nl.bartpelle.veteres.util.JGson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import redis.clients.jedis.JedisPubSub;

import java.util.HashSet;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by Bart on 12-3-2015.
 * <p>
 * Provides inter-world private messaging support.
 */
public class RedisPmService extends JedisPubSub implements PmService {
	
	private static final Logger logger = LogManager.getLogger();
	
	private static final String CHANNEL_LOGIN = "userlogin";
	private static final String CHANNEL_LOGOUT = "userlogout";
	private static final String CHANNEL_PM = "pm";
	
	private GameServer server;
	private RedisService redisService;
	private Gson gson;
	private SqlFriendServer sqlServer;
	private ForkJoinPool fjp = new ForkJoinPool();
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.gson = JGson.get();
		this.server = server;
		this.sqlServer = new SqlFriendServer(this);
	}
	
	public GameServer server() {
		return server;
	}
	
	@Override
	public void addFriend(Player player, String name) {
		sqlServer.addFriend(player, name);
	}
	
	@Override
	public void removeFriend(Player player, int id) {
		sqlServer.removeFriend(player, id);
	}
	
	@Override
	public void removeAllFriends(Player player) {
		sqlServer.removeAllFriends(player);
	}
	
	@Override
	public void addIgnore(Player player, String name) {
		sqlServer.addIgnore(player, name);
	}
	
	@Override
	public void removeIgnore(Player player, int id) {
		sqlServer.removeIgnore(player, id);
	}
	
	@Override
	public void onMessage(String channel, String message) {
		try {
			switch (channel) {
				case CHANNEL_PM: {
					Optional<PrivateMessage> pm = safeFromJson(message, PrivateMessage.class);
					if (pm.isPresent()) {
						logger.debug("Private message event: {}.", message);
						
						// Try to find that user in our server
						PrivateMessage pmobj = pm.get();
						server.world().playerForId(pmobj.to).ifPresent(player -> {
							player.write(new AddReceivedPrivateMessage(pmobj.from, pmobj.icon, pmobj.world, pmobj.message));
						});
					}
					break;
				}
				case CHANNEL_LOGIN: {
					Optional<StatusMessage> status = safeFromJson(message, StatusMessage.class);
					if (status.isPresent()) {
						logger.debug("Login message event: {}.", message);
						
						// Try to find that user in our server
						StatusMessage pmobj = status.get();
						
						server.world().players().forEach(p -> {
							int world = pmobj.world;
							if (pmobj.friends != null &&
									(pmobj.friends.isEmpty() ||
											//dont bother doing contains since he has 0 friends like coco, dragonkk, and tyluur
											//and we cherish cpu usage like african's cherish clean water
											!pmobj.friends.contains((Integer) p.id()))) {
								world = 0;
							}
							p.social().updateFriend(pmobj.id, world);
						});
					}
					break;
				}
				case CHANNEL_LOGOUT: {
					Optional<StatusMessage> status = safeFromJson(message, StatusMessage.class);
					if (status.isPresent()) {
						logger.debug("Logout message event: {}.", message);
						
						// Try to find that user in our server
						StatusMessage pmobj = status.get();
						
						server.world().players().forEach(p -> {
							p.social().updateFriend(pmobj.id, 0);
						});
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onUserOnline(Player player) {
		sqlServer.updateSocialFor(player, () -> {
			if (VarbitAttributes.varbit(player, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) == 2)
				return; // Private is OFF.
			
			boolean friendsOnly = VarbitAttributes.varbit(player, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) == 1;
			
			fjp.execute(() -> {
				StatusMessage sm = new StatusMessage(player.name(), (Integer) player.id(), player.world().id(), friendsOnly ? player.social().buildFriendIds() : null);
				redisService.doOnPool(j -> j.publish(CHANNEL_LOGIN, gson.toJson(sm)));
			});
		});
	}
	
	@Override
	public void onUserOffline(Player player) {
		sqlServer.updateSocialFor(player, () -> {
			fjp.execute(() -> {
				redisService.doOnPool(j -> j.publish(CHANNEL_LOGOUT, gson.toJson(new StatusMessage(player.name(), (Integer) player.id(), 0))));
			});
		});
	}
	
	@Override
	public void privateMessageDispatched(Player from, int target, String message) {
		int icon = from.calculateBaseIcon();
		
		fjp.execute(() -> {
			redisService.doOnPool(j -> j.publish(CHANNEL_PM, gson.toJson(new PrivateMessage(from.name(), (int) from.id(), target, from.world().id(), message, icon))));
		});
	}
	
	private <T> Optional<T> safeFromJson(String json, Class<T> typeOf) {
		try {
			return Optional.of(gson.fromJson(json, typeOf));
		} catch (Throwable t) {
			return Optional.empty();
		}
	}
	
	@Override
	public boolean start() {
		Optional<RedisService> service = server.service(RedisService.class, false);
		if (!service.isPresent()) {
			logger.error("Cannot use Redis PM service if the Redis service is disabled");
			return false;
		}
		
		redisService = service.get();
		
		if (redisService.isAlive()) {
			ExecutorService executor = Executors.newFixedThreadPool(3);
			executor.submit(() -> redisService.doOnPool(j -> j.subscribe(this, CHANNEL_LOGOUT)));
			executor.submit(() -> redisService.doOnPool(j -> j.subscribe(this, CHANNEL_PM)));
			executor.submit(() -> redisService.doOnPool(j -> j.subscribe(this, CHANNEL_LOGIN)));
			
			logger.info("Redis PM provider ready to dispatch and receive messages.");
		} else {
			logger.error("Cannot use Redis PM service if the Redis service failed to start");
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
	// Gson classes
	private static class PrivateMessage {
		public String from;
		public int fromId;
		public int icon;
		public int to;
		public int world;
		public String message;
		
		public PrivateMessage(String from, int fromId, int to, int world, String message, int icon) {
			this.from = from;
			this.fromId = fromId;
			this.to = to;
			this.world = world;
			this.message = message;
			this.icon = icon;
		}
	}
	
	private static class StatusMessage {
		public String name;
		public int id;
		public int world;
		public HashSet<Integer> friends; //cuz we gonna be doing .contains a lot
		
		public StatusMessage(String name, int id, int world) {
			this(name, id, world, null);
		}
		
		public StatusMessage(String name, int id, int world, HashSet<Integer> friends) {
			this.name = name;
			this.id = id;
			this.world = world;
			this.friends = friends;
		}
	}
	
}
