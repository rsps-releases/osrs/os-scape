package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.content.Teleports;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

public class SendTeleports extends Command {

	private String title;
	private int selectedCategoryIndex;
	private Teleports.Category[] categories;
	private int selectedSubcategoryIndex;
	private Teleports.Subcategory[] subcategories;
	private Teleports.Teleport[] teleports;


	public SendTeleports(String title,
						 int selectedCategoryIndex, Teleports.Category[] categories, int selectedSubcategoryIndex,
						 Teleports.Subcategory[] subcategories, Teleports.Teleport[] teleports) {
		this.title = title;
		this.selectedCategoryIndex = selectedCategoryIndex;
		this.categories = categories;
		this.selectedSubcategoryIndex = selectedSubcategoryIndex;
		this.subcategories = subcategories;
		this.teleports = teleports;
	}
	
	@Override
	protected RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(255)).packet(90).writeSize(RSBuffer.SizeType.SHORT); //6 for entry

		if(title != null)
			buffer.writeString(title);
		else
			buffer.writeByte(0);

		buffer.writeByte(selectedCategoryIndex);
		if(categories != null) {
			buffer.writeByte(categories.length);
			for(Teleports.Category c : categories)
				buffer.writeString(c.name);
		} else {
			buffer.writeByte(0);
		}

		buffer.writeByte(selectedSubcategoryIndex);
		if(subcategories != null) {
			buffer.writeByte(subcategories.length);
			for(Teleports.Subcategory c : subcategories)
				buffer.writeString(c.name);
		} else {
			buffer.writeByte(0);
		}

		if(teleports != null) {
			for(Teleports.Teleport t : teleports)
				buffer.writeString(t.name);
		}
		
		return buffer;
	}
}
