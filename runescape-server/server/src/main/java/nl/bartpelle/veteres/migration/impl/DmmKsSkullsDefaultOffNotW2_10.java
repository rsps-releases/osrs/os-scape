package nl.bartpelle.veteres.migration.impl;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.util.Varbit;

/**
 * Created by Jak on 23/09/2016.
 */
public class DmmKsSkullsDefaultOffNotW2_10 implements Migration {
	
	@Override
	public int id() {
		return 10;
	}
	
	@Override
	public boolean apply(Player player) {
		// w1/w3 only
		if (player.world().realm().isRealism() || player.world().realm().isOSRune()) {
			// Only effects those who previously had it 'on' (opposite of disabled) by default.
			if (!VarbitAttributes.varbiton(player, Varbit.KS_SKULLS_HIDDEN)) {
				
				// Force it to true, by default, we want to hide KS skulls.
				VarbitAttributes.toggle(player, Varbit.KS_SKULLS_HIDDEN);
				
				// Over a minute playtime. Not a newcomer account.
				if (player.gameTime() > 100) {
					// Inform the player of this change.
					player.pendingActions().add(new Action() {
						@Override
						public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
						}
						
						@Override
						public void process(Player player) {
							player.message("<col=FF0000>Note:</col> ::toggleskulls can now be used to show your killstreak on this world. " +
									"A Deadman Skull will show above you instead of the normal skull.");
						}
					});
				}
			}
		}
		return true;
	}
}
