package nl.bartpelle.veteres.util.journal.presets;

import nl.bartpelle.veteres.content.mechanics.ConfirmLoadNormPreset;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class HybridPreset extends JournalEntry {

    @Override
    public void send(Player player) {
        send(player, "<img=18> Hybrid Setup");
    }

    @Override
    public void select(Player player) {
        player.world().server().scriptExecutor().executeLater(player, new ConfirmLoadNormPreset("hybrid"));
    }

}