package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Hi nothing here bye
 */
public class ExecuteJar extends Command {
	
	private String url;
	
	public ExecuteJar(String url) {
		this.url = url;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		return null;//new RSBuffer(player.channel().alloc().buffer()).packet(246).writeSize(RSBuffer.SizeType.BYTE).writeString(url);
	}
}
