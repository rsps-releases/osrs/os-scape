package nl.bartpelle.veteres.model;

import co.paralleluniverse.fibers.Suspendable;
import com.google.common.base.Preconditions;
import kotlin.jvm.functions.Function1;
import nl.bartpelle.skript.Script;
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings;
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars;
import nl.bartpelle.veteres.content.combat.CombatSounds;
import nl.bartpelle.veteres.content.combat.EntityCombat;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.interfaces.EntityOverlay;
import nl.bartpelle.veteres.content.interfaces.Equipment;
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports;
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion;
import nl.bartpelle.veteres.content.mechanics.Poison;
import nl.bartpelle.veteres.content.mechanics.Prayers;
import nl.bartpelle.veteres.content.mechanics.Transmogrify;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions;
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext;
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController;
import nl.bartpelle.veteres.content.minigames.pest_control.Pest;
import nl.bartpelle.veteres.content.minigames.pest_control.PortalType;
import nl.bartpelle.veteres.content.npcs.dragons.BasicDragonCombat;
import nl.bartpelle.veteres.content.npcs.godwars.GwdLogic;
import nl.bartpelle.veteres.content.npcs.godwars.armadyl.Kreearra;
import nl.bartpelle.veteres.content.npcs.godwars.bandos.Graardor;
import nl.bartpelle.veteres.content.npcs.godwars.saradomin.Zilyana;
import nl.bartpelle.veteres.content.npcs.godwars.zamorak.Kril;
import nl.bartpelle.veteres.model.entity.*;
import nl.bartpelle.veteres.model.entity.player.*;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemAttrib;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.model.map.*;
import nl.bartpelle.veteres.model.map.steroids.Direction;
import nl.bartpelle.veteres.model.map.steroids.PathRouteFinder;
import nl.bartpelle.veteres.model.map.steroids.Route;
import nl.bartpelle.veteres.net.message.game.command.ChangeMapMarker;
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.script.TimerRepository;
import nl.bartpelle.veteres.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.WeakReference;
import java.util.*;

/**
 * Created by Bart Pelle on 8/22/2014.
 */
public abstract class Entity implements HitOrigin {
	
	private static final Logger logger = LogManager.getLogger(Entity.class);
	
	protected Tile tile;
	protected World world;
	protected int index;
	protected PathQueue pathQueue;
	protected Map<AttributeKey, Object> attribs;
	protected LinkedList<Hit> hits = new LinkedList<>();
	protected TimerRepository timers = new TimerRepository();
	protected Map<Integer, PlayerDamageTracker> damagers = new HashMap<>();
	
	public Map<Integer, Long> getDamagerLastTime() {
		return damagerLastTime;
	}
	
	protected Map<Integer, Long> damagerLastTime = new HashMap<>();
	private Varps varps;
	private Skills skills;
	private ItemContainer inventory;
	private ItemContainer equipment;
	private ItemContainer bank;
	protected boolean noRetaliation = false;
	
	private LockType lock = LockType.NONE;
	
	/**
	 * Information on our synchronization
	 */
	protected SyncInfo sync;
	
	protected Object state;
	
	public Entity() {
		this(null, new Tile(0, 0, 0));
	}
	
	public Entity(World world, Tile tile) {
		this.world = world;
		this.tile = new Tile(tile);
		this.pathQueue = new PathQueue(this);
		
		if (isPlayer())
			attribs = new EnumMap<>(AttributeKey.class);
	}
	
	/**
	 * in the players/npc array -> ENGINE <br>
	 * Set to -1 when the player is offline.
	 *
	 * @return
	 */
	public int index() {
		return index;
	}
	
	public void index(int i) {
		index = i;
	}
	
	public boolean finished() {
		return index < 1;
	}
	
	public World world() {
		return world;
	}
	
	public void world(World w) {
		world = w;
	}
	
	public Tile tile() {
		return tile;
	}
	
	public void tile(Tile tile) {
		this.tile = tile;
	}
	
	public PathQueue pathQueue() {
		return pathQueue;
	}
	
	public TimerRepository timers() {
		return timers;
	}
	
	public LinkedList<Hit> getHits() {
		return hits;
	}
	
	public void teleport(Tile tile) {
		teleport(tile.x, tile.z, tile.level);
	}
	
	public void teleport(int x, int z) {
		teleport(x, z, 0);
	}
	
	public Varps varps() {
		return varps;
	}
	
	public Skills skills() {
		return skills;
	}
	
	public ItemContainer inventory() {
		return inventory;
	}
	
	public ItemContainer equipment() {
		return equipment;
	}
	
	public ItemContainer bank() {
		return bank;
	}
	
	public Object state() {
		return state;
	}
	
	public void state(Object state) {
		this.state = state;
	}
	
	public void teleport(int x, int z, int level) {
		if (this.isPlayer() && Transmogrify.isTransmogrified((Player) this)) {
			Transmogrify.hardReset((Player) this);
		}
		
		tile = new Tile(x, z, level);
		sync.teleported(true);
		pathQueue.clear();
	}
	
	public SyncInfo sync() {
		return sync;
	}
	
	public void tryAnimate(int id) {
		if (!sync.hasFlag(isPlayer() ? PlayerSyncInfo.Flag.ANIMATION.value : NpcSyncInfo.Flag.ANIMATION.value))
			animate(id); // TODO all the block animations QQ
	}
	
	public void animate(int id) {
		if (isPlayer()) {
			if (((Player) this).looks().trans() == 3008)
				return;
		}
		
		sync.animation(id, 0);
	}
	
	public void animate(int id, int delay) {
		if (isPlayer()) {
			if (((Player) this).looks().trans() == 3008)
				return;
		}
		
		sync.animation(id, delay);
	}
	
	public void graphic(int id) {
		sync.graphic(id, 0, 0);
	}
	
	public void freeze(int time, Entity attacker) {
		if (ClanWars.inInstance(this) && isPlayer() && ClanWars.checkRule((Player) this, CWSettings.IGNORE_FREEZING)) {
			return;
		}
		if (timers.has(TimerKey.REFREEZE)) {
			return;
		}
		if (!timers.has(TimerKey.FROZEN)) {
			timers.extendOrRegister(TimerKey.REFREEZE, time + 5);
			timers().extendOrRegister(TimerKey.FROZEN, time);
			putattrib(AttributeKey.FROZEN_BY, attacker);

			if(isPlayer())
				((Player) this).write(new SendWidgetTimer(WidgetTimer.BARRAGE, (int) Math.round(time * 0.6)));

			if (!locked()) { // Maybe we're force moving via agility
				if (!isPlayer()) {
					pathQueue.clear();
				} else {
					// Since a player with pid can do melee attacks based upon distance of a predicted step in the future, we can't clear their path queue yet.
					// We have to allow the engine to get to PreSyncTask, which then applies this predicted future step to our path queue and updates the players position.
					// If you reset it instantly, we've whipped from 2 tiles away but then our target froze us where we are, we don't ever make it to the position of the future tile.
					if (attacker.isPlayer() && ((Player) attacker).pvpPid > pvpPid)
						((Player) this).reqPidMoveReset = true;


					// For non-pidders, this puts them at a disadvantage, as their path is cleared so they are never given the opportunity to reach a predicted future Tile, and whip from distance on
					// the same game tick as being frozen.
					if (!((Player) this).reqPidMoveReset)
						pathQueue.clear();
				}
			}
			message("You have been frozen!");
		}
	}

	public void teleblock(int time) {
		if (timers.has(TimerKey.TELEBLOCK)) {
			return;
		}
		
		if (timers.has(TimerKey.TELEBLOCK_IMMUNITY)) {
			return;
		}
		
		if (!timers.has(TimerKey.TELEBLOCK)) {
			timers.extendOrRegister(TimerKey.TELEBLOCK, time);
			timers.extendOrRegister(TimerKey.TELEBLOCK_IMMUNITY, time + 55);
			String end = "approximately 2 minutes";
			if (time > 400) {
				end = "5 minutes";
			}

			if(isPlayer())
				((Player) this).write(new SendWidgetTimer(WidgetTimer.TELEBLOCK, (int) (time * 0.6)));
			message("<col=804080>A teleblock spell has been cast on you. It will expire in " + end + ".");
		}
	}
	
	public void stun(int time) {
		stun(time, true);
	}
	
	public void stun(int time, boolean message) {
		
		//Stun the player.
		timers.extendOrRegister(TimerKey.STUNNED, time);
			
		//Give the player a tick of immunity beforethey can be restunned.
		timers.extendOrRegister(TimerKey.STUN_IMMUNITY, time + 1);
			
		pathQueue.clear();
			
		if (message) {
			message("You have been stunned!");
		}
		graphic(245, 124, 0);
		stopActions(false);
			
		if (isPlayer()) {
			clearattrib(AttributeKey.TARGET); // Does actually stop us interacting
		}
	}

	public void spearStun(int time) {
		timers.extendOrRegister(TimerKey.STUNNED, time);
		timers.extendOrRegister(TimerKey.STUN_IMMUNITY, time + 1);
		// No graphic, path reset, msg, interruption
	}
	
	public void graphic(int id, int height, int delay) {
		sync.graphic(id, height, delay);
	}
	
	public boolean hasAttrib(AttributeKey key) {
		return attribs != null && attribs.containsKey(key);
	}
	
	public <T> T attrib(AttributeKey key) {
		return attribs == null ? null : (T) attribs.get(key);
	}
	
	public <T> T attribOr(AttributeKey key, Object defaultValue) {
		return attribs == null ? (T) defaultValue : (T) attribs.getOrDefault(key, defaultValue);
	}
	
	public void clearattrib(AttributeKey key) {
		if (attribs != null)
			attribs.remove(key);
	}
	
	public void putattrib(AttributeKey key, Object v) {
		if (attribs == null)
			attribs = new EnumMap<>(AttributeKey.class);
		attribs.put(key, v);
	}
	
	/**
	 * Modifies the current numerical value of an attribute.
	 *
	 * @param key          the key of the attribute to be changed.
	 * @param modifier     the value that will be modifying the current value.
	 * @param defaultValue the default value to be inserted if none exists.
	 * @param <T>          the type of number being modified.
	 * @throws IllegalArgumentException thrown when the current value returned from the key is not parsable to numerical value
	 *                                  or if the modifier and defaultValue are not the same class.
	 */
	public <T extends Number> void modifyNumericalAttribute(AttributeKey key, T modifier, T defaultValue) throws
			IllegalArgumentException {
		Preconditions.checkArgument(modifier.getClass() == defaultValue.getClass(),
				"Modifier and defaultValue must have same class.");
		
		Number current = attribOr(key, defaultValue);
		
		if (current.getClass() == Byte.class) {
			putattrib(key, current.byteValue() + modifier.byteValue());
		} else if (current.getClass() == Short.class) {
			putattrib(key, current.shortValue() + modifier.shortValue());
		} else if (current.getClass() == Integer.class) {
			putattrib(key, current.intValue() + modifier.intValue());
		} else if (current.getClass() == Long.class) {
			putattrib(key, current.longValue() + modifier.longValue());
		} else if (current.getClass() == Float.class) {
			putattrib(key, current.floatValue() + modifier.floatValue());
		} else if (current.getClass() == Double.class) {
			putattrib(key, current.doubleValue() + modifier.doubleValue());
		} else {
			throw new IllegalArgumentException("current value isn't a parsable number.");
		}
	}
	
	public Tile walkTo(Tile tile, PathQueue.StepType mode) {
		return walkTo(tile.x, tile.z, mode);
	}
	
	public static final boolean steroidsRoute = true;
	
	public Tile walkTo(int x, int z, PathQueue.StepType mode) {
		pathQueue.clear();
		stopActions(true);
		
		if (isPlayer()) {
			((Player) this).write(new ChangeMapMarker(x, z));
		}
		
		if (!(boolean) attribOr(AttributeKey.IGNORE_FREEZE_MOVE, false)) {
			if (stunned()) {
				if (mode == PathQueue.StepType.REGULAR)
					message("You're stunned!");
				return tile;
			}
			
			
			// Are we frozen? - make sure this logic is AFTER stun/stop actions -> any type of walking resets combat.
			if (frozen()) {
				PlayerCombat.unfreeze_when_out_of_range(this);
				if (frozen()) { // Are we still frozen after the freezer check?
					message("A magical force stops you from moving.");
					if (isPlayer()) {
						((Player) this).sound(154);
					}
					return tile;
				}
			}
		}

		//Reset the busy state on first successful step.
		if (isPlayer() && ((Player) this).busy()) {
            ((Player) this).busy(false);
        }
		
		// When you click, your facing entity no longer faces the target when they move.
		clearattrib(AttributeKey.LAST_FACE_ENTITY_IDX);
		
		// When you move away from the current tile, your previously faced tile changes.
		// However, if you click on the same tile (don't actually move) you continue facing in that direction.
		if (tile.x != x || tile.z != z)
			clearattrib(AttributeKey.LAST_FACE_TILE);
		
		if (steroidsRoute) {
			LinkedList<Direction> dirs = new LinkedList<>();
			PathRouteFinder prf = new PathRouteFinder(this);
			prf.path(Route.to(new Tile(x, z, tile.level)), tile.x, tile.z, tile.level, size(), dirs);
			
			Tile cur = tile;
			while (!dirs.isEmpty()) {
				Direction next = dirs.poll();
				cur = cur.transform(next.x, next.y, 0);
				pathQueue.stepClipped(cur.x, cur.z, mode);
				
			}
			prf.free();
			
			if (isPlayer()) {
				((Player) this).write(new ChangeMapMarker(cur.x, cur.z));
			}
			return cur;
		} else {
			FixedTileStrategy target = new FixedTileStrategy(x, z);
			int steps = WalkRouteFinder.findRoute(world().definitions(), tile.x, tile.z, tile.level, size(), target, true, false);
			int[] bufferX = WalkRouteFinder.getLastPathBufferX();
			int[] bufferZ = WalkRouteFinder.getLastPathBufferZ();
			
			for (int i = steps - 1; i >= 0; i--) {
				pathQueue.interpolateClipped(bufferX[i], bufferZ[i], mode);
			}
			
			if (isPlayer()) {
				((Player) this).write(new ChangeMapMarker(bufferX[0], bufferZ[0]));
			}
			
			return new Tile(bufferX[0], bufferZ[0], tile.level);
		}
	}
	
	public void unclippedStep(Tile target, PathQueue.StepType mode) {
		pathQueue.step(target.x, target.z, mode);
	}
	
	public int size() {
		return 1;
	}
	
	public boolean walkTo(MapObj obj, PathQueue.StepType mode) {
		pathQueue.clear();
		
		if (stunned()) {
			if (mode == PathQueue.StepType.REGULAR)
				message("You're stunned!");
			return false;
		}
		
		if (steroidsRoute) {
			LinkedList<Direction> dirs = new LinkedList<>();
			PathRouteFinder finder = new PathRouteFinder(this);
			Route route = Route.to(world, obj);
			finder.path(route, tile.x, tile.z, tile.level, size(), dirs);
			
			Tile cur = tile;
			while (!dirs.isEmpty()) {
				Direction next = dirs.poll();
				cur = cur.transform(next.x, next.y, 0);
				pathQueue.stepClipped(cur.x, cur.z, mode);
				
			}
			
			finder.free();
			return !route.alternative;//temp
		} else {
			ObjectStrategy target = new ObjectStrategy(world, obj);
			int steps = WalkRouteFinder.findRoute(world().definitions(), tile.x, tile.z, tile.level, 1, target, true, false);
			int[] bufferX = WalkRouteFinder.getLastPathBufferX();
			int[] bufferZ = WalkRouteFinder.getLastPathBufferZ();
			
			for (int i = steps - 1; i >= 0; i--) {
				pathQueue.interpolateClipped(bufferX[i], bufferZ[i], mode);
			}
			
			return !WalkRouteFinder.isAlternative;
		}
	}
	
	public boolean walkTo(Entity entity, PathQueue.StepType mode) {
		pathQueue.clear();
		
		if (stunned()) {
			if (mode == PathQueue.StepType.REGULAR)
				message("You're stunned!");
			return false;
		}
		
		if (steroidsRoute) {
			LinkedList<Direction> dirs = new LinkedList<>();
			PathRouteFinder finder = new PathRouteFinder(this);
			Route route = Route.to(entity);
			finder.path(route, tile.x, tile.z, tile.level, size(), dirs);
			
			Tile cur = tile;
			while (!dirs.isEmpty()) {
				Direction next = dirs.poll();
				cur = cur.transform(next.x, next.y, 0);
				pathQueue.stepClipped(cur.x, cur.z, mode);
				
			}
			
			finder.free();
			return !route.alternative;//temp
		} else {
			EntityStrategy target = new EntityStrategy(entity);
			int steps = WalkRouteFinder.findRoute(world().definitions(), tile.x, tile.z, tile.level, size(), target, true, false);
			int[] bufferX = WalkRouteFinder.getLastPathBufferX();
			int[] bufferZ = WalkRouteFinder.getLastPathBufferZ();
			
			for (int i = steps - 1; i >= 0; i--) {
				pathQueue.interpolateClipped(bufferX[i], bufferZ[i], mode);
			}
			
			return !WalkRouteFinder.isAlternative;
		}
	}
	
	public boolean frozen() {
		return timers().has(TimerKey.FROZEN);
	}
	
	public boolean stunned() {
		return timers().has(TimerKey.STUNNED);
	}
	
	public void noRetaliation(boolean b) {
		noRetaliation = b;
	}
	
	public Tile stepTowards(Entity e, int maxSteps) {
		return stepTowards(e, e.tile, maxSteps);
	}
	
	public Tile stepTowards(Entity e, Tile t, int maxSteps) {
		if (e == null)
			return tile;
		
		EntityStrategy target = new EntityStrategy(e, 0, t);
		
		int steps = WalkRouteFinder.findRoute(world().definitions(), tile.x, tile.z, tile.level, size(), target, true, false);
		int[] bufferX = WalkRouteFinder.getLastPathBufferX();
		int[] bufferZ = WalkRouteFinder.getLastPathBufferZ();
		
		Tile last = tile;
		for (int i = steps - 1; i >= 0; i--) {
			maxSteps -= pathQueue.interpolate(bufferX[i], bufferZ[i], PathQueue.StepType.REGULAR, maxSteps, true);
			
			last = new Tile(bufferX[i], bufferZ[i], tile.level);
			if (maxSteps <= 0)
				break;
		}
		
		return last;
	}
	
	public Tile stepTowards(Tile t, int maxSteps) {
		EntityStrategy target = new EntityStrategy(t);
		
		int steps = WalkRouteFinder.findRoute(world().definitions(), tile.x, tile.z, tile.level, size(), target, true, false);
		int[] bufferX = WalkRouteFinder.getLastPathBufferX();
		int[] bufferZ = WalkRouteFinder.getLastPathBufferZ();
		
		Tile last = tile;
		for (int i = steps - 1; i >= 0; i--) {
			maxSteps -= pathQueue.interpolate(bufferX[i], bufferZ[i], PathQueue.StepType.REGULAR, maxSteps, true);
			
			last = new Tile(bufferX[i], bufferZ[i], tile.level);
			if (maxSteps <= 0)
				break;
		}
		
		return last;
	}
	
	public boolean touches(Entity e) {
		return touches(e, tile);
	}
	
	public boolean touches(Entity targ, Tile from) {
		EntityStrategy strat = new EntityStrategy(targ, 0, true);
		int[][] clipAround = world.clipSquare(targ.tile().transform(-5, -5, 0), 11); // TODO better algo for determining the size we need..
		try {
			return strat.canExit(from.x, from.z, size(), clipAround, targ.tile.x - 5, targ.tile.z - 5);
		} catch (ArrayIndexOutOfBoundsException e) {
			logger.error("AI OOB thrown using args {}, {}, {}, {}", targ, targ.tile, from, e.getMessage());
			logger.error(e);
			return false;
		}
	}
	
	public boolean locked() {
		return lock != null && lock != LockType.NONE;
	}
	
	public String lockState() {
		return lock == null ? "NULL" : lock.name();
	}
	
	public boolean isLogoutOkLocked() {
		return lock == LockType.FULL_LOGOUT_OK;
	}
	
	public boolean moveLocked() {
		return lock == LockType.MOVEMENT;
	}
	
	public void lock() {
		lock = LockType.FULL;
	}
	
	public void lockNoDamage() {
		lock = LockType.NULLIFY_DAMAGE;
	}
	
	public void lockDelayDamage() {
		lock = LockType.DELAY_DAMAGE;
	}
	
	public void lockDamageOk() {
		lock = LockType.FULL_WITHDMG;
	}
	
	public boolean isNullifyDamageLock() {
		return lock == LockType.NULLIFY_DAMAGE;
	}
	
	public boolean isDelayDamageLocked() {
		return lock == LockType.DELAY_DAMAGE;
	}
	
	/**
	 * Locked, unable to attack. Cerberus.
	 */
	public boolean isNoAttackLocked() {
		return lock == LockType.NO_ATTACK;
	}
	
	public boolean isDamageOkLocked() {
		return lock == LockType.FULL_WITHDMG;
	}
	
	public void lockMovement() {
		lock = LockType.MOVEMENT;
	}
	
	public void lockNoAttack() {
		lock = LockType.NO_ATTACK;
	}
	
	public void unlock() {
		lock = LockType.NONE;
	}
	
	public abstract void hp(int hp, int exceed);
	
	public abstract int hp();
	
	public abstract int maxHp();
	
	public void message(String format, Object... params) {
		// Stub to ease player-specific messaging
	}
	
	public void heal(int amount) {
		heal(amount, 0);
	}
	
	public void heal(int amount, int exceed) {
		hp(hp() + amount, exceed);
	}
	
	public Hit hit(HitOrigin origin, int hit) {
		return hit(origin, hit, 0);
	}
	
	public Hit hit(HitOrigin origin, int hit, int delay) {
		return hit(origin, hit, delay, null, true);
	}
	
	public Hit hit(HitOrigin origin, int hit, int delay, Hit.Type hittype) {
		return hit(origin, hit, delay, hittype, true);
	}
	
	/**
	 * Hit constructor with overhead protection prayers, PID and a CombatStyle chained on.
	 * Marked as not built so hit.submit() is required to finalize.
	 */
	public Hit hitpvp(HitOrigin origin, int hit, int delay, CombatStyle style) {
		Hit h = hit(origin, hit, delay, false).combatStyle(style).applyProtection().pidAdjust();
		return h;
	}
	
	public Hit hit(HitOrigin origin, int hit, int delay, boolean built) {
		return hit(origin, hit, delay, null, built);
	}
	
	public Hit hit(HitOrigin origin, int hit, int delay, Hit.Type type, boolean built) {
		Hit h = new Hit(hit, type != null ? type : hit > 0 ? Hit.Type.REGULAR : Hit.Type.MISS, delay, built).origin(origin).target(this).applyDamageReduction();
		
		// Target is performing a delayed action where damage is disregarded.
		if (!isNullifyDamageLock() && h.built()) {
			// No problems with instant taking 0 delay hits, rather than queuing them. They are the first thing (juust after timers) in player processed, before scripts.
			// Since Npc process is after player process (and hence player.hitprocess()) npc 0t hits must be applied instantly because the hitprocess() won't be called again until the next tick!
			if (delay <= 0 && h.hasPidAdjusted) {
				takeHit(h);
			} else {
				hits.add(h);
			}
		}
		
		// If this was an entity that damaged us, register it.
		if (origin instanceof Entity) {
			// Put a timer on 17 ticks to avoid logging out now.
			timers.extendOrRegister(TimerKey.COMBAT_LOGOUT, 20);
			
			Entity fromEntity = (Entity) origin;
			if (fromEntity.isNpc()) {
				// Every single damage attack from an Npc subjects the Npc to being venomed by the victim.
				// For players, this is checked in PlayerCombat, per 'attack' rather than per 'hit' (such as claws are 4 hits.. affects chance)
				Equipment.checkTargetVenomGear(fromEntity, this);
			}
		}
		return h;
	}
	
	/**
	 * Does the block animation.
	 */
	public void blockHit(Hit hit) {
		if (hit != null && hit.style() == CombatStyle.RANGE && hit.origin() != null && hit.origin() instanceof Player) {
			// range attacks trigger block animation when the attack is done, not after! Been on 07 to prove.
		} else {
			animate(424);
		}
	}
	
	/**
	 * Sounds that happen when the hit appears.
	 * Two distinct: a sound if damage>0 .. and a shield block sound.
	 * Note: these sounds are special because they are _personal_ 'effect' sounds - not AREA sounds broadcast to closeby players.
	 */
	public void takehitSound(Hit hit) {
		if (hit == null)
			return;
		// block sounds depends entirely on entity type
		
		if (hit.origin() != null && hit.origin() instanceof Player) {
			if (hit.damage() > 0)
				((Player) hit.origin()).sound(CombatSounds.damage_sound(world), 20, 0);
		}
	}
	
	public boolean poison(int damage) {
		return poison(damage, true);
	}
	
	public boolean poison(int dmg, boolean sendmsg) {
		int venomState = attribOr(AttributeKey.VENOM_TICKS, 0);
		// Can't inflict poison if venomed, it takes priority.
		if (venomState > 0)
			return false;
		
		if (isPlayer()) {
			Player p = (Player) this;
			if (Equipment.venomHelm(p)) { // Serp helm stops poison.
				return false;
			}
			if (p.varps().varp(Varp.HP_ORB_COLOR) != 0) {
				// Immune or already poisoned.
				return false;
			}
			if (sendmsg)
				message("You have been poisoned!");
			p.varps().varp(Varp.HP_ORB_COLOR, Poison.ticksForDamage(dmg));
		} else if (isNpc()) {
			Npc me = (Npc) this;
			if (me.isPoisonImmune())
				return false;
			if ((int)attribOr(AttributeKey.POISON_TICKS, 0) != 0) {
				// Immune / already poisoned
				return false;
			}
			putattrib(AttributeKey.POISON_TICKS, Poison.ticksForDamage(dmg));
		}
		return true;
	}
	
	public void venom(Entity source) {
		if (source == null)
			return;
		if (isPlayer()) {
			Player p = (Player) this;
			if (Equipment.venomHelm(p)) { // Serp helm stops venom.
				return;
			}
			boolean admin_bypass = false;
			if (source.isPlayer()) {
				boolean fromAdmin = ((Player) source).privilege().eligibleTo(Privilege.ADMIN);
				if (fromAdmin && GameCommands.VENOM_FROM_ADMINS_ON) {
					admin_bypass = true;
				}
			}
			// Source was a normal player
			if (!GameCommands.VENOM_VS_PLAYERS_ON && !admin_bypass)
				return;
		} else if (isNpc()) {
			Npc me = (Npc) this;
			if (me.isVenomImmune()) {
				return;
			}
		}
		if ((int) attribOr(AttributeKey.VENOM_TICKS, 0) == 0) {
			if (isPlayer()) {
				Player me = (Player) this;
				me.varps().varp(Varp.HP_ORB_COLOR, 1_000_000); // Now venomed
				message("<col=145A32>You've been infected with venom!");
			}
			putattrib(AttributeKey.VENOM_TICKS, 8); // default start. 8 cycles
			timers().register(TimerKey.VENOM, 34);
		}
	}
	
	public Map<Integer, PlayerDamageTracker> damagers() {
		return damagers;
	}
	
	public void clearDamagers() {
		damagers.clear();
	}
	
	public void clearDamageTimes() {
		damagerLastTime.clear();
	}
	
	public abstract Optional<Integer> killer();
	
	public boolean dead() {
		//int queuedDamage = hits.stream().mapToInt(Hit::damage).sum();
		return hp()/* - queuedDamage*/ < 1;
	}
	
	/**
	 * Determines if the entity is alive or not depending on the
	 * state of {@link #dead()}.
	 *
	 * @return {@code true} if the entity is alive, depending on the aforementioned
	 * variable.
	 * @see #dead()
	 */
	public boolean alive() {
		return !dead();
	}
	
	public void stopActions(boolean cancelMoving) {
		if (locked()) {
			return;
		}
		
		world.server().scriptExecutor().interruptFor(this);
		sync.faceEntity(null);
		// Graphics and animations are not reset when you walk.
		if (cancelMoving)
			pathQueue.clear();
	}
	
	public void autoRetaliate(Entity attacker) {
		if (dead() || hp() < 1 || noRetaliation || locked() || attacker == this)
			return;
		
		// Override logic
		putattrib(AttributeKey.TARGET, new WeakReference<>(attacker));
		// As soon as the hit on us appears, we'll turn around and face the attacker.
		face(attacker);
		world.server().scriptExecutor().executeScript(this, EntityCombat.autoretaliate);
	}
	
	public void attack(Entity e) {
		if (e.dead() || dead() || isNoAttackLocked()) { // Fuck that
			return;
		}
		stopActions(true);
		
		putattrib(AttributeKey.TARGET, new WeakReference<>(e));
		// TODO hardcoded, please softcode me
		if (isPlayer()) {
			world.server().scriptExecutor().executeScript(this, PlayerCombat.script);
		} else {
			if (isNpc()) {
				Npc npc = (Npc) this;
				if (npc.combatInfo() != null && npc.combatInfo().scripts != null && npc.combatInfo().scripts.combat_ != null) {
					world.server().scriptExecutor().executeScript(this, npc.combatInfo().scripts.combat_);
					return;
				}
			}
			
			// NOTE: do not put any combat scripts here. NPC info has support for passing a script in the definitions.
			//       Look at how certain boss definitions work.
			int id;
			if (isNpc() && (((id = ((Npc) this).id()) >= 247 && id <= 269) || id == 6593)) {
				world.server().scriptExecutor().executeScript(this, BasicDragonCombat.script);
			} else {
				world.server().scriptExecutor().executeScript(this, EntityCombat.script);
			}
		}
	}
	
	public void face(Entity e) {
		sync.faceEntity(e);
	}
	
	/**
	 * Face coordinates, but take into consideration the center of a large than 1x1 object
	 */
	public void faceObj(MapObj obj) {
		int x = obj.tile().x;
		int z = obj.tile().z;
		
		// Do some trickery to face properly
		if (tile.x == x && tile.z == z && (obj.type() == 0 || obj.type() == 5)) {
			if (obj.rot() == 0) {
				x--;
			} else if (obj.rot() == 1) {
				z++;
			} else if (obj.rot() == 2) {
				x++;
			} else if (obj.rot() == 3) {
				z--;
			}
		}
		
		int sx = obj.definition(world).sizeX;
		int sz = obj.definition(world).sizeY;
		
		//sync.facetile(new Tile((int) (x * 2) + sx, (int) (z * 2) + sz));
		sync.facetile(new Tile(x + (sx / 2), z + (sz / 2)));
	}
	
	public void faceTile(Tile tile) {
		sync.facetile(new Tile(tile.x, tile.z));
	}
	
	public void faceTile(double x, double z) {
		sync.facetile(new Tile((int) x, (int) z));
	}
	
	public void cycle() {
		timers.cycle();
	}
	
	public void cycle_hits(boolean fromPlayerOrigin) {
		
		// Only process hits if not locked!
		if (hp() > 0) {
			
			// When teleporting, all damage is forgotten
			if (lock == LockType.NULLIFY_DAMAGE) {
				hits.clear();
				
				// In other situations, damage is only shown when not locked.
			} else if (!locked() || lock == LockType.FULL_WITHDMG) {
				for (int i = 0; i < hits.size(); i++) {
					Hit hit = hits.get(i);
					
					// For Player#process -> iterate player hits before scripts execute.
					if (fromPlayerOrigin && (hit.origin() == null || !(hit.origin() instanceof Player))) {
						continue;
					} else if (!fromPlayerOrigin && hit.origin() != null && hit.origin() instanceof Player) {
						continue;
					}
					
					// Note: due to instant-hitting 0t hits (from Npcs or PID players) exclusions must be in takeHit() ..
					// since this cycle method isn't used for 0t hits! They go straight to takeHit
					if (hit.invalid()) {
						hits.remove(i);
						i--;
						continue;
					}
					
					// See #blockHit for the mechanics of when block animations are performed.
					if (hit.delay() == 1 && hit.style() == CombatStyle.RANGE && hit.block() && !hit.hasPidAdjusted) {
						blockHit(hit);
					}
					if (hit.delay() <= 0) {
						hits.remove(i);
						i--;
						
						takeHit(hit);
					} else {
						hit.delay(hit.delay() - 1);
					}
				}
			}
		}
		
		if (hp() < 1 && !locked()) { // Avoid dieing while doing something critical! Such as getting speared...
			hits.clear();
			die();
		}
	}
	
	public void takeHit(Hit hit) {
		if (hit.beenProcessed())
			return;
		hit.processed();
		
		if (hit.invalid())
			return;
		
		putattrib(AttributeKey.LAST_HIT, hit);
		
		int damage = hit.damage();
		// Protection prayers :)
		if (isPlayer() && hit.fromEntity()) {
			Player us = (Player) this;
			
			// NOTE: Protection prayers are applied in playercombat when you swing your weapon, not when the hit is processed.
			// We haven't yet done this for Npcs...
			// Reason: changing it will affect jad timers and that's pretty complex.
			if (hit.origin() instanceof Npc && ((Npc) hit.origin()).id() != InfernoContext.TZKAL_ZUK) {
				if (us.varps().varbit(Varbit.PROTECT_FROM_MELEE) == 1 && hit.style() == CombatStyle.MELEE) {
					damage = 0;
				} else if (us.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1 && hit.style() == CombatStyle.MAGIC) {
					damage = 0;
				} else if (us.varps().varbit(Varbit.PROTECT_FROM_MISSILES) == 1 && hit.style() == CombatStyle.RANGE) {
					damage = 0;
				}
			}
		}
		
		boolean meIsPlayer = isPlayer() && hit.origin() instanceof Entity;
		if (damage > 0 && meIsPlayer) {
			Player me = (Player) this;
			
			//Damage value at this point is final, as modified by shield effects, protection prayers, etc.
			if (!hit.isRecoilDamage()) {
				
				//If we have used the SOTD special attack, reduce incoming melee damage by 50%.
				if (timers().has(TimerKey.SOTD_DAMAGE_REDUCTION) && equipment().hasAny(11791, 12904, 12902) && hit.style() == CombatStyle.MELEE) {
					damage *= 0.5;
				}
				
				// Passive effect of the Elysian spirit shield
				if (me.equipment().hasAt(EquipSlot.SHIELD, 12817) && world.rollDie(100, 70)) {
					damage -= damage / 4; // Remove 25% of the original damage. Don't worry for < 0, code below handles that.
					graphic(321);
				}
				
				// Passive effect of Dinh's Bulwark on Block mode
				if (me.equipment().hasAt(EquipSlot.SHIELD, 21015) && me.varps().varp(Varp.ATTACK_STYLE) == 3 && hit.origin() instanceof Npc) {
					damage -= damage / 5;
				}
				
				int hpdmg = damage > hp() ? hp() : damage;
				
				Entity src = ((Entity) hit.origin());
				
				// Veng
				if (hit.origin() != this && (Boolean) attribOr(AttributeKey.VENGEANCE_ACTIVE, false)) {
					clearattrib(AttributeKey.VENGEANCE_ACTIVE);
					//PID on rs may mean its instant. by default, 1 tick tho
					src.hit(this, (int) (hpdmg * 0.75), hit.hasPidAdjusted ? 1 : 0).block(false).setIsRecoil();
					sync.shout("Taste vengeance!");
					if (src.isNpc()) {
						if (((Npc) src).id() == 319) {
							varps().varp(Varp.CORP_BEAST_DAMAGE, varps().varp(Varp.CORP_BEAST_DAMAGE) + (int) (hpdmg * .75));
						}
					}
				}
				
				if (Equipment.hasAmmyOfDamned(me) && CombatFormula.fullDharok(me) && world.rollDie(100, 25)) {
					src.hit(this, (int) (hpdmg * 0.15), hit.hasPidAdjusted ? 1 : 0).block(false).setIsRecoil();
				}
				
				// Smite
				if (me.skills().level(Skills.PRAYER) > 0) {
					if (hit.origin() != null && hit.origin() instanceof Player) {
						Player attacker = (Player) hit.origin();
						if (attacker.varps().varbit(Varbit.SMITE) == 1 && !VarbitAttributes.varbiton(this, Varbit.INFPRAY)) {//infpray disabled
							double deduct = (double) damage / 4.0D;
							int amt = (int) Math.floor(deduct);
							if (deduct > 0 && amt > 0) {
								skills().setLevel(Skills.PRAYER, Math.max(0, skills().level(Skills.PRAYER) - amt));
								// Did that render the player out of prayer points?
								if (skills().level(Skills.PRAYER) < 1) {
									Prayers.disableAllPrayers(me);
									message("You have run out of prayer points, you must recharge at an altar.");
								}
							}
						}
					}
				}
				
				// pheonix necklace
				if (me.equipment().hasAt(EquipSlot.AMULET, 11090) && !DuelOptions.ruleToggledOn(me, DuelOptions.NO_FOOD) && !ClanWars.checkRule(me, CWSettings.FOOD)) {
					int newhp = hp() - damage;
					if (newhp > 0 && newhp <= me.skills().xpLevel(Skills.HITPOINTS) * 0.2) {
						me.heal((int) (me.skills().xpLevel(Skills.HITPOINTS) * 0.3));
						equipment().remove(new Item(11090), true);
						message("Your pheonix necklace heals you, but is destroyed in the process.");
					}
				}
				
				// Ring of recoil damage
				if (hit.origin() instanceof Entity && hit.origin() != this) {
					if (me.equipment().hasAt(EquipSlot.RING, 2550)) {
						int charges = (int) attribOr(AttributeKey.RING_OF_RECOIL_CHARGES, 40) - 1;
						
						if (charges == 0) {
							putattrib(AttributeKey.RING_OF_RECOIL_CHARGES, 40);
							equipment().remove(new Item(2550), true);
							message("<col=804080>Your ring of recoil has shattered!");
						} else {
							putattrib(AttributeKey.RING_OF_RECOIL_CHARGES, charges);
							((Entity) hit.origin()).hit(this, damage > 10 ? (damage / 10) : 1, 1).block(false).combatStyle(CombatStyle.MELEE).setIsRecoil(); // Typeless damage
							if (src.isNpc()) {
								if (((Npc) src).id() == 319) {
									varps().varp(Varp.CORP_BEAST_DAMAGE, varps().varp(Varp.CORP_BEAST_DAMAGE) + (damage > 10 ? (damage / 10) : 1));
								}
							}
						}
					}
					boolean suffering_r = me.equipment().hasAt(EquipSlot.RING, 20655);
					if (!VarbitAttributes.varbiton(this, Varbit.RING_OF_SUFFERING_RECOIL_DISABLED) && (suffering_r || me.equipment().hasAt(EquipSlot.RING, 20657))) {
						int charges = me.equipment().get(EquipSlot.RING).propertyOr(ItemAttrib.CHARGES, 0);
						if (charges > 0) {
							me.equipment().get(EquipSlot.RING).property(ItemAttrib.CHARGES, charges - 1);
							((Entity) hit.origin()).hit(this, damage > 10 ? (damage / 10) : 1, 1).block(false).combatStyle(CombatStyle.MELEE).setIsRecoil(); // Typeless damage
							if (src.isNpc()) {
								if (((Npc) src).id() == 319) {
									varps().varp(Varp.CORP_BEAST_DAMAGE, varps().varp(Varp.CORP_BEAST_DAMAGE) + (damage > 10 ? (damage / 10) : 1));
								}
							}
							if (charges - 1 == 0) {
								me.message("Your Ring of Suffering has run out of charges.");
								me.equipment().remove(me.equipment().get(EquipSlot.RING), false, EquipSlot.RING);
								me.equipment().add(new Item(suffering_r ? 19550 : 19710), false, EquipSlot.RING);
							}
						}
					}
				}
			}
			
			// Also applies to recoil or veng hits.
			// Ring of Life doesn't work on Deadman worlds.
			if (!world.realm().isDeadman()) {
				// Under 10% hp, hit won't kill us
				if (hp() - damage > 0 && hp() <= me.skills().xpLevel(Skills.HITPOINTS) / 10) {
					boolean ring = me.equipment().hasAt(EquipSlot.RING, 2570);
					
					boolean defenceCape = (int) me.attribOr(AttributeKey.DEFENCE_PERK_TOGGLE, 0) == 1 &&
							(me.equipment().hasAt(EquipSlot.CAPE, CapeOfCompletion.DEFENCE.getTrimmed())
									|| me.equipment().hasAt(EquipSlot.CAPE, CapeOfCompletion.DEFENCE.getUntrimmed()));
					
					if (ring || (Equipment.wearingMaxCape(me) && VarbitAttributes.varbiton(me, Varbit.MAXCAPE_ROL_ON)) || defenceCape) {
						if (Teleports.rolTeleport(me)) {
							world.server().scriptExecutor().executeScript(this, Teleports.ringOfLifeTeleport);
							if (ring) {
								me.equipment().set(EquipSlot.RING, null);
								message("Your Ring of Life shatters as it teleports you to safety!");
							} else if (defenceCape) {
								message("Your Defence Cape's Ring of Life effects kicks in and teleports you to safety!");
							} else {
								message("Your Max Cape's Ring of Life effect kicks in and teleports you to safety!");
							}
						}
					}
				}
			}
			
			// New crystal shield. Tradeable form. Won't turn to a seed on PvP death.
			// Upon being damaged, turn it into 'full' shield - which does turn to a seed
			if (me.equipment().hasAt(EquipSlot.SHIELD, 4224)) {
				me.equipment().remove(new Item(4224), true, EquipSlot.SHIELD);
				me.equipment().add(new Item(4225), true, EquipSlot.SHIELD);
				me.message("Your crystal shield has degraded!");
			}
		}
		
		
		if (isNpc() && hit.origin() instanceof Entity) {
			Npc npc = (Npc) this;
			
			//Update the inferno controller for the npc if its active.
			if (attribOr(AttributeKey.INFERNO_CONTROLLER, null) != null) {
				InfernoNpcController controller = attrib(AttributeKey.INFERNO_CONTROLLER);
				controller.onDamage();
			}
			
			/*
			 * TODO Allow for an event to take place on hit so that the damage can be applied
			 * TODO in the form of a task rather than this hardcoding.
			 */
			if (hit.origin() instanceof Player) {
				Player playerOrigin = (Player) hit.origin();
				
				if (Pest.Companion.getIDS().contains(npc.id()) || PortalType.Companion.getIDS().contains(npc.id())) {
//					int current = playerOrigin.attribOr(AttributeKey.TEMPORARY_PEST_CONTROL_POINTS, 0);
					
					playerOrigin.putattrib(AttributeKey.TEMPORARY_PEST_CONTROL_POINTS,
							(int) playerOrigin.attribOr(AttributeKey.TEMPORARY_PEST_CONTROL_POINTS, 0) + hit.damage());
				}
			}
			
			// You can't damage these Npcs with anything.. at least it causes minimal damage.
			if (npc.id() == 5534) { // Kraken tent
				damage = 0;
			}
			if (npc.id() == 496) { // Big kraken whirlpool (boss)
				if (npc.sync().transmog() != 494)
					damage = 0;
				else if (CombatStyle.MAGIC != hit.style())
					damage = 0;
			}
			
			if (npc.id() == 5886 || npc.id() == 5887 || npc.id() == 5888 || npc.id() == 5889)
				putattrib(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, hit.origin());
			
			
			//Demonic Gorilla
			if (npc.id() == 7144) {
				int mage_hit = npc.attribOr(AttributeKey.DEMONIC_GORILLA_MAGE, 0);
				int melee_hit = npc.attribOr(AttributeKey.DEMONIC_GORILLA_MELEE, 0);
				double range_hit = npc.attribOr(AttributeKey.DEMONIC_GORILLA_RANGE, 0.0);
				
				if (hit.style() == CombatStyle.MELEE) {
					if (npc.attribOr(AttributeKey.DEMONIC_GORILLA_PRAY_MELEE, false)) damage = 0;
					npc.putattrib(AttributeKey.DEMONIC_GORILLA_MELEE, melee_hit + 1);
					
					if (melee_hit >= 3) {
						npc.sync().transmog(7144);
						npc.animate(7228);
						damage = 0;
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_MELEE, 0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_MAGE, 0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_RANGE, 0.0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_MELEE, true);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_MAGE, false);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_RANGE, false);
					}
				}
				
				if (hit.style() == CombatStyle.MAGIC) {
					if (npc.attribOr(AttributeKey.DEMONIC_GORILLA_PRAY_MAGE, false)) damage = 0;
					npc.putattrib(AttributeKey.DEMONIC_GORILLA_MAGE, mage_hit + 1);
					
					if (mage_hit >= 3) {
						npc.sync().transmog(7146);
						npc.animate(7228);
						damage = 0;
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_MELEE, 0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_MAGE, 0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_RANGE, 0.0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_MELEE, false);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_MAGE, true);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_RANGE, false);
					}
				}
				
				if (hit.style() == CombatStyle.RANGE) {
					if (npc.attribOr(AttributeKey.DEMONIC_GORILLA_PRAY_RANGE, false)) damage = 0;
					
					Player attacker = (Player) hit.origin();
					Item weaponId = attacker.equipment().get(3);
					
					if (EquipmentInfo.attackStyleFor(attacker).equals(AttackStyle.RAPID) && attacker.world().equipmentInfo().weaponType(weaponId.id()) != WeaponType.CROSSBOW &&
							(attacker.world().equipmentInfo().weaponType(weaponId.id()) != WeaponType.BOW || weaponId.id() != 12924 && weaponId.id() != 12926))
						
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_RANGE, range_hit + 0.5);
					else
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_RANGE, range_hit + 1.0);
					
					if (range_hit >= 3.0) {
						npc.sync().transmog(7145);
						npc.animate(7228);
						damage = 0;
						attacker.stopActions(false);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_MELEE, 0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_MAGE, 0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_RANGE, 0.0);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_MELEE, false);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_MAGE, false);
						npc.putattrib(AttributeKey.DEMONIC_GORILLA_PRAY_RANGE, true);
					}
				}
			}
			
			// Gargs
			if (npc.hp() - damage <= 0) {
				if (npc.id() == 412 || npc.id() == 413 || npc.id() == 1543) {
					Player attacker = (Player) hit.origin();
					if (attacker.varps().varbit(Varbit.GARGOYLE_SMASHER) == 1) {
						attacker.animate(401);
						npc.hp(0, 0);
					} else {
						attacker.message("Gargoyles can only be killed using a Rockhammer.");
						damage = 0;
						hit.type(Hit.Type.MISS);
					}
				}
			}
			
			//Tz-Kek recoil effect
			if (npc.id() == 3118 && hit.origin() instanceof Entity && hit.origin() != this) {
				if (hit.style() == CombatStyle.MELEE && damage > 0) {
					Player attacker = (Player) hit.origin();
					attacker.hit(this, 1).block(false).combatStyle(CombatStyle.MELEE).setIsRecoil();
				}
			}
			
			// Bosses agro setting
			if (npc.id() == 2215 && Graardor.getBANDOS_AREA().contains(npc)) { // Bandos, in the real area.
				Graardor.setLastBossDamager((Entity) hit.origin());
			} else if (npc.id() == 3162 && Kreearra.getENCAMPMENT().contains(npc)) { // Arma, in the real area.
				Kreearra.setLastBossDamager((Entity) hit.origin());
			} else if (npc.id() == 2205 && Zilyana.getENCAMPMENT().contains(npc)) { // sara, in the real area.
				Zilyana.setLastBossDamager((Entity) hit.origin());
			} else if (npc.id() == 3129 && Kril.getENCAMPMENT().contains(npc)) { // zam, in the real area.
				Kril.setLastBossDamager((Entity) hit.origin());
			}
			
			if (GwdLogic.isBoss(npc.id())) {
				Map<Entity, Long> last_attacked_map = attribOr(AttributeKey.LAST_ATTACKED_MAP, new HashMap<>());
				last_attacked_map.put((Entity) hit.origin(), System.currentTimeMillis());
				//System.out.println("recorded from "+(Entity)hit.origin());
				putattrib(AttributeKey.LAST_ATTACKED_MAP, last_attacked_map);
			}

			if (npc.id() == 6611 && (boolean)attribOr(AttributeKey.VETION_HELLHOUND_SPAWNED, false)) {
				damage = 0;
				hit.type(Hit.Type.MISS);
				// hit script will tell attacker that minions must be killed off before vetion can be damaged again
			}
		}
		
		
		hit.overkill(damage);
		if (damage > hp()) {
			damage = hp();
		}
		
		// Make the zero always the block hit
		if (damage < 1) {
			damage = 0;
			hit.type(Hit.Type.MISS);
		}
		
		final int dmg = damage;
		takehitSound(hit.adjustDmg(dmg));
		
		if (hit.origin() != null && hit.origin() instanceof Player) {
			Player attacker = (Player) hit.origin();
			
			if (hit.getTarget() != null) {
				
				// Don't record damage delt to ourself if we're a player.
				if (isNpc() || (isPlayer() && ((Player) this).id() != attacker.id())) {
					
					if (attacker.id() instanceof Integer) { // String on login
						
						// Try to find an existing damage entry..
						boolean[] matched = {false}; // Required as an Array due to use in Lambda function
						damagers.entrySet().forEach(e -> {
							if (e.getKey() == (int) attacker.id()) {
								e.getValue().update(attacker, dmg);
								matched[0] = true;
								damagerLastTime.put((int) attacker.id(), System.currentTimeMillis());
								//message("updated tracked dmg total on me id-"+this.index()+" by "+attacker.name()+"  total dmg="+e.getValue().damage()+" (+"+hit+")"); // Debug
							}
						});
						
						// Did we not update an existing one? Insert new.
						if (!matched[0]) {
							damagers.put((int) attacker.id(), new PlayerDamageTracker().update(attacker, dmg));
							//message("registered new tracking dmg on "+this.index()+" by "+attacker.name()+"  dmg="+hit); // Debug
							damagerLastTime.put((int) attacker.id(), System.currentTimeMillis());
						}
					}
				}
			}
		}
		
		//Now apply damage and set in GPI
		if (!VarbitAttributes.varbiton(this, Varbit.INFHP)) {//infhp disabled.
			if (isNpc()) {
				Npc npc = (Npc) this;
				if (npc.id() != 2668) {
					hp(hp() - damage, 0);
					if (hp() < 1) {
						if (hit.origin() instanceof Entity) {
							putattrib(AttributeKey.KILLER, hit.origin());
						}
					}
				}
			} else {
				hp(hp() - damage, 0);
				if (hp() < 1) {
					if (hit.origin() instanceof Entity) {
						putattrib(AttributeKey.KILLER, hit.origin());
					}
				}
			}
			
			//Used for updating the boss overlay in The Inferno. Only request an update if dmg was positive.
			if (hit.origin() != null & hit.origin() instanceof Player) {
				Player attacker = (Player) hit.origin();
				Entity victim = hit.getTarget();
				
				if (victim.isNpc()) {
					Npc n = (Npc) victim;
					if (hit.damage() > 0 && InfernoContext.shouldUpdateOverlay(attacker, n)) {
						if (InfernoContext.inSession(attacker)) {
							InfernoContext.updateBossOverlay(n, attacker);
						}
					}
				}
				
				//Update the overlay, if needed.
				if (EntityOverlay.enabled(attacker)) {
					EntityOverlay.update(attacker, victim);
				}
			}
		}
		
		boolean magic_splash = damage == 0
				&& hit.type() != null && hit.type() == Hit.Type.MISS
				&& hit.style() != null && hit.style() == CombatStyle.MAGIC
				&& hit.graphic() != null && hit.graphic().id() == 85;
		
		if (!magic_splash) { // splashing doesn't show the 0
			if (hit.type() == Hit.Type.PRAYER)
				hit.type(Hit.Type.REGULAR);
			sync.hit(hit);
		}
		
		if (isNpc()) {
			Npc npc = (Npc) this;
			if (npc.combatInfo() != null && npc.combatInfo().scripts != null && npc.combatInfo().scripts.hit_ != null) {
				putattrib(AttributeKey.LAST_HIT_DMG, hit);
				world.server().scriptExecutor().executeScript(this, npc.combatInfo().scripts.hit_);
			}
		}
		
		// Auto-retaliate
		if (hit.fromEntity() && hit.style() != CombatStyle.GENERIC && hit.origin() instanceof Entity) {
			if (!timers.has(TimerKey.IN_COMBAT)) {
				if (isPlayer()) {
					Player player = ((Player) this);
					if (player.varps().varp(Varp.AUTO_RETALIATE_DISABLED) == 0) {
						// Players only auto retal the attacker when out of combat.
						boolean mayAttack = true;
						
						// Check if we're in combat right now with someone who's alive
						if (player.timers().has(TimerKey.IN_COMBAT)) {
							mayAttack = false;
						}
						
						// Check attackability
						if (hit.origin() instanceof Entity && !PlayerCombat.canAttack(this, (Entity) hit.origin())) {
							mayAttack = false;
						}
						
						if (mayAttack) {
							autoRetaliate((Entity) hit.origin());
						}
					}
				} else {
					Npc npc = (Npc) this;
					
					// 2668 = Combat dummy
					if (npc.id() != 2668)
						autoRetaliate((Entity) hit.origin());
				}
				
				timers.register(TimerKey.IN_COMBAT, 5);
			} else if (isNpc()) {
				// Npcs do switch aggro context if they get attacked.
				autoRetaliate((Entity) hit.origin());
			}
		}
		
		if (hit.graphic() != null)
			graphic(hit.graphic().id(), hit.graphic().height(), hit.graphic().delay());
		
		// See #blockHit for circumstance for Block animation triggers.
		if (hit.block() && (hit.style() != CombatStyle.MAGIC || !isPlayer())) {
			blockHit(hit);
		}
	}
	
	/**
	 * Max player capacity 254 since the client limit is 255 including yourself.
	 */
	public Player[] closePlayers(int span) {
		return closePlayers(254, span);
	}
	
	public Player[] closePlayers(int maxCapacity, int span) {
		Player[] targs = new Player[maxCapacity];
		int caret = 0;
		for (int idx = 0; idx < 2048; idx++) {
			Player p = world().players().get(idx);
			if (p == null || p == this || tile().distance(p.tile()) > 14 || p.tile().level != tile().level || p.looks().hidden() || p.finished()) {
				continue;
			}
			//already looping 2048 times this will be far too expensive!
			/*if(!((Player)this).sync().hasInView(p.index())){
				continue;
			}*/
			if (p.tile().inSqRadius(tile, span)) {
				targs[caret++] = p;
			}
			if (caret >= targs.length) {
				break;
			}
		}
		Player[] set = new Player[caret];
		System.arraycopy(targs, 0, set, 0, caret);
		return set;
	}
	
	public Npc[] closeNpcs(int span) {
		return closeNpcs(254, span);
	}
	
	public Npc[] closeNpcs(int maxCapacity, int span) {
		Npc[] targs = new Npc[maxCapacity];
		
		int caret = 0;
		for (int idx = 0; idx < world.npcs().size(); idx++) {
			Npc npc = world().npcs().get(idx);
			if (npc == null || npc == this || tile().distance(npc.tile()) > 14 || npc.tile().level != tile().level || npc.finished()) {
				continue;
			}
			if (npc.tile().inSqRadius(tile, span)) {
				targs[caret++] = npc;
			}
			if (caret >= targs.length) {
				break;
			}
		}
		Npc[] set = new Npc[caret];
		System.arraycopy(targs, 0, set, 0, caret);
		return set;
	}
	
	public abstract boolean isPlayer();
	
	public abstract boolean isNpc();
	
	protected abstract void die();
	
	public abstract int attackAnimation();
	
	public Area bounds() {
		return new Area(tile.x, tile.z, tile.x, tile.z);
	}
	
	public Area pathbounds() {
		Tile t = pathQueue.peekLastTile();
		return new Area(t.x, t.z, t.x, t.z);
	}
	
	public Area basebounds(Tile t) {
		return new Area(t.x, t.z, t.x, t.z);
	}
	
	@Suspendable
	public void executeScript(Function1<Script, Object> s) {
		world.server().scriptExecutor().executeScript(this, s); // context set to the entity
	}

	@Suspendable
	public final void executeKtScript(final Function1<Script, ?> function) {
		world.server().scriptExecutor().executeScript(this, function);
	}
	
	public int pvpPid = -1;
	
	public abstract void post_cycle_movement();
}
