package nl.bartpelle.veteres.services.login;

import co.paralleluniverse.strands.Strand;
import io.netty.buffer.ByteBuf;
import nl.bartpelle.veteres.crypto.IsaacRand;
import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.net.ServerHandler;
import nl.bartpelle.veteres.net.future.ClosingChannelFuture;
import nl.bartpelle.veteres.net.message.LoginRequestMessage;
import nl.bartpelle.veteres.services.serializers.PlayerLoadResult;
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer;
import nl.bartpelle.veteres.util.GameCommands;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Created by Bart on 8/1/2015.
 */
public class LoginWorker implements Runnable {
	
	private static final Logger logger = LogManager.getLogger(LoginWorker.class);
	
	private LoginService service;
	
	public static boolean acceptLogins = false;
	
	// Used to check if the user has a username which 100% an admin. Means you don't have
	// to bother loading the profile _just_ to check rights.
	public static List<String> hardcodeAdmins = Arrays.asList("bart", "airo", "simplemind");
	
	public LoginWorker(LoginService service) {
		this.service = service;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				processLoginJob();
			} catch (Exception e) {
				logger.error("Error processing login worker job!", e);
			}
		}
	}

	private void processLoginJob() throws Exception {
		LoginRequestMessage message = service.messages().take();

		if (message == null) {
			return;
		} else if (message.delayedUntil() > System.currentTimeMillis()) {
			service.enqueue(message);
			Strand.sleep(30);
			return;
		}

		logger.debug("Attempting to process login request for {}.", message.username());

		for (int i = 0; i < 16; i++) {
			if (message.crcs()[i] != service.server().store().getIndex(i).getCRC()) {
				ByteBuf resp = message.channel().alloc().buffer(1).writeByte(6);
				message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
				continue;
			}
		}

		// Check IP
		World world = service.server().world();
		InetSocketAddress socketAddress = (InetSocketAddress) message.channel().remoteAddress();
		InetAddress inetaddress = socketAddress.getAddress();
		String ip = inetaddress.getHostAddress(); // IP address of client
		long altCount = world.players().stream().filter(p -> p != null && !p.privilege().eligibleTo(Privilege.ADMIN) && !p.bot() && ip.equals(p.ip())).count();
		boolean admin = hardcodeAdmins.stream().filter(name -> name.equalsIgnoreCase(message.username())).count() > 0;

		if (altCount >= GameCommands.MAX_ALTS(world) && !admin) {
			ByteBuf resp = message.channel().alloc().buffer(1).writeByte(9);
			message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
			//logger.info("Blocked login for " + ip + " with user " + message.username());
			return;
		}

		if (!service.server().isDevServer() && !acceptLogins && !admin) {
			ByteBuf resp = message.channel().alloc().buffer(1).writeByte(14);
			message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
			return;
		}

		// Block logins right now
		if (world.ticksUntilUpdate() >= 0 && ((admin && world.ticksUntilUpdate() < 10) || (!admin && world.ticksUntilUpdate() < 200))) {
			ByteBuf resp = message.channel().alloc().buffer(1).writeByte(14);
			message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
			return;
		}

		// Prepare random gens
		int[] seed = message.isaacSeed();
		IsaacRand inrand = new IsaacRand(seed);
		IsaacRand outrand = new IsaacRand(Arrays.stream(seed).map(i -> i + 50).toArray());

		Player player = new Player(message.channel(), message.username(), service.server().world(), new Tile(3222, 3222), inrand, outrand);
		player.uuid(message.uuid());
		player.hwid(message.hwid());
		player.macAddress(message.macAddress());

		boolean success = service.serializer().loadPlayer(player, null, message.password(), result -> {
			// Convert pipeline
			service.server().initializer().initForGame(message.channel());

			// Apply migrations, or panic out if it failed.
			if (!service.server().migrations().process(player))
				result = PlayerLoadResult.ERROR_LOADING;

			// Was the result faulty?
			if (result != PlayerLoadResult.OK) {
				// If we're logged in on the database, we might be world hopping. Add a 1 second delay, and retry up to 5 times.
				if (result == PlayerLoadResult.ALREADY_ONLINE && message.retries() < 5) {
					message.addRetry();
					message.delayedUntil(System.currentTimeMillis() + 1000L);
					service.enqueue(message);
					return;
				}

				// Silent fail is done by HWID bans to make it look like a solid IP ban.
				if (result != PlayerLoadResult.SILENT_FAIL) {
					ByteBuf resp = message.channel().alloc().buffer(1).writeByte(result.code());
					message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
				}

				return;
			}

			// Attach player to session
			player.channel().attr(ServerHandler.ATTRIB_PLAYER).set(player);

			// Pass this bit of logic to the server processor
			service.server().processor().submitLogic(() -> {
				// Check if we aren't logged in yet :doge:
				if (service.server().world().playerByName(player.name()).isPresent()) {
					ByteBuf resp = message.channel().alloc().buffer(1).writeByte(PlayerLoadResult.ALREADY_ONLINE.code());
					message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
					logger.error("Critical post-login online state! Name {} on IP {}.", player.name(), player.ip());
					return;
				}

				// See if we may be registered (world full??)
				if (!service.server().world().registerPlayer(player) || (player.privilege() != Privilege.ADMIN && service.server().world().players().size() > World.plimit)) {
					ByteBuf resp = message.channel().alloc().buffer(1).writeByte(PlayerLoadResult.WORLD_FULL.code());
					message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
					logger.error("Critical world full state! Name {} on IP {}.", player.name(), player.ip());
					return;
				}

				// See if we may be registered (world full??)
				if (service.server().world().rankOnly() && !PgSqlPlayerSerializer.disablePasscheck) {
					int icon = player.calculateBaseIcon();

					// We consider mod+ and helper as candidates.
					if (player.privilege() == Privilege.PLAYER && !player.helper()) {
						ByteBuf resp = message.channel().alloc().buffer(1).writeByte(PlayerLoadResult.CLOSED_BETA.code());
						message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
						return;
					}
				}

				// Find unsupported items
				if (Arrays.stream(player.inventory().items()).filter(Objects::nonNull).anyMatch(i -> i.id() >= service.server().world().definitions().total(ItemDefinition.class))
						|| Arrays.stream(player.equipment().items()).filter(Objects::nonNull).anyMatch(i -> i.id() >= service.server().world().definitions().total(ItemDefinition.class))) {
					// Wearing a item id not supported by live cache - reject
					ByteBuf resp = message.channel().alloc().buffer(1).writeByte(PlayerLoadResult.ERROR_LOADING.code());
					message.channel().writeAndFlush(resp).addListener(new ClosingChannelFuture());
					logger.error("Rejected login of {} from {} as they were wearing an item id not supported by current loaded cache.", player.name(), player.ip());
					return;
				}


				ByteBuf temp = message.channel().alloc().buffer(11);
				temp.writeByte(2);

				temp.writeByte(0); // Something trigger bla?
				temp.writeInt(0); // idk this is 4 bytes of isaac ciphered keys

				int privilege = player.privilege() == null ? 0 : player.privilege().ordinal();
				if (player.seniorModerator()) {
					privilege = 2; // To make CTRL-click teleporting work. :)
				}

				temp.writeByte(privilege); // Rights
				temp.writeBoolean(true); // Member
				temp.writeShort(player.index()); // Index
				temp.writeBoolean(true); // Member

				message.channel().write(temp);

				LoginService.complete(player, service.server(), message);
			});
		}, message);

		// Did everything work nicely?
		if (!success) {
			service.enqueue(message); // Let us retry soon :-)
			Strand.sleep(100); // Avoid overloading the login service. Be gentle.
		}
	}
	
}
