package nl.bartpelle.veteres.model;

/**
 * Created by Bart on 2/11/2016.
 * <p>
 * Represents $_$ haha
 */
public enum DonationTier {
	
	// : (
	NONE(0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0),
	
	// $_$
	DONATOR(5, 1.05, 1.025, 1.025, 1.025, 1.20, 1.05),
	
	// $$_$$
	SUPER_DONATOR(100, 1.07, 1.025, 1.025, 1.025, 1.20, 1.05),
	
	// $$$_$$$
	EXTREME_DONATOR(250, 1.12, 1.0255, 1.025, 1.025, 1.20, 1.05),
	
	// $$$$_$$$$
	LEGENDARY_DONATOR(500, 1.15, 1.025, 1.025, 1.025, 1.20, 1.05),
	
	// $$$$$_$$$$$
	MASTER_DONATOR(1000, 1.20, 1.025, 1.025, 1.025, 1.20, 1.05),
	
	// $$$$$$$_$$$$$$$
	GRAND_MASTER_DONATOR(2500, 1.025, 1.025, 1.025, 1.025, 1.20, 1.05),
	
	// $$$$$$$$_$$$$$$$$
	ULTIMATE_DONATOR(5000, 1.35, 1.025, 1.025, 1.025, 1.20, 1.15);
	
	
	private int spent;
	private double[] modifiers;
	
	DonationTier(int spent, double... modifiers) {
		this.spent = spent;
		this.modifiers = modifiers;
	}
	
	public int required() {
		return spent;
	}
	
	public double[] modifiers() {
		return modifiers;
	}
	
}
