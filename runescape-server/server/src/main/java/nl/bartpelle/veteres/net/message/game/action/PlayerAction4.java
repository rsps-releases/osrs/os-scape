package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.mechanics.PlayerInteraction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

import java.lang.ref.WeakReference;

/**
 * Created by Jak on 18/10/2016.
 */
@PacketInfo(size = 3)
public class PlayerAction4 implements Action {
	
	private int index, opcode, size;
	private boolean run;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		index = buf.readULEShortA();
		run = buf.readByte() == 1;
		
		this.opcode = opcode;
		this.size = size;
	}
	
	@Override
	public void process(Player player) {
		player.stopActions(true);
		
		Player other = player.world().players().get(index);
		if (other == null) {
			player.message("Unable to find player.");
		} else {
			log(player, opcode, size, "id=%d", (Integer) other.id());
			player.debug("Click 4 db-id=%d pid=%d  dead: %s %s  attackable=%s",
					other.id(), index, other.dead() ? "Y" : "N", player.dead() ? "Y" : "N",
					player.attribOr(AttributeKey.DEBUG, false) ? player.attribOr(AttributeKey.ATTACK_OP, false) ? PlayerCombat.canAttack(player, other) : true : "?");
			
			if (player.locked() || player.dead()) {
				return;
			}
			player.face(other);
			if (!other.dead()) {
				player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(other));
				player.putattrib(AttributeKey.INTERACTION_OPTION, 4);
				player.world().server().scriptExecutor().executeLater(player, new PlayerInteraction(4));
			}
		}
	}
	
}
