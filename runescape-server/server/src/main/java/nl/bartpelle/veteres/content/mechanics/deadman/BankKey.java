package nl.bartpelle.veteres.content.mechanics.deadman;

import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;

/**
 * Created by Jak on 04/11/2016.
 * Links an Item Container and a Value.
 * By saving the value we will be less stressful on the server having to re-evaluate lots of items repeatedly.
 */
public class BankKey {
	
	public World world;
	public ItemContainer lootcontainer;
	public long value;
	public long lastGametickValueUpdate;
	
	public BankKey(World world) {
		this.world = world;
		lootcontainer = new ItemContainer(world, 10, ItemContainer.Type.FULL_STACKING);
	}
	
	public BankKey(World world, ItemContainer contents, long value) {
		this.world = world;
		this.lootcontainer = contents;
		this.value = value;
	}
	
	public BankKey(ItemContainer contents) {
		this.lootcontainer = contents;
		for (Item item : lootcontainer.items()) {
			if (item == null) continue;
			ItemDefinition def = item.definition(lootcontainer.world());
			if (def == null) continue;
			value += item.realPrice(world) * item.amount();
		}
	}
	
	public BankKey copy() {
		return new BankKey(world, lootcontainer, value);
	}
}
