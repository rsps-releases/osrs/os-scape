package nl.bartpelle.veteres.services.pvp;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.services.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Created by Bart on 1/5/2016.
 * Mack - code modifications.
 */
public class FarmingPrevention implements Service {
	
	private static final Logger logger = LogManager.getLogger(GameServer.class);
	
	// Cache holding kills for 5 minutes until they expire.
	private Cache<IPKill, Long> recentKills = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).build();
	
	public void log(Player killer, Player killed) {
		recentKills.put(new IPKill(killer, killed), System.currentTimeMillis());
	}
	
	public boolean prevents(Player killer, Player killed) {
		if (killer.ip().equals(killed.ip())/* || killer.macAddress().equals(killed.macAddress())*/)
			return true;
		
		recentKills.cleanUp();
		return recentKills.getIfPresent(new IPKill(killer, killed)) != null;
	}
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		logger.info("Farming prevention is active and ready to intervene."); // I like to pretend I am the terminator
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
	static class IPKill {
		public String killerIp;
		public String killedIp;
		public String killedrMac;
		public String killedMac;
		
		public IPKill(Player killer, Player killed) {
			killerIp = killer.ip();
			killedIp = killed.ip();
			killedrMac = killer.macAddress();
			killedMac = killed.macAddress();
		}
		
		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			IPKill ipKill = (IPKill) o;
			return Objects.equals(killerIp, ipKill.killerIp) &&
					Objects.equals(killedIp, ipKill.killedIp);// &&
					//Objects.equals(killedrMac, ipKill.killedrMac) &&
					//Objects.equals(killedMac, ipKill.killedMac);
		}
		
		@Override
		public int hashCode() {
			return Objects.hash(killerIp, killedIp, killedrMac, killedMac);
		}
		
	}
	
}
