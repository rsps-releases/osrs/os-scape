package nl.bartpelle.veteres.services.intercom.clanchat;

import com.google.gson.Gson;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.net.message.game.command.AddClanChatMessage;
import nl.bartpelle.veteres.services.intercom.ClanChatService;
import nl.bartpelle.veteres.services.redis.RedisService;
import nl.bartpelle.veteres.util.JGson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import redis.clients.jedis.JedisPubSub;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class RedisClanChatService extends JedisPubSub implements ClanChatService {
	
	private static final Logger logger = LogManager.getLogger();
	
	private String clanchatChannel = "friends_chat_os";
	private String clanchatCmdChannel = "friends_chat_cmd_os";
	
	private GameServer server;
	private RedisService redisService;
	private Gson gson;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.gson = JGson.get();
		this.server = server;
		
		if (serviceConfig.hasPath("channel")) {
			clanchatChannel = serviceConfig.getString("channel");
		}
	}
	
	@Override
	public void onMessage(String channel, String message) {
		try {
			if (channel.equals(clanchatChannel)) {
				Optional<FriendsChatMessage> pm = safeFromJson(message, FriendsChatMessage.class);
				if (pm.isPresent()) {
					FriendsChatMessage msg = pm.get();
					ClanChat chat = server.world().chats().get(msg.chatid);
					if (chat != null && !chat.members().isEmpty()) {
						for (Friend friend : chat.members()) {
							server.world().playerForId(friend.id).ifPresent(p -> {
								if ((!msg.shadowMuted || p.name().equalsIgnoreCase(msg.username))) {
									p.write(new AddClanChatMessage(msg.username, msg.chatname == null ? chat.name() : msg.chatname, msg.computeIcon(), msg.world, msg.message));
								}
							});
						}
					}
				}
			} else if (channel.equals(clanchatCmdChannel)) {
				Optional<ChatCmd> chatCmd = safeFromJson(message, ChatCmd.class);
				if (chatCmd.isPresent()) {
					ChatCmd cmd = chatCmd.get();
					switch (cmd.cmd) {
						case "refresh": {
							ClanChat chat = server.world().chats().get(cmd.channel);
							if (chat != null) {
								chat.refreshRanks();
							}
							break;
						}
						case "refreshusers": {
							ClanChat chat = server.world().chats().get(cmd.channel);
							if (chat != null) {
								chat.refreshOnline();
							}
							break;
						}
						case "kick": {
							ClanChat chat = server.world().chats().get(cmd.channel);
							if (chat != null) {
								chat.kick(cmd.account, true);
							}
							break;
						}
						case "kickall": {
							ClanChat chat = server.world().chats().get(cmd.channel);
							if (chat != null) {
								chat.kickall();
								server.world().chats().remove(cmd.channel);
							}
							break;
						}
						case "reloadinfo": {
							ClanChat chat = server.world().chats().get(cmd.channel);
							if (chat != null) {
								chat.updateInformation();
							}
							break;
						}
						case "reloadignores": {
							ClanChat chat = server.world().chats().get(cmd.channel);
							if (chat != null) {
								chat.updateIgnores();
							}
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void broadcast(Player player, ClanChat chat, String message, String nameOverride) {
		FriendsChatMessage msg = new FriendsChatMessage();
		msg.chatid = chat.ownerId();
		msg.icon = player.calculateBaseIcon();
		msg.uid = "";
		msg.message = message;
		msg.username = player.name();
		msg.world = player.world().id();
		msg.chatname = nameOverride;
		msg.shadowMuted = player.shadowMuted();
		redisService.doOnPool(j -> j.publish(clanchatChannel, toJson(msg)));
	}
	
	@Override
	public void command(int chat, String command, int acc) {
		redisService.doOnPool(j -> j.publish(clanchatCmdChannel, toJson(new ChatCmd(command, chat, acc))));
	}
	
	private <T> Optional<T> safeFromJson(String json, Class<T> typeOf) {
		try {
			return Optional.of(gson.fromJson(json, typeOf));
		} catch (Throwable t) {
			return Optional.empty();
		}
	}
	
	private String toJson(Object o) {
		return gson.toJson(o);
	}
	
	@Override
	public boolean start() {
		Optional<RedisService> service = server.service(RedisService.class, false);
		if (!service.isPresent()) {
			logger.error("Cannot use Redis Clan Chat service if the Redis service is disabled");
			return false;
		}
		
		redisService = service.get();
		
		if (redisService.isAlive()) {
			ExecutorService executor = Executors.newFixedThreadPool(2);
			executor.submit(() -> redisService.doOnPool(j -> j.subscribe(this, clanchatChannel)));
			executor.submit(() -> redisService.doOnPool(j -> j.subscribe(this, clanchatCmdChannel)));
			
			logger.info("Redis Clan Chat provider ready to dispatch and receive messages.");
		} else {
			logger.error("Cannot use Redis Clan Chat service if the Redis service failed to start");
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
	
	public static class FriendsChatMessage {
		public String uid;
		public int chatid;
		public String message;
		public int icon;
		public String username;
		public String chatname;
		public int ironmode;
		public int world;
		public boolean shadowMuted;
		
		public int computeIcon() {
			return icon;
		}
	}
	
	public static class ChatCmd {
		public String cmd;
		public int channel;
		public int account;
		
		public ChatCmd(String cmd, int channel, int account) {
			this.cmd = cmd;
			this.channel = channel;
			this.account = account;
		}
	}
	
}
