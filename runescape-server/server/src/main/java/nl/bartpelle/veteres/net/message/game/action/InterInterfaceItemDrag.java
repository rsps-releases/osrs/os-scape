package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.interfaces.Bank;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.util.Varbit;

/**
 * Created by Bart on 8/11/2015.
 */
@PacketInfo(size = 16)
public class InterInterfaceItemDrag implements Action {
	
	private int fromItem;
	private int toItem;
	private int fromHash;
	private int toHash;
	private int fromSlot;
	private int toSlot;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		fromSlot = buf.readUShortA();
		fromItem = buf.readULEShortA();
		toHash = buf.readIntV1();
		toItem = buf.readULEShort();
		fromHash = buf.readIntV2();
		toSlot = buf.readUShort();
		
		log(player, opcode, size, "from=[%d:%d] to=[%d:%d] fromitem=%d toitem=%d fromslot=%d toslot=%d", fromHash >> 16, fromHash & 0xFFFF, toHash >> 16, toHash & 0xFFFF, fromItem, toItem, fromSlot, toSlot);
	}
	
	@Override
	public void process(Player player) {
		if (player.privilege().eligibleTo(Privilege.ADMIN) && player.<Boolean>attribOr(AttributeKey.DEBUG, false))
			player.message("GlobalDrag: [%d:%d]=>[%d:%d] item: [%d=>%d] slot: [%d=>%d]", fromHash >> 16, fromHash & 0xFFFF, toHash >> 16, toHash & 0xFFFF, fromItem, toItem, fromSlot, toSlot);
		
		int fromInterface = fromHash >> 16;
		int toInterface = toHash >> 16;
		int fromChild = fromHash & 0xFFFF;
		int toChild = toHash & 0xFFFF;
		
		if (fromInterface == 15 && toInterface == 15 && fromChild == 3 && toChild == 3) {
			player.inventory().swap(fromSlot, toSlot);
		} else if (fromInterface == 12 && toInterface == 12 && fromChild == 23 && toChild == 23) { // Moving on bank interface
			if (fromSlot >= player.bank().items().length) {
				return;
			}
			
			Item from = player.bank().get(fromSlot);
			if (from == null) {
				return;
			}
			
			if (toSlot >= player.bank().items().length) {
				int toTab = toSlot - 818;
				int fromTab = Bank.findInTab(player, from);
				
				if (fromTab != -1)
					Bank.deposit(from.amount(), player, fromSlot, from, player.bank(), toTab, fromTab);
			} else {
				boolean insert = player.varps().varbit(Varbit.BANK_INSERT) != 0;
				
				Item to = player.bank().get(toSlot);
				
				if (insert && to != null) {
					// Determine target tab index
					int start = 0;
					int targetTab = 0;
					for (int i = 1; i <= 10; i++) {
						int ts = Bank.tabStart(player, i - 1);
						
						if (toSlot < ts) {
							targetTab = i - 1;
							break;
						}
						
						start = ts;
					}
					
					player.bank().set(fromSlot, null);
					
					ItemContainer[] tabs = Bank.buildTabs(player);
					ItemContainer tab = tabs[targetTab];
					int tabslot = toSlot - start;
					
					// Cut the tab open, insert the item, close the tab!
					Item[] clone = new Item[tab.size() + 2];
					System.arraycopy(tab.items(), 0, clone, 0, tabslot);
					clone[tabslot] = from;
					System.arraycopy(tab.items(), tabslot, clone, tabslot + 1, tab.size() - tabslot - 2);
					tabs[targetTab].items(clone);
					
					Bank.meltTabs(player, tabs);
					player.bank().makeDirty();
				} else {
					player.bank().set(toSlot, from);
					player.bank().set(fromSlot, to);
				}
			}
		} else if (fromInterface == 12 && fromChild == 23 && toInterface == 12 && toChild == 21) { // Drag an item to a tab
			Bank.dragToSlot(player, fromSlot, toSlot);
		} else if (fromInterface == 85 && toInterface == 85 && toChild == 0 && fromChild == 0) {
			player.inventory().swap(fromSlot, toSlot);
		} else if (fromInterface == 192 && toInterface == 192 && fromChild == 2 && toChild == 2) {
			player.inventory().swap(fromSlot, toSlot);
		}
	}
	
}
