package nl.bartpelle.veteres.model;

/**
 * A class to store all boosts donators can receive. These arbitrary values are defined in {@link DonationTier}.
 * Values defined must be in respective order to {@link DonatorBoost} ordinal.
 * @author Heaven
 */
public enum DonatorBoost {
	SIGMUND,
	PET,
	BLOODY_KEYS,
	BLOOD_MONEY,
	BLOODY_VOLCANO,
	SLAYER_PTS
}
