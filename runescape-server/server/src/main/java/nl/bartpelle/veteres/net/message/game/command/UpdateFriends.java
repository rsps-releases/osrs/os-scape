package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart on 11/14/2015.
 */
public class UpdateFriends extends Command {
	
	private Friend[] infoList;
	
	public UpdateFriends(Friend[] info) {
		this.infoList = info;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(128)); // Estimated. Might need some data collection. TODO.
		
		buffer.packet(71).writeSize(RSBuffer.SizeType.SHORT);
		
		for (Friend info : infoList) {
			buffer.writeByte(0); // Boolean. Unknown. Silent?
			buffer.writeString(info.name);
			buffer.writeString(info.prevname);
			buffer.writeShort(info.world);
			buffer.writeByte(info.clanrank); // Icon
			buffer.writeByte(0); // Flags. Investigate thanks
			
			if (info.world > 0) { // Waste code, not used.
				buffer.writeString("");
				buffer.writeByte(0);
				buffer.writeInt(0);
			}
			
			buffer.writeString(""); // Waste
		}
		
		return buffer;
	}
	
}
