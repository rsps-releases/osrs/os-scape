package nl.bartpelle.veteres.migration.impl;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;

/**
 * Created by Jak on 21/05/2017.
 */
public class ConvertDiceBagId_15 implements Migration {
	@Override
	public int id() {
		return 15;
	}
	
	@Override
	public boolean apply(Player player) {
		long removed = removeAll(player, new Item(12020, Integer.MAX_VALUE));
		
		if (removed > 0) {
			player.bank().add(new Item(6990, (int) removed), true);
			
			player.pendingActions().add(new Action() {
				@Override
				public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
				}
				
				@Override
				public void process(Player player) {
					player.message("%d %s removed and re-added to your bank <col=FF0000>due to an update</col>.", removed, removed == 1 ? "die bag was" : "dice bags were");
				}
			});
		}
		return true;
	}
}
