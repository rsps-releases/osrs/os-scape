package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;

/**
 * Created by Bart on 4/3/2016.
 * <p>
 * Im a tool
 */
public class ChangeXPPost99RealismFix_6 implements Migration {
	
	private static final int[] AFFECTED_SKILLS = {Skills.ATTACK, Skills.STRENGTH, Skills.DEFENCE, Skills.RANGED,
			Skills.MAGIC, Skills.HITPOINTS};
	private static final double XP_TO_99 = 13034431;
	
	@Override
	public int id() {
		return 6;
	}
	
	@Override
	public boolean apply(Player player) {
		if (player.world().realm().isRealism()) {
			for (int skill : AFFECTED_SKILLS) {
				if (player.skills().xp()[skill] > XP_TO_99) {
					double delta = player.skills().xp()[skill] - XP_TO_99;
					double addition = delta * 50.0;
					player.skills().xp()[skill] = XP_TO_99 + addition;
				}
			}
		}
		
		return true;
	}
	
}
