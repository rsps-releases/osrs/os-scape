package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Jak on 09/02/2016.
 */
public class DestoryItemContainer extends Command {
	
	private int containerId;
	
	public DestoryItemContainer(int target) {
		containerId = target;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(2));
		buffer.packet(89);
		
		buffer.writeShort(containerId);
		
		return null;//buffer;
	}
	
}
