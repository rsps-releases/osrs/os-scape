package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.SpellOnEntity;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.WeakReference;

/**
 * Created by Bart on 8/20/2015.
 */
@PacketInfo(size = 9)
public class SpellOnPlayer implements Action {
	
	private static final Logger logger = LogManager.getLogger(SpellOnPlayer.class);
	
	private int slot;
	private int targetIndex;
	private int interfaceId;
	private int child;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		int hash = buf.readLEInt();
		targetIndex = buf.readULEShort();
		slot = buf.readUShortA();
		boolean run = buf.readByteA() == 1;

		interfaceId = hash >> 16;
		child = hash & 0xFFFF;
	}
	
	@Override
	public void process(Player player) {
		logger.debug("Spell on player %d spell from [%d,%d] slot %d.", targetIndex, interfaceId, child, slot);
		player.debug("Spell on player %d spell from [%d,%d] slot %d.", targetIndex, interfaceId, child, slot);
		
		Player other = player.world().players().get(targetIndex);
		if (other == null) {
			player.message("Unable to find player.");
		} else {
			player.stopActions(false);
			
			if (!player.locked() && !player.dead()) {
				player.face(other);
				if (!other.dead()) {
					player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(other));
					player.putattrib(AttributeKey.INTERACTION_OPTION, 1);
					player.putattrib(AttributeKey.INTERACTED_WIDGET_INFO, new int[]{interfaceId, child});
					player.world().server().scriptExecutor().executeLater(player, SpellOnEntity.script);
				}
			}
		}
	}
	
}
