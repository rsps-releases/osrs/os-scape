package nl.bartpelle.veteres.services.worldlist;

import co.paralleluniverse.fibers.SuspendExecution;
import co.paralleluniverse.strands.Strand;
import com.typesafe.config.Config;
import io.netty.buffer.Unpooled;
import io.undertow.util.Headers;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.http.UndertowService;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bart on 10/3/2015.
 * <p>
 * Simple HTTP server that handles the world list requests. Makes use of SQL when possible to fetch player counts.
 */
public class WorldListServer implements Service {
	
	private static final Logger logger = LogManager.getLogger(WorldListServer.class);
	
	private GameServer server;
	private byte[] list;
	private Map<Integer, Integer> players = new HashMap<>();
	public Map<Integer, WorldListEntry> worlds = new HashMap<>();
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
	}
	
	@Override
	public boolean start() {
		if (!server.serviceAlive(UndertowService.class)) {
			logger.error("Cannot start the world list server without Undertow being ready.");
			return false;
		}
		
		updateList();
		
		server.service(UndertowService.class, true).ifPresent(undertow -> {
			undertow.pathHandler().add("/worldlist.ws", exchange -> {
				exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/octet-stream");
				exchange.getResponseSender().send(ByteBuffer.wrap(list));
			});
		});
		
		new Thread(() -> {
			while (true) {
				try {
					Strand.sleep(10000);
					server.service(PgSqlService.class, true).ifPresent(serv -> {
						serv.worker().submit(new SqlTransaction() {
							@Override
							public void execute(Connection connection) throws Exception {
								PreparedStatement statement = connection.prepareStatement("SELECT world_id, count(*) FROM online_characters GROUP BY world_id");
								
								ResultSet set = statement.executeQuery();
								players.clear();
								
								while (set.next()) {
									players.put(set.getInt("world_id"), set.getInt("count"));
								}
								
								connection.commit();
								updateList();
							}
						});
					});
				} catch (InterruptedException | SuspendExecution e) {
					e.printStackTrace();
				}
			}
		}).start();
		
		return true;
	}
	
	@Override
	public boolean stop() {
		return false;
	}
	
	@Override
	public boolean isAlive() {
		return false;
	}
	
	private void updateList() {
		server.service(PgSqlService.class, true).ifPresent(serv -> {
			serv.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					ResultSet set = connection.prepareStatement("SELECT * FROM worlds WHERE public = TRUE ORDER BY sort_order ASC").executeQuery();
					
					int count = 0;
					RSBuffer buffer = new RSBuffer(Unpooled.buffer());
					buffer.writeInt(0); // Placeholder for size
					buffer.writeShort(0); // Placeholder for worlds
					
					worlds.clear();
					while (set.next()) {
						count++;
						
						buffer.writeShort(set.getInt("id")); // Id
						buffer.writeInt(set.getInt("flags")); // Settings 1=member
						buffer.writeString(set.getString("ip")); // Address
						buffer.writeString(set.getString("name")); // Activity
						buffer.writeByte(0); // Flag
						buffer.writeShort((int) (players.getOrDefault(set.getInt("id"), 0) * set.getDouble("boost_multiplier"))); // Players
						
						// Put it in the map for the world switcher
						WorldListEntry entry = new WorldListEntry();
						entry.ip = set.getString("ip");
						entry.flags = set.getInt("flags");
						entry.id = set.getInt("id");
						
						worlds.put(count, entry);
					}
					
					byte[] out = new byte[buffer.get().writerIndex()];
					buffer.get().setInt(0, out.length - 4); // Set size
					buffer.get().setShort(4, count);
					buffer.get().getBytes(0, out);
					list = out;
					
					connection.commit();
				}
			});
		});
	}
	
	public static class WorldListEntry {
		public int id;
		public String ip;
		public int flags;
	}
	
}
