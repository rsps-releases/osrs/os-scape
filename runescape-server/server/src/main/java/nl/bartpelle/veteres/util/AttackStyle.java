package nl.bartpelle.veteres.util;

/**
 * @author Savions Sw, the legendary.
 */
public enum AttackStyle {
	
	ACCURATE, AGGRESIVE, CONTROLLED, DEFENSIVE, SPECIAL, RAPID, LONG_RANGE
}

