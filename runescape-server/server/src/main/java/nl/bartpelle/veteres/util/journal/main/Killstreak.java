package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class Killstreak extends JournalEntry {

    @Override
    public void send(Player player) {
        int killstreak = player.attribOr(AttributeKey.KILLSTREAK, 0);
        send(player, "<img=" + getKillstreakIcon(player) + "> Killstreak", killstreak, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int killstreak = player.attribOr(AttributeKey.KILLSTREAK, 0);
        player.sync().shout("<img=" + getKillstreakIcon(player) + "> I have a kill streak of " + killstreak + ".");
    }


    private static int getKillstreakIcon(Player player) {
        int streak = player.attribOr(AttributeKey.KILLSTREAK, 0);
        if (streak >= 10) {
            return 30;
        } else if (streak >= 8) {
            return 29;
        } else if (streak >= 6) {
            return 28;
        } else if (streak >= 4) {
            return 27;
        } else if (streak >= 2) {
            return 26;
        }
        return 26;
    }

}