package nl.bartpelle.veteres.services.grandexchange;

import co.paralleluniverse.strands.Strand;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeOffer;
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeType;
import nl.bartpelle.veteres.model.entity.player.content.exchange.GrandExchange;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Bart on 10/29/2016.
 */
public class GrandExchangeService implements Service {
	
	private static final Logger logger = LogManager.getLogger(GrandExchangeService.class);
	private static final int SYNC_INTERVAL = 1200;
	
	private GameServer server;
	private int serviceId;
	private PgSqlService sql;
	
	private List<ExchangeOffer> sellOffers = new LinkedList<>();
	private List<ExchangeOffer> buyOffers = new LinkedList<>();
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
		this.serviceId = server.serviceId();
		
		sql = server.service(PgSqlService.class, true).get();
		
		final boolean[] loaded = {false};
		sql.worker().submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) {
				try {
					PreparedStatement stmt = connection.prepareStatement("SELECT * FROM exchange_offers WHERE service_id = ? AND (pending_coins > 0 or pending_items > 0 or (completed <> requested and not cancelled));");
					stmt.setInt(1, serviceId);
					
					ResultSet set = stmt.executeQuery();
					while (set.next()) {
						ExchangeOffer o = getOffer(set);
						if (o.type() == ExchangeType.SELL)
							sellOffers.add(o);
						else
							buyOffers.add(o);
					}
					sort();
					logger.info("Loaded {} buy offers, and {} sell offers!", buyOffers.size(), sellOffers.size());
					loaded[0] = true;
					connection.close();
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(e);
				}
			}
		});
		while (!loaded[0])
			try {
				Strand.sleep(250); //Other stuff depends that this shit is FULLY loaded
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		new Thread(() -> {
			logger.info("Grand exchange worker alive on service {}.", serviceId);
			
			long lastSync = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(30); //Lets sync in 30 seconds
			
			while (true) {
				try {
					Strand.sleep(SYNC_INTERVAL); // No need for continuous execution.
					if (GrandExchange.enabled) {
						processOffers();
					}
					if (System.currentTimeMillis() > lastSync) {
						databaseFlush();
						lastSync = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(20);
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(e);
				}
			}
		}).start();
	}
	
	private void sort() {
		sortSell();
		sortBuy();
	}
	
	private void sortSell() {
		sellOffers.sort(Comparator.comparingInt(ExchangeOffer::pricePer));
	}
	
	private void sortBuy() {
		buyOffers.sort(Collections.reverseOrder(Comparator.comparingInt(ExchangeOffer::pricePer)));
	}
	
	public void addOffer(ExchangeOffer offer) {
		offer.changed(true);
		if (offer.type() == ExchangeType.SELL) {
			sellOffers.add(offer);
			sortSell();
		} else {
			buyOffers.add(offer);
			sortBuy();
		}
	}
	
	public void abortOffer(Player player, ExchangeOffer offer) {
		if (offer == null || offer.id() == -1) {
			return;
		}
		List<ExchangeOffer> offers = offer.type() == ExchangeType.SELL ? sellOffers : buyOffers;
		
		for (ExchangeOffer o : offers) {
			if (o != null && o.id() == offer.id()) {
				o.processed(true);
				o.cancel();
				offer.changed(true);
				return;
			}
		}
	}
	
	public void databaseFlush() {
		if (server.isVerbose())
			logger.info("Pushing Grand Exchange offers to the database. Active offers: {} buy, {} sell.", buyOffers.size(), sellOffers.size());
		sql.worker().submit(new SqlTransaction() {
			@Override
			public void execute(Connection c) {
				try {
					PreparedStatement update = c.prepareStatement(
							"update exchange_offers set completed = ?, spent = ?, pending_coins = ?, pending_items = ?, cancelled = ?, processed = ?" +
									" where exchange_offers.id = ? and exchange_offers.service_id = ? and exchange_offers.account_id = ?");
					if (addBatch(c, update, sellOffers)) {
						sortSell();
					}
					if (addBatch(c, update, buyOffers)) {
						sortBuy();
					}
					
					update.executeBatch();
					
					c.commit();
					c.close();
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(e);
				}
			}
		});
	}
	
	private boolean addBatch(Connection c, PreparedStatement update, List<ExchangeOffer> offers) {
		try {
			boolean removed = false;
			for (Iterator<ExchangeOffer> it = offers.iterator(); it.hasNext(); ) {
				ExchangeOffer offer = it.next();
				if (offer != null) {
					if (offer.changed()) {
						if (offer.id() == -1) {
							PreparedStatement insert = c.prepareStatement(
									"insert into exchange_offers (account_id, service_id, slot, is_sell, item_id, price_per, requested, completed, spent, pending_coins, pending_items, cancelled, processed)" +
											" values (?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id");
							insert.setInt(1, offer.owner());
							insert.setInt(2, serviceId);
							insert.setInt(3, offer.slot());
							insert.setBoolean(4, offer.type() == ExchangeType.SELL);
							insert.setInt(5, offer.item());
							insert.setInt(6, offer.pricePer());
							insert.setInt(7, offer.requested());
							insert.setInt(8, offer.completed());
							insert.setInt(9, offer.spent());//spent
							insert.setInt(10, offer.coins());
							insert.setInt(11, offer.items());
							insert.setBoolean(12, offer.cancelled());
							insert.setBoolean(13, offer.processed());
							ResultSet set = insert.executeQuery(); // Execute the insert
							set.next();
							
							long id = set.getLong("id");
							offer.id(id);
						} else {
							update.setInt(1, offer.completed());
							update.setInt(2, offer.spent());//spent
							update.setInt(3, offer.coins());
							update.setInt(4, offer.items());
							update.setBoolean(5, offer.cancelled());
							update.setBoolean(6, offer.processed());
							update.setLong(7, offer.id());
							update.setInt(8, serviceId);
							update.setInt(9, offer.owner());
							update.addBatch();
						}
						offer.setUpdated(new Timestamp(System.currentTimeMillis()));
						offer.changed(false);
					}
					
					if (offer.processed() && (offer.cancelled() || offer.finished()) && offer.coins() <= 0 && offer.items() <= 0) {
						it.remove();
						removed = true;
					}
				}
			}
			return removed;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
		return false;
	}
	
	public void processOffers() {
		for (ExchangeOffer offer : sellOffers) {
			if (offer.id() == -1 || offer.cancelled() || offer.processed()) {
				continue;
			}
			try {
				//logger.info("Processing sell offer {} for {} x {}.", offer.id(), offer.requested(), offer.item());
				processSellOffer(offer);
				offer.processed(true);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
		
		for (ExchangeOffer offer : buyOffers) {
			if (offer.id() == -1 || offer.cancelled() || offer.processed()) {
				continue;
			}
			try {
				//logger.info("Processing buy offer {} for {} x {}.", offer.id(), offer.requested(), offer.item());
				processBuyOffer(offer);
				offer.processed(true);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
	}
	
	private void processBuyOffer(final ExchangeOffer buyOffer) throws Exception {
		for (ExchangeOffer offer : sellOffers) {
			if (offer.id() == -1 ||
					offer.item() != buyOffer.item() ||
					offer.pricePer() > buyOffer.pricePer() ||
					offer.cancelled() ||
					!offer.processed() ||
					offer.completed() == offer.requested()) {
				continue;
			}
			try {
				if (buyOffer.id() == -1 || (!server.isDevServer() && offer.owner() == buyOffer.owner())) {
					continue;
				}
				int availableInOffer = offer.requested() - offer.completed();
				int numExchangable = Math.min(buyOffer.remaining(), availableInOffer);
				
				logger.info("Found a seller, ready to take {}, buyerid={}, sellerid={}.", numExchangable, buyOffer.owner(), offer.owner());
				
				// Update our offer (just the variable, actual update happens later)
				buyOffer.completed(buyOffer.completed() + numExchangable);
				buyOffer.items(buyOffer.items() + numExchangable);
				
				// Compute how much we overpaid, and refund that to the buyer.
				int margin = buyOffer.pricePer() - offer.pricePer();
				int coinRefund = margin * numExchangable;
				if (buyOffer.getCreated().getTime() >= offer.getCreated().getTime()) {
					buyOffer.coins(buyOffer.coins() + coinRefund);
					buyOffer.spent(buyOffer.spent() + (((numExchangable * buyOffer.pricePer()) - coinRefund)));
				} else {
					offer.coins(offer.coins() + coinRefund);
					offer.spent(offer.coins());
				}
				
				// Update their offer
				offer.completed(offer.completed() + numExchangable);
				offer.coins(offer.coins() + (numExchangable * offer.pricePer()));
				offer.spent(offer.coins());
				processNotify(offer);
				
				server.world().playerForId(buyOffer.owner()).ifPresent(player -> {
					player.message("One or more of your grand exchange buy offers has been updated!");
					player.grandExchange().updateOffer(buyOffer);
				});
				
				server.service(LoggingService.class, true).ifPresent(logging -> {
					logging.logExchange(buyOffer.owner(), offer.owner(), buyOffer.item(), numExchangable, buyOffer.pricePer(), server.serviceId());
				});
				
				// Should we stop with this offer?
				if (buyOffer.completed() == buyOffer.requested()) {
					logger.info("Offer has been completed!");
					server.service(GrandExchangePriceService.class, true).ifPresent(ge -> ge.addOffer(buyOffer));
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
	}
	
	private void processSellOffer(final ExchangeOffer sellOffer) throws Exception {
		for (ExchangeOffer offer : buyOffers) {
			if (offer.id() == -1 ||
					offer.item() != sellOffer.item() ||
					offer.pricePer() < sellOffer.pricePer() ||
					offer.cancelled() ||
					!offer.processed() ||
					offer.completed() == offer.requested()) {
				continue;
			}
			try {
				if (sellOffer.id() == -1 || (!server.isDevServer() && offer.owner() == sellOffer.owner())) {
					continue;
				}
				int availableInOffer = offer.requested() - offer.completed();
				int numExchangable = Math.min(sellOffer.remaining(), availableInOffer);
				
				logger.info("Found a buyer, ready to supply {}, buyerid={}, sellerid={}.", numExchangable, offer.owner(), sellOffer.owner());
				
				// Update our offer (just the variable, actual update happens later)
				sellOffer.completed(sellOffer.completed() + numExchangable);
				sellOffer.coins(sellOffer.coins() + (numExchangable * offer.pricePer()));
				sellOffer.spent(sellOffer.coins());
				
				// Update their offer
				offer.completed(offer.completed() + numExchangable);
				offer.items(offer.items() + numExchangable);
				offer.spent(offer.spent() + (numExchangable * offer.pricePer()));
				processNotify(offer);
				
				server.world().playerForId(sellOffer.owner()).ifPresent(player -> {
					player.message("One or more of your grand exchange sell offers has been updated!");
					player.grandExchange().updateOffer(sellOffer);
				});
				
				server.service(LoggingService.class, true).ifPresent(logging -> {
					logging.logExchange(offer.owner(), sellOffer.owner(), sellOffer.item(), numExchangable, sellOffer.pricePer(), server.serviceId());
				});
				
				// Should we stop with this offer?
				if (sellOffer.completed() == sellOffer.requested()) {
					logger.info("Offer has been completed!");
					server.service(GrandExchangePriceService.class, true).ifPresent(ge -> ge.addOffer(sellOffer));
					break;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
			}
		}
	}
	
	public ExchangeOffer[] offersByPlayer(Player player) {
		ExchangeOffer[] offerList = new ExchangeOffer[8];
		for (ExchangeOffer offer : sellOffers) {
			if (offer != null && offer.owner() == (int) player.id()) {
				if (offerList[offer.slot()] != null) {
					player.message("Bit of an issue here pal. Send this to staff [" + offer + "].");
				}
				offerList[offer.slot()] = offer;
			}
		}
		
		for (ExchangeOffer offer : buyOffers) {
			if (offer != null && offer.owner() == (int) player.id()) {
				if (offerList[offer.slot()] != null) {
					player.message("Bit of an issue here pal. Send this to staff [" + offer + "].");
				}
				offerList[offer.slot()] = offer;
			}
		}
		return offerList;
	}
	
	private void processNotify(ExchangeOffer offer) throws Exception {
		server.world().playerForId(offer.owner()).ifPresent(player -> {
			player.message("One or more of your grand exchange " + (offer.type() == ExchangeType.SELL ? "sell" : "buy") + " offers has been updated!");
			player.grandExchange().updateOffer(offer);
		});
	}
	
	private ExchangeOffer getOffer(ResultSet set) throws SQLException {
		ExchangeType type = set.getBoolean("is_sell") ? ExchangeType.SELL : ExchangeType.BUY;
		long id = set.getLong("id");
		int owner = set.getInt("account_id");
		int slot = set.getInt("slot");
		int itemId = set.getInt("item_id");
		int spent = set.getInt("spent");
		int requested = set.getInt("requested");
		int completed = set.getInt("completed");
		int pricePer = set.getInt("price_per");
		int pendingCoins = set.getInt("pending_coins");
		int pendingItems = set.getInt("pending_items");
		boolean cancelled = set.getBoolean("cancelled");
		boolean processed = set.getBoolean("processed");
		Timestamp created = set.getTimestamp("created_at");
		Timestamp updated = set.getTimestamp("updated_at");
		
		return new ExchangeOffer(id, owner, slot, type, pricePer, itemId, requested, completed, cancelled,
				pendingCoins, spent, pendingItems, server.world(), processed, created, updated);
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
}
