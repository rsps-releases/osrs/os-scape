package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart on 8/22/2015.
 */
public class RemoveGroundItem extends Command {
	
	private GroundItem item;
	
	public RemoveGroundItem(GroundItem item) {
		this.item = item;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer packet = new RSBuffer(player.channel().alloc().buffer(6)).packet(75);

		packet.writeLEShortA(item.item().id());
		packet.writeByteA(((item.tile().x % 8) << 4) | (item.tile().z % 8));
		
		return packet;
	}
	
}

