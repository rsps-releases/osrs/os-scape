package nl.bartpelle.veteres.util;

/**
 * Created by Bart on 4/5/2016.
 */
public class ItemSpawn {
	
	public int id;
	public int amount = 1;
	public int x;
	public int z;
	public int level;
	public int delay = 100;
	public boolean PVPWorldExclusive = false;
	
}
