package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.Censor;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.ChatMessage;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.util.HuffmanCodec;
import nl.bartpelle.veteres.util.L10n;

import java.util.Optional;

/**
 * Created by Bart Pelle on 8/23/2014.
 */
@PacketInfo(size = -1)
public class PublicChat implements Action {
	
	private static final int TYPE_NORMAL = 0;
	private static final int TYPE_AUTOCHAT = 1;
	private static final int TYPE_CLANCHAT = 2;
	
	private int effect;
	private int color;
	private int len;
	private byte[] data;
	private int type;
	
	@Override
	public void process(Player player) {
		// Don't allow muted people to speak. They are baddies.
		// If they shadow muted they get special treatment :D
		if (!player.shadowMuted() && player.muted()) {
			player.message("You're currently muted and cannot talk.");
			return;
		}
		
		// Decode huffman data
		byte[] stringData = new byte[256];
		HuffmanCodec codec = player.world().server().huffman();
		codec.decode(data, stringData, 0, 0, len);
		String message = new String(stringData, 0, len);
		
		if (type == TYPE_CLANCHAT) {
			// Resolve our clan and print if none
			Optional<ClanChat> chat = ClanChat.current(player);
			if (!chat.isPresent()) {
				player.message("You are currently not in a clan chat channel.");
				return;
			}
			
			String line = L10n.formatChatMessage(message.substring(1));
			chat.get().chat(player, line);
			return;
		}

		int icon = player.calculateBaseIcon();
		
		ChatMessage chatMessage = new ChatMessage(message, effect, color, icon, type == TYPE_AUTOCHAT);
		player.sync().publicChatDampered = Censor.dampered(player, message);
		chatMessage.starred(Censor.starred(message));
		player.sync().publicChatMessage(chatMessage);
		
		// Log it
		if (type != TYPE_AUTOCHAT && !player.shadowMuted() && !player.bot()) {
			player.world().server().service(LoggingService.class, true).ifPresent(s -> {
				s.logPublicChat((Integer) player.id(), player.world().id(), player.tile().x, player.tile().z, player.tile().level, message);
			});
		}
	}
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		type = buf.readByte();
		color = buf.readByte();
		effect = buf.readByte();
		
		len = buf.readCompact();
		data = new byte[buf.get().readableBytes()];
		buf.get().readBytes(data);
	}
	
}
