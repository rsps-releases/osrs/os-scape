package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.skript.WaitReason;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Jak on 22/05/2017.
 * A left over from suuuper old 317-style interfaces. This packet is for the buttons
 * on the ranging guild shop interface. Gets a packet all to itself!
 */
@PacketInfo(size = 8)
public class UniqueButton2 implements Action {
	
	private int hash, slot, item;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		item = buf.readShort();
		slot = buf.readULEShortA();
		hash = buf.readLEInt();
		if (item == 0xFFFF)
			item = -1;
		if (slot == 0xFFFF)
			slot = -1;
		log(player, opcode, size, "inter=%d child=%d slot=%d item=%d", hash >> 16, hash & 0xFFFF, slot, item);
	}
	
	@Override
	public void process(Player player) {
		if (player.privilege().eligibleTo(Privilege.ADMIN) && player.<Boolean>attribOr(AttributeKey.DEBUG, false))
			player.message("Unique button on [%d:%d], item: %d, slot: %d", hash >> 16, hash & 0xFFFF, item, slot);
		
		if (player.interfaces().visible(hash >> 16)) {
			if (hash >> 16 == 382) {
				player.world().server().scriptExecutor().continueFor(player, WaitReason.DIALOGUE, hash & 0xFFFF);
			} else {
				player.world().server().scriptRepository().triggerButton(player, hash >> 16, hash & 0xFFFF, slot, 200, item);
			}
		} else {
			player.debug("Widget <col=FF0000>not visible</col> - %d,%d%n", hash >> 16, hash & 0xffff);
		}
	}
}
