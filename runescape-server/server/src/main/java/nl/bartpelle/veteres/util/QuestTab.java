package nl.bartpelle.veteres.util;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Hans;
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.mechanics.EloRating;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent;
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Interfaces;
import nl.bartpelle.veteres.net.message.game.Command;
import nl.bartpelle.veteres.net.message.game.command.InterfaceText;
import nl.bartpelle.veteres.script.TimerKey;

/**
 * Created by Situations on 12/29/2015.
 */

public class QuestTab {
	/**
	 custom mapping:
	 base interface 910: 399 | 4
	 911 | 399|7
	 912 = 399|8
	 913 = 399|9
	 914 = 399|10
	 *
	 399|4 0-8 , , , , , , , ,
[F2P]	 399|7 0-20 Black Knights' Fortress, Cook's Assistant, Demon Slayer, Doric's Quest, Dragon Slayer, Ernest the Chicken, Goblin Diplomacy, Imp Catcher, The Knight's Sword, Pirate's Treasure, Prince Ali Rescue, The Restless Ghost, Romeo & Juliet, Rune Mysteries, Sheep Shearer, Shield of Arrav, Vampire Slayer, Witch's Potion, Misthalin Mystery, <col=eb981f>Server Related,
[P2P]	 399|8 0-116 Animal Magnetism, Between a Rock..., Big Chompy Bird Hunting, Biohazard, Cabin Fever, Clock Tower, Contact!, Zogre Flesh Eaters, Creature of Fenkenstrain, Darkness of Hallowvale, Death to the Dorgeshuun, Death Plateau, Desert Treasure, Devious Minds, The Dig Site, Druidic Ritual, Dwarf Cannon, Eadgar's Ruse, Eagles' Peak, Elemental Workshop I, Elemental Workshop II, Enakhra's Lament, Enlightened Journey, The Eyes of Glouphrie, Fairytale I - Growing Pains, Fairytale II - Cure a Queen, Family Crest, The Feud, Fight Arena, Fishing Contest, Forgettable Tale..., The Fremennik Trials, Waterfall Quest, Garden of Tranquillity, Gertrude's Cat, Ghosts Ahoy, The Giant Dwarf, The Golem, The Grand Tree, The Hand in the Sand, Haunted Mine, Hazeel Cult, Heroes' Quest, Holy Grail, Horror from the Deep, Icthlarin's Little Helper, In Aid of the Myreque, In Search of the Myreque, Jungle Potion, Legends' Quest, Lost City, The Lost Tribe, Lunar Diplomacy, Making History, Merlin's Crystal, Monkey Madness I, Monk's Friend, Mountain Daughter, Mourning's Ends Part I, Mourning's Ends Part II, Murder Mystery, My Arm's Big Adventure, Nature Spirit, Observatory Quest, One Small Favour, Plague City, Priest in Peril, Rag and Bone Man, Ratcatchers, Recipe for Disaster, Recruitment Drive, Regicide, Roving Elves, Royal Trouble, Rum Deal, Scorpion Catcher, Sea Slug, The Slug Menace, Shades of Mort'ton, Shadow of the Storm, Sheep Herder, Shilo Village, A Soul's Bane, Spirits of the Elid, Swan Song, Tai Bwo Wannai Trio, A Tail of Two Cats, Tears of Guthix, Temple of Ikov, Throne of Miscellania, The Tourist Trap, Witch's House, Tree Gnome Village, Tribal Totem, Troll Romance, Troll Stronghold, Underground Pass, Wanted!, Watchtower, Cold War, The Fremennik Isles, Tower of Life, The Great Brain Robbery, What Lies Below, Olaf's Quest, Another Slice of H.A.M., Dream Mentor, Grim Tales, King's Ransom, Monkey Madness II, Client of Kourend, Rag and Bone Man II, Bone Voyage, The Queen of Thieves, The Depths of Despair, <col=eb981f>Game Actions,
[MINI Q] 399|9 0-13 Enter the Abyss, Architectural Alliance, Bear your Soul, Alfred Grimhand's Barcrawl, Curse of the Empty Lord, Enchanted Key, The General's Shadow, Skippy and the Mogres, The Mage Arena, Lair of Tarn Razorlor, Family Pest, The Mage Arena II, Miniquests,
	 399|10 0-6 , , , , , ,
	 */

	public static final int F2P = 911, P2P = 912, MINIQ = 913;

	public static final InterfaceText TITLE_STATS = new InterfaceText(Interfaces.QUEST_TAB, 0, "Statistics").markReusable();
	public static final InterfaceText TITLE_PLAYER_JOURNAL = new InterfaceText(Interfaces.QUEST_TAB, 0, "Statistics").markReusable();

	public static final InterfaceText TITLE_SERVER_RELATED = new InterfaceText(F2P, 19, "<col=eb981f> Server Related").markReusable();
	public static final InterfaceText GAME_ACTIONS = new InterfaceText(P2P, 115, "<col=eb981f> Game Actions").markReusable();
	public static final InterfaceText HYBRID_SETUP = new InterfaceText(P2P, 0, "<col=D37E2A><img=18> Hybrid Setup").markReusable();
	public static final InterfaceText MELEE_SETUP = new InterfaceText(P2P, 105, "<col=D37E2A><img=21> Melee Setup").markReusable();
	public static final InterfaceText PURE_SETUP = new InterfaceText(P2P, 1, "<col=D37E2A><img=20> Pure NH Setup").markReusable();
	public static final InterfaceText ZERKER_SETUP = new InterfaceText(P2P, 2, "<col=D37E2A><img=19> Zerker Setup").markReusable();

	public static final InterfaceText DELETE_4 = new InterfaceText(F2P, 4, "").markReusable();
	public static final InterfaceText DELETE_112 = new InterfaceText(P2P, 112, "").markReusable();
	public static final InterfaceText DELETE_7 = new InterfaceText(F2P, 7, "").markReusable();//<DELETE>
	public static final InterfaceText TASK_UNASSIGNED = new InterfaceText(F2P, 13, "<col=D37E2A><img=39> Task:<col=00FF00> Unassigned").markReusable();
	
	public static final InterfaceText BLOCK_RUSH_ON = new InterfaceText(F2P, 15, "<col=D37E2A><img=15> Block gmaul rushing: <col=FF0000>Off").markReusable();
	public static final InterfaceText BLOCK_RUSH_OFF = new InterfaceText(F2P, 15, "<col=D37E2A><img=15> Block gmaul rushing: <col=00FF00>On").markReusable();
	public static final InterfaceText SPEC_TELE_ON = new InterfaceText(F2P, 16, "<col=D37E2A><img=15> Block spec and tele: <col=FF0000>Off").markReusable();
	public static final InterfaceText SPEC_TELE_OFF = new InterfaceText(F2P, 16,  "<col=D37E2A><img=15> Block spec and tele: <col=00FF00>On").markReusable();
	public static final InterfaceText PRAYER_POS_NEW = new InterfaceText(F2P, 17, "<col=D37E2A><img=25> Prayer positions: <col=00FF00>New").markReusable();
	public static final InterfaceText PRAYER_POS_OLD = new InterfaceText(F2P, 17, "<col=D37E2A><img=25> Prayer positions: <col=00FF00>Old").markReusable();
	
	public static InterfaceText UPTIME, PLAYERS_ONLINE, PLAYERS_PKING;
	
	private int ticksUntil = 0; //Run every 5 ticks
	
	private ObjectList<Command> commands = new ObjectArrayList<>();
	
	public void update(Player player) {
		int deaths = player.varps().varp(Varp.DEATHS);
		int kills = player.varps().varp(Varp.KILLS);
		int killstreakTop = player.attribOr(AttributeKey.KILLSTREAK_RECORD, 0);
		int shutdownTop = player.attribOr(AttributeKey.SHUTDOWN_RECORD, 0);
		int votingPoints = player.attribOr(AttributeKey.VOTING_POINTS, 0);
		int eloRating = player.attribOr(AttributeKey.ELO_RATING, EloRating.DEFAULT_ELO_RATING);
		
		if (ticksUntil-- > 0) return;
		
		if (UPTIME == null) {
			UPTIME = new InterfaceText(F2P, 0, "<col=D37E2A><img=24> Uptime: <col=00FF00>" + player.world().getTime(true));
			PLAYERS_ONLINE = new InterfaceText(F2P, 2, "<col=D37E2A><img=12> Players Online: <col=00FF00>" + Misc.playerCount(player.world()));
			PLAYERS_PKING = new InterfaceText(F2P, 3, "<col=D37E2A><img=15> Players PKing: <col=00FF00>" + total(player));
		}
		commands.add(TITLE_SERVER_RELATED);
		
		if (player.world().realm().isPVP()) {
			String risk = WildernessLevelIndicator.inAttackableArea(player) ? "<col=00FF00>" + L10n.format((int) player.attribOr(AttributeKey.RISKED_WEALTH, 0) +
					(int) player.attribOr(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0)) + " BM</col>" : "<col=FF0000>Not in wild</col>";
			
			commands.add(TITLE_STATS);
			
			commands.add(UPTIME);
			commands.add(new InterfaceText(F2P, 1, "<col=D37E2A><img=25> Playtime: <col=00FF00>" + Hans.getTimeForQuestTab(player)));
			commands.add(PLAYERS_ONLINE);
			commands.add(PLAYERS_PKING);
			commands.add(DELETE_4);
			
			commands.add(new InterfaceText(F2P, 5, "<col=D37E2A><img=24> Vote Points: <col=00FF00>" + votingPoints));

			double spent = Math.round(player.totalSpent() * 100.0) / 100.0;
			// Note this is TWO on ONE line using <br> which makes it slightly cramped but required to fit everything into f2p section
			commands.add(new InterfaceText(F2P, 6, "<col=D37E2A><img=" + getTotalSpentIcon(player) + "> Total Spent: <col=00FF00>$" + spent + "<br><col=D37E2A><img=24> Credits: <col=00FF00>" + player.credits()));
			commands.add(DELETE_7);
			
			commands.add(new InterfaceText(F2P, 8, "<col=D37E2A><img=34> Kills: <col=00FF00>" + kills));
			commands.add(new InterfaceText(F2P, 18, "<col=D37E2A><img=13> Deaths: <col=00FF00>" + deaths));
			commands.add(new InterfaceText(F2P, 9, "<col=D37E2A><img=14> K/D Ratio: <col=00FF00>" + getKdr(player)));
			commands.add(new InterfaceText(F2P, 10, "<col=D37E2A><img=" + getKillstreakIcon(player) + "> Killstreak: <col=00FF00>" + killstreakOf(player)));
			commands.add(new InterfaceText(F2P, 11, "<col=D37E2A><img=31> Highest Killstreak: <col=00FF00>" + killstreakTop));
			commands.add(new InterfaceText(F2P, 12, "<col=D37E2A><img=32> Highest Shutdown: <col=00FF00>" + shutdownTop));
			commands.add(new InterfaceText(F2P, 13, "<col=D37E2A><img=40> Elo Rating : " + eloRating));
//			commands.add(new InterfaceText(F2P, 14, "<col=D37E2A><img=40> " + QuestTab.risk_prot_state_string(player)));
			commands.add(VarbitAttributes.varbiton(player, 18) ? BLOCK_RUSH_ON : BLOCK_RUSH_OFF);
			commands.add(VarbitAttributes.varbiton(player, 19) ? SPEC_TELE_ON : SPEC_TELE_OFF);
			commands.add(VarbitAttributes.varbiton(player, 20) ? PRAYER_POS_OLD : PRAYER_POS_NEW);
			
			commands.add(GAME_ACTIONS);
			commands.add(HYBRID_SETUP);
			commands.add(MELEE_SETUP);
			commands.add(PURE_SETUP);
			commands.add(ZERKER_SETUP);
			commands.add(new InterfaceText(P2P, 3, "<img=33><col=D37E2A> Presets: <col=00FF00>" + player.presetRepository().used() + "/" + player.presetRepository().totalStorage()));

			commands.add(new InterfaceText(P2P, 4, "<col=D37E2A><img=12> Guide & Help"));
			
		} else if (player.world().realm().isOSRune()) {
			commands.add(TITLE_PLAYER_JOURNAL);
			
			commands.add(UPTIME);
			commands.add(new InterfaceText(F2P, 1, "<col=D37E2A><img=25> Playtime: <col=00FF00>" + Hans.getTimeForQuestTab(player)));
			commands.add(PLAYERS_ONLINE);
			commands.add(PLAYERS_PKING);
            commands.add(new InterfaceText(F2P, 4, "<img=22><col=D37E2A> Mode: <col=00FF00>"+ Misc.modeForDisplay(player)));
			commands.add(new InterfaceText(F2P, 5, "<col=D37E2A><img=24> Vote Points: <col=00FF00>" + votingPoints));
			double spent = Math.round(player.totalSpent() * 100.0) / 100.0;
			commands.add(new InterfaceText(F2P, 6, "<col=D37E2A><img=" + getTotalSpentIcon(player) + "> Total Spent: <col=00FF00>$" + spent + "<br><col=D37E2A><img=24> Credits: <col=00FF00>" + player.credits()));

			commands.add(new InterfaceText(F2P, 8, "<col=D37E2A><img=34> Kills: <col=00FF00>" + kills));
			commands.add(new InterfaceText(F2P, 18, "<col=D37E2A><img=13> Deaths: <col=00FF00>" + deaths));
			commands.add(new InterfaceText(F2P, 9, "<col=D37E2A><img=14> K/D Ratio: <col=00FF00>" + getKdr(player)));
			commands.add(new InterfaceText(F2P, 10, "<col=D37E2A><img=" + getKillstreakIcon(player) + "> Killstreak: <col=00FF00>" + killstreakOf(player)));
			commands.add(new InterfaceText(F2P, 11, "<col=D37E2A><img=31> Highest Killstreak: <col=00FF00>" + killstreakTop));
			commands.add(new InterfaceText(F2P, 12, "<col=D37E2A><img=32> Highest Shutdown: <col=00FF00>" + shutdownTop));
			
			if ((int)player.attribOr(AttributeKey.SLAYER_TASK_AMT, 0) < 1) {
				commands.add(TASK_UNASSIGNED);
			} else {
				SlayerCreature monster = SlayerCreature.lookup(player.attribOr(AttributeKey.SLAYER_TASK_ID, 0));
				commands.add(new InterfaceText(F2P, 13, "<col=D37E2A><img=39> Task:<col=00FF00> " + capitalizeFirstLetter(monster.name().replace("_", " ").toLowerCase()) + " (" + player.attribOr(AttributeKey.SLAYER_TASK_AMT, 0) + ")"));
			}
			
			
			commands.add(new InterfaceText(F2P, 14, "<col=D37E2A><img=25> Blessing:<col=00FF00> " + BonusContent.activeBlessing.getTitle()));
		} else if (player.world().realm().isRealism()) {
			// empty
		} else {
			commands.add(TITLE_PLAYER_JOURNAL);
			
			commands.add(UPTIME);
			commands.add(new InterfaceText(F2P, 1, "<col=D37E2A><img=25> Playtime: <col=00FF00>" + Hans.getTimeForQuestTab(player)));
			commands.add(PLAYERS_ONLINE);
			commands.add(PLAYERS_PKING);
			commands.add(DELETE_4);
			
			commands.add(new InterfaceText(F2P, 5, "<col=D37E2A><img=24> Vote Points: <col=00FF00>" + votingPoints));
			double spent = Math.round(player.totalSpent() * 100.0) / 100.0;
			commands.add(new InterfaceText(F2P, 6, "<col=D37E2A><img=" + getTotalSpentIcon(player) + "> Total Spent: <col=00FF00>$" + spent + "<br><col=D37E2A><img=24> Credits: <col=00FF00>" + player.credits()));
			commands.add(DELETE_7);
			
			commands.add(new InterfaceText(F2P, 8, "<col=D37E2A><img=34> Kills: <col=00FF00>" + kills));
			commands.add(new InterfaceText(F2P, 18, "<col=D37E2A><img=13> Deaths: <col=00FF00>" + deaths));
			commands.add(new InterfaceText(F2P, 9, "<col=D37E2A><img=14> K/D Ratio: <col=00FF00>" + getKdr(player)));
			
			commands.add(new InterfaceText(P2P, 3, "<img=33><col=D37E2A> Presets: <col=00FF00>" + player.presetRepository().used() + "/" + player.presetRepository().totalStorage()));
		}
		
		// Flush the list of messages
		player.write(commands.toArray());
		commands.clear();
		
		ticksUntil = 5;
	}
	
	public void forceUpdate() {
		ticksUntil = 0;
	}
	
	public static void resetConstants() {
		UPTIME = null;
		PLAYERS_ONLINE = null;
		PLAYERS_PKING = null;
	}
	
	public static String capitalizeFirstLetter(String original) {
		if (original == null || original.length() == 0) {
			return original;
		}
		return original.substring(0, 1).toUpperCase() + original.substring(1);
	}
	
	
	private int getTotalSpentIcon(Player player) {
		double totalSpent = player.totalSpent();
		if (totalSpent >= 5000) {
			return 49; // Gold image
		} else if (totalSpent >= 2500) {
			return 48; // Pink image
		} else if (totalSpent >= 1000) {
			return 47; // Gold image
		} else if (totalSpent >= 500) {
			return 46; // Pink image
		} else if (totalSpent >= 250) {
			return 45; // Green image
		} else if (totalSpent >= 100) {
			return 44; // Blue image
		} else if (totalSpent >= 5) {
			return 43; // Red icon
		}
		return 43;
	}
	
	public static int total(Player player) {
		int wildy = Misc.inWildCount(player.world());
		
		if (player.world().realm().isPVP()) {
			int maps = Misc.inPvpInstanceCount(player.world());
			return wildy + maps;
		} else {
			return wildy;
		}
	}
	
	public static int getKillstreakIcon(Player player) {
		int streak = player.attribOr(AttributeKey.KILLSTREAK, 0);
		if (streak >= 10) {
			return 30;
		} else if (streak >= 8) {
			return 29;
		} else if (streak >= 6) {
			return 28;
		} else if (streak >= 4) {
			return 27;
		} else if (streak >= 2) {
			return 26;
		}
		return 26;
	}
	
	public static String getKdr(Player player) {
		int deaths = player.varps().varp(Varp.DEATHS);
		int kills = player.varps().varp(Varp.KILLS);
		
		if (deaths == 0) {
			return String.valueOf(kills);
		}
		
		int one = (kills * 10 / deaths);
		int two = one / 10;
		int three = one % 10;
		return String.valueOf(two) + "." + String.valueOf(three);
	}
	
	private static int killstreakOf(Player player) {
		return player.attribOr(AttributeKey.KILLSTREAK, 0);
	}

	public static void clearList(Player player) {
		for (int i = 0; i < 20; i++) {
			player.write(new InterfaceText(QuestTab.F2P, i, ""));
		}
		for (int i = 0; i < 116; i++) {
			player.write(new InterfaceText(QuestTab.P2P, i, ""));
		}
		for (int i = 0; i < 13; i++) {
			player.write(new InterfaceText(QuestTab.MINIQ, i, ""));
		}
	}
}
