package nl.bartpelle.veteres.services.serializers.pg;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.typesafe.config.Config;
import kotlin.Function;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.PremiumTier;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.ServerHandler;
import nl.bartpelle.veteres.net.message.LoginRequestMessage;
import nl.bartpelle.veteres.services.serializers.PlayerLoadResult;
import nl.bartpelle.veteres.services.serializers.PlayerSerializer;
import nl.bartpelle.veteres.services.serializers.pg.part.*;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.PgSqlWorker;
import nl.bartpelle.veteres.services.sql.SharableStatement;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.RSFormatter;
import nl.bartpelle.veteres.util.Varbit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import java.math.BigInteger;
import java.sql.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.function.Consumer;

/**
 * Created by Bart on 8/10/2015.
 */
public class PgSqlPlayerSerializer extends PlayerSerializer {
	
	private static final Logger logger = LogManager.getLogger(PgSqlPlayerSerializer.class);
	private static SharableStatement credentialStatement = new SharableStatement("SELECT * FROM accounts WHERE username=?;");
	private static SharableStatement uuidStatement = new SharableStatement("SELECT * FROM uuid_bans WHERE uuid=?;");
	private static SharableStatement hwidStatement = new SharableStatement("SELECT 1 FROM hwid_bans WHERE hwid = ?::citext;");
	private static SharableStatement loggingStatement = new SharableStatement("SELECT * FROM packet_log_targets WHERE target = ? OR target = ?;");
	private static SharableStatement characterStatement = new SharableStatement("SELECT * FROM characters WHERE account_id=? AND service_id=?;");
	private static SharableStatement characterUpdateStatement = new SharableStatement("UPDATE characters SET x=?,z=?,level=?," +
			"skills=?::JSONB,varps=?::JSONB,playtime=?,ironmode=?,attribs=?::JSONB,gamemode=?," +
			"migration=?,gender=?,looks=?::INT[],colors=?::INT[] WHERE account_id=(SELECT id FROM accounts WHERE username=?) AND service_id=?;");
	private static SharableStatement accountCreationStatement = new SharableStatement("INSERT INTO accounts (username, password, email, displayname) " +
			"VALUES (?,?,?,?) RETURNING id;");
	private static SharableStatement characterCreationStatement = new SharableStatement("INSERT INTO characters (account_id, service_id) VALUES (?,?) RETURNING id, x, z, varps;");
	private static SharableStatement checkOnline = new SharableStatement("SELECT * FROM online_characters WHERE account_id=? AND service_id=? AND world_id=?;");
	private static SharableStatement removeOnline = new SharableStatement("DELETE FROM online_characters WHERE account_id=? AND service_id=? AND world_id=?;");
	private static SharableStatement addOnline = new SharableStatement("INSERT INTO online_characters(account_id,service_id,character_id,world_id,ip,uuid) VALUES (?,?,?,?,?,?);");
	private static SharableStatement banPlayer = new SharableStatement("UPDATE accounts SET banned_until = ? WHERE displayname ILIKE ?;");
	private static SharableStatement uuidBan = new SharableStatement("INSERT INTO uuid_bans (uuid, account_id, by_account_id) VALUES (?, ?, ?)");
	private static SharableStatement uuidUnban = new SharableStatement("DELETE FROM uuid_bans WHERE account_id = (SELECT id FROM accounts WHERE displayname ILIKE ?)");
	private static SharableStatement unbanPlayer = new SharableStatement("UPDATE accounts SET banned_until = NULL WHERE displayname ILIKE ?;");
	private static SharableStatement mutePlayer = new SharableStatement("UPDATE accounts SET muted_until = ? WHERE displayname ILIKE ?;");
	private static SharableStatement unmutePlayer = new SharableStatement("UPDATE accounts SET muted_until = NULL WHERE displayname ILIKE ?;");
	private static SharableStatement totalDonated = new SharableStatement("SELECT sum(price::DOUBLE PRECISION) as donated FROM purchases WHERE account_id = ?;");
	private static SharableStatement totalDonatedInvoices = new SharableStatement("SELECT sum(total_price) as donated FROM invoices WHERE account_id = ? AND paid = TRUE ;");
	private static SharableStatement twofactorWhitelist = new SharableStatement("SELECT * FROM twofactor_whitelist WHERE (account_id = ? AND ip = ?) OR (account_id = -1 AND ip = ?)");
	private static SharableStatement registerDevice = new SharableStatement("INSERT INTO twofactor_whitelist (account_id, ip, added_on, last_used) VALUES (?, ?, now(), now())");
	private static SharableStatement updateDevice = new SharableStatement("UPDATE twofactor_whitelist SET last_used = now() WHERE account_id = ? AND ip = ?");
	private static SharableStatement installTwofactor = new SharableStatement("UPDATE accounts SET twofactor_key = ?, email = ? WHERE id = ?");
	private static SharableStatement disableTwofactor = new SharableStatement("UPDATE accounts SET twofactor_key = NULL WHERE id = ?");
	private static SharableStatement inviteTwofactor = new SharableStatement("INSERT INTO twofactor_invites (id, code, player_name, player_name_url) VALUES (?, ?, ?, ?)");
	private static SharableStatement updateAccountLastSession = new SharableStatement("UPDATE accounts SET last_ip = ?, last_online = now(), last_uuid = ?, last_hwid = ? where id = ?");
	private static SharableStatement shadowMutePlayer = new SharableStatement("UPDATE accounts SET shadowmuted = true WHERE displayname ILIKE ?;");
	private static SharableStatement unShadowMutePlayer = new SharableStatement("UPDATE accounts SET shadowmuted = false WHERE displayname ILIKE ?;");
	private static SharableStatement characterUpdatePMStatusStatement = new SharableStatement("UPDATE characters SET pmstatus=? WHERE account_id=? AND service_id=?;");
	private static SharableStatement getRefundInformation = new SharableStatement("SELECT * FROM refunds WHERE claimed_by = ?;");
	private static SharableStatement getOldRefundDetails = new SharableStatement("SELECT * FROM refunds WHERE username=?;");
	private static SharableStatement claimCreditChunk = new SharableStatement("UPDATE refunds SET credits_remaining = credits_remaining - ? " +
			"WHERE claimed_by=? AND credits_remaining >= ?;");
	private static SharableStatement linkOldAccount = new SharableStatement("UPDATE refunds SET claimed_by = ? WHERE username = ? AND claimed_by IS NULL OR claimed_by = 0 RETURNING total_usd, credits_remaining, old_account_id, credits_per_chunk;");
	
	// No longer needed as jail does not expire. You HAVE to mine ores to escape. Or get unjailed manually.
	//private static SharableStatement jailPlayer = new SharableStatement("UPDATE accounts SET jailed_until = ? WHERE displayname ILIKE ?;");
	//private static SharableStatement unjailPlayer = new SharableStatement("UPDATE accounts SET jailed_until = NULL WHERE displayname ILIKE ?;");
	
	private Gson gson;
	private GameServer server;
	private PgSqlService sqlService;
	private JsonParser parser;
	private Cache<Long, String> loginFailures = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES).build();
	
	public static boolean disablePasscheck = false;
	public static boolean disableIngameCreation = true;
	
	/**
	 * Shared queue for workers.
	 */
	private LinkedBlockingQueue<SqlTransaction> transactions = new LinkedBlockingQueue<>();
	
	private List<PgJsonPart> parts = new LinkedList<PgJsonPart>() {
		{
			add(new TilePart());
			add(new SkillsPart());
			add(new VarpsPart());
			add(new PlaytimePart());
			add(new IronmodePart());
			add(new AttribsPart());
			add(new GameModePart());
			add(new MigrationPart());
			add(new LooksPart());
			add(new ItemsPart());
			add(new TimersPart());
		}
	};
	
	
	public PgSqlPlayerSerializer() {
		super(null);
		
		gson = new GsonBuilder().create(); //TODO configuration
		parser = new JsonParser();
	}
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		super.setup(server, serviceConfig);
		this.server = server;
	}
	
	@Override
	public boolean start() {
		sqlService = server.service(PgSqlService.class, false).get();
		
		// Let the worker run in its own thread
		ForkJoinPool pool = new ForkJoinPool();
		for (int i = 0; i < 8; i++) {
			pool.execute(new PgSqlWorker(sqlService, transactions));
		}
		
		return true;
	}
	
	private void throttle(String ip) {
		loginFailures.put(System.currentTimeMillis(), ip);
	}
	
	private boolean isThrottled(String ip) {
		loginFailures.cleanUp();
		return loginFailures.asMap().values().stream().filter(ipp -> ipp.equals(ip)).count() > 10;
	}
	
	@Override
	public boolean loadPlayer(Player player, Object uid, String password, Consumer<PlayerLoadResult> fn, LoginRequestMessage loginRequestMessage) {
		// Check username prior to processing :)
		if (!validName(player.username()) || !validName(player.name())) {
			fn.accept(PlayerLoadResult.UNREGISTERED_ACCOUNT);
			return true;
		}
		
		// Submit work to the almighty transaction worker!
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				// Is the Netty connection still open? Useless processing is not welcome.
				if (!player.channel().isActive() || !player.channel().isOpen()) {
					return;
				}
				
				// Are we UUID banned?
				PreparedStatement uuidcheck = uuidStatement.using(connection);
				uuidcheck.setString(1, player.uuid());
				if (uuidcheck.executeQuery().next()) {
					logger.info("Rejected User login from IP: {} Username: {}. Reason: UUID Banned {}", player.ip(), player.name().toLowerCase(), player.uuid());
					connection.rollback();
					fn.accept(PlayerLoadResult.BANNED);
					return;
				}
				
				// Are we HWID banned?
				if (loginRequestMessage.hwid() != null && !loginRequestMessage.hwid().isEmpty()) {
					PreparedStatement hwidCheck = hwidStatement.using(connection);
					hwidCheck.setString(1, loginRequestMessage.hwid().trim());
					if (hwidCheck.executeQuery().next()) {
						logger.info("Rejected User login from IP: {} Username: {}. Reason: HWID Banned {}", player.ip(), player.name().toLowerCase(), loginRequestMessage.hwid().trim());
						connection.rollback();
						fn.accept(PlayerLoadResult.SILENT_FAIL);
						return;
					}
				}
				
				// Is this IP being throttled?
				if (isThrottled(player.ip())) {
					connection.rollback();
					fn.accept(PlayerLoadResult.LOGIN_THROTTLE);
					return;
				}
				
				PreparedStatement accountStatement = credentialStatement.using(connection);
				accountStatement.setString(1, player.username().toLowerCase());
				ResultSet accountInfo = accountStatement.executeQuery();
				
				int characterId = 0;
				
				// Did we have a match?
				if (accountInfo.next()) {
					// Verify the password using BCrypt.
					String storedpass = accountInfo.getString("password");
					
					if (!(disablePasscheck && player.ip().equals("127.0.0.1"))) {
						if (disablePasscheck || !BCrypt.checkpw(password, storedpass)) {
							fn.accept(PlayerLoadResult.INVALID_DETAILS);
							logger.info("Rejected User login from IP: {} Username: {}. Reason: Invalid Credentials", player.ip(), player.name().toLowerCase());
							connection.rollback();
							throttle(player.ip());
							return;
						}
					}
					
					// Banned?
					if (!disablePasscheck && !player.ip().equals("127.0.0.1")) {
						Timestamp bannedUntil = accountInfo.getTimestamp("banned_until");
						if (bannedUntil != null && !bannedUntil.before(Date.from(Instant.now()))) {
							fn.accept(PlayerLoadResult.BANNED);
							logger.info("Rejected User login from IP: {} Username: {}. Reason: Account Banned", player.ip(), player.name().toLowerCase());
							connection.rollback();
							return;
						}
					}
					
					// Check for two-factor auth
					String twofactorKey = accountInfo.getString("twofactor_key");
					if (twofactorKey != null && !twofactorKey.isEmpty() && !(disablePasscheck && player.ip().equals("127.0.0.1"))) {
						// Are we a whitelisted IP?
						PreparedStatement whitelist = twofactorWhitelist.using(connection);
						whitelist.setInt(1, accountInfo.getInt("id"));
						whitelist.setString(2, player.ip());
						whitelist.setString(3, player.ip());
						
						// Did we match a whitelisted entry?
						if (!whitelist.executeQuery().next()) {
							// Verify the key provided in the login request, otherwise wave it off
							if (!server.authenticator().authorize(twofactorKey, loginRequestMessage.authpin())) {
								logger.info("Rejected User login from IP: {} Username: {}. Reason: Authkey Verification", player.ip(), player.name().toLowerCase());
								fn.accept(loginRequestMessage.authpin() == -1 ? PlayerLoadResult.ENTER_AUTH : PlayerLoadResult.AUTH_MISMATCH);
								connection.rollback();
								throttle(player.ip());
								return;
							} else {
								// Authentication worked, but we weren't listed yet. Insert into database to avoid later on being prompted.
								PreparedStatement registerAuth = registerDevice.using(connection);
								registerAuth.setInt(1, accountInfo.getInt("id"));
								registerAuth.setString(2, player.ip());
								registerAuth.executeUpdate();
							}
						} else {
							// This IP was whitelisted, but we still update the last usage to make the user know this device was used.
							PreparedStatement updateauth = updateDevice.using(connection);
							updateauth.setInt(1, accountInfo.getInt("id"));
							updateauth.setString(2, player.ip());
							updateauth.executeUpdate();
						}
					}
					
					// Log current data
					PreparedStatement accUpdate = updateAccountLastSession.using(connection);
					accUpdate.setString(1, player.ip());
					accUpdate.setString(2, player.uuid());
					accUpdate.setString(3, player.hwid());
					accUpdate.setInt(4, accountInfo.getInt("id"));
					accUpdate.executeUpdate();
					
					// Basic information
					player.displayName(accountInfo.getString("displayname"));
					player.helper(accountInfo.getBoolean("helper"));
					player.seniorModerator(accountInfo.getBoolean("seniormod"));
					player.privilege(Privilege.values()[accountInfo.getInt("rights")]);
					player.id(accountInfo.getInt("id"));
					player.premium(PremiumTier.values()[accountInfo.getInt("premium_rank")]);
					player.muted(accountInfo.getTimestamp("muted_until"));
					player.logged(accountInfo.getBoolean("logged"));
					player.defaultIcon(accountInfo.getInt("default_icon"));
					player.twofactorKey(accountInfo.getString("twofactor_key"));
					player.deadmanParticipant(accountInfo.getBoolean("deadman_participant"));
					player.shadowMuted(accountInfo.getBoolean("shadowmuted"));
					player.credits(accountInfo.getInt("premium_points"));
					
					if (accountInfo.getTimestamp("premium_until") != null)
						player.premiumUntil(accountInfo.getTimestamp("premium_until").getTime());
					
					// Make sure we're not giving expired status here
					if (player.premiumUntil() < System.currentTimeMillis()) {
						player.premium(PremiumTier.NONE);
					}
					
					// Resolve the total donated
					PreparedStatement totalPaid = totalDonated.using(connection);
					totalPaid.setInt(1, accountInfo.getInt("id"));
					ResultSet paid = totalPaid.executeQuery();
					paid.next();
					double totalDonated = paid.getDouble("donated");
					
					totalPaid = totalDonatedInvoices.using(connection);
					totalPaid.setInt(1, accountInfo.getInt("id"));
					paid = totalPaid.executeQuery();
					paid.next();
					totalDonated += paid.getDouble("donated");
					
					// Get information about the account we're refunding from - if any.
					PreparedStatement refunds = getRefundInformation.using(connection);
					refunds.setInt(1, accountInfo.getInt("id"));
					ResultSet refundData = refunds.executeQuery();
					
					if (refundData.next()) {
						player.setRefundInformation(refundData.getInt("old_account_id"),
								refundData.getInt("credits_remaining"),
								refundData.getInt("credits_per_chunk"));
					}
					
					// New style boys.
					totalDonated += accountInfo.getDouble("extra_usd");
					
					player.totalSpent(totalDonated);
					
					// Let's proceed; grab the jsonb data =)
					PreparedStatement charStmt = characterStatement.using(connection);
					charStmt.setInt(1, accountInfo.getInt("id"));
					charStmt.setInt(2, server.serviceId());
					ResultSet characterInfo = charStmt.executeQuery();
					
					// Were we unable to find the account?
					if (!characterInfo.next()) {
						characterId = createCharacter(accountInfo.getInt("id"), player, connection);
					} else {
						characterId = characterInfo.getInt("id");
						
						// Serialize all the parts of the account
						for (PgJsonPart part : parts) {
							part.decode(player, characterInfo, characterId, connection);
						}
					}
					
					player.characterId(characterId);
				} else {
					// If we disable account creation in-game, show invalid deets.
					if (disableIngameCreation) {
						fn.accept(PlayerLoadResult.INVALID_DETAILS);
						connection.rollback();
						return;
					}
					
					if (illegalName(player.username()) || illegalName(player.name())) {
						fn.accept(PlayerLoadResult.INVALID_DETAILS);
						connection.rollback();
						logger.info("Rejected User login from IP: {} Username: {}. Reason: Invalid Username", player.ip(), player.name());
						return;
					}
					
					PreparedStatement accStmt = accountCreationStatement.using(connection);
					accStmt.setString(1, player.username().toLowerCase());
					accStmt.setString(2, BCrypt.hashpw(password, BCrypt.gensalt(8)));
					accStmt.setString(3, "no@email.com");
					accStmt.setString(4, RSFormatter.formatDisplayname(player.username()));
					ResultSet set = accStmt.executeQuery(); // Execute the insert
					set.next();
					
					int id = set.getInt("id");
					player.id(id);
					
					// Finally execute the insert :)
					characterId = createCharacter(id, player, connection);
				}
				
				// Finally, do one more 'check' and remove the player from the players online.
				try {
					PreparedStatement stmt = addOnline.using(connection);
					stmt.setInt(1, (int) player.id()); // account_id
					stmt.setInt(2, server.serviceId()); // service_id
					stmt.setInt(3, characterId); // world_id
					stmt.setInt(4, server.world().id()); // world_id
					stmt.setString(5, player.ip());
					stmt.setString(6, player.uuid());
					
					// Make sure that was valid and stuff. 0 results altered means we failed to remove it.
					if (stmt.executeUpdate() == 0) {
						connection.rollback();
						fn.accept(PlayerLoadResult.ALREADY_ONLINE);
						return;
					}
				} catch (Exception e) { // This kind of comes expected. It's thrown if it violates the uniqueness.
					connection.rollback();
					fn.accept(PlayerLoadResult.ALREADY_ONLINE);
					return;
				}
				
				// Are we packet logged? Fetch for both the IP and the UUID. Can be bound to both!
				PreparedStatement loggingCheck = loggingStatement.using(connection);
				loggingCheck.setString(1, player.ip());
				loggingCheck.setString(2, player.uuid());
				ResultSet set = loggingCheck.executeQuery();
				
				// Any result automatically means we're logged.
				if (set.next()) {
					player.logged(true);
				}
				
				connection.commit();
				
				// Attach player to session
				player.channel().attr(ServerHandler.ATTRIB_PLAYER).set(player);
				
				// If everything worked, we may go on.
				fn.accept(PlayerLoadResult.OK);
			}
		});
		
		return true;
	}
	
	private int createCharacter(int accountId, Player p, Connection connection) throws SQLException {
		PreparedStatement charStmt = characterCreationStatement.using(connection);
		charStmt.setInt(1, accountId);
		charStmt.setInt(2, server.serviceId());
		ResultSet insertedCharacter = charStmt.executeQuery();
		insertedCharacter.next();
		
		int characterId = insertedCharacter.getInt("id");
		p.characterId(characterId);
		p.teleport(insertedCharacter.getInt("x"), insertedCharacter.getInt("z"));
		new VarpsPart().decode(p, insertedCharacter, characterId, connection);
		
		// On the PVP world we teleport the player to edgeville.
		if (server.world().realm().isPVP()) {
			p.teleport(3087, 3501, 0);
			
			// Predefined bank
			int i = 0;
			p.bank().set(i++, new Item(4587, 20000)); // Scim
			p.bank().set(i++, new Item(1215, 20000)); // Dagger
			p.bank().set(i++, new Item(4089, 20000)); // Mystic
			p.bank().set(i++, new Item(4109, 20000)); // Mystic
			p.bank().set(i++, new Item(4099, 20000)); // Mystic
			p.bank().set(i++, new Item(7400, 20000)); // Enchanted
			p.bank().set(i++, new Item(3755, 20000)); // farseer helm
			p.bank().set(i++, new Item(1163, 20000)); // rune full helm
			p.bank().set(i++, new Item(1305, 20000)); // d long
			p.bank().set(i++, new Item(4675, 20000)); // ancient staff
			p.bank().set(i++, new Item(4091, 20000)); // Mystic
			p.bank().set(i++, new Item(4111, 20000)); // Mystic
			p.bank().set(i++, new Item(4101, 20000)); // Mystic
			p.bank().set(i++, new Item(7399, 20000)); // enchanted
			p.bank().set(i++, new Item(3751, 20000)); // hat
			p.bank().set(i++, new Item(1127, 20000)); // rune
			p.bank().set(i++, new Item(1434, 20000)); // mace
			p.bank().set(i++, new Item(9185, 20000)); // crossbow
			p.bank().set(i++, new Item(4093, 20000)); // Mystic
			p.bank().set(i++, new Item(4113, 20000)); // Mystic
			p.bank().set(i++, new Item(4103, 20000)); // Mystic
			p.bank().set(i++, new Item(7398, 20000)); // enchanted
			p.bank().set(i++, new Item(3753, 20000)); // helm
			p.bank().set(i++, new Item(1079, 20000)); // rune
			p.bank().set(i++, new Item(5698, 20000)); // dagger
			p.bank().set(i++, new Item(10499, 20000)); // avas
			p.bank().set(i++, new Item(4097, 20000)); // Mystic
			p.bank().set(i++, new Item(4117, 20000)); // Mystic
			p.bank().set(i++, new Item(4107, 20000)); // Mystic
			p.bank().set(i++, new Item(2579, 20000)); // wiz boots
			p.bank().set(i++, new Item(3749, 20000)); // helm
			p.bank().set(i++, new Item(4131, 20000)); // rune boots
			p.bank().set(i++, new Item(2503, 20000)); // hides
			p.bank().set(i++, new Item(2497, 20000)); // hides
			p.bank().set(i++, new Item(12492, 20000)); // hides
			p.bank().set(i++, new Item(12508, 20000)); // hides
			p.bank().set(i++, new Item(12500, 20000)); // hides
			p.bank().set(i++, new Item(3105, 20000)); // climbers
			p.bank().set(i++, new Item(1093, 20000)); // rune
			p.bank().set(i++, new Item(1201, 20000)); // rune
			p.bank().set(i++, new Item(3842, 20000)); // god book
			p.bank().set(i++, new Item(12612, 20000)); // god book
			p.bank().set(i++, new Item(12494, 20000)); // hides
			p.bank().set(i++, new Item(12510, 20000)); // hides
			p.bank().set(i++, new Item(12502, 20000)); // hides
			p.bank().set(i++, new Item(6108, 20000)); // ghostly
			p.bank().set(i++, new Item(6107, 20000)); // ghostly
			p.bank().set(i++, new Item(6109, 20000)); // ghostly
			
			// END OF TAB 1
			p.bank().set(i++, new Item(2436, 20000)); // pots
			p.bank().set(i++, new Item(2440, 20000)); // pots
			p.bank().set(i++, new Item(2442, 20000)); // pots
			p.bank().set(i++, new Item(2444, 20000)); // pots
			p.bank().set(i++, new Item(3040, 20000)); // pots
			p.bank().set(i++, new Item(10925, 20000)); // pots
			p.bank().set(i++, new Item(3024, 20000)); // pots
			p.bank().set(i++, new Item(6685, 20000)); // pots
			p.bank().set(i++, new Item(145, 20000)); // pots
			p.bank().set(i++, new Item(157, 20000)); // pots
			p.bank().set(i++, new Item(163, 20000)); // pots
			p.bank().set(i++, new Item(169, 20000)); // pots
			p.bank().set(i++, new Item(3042, 20000)); // pots
			p.bank().set(i++, new Item(10927, 20000)); // pots
			p.bank().set(i++, new Item(3026, 20000)); // pots
			p.bank().set(i++, new Item(6689, 20000)); // pots
			p.bank().set(i++, new Item(147, 20000)); // pots
			p.bank().set(i++, new Item(159, 20000)); // pots
			p.bank().set(i++, new Item(165, 20000)); // pots
			p.bank().set(i++, new Item(171, 20000)); // pots
			p.bank().set(i++, new Item(3044, 20000)); // pots
			p.bank().set(i++, new Item(10929, 20000)); // pots
			p.bank().set(i++, new Item(3028, 20000)); // pots
			p.bank().set(i++, new Item(6687, 20000)); // pots
			p.bank().set(i++, new Item(149, 20000)); // pots
			p.bank().set(i++, new Item(161, 20000)); // pots
			p.bank().set(i++, new Item(167, 20000)); // pots
			p.bank().set(i++, new Item(173, 20000)); // pots
			p.bank().set(i++, new Item(3046, 20000)); // pots
			p.bank().set(i++, new Item(10931, 20000)); // pots
			p.bank().set(i++, new Item(3030, 20000)); // pots
			p.bank().set(i++, new Item(6691, 20000)); // pots
			p.bank().set(i++, new Item(385, 20000)); // sharks
			p.bank().set(i++, new Item(3144, 20000)); // karambwan
			p.bank().set(i++, new Item(560, 20000000)); // runes
			p.bank().set(i++, new Item(565, 20000000)); // runes
			p.bank().set(i++, new Item(555, 20000000)); // runes
			p.bank().set(i++, new Item(562, 20000000)); // runes
			p.bank().set(i++, new Item(557, 20000000)); // runes
			p.bank().set(i++, new Item(559, 20000000)); // runes
			p.bank().set(i++, new Item(564, 20000000)); // runes
			p.bank().set(i++, new Item(554, 20000000)); // runes
			p.bank().set(i++, new Item(9075, 20000000)); // runes
			p.bank().set(i++, new Item(556, 20000000)); // runes
			p.bank().set(i++, new Item(563, 20000000)); // runes
			p.bank().set(i++, new Item(559, 20000000)); // runes
			p.bank().set(i++, new Item(566, 20000000)); // runes
			p.bank().set(i++, new Item(561, 20000000)); // runes
			p.bank().set(i++, new Item(9241, 20000)); // bolts
			p.bank().set(i++, new Item(9244, 20000)); // bolts
			p.bank().set(i++, new Item(9245, 20000)); // bolts
			p.bank().set(i++, new Item(9243, 20000)); // bolts
			p.bank().set(i++, new Item(9242, 20000)); // bolts
			p.bank().set(i++, new Item(892, 20000)); // rune arrows
			p.bank().set(i++, new Item(10828, 20000)); // neit helm
			p.bank().set(i++, new Item(2412, 20000)); // sara god cape
			p.bank().set(i++, new Item(8013, 20000)); // home teleport tab
			p.bank().set(i++, new Item(7458, 20000)); // mithril gloves for pures
			p.bank().set(i++, new Item(7462, 20000)); // gloves
			p.bank().set(i++, new Item(11978, 20000)); // glory (6)
			p.varps().varbit(Varbit.BANKTAB_DISPLAY_TYPE, 0); // Bank tab display settings - number, roman numeral.
			p.varps().varbit(4171, 48);// bank tab sizes
			p.varps().varbit(4172, 0);
			p.varps().varbit(4173, 0);
			p.varps().varbit(4174, 0);
			p.varps().varbit(4175, 0);
			p.varps().varbit(4176, 0);
			p.varps().varbit(4177, 0);
			p.varps().varbit(4178, 0);
			p.varps().varbit(4179, 0);
			
			p.putattrib(AttributeKey.NEW_ACCOUNT, true);
		} else if (p.world().realm().isRealism()) {
			// Economy
			
			// Predefined bank. Runescape default starter pack items
			int i = 0;
			p.bank().set(i++, new Item(1351, 1)); // Bronze axe
			p.bank().set(i++, new Item(590, 1)); // Tinderbox
			p.bank().set(i++, new Item(303, 1)); // Small fishing net
			p.bank().set(i++, new Item(315, 1)); // Cooked shrimp
			p.bank().set(i++, new Item(1925, 1)); // Bucket
			p.bank().set(i++, new Item(1931, 1)); // Empty pot
			p.bank().set(i++, new Item(2309, 1)); // Bread
			p.bank().set(i++, new Item(1265, 1)); // Bronze pickaxe
			p.bank().set(i++, new Item(1205, 1)); // Bronze dagger
			p.bank().set(i++, new Item(1277, 1)); // Bronze sword
			p.bank().set(i++, new Item(1171, 1)); // Wooden shield
			p.bank().set(i++, new Item(841, 1)); // Shortbow
			p.bank().set(i++, new Item(882, 25)); // Bronze arrows
			// No runes.
			
			// On non-pvp world, it defaults to TRUE - so dmm skulls are hidden. We're going to allow them to be enabled though.
			VarbitAttributes.set(p, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.getVarbitid(), 1);
		} else if (p.world().realm().isOSRune()) {
			// OSRune
			p.teleport(3094, 3107, 0);
			p.inventory().add(new Item(995, 150000), false); // 150k gold
			p.inventory().add(new Item(380, 100), true); // 100x lobster
			p.inventory().add(new Item(1153), true); // Helm
			p.inventory().add(new Item(1115), true); // Body
			p.inventory().add(new Item(1067), true); // Legs
			p.inventory().add(new Item(1191), true); // Shield
			p.inventory().add(new Item(1323), true); // Scimitar
			p.inventory().add(new Item(3105), true); // Climb boots
			p.inventory().add(new Item(1731), true); // Amulet of power
			p.inventory().add(new Item(1169), true); // Coif
			p.inventory().add(new Item(1129), true); // Leather body
			p.inventory().add(new Item(1095), true); // Leather chaps
			p.inventory().add(new Item(841), true); // Shortbow
			p.inventory().add(new Item(882, 250), true); // Bronze arrows
			p.inventory().add(new Item(556, 250), true); // Air rune
			p.inventory().add(new Item(558, 250), true); // Mind rune
			p.inventory().add(new Item(554, 250), true); // Fire rune
			p.inventory().add(new Item(555, 250), true); // Water rune
			p.inventory().add(new Item(557, 250), true); // Earth rune
			p.inventory().add(new Item(562, 35), true); // Chaos rune
			p.inventory().add(new Item(563, 25), true); // Law rune
			
			p.putattrib(AttributeKey.NEW_ACCOUNT, true);
			
			// On non-pvp world, it defaults to TRUE - so dmm skulls are hidden. We're going to allow them to be enabled though.
			VarbitAttributes.set(p, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.getVarbitid(), 1);
		}
		
		return characterId;
	}
	
	private static boolean illegalName(String n) {
		n = n.toLowerCase();
		if (n.length() > 12 || n.contains("mod ") || n.contains("admin ") || n.contains("owner ") || n.contains("coder ") || n.contains("developer ") || n.contains("modera")) {
			return true;
		}
		
		// Double dashes
		if (n.contains("--"))
			return true;
		
		boolean normal = false;
		for (char c : n.toCharArray()) {
			if ((c >= 'A' && c <= 'Z') && (c >= 'a' && c <= 'z') && (c >= '0' && c <= '9')) {
				normal = true;
				break;
			}
		}
		
		if (!normal)
			return false;
		
		return false;
	}
	
	@Override
	public void savePlayer(Player player, boolean removeOnline) {
		if (player.bot())
			return;
		
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					long now = System.nanoTime();
					
					// See if we're online
					PreparedStatement stmt = checkOnline.using(connection);
					stmt.setInt(1, (int) player.id()); // account_id
					stmt.setInt(2, server.serviceId()); // service_id
					stmt.setInt(3, server.world().id()); // world_id
					
					// Did it return a result? If it did, we're online. If not, we're in trouble.
					ResultSet online = stmt.executeQuery();
					if (!online.next()) {
						connection.rollback();
						System.out.println("Rolling back, could not verify first stage querying.");
						throw new Exception("Rolling back, could not verify first stage querying.");
					}
					
					// Grab the character id from the online table
					int characterId = online.getInt("character_id");
					
					// Proceed with adding our new data
					stmt = characterUpdateStatement.using(connection);
					
					for (PgJsonPart part : parts) {
						part.encode(player, stmt, characterId, connection, removeOnline);
					}
					
					stmt.setString(14, player.username().toLowerCase());
					stmt.setInt(15, server.serviceId());
					stmt.executeUpdate();
					
					if (removeOnline) {
						// Finally, do one more 'check' and remove the player from the players online.
						stmt = PgSqlPlayerSerializer.removeOnline.using(connection);
						stmt.setInt(1, (int) player.id()); // account_id
						stmt.setInt(2, server.serviceId()); // service_id
						stmt.setInt(3, server.world().id()); // world_id
						
						// Make sure that was valid and stuff. 0 results altered means we failed to remove it.
						if (stmt.executeUpdate() == 0) {
							System.out.println("Rolling back, could not verify last stage removal.");
							connection.rollback();
							throw new Exception("Rolling back, could not verify last stage removal.");
						}
					} else {
						// See if we're online
						stmt = checkOnline.using(connection);
						stmt.setInt(1, (int) player.id()); // account_id
						stmt.setInt(2, server.serviceId()); // service_id
						stmt.setInt(3, server.world().id()); // world_id
						
						// Did it return a result? If it did, we're online. If not, we're in trouble.
						online = stmt.executeQuery();
						if (!online.next()) {
							connection.rollback();
							System.out.println("Rolling back, could not verify last stage querying.");
							throw new Exception("Rolling back, could not verify last stage querying.");
						}
					}
					
					// Nice, that went perfectly.
					connection.commit();
					logger.info("Finished saving player {} in {}ms.", player.name(), (System.nanoTime() - now) / 1_000_000);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not save Player {} info! Remove online: {}", player.name(), removeOnline,
							e.getMessage() != null && e.getMessage().equals("Rolling back, could not verify first stage querying.") ? null : e);
					throw e;
				}
			}
		});
	}
	
	public void banPlayer(String player) {
		banPlayer(player, System.currentTimeMillis() + 31536000000L);
	}
	
	public void banPlayer(String player, long until) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = banPlayer.using(connection);
					stmt.setTimestamp(1, new Timestamp(until));
					stmt.setString(2, player);
					
					if (stmt.executeUpdate() > 0) {
						logger.info("Banned player {}.", player);
					} else {
						logger.info("Could not find + ban player {}.", player);
					}
					
					connection.commit();
					
					server.world().playerByName(player).ifPresent(Player::logout);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not ban player!", e);
					throw e;
				}
			}
		});
	}
	
	public void uuidPlayerBan(Player player, Integer banner) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = uuidBan.using(connection);
					stmt.setString(1, player.uuid());
					stmt.setInt(2, (Integer) player.id());
					stmt.setInt(3, banner);
					
					if (stmt.executeUpdate() > 0) {
						logger.info("UUID Banned player {} with UUID {}.", player.name(), player.uuid());
					} else {
						logger.info("Could not find + uuid ban player {}.", player.name());
					}
					
					connection.commit();
					
					server.world().playerByName(player.name()).ifPresent(Player::logout);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not ban player!", e);
					throw e;
				}
			}
		});
	}
	
	// Details taken off ACP.
	public void uuidBan(String uuid, Integer account_id, Integer banner) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = uuidBan.using(connection);
					stmt.setString(1, uuid);
					stmt.setInt(2, account_id);
					stmt.setInt(3, banner);
					
					if (stmt.executeUpdate() > 0) {
						logger.info("UUID Banned player with UUID {} and account id {}.", uuid, account_id);
					} else {
						logger.info("Could not find UUID {} or account number {} of player.", uuid, account_id);
					}
					
					connection.commit();
					
					// No need to log a player out. We dont have UUID to player mappings anyway. It's account ID.
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not ban player!", e);
					throw e;
				}
			}
		});
	}
	
	public void uuidUnban(String player) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = uuidUnban.using(connection);
					stmt.setString(1, player);
					stmt.executeUpdate();
					
					connection.commit();
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not ban player!", e);
					throw e;
				}
			}
		});
	}
	
	public void unbanPlayer(String name) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = unbanPlayer.using(connection);
					stmt.setString(1, name); // name
					stmt.executeUpdate();
					connection.commit();
					logger.info("Unbanned player {}.", name);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not unban player!", e);
					throw e;
				}
			}
		});
	}
	
	public void mutePlayer(String name) {
		mutePlayer(name, System.currentTimeMillis() + 3600000L);
	}
	
	public void mutePlayer(String name, long until) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = mutePlayer.using(connection);
					stmt.setTimestamp(1, new Timestamp(until));
					stmt.setString(2, name);
					stmt.executeUpdate();
					connection.commit();
					logger.info("Muted player {}.", name);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not mute player!", e);
					throw e;
				}
			}
		});
	}
	
	public void unmutePlayer(String name) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = unmutePlayer.using(connection);
					stmt.setString(1, name); // name
					stmt.executeUpdate();
					connection.commit();
					logger.info("Unmuted player {}.", name);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not unmute player!", e);
					throw e;
				}
			}
		});
	}
	
	public void shadowMutePlayer(String name) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = shadowMutePlayer.using(connection);
					stmt.setString(1, name);
					stmt.executeUpdate();
					connection.commit();
					logger.info("Shadow Muted player {}.", name);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not shadow mute player!", e);
					throw e;
				}
			}
		});
	}
	
	public void unShadowMutePlayer(String name) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = unShadowMutePlayer.using(connection);
					stmt.setString(1, name); // name
					stmt.executeUpdate();
					connection.commit();
					logger.info("Unshadowmuted player {}.", name);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not unshadowmuted player!", e);
					throw e;
				}
			}
		});
	}
	
	public void setTwoFactor(String key, int id, String email) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = installTwofactor.using(connection);
					stmt.setString(1, key);
					stmt.setString(2, email);
					stmt.setInt(3, id);
					stmt.executeUpdate();
					connection.commit();
				} catch (Exception e) {
					connection.rollback();
					throw e;
				}
			}
		});
	}
	
	public void disableTwoFactor(int id) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = disableTwofactor.using(connection);
					stmt.setInt(1, id);
					stmt.executeUpdate();
					connection.commit();
				} catch (Exception e) {
					connection.rollback();
					throw e;
				}
			}
		});
	}
	
	public String addTwofactorInvite(String key, String email, String name, String nameproto) {
		byte[] rng = new byte[16];
		new Random().nextBytes(rng);
		String uid = new BigInteger(rng).toString(16).toUpperCase();
		System.out.println("2fa uid: " + uid);
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = inviteTwofactor.using(connection);
					stmt.setString(1, uid);
					stmt.setString(2, key);
					stmt.setString(3, name);
					stmt.setString(4, nameproto);
					stmt.executeUpdate();
					connection.commit();
				} catch (Exception e) {
					connection.rollback();
					throw e;
				}
			}
		});
		return uid;
	}
	
	public void joinDeadman(Player player) {
		int playerId = (int) player.id();
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = connection.prepareStatement("UPDATE accounts SET deadman_participant = TRUE WHERE id = ?");
					stmt.setInt(1, playerId);
					stmt.executeUpdate();
					connection.commit();
				} catch (Exception e) {
					connection.rollback();
					throw e;
				}
			}
		});
	}
	
	public void updateDefaultIcon(int playerId, int iconId) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = connection.prepareStatement("UPDATE accounts SET default_icon = ? WHERE id = ?");
					stmt.setInt(1, iconId);
					stmt.setInt(2, playerId);
					stmt.executeUpdate();
					connection.commit();
				} catch (Exception e) {
					connection.rollback();
					throw e;
				}
			}
		});
	}
	
	public void pmStatus(Player player, int pmStatus) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = characterUpdatePMStatusStatement.using(connection);
					stmt.setInt(1, pmStatus);
					stmt.setInt(2, (Integer) player.id());
					stmt.setInt(3, player.world().server().serviceId());
					stmt.executeUpdate();
					connection.commit();
				} catch (Exception e) {
					connection.rollback();
					throw e;
				}
			}
		});
	}
	
	public enum RefundVerificationResult {
		SUCCESS, INVALID_USER, INVALID_PASS, ALREADY_LINKED, ERROR
	}
	
	public void claimCreditChunk(Player claimer, int count, Function1<Boolean, Unit> cb) {
		if (!claimer.hasRefundAccount()) {
			return;
		}
		
		transactions.add(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement stmt = claimCreditChunk.using(connection);
					stmt.setInt(1, count);
					stmt.setInt(2, (Integer) claimer.id());
					stmt.setInt(3, count);
					
					int affected = stmt.executeUpdate();
					cb.invoke(affected > 0); // If we had any matches, that means we succeeded, thus > 0.
					connection.commit();
				} catch (Exception e) {
					e.printStackTrace();
					cb.invoke(false);
				}
			}
		});
	}
	
	public void verifyAndLinkRefundDetails(Player claimer, String username, String password, Function1<RefundVerificationResult, Void> cb) {
		transactions.add(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				try {
					PreparedStatement query = getOldRefundDetails.using(connection);
					query.setString(1, username);
					ResultSet results = query.executeQuery();
					
					if (!results.next()) {
						connection.commit();
						cb.invoke(RefundVerificationResult.INVALID_USER);
						return;
					}
					
					// Verify old stored passowrd
					String storedPass = results.getString("password");
					if (!BCrypt.checkpw(password, storedPass)) {
						connection.commit();
						cb.invoke(RefundVerificationResult.INVALID_PASS);
						return;
					}
					
					// See if that account already has someone sapping from the credits..
					int claimedBy = results.getInt("claimed_by");
					if (claimedBy > 0) {
						connection.commit();
						cb.invoke(RefundVerificationResult.ALREADY_LINKED);
						return;
					}
					
					// If none of the checks matched, we can link the account.
					PreparedStatement link = linkOldAccount.using(connection);
					link.setInt(1, (Integer) claimer.id());
					link.setString(2, username);
					ResultSet resultData = link.executeQuery();
					
					if (!resultData.next()) {
						connection.commit();
						cb.invoke(RefundVerificationResult.ERROR);
						return;
					}
					
					double totalUsd = resultData.getDouble("total_usd");
					int linkedId = resultData.getInt("old_account_id");
					int creditsRemaining = resultData.getInt("credits_remaining");
					int creditsPerChunk = resultData.getInt("credits_per_chunk");
					
					connection.commit();
					claimer.setRefundInformation(linkedId, creditsRemaining, creditsPerChunk);
					cb.invoke(RefundVerificationResult.SUCCESS);
				} catch (Exception e) {
					e.printStackTrace();
					cb.invoke(RefundVerificationResult.ERROR);
				}
			}
		});
	}

	/*public void jailPlayer(String name) {
		jailPlayer(name, System.currentTimeMillis() + 3600000L);
	}

	public void jailPlayer(String name, long until) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = jailPlayer.using(connection);
					stmt.setTimestamp(1, new Timestamp(until));
					stmt.setString(2, name);
					stmt.executeUpdate();
					connection.commit();
					logger.info("Jailed player {}.", name);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not jail player!", e);
					throw e;
				}
			}
		});
	}

	public void unjailPlayer(String name) {
		transactions.add(new SqlTransaction() {
			public void execute(Connection connection) throws Exception {
				try {
					// See if we're online
					PreparedStatement stmt = unjailPlayer.using(connection);
					stmt.setString(1, name); // name
					stmt.executeUpdate();
					connection.commit();
					logger.info("Unjailed player {}.", name);
				} catch (Exception e) {
					connection.rollback();
					logger.error("Could not unjail player!", e);
					throw e;
				}
			}
		});
	}*/
	
	public void transact(SqlTransaction t) {
		transactions.add(t);
	}
	
	private static final char[] VALID_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789- ".toCharArray();
	private static final char[] VALID_CHARS2 = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
	
	static {
		Arrays.sort(VALID_CHARS);
		Arrays.sort(VALID_CHARS2);
	}
	
	private static boolean validName(String s) {
		if (s.length() < 3 || s.length() > 12)
			return false;
		
		for (char c : s.toLowerCase().toCharArray())
			if (Arrays.binarySearch(VALID_CHARS, c) < 0)
				return false;
		
		// Banhammertime
		boolean allowed = false;
		for (char c : s.toLowerCase().toCharArray())
			if (Arrays.binarySearch(VALID_CHARS2, c) >= 0)
				allowed = true;
		
		return allowed;
	}
	
}
