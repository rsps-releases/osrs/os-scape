package nl.bartpelle.veteres.net.codec.game;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.ServerHandler;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.net.message.game.action.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bart Pelle on 8/23/2014.
 */
public class ActionDecoder extends ByteToMessageDecoder {
	
	private static final Logger logger = LogManager.getLogger(ActionDecoder.class);
	
	@SuppressWarnings("unchecked")
	private Class<? extends Action>[] actionRepository = new Class[256];
	private Map<Integer, Integer> ignored = new HashMap<>();
	private int[] actionSizes = new int[] { 9, -1, 3, -1, 7, 2, -1, 0, 9, 8, -1, 8, 7, 4, 8, 4, 7, 0, -1, -2, 8, 3, 8,
			8, 7, 8, 7, 15, 3, 16, 2, 8, -2, 2, 7, 9, 7, 13, 13, 8, 8, 10, 8, 11, -1, 8, 3, 7, -1, 14, 4, 7, 4, 6, 11,
			13, 8, 3, 8, -1, 16, 16, 1, 8, 15, 8, -1, 8, -1, 0, 0, 3, 3, 3, -1, -1, 0, 9, 8, 7, 5, 2, 3, -2, -1, 3, -1,
			3, 3, 3, -1, 8, 6, -1, 3, 4, 8, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	
	private State state = State.OPCODE;
	private int opcode;
	private int size;
	
	public ActionDecoder() {
		/* Fill repo, maybe through xml/json? */
		//actionRepository[232] = GrandExchangeSearch.class;
		actionRepository[84] = WalkMap.class;
		actionRepository[97] = WalkMap.class;
		actionRepository[44] = PublicChat.class;
		
		actionRepository[90] = ConsoleAction.class;
		
		actionRepository[40] = ItemAction1.class;
		actionRepository[58] = ItemAction2.class;
		actionRepository[39] = ItemAction3.class;
		actionRepository[56] = ItemAction4.class;
		actionRepository[42] = ItemAction5.class;
		actionRepository[33] = ExamineItem.class;
		
		actionRepository[35] = ItemDragAction.class;
		actionRepository[61] = ItemOnItemAction.class;
		actionRepository[29] = InterInterfaceItemDrag.class;
		actionRepository[27] = ItemOnGroundItem.class;
		
		actionRepository[71] = PlayerAction1.class; // Duel
		actionRepository[46] = PlayerAction2.class;
		actionRepository[2] = PlayerAction3.class; // Pretty odd - if you set p option 3 & 4 they call the same script (so packet? haven't debugged.. TODO)
		actionRepository[85] = PlayerAction4.class;
		//actionRepository[164] = PlayerAction5.class;
		//actionRepository[161] = PlayerAction6.class;
		//actionRepository[28] = PlayerAction7.class;
		//actionRepository[235] = PlayerAction8.class;
		
		actionRepository[77] = SpellOnPlayer.class;
		actionRepository[54] = ItemOnPlayer.class;
		
		actionRepository[62] = WindowStateChanged.class;
		actionRepository[80] = ChangeDisplayMode.class;
		
		actionRepository[36] = ObjectAction1.class;
		actionRepository[34] = ObjectAction2.class;
		//actionRepository[87] = ObjectAction3.class;
		//actionRepository[2] = ObjectAction4.class;
		//actionRepository[189] = ObjectAction5.class;
		actionRepository[81] = ExamineObject.class;
		actionRepository[64] = ItemOnObject.class;
		//actionRepository[175] = SpellOnObject.class;
		
		actionRepository[88] = NpcAttack.class;
		actionRepository[72] = NpcAction1.class;
		actionRepository[21] = NpcAction2.class;
		actionRepository[94] = NpcAction3.class;
		//actionRepository[78] = NpcAction4.class;
		actionRepository[30] = ExamineNpc.class;
		actionRepository[0] = SpellOnNpc.class;
		actionRepository[43] = ItemOnNpc.class;
		
		actionRepository[51] = GroundItemAction2.class;
		actionRepository[26] = TakeGroundItem.class;
		//actionRepository[117] = SpellOnGroundItem.class;
		
		actionRepository[92] = DialogueContinue.class;
		
		actionRepository[69] = CloseMainInterface.class;
		actionRepository[15] = IntegerInput.class;
		actionRepository[1] = StringInput.class;
		//actionRepository[253] = DialogueOption.class;
		
		//actionRepository[83] = SpellOnItem.class;
		
		actionRepository[19] = SendPrivateMessage.class;
		actionRepository[59] = AddFriend.class;
		actionRepository[68] = RemoveFriend.class;
		actionRepository[86] = AddIgnore.class;
		actionRepository[48] = RemoveIgnore.class;
		actionRepository[38] = SetLooks.class;
		actionRepository[6] = JoinClanChat.class;
		actionRepository[3] = ChangeClanRank.class;
		actionRepository[18] = KickClanMember.class;
		actionRepository[57] = PrivacySetting.class;
		
		actionRepository[17] = PingPacket.class;

		//actionRepository[135] = ClientCrash.class;
//		actionRepository[123] = UniqueButton1.class;
//		actionRepository[200] = UniqueButton2.class;
		
		Arrays.stream(ButtonAction.OPCODES).forEach(i -> actionRepository[i] = ButtonAction.class);
		
		// Ignore a few classes
		ignored.put(93, -1); // Mouse history
		ignored.put(83, -2); // Key history
		ignored.put(53, 6); // Mouse click
		//ignored.put(114, 4); // Camera movement
		ignored.put(50, 4); // Key
		//ignored.put(174, 0); // Sent on login
		//ignored.put(116, 4); //Weird random packet?
		ignored.put(76, 0); // Ping
	}
	
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if (in.readableBytes() < 1) {
			/* Disconnection */
			return;
		}
		
		Player player = ctx.channel().attr(ServerHandler.ATTRIB_PLAYER).get();
		RSBuffer buffer = new RSBuffer(in);
		
		switch (state) {
			case OPCODE:
				if (in.readableBytes() < 1)
					return;
				opcode = buffer.readByte() /*- player.inrand().nextInt()*/ & 0xFF;
				state = State.SIZE;
				in.markReaderIndex();
			
			case SIZE:
				if (actionRepository[opcode] == null && !ignored.containsKey(opcode)) {
					logger.warn("Unknown action {}, probable size: {}.", opcode, buffer.get().readableBytes());
					
					size = in.readableBytes();
				} else {
					int required = actionSizes[opcode];
					
					if (required == -1) {
						if (in.readableBytes() < 1) {
							in.resetReaderIndex();
							return;
						}
						
						size = buffer.readUByte();
					} else if (required == -2) {
						if (in.readableBytes() < 2) {
							in.resetReaderIndex();
							return;
						}
						
						size = buffer.readUShort();
					} else {
						size = required;
					}
				}
				
				state = State.DATA;
				in.markReaderIndex();
			
			case DATA:
				if (in.readableBytes() < size) {
					// Too much data? Skip it.
					if (size > 25000) {
						in.skipBytes(in.readableBytes());
						state = State.OPCODE;
					}
					
					break;
				}
				
				if (!ignored.containsKey(opcode) && actionRepository[opcode] != null) {
					Action a = actionRepository[opcode].newInstance();
					int bufferStart = buffer.get().readerIndex();

					try {
						a.decode(new RSBuffer(buffer.get().slice(bufferStart, size)), ctx, opcode, size, player);
						player.pendingActions().add(a);
					} catch (Exception e) {
						logger.error(e);
					}

					player.lastPing(System.currentTimeMillis());
					buffer.get().readerIndex(bufferStart + size);
				} else {
					buffer.skip(size);
				}
				
				state = State.OPCODE;
				in.markReaderIndex();
				break;
		}
	}
	
	public Class<? extends Action>[] repository() {
		return actionRepository;
	}
	
	enum State {
		OPCODE,
		SIZE,
		DATA
	}
	
}
