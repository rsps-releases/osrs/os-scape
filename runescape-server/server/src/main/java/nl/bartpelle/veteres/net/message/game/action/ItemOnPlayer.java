package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.PlayerInteraction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.WeakReference;

/**
 * Created by Bart on 8/20/2015.
 */
@PacketInfo(size = 11)
public class ItemOnPlayer implements Action {
	
	private static final Logger logger = LogManager.getLogger(ItemOnPlayer.class);
	
	private int playerIdx;
	private int slot;
	private int item;
	private int interfaceId;
	private int child;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		slot = buf.readULEShortA();
		item = buf.readULEShort();
		int hash = buf.readIntV1();
		interfaceId = hash >> 16;
		child = hash & 0xFFFF;
		boolean run = buf.readByteS() == 1;
		playerIdx = buf.readUShort();
	}
	
	@Override
	public void process(Player player) {
		logger.debug("Item %d on player from [%d,%d] pid %d inv-slot %d.", item, interfaceId, child, playerIdx, slot);
		player.debug("Item %d on player from [%d,%d] pid %d inv-slot %d.", item, interfaceId, child, playerIdx, slot);
		
		Player other = player.world().players().get(playerIdx);
		if (other == null) {
			player.message("Unable to find player.");
		} else {
			player.stopActions(false);
			
			// Verify item
			Item item = player.inventory().get(slot);
			if (item == null || item.id() != this.item) {
				return;
			}
			
			if (!player.locked() && !player.dead()) {
				player.face(other);
				if (!other.dead()) {
					player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(other));
					player.putattrib(AttributeKey.INTERACTION_OPTION, -1); // Special action
					player.putattrib(AttributeKey.ITEM_SLOT, slot);
					player.putattrib(AttributeKey.ITEM_ID, item.id());
					player.putattrib(AttributeKey.FROM_ITEM, item);
					player.world().server().scriptExecutor().executeLater(player, new PlayerInteraction(-1));
				}
			}
		}
	}
	
}
