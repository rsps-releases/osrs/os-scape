package nl.bartpelle.veteres.net.message.game.action;

import com.google.common.base.Strings;
import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.net.message.game.command.AddPrivateMessage;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.HuffmanCodec;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by Bart Pelle on 8/23/2014.
 */
@PacketInfo(size = -2)
public class SendPrivateMessage implements Action {
	
	private String target;
	private int len;
	private byte[] data;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		target = buf.readString();
		
		len = buf.readCompact();
		data = new byte[buf.get().readableBytes()];
		buf.get().readBytes(data);
	}
	
	@Override
	public void process(Player player) {
		// Don't bother if we're muted..
		if (!player.shadowMuted() && player.muted()) {
			player.message("You're currently muted and cannot talk.");
			return;
		}
		
		if (Strings.isNullOrEmpty(target)) {
			return;
		}
		
		// Decode huffman data
		byte[] stringData = new byte[256];
		HuffmanCodec codec = player.world().server().huffman();
		codec.decode(data, stringData, 0, 0, len);
		String message = new String(stringData, 0, len);
		
		player.world().server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement(
							"SELECT * FROM canprivatemessage(?, ?::citext);");
					
					s.setInt(1, (Integer) player.id());
					s.setString(2, target.toLowerCase());
					
					ResultSet set = s.executeQuery();
					if (set.next()) {
						boolean canPM = set.getBoolean("result");
						if (canPM) {
							int id = set.getInt("target_id");
							
							player.write(new AddPrivateMessage(target, message));
							
							if (VarbitAttributes.varbit(player, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) == 2) { // Our PM status is off right now.
								VarbitAttributes.set(player, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid(), 1); // Set private status to Friends.
							}
							
							if (!player.shadowMuted()) { //Hehehe sneaky sneaky
								// Dispatch interserver
								player.world().server().service(PmService.class, true).ifPresent(s2 -> s2.privateMessageDispatched(player, id, message));
								
								// Log
								player.world().server().service(LoggingService.class, true).ifPresent(s2 ->
										s2.logPrivateMessage((Integer) player.id(), id, player.world().id(), player.tile(), message));
							}
						} else {
							player.message("That player is currently offline.");
						}
					}
					connection.commit();
				}
			});
		});
	}
	
}
