package nl.bartpelle.veteres.services.serializers.pg.part;

import com.google.gson.*;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;
import nl.bartpelle.veteres.util.JGson;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Bart on 8/10/2015.
 */
public class SkillsPart implements PgJsonPart {
	
	private JsonParser parser = new JsonParser();
	private Gson gson = JGson.get();
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		JsonObject inventory = parser.parse(resultSet.getString("skills")).getAsJsonObject();
		JsonArray levels = inventory.getAsJsonArray("level");
		JsonArray xps = inventory.getAsJsonArray("xp");
		for (int i = 0; i < Skills.SKILL_COUNT; i++) {
			int lvl = levels.get(i).getAsInt();
			double xp = xps.get(i).getAsDouble();
			player.skills().xp()[i] = xp;
			player.skills().levels()[i] = lvl;
		}
		
		player.discardWrites(true);
		player.skills().recalculateCombat();
		player.discardWrites(false);
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection connection, boolean removeOnline) throws SQLException {
		JsonArray levels = new JsonArray();
		JsonArray xps = new JsonArray();
		
		for (int i = 0; i < Skills.SKILL_COUNT; i++) {
			levels.add(new JsonPrimitive(player.skills().level(i)));
			xps.add(new JsonPrimitive(player.skills().xp()[i]));
		}
		
		JsonObject itemobj = new JsonObject();
		itemobj.add("level", levels);
		itemobj.add("xp", xps);
		
		characterUpdateStatement.setString(4, gson.toJson(itemobj));
	}
	
}
