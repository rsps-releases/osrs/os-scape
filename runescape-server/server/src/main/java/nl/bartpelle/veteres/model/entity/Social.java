package nl.bartpelle.veteres.model.entity;

import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.net.message.game.command.UpdateFriends;
import nl.bartpelle.veteres.net.message.game.command.UpdateIgnores;
import nl.bartpelle.veteres.util.Varp;

import java.util.*;

/**
 * Created by Bart on 11/15/2015.
 */
public class Social {
	
	private Player player;
	private List<Friend> friends = new LinkedList<>();
	private List<Friend> ignores = new LinkedList<>();
	
	public Social(Player player) {
		this.player = player;
	}
	
	public boolean friendsReady() {
		return friends != null;
	}
	
	public List<Friend> friends() {
		return friends;
	}
	
	public void friends(List<Friend> friends) {
		this.friends = friends;
	}
	
	public boolean ignoresReady() {
		return ignores != null;
	}
	
	public List<Friend> ignores() {
		return ignores;
	}
	
	public void ignores(List<Friend> ignores) {
		this.ignores = ignores;
	}
	
	public void pushFriends() { //TODO fix this shit
		List<Friend> filteredFriends = new ArrayList<>();
		if (friends != null) {
			for (Friend f : friends) {
				Optional<Player> other = player.world().playerForId(f.id);
				
				int pmStatus = other.isPresent() ? VarbitAttributes.varbit(other.get(), VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) : -1;
				boolean forceOffline = pmStatus == 2;
				boolean friendsOnly = pmStatus == 1;
				
				if (friendsOnly) {
					//forceOffline = !FRIENDS_ONLY.test(new Tuple<>(player, other.get()));
				}
				
				f.world = forceOffline ? 0 : f.world;
				filteredFriends.add(f);
			}
		}
		
		player.write(new UpdateFriends(filteredFriends.toArray(new Friend[filteredFriends.size()])));
		player.varps().varp(Varp.CLAN_PRIVACY, 0);
		//player.varps().varp(Varp.CLIENT_SETTINGS, 0); // TODO find whatever varbit you want to use here.. don't reset the whole varp!
	}
	
	public void updateFriend(int id, int world) {
		if (friends != null) {
			for (Friend f : friends) {
				if (f.id == id) {
					f.world = world;
					player.write(new UpdateFriends(new Friend[]{f}));
				}
			}
		}
	}
	
	public void pushIgnores() {
		player.write(new UpdateIgnores(ignores.toArray(new Friend[ignores.size()])));
	}
	
	public HashSet<Integer> buildFriendIds() {
		HashSet<Integer> ids = new HashSet<>(friends.size());
		for (Friend friend : friends) {
			ids.add(friend.id());
		}
		return ids;
	}
}
