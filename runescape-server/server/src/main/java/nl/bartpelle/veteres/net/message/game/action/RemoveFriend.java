package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.services.intercom.PmService;

/**
 * Created by Bart on 11/15/2015.
 */
@PacketInfo(size = -1)
public class RemoveFriend implements Action {
	
	private String name;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		name = buf.readString();
	}
	
	@Override
	public void process(Player player) {
		// Find the friend in our list, then grab his id and remove him.
		for (Friend friend : player.social().friends()) {
			if (friend.name.equalsIgnoreCase(name)) {
				player.world().server().service(PmService.class, true).ifPresent(pmService -> {
					pmService.removeFriend(player, friend.id);
				});
				break;
			}
		}
		
		// And remove it immediately too thanks
		player.social().friends().removeIf(f -> f.name.equalsIgnoreCase(name));
	}
	
}
