package nl.bartpelle.veteres.model.map.steroids;


import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.World;

public abstract class EntityStepSupplier implements StepSupplier {
	
	public Entity follower;
	
	public EntityStepSupplier(Entity follower) {
		this.follower = follower;
	}
	
	public abstract boolean reached(World world, Entity target);
	
}
