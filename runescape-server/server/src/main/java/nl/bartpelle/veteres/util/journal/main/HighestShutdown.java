package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class HighestShutdown extends JournalEntry {

    @Override
    public void send(Player player) {
        int shutdownTop = player.attribOr(AttributeKey.SHUTDOWN_RECORD, 0);
        send(player, "<img=32> Highest Shutdown", shutdownTop, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int shutdownTop = player.attribOr(AttributeKey.SHUTDOWN_RECORD, 0);
        player.sync().shout("<img=32> My highest shutdown is "+ shutdownTop +".");
    }

}