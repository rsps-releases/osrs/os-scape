package nl.bartpelle.veteres.model;

/**
 * Created by Bart on 6/9/2016.
 */
public enum Realm {
	
	REALISM(1),
	PVP(2),
	OSRUNE(3),
	DEADMAN(5);
	
	private int serviceId;
	
	Realm(int id) {
		serviceId = id;
	}
	
	public int id() {
		return serviceId;
	}
	
	public boolean isPVP() {
		return this == PVP;
	}
	
	public boolean isRealism() {
		return this == REALISM;
	}
	
	public boolean isOSRune() {
		return this == OSRUNE;
	}
	
	public boolean isDeadman() {
		return this == DEADMAN;
	}
	
	public static Realm forService(int id) {
		for (Realm r : values()) {
			if (r.serviceId == id)
				return r;
		}
		
		return null;
	}
	
}
