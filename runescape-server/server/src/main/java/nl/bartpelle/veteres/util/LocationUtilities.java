package nl.bartpelle.veteres.util;

import nl.bartpelle.veteres.model.Area;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.instance.InstancedMap;

import java.util.ArrayList;

/**
 * @author Simplemind
 */
public class LocationUtilities {

    public static Tile dynamicTileFor(Tile targetTile, Tile mapCorner, InstancedMap map) {
        Tile regionCorner = new Tile((mapCorner.region() >> 8) << 6, (mapCorner.region() & 0xFF) << 6);
        int deltaX = targetTile.x - regionCorner.x;
        int deltaY = targetTile.z - regionCorner.z;
        return new Tile(map.x1 + deltaX, map.z1 + deltaY);
    }

    public static Area copyAreaToInstance(Area from, Tile mapCorner, InstancedMap map) {
        Tile low = dynamicTileFor(from.bottomLeft(), mapCorner, map);
        Tile high = dynamicTileFor(from.topRight(), mapCorner, map);
        return new Area(low.x, low.z, high.x, high.z);
    }

    public static Tile dynamicTileFor(Tile targetTile, InstancedMap map) {
        return dynamicTileFor(targetTile, targetTile, map);
    }

    public static ArrayList<Npc> copyNpcSpawnsFrom(World world, Tile from, int radius) {
        ArrayList<Npc> copy = new ArrayList<>();
        world.npcs().forEach(npc -> {
            if (from.area(radius).contains(npc.spawnTile())) {
                copy.add(npc);
            }
        });

        return copy;
    }

}
