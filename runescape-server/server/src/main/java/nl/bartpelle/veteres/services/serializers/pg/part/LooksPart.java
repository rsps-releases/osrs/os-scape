package nl.bartpelle.veteres.services.serializers.pg.part;

import nl.bartpelle.veteres.model.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by Bart on 8/10/2015.
 */
public class LooksPart implements PgJsonPart {
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		player.looks().female(resultSet.getInt("gender") == 1);
		Object[] looks = (Object[]) resultSet.getArray("looks").getArray();
		int[] looks_ = new int[7];
		for (int i = 0; i < 7; i++) {
			looks_[i] = (Integer) looks[i];
		}
		Object[] colors = (Object[]) resultSet.getArray("colors").getArray();
		int[] colors_ = new int[5];
		for (int i = 0; i < 5; i++) {
			colors_[i] = (Integer) colors[i];
		}
		player.looks().looks(looks_);
		player.looks().colors(colors_);
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection connection, boolean removeOnline) throws SQLException {
		characterUpdateStatement.setInt(11, player.looks().female() ? 1 : 0);
		characterUpdateStatement.setString(12, "{" + Arrays.stream(player.looks().looks()).mapToObj(String::valueOf).collect(Collectors.joining(",")) + "}");
		characterUpdateStatement.setString(13, "{" + Arrays.stream(player.looks().colors()).mapToObj(String::valueOf).collect(Collectors.joining(",")) + "}");
	}
	
}
