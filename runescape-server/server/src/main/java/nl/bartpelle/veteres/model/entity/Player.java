package nl.bartpelle.veteres.model.entity;

import com.google.common.base.MoreObjects;
import io.netty.channel.Channel;
import nl.bartpelle.skript.Conditions;
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars;
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest;
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.combat.CombatSounds;
import nl.bartpelle.veteres.content.combat.melee.MeleeSpecialAttacks;
import nl.bartpelle.veteres.content.mechanics.*;
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterManager;
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions;
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking;
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext;
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveGame;
import nl.bartpelle.veteres.content.npcs.pets.PetAI;
import nl.bartpelle.veteres.content.quests.QuestGuide;
import nl.bartpelle.veteres.content.skills.hunter.BirdSnaring;
import nl.bartpelle.veteres.content.skills.hunter.Chinchompas;
import nl.bartpelle.veteres.crypto.IsaacRand;
import nl.bartpelle.veteres.model.*;
import nl.bartpelle.veteres.model.entity.player.*;
import nl.bartpelle.veteres.model.entity.player.content.exchange.GrandExchange;
import nl.bartpelle.veteres.model.instance.InstancedMap;
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.model.item.Shop;
import nl.bartpelle.veteres.model.map.steroids.Direction;
import nl.bartpelle.veteres.model.map.steroids.RouteFinder;
import nl.bartpelle.veteres.net.future.ClosingChannelFuture;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.command.*;
import nl.bartpelle.veteres.script.Timer;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.services.dupewatch.DupeWatch;
import nl.bartpelle.veteres.services.financial.BMTPostback;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.services.serializers.PlayerSerializer;
import nl.bartpelle.veteres.util.*;
import nl.bartpelle.veteres.util.journal.Journal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Bart Pelle on 8/22/2014.
 */
public class Player extends Entity {
	
	
	private static final Logger logger = LogManager.getLogger(Entity.class);
	
	/**
	 * A unique ID to identify a player, even after he or she has disconnected.
	 */
	private Object id;
	
	/**
	 * The name that the player had used to log in with (not always the display name!)
	 */
	private String username;
	
	/**
	 * The name of the player, actually seen in-game.
	 */
	private String displayName;
	
	/**
	 * The player's Netty connection channel
	 */
	private Channel channel;
	
	/**
	 * The privilege level of this player.
	 */
	private Privilege privilege = Privilege.PLAYER;
	
	/**
	 * Our achieved skill levels
	 */
	private Skills skills;
	
	/**
	 * Our looks (clothes, colours, gender)
	 */
	private Looks looks;
	
	private Interfaces interfaces;
	
	/**
	 * The map which was recently sent to show
	 */
	private Tile activeMap;
	
	/**
	 * The ISAAC Random Generator for incoming packets.
	 */
	private IsaacRand inrand;
	
	/**
	 * The ISAAC Random Generator for outgoing packets.
	 */
	private IsaacRand outrand;
	
	/**
	 * A list of pending actions which are decoded at the next game cycle.
	 */
	private ConcurrentLinkedQueue<Action> pendingActions = new ConcurrentLinkedQueue<Action>();
	
	private ItemContainer inventory;
	private ItemContainer equipment;
	private ItemContainer bank;
	private ItemContainer lootingBag;
	
	private Varps varps;
	
	/**
	 * Social status, friends and ignores of this player.
	 */
	private Social social;
	
	/**
	 * The ID of the last applied migration.
	 */
	private int migration;
	
	/**
	 * Game time, in ticks played.
	 */
	private int gametime;
	
	/**
	 * Weight of the player
	 */
	private double weight;

	private IronMode ironMode = IronMode.NONE;
	private GameMode mode = GameMode.CLASSIC;
	private PremiumTier premiumMode = PremiumTier.NONE;
	private DupeWatch dupeWatch = new DupeWatch();
	
	private QuestTab questTab = new QuestTab();

	private Journal journal = Journal.MAIN;
	
	private RunEnergy runEnergy = new RunEnergy(this);
	
	private String ip;
	
	/**
	 * Unix timestamp
	 */
	private long premiumUntil = 0L;
	
	/**
	 * The character ID from the database we use.
	 */
	private int characterId;
	/**
	 * Time that mute expires
	 */
	private Timestamp mutedUntil;
	
	private Map<Integer, Integer> npcKills = new HashMap<>();
	
	/**
	 * 128-bit UUID represented as base 16 string.
	 */
	private String uuid;
	
	/**
	 * 20-byte (SHA1) base16 encoded machine ID.
	 */
	private String hwid;
	
	/**
	 * macAddress for network card.
	 */
	private String macAddress = "";
	
	/**
	 * Whether this player is having it's entire (relevant) incoming packet history logged.
	 */
	private boolean logged;
	
	/**
	 * How much this kid filled our pockets hahahahahah
	 */
	public double totalSpent;
	
	/**
	 * The default icon this player has (e.g. the dicing icon or the youtuber icon).
	 */
	private int defaultIcon;
	
	/**
	 * If true, all sent packets are discarded (not sent). Mainly used to apply things during migrations that
	 * would otherwise cause the client to crash.
	 */
	private boolean discardWrites;
	
	/**
	 * The two-factor secret key. Or null, when it's not set-up.
	 */
	private String twofactorKey;
	
	/**
	 * Helper rank is a mechanism where helpers can use simple moderator commands without too much power to keep
	 * the game clean when mods are off. They're below the moderators in hierarchy but obviously above players.
	 */
	private boolean helper;
	
	/**
	 * Extra privilege "level" over the default moderator privileges that allows one to do a little more, such
	 * as teleporting, invisibility, changing passwords and removing 2FA. Has a custom crown which is a mix
	 * between the mod and the admin crowns.
	 */
	private boolean seniorModerator;
	
	/**
	 * Special type of mute where messages still be sent as regular, but everyone else cant see them.
	 * Stops spammers from making new accounts because they have no idea they are muted.
	 */
	private boolean shadowMuted;
	
	/**
	 * Preset repository used to store and load a player's presets.
	 */
	private PresetRepository presetRepository = new PresetRepository(this);
	
	/**
	 * The player's grand exchange, including states and history.
	 */
	private GrandExchange grandExchange;
	
	/**
	 * Whether this player has enlisted themselves for Deadman using a pass.
	 */
	private boolean deadmanParticipant;
	
	/**
	 * Whether this player is a performance testing bot or not
	 */
	private boolean bot;
	
	/**
	 * Last ping
	 */
	private long lastPing = System.currentTimeMillis();
	
	private Tile remoteLocation;
	
	/**
	 * The amount of store credits the player has stored in the web panel. The credits in the game are not counted!
	 */
	private int credits;
	
	/**
	 * The account ID from the last database to which this account is linked. Can be 0 if not linked to any.
	 */
	private int linkedRefundAccount;
	
	/**
	 * The total amount of credits left to be claimed.
	 */
	private int refundCreditsRemaining;
	
	/**
	 * The amount of credits per chunk given in a 20-hour time frame.
	 */
	private int refundCreditsPerChunk;

	/**
	 * Buy Credits
	 */
	public int selectedCreditPackage, selectedPaymentMethod;
	
	public Player(Channel channel, String username, World world, Tile tile, IsaacRand inrand, IsaacRand outrand) {
		super(world, tile);
		remoteLocation = tile;
		
		this.channel = channel;
		this.inrand = inrand;
		this.outrand = outrand;
		this.username = username;
		this.displayName = RSFormatter.formatDisplayname(username);
		
		this.sync = new PlayerSyncInfo(this);
		this.skills = new Skills(this);
		this.looks = new Looks(this);
		this.interfaces = new Interfaces(this);
		this.inventory = new ItemContainer(world, 28, ItemContainer.Type.REGULAR);
		this.equipment = new ItemContainer(world, 14, ItemContainer.Type.REGULAR);
		this.bank = new ItemContainer(world, 800, ItemContainer.Type.FULL_STACKING);
		this.lootingBag = new ItemContainer(world, 28, ItemContainer.Type.REGULAR);
		this.varps = new Varps(this);
		this.social = new Social(this);
		this.grandExchange = new GrandExchange(this);
		this.bot = channel == null;
		this.discardWrites = bot;
		
		looks().update();
		
		// Extract ip
		InetSocketAddress socketAddress;
		InetAddress inetaddress = null;
		if (channel != null) {
			socketAddress = (InetSocketAddress) channel.remoteAddress();
			inetaddress = socketAddress.getAddress();
		}
		ip = inetaddress == null ? "127.0.0.1" : inetaddress.getHostAddress(); // IP address of client
		if (bot) {
			// Sets the internal activeMap variable
			write(new DisplayMap(this, true));
			id = 1; // bots don't communiate with db so this shouldn't break anything backend.
			// required for player interaction (following)
		}
	}
	
	/**
	 * No-args constructor solely for Hibernate.
	 */
	public Player() {
		super(null, null);
		bot = true;
	}
	
	public boolean muted() {
		return mutedUntil != null && mutedUntil.after(Timestamp.from(Instant.now()));
	}
	
	public void muted(Timestamp muted) {
		mutedUntil = muted;
	}
	
	public Timestamp getMutedUntil() {
		return mutedUntil;
	}
	
	public boolean jailed() {
		return (int) attribOr(AttributeKey.JAILED, 0) == 1;
	}
	
	public QuestTab questTab() {
		return questTab;
	}

	public Journal journal() {
		return journal;
	}

	public void setJournal(Journal journal) {
		this.journal = journal;
	}

	/**
	 * Sends everything required to make the user see the game.
	 */
	public void initiate() {
		skills.update();
		
		// Send simple player options
		write(new SetPlayerOption(3, false, "Follow"));
		write(new SetPlayerOption(4, false, "Trade with"));
		if (equipment.get(3) != null && equipment.get(3).id() == 10501) // Snowball
			write(new SetPlayerOption(5, true, "Pelt"));


		int worldFlag = 1;
		// Update the state for the world type. Deadman, PVP etc.
		if (world().realm().isDeadman()) {
			worldFlag = 536870913;
		} else if (world().realm().isPVP()) {
			if (attribOr(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false)) {
				worldFlag = 1;
			} else {
				worldFlag = 1; // Was 33
			}
		}
		write(new UpdateStateCustom(worldFlag));
		
		// Trigger a scripting event
		world.server().scriptRepository().triggerLogin(this);
		looks.update();
		
		// Sync varps
		varps.syncNonzero();
		varps.sync(Varp.CLAN_PRIVACY);
		varps.sync(Varp.CLIENT_SETTINGS);
		
		// Piety chivy plzzz
		varps.varbit(3909, 8);
		
		// Since recent revisions, synching the varbit for attack options is required.
		// Since the varps are by default 0, and 0 does not get synched, attack options are missing.
		varps.sync(Varp.NPC_ATTACK_PRIORITY);
		varps.sync(Varp.PLAYER_ATTACK_PRIORITY);
		
		world.server().service(PmService.class, true).ifPresent(pmService -> {
			pmService.onUserOnline(this);
		});
		
		write(new UpdateSocialStatus(VarbitAttributes.varbit(this, VarbitAttributes.VarbitInfo.PUBLIC_CHAT_STATUS.getVarbitid()),
				VarbitAttributes.varbit(this, VarbitAttributes.VarbitInfo.TRADE_STATUS.getVarbitid())));
		write(new UpdateFriendsStatus(VarbitAttributes.varbit(this, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid())));
		
		// Please gib donations :(
		world.server().service(BMTPostback.class, true).ifPresent(bmt -> bmt.acquire((Integer) id()));
		
		// Load our presets
		presetRepository.load();
		
		// Set the correct varbit for the Iron Man mode.
		if (ironMode != IronMode.NONE) {
			varps.varbit(Varbit.IRON_MODE, ironMode.ordinal());
		}

		//Load the ge offers and notify, if needed.
		grandExchange.loadOffers();
		if (grandExchange.hasOffers()) {
			grandExchange().notifyOnLogin();
		}

		// Sets display name status to 'configured'. -1 means no name.
		invokeScript(1105, 1);
	}
	
	public String name() {
		return displayName;
	}
	
	public void displayName(String n) {
		displayName = n;
	}
	
	/**
	 * @deprecated use {@link #name()} instead
	 */
	@Deprecated
	public String username() {
		return username;
	}
	
	public String ip() {
		return ip;
	}
	
	public void uuid(String uuid) {
		this.uuid = uuid;
	}
	
	public String uuid() {
		return uuid;
	}
	
	public void hwid(String hwid) {
		this.hwid = hwid;
	}
	
	public String macAddress() {
		return hwid;
	}
	
	public void macAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	
	public String hwid() {
		return hwid;
	}
	
	public void logged(boolean b) {
		logged = b;
	}
	
	public boolean logged() {
		return logged;
	}
	
	public void totalSpent(double d) {
		totalSpent = d;
	}
	
	public double totalSpent() {
		return totalSpent;
	}
	
	public void credits(int c) {
		credits = c;
	}
	
	public int credits() {
		return credits;
	}
	
	public void defaultIcon(int i) {
		defaultIcon = i;
	}
	
	public int getDefaultIcon() {
		return defaultIcon;
	}
	
	public void discardWrites(boolean b) {
		discardWrites = b;
	}
	
	public boolean helper() {
		return helper;
	}
	
	public void helper(boolean b) {
		helper = b;
	}
	
	public boolean seniorModerator() {
		return seniorModerator;
	}
	
	public void seniorModerator(boolean b) {
		seniorModerator = b;
	}
	
	public boolean shadowMuted() {
		return shadowMuted;
	}
	
	public void shadowMuted(boolean b) {
		shadowMuted = b;
	}
	
	public void twofactorKey(String key) {
		twofactorKey = key;
	}
	
	public String twofactorKey() {
		return twofactorKey;
	}
	
	public PresetRepository presetRepository() {
		return presetRepository;
	}
	
	public GrandExchange grandExchange() {
		return grandExchange;
	}
	
	public boolean deadmanParticipant() {
		return deadmanParticipant;
	}
	
	public void deadmanParticipant(boolean deadmanParticipant) {
		this.deadmanParticipant = deadmanParticipant;
	}
	
	public Tile remoteLocation() {
		return remoteLocation;
	}
	
	public void updateRemoteLocation() {
		remoteLocation = tile;
	}
	
	public boolean active() { return (bot || channel().isActive()); }
	
	public void setRefundInformation(int linkedRefundAccount, int refundCreditsRemaining, int refundCreditsPerChunk) {
		this.linkedRefundAccount = linkedRefundAccount;
		this.refundCreditsRemaining = refundCreditsRemaining;
		this.refundCreditsPerChunk = refundCreditsPerChunk;
	}
	
	public boolean hasRefundAccount() {
		return linkedRefundAccount > 0;
	}
	
	public int refundCreditsRemaining() {
		return refundCreditsRemaining;
	}
	
	public void removeRefundCredits(int sub) {
		refundCreditsRemaining -= sub;
	}
	
	public int refundCreditsPerChunk() {
		return refundCreditsPerChunk;
	}
	
	public int linkedRefundAccount() {
		return linkedRefundAccount;
	}
	
	public boolean canClaimRefund() {
		long lastClaim = attribOr(AttributeKey.LAST_REFUND_CLAIM, 0L);
		long claimDelay = (20 * 60 * 60 * 1000); // 20 hours (because 24h is inconvenient)
		
		return lastClaim == 0 || System.currentTimeMillis() >= lastClaim + claimDelay;
	}

	public long timeUntilNextRefund() {
		long lastClaim = attribOr(AttributeKey.LAST_REFUND_CLAIM, 0L);
		long claimDelay = (20 * 60 * 60 * 1000); // 20 hours (because 24h is inconvenient)

		return (lastClaim + claimDelay) - System.currentTimeMillis();
	}
	
	public void updateLastRefundClaim() {
		putattrib(AttributeKey.LAST_REFUND_CLAIM, System.currentTimeMillis());
	}

	public boolean busy() {
	    return (attribOr(AttributeKey.BUSY, false));
    }

    public void busy(boolean isBusy) {
        putattrib(AttributeKey.BUSY, isBusy);
    }
	@Override
	public void tile(Tile tile) {
		super.tile(tile);
		remoteLocation = tile;
	}
	
	public void debug(String format, Object... params) {
		if (privilege == Privilege.ADMIN) {
			if (attribOr(AttributeKey.DEBUG, false)) {
				write(new AddMessage(params.length > 0 ? String.format(format, (Object[]) params) : format));
			}
		}
	}
	
	public void colmessage(String mes, String col) {
		colmessage2(mes, col);
	}
	
	public void colmessage2(String mes, String col, Object... params) {
		message(new StringBuilder("<col=").append(col).append(">").append(mes).toString(), params);
	}
	
	public void message(String format, Object... params) {
		write(new AddMessage(params.length > 0 ? String.format(format, (Object[]) params) : format));
	}

	public void sendScroll(String title, String... lines) {
		if(interfaces.visible(275))
			interfaces.closeMain();
		interfaces.text(275, 2, title);
		int childId = 4;
		interfaces.text(275, childId++, "");
		for(String s : lines)
			interfaces.text(275, childId++, s);
		interfaces.invokeScript(917, -1, -1);
		for (int i = childId; i <= 133; i++)
			interfaces.text(275, i, "");
		interfaces.sendMain(275);
	}

	public void teleblockMessage() {
		if (!timers().has(TimerKey.TELEBLOCK))
			return;
		long millis = timers().left(TimerKey.TELEBLOCK) * 600;
		
		message("A teleport block has been cast on you. It should wear off in %d minutes, %d seconds.",
				TimeUnit.MILLISECONDS.toMinutes(millis),
				TimeUnit.MILLISECONDS.toSeconds(millis) -
						TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
		);
	}
	
	@Override
	public void stopActions(boolean cancelMoving) {
		super.stopActions(cancelMoving);
		
		if (cancelMoving) write(new ChangeMapMarker(0, 0)); //Clear map marker
		
		if (interfaces.visible(interfaces.activeRoot(), interfaces.mainComponent())) {
			interfaces.close(interfaces.activeRoot(), interfaces.mainComponent());
		}
	}
	
	@Override
	public void autoRetaliate(Entity attacker) {
		if (dead() || hp() < 1 || !pathQueue().empty())
			return;
		// Little did we know that being frozen literally stops auto retal DEAD. EVEN if the attacker is meleeing you in distance.
		// Range or magic won't retal!
		if (frozen()) {
			message("A magical force stops you from moving.");
			return;
		}
		super.autoRetaliate(attacker);
	}
	
	public void filterableMessage(String format, Object... params) {
		write(new AddMessage(params.length > 0 ? String.format(format, (Object[]) params) : format, AddMessage.Type.GAME_FILTER));
	}
	
	/**
	 * NOT the array-engine index for players/npcs. its the ACCOUNT ID IN THE DATABASE! Integer when acc loaded, username when logging in.
	 *
	 * @param id
	 */
	public void id(Object id) {
		this.id = id;
	}
	
	public Object id() {
		return id; // Temporary!
	}
	
	public void characterId(int id) {
		this.characterId = id;
	}
	
	public int characterId() {
		return characterId;
	}
	
	public ConcurrentLinkedQueue<Action> pendingActions() {
		return pendingActions;
	}
	
	public Looks looks() {
		return looks;
	}
	
	public Channel channel() {
		return channel;
	}
	
	public Skills skills() {
		return skills;
	}
	
	public void skills(Skills skills) {
		this.skills = skills;
	}
	
	public Tile activeMap() {
		return activeMap;
	}
	
	public Area activeArea() {
		if (activeMap == null) {
			return new Area(tile.x - 52, tile.z - 52, tile.x + 52, tile.z + 52, tile().level);
		}
		
		return new Area(activeMap.x, activeMap.z, activeMap.x + 104, activeMap.z + 104, activeMap.level);
	}
	
	public void activeMap(Tile t) {
		activeMap = t;
	}
	
	public boolean seesChunk(int x, int z) {
		return activeArea().contains(new Tile(x, z));
	}
	
	public IsaacRand inrand() {
		return inrand;
	}
	
	public IsaacRand outrand() {
		return outrand;
	}
	
	public Privilege privilege() {
		return privilege;
	}
	
	public void privilege(Privilege p) {
		privilege = p;
	}
	
	public Interfaces interfaces() {
		return interfaces;
	}
	
	public ItemContainer inventory() {
		return inventory;
	}
	
	public ItemContainer equipment() {
		return equipment;
	}
	
	public ItemContainer bank() {
		return bank;
	}
	
	public ItemContainer lootingBag() {
		return lootingBag;
	}
	
	public Varps varps() {
		return varps;
	}
	
	public void migration(int m) {
		migration = m;
	}
	
	public int migration() {
		return migration;
	}
	
	public int gameTime() {
		return gametime;
	}
	
	public void gameTime(int t) {
		gametime = t;
	}
	
	public IronMode ironMode() {
		return ironMode;
	}
	
	public void ironMode(IronMode mode) {
		ironMode = mode;
	}
	
	public GameMode mode() {
		return mode;
	}
	
	public void mode(GameMode mode) {
		this.mode = mode;
	}
	
	public DupeWatch dupeWatch() {
		return dupeWatch;
	}
	
	public boolean isPremiumUser() {
		return !(premiumMode.equals(PremiumTier.NONE) || premiumUntil < System.currentTimeMillis()) || privilege.equals(Privilege.ADMIN);
	}
	
	public PremiumTier premium() {
		return premiumMode;
	}
	
	public void premium(PremiumTier mode) {
		this.premiumMode = mode;
	}
	
	public long premiumUntil() {
		return premiumUntil;
	}
	
	public void premiumUntil(long l) {
		premiumUntil = l;
	}
	
	public Social social() {
		return social;
	}
	
	public RunEnergy runenergy() {
		return runEnergy;
	}
	
	@Override
	public int hp() {
		return skills.level(Skills.HITPOINTS);
	}
	
	@Override
	public int maxHp() {
		return skills.xpLevel(Skills.HITPOINTS);
	}
	
	@Override
	public void hp(int hp, int exceed) {
		skills.setLevel(Skills.HITPOINTS, Math.max(0, Math.min(Math.max(hp(), maxHp() + exceed), hp)));//max(0, 114)  -> 114= min(99+16, 119)  -> 99+16 needs to equal min(hp() so brew doesnt reset!, newval)
		//but then max(0, 99) -> 99= min(99, 105) -> the 99 would be broke by min (99 already not brewed yet)
	}
	
	@Override
	public PlayerSyncInfo sync() {
		return (PlayerSyncInfo) sync;
	}
	
	public void sound(int id) {
		write(new PlaySound(id, 0));
	}
	
	public void sound(int id, int delay) {
		write(new PlaySound(id, delay));
	}
	
	public void sound(int id, int delay, int times) {
		write(new PlaySound(id, delay, times));
	}
	
	public void invokeScript(int id, Object... args) {
		write(new InvokeScript(id, args));
	}
	
	public void forceMove(ForceMovement move) {
		sync().forceMove(move);
	}
	
	/**
	 * Unregisters this player from the world it's in.
	 */
	public void unregister() {
		world.unregisterPlayer(this);
		savePlayer(true);
		saveHighscores();
	}
	
	public void savePlayer(boolean removeOnline) {
		world.server().service(PlayerSerializer.class, true).get().savePlayer(this, removeOnline);
	}
	
	public void saveHighscores() {
		if (world.realm().isPVP()) {
			world.server().service(LoggingService.class, true).ifPresent(serv -> serv.logPvPHighscore(this));
		} else {
			world.server().service(LoggingService.class, true).ifPresent(serv -> serv.logEcoHighscore(this));
		}
	}
	
	// Used to filter ground items belonging to this player, and is a void set piece.
	final Predicate<GroundItem> isVoidSetItem() {
		return p -> p.owner() == id() && ((p.item().id() >= 8839 && p.item().id() <= 8842) || (p.item().id() >= 11663 && p.item().id() <= 11665)
				|| p.item().id() == 13072 || p.item().id() == 13073);
	}
	
	/**
	 * Dispatches a logout message, and hooks a closing future to that. Once it's flushed, the channel is closed.
	 * The player is also immediately removed from the player list.
	 */
	@SuppressWarnings("unchecked")
	public void logout() {
		try {
			stopActions(true);
		} catch (Exception e) {
			logger.error("Exception during logout => stopactions for Player '{}'", name(), e);
		}
		
		try {
			// Forfeit the stake on logout. NEED TO CHECK IF OPPONENT DEAD. If they are, don't logout for a further 5 seconds to claim the winnings.
			Staking.on_logout(this);
			Trading.on_logout(this);
		} catch (Exception e) {
			logger.error("Exception during logout => staking/trading for Player '{}'", name(), e);
		}
		
		try {
			FightCaveGame.onLogout(this);
		} catch (Exception e) {
			logger.error("Exception during logout => fight caves for Player '{}'", name(), e);
		}
		
		try {
			InfernoContext.canLogout(this);
		} catch (Exception e) {
			logger.error("Exception thrown during attempted logout within The Inferno from Player {} .", name(), e);
		}
		
		try {
			Chinchompas.onLogout(this);
			BirdSnaring.onLogout(this);
			// GravestoneMarker.removeOldGravestone(this);
		} catch (Exception e) {
			logger.error("Exception during logout => hunter for Player '{}'", name(), e);
		}
		
		try {
			BountyHunterManager.remove(this, true);
		} catch (Exception e) {
			logger.error("Exception thrown during attempted logout within bounty hunter from Player {} .", name(), e);
		}
		
		try {
			// Give back our important stuff before the update removes it all!
			if (world.ticksUntilUpdate() > 0 && world.ticksUntilUpdate() < 1000) {
				GameCommands.returnGroundItemsPreShutdown(world, this, false);
			}
		} catch (Exception e) {
			logger.error("Exception during logout => return gitems for Player '{}'", name(), e);
		}
		
		// Drop blood keys ]:D
		if (WildernessLevelIndicator.inWilderness(tile)) {
			for (int i = 0; i < 28; i++) {
				Item item = inventory.get(i);
				
				if (item != null && BloodChest.isKey(item.id())) {
					inventory.set(i, null); // Trust me, I wear a seatbelt.
				}
			}
		}
		
		try {
			// If we're in an instance, teleport to the proper tile before logging out.
			Optional<InstancedMap> active = world.allocator().active(tile);
			if (active.isPresent()) {
				InstancedMap mymap = active.get();
				if (mymap.getIdentifier().isPresent() && mymap.getIdentifier().get() == InstancedMapIdentifier.FIGHT_CAVE) {
					// Teleport the player to the real position equivilent of where we were in the instance.
					// We can then accurately place the player into an instance on login.
					int instance_local_x = tile.x - mymap.bottomLeft().x;
					int instance_local_z = tile.z - mymap.bottomLeft().z;
					teleport(2368 + instance_local_x, 5056 + instance_local_z);
				} else {
					// Teleport to the specified exit.
					teleport(mymap.exit());
				}
				
				if (mymap.deallocatesOnLogout() && mymap.isCreatedFor(this)) {
					world.allocator().deallocate(mymap);
				}
			}
			
		} catch (Exception e) {
			logger.error("Exception during logout => Cerb / Instances for Player '{}'", name(), e);
		}
		
		try {
			// Do we have a pet?
			PetAI.despawnOnLogout(this);
		} catch (Exception e) {
			logger.error("Exception during logout => Pet AI '{}'", name(), e);
		}
		
		try {
			// Remove all NPCS owned by us
			List<Npc> remove = new LinkedList<>();
			world.npcs().forEach(n -> {
				// Unchecked cast from attribmap result Obj to Tuple
				if (n != null && ((Tuple<Integer, Player>) n.attribOr(AttributeKey.OWNING_PLAYER, new Tuple<>(-1, null))).first().equals(id)) {
					remove.add(n);
				}
			});
			remove.forEach(world::unregisterNpc);
		} catch (Exception e) {
			logger.error("Exception during logout => owned Npcs for Player '{}'", name(), e);
		}
		
		if (bot) {
			PkBots.LastBotActions info = attribOr(AttributeKey.BOT_INFO, null);
			if (info != null && info.getOwner() != null) {
				info.getOwner().getBots().remove(this);
			}
		}
		
		try {
			// If we're logged in and the channel is active, begin with sending a logout message and closing the channel.
			// We use writeAndFlush here because otherwise the message won't be flushed cos of the next unregister() call.
			if (channel != null && channel.isActive()) {
				try {
					// Are we trying to world hop?
					if (attrib(AttributeKey.WORLDHOP_LOGOUTMSG) != null) {
						channel.writeAndFlush(attrib(AttributeKey.WORLDHOP_LOGOUTMSG)).addListener(new ClosingChannelFuture());
					} else {
						channel.writeAndFlush(new Logout()).addListener(new ClosingChannelFuture());
					}
				} catch (Exception e) {
					// Silenced
				}
			}
		} catch (Exception e) {
			logger.error("Exception during logout => Channel closing for Player '{}'", name(), e);
		}
		
		// Then nicely unregister the player from the game.
		unregister();
	}
	
	/**
	 * Does the block animation. Note, depending on the combat style, block animations appear at different times.
	 * For melee, the target does the block animation instantly, before a hitsplat can appear. PID does not affect melee blocking.
	 * For magic, there is no block animation AT ALL. A player can splash on the target, that is it for visually telling what happened.
	 * For range, it depends on PID. With pid, the block and hit appear at the same time. Without pid, the block is 1 tick earlier.
	 */
	@Override
	public void blockHit(Hit hit) {
		if (hit != null && hit.style() == CombatStyle.RANGE && hit.origin() != null && hit.origin() instanceof Player) {
			// range attacks trigger block animation when the attack is done, not after! Been on 07 to prove.
		} else if (!sync().hasFlag(PlayerSyncInfo.Flag.ANIMATION.value)) {
			animate(EquipmentInfo.blockAnimationFor(this));
		}
	}
	
	@Override
	public void takehitSound(Hit hit) {
		if (hit == null)
			return;
		int blocksound = CombatSounds.block_sound(this, hit.damage());
		int dmgsound = CombatSounds.damage_sound(world);
		
		//sound(blocksound);
		if (hit.damage() > 0)
			sound(dmgsound, 20, 1);
		
		if (hit.origin() != null && hit.origin() instanceof Player) {
			Player from = (Player) hit.origin();
			from.sound(blocksound);
			if (hit.damage() > 0)
				from.sound(dmgsound, 20, 1);
		}
	}
	
	// NOTE : Currently NPCs are NOT tracked. only player database-ids are tracked. TODO if you add NPC v NPC update this
	@Override
	public Optional<Integer> killer() {
		// If we don't even have any killers, then return an empty optional.
		if (damagers().isEmpty())
			return Optional.empty();
		
		// Obtain a stream set of all information tracked.
		Set<Map.Entry<Integer, PlayerDamageTracker>> trackset = damagers().entrySet()
				.stream()
				.filter(info -> System.currentTimeMillis() - getDamagerLastTime().get(info.getKey()) <= 300000)
				.filter(info -> !info.getValue().killer().name().equalsIgnoreCase(name()))
				.collect(Collectors.toSet());
		
		// Try to find the killer based on hitters
		Comparator<Map.Entry<Integer, PlayerDamageTracker>> comparator = (e1, e2) -> e2.getValue().damage().compareTo(e1.getValue().damage());
		
		// Identify the result
		Map.Entry<Integer, PlayerDamageTracker> result = trackset.stream().sorted(comparator).findFirst().orElse(null);
		
		// An anti-farming mechanic on W2 - checks if you ate or just suicided. Why suicide?
		int[] totalDamageTaken = new int[1];
		trackset.forEach(e -> totalDamageTaken[0] += e.getValue().damage());
		
		int account_id = result != null ? result.getKey() : -1;
		if (isPlayer() && account_id != -1) {
			putattrib(AttributeKey.MOST_DAM_TRACKER, new Tuple<>(totalDamageTaken[0], result.getValue()));
			return Optional.ofNullable(account_id);
		} else {
			clearattrib(AttributeKey.MOST_DAM_TRACKER);
			return Optional.empty();
		}
	}
	
	public void addKills(int kills) {
		int count = varps().varp(Varp.KILLS);
		count = count + kills;
		varps().varp(Varp.KILLS, count);
		
		saveHighscores();
	}
	
	public double getEnergyDeprecation() {
		double weight = Math.max(0, Math.min(54, getWeight())); // Capped at 54kg - where stamina affect no longer works.. for a QoL. Stamina always helpful!
		return (0.67) + weight / 100.0;
	}
	
	public double getRecoveryRate() {
		return (8.0 + (skills().level(Skills.AGILITY) / 6.0)) / 100;
	}
	
	public void setRunningEnergy(double runningEnergy, boolean send) {
		if (runningEnergy > 100) {
			runningEnergy = 100;
		} else if (runningEnergy < 0) {
			runningEnergy = 0;
		}
		
		if (runningEnergy < 1.0) {
			varps().varp(Varp.RUNNING_ENABLED, 0);
		}
		
		putattrib(AttributeKey.RUN_ENERGY, runningEnergy);
		
		int re = (int) runningEnergy;
		if (send) {
			write(SetRunEnergy.get(re));
		}
	}
	
	@Override
	public void cycle() {
		
		gametime++; // Increment ticks we've played for
		
		if (fire_logout()) {
			// logout complete, no need to process other stuff
			return;
		}
		
		// First in p process, cycle the damaged queued on us.
		// This means we can die before any Script actions (such as combat) can execute.
		super.cycle_hits(true);

		/*if (isPlayer())
		    debug(String.format("%s: pcycle (first) ->  [e-pid:%s][pk-pid:%s]: %s %s", world.cycleCount(), index, pvpPid,
					timers.has(TimerKey.COMBAT_ATTACK) ? timers().left(TimerKey.COMBAT_ATTACK) : "-", dead()));*/
		
		// Now scripts after we're sure we haven't died from incoming damage this cycle.
		world.server().scriptExecutor().cycle(Conditions.context(this));
		
		cycle_hits(false);
		
		// Decrease timers
		super.cycle();
		
		// Fire timers
		fire_timers();
		
		// DupeWatch (TM)
		dupeWatch.update(this);
		
		//Quest Tab
		journal.update(this);

		//Granite maul special queue check
		MeleeSpecialAttacks.checkGraniteMaul(this);
		
		WildernessLevelIndicator.areascycle(this);
				
		// Region enter and leave triggers
		int lastregion = attribOr(AttributeKey.LAST_REGION, -1);
		
		if (lastregion != tile.region()) {
			world.server().scriptRepository().triggerRegionExit(this, lastregion);
			world.server().scriptRepository().triggerRegionEnter(this, tile.region());
		}
		
		// Chunk enter and leave triggers (stop molesting my pretty code)
		int lastChunk = attribOr(AttributeKey.LAST_CHUNK, -1);
		if (lastChunk != tile.chunk()) {
			world.server().scriptRepository().triggerChunkExit(this, lastChunk);
			world.server().scriptRepository().triggerChunkEnter(this, tile.chunk());
		}
		
		if (attribOr(AttributeKey.WORLD_MAP, false)) {
			invokeScript(1749, tile().hash30());
		}
		
		// check if our last tile stepped on is the current.
//		if (pathQueue().lastStep() != null && !pathQueue.lastStep().equals(tile.x, tile.z)) {
//			world.server().scriptRepository().triggerWalkDestination(this);
//		}
		
		// Update last region and chunk ids
		putattrib(AttributeKey.LAST_REGION, tile.region());
		putattrib(AttributeKey.LAST_CHUNK, tile.chunk());
		
		
		if (System.currentTimeMillis() > nextSave) {
			savePlayer(false);
			nextSave = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(10 + world.random(5));
		}
	}
	
	private long nextSave = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(10);
	
	private void fire_timers() {
		try {
			timerloop:
			for (Iterator<Timer> it = timers.timers().iterator(); it.hasNext(); ) {
				Timer entry = it.next();
				if (entry != null && entry.ticks() < 1) {
					TimerKey key = entry.key();
					int oldTicks, attemptsLeft = 50; // We cap attempts to 50 to avoid bugs.
					
					while ((oldTicks = entry.ticks()) <= 0 && attemptsLeft-- >= 0) {
						// This one is hardcoded: (disabled until more testing can be done)
						if (key == TimerKey.CONNECTION_FORCE_LOGOUT && hp() > 0 && !dead() && !locked()) { // After 60s of no connection we are force logged.
							logout();
							return;
						}
						
						// Did it not get re-fired, or was the timer removed?
						boolean cont = false;
						if (entry == null || entry.ticks() == oldTicks || !timers.has(key)) {
							timers.cancel(key);
							cont = true;
						}
						
						world.server().scriptRepository().triggerTimer(this, key); // Fire event
						
						if (cont) {
							continue timerloop; // Stop firing more events.
						}
					}
					
					if (attemptsLeft < 0) {
						timers.cancel(key);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error processing timers for {}.", this, e);
		}
	}
	
	public void postcycle_dirty() {
		// Does weight need to be recomputed?
		if (inventory.dirty() || equipment.dirty()) {
			ItemWeight.calculateWeight(this);
		}
		
		// Sync inventory
		if (inventory.dirty()) {
			write(new SetItems(93, 149, 0, inventory));
			inventory.clean();
		}
		
		// Sync equipment if dirty
		if (equipment.dirty()) {
			write(new SetItems(94, equipment));
			looks.update();
			equipment.clean();
			
			// Also send the stuff required to make the weaponry panel proper
			updateWeaponInterface();
		}
		
		// Sync bank if dirty
		if (bank.dirty()) {
			write(new SetItems(95, bank));
			bank.clean();
		}
		
		//Sync looting bag if dirty
		if (lootingBag.dirty()) {
			write(new SetItems(516, lootingBag));
			lootingBag.clean();
		}
		
		// Sync skills if dirty
		skills.syncDirty();
		
		if (attribOr(AttributeKey.SHOP_DIRTY, false)) {
			Shop shop = attribOr(AttributeKey.SHOP, null);
			if (shop != null) {
				shop.refreshFor(this);
			}
			clearattrib(AttributeKey.SHOP_DIRTY);
		}
	}
	
	public double getWeight() {
		return weight;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public void updateWeaponInterface() {
		Item wep = equipment.get(EquipSlot.WEAPON);
		write(new InterfaceText(593, 1, wep == null ? "Unarmed" : wep.definition(world).name));
		write(new InterfaceText(593, 2, "Combat Lvl: " + skills.combatLevel()));
		
		// Set the varp that holds our weapon interface panel type
		int panel = wep == null ? 0 : world.equipmentInfo().weaponType(wep.id());
		varps.varp(843, panel);
	}
	
	public int calculateBaseIcon() {
		// If we have a prefixed icon, use that one.
		if (defaultIcon > 0) {
			return defaultIcon;
		}
		
		//If we're a part of the staff team.
		if (privilege().ordinal() != 0) {
			if (seniorModerator) {
				return 22;
			}
			
			return privilege().ordinal();
		}
		
		// Helper is hardcoded.
		if (helper) {
			return 11;
		}
		
		//If we are playing Ironman or Ultimate Ironman. Or... Hardcore ]:)
		if (ironMode == IronMode.REGULAR) {
			return 3;
		} else if (ironMode == IronMode.ULTIMATE) {
			return 4;
		} else if (ironMode == IronMode.HARDCORE) {
			return 14;
		}
		
		//If we've spent some mucho dinero.
		if (totalSpent >= 5000) {
			return 21; //Black image
		} else if (totalSpent >= 2500) {
			return 20; //White image
		} else if (totalSpent >= 1000) {
			return 19; // Gold image
		} else if (totalSpent >= 500) {
			return 18; // Pink image
		} else if (totalSpent >= 250) {
			return 17; // Green image
		} else if (totalSpent >= 100) {
			return 16; // Blue image
		} else if (totalSpent >= 5) {
			return 15; // Red dumb icon (pls donate more :))
		}
		
		return 0;
	}
	
	public DonationTier donationTier() {
		//If we've spent some mucho dinero.
		if (totalSpent >= 5000) {
			return DonationTier.ULTIMATE_DONATOR; // Black image
		} else if (totalSpent >= 2500) {
			return DonationTier.GRAND_MASTER_DONATOR; // White image
		} else if (totalSpent >= 1000) {
			return DonationTier.MASTER_DONATOR; // Gold image
		} else if (totalSpent >= 500) {
			return DonationTier.LEGENDARY_DONATOR; // Pink image
		} else if (totalSpent >= 250) {
			return DonationTier.EXTREME_DONATOR; // Green image
		} else if (totalSpent >= 100) {
			return DonationTier.SUPER_DONATOR; // Blue image
		} else if (totalSpent >= 5) {
			return DonationTier.DONATOR; // Red dumb icon (pls donate more :))
		}
		return DonationTier.NONE;
	}
	
	public void registerNpcKill(int npc) {
		npcKills.compute(npc, (k, v) -> v == null ? 1 : (v + 1));
	}
	
	public Map<Integer, Integer> npcKills() {
		return npcKills;
	}
	
	@Override
	public boolean isPlayer() {
		return true;
	}
	
	@Override
	public boolean isNpc() {
		return false;
	}
	
	@Override
	protected void die() {
		stopActions(true);
		lock();
		putattrib(AttributeKey.ARENA_DEATH_TICK, world().cycleCount());
		world.server().scriptExecutor().executeScript(this, Death.script);
	}
	
	@Override
	public int attackAnimation() {
		return 422;
	}
	
	public void write(Object... o) {
		if (!discardWrites) {
			if (channel.isActive()) {
				for (Object msg : o) {
					channel.write(msg);
				}
			}
		}
	}
	
	public boolean hasItem(int... ids) {
		for (int i : ids) {
			if (inventory.has(i)) return true;
			if (equipment.has(i)) return true;
			if (bank.has(i)) return true;
			if (grandExchange.has(i)) return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("id", id).add("username", username)
				.add("displayName", displayName).add("tile", tile).add("privilege", privilege).toString();
	}
	
	public boolean fire_logout() {
		boolean active = channel == null || channel.isActive();
		// Are we requested to be logged out?
		if ((Boolean) attribOr(AttributeKey.LOGOUT, false) || !active) {
			
			//Is the player in fight caves and hasn't already attempted to logout?
			int currentWave = this.attribOr(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, 0);
			boolean logoutWarningActivated = this.attribOr(AttributeKey.TZHAAR_LOGOUT_WARNING, false);
			
			//Inferno checks
			int infernoWave = this.attribOr(AttributeKey.INFERNO_SAVED_WAVE, 0);
			
			if (infernoWave != 0 && !InfernoContext.canLogout(this)) {
				return false;
			} else if (currentWave != 0 && !logoutWarningActivated) {
				FightCaveGame.attemptLogoutFightCaves(this);
				return false;
			} else if (active && ClanWars.inInstance(this)) {
				message("You cannot logout during a clan wars.");
			} else if (active && Staking.in_duel(this)) {
				message("You cannot logout during a duel.");
			} else if (active && BloodChest.hasRewardKey(this) && WildernessLevelIndicator.inWilderness(tile)) {
				message("<col=6a1a18><img=50> The binding power of the Blood Key in your inventory does not let you log out.");
				// Are we in combat? Allow logout when you are hidden in GPI (entering PIN code - you are not visible to other players)
			} else if (!timers.has(TimerKey.COMBAT_LOGOUT) && !timers.has(TimerKey.BLOODCHEST_HUNTED) && !dead() && ((boolean) attribOr(AttributeKey.NO_GPI, false) || !locked())) {
				logout();
				return true;
			} else {
				if (timers.has(TimerKey.COMBAT_LOGOUT)) {
					message("You can't log out until 10 seconds after the end of combat.");
				} else if (timers.has(TimerKey.BLOODCHEST_HUNTED)) {
					message("You're Hunted! You need to wait 60 seconds after picking up a key before logging out.");
				}
			}
			
			putattrib(AttributeKey.LOGOUT, false);
			clearattrib(AttributeKey.WORLDHOP_LOGOUTMSG);
		}
		return false;
	}
	
	@Override
	public void post_cycle_movement() {
		Player player = this;
		Tile origin = tile();
		
		// Were we teleported?
		if (sync().teleported() || !remoteLocation().equals(tile())) {
			sync().teleportMode(127);
			pathQueue.clear();
			sync.clearMovement();
			MultiwayCombat.tileChanged(player);
			player.world().server().scriptRepository().triggerWalkDestination(player);
		}
		
		// Process path
		if (!player.pathQueue().empty()) {
			if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_MOVEMENT)) {
				player.message("No movement is enabled for this duel.");
				player.pathQueue().clear();
			} else {
				PathQueue.Step walkStep = player.pathQueue().next();
				int walkDirection = PathQueue.calculateDirection(player.tile().x, player.tile().z, walkStep.x, walkStep.z);
				int runDirection = -1;
				
				// Make sure this clip tile is still valid
				boolean legal = !walkStep.clipped || RouteFinder.isLegal(player.world(), player.tile(), Direction.orthogonal(walkStep.x - player.tile().x, walkStep.z - player.tile().z), 1);
				if (!legal) {
					walkDirection = -1;
					player.pathQueue().clear();
					player.write(new ChangeMapMarker(player.tile().x, player.tile().z));
				} else {
					player.putattrib(AttributeKey.FACING_DIRECTION, walkDirection);
					player.tile(new Tile(walkStep.x, walkStep.z, player.tile().level));
					player.sync().movementMode(1);
					
					boolean running = (walkStep.type == PathQueue.StepType.FORCED_RUN || player.pathQueue().running()) && !player.pathQueue().empty() && walkStep.type != PathQueue.StepType.FORCED_WALK;
					if (!running) {
						player.runenergy().update();
					} else {
						player.runenergy().drainForMove();
						
						PathQueue.Step runStep = player.pathQueue().next();
						runDirection = PathQueue.calculateRunDirection(origin.x, origin.z, runStep.x, runStep.z);
						legal = !runStep.clipped || RouteFinder.isLegal(player.world(), player.tile(), Direction.orthogonal(runStep.x - player.tile().x, runStep.z - player.tile().z), 1);
						
						if (!legal) {
							runDirection = -1;
							player.pathQueue().clear();
							player.write(new ChangeMapMarker(player.tile().x, player.tile().z));
						} else {
							// New GPI has a thing where run mode can also take a walk step, e.g. diagonal and then pathfind
							// its way there.
							if (runDirection == -1) {
								walkDirection = PathQueue.calculateDirection(origin.x, origin.z, runStep.x, runStep.z);
							}
							
							player.putattrib(AttributeKey.FACING_DIRECTION, runDirection);
							player.tile(new Tile(runStep.x, runStep.z, player.tile().level));
							player.sync().movementMode(2);
						}
					}
					MultiwayCombat.tileChanged(player);
					player.world().server().scriptRepository().triggerWalkDestination(player);
				}

				player.sync().step(walkDirection, runDirection);
			}
		} else {
			int currentOrDefaultFaceDir = player.attribOr(AttributeKey.FACING_DIRECTION, -1);
			if (currentOrDefaultFaceDir == -1) { // Attrib doesn't exist yet.
				currentOrDefaultFaceDir = 6;
				player.putattrib(AttributeKey.FACING_DIRECTION, currentOrDefaultFaceDir);
			}
			player.runenergy().update();
		}
		
		player.sync().updateAttributeMapFlags();
		
		if (player.reqPidMoveReset) {
			pathQueue.clear();
			player.reqPidMoveReset = false;
		}
	}
	
/*	public void syncMap() {
		Player player = this;
		// Send map if necessary
		if (player.activeMap() == null) {
			Optional<InstancedMap> activeMap = player.world().allocator().active(player.tile());
			if (activeMap.isPresent()) {
				player.write(new DisplayInstancedMap(activeMap.get(), player));
			} else {
				player.write(new DisplayMap(player, false));
			}
			player.regionChanging = true;
			
			player.world().syncMap(player, null, false);
		} else {
			Area prev = player.activeArea();
			int mapx = player.activeMap().x;
			int mapz = player.activeMap().z;
			int dx = player.tile().x - mapx;
			int dz = player.tile().z - mapz;
			
			if (dx < 16 || dz < 16 || dx >= 88 || dz >= 88) { // should this be 15 & 87 ?
				Optional<InstancedMap> activeMap = player.world().allocator().active(player.tile());
				if (activeMap.isPresent()) {
					player.write(new DisplayInstancedMap(activeMap.get(), player));
				} else {
					player.write(new DisplayMap(player, false));
				}
				player.regionChanging = true;
				
				player.world().syncMap(player, prev, false);

				player.channel().flush();
			}

			if (player.activeMap().level != player.tile().level) { // Our height changed.
				player.world().syncDespawnOldHeight(player, player.activeArea());
				//player.debug("H change from "+player.activeMap().level+" to "+player.tile().level);
				// NOTE: do NOT re-sync in the _pre-update_ when the h-level has changed. Do it _post_ update
			}
		}
	}*/
	
	public boolean reqPidMoveReset;
	
	public int currency() {
		return world.currency();
	}
	
	public boolean bot() {
		return bot;
	}
	
	public long lastPing() {
		return lastPing;
	}
	
	public void lastPing(long lastPing) {
		this.lastPing = lastPing;
	}
	
}
