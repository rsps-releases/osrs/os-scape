package nl.bartpelle.veteres.services.serializers.pg.part;

import nl.bartpelle.veteres.model.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Bart on 8/10/2015.
 */
public class MigrationPart implements PgJsonPart {
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		player.migration(resultSet.getInt("migration"));
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection connection, boolean removeOnline) throws SQLException {
		characterUpdateStatement.setInt(10, player.migration());
	}
	
}
