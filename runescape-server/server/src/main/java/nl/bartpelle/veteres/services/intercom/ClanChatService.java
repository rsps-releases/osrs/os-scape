package nl.bartpelle.veteres.services.intercom;

import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.services.Service;

/**
 * Created by bart on 9/6/15.
 */
public interface ClanChatService extends Service {
	
	public void broadcast(Player player, ClanChat chat, String message, String nameOverride);
	
	public default void command(ClanChat chat, String command) {
		command(chat, command, 0);
	}
	
	public default void command(ClanChat chat, String command, int account) {
		command(chat.ownerId(), command, account);
	}
	
	public void command(int chat, String command, int account);
	
}
