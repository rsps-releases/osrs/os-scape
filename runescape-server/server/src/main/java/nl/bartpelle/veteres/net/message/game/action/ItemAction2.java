package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.interfaces.Equipment;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 5-2-2015.
 */
@PacketInfo(size = 8)
public class ItemAction2 extends ItemAction {
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		slot = buf.readUShort();
		item = buf.readULEShort();
		hash = buf.readLEInt();
		
		log(player, opcode, size, "inter=%d child=%d item=%d slot=%d", hash >> 16, hash & 0xffff, item, slot);
	}
	
	@Override
	protected int option() {
		return 1;
	}
	
	@Override
	public void process(Player player) {
		super.process(player);
		if (slot < 0 || slot > 27) return;
		// Not possible when locked
		if (player.locked() || player.dead() || !player.inventory().hasAt(slot))
			return;
		
		// Check if we used the item that we think we used.
		Item used = player.inventory().get(slot);
		if (used != null && used.id() == item) {
			player.putattrib(AttributeKey.FROM_ITEM, item);
			Equipment.equip(player, slot, used, true);
			
			player.world().server().scriptRepository().triggerItemOption2(player, item, slot); // Currently just hunter box trap laying.
		}
	}
}
