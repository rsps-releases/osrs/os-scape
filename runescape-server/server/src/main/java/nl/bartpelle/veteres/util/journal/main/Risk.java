package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.L10n;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class Risk extends JournalEntry {

    public static final Risk INSTANCE = new Risk();

    @Override
    public void send(Player player) {
        String risk = WildernessLevelIndicator.inAttackableArea(player) ? "<col=00FF00>" + L10n.format((int) player.attribOr(AttributeKey.RISKED_WEALTH, 0) +
                (int) player.attribOr(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0)) + " BM</col>" : "<col=FF0000>Not in wild</col>";
        send(player, "<img=40> Risk", risk, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        player.world().server().scriptExecutor().executeLater(player, new QuestTab.RiskedWealth(player));
    }

}