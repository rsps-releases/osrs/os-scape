package nl.bartpelle.veteres.content.areas.wilderness;

import nl.bartpelle.veteres.content.areas.clanwars.ClanWars;
import nl.bartpelle.veteres.content.areas.clanwars.FFAClanWars;
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance;
import nl.bartpelle.veteres.content.areas.instances.PVPAreas;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.events.BloodyVolcano;
import nl.bartpelle.veteres.content.mechanics.PkBots;
import nl.bartpelle.veteres.content.mechanics.Transmogrify;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics;
import nl.bartpelle.veteres.content.mechanics.deadman.safezones.DmmZones;
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterManager;
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking;
import nl.bartpelle.veteres.model.*;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Interfaces;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.command.*;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.util.GameCommands;
import nl.bartpelle.veteres.util.ItemsOnDeath;
import nl.bartpelle.veteres.util.LocationUtilities;
import nl.bartpelle.veteres.util.Varbit;
import nl.bartpelle.veteres.util.journal.main.Hotspot;
import nl.bartpelle.veteres.util.journal.main.Risk;
import nl.bartpelle.veteres.util.journal.toggles.RiskProtectionState;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Bart on 8/13/2016.
 */
public final class WildernessLevelIndicator {
	
	public static int wildernessLevel(Tile tile) {
		int z = (tile.z > 6400) ? tile.z - 6400 : tile.z;
		
		if (!(tile.x > 2941 && tile.x < 3392 && tile.z > 3524 && tile.z < 3968) && !inUndergroundWilderness(tile))
			return 0;
		
		// North of black knights fortress and more - people lure here.
		if (tile.inArea(2998, 3525, 3026, 3536) || tile.inArea(3005, 3537, 3023, 3545)
				|| tile.equals(2997, 3525) || tile.inArea(3024, 3537, 3026, 3542)
				|| tile.inArea(3027, 3525, 3032, 3530) || tile.inArea(3003, 3537, 3004, 3538)
				// And level 20, west side of wildy, trollhiem shortcut. More people lure here :)
				|| tile.inArea(2941, 3676, 2947, 3681)) {
			return 0;
		}
		
		return ((z - 3520) >> 3) + 1;
	}
	
	public static boolean inUndergroundWilderness(Tile tile) {
		int region = tile.region();
		
		// Revenant caves:
		if (region == 12701 || region == 12702 || region == 12703 || region == 12957 || region == 12958 || region == 12959) return true;
		
		return region == 12192 || region == 12193 || region == 12961 || region == 11937 || region == 12443 || region == 12190;
	}
	
	public static boolean inWilderness(Tile tile) {
		return wildernessLevel(tile) > 0;
	}
	
	public static boolean isTutorialIsland(int regionId) {
		return regionId == 12080 || regionId == 12336 || regionId == 12592 || regionId == 12079 || regionId == 12335;
	}
	
	/**
	 * If the given Entity is in a Dangerous Attackable area within a PvP instance. So, NOT in a bank which is the only safe place in a PvP instance.
	 */
	public static boolean inPvpInstanceAttackable(Entity player) {
		return (PVPAreas.inPVPArea(player) && !WildernessLevelIndicator.inPvpInstanceSafezone(player.tile()));
	}
	
	// A small custom area between 1-4 wilderness were range is disabled in an attempt to stop raggers. Note: may not be in use.
	public static boolean inRestrictedRangeZone(Tile tile) {
		return tile.inArea(3041, 3548, 3069, 3561);
	}
	
	/**
	 * Any area, such as Wilderness, dangerous Instances, FFA clan war arena which a Player can attack another Player
	 */
	public static boolean inAttackableArea(Entity player) {
		return WildernessLevelIndicator.inWilderness(player.tile()) || inPvpInstanceAttackable(player)
				|| FFAClanWars.inFFAWilderness(player) || player.<Boolean>attribOr(AttributeKey.IN_STAKE, false)
				|| (player.world().realm().isDeadman() && DmmZones.inDangerousDMM(player))
				|| (PVP_WORLD && !inPVPWorldSafeBank(player.tile()))
				|| ClanWars.INSTANCE.inInstance(player);
	}
	
	private static boolean inPVPWorldSafeBank(Tile tile) {
		// TODO add all banks lol.. maybe carbon copy of my excellent (blow me) dmm safe zones system for lots of non-square/rectangular zones
		return false;
	}
	
	public static boolean at_west_dragons(Tile tile) {
		return tile.inArea(2964, 3585, 2999, 3622);
	}
	
	public static boolean inside_pirates_hideout(Tile tile) {
		Area original = new Area(3038, 3949, 3044, 3959);
		return tile.inArea(original) || tile.inArea(LocationUtilities.copyAreaToInstance(original, MageBankInstance.getMageBankWildTileCorner(), MageBankInstance.getMageBank()));
	}
	
	public static boolean inside_axehut(Tile tile) {
		return tile.inArea(3187, 3958, 3194, 3962);
	}
	
	public static boolean inside_rouges_castle(Tile tile) {
		return tile.inArea(3275, 3922, 3297, 3946);
	}
	
	public static boolean inside_extended_pj_timer_zone(Tile tile) {
		return tile.inArea(3047, 3524, 3096, 3539);
	}
	
	public static boolean inPvpInstanceSafezone(Tile tile) {
		final boolean[] in_instance_safe = {false};
		if (PVPAreas.safearea_coords.size() > 0) {
			PVPAreas.safearea_coords.forEach((area) -> {
				if (tile.inArea(area)) {
					in_instance_safe[0] = true;
				}
			});
		}
		if (Objects.requireNonNull(MageBankInstance.getMageBankSafe()).contains(tile)) {
		    in_instance_safe[0] = true;
        }
		return tile.inArea(3091, 3488, 3098, 3499) || in_instance_safe[0];
	}
	
	/**
	 * This changes when Jagex update the interface. Interface 90 is a combination of the BH overlay, Skull overlay,
	 * PVP level overlay and level-range. They hide components depending on the situation!
	 * <p>
	 * DEADMAN: text "protection=43" and time ("2h 40m")=44
	 * Dec 31: ID 30 -> 40
	 * March 15: ID 40 -> 43
	 */
	public static int CROSSED_OUT_SKULL_CHILD_ID = 43;
	
	
	/**
	 * See #SKULL_CHILD_ID. As of rev 123 Oct 16 2016.
	 */
	public static int WILDY_LEVEL_CHILD_ID = 33;
	
	// BH world should be an opted in boolean for each player. For now (testing) it can be static
	public static boolean PVP_WORLD = false, BH_WORLD = false;
	
	/**
	 * Required to be run every server cycle. Interface and condition changes depending on the position of this player during this cycle.
	 * <p>
	 * If you're in the wilderness, shows the skull overlay.
	 * <p>
	 * If you're in a PVP world (don't have to be in the actual wilderness) shows the PVP overlay
	 * If you're in a PVP world, in a dangerous area - show the skull and level-range which you can attack
	 * If a PVP world safezone, show the 'cross out' over the skull. Interface 90 _still shows_
	 * <p>
	 * When entering _any_ sort of PVP area, show the player "Attack" option.
	 * In the duel arena, show "Fight" and "Challenge"
	 * <p>
	 * When leaving a PVP area, send "null" as option (which hides "attack")
	 *
	 * @param player The Player to run the cycle check for.
	 */
	public static void areascycle(Player player) {
		
		// Establish some things that we'll be checking multiple times. No need to call the same big check many times for the same answer!
		int wep = player.equipment().get(3) == null ? -1 : player.equipment().get(3).id();
		boolean snowball = wep == 10501;
		boolean dmmMode = player.world().realm().isDeadman();
		boolean inWildTile = inWilderness(player.tile());
		boolean inpvpinstance = PVPAreas.inPVPArea(player) || PVP_WORLD;
		boolean in_attackable_zone = dmmMode || inAttackableArea(player); // Deadman shows "Attack" option EVERYWHERE.
		boolean dmm = DmmZones.inDangerousDMM(player); // Do not include the Realm check, so it runs silently on any realm.
		// IMPORTANT: Attributes may be adjusted during this method, so this boolean also needs to update when that happens for the logic to flow right.
		boolean attackOptionVisible = player.<Boolean>attribOr(AttributeKey.ATTACK_OP, false);
		boolean inFfaPortal = FFAClanWars.inFFAMap(player);
		boolean inWildFlagged = player.<Integer>attribOr(AttributeKey.INWILD, 0) > 0;
		int wildHash = player.interfaces().whereIs(Interfaces.SKULL); // Wilderness interface visible?
		
		// Wilderness interface not currently visible.
		if (wildHash == -1) {
			// Circumstances to show widget 90.
			if (inWildTile || inpvpinstance || PVP_WORLD || dmmMode || inFfaPortal || (BountyHunterManager.hasTarget(player))) {
				player.interfaces().sendWidgetOn(90, Interfaces.InterSwitches.T);
				wildHash = player.interfaces().whereIs(Interfaces.SKULL); // Update
			}
		} else {
			// pvp and dmm worlds show face 90 all the time everywhere
			if (!PVP_WORLD && !dmmMode) {
				// Close 90 when not in the wilderness. It shouldn't be visible outside. TODO check for bhInterfaceCooldown timer
				if (!inWildTile && !inpvpinstance && !inFfaPortal && !BountyHunterManager.hasTarget(player)) {
					player.interfaces().closeById(90);
					wildHash = -1; // Update
				}
			}
		}
		
		if (Transmogrify.isTransmogrified(player)) {
			Transmogrify.hardReset(player);
		}
		
		//If the player is in the wilderness and isn't under a cycle task then add here.
		if (inWilderness(player.tile()) && BountyHunterManager.getActive() && player.attribOr(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false) == Boolean.FALSE) {
			
			if (BountyHunterManager.canAdd(player)) {
				BountyHunterManager.addToCollection(player);
			}
		}
		
		int skullVisiblilityChange = 0; // 0=no change, don't send. 1=true, 2=false.
		
		if (dmmMode) {
			boolean onTutIsland = WildernessLevelIndicator.isTutorialIsland(player.tile().region());
			// 3 possible values, 1, 2, 3 for dmm, guarded and "no pvp" - not sure where the no pvp is used. maybe tut island?
			int dmmvarbit = (((int) player.attribOr(AttributeKey.GENERAL_VARBIT1, 0) >> 22) & 3);
			//player.debug("Dmm varbit: %d and zone: %s. Tut isle:%s", dmmvarbit, dmm, onTutIsland);
			if (dmmvarbit != (dmm ? 1 : 2)) {
				player.write(new InvokeScript(1137, dmm ? 0 : onTutIsland ? 2 : 1)); // 2 = "No PvP"
				VarbitAttributes.set(player, 15, dmm ? 1 : onTutIsland ? 3 : 2);
				skullVisiblilityChange = dmm ? 1 : 2;
				if (!dmm) {
					DeadmanMechanics.triggerGuards(player);
				}
			}
		}
		
		// Is our player inside the wilderness? If so, show the wilderness level or pvp/safe zone interface!
		// Note: There is no "wilderness" on DMM worlds.
		if (!dmmMode && inWildTile) {
			if (!inWildFlagged) {
				player.putattrib(AttributeKey.INWILD, player.world().gameCycles);
				inWildFlagged = true;
				if (inpvpinstance) {
					if (wildHash != -1) {
						player.write(new InvokeScript(390, 1)); // script for pvp world level range
						player.write(new InterfaceVisibility((Interfaces.SKULL << 16) | WILDY_LEVEL_CHILD_ID, !inWildTile)); // Only show wildy level if in it.
					}
				}
				
				if (player.attribOr(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false)) {
					player.message("<col=FF0000>Note: You currently have bounty hunter toggled off therefore you won't get a target.");
					player.message("<col=FF0000>Note: Speak to the Emblem Trader at home to turn it on.");
				}
			}
		} else if (inWildFlagged) {
			// You're leaving the wilderness.
			if (inpvpinstance) {
				if (wildHash != -1) {
					player.write(new InvokeScript(390, 0)); // script for pvp world level range
					player.write(new InterfaceVisibility((Interfaces.SKULL << 16) | WILDY_LEVEL_CHILD_ID, !inWildTile)); // Only show wildy level if in it.
				}
			}
			
			player.clearattrib(AttributeKey.INWILD);
			player.clearattrib(AttributeKey.PVP_WILDY_AGGRESSION_TRACKER);
			inWildFlagged = false;
			
			if (BountyHunterManager.canRemove(player)) {
				BountyHunterManager.remove(player, false);
			}
		}
		
		// Is our player inside a PVP area? If so, let's update the interfaces and send the "attack" option!
		if (in_attackable_zone) {
			if (!attackOptionVisible) {
				
				player.putattrib(AttributeKey.ATTACK_OP, true); // Show attack option
				attackOptionVisible = true;
				
				if (wildHash != -1 && !dmmMode) { // Dmm mode sends this via other methods
					skullVisiblilityChange = 1;
				}
				
				if (inpvpinstance) {
					player.clearDamageTimes(); // clear who damaged us for the next fight.
					player.clearDamagers();
					PkBots.pvpFollowersInZone(player).ifPresent(fb -> {
						if (!player.bot() && fb.getTarget() == null) {
							fb.setTarget(player);
							//System.out.println("target set for " + fb.getZone()+" in "+fb);
						}
					});
				}
				if (player.<Boolean>attribOr(AttributeKey.CHALLENGE_OP, false)) {
					player.write(new SetPlayerOption(1, false, "null")); // Remove challenge
					player.clearattrib(AttributeKey.CHALLENGE_OP);
				}
				
				player.write(new SetPlayerOption(2, !snowball,
						(player.attribOr(AttributeKey.IN_STAKE, false)) ? "Fight" : attackOptionVisible ? "Attack" : "null")); // Show fight/attack

				// Update our carried wealth
				updateCarriedWealthProtection(player);
				Risk.INSTANCE.send(player);

				//Check our risk protection to ensure risk isn't less than protection
				RiskProtectionState.monitorRiskProtection(player);
			}
			
			//If our player left the PVP area, let's update the interfaces, attack options, and damage saving!
		} else if (attackOptionVisible) {
			
			player.write(new SetPlayerOption(2, false, "null")); // Remove attack
			player.clearattrib(AttributeKey.ATTACK_OP);
			attackOptionVisible = false;
			
			if (wildHash != -1 && !dmmMode) {
				skullVisiblilityChange = 2;
			}
			
			if (inpvpinstance) {
				player.clearDamageTimes(); // Clear who damaged us for the next fight.
				player.clearDamagers();
			}
			
			//If our player is teleblocked, let's remove it when they leave the wilderness.
			if (WildernessLevelIndicator.wildernessLevel(player.tile()) < 1) {
				if (player.timers().has(TimerKey.TELEBLOCK)) {
					player.timers().cancel(TimerKey.TELEBLOCK);
					player.timers().cancel(TimerKey.TELEBLOCK_IMMUNITY);
					player.message("The teleport block fades as you leave the wilderness...");
					player.timers().cancel(TimerKey.BLOCK_SPEC_AND_TELE);
					player.write(new SendWidgetTimer(WidgetTimer.TELEBLOCK, 0));
					Risk.INSTANCE.send(player);
				}
			}
			if (!player.bot()) {
				PkBots.pvpFollowersOnMe(player).ifPresent(fb -> {
					fb.setTarget(null);
					fb.getBots().forEach(b -> {
						b.stopActions(true);
						PlayerCombat.clearTarget(b);
					});
				});
			}
		}
		if (skullVisiblilityChange != 0) {
			//player.debug("skullVisiblilityChange: "+skullVisiblilityChange);
			player.write(new InterfaceVisibility((Interfaces.SKULL << 16) | CROSSED_OUT_SKULL_CHILD_ID, skullVisiblilityChange == 1)); // Show the skull icon
		}
		
		if (player.world().realm().isPVP()) {
			boolean in_non_plus1 = PVPAreas.in_high_risk_area(player);
			if (in_non_plus1) {
				if (!player.<Boolean>attribOr(AttributeKey.NON_PLUS1_AREA, false)) {
					player.putattrib(AttributeKey.NON_PLUS1_AREA, true);
					player.write(new UpdateStateCustom(1029));
					player.varps().varbit(Varbit.PROTECT_ITEM, 0);
					player.message("You've entered a <col=FF0000>High Risk</col> area where Protect Item will not work.");
				}
			} else {
				if (player.<Boolean>attribOr(AttributeKey.NON_PLUS1_AREA, false)) {
					player.clearattrib(AttributeKey.NON_PLUS1_AREA);
					boolean hadProtItemOn = player.varps().varbit(Varbit.PROTECT_ITEM) == 1;
					player.varps().varbit(Varbit.PROTECT_ITEM, 1); // Makes it flick on and off
					if (!hadProtItemOn) {
						player.varps().varbit(Varbit.PROTECT_ITEM, 0);
					}
					player.message("You've left the High Risk zone and can now use the Protect Item prayer.");
					player.write(new UpdateStateCustom(inpvpinstance ? 47 : 1));
				}
			}
		}
		
		// Staking disabled on DMM worlds.
		if (!dmmMode) {
			boolean challengeOpVisible = player.<Boolean>attribOr(AttributeKey.CHALLENGE_OP, false);
			if (Staking.inChallengeArea(player.tile()) || ClanWars.INSTANCE.getLOBBY().contains(player)) {
				if (!player.<Boolean>attribOr(AttributeKey.IN_STAKE, false)) {
					// Option not showing. Show it.
					if (!challengeOpVisible) {
						player.write(new SetPlayerOption(1, false, "Challenge")); // Show challenge
						player.write(new SetPlayerOption(2, false, "null")); // Remove attack
						player.putattrib(AttributeKey.CHALLENGE_OP, true);
					}
				}
			} else {
				// Remove the option if it's visibile but we've left the area.
				if (challengeOpVisible) {
					player.write(new SetPlayerOption(1, false, "null")); // Remove challenge
					player.clearattrib(AttributeKey.CHALLENGE_OP);
					
					// Before removing option #1 (attack - playeraction1) double check if we're going to overwrite the Attack option!
					if (in_attackable_zone) {
						if (attackOptionVisible) {
							player.write(new SetPlayerOption(2, snowball ? false : true,
									(player.attribOr(AttributeKey.IN_STAKE, false)) ? "Fight" : attackOptionVisible ? "Attack" : "null")); // Show fight/attack
						}
					}
				}
			}
			
			
			//Is our player at west dragons? Sometimes, we don't allow range based attacks there! Let the player know.
			if (GameCommands.westsAntiragEnabled) {
				boolean rangeonlyzone = player.<Boolean>attribOr(AttributeKey.IN_ANTIRAG_ZONE, false);
				if (at_west_dragons(player.tile())) {
					if (!rangeonlyzone) {
						player.putattrib(AttributeKey.IN_ANTIRAG_ZONE, true);
						player.message("<col=9A2EFE>You have entered a range-restricted zone.");
					}
				} else {
					if (rangeonlyzone) {
						player.clearattrib(AttributeKey.IN_ANTIRAG_ZONE);
						player.message("<col=9A2EFE>You have left a range-restricted zone.");
					}
				}
			}

			if(!PVPAreas.inPVPArea(player) && inWilderness(player.tile()) && (boolean) player.attribOr(AttributeKey.UPDATE_STATE_CUSTOM, true)) {
				player.write(new UpdateStateCustom(1));
			}

			//Hotspot tile notifier
			boolean hotspotNotification = player.<Boolean>attribOr(AttributeKey.WILDERNESS_HOTSPOT_NOTIFICATION, false);
			if(player.tile().inArea(Hotspot.ACTIVE.area)) {
				if (!hotspotNotification) {
					player.filterableMessage("<col=6a1a18><img=15> You have entered the wilderness hotspot. Blood money from kills are doubled in here!");
					player.putattrib(AttributeKey.WILDERNESS_HOTSPOT_NOTIFICATION, true);
				}
			} else {
				if(hotspotNotification) {
					player.filterableMessage("<col=6a1a18><img=15> You have left the wilderness hotspot.");
					player.putattrib(AttributeKey.WILDERNESS_HOTSPOT_NOTIFICATION, false);
				}
			}

			// Active Volcano check
			if(player.tile().inArea(new Area(3333, 3914, 3390, 3975, -1)) && BloodyVolcano.INSTANCE.getBoulder() != null) {
				BloodyVolcano.entered(player);
			} else {
				BloodyVolcano.exited(player);
			}
		}
	}
	
	/**
	 * Updates our 'carried wealth' attribute. The wealth is used in the anti-rag Risk Protection mechanic.
	 * This identifies the item which costs the most, and excludes it, as it's likely our +1 protected item.
	 * <p>
	 * This does not consider if you're skulled as not, it assumes you'll lose all but your +1
	 * If we were to add skull checks, we'd have to hook into skulling/unskulling, adding a bit more weight to the system
	 * performance, which isn't needed.
	 * <p>
	 * There are TWO 'carriedWealth' values - one which includes items lost above 20+ wild and one that doesn't.
	 * Depending on the wild level you were at when in combat, it'll decide which value to use.
	 */
	public static void updateCarriedWealthProtection(Player p) {
		ArrayList<Integer> checked = new ArrayList<>(); // Ignore multiple zero-value (non BM) items
		int value = 0; // Temp value
		
		int risked = 0, mostExpensive = 0, protectedUnder20risk = 0;
		
		for (Item item : p.inventory()) {
			if (item == null) continue; // Blood money is irrelevant
			boolean keptUnder20 = ItemsOnDeath.isRSUntradeableItem(item);
			// Skip repeated items, items always kept, buut still include untradables lost over 20 wild.
			if (checked.contains(item.id()) || (!item.rawtradable(p.world()) && !item.lostUntradable(p.world()) && !keptUnder20))
				continue;
			value = p.world().prices().getOrElse(item.id(), 0) * item.amount();
			if (value > 0) {
				if (junk_bm(item.id())) {
					checked.add(item.id());
					continue;
				}
				if (keptUnder20) {
					protectedUnder20risk += value * 0.25; // killer gets 25% of buy price in blood money above 20 wild for these items
				} else {
					risked += value;
				}
				if (value > mostExpensive) mostExpensive = value;
			} else {
				checked.add(item.id()); // Ignore this
			}
			value = 0; // Reset to use for the next
		}
		for (Item item : p.equipment()) {
			if (item == null) continue;
			boolean keptUnder20 = ItemsOnDeath.isRSUntradeableItem(item);
			// Skip repeated items, items always kept, buut still include untradables lost over 20 wild.
			if (checked.contains(item.id()) || (!item.rawtradable(p.world()) && !item.lostUntradable(p.world()) && !keptUnder20))
				continue;
			value = p.world().prices().getOrElse(item.id(), 0) * item.amount();
			if (value > 0) {
				if (junk_bm(item.id())) {
					checked.add(item.id());
					continue;
				}
				if (keptUnder20) {
					protectedUnder20risk += value * 0.25; // killer gets 25% of buy price in blood money above 20 wild for these items
				} else {
					risked += value;
				}
				if (value > mostExpensive) mostExpensive = value;
			} else {
				checked.add(item.id()); // Ignore this
			}
			value = 0; // Reset to use for the next
		}
		
		// Take away our protected +1 item to see what is left.
		if (mostExpensive > 0) {
			risked -= mostExpensive;
		}
		
		if (risked < 0)
			risked = 0;
		if (protectedUnder20risk < 0)
			protectedUnder20risk = 0;
		
		// Store values
		p.putattrib(AttributeKey.RISKED_WEALTH, risked);
		p.putattrib(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, protectedUnder20risk);
		
		// Debug
		if (p.attribOr(AttributeKey.DEBUG, false)) {
			if (protectedUnder20risk == 0)
				p.debug("Your wealth risked is %d blood money.", risked);
			else
				p.debug("Your wealth risked is %d blood money%s.", risked + protectedUnder20risk, " (" + (protectedUnder20risk) + " under 20 wilderness)");
		}
	}
	
	public static final int[] BM_JUNK = new int[]{
			11936, 12695, 12905, 11212, 2550, 12642, 19484
	};
	
	private static boolean junk_bm(int id) {
		for (int j : BM_JUNK)
			if (j == id)
				return true;
		return (id >= 9069 && id <= 9074);
	}
	
}
