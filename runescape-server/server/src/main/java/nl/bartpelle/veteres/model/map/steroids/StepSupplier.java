package nl.bartpelle.veteres.model.map.steroids;

import nl.bartpelle.veteres.model.World;

import java.util.function.Supplier;

public interface StepSupplier extends Supplier<Direction> {
	
	public boolean reached(World world);
	
}
