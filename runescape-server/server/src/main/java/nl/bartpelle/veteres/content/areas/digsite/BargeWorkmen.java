package nl.bartpelle.veteres.content.areas.digsite;

import nl.bartpelle.skript.ScriptMain;
import nl.bartpelle.veteres.script.ScriptRepository;

public class BargeWorkmen {
	
	public static final int[] IDS = new int[] {1934, 1935, 1936, 1937};
	
	private static final String[] SHOUTS = new String[] {
			"Hey I'm working here, I'm working here.",
			"Can't stop. Too busy.",
			"Ouch! That was my finger!",
			"Is it lunch break yet?",
			"This work isn't going to do itself.",
			"Wonder when I'll get paid.",
	};
	
	@ScriptMain
	public static void register(ScriptRepository r) {
		for (int id : IDS) {
			r.onNpcOption1(id, script -> {
				script.targetNpc().sync().shout(script.player().world().random(SHOUTS));
				return null;
			});
		}
	}
	
}
