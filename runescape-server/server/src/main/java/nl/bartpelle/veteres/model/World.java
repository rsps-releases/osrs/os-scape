package nl.bartpelle.veteres.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.typesafe.config.Config;
import droptables.ScalarLootTable;
import kotlin.jvm.functions.Function1;
import kotlin.ranges.IntRange;
import nl.bartpelle.skript.Script;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.content.Help;
import nl.bartpelle.veteres.content.Teleports;
import nl.bartpelle.veteres.content.interfaces.Equipment;
import nl.bartpelle.veteres.content.lottery.Lottery;
import nl.bartpelle.veteres.content.mechanics.Skulling;
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.content.mechanics.deadman.safezones.DmmZones;
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterEmblem;
import nl.bartpelle.veteres.content.npcs.pets.Pet;
import nl.bartpelle.veteres.fs.DefinitionRepository;
import nl.bartpelle.veteres.fs.MapDefinition;
import nl.bartpelle.veteres.fs.NpcDefinition;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.npc.NpcCombatInfo;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemPriceRepository;
import nl.bartpelle.veteres.model.item.Shop;
import nl.bartpelle.veteres.model.item.W2tradables;
import nl.bartpelle.veteres.model.map.Flags;
import nl.bartpelle.veteres.model.map.MapObj;
import nl.bartpelle.veteres.model.map.steroids.PathRouteFinder;
import nl.bartpelle.veteres.model.map.steroids.Route;
import nl.bartpelle.veteres.net.message.game.command.*;
import nl.bartpelle.veteres.script.Timer;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.script.TimerRepository;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static nl.bartpelle.veteres.model.Realm.PVP;

/**
 * Created by Bart Pelle on 8/22/2014.
 */
public class World {
	
	private static final Logger logger = LogManager.getLogger(World.class);
	
	private EntityList<Player> players = new EntityList<>(2048);
	private EntityList<Player> pvpShuffablePid = new EntityList<>(2048);
	
	/**
	 * object is the database account ID
	 */
	private Map<Object, Player> playerLookupMap = new HashMap<>();
	private Map<String, Player> playerNameLookupMap = new HashMap<>();
	private EntityList<Npc> npcs = new EntityList<>(0xFFFF);
	private List<GroundItem> groundItems = new LinkedList<>();
	private List<MapObj> spawnedObjs = new LinkedList<>();
	private List<MapObj> removedObjs = new LinkedList<>();
	private DefinitionRepository definitionRepository;
	private TimerRepository timers = new TimerRepository();
	private ExamineRepository examineRepository;
	private EquipmentInfo equipmentInfo;
	private ItemPriceRepository itemPriceRepository;
	private ItemWeight itemWeight;
	private GameServer server;
	private Map<Integer, Shop> shops = new HashMap<>();
	private NpcCombatInfo[] combatInfo;
	private int id;
	private String name;
	private Random random = new SecureRandom();
	private final boolean emulation;
	private boolean performance;
	private Map<Integer, ClanChat> chats = new HashMap<>();
	private InstanceAllocator instanceAllocator = new InstanceAllocator(this);
	public static int plimit = 2000;
	private boolean rankOnly;
	
	private int combatXpMult;
	private int prayerXpMult;
	private int skillingXpMult;
	public static int xpMultiplier = 50;
	public static int bmMultiplier = 2;
	public int gameCycles; // Used more accurately identify an action that happened on a cycle, regardless of PID
	private final Realm realm;
	
	private long lastMinuteScan;
	
	private int ticksUntilSystemUpdate = -1;
	
	private Config config;
	
	public World(GameServer server) {
		this.server = server;
		definitionRepository = server.definitions();
		examineRepository = new ExamineRepository(definitionRepository);
		
		// Acquire some info from config
		name = server.config().getString("world.name");
		id = server.config().getInt("world.id");
		emulation = server.config().hasPath("world.emulation") && server.config().getBoolean("world.emulation");
		rankOnly = server.config().hasPath("world.rank_only") && server.config().getBoolean("world.rank_only");
		combatXpMult = server.config().getInt("world.xprate.combat");
		prayerXpMult = server.config().getInt("world.xprate.prayer");
		skillingXpMult = server.config().getInt("world.xprate.skilling");
		
		realm = Realm.forService(server.serviceId());
		
		this.config = server.config();
	}
	
	public void postLoad() {
		loadPrices();
		
		loadEquipmentInfo();
		
		// Load npc spawns
		loadNpcCombatInfo();

		loadShops();
		itemPriceRepository.reload();
		
		loadNpcSpawns(new File("data/map/npcs"));
		logger.info("Loaded {} NPC spawns.", npcs.size());
		
		loadItemSpawns(new File("data/map/items"));
		ItemWeight.init();
		loadDrops();
		Teleports.loadTeleports();
		Help.loadHelp();
		DmmZones.loadJson();
		W2tradables.verify(this);
	}
	
	public void loadDrops() {
		if (realm == Realm.REALISM) {
			ScalarLootTable.loadAll(this, new File("data/combat/drops-realm-1-realism"));
		} else if (realm == Realm.PVP) {
			ScalarLootTable.loadAll(this, new File("data/combat/drops-realm-2-pvp"));
		} else if (realm == Realm.OSRUNE || realm == Realm.DEADMAN) {
			ScalarLootTable.loadAll(this, new File("data/combat/drops-realm-3-osrune"));
		} else if (realm.isDeadman()) {
			ScalarLootTable.loadAll(this, new File("data/combat/drops-realm-5-deadman"));
		} else {
			logger.warn("No NPC drops folder specified for Realm: " + realm);
		}
	}
	
	/**
	 * Flags items as tradable or not depending on custom conditions.
	 *
	 * @param item
	 * @return
	 */
	public boolean flagTradable(Item item) {
		
		if (realm.isPVP()) {
			//Ugly but this check below causes slayer helms to be tradable so until someone can run a query to remove them i'll keep this here.
			if ((item.id() >= 21282 && item.id() <= 21297) || item.id() == 11865 || (item.id() >= 19639 && item.id() <= 19649) || item.id() == 21264 || item.id() == 21266) {
				return false;
			}
			
			int shopId = 4;
			//All blood money untradable items should be tradable amongst players in pvp world.
			for (int index = 0; index < shop(shopId).stock().length; index++) {
				if (shop(shopId).stock()[index] != null && shop(shopId).stock()[index].id() == item.id()) {
					return true;
				}
			}
			
			shopId = 55;
/*			//Slayer shop
			for (int index = 0; index < shop(shopId).stock().length; index++) {
				if (shop(shopId).stock()[index] != null && shop(shopId).stock()[index].id() == item.id()) {
					return false;
				}
			}*/
			
			//cheaphax0r the max capes & hoods for now.
			boolean maxCape = Arrays.stream(Equipment.getMAX_CAPES()).anyMatch(cape -> cape == item.id() || (cape + 1) == item.id());
			
			//infernal cape
			if (item.id() == 21285 || item.id() == 21282) {
				maxCape = true;
			}
			
			if (maxCape) {
				return false;
			}
			
			//Emblem checking.
			if (BountyHunterEmblem.isEmblem(item)) {
				return true;
			}
			
			if (prices().containsKey(item.id()) && Pet.getPetByItem(item.id()) == null) {
				return true;
			}
		}
		
		return true;
	}
	
	public Realm realm() {
		return realm;
	}
	
	public Config config() {
		return config;
	}
	
	public void loadEquipmentInfo() {
		equipmentInfo = new EquipmentInfo(
				new File("data/list/equipment_info.json"),
				new File("data/list/renderpairs.txt"),
				new File("data/list/bonuses.json"),
				new File("data/list/weapon_types.txt"),
				new File("data/list/weapon_speeds.txt"));
	}
	
	public void loadPrices() {
		server().service(PgSqlService.class, true).ifPresent(sql -> itemPriceRepository = new ItemPriceRepository(sql, this));
	}
	
	public void update(int ticks) {
		ticksUntilSystemUpdate = ticks;
	}
	
	public int ticksUntilUpdate() {
		return ticksUntilSystemUpdate;
	}
	
	public boolean rankOnly() {
		return rankOnly;
	}
	
	public boolean performance() {
		return performance;
	}
	
	public void performance(boolean b) {
		performance = b;
	}
	
	public void loadNpcSpawns(File dir) {
		Gson gson = JGson.get();
		
		for (File spawn : dir.listFiles()) {
			if (spawn.getName().endsWith(".json")) {
				try {
					NpcSpawn[] s = gson.fromJson(new FileReader(spawn), NpcSpawn[].class);
					
					for (NpcSpawn sp : s) {
						if (sp == null)
							continue;
						
						Tile spawnTile = new Tile(sp.x, sp.z, sp.level);
						Npc npc = new Npc(sp.id, this, spawnTile);
						npc.spawnDirection(sp.dir());
						npc.walkRadius(sp.radius);
						
						if (sp.PVPWorldExclusive && realm().isPVP()) {
							registerNpc(npc);
						}
						if (sp.realismExclusive && realm().isRealism()) {
							registerNpc(npc);
						}
						if (sp.economyExclusive && realm().isOSRune()) {
							registerNpc(npc);
						}
						if (sp.deadmanExclusive && realm().isDeadman()) {
							registerNpc(npc);
						}
						
						if (!sp.realismExclusive && !sp.PVPWorldExclusive &&
								!sp.economyExclusive && !sp.deadmanExclusive) {
							registerNpc(npc);
						}
					}
				} catch (JsonParseException e) {
					throw new RuntimeException("Failed to parse npc spawn: " + spawn.getAbsolutePath() + " (" + e.getMessage() + ")");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			} else if (spawn.isDirectory()) {
				loadNpcSpawns(spawn);
			}
		}
	}
	
	public void loadItemSpawns(File dir) {
		int total = 0;
		Gson gson = JGson.get();
		for (File spawn : dir.listFiles()) {
			if (spawn.getName().endsWith(".json")) {
				try {
					ItemSpawn[] s = gson.fromJson(new FileReader(spawn), ItemSpawn[].class);
					for (ItemSpawn sp : s) {
						if (sp == null)
							continue;
						
						Tile spawnTile = new Tile(sp.x, sp.z, sp.level);
						GroundItem item = new GroundItem(this, new Item(sp.id, sp.amount), spawnTile, null);
						item.respawns(true).respawnTimer(sp.delay);
						
						if (sp.PVPWorldExclusive && realm().isPVP()) {
							spawnGroundItem(item);
						} else if (!sp.PVPWorldExclusive && !realm().isPVP()) {
							spawnGroundItem(item);
						}
					}
					total += s.length;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			} else if (spawn.isDirectory()) {
				loadItemSpawns(spawn);
			}
		}
		logger.info("Loaded {} item spawns.", total);
	}
	
	public void loadNpcCombatInfo() {
		combatInfo = new NpcCombatInfo[definitionRepository.total(NpcDefinition.class)];
		
		int total = 0;
		Gson gson = new GsonBuilder().registerTypeAdapterFactory(new GsonPropertyValidator()).create();
		File defs = new File("data/combat/npc");
		for (File def : defs.listFiles()) {
			if (def.getName().endsWith(".json")) {
				NpcCombatInfo[] s = null;
				try {
					s = gson.fromJson(new FileReader(def), NpcCombatInfo[].class);
				} catch (JsonParseException e) {
					throw new RuntimeException("Failed to parse npc combat def: " + def.getAbsolutePath() + " (" + e.getMessage() + ")");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				
				if (s == null)
					continue;
				
				for (NpcCombatInfo cbInfo : s) {
					if (cbInfo == null)
						continue;
					
					// Store original stats to restore after respawning.
					cbInfo.originalStats = cbInfo.stats.clone();
					
					// Remove slayer requirement on PvP
					if (realm.isPVP()) {
						cbInfo.slayerlvl = 1;
					}
					
					// Resolve scripts
					if (cbInfo.scripts != null) {
						cbInfo.scripts.resolve();
					}
					
					if (cbInfo.ids != null) {
						for (int i : cbInfo.ids) {
							combatInfo[i] = cbInfo;
						}
					} else {
						combatInfo[cbInfo.id] = cbInfo;
					}
				}
				total += s.length;
			}
		}
		logger.info("Loaded {} NPC combat info sheets.", total);
	}

	public void loadShops() {
		shops.clear();
		
		Gson gson = JGson.get();
		File shops = null;
		
		if (realm == Realm.OSRUNE) {
			shops = new File("data/map/shops-realm-3.json");
		} else if (realm == Realm.PVP) {
			shops = new File("data/map/shops-pvp.json");
		} else {
			shops = new File("data/map/shops.json");
		}
		
		try {
			
			Shop.Def[] s = gson.fromJson(new FileReader(shops), Shop.Def[].class);
			Item[] shopItems;
			int counter = 0;
			
			for (Shop.Def def : s) {
				if (def == null)
					continue;
				
				//Does the shop have stock? Let's grab the info.
				if (def.stock != null) {
					
					//Set the container length.
					shopItems = new Item[def.stock.length];
					
					for (int i = 0; i < shopItems.length; i++) {
						
						if (def.stock[i] == null) {
							continue;
						}
						
						if (def.stock[i].id() < 0 || def.stock[i].amount() < 0) {
							throw new IllegalStateException("Shop item ID or amount was found negative!");
						}
						
						//Assign the item.
						shopItems[i] = def.stock[i].toItem();
					}
					
					Shop shop = new Shop(def.id, def.name, this, def.stock, shopItems, def.general, def.sellBack, def.currency);
					this.shops.put(def.id, shop);
					counter++;
				}
			}
			
			logger.info("Loaded {} shops.", counter);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Map<Integer, Shop> shops() {
		return shops;
	}
	
	public Map<Integer, ClanChat> chats() {
		return chats;
	}
	
	public void restockShops(boolean fastMode) {
		shops.values().forEach(s -> s.restock(fastMode));
	}
	
	public int id() {
		return id;
	}
	
	public boolean emulation() {
		return emulation;
	}
	
	public int combatMultiplier() {
		return combatXpMult;
	}
	
	public int prayerMultiplier() {
		return prayerXpMult;
	}
	
	public int skillingMultiplier() {
		return skillingXpMult;
	}
	
	public InstanceAllocator allocator() {
		return instanceAllocator;
	}
	
	public int cycleCount() {
		return gameCycles;
	}
	
	public boolean registerPlayer(Player player) {
		int slot = players.add(player);
		
		
		if (slot == -1)
			return false;
		
		player.index(slot);
		player.pvpPid = getPvpShuffablePid().add(player);
		playerLookupMap.put(player.id(), player);
		playerNameLookupMap.put(player.name().toLowerCase(), player);
		return true;
	}
	
	public void unregisterPlayer(Player player) {
		players.remove(player);
		getPvpShuffablePid().remove(player);
		player.pvpPid = -1;
		playerLookupMap.remove(player.id());
		playerNameLookupMap.remove(player.name().toLowerCase());
		player.index(-1);//sets our PID as -1, representing this instance is no longer valid.
		
		try {
			// If status is offline, this will already have been pushed.
			if (VarbitAttributes.varbit(player, VarbitAttributes.VarbitInfo.PM_STATUS.getVarbitid()) != 2) {
				server.service(PmService.class, true).ifPresent(s -> {
					s.onUserOffline(player);
				});
			}
			
			// Clan chat removal
			ClanChat.current(player).ifPresent(cc -> cc.leave(player, true));
			
			//Log kills
			server.service(LoggingService.class, true).ifPresent(s -> {
				player.npcKills().forEach((k, v) -> {
					s.logNpcKill(player.characterId(), k, v);
				});
			});
		} catch (Exception e) {
			logger.error("Error unregistering player!", e);
		}
	}
	
	public boolean registerNpc(Npc npc) {
		int slot = npcs.add(npc);
		
		if (slot == -1)
			return false;
		
		npc.index(slot);
		npc.spawnStack = new Throwable().getStackTrace()[1].toString();
		server.scriptRepository().triggerNpcSpawn(npc);
		server.scriptRepository().triggerNpcCreate(npc);
		return true;
	}
	
	public void unregisterNpc(Npc npc) {
		npcs.remove(npc);
		npc.index(-1);
	}
	
	public void cycle() {
		timers.cycle();
		
		//Temporary minute check until a better system is created.
		if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - lastMinuteScan) >= 60) {
			players.forEach(p -> {
				if (Skulling.skulled(p)) {
					Skulling.decrementSkullCycle(p);
				}
			});

			lastMinuteScan = System.currentTimeMillis();
		}
		
		// Fire timers
		for (Iterator<Timer> it = timers.timers().iterator(); it.hasNext(); ) {
			Timer entry = it.next();

			if (entry != null && entry.ticks() < 1) {
				TimerKey key = entry.key();
				timers.cancel(key);

				server.scriptRepository().triggerWorldTimer(this, key);
			}
		}
		
		// Ground items which need synching...
		groundItems.stream().filter(g -> !g.broadcasted() && g.shouldBroadcast()).forEach(item -> {
			item.broadcasted(true);
			item.forceBroadcast(false);
			
			// See who's getting broadcasted!
			players().forEach(p -> {
				if (!p.bot() && !p.id().equals(item.owner()) && p.seesChunk(item.tile().x, item.tile().z) && Tile.sameH(p, item)) {
					p.write(new SetMapBase(p, item.tile()));
					p.write(new AddGroundItem(item));
				}
			});
		});
		
		// Ground items which need removal..
		for (Iterator<GroundItem> gi = groundItems.iterator(); gi.hasNext(); ) {
			GroundItem g = gi.next();
			if (g.shouldBeRemoved()) {
				this.despawnItem(g);
				onDespawned(g, this);
				gi.remove();
			}
		}
		
		// System update
		if (ticksUntilSystemUpdate-- == 0) {
			players.forEach(p -> {
				if (p != null) {
					try {
						GameCommands.returnGroundItemsPreShutdown(this, p, false);
					} catch (Exception e) {
						logger.error("give-back gitems on start error!", e);
					}
					try {
						p.logout();
					} catch (Exception e) {
						logger.error("Error logging player from game!", e);
					}
				}
			});
			
			Lottery.push();//Lets go ahead and save
			
			// Signal the QA control service that we can apply the patch.
			if (server.isTestServer()) {
				//server.service(QAControl.class, true).ifPresent(QAControl::patch);
			}
			
			ticksUntilSystemUpdate = 1; // Next tick we GO ALL OVER AGAIN to make sure everyone goes out.
		}
	}
	
	/**
	 * Unregisters all entities in the {@link #npcs} and {@link #players} collection.
	 *
	 * @param area the area the entities must be within bounds of.
	 */
	public void unregisterAll(Area area) {
		if (area == null) return;
		List<Npc> npcsToRemove = npcs.stream().filter(Objects::nonNull).filter(area::contains).
				collect(Collectors.toList());
		npcsToRemove.forEach(npc -> {
			npc.stopActions(true);
			unregisterNpc(npc);
		});
		List<GroundItem> itemsToRemove = groundItems.stream().filter(Objects::nonNull).filter(item -> area.contains(item.tile())).
				collect(Collectors.toList());
		itemsToRemove.forEach(this::removeGroundItem);
		
		spawnedObjs.removeIf(obj -> obj != null && area.contains(obj.tile()));
		removedObjs.removeIf(obj -> obj != null && area.contains(obj.tile()));
	}
	
	private void despawnItem(GroundItem item) {
		players().forEach(p -> {
			if (p.seesChunk(item.tile().x, item.tile().z) && Tile.sameH(p, item)) {
				if (item.broadcasted() || p.id().equals(item.owner())) {
					p.write(new SetMapBase(p, item.tile()));
					p.write(new RemoveGroundItem(item));
				}
			}
		});
	}
	
	/**
	 * Obtains an online player using their DATABASE-ID
	 *
	 * @param id
	 * @return in the playerLookupMap
	 */
	public Optional<Player> playerForId(Object id) {
		return Optional.ofNullable(playerLookupMap.get(id));
	}
	
	public Optional<Player> playerByName(String glue) {
		return Optional.ofNullable(playerNameLookupMap.get(glue.toLowerCase()));
	}
	
	public void announce(String message) {
		players.forEach(p -> {
			if (p != null) {
				p.message(message);
			}
		});
	}
	
	public void filterableAnnounce(String message) {
		players.forEach(p -> {
			if (p != null) {
				p.filterableMessage(message);
			}
		});
	}
	
	public EntityList<Player> players() {
		return players;
	}
	
	public EntityList<Npc> npcs() {
		return npcs;
	}
	
	public GameServer server() {
		return server;
	}
	
	public DefinitionRepository definitions() {
		return definitionRepository;
	}
	
	public ExamineRepository examineRepository() {
		return examineRepository;
	}
	
	public EquipmentInfo equipmentInfo() {
		return equipmentInfo;
	}
	
	public ItemPriceRepository prices() {
		return itemPriceRepository;
	}
	
	public ItemWeight itemWeight() {
		return itemWeight;
	}
	
	public String name() {
		return name;
	}
	
	public Random random() {
		return random;
	}
	
	/**
	 * @param i Maximum - INCLUSIVE!
	 * @return Integer between 1 - MAX
	 */
	public int random(int i) {
		if (i < 1) {
			return 0;
		}
		
		return random.nextInt(i + 1);
	}
	
	public double randomDouble() {
		return random.nextDouble();
	}
	
	public <T> T random(T[] i) {
		return i[random.nextInt(i.length)];
	}
	
	public int random(int[] i) {
		return i[random.nextInt(i.length)];
	}
	
	public int random(IntRange range) {
		return random.nextInt(range.getEndInclusive() - range.getStart() + 1) + range.getStart();
	}
	
	public boolean rollDie(int dieSides, int chance) {
		return random(dieSides) < chance;
	}
	
	public TimerRepository timers() {
		return timers;
	}
	
	public Shop shop(int id) {
		return shops.get(id);
	}
	
	public NpcCombatInfo combatInfo(int id) {
		return id > combatInfo.length - 1 ? null : combatInfo[id];
	}
	
	private final long serverStartupTime = System.currentTimeMillis();
	
	public long getServerStartupTime() {
		return serverStartupTime;
	}
	
	public List<GroundItem> groundItems() {
		return groundItems;
	}
	
	public String getTime() {
		return getTime(false);
	}
	
	public String getTime(boolean prefix) {
		long l = System.currentTimeMillis() - getServerStartupTime();
		long days = (l / 86400000L);
		long hours = ((l / 3600000L) % 24L);
		long minutes = ((l / 60000L) % 60L);
		long seconds = ((l / 1000L) % 60L);
		String string = "";
		if (days > 0) {
			String s = days == 1 ? " d " : " d ";//day/days
			string += days + s;
		}
		if (hours > 0) {
			String s = hours == 1 ? prefix ? " h " : " hour " : prefix ? " hrs " : " hours ";
			string += hours + s;
		}
		if (minutes > 0 && hours > 0) {
			String s = minutes == 1 ? " min " : " mins ";
			string += minutes + s;
		}
		if (minutes > 0 && hours == 0) {
			String s = minutes == 1 ? prefix ? " min " : " minute " : prefix ? " mins " : " minutes ";
			string += minutes + s;
		} else if (hours == 0) {
			string += seconds + (prefix ? " s" : " seconds");
		}
		return string;
	}
	
	public String getTimeForQuestTab() {
		long l = System.currentTimeMillis() - getServerStartupTime();
		long days = (l / 86400000L);
		long hours = ((l / 3600000L) % 24L);
		long minutes = ((l / 60000L) % 60L);
		return String.format("%02d:%02d:%02d", days, hours, minutes);
	}
	
	public void broadcast(String message) {
		broadcast(message, Privilege.PLAYER);
	}
	
	public void broadcast(String message, Privilege priv) {
		players.forEach(p -> {
			if (p.privilege().eligibleTo(priv) || (priv == Privilege.MODERATOR && p.helper())) {
				p.message(message);
			}
		});
	}
	
	public GroundItem getGroundItem(Player player, int x, int z, int level, int id) {
		return groundItems.stream().filter(g -> g.item().id() == id && g.tile().x == x && g.tile().z == z
				&& (g.broadcasted() || (g.owner() == null || player.id().equals(g.owner())))).findAny().orElse(null);
	}
	
	public boolean groundItemValid(GroundItem item) {
		return groundItems.contains(item);
	}
	
	public boolean removeGroundItem(GroundItem item) {
		boolean b = groundItems.remove(item);
		despawnItem(item);
		// Being picked up, not despawning by lifetime expiration
		return b;
	}
	
	// Despawn because the time ran out, NOT because it was picked up or called to be removed some other way.
	public static void onDespawned(GroundItem item, World world) {
		int droppedId = item.item().id();
		for (int untradId : ItemsOnDeath.RS_UNTRADABLES_LIST) {
			if (droppedId == untradId) {
				world.playerForId(item.owner()).ifPresent(p -> {
					p.message("<col=FF0000>Your " + item.item().name(p.world()) + " despawned and has been lost forever.");
				});
			}
		}
		if (item.item() != null && item.item().realPrice(world) >= (world.realm().isPVP() ? 5_000 : 500_000)) {
			world.server().service(LoggingService.class, true).ifPresent(s -> s.logGrounditemExpired(item, world));
		}
	}
	
	public GroundItem spawnGroundItem(GroundItem item) {
		// Nonstackable but more than 1?
		if (item.item().amount() > 1 && !item.item().definition(this).stackable()) {
			for (int i = 0; i < Math.min(10, item.item().amount()); i++) {
				spawnGroundItem(new GroundItem(this, new Item(item.item(), 1), item.tile(), item.owner()));
			}
			
			return item;
		}
		
		/// Try to merge
		boolean updated = false;
		GroundItem newItem = item;
		if (item.item().definition(this).stackable()) {
			for (GroundItem g : groundItems) {
				if (g.tile().equals(item.tile()) && g.item().id() == item.item().id() && (Objects.equals(g.owner(), item.owner()) || (g.owner() == null && item.owner() == null))) {
					if (!((long) g.item().amount() + (long) item.item().amount() > Integer.MAX_VALUE)) { // Amt becomes negative.
						g.item(new Item(g.item().id(), g.item().amount() + item.item().amount()));
						updated = true;
						newItem = g;
						break;
					}
				}
			}
		}
		
		if (!updated) {
			groundItems.add(item);
			
			final GroundItem finalNewItem = newItem;
			// Broadcast it.
			players.forEach(p -> {
				if (p.activeArea().contains(finalNewItem.tile()) && Tile.sameH(p, finalNewItem)) {
					// Is this an item for us?
					if (finalNewItem.owner() == null || p.id().equals(finalNewItem.owner()) || finalNewItem.broadcasted()) {
						p.write(new SetMapBase(p, finalNewItem.tile()));
						p.write(new AddGroundItem(finalNewItem));
					}
				}
			});
		}
		
		return item;
	}
	
	/**
	 * Attempts to return any spawned object for the given tile by filtering through the list
	 * of {@link #spawnedObjs}.
	 *
	 * @param tile the tile that the spawned object must reside to be returned.
	 * @return an optional containing the map obj with the same tile as the parameter, or none.
	 */
	public Optional<MapObj> getSpawnedObj(int objectId, Tile tile) {
		return spawnedObjs.stream().filter(spawned -> spawned.id() == objectId && spawned.tile()
				.equals(tile.x, tile.z)).findAny();
	}
	
	public MapObj spawnObj(MapObj obj, boolean addClip) {
		spawnedObjs.removeIf(o -> o.type() == obj.type() && o.tile().equals(obj.tile())); // TODO remove the clip of that obj
		spawnedObjs.add(obj);
		
		MapDefinition def = definitionRepository.get(MapDefinition.class, obj.tile().region());
		if (addClip && def != null) {
			def.clip(this, obj);
		}
		// Remove any previously spawned obj
		removedObjs.removeIf(o -> o.type() == obj.type() && o.tile().equals(obj.tile())); // TODO remove the clip of that obj
		
		players.forEach(p -> {
			if (p.activeArea().contains(obj.tile()) && Tile.sameH(p, obj)) {
				p.write(new SetMapBase(p, obj.tile()));
				p.write(new SpawnObject(obj));
			}
		});
		return obj;
	}
	
	public List<MapObj> spawnedObjs() {
		return spawnedObjs;
	}
	
	public List<MapObj> removedObjs() {
		return removedObjs;
	}
	
	public MapObj spawnObj(MapObj obj) {
		return spawnObj(obj, true);
	}
	
	public void removeObj(MapObj obj, boolean removeClip) {
		if (obj == null || obj.tile() == null)
			return;
		// Remove duplicates!
		removedObjs.removeIf(o -> o == null || o.tile() == null || o.type() == obj.type() && o.tile().equals(obj.tile()));
		removedObjs.add(obj);
		
		MapDefinition def = definitionRepository.get(MapDefinition.class, obj.tile().region());
		if (removeClip && def != null)
			def.unclip(this, obj);
		// Remove any previously spawned obj
		spawnedObjs.removeIf(o -> o == null || o.tile() == null || o.type() == obj.type() && o.tile().equals(obj.tile()));
		
		players.forEach(p -> {
			if (p.activeArea().contains(obj.tile()) && Tile.sameH(p, obj)) {
				p.write(new SetMapBase(p, obj.tile()));
				p.write(new RemoveObject(obj));
			}
		});
	}
	
	public void removeObj(MapObj obj) {
		removeObj(obj, true);
	}
	
	public MapObj removeObjSpawn(MapObj obj) { // TODO i'm pretty sure this is redundant. this needs refactoring.
		return removeObjSpawn(obj, false);
	}
	
	public MapObj removeObjSpawn(MapObj obj, boolean ignoreOriginal) {
		return removeObjSpawn(obj, ignoreOriginal, true);
		
	}
	
	public MapObj removeObjSpawn(MapObj obj, boolean ignoreOriginal, boolean unclip) { // TODO i'm pretty sure this is redundant. this needs refactoring.
		// TODO best way to do that is to actually check, when spawning objects, if the original
		// TODO map definition contained that obj there, if so, just remove the 'removed obj' and it'll be all good
		// TODO otherwise we end up with a load of extra object spawns internally which actually are already in the map
		// TODO themselves, which is stupid
		spawnedObjs.remove(obj);
		
		MapDefinition def = definitionRepository.get(MapDefinition.class, obj.tile().region());
		if (unclip)
			def.unclip(this, obj);
		MapObj original = objByType(obj.type(), obj.tile().x, obj.tile().z, obj.tile().level);
		if (!ignoreOriginal && original != null) {
			def.clip(this, original);
			
			players.forEach(p -> {
				if (p.activeArea().contains(original.tile()) && Tile.sameH(p, original)) {
					p.write(new SetMapBase(p, original.tile()));
					p.write(new SpawnObject(original));
				}
			});
		} else {
			players.forEach(p -> {
				if (p.activeArea().contains(obj.tile()) && Tile.sameH(p, obj)) {
					p.write(new SetMapBase(p, obj.tile()));
					p.write(new RemoveObject(obj));
				}
			});
		}
		return original;
	}
	
	public void tileGraphic(int id, Tile tile, int height, int delay) {
		players.forEach(p -> {
			if (p.activeArea().contains(tile)) {
				p.write(new SetMapBase(p, tile));
				p.write(new SendTileGraphic(id, tile, height, delay));
			}
		});
	}
	
	public void syncMap(Player player, Area previousMap, boolean levelChange) {
		Area active = player.activeArea();
		for (int x = active.x1(); x < active.x2(); x += 8) {
			for (int z = active.z1(); z < active.z2(); z += 8) {
				if (previousMap == null || !previousMap.containsClosed(new Tile(x, z)) || levelChange) { // prev map is 104x104. dont re-send already visible stuff.
					syncChunk(player, x, z);
				}
			}
		}
	}
	
	public void syncDespawnOldHeight(Player player, Area previousMap) {
		for (int x = previousMap.x1(); x < previousMap.x2(); x += 8) {
			for (int z = previousMap.z1(); z < previousMap.z2(); z += 8) {
				if (previousMap.containsClosed(new Tile(x, z))) { // prev map is 104x104. dont re-send already visible stuff.
					desyncChunk(player, x, z);
				}
			}
		}
	}
	
	private void desyncChunk(Player p, int x, int z) { // PRE UPDATE
		Area chunk = new Area(x, z, x + 7, z + 7);
		groundItems.stream().filter(g -> (g.broadcasted() || g.owner() == p.id()) && chunk.contains(g.tile()) && Tile.sameLastH(p, g)).forEach(item -> {
			p.write(new SetMapBase(p, item.tile()));
			p.write(new RemoveGroundItem(item));
		});
		for (MapObj obj : spawnedObjs) {
			if (obj != null && chunk.contains(obj.tile()) && Tile.sameLastH(p, obj)) {
				p.write(new SetMapBase(p, obj.tile()));
				p.write(new RemoveObject(obj));
			}
		}
	}
	
	private void syncChunk(Player p, int x, int z) { // POST UPDATE
		Area area = new Area(x, z, x + 7, z + 7);
		for (GroundItem item : groundItems) {
			if (item != null && area.contains(item.tile()) && Tile.sameH(p, item)) {
				// Is this an item for us?
				if (!p.id().equals(item.owner()) && !item.broadcasted()) {// Not ours, and not public yet. Bye.
					continue;
				}
				p.write(new SetMapBase(p, item.tile()));
				p.write(new AddGroundItem(item));
			}
		}
		for (MapObj obj : removedObjs) {
			if (obj != null && area.contains(obj.tile()) && Tile.sameH(p, obj)) {
				p.write(new SetMapBase(p, obj.tile()));
				p.write(new RemoveObject(obj));
			}
		}
		for (MapObj obj : spawnedObjs) {
			if (obj != null && area.contains(obj.tile()) && Tile.sameH(p, obj)) {
				p.write(new SetMapBase(p, obj.tile()));
				p.write(new SpawnObject(obj));
			}
		}
	}
	
	public void spawnSound(Tile at, int id, int delay, int radius) {
		players.forEachWithinDistance(at, radius, p -> { // TODO how far? viewport = 14 max..
			Tile base = p.activeMap();
			int relx = at.x - base.x;
			int relz = at.z - base.z;
			
			p.write(new SetMapBase(relx / 8 * 8, relz / 8 * 8));
			p.write(new SpawnSound(at, id, radius, delay));
		});
	}
	
	public void spawnProjectile(Tile from, Entity to, int gfx, int startHeight, int endHeight, int delay, int lifetime, int angle, int steepness) {
		players.forEachWithinDistance(from, 15, p -> { // TODO how far? viewport = 14 max..
			Tile base = p.activeMap();
			int relx = from.x - base.x;
			int relz = from.z - base.z;
			
			Tile origin = new Tile(relx % 8, relz % 8);
			p.write(new SetMapBase(relx / 8 * 8, relz / 8 * 8));
			p.write(new FireProjectile(origin, to, gfx, startHeight, endHeight, delay, lifetime, angle, steepness));
		});
	}
	
	public void spawnProjectile(Entity from, Entity to, int gfx, int startHeight, int endHeight, int delay, int lifetime, int angle, int steepness) {
		Tile base = from.tile();
		if (from.size() > 1) {
			base = from.tile().transform(from.size() / 2, from.size() / 2, 0);
		}
		spawnProjectile(base, to, gfx, startHeight, endHeight, delay, lifetime, angle, steepness);
	}
	
	public void spawnProjectile(Tile from, Tile to, int gfx, int startHeight, int endHeight, int delay, int lifetime, int angle, int steepness) {
		players.forEachWithinDistance(from, 15, p -> { // TODO how far? viewport = 14 max..
			Tile base = p.activeMap();
			int relx = from.x - base.x;
			int relz = from.z - base.z;
			
			Tile origin = new Tile(relx % 8, relz % 8);
			p.write(new SetMapBase(relx / 8 * 8, relz / 8 * 8));
			p.write(new FireProjectile(origin, new Tile(to.x - from.x, to.z - from.z), gfx, startHeight, endHeight, delay, lifetime, angle, steepness));
		});
	}
	
	public List<MapObj> objByTile(int x, int z, int level) {
		Optional<MapObj> removed = removedObjs.stream().filter(m -> m.tile().equals(x, z, level)).findAny();
		if (removed.isPresent())
			return null;
		
		Optional<MapObj> spawned = spawnedObjs.stream().filter(m -> m.tile().equals(x, z, level)).findAny();
		if (spawned.isPresent())
			return Collections.singletonList(spawned.get());
		MapDefinition mapdef = definitionRepository.get(MapDefinition.class, Tile.coordsToRegion(x, z)); // Can be null, don't want to operate on such.
		return mapdef == null ? null : mapdef.objs(level, x & 63, z & 63);
	}
	
	public List<MapObj> objByTile(Tile tile) {
		return objByTile(tile.x, tile.z, tile.level);
	}
	
	public MapObj objById(int id, int x, int z, int level) {
		Optional<MapObj> removed = removedObjs.stream().filter(m -> m.id() == id && m.tile().equals(x, z, level)).findAny();
		if (removed.isPresent())
			return null;
		
		Optional<MapObj> spawned = spawnedObjs.stream().filter(m -> m.id() == id && m.tile().equals(x, z, level)).findAny();
		if (spawned.isPresent())
			return spawned.get();
		MapDefinition mapdef = definitionRepository.get(MapDefinition.class, Tile.coordsToRegion(x, z)); // Can be null, don't want to operate on such.
		return mapdef == null ? null : mapdef.objById(level, x & 63, z & 63, id);
	}
	
	public MapObj objById(int id, Tile tile) {
		return objById(id, tile.x, tile.z, tile.level);
	}
	
	public MapObj anyObjInDistance(int id, Tile origin, int distanceInclusive) {
		Optional<MapObj> spawned = spawnedObjs.stream().filter(m -> m.id() == id && m.tile().distance(origin)
				<= distanceInclusive).findAny();
		if (spawned.isPresent())
			return spawned.get();
		MapDefinition mapdef = definitionRepository.get(MapDefinition.class, origin.region()); // Can be null, don't want to operate on such.
		return mapdef == null ? null : mapdef.objByIdWithinDistance(origin.level, origin.x & 63, origin.z & 63, id, distanceInclusive);
	}
	
	public MapObj objByType(int type, Tile tile) {
		return objByType(type, tile.x, tile.z, tile.level);
	}
	
	public MapObj objByType(int type, int x, int z, int level) {
		Optional<MapObj> removed = removedObjs.stream().filter(m -> m.type() == type && m.tile().equals(x, z, level)).findAny();
		if (removed.isPresent())
			return null;
		
		Optional<MapObj> spawned = spawnedObjs.stream().filter(m -> m.type() == type && m.tile().equals(x, z, level)).findAny();
		if (spawned.isPresent())
			return spawned.get();
		MapDefinition mapdef = definitionRepository.get(MapDefinition.class, Tile.coordsToRegion(x, z)); // Can be null, don't want to operate on such.
		return mapdef == null ? null : mapdef.objByType(level, x & 63, z & 63, type);
	}
	
	public List<MapObj> allObjsByType(int type, Tile t) {
		return allObjsByType(type, t.x, t.z, t.level);
	}
	
	public List<MapObj> allObjsByType(int type, int x, int z, int level) {
		Optional<MapObj> removed = removedObjs.stream().filter(m -> m.type() == type && m.tile().equals(x, z, level)).findAny();
		if (removed.isPresent())
			return Collections.emptyList();
		
		Optional<MapObj> spawned = spawnedObjs.stream().filter(m -> m.type() == type && m.tile().equals(x, z, level)).findAny();
		if (spawned.isPresent())
			return Arrays.asList(spawned.get());
		MapDefinition mapdef = definitionRepository.get(MapDefinition.class, Tile.coordsToRegion(x, z)); // Can be null, don't want to operate on such.
		//System.out.println("falling back to landscape defaults");
		if (mapdef == null) return Collections.emptyList();
		return mapdef.objs(level, x & 63, z & 63);
	}
	
	public int[][] clipAround(Tile base, int radius) {
		Tile src = base.transform(-radius, -radius, 0);
		return clipSquare(src, radius * 2 + 1);
	}
	
	public int[][] clipSquare(Tile base, int size) {
		int[][] clipping = new int[size][size];
		
		MapDefinition active = definitionRepository.get(MapDefinition.class, base.region());
		int activeId = base.region();
		
		for (int x = base.x; x < base.x + size; x++) {
			for (int z = base.z; z < base.z + size; z++) {
				int reg = Tile.coordsToRegion(x, z);
				if (reg != activeId) {
					activeId = reg;
					active = definitionRepository.get(MapDefinition.class, activeId);
				}
				
				if (active != null)
					clipping[x - base.x][z - base.z] = active.getMasks()[base.level][x & 63][z & 63]; // TODO this has -1 AOOBE
			}
		}
		
		return clipping;
	}
	
	public int clipAt(int x, int z, int level) {
		return clipAt(new Tile(x, z, level));
	}
	
	public int clipAt(Tile tile) {
		MapDefinition active = definitionRepository.get(MapDefinition.class, tile.region());
		return active == null ? 0 : active.getMasks() == null ? 0 : active.getMasks()[tile.level][tile.x & 63][tile.z & 63];
	}
	
	public int floorAt(Tile tile) {
		MapDefinition active = definitionRepository.get(MapDefinition.class, tile.region());
		return active == null ? 0 : active.getFloors()[tile.level][tile.x & 63][tile.z & 63];
	}
	
	public boolean canWalk(Tile tile) {
		return canWalk(tile, 1);
	}
	
	public boolean canWalk(Tile tile, int size) {
		return isFloorFree(tile, size);
	}
	
	public boolean isTileFree(Tile t, int size) {
		for (int tileX = t.x; tileX < t.x + size; tileX++)
			for (int tileY = t.z; tileY < t.z + size; tileY++)
				if (!isFloorFree(t) || !isWallsFree(t))
					return false;
		return true;
	}
	
	public boolean isFloorFree(Tile t, int size) {
		for (int tileX = t.x; tileX < t.x + size; tileX++)
			for (int tileY = t.z; tileY < t.z + size; tileY++)
				if (!isFloorFree(t))
					return false;
		return true;
	}
	
	public boolean isFloorFree(Tile t) {
		return (clipAt(t) & (Flags.FLOOR_BLOCKSWALK | Flags.FLOORDECO_BLOCKSWALK | Flags.OBJ)) == 0;
	}
	
	public boolean isWallsFree(Tile t) {
		return (clipAt(t) & (Flags.CORNEROBJ_NORTHEAST | Flags.CORNEROBJ_NORTHWEST | Flags.CORNEROBJ_SOUTHEAST | Flags.CORNEROBJ_SOUTHWEST | Flags.WALLOBJ_EAST | Flags.WALLOBJ_NORTH | Flags.WALLOBJ_SOUTH | Flags.WALLOBJ_WEST)) == 0;
	}
	
	public Tile randomTileAround(Tile base, int radius) {
		int[][] clip = clipSquare(base.transform(-radius, -radius, 0), radius * 2 + 1);
		
		for (int i = 0; i < 100; i++) {
			int x = random.nextInt(radius * 2 + 1), z = random.nextInt(radius * 2 + 1);
			if (clip[x][z] == 0) {
				return base.transform(x - radius, z - radius, 0);
			}
		}
		
		return base;
	}
	
	public void executeScript(Function1<Script, Object> s) {
		server.scriptExecutor().executeScript(this, s);
	}
	
	public boolean routeExists(Tile from, Tile to) {
		PathRouteFinder prf = new PathRouteFinder(from, this);
		Route req = Route.to(to, false);
		prf.path(req, from.x, from.z, from.level, 1, new LinkedList<>());
		prf.free();
		return !req.failed;
	}
	
	public EntityList<Player> getPvpShuffablePid() {
		return pvpShuffablePid;
	}
	
	public void setPvpShuffablePid(EntityList<Player> pvpShuffablePid) {
		this.pvpShuffablePid = pvpShuffablePid;
	}
	
	public int currency() {
		if (realm == PVP)
			return 13307;
		return 995;
	}
}
