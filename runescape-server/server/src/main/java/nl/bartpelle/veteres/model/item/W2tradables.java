package nl.bartpelle.veteres.model.item;

import nl.bartpelle.veteres.model.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class W2tradables {

    private static final Logger logger = LogManager.getLogger(W2tradables.class);

    private static final int[] EXPECTED_TRADABLES = new int[] {
            21820, // ether
            6808, // imbued scroll for (i) rings
            5020, // vote ticket
            13307, // bm
            6990, // dice bag
            10943, // reward token (vote ticket 2018)
            13215, // bloody token (wtf is this)
            12791, // runepouch
            4093, // mystic bottom (a freespawn item)
            4094, // noted mystic bottom
            10551, // torso
            12954, // dragon defender
            13072, // elite void
            12746, // bh t1 emblem
    };

    // picked out from various shops and arrays
    private static final int[] EXPECTED_UNTRADABLES = new int[] {
            13262, // abby pet
            21282, // infernal max hood
            11865, // slay helm
            19639, // nigga slay helm
            21264, // purple slay helm
            4155, // slayer gem
            20760, // max cape
            21776, // imbued god max cape
            21285, // infernal
    };

    // Run while server going on dev mode
    public static void verify(World world) {
        if (!world.realm().isPVP())
            return;

        for (int i : EXPECTED_TRADABLES) {
            assert new Item(i).tradable(world);
        }

        for (int i : EXPECTED_UNTRADABLES) {
            assert !new Item(i).tradable(world);
        }
        logger.info("Tradable items tests complete.");
    }
}
