package nl.bartpelle.veteres.net.message;

import io.netty.channel.Channel;

/**
 * Created by Bart Pelle on 8/22/2014.
 */
public class LoginRequestMessage {
	
	private Channel channel;
	private String username;
	private String password;
	private int[] isaacSeed;
	private int[] crcs;
	private int revision;
	private byte[] random_dat;
	private int displayMode;
	private boolean resizableInterfaces;
	private String uuid;
	private String hwid;
	private String macAddress;
	private int authpin;
	private long delayedUntil;
	private int retries;
	
	public LoginRequestMessage(Channel channel, String username, String password, int[] isaacSeed, int[] crcs,
	                           int revision, byte[] random_dat, boolean resizableInterfaces, String uuid, int authpin,
	                           String hwid, String macAddress) {
		this.channel = channel;
		this.username = username;
		this.password = password;
		this.isaacSeed = isaacSeed;
		this.crcs = crcs;
		this.revision = revision;
		this.random_dat = random_dat;
		this.resizableInterfaces = resizableInterfaces;
		this.uuid = uuid;
		this.authpin = authpin;
		this.hwid = hwid;
		this.macAddress = macAddress;
	}
	
	public String username() {
		return username;
	}
	
	public String password() {
		return password;
	}
	
	public int[] isaacSeed() {
		return isaacSeed;
	}
	
	public int[] crcs() {
		return crcs;
	}
	
	public int revision() {
		return revision;
	}
	
	public int authpin() {
		return authpin;
	}
	
	public byte[] randomDat() {
		return random_dat;
	}
	
	public String uuid() {
		return uuid;
	}
	
	public String hwid() {
		return hwid;
	}
	
	public String macAddress() {
		return macAddress;
	}
	
	public boolean resizableInterfaces() {
		return resizableInterfaces;
	}
	
	public Channel channel() {
		return channel;
	}
	
	public long delayedUntil() {
		return delayedUntil;
	}
	
	public void delayedUntil(long l) {
		delayedUntil = l;
	}
	
	public int retries() {
		return retries;
	}
	
	public void addRetry() {
		retries++;
	}
	
}
