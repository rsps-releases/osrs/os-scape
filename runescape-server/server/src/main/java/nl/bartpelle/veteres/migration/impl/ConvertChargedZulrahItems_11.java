package nl.bartpelle.veteres.migration.impl;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;

/**
 * Created by Jak on 13/10/2016.
 * <p>
 * The ability to charge Zulrah items- specifically the blowpipe and trident were release lowkey on w1/w3.
 * Even though these items don't require charges to use and never have, someone came across the feature and used it.
 * Since the charging feature was removed, people are stuck with uncharged tridents and blowpipes, which were previously unobtainable.
 * <p>
 * This scans for any empty items, and converts them to the normal permanantly charged versions.
 * The charging mechanic has since been completed and is disabled by default. It may be implemented in the future.
 */
public class ConvertChargedZulrahItems_11 implements Migration {
	
	@Override
	public int id() {
		return 11;
	}
	
	@Override
	public boolean apply(Player player) {
		// Migration removed as of migration #14 we've added venom and charging of zulrah items. No need to prune uncharged versions.
		return true;
	}
	
	private void notify(Player player, String msg) {
		player.pendingActions().add(new Action() {
			@Override
			public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
			}
			
			@Override
			public void process(Player player) {
				player.message("<col=FF0000>" + msg);
			}
		});
	}
}
