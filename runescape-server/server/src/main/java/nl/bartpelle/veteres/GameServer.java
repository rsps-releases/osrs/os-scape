package nl.bartpelle.veteres;

import com.typesafe.config.*;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.UnpooledByteBufAllocator;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.skript.ScriptExecutor;
import nl.bartpelle.veteres.fs.DefinitionRepository;
import nl.bartpelle.veteres.migration.MigrationRepository;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.item.ItemAttrib;
import nl.bartpelle.veteres.model.uid.UIDProvider;
import nl.bartpelle.veteres.model.uid.providers.SimpleUIDProvider;
import nl.bartpelle.veteres.net.ClientInitializer;
import nl.bartpelle.veteres.script.JSScripts;
import nl.bartpelle.veteres.script.ScriptRepository;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.util.HuffmanCodec;
import nl.bartpelle.veteres.util.map.MapDecryptionKeys;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.Optional;

/**
 * Created by Bart on 8/4/2014.
 */
public class GameServer {
	
	static {
		System.setProperty("log4j.configurationFile", new File("log4j2.xml").getAbsolutePath());
	}
	
	/**
	 * Logger instance for this class.
	 */
	private static final Logger logger = LogManager.getLogger(GameServer.class);
	
	/**
	 * Filestore instance
	 */
	private final DataStore fileStore;
	
	/**
	 * Netty's server bootstrap instance.
	 */
	private ServerBootstrap bootstrap;
	
	/**
	 * The {@link nl.bartpelle.veteres.net.ClientInitializer} which sets the initial pipeline of the new connections.
	 */
	private ClientInitializer connectionInitializer;
	
	/**
	 * Our game world containing all the magic.
	 */
	private final World world;
	
	/**
	 * The 'heart' of our server, better known as the thread that lives once per 600 milliseconds.
	 * Every 600ms, this thread 'pulses' and does all the logic.
	 */
	private ServerProcessor processor;
	
	/**
	 * The scripting repository.
	 */
	private ScriptRepository scriptRepository;
	
	/**
	 * The script executor
	 */
	private ScriptExecutor scriptExecutor;
	
	/**
	 * Our list of services currently loaded.
	 */
	private final List<Service> services = new LinkedList<>();
	
	/**
	 * The UID provider we use to generate unique IDs for a player.
	 */
	private UIDProvider uidProvider;
	
	/**
	 * The Huffman codec instance we use to encode and decode chat, to save bytes.
	 */
	private final HuffmanCodec huffman;
	
	/**
	 * The config instance, read from server.conf.
	 */
	private final Config config;
	
	/**
	 * The migration repository, which contains all (reflection-scanned) migrations.
	 */
	private MigrationRepository migrations;
	
	/**
	 * The id of the service we're running. This can be used to host different character files.
	 */
	private final int serviceId;
	
	private final DefinitionRepository definitions;
	
	/**
	 * Google authenticator instance
	 */
	private final GoogleAuthenticator googleauth = new GoogleAuthenticator();
	
	/**
	 * Whether this server is an instance of the QA development group. Servers on QA allow you to spawn items, but
	 * use a separate local database to avoid characters getting merged into live. The chat is also not connected in
	 * any way and QA servers utilize an IP whitelist to allow/deny users on login.
	 */
	private final boolean testServer;
	
	/**
	 * true if developer's environment, false if production (live server)
	 */
	private final boolean devServer;
	
	/**
	 * defaults to true, used as a flag for those of us who don't want console prints 24.7
	 */
	private final boolean verbose;
	
	/**
	 * defaults to 60_000 millis, used to kick players if they haven't pinged in x time
	 */
	private final long pingTimeout;
	
	/**
	 * Creates a new server instance from the passed configuration.
	 *
	 * @param config The configuration to load settings from.
	 */
	public GameServer(Config config, File store) throws Exception {
		if (!store.exists()) {
			throw new FileNotFoundException("Cannot load data store from " + store.getAbsolutePath() + ", aborting.");
		}
		
		System.setProperty("io.netty.buffer.bytebuf.checkAccessible", "false"); //Oh look at me im netty i dictate my buffers MORE THAN SADDAM HUSSEIN
		
		this.config = config;
		
		serviceId = config.hasPath("server.service") ? config.getInt("server.service") : 1;
		testServer = config.hasPath("server.test") && config.getBoolean("server.test");
		devServer = config.hasPath("server.dev") && config.getBoolean("server.dev");
		verbose = !config.hasPath("server.verbose") || config.getBoolean("server.verbose");
		pingTimeout = !config.hasPath("server.pingTimeoutMillis") ? 60_000 : config.getLong("server.pingTimeoutMillis");
		
		// Warn when using the test server
		if (testServer) {
			System.out.println("WARNING: The server is running in QA (test server) mode.");
			System.out.println("This means that regular users can perform admin commands.");
			
			Scanner s = new Scanner(System.in);
			while (true) {
				System.out.println("Please type 'test server' to proceed with server startup.");
				String line = s.nextLine();
				
				if (line.equalsIgnoreCase("test server")) {
					break;
				}
			}
			
			System.out.println("Starting server in insecure (QA/test) mode.");
		}
		
		MapDecryptionKeys.load(new File(config.getString("server.mapkeys")));
		fileStore = new DataStore(store);
		this.definitions = new DefinitionRepository(this);
		world = new World(this);
		
		// Load the "fast" services
		setupFastServices();
		
		loadScripts();
		world.postLoad();
		huffman = new HuffmanCodec(fileStore);
	}
	
	/**
	 * Starts listening on the port passed with the constructor.
	 */
	public void start() throws Exception {
		// Set up uncaught exception handler
		Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
			logger.error("Uncaught server exception in thread {}!", t, e);
		});
		
		// Load the remaining "slow" services
		setupSlowServices();
		
		// Verify data/code integrity
		TimerKey.verifyIntegrity();
		ItemAttrib.verifyIntegrity();
		
		//init script engine
		JSScripts.reload();
		
		// Load the UID provider
		setupUIDProvider();
		
		// Start the engine
		processor = new ServerProcessor(this);
		
		// Load migrations
		migrations = new MigrationRepository();
		
		// Construct bootstrap
		EventLoopGroup acceptGroup = new NioEventLoopGroup(config.getInt("net.acceptthreads"));
		EventLoopGroup ioGroup = new NioEventLoopGroup(config.getInt("net.iothreads"));
		connectionInitializer = new ClientInitializer(this);
		
		bootstrap = new ServerBootstrap();
		bootstrap.group(acceptGroup, ioGroup);
		bootstrap.channel(NioServerSocketChannel.class);
		bootstrap.childHandler(connectionInitializer);
		bootstrap.childOption(ChannelOption.CONNECT_TIMEOUT_MILLIS, 30000);
		bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
		bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
		bootstrap.option(ChannelOption.ALLOCATOR, new UnpooledByteBufAllocator(false));
		bootstrap.childOption(ChannelOption.ALLOCATOR, new UnpooledByteBufAllocator(false));
		
		System.gc();
		
		// Bind to the address/port from the configuration
		logger.info("Binding to {}:{} as realm {}{}.", config.getString("net.address"), config.getInt("net.port"), world.realm(), testServer ? " in QA mode." : "");
		bootstrap.bind(config.getString("net.address"), config.getInt("net.port")).sync().awaitUninterruptibly();
	}
	
	/**
	 * Shut down the server in a graceful method. This will first terminate the Netty server (if any is active),
	 * and then terminate the 600ms logic thread in a graceful manner.
	 */
	public void shutdown() {
		if (bootstrap != null) {
			bootstrap.config().group().shutdownGracefully();
			bootstrap.config().childGroup().shutdownGracefully();
		}
		
		if (processor != null)
			processor.terminate();
	}
	
	/**
	 * Attack the JS scripting context to the calling thread
	 */
	public void loadScripts() throws IOException {
		logger.log(Level.INFO, "Loading scripts...");
		scriptExecutor = new ScriptExecutor();
		scriptRepository = new ScriptRepository(this, scriptExecutor);
		scriptRepository.load();
	}
	
	/**
	 * Instantiate and setup any services that are provided through the configuration file.
	 */
	private void setupSlowServices() {
		ConfigList serviceDefinitions = config.getList("services");
		
		List<Service> services = new ArrayList<>();
		for (ConfigValue serv : serviceDefinitions) {
			Class<? extends Service> serviceClass = null;
			Config object = ((ConfigObject) serv).toConfig();
			
			boolean quickStart = false;
			try {
				quickStart = object.getBoolean("quickstart");
			} catch (ConfigException ignored) {
			}
			if (!quickStart) {
				logger.info("Loading service '{}'...", object.getString("class"));
				
				// Try to resolve it
				try {
					serviceClass = Class.forName(object.getString("class")).asSubclass(Service.class);
				} catch (ClassNotFoundException e) {
					logger.error("Cannot find service class '{}'.", object.getString("class"));
				} catch (ClassCastException e) {
					logger.error("Unable to cast '{}' to subtype of Service.", object.getString("class"));
				}
				
				// Have we found the class? Try to add it to our list.
				if (serviceClass != null) {
					try {
						Service service = serviceClass.newInstance();
						this.services.add(service);
						service.setup(this, object);
						services.add(service);
						
						logger.info("Loaded service '{}'.", service.getClass().getSimpleName());
					} catch (Exception e) {
						logger.error("Unable to instantiate '{}'.", object.getString("class"), e);
					}
				}
			}
		}
		
		// Start all the services we loaded
		services.forEach(Service::start);
		logger.info("Finished loading {} configured service(s).", this.services.size());
	}
	
	/**
	 * Instantiate and setup any "fast" services that are provided through the configuration file.
	 * A fast service is crucial and needs to be started ASAP example: Sql/Redis service
	 */
	private void setupFastServices() {
		ConfigList serviceDefinitions = config.getList("services");
		
		for (ConfigValue serv : serviceDefinitions) {
			Class<? extends Service> serviceClass = null;
			Config object = ((ConfigObject) serv).toConfig();
			
			boolean quickStart = false;
			try {
				quickStart = object.getBoolean("quickstart");
			} catch (ConfigException ignored) {
			}
			if (quickStart) {
				logger.info("Loading quickstart service '{}'...", object.getString("class"));
				
				// Try to resolve it
				try {
					serviceClass = Class.forName(object.getString("class")).asSubclass(Service.class);
				} catch (ClassNotFoundException e) {
					logger.error("Cannot find quickstart service class '{}'.", object.getString("class"));
				} catch (ClassCastException e) {
					logger.error("Unable to cast '{}' to subtype of Service.", object.getString("class"));
				}
				
				// Have we found the class? Try to add it to our list.
				if (serviceClass != null) {
					try {
						Service service = serviceClass.newInstance();
						service.setup(this, object);
						services.add(service);
						
						logger.info("Loaded quickstart service '{}'.", service.getClass().getSimpleName());
					} catch (Exception e) {
						logger.error("Unable to instantiate '{}'.", object.getString("class"), e);
					}
				}
			}
		}
		
		// Start all the services we loaded
		services.forEach(Service::start);
		logger.info("Finished loading {} configured quickstart service(s).", services.size());
	}
	
	/**
	 * Attempts to set up a UID provider as configured in the json configuration file.
	 */
	private void setupUIDProvider() {
		uidProvider = new SimpleUIDProvider(this);
		
		try {
			Class<?> untyped = Class.forName(config.getString("server.uidprovider"));
			Class<? extends UIDProvider> providerClass = untyped.asSubclass(UIDProvider.class);
			uidProvider = providerClass.getConstructor(GameServer.class).newInstance(this);
			
			logger.info("Using {} as UID provider.", uidProvider.getClass().getSimpleName());
		} catch (Exception e) {
			logger.warn("Could not properly initialize UID provider of choice, falling back to SimpleUIDProvider.");
			logger.warn("Reason: {}", e.getCause().toString());
		}
	}
	
	/**
	 * Finds a service based on its type.
	 *
	 * @param serviceType   The class of the service to look for. The class must be a subclass of Service.
	 * @param allowSubclass
	 * @return An optional holding either nothing if there was no such service active, or with the service.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Service> Optional<T> service(Class<? extends T> serviceType, boolean allowSubclass) {
		if (allowSubclass) {
			return (Optional<T>) services.stream().filter(s -> serviceType.isAssignableFrom(s.getClass())).findAny();
		}
		
		return (Optional<T>) services.stream().filter(s -> s.getClass() == serviceType).findAny();
	}
	
	/**
	 * Tries to resolve a service by its type, and checks if the service is alive and running properly.
	 *
	 * @param serviceType The class of the service to check.
	 * @return <code>true</code> if the service both exists and is alive, <code>false</code> if not.
	 */
	public boolean serviceAlive(Class<? extends Service> serviceType) {
		Optional<Service> s = service(serviceType, false);
		return s.isPresent() && s.get().isAlive();
		
	}
	
	public DataStore store() {
		return fileStore;
	}
	
	public World world() {
		return world;
	}
	
	public ClientInitializer initializer() {
		return connectionInitializer;
	}
	
	public ServerProcessor processor() {
		return processor;
	}
	
	public ScriptRepository scriptRepository() {
		return scriptRepository;
	}
	
	public ScriptExecutor scriptExecutor() {
		return scriptExecutor;
	}
	
	public HuffmanCodec huffman() {
		return huffman;
	}
	
	public Config config() {
		return config;
	}
	
	public UIDProvider uidProvider() {
		return uidProvider;
	}
	
	public MigrationRepository migrations() {
		return migrations;
	}
	
	public int serviceId() {
		return serviceId;
	}
	
	public DefinitionRepository definitions() {
		return definitions;
	}
	
	public GoogleAuthenticator authenticator() {
		return googleauth;
	}
	
	public boolean isTestServer() {
		return testServer;
	}
	
	public boolean isDevServer() {
		return devServer;
	}
	
	public boolean isVerbose() {
		return verbose;
	}
	
	public long pingTimeout() {
		return pingTimeout;
	}
	
	public static void main(String[] args) {
		logger.info("Starting OS-Scape server...");
		
		try {
			Config c = ConfigFactory.systemProperties().withFallback(ConfigFactory.parseFileAnySyntax(new File("server.conf")));
			new GameServer(c, new File(c.getString("server.filestore"))).start();
		} catch (Exception e) {
			logger.fatal("Server has died unexpectedly", e);
		}
	}
	
}
