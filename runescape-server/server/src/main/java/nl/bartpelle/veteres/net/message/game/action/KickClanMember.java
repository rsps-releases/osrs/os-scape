package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.services.intercom.ClanChatService;

/**
 * Created by Bart on 12/11/2015.
 */
@PacketInfo(size = -1)
public class KickClanMember implements Action {
	
	private String name;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		name = buf.readString();
	}
	
	@Override
	public void process(Player player) {
		ClanChat.current(player).ifPresent(chat -> {
			if (chat.canKick((Integer) player.id())) {
				chat.members().stream().filter(f -> f != null && f.name.equalsIgnoreCase(name)).findFirst().ifPresent(friend -> {
					if (friend.id == chat.ownerId()) {
						player.message(friend.name() + " owns this clan chat, you can't kick them!");
						return;
					}
					player.world().server().service(ClanChatService.class, true).ifPresent(cc -> {
						cc.command(chat, "kick", friend.id);
					});
				});
			}
		});
	}
	
}
