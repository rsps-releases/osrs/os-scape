package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.ClientCrashLog;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.util.HuffmanCodec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Jak on 10/12/2016.
 */
@PacketInfo(size = -2)
public class ClientCrash implements Action {
	
	private static final Logger logger = LogManager.getLogger(ClientCrash.class);
	
	/**
	 * type 1, 2 = npc sync
	 * 4 = force crashed by command
	 */
	private int crashType;
	private int len;
	private byte[] data;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		crashType = buf.readByte();
		
		len = buf.readCompact();
		data = new byte[buf.get().readableBytes()];
		buf.get().readBytes(data);
	}
	
	@Override
	public void process(Player player) {
		
		try {
			// Decode huffman data
			byte[] stringData = new byte[768];
			HuffmanCodec codec = player.world().server().huffman();
			codec.decode(data, stringData, 0, 0, len);
			String message = new String(stringData, 0, len);
			
			ClientCrashLog.append(player.name(), message, player.tile().toStringSimple());
			if (crashType != 8) { // A silent non-deadly exception, client can still be played.
				logger.error("Client crash exception ID " + crashType + " for " + player.name() + " at " + player.tile().toStringSimple() + " : " + message);
			}
		} catch (Exception e) {
			System.err.println("error with crash report huffman unpacking / logging");
		}
		
		// System.err.println("Remote user clientcrash! Type "+crashType+" for "+player.name()+" at "+player.tile().toStringSimple()+". See ::crashlog");
		// System.err.printf("Crash: '%s'%n", message);
		
		// type 4 is caused by a command by testing. don't let people abuse this!
		if (!(crashType == 4 && !player.privilege().eligibleTo(Privilege.ADMIN))) {
			
			// Stop immune to combat and stop all damage.
			//player.timers().extendOrRegister(TimerKey.COMBAT_IMMUNITY, 150);
			// player.lockNoDamage(); // Stop damage
			
			// Repeatedly say we've crashed to notify those around is. Nvm this is fucking spam
			//player.world().server().scriptExecutor().executeScript(player, new DelayedLogoutRequest(player));
		}
	}
}
