package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;
import nl.bartpelle.veteres.util.journal.JournalCategory;

import java.util.List;

public class SendJournal extends Command {

	private int type;
	private List<JournalCategory> categories;

	public SendJournal(int type, List<JournalCategory> categories) {
		this.type = type;
		this.categories = categories;
	}
	
	@Override
	protected RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(255)).packet(84).writeSize(RSBuffer.SizeType.SHORT); //6 for entry

		buffer.writeByte(type);

		for(JournalCategory category : categories) {
			buffer.writeString(category.name);
			buffer.writeCompact(category.count);
		}
		
		return buffer;
	}
}
