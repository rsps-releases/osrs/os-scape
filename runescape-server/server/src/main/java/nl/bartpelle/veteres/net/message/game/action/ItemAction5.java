package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.DropItem;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 5-2-2015.
 */
@PacketInfo(size = 8)
public class ItemAction5 extends ItemAction {
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		hash = buf.readIntV1();
		slot = buf.readULEShort();
		item = buf.readUShortA();
		
		log(player, opcode, size, "inter=%d child=%d item=%d slot=%d", hash >> 16, hash & 0xffff, item, slot);
	}
	
	@Override
	protected int option() {
		return 4;
	}
	
	@Override
	public void process(Player player) { // Drop item
		super.process(player);
		if (!player.locked() && !player.dead()) {
			if ((hash >> 16) == 149 && (hash & 0xffff) == 0) { // Inventory
				if (slot < 0 || slot > 27) return;
				Item item = player.inventory().get(slot);
				if (item != null && item.id() == this.item) {
					if (!player.world().server().scriptRepository().triggerItemOption5(player, item.id(), slot, hash)) {
						player.putattrib(AttributeKey.ITEM_ID, item.id());
						player.putattrib(AttributeKey.ITEM_SLOT, slot);
						player.putattrib(AttributeKey.FROM_ITEM, item);
						player.world().server().scriptExecutor().executeScript(player, DropItem.script);
					}
				}
			} else {
				player.world().server().scriptRepository().triggerItemOption5(player, item, slot, hash);
			}
		}
	}
}
