package nl.bartpelle.veteres.model;

import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.journal.Journal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bart on 6/17/2016.
 */
public class PresetRepository {
	
	private static final int FREE_SLOTS = 5;
	
	private Player player;
	private Map<String, Preset> presets = new HashMap<>();
	private boolean loaded = false;
	
	public PresetRepository(Player player) {
		this.player = player;
	}
	
	public Preset get(String id) {
		return presets.get(id.toLowerCase());
	}
	
	public int used() {
		return presets.size();
	}
	
	public void put(String name, Preset preset) {
		presets.put(name.toLowerCase(), preset);
	}
	
	public boolean has(String name) {
		return presets.containsKey(name.toLowerCase());
	}
	
	public Map<String, Preset> presets() {
		return presets;
	}
	
	public void save(Preset preset) {
		player.world().server().service(PgSqlService.class, true).ifPresent(service -> {
			service.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					int accountId = (int) player.id();
					PreparedStatement presetStmt = connection.prepareStatement("INSERT INTO presets (account_id, name) VALUES (?, ?) RETURNING id");
					presetStmt.setInt(1, accountId);
					presetStmt.setString(2, preset.name());
					
					// Execute and gather data generated
					ResultSet presetData = presetStmt.executeQuery();
					presetData.next();
					int presetId = presetData.getInt("id");
					preset.id(presetId);
					
					// Insert the items
					PreparedStatement itemStmt = connection.prepareStatement("INSERT INTO preset_items (preset_id, account_id, item_id, amount, slot, equipment) VALUES (?, ?, ?, ?, ?, ?)");
					
					// Inventory..
					for (int i = 0; i < 28; i++) {
						Item item = preset.inventory()[i];
						
						if (item != null) {
							itemStmt.setInt(1, presetId);
							itemStmt.setInt(2, accountId);
							itemStmt.setInt(3, item.id());
							itemStmt.setInt(4, item.amount());
							itemStmt.setInt(5, i);
							itemStmt.setBoolean(6, false); // equipment = false, because it's inventory
							
							itemStmt.addBatch();
						}
					}
					
					// And equipment..
					for (int i = 0; i < 14; i++) {
						Item item = preset.equipment()[i];
						
						if (item != null) {
							itemStmt.setInt(1, presetId);
							itemStmt.setInt(2, accountId);
							itemStmt.setInt(3, item.id());
							itemStmt.setInt(4, item.amount());
							itemStmt.setInt(5, i);
							itemStmt.setBoolean(6, true); // equipment = true, because it's equipment
							
							itemStmt.addBatch();
						}
					}
					
					// Complete it, and commit it.
					itemStmt.executeBatch();

					PreparedStatement levelsStmt = connection.prepareStatement("INSERT INTO preset_levels (preset_id, account_id, id, level) VALUES (?, ?, ?, ?)");
					for (int i = 0; i < preset.levels().length; i++) {
						levelsStmt.setInt(1, presetId);
						levelsStmt.setInt(2, accountId);
						levelsStmt.setInt(3, i);
						levelsStmt.setInt(4, preset.levels()[i]);

						levelsStmt.addBatch();
					}
					levelsStmt.executeBatch();

					connection.commit();

					player.message("'" + preset.name() + "' has been saved!");
				}
			});
		});
	}
	
	public void load() {
		player.world().server().service(PgSqlService.class, true).ifPresent(service -> {
			service.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("SELECT * FROM presets LEFT JOIN preset_items ON preset_id = presets.id WHERE presets.account_id = ?");
					stmt.setInt(1, (Integer) player.id());
					
					ResultSet resultSet = stmt.executeQuery();
					while (resultSet.next()) {
						String name = resultSet.getString("name");
						boolean equipment = resultSet.getBoolean("equipment");
						int slot = resultSet.getInt("slot");
						int item = resultSet.getInt("item_id");
						int amount = resultSet.getInt("amount");
						int id = resultSet.getInt("id");
						
						Preset preset = presets.get(name.toLowerCase());
						if (preset == null) {
							preset = new Preset(id, name);
							presets.put(name.toLowerCase(), preset);
						}
						
						preset.set(equipment, slot, item, amount);
					}

					// Now fetch levels
					stmt = connection.prepareStatement("SELECT * FROM presets LEFT JOIN preset_levels ON preset_id = presets.id WHERE presets.account_id = ?");
					stmt.setInt(1, (Integer) player.id());

					resultSet = stmt.executeQuery();
					while (resultSet.next()) {
						int lvl = resultSet.getInt("level");
						int skill = resultSet.getInt("id");
						String name = resultSet.getString("name");

						Preset preset = presets.get(name.toLowerCase());
						if (preset != null && skill < preset.levels().length) {
							preset.levels()[skill] = lvl;
						}
					}
					
					connection.commit();
					loaded = true;
				}
			});
		});
	}
	
	public void rename(Preset preset, String name) {
		if (name.trim().length() < 1) {
			return;
		}
		
		String oldname = preset.name();
		presets.remove(preset.name().toLowerCase());
		presets.put(name.toLowerCase(), preset);
		preset.name(name);
		
		// Execute the sql query to update the name
		player.world().server().service(PgSqlService.class, true).ifPresent(service -> {
			service.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("UPDATE presets SET name = ? WHERE account_id = ? AND name ilike ?");
					stmt.setString(1, name);
					stmt.setInt(2, (Integer) player.id());
					stmt.setString(3, oldname);
					
					stmt.executeUpdate();
					connection.commit();

					Journal.PRESETS.send(player);
				}
			});
		});
	}
	
	public void delete(Preset preset) {
		presets.remove(preset.name().toLowerCase());
		
		player.world().server().service(PgSqlService.class, true).ifPresent(service -> {
			service.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("DELETE FROM presets WHERE account_id = ? AND name = ? RETURNING id");
					stmt.setInt(1, (Integer) player.id());
					stmt.setString(2, preset.name());
					
					ResultSet query = stmt.executeQuery();
					query.next();
					int presetId = query.getInt("id");
					
					PreparedStatement itemstmt = connection.prepareStatement("DELETE FROM preset_items WHERE preset_id = ?");
					itemstmt.setInt(1, presetId);
					itemstmt.execute();

					PreparedStatement levelsStmt = connection.prepareStatement("DELETE FROM preset_levels WHERE preset_id = ?");
					levelsStmt.setInt(1, presetId);
					levelsStmt.execute();
					
					connection.commit();
				}
			});
		});
	}
	
	public void deleteAll() {
		presets.clear();
		
		player.world().server().service(PgSqlService.class, true).ifPresent(service -> {
			service.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("DELETE FROM presets WHERE account_id = ?");
					stmt.setInt(1, (Integer) player.id());
					stmt.execute();
					
					PreparedStatement itemstmt = connection.prepareStatement("DELETE FROM preset_items WHERE account_id = ?");
					itemstmt.setInt(1, (Integer) player.id());
					itemstmt.execute();

					PreparedStatement levelsStmt = connection.prepareStatement("DELETE FROM preset_levels WHERE account_id = ?");
					levelsStmt.setInt(1, (Integer) player.id());
					levelsStmt.execute();
					
					connection.commit();
					player.message("All presets have been deleted from your account.");
				}
			});
		});
	}
	
	public boolean loaded() {
		return loaded;
	}
	
	public Preset createFromState(String name) {
		if (name.trim().length() < 1) {
			player.message("You can't use an empty name.");
			return null;
		}
		
		if (presets.containsKey(name.toLowerCase())) {
			player.message("You already have a preset with that name!");
			return null;
		}
		
		if (presets.size() >= totalStorage()) {
			player.message("You don't have any more free preset slots!");
			
			if (player.donationTier() != DonationTier.ULTIMATE_DONATOR) {
				int remaining = (int) Math.ceil(DonationTier.values()[player.donationTier().ordinal() + 1].required() - player.totalSpent);
				player.message("Your next two preset slots unlock after $" + remaining + " more spent in the store.");
			}
			
			return null;
		}
		
		Preset preset = new Preset(-1, name);
		preset.copyFrom(player);
		presets.put(name.toLowerCase(), preset);
		save(preset);
		player.message("Your preset is being captured...");
		return preset;
	}
	
	public int totalStorage() {
		switch (player.donationTier()) {
			case NONE:
				return FREE_SLOTS;
			case DONATOR:
				return FREE_SLOTS + 2;
			case SUPER_DONATOR:
				return FREE_SLOTS + 4;
			case EXTREME_DONATOR:
				return FREE_SLOTS + 6;
			case LEGENDARY_DONATOR:
				return FREE_SLOTS + 8;
			case MASTER_DONATOR:
				return FREE_SLOTS + 10;
			case GRAND_MASTER_DONATOR:
				return FREE_SLOTS + 12;
			case ULTIMATE_DONATOR:
				return FREE_SLOTS + 15;
			
			default:
				return FREE_SLOTS;
		}
	}
	
}
