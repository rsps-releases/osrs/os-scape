package nl.bartpelle.veteres.util.journal.presets;

import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class PresetsInformation extends JournalEntry {

    @Override
    public void send(Player player) {
        send(player, "<img=58> Presets Information");
    }

    @Override
    public void select(Player player) {
        player.world().server().scriptExecutor().executeLater(player, new QuestTab.PresetInformation(player));
    }

}