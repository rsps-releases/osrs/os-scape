package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.SetAlignment;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class SwapRangePrayers extends JournalEntry {

    public static void update(Player player) {
        boolean swap = player.attribOr(AttributeKey.SWAP_RANGE_PRAYERS, false);
        player.write(new SetAlignment(541, (swap ? 26 : 30), 74, 185));
        player.write(new SetAlignment(541, (swap ? 30 : 26), 148, 111));
    }

    @Override
    public void send(Player player) {
        boolean old = player.attribOr(AttributeKey.SWAP_RANGE_PRAYERS, false);
        if(old)
            send(player, "<img=58> Swap Range Prayer", "On", Color.GREEN);
        else
            send(player, "<img=58> Swap Range Prayer", "Off", Color.RED);
        update(player);
    }

    @Override
    public void select(Player player) {
        boolean old = player.attribOr(AttributeKey.SWAP_RANGE_PRAYERS, false);
        player.putattrib(AttributeKey.SWAP_RANGE_PRAYERS, !old);
        send(player);
    }

}