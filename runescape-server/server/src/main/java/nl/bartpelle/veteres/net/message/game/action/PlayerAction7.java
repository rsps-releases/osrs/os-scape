package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.mechanics.PlayerInteraction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

import java.lang.ref.WeakReference;

/**
 * Created by Jak on 10/01/2016.
 */
@PacketInfo(size = 3)
public class PlayerAction7 implements Action {
	
	protected boolean run;
	protected int index;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		run = buf.readByteS() == 1; // Always 0 in client.
		index = buf.readUShortA();
		
	}
	
	@Override
	public void process(Player player) {
		player.stopActions(true);
		
		Player other = player.world().players().get(index);
		if (other == null) {
			player.message("Unable to find player.");
		} else {
			player.debug("Click 7 db-id=%d pid=%d  dead: %s %s  attackable=%s",
					other.id(), index, other.dead() ? "Y" : "N", player.dead() ? "Y" : "N",
					player.attribOr(AttributeKey.DEBUG, false) ? player.attribOr(AttributeKey.ATTACK_OP, false) ? PlayerCombat.canAttack(player, other) : true : "?");
			if (!player.locked() && !player.dead()) {
				player.face(other);
				
				if (!other.dead()) {
					player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(other));
					player.putattrib(AttributeKey.INTERACTION_OPTION, 7);
					player.world().server().scriptExecutor().executeLater(player, new PlayerInteraction(7));
				}
			}
		}
	}
	
}
