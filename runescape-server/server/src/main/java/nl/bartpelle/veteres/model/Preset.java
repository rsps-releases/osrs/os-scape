package nl.bartpelle.veteres.model;

import nl.bartpelle.veteres.content.areas.clanwars.ClanWars;
import nl.bartpelle.veteres.content.interfaces.Bank;
import nl.bartpelle.veteres.content.interfaces.SpellSelect;
import nl.bartpelle.veteres.content.mechanics.Prayers;
import nl.bartpelle.veteres.content.mechanics.Transmogrify;
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking;
import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.entity.player.Skills;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.util.GameCommands;
import nl.bartpelle.veteres.util.Tuple;
import nl.bartpelle.veteres.util.Varbit;

import java.util.Map;

/**
 * Created by Bart on 6/17/2016.
 */
public class Preset {

	private int id;
	private String name;
	private Item[] inventory = new Item[28];
	private Item[] equipment = new Item[14];
	private int[] levels = new int[7];
	
	public Preset(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int id() {
		return id;
	}

	public void id(int id) {
		this.id = id;
	}

	public String name() {
		return name;
	}
	
	public void name(String name) {
		this.name = name;
	}
	
	public Item[] inventory() {
		return inventory;
	}
	
	public Item[] equipment() {
		return equipment;
	}

	public int[] levels() { return levels; }
	
	public void copyFrom(Player player) {
		inventory = player.inventory().copy();
		equipment = player.equipment().copy();
		for (int i = 0; i < levels.length; i++)
			levels[i] = player.skills().xpLevel(i);
	}
	
	public void set(boolean isEquipment, int slot, int id, int amount) {
		if (isEquipment) {
			equipment[slot] = new Item(id, amount);
		} else {
			inventory[slot] = new Item(id, amount);
		}
	}
	
	public void apply(Player player) {
		if (player.locked() || player.dead() || player.hp() < 0 || player.frozen() || player.finished())
			return;
		
		// More security
		if (GameCommands.inWildBypassable(player, "You can't use presets in the wilderness.", true)) {
			return;
		}
		if (Staking.in_duel(player)) {
			player.message("You cannot use presets during a duel.");
			return;
		}
		if (ClanWars.inInstance(player)) {
			player.message("You cannot use presets during a clan wars.");
			return;
		}
		if (player.jailed()) {
			player.message("You cannot use a preset when you're in jail.");
			return;
		}
		if (!GameCommands.inSafeZone(player)) {
			player.message("You cannot withdraw any presets outside of Edgeville.");
			return;
		}
		
		player.putattrib(AttributeKey.LAST_TIME_SPAWNSETUP_USED, String.valueOf(System.currentTimeMillis()));
		player.stopActions(true);

		//Turn off prayers when applying a new preset.
		Prayers.disableAllPrayers(player);
		
		// Bank everything boys
		Bank.depositInventory(player);
		Bank.depositEquipment(player);
		SpellSelect.reset(player, true, false);

		if (player.world.realm().isPVP()) {
			for (int i = 0; i < levels.length; i++)
				if (levels[i] > 0)
					player.skills().setXp(i, Skills.levelToXp(levels[i]));
		}
		
		// Do the containers
		withdraw(player, inventory, player.inventory().items(), "inventory");
		withdraw(player, equipment, player.equipment().items(), "equipment");
		
		// Make the containers dirty to force a refresh
		Bank.meltTabs(player, Bank.buildTabs(player));
		player.inventory().makeDirty();
		player.equipment().makeDirty();
		
		//Small check for sneaky players.
		if (Transmogrify.isTransmogrified(player)) {
			Transmogrify.hardReset(player);
		}
	}
	
	private static void withdraw(Player player, Item[] withdraw, Item[] playerinv, String name) {
		World world = player.world();
		boolean admin = player.privilege().eligibleTo(Privilege.ADMIN);
		
		for (int i = 0; i < withdraw.length; i++) {
			if (withdraw[i] != null) {
				if (playerinv[i] != null) {
					player.message("Couldn't fill " + name + " slot " + (i + 1) + " as it was occupied.");
				} else {
					// Target container is equipment. Let's check requirements! Stop pures wielding max mage yeah?
					if (playerinv.length == 14 && !player.privilege().eligibleTo(Privilege.ADMIN)) {
						boolean[] needsreq = new boolean[1];
						Map<Integer, Integer> reqs = player.world().equipmentInfo().requirementsFor(withdraw[i].id());
						if (reqs != null && reqs.size() > 0) {
							reqs.forEach((key, value) -> {
								if (!needsreq[0] && player.skills().xpLevel(key) < value) {
									player.message("You need %s %s level of %d to equip this.", Skills.SKILL_INDEFINITES[key], Skills.SKILL_NAMES[key], value);
									needsreq[0] = true;
								}
							});
						}
						// We don't meet a requirement.
						if (needsreq[0]) {
							player.message("<col=FF0000>You don't have the level requirements to wear: %s.", player.world().definitions().get(ItemDefinition.class, withdraw[i].id()).name);
							continue;
						}
					}
					Tuple<Integer, Item> firstresult = player.bank().findFirst(withdraw[i].id());
					ItemContainer.Result removed = player.bank().remove(withdraw[i], true);
					
					if (removed.completed() > 0 || admin) {
						playerinv[i] = new Item(withdraw[i].id(), admin ? withdraw[i].amount() : removed.completed());

						if (firstresult.first() > -1 && firstresult.second().hasProperties()) {
							playerinv[i].duplicateProperties(firstresult.second());
							player.message("<col=FF0000>%s</col> was withdrawn. Check charges/scales before dangerous activities.", playerinv[i].definition(world).name);
						}
						
						// Did we manage to get ALL the items?
						if (removed.completed() != removed.requested()) {
							player.message("Could only put " + removed.completed() + " out of " + removed.requested() + " x " + withdraw[i].name(world) + " in your " + name + ".");
						}
						
						// Placehold this? Varp enabled plus we've withdrawn ALL of that item from the bank.
						if (player.varps().varbit(Varbit.BANK_PLACEHOLDERS) == 1 && firstresult.first() > -1 && player.bank().get(firstresult.first()) == null) {
							// Does that item have a placeholder?
							int placeholderTemplate = new Item(firstresult.second()).definition(player.world()).pheld14401;
							int placeholderId = new Item(firstresult.second()).definition(player.world()).placeheld;
							
							if (placeholderTemplate == -1 && placeholderId > 0) {
								player.bank().set(firstresult.first(), new Item(placeholderId));
							}
						}
					} else {
						player.message("Couldn't find " + withdraw[i].name(world) + " in your bank.");
					}
				}
			}
		}
	}
	
}
