package nl.bartpelle.veteres.services.logging;

import co.paralleluniverse.strands.Strand;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.entity.player.Skills;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.PgSqlWorker;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.ItemsOnDeath;
import nl.bartpelle.veteres.util.QuestTab;
import nl.bartpelle.veteres.util.Varp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Bart on 11/25/2015.
 */
public class SqlLoggingService implements LoggingService {
	
	private static final Logger logger = LogManager.getLogger(SqlLoggingService.class);
	
	private GameServer server;
	private PgSqlWorker worker;
	private PgSqlService sqlService;
	private final ConcurrentLinkedQueue<PublicChatLogEntry> publicChats = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<PmEntry> privateMessages = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<LoginEntry> logins = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<PkLogEntry> pks = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<PacketLog> packets = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<PickupLog> pickups = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<CommandLogEntry> commands = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<LotteryLogEntry> lottery = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<ExchangeLogEntry> exchange = new ConcurrentLinkedQueue<>();
	private final ConcurrentLinkedQueue<ExpiredGroundItemEntry> expiredItems = new ConcurrentLinkedQueue<>();
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
		
		sqlService = server.service(PgSqlService.class, false).get();
		worker = new PgSqlWorker(sqlService).delay(5000);
		
		new Thread(worker).start();
		
		// Chat aggregation
		new Thread(() -> {
			while (true) {
				try {
					processLogAggregations();
					Strand.sleep(5000);
				} catch (Exception e) {
					logger.error("Error processing chat aggregation!", e);
				}
			}
		}).start();
	}
	
	//TODO maybe batch this
	@Override
	public void logNpcKill(Integer characterId, int npc, int kills) {
		// TODO wait for pg9.5 i guess... unsupported on 9.4
		
		//worker.submit(new SqlTransaction() {
		//	@Override public void execute(Connection connection) throws Exception {
		//		System.out.println("hello");
		//		PreparedStatement s = connection.prepareStatement("INSERT INTO npc_kills(character_id, npc_id, kills) VALUES (?,?,?) ON CONFLICT(character_id,npc_id) DO UPDATE SET kills=kills+?");
		//		s.setInt(1, characterId);
		//		s.setInt(2, npc);
		//		s.setInt(3, kills);
		//		s.setInt(4, kills);
		//		s.executeUpdate();
		//		connection.commit();
		//		System.out.println("done");
		//	}
		//});
	}
	
	@Override
	public void logPublicChat(Integer accountId, int world, int x, int z, int level, String message) {
		PublicChatLogEntry e = new PublicChatLogEntry();
		e.acccountId = accountId;
		e.world = world;
		e.x = x;
		e.z = z;
		e.level = level;
		e.message = message;
		publicChats.add(e);
	}
	
	@Override
	public void logPrivateMessage(Integer accountId, Integer toAccountId, int fromWorld, Tile tile, String message) {
		PmEntry e = new PmEntry();
		e.fromId = accountId;
		e.toId = toAccountId;
		e.world = fromWorld;
		e.t = tile;
		e.message = message;
		privateMessages.add(e);
	}
	
	@Override
	public void logLogin(Player player) {
		if (player.bot())
			return;
		
		LoginEntry e = new LoginEntry();
		e.fromId = (Integer) player.id();
		e.ip = player.ip();
		e.world = player.world().id();
		e.uuid = player.uuid();
		e.hwid = player.hwid();
		logins.add(e);
	}
	
	@Override
	public void logKill(Player killer, Player killed, long value, ItemsOnDeath.KillersLoot lootTable) {
		if (killer.bot() || killed.bot())
			return;
		
		PkLogEntry e = new PkLogEntry();
		e.killerId = (Integer) killer.id();
		e.killedId = (Integer) killed.id();
		e.killerIp = killer.ip();
		e.killedIp = killed.ip();
		e.totalValue = value;
		e.world = killer.world().id();
		e.tile = killed.tile();
		ArrayList<Item> loot = new ArrayList<>(lootTable.loot.size());
		lootTable.loot.forEach(item -> {
			if (item.item.realPrice(killed.world()) >= (killed.world().realm().isPVP() ? 3000 : 100_000)) {
				loot.add(item.item);
			}
		});
		e.loot = loot;
		pks.add(e);
	}
	
	@Override
	public void logPacket(Player player, int id, int size, String desc) {
		if (player.bot())
			return;
		
		PacketLog e = new PacketLog();
		e.description = desc;
		e.id = id;
		e.size = size;
		e.ip = player.ip();
		e.uuid = player.uuid();
		e.x = player.tile().x;
		e.z = player.tile().z;
		e.level = player.tile().level;
		e.player = (Integer) player.id();
		packets.add(e);
	}
	
	@Override
	public void logPvPHighscore(Player player) {
		if (player.bot())
			return;
		
		try {
			int id = (int) player.id();
			int kills = player.varps().varp(Varp.KILLS);
			int deaths = player.varps().varp(Varp.DEATHS);
			int killstreak = player.attribOr(AttributeKey.KILLSTREAK, 0);
			int killstreakTop = Math.max(killstreak, player.attribOr(AttributeKey.KILLSTREAK_RECORD, 0));
			int shutdownTop = player.attribOr(AttributeKey.SHUTDOWN_RECORD, 0);
			double kdr = Double.parseDouble(QuestTab.getKdr(player));
			
			worker.submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					// Forgive for I interpolate a sql string. Otherwise it's just so long :(
					connection.prepareStatement("DELETE FROM pvp_highscores WHERE account_id = " + id).execute();
					
					PreparedStatement s = connection.prepareStatement("INSERT INTO pvp_highscores (account_id, kills, deaths, kdr, shutdown, killstreak, killstreak_top) VALUES (?, ?, ?, ?, ?, ?, ?)");
					s.setInt(1, id);
					s.setInt(2, kills);
					s.setInt(3, deaths);
					s.setDouble(4, kdr);
					s.setInt(5, shutdownTop);
					s.setInt(6, killstreak);
					s.setInt(7, killstreakTop);
					s.executeUpdate();
					
					connection.commit();
				}
			});
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@Override
	public void logEcoHighscore(Player player) {
		if (player.privilege() == Privilege.ADMIN || player.bot())
			return;
		
		int accountId = (int) player.id();
		int characterId = player.characterId();
		
		try {
			worker.submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					Map<Integer, EcoHighscoreEntry> eco = new HashMap<>();
					PreparedStatement fetch = connection.prepareStatement("DELETE FROM eco_highscores WHERE account_id = ? AND service_id = ? RETURNING skill, xp, last_updated");
					fetch.setInt(1, accountId);
					fetch.setInt(2, player.world().realm().id());
					
					ResultSet set = fetch.executeQuery();
					
					// Get the deleted data, and put it into the map to analyze it later
					while (set.next()) {
						EcoHighscoreEntry entr = new EcoHighscoreEntry();
						entr.skill = set.getInt("skill");
						entr.lastUpdated = set.getTimestamp("last_updated");
						entr.xp = set.getInt("xp");
						
						eco.put(entr.skill, entr);
					}
					
					// Now insert new data, but pay attention to the maxed skills. Don't want to kick one from his 200M pos!
					PreparedStatement insert = connection.prepareStatement("INSERT INTO eco_highscores (account_id, skill, character_id, xp, last_updated, game_mode, iron_mode, lvl, service_id) VALUES(?, ?, ?, ?, ?::TIMESTAMP, ?, ?, ?, ?)");
					for (int skill = 0; skill < Skills.SKILL_COUNT; skill++) {
						insert.setInt(1, accountId);
						insert.setInt(2, skill);
						insert.setInt(3, characterId);
						insert.setInt(4, (int) Math.min(200_000_000, Math.ceil(player.skills().xp()[skill])));
						
						// This one is the only special case. When you reach 200M xp, you get "frozen" at a spot, which means
						// ordering happens based on last updated. If we cleared an entry which was 200M, we use the borrowed
						// date, otherwise just now().
						if (eco.containsKey(skill) && eco.get(skill).xp >= 200_000_000) {
							insert.setTimestamp(5, eco.get(skill).lastUpdated);
						} else {
							insert.setString(5, "now()"); // This gets casted to a TIMESTAMP.
						}
						
						insert.setInt(6, player.mode().uid());
						insert.setInt(7, player.ironMode().ordinal());
						insert.setInt(8, Skills.xpToLevel((int) Math.min(200_000_000, Math.ceil(player.skills().xp()[skill]))));
						insert.setInt(9, player.world().realm().id());
						
						// Mark this batch as done, next!
						insert.addBatch();
					}
					
					insert.executeBatch();
					connection.commit();
				}
			});
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@Override
	public void logTrade(Player trader1, Player trader2, ItemContainer offer1, ItemContainer offer2) {
		if (trader1.bot() || trader2.bot())
			return;
		
		try {
			// Calculate values
			World world = trader1.world();
			long value1 = 0;
			long value2 = 0;
			boolean world2 = trader1.world().realm().isPVP();
			
			// Make copies, these can change immediately after submission
			Item[] offer1Items = offer1.copy();
			Item[] offer2Items = offer2.copy();
			
			// Calculate total price of first offer
			for (Item i : offer1Items) {
				if (i != null) {
					Item item = i.unnote(trader1.world());
					
					if (world2) {
						value1 += world.prices().getOrElse(item.id(), 0) * item.amount();
					} else {
						value1 += item.realPrice(world) * item.amount();
					}
				}
			}
			
			// Calculate total of the second half
			for (Item i : offer2Items) {
				if (i != null) {
					Item item = i.unnote(trader2.world());
					if (world2) {
						value2 += world.prices().getOrElse(item.id(), 0) * item.amount();
					} else {
						value2 += item.realPrice(world) * item.amount();
					}
				}
			}
			
			// Final values to use them in the anonymous subclass
			final long finalPrice1 = value1;
			final long finalPrice2 = value2;
			final Integer trader1Id = (Integer) trader1.id();
			final Integer trader2Id = (Integer) trader2.id();
			final String ip1 = trader1.ip();
			final String ip2 = trader2.ip();
			final Tile tile = trader1.tile();
			
			worker.submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("INSERT INTO trades" +
							"(trader1_account_id, trader2_account_id, trader1_value, trader2_value, trader1_ip, trader2_ip, world, x, z, level) " +
							"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;");
					
					// Set base parameters..
					s.setInt(1, trader1Id);
					s.setInt(2, trader2Id);
					s.setLong(3, finalPrice1);
					s.setLong(4, finalPrice2);
					s.setString(5, ip1);
					s.setString(6, ip2);
					s.setInt(7, world.id());
					s.setInt(8, tile.x);
					s.setInt(9, tile.z);
					s.setInt(10, tile.level);

					// Execute the insert and grab the trade id
					ResultSet set = s.executeQuery();
					set.next();
					int tradeId = set.getInt("id");
					
					s = connection.prepareStatement("INSERT INTO trade_items (trade_id, account_id, item_id, amount) VALUES (?,?,?,?);");
					
					// Drill the offers into..
					for (Item item : offer1Items) {
						if (item != null) {
							s.setInt(1, tradeId);
							s.setInt(2, trader1Id);
							s.setInt(3, item.id());
							s.setInt(4, item.amount());
							s.addBatch();
						}
					}
					// Other half too..
					for (Item item : offer2Items) {
						if (item != null) {
							s.setInt(1, tradeId);
							s.setInt(2, trader2Id);
							s.setInt(3, item.id());
							s.setInt(4, item.amount());
							s.addBatch();
						}
					}
					
					s.executeBatch();
					connection.commit();
				}
			});
		} catch (Exception e) {
			logger.error("Error while submitting tradelog between {} and {}!", trader1.name(), trader2.name(), e);
		}
	}
	
	@Override
	public void logDropTrade(Player player, GroundItem gitem) {
		if (player.bot())
			return;
		
		try {
			long value = 0L;
			Item unnoted = gitem.item().unnote(player.world());
			if (player.world().realm().isPVP()) {
				value = player.world().prices().getOrElse(unnoted.id(), 0) * gitem.item().amount();
			} else {
				value = unnoted.realPrice(player.world()) * gitem.item().amount();
			}
			
			// We don't log too small ones.
			if (value < 200) {
				return;
			}
			
			PickupLog entry = new PickupLog();
			entry.totalValue = value;
			entry.world = player.world().id();
			entry.amount = gitem.item().amount();
			entry.id = gitem.item().id();
			entry.x = gitem.tile().x;
			entry.z = gitem.tile().z;
			entry.level = gitem.tile().level;
			entry.takerId = (Integer) player.id();
			entry.takerIp = player.ip();
			entry.takerHwid = player.hwid();
			entry.takerUuid = player.uuid();
			entry.dropperId = (Integer) gitem.owner();
			
			player.world().playerForId(gitem.owner()).ifPresent(other -> {
				entry.dropperHwid = other.hwid();
				entry.dropperIp = other.ip();
				entry.dropperUuid = other.uuid();
			});
			
			pickups.add(entry);
		} catch (Exception e) {
			logger.error("Error while submitting drop-trade log for {}!", player.name(), e);
		}
	}
	
	@Override
	public void logStake(Player winner, Player loser, ItemContainer winnerItems, ItemContainer loserItems) {
		if (winner.bot() || loser.bot())
			return;
		
		try {
			// Calculate values
			World world = winner.world();
			long winnerValue = 0;
			long loserValue = 0;
			boolean spawn = winner.world().realm().isPVP();
			
			// Make copies, these can change immediately after submission
			Item[] winnerItemArray = winnerItems.copy();
			Item[] loserItemArray = loserItems.copy();
			
			// Calculate total price of first offer
			for (Item i : winnerItemArray) {
				if (i != null) {
					Item item = i.unnote(winner.world());
					
					if (spawn) {
						winnerValue += world.prices().getOrElse(item.id(), 0) * item.amount();
					} else {
						winnerValue += item.realPrice(world) * item.amount();
					}
				}
			}
			
			// Calculate total of the second half
			for (Item i : loserItemArray) {
				if (i != null) {
					Item item = i.unnote(winner.world());
					
					if (spawn) {
						loserValue += world.prices().getOrElse(item.id(), 0) * item.amount();
					} else {
						loserValue += item.realPrice(world) * item.amount();
					}
				}
			}
			
			// Final values to use them in the anonymous subclass
			final long finalWinnerValue = winnerValue;
			final long finalLoserValue = loserValue;
			final Integer winnerId = (Integer) winner.id();
			final Integer loserId = (Integer) loser.id();
			final String winnerIP = winner.ip();
			final String loserIP = loser.ip();
			final String winnerUUID = winner.uuid();
			final String loserUUID = loser.uuid();
			
			worker.submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("INSERT INTO stakes" +
							"(winner_id, loser_id, winner_value, loser_value, winner_ip, loser_ip, world, winner_uuid, loser_uuid) " +
							"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;");
					
					// Set base parameters..
					s.setInt(1, winnerId);
					s.setInt(2, loserId);
					s.setLong(3, finalWinnerValue);
					s.setLong(4, finalLoserValue);
					s.setString(5, winnerIP);
					s.setString(6, loserIP);
					s.setInt(7, world.id());
					s.setString(8, winnerUUID);
					s.setString(9, loserUUID);
					
					// Execute the insert and grab the trade id
					ResultSet set = s.executeQuery();
					set.next();
					int tradeId = set.getInt("id");
					
					s = connection.prepareStatement("INSERT INTO stake_items (stake_id, account_id, item_id, amount) VALUES (?,?,?,?);");
					
					// Drill the offers into..
					for (Item item : winnerItemArray) {
						if (item != null) {
							s.setInt(1, tradeId);
							s.setInt(2, winnerId);
							s.setInt(3, item.id());
							s.setInt(4, item.amount());
							s.addBatch();
						}
					}
					
					// Other half too..
					for (Item item : loserItemArray) {
						if (item != null) {
							s.setInt(1, tradeId);
							s.setInt(2, loserId);
							s.setInt(3, item.id());
							s.setInt(4, item.amount());
							s.addBatch();
						}
					}
					
					s.executeBatch();
					connection.commit();
				}
			});
		} catch (Exception e) {
			logger.error("Error while submitting duel log between {} and {}!", winner.name(), loser.name(), e);
		}
	}
	
	private void processLogAggregations() {
		if (server.isVerbose())
			logger.info("Processing chat logs...");
		
		Object[] chats = publicChats.toArray(); // Capture only shortly, don't block the queue
		publicChats.clear();
		Object[] pms = privateMessages.toArray(); // Capture only shortly, don't block the queue
		privateMessages.clear();
		Object[] pks = this.pks.toArray(); // Capture only shortly, don't block the queue
		this.pks.clear();
		Object[] logins = this.logins.toArray(); // Capture only shortly, don't block the queue
		this.logins.clear();
		Object[] packets = this.packets.toArray(); // Capture only shortly, don't block the queue
		this.packets.clear();
		Object[] pickups = this.pickups.toArray(); // Capture only shortly, don't block the queue
		this.pickups.clear();
		Object[] commands = this.commands.toArray(); // Capture only shortly, don't block the queue
		this.commands.clear();
		Object[] lottery = this.lottery.toArray(); // Capture only shortly, don't block the queue
		this.lottery.clear();
		Object[] exchange = this.exchange.toArray(); // Capture only shortly, don't block the queue
		this.exchange.clear();
		Object[] gitem_despawns = this.expiredItems.toArray();
		this.expiredItems.clear();
		
		worker.submit(new SqlTransaction() {
			private Object[] exchange_ = exchange.clone(); // Clone it.. just to be sure hey?
			
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO exchange_logs(buyer_id, seller_id, itemid, amount, cost_per, serviceid) VALUES (?,?,?,?,?,?)");
				
				for (Object o : exchange_) {
					ExchangeLogEntry e = (ExchangeLogEntry) o;
					
					s.setInt(1, e.buyerId);
					s.setInt(2, e.sellerId);
					s.setInt(3, e.itemid);
					s.setInt(4, e.amount);
					s.setInt(5, e.cost_per);
					s.setInt(6, e.serviceId);
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			private Object[] lottery_ = lottery.clone(); // Clone it.. just to be sure hey?
			
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO lottery_log(account_id, world_id, amt, previous_amount, ip, currency,type) VALUES (?,?,?,?,?,?,?)");
				
				for (Object o : lottery_) {
					LotteryLogEntry e = (LotteryLogEntry) o;
					
					s.setInt(1, e.acccountId);
					s.setInt(2, e.world);
					s.setLong(3, e.amt);
					s.setLong(4, e.previous_amt);
					s.setString(5, e.ip);
					s.setInt(6, e.currency);
					s.setString(7, e.type);
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			private Object[] commands_ = commands.clone(); // Clone it.. just to be sure hey?
			
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO command_log(account_id, world_id, x, z, level, command, command_privilege) VALUES (?,?,?,?,?,?,?)");
				
				for (Object o : commands_) {
					CommandLogEntry e = (CommandLogEntry) o;
					
					s.setInt(1, e.acccountId);
					s.setInt(2, e.world);
					s.setInt(3, e.x);
					s.setInt(4, e.z);
					s.setInt(5, e.level);
					s.setString(6, e.command);
					s.setInt(7, e.privilege);
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			private Object[] chats_ = chats.clone(); // Clone it.. just to be sure hey?
			
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO public_chats(account_id, world_id, x, z, level, message) VALUES (?,?,?,?,?,?)");
				
				for (Object o : chats_) {
					PublicChatLogEntry e = (PublicChatLogEntry) o;
					
					s.setInt(1, e.acccountId);
					s.setInt(2, e.world);
					s.setInt(3, e.x);
					s.setInt(4, e.z);
					s.setInt(5, e.level);
					s.setString(6, e.message);
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO private_messages" +
						"(from_account_id, to_account_id, from_x, from_z, from_level, from_world, message) VALUES (?,?,?,?,?,?,?)");
				
				for (Object o : pms) {
					PmEntry e = (PmEntry) o;
					
					s.setInt(1, e.fromId);
					s.setInt(2, e.toId);
					s.setInt(3, e.t.x);
					s.setInt(4, e.t.z);
					s.setInt(5, e.t.level);
					s.setInt(6, e.world);
					s.setString(7, e.message);
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO logins (account_id, ip, world, uuid, hwid) VALUES (?,?,?,?,?)");
				
				for (Object o : logins) {
					LoginEntry e = (LoginEntry) o;
					
					s.setInt(1, e.fromId);
					s.setString(2, e.ip);
					s.setInt(3, e.world);
					s.setString(4, e.uuid);
					s.setString(5, e.hwid);
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO player_kills(killer_id, killed_id, killer_ip, killed_ip, world, total_value, x, z, level) " +
						"VALUES (?,?,?,?,?,?,?,?,?) RETURNING id;");
				
				for (Object o : pks) {
					PkLogEntry e = (PkLogEntry) o;

					s.setInt(1, e.killerId);
					s.setInt(2, e.killedId);
					s.setString(3, e.killerIp);
					s.setString(4, e.killedIp);
					s.setInt(5, e.world);
					s.setLong(6, e.totalValue);
					s.setInt(7, e.tile.x);
					s.setInt(8, e.tile.z);
					s.setInt(9, e.tile.level);

					// Execute the insert and grab the trade id
					ResultSet set = s.executeQuery();
					set.next();
					int killId = set.getInt("id");

					PreparedStatement items = connection.prepareStatement("INSERT INTO pk_items (kill_id, item_id, item_amount) VALUES (?,?,?);");
					for (Item item : e.loot) {
						items.setInt(1, killId);
						items.setInt(2, item.id());
						items.setInt(3, item.amount());
						items.addBatch();
					}
					items.executeBatch();
				}

				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO packet_logs(account_id, ip, uuid, packet_id, packet_size, description, system_time, x, z, level) " +
						" VALUES (?,?,?,?,?,?,?,?,?,?)");
				
				for (Object o : packets) {
					PacketLog e = (PacketLog) o;
					
					s.setInt(1, e.player);
					s.setString(2, e.ip);
					s.setString(3, e.uuid);
					s.setInt(4, e.id);
					s.setInt(5, e.size);
					s.setString(6, e.description);
					s.setLong(7, e.time);
					s.setInt(8, e.x);
					s.setInt(9, e.z);
					s.setInt(10, e.level);
					
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});
		
		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO drop_logs " +
						"(taker_id, dropper_id, taker_ip, dropper_ip, id, amount, value, x, z, level, world, dropper_uuid, taker_uuid, dropper_hwid, taker_hwid) " +
						" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				
				for (Object o : pickups) {
					PickupLog e = (PickupLog) o;
					
					s.setInt(1, e.takerId);
					s.setInt(2, e.dropperId);
					s.setString(3, e.takerIp);
					s.setString(4, e.dropperIp);
					s.setInt(5, e.id);
					s.setInt(6, e.amount);
					s.setLong(7, e.totalValue);
					s.setInt(8, e.x);
					s.setInt(9, e.z);
					s.setInt(10, e.level);
					s.setInt(11, e.world);
					s.setString(12, e.dropperUuid);
					s.setString(13, e.takerUuid);
					s.setString(14, e.dropperHwid);
					s.setString(15, e.takerHwid);
					
					s.addBatch();
				}
				
				s.executeBatch();
				connection.commit();
			}
		});

		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO gitems_expired_logs(account_id, item_id, item_amount, value, system_time, x, z, level, world) " +
						" VALUES (?,?,?,?,?,?,?,?,?)");

				for (Object o : gitem_despawns) {
					ExpiredGroundItemEntry e = (ExpiredGroundItemEntry) o;

					s.setInt(1, e.ownedAccountId);
					s.setInt(2, e.itemid);
					s.setInt(3, e.itemAmount);
					s.setInt(4, e.value);
					s.setLong(5, e.time);
					s.setInt(6, e.x);
					s.setInt(7, e.z);
					s.setInt(8, e.level);
					s.setInt(9, e.realm);

					s.addBatch();
				}

				s.executeBatch();
				connection.commit();
			}
		});
	}
	
	@Override
	public void logPunishment(Player punisher, String account, String type) {
		if (punisher.bot())
			return;
		
		Integer pid = (Integer) punisher.id();
		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement s = connection.prepareStatement("INSERT INTO punish_log(account_id, mod_id, from_cpanel, type) VALUES ((SELECT id FROM accounts WHERE displayname ilike ?), ?, false, ?::punish_type); ");
				s.setString(1, account);
				s.setInt(2, pid);
				s.setString(3, type);
				
				s.execute();
				connection.commit();
			}
		});
	}

	public void logRefundClaim(Player claimer, int count, int from, int newBalance) {
		Integer claimerId = (Integer) claimer.id();

		worker.submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				PreparedStatement stmt = connection.prepareStatement("INSERT INTO refund_log(account_id, credits, new_balance, time, from_account_id) VALUES (?, ?, ?, now(), ?)");
				stmt.setInt(1, claimerId);
				stmt.setInt(2, count);
				stmt.setInt(3, newBalance);
				stmt.setInt(4, from);
				stmt.execute();

				connection.commit();
			}
		});
	}
	
	@Override
	public void logCommand(Integer accountId, int world, int x, int z, int level, String command, int privilege) {
		CommandLogEntry e = new CommandLogEntry();
		e.acccountId = accountId;
		e.world = world;
		e.x = x;
		e.z = z;
		e.level = level;
		e.command = command;
		e.privilege = privilege;
		commands.add(e);
	}
	
	@Override
	public void logLottery(Integer accountId, int world, String type, long amt, long previous_amt, String ip, int currency) {
		LotteryLogEntry e = new LotteryLogEntry();
		e.acccountId = accountId;
		e.world = world;
		e.type = type;
		e.amt = amt;
		e.previous_amt = previous_amt;
		e.ip = ip;
		e.currency = currency;
		lottery.add(e);
	}
	
	@Override
	public void logExchange(Integer buyerId, Integer sellerId, int itemid, int amount, int cost_per, int serviceId) {
		ExchangeLogEntry e = new ExchangeLogEntry();
		e.buyerId = buyerId;
		e.sellerId = sellerId;
		e.itemid = itemid;
		e.amount = amount;
		e.cost_per = cost_per;
		e.serviceId = serviceId;
		exchange.add(e);
	}

	@Override
	public void logGrounditemExpired(GroundItem gitem, World world) {
		if (gitem.owner() == null || !(gitem.owner() instanceof Integer)) {
			logger.error("Tried logging expired ground item with no owner: {} at {}", gitem.item(), gitem.tile());
			return;
		}
		ExpiredGroundItemEntry e = new ExpiredGroundItemEntry();
		e.ownedAccountId = (Integer) gitem.owner();
		e.itemAmount = gitem.item().amount();
		e.itemid = gitem.item().id();
		e.value = gitem.item().realPrice(world);
		e.realm = world.realm().id();
		e.x = gitem.tile().x;
		e.level = gitem.tile().level;
		e.z = gitem.tile().z;
		e.time = System.currentTimeMillis();
		expiredItems.add(e);
	}

	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}

	static class ExpiredGroundItemEntry {
		Integer ownedAccountId;
		int realm;
		int x;
		int level;
		int z;
		int itemid;
		int itemAmount;
		int value;
		long time;
	}
	
	static class ExchangeLogEntry {
		Integer buyerId;
		Integer sellerId;
		int itemid;
		int amount;
		int cost_per;
		int serviceId;
	}
	
	static class PublicChatLogEntry {
		Integer acccountId;
		int world;
		int x;
		int z;
		int level;
		String message;
	}
	
	static class CommandLogEntry {
		Integer acccountId;
		int world;
		int x;
		int z;
		int level;
		String command;
		int privilege;
	}
	
	static class LotteryLogEntry {
		Integer acccountId;
		int world;
		String type;
		long amt;
		long previous_amt;
		String ip;
		int currency;
	}
	
	static class PmEntry {
		Integer fromId;
		Integer toId;
		int world;
		Tile t;
		String message;
	}
	
	static class LoginEntry {
		Integer fromId;
		String ip;
		int world;
		String uuid;
		String hwid;
	}
	
	static class PkLogEntry {
		Integer killerId;
		Integer killedId;
		String killerIp;
		String killedIp;
		long totalValue;
		int world;
		Tile tile;
		ArrayList<Item> loot;
	}
	
	static class PickupLog {
		Integer takerId;
		Integer dropperId;
		String takerIp;
		String dropperIp;
		String takerUuid;
		String dropperUuid;
		String takerHwid;
		String dropperHwid;
		long totalValue;
		int id;
		int amount;
		int x;
		int z;
		int level;
		int world;
	}
	
	static class PacketLog {
		Integer player;
		String ip;
		long time = System.currentTimeMillis();
		String uuid;
		int id;
		int size;
		String description;
		int x;
		int z;
		int level;
	}
	
	static class EcoHighscoreEntry {
		Integer accountId;
		int skill;
		int xp;
		Timestamp lastUpdated;
	}
	
}