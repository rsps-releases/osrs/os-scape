package nl.bartpelle.veteres.util;

/**
 * @author Mack
 */
public class ClientScriptUtils {

    public static final String CS2_STR_ARRAY_TERMINATOR = "|";

    public static String addRow(String info) {
        return (info + CS2_STR_ARRAY_TERMINATOR);
    }
}
