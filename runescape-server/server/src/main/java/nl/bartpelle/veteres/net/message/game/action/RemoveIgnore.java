package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Friend;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.services.intercom.PmService;

/**
 * Created by Bart on 11/15/2015.
 */
@PacketInfo(size = -1)
public class RemoveIgnore implements Action {
	
	private String name;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		name = buf.readString();
	}
	
	@Override
	public void process(Player player) {
		// Find the friend in our list, then grab his id and remove him.
		Friend[] removed = new Friend[]{null};
		for (Friend ignore : player.social().ignores()) {
			if (ignore != null && ignore.name.equalsIgnoreCase(name)) {
				removed[0] = ignore;
				player.world().server().service(PmService.class, true).ifPresent(pmService -> {
					pmService.removeIgnore(player, ignore.id);
				});
				break;
			}
		}
		// Update it locally on our players Social instance.
		if (removed[0] != null) {
			player.social().ignores().removeIf(ignore -> ignore.id == removed[0].id);
			player.message(removed[0].name() + " was removed from your ignore list.");
		}
	}
	
}
