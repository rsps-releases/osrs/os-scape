package nl.bartpelle.veteres.content;

import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.SendCredits;

public class BuyCredits {

    public final int purchasePrice;

    public final int purchaseAmount;

    public final int freeAmount;

    private BuyCredits(int purchasePrice, int purchaseAmount, int freeAmount) {
        this.purchasePrice = purchasePrice;
        this.purchaseAmount = purchaseAmount;
        this.freeAmount = freeAmount;
    }

    private static void send(Player player) {
        if(player.interfaces().visible(66))
            player.interfaces().closeMain();
        player.interfaces().sendMain(66);
        player.write(new SendCredits(CUSTOM_MESSAGE, DISCOUNT_PERCENT, player.selectedCreditPackage, player.selectedPaymentMethod, PACKS));
    }

    public static void open(Player player) {
        player.selectedCreditPackage = -1;
        player.selectedPaymentMethod = 0;
        send(player);
    }

    /**
     * Misc
     */

    private static String CUSTOM_MESSAGE = "";

    private static int DISCOUNT_PERCENT = 0;

    private static final BuyCredits[] PACKS = {
            new BuyCredits(5, 500, 0),
            new BuyCredits(10, 1000, 100),
            new BuyCredits(25, 2500, 500),
            new BuyCredits(50, 5000, 1500),
            new BuyCredits(100, 10000, 4000),
            new BuyCredits(250, 25000, 12500),
    };

    public static void set(String message, int percent) {
        CUSTOM_MESSAGE = message;
        DISCOUNT_PERCENT = percent;
    }

    public static void setMessage(String message) {
        set(message, DISCOUNT_PERCENT);
    }

    public static void setDiscount(int percent) {
        set(CUSTOM_MESSAGE, percent);
    }

}