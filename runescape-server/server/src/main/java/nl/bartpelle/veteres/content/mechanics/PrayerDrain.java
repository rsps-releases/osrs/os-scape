package nl.bartpelle.veteres.content.mechanics;

import nl.bartpelle.skript.ScriptMain;
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup;
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent;
import nl.bartpelle.veteres.fs.DefinitionRepository;
import nl.bartpelle.veteres.fs.VarbitDefinition;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Varps;
import nl.bartpelle.veteres.script.ScriptRepository;
import nl.bartpelle.veteres.util.Varbit;

/**
 * Created by Bart on 8/13/2016.
 */
public class PrayerDrain {
	
	private static VarbitDefinition THICK_SKIN, BURST_OF_STRENGTH, CLARITY_OF_THOUGHT, SHARP_EYE, MYSTIC_WILL,
			ROCK_SKIN, SUPERHUMAN_STRENGTH, IMPROVED_REFLEXIS, RAPID_RESTORE, RAPID_HEAL, PROTECT_ITEM, HAWK_EYE,
			MYSTIC_LORE, STEEL_SKIN, ULTIMATE_STRENGTH, INCREDIBLE_REFLEXES, PROTECT_FROM_MELEE,
			PROTECT_FROM_MAGIC, PROTECT_FROM_MISSILES, EAGLE_EYE, MYSTIC_MIGHT, RETRIBUTION, REDEMPTION,
			SMITE, CHIVALRY, PIETY, RIGOUR, AUGURY;
	
	@ScriptMain
	public static void register(ScriptRepository repository) {
		// Method mainly used to cache data.
		DefinitionRepository definitions = repository.server().world().definitions();
		
		THICK_SKIN = definitions.get(VarbitDefinition.class, Varbit.THICK_SKIN);
		BURST_OF_STRENGTH = definitions.get(VarbitDefinition.class, Varbit.BURST_OF_STRENGTH);
		CLARITY_OF_THOUGHT = definitions.get(VarbitDefinition.class, Varbit.CLARITY_OF_THOUGHT);
		SHARP_EYE = definitions.get(VarbitDefinition.class, Varbit.SHARP_EYE);
		MYSTIC_WILL = definitions.get(VarbitDefinition.class, Varbit.MYSTIC_WILL);
		ROCK_SKIN = definitions.get(VarbitDefinition.class, Varbit.ROCK_SKIN);
		SUPERHUMAN_STRENGTH = definitions.get(VarbitDefinition.class, Varbit.SUPERHUMAN_STRENGTH);
		IMPROVED_REFLEXIS = definitions.get(VarbitDefinition.class, Varbit.IMPROVED_REFLEXIS);
		RAPID_RESTORE = definitions.get(VarbitDefinition.class, Varbit.RAPID_RESTORE);
		RAPID_HEAL = definitions.get(VarbitDefinition.class, Varbit.RAPID_HEAL);
		PROTECT_ITEM = definitions.get(VarbitDefinition.class, Varbit.PROTECT_ITEM);
		HAWK_EYE = definitions.get(VarbitDefinition.class, Varbit.HAWK_EYE);
		MYSTIC_LORE = definitions.get(VarbitDefinition.class, Varbit.MYSTIC_LORE);
		STEEL_SKIN = definitions.get(VarbitDefinition.class, Varbit.STEEL_SKIN);
		ULTIMATE_STRENGTH = definitions.get(VarbitDefinition.class, Varbit.ULTIMATE_STRENGTH);
		INCREDIBLE_REFLEXES = definitions.get(VarbitDefinition.class, Varbit.INCREDIBLE_REFLEXES);
		PROTECT_FROM_MELEE = definitions.get(VarbitDefinition.class, Varbit.PROTECT_FROM_MELEE);
		PROTECT_FROM_MAGIC = definitions.get(VarbitDefinition.class, Varbit.PROTECT_FROM_MAGIC);
		PROTECT_FROM_MISSILES = definitions.get(VarbitDefinition.class, Varbit.PROTECT_FROM_MISSILES);
		EAGLE_EYE = definitions.get(VarbitDefinition.class, Varbit.EAGLE_EYE);
		MYSTIC_MIGHT = definitions.get(VarbitDefinition.class, Varbit.MYSTIC_MIGHT);
		RETRIBUTION = definitions.get(VarbitDefinition.class, Varbit.RETRIBUTION);
		REDEMPTION = definitions.get(VarbitDefinition.class, Varbit.REDEMPTION);
		SMITE = definitions.get(VarbitDefinition.class, Varbit.SMITE);
		CHIVALRY = definitions.get(VarbitDefinition.class, Varbit.CHIVALRY);
		PIETY = definitions.get(VarbitDefinition.class, Varbit.PIETY);
		RIGOUR = definitions.get(VarbitDefinition.class, Varbit.RIGOUR);
		AUGURY = definitions.get(VarbitDefinition.class, Varbit.AUGURY);
	}
	
	public static double compute(Player player) {
		double rate = 0;
		
		// Work with a cached variable to speed up lookups too
		int varpValue = player.varps().varp(83);
		
		if (Varps.varbitGet(varpValue, THICK_SKIN) == 1)
			//12 seconds level 1
			//1188 seconds level 99
			rate += 0.083;
		if (Varps.varbitGet(varpValue, BURST_OF_STRENGTH) == 1)
			rate += 0.083;
		if (Varps.varbitGet(varpValue, CLARITY_OF_THOUGHT) == 1)
			rate += 0.083;
		if (Varps.varbitGet(varpValue, SHARP_EYE) == 1)
			rate += 0.083;
		if (Varps.varbitGet(varpValue, MYSTIC_WILL) == 1)
			rate += 0.083;
		if (Varps.varbitGet(varpValue, ROCK_SKIN) == 1)
			rate += 0.17;
		if (Varps.varbitGet(varpValue, SUPERHUMAN_STRENGTH) == 1)
			rate += 0.17;
		if (Varps.varbitGet(varpValue, IMPROVED_REFLEXIS) == 1)
			rate += 0.17;
		if (Varps.varbitGet(varpValue, RAPID_RESTORE) == 1)
			rate += 0.04;
		if (Varps.varbitGet(varpValue, RAPID_HEAL) == 1)
			rate += 0.06;
		if (Varps.varbitGet(varpValue, PROTECT_ITEM) == 1)
			rate += 0.06;
		if (Varps.varbitGet(varpValue, HAWK_EYE) == 1)
			rate += 0.17;
		if (Varps.varbitGet(varpValue, MYSTIC_LORE) == 1)
			rate += 0.17;
		if (Varps.varbitGet(varpValue, STEEL_SKIN) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, ULTIMATE_STRENGTH) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, INCREDIBLE_REFLEXES) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, PROTECT_FROM_MELEE) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, PROTECT_FROM_MAGIC) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, PROTECT_FROM_MISSILES) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, EAGLE_EYE) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, MYSTIC_MIGHT) == 1)
			rate += 0.33;
		if (Varps.varbitGet(varpValue, RETRIBUTION) == 1)
			rate += 0.08;
		if (Varps.varbitGet(varpValue, REDEMPTION) == 1)
			rate += 0.17;
		if (Varps.varbitGet(varpValue, SMITE) == 1)
			rate += 0.56;
		if (Varps.varbitGet(varpValue, CHIVALRY) == 1)
			rate += 0.56;
		if (Varps.varbitGet(varpValue, PIETY) == 1)
			rate += 0.67;
		if (Varps.varbitGet(varpValue, RIGOUR) == 1)
			rate += 0.67;
		if (Varps.varbitGet(varpValue, AUGURY) == 1)
			rate += 0.67;
		
		if (BonusContent.isActive(player, BlessingGroup.DEVOTION) && !WildernessLevelIndicator.inWilderness(player.tile())) {
			rate /= 2;
		}
		
		return rate;
	}
	
}
