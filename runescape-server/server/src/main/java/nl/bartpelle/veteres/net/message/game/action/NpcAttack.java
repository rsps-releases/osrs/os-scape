package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.util.Tuple;

import java.lang.ref.WeakReference;

/**
 * Created by Bart on 8/12/2015.
 */
@PacketInfo(size = 3)
public class NpcAttack implements Action {
	
	private boolean run;
	private int index;
	
	private int opcode;
	private int size;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		index = buf.readULEShortA();
		run = buf.readByteS() == 1;
		
		this.opcode = opcode;
		this.size = size;
	}
	
	@Override
	public void process(Player player) {
		Npc other = player.world().npcs().get(index);
		
		if (other == null) {
			player.debug("NPC index=%d is null.", index);
		} else {
			log(player, opcode, size, "id=%d index=%d", other.id(), index);
			if (player.attribOr(AttributeKey.DEBUG, false))
				player.debug("Npc id=%d index=%d  dead: %s %s can-attack=%s", other.id(), index, other.dead() ? "Y" : "N", player.dead() ? "Y" : "N",
						player.attribOr(AttributeKey.DEBUG, false) ? PlayerCombat.canAttack(player, other) : "?");
			
			if (!player.locked() && !player.dead()) {
				player.stopActions(true);
				player.face(other);
				
				if (!other.dead()) {
					if (other.combatInfo() == null) {
						player.message("Without combat attributes this monster is unattackable.");
						return;
					}
					
					int slayerReq = SlayerCreature.slayerReq(other.id());
					int slayerReq2 = other.combatInfo().slayerlvl;
					if (!player.world().realm().isPVP() && Math.max(slayerReq, slayerReq2) > player.skills().level(Skills.SLAYER)) {
						player.message("You need a slayer level of " + Math.max(slayerReq, slayerReq2) + " to harm this NPC.");
						return;
					}
					
					// See if it's exclusively owned
					Tuple<Integer, Player> ownerLink = other.attribOr(AttributeKey.OWNING_PLAYER, new Tuple(-1, null));
					if (ownerLink.first() != null && ownerLink.first() >= 0 && ownerLink.first() != player.id()) {
						player.message("They don't seem interested in fighting you.");
						return;
					}
					
					player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(other));
					player.world().server().scriptExecutor().executeLater(player, PlayerCombat.script);
				}
			}
		}
	}
	
}
