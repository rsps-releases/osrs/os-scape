package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart Pelle on 8/23/2014.
 */
public class Logout extends Command {
	
	@Override
	public RSBuffer encode(Player player) {
		return new RSBuffer(player.channel().alloc().buffer(1)).packet(68);
	}
	
}
