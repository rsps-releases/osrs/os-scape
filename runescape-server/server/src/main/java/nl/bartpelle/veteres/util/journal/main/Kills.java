package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.Varp;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class Kills extends JournalEntry {

    @Override
    public void send(Player player) {
        int kills = player.varps().varp(Varp.KILLS);
        send(player, "<img=34> Kills", kills, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int kills = player.varps().varp(Varp.KILLS);
        player.sync().shout("<img=34> I have " + kills + " kills.");
    }

}