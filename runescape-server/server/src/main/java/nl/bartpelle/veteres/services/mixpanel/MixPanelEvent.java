package nl.bartpelle.veteres.services.mixpanel;

/**
 * Created by Bart on 11/1/2016.
 */
public enum MixPanelEvent {
	
	ADDED_FRIEND("Added Friend"),;
	
	private String identifier;
	
	MixPanelEvent(String identifier) {
		this.identifier = identifier;
	}
	
	public String identifier() {
		return identifier;
	}
	
}
