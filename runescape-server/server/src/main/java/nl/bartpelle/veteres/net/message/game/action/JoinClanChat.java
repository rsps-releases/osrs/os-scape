package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 12/11/2015.
 */
@PacketInfo(size = -1)
public class JoinClanChat implements Action {
	
	private String channel;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		if (size > 0) {
			channel = buf.readString();
		}
	}
	
	@Override
	public void process(Player player) {
		// If it's null, we're leaving
		if (channel == null) {
			int chan = player.attribOr(AttributeKey.CLAN_CHANNEL, -1);
			
			if (chan >= 0) {
				ClanChat chat = player.world().chats().get(chan);
				
				if (chat != null) {
					chat.leave(player, false);
					player.clearattrib(AttributeKey.CLAN_CHANNEL); // Don't rejoin on login.
				}
			}
			
			return;
		}
		
		ClanChat.join(player, channel);
		
		player.invokeScript(101);
	}
	
}
