package nl.bartpelle.veteres.util;

import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.interfaces.Equipment;
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup;
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;

/**
 * Created by Situations on 12/31/2015.
 */

public class RunEnergy {
	
	private Player player;
	
	public RunEnergy(Player forplayer) {
		this.player = forplayer;
	}
	
	public void update() {
		double energy = player.attribOr(AttributeKey.RUN_ENERGY, 0.0);
		
		double add = player.getRecoveryRate();
		
		if (player.world().realm().isOSRune() && !WildernessLevelIndicator.inWilderness(player.tile())) {
			add *= 2; // Double energy regeneration if we're not in the wilderness.
		}
		
		if (Equipment.wearsFullGraceful(player) || Equipment.wearingMaxCape(player)) {
			add *= 1.3; // 30% increase in restore rate when wearing full graceful
		}
		
		player.setRunningEnergy(energy + add, true);
	}
	
	public void drainForMove() {
		//if (player.world().realm().isDeadman() && player.world().server().config().hasPathOrNull("deadman.jaktestserver"))
		//	return;
		
		// The Obdurate blessing grants infinite run.
		if (BonusContent.isActive(player, BlessingGroup.OBDURATE) && !WildernessLevelIndicator.inWilderness(player.tile())) {
			return;
		}
		
		//Grabs the players energy %
		double energy = player.attribOr(AttributeKey.RUN_ENERGY, 0);
		//Grabs the change in energy
		double change = player.getEnergyDeprecation();
		//Check to see if the player has drank a stamina potion
		int stamina = player.attribOr(AttributeKey.STAMINA_POTION, 0);
		//If the player has drank a stamina potion, energy drain is reduced by 70%
		if (stamina > 0) change *= 0.3;
		//If for some reason the change is less then 0, we set it to 0.05
		if (change < 0) change = 0.05;
		
		//We apply the change to the players energy level
		player.setRunningEnergy(energy - change, true);
	}
}
