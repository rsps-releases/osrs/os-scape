package nl.bartpelle.veteres.services.financial;

import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.DonationTier;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.IronMode;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Bart on 1/1/2016.
 */
public class BMTPostback implements Service {
	
	private GameServer server;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
	}
	
	public void acquire(int account) {
		// Why would you lookup for someone not online? It's going to fail later.
		if (!server.world().playerForId(account).isPresent())
			return;
		
		acquireNew(account);
	}
	
	private void acquireNew(int account) {
		server.service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement(
							"UPDATE credit_transactions SET acquired = true WHERE account_id = ? AND acquired = false AND service_id = ?" +
									" RETURNING id, item, amount;");
					
					stmt.setInt(1, account);
					stmt.setInt(2, server.serviceId());
					
					List<Purchase> purchases = new LinkedList<>();
					ResultSet set = stmt.executeQuery();
					while (set.next()) {
						Purchase p = new Purchase();
						p.amount = set.getInt("amount");
						p.uid = set.getInt("id");
						p.product = set.getInt("item");
						
						purchases.add(p);
					}
					
					connection.commit();
					
					// Apply those
					Player player = server.world().playerForId(account).orElse(null);
					if (player != null) {
						for (Purchase purchase : purchases) {
							// Did we fail in applying this purchase? Reset the state.
							if (!applyNew(player, new Item(purchase.product, purchase.amount))) {
								PreparedStatement s = connection.prepareStatement("UPDATE credit_transactions SET acquired = FALSE WHERE id = ?;");
								s.setInt(1, purchase.uid);
								s.executeUpdate();
								connection.commit();
							}
						}
					} else {
						// Revert the changes
						for (Purchase purchase : purchases) {
							PreparedStatement s = connection.prepareStatement("UPDATE credit_transactions SET acquired = FALSE WHERE id = ?;");
							s.setInt(1, purchase.uid);
							s.executeUpdate();
							connection.commit();
						}
					}
					
					repollRank(account);
				}
			});
		});
	}
	
	private void repollRank(int account) {
		server.service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					int credits = 0;
					
					PreparedStatement stmt1 = connection.prepareStatement("SELECT sum(price::DOUBLE PRECISION) as donated FROM purchases WHERE account_id = ?;");
					PreparedStatement stmt2 = connection.prepareStatement("SELECT sum(total_price) as donated FROM invoices WHERE account_id = ? AND paid = TRUE AND ignore_price = FALSE;");
					PreparedStatement stmt3 = connection.prepareStatement("SELECT extra_usd, premium_points FROM accounts WHERE id = ?;");
					
					stmt1.setInt(1, account);
					stmt2.setInt(1, account);
					stmt3.setInt(1, account);
					
					ResultSet query1 = stmt1.executeQuery();
					ResultSet query2 = stmt2.executeQuery();
					ResultSet query3 = stmt3.executeQuery();
					
					double total = 0;
					
					if (query1.next())
						total += query1.getDouble("donated");
					if (query2.next())
						total += query2.getDouble("donated");
					if (query3.next()) {
						total += query3.getDouble("extra_usd");
						credits = query3.getInt("premium_points");
					}
					
					connection.commit();
					
					double finalTotal = total;
					int finalCredits = credits;
					server.world().playerForId(account).ifPresent(player -> {
						DonationTier oldRank = player.donationTier();
						int oldCredits = player.credits();
						player.totalSpent(finalTotal);
						player.credits(finalCredits);
						if (player.donationTier() != oldRank) {
							player.message("<col=1e44b3><img=22> Congratulations! You've advanced a donation rank!");
						}
						if (player.credits() > oldCredits) {
							player.message("<col=ff0000> Your online credits have been updated from " +
									NumberFormat.getInstance().format(oldCredits) + " to " +
									NumberFormat.getInstance().format(player.credits()) + ".");
						}
					});
				}
			});
		});
	}
	
	public void redeemBond(int account, int cr, boolean givesUsd) {
		server.service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("UPDATE accounts SET premium_points = premium_points + ? WHERE id = ? RETURNING premium_points;");
					stmt.setInt(1, cr);
					stmt.setInt(2, account);
					
					ResultSet set = stmt.executeQuery();
					
					if (set.next()) {
						int credits = set.getInt("premium_points");
						Player player = server.world().playerForId(account).orElse(null);
						if (player != null) {
							player.credits(credits);
							
							if (givesUsd) {
								player.totalSpent(player.totalSpent() + (double) cr * 0.1);
							}
							
							if (cr == 1) {
								player.message("Your credit has been added. You now have %s credits.", NumberFormat.getInstance().format(credits));
							} else {
								player.message("Your %d credits have been added. You now have %s credits.", cr, NumberFormat.getInstance().format(credits));
							}
							
							player.message("You can spend your credits at ::store.");
						}
					}
					
					connection.commit();
				}
			});
		});
	}
	
	private boolean applyNew(Player player, Item give) {
		// Dice bag
		if (give.id() == 12020) {
			// change to the new id
			give = new Item(6990, give.amount());
		}
		
		if (player.ironMode() == IronMode.ULTIMATE) {
			if (player.inventory().add(give, false).success()) {
				player.message("<col=ff0000>Your purchase of " + NumberFormat.getInstance().format(give.amount()) + " x " + give.name(player.world()) + " has been added to your inventory!");
			} else {
				GroundItem groundItem = new GroundItem(player.world(), give, player.tile(), player.id()).lifetime(30_000);
				player.world().spawnGroundItem(groundItem);
				player.message("<col=ff0000>Your purchase of " + NumberFormat.getInstance().format(give.amount()) + " x " + give.name(player.world()) + " has been dropped below you!");
			}
		} else {
			if (player.bank().add(give, false).failed()) {
				return false;
			}
			
			if (give.id() == 13190) {
				player.message("<col=ff0000>Your " + NumberFormat.getInstance().format(give.amount()) + " credit(s) have been added to your bank.");
			} else {
				player.message("<col=ff0000>Your purchase of " + NumberFormat.getInstance().format(give.amount()) + " x " + give.name(player.world()) + " has been stored in your bank!");
			}
		}
		
		return true;
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return false;
	}
	
	@Override
	public boolean isAlive() {
		return false;
	}
	
}
