package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class TotalSpent extends JournalEntry {

    @Override
    public void send(Player player) {
        double spent = Math.round(player.totalSpent() * 100.0) / 100.0;
        send(player, "<img=" + getTotalSpentIcon(player) + "> Total Spent", "$" +spent, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        player.world().server().scriptExecutor().executeScript(player, new QuestTab.TotalSpent(player));

    }

    private int getTotalSpentIcon(Player player) {
        double totalSpent = player.totalSpent();
        if (totalSpent >= 5000) {
            return 49; // Gold image
        } else if (totalSpent >= 2500) {
            return 48; // Pink image
        } else if (totalSpent >= 1000) {
            return 47; // Gold image
        } else if (totalSpent >= 500) {
            return 46; // Pink image
        } else if (totalSpent >= 250) {
            return 45; // Green image
        } else if (totalSpent >= 100) {
            return 44; // Blue image
        } else if (totalSpent >= 5) {
            return 43; // Red icon
        }
        return 43;
    }


}