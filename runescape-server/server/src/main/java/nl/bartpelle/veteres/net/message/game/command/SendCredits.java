package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.content.BuyCredits;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

public class SendCredits extends Command {

	private String message;
	private int discountPercent, selectedCreditPack, selectedPaymentMethod;
	private BuyCredits[] packs;


	public SendCredits(String message, int discountPercent, int selectedCreditPack, int selectedPaymentMethod, BuyCredits... packs) {
		this.message = message;
		this.discountPercent = discountPercent;
		this.selectedCreditPack = selectedCreditPack;
		this.selectedPaymentMethod = selectedPaymentMethod;
		this.packs = packs;
	}
	
	@Override
	protected RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(3 + strLen(message) + 3 + (packs.length * 12))).packet(-1).writeSize(RSBuffer.SizeType.SHORT);

		buffer.writeString(message);
		buffer.writeByte(discountPercent);
		buffer.writeByte(selectedCreditPack);
		buffer.writeByte(selectedPaymentMethod);

		for(BuyCredits pack : packs) {
			buffer.writeInt(pack.purchasePrice);
			buffer.writeInt(pack.purchaseAmount);
			buffer.writeInt(pack.freeAmount);
		}
		
		return buffer;
	}

	private static int strLen(String string) {
		return string == null ? 0 : string.length() + 1;
	}


}
