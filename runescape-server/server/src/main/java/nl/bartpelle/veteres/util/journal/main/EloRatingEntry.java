package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.content.mechanics.EloRating;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

/**
 * @author Heaven
 */
public class EloRatingEntry extends JournalEntry {

	@Override
	public void send(Player player) {
		int currentEloRating = player.attribOr(AttributeKey.ELO_RATING, EloRating.DEFAULT_ELO_RATING);
		send(player, "<img=82> Elo Rating: "+ getColorizer(currentEloRating).wrap(Integer.toString(currentEloRating)));
	}

	@Override
	public void select(Player player) {
		int currentEloRating = player.attribOr(AttributeKey.ELO_RATING, EloRating.DEFAULT_ELO_RATING);
		player.world().server().scriptExecutor().executeScript(player, new QuestTab.EloRatingInfo(player, currentEloRating, getColorizer(currentEloRating)));
	}

	private Color getColorizer(int value) {
		if (value > EloRating.DEFAULT_ELO_RATING) {
			return Color.GREEN;
		} else if (value < EloRating.DEFAULT_ELO_RATING) {
			return Color.RED;
		}

		return Color.DARK_YELLOW;
	}
}
