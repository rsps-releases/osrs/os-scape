package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 11/9/2015.
 */
@PacketInfo(size = 14)
public class SpellOnItem implements Action {
	
	private int desthash;
	private int sourcehash;
	private int sourceitem;
	private int destslot;
	private int destitem;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		desthash = buf.readIntV1();
		sourceitem = buf.readUShortA();
		sourcehash = buf.readLEInt();
		destitem = buf.readULEShort();
		destslot = buf.readULEShort();
		
		log(player, opcode, size, "source=[%d:%d] dest=[%d:%d] fromitem=%d toitem=%d toslot=%d",
				sourcehash >> 16, sourcehash & 0xFFFF, desthash >> 16, desthash & 0xFFFF, sourceitem, destitem, destslot);
	}
	
	@Override
	public void process(Player player) {
		if (player.locked()) {
			return;
		}
		
		int destid = desthash >> 16;
		int destchild = desthash & 0xFFFF;
		
		if (destid == 149 && destchild == 0) {
			player.debug("SpellOnItem: trigger=[%d, %d], %d, %d, %d", sourcehash >> 16, sourcehash & 0xFFFF, sourceitem, destslot, destitem);
			
			// Some checks
			if (destslot < 0 || destslot > 27) {
				return;
			}
			Item item = player.inventory().get(destslot);
			if (item == null || item.id() != destitem) {
				return;
			}
			
			player.stopActions(false); // Seems like this does not cancel moving on rs? I can alch while I run.
			player.world().server().scriptRepository().triggerSpellOnItem(player, sourcehash >> 16, sourcehash & 0xFFFF, destslot);
		}
	}
}
