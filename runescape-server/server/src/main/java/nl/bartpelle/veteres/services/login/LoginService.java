package nl.bartpelle.veteres.services.login;

import com.typesafe.config.Config;
import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.instance.InstancedMap;
import nl.bartpelle.veteres.model.uid.UIDProvider;
import nl.bartpelle.veteres.net.ServerHandler;
import nl.bartpelle.veteres.net.message.LoginRequestMessage;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.command.DisplayMap;
import nl.bartpelle.veteres.net.message.game.command.SetRealm;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.logging.DataLoggingService;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.services.serializers.JSONFileSerializer;
import nl.bartpelle.veteres.services.serializers.PlayerSerializer;
import nl.bartpelle.veteres.util.GameCommands;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Bart on 8/1/2015.
 * <p>
 * Handles logging in, logging out... and being logged in?
 */
public class LoginService implements Service {
	
	private static final Logger logger = LogManager.getLogger(LoginService.class);
	
	/**
	 * The queue of pending login requests, which is concurrent because there's (at least) two threads
	 * accessing this at the same time. One (or more) being the decoder thread from Netty, one (or more) being
	 * the login service worker.
	 */
	private LinkedBlockingQueue<LoginRequestMessage> messages = new LinkedBlockingQueue<>();
	
	/**
	 * The executor which houses the login service workers.
	 */
	private Executor executor;
	
	/**
	 * A reference to the game server which we process requests for.
	 */
	private GameServer gameServer;
	
	/**
	 * The serializer we use to (de)serialize our lovely player's data. Defaults to JSON.
	 */
	private PlayerSerializer serializer;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		gameServer = server;
		
		UIDProvider uidProvider = server.uidProvider();
		serializer = server.service(PlayerSerializer.class, true).orElse(new JSONFileSerializer(uidProvider));
		
		logger.info("Using {} to serialize and deserialize player data.", serializer.getClass().getSimpleName());
	}
	
	public void enqueue(LoginRequestMessage message) {
		messages.add(message);
	}
	
	public LinkedBlockingQueue<LoginRequestMessage> messages() {
		return messages;
	}
	
	@Override
	public boolean isAlive() {
		return true; // How could this service possibly be dead??
	}
	
	@Override
	public boolean start() {
		executor = Executors.newFixedThreadPool(2);
		
		for (int i = 0; i < 3; i++)
			executor.execute(new LoginWorker(this));
		
		return true;
	}
	
	@Override
	public boolean stop() {
		return false; // TODO disable the login service, denying requests. Might be a nice feature.
	}
	
	public GameServer server() {
		return gameServer;
	}
	
	public PlayerSerializer serializer() {
		return serializer;
	}
	
	public static void complete(Player player, GameServer server, LoginRequestMessage message) {
		// Log this login as successful
		server.service(DataLoggingService.class, true).ifPresent(DataLoggingService::increase);
		
		player.interfaces().resizable(message.resizableInterfaces());
		player.teleport(player.tile());
		
		// Attach player to session
		player.channel().attr(ServerHandler.ATTRIB_PLAYER).set(player);
		
		Optional<InstancedMap> activeMap = player.world().allocator().active(player.tile());
		if (activeMap.isPresent()) {
			// This is bad. Log it?
			player.write(new DisplayMap(player, true)); // This has to be the first packet!
			//player.write(new DisplayInstancedMap(activeMap.get(), player, player.tile(), true, true)); // This has to be the first packet!
		} else {
			player.write(new DisplayMap(player, true)); // This has to be the first packet!
		}
		
		player.write(new SetRealm(player.world().realm().id()));
		
		player.world().syncMap(player, null, false);
		player.channel().flush();
		
		// Log login
		if ((int) player.world().server().config().getInt("net.port") != 43595) {//dont log testing
			server.service(LoggingService.class, true).ifPresent(log -> {
				log.logLogin(player);
			});
		}
		
		// This isn't really a packet but it's required to be done on the logic thread
		player.pendingActions().add(new Action() {
			public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
			}
			
			public void process(Player player) {
				if (true || GameCommands.LOGIN_PROMPT_DISABLED || WildernessLevelIndicator.inAttackableArea(player) || player.tile().distance(new Tile(3222, 3222)) > 10) { // Dont show for pkers
					player.interfaces().setupGamepane(false);
					player.initiate();
				} else {
					player.putattrib(AttributeKey.NO_GPI, true); // dont get bodied on login lolz
					player.interfaces().loginScreen();
				}
			}
		});
	}
	
}
