package nl.bartpelle.veteres.services.qualityassurance;

import co.paralleluniverse.strands.Strand;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.http.UndertowService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Created by Bart on 4/26/2016.
 */
public class QAControl implements Service {
	
	private static final Logger logger = LogManager.getLogger(QAControl.class);
	
	private GameServer server;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
	}
	
	public void patch() {
		new Thread(() -> {
			logger.info("Applying QA server update...");
			try {
				Runtime.getRuntime().exec("tar", new String[]{"xf", "/root/distrib.tar"}, new File("."));
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("Exiting with error code 100 to signal reboot.");
			try {
				Strand.sleep(5000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.exit(100);
		}).start();
	}
	
	@Override
	public boolean start() {
		if (!server.serviceAlive(UndertowService.class)) {
			logger.error("Cannot start the world list server without Undertow being ready.");
			return false;
		}
		
		server.service(UndertowService.class, true).ifPresent(undertow -> {
			undertow.pathHandler().add("/worldlist.ws", exchange -> {
				server.world().update(50); // Half a minute. Nobody plays there anyway.
			});
		});
		
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
}
