package nl.bartpelle.veteres.services.http;

import com.typesafe.config.Config;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathTemplateHandler;
import io.undertow.util.StatusCodes;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.services.Service;

/**
 * Created by Bart on 5/29/2016.
 */
public class UndertowService implements Service {
	
	/**
	 * The port the HTTP service will respond on.
	 */
	private int port = 8180;
	
	/**
	 * The host to listen on for requests. On multi-world servers this has to be the public IP.
	 */
	private String host;
	
	/**
	 * SHA256 hash used to protect sensitive data accessing routes from outsiders.
	 */
	private String auth;
	
	/**
	 * The undertow instance, for stopping if needed.
	 */
	private Undertow server;
	
	/**
	 * The route handler for incoming requests. Other services can hook into this by adding a new path with a handler.
	 */
	private PathTemplateHandler pathHandler = Handlers.pathTemplate();
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		// Port defaults to 8180.
		if (serviceConfig.hasPath("port")) {
			port = serviceConfig.getInt("port");
		}
		
		// Mandatory to set it up.
		host = serviceConfig.getString("host");
		auth = serviceConfig.getString("auth");
	}
	
	@Override
	public boolean start() {
		server = Undertow.builder().addHttpListener(port, host).setHandler(pathHandler).build();
		server.start();
		return true;
	}
	
	@Override
	public boolean stop() {
		server.stop();
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
	public PathTemplateHandler pathHandler() {
		return pathHandler;
	}
	
	public HttpHandler authHandler(HttpHandler protectedHandler) {
		return exchange -> {
			if (auth.equals(exchange.getRequestHeaders().get("Auth", 0))) {
				protectedHandler.handleRequest(exchange);
			} else {
				exchange.setStatusCode(StatusCodes.UNAUTHORIZED);
				exchange.getResponseSender().send("Auth denied.");
			}
		};
	}
	
}
