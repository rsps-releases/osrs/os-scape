package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.services.intercom.PmService;
import nl.bartpelle.veteres.services.mixpanel.MixPanelEvent;
import nl.bartpelle.veteres.services.mixpanel.MixPanelService;

/**
 * Created by Bart on 11/15/2015.
 */
@PacketInfo(size = -1)
public class AddFriend implements Action {
	
	private String name;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		name = buf.readString();
	}
	
	@Override
	public void process(Player player) {
		player.world().server().service(PmService.class, true).ifPresent(pmService -> {
			if (player.social().friends().size() >= 200) {
				player.message("Your friends list is full. Maximum of 200 friends.");
				return;
			}
			
			pmService.addFriend(player, name);
			
			player.world().server().service(MixPanelService.class, true).ifPresent(mp -> {
				mp.event(MixPanelEvent.ADDED_FRIEND, player.ip(), player.world().realm().id());
			});
		});
		//TODO check if exists
	}
	
}
