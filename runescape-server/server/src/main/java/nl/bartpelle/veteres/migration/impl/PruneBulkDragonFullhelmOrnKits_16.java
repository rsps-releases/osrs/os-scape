package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;

/**
 * Created by Jak on 23/05/2017.
 */
public class PruneBulkDragonFullhelmOrnKits_16 implements Migration {
	@Override
	public int id() {
		return 16;
	}
	
	@Override
	public boolean apply(Player player) {
		// Prunes all orny kits. They used to be 0bm in shop on w2 then became 5k, people could sell back for 3750 each making millions of bm.
		if (player.world().realm().isPVP()) {
			Item kit = new Item(12538, Integer.MAX_VALUE); // Dragon full helm orn kit
			long removed = removeAll(player, kit);
			removed += removeAll(player, new Item(12539, Integer.MAX_VALUE)); // noted
			
			int refund = Math.min(2, (int) removed);
			
			if (refund > 0) {
				Item rf = new Item(13307, refund * 3750);
				player.bank().add(rf, false);
				
				String oldn = kit.name(player.world());
				
				// Correct name for feedback.
				if (kit.definition(player.world()).noted())
					oldn = kit.unnote(player.world()).name(player.world());
				
				Zulrah_uncharge_all_14.inform(player, "<col=FF0000>[UPDATE]</col> Item: <col=7F00FF>" + removed + " x " + oldn + "</col> was refunded as <col=7F00FF>" + rf.amount() + " x Blood money</col>.");
			}
		}
		return true;
	}
}
