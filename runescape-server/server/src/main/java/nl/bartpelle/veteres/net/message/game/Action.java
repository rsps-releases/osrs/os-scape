package nl.bartpelle.veteres.net.message.game;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.services.logging.LoggingService;

/**
 * Created by Bart Pelle on 8/22/2014.
 * <p>
 * Represents an incoming action sent by the client to the server.
 */
public interface Action {
	
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player);
	
	public void process(Player player);
	
	default void log(Player player, int id, int size, String format, Object... params) {
		if (player != null && player.logged()) {
			player.world().server().service(LoggingService.class, true).ifPresent(logging -> {
				logging.logPacket(player, id, size, String.format(format, params));
			});
		}
	}
	
}
