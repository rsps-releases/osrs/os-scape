package nl.bartpelle.veteres.migration.impl;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;

/**
 * Created by Jak on 26/07/2016.
 */
public class KbdDropWrongItemIdRangerBoots_9 implements Migration {
	
	@Override
	public int id() {
		return 9;
	}
	
	@Override
	public boolean apply(Player player) {
		
		long removed = removeAll(player, new Item(10696, Integer.MAX_VALUE));
		if (removed > 0L) {
			player.bank().add(new Item(2577, (int) removed), true);
			
			// Tell the player what has happened
			player.pendingActions().add(new Action() {
				@Override
				public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
				}
				
				@Override
				public void process(Player player) {
					player.message("<col=FF0000>Your broken Ranger's boots have been fixed and you can now wear them. They were put into your bank!");
				}
			});
			
		}
		return true;
	}
}
