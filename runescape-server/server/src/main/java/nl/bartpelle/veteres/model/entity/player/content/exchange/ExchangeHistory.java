package nl.bartpelle.veteres.model.entity.player.content.exchange;

/**
 * Created by Bart on 10/28/2016.
 */
public class ExchangeHistory {
	
	private int id;
	private ExchangeType type;
	private int amount;
	private int price;
	
	public ExchangeHistory(int id, int amount, int price, ExchangeType type) {
		this.id = id;
		this.amount = amount;
		this.price = price;
		this.type = type;
	}
	
	public int id() {
		return id;
	}
	
	public int amount() {
		return amount;
	}
	
	public int price() {
		return price;
	}
	
	public ExchangeType type() {
		return type;
	}
	
	@Override
	public String toString() {
		return "ExchangeHistory{" +
				"id=" + id +
				", type=" + type +
				", amount=" + amount +
				", price=" + price +
				'}';
	}
}
