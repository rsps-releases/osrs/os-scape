package nl.bartpelle.veteres.util;

/**
 * Created by Jason MacKeigan on 2016-09-29 at 10:27 AM
 */
public interface CycleParser {
	
	int toSeconds();
	
	int toMinutes();
	
	int toHours();
	
}
