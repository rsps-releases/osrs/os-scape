package nl.bartpelle.veteres.services.sql;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import nl.bartpelle.veteres.GameServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Bart on 8/1/2015.
 * <p>
 * Sql service for PostgreSQL. Veteres is built towards PgSql but may or may not provide housing for other database
 * services soon.
 */
public class PgSqlService implements SqlService {
	
	private static final Logger logger = LogManager.getLogger(PgSqlService.class);
	private static final boolean USE_HIKARI_CP = true;
	
	/**
	 * The connection pool we acquire connections from.
	 */
	private DataSource connectionPool;
	
	private GameServer server;
	private PgSqlWorker worker;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
		
		// Read configuration from the conf file
		String user = serviceConfig.getString("user");
		String pass = serviceConfig.getString("pass");
		String host = serviceConfig.getString("host");
		int port = serviceConfig.getInt("port");
		String database = serviceConfig.getString("database");
		
		if (USE_HIKARI_CP) {
			HikariConfig hikariConfig = new HikariConfig();
			hikariConfig.setAutoCommit(false);
			hikariConfig.setMinimumIdle(16);
			hikariConfig.setConnectionTimeout(1000);
			hikariConfig.setMaximumPoolSize(512);
			hikariConfig.setUsername(user);
			hikariConfig.setPassword(pass);
			hikariConfig.setRegisterMbeans(true);
			hikariConfig.setPoolName("hikari");
			hikariConfig.setLeakDetectionThreshold(2000);
			hikariConfig.setJdbcUrl(String.format("jdbc:postgresql://%s:%d/%s?prepareThreshold=0", host, port, database));
			hikariConfig.validate();
			
			connectionPool = new HikariDataSource(hikariConfig);
		} else {
			ComboPooledDataSource c3p0Config = new ComboPooledDataSource();
			c3p0Config.setMinPoolSize(8);
			c3p0Config.setInitialPoolSize(8);
			c3p0Config.setMaxPoolSize(4000);
			c3p0Config.setAcquireIncrement(8);
			c3p0Config.setUser(user);
			c3p0Config.setPassword(pass);
			c3p0Config.setJdbcUrl(String.format("jdbc:postgresql://%s:%d/%s?prepareThreshold=0", host, port, database));
			
			connectionPool = c3p0Config;
		}
		
		worker = new PgSqlWorker(this);
		new Thread(worker).start();
	}
	
	@Override
	public Connection connection() {
		SQLException e = null;
		
		for (int i = 0; i < 5; i++) {
			try {
				return connectionPool.getConnection();
			} catch (SQLException ee) {
				logger.error("Error acquiring connection from psql pool", ee);
				e = ee;
			}
		}
		
		throw new RuntimeException(e);
	}
	
	public PgSqlWorker worker() {
		return worker;
	}
	
	@Override
	public boolean start() {
		try {
			// Create a simple query to check for the version we use.
			try (Connection connection = connectionPool.getConnection()) {
				PreparedStatement version = connection.prepareStatement("SHOW SERVER_VERSION;");
				version.execute();
				version.getResultSet().next();
				
				// .. and print it to the console, cos we all love info. Yeah.
				logger.info("PostgreSQL service active; connected to version {}.", version.getResultSet().getString(1));
				
				// Also quickly remove the world online counter. Just in case.
				PreparedStatement removeOnline = connection.prepareStatement("DELETE FROM online_characters WHERE world_id=? AND service_id=?;");
				removeOnline.setInt(1, server.world().id());
				removeOnline.setInt(2, server.serviceId()); // Service id
				int removed = removeOnline.executeUpdate();
				
				// If the last shutdown wasn't graceful we need to inform.
				if (removed > 0) {
					logger.warn("Removed {} online user(s). Previous shutdown was most likely not graceful.", removed);
				}
				
				connection.commit();
			}
		} catch (Exception e) {
			logger.error("Could not connect to the database.", e);
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean stop() {
		connectionPool = null;
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return connectionPool != null;
	}
	
}
