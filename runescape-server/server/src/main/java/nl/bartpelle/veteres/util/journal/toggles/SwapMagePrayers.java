package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.SetAlignment;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class SwapMagePrayers extends JournalEntry {

    public static void update(Player player) {
        boolean swap = player.attribOr(AttributeKey.SWAP_MAGE_PRAYERS, false);
        player.write(new SetAlignment(541, (swap ? 31 : 27), 0, 148));
        player.write(new SetAlignment(541, (swap ? 27 : 31), 111, 185));
    }

    @Override
    public void send(Player player) {
        boolean old = player.attribOr(AttributeKey.SWAP_MAGE_PRAYERS, false);
        if(old)
            send(player, "<img=58> Swap Mage Prayer", "On", Color.GREEN);
        else
            send(player, "<img=58> Swap Mage Prayer", "Off", Color.RED);
        update(player);
    }

    @Override
    public void select(Player player) {
        boolean old = player.attribOr(AttributeKey.SWAP_MAGE_PRAYERS, false);
        player.putattrib(AttributeKey.SWAP_MAGE_PRAYERS, !old);
        send(player);
    }

}