package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class BreakVials extends JournalEntry {

    @Override
    public void send(Player player) {
        boolean keepVials = player.attribOr(AttributeKey.GIVE_EMPTY_POTION_VIALS, false);
        if(!keepVials)
            send(player, "<img=58> Break Vials", "On", Color.GREEN);
        else
            send(player, "<img=58> Break Vials", "Off", Color.RED);
    }

    @Override
    public void select(Player player) {
        boolean breakVials = player.attribOr(AttributeKey.GIVE_EMPTY_POTION_VIALS, false);
        player.putattrib(AttributeKey.GIVE_EMPTY_POTION_VIALS, !breakVials);
        send(player);
    }

}