package nl.bartpelle.veteres.migration.impl;

import nl.bartpelle.veteres.migration.Migration;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;

/**
 * Created by Bart on 4/3/2016.
 */
public class RemoveSerpAndTsotd_4 implements Migration {
	
	private static final Item UNCHARGED_SERPS = new Item(12929, Integer.MAX_VALUE);
	private static final Item UNCHARGED_NOTED_SERPS = new Item(12930, Integer.MAX_VALUE);
	private static final Item SERPS = new Item(12931, Integer.MAX_VALUE);
	private static final Item ALL_TSOTDS = new Item(12904, Integer.MAX_VALUE);
	
	@Override
	public int id() {
		return 4;
	}
	
	@Override
	public boolean apply(Player player) {
		// Remove serpentines
		long serp = removeAll(player, UNCHARGED_SERPS) + removeAll(player, UNCHARGED_NOTED_SERPS) + removeAll(player, SERPS);
		player.bank().add(new Item(13307, (int) serp * 7_000), true);
		
		// Remote toxic staves
		long staves = removeAll(player, ALL_TSOTDS);
		player.bank().add(new Item(13307, (int) staves * 7_000), true);
		
		return true;
	}
	
}
