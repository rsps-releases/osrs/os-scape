package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.quests.QuestGuide;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.Area;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

import java.util.Random;

public class Hotspot {

    public static final Hotspot[] HOTSPOTS = {
            new Hotspot("Edgeville", new Area(2993, 3523, 3124, 3597, -1)),
            new Hotspot("Mage Bank", new Area(3070, 3905, 3132, 3974, -1)),
            new Hotspot("Deserted Keep", new Area(3144, 3911, 3177, 3968, -1)),
            new Hotspot("East Dragons", new Area(3316, 3625, 3415, 3735, -1)),
            new Hotspot("West Dragons", new Area(2949, 3567, 3015, 3653, -1)),
            new Hotspot("Level 44 Obelisk", new Area(2966, 3834, 3024, 3888, -1)),
            new Hotspot("Level 50 Obelisk", new Area(3282, 3904, 3329, 3952, -1)),
            new Hotspot("Graveyard", new Area(3132, 3644, 3195, 3696, -1)),
            new Hotspot("Demonic Ruins", new Area(3261, 3856, 3346, 3903, -1)),
    };

    public static Hotspot ACTIVE = randomHotspot();

    public static Hotspot randomHotspot() {
        Random generator = new Random();
        int randomIndex = generator.nextInt(HOTSPOTS.length);
        return HOTSPOTS[randomIndex];
    }

    /**
     * Separator
     */

    public final String name;

    public final Area area;

    public Hotspot(String name, Area area) {
        this.name = name;
        this.area = area;
    }

    public static final class Entry extends JournalEntry {

        public static final Entry INSTANCE = new Entry();

        @Override
        public void send(Player player) {
            send(player, "<img=58> Hotspot", ACTIVE.name, Color.GREEN);
        }

        @Override
        public void select(Player player) {
            player.sendScroll("Wilderness Hotspots",
                    "Hotspots are locations inside the wilderness which give <col=006600>double",
                    "<col=006600>blood money</col> when you kill a player. The hotspot location changes",
                    "every 20 minutes. It's suggested that you PK here if you're",
                    "looking to make the most blood money per kill while pking.");
        }

    }
}
