package nl.bartpelle.veteres.content.mechanics.deadman.safezones;

/**
 * Created by Jak on 02/11/2016.
 * Represents a chunk with a specific Height level.. example (3200, 3200, 3) on top on Lumbridge.
 * The chunk ID is resolved to coordinates.
 */
public class LeveledChunk {
	
	public int chunkId, level;
	
	public LeveledChunk(int id) {
		this.chunkId = id;
	}
	
	public LeveledChunk(int id, int level) {
		this.chunkId = id;
		this.level = level;
	}
}
