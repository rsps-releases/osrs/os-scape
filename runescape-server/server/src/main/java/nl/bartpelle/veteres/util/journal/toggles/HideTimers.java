package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.SetVarp;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class HideTimers extends JournalEntry {

    public static void update(Player player) {
        boolean show = player.attribOr(AttributeKey.HIDE_TIMERS, false);
        player.write(new SetVarp(20_000, show ? 0 : 1));
    }

    @Override
    public void send(Player player) {
        boolean show = player.attribOr(AttributeKey.HIDE_TIMERS, false);
        if(show)
            send(player, "<img=58> Hide Timers", "On", Color.GREEN);
        else
            send(player, "<img=58> Hide Timers", "Off", Color.RED);
        update(player);
    }

    @Override
    public void select(Player player) {
        boolean show = player.attribOr(AttributeKey.HIDE_TIMERS, false);
        player.putattrib(AttributeKey.HIDE_TIMERS, !show);
        send(player);
    }

}