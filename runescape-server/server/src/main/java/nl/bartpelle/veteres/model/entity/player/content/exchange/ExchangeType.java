package nl.bartpelle.veteres.model.entity.player.content.exchange;

/**
 * Created by Bart on 10/28/2016.
 */
public enum ExchangeType {
	
	NULL,
	BUY,
	SELL
	
}
