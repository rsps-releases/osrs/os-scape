package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.Varp;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class Deaths extends JournalEntry {

    @Override
    public void send(Player player) {
        int deaths = player.varps().varp(Varp.DEATHS);
        send(player, "<img=13> Deaths", deaths, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int deaths = player.varps().varp(Varp.DEATHS);
        player.sync().shout("<img=13> I have " + deaths + " deaths.");
    }

}