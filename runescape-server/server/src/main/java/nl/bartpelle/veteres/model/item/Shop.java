package nl.bartpelle.veteres.model.item;

import nl.bartpelle.veteres.content.areas.edgeville.osrune.BMPrices;
import nl.bartpelle.veteres.content.interfaces.LostPropertyShop;
import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Skills;
import nl.bartpelle.veteres.net.message.game.command.InvokeScript;
import nl.bartpelle.veteres.net.message.game.command.SetItems;
import nl.bartpelle.veteres.net.message.game.command.UpdateCustomShop;
import nl.bartpelle.veteres.util.ItemsOnDeath;

import java.util.*;

/**
 * Created by Bart on 9/28/2015.
 */
public class Shop {
	
	private int id;
	private String name;
	private World world;
	private final Item[] original;
	private ShopItemEntry[] blob;
	private Item[] stock;
	private ItemContainer playerStock;
	private boolean sellBack;
	private boolean generalStore;
	private Set<Object> viewers = new HashSet<>(); // Player IDs
	private boolean noiron = false;
	private boolean free = false;
	private int currency;
	
	public Shop(int id, String name, World world, ShopItemEntry[] blob, Item[] stock, boolean general, boolean sellBack, int currency) {
		this.id = id;
		this.name = name;
		this.world = world;
		this.blob = blob;
		this.stock = stock.clone();
		this.original = stock.clone();
		generalStore = general;
		this.sellBack = sellBack;
		
		if (currency == 0 && world.realm().isPVP()) {
			this.currency = 13307;
		} else {
			this.currency = currency;
		}
		
		if (general) {
			playerStock = new ItemContainer(world, 40 - stock.length, ItemContainer.Type.FULL_STACKING);
		}
	}
	
	public boolean hasPlayerStock() {
		return playerStock != null;
	}
	
	public ItemContainer playerStock() {
		return playerStock;
	}
	
	public Item[] stock() {
		return stock;
	}

	public String name() {
		return name;
	}
	
	public boolean generalStore() {
		return generalStore;
	}
	
	public int currency() {
		return currency;
	}

	public ShopItemEntry[] container() {
		return blob;
	}
	
	public boolean canSellBack() {
		return sellBack;
	}
	
	public boolean warriorTokenShop() {
		if (world.realm().isOSRune() && id == 24)
			return true;
		else
			return false;
	}
	
	public int id() {
		return id;
	}
	
	public Set<Object> playerIdsViewing() {
		return viewers;
	}
	
	//Called whenever someone buys something
	public void refresh() {
		for (Iterator<Object> it = viewers.iterator(); it.hasNext(); ) {
			Object viewer = it.next();
			
			if (viewer != null) {
				Optional<Player> p_ = world.playerForId(viewer);
				
				// Was this player still online?
				if (!p_.isPresent()) {
					it.remove();
					continue;
				}
				
				Player p = p_.get();
				
				// Check if the player has the shop
				if (p.interfaces().visible(300)) {
					p.putattrib(AttributeKey.SHOP_DIRTY, true);
				} else {
					// Unregister from viewers
					it.remove();
				}
			} else {
				it.remove();
			}
		}
	}
	
	public void restock(boolean fast) {
		// Process stock first
		for (int i = 0; i < stock.length; i++) {
			Item item = stock[i];
			Item base = original[i];
			
			// Safety checks :)
			if (item == null || base == null || base.id() != item.id() && item.amount() != base.amount())
				continue;
			
			boolean restockToMaxStock = false;
			int stockid = item.id();
			
			if (world.realm().isPVP()) {
				//stock doesnt affect supply at all we've got like 100k of each item.
				restockToMaxStock = true;
			} else {
				// Other realms with inf stock
				
				if (id == 8) { // Aubury's runes shop (w1)
					// By having a slow restock rate (w1) for natures etc, we make runecrafting worthwhile.
					// only for ESSENTIAL runes. blood, death, chaos, elemental.
					// Limited stock (5k) for - nats, cosmic, laws
					if ((stockid >= 554 && stockid <= 560) || stockid == 562 || stockid == 565) {
						restockToMaxStock = true;
					}
				} else if (id == 7) { // range ammo (w1)
					// low level ammo. Bronze - addy arrows, bronze-mith bolts.
					if ((stockid >= 882 && stockid <= 890) || stockid == 887 || (stockid >= 9140 && stockid <= 9142)) {
						restockToMaxStock = true;
					}
				}
				// Varrock supplies store. w3
				// Gen store & supplies w3/w1
				// Runes / range ammo w3 - since the base stock is 50k, having infiniate replenish rates isn't as much of an impact
				// as it would be for limited stocks of w1.
				if (id == 28 || id == 6 || id == 19 || id == 1359 || id == 1362 || id == 25 || id == 26 || id == 22 || id == 21 || id == 10) {
					restockToMaxStock = true;
				}
			}
			if (restockToMaxStock && item.amount() < base.amount()) {
				stock[i] = new Item(item.id(), base.amount()); // set back to default
				continue;
			}
			
			// If fast we should only restock the 100+ blob
			if (base.amount() >= 100 && !fast)
				continue;
			if (base.amount() < 100 && fast)
				continue;
			
			// Do we need to remove one, or add one?
			if (item.amount() < base.amount()) {
				stock[i] = new Item(item.id(), item.amount() + 1); // Increase stock levels
			} else if (item.amount() > base.amount()) {
				stock[i] = new Item(item.id(), item.amount() - 1); // Decrease stock levels
			}
		}
		
		// Process the general store stocks (fastmode does not apply to general stores)
		if (generalStore && !fast) {
			for (int i = 0; i < playerStock.size(); i++) {
				Item item = playerStock.get(i);
				if (item == null)
					continue;
				
				// Ensure we didn't accidentally add a 0-size item
				if (item.amount() < 1) {
					playerStock.set(i, null);
					continue;
				}
				
				// Items with size 1 can be nullified :-)
				if (item.amount() == 1) {
					playerStock.set(i, null);
				} else {
					// Remove a stock sample
					playerStock.set(i, new Item(item.id(), item.amount() - 1));
				}
			}
		}
		
		// Synch our changes to players around the world.
		refresh();
	}
	
	public void display(Player player) {
		
		// Perdu's shop has been phased out.
		if (id == 1352) {
			player.message("Perdu's shop is closed.");
			return;
		}
		
		if (player.world().realm().isPVP() && player.world().rollDie(6, 1)) {
			player.filterableMessage("Running short on Blood money? Check out the store at store.os-scape.com!");
		}
		
		int mainInter = 300;
		
		if (player.world().realm().isPVP()) {
			if (id == 52) {
				mainInter = 178;
			} else {
				mainInter = 100;
			}
		}
		
		player.putattrib(AttributeKey.SHOP, this);
		viewers.add(player.id());
		
		player.invokeScript(InvokeScript.SETVARCS, -1, -1);
		player.interfaces().sendInventory(301);
		
		if (player.world().realm().isPVP()) {
			player.interfaces().setting(mainInter, (mainInter == 178 ? 2 : 3), 0, stock.length, 2046);//1086
			player.write(new UpdateCustomShop(name, mainInter, stock));
			player.interfaces().sendMain(mainInter);
		} else {
			player.interfaces().setting(300, 2, 0, 40, 1278); // was 1054
		}
		
		player.interfaces().setting(301, 0, 0, 27, 1086);
		player.invokeScript(1074, 13, name);
		
		player.invokeScript(149, 19726336, 93, 4, 7, 0, -1, "Value<col=ff9040>", "Sell 1<col=ff9040>", "Sell 5<col=ff9040>", "Sell 10<col=ff9040>", "Sell 50<col=ff9040>");
		refreshFor(player);
	}
	
	public void refreshFor(Player player) {
		if (generalStore) {
			player.write(new SetItems(13, new Item[][]{stock, playerStock.copy()}));
		} else if (id == 1352) { // Perdu's shop
			ArrayList<Item> lost_items = new ArrayList<Item>();
			
			for (int untradId : ItemsOnDeath.RS_UNTRADABLES_LIST) {
				if (untradId != 19722 && !(untradId >= 8844 && untradId <= 8850)) // dragon defender (t) is a special case and cannot be reclaimed. It converts to the normal one when lost.
					lost_items.add(new Item(untradId, LostPropertyShop.getCount(player, untradId)));
			}
			
			player.write(new SetItems(13, lost_items.toArray(new Item[lost_items.size()])));

            /*int count = 0; // debug
            for (Item i : lost_items) {
                if (i != null) {
                    count += i.amount();
                }
            }*/
			//player.debug("lost blob count: "+count);
		} else if (id == 48) { // Ranging guild, if 99 range you can buy the cape.
			
			ArrayList<Item> dynamic_stock = new ArrayList<>();
			
			// Rebuild stock, excluding the Mastery cape if you haven't mastered the skill.
			for (Item i : this.stock)
				if (i != null && !(i.id() == 9756 && player.skills().xpLevel(Skills.RANGED) < 99))
					dynamic_stock.add(i);
			
			player.write(new SetItems(13, dynamic_stock.toArray(new Item[dynamic_stock.size()])));
		} else {
			player.write(new SetItems(13, stock));
		}
		player.invokeScript(1074, 13, name);// TODO: 9/29/2015 is this necessary
	}
	
	// Fixed economy server item prices (buy price). sell price is this reduced by x%
	public static int SHARK = 1500, ATT = 6000, STR = 7000, DEF = 3000, RANGE = 8500, PRAY = 13500, BREW = 12500, REST = 14000;
	
	// Fixed economy server item prices (buy price). sell price is this reduced by x%
	public static int SHARK_OSRUNE = 1500, ATT_OSRUNE = 4000, STR_OSRUNE = 5000, DEF_OSRUNE = 4000,
			RANGE_OSRUNE = 8500, PRAY_OSRUNE = 7000, BREW_OSRUNE = 6000, REST_OSRUNE = 8000;
	
	/**
	 * UN NOTED VERSIONS
	 *
	 * @param item
	 * @return
	 */
	public int buyPriceOf(int item) {
		if (item == 5514)  return 300_000; //Giant pouch
		else if (item == 5512) return 200_000;//Large pouch
		else if (item == 5510) return 100_000;//Medium pouch
		else if (item == 5509) return 50_000;//Small pouch

		// Spawn PVP does not modify prices based on current stock.
		if (world.realm().isPVP() && id != 51) {
			if (specialPriceOf(item) != -1) {
				return specialPriceOf(item);
			} else if (BMPrices.getPriceOf(item) != -1) {
				return BMPrices.getPriceOf(item);
			}
			if (id == 1352) { // lost property
				return (int) (basePriceOf(item) / 2); // 50% cheaper for lost blob
			}
			return basePriceOf(item);
		}
		if (world.realm().isOSRune()) {
			if (id == 1363) return 1000; // 1k teleports
			if (item == 9075) return 186; // astral
			switch (item) {
				case 560: return 302; // deaths
				case 561: return 298; // nature
				case 562: return 101; // chaos rune
				case 563: return 290; // law
				case 564: return 192; // cosmic
				case 12738: return 10100; // chaos rune pack
				case 9185: return 61000; // rune crossbow
				case 3105: return 1000; // climbers
				case 2503: return 18000; // black hide top
				case 2497: return 13000; // black hide bottom
				case 9243: return 600; // diamond bolts
				case 9244: return 2700; // drag bolts e
				case 868: return 600; // rune knife
			}
		}
		if (id == 28) { //Supply store
			if (item == 385)
				if (world.realm().isPVP()) return SHARK; else return SHARK_OSRUNE;
			 //Sharks
			if (item == 2436)
				if (world.realm().isPVP()) return ATT; else return ATT_OSRUNE;
			 //Super attack potion
			if (item == 2440)
				if (world.realm().isPVP()) return STR; else return STR_OSRUNE;
			 //Super strength potion
			if (item == 2442)
				if (world.realm().isPVP()) return DEF; else return DEF_OSRUNE;
			 //Super defence
			if (item == 2444)
				if (world.realm().isPVP()) return RANGE; else return RANGE_OSRUNE;
			 //Range potion
			if (item == 2434)
				if (world.realm().isPVP()) return PRAY; else return PRAY_OSRUNE;
			 //Prayer potion
			if (item == 6685)
				if (world.realm().isPVP()) return BREW; else return BREW_OSRUNE;
			 //Saradomin brew
			if (item == 3024)
				if (world.realm().isPVP()) return REST; else return REST_OSRUNE;
			 //Super restore
		}
		
		if (world.realm().isOSRune()) {
			if (item == 3842 || item == 3840 || item == 3844 || item == 12608 || item == 12610 || item == 12612) { // Mage books
				return 100000;
			}
		}
		
		int originalQty = baseStockOf(item);
		double basePrice = basePriceOf(item);
		int qty = stockOf(item);
		
		double modifier = (double) basePrice / 50d;
		if (generalStore) {
			modifier = basePrice * 0.6d / 20d;
		}
		
		int mult = originalQty - qty;
		double change = modifier * mult;
		
		// If this item is not by default sold, the price is different.
		int cap = (int) (basePrice / 2);
		int hicap = (int) (basePrice + basePrice / 2);
		//System.out.printf("prices -- cap:%d  hicap:%d  gen=%s mult=%d change=%f  base gp=%f%n", cap, hicap, generalStore, mult, change, basePrice);
		if (generalStore) {
			return Math.max(cap, Math.min(hicap, Math.max((int) (basePrice * 0.3), (int) ((basePrice * 1.3) + change))));
		} else {
			return Math.max(cap, Math.min(hicap, Math.max((int) (basePrice * 0.3), (int) (basePrice + change))));
		}
	}
	
	public int warriorTokenBuyPriceOf(int item) {
		if (world.realm().isOSRune()) {
			if (item == 8844) {
				return 50;
			} // Bronze Defender
			if (item == 8845) {
				return 50;
			} // Iron Defender
			if (item == 8846) {
				return 50;
			} // Steel Defender
			if (item == 8847) {
				return 50;
			} // Black Defender
			if (item == 8848) {
				return 50;
			} // Mithril Defender
			if (item == 8849) {
				return 50;
			} // Adamant Defender
			if (item == 8850) {
				return 200;
			} // Runite Defender
			if (item == 12954) {
				return 300;
			} // Dragon Defender
			if (item == 10548) {
				return 300;
			} // Fighter hat
			if (item == 10551) {
				return 500;
			} // Fighter torso
			if (item == 7454) {
				return 50;
			} // Bronze Gloves
			if (item == 7455) {
				return 50;
			} // Iron Gloves
			if (item == 7456) {
				return 50;
			} // Steel Gloves
			if (item == 7457) {
				return 50;
			} // Black Gloves
			if (item == 7458) {
				return 50;
			} // Mithril Gloves
			if (item == 7459) {
				return 50;
			} // Adamant Gloves
			if (item == 7460) {
				return 50;
			} // Runite Gloves
			if (item == 7461) {
				return 50;
			} // Dragon Gloves
			if (item == 7462) {
				return 100;
			} // Barrows Gloves
			if (item == 8839) {
				return 3000;
			} // Void Knight Top
			if (item == 8840) {
				return 3000;
			} // Void Knight Robe
			if (item == 8841) {
				return 350;
			} // Void Knight Mace
			if (item == 8842) {
				return 350;
			} // Void Knight Gloves
			if (item == 11663) {
				return 3000;
			} // Void Mage Helm
			if (item == 11664) {
				return 3000;
			} // Void Ranger Helm
			if (item == 11665) {
				return 3000;
			} // Void Melee Helm
			if (item == 13072) {
				return 4500;
			} // Elite Void Top
			if (item == 13073) {
				return 4500;
			} // Elite Void Robe
			if (item == 10499) {
				return 200;
			} // Ava's Accumulator
			else return 9999; // Can't find the item.
		} else {
			if (item == 2414) {
				return 120;
			} // Zamorak Cape
			if (item == 2412) {
				return 120;
			} // Saradomin Cape
			if (item == 2413) {
				return 120;
			} // Guthix Cape
			if (item == 3842) {
				return 320;
			} // Unholy Book
			if (item == 3840) {
				return 320;
			} // Holy Book
			if (item == 3844) {
				return 320;
			} // Book of Balance
			if (item == 8844) {
				return 100;
			} // Bronze Defender
			if (item == 8845) {
				return 200;
			} // Iron Defender
			if (item == 8846) {
				return 300;
			} // Steel Defender
			if (item == 8847) {
				return 400;
			} // Black Defender
			if (item == 8848) {
				return 500;
			} // Mithril Defender
			if (item == 8849) {
				return 600;
			} // Adamant Defender
			if (item == 8850) {
				return 700;
			} // Runite Defender
			if (item == 12954) {
				return 800;
			} // Dragon Defender
			if (item == 10548) {
				return 1000;
			} // Fighter hat
			if (item == 10551) {
				return 1000;
			} // Fighter torso
			if (item == 7454) {
				return 25;
			} // Bronze Gloves
			if (item == 7455) {
				return 50;
			} // Iron Gloves
			if (item == 7456) {
				return 100;
			} // Steel Gloves
			if (item == 7457) {
				return 150;
			} // Black Gloves
			if (item == 7458) {
				return 200;
			} // Mithril Gloves
			if (item == 7459) {
				return 250;
			} // Adamant Gloves
			if (item == 7460) {
				return 300;
			} // Runite Gloves
			if (item == 7461) {
				return 350;
			} // Dragon Gloves
			if (item == 7462) {
				return 400;
			} // Barrows Gloves
			if (item == 8839) {
				return 3000;
			} // Void Knight Top
			if (item == 8840) {
				return 3000;
			} // Void Knight Robe
			if (item == 8841) {
				return 350;
			} // Void Knight Mace
			if (item == 8842) {
				return 350;
			} // Void Knight Gloves
			if (item == 11663) {
				return 3000;
			} // Void Mage Helm
			if (item == 11664) {
				return 3000;
			} // Void Ranger Helm
			if (item == 11665) {
				return 3000;
			} // Void Melee Helm
			if (item == 13072) {
				return 4500;
			} // Elite Void Top
			if (item == 13073) {
				return 4500;
			} // Elite Void Robe
			if (item == 10499) {
				return 200;
			} // Ava's Accumulator
			if (item == 13124) {
				return 5000;
			} // Ardoungne Cloak 4
			else return 9999; // Can't find the item.
		}
	}
	
	/**
	 * UN NOTED ID
	 *
	 * @param item
	 * @return
	 */
	public int sellPriceOf(int item) {
		// Spawn PVP does not modify prices based on current stock.
		if (world.realm().isPVP()) {
			return (int) (buyPriceOf(item) * 0.75);
		}
		
		if (id == 28) { // supplies (eco)
			if (item == 385) {//shark
				return 650;
			}
			if (item == 2436) {//att
				return 1400;
			}
			if (item == 2440) {//str
				return 2200;
			}
			if (item == 2442) { // super def
				return 600;
			}
			if (item == 2444) {//range
				return 3200;
			}
			if (item == 2434) {//pray
				return 4100;
			}
			if (item == 6685) {//brew
				return 4700;
			}
			if (item == 3024) {//rest
				return 4900;
			}
		}
		
		int originalQty = baseStockOf(item);
		double basePrice = basePriceOf(item);
		int qty = stockOf(item);
		
		double modifier = basePrice / 50d;
		if (generalStore) {
			modifier = basePrice * 0.6d / 20d;
		}
		
		int mult = originalQty - qty;
		double change = modifier * mult;
		
		// If this item is not by default sold, the price is different.
		int cap = (int) (basePrice + (basePrice / 3));
		
		if (generalStore) {
			return Math.min(cap, Math.max((int) (basePrice * 0.3 / 3), (int) (basePrice / 2.5 + change)));
		} else if (currency == 6529) { // Tokkul
			return Math.min((int) (basePrice * 0.1), Math.max((int) (basePrice * 0.1 / 3), (int) (((basePrice * 0.1) + change))));
		} else if (currency == 11849) { // Graceful
			return (int) (basePrice * 0.8);
		} else {
			return Math.min(cap, Math.max((int) (basePrice * 0.3 / 3), (int) (((basePrice * 0.6) + change))));
		}
	}
	
	private int basePriceOf(int id) {
		// On a pk world, we take the catalog price of an item.
		int x = new Item(id).unnote(world).id();
		if (world.realm().isPVP()) {
			if (id() == 51) {
				return (world.definitions().get(ItemDefinition.class, id).cost + (world.definitions().get(ItemDefinition.class, id).cost / 2));
			}
			// Catalog contains prices.
			switch (id) {
				case 11808:
					return 6_000; // Zamorak godsword
				case 20374:
					return 6_000; // zgs (or)
				case 11806:
					return 6_000; // Saradomin godsword
				case 20372:
					return 6_000; // sgs (or)
				case 11804:
					return 10_000; // Bandos godsword
				case 20370:
					return 10_000; // bgs (or)
				case 11802:
					return 30_000; // Armadyl godsword
				case 20368:
					return 30_000; // ags (or)
				case 12851:
					return 25_000; // Amulet of the damned
				case 12853:
					return 25_000; // Amulet of the damned
				case 11832:
					return 5_000; // Bandos chestplate
				case 11834:
					return 7_500; // Bandos tassets
				case 11826:
					return 4_000; // Armadyl helmet
				case 11828:
					return 7_000; // Armadyl chestplate
				case 11830:
					return 7_000; // Armadyl chainskirt
				case 6918:
					return 500; // Infinity hat
				case 6916:
					return 750; // Infinity top
				case 6924:
					return 750; // Infinity bottoms
				case 6920:
					return 750; // Infinity boots
				case 6922:
					return 500; // Infinity gloves
				case 12419:
					return 5_500; // Light infinity hat (ornament)
				case 12420:
					return 5_750; // Light infinity top (orn)
				case 12421:
					return 5_750; // Light infinity bottoms (orn)
				case 12457:
					return 5_500; // Dark infinity hat (orn)
				case 12458:
					return 5_750; // Dark infinity top (orn)
				case 12459:
					return 5_750; // Dark infinity bottoms (orn)
				case 11791:
					return 5_000; // Staff of the dead
				case 12902:
					return 7_000; // Uncharged Toxic staff of the dead
				case 12904:
					return 7_000; // Toxic staff of the dead
				case 11283:
					return 9_000; // Dragonfire shield
				case 13265:
					return 13_000; // Abyssal dagger normal
				case 13267:
					return 13_000; // Abyssal dagger p
				case 13269:
					return 13_000; // Abyssal dagger p+
				case 13271:
					return 13_000; // Abyssal dagger p++
				case 6585:
					return 3_500; // Amulet of fury
				case 12436:
					return 3_500; // Amulet of fury (or)
				case 12929:
					return 7_000; // Serpentine helm uncharged
				case 12931:
					return 7_000; // Serpentine helm
				case 13196:
					return 7_000; // Tanz helm uncharged
				case 13197:
					return 7_000; // Tanz helm
				case 13198:
					return 7_000; // Magma helm uncharged
				case 13199:
					return 7_000; // Magma helm
				case 11924:
					return 7_000; // Malediction ward
				case 11926:
					return 7_000; // Odium ward
				case 12806:
					return 13_000; // Malediction (or)
				case 12807:
					return 13_000; // Odium (or)
				case 4224:
					return 6_500; // New crystal shield
				case 4212:
					return 10_000; // New crystal bow
				case 4225:
					return 6_500; // Crystal shield (full)
				case 4214:
					return 10_000; // Crystal bow (full)
				case 4207:
					return 3_000; // Crystal seed. Can be enchanted for BM into other blob.
				case 13080:
					return 9_000; // Crystal halberd (i) (new)
				case 13081:
					return 9_000; // Crystal halberd (i) (full)
				case 13091:
					return 9_000; // Crystal halberd (new)
				case 13092:
					return 9_000; // Crystal halberd  (full)
				case 12002:
					return 3_000; // Occult necklace
				case 19720:
					return 3_000; // Occult necklace (or)
				case 11785:
					return 15_000; // Armadyl crossbow
				case 12829:
					return 600; // Spirit shield
				case 12831:
					return 4500; // Blessed spirit shield
				case 12817:
					return 75_000; // Elysian spirit shield
				case 12825:
					return 50_000; // Arcane spirit shield
				case 12821:
					return 30_100; // Spectral spirit shield
				case 4151:
					return 750; // Abyssal whip
				case 11838:
					return 3200; // Saradomin sword
				case 12006:
					return 4_000; // Abyssal tentacle
				case 12808:
					return 4_000; // Blessed sara sword (full)
				case 12809:
					return 4_000; // Blessed sara sword
				case 11235:
					return 750; // Dark bow
				case 11840:
					return 550; // Dragon boots
				case 2577:
					return 1_000; // Ranger boots
				case 13227:
					return 2_500; // Eternal crystal
				case 13229:
					return 2_500; // Pegasian crystal
				case 13231:
					return 2_500; // Primordial crystal
				case 6889:
					return 4_000; // Mage's book
				case 6914:
					return 2_000; // Master wand
				case 11905:
					return 4_000; // Trident
				case 11907:
					return 4_000; // Trident
				case 11908:
					return 4_000; // Trident
				case 12899:
					return 4_000; // Trident
				case 12900:
					return 4_000; // Trident
				case 6524:
					return 400; // Obby shield (defensive rune defender) aka Toktz-ket-xil
				case 13235:
					return 3_250; // Eternal boots. Combined from infinity & eternal crystal.
				case 13237:
					return 3_500; // Pegasian boots. Rangers & peg crystal.
				case 13239:
					return 3_050; // Primordial boots. Dragon boots & prim crystal.
				case 1038:
					return 80_000; // Red Partyhat
				case 1040:
					return 90_000; // Yellow Partyhat
				case 1042:
					return 100_000; // Blue Partyhat
				case 1044:
					return 75_000; // Green Partyhat
				case 1046:
					return 50_000; // Purple Partyhat
				case 1048:
					return 70_000; // White Partyhat
				case 11862:
					return 120_000; // Black Partyhat
				case 11863:
					return 120_000; // Rainbow Partyhat
				case 12399:
					return 120_000; // Partyhat & specs
				case 1053:
					return 40_000; // Green halloween mask
				case 1055:
					return 40_000; // Blue halloween mask
				case 1057:
					return 40_000; // Red halloween mask
				case 11847:
					return 45_000; // Black halloween mask
				case 1050:
					return 40_000; // Santa hat
				case 13343:
					return 40_000; // Black santa hat
				case 13344:
					return 40_000; // Inverted santa hat
				case 12845:
					return 5_000; // Grim reaper hood
				case 1419:
					return 5_000; // Scythe
				case 9920:
					return 7_500; // Jack lantern mask
				case 9925:
					return 7_500; // Skeleton mask
				case 9924:
					return 7_500; // Skeleton shirt
				case 9923:
					return 7_500; // Skeleton leggings
				case 9922:
					return 3_500; // Skeleton gloves
				case 9921:
					return 3_500; // Skeleton boots
				case 13283:
					return 7_500; // Gravedigger mask
				case 13284:
					return 7_500; // Gravedigger top
				case 13285:
					return 7_500; // Gravedigger leggings
				case 13286:
					return 3_500; // Gravedigger gloves
				case 13287:
					return 3_500; // Gravedigger boots
				case 6665:
					return 7_500; // Mudskipper hat
				case 6666:
					return 7_500; // Flippers
				case 4566:
					return 5_000; // Rubber chicken
				case 11919:
					return 7_500; // Cow mask
				case 12956:
					return 7_500; // Cow top
				case 12957:
					return 7_500; // Cow leggings
				case 12958:
					return 3_500; // Cow gloves
				case 12959:
					return 3_500; // Cow boots
				case 1037:
					return 5_000; // Bunny ears
				case 13182:
					return 5_000; // Bunny feet
				case 4565:
					return 5_000; // Easter basket
				case 12887:
					return 7_500; // Santa mask
				case 12888:
					return 7_500; // Santa top
				case 12889:
					return 7_500; // Santa leggings
				case 12890:
					return 3_500; // Santa gloves
				case 12891:
					return 3_500; // Santa boots
				case 12353:
					return 5_000; // Monocle
				case 12434:
					return 5_000; // Top hat & monocle
				case 12337:
					return 7_500; // Sagacious spectacles
				case 12892:
					return 7_500; // Anti-santa mask
				case 12893:
					return 7_500; // Anti-santa top
				case 12894:
					return 7_500; // Anti-santa leggings
				case 12895:
					return 3_500; // Anti-santa gloves
				case 12896:
					return 3_500; // Anti-santa boots
				case 7537:
					return 5_000; // Crab claw
				case 8929:
					return 5_000; // Crab claw hook
				case 2997:
					return 5_000; // Pirate's hook
				case 8924:
					return 500; // Bandana eyepatch
				case 8925:
					return 500; // Bandana eyepatch
				case 8926:
					return 500; // Bandana eyepatch
				case 8927:
					return 500; // Bandana eyepatch
				case 8950:
					return 1_500; // Pirate's hat
				case 12412:
					return 2_500; // Piate's hat & patch
				case 12355:
					return 2_000; // Big pirate's hat
				case 2631:
					return 600; // Highwayman mask
				case 2639:
					return 400; // Tan cavalier
				case 2641:
					return 400; // Dark cavalier
				case 12325:
					return 600; // Navy cavalier
				case 12323:
					return 600; // Red cavalier
				case 12321:
					return 800; // White cavalier
				case 2643:
					return 800; // Black cavalier
				case 11280:
					return 800; // Cavalier mask
				case 12319:
					return 1_500; // Crier hat
				case 12249:
					return 2_500; // Imp mask
				case 12251:
					return 2_500; // Goblin mask
				case 12361:
					return 2_500; // Cat mask
				case 12428:
					return 2_500; // Penguin mask
				case 12245:
					return 3_000; // Beanie
				case 12430:
					return 3_000; // Afro
				case 12359:
					return 2_500; // Leprechaun hat
				case 12540:
					return 2_500; // Deerstalker
				case 2645:
					return 400; // Red headband
				case 2647:
					return 400; // Black headband
				case 2649:
					return 400; // Brown headband
				case 12299:
					return 400; // White headband
				case 12301:
					return 400; // Blue headband
				case 12303:
					return 400; // Gold headband
				case 12305:
					return 400; // Pink headband
				case 12307:
					return 400; // Green headband
				case 10316:
					return 300; // Bob's red shirt
				case 10318:
					return 300; // Bob's blue shirt
				case 10322:
					return 300; // Bob's black shirt4
				case 10324:
					return 300; // Bob's purple shirt
				case 12375:
					return 500; // Black cane
				case 12377:
					return 1_500; // Adamant cane
				case 12379:
					return 3_000; // Rune cane
				case 12373:
					return 5_000; // Dragon cane
				case 12363:
					return 2_500; // Bronze dragon mask
				case 12365:
					return 750; // Iron dragon mask
				case 12367:
					return 1_000; // Steel dragon mask
				case 12369:
					return 1_250; // Mithril dragon mask
				case 12371:
					return 2_500; // Lava dragon mask
				case 12518:
					return 1_500; // Green dragon mask
				case 12522:
					return 1_500; // Red dragon mask
				case 12524:
					return 1_500; // Black dragon mask
				case 12526:
					return 5_000; // Fury ornament kit
				case 12528:
					return 5_000; // Dark infinity colour kit
				case 12530:
					return 5_000; // Light infinity colour kit
				case 12532:
					return 5_000; // Dragon sq shield ornament kit
				case 12534:
					return 5_000; // Dragon chainbody ornament kit
				case 12536:
					return 5_000; // Dragon plate/skirt ornament kit
				case 12538:
					return 5_000; // Dragon full helm ornament kit
				case 12849:
					return 5_000; // Granite clamp (kit)
				case 12848:
					return 6_000; // Granite maul (ornament)
				case 12757:
					return 1_000; // Blue dark bow paint
				case 12759:
					return 1_000; // Green dark bow paint
				case 12761:
					return 1_000; // Yellow dark bow paint
				case 12763:
					return 1_000; // White dark bow paint
				case 12769:
					return 1_000; // Frozen whip mix
				case 12771:
					return 1_000; // Volcanic whip mix
				case 12774:
					return 5_750; // Frozen Abyssal whip (not listed)
				case 12773:
					return 5_750; // Volcanic Abyssal whip (not listed)
				case 12766:
					return 5_750; // Colored dbow (not listed)
				case 12765:
					return 5_750; // Colored dbow (not listed)
				case 12767:
					return 5_750; // Colored dbow (not listed)
				case 12768:
					return 5_750; // Colored dbow (not listed)
				case 12798:
					return 5_000; // Steam staff upgrade kit
				case 12796:
					return 5_000; // Steam staff (ornament)
				case 8844:
					return 0; // Bronze defender
				case 8845:
					return 0; // Iron defender
				case 8846:
					return 0; // Steel defender
				case 8847:
					return 0; // Black defender
				case 8848:
					return 0; // Mithril defender
				case 8849:
					return 0; // Addy defender
				case 8850:
					return 0; // Rune defender
				case 12954:
					return 800; // Dragon defender
				case 6570:
					return 3_000; // Fire cape
				case 21295:
					return 4_500; //Infernal cape
				case 13280:
					return 4_000; // Max cape
				case 13342:
					return 4_000; // Max cape with right-click equip options
				case 13329:
					return 5_000; // Fire max cape
				case 13331:
					return 5_000; // Saradomin max cape
				case 13333:
					return 5_000; // Zamorak max cape
				case 13335:
					return 5_000; // Guthix max cape
				case 13337:
					return 5_000; // Ava's max cape
				case 13124:
					return 3_000; // Ardougne cloak 4
				case 20760:
					return 5_000; // Ardougne max cape.
				case 11663:
					return 1_500; // Mage helm
				case 11664:
					return 1_500; // Range helm
				case 11665:
					return 1_500; // Melee helm
				case 13072:
					return 3_500; // Elite void top
				case 8839:
					return 2_500; // Void top
				case 13073:
					return 3_500; // Elite void robe
				case 8840:
					return 2_500; // Void robe
				case 8842:
					return 2_000; // Void gloves
				case 10547:
					return 1_000; // Healing hat
				case 10548:
					return 1_000; // Fighter hat
				case 10549:
					return 1_000; // Runner hat
				case 10550:
					return 1_000; // Ranger hat
				case 10551:
					return 2_000; // Fighter torso
				case 10552:
					return 1_000; // Runner boots
				case 10555:
					return 1_000; // Penance skirt
				case 4708:
					return 500; // Ahrims hood
				case 4716:
					return 500; // Dharoks helm
				case 4724:
					return 500; // Guthans helm
				case 4732:
					return 500; // Karils coif
				case 4745:
					return 500; // Torags helm
				case 4753:
					return 500; // Veracs helm
				case 4712:
					return 750; // Ahrims robetop
				case 4720:
					return 500; // Dharoks platebody
				case 4728:
					return 500; // Guthans platebody
				case 4736:
					return 750; // Karils leathertop
				case 4749:
					return 500; // Torags platebody
				case 4757:
					return 500; // Veracs brassard
				case 4714:
					return 750; // Ahrims robeskirt
				case 4722:
					return 500; // Dharoks platelegs
				case 4730:
					return 500; // Guthans skirt
				case 4738:
					return 500; // Karils leatherskirt
				case 4751:
					return 500; // Torags platelegs
				case 4759:
					return 500; // Veracs plateskirt
				case 4710:
					return 500; // Ahrims Staff
				case 4718:
					return 500; // Dharoks axe
				case 4726:
					return 500; // Guthans warpsear
				case 4734:
					return 500; // Karils crossbow
				case 4747:
					return 500; // Torags hammers
				case 4755:
					return 500; // Veracs flail
				case 21978: // Super antifire potion
					return 500;
				case 12695:
					return 10; // Super combat potion (4)
				case 12913:
					return 100; // Anti-venom+(4)
				case 12907:
					return 25; // Anti-venom(4)
				case 12909:
					return 18; // Anti-venom(3)
				case 12911:
					return 12; // Anti-venom(2)
				case 12905:
					return 6; // Anti-venom(1)
				case 12625:
					return 0; // Stamina potion KEEP THIS AS 0. PEOPLE HAVE BOUGHT OUT 5K BY MISTAKE. CANNOT use migration tool to give back value=dupe!
				case 11936:
					return 2; // Dark crab
				case 3144:
					return 0; // Cooked Karambwuan SAME AS STAMINA. Id was noted instead of non-noted: return people bought 100k out in 10 minutes. Can't refund.
				case 385:
					return 0; //Shark
				case 11212:
					return 10; // Dragon arrows
				case 8013:
					return 0; // Home teles
				case 9244:
					return 0; // Spawnable d bolts (e)
				case 9243:
					return 0; // Spawnable diamond bolts (e)
				case 892:
					return 0; // Spawnable rune arrows
				case 4153:
					return 1000; // Granite maul
				case 1725:
					return 0; // Str amulet
				case 2550:
					return 2; // Recoil rings
				case 12642:
					return 2; // Lumberyard teleport scroll
				case 6731:
					return 2500; // Seers ring
				case 6733:
					return 4000; // Archers ring
				case 6735:
					return 800; // Warriors ring
				case 6737:
					return 4250; // Berserker ring
				case 11770:
					return 4000; // Seers ring (i)
				case 11771:
					return 7500; // Archer ring (i)
				case 11772:
					return 1400; // Warriors ring (i)
				case 11773:
					return 7750; // Berserker ring (i)
				case 2412:
					return 0; // God cape
				case 2413:
					return 0; // God cape
				case 2414:
					return 0; // God cape
				case 2581:
					return 15_000; // Robin hood hat
				case 11128:
					return 1000; // Obby neck
				case 6528:
					return 200; // Obby maul
				case 6525:
					return 180; // Obby dagger/knife
				case 12788:
					return 150; // Msb (i)
				case 12596:
					return 25_000; // Ranger's tunic
				case 11898:
					return 500; // Decorative hat
				case 11896:
					return 500; // Decorative top
				case 11897:
					return 500; // Decorative bottom
				case 11899:
					return 500; // Decorative range top
				case 11900:
					return 500; // Decorative range bottom
				case 12637:
					return 500; // Sara halo
				case 12638:
					return 500; // Zam halo
				case 12639:
					return 500; // Guth halo
				case 10156:
					return 100; // Hunter's crossbow
				case 13028:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13030:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13032:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13034:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13052:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13054:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13056:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13058:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13060:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13062:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13040:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13042:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13044:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13046:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13048:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 13050:
					return 35; // Rune armour set -- could be a god set: return (t) or (g) (sk) or (pl)
				case 12453:
					return 600; // Black wiz (g) hat
				case 12449:
					return 1000; // Black wiz (g) top
				case 12445:
					return 800; // Black wiz (g) bottom
				case 12455:
					return 600; // Black wiz (t) hat
				case 12451:
					return 1000; // Black wiz (t) top
				case 12447:
					return 800; // Black wiz (t) bottom
				case 10452:
					return 100; // Vestment robes
				case 10446:
					return 100; // Vestment robes
				case 10454:
					return 100; // Vestment robes
				case 10448:
					return 100; // Vestment robes
				case 10456:
					return 100; // Vestment robes
				case 10450:
					return 100; // Vestment robes
				case 10458:
					return 300; // Vestment robes
				case 10464:
					return 300; // Vestment robes
				case 10460:
					return 300; // Vestment robes
				case 10468:
					return 300; // Vestment robes
				case 10462:
					return 300; // Vestment robes
				case 10466:
					return 300; // Vestment robes
				case 12193:
					return 300; // Vestment robes
				case 12195:
					return 300; // Vestment robes
				case 12253:
					return 300; // Vestment robes
				case 12255:
					return 300; // Vestment robes
				case 12265:
					return 300; // Vestment robes
				case 12267:
					return 300; // Vestment robes
				case 13307:
					return 1; // Blood money is worth 1 bm duh
				case 13576:
					return 35_000; // Dragon warhammer: NOT IN STORES. Only through webstore (but protection price..)
				case 1249:
					return 250; // D spear. Also from pvm in limited supply (dont fuck prices that much)
				case 1187:
					return 900; // D sq
				case 12418:
					return 900; // d sq (g)
				case 4585:
					return 380; // D plateskirt
				case 12416:
					return 380; // d skirt (g)
				case 4087:
					return 380; // d legs
				case 12415:
					return 380; // d legs (g)
				case 3140:
					return 450; // d chain
				case 12414:
					return 450; // d chain (g)
				case 11335:
					return 5000; // d full helm
				case 12417:
					return 5000; // d full helm (g)
				case 6809:
					return 150; // Granite platelegs
				case 3122:
					return 200; // Granite shield
				case 9069:
					return 15; // Moonclan armour peice
				case 9070:
					return 15; // Moonclan armour peice
				case 9071:
					return 15; // Moonclan armour peice
				case 9072:
					return 15; // Moonclan armour peice
				case 9073:
					return 15; // Moonclan armour peice
				case 9074:
					return 15; // Moonclan armour peice
				case 13652:
					return 60_000; // Dragon claws
				case 12924:
					return 20_000; // Toxic blowpipe (empty)
				case 12926:
					return 20_000; // Toxic blowpipe
				case 1523:
					return 0; // Lockpick
				case 2572:
					return 5_000; // Ring of wealth
				case 1727:
					return 0; // Amulet of Magic
				case 1729:
					return 0; // Amulet of defence
				case 1731:
					return 0; // Amulet of power
				case 1478:
					return 0; // Amulet of accuracy
				case 11978:
					return 0; // Amulet of glory
				case 11090:
					return 0; // Pheonix necklace
				case 7458:
					return 0; // Mithril gloves
				case 7459:
					return 0; // Addy gloves
				case 7460:
					return 0; // Rune gloves
				case 7461:
					return 0; // Dragon gloves
				case 7462:
					return 0; // Barrows gloves
				case 2446:
					return 0; // Antipoison(4)
				case 3840:
					return 0; // Holy book
				case 3842:
					return 0; // Unholy book
				case 3844:
					return 0; // Book of balance
				case 12608:
					return 0; // Book of war
				case 12610:
					return 0; // Book of law
				case 12612:
					return 0; // Book of darkness
				case 2415:
					return 0; // Saradomin staff
				case 2416:
					return 0; // Guthix staff
				case 2417:
					return 0; // Zamorak staff
				case 861:
					return 0; // Magic shortbow
				case 6328:
					return 0; // Snakeskin boots
				case 4740:
					return 0; // Bolt rack
				case 868:
					return 0; // Rune knife
				case 811:
					return 0; // Rune dart
				case 10034:
					return 0; // Chincompa
				
				// THIRD AGE
				case 10330:
					return 32000; // range top
				case 10332:
					return 32000; // range legs
				case 10334:
					return 32000; // range coif
				case 10336:
					return 32000; // vambs
				case 10338:
					return 32000; // ropbe top
				case 10340:
					return 32000; // robe bottom
				case 10342:
					return 32000; // mage hat
				case 10344:
					return 32000; // ammy
				case 10346:
					return 32000; // platelegs
				case 10348:
					return 32000; // platebody
				case 10350:
					return 32000; // full helm
				case 10352:
					return 32000; // kiteshield
				case 12422:
					return 32000; // wand
				case 12424:
					return 32000; // bow
				case 12426:
					return 32000; // longsword
				case 12437:
					return 32000; // cloak
				case 6990:
					return 1000; // dice bag
				
				case 19478:
					return 10_000; // Light ballista
				case 19481:
					return 31_000; // Heavy ballista
				case 19484:
					return 500; // Dragon Javelin
				case 19553:
					return 30_900; // Amulet of torture- above ags: return spectral: return under arc: return ballista
				case 20366:
					return 30_900; // torture (or)
				case 19547:
					return 30_200; // Necklace of anguish- above ags: return spectral: return under arc: return ballista
				case 19550:
					return 25_000; // Ring of suffering - defensive ring
				case 19710:
					return 25_000; // Ring of suffering (i)
				case 20655:
					return 25_000; // Ring of suffering (r)
				case 20657:
					return 25_000; // Ring of suffering (ri)
				case 19544:
					return 30_500; // Tormented bracelet - above ags: return spectral: return under arc: return ballista
				
				case 4860:
					return 100; // Barrows 0
				case 4866:
					return 100; // Barrows 0
				case 4872:
					return 100; // Barrows 0
				case 4878:
					return 100; // Barrows 0
				case 4884:
					return 100; // Barrows 0
				case 4890:
					return 100; // Barrows 0
				case 4896:
					return 100; // Barrows 0
				case 4902:
					return 100; // Barrows 0
				case 4908:
					return 100; // Barrows 0
				case 4914:
					return 100; // Barrows 0
				case 4920:
					return 100; // Barrows 0
				case 4926:
					return 100; // Barrows 0
				case 4932:
					return 100; // Barrows 0
				case 4938:
					return 100; // Barrows 0
				case 4944:
					return 100; // Barrows 0
				case 4950:
					return 100; // Barrows 0
				case 4956:
					return 100; // Barrows 0
				case 4962:
					return 100; // Barrows 0
				case 4968:
					return 100; // Barrows 0
				case 4974:
					return 100; // Barrows 0
				case 4980:
					return 100; // Barrows 0
				case 4986:
					return 100; // Barrows 0
				case 4992:
					return 100; // Barrows 0
				case 4998:
					return 100; // Barrows 0
				case 299:
					return 1; // Mithril seeds.
				case 11824:
					return 5000; // Zammy spear
				case 11889:
					return 5000; // Zammy hasta
				case 19566:
					return 3000; // Skotizo's key: return to make it tradable.
				case 20000:
					return 20000; // Dragon scimitar (g)
				case 20002:
					return 20000; // Dragon scimitar ornament kit
				case 20143:
					return 10000; // Dragon defender ornament kit
				case 19722:
					return 10800; // Dragon defender (ornament)
				case 20062:
					return 12500; // Amulet of torture ornament kit
				case 20065:
					return 12500; // Occult ornament kit
				case 20068:
					return 15000; // Armadyl godsword ornament kit
				case 20071:
					return 15000; // Bandos godsword ornament kit
				case 20074:
					return 15000; // Saradomin godsword ornament kit
				case 20077:
					return 15000; // Zamorak godsword ornament kit
				case 12802:
					return 10000; // Ward update kit
				case 7158:
					return 3000; //Dragon 2H
				case 12800:
					return 5000; // Dragon pickaxe upgrade kit
				case 12797:
					return 5000; // Dragon pickaxe (orn)
				case 11920:
					return 250; // Dragon pickaxe
				
				case 21003:
					return 100_000; // Elder maul
				case 21006:
					return 60_000; // Kodai wand
				case 21018:
					return 60_000; // Ancestral hat
				case 21021:
					return 76_000; // Ancestral robe top
				case 21024:
					return 67_000; // Ancestral robe bottom
				case 20997:
					return 135_000; // Twisted bow
				case 20849:
					return 1_600; // Dragon thrownaxe
				case 21000:
					return 50_000; // Twisted buckler
				case 21028:
					return 1_600; // Dragon harpoon
				case 21012:
					return 37_000; // Dragon hunter crossbow
				case 21009:
					return 7_000; // Dragon sword
				case 21015:
					return 80_000; // Dinh's Bulwark
				case 13215:
					return 1_000; //Blood token
				case 12791:
					return 5_000; // Rune pouch
				case 11061:
					return 800; // Ancient Mace
				case 10887: //Barrelchest anchor
					return 500;
				case 22296:
					return 9_000; // Staff of light
			}
			
			// Maybe free shop?
			if (free) {
				return 0;
			}
			
			// No? Zero!
			return 0;
		}
		
		switch (id) {
			case 9185: return 90000; // Rune crossbow
			case 385: return 800; // Shark
			case 1712: return 150000; // Ammy of glory
			
			case 3024: return 7500; // Super restore (4)
			case 6685: return 3846; // Sara brew (4)
			case 565: return 300; // Bloods
			
			case 209: return 1500; // Irit
			case 213: return 3500; // Kwuarm
			case 215: return 2500; // Cadantine
			case 3051: return 6000; // Snapdragon
			case 3049: return 2000; // Toadflax
			
			// Teletabs:
			case 8007:
			case 8008:
			case 8009:
			case 8010:
			case 8011:
			case 8012: return 250;
			
			// Arceeus teletabs:
			case 19613:
			case 19615:
			case 19617:
			case 19619:
			case 19621:
			case 19623:
			case 19625:
			case 19627:
			case 19629:
			case 19631: return 1250;
		}
		
		// Tokkul shops are 1.5x cost
		if (currency == 6529) {
			return (int) (world.definitions().get(ItemDefinition.class, id).cost + (world.definitions().get(ItemDefinition.class, id).cost / 2));
		} else {
			return world.definitions().get(ItemDefinition.class, id).cost;
		}
	}
	
	public int removeStock(int id, int num) {
		for (int i = 0; i < stock.length; i++) {
			Item item = stock[i];
			if (item != null && item.id() == id) {
				Item repl = new Item(item.id(), item.amount() - num);
				stock[i] = repl;
				
				refresh();
				return Math.min(num, item.amount());
			}
		}
		
		// Check for player-sold blob to the general store
		if (generalStore) {
			for (int i = 0; i < playerStock.size(); i++) {
				Item item = playerStock.get(i);
				if (item != null && item.id() == id) {
					Item repl = new Item(item.id(), item.amount() - num);
					playerStock.set(i, repl);
					
					refresh();
					return Math.min(num, item.amount());
				}
			}
			
			// Couldn't sell yet, try to add it to a new general slot
			if (playerStock.freeSlots() > 0) {
				ItemContainer.Result result = playerStock.add(new Item(id, 0 - num), true);
				refresh();
				return Math.min(num, result.completed());
			}
		}
		
		return 0;
	}
	
	public int specialPriceOf(int id) {
		for (int i = 0; i < blob.length; i++) {
			if (blob[i].id() == id && blob[i].specialPrice() > -1) {
				return blob[i].specialPrice();
			}
		}
		return -1;
	}
	
	public int stockOf(int item) {
		return stockOf(item, stock) + (generalStore ? stockOf(item, playerStock.items()) : 0);
	}
	
	public int baseStockOf(int item) {
		return stockOf(item, original);
	}
	
	private int stockOf(int item, Item[] check) {
		for (Item i : check) {
			if (i != null && i.id() == item)
				return i.amount();
		}
		
		return 0;
	}
	
	public boolean sells(int item) {
		for (Item i : stock) {
			if (i != null && i.id() == item)
				return true;
		}
		
		return false;
	}
	
	public static class Def {
		public int id;
		public String name;
		public boolean general;
		public boolean sellBack;
		public ShopItemEntry[] stock;
		public int currency;
	}
	
	public static class ShopItemEntry {
		
		private int id;
		private int amount;
		private int specialPrice = -1;
		private Item[] array;
		
		public int id() {
			return id;
		}
		
		public int amount() {
			return amount;
		}
		
		public int specialPrice() {
			return specialPrice;
		}
		
		public Item toItem() {
			return new Item(id(), amount());
		}
		
		public void resolveArray(Item[] array) {
			this.array = array;
		}
		
		public Item[] toArray() {
			return array;
		}
	}
}
