package nl.bartpelle.veteres.services.sql;

import co.paralleluniverse.strands.Strand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Bart on 9/18/2015.
 * <p>
 * Represents a worker for the queries and transactions.
 */
public class PgSqlWorker implements Runnable {
	
	private static final Logger logger = LogManager.getLogger(PgSqlWorker.class);
	
	private LinkedBlockingQueue<SqlTransaction> transactions = new LinkedBlockingQueue<>();
	private PgSqlService sqlService;
	private int delay = 0;
	
	public PgSqlWorker(PgSqlService service) {
		sqlService = service;
	}
	
	public PgSqlWorker(PgSqlService service, LinkedBlockingQueue<SqlTransaction> queue) {
		sqlService = service;
		transactions = queue;
	}
	
	public PgSqlWorker delay(int d) {
		delay = d;
		return this;
	}
	
	public void submit(SqlTransaction transaction) {
		transactions.add(transaction);
	}
	
	@Override
	public void run() {
		while (true) {
			logic();
			
			try {
				Strand.sleep(delay > 0 ? delay : 100);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	private void logic() {
		try {
			// Have we got a valid connection?
			while (!transactions.isEmpty()) {
				SqlTransaction transaction = null;
				
				try {
					transaction = transactions.take();
				} catch (Exception ignored) {
				}
				
				// Did we grab a transaction successfully?
				if (transaction == null) {
					return;
				}
				
				Connection connection = sqlService.connection();
				
				// Execute the query
				try {
					connection.clearWarnings();
					connection.setAutoCommit(false);
					transaction.execute(connection);
					safeClose(connection);
				} catch (Exception e) {
					e.printStackTrace();
					// Let's not spam the log with the strack trace in this case, it's common and we know about it.
					if (e.getMessage() == null || !e.getMessage().equals("Rolling back, could not verify first stage querying."))
						logger.error(e);
					try {
						connection.rollback();
						connection.clearWarnings();
					} catch (SQLException ignored) {
					} // If that fails we might as well jump off a cliff
					
					// Yeah, we failed. Let's increase the counter.
					transaction.increaseAttempts();
					
					// We have a limit on 5 failures. If it's more, there's trouble going on. Retry, or notify.
					if (transaction.attempts() <= 5)
						transactions.add(transaction);
					else {
						logger.error("Could not execute transaction {} after 5 attempts!", transaction, e);
					}
					
					// Always close this connection
					safeClose(connection);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	private void safeClose(Connection connection) {
		try {
			connection.close();
		} catch (Exception ignored) {
		}
	}
	
}
