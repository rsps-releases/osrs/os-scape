package nl.bartpelle.veteres.services.intercom;

import com.google.gson.Gson;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.financial.BMTPostback;
import nl.bartpelle.veteres.services.redis.RedisService;
import nl.bartpelle.veteres.util.JGson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import redis.clients.jedis.JedisPubSub;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by Bart on 12-3-2015.
 * <p>
 * Provides inter-world private messaging support.
 */
public class RedisCommander extends JedisPubSub implements Service {
	
	private static final Logger logger = LogManager.getLogger();
	
	private static final String CHANNEL = "commander_addr";
	
	private GameServer server;
	private RedisService redisService;
	private Gson gson;
	private ForkJoinPool fjp = new ForkJoinPool();
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.gson = JGson.get();
		this.server = server;
	}
	
	public GameServer server() {
		return server;
	}
	
	
	@Override
	public void onMessage(String channel, String message) {
		try {
			switch (channel) {
				case CHANNEL:
					Optional<RedisCommand> pm = safeFromJson(message, RedisCommand.class);
					if (pm.isPresent()) {
						logger.debug("Redis commander message event: {}.", message);
						
						// Process this
						RedisCommand cmdobj = pm.get();
						
						// See if this is aimed at this server
						if (cmdobj.worldTarget == null || cmdobj.worldTarget < 0 || cmdobj.worldTarget.equals(server.world().id())) {
							processCommand(cmdobj);
						}
					}
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void processCommand(RedisCommand command) {
		switch (command.command) {
			case "mute": {
				playerFromCommand(command).ifPresent(player -> {
					player.muted(Timestamp.from(Instant.now().plus(1, ChronoUnit.YEARS)));
					player.message("You have been muted.");
				});
				break;
			}
			case "unmute": {
				playerFromCommand(command).ifPresent(player -> {
					player.muted(null);
					player.message("You have been unmuted.");
				});
				break;
			}
			case "fetchpurchases": {
				playerFromCommand(command).ifPresent(player -> {
					player.world().server().service(BMTPostback.class, true).ifPresent(bmt -> bmt.acquire((Integer) player.id()));
				});
				break;
			}
			case "kick": {
				playerFromCommand(command).ifPresent(Player::logout);
				break;
			}
			case "log": {
				playerFromCommand(command).ifPresent(p -> p.logged(true));
				break;
			}
			case "unlog": {
				playerFromCommand(command).ifPresent(p -> p.logged(false));
				break;
			}
		}
	}
	
	private Optional<Player> playerFromCommand(RedisCommand command) {
		if (command.params == null || !command.params.containsKey("playerid")) {
			return Optional.empty();
		}
		
		return server.world().playerForId(Integer.parseInt(command.params.get("playerid").toString()));
	}
	
	private <T> Optional<T> safeFromJson(String json, Class<T> typeOf) {
		try {
			return Optional.of(gson.fromJson(json, typeOf));
		} catch (Throwable t) {
			return Optional.empty();
		}
	}
	
	@Override
	public boolean start() {
		Optional<RedisService> service = server.service(RedisService.class, false);
		if (!service.isPresent()) {
			logger.error("Cannot use Redis commander if the Redis service is disabled");
			return false;
		}
		
		redisService = service.get();
		
		if (redisService.isAlive()) {
			ExecutorService executor = Executors.newSingleThreadExecutor();
			executor.submit(() -> redisService.doOnPool(j -> j.subscribe(this, CHANNEL)));
			
			logger.info("Redis commander ready to dispatch and process commands.");
		} else {
			logger.error("Cannot use Redis commander if the Redis service failed to start");
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
	// Gson classes
	private static class RedisCommand {
		
		public String command;
		public int account; // Account id of the user invoking this (can be zero)
		public int name; // Displayname of the user invoking this
		public Integer worldTarget; // Nullable (null means all worlds)
		public Map<String, Object> params; // Optional parameter array
		
	}
	
}
