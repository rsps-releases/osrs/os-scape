package nl.bartpelle.veteres.services.grandexchange;

import co.paralleluniverse.strands.Strand;
import com.typesafe.config.Config;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeOffer;
import nl.bartpelle.veteres.services.Service;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.MutableTuple;
import nl.bartpelle.veteres.util.Tuple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Bart on 10/29/2016.
 */
public class GrandExchangePriceService implements Service {
	
	private static final Logger logger = LogManager.getLogger(GrandExchangePriceService.class);
	private static final long SYNC_INTERVAL = TimeUnit.HOURS.toMillis(24);
	
	private GameServer server;
	private int serviceId;
	private Int2ObjectArrayMap<MutableTuple<Integer, Long>> completedOffers;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		this.server = server;
		this.serviceId = server.serviceId();
		this.completedOffers = new Int2ObjectArrayMap<>();
		
		new Thread(() -> {
			logger.info("Grand exchange price worker alive on service {}.", serviceId);
			
			while (true) {
				try {
					Strand.sleep(SYNC_INTERVAL); // No need for continuous execution.
					computerPrices();
				} catch (Exception e) {
					e.printStackTrace();
					logger.error(e);
				}
			}
		}).start();
	}
	
	public void addOffer(ExchangeOffer offer) {
		MutableTuple<Integer, Long> pair = completedOffers.getOrDefault(offer.item(), new MutableTuple<>(0, 0L));
		pair.first(pair.first() + offer.completed());
		pair.second(pair.second() + (offer.completed() * offer.pricePer()));
		
		completedOffers.put(offer.item(), pair);
	}
	
	private void computerPrices() {
		logger.info("Computing grand exchange price changes...");
		server.service(PgSqlService.class, true).ifPresent(sql -> sql.worker().submit(new SqlTransaction() {
			@Override
			public void execute(Connection connection) throws Exception {
				if (!completedOffers.isEmpty()) {
					Map<Integer, Tuple<Integer, Integer>> pricesChanged = new HashMap<>();
					for (int item : completedOffers.keySet()) {
						MutableTuple<Integer, Long> pair = completedOffers.get(item);
						if (pair != null) {
							long oldPrice = server.world().prices().get(item);
							double newPrice = ((double) pair.second() / (double) pair.first());
							
							if (oldPrice == 0)
								continue;
								
							if (newPrice > oldPrice) {
								newPrice = Math.min(oldPrice * 1.05, newPrice);
							} else {
								newPrice = Math.max(oldPrice / 1.05, newPrice);
							}
							
							if (newPrice > Integer.MAX_VALUE) {
								newPrice = Integer.MAX_VALUE;
							}
							if (newPrice <= 0) {
								newPrice = 1;
							}
							
							server.world().prices().set(item, (int) newPrice);
							
							if (oldPrice != newPrice) {
								pricesChanged.put(item, new Tuple<>((int) oldPrice, (int) newPrice));
							}
						}
					}
					completedOffers.clear();
					
					if (!pricesChanged.isEmpty()) {
						PreparedStatement itemStmt = connection.prepareStatement("INSERT INTO price_history (serviceid, itemid, old_price, new_price) VALUES (?, ?, ?, ?)");
						
						for (int item : pricesChanged.keySet()) {
							itemStmt.setInt(1, server.serviceId());
							itemStmt.setInt(2, item);
							itemStmt.setInt(3, pricesChanged.get(item).first());
							itemStmt.setInt(4, pricesChanged.get(item).second());
							itemStmt.addBatch();
						}
						itemStmt.executeBatch();
						
						for (int item : pricesChanged.keySet()) {
							PreparedStatement s;
							switch (server.world().realm()) {
								case PVP:
									s = connection.prepareStatement("UPDATE item_definitions SET bm_value=? WHERE id=?;");
									s.setInt(1, server.world().prices().bloodyMoney(item));
									break;
								case OSRUNE:
									s = connection.prepareStatement("UPDATE item_definitions SET exchange_price_w3=? WHERE id=?;");
									s.setInt(1, server.world().prices().coins(item));
									break;
								default:
									s = connection.prepareStatement("UPDATE item_definitions SET exchange_price=? WHERE id=?;");
									s.setInt(1, server.world().prices().coins(item));
									break;
							}
							s.setLong(2, item);
							s.executeUpdate();
						}
						connection.commit();
					}
				}
				connection.close();
			}
		}));
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
}
