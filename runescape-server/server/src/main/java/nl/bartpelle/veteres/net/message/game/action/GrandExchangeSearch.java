package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.interfaces.PriceChecker;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Jonathan on 3/19/2017.
 */
@PacketInfo(size = 2)
public class GrandExchangeSearch implements Action {
	
	private int itemId;
	
	@Override
	public void process(Player player) {
		if (itemId < 0)
			return;
		if (player.interfaces().activeMain() == 464) {//Price checker
			PriceChecker.show(player, itemId);
		} else {
			player.grandExchange().showBuyOffer(itemId);
		}
	}
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		itemId = buf.readShort();
	}
}
