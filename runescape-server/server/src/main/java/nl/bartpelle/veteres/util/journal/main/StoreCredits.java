package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class StoreCredits extends JournalEntry {

    @Override
    public void send(Player player) {
        send(player, "<img=24> Store Credits", player.credits(), Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int credits = player.credits();
        player.message("You currently have " + credits + " store credits.");
    }

}