package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeOffer;
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeType;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart on 10/28/2016.
 */
public class GrandExchangeUpdate extends Command {
	
	private int slot;
	private ExchangeOffer offer;
	
	private static final ExchangeOffer EMPTY = new ExchangeOffer(-1, 0, ExchangeType.NULL, 0, 0, 0, 0, false, 0, 0, 0, null, true, null, null);
	
	public GrandExchangeUpdate(int slot, ExchangeOffer offer) {
		this.slot = slot;
		this.offer = offer;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		if (offer == null) {
			offer = EMPTY;
		}
		
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(21)).packet(245);
		
		buffer.writeByte(slot);
		buffer.writeByte(offer.computeType()); // State
		buffer.writeShort(offer.item());
		buffer.writeInt(offer.pricePer());
		buffer.writeInt(offer.requested());
		buffer.writeInt(offer.completed());
		buffer.writeInt(offer.spent());
		
		return buffer;
	}
	
}
