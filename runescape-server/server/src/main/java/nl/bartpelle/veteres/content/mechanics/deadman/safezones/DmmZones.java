package nl.bartpelle.veteres.content.mechanics.deadman.safezones;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import nl.bartpelle.veteres.model.*;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.util.JGson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jak on 24/10/2016.
 * <p>
 * By default, the entire World Map is dangerous.
 * To find if we're in a safezone, we find the instance of SafeRegion linked to our current region.
 * This contains specific safe areas.. chunks and tiles
 */
public class DmmZones {
	
	public static DmmZones INSTANCE = new DmmZones();
	
	private static final Logger logger = LogManager.getLogger(DmmZones.class);
	
	// Map of region ids to information about safezones
	// Executing the instrument may under some circumstances clear this so you need to manually reload via JS ::reloadzones
	public Int2ObjectMap<SafeRegion> SAFE_ZONES = new Int2ObjectArrayMap<>();
	
	// Test json stoof
	public static void main(String[] args) {
	    /*SafeRegion sr = new SafeRegion();
	    sr.entireRegionSafe = true;

        SafeRegion sr2 = new SafeRegion();
        sr2.safeTiles.add(new Tile(1, 1));
        sr2.dangerousTileOverrides.add(new Tile(10, 10));
        sr2.safeChunks.add(100);

        INSTANCE.SAFE_ZONES.put(1, sr);
        INSTANCE.SAFE_ZONES.put(2, sr2);*/
		
		saveToJson();
		loadJson();
		printAll();
	}
	
	// Convert the old format json file into my new system, which supports different Height Levels.
	public static void convert() {
		Map<Integer, SafeRegion> ZONES = new HashMap<Integer, SafeRegion>();
		INSTANCE.SAFE_ZONES.forEach((key, value) -> {
			SafeRegion sr2 = new SafeRegion();
			sr2.entireRegionSafe = value.entireRegionSafe;
            /*value.safeChunks.forEach(chunkId -> {
                sr2.safeChunks.add(new LeveledChunk(chunkId, 0)); // Havent got non-0 levels yet
            });*/
			sr2.safeTiles = value.safeTiles; // Tiles have level already
			sr2.dangerousTileOverrides = value.dangerousTileOverrides; // Tiles have level already
			value.dangerousAreaOverrides.forEach(area -> {
				sr2.dangerousAreaOverrides.add(new Area(area, 0)); // Havent got non-0 levels yet
			});
			ZONES.put(key, sr2);
		});
		logger.info(String.format("Converted %d safe region entries.%n", ZONES.size()));
		saveToJson(ZONES, "data/safezones2.json");
	}
	
	// Looad from json
	public static void loadJson() {
		long start = System.currentTimeMillis();
		Gson gson = JGson.get();
		try {
			Map<Integer, SafeRegion> regions = gson.fromJson(new FileReader("data/safezones.json"), new TypeToken<Map<Integer, SafeRegion>>() {
			}.getType());
			INSTANCE.SAFE_ZONES = new Int2ObjectArrayMap<>(regions);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		logger.info(String.format("Safezones loaded in %dms.", System.currentTimeMillis() - start));
	}
	
	// Saaave to json
	public static void saveToJson() {
		saveToJson(INSTANCE.SAFE_ZONES, "data/safezones.json");
	}
	
	// To certain filename.. if I'm messing with different Object sources
	public static void saveToJson(Object src, String filename) {
		Gson save = new GsonBuilder().setPrettyPrinting().setLenient().create();
		try {
			FileWriter fw = new FileWriter(filename);
			fw.write(save.toJson(src));
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Spew out info on all safe zones into output
	public static void printAll() {
		INSTANCE.SAFE_ZONES.forEach((key, value) -> {
			logger.info(String.format("Region %d safe: %s ->\n\t %s \n\t%s \n\t%s \n", key, value.entireRegionSafe,
					Arrays.toString(value.safeChunks.toArray()),
					Arrays.toString(value.safeTiles.toArray()),
					Arrays.toString(value.dangerousTileOverrides.toArray())));
		});
	}
	
	// Get the info about safezones for the given region. Also store if didn't exist.
	public static SafeRegion get(int regionId) {
		SafeRegion safeRegion = INSTANCE.SAFE_ZONES.getOrDefault(regionId, new SafeRegion());
		INSTANCE.SAFE_ZONES.put(regionId, safeRegion);
		return safeRegion;
	}
	
	/**
	 * Is this tile a dangerous area? The entire world is dangerous excluding some cities.
	 */
	public static boolean inDangerousDMM(Tile tile) {
		if (INSTANCE == null || INSTANCE.SAFE_ZONES == null || INSTANCE.SAFE_ZONES.isEmpty()) {
			//System.out.println("wtf ffs nigga??ASDF");
			return true;
		}
		if (!INSTANCE.SAFE_ZONES.containsKey(tile.region())) {
			//System.out.println("No mappin for "+tile.region());
			return true;
		}
		SafeRegion safeinfo = INSTANCE.SAFE_ZONES.get(tile.region());
		if (safeinfo != null) {
			boolean override = safeinfo.dangerousTileOverrides.contains(tile);
			boolean overrideArea = safeinfo.dangerousAreaOverrides.stream().anyMatch(area -> area.contains(tile) && area.level == tile.level);
			if (override || overrideArea) {
				// Override being in a safe Tile or Area
				return true;
			}
			if (safeinfo.entireRegionSafe) {
				// We're in a safe region
				return false;
			} else {
				// Loops safe chunks to check if we're in them.
				for (LeveledChunk chunkInfo : safeinfo.safeChunks) {
					Tile chunkTile = Tile.chunkToTile(chunkInfo.chunkId);
					Area chunk = new Area(chunkTile, chunkTile.transform(8, 8), chunkInfo.level);
					if (chunk.contains(tile) && (chunk.level == 0 || (chunk.level != 0 && chunk.level == tile.level))) {
						// We're in a safe chunk
						return false;
					}
				}
				// Loop safe Tiles to see if we're in any of those.
				for (Tile safeTile : safeinfo.safeTiles) {
					if (tile.equals(safeTile)) {
						return false;
					}
				}
			}
		}
		// By default, entire game is dangerous!!
		return true;
	}
	
	// Outlines a region via ground items
	public static void test(Player p) {
		Tile corner = p.tile().regionCorner();
		p.message("You're at " + p.tile().toStringSimple() + " and corner is " + corner.toStringSimple() + "  dist:" + corner.distance(p.tile()));
		for (int x = 0; x < 63; x++) {
			spawn(p.world(), corner.transform(x, 0));
		}
		for (int x = 0; x < 63; x++) {
			spawn(p.world(), corner.transform(0, x));
		}
		for (int x = 0; x < 63; x++) {
			spawn(p.world(), corner.transform(63, x));
		}
		for (int x = 0; x < 64; x++) { // goes to 64 to corner the top right corner
			spawn(p.world(), corner.transform(x, 63));
		}
	}
	
	// Outlines a CHUNK via ground items
	public static void test2(Player p) {
		Tile corner = p.tile().chunkCorner();
		p.message("You're at " + p.tile().toStringSimple() + " and corner is " + corner.toStringSimple() + "  dist:" + corner.distance(p.tile()));
		for (int x = 0; x < 7; x++) {
			spawn(p.world(), corner.transform(x, 0));
		}
		for (int x = 0; x < 7; x++) {
			spawn(p.world(), corner.transform(0, x));
		}
		for (int x = 0; x < 7; x++) {
			spawn(p.world(), corner.transform(7, x));
		}
		for (int x = 0; x < 8; x++) { // goes to 8 to corner the top right corner
			spawn(p.world(), corner.transform(x, 7));
		}
	}
	
	// Paint the world with ground items representing safezones
	public static void showSafetiles(World world, Tile tile) {
		SafeRegion sr = DmmZones.get(tile.region());
		if (sr.entireRegionSafe) {
			//System.out.println("Region is safe: "+tile.region()+" ["+tile.regionCorner()+"]");
			//System.out.print("Excluded tiles: ");
			for (int x = 0; x < 64; x++) {
				for (int y = 0; y < 64; y++) {
					Tile pos = tile.regionCorner().transform(x, y, tile.level);
					if (!sr.excluded(pos)) {
						spawn(world, pos);
					} else {
						//System.out.print(pos.toStringSimple()+", ");
					}
				}
			}
			//System.out.println();
		} else {
			StringBuilder sb = new StringBuilder();
            /*for (int cid : sr.safeChunks) {
                sb.append(Tile.chunkToTile(cid)+", ");
            }*/
			//System.out.printf("Region %d safe chunks: %s => %s%n", tile.region(), Arrays.toString(sr.safeChunks.toArray()), "["+sb.toString()+"]");
			//System.out.print("Excluded tiles: ");
			for (LeveledChunk chunkInfo : sr.safeChunks) {
				Tile corner = Tile.chunkToTile(chunkInfo.chunkId);
				for (int x = 0; x < 8; x++) {
					for (int y = 0; y < 8; y++) {
						Tile pos = corner.transform(x, y, chunkInfo.level == 0 ? tile.level : chunkInfo.level); // Custom level or 0 for all
						if (!sr.excluded(pos)) {
							spawn(world, pos);
						}// else {
						//    System.out.print(pos.toStringSimple()+", ");
						//}
					}
				}
			}
			//System.out.println();
			for (Tile t : sr.safeTiles) {
				if (!sr.excluded(t)) {
					spawn(world, t);
				}
			}
		}
	}
	
	// Spawn a ground item on tile to show it's safe
	public static void spawn(World world, Tile tile) {
		if (world.players().size() > 10 || (!DMM_ZONE_CHECK_ENABLED && !world.server().config().hasPathOrNull("deadman.jaktestserver"))) {
			System.out.println("NOT TESTING DMM - STOP BREWS");
			return; // Don't spawn brews on the live game.. jesus
		}
		GroundItem gitem = new GroundItem(world, new Item(6685), tile, null).lifetime(100000000L);
		world.spawnGroundItem(gitem);
		//System.out.println("spawned on "+tile.toStringSimple());
	}
	
	/**
	 * A toggle to check if the player is in a safezone.. since it might be a stressful operation.
	 * Can be removed once tested on live game and/or optimised.
	 */
	public static boolean DMM_ZONE_CHECK_ENABLED = false;
	
	// Testing compute time on areas
	public static long computetime = 0L, totalcomputetime;
	public static int this_engine_cycle;
	public static String[] INFO_STRING = new String[2];
	
	// Is this entity in a dangerous zone?
	public static boolean inDangerousDMM(Entity entity) {
		if (!DMM_ZONE_CHECK_ENABLED) {
			return false;
		}
		// Update the debug/tracking info so we can see the effect on performance.
		if (this_engine_cycle != entity.world().cycleCount()) {
			this_engine_cycle = entity.world().cycleCount();
			INFO_STRING[0] = String.format("Cycle %d took %dms to compute safezones for %d players.%n", (this_engine_cycle - 1), computetime, entity.world().players().size());
			INFO_STRING[1] = String.format("Total running compute time: %dms.", totalcomputetime);
			totalcomputetime += computetime;
			computetime = 0L;
		}
		long start = System.nanoTime();
		boolean is = inDangerousDMM(entity.tile());
		computetime += (System.nanoTime() - start) / 1_000_000; // duration
		return is;
	}
}
