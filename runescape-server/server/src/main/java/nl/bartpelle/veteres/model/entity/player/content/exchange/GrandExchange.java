package nl.bartpelle.veteres.model.entity.player.content.exchange;

import com.google.common.collect.ImmutableSet;
import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Realm;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.IronMode;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.net.message.game.command.GrandExchangeUpdate;
import nl.bartpelle.veteres.net.message.game.command.InvokeScript;
import nl.bartpelle.veteres.net.message.game.command.SetItems;
import nl.bartpelle.veteres.services.grandexchange.GrandExchangeService;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.RSFormatter;
import nl.bartpelle.veteres.util.Varbit;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by Bart on 10/28/2016.
 * Edits by Mack on 7/20/2017.
 */
public class GrandExchange {
	
	public static boolean enabled = false;
	
	private Player player;
	private ExchangeOffer[] offers = new ExchangeOffer[8];
	private List<ExchangeHistory> history = new LinkedList<>();
	
	/**
	 * A {@link ImmutableSet} definition for storing all "spawnable" items & free items players can receive on W2. This collection should ONLY
	 * be applied to selling items in W2 as we shouldn't allow players to sell items that are free into the exchange.
	 */
	private ImmutableSet<Item> unsellables = ImmutableSet.of(new Item(4587), new Item(1215));
	
	public GrandExchange(Player player) {
		this.player = player;
	}
	
	public void loadOffers() {
		player.world().server().service(GrandExchangeService.class, true).ifPresent(ge -> {
			offers = ge.offersByPlayer(player);
			player.world().server().service(PgSqlService.class, true).ifPresent(sql -> sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement s = connection.prepareStatement("SELECT * FROM exchange_offers WHERE service_id = ? AND account_id=? AND completed = requested ORDER BY updated_at DESC LIMIT 20;");
					s.setInt(1, player.world().server().serviceId());
					s.setInt(2, (Integer) player.id());
					
					ResultSet set = s.executeQuery();
					while (set.next()) {
						ExchangeType type = set.getBoolean("is_sell") ? ExchangeType.SELL : ExchangeType.BUY;
						int itemId = set.getInt("item_id");
						int completed = set.getInt("completed");
						int pricePer = set.getInt("spent");
						
						ExchangeHistory h = new ExchangeHistory(itemId, completed, pricePer, type);
						getHistory().add(0, h);
					}
					Collections.reverse(getHistory());
					updateOffers();
					
					connection.commit();
					connection.close();
				}
			}));
		});
	}
	
	public void updateOffers() {
		int[] keys = {518, 519, 520, 521, 522, 523, 539, 540};
		
		for (int i = 0; i < offers.length; i++) {
			ExchangeOffer o = offers[i];
			player.write(new GrandExchangeUpdate(i, offers[i]));
			
			ItemContainer c = new ItemContainer(player.world(), 0, ItemContainer.Type.FULL_STACKING);
			
			if (o != null && o.collectItems() != null) {
				c = o.collectItems();
			}
			
			player.write(new SetItems(keys[i], -1, 65435, c));
		}
	}
	
	public void display() {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		player.invokeScript(828, 1); // Unlocks the extra option slots (members)
		player.invokeScript(InvokeScript.SETVARCS, -1, -1);
		player.invokeScript(InvokeScript.OPEN_TAB, 3); // Opens inventory
		
		player.interfaces().sendMain(465);
		player.interfaces().sendInventory(467);
		player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT, -1);
		player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE, 0); //Cost
		player.varps().varbit(4397, 0); //Amount
		player.varps().varp(1151, -1);
		player.invokeScript(299, 1, 1); // Closes the chatbox
		
		for (int i = 7; i <= 14; i++) {
			player.interfaces().setting(465, i, 2, 2, 6);
			player.interfaces().setting(465, i, 3, 4, 2);
		}
		
		player.interfaces().setting(465, 22, 0, 0, 6);
		player.interfaces().setting(465, 23, 2, 3, 1038);
		player.interfaces().setting(465, 6, 0, 0, 6);
		player.interfaces().setting(465, 24, 0, 13, 2);
		player.interfaces().setting(467, 0, 0, 27, 1026);
		
		updateOffers();
	}
	
	public void showBuy(int slot) {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		ExchangeOffer offer = offers[slot];
		player.varps().varbit(4397, 0);
		
		player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT, slot + 1);
		player.putattrib(AttributeKey.GRANDEXCHANGE_MODE, ExchangeType.BUY);
		player.interfaces().text(465, 25, offer == null ? "Click the icon on the left to cycle for items." : player.world().examineRepository().item(offer.item()));
		player.interfaces().text(465, 26, offer == null ? "" : RSFormatter.formatNumber(offer.pricePer()));
		player.varps().varp(1151, -1);
		player.invokeScript(750, "What would you like to buy?", 1, -1);
	}
	
	public void showSell(int slot) {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		ExchangeOffer offer = offers[slot];
		
		player.varps().varbit(4397, 1);
		
		player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT, slot + 1);
		player.putattrib(AttributeKey.GRANDEXCHANGE_MODE, ExchangeType.SELL);
		player.interfaces().text(465, 25, "Choose an item from your inventory to sell.");
		player.interfaces().text(465, 26, "");
		player.interfaces().text(465, 16, offer == null ? "" : player.world().examineRepository().item(offer.item()));
		player.interfaces().text(465, 17, offer == null ? "" : RSFormatter.formatNumber(offer.pricePer()));
		player.varps().varp(1151, -1);
	}
	
	public void abortOffer() {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		int slot = player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT) - 1;
		if (slot < 0) {
			return;
		}
		
		abortOffer(slot);
	}
	
	public void abortOffer(int slot) {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		ExchangeOffer offer = offers[slot];
		
		if (offer == null || offer.cancelled() || offer.finished()) {
			return;
		}
		player.world().server().service(GrandExchangeService.class, true).ifPresent(ge -> {
			ge.abortOffer(player, offer);
			updateOffers();
		});
	}
	
	public void showBuyOffer(int id) {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		int slot = checkSlot();
		if (slot == -1) {
			return;
		}
		
		Item item = new Item(id, 1);
		if (item.definition(player.world()) == null) {
			player.message("Pickle 'o pie budd. Send this to staff " + id);
			return;
		}
		if (!item.definition(player.world()).grandexchange || item.realPrice(player.world()) == 0) {
			return;
		}
		
		player.putattrib(AttributeKey.GRANDEXCHANGE_MODE, ExchangeType.BUY);
		player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT, slot + 1);
		player.varps().varbit(Varbit.GRAND_EXCHANGE_AMOUNT, 1);
		player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE, item.realPrice(player.world()));
		player.interfaces().text(465, 25, player.world().examineRepository().item(item));
		player.interfaces().text(465, 26, RSFormatter.formatNumber(item.realPrice(player.world())));
		player.varps().varp(1151, item.id());
	}
	
	
	public void showSellOffer(Item item) {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		if (item == null) {
			return;
		}
		
		int slot = checkSlot();
		if (slot == -1) {
			return;
		}
		
		long count = item.amount();
		if (!item.definition(player.world()).grandexchange) {
			item = item.unnote(player.world());
			if (!item.definition(player.world()).grandexchange) {
				player.message("You can't trade that item on the Grand Exchange.");
				return;
			}
			count += player.inventory().count(item.id());
		}
		if (item.realPrice(player.world()) == 0) {
			player.message("You can't trade that item on the Grand Exchange.");
			return;
		}
		count = Math.min(Integer.MAX_VALUE, count);
		
		showSell(slot);
		
		player.interfaces().text(465, 25, player.world().examineRepository().item(item));
		player.varps().varbit(Varbit.GRAND_EXCHANGE_AMOUNT, (int) count);
		player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE, item.realPrice(player.world()));
		player.varps().varp(1151, item.id());
	}
	
	public int checkSlot() {
		int slot = player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT) - 1;
		if (slot < 0 || slot >= offers.length || offers[slot] != null) {
			slot = nextSlot();
			
			if (slot < 0) {
				player.message("You have no Grand Exchange slots available.");
				return -1;
			}
		}
		return slot < 0 || slot >= offers.length || offers[slot] != null ? -1 : slot;
	}
	
	public void confirmBuyOffer(int slot, Item item, int pricePer) {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		
		if (offers[slot] != null || item.amount() <= 0) {
			player.invokeScript(299, 1, 1); // Closes the chatbox
			player.message("Please choose an item.");
			return;//not sure if this can happen
		}
		
		if (pricePer <= 0) {
			player.message("You can't trade that item on the Grand Exchange.");
			return;
		}
		
		BigInteger total = BigInteger.valueOf(pricePer).multiply(BigInteger.valueOf(item.amount()));
		if (total.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0 || total.intValue() <= 0) {
			player.message("That offer is worth to much money, try buying less items or lowering the price.");
			return;
		}
		
		ItemContainer.Result result = player.inventory().remove(new Item(player.currency(), pricePer * item.amount()), false);
		
		if (result.success()) {
			ExchangeOffer offer = new ExchangeOffer((int) player.id(), slot, ExchangeType.BUY, pricePer, item.id(), item.amount(), 0, false, 0, 0, 0, player.world(), false, new Timestamp(System.currentTimeMillis()), null);
			
			player.world().server().service(GrandExchangeService.class, true).ifPresent(ge -> {
				ge.addOffer(offer);
				offers[offer.slot()] = offer;
			});
			display();
		} else {
			player.message("That offer costs %s " + (player.world().realm() == Realm.PVP ? "blood money" : "coins") + ". You haven't got enough.", RSFormatter.formatNumber(pricePer * item.amount()));
		}
	}
	
	public void confirmSellOffer(int slot, Item item, int cost) {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		if (offers[slot] != null || item.amount() <= 0) {
			player.message("Please choose an item.");
			return;//not sure if this can happen
		}
		if (cost <= 0) {
			player.message("You can't trade that item on the Grand Exchange.");
			return;
		}
		
		int originalAmount = item.amount();
		
		boolean isNoted = item.noted(player.world());
		
		Item noted = isNoted ? item : item.note(player.world());
		Item unnoted = !isNoted ? item : item.unnote(player.world());
		
		int id = item.id();
		long total = Math.min((long) player.inventory().count(item.id()) +
				(isNoted ? player.inventory().count(unnoted.id()) : player.inventory().count(noted.id())), (long) item.amount());
		
		if (total < item.amount()) {
			System.out.println("Nope? " + total + ", " + item.amount());
			return;
		}
		
		BigInteger safeTotal = BigInteger.valueOf(cost).multiply(BigInteger.valueOf(total));
		if (safeTotal.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0 || safeTotal.intValue() <= 0) {
			player.message("That offer is worth to much money, try buying less items or lowering the price.");
			return;
		}
		
		ItemContainer.Result result = null;
		if ((isNoted || item.noteable(player.world())) && player.inventory().has(noted)) {
			noted = new Item(noted.id(), Math.min(item.amount(), player.inventory().count(noted.id())));
			result = player.inventory().remove(noted, false);
			if (result.completed() == item.amount()) {
				item = null;
			} else if (result.completed() > 0) {
				item = new Item(isNoted ? unnoted.id() : item.id(), item.amount() - result.completed());
			}
		}
		
		
		if (item != null && item.amount() > 0)
			result = player.inventory().remove(item, false);
		
		if (result != null && result.success()) {
			ExchangeOffer offer = new ExchangeOffer((int) player.id(), slot, ExchangeType.SELL, cost, id, originalAmount, 0, false, 0, 0, 0, player.world(), false, new Timestamp(System.currentTimeMillis()), null);
			
			player.world().server().service(GrandExchangeService.class, true).ifPresent(ge -> {
				ge.addOffer(offer);
				offers[offer.slot()] = offer;
			});
			
			display();
		}
	}
	
	public void openCollectionBox() {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		player.invokeScript(InvokeScript.SETVARCS, -1, -1);
		player.invokeScript(299, 1, 1); // Closes the chatbox
		player.interfaces().sendMain(402);
		for (int i = 5; i <= 12; i++) {
			player.interfaces().setting(402, i, 3, 4, 1038);
		}
	}
	
	public void openHistory() {
		if (!enabled) {
			player.message("The Grand Exchange is not available at the moment.");
			return;
		}
		// Ironman? fuck off lol!!
		if (player.ironMode() != IronMode.NONE && player.privilege() != Privilege.ADMIN) {
			player.message("You are an Iron Man. You stand alone.");
			return;
		}
		player.invokeScript(InvokeScript.SETVARCS, -1, -1);
		player.interfaces().send(149, 548, 66, true);
		player.interfaces().sendMain(383);
		player.interfaces().setting(548, 48, -1, -1, 2);
		player.interfaces().setting(383, 3, 0, 114, 1026);
		player.invokeScript(299, 1, 1); // Closes the chatbox
		
		player.invokeScript(1644);
		
		for (int i = 0; i < getHistory().size(); i++) {
			if (i > getHistory().size()) {
				break;
			}
			ExchangeHistory h = getHistory().get(i);
			if (h != null) {
				player.invokeScript(1645, i, h.id(), h.type() == ExchangeType.SELL ? 1 : 2, h.amount(), h.price());
			}
		}
		player.invokeScript(1646);
	}
	
	public void updateOffer(ExchangeOffer o) {
		if (o.slot() < 0 || o.slot() >= offers.length)
			return;
		
		if (o.completed() == o.requested()) {
			getHistory().add(0, new ExchangeHistory(o.item(), o.completed(), o.spent(), o.type()));
			if (getHistory().size() > 20)
				getHistory().remove(getHistory().size() - 1);
		}
		
		offers[o.slot()] = o;
		updateOffers();
	}
	
	public int nextSlot() {
		for (int i = 0; i < offers.length; i++) {
			if (offers[i] == null)
				return i;
		}
		return -1;
	}

	public boolean hasOffers() {
		if (!enabled) {
			return false;
		}
		for (ExchangeOffer offer : offers) {
			if (offer != null) {
				return true;
			}
		}
		return false;
	}
	
	public ExchangeOffer[] offers() {
		return offers;
	}
	
	public ExchangeHistory getHistoryItem(int slot) {
		if (slot < 0 || slot >= getHistory().size()) {
			return null;
		}
		return getHistory().get(slot);
	}
	
	public boolean has(int i) {
		for (ExchangeOffer o : offers) {
			if (o != null &&
					((o.type() == ExchangeType.SELL && !o.finished() && o.item() == i)
							|| (o.type() == ExchangeType.BUY && o.item() == i && o.items() > 0)))
				return true;
		}
		return false;
	}

	public void notifyOnLogin() {
		Arrays.stream(offers).filter(Objects::nonNull).filter(ExchangeOffer::finished).forEach(offer -> {
			ItemDefinition def = player.world().definitions().get(ItemDefinition.class, offer.item());
			String name = def != null ? def.name : "null";
			String pluralized = (offer.completed() > 1 && !name.endsWith("s")) ? name.concat("s") : name;
			String actionName = offer.type() == ExchangeType.BUY ? "buying" : "selling";
			player.message("<col=135E00>Grand Exchange: Finished "+ actionName +" "+ offer.completed() +" x "+ pluralized +".");
		});
	}
	
	public List<ExchangeHistory> getHistory() {
		return history;
	}
}
