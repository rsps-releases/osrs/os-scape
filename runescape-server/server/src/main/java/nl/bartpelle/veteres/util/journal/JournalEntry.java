package nl.bartpelle.veteres.util.journal;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.SendJournalEntry;

public abstract class JournalEntry {

    public int childId;

    protected final void send(Player player, String text) {
        player.write(new SendJournalEntry(childId, ("<col=D37E2A>" + text + "</col>"), -1));
    }

    protected final void send(Player player, String text, Color color) {
        player.write(new SendJournalEntry(childId, text, color.ordinal()));
    }

    protected final void send(Player player, String key, String value, Color color) {
        player.write(new SendJournalEntry(childId, ("<col=D37E2A>" + key + ":</col> " + value), color.ordinal()));
    }

    protected final void send(Player player, String key, int value, Color color) {
        player.write(new SendJournalEntry(childId, ("<col=D37E2A>" + key + ":</col> " + value), color.ordinal()));
    }

    public abstract void send(Player player);

    public abstract void select(Player player);

}