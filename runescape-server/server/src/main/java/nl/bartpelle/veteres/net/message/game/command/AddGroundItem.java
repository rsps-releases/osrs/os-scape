package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart on 8/22/2015.
 */
public class AddGroundItem extends Command {
	
	private GroundItem item;
	
	public AddGroundItem(GroundItem item) {
		this.item = item;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer packet = new RSBuffer(player.channel().alloc().buffer(6)).packet(61);
		
		int x = item.tile().x % 8;
		int z = item.tile().z % 8;

		packet.writeByteS((x << 4) | z);
		packet.writeShortA(item.item().id());
		packet.writeLEShort(item.item().amount());
		
		return packet;
	}
	
}

