package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.Shop;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Mack on 9/16/2017.
 */
public class UpdateCustomShop extends Command {
	
	private int interfaceId;
	private Item[] stock;
	private String name;

	public UpdateCustomShop(String name, int interfaceId, Item[] stock) {
		this.interfaceId = interfaceId;
		this.stock = stock;
		this.name = name;
	}
	
	@Override
	protected RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(player.channel().alloc().buffer(3 + name.length() + 1 + 2 + (stock.length * 6))).packet(88).writeSize(RSBuffer.SizeType.SHORT);

		buffer.writeString(name);
		buffer.writeInt(interfaceId);
		buffer.writeShort(stock.length);

		for (Item item : stock) {
			if (item != null) {
				buffer.writeShort(item.id());
				Shop shop = player.attribOr(AttributeKey.SHOP, 0);
				buffer.writeInt(shop.buyPriceOf(item.id()));
			}
		}
		
		return buffer;
	}
}
