package nl.bartpelle.veteres.services.logging;

import co.paralleluniverse.strands.Strand;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.services.Service;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Bart on 5/4/2016.
 */
public class DataLoggingService implements Service {
	
	private static final Logger logger = LogManager.getLogger(DataLoggingService.class);
	
	private static final String API_KEY = "1b49ca09-b21f-403d-b76b-44138703a7d6";
	private static final String PAGE_ID = "hvbyz8jdzwn7";
	private static final String API_BASE = "https://api.statuspage.io/v1/";
	
	private AtomicInteger logins = new AtomicInteger(0);
	private String metric;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		metric = serviceConfig.getString("metric");
		
		new Thread(() -> {
			logger.info("Data logging service providing metrics to {}.", metric);
			
			while (true) {
				log();
			}
		}).start();
	}
	
	public void increase() {
		logins.incrementAndGet();
	}
	
	private void log() {
		try {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpPost post = new HttpPost(API_BASE + "/pages/" + PAGE_ID + "/metrics/" + metric + "/data.json");
			post.addHeader("Authorization", "OAuth " + API_KEY);
			List<NameValuePair> params = Arrays.asList(new BasicNameValuePair("data[timestamp]", String.valueOf(System.currentTimeMillis() / 1000)),
					new BasicNameValuePair("data[value]", String.valueOf(logins.getAndSet(0))));
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params);
			post.setEntity(entity);
			CloseableHttpResponse execute = httpClient.execute(post);
			
			Strand.sleep(60000);
		} catch (Exception e) {
			logger.error("Error in logging data!", e);
		}
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
}
