package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.Varp;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class KDRatio extends JournalEntry {

    @Override
    public void send(Player player) {
        send(player, "<img=14> K/D Ratio", getKdr(player), Color.GREEN);
    }

    @Override
    public void select(Player player) {
        player.sync().shout("<img=14> I have a k/d ratio of " + getKdr(player) + ".");
    }

    private static String getKdr(Player player) {
        int deaths = player.varps().varp(Varp.DEATHS);
        int kills = player.varps().varp(Varp.KILLS);

        if (deaths == 0) {
            return String.valueOf(kills);
        }

        int one = (kills * 10 / deaths);
        int two = one / 10;
        int three = one % 10;
        return String.valueOf(two) + "." + String.valueOf(three);
    }

}