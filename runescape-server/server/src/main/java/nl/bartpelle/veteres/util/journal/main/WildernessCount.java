package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.areas.instances.PVPAreas;
import nl.bartpelle.veteres.content.quests.QuestGuide;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.util.Misc;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class WildernessCount extends JournalEntry {

    public static final WildernessCount INSTANCE = new WildernessCount();

    @Override
    public void send(Player player) {
        send(player, "<img=15> Players PKing", total(player), Color.GREEN);
    }

    @Override
    public void select(Player player) {
        int wildCount = Misc.inWildCount(player.world());
        int instanceCount = Misc.inPvpInstanceCount(player.world());

        if (player.privilege().eligibleTo(Privilege.ADMIN)) {
            player.stopActions(false);
            QuestGuide.clear(player);

            player.interfaces().text(275, 2, "<img=1> Wilderness breakdown");

            player.interfaces().text(275, 4, "<col=800000>Level 0-10: </col>" + PVPAreas.getRange0_10());
            player.interfaces().text(275, 5, "<col=800000>Level 11-20: </col>" + PVPAreas.getRange11_20());
            player.interfaces().text(275, 6, "<col=800000>Level 21-30: </col>" + PVPAreas.getRange21_30());
            player.interfaces().text(275, 7, "<col=800000>Level 31-40: </col>" + PVPAreas.getRange31_40());
            player.interfaces().text(275, 8, "<col=800000>Level 41-49: </col>" + PVPAreas.getRange41_49());
            player.interfaces().text(275, 9, "<col=800000>Level 50+: </col>" + PVPAreas.getRange50plus());

            player.interfaces().text(275, 11, "<col=800000>Total players inside wilderness:  </col>" + wildCount);
            player.interfaces().text(275, 12, "<col=800000>Total players in PVP instances:  </col>" + instanceCount);

            QuestGuide.open(player);
        }
    }

    public static int total(Player player) {
        int wildernessCount = Misc.inWildCount(player.world());
        int PVPInstanceCount = Misc.inPvpInstanceCount(player.world());
        return wildernessCount + PVPInstanceCount;
    }

}