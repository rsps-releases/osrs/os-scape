package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.services.intercom.PmService;

/**
 * Created by Bart on 11/15/2015.
 */
@PacketInfo(size = -1)
public class AddIgnore implements Action {
	
	private String name;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		name = buf.readString();
	}
	
	@Override
	public void process(Player player) {
		player.world().server().service(PmService.class, true).ifPresent(pmService -> {
			pmService.addIgnore(player, name);
		});
		//TODO check if exists
	}
	
}
