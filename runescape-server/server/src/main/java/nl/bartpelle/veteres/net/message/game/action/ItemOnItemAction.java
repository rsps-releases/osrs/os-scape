package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 9/28/2015.
 */
@PacketInfo(size = 16)
public class ItemOnItemAction implements Action {
	
	private int fromHash;
	private int toSlot;
	private int fromSlot;
	private int toId;
	private int fromId;
	private int toHash;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		fromHash = buf.readLEInt();
		toSlot = buf.readULEShort();
		fromId = buf.readULEShort();
		toHash = buf.readIntV2();
		fromSlot = buf.readUShort();
		toId = buf.readULEShort();
		
		log(player, opcode, size, "from=[%d:%d] to=[%d:%d] fromslot=%d toslot=%d fromid=%d toid=%d",
				fromHash >> 16, fromHash & 0xFFFF, toHash >> 16, toHash & 0xFFFF, fromSlot, toSlot, fromId, toId);
	}
	
	@Override
	public void process(Player player) {
		if (fromSlot < 0 || toSlot < 0 || fromSlot > 27 || toSlot > 27) {
			return;
		}
		Item fromItem = player.inventory().get(fromSlot);
		Item toItem = player.inventory().get(toSlot);
		
		if (fromItem == null || toItem == null) // Avoid null items
			return;
		if (fromItem.id() != fromId || toItem.id() != toId) // Avoid latency click
			return;
		
		if (!player.locked() && !player.dead()) {
			player.stopActions(false);
			if (player.attribOr(AttributeKey.DEBUG, false))
				player.debug("from=[%d:%d] to=[%d:%d] fromslot=%d toslot=%d fromid=%d toid=%d",
						fromHash >> 16, fromHash & 0xFFFF, toHash >> 16, toHash & 0xFFFF, fromSlot, toSlot, fromId, toId);
			player.world().server().scriptRepository().triggerItemOnItemOption(player, fromItem, toItem, fromSlot, toSlot);
		}
	}
	
}
