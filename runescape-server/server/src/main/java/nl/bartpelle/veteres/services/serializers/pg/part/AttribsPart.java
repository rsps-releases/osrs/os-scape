package nl.bartpelle.veteres.services.serializers.pg.part;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import nl.bartpelle.veteres.model.AttribType;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.JGson;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bart on 10/28/2015.
 */
public class AttribsPart implements PgJsonPart {
	
	private static final AttributeKey[] VALUES = AttributeKey.values();
	
	private JsonParser parser = new JsonParser();
	
	private Gson gson = JGson.get();
	
	private final static Type mapStringStringType = new TypeToken<Map<String, String>>() {
	}.getType();
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		JsonArray array = parser.parse(resultSet.getString("attribs")).getAsJsonArray();
		
		// Iterate through values
		for (JsonElement jsonElement : array) {
			JsonObject obj = jsonElement.getAsJsonObject();
			
			Map.Entry<String, JsonElement> entry = obj.entrySet().iterator().next();
			for (AttributeKey key : VALUES) {
				if (key.saveName() != null && key.saveName().equals(entry.getKey())) {
					if (key.saveType() == AttribType.INTEGER) {
						player.putattrib(key, entry.getValue().getAsInt());
					} else if (key.saveType() == AttribType.STRING) {
						player.putattrib(key, entry.getValue().getAsString());
					} else if (key.saveType() == AttribType.DOUBLE) {
						player.putattrib(key, entry.getValue().getAsDouble());
					} else if (key.saveType() == AttribType.LONG) {
						player.putattrib(key, entry.getValue().getAsLong());
					} else if (key.saveType() == AttribType.BOOLEAN) {
						player.putattrib(key, entry.getValue().getAsBoolean());
					} else if (key.saveType() == AttribType.ARRAY) {
					
					} else if (key.saveType() == AttribType.STRING_STRING_MAP) {
						player.putattrib(key, jsonToMapStringString(entry.getValue().getAsString()));
					}
				}
			}
		}
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection connection, boolean removeOnline) throws SQLException {
		JsonArray values = new JsonArray();
		
		// Populate the array with our values
		for (AttributeKey key : VALUES) {
			// Only save if we want to save.
			if (key.saveName() == null) {
				continue;
			}
			
			if (key.saveType() == AttribType.INTEGER) {
				int v = player.attribOr(key, 0);
				if (v != 0) {
					JsonObject obj = new JsonObject();
					obj.addProperty(key.saveName(), v);
					values.add(obj);
				}
			} else if (key.saveType() == AttribType.STRING_STRING_MAP) {
				Map<String, String> v = player.attribOr(key, null);
				if (v != null) {
					JsonObject obj = new JsonObject();
					obj.addProperty(key.saveName(), mapStringStringToJson(v));
					values.add(obj);
				}
			} else if (key.saveType() == AttribType.STRING) {
				String v = player.attribOr(key, null);
				if (v != null) {
					JsonObject obj = new JsonObject();
					obj.addProperty(key.saveName(), v);
					values.add(obj);
				}
			} else if (key.saveType() == AttribType.LONG) {
				Long value = player.attribOr(key, 0L);
				
				if (value != 0) {
					JsonObject obj = new JsonObject();
					
					obj.addProperty(key.saveName(), value);
					values.add(obj);
				}
			} else if (key.saveType() == AttribType.BOOLEAN) {
				Boolean state = player.attribOr(key, false);
				
				JsonObject obj = new JsonObject();
				
				obj.addProperty(key.saveName(), state);
				values.add(obj);
			} else {
				Double v = player.attribOr(key, 0.0);
				if (v != null) {
					JsonObject obj = new JsonObject();
					obj.addProperty(key.saveName(), v);
					values.add(obj);
				}
			}
		}
		
		characterUpdateStatement.setString(8, gson.toJson(values));
	}
	
	private Map<String, String> jsonToMapStringString(String json) {
		try {
			if (json == null || json.isEmpty())
				return new HashMap<>();
			return gson.fromJson(json, mapStringStringType);
		} catch (Exception e) {
			return new HashMap<>(); //Ultimate backwards compatibility
		}
	}
	
	private String mapStringStringToJson(Map<String, String> map) {
		if (map == null)
			map = new HashMap<>();
		return gson.toJson(map);
	}
	
}
