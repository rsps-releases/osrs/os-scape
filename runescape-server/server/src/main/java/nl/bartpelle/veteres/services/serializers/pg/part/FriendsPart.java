package nl.bartpelle.veteres.services.serializers.pg.part;

import nl.bartpelle.veteres.model.entity.Player;

import java.sql.*;

/**
 * Created by Bart on 10/28/2015.
 */
public class FriendsPart implements PgJsonPart {
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		Array array = resultSet.getArray("friends");
		Integer[] ids = (Integer[]) array.getArray();
		
		for (int i : ids) {
			System.out.println("Feidnd: " + i);
		}
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection connection, boolean removeOnline) throws SQLException {
		Integer[] values = new Integer[1];
		values[0] = 4;
		Array array = characterUpdateStatement.getConnection().createArrayOf("INT", values);
		characterUpdateStatement.setArray(13, array);
	}
	
}
