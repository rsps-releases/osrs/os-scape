package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

import java.text.NumberFormat;

public class RiskProtectionState extends JournalEntry {

    public static final int TIER_ONE = 10_000;
    public static final int TIER_TWO = 25_000;
    public static final int TIER_THREE = 50_000;

    public static final RiskProtectionState INSTANCE = new RiskProtectionState();

    @Override public void send(Player player) {
        int tier = player.attribOr(AttributeKey.RISK_PROT_TIER, 5);
        send(player, "<img=58> Risk Protection", protectionState(player), tier == 0 ? Color.RED : Color.GREEN);
    }

    @Override public void select(Player player) {
        WildernessLevelIndicator.updateCarriedWealthProtection(player);
        player.world().server().scriptExecutor().executeLater(player, new QuestTab.ToggleRiskProtection(player));
        send(player);
    }
    
    private String protectionState(Player player) {
        int tier = player.attribOr(AttributeKey.RISK_PROT_TIER, 3);
        return tier == 0 ? "Forfeited" : NumberFormat.getInstance().format(protectionValue(player)) + "+";
    }

    public static int protectionValue(Player player) {
        int tier = player.attribOr(AttributeKey.RISK_PROT_TIER, 3);
        switch(tier) {
            case 1:
                return TIER_ONE;
            case 2:
                return TIER_TWO;
            case 3:
                return TIER_THREE;
            default:
                return 0;
        }
    }

    public static int valueForTier(int tier) {
        switch(tier) {
            case 1:
                return TIER_ONE;
            case 2:
                return TIER_TWO;
            case 3:
                return TIER_THREE;
            default:
                return 0;
        }
    }

    public static void monitorRiskProtection(Player player) {
        int riskProtectionValue = RiskProtectionState.protectionValue(player);
        int risked = (int) player.attribOr(AttributeKey.RISKED_WEALTH, 0) + (int) player.attribOr(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0);
        if(risked < riskProtectionValue) {
            if(risked < RiskProtectionState.TIER_ONE)
                RiskProtectionState.toggleRiskProtection(player, 10); // 10 will work as 0 blood money risk protection
            else if(risked < RiskProtectionState.TIER_TWO)
                RiskProtectionState.toggleRiskProtection(player, 1);
            else if(risked < RiskProtectionState.TIER_THREE)
                RiskProtectionState.toggleRiskProtection(player, 2);
        }
    }

    public static void toggleRiskProtection(Player player, int tier) {
        int risked = (int) player.attribOr(AttributeKey.RISKED_WEALTH, 0) + (int) player.attribOr(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0);
        if(risked < valueForTier(tier)) {
            player.message(Color.DARK_RED.wrap("Your risk protection value can't be set higher than your current risk."));
            return;
        }
        player.putattrib(AttributeKey.RISK_PROT_TIER, tier);
        player.message(Color.DARK_RED.wrap("You now have a risk protection value up to " + NumberFormat.getInstance().format(protectionValue(player)) + " Blood money."));
        INSTANCE.send(player);
    }
}
