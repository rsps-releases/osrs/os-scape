package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.mechanics.GroundItemAction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 11/18/2015.
 */
@PacketInfo(size = 15)
public class ItemOnGroundItem implements Action {
	
	private int groundItemId, x, y;
	private int itemUsedId, slot;
	private int interfaceHash, varp82;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		groundItemId = buf.readULEShortA();
		varp82 = buf.readByte();
		itemUsedId = buf.readUShortA();
		x = buf.readUShortA();
		y = buf.readUShort();
		interfaceHash = buf.readInt();
		slot = buf.readShort();

		log(player, opcode, size, "ItemOnGroundItem %d at %d,%d used %d at %d on %d",
				groundItemId, x, y, itemUsedId, slot, interfaceHash);
	}
	
	@Override
	public void process(Player player) {
		player.debug("ItemOnGroundItem %d at %d,%d used %d at %d on %d",
				groundItemId, x, y, itemUsedId, slot, interfaceHash);
		
		GroundItem gitem = player.world().getGroundItem(player, x, y, player.tile().level, groundItemId);
		
		if (!player.locked() || !player.dead()) {
			player.stopActions(true);
			Item item = player.inventory().get(slot);
			if (item == null) return;
			player.putattrib(AttributeKey.FROM_ITEM, item);
			player.putattrib(AttributeKey.ITEM_SLOT, slot);
			player.putattrib(AttributeKey.INTERACTED_GROUNDITEM, gitem);
			player.putattrib(AttributeKey.INTERACTION_OPTION, -1);
			player.world().server().scriptExecutor().executeLater(player, GroundItemAction.script);
		}
	}
	
}
