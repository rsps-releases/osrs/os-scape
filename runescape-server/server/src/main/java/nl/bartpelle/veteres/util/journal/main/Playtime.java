package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Hans;
import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class Playtime extends JournalEntry {

    public static final Playtime INSTANCE = new Playtime();

    @Override
    public void send(Player player) {
        send(player, "<img=25> Playtime",  Hans.getTimeForQuestTab(player), Color.GREEN);
    }

    @Override
    public void select(Player player) {
        player.world().server().scriptExecutor().executeScript(player, new QuestTab.PlayTime(player));
    }

}