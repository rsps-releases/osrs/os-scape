package nl.bartpelle.veteres.net.message.game.command;

import io.netty.buffer.Unpooled;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart Pelle on 8/22/2014.
 */
public class ChangeMapVisibility extends Command {
	
	private int state;
	
	public ChangeMapVisibility(int state) {
		this.state = state;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		RSBuffer buffer = new RSBuffer(Unpooled.buffer());
		buffer.packet(82);
		buffer.writeByte(state);
		return buffer;
	}

}
