package nl.bartpelle.veteres.services.mixpanel;

import co.paralleluniverse.strands.Strand;
import com.mixpanel.mixpanelapi.ClientDelivery;
import com.mixpanel.mixpanelapi.MessageBuilder;
import com.mixpanel.mixpanelapi.MixpanelAPI;
import com.typesafe.config.Config;
import nl.bartpelle.veteres.GameServer;
import nl.bartpelle.veteres.services.Service;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Bart on 11/1/2016.
 */
public class MixPanelService implements Service {
	
	private ClientDelivery delivery = new ClientDelivery();
	private MixpanelAPI mixpanel = new MixpanelAPI();
	private MessageBuilder messageBuilder;
	private String token;
	
	@Override
	public void setup(GameServer server, Config serviceConfig) {
		token = serviceConfig.getString("token");
		messageBuilder = new MessageBuilder(token);
		
		// Fire thread that submits the queue once per minute.
		new Thread(() -> {
			while (true) {
				try {
					Strand.sleep(60_000);
					submitMessages();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public void event(MixPanelEvent event, String ip, Integer world) {
		try {
			JSONObject props = new JSONObject();
			if (world != null)
				props.put("World", world);
			
			JSONObject update = messageBuilder.event(ip, event.identifier(), props);
			deliver(update);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void submitMessages() {
		ClientDelivery d_ = delivery;
		delivery = new ClientDelivery();
		
		try {
			mixpanel.deliver(d_);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deliver(JSONObject message) {
		delivery.addMessage(message);
	}
	
	@Override
	public boolean start() {
		return true;
	}
	
	@Override
	public boolean stop() {
		return true;
	}
	
	@Override
	public boolean isAlive() {
		return true;
	}
	
}
