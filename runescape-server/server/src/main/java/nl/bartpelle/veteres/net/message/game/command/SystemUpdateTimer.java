package nl.bartpelle.veteres.net.message.game.command;

import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Command;

/**
 * Created by Bart on 12/1/2015.
 */
public class SystemUpdateTimer extends Command {
	
	private int ticks;
	
	public SystemUpdateTimer(int ticks) {
		this.ticks = ticks;
	}
	
	@Override
	public RSBuffer encode(Player player) {
		return new RSBuffer(player.channel().alloc().buffer(3)).packet(5).writeLEShort(ticks);
	}
	
}
