package nl.bartpelle.veteres.util;

import co.paralleluniverse.fibers.Instrumented;
import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.Parser;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * @author Savions Sw
 */
public class RSFormatter {
	
	public static final String formatDisplayname(String input) {
		if (input == null || input.length() < 1)
			return input;
		String[] parts = input.split(" ");
		if (parts == null || parts.length < 1) {
			parts = new String[1];
			parts[0] = input;
		}
		String result = "";
		for (int i = 0; i < parts.length; i++) {
			final String part = parts[i];
			if (part.length() > 0) {
				final Character c = Character.toUpperCase(part.charAt(0));
				parts[i] = c + part.substring(1);
			}
			result += parts[i];
			if (i + 1 < parts.length)
				result += " ";
		}
		return result;
	}
	
	private static final Parser parser = new Parser(TimeZone.getTimeZone("UTC"));
	
	@Instrumented
	public static Date parseDate(String s) { // Quasar is so dumb when it comes to this shit
		List<DateGroup> groups = parser.parse(s);
		Date cur = null;
		for (DateGroup group : groups) {
			for (Date date : group.getDates())
				cur = date;
		}
		return cur;
	}
	
	public static String formatNumber(int number) {
		return String.format("%,d", number);
	}
	
	public static String formatNumber(long number) {
		return String.format("%,d", number);
	}
	
	public static String formatItemAmount(int amount) {
		return formatItemAmount((long) amount);
	}
	
	public static String formatItemAmount(long amount) {
		if (amount > 1000) {
			if (amount < 999_999)
				return amount / 1000 + "K";
			else if (amount > 999_999)
				return amount / 1000000 + "M";
		}
		return "" + amount;
	}
}
