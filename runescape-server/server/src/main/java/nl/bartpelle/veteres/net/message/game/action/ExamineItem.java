package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart Pelle on 8/9/2015.
 */
@PacketInfo(size = 2)
public class ExamineItem implements Action {
	
	private int id;
	
	@Override
	public void process(Player player) {
		String extra = "";
		if (player.attribOr(AttributeKey.DEBUG, false))
			extra = " (" + id + ")";
		String examine = player.world().examineRepository().item(id);
		if (id == 4207) {
			if (player.world().realm().isPVP()) {
				examine = "I can pay <col=FF0000>Ilfeen</col> in <col=FF0000>Edgeville</col> to enchant this into some form of Crystal Equipment.";
			}
		}
		player.message(examine + extra);
	}
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		id = buf.readUShortA();
	}
	
}
