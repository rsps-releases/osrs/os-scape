package nl.bartpelle.veteres.util;

import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.entity.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bart on 22/04/2017.
 */
public class FastTravel {
	
	private static Map<String, Tile> travelRepo = new HashMap<String, Tile>();
	
	public static void load() {
		put(3222, 3222, "lumbridge", "lummy");
		put(3492, 3485, "canifis");
		put(3214, 3423, "varrock");
		put(3087, 3507, "edge", "edgeville");
		put(3052, 3491, "monastery", "church");
		put(3165, 3485, "ge", "exchange", "grandexchange", "g.e.", "grand_exchange");
		put(3078, 3420, "barb", "barbarian", "barbarians", "barbvillage", "barbarianvillage");
		put(2966, 3380, "fally", "falador");
		put(3019, 3242, "psarim", "portsarim", "port_sarim", "sarim");
		put(1780, 3690, "zeah", "kourend");
		put(1587, 3487, "wcguild", "woodcuttingguild", "wc_guild");
		put(1435, 3705, "lizardmen", "lizardman");
		put(1631, 3940, "wintertodt", "wintertod");
		put(3093, 3248, "draynor");
		put(2957, 3216, "rimmington", "rimington");
		put(2933, 3284, "craftguild", "craftingguild", "crafting_guild");
		put(2896, 3455, "taverly", "taverley");
		put(2901, 3542, "burthorpe", "burtorpe", "burthorph", "burthorp", "burtorp", "burthope");
		put(2852, 3547, "warrior", "warriorguild", "warriorsguild", "warriors_guild", "warrior_guild", "wguild");
		put(2804, 3433, "camelot", "catherby", "cammy");
		put(2725, 3848, "seers", "seersvillage", "seer", "seervillage", "seers_village", "seer_village", "seer's");
		put(2667, 3426, "rangeguild", "ranging", "rguild", "ranging_guild");
		put(2662, 3310, "ardy", "ardougne", "ardougn");
		put(2460, 3400, "gnomes", "gnomestronghold", "gnome_stronghold");
		put(2466, 3491, "grandtree", "grand_tree", "thegrandtree");
		put(2534, 3570, "barbassault", "barbarianassault", "barbarianoutpost", "barbarian_outpost", "barboutpost");
		put(3317, 3235, "duelarena", "da", "duel");
		put(3269, 3166, "kharid", "alkharid", "desert", "alk");
		put(3371, 3163, "cw", "clanwars", "clan_wars", "cwars", "clans", "clan");
		put(2440, 3088, "castle_wars", "castlewars", "castle");
		put(2549, 3088, "yanille", "yanile", "yanill", "yanil");
		put(3565, 3287, "barrows", "barrow");
		put(3844, 3287, "morton", "mortton", "mort'ton");
		put(2923, 3150, "karamja", "banana_island", "tyluur");
		put(2772, 3216, "brinhaven", "brimhaven", "bhaven", "brim");
		put(2486, 5166, "tzhaar", "tzhar", "tzharr", "tzaar");
		put(3169, 3031, "bedabin", "bedabincamp", "bendabin");
		put(3173, 2979, "banditcamp", "bandits", "bandit");
		put(3351, 2959, "polnivneach", "pollnivneach", "polly", "pollivneach", "polniveach");
		put(3422, 2916, "nardah", "narda");
	}
	
	public static void process(Player player, String dest) {
		if (travelRepo.isEmpty() || dest.equals("--reload")) {
			load();
		}
		
		dest = dest.toLowerCase();
		
		Tile tile = travelRepo.get(dest);
		if (tile == null) {
			player.message("No fast travel target found for %s.", dest);
			return;
		}
		
		player.message("Fast traveling to %s: %d, %d, %d.", dest, tile.x, tile.z, tile.level);
		player.teleport(tile);
	}
	
	private static void put(int x, int y, int level, String... aliases) {
		Tile t = new Tile(x, y, level);
		
		for (String s : aliases) {
			travelRepo.put(s.toLowerCase(), t);
		}
	}
	
	private static void put(int x, int y, String... aliases) {
		put(x, y, 0, aliases);
	}
	
}
