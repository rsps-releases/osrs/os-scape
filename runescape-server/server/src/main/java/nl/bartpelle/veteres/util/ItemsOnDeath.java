package nl.bartpelle.veteres.util;

import com.google.common.base.Preconditions;
import nl.bartpelle.veteres.content.achievements.AchievementAction;
import nl.bartpelle.veteres.content.achievements.AchievementCategory;
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars;
import nl.bartpelle.veteres.content.areas.clanwars.FFAClanWars;
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest;
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue.Herman;
import nl.bartpelle.veteres.content.areas.instances.PVPAreas;
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.combat.FoodItems;
import nl.bartpelle.veteres.content.combat.Potions;
import nl.bartpelle.veteres.content.events.BloodyVolcano;
import nl.bartpelle.veteres.content.items.equipment.DragonfireShield;
import nl.bartpelle.veteres.content.items.equipment.RingOfSuffering;
import nl.bartpelle.veteres.content.items.equipment.RunePouch;
import nl.bartpelle.veteres.content.items.equipment.TomeOfFire;
import nl.bartpelle.veteres.content.mechanics.*;
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanKeyReward;
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterEmblem;
import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.*;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.entity.player.ClanChat;
import nl.bartpelle.veteres.model.entity.player.IronMode;
import nl.bartpelle.veteres.model.entity.player.PlayerDamageTracker;
import nl.bartpelle.veteres.model.entity.player.Privilege;
import nl.bartpelle.veteres.model.hitorigin.PoisonOrigin;
import nl.bartpelle.veteres.model.hitorigin.VenomOrigin;
import nl.bartpelle.veteres.model.instance.InstancedMap;
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemAttrib;
import nl.bartpelle.veteres.model.item.ItemContainer;
import nl.bartpelle.veteres.net.message.game.command.AddReceivedPrivateMessage;
import nl.bartpelle.veteres.net.message.game.command.SetItems;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.services.logging.LoggingService;
import nl.bartpelle.veteres.services.pvp.FarmingPrevention;
import nl.bartpelle.veteres.services.sql.PgSqlService;
import nl.bartpelle.veteres.services.sql.SqlTransaction;
import nl.bartpelle.veteres.util.journal.Journal;
import nl.bartpelle.veteres.util.journal.main.Hotspot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Carl on 2015-08-23.
 * <p>
 * Plenty more mechanics added by the Team.
 * Specific items (rune pouch, 07 untradeables which are made tradeable on w2 because they are worth BM) are always 'kept' and
 * not filtered by value for the top-3 items kept when unskulled.
 * <p>
 * Of these 'always kept' items - the 07 untradeables like fire capes are (1) lost forever when dying in > 20 wildernes or (2) dropped to the floor
 * so the dead player can run back and pick them up. They disappear after 10 minutes, after which on w2 they go into Perdu's shop (if the player is online at time of despawn)
 * Perdu's shop sells only the items you've lost for 50% of normal BM price. (only items you've lost otherwise you could buy 50% and resell normal store specialPrpriceice = coin dupe)
 * <p>
 * All other items including non-bloodmoney untradeables are filtered by value. Follow same mechanic of (1) lost forever in 20+ or (2) dropped to floor to pick back up.
 * <p>
 * On w2 and w3, you get blood money reward depending on kill streak shutdown.
 * <p>
 * Items with cosmetic kits applied are split. Malediction shield -> normal shield + kit.
 * - The dragon defender (T) is a special case - the defender is an untradeable. Kit drops to killer, dead player can return for it.
 * - Crystal items revert to seeds
 * <p>
 * Items with charges are split - A charged toxic staff becomes an uncharged toxic staff + scales
 * <p>
 * - NOTE: Nice-guy mechanic: usually you lost untradeables > 20 wild. If you died in pvm though/suicide - you can run back (not lost!)
 * Above 20 wilderness, when an untradeable item is always lost, such as the Fire cape, it converts to 50% store price (w2) or 50% alch price (w1/w3) and drops
 * to killer.
 * Since under 20 wild the dead player can return to reobtain their untradeables, killer does NOT get a coin/bm drop like above. This would be a dupe.
 */
public class ItemsOnDeath {
	
	private static final Logger logger = LogManager.getLogger(ItemsOnDeath.class);
	
	// TODO genders
	public static final String[] KILL_MESSAGES = {
			"%s will probably tell you he wanted a free teleport after that performance.",
			"Such a shame that %s can't play this game.",
			"%s was made to sit down.",
			"You have defeated %s.",
			"A humiliating defeat for %s.",
			"How not to do it right: Written by %s.",
			"The struggle for %s is real.",
			"%s falls before your might.",
			"Can anyone defeat you? Certainly not %s.",
			"%s didn't stand a chance against you.",
			"What was %s thinking challenging you...",
			"%s should take lessons from you. You're clearly too good for him."
	};
	
	private static final String[] barrowsDegrade = new String[]{
			"ahrim", "dharok", "verac", "karil", "guthan", "torag"
	};
	
	public static Player identifyMostDamagerKiller(Player dead) {
		// Establish the player database-id that did the most damage to us
		// NPC damage is NOT tracked, therefore if we die to an 'unknown' like suicide, or to an npc, we get our own loot anyway.
		Optional<Integer> killer_id = dead.killer();
		
		// Now try and get a Player instance of the killer.
		Optional<Player> mostdam_instance = killer_id.isPresent() ? dead.world().playerForId(killer_id.get()) : Optional.empty();
		
		// If killer is offline, we can grab their old instance that killed this player.
		if (!mostdam_instance.isPresent()) {
			Tuple<Integer, PlayerDamageTracker> pair = dead.attribOr(AttributeKey.MOST_DAM_TRACKER, null);
			PlayerDamageTracker tracker = pair == null ? null : pair.second();
			if (tracker != null) {
				mostdam_instance = Optional.ofNullable(tracker.killer());
			}
		}
		return mostdam_instance.isPresent() ? mostdam_instance.get() : null;
	}
	
	enum Conversions {
		DRAG_CHAIN_G(new Item(12414), new Item[]{new Item(3140), new Item(12534)}),
		DRAG_LEGS_G(new Item(12415), new Item[]{new Item(4087), new Item(12536)}),
		DRAG_SKIRT_G(new Item(12416), new Item[]{new Item(4585), new Item(12536)}),
		DRAG_FULLHELM_G(new Item(12417), new Item[]{new Item(11335), new Item(12538)}),
		DRAG_SQ_G(new Item(12418), new Item[]{new Item(1187), new Item(12532)}),
		AGS(new Item(20368), new Item[]{new Item(11802), new Item(20068)}),
		BGS(new Item(20370), new Item[]{new Item(11804), new Item(20071)}),
		SGS(new Item(20372), new Item[]{new Item(11806), new Item(20074)}),
		ZGS(new Item(20374), new Item[]{new Item(11808), new Item(20077)}),
		TORTURE(new Item(20366), new Item[]{new Item(19553), new Item(20062)}),
		DSCIM(new Item(20000), new Item[]{new Item(4587), new Item(20002)}),
		FURY(new Item(12436), new Item[]{new Item(6585), new Item(12526)}),
		OCCULT(new Item(19720), new Item[]{new Item(12002), new Item(20065)}),
		GRANITE_CLAMP(new Item(12848), new Item[]{new Item(4153), new Item(12849)}),
		ODIUM_ORN(new Item(12807), new Item[]{new Item(11926), new Item(12802)}),
		MALEDICTION_ORN(new Item(12806), new Item[]{new Item(11924), new Item(12802)}),
		STEAM_STAFF_ORN(new Item(12796), new Item[]{new Item(11789), new Item(12798)}),
		FROZEN_WHIP(new Item(12774), new Item[]{new Item(4151), new Item(12769)}),
		VOLCANIC_WHIP(new Item(12773), new Item[]{new Item(4151), new Item(12771)}),
		BLUE_DARKBOW(new Item(12765), new Item[]{new Item(11235), new Item(12757)}),
		GREEN_DARKBOW(new Item(12766), new Item[]{new Item(11235), new Item(12759)}),
		YELLOW_DARKBOW(new Item(12767), new Item[]{new Item(11235), new Item(12761)}),
		WHITE_DARKBOW(new Item(12768), new Item[]{new Item(11235), new Item(12763)}),
		LIGHT_INFINITY_HAT(new Item(12419), new Item[]{new Item(6918), new Item(12530)}),
		LIGHT_INFINITY_TOP(new Item(12420), new Item[]{new Item(6916), new Item(12530)}),
		LIGHT_INFINITY_SKIRT(new Item(12421), new Item[]{new Item(6924), new Item(12530)}),
		DARK_INFINITY_HAT(new Item(12457), new Item[]{new Item(6918), new Item(12528)}),
		DARK_INFINITY_TOP(new Item(12458), new Item[]{new Item(6916), new Item(12528)}),
		DARK_INFINITY_SKIRT(new Item(12459), new Item[]{new Item(6924), new Item(12528)}),
		UNCHARGED_MAGMA_HELMET(new Item(13198), new Item[]{new Item(12929)}),
		UNCHARGED_TANZANITE_HELMET(new Item(13196), new Item[]{new Item(12929)}),
		RING_OF_SUFFERING_I(new Item(RingOfSuffering.getSUFFERING_I()), new Item[]{new Item(RingOfSuffering.getRING_OF_SUFFERING())}),
		RING_OF_SUFFERING_RI(new Item(RingOfSuffering.getSUFFERING_RI()), new Item[]{new Item(RingOfSuffering.getRING_OF_SUFFERING())}),
		RING_OF_SUFFERING_R(new Item(RingOfSuffering.getSUFFERING_R()), new Item[]{new Item(RingOfSuffering.getRING_OF_SUFFERING())}),
		SARADOMINS_BLESSED_SWORD(new Item(12809), new Item[]{new Item(12804, 11838)}),
		BERSERKER_RING_I(new Item(11773), new Item[]{new Item(6737)}),
		ARCHERS_RING_I(new Item(11771), new Item[]{new Item(6733)}),
		SEERS_RING_I(new Item(11770), new Item[]{new Item(6731)}),
		WARRIOR_RING_I(new Item(11772), new Item[]{new Item(6735)}),
		TREASONOUS_RING_I(new Item(12692), new Item[]{new Item(12605)}),
		TYRANNICAL_RING_I(new Item(12691), new Item[]{new Item(12603)}),
		RING_OF_THE_GODS_I(new Item(13202), new Item[]{new Item(12601)}),
		GRANITE_RING_I(new Item(21752), new Item[]{new Item(21739)}),
		DRAGON_PLATEBODY(new Item(22242), new Item[] {new Item(21892), new Item(22236)}),
		DRAGON_KITESHIELD(new Item(22244), new Item[] {new Item(21895), new Item(22239)}),
		DRAGON_BOOTS(new Item(22234), new Item[] { new Item(11840), new Item(22231)}),
		ANGUISH(new Item(22249), new Item[]{new Item(19547), new Item(22246)}),
		STAFF_OF_LIGHT(new Item(22296), new Item[] { new Item(11791), new Item(13256)})

		;//end of enum
		
		public Item src;
		public Item[] out;
		
		Conversions(Item src, Item[] output) {
			this.src = src;
			this.out = output;
		}
	}
	
	// These untradable items will fall to the floor on death. You have to run back to pick them up.
	public static final int[] RS_UNTRADABLES_LIST = new int[]{
			8839, 8840, 8842, 11663, 11664, 11665, 13072, 13073, // Void
			12954, 8844, 8845, 8846, 8847, 8848, 8849, 8850, // Defenders
			6570, // Fire cape
			21295, 21285, //infernal cape
			13280, 13329, 13331, 13333, 13335, 13337, // Max cape variants
			10547, 10548, 10549, 10550, 10551, 10552, 10555, // Pennence gear
			19722, // Defender (t)
	};
	
	// Variants of crystal bow, shield, halberd, (i)
	// Note this does NOT INCLUDE 4212 ('new' bow) or 4224 ('new' shield)
	// Those are un-used crystal items, which drop as themselves.. they can be noted!
	// The halberd is a quest item and has no noted form.
	public static boolean isCrystal(int id) {
		return (id >= 4214 && id <= 4223) || (id >= 4225 && id <= 4234)
				|| (id >= 11748 && id <= 11758)
				|| (id >= 11759 && id <= 11769)
				|| (id >= 13080 && id <= 13101);
	}
	
	public static final int LOOTING_BAG = 11941, DRAG_DEFENDER_TRIM = 19722;

	public static class KillersLoot {
		public LinkedList<LootItem> loot = new LinkedList<>();
		public boolean wildernessDeath;

		public KillersLoot(boolean wildy) {
			this.wildernessDeath = wildy;
		}
	}

	public static class LootItem {
		public Item item;
		public boolean hidden;

		public LootItem(Item i, boolean hidden) {
			this.item = i;
			this.hidden = hidden;
		}
	}
	
	// Drop the 'targets' items to the person that did the most damage
	// If an Npc did most damage, the mostdam entity is ourself... act as we suicided.
	private static void dropItems(Entity mostdam, Player target) {
		Preconditions.checkArgument(mostdam != null);
		Preconditions.checkArgument(target != null);
		
		if (target.index() == -1) { // Security.
			target.world().players().forEach(p -> {
				if (p != null && p.privilege().eligibleTo(Privilege.MODERATOR)) {
					p.write(new AddReceivedPrivateMessage("DupeWatch", 2, 0, "WARNING: apparent user duping: " + target.name() + "; see chatbox!"));
					p.message("<col=ff0000>[DUPEWATCH] " + target.name() + " just died at " + target.tile().x + "," + target.tile().z + "," + target.tile().level + " - but is offline. Tell an admin.");
				}
			});
			if (mostdam.isPlayer() && (boolean) mostdam.attribOr(AttributeKey.DEBUG, false)) {
				mostdam.message("Odd.");
			}
			return;
		}
		
		if (target.world().realm().isDeadman() && mostdam.isPlayer()) {
			DeadmanKeyReward.handle_deadman_death(target, (Player) mostdam);
		}
		
		int wildlvl = WildernessLevelIndicator.wildernessLevel(target.tile());
		
		// Do we drop to an npc or to a player? If player, set the id, else null (global)
		Object killerId = mostdam.isPlayer() ? ((Player) mostdam).id() : target.id();
		boolean inwild = WildernessLevelIndicator.inWilderness(target.tile()) || PVPAreas.inPVPArea(target);
		boolean selfKill = target.id() == killerId;
		boolean validKill = FarmPrevention.valid(mostdam, target);

		// Initially everything we're holding, then untradeables and +1s will be removed.
		// Items in here will be converted if they have charges into their components and dropped individually.
		LinkedList<Item> lostItems = new LinkedList<>();
		
		// On test worlds, drop items as usual, and give back our stuff as it was before death.
		Item[] inv = null, equip = null;
		if (target.world().realm().isDeadman() && target.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
			inv = target.inventory().copy();
			equip = target.equipment().copy();
		}
		
		// Grab all items
		Collections.addAll(lostItems, target.inventory().copy());
		Collections.addAll(lostItems, target.equipment().copy());
		
		//Collect any emblems and remove them from the lostItems collection.
		List<Item> emblems = lostItems.stream().filter(item -> item != null && BountyHunterEmblem.isEmblem(item)).collect(Collectors.toList());
		lostItems.removeIf(item -> item != null && BountyHunterEmblem.isEmblem(item));
		
		//Item arrays for objects we need to manipulate in the lostItems collection.
		Item[] bloodKeys = lostItems.stream().filter(key -> key != null && BloodChest.isKey(key.id())).toArray(Item[]::new);
		
		//Removing bloody keys from lost collection.
		lostItems.removeIf(toRemove -> toRemove != null && (BloodChest.isKey(toRemove.id())));
		
		// Establish what items are untradables. These are PERMA kept under all circumstances, and do not need to be
		// Part of the top 3/4 protected items.
		// NOTE - for PVP world - ANY bm items (even worth 0) are tr
		Item[] untrades = lostItems.stream().filter(item ->
				item != null && (item.id() == DRAG_DEFENDER_TRIM || item.id() == LOOTING_BAG ||
						(!item.rawtradable(target.world()) && !item.lostUntradable(target.world())))).toArray(Item[]::new);
		
		// Remove these perm-kept items from the lost-items array
		lostItems.removeIf(item -> item == null || item.id() == DRAG_DEFENDER_TRIM || (!item.rawtradable(target.world()) && !item.lostUntradable(target.world())));
		
		// Sort remaining lost items by value.
		lostItems.sort((o1, o2) -> {
			o1 = o1.unnote(target.world());
			o2 = o2.unnote(target.world());
			
			ItemDefinition def = o1.definition(target.world());
			ItemDefinition def2 = o2.definition(target.world());
			
			int v1 = 0;
			int v2 = 0;
			
			if (def != null) {
				v1 = o1.realPrice(target.world());
				if (v1 <= 0 && !target.world().realm().isPVP()) {
					v1 = def.cost;
				}
			}
			if (def2 != null) {
				v2 = o2.realPrice(target.world());
				if (v2 <= 0 && !target.world().realm().isPVP()) {
					v2 = def2.cost;
				}
			}
			
			return Integer.compare(v2, v1);
		});
		
		// Empty the containers
		target.inventory().empty();
		target.equipment().empty();
		
		int left = Skulling.skulled(target) ? 0 : 3;
		long keptPrice = 0;
		if (target.varps().varbit(Varbit.PROTECT_ITEM) == 1)
			if (target.attribOr(AttributeKey.NON_PLUS1_AREA, false)) {
				target.message("<col=FF0000>Warning:</col> You are in a High Risk area where the Protect item prayer does not work.");
			} else {
				left++;
			}
		
		// On Ultimate Iron Man, you drop everything!
		if (target.ironMode() == IronMode.ULTIMATE) {
			left = 0;
		}
		
		// Give back items we kept, from either not skulling or using protect item prayer.
		while (left-- > 0 && !lostItems.isEmpty()) {
			Item head = lostItems.peek();
			if (head == null) {
				left++;
				lostItems.poll();
				continue;
			}
			
			target.inventory().add(new Item(head.id(), 1).duplicateProperties(head), true);
			
			if (head.amount() == 1) { // Amount 1? Remove the item entirely.
				lostItems.poll();
			} else { // Amount more? Subtract one amount.
				int index = lostItems.indexOf(head);
				lostItems.set(index, new Item(head, head.amount() - 1).duplicateProperties(head));
			}
		}
		
		// Untradables, give straight back to their owner rather than drop on the floor requiring them to run back for them.
		
		// bm, 25% cost of untradables that have been lost for the killer.
		int bmdrop = 0;
		// Dying on w1/w3 with untradables above 20 wild (lost) give the alch price back to killer.
		int coindrop = 0;

		KillersLoot loot = new KillersLoot(inwild);

		for (Item untradable : untrades) {
			
			// 13342 is not in the perdu untradale list array because the array is used for other stuff too - which this item does not apply.
			boolean dropToFloorItem = isRSUntradeableItem(untradable); // flag to drop to floor.
			
			// Stuff that doesn't go to floor as (broken) or lost over 20 wild. Give back 'normal' quest item/rune pouch-like untradables.
			if (!dropToFloorItem) {
				target.inventory().add(untradable, true);
				
				// Never remove this check, otherwise it'll dupe items. To make our loot pile realistic, we drop any runes that would
				// be lost from our rune pouch. However, on pvp, as a QoL update, the runes are not cleared from our pouch - just to save our time.
				if (!target.world().realm().isPVP() && untradable.id() == 12791) {
					for (Item rune : RunePouch.INSTANCE.allRunesOf(target)) {
						if (rune == null) {
							continue;
						}
						loot.loot.add(new LootItem(rune, false));
					}
					RunePouch.INSTANCE.setRuneAt(target, 0, null);
					RunePouch.INSTANCE.setRuneAt(target, 1, null);
					RunePouch.INSTANCE.setRuneAt(target, 2, null);
				}
				
				// Looting bag drops ALL items on the floor for the killer, no matter the value.
				if (untradable.id() == LOOTING_BAG) {
					for (Item item : target.lootingBag()) {
						if (item != null) {
							loot.loot.add(new LootItem(item, false));
						}
					}
					
					target.lootingBag().empty();
				}
				
			} else {
				
				// The max cape has 2 different forms. Drop the right one for inventory use.
				if (untradable.id() == 13342) { // Worn one with r-click options
					untradable = new Item(13280, untradable.amount()).duplicateProperties(untradable);
				}
				
				// <=20 wild, you can return to pick up. Or, a pvm death! We'll let you return for untradeables instead of losing forever :)
				if (wildlvl <= 20 || selfKill) {
					
					// Killed by ourselves or Npc death? Don't bother converting untradables to kits/broken versions.
					if (!selfKill) {
						// Defender (t) - special case! Opp gets the kit, we get our defender.
						if (untradable.id() == DRAG_DEFENDER_TRIM) {
							// Give them the kit.
							loot.loot.add(new LootItem(new Item(20143), false));
							// Set the untradable lost item as a normal defender.
							untradable = new Item(12954);
						}
						
						if (!target.world().realm().isDeadman() && !selfKill) {
							// Check the Broken untradeables map.
							BrokenUntradeables.BrokenUntradeablesMap entry = BrokenUntradeables.BrokenUntradeablesMap.Companion.forItemId(untradable.id());
							if (entry != null) {
								untradable = new Item(entry.getBrokenId());
								if (target.world().realm().isPVP()) {
									bmdrop += entry.getBmRepairCost();
								} else {
									coindrop += (int) (new Item(entry.getFixedItemId()).realPrice(target.world()) * 0.5);
								}
							}
						}
					}

					// QoL: fuck it lets just be nice - jagex have done it too. No longer have to run back for untradables.
					target.inventory().add(untradable, true);
					
					// Spawn the untradable for us to run back
					//GroundItem g = target.world().spawnGroundItem(new GroundItem(target.world(), untradable, target.tile(), target.id()));
					// Give it a lifetime of 10 minutes to run back.
					//g.lifetime(10 * (60 * 1000)).hidden();
					//target.message("<col=FF0000>You have 10 minutes to run back and get your " + g.item().name(target.world()) + ".");
					
				} else if (wildlvl >= 21) {
					target.message("<col=FF0000>Your " + untradable.name(target.world()) + " has disappeared forever as you were above 20 Wilderness.");
					
					// Convert lost untradables (above 20 wild) to BM/coins depending on Realm
					if (!selfKill) {
						// 20+ wild it instantly despawns. RS mechanic. fk raggers!
						if (target.world().realm().isPVP()) {
							// Firecape value is 3k (shop buy) - which we don't want to change due to protection values.
							// Force this to be very much reduced when converted to BM, since it can now be obtained in around a minute via Fight Caves (w2)
							if (untradable.id() == 6570) {
								bmdrop += 50;
							} else {
								bmdrop += Math.round(target.world().prices().getOrElse(untradable.id(), 0) * untradable.amount() * 0.5D);
							}
						} else {
							if (untradable.skillcape())
								coindrop += 11800;
							else
								coindrop += Math.round(untradable.realPrice(target.world()) * untradable.amount() * 0.5D);
						}
					}
				} else {
					// Safety otherwise it'll be gone forever.
					target.inventory().add(untradable, true);
				}
			}
		}
		
		if (mostdam.isPlayer()) {
			if (((Player) mostdam).id() != target.id()) {
				target.message("<col=1e44b3>You died to %s, therefore they got your loot.", ((Player) mostdam).name());
			} else {
				int mins = target.id() == killerId && !inwild ? 30 : 1;
				target.message("<col=1e44b3>You have %s %s to return to pick up your items before they disappear.", mins, mins == 1 ? "minute" : "minutes");
			}
		}
		
		final long[] value = {0};
		
		// On a new account, we do NOT give people loot. The loot appears for the person that died.
		boolean[] maygive = new boolean[]{target.gameTime() >= 3000};
		
		// If killer is also a new player, the person that died gets it back.
		if (mostdam.isPlayer() && ((Player) mostdam).gameTime() < 3000) {
			maygive[0] = false;
		}
		// Ignore gametime restrictions on PVP
		if (target.world().realm().isPVP() || target.world().realm().isDeadman())
			maygive[0] = true;
		
		boolean degrades = GameCommands.BARROWS_DEGRADING_ENABLED;
		
		// Drop blood keys since this system has become so fucking ugly I can't even find a place to add it. Good job.
		for (Item key : bloodKeys) {
			loot.loot.add(new LootItem(key, false));
		}
		
		//Degrade and drop our bounty emblems to the killer.
		if (target.world().realm().isPVP()) {
			
			//If all conditions are met, we degrade and drop the emblems.
			if (!emblems.isEmpty() && !selfKill && validKill) {
				for (Item it : emblems) {
					BountyHunterEmblem type = BountyHunterEmblem.forEmblemType(it);
					it = BountyHunterEmblem.previous(type);
					
					loot.loot.add(new LootItem(it, false));
				}
				
				//Clear emblem collection.
				emblems.clear();
			}
		}
		
		// Loop all lost items - drop them for the killer
		lostItems.stream().filter(Objects::nonNull).sorted(Comparator.comparingInt(o -> ((Item) o).realPrice(target.world())).reversed()).forEach(drop -> {
			boolean hide = false;
			
			// The timer check does not apply to PK server mode
			if (target.world().realm().isPVP()) {
				if (FoodItems.get(drop.id()) != null || Potions.get(drop.id()) != null) {
					hide = true;
				}
			}
			
			if (!maygive[0]) {
				// Drop is given back to person that died, not visible for others. Stops starter drop trading.
				// THIS BIT IS NOT FOR UNTRADABLES - giving untradbles lost on death (instead of perma kept) uses INTELLIGENT spawnGitemForKiller method
				//target.world().spawnGroundItem(new GroundItem(target.world(), drop, target.tile(), target.id()).hidden().lifetime(1000 * 60 * 30));
				
				// QOL: give back rather than have to run back
				target.inventory().add(drop, true);
				
			} else { // Defo dropping to oppnent
				
				// Re-define the drop if barrows. Also only break if we didn't die to ourselves. Suicide won't break our barrows. QoL.
				if (!selfKill) {
					if (!drop.definition(target.world()).stackable() && degrades) { // Non stacking, pvp world, degrade enabled
						for (String barrow : barrowsDegrade) {
							if (drop.definition(target.world()).name.toLowerCase().contains(barrow.toLowerCase())) { // Faster than looping entire barrows set for any non-stacking item we're dropping.
								drop = new Item(Herman.brokenForFixed(drop.id()), drop.amount());
								//target.debug("Converted barrows to "+drop.id()+" "+drop.name(target.world()));
							}
						}
					}
				}
				ArrayList<Item> actualDrops = new ArrayList<>();
				
				boolean[] converted = new boolean[1];
				if (!selfKill) { // Don't bother converting for suicide
					for (Conversions con : ItemsOnDeath.Conversions.values()) {
						if (!converted[0] && con.src.id() == drop.id()) {
							converted[0] = true; // Set that the original item should not be dropped
							for (Item out : con.out)
								actualDrops.add(new Item(out)); // Drop these instead
							break;
						}
					}
				}
				
				// Not yet converted... check some more. Only if non-suicide (you got pked) otherwise just be nice and give back the item untouched.
				if (!converted[0] && !selfKill) {
					if (drop.id() >= 8714 && drop.id() <= 8744) {
						drop = new Item(1201); // Rune kiteshield
					} else if (drop.id() >= 8682 && drop.id() <= 8712) {
						drop = new Item(1157); // Steel fullhelm
					} else if (drop.id() == 12006) {
						drop = new Item(12004); //Abyssal tent -> Kraken tent
					} else if (isCrystal(drop.id())) {
						drop = new Item(4207);
					} else if (drop.skillcape()) {
						drop = new Item(995, 11800);
					}
				}
				
				// This item hasn't yet been converted. Check more.
				// Zulrah items always convert to raw forms EVEN IF SUICIDE (only if they have charges though...)
				if (!converted[0] /* convert even if suicide*/) {
					if (drop.id() == 12926) { // Charged toxic blowpipe
						int scales = drop.property(ItemAttrib.ZULRAH_SCALES);
						int darts = drop.property(ItemAttrib.DARTS_COUNT);
						if (scales > 0 || darts > 0) {
							converted[0] = true; // Don't drop original item!
							actualDrops.add(new Item(12924)); // Uncharged
							if (scales > 0)
								actualDrops.add(new Item(12934, scales)); // Scales
							if (darts > 0)
								actualDrops.add(new Item(drop.property(ItemAttrib.DART_ITEMID), darts)); // Darts
						}
					} else if (drop.id() == 12904) { // Charged toxic SOTD
						int scales = drop.property(ItemAttrib.ZULRAH_SCALES);
						if (scales > 0) {
							converted[0] = true; // Don't drop original item!
							actualDrops.add(new Item(12902)); // Uncharged
							actualDrops.add(new Item(12934, scales)); // Scales
						}
					} else if (drop.id() == 21633) {//wyvern shield
						drop.property(ItemAttrib.CHARGES, 0); //clear properties
						converted[0] = true;
						actualDrops.add(new Item(21634)); //convert to uncharged shield.
					} else if (drop.id() == 11907) { // Normal trident (used) .. note full version doesn't need converting, its tradable fully charged
						int charges = drop.property(ItemAttrib.CHARGES);
						if (charges > 0) {
							converted[0] = true; // Don't drop original item!
							actualDrops.add(new Item(11908)); // Trident (uncharged)
							actualDrops.add(new Item(554, charges * 5)); // fire
							actualDrops.add(new Item(560, charges)); // death
							actualDrops.add(new Item(562, charges)); // chaos
						}
					} else if (drop.id() == 12899) { // Toxic trident (used) .. note there is no full version, since the toxic version is NOT tradable.
						int charges = drop.property(ItemAttrib.CHARGES);
						if (charges > 0) {
							converted[0] = true; // Don't drop original item!
							actualDrops.add(new Item(12900)); // TOXIC Trident (uncharged)
							actualDrops.add(new Item(554, charges * 5)); // fire
							actualDrops.add(new Item(560, charges)); // death
							actualDrops.add(new Item(562, charges)); // chaos
							actualDrops.add(new Item(12934, charges)); // scales
						}
					} else if (drop.id() == 12931 || drop.id() == 13197 || drop.id() == 13199) { // Mutagen helm (charged only)
						int scales = drop.property(ItemAttrib.ZULRAH_SCALES);
						if (scales > 0) {
							converted[0] = true; // Don't drop original item!
							actualDrops.add(new Item(12929)); // Uncharged serp
							actualDrops.add(new Item(12934, scales)); // Scales
						}
					} else if (!target.world().realm().isPVP() && drop.id() == TomeOfFire.INSTANCE.getTOME()) { // Tome of Fire
						int charges = drop.property(ItemAttrib.CHARGES);
						if (charges > 0) {
							converted[0] = true; // Don't drop original item!
							actualDrops.add(new Item(20716)); //
							if (charges >= 20) // 1 page = 20 charges
								actualDrops.add(new Item(20718, charges / 20));
						}
					} else if (drop.id() == 12809) { //Saradomin's blessed sword
						converted[0] = true;
						actualDrops.add(new Item(11838));
						actualDrops.add(new Item(12804));
					} else if (drop.id() == RingOfSuffering.getSUFFERING_R()) { // Ring of suffering (r)
						drop.property(ItemAttrib.CHARGES, 0);
						converted[0] = true; // Don't drop original item!
						actualDrops.add(new Item(19550)); // Uncharged suffering
					} else if (drop.id() == RingOfSuffering.getSUFFERING_I()) { // Ring of suffering (I)
                        drop.property(ItemAttrib.CHARGES, 0);
						converted[0] = true; // Don't drop original item!
						actualDrops.add(new Item(19550)); // Uncharged suffering
					} else if (drop.id() == RingOfSuffering.getSUFFERING_RI()) { // Ring of suffering (rI)
                        drop.property(ItemAttrib.CHARGES, 0);
						converted[0] = true; // Don't drop original item!
						actualDrops.add(new Item(19550)); // Uncharged suffering
					} else if (drop.id() == DragonfireShield.DragonfireShieldType.WARD.charged()) {
						drop.property(ItemAttrib.CHARGES, 0);
						converted[0] = true;
						actualDrops.add(new Item(DragonfireShield.DragonfireShieldType.WARD.uncharged()));
					}
				}
				
				// DONT ADD ANY MORE CONVERTED CHECKS BELOW HERE
				if (!converted[0]) { // The item was not changed
					actualDrops.add(drop); // Drop the original item intended.
				}
				for (Item actual : actualDrops) {
					if (target.world().realm().isPVP()) {
						value[0] += target.world().prices().getOrElse(actual.unnote(target.world()).id(), 0) * actual.amount();
					} else {
						value[0] += actual.realPrice(target.world()) * actual.amount(); // ??
					}
					
					// Actually generate the ground item
					loot.loot.add(new LootItem(actual, hide));
				}
			}
		});
		
		if (!maygive[0]) {
			target.message("<col=FF0000>Either your or the person that killed you's account is newly created, therefore you can run back for your items.");
		}
		
		if (!target.world().realm().isDeadman()) {
			target.world().server().scriptExecutor().executeScript(target, GravestoneMarker.script);
		}
		
		// Drop a set of bones. How creepy :)
		loot.loot.add(new LootItem(new Item(526), false));
		
		// Like RS converts some untradables to GP (and other person can't get them back, like b gloves) we'll give them some BM.
		if (bmdrop > 0 && killerId != target.id()) {
			loot.loot.add(new LootItem(new Item(13307, bmdrop), false));
		}
		if (coindrop > 0 && killerId != target.id()) {
			loot.loot.add(new LootItem(new Item(995, coindrop), false));
		}
		
		target.putattrib(AttributeKey.LASTDEATH_VALUE, value[0]);
		target.putattrib(AttributeKey.LAST_DEATH_TILE, target.tile());
		
		// Rewards!
		if (mostdam != null && mostdam != target && mostdam.isPlayer()) {
			bloodMoneyReward((Player) mostdam, target, validKill);
		}

		loot.loot.forEach(lootItem -> {
			GroundItem g = new GroundItem(target.world(), lootItem.item, target.tile(), lootItem.item.rawtradable(target.world()) ? killerId : target.id());
			g.pkedFrom(target.id()); // Mark item as from PvP to avoid ironmen picking it up.

			if (target.world().realm().isPVP() && lootItem.hidden) {
				g.hidden(); // Food/potions only are visible to killer - dont show up for others -> so people can't suicide and drop brews during deep wild pking
			}

			// Is it untradable, and are we on the eco server?
			if (!lootItem.item.rawtradable(target.world()) && !target.world().realm().isPVP()) {
				g.lifetime(GroundItem.UNTRADABLE_LIFETIME); // 3 minutes to run back for it.
			}

			// KillerId defaults to yourself if we died to a NPC
			if (!inwild) {
				// Make the drop for (ourselves if dying in pvm OR pker who killed us) become hidden and last for 30 mins. Run back.
				if (target.id() == killerId) {
					g.hidden(); // Don't make our stuff show to others so they can take it. Screw that mechanic.
					g.lifetime(1000 * 60 * 30); // 30 mins for BM items
				}
			}
			// Spawn it now settings are complete.
			target.world().spawnGroundItem(g);
		});
		
		// Give back items on deadman testing.
		if (target.world().realm().isDeadman() && inv != null && equip != null && target.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
			target.inventory().empty();
			target.equipment().empty();
			target.inventory().items(inv);
			target.equipment().items(equip);
			target.inventory().makeDirty();
			target.equipment().makeDirty();
		}
		
		// Logging who got the items
		try {
			if (mostdam.isPlayer()) {
				target.world().server().service(LoggingService.class, true).ifPresent(s -> s.logKill((Player) mostdam, target, value[0], loot));
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	/**
	 * Items which on 07 would be untradable. We have this hardcoded check for specific items like crystal bows
	 * because of OSS they're tradable in their definition. Used for IKOD checks.
	 */
	public static boolean isRSUntradeableItem(Item unt_item) {
		int id = unt_item.id();
		
		// Max cape (regular)
		if (id == 13342) {
			return true;
		}
		
		for (int untradable : RS_UNTRADABLES_LIST) {
			if (id == untradable) {
				return true;
			}
		}
		
		if (isCrystal(id)) {
			return true;
		}
		
		return false;
	}
	
	public static void bloodMoneyReward(Player killer, Player target, boolean valid) {
		
		// Ruin his kill streak. Only when dying to a player.
		int targetks = target.attribOr(AttributeKey.KILLSTREAK, 0);
		target.clearattrib(AttributeKey.KILLSTREAK);
		target.saveHighscores();
		
		// Add a death. Only when dying to a player.
		Death.deaths(target, 1);
		
		// Blood money on PK worlds
		try {
			if (target.world().realm().isRealism()) {
				killer.addKills(1);
			} else if (target.world().realm().isPVP() || target.world().realm().isOSRune()) {
				
				// Check if farming prevention blocks this kill
				FarmingPrevention prevention = target.world().server().service(FarmingPrevention.class, true).orElse(null);
				
				// Addresses have recently killed the other.
				boolean ip_farmed = prevention != null && prevention.prevents(killer, target);
				
				// Prevention being null means no such recent death between these who addresses has been tracked.
				boolean legitimate_kill = prevention == null || (!ip_farmed);
				
				if (killer.privilege().eligibleTo(Privilege.ADMIN) && !legitimate_kill) {
					killer.debug("Anti kill farming checks bypassed as an admin.");
					legitimate_kill = true;
				}
				
				if (!legitimate_kill) {
					//killer.debug("Not legit! %s %s", ip_farmed, no_heal_attempt);
					// Seems like this was blocked. We'll inform, unless it's the same IP, in which case you know you're farming lmfao KYS (silent)
					if (!killer.ip().equals(target.ip())) {
						// Feedback for non legit kills.
						if (ip_farmed) {
							killer.message("You have killed this member previously in the past 5 minutes, and have not been awarded.");
						}
					} else if (killer.privilege().eligibleTo(Privilege.ADMIN)) {
						killer.debug("Booster! " + (ip_farmed ? "You've killed " + target.name() + " recently." : "Dunno how tho lol xd xx"));
					}
				} else {
					// Legit! Let's reward...
					
					// Add a kill when the kill is valid (not farming) and it's not in duel arena/FFA CW
					if (valid) {
						killer.addKills(1);

						// Elo rating check.
//						EloRating.modify(killer, target);
//						AchievementAction.INSTANCE.processCategoryAchievement(killer, AchievementCategory.PVP);
						
						int ks = (Integer) killer.attribOr(AttributeKey.KILLSTREAK, 0) + 1;
						killer.putattrib(AttributeKey.KILLSTREAK, ks);
						
						// Did we reach a new high in terms of KS?
						int ksRecord = killer.attribOr(AttributeKey.KILLSTREAK_RECORD, 0);
						if (ks > ksRecord) {
							killer.putattrib(AttributeKey.KILLSTREAK_RECORD, ks);
							killer.saveHighscores();
						}
						
						// Declare base value for our kill. On OSRune worlds, 10bm. Otherwise variable.
						int bloodReward = target.world().realm().isOSRune() ? bloodMoneyForOSRune(killer) : bloodMoneyFor(killer, target);

						// Apply donation boost, if any
						bloodReward = (int) ((double )bloodReward * killer.donationTier().modifiers()[DonatorBoost.BLOOD_MONEY.ordinal()]);
						// Killstreak going on? Print :)
						if (ks > 1) {
							killer.message("You're currently on a killing spree of " + ks + "!");
							
							AchievementAction.INSTANCE.processCategoryAchievement(killer, AchievementCategory.KILLSTREAK, ks);
							
							if (target.world().realm().isPVP() && (ks % 5 == 0 || ks > 15)) {
								killer.world().players().forEach(other -> {
									if (!VarbitAttributes.varbiton(other, Varbit.HIDE_KS_BROADCASTS)) {
										other.filterableMessage("<col=ff0000><img=" + QuestTab.getKillstreakIcon(killer) + "> " + killer.name() + " has a killing spree of " + ks + " and can be shut down for " + (100 + shutdownValueOf(ks)) + " Blood money!");
									}
								});
							}
						}
						
						// Announce if you shut down a killstreak
						if (targetks >= 5) {
							killer.world().players().forEach(other -> {
								if (!VarbitAttributes.varbiton(other, Varbit.HIDE_KS_BROADCASTS)) {
									other.filterableMessage("<col=ff0000><img=" + QuestTab.getKillstreakIcon(target) + "> " + killer.name() + " has shut down " + target.name() + " with a killing spree of " + targetks + ".");
								}
							});
						}
						
						// If this passes our shutdown record, change it
						int record = killer.attribOr(AttributeKey.SHUTDOWN_RECORD, 0);
						if (targetks > record) {
							killer.putattrib(AttributeKey.SHUTDOWN_RECORD, targetks);
							killer.saveHighscores();
						}
						
						// Apply target's killstreak on our reward. Oh, and our streak. Not on OSRune!
						if (target.world().realm().isPVP()) {
							bloodReward += shutdownValueOf(targetks); //Add the shutdown value bonus to the blood reward
							bloodReward += killstreakValueOf(ks); //Add the killstreak value bonus to the blood reward
							bloodReward += WildernessLevelIndicator.wildernessLevel(killer.tile()) * 2; //Add the wilderness level bonus to the reward
						}
						
						// Give a 15% bonus if we're authenticated through two-factor
						if (killer.twofactorKey() != null && !killer.twofactorKey().isEmpty() && target.world().realm().isPVP()) {
							bloodReward *= 1.15;
						}
						
						// Double Blood money, if enabled. Can be toggled with ::bmmultiplier <int>. Default 1.
						bloodReward *= World.bmMultiplier;
						
						if (target.world().realm().isPVP()) {
							bloodReward += firstKillOfTheDay(killer); //2000bm for first kill of the day
						}

						if(killer.tile().inArea(Hotspot.ACTIVE.area)) {
							bloodReward *= 2.0;
							killer.message("<col=6a1a18><img=15> You get double blood money for killing a player in a hotspot!");
						}

						// Increment the wilderness deaths
						if(BloodyVolcano.INSTANCE.getWildernessKillsRequired() > 0)
							BloodyVolcano.INSTANCE.setWildernessKillsRequired(BloodyVolcano.INSTANCE.getWildernessKillsRequired() - 1);
						
						// Add blood money to inventory or add to bank
						if (killer.world().realm().isOSRune()) {
							killer.bank().add(new Item(13307, bloodReward), true);
							killer.message("x" + bloodReward + " Blood money has been added to your bank as a reward for slaughtering " + target.name() + ".");
						} else {
							if (killer.inventory().add(new Item(13307, bloodReward), false).failed()) {
								killer.world().spawnGroundItem(new GroundItem(killer.world(), new Item(13307, bloodReward), target.tile(), killer.id()));
								killer.message("<col=ff0000>You've no room to take the Blood money, it was left where your victim died.");
							} else {
								killer.message("<col=ff0000>You take the Blood money collected from your opponents corpse.");
							}
						}
						
						// On PvP give a nice Blood Key if you're lucky and above level 5 Wilderness.
						int wildlvl = WildernessLevelIndicator.wildernessLevel(target.tile());
						if (wildlvl >= 5 && WildernessLevelIndicator.inWilderness(target.tile())) {
							int attackerRisk = Math.max(12_000, Math.min(50_000, killer.attribOr(AttributeKey.RISKED_WEALTH, 0)));
							int targetRisk = Math.max(12_000, Math.min(50_000, target.attribOr(AttributeKey.RISKED_WEALTH, 0)));

							double chance = (500 + (attackerRisk / 550) * 2 + (targetRisk / 550) * 2 + wildlvl * 11) * killer.donationTier().modifiers()[DonatorBoost.BLOODY_KEYS.ordinal()];
							if (Math.random() <= chance / (killer.tile().inArea(Hotspot.ACTIVE.area) ? 50000d : 100000d)) {
								BloodChest.dropKey(killer, wildlvl, attackerRisk + targetRisk, target.tile());
							}
						}
						
					}
				}
				
				// If farming prevention is active, put this as an entry.
				if (prevention != null) {
					prevention.log(killer, target);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	private static int shutdownValueOf(int streak) {
		return (10 * streak + 50 * (streak / 10));
	}
	
	private static int killstreakValueOf(int streak) {
		return 5 * streak;
	}
	
	private static int firstKillOfTheDay(Player killer) {
		if (System.currentTimeMillis() >= (long) killer.attribOr(AttributeKey.FIRST_KILL_OF_THE_DAY, 0L)) {
			killer.message("You've earned 2,500 additional bm for your first kill of the day!");
			killer.putattrib(AttributeKey.FIRST_KILL_OF_THE_DAY, System.currentTimeMillis() + TimeUnit.HOURS.toMillis(24));
			return 2500;
		}
		return 0;
	}
	
	public static void sendIKODScript(Player player) {
		LinkedList<Item> items = new LinkedList<>();
		
		Collections.addAll(items, player.inventory().copy());
		Collections.addAll(items, player.equipment().copy());
		
		// Keep untradables if NOT on the PVP world!
		if (!player.world().realm().isPVP())
			items.removeIf(item -> item == null || !item.rawtradable(player.world()));
		else
			items.removeIf(item -> item == null);
		
		items.sort((o1, o2) -> {
			o1 = o1.unnote(player.world());
			o2 = o2.unnote(player.world());
			
			ItemDefinition def = o1.definition(player.world());
			ItemDefinition def2 = o2.definition(player.world());
			
			int v1 = 0;
			int v2 = 0;
			
			if (def != null) {
				v1 = o1.realPrice(player.world());
				if (v1 <= 0 && !player.world().realm().isPVP()) {
					v1 = def.cost;
				}
			}
			if (def2 != null) {
				v2 = o2.realPrice(player.world());
				if (v2 <= 0 && !player.world().realm().isPVP()) {
					v2 = def2.cost;
				}
			}
			
			return Integer.compare(v2, v1);
		});
		
		long totalWealth = 0;
		for (Item item : items) {
			if (item != null) {
				totalWealth += item.realPrice(player.world()) * item.amount();
			}
		}
		
		int left = Skulling.skulled(player) ? 0 : 3;
		long keptPrice = 0;
		if (player.varps().varbit(Varbit.PROTECT_ITEM) == 1)
			if (player.attribOr(AttributeKey.NON_PLUS1_AREA, false)) {
				player.message("<col=FF0000>Warning:</col> You are in a High Risk area where the Protect item prayer does not work.");
			} else {
				left++;
			}
		
		// On Ultimate Iron Man, you drop everything!
		if (player.ironMode() == IronMode.ULTIMATE) {
			left = 0;
		}
		
		int itemsKept = left;
		
		ItemContainer container = new ItemContainer(player.world(), 6, ItemContainer.Type.REGULAR);
		
		while (left-- > 0 && !items.isEmpty()) {
			Item head = items.peek();
			if (head == null) {
				left++;
				items.poll();
				continue;
			}
			
			container.add(new Item(head.id(), 1), true);
			keptPrice += head.realPrice(player.world()) * head.amount();
			
			if (head.amount() == 1) { // Amount 1? Remove the item entirely.
				items.poll();
			} else { // Amount more? Subtract one amount.
				int index = items.indexOf(head);
				items.set(index, new Item(head, head.amount() - 1));
			}
		}
		
		ItemContainer keptItems = new ItemContainer(player.world(), player.inventory().size() + player.equipment().size(), ItemContainer.Type.REGULAR);
		for (Item i : items) {
			if (i != null) {
				keptItems.add(i, true);
			}
		}
		
		player.write(new SetItems(584, -1, 63718, container));
		player.write(new SetItems(468, -1, 63834, keptItems));
		
		long riskedWealth = totalWealth - keptPrice;
		String s = "<col=FF3031>Lots!</col>";
		if (riskedWealth < Integer.MAX_VALUE) {
			s = RSFormatter.formatNumber(totalWealth - keptPrice) + " " + (player.world().realm().isPVP() ? "bm" : "gp");
		}
		player.invokeScript(118, 0, "", itemsKept, 0, 0, s);
	}
	
	private static int id(Item item) {
		return item == null ? 0 : item.id();
	}

	public static int BASE_W2_BLOOD_MONEY_REWARD = 500;
	
	private static int bloodMoneyFor(Entity killer, Player target) {
		if (killer instanceof Player) {
			return BASE_W2_BLOOD_MONEY_REWARD;
		}
		
		return 1;
	}
	
	private static int bloodMoneyForOSRune(Entity killer) {
		int wild_level = WildernessLevelIndicator.wildernessLevel(killer.tile());
		
		if (wild_level > 40)
			return 20;
		else if (wild_level > 20)
			return 15;
		else
			return 10;
	}
	
	public static final String randomKillMessage(Player killer) {
		return KILL_MESSAGES[killer.world().random(KILL_MESSAGES.length - 1)];
	}
	
	private static final Area BOTTOM_LINE = new Area(2944, 3523, 3358, 3590);
	
	public static final int[] accountIdsPetMechanicEnabled = new int[]{}; // Doxer, Tardis, Belaru, Dmm_gun (jak)
	public static final int[] accountIdsForClanchatMechanic = new int[]{}; // Clanchat, pavic, mosque (gund)
	public static final String[] validClanchatNames = new String[]{"mosque"};
	
	/**
	 * If our account has the ability for the custom Pet Shout mechanic - where when you kill someone
	 * your pet will shout something.
	 */
	public static boolean hasShoutAbility(Player player) {
		// Are we a user with the mechanic enabled
		for (int accId : accountIdsPetMechanicEnabled) {
			if ((int) player.id() == accId) {
				return true;
			}
		}
		Optional<ClanChat> cco = ClanChat.current(player);
		if (cco.isPresent()) {
			ClanChat cc = cco.get();
			// Clanchats with specific names get the mechanic
			for (String name : validClanchatNames) {
				if (cc.name().equalsIgnoreCase(name)) {
					return true;
				}
			}
			// Clanchats owned by certain accounts get the mechanic.
			for (int ccOwnerId : accountIdsForClanchatMechanic) {
				if (ccOwnerId == cc.ownerId()) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Drops items to the person who did most damage, if not in a safe zone.
	 *
	 * @param player The dead player
	 */
	public static void droplootToKiller(Player player, Player mostdam) {
		
		// If we're in FFA clan wars, don't drop our items.
		// Have these safe area checks before we do some expensive code ... looking for who killed us.
		if (FFAClanWars.inFFAMap(player) || ClanWars.inInstance(player) || (boolean) player.attribOr(AttributeKey.IN_STAKE, false) || player.timers().has(TimerKey.POST_ARENA_DEATH_IMMUNITY)) {
			return;
		}
		
		// In a zulrah map, you're not really dropping anything. This may need some finetuning when we add more dyn maps..
		Optional<InstancedMap> active = player.world().allocator().active(player.tile());
		if (active.isPresent() && !active.get().dangerous()) {
			// Zulrah is Hardcore-dangerous, but you keep your items.
			if (active.get().getIdentifier().orElse(null) == InstancedMapIdentifier.ZULRAH && player.ironMode() == IronMode.HARDCORE) {
				stripHardcoreRank(player);
			}
			
			return;
		}
		
		// If it's not a safe death, turn a Hardcore Ironman into a regular.
		if (player.ironMode() == IronMode.HARDCORE) {
			stripHardcoreRank(player);
		}
		
		// Past this point.. we're in a dangerous zone! Drop our items....
		
		if (mostdam == player) {
			//player.debug("non player death.");
			// At this point, if we still can't find any instance of the killer at all.
			// We (a) suicided or (b) died to an NPC
			ItemsOnDeath.dropItems(player, player);
			return;
		}
		
		Preconditions.checkArgument(mostdam != null); // No items lost if this is the case. Impossible.
		Entity pker = player.attribOr(AttributeKey.KILLER, null);
		/*if (pker == null) {
			player.debug("[%s] died to [~unknown]. Most dmg was [%s]\n", player.name(), mostdam.username());
		} else if (pker.isPlayer()) {
			player.debug("[%s] died to [Player: %s]. Most dmg was [%s]\n", player.name(), ((Player)pker).name(), mostdam.name());
		} else if (pker.isNpc()) {
			player.debug("[%s] died to [NPC: %s]. Most dmg was [%s]\n", player.name(), ((Npc)pker).def().name, mostdam.name());
		}*/
		
		mostdam.message(ItemsOnDeath.randomKillMessage(mostdam), player.name());
		ItemsOnDeath.dropItems(mostdam, player);
		
		// Send pker a message. Could be an npc.
		if (pker != null && pker.isPlayer()) {
			Player player_pker = (Player) pker;
			if (player_pker.id() != mostdam.id()) {
				pker.message("You killed " + player.name() + ", however " + mostdam.name() + " did more damage, so they got the drop.");
			}
			
			// Update quest tab
			Journal.MAIN.send(player_pker);
		}
	}
	
	private static void stripHardcoreRank(Player player) {
		player.ironMode(IronMode.REGULAR); // Revert mode
		
		Hit hit = player.attrib(AttributeKey.LAST_HIT);
		String reason = "to an unknown power";
		
		if (hit != null) {
			HitOrigin origin = hit.origin();
			
			if (origin != null) {
				if (origin instanceof PoisonOrigin) {
					reason = "by succumbing to poison";
				} else if (origin instanceof VenomOrigin) {
					reason = "by succumbing to venom";
				} else if (origin instanceof Player) {
					Player killer = ((Player) origin);
					reason = "in a PVP battle with " + killer.name();
				} else if (origin instanceof Npc) {
					Npc killer = ((Npc) origin);
					
					if (killer.def() != null) {
						reason = "fighting against: " + killer.def().name;
					}
				}
			}
		}
		
		// Broadcast if total is above 400
		if (player.skills().totalLevel() >= 400) {
			player.world().broadcast("<img=10><col=FE0000> " + player.name() +
					" just died in Hardcore Ironman mode with a skill total of " + L10n.format(player.skills().totalLevel()) + " " + reason + " <img=9>.");
		}
		
		// Schedule SQL removal of the rank.
		player.world().server().service(PgSqlService.class, true).ifPresent(sql -> {
			sql.worker().submit(new SqlTransaction() {
				@Override
				public void execute(Connection connection) throws Exception {
					PreparedStatement stmt = connection.prepareStatement("UPDATE characters SET ironmode = 1 WHERE id = ?");
					stmt.setInt(1, player.characterId());
					stmt.executeUpdate();
					connection.commit();
				}
			});
		});
	}
	
}
