package nl.bartpelle.veteres.util.journal.toggles;

import nl.bartpelle.veteres.content.mechanics.VarbitAttributes;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class HideSpreeIcons extends JournalEntry {

    public static void update(Player player) {
        boolean hide = VarbitAttributes.varbiton(player, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.getVarbitid());
        player.write(UpdateStateCustom.skullToggle(!hide));
    }

    @Override
    public void send(Player player) {
        boolean hide = VarbitAttributes.varbiton(player, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.getVarbitid());
        if(hide)
            send(player, "<img=58> Hide Spree Icons", "On", Color.GREEN);
        else
            send(player, "<img=58> Hide Spree Icons", "Off", Color.RED);
        update(player);
    }

    @Override
    public void select(Player player) {
        VarbitAttributes.toggle(player, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.getVarbitid());
        send(player);
    }

}