package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.veteres.content.combat.PlayerCombat;
import nl.bartpelle.veteres.content.mechanics.PlayerInteraction;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.Entity;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;
import nl.bartpelle.veteres.script.JSScripts;

import java.lang.ref.WeakReference;

/**
 * Created by Jak on 10/01/2016.
 */
@PacketInfo(size = 3)
public class PlayerAction6 implements Action {
	
	protected boolean run;
	protected int index;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		index = buf.readShort();
		run = buf.readByte() == 1;
	}
	
	@Override
	public void process(Player player) {
		player.stopActions(true);
		
		Player other = player.world().players().get(index);
		if (other == null) {
			player.message("Unable to find player.");
		} else {
			player.debug("Click 6 db-id=%d pid=%d  dead: %s %s  attackable=%s",
					other.id(), index, other.dead() ? "Y" : "N", player.dead() ? "Y" : "N",
					player.attribOr(AttributeKey.DEBUG, false) ? player.attribOr(AttributeKey.ATTACK_OP, false) ? PlayerCombat.canAttack(player, other) : true : "?");
			if (!player.locked() && !player.dead()) {
				player.face(other);
				
				if (!other.dead()) {
					player.putattrib(AttributeKey.TARGET, new WeakReference<Entity>(other));
					player.putattrib(AttributeKey.INTERACTION_OPTION, 6);
					player.world().server().scriptExecutor().executeLater(player, new PlayerInteraction(6));
				}
				
				if ((int) player.attribOr(AttributeKey.JSS_USE, 0) == 1) {
					if (JSScripts.invokeWithFailTest("paction6", player, other)) {
						// Nothing
					}
				}
			}
		}
	}
	
}
