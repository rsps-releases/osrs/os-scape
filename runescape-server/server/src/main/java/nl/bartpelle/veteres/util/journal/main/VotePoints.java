package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.interfaces.questtab.QuestTab;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.AttributeKey;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class VotePoints extends JournalEntry {

    @Override
    public void send(Player player) {
        int votingPoints = player.attribOr(AttributeKey.VOTING_POINTS, 0);
        send(player, "<img=23> Vote Points", votingPoints, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        player.world().server().scriptExecutor().executeScript(player, new QuestTab.VotePoints(player));
    }

}