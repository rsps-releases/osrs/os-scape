package nl.bartpelle.veteres.util.journal;

import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.command.SendJournal;
import nl.bartpelle.veteres.util.journal.main.*;
import nl.bartpelle.veteres.util.journal.presets.*;
import nl.bartpelle.veteres.util.journal.toggles.*;

import java.util.ArrayList;

public enum Journal {

    MAIN,
    PRESETS;

    private ArrayList<JournalCategory> categories = new ArrayList<>();

    private JournalCategory lastCategory;

    private void addCategory(String name) {
        categories.add(lastCategory = new JournalCategory(name));
        categories.trimToSize();
    }

    private ArrayList<JournalEntry> entries = new ArrayList<>();

    private void addEntry(JournalEntry entry) {
        if (lastCategory == null)
            addCategory("");
        entry.childId = entries.size();
        entries.add(entry);
        entries.trimToSize();
        lastCategory.count++;
    }

    public void send(Player player) {
        player.setJournal(this);
        player.write(new SendJournal(ordinal(), categories));
        for (JournalEntry entry : entries)
            entry.send(player);
    }

    public void select(Player player, int childId) {
        if (childId < 0 || childId >= entries.size())
            return;
        entries.get(childId).select(player);
    }

    public void update(Player player) {
        if(player.journal() == MAIN) {
            Uptime.INSTANCE.send(player);
            PlayersOnline.INSTANCE.send(player);
            WildernessCount.INSTANCE.send(player);
            Playtime.INSTANCE.send(player);
            WildernessBoss.INSTANCE.send(player);
            Hotspot.Entry.INSTANCE.send(player);
            BloodyVolcanoEvent.Entry.INSTANCE.send(player);
        }
    }

    static {
        /**
         * Main
         */
        MAIN.addCategory("Server");
        MAIN.addEntry(Uptime.INSTANCE);
        MAIN.addEntry(Playtime.INSTANCE);
        MAIN.addEntry(PlayersOnline.INSTANCE);
        MAIN.addEntry(WildernessCount.INSTANCE);
        MAIN.addCategory("Player");
        MAIN.addEntry(new VotePoints());
        MAIN.addEntry(new TotalSpent());
        MAIN.addEntry(new StoreCredits());
        MAIN.addCategory("Wilderness");
        MAIN.addEntry(new Kills());
        MAIN.addEntry(new Deaths());
        MAIN.addEntry(new KDRatio());
        MAIN.addEntry(new Killstreak());
        MAIN.addEntry(new HighestKillstreak());
        MAIN.addEntry(new HighestShutdown());
        MAIN.addEntry(Risk.INSTANCE);
        MAIN.addEntry(new EloRatingEntry());
        MAIN.addCategory("Presets");
        MAIN.addEntry(new HybridPreset());
        MAIN.addEntry(new MeleePreset());
        MAIN.addEntry(new ZerkerPreset());
        MAIN.addEntry(new PureNHPreset());
        MAIN.addEntry(new CustomPresetOptions());
        MAIN.addCategory("Events");
        MAIN.addEntry(WildernessBoss.INSTANCE);
        MAIN.addEntry(Hotspot.Entry.INSTANCE);
        MAIN.addEntry(BloodyVolcanoEvent.Entry.INSTANCE);
        MAIN.addCategory("Toggles");
        MAIN.addEntry(new SwapMagePrayers());
        MAIN.addEntry(new SwapRangePrayers());
        MAIN.addEntry(new BreakVials());
        MAIN.addEntry(new HideSpreeIcons());
        MAIN.addEntry(new HPOverlay());
        MAIN.addEntry(new HideTimers());
        MAIN.addEntry(RiskProtectionState.INSTANCE);

        /**
         * Presets
         */
        PRESETS.addCategory("Main");
        PRESETS.addEntry(new HybridPreset());
        PRESETS.addEntry(new MeleePreset());
        PRESETS.addCategory("Zerker");
        PRESETS.addEntry(new ZerkerPreset());
        PRESETS.addCategory("Pure");
        PRESETS.addEntry(new PureNHPreset());
        PRESETS.addCategory("Custom");
        PRESETS.addEntry(new PresetsInformation());
        for(int i = 0; i < 20; i ++) {
            PRESETS.addEntry(new CustomPresets(i));
        }
    }

}