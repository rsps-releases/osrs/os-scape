package nl.bartpelle.veteres.services.serializers.pg.part;

import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import nl.bartpelle.veteres.model.item.ItemAttrib;
import nl.bartpelle.veteres.model.item.ItemContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Bart on 1/31/2016.
 */
public class ItemsPart implements PgJsonPart {
	
	private static final Logger logger = LogManager.getLogger(ItemsPart.class);
	
	@Override
	public void decode(Player player, ResultSet resultSet, int characterId, Connection connection) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("SELECT * FROM items WHERE character_id = ?");
		statement.setInt(1, characterId);
		
		// Load all the items by our character id..
		ResultSet items = statement.executeQuery();
		while (items.next()) {
			Item item = new Item(items.getInt("id"), items.getInt("amount"));
			int slot = items.getInt("slot");
			String container = items.getString("container");
			
			// Put it in the right container, based on type.
			getContainer(player, container).set(slot, item);
		}
		
		// Mark all containers as dirty to force a refresh (well, not bank)
		player.inventory().makeDirty();
		player.equipment().makeDirty();
		
		PreparedStatement stm_attribs = connection.prepareStatement("SELECT * FROM itemattribs WHERE character_id = ?");
		stm_attribs.setInt(1, characterId);
		
		// Load all the items by our character id..
		ResultSet attr_r = stm_attribs.executeQuery();
		while (attr_r.next()) {
			int slot = attr_r.getInt("slot");
			String container = attr_r.getString("container");
			int keyid = attr_r.getInt("attribkey");
			int keyval = attr_r.getInt("attribvalue");
			
			// Put it in the right container, based on type.
			Item item = getContainer(player, container).get(slot);
			if (item == null)
				continue;
			Optional<ItemAttrib> iattr = ItemAttrib.forPersistencyKey(keyid);
			if (iattr.isPresent()) {
				item.property(iattr.get(), keyval);
			}
		}
	}
	
	@Override
	public void encode(Player player, PreparedStatement characterUpdateStatement, int characterId, Connection con, boolean removeOnline) throws SQLException {
		long now = System.nanoTime();
		
		// Start off by deleting all the items the player has.
		PreparedStatement remove = con.prepareStatement("DELETE FROM items WHERE character_id = ?;");
		remove.setInt(1, characterId);
		remove.executeUpdate();
		
		// Now begin by batch-inserting the items into the table.
		PreparedStatement insert = con.prepareStatement("INSERT INTO items (character_id, id, amount, container, slot) VALUES (?, ?, ?, ?::container_type, ?)");
		
		// Put all the required containers into the table.
		putItems(characterId, "inventory", player.inventory(), insert);
		putItems(characterId, "equipment", player.equipment(), insert);
		putItems(characterId, "bank", player.bank(), insert);
		putItems(characterId, "lootingbag", player.lootingBag(), insert);
		
		// Mark the batch as done, commit it.
		insert.executeBatch();
		
		
		// Start off by deleting all the items the player has.
		PreparedStatement attrib_remove = con.prepareStatement("DELETE FROM itemattribs WHERE character_id = ?;");
		attrib_remove.setInt(1, characterId);
		attrib_remove.executeUpdate();
		
		// Now begin by batch-inserting the items into the table.
		PreparedStatement attrib_insert = con.prepareStatement("INSERT INTO itemattribs (character_id, attribkey, attribvalue, container, slot) VALUES (?, ?, ?, ?::container_type, ?)");
		
		// Put all the required containers into the table.
		putAttribs(characterId, "inventory", player.inventory(), attrib_insert);
		putAttribs(characterId, "equipment", player.equipment(), attrib_insert);
		putAttribs(characterId, "bank", player.bank(), attrib_insert);
		putAttribs(characterId, "lootingbag", player.lootingBag(), attrib_insert);
		
		// Mark the batch as done, commit it.
		attrib_insert.executeBatch();
		
		logger.info("Finished saving items in {}ms.", (System.nanoTime() - now) / 1_000_000);
	}
	
	private void putAttribs(int characterId, String name, ItemContainer container, PreparedStatement attrib_insert) throws SQLException {
		for (int slot = 0; slot < container.size(); slot++) {
			Item item = container.get(slot);
			
			// Only put items into the table that are non-null, obviously :)
			if (item != null && item.hasProperties()) {
				for (Map.Entry<ItemAttrib, Integer> e : item.propset()) {
					// Ignore non-persistent pairs
					if (!e.getKey().isPersistent())
						continue;
					attrib_insert.setInt(1, characterId);
					attrib_insert.setInt(2, e.getKey().getPersistencyKey());
					attrib_insert.setInt(3, e.getValue());
					attrib_insert.setString(4, name);
					attrib_insert.setInt(5, slot);
					
					// Mark the batch entry and continue.
					attrib_insert.addBatch();
				}
			}
		}
	}
	
	private static void putItems(int characterId, String name, ItemContainer container, PreparedStatement insert) throws SQLException {
		for (int slot = 0; slot < container.size(); slot++) {
			Item item = container.get(slot);
			
			// Only put items into the table that are non-null, obviously :)
			if (item != null) {
				insert.setInt(1, characterId);
				insert.setInt(2, item.id());
				insert.setInt(3, item.amount());
				insert.setString(4, name);
				insert.setInt(5, slot);
				
				// Mark the batch entry and continue.
				insert.addBatch();
			}
		}
	}
	
	private static ItemContainer getContainer(Player player, String name) {
		switch (name) {
			case "inventory":
				return player.inventory();
			case "equipment":
				return player.equipment();
			case "bank":
				return player.bank();
			case "lootingbag":
				return player.lootingBag();
		}
		
		// This shouldn't really ever be reached.. so throw an exception.
		throw new IllegalArgumentException("invalid item container type: " + name);
	}
	
}
