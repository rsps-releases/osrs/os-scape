package nl.bartpelle.veteres.util.journal.main;

import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator;
import nl.bartpelle.veteres.content.npcs.bosses.wilderness.WildernessEventController;
import nl.bartpelle.veteres.fs.Color;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.script.TimerKey;
import nl.bartpelle.veteres.util.journal.JournalEntry;

public class WildernessBoss extends JournalEntry {

    public static final WildernessBoss INSTANCE = new WildernessBoss();

    @Override
    public void send(Player player) {
        String message = "";
        if(player.world().timers().has(TimerKey.WILDERNESS_BOSS_TIMEOUT)) {
            message = "Spawned";
        } else {
            if(message.isEmpty())
                message = player.world().timers().asMinutesLeft(TimerKey.WILDERNESS_BOSS_TIMER);
            if (message.isEmpty())
                message = player.world().timers().asSeconds(TimerKey.WILDERNESS_BOSS_TIMER);
        }
        send(player, "<img=58> Wilderness Boss", message, Color.GREEN);
    }

    @Override
    public void select(Player player) {
        if(player.world().timers().has(TimerKey.WILDERNESS_BOSS_TIMEOUT)) {
            String despawnTimer = player.world().timers().asMinutesAndSecondsLeft(TimerKey.WILDERNESS_BOSS_TIMEOUT);
            String bossSpawned = WildernessEventController.INSTANCE.getActiveEvent().getDesc();
            Tile eventTile = WildernessEventController.INSTANCE.getActiveNpc().get().tile();
            int wildernessLevel = WildernessLevelIndicator.wildernessLevel(eventTile);
            player.message("<col=6a1a18><img=15> " + bossSpawned + " is currently active in level " + wildernessLevel +
                    " wilderness for another " + despawnTimer + "!");
        } else {
            String spawnTimer = player.world().timers().asMinutesAndSecondsLeft(TimerKey.WILDERNESS_BOSS_TIMER);
            if(spawnTimer.isEmpty())
                spawnTimer = player.world().timers().asSeconds(TimerKey.WILDERNESS_BOSS_TIMER);
            player.message("<col=6a1a18><img=15> The next wilderness boss will spawn in " + spawnTimer + ".");
        }
    }

}