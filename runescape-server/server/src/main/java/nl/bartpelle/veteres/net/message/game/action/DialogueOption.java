package nl.bartpelle.veteres.net.message.game.action;

import io.netty.channel.ChannelHandlerContext;
import nl.bartpelle.skript.WaitReason;
import nl.bartpelle.veteres.io.RSBuffer;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.net.message.game.Action;
import nl.bartpelle.veteres.net.message.game.PacketInfo;

/**
 * Created by Bart on 10/3/2015.
 */
@PacketInfo(size = 4)
public class DialogueOption implements Action {
	
	private int hash;
	
	@Override
	public void decode(RSBuffer buf, ChannelHandlerContext ctx, int opcode, int size, Player player) {
		hash = buf.readInt();
		
		log(player, opcode, size, "inter=%d child=%d", hash >> 16, hash & 0xFFFF);
	}
	
	@Override
	public void process(Player player) {
		player.debug("Dialogue option: [%d:%d]", hash >> 16, hash & 0xFFFF);
		player.world().server().scriptExecutor().continueFor(player, WaitReason.DIALOGUE, hash);
		player.world().server().scriptRepository().triggerButton(player, hash >> 16, hash & 0xFFFF, 0, 0, -1);
	}
	
}
