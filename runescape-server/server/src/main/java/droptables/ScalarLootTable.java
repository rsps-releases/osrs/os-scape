package droptables;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import nl.bartpelle.dawnguard.DataStore;
import nl.bartpelle.veteres.content.npcs.pets.Pet;
import nl.bartpelle.veteres.fs.DefinitionRepository;
import nl.bartpelle.veteres.fs.ItemDefinition;
import nl.bartpelle.veteres.model.World;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;
import org.apache.commons.lang.math.Fraction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.util.*;

/**
 * Created by Bart on 6/8/2015.
 */
public class ScalarLootTable {
	
	public static class TableItem {
		public int id;
		public int points = 1;
		public int from;
		public int amount = 1;
		public int min, max;
		public boolean rare = false;
		public String name;
		public Fraction computedFraction;
		
		public Item convert() {
			return new Item(id, amount);
		}
	}
	
	public static Map<Integer, ScalarLootTable> registered = new HashMap<>();
	private static final Random random = new Random();
	public TableItem[] items;
	public TableItem[] guaranteed;
	public ScalarLootTable[] tables;
	private int[] npcs;
	public int rndcap;
	private int tblrndcap;
	private boolean debug = false;
	private int rowmod = 1;
	public int points = 1;
	private int from;
	private boolean rareaffected;
	public String name;
	private int tblpts;
	private transient ScalarLootTable root;
	private boolean PVPWorld = false;
	private boolean global = false;
	public int petRarity;
	public int petItem;
	public int odds = 0;
	
	public static void loadAll(World world, File dir) {
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				loadAll(world, f);
			} else {
				try {
					if (f.getName().endsWith(".json")) {
						ScalarLootTable t = load(f);
						
						if ((world.realm().isPVP() && t.PVPWorld) || (!world.realm().isPVP() && !t.PVPWorld) || t.global) {
							t.process();
							
							for (int n : t.npcs)
								registered.put(n, t);
						}
					}
				} catch (Exception e) {
					System.err.println("Error loading drop file " + f);
					e.printStackTrace();
				}
			}
		}
	}
	
	private void process() {
		// Upscale it
		int current = 0;
		if (items != null) {
			for (TableItem item : items) {
				if (item.points == 0) // Filter out guaranteed drops
					continue;
				
				item.from = current;
				current += item.points;
				
				if (item.rare)
					rareaffected = true;
			}
			
			rndcap = current;
		}
		
		// Normalize the nested tables
		if (tables != null) {
			for (ScalarLootTable table : tables) {
				table.from = current;
				current += table.points;
				
				table.process();
			}
			
			rndcap = current;
		}
		
		calcRare();
	}

	private void recursiveCalcChances(int num, int denom) {
		int computed = ptsTotal();
		// Items..
		if (items != null) {
			for (TableItem item : items) {
				double chance = (double) (item.points * num) / (double) (denom * computed);
				item.computedFraction = Fraction.getFraction(chance);
			}
		}
		
		// Tables
		if (tables != null) {
			for (ScalarLootTable table : tables) {
				table.recursiveCalcChances(num * table.points, denom * computed);
			}
		}
	}
	
	private void setRoot(ScalarLootTable root) {
		this.root = root;
		if (tables != null) {
			for (ScalarLootTable table : tables) {
				table.setRoot(root);
			}
		}
	}
	
	public int ptsTotal() {
		int a = 0;
		if (items != null) {
			for (TableItem item : items) {
				a += item.points;
			}
		}
		if (tables != null) {
			for (ScalarLootTable table : tables) {
				a += table.points;
			}
		}
		return a;
	}
	
	private void calcRare() {
		// Ingest rare knowledge
		if (tables != null) {
			for (ScalarLootTable table : tables) {
				if (table.rareaffected)
					rareaffected = true;
			}
		}
	}
	
	public static ScalarLootTable forNPC(int npc) {
		return registered.get(npc);
	}
	
	public ScalarLootTable randomTable(Random random, boolean ringOfWealth) {
		int drop = random.nextInt(tblrndcap - (ringOfWealth ? (rowmod == 0 ? 1 : rowmod) : 0));
		for (ScalarLootTable table : tables) {
			if (drop >= table.from && drop < table.from + table.points) {
				return table;
			}
		}
		
		return null;
	}
	
	public Item randomItem(Random random, boolean ringOfWealth, boolean realism, boolean laidback) {
		if (rndcap < 1) {
			return null;
		}
		
		int cap = rndcap;
		
		int drop = random.nextInt(cap < 1 ? 1 : cap);
		if (items != null) {
			for (TableItem item : items) {
				if (drop >= item.from && drop < item.from + item.points) {
					if (item.min == 0) {
						return new Item(item.id, item.amount);
					} else {
						return new Item(item.id, random(random, item.min, item.max));
					}
				}
			}
		}
		
		// Try a table now
		if (tables != null && tables.length > 0) {
			for (ScalarLootTable table : tables) {
				if (drop >= table.from && drop < table.from + table.points) {
					Item item = table.randomItem(random, ringOfWealth, realism, laidback);
					return item;
				}
			}
		}
		
		return null;
	}
	
	static int random(Random random, int min, int max) {
		final int n = Math.abs(max - min);
		return Math.min(min, max) + (n == 0 ? 0 : random.nextInt(n + 1));
	}
	
	public List<TableItem> getGuaranteedDrops() {
		return guaranteed == null ? Collections.emptyList() : Arrays.asList(guaranteed);
	}
	
	public List<Item> simulate(Random r, boolean ringOfWealth, int samples, boolean realism, boolean laidback) {
		List<Item> list = new LinkedList<>();
		Map<Integer, Integer> state = new HashMap<>();
		
		for (int i = 0; i < samples; i++) {
			Item random = randomItem(r, ringOfWealth, realism, laidback);
			if (random != null)
				state.compute(random.id(), (key, value) -> value == null ? random.amount() : (value + random.amount()));
		}
		
		state.forEach((k, v) -> list.add(new Item(k, v)));
		return list;
	}
	
	public static ScalarLootTable load(File file) {
		try {
			ScalarLootTable table = load(new String(Files.readAllBytes(file.toPath())));
			table.process();
			table.calcRare();
			table.calcRare();
			table.tblpts = table.ptsTotal();
			table.setRoot(table);
			table.recursiveCalcChances(1, 1);
			return table;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ScalarLootTable load(String json) {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		builder.setLenient();
		Gson gson = builder.create();
		ScalarLootTable table = gson.fromJson(json, ScalarLootTable.class);
		table.process();
		table.tblpts = table.ptsTotal();
		table.setRoot(table);
		table.recursiveCalcChances(1, 1);
		return table;
	}
	
	public Optional<Pet> rollForPet(Player player) {
		if (petItem > 0 && petRarity > 0) {
			//player.debug("pet %d is 1/%d", petItem, petRarity);
			if (player.world().rollDie(petRarity, 1)) {
				return Optional.ofNullable(Pet.getPetByItem(petItem));
			}
		}
		
		return Optional.empty();
	}
	
	public static void main(String[] args) throws Exception {
		Scanner scanner = new Scanner(System.in);
		SecureRandom rand = new SecureRandom();
		DefinitionRepository repo = new DefinitionRepository(new DataStore("data/filestore", true), true);
		
		while (true) {
			for (int i = 0; i < 10; i++) System.out.println();
			while (System.in.available() > 0) System.in.read();
			System.out.print("Drop file: ./data/combat/");
			scanner = new Scanner(System.in);
			String file = scanner.nextLine().trim();
			System.out.print("Number of kills: ");
			int kills = scanner.nextInt();
			
			ScalarLootTable root = load(new File("./data/combat/" + file));
			
			List<Item> simulate = root.simulate(rand, false, kills, true, false);
			simulate.sort((o1, o2) -> {
				int oo1 = kills / Math.max(1, o1.amount());
				int oo2 = kills / Math.max(1, o2.amount());
				return Integer.compare(oo1, oo2);
			});
			
			for (Item item : simulate) {
				int indiv = kills / Math.max(1, item.amount());
				System.out.println(item.amount() + " x " + repo.get(ItemDefinition.class, new Item(item.id()).unnote(repo).id()).name + " (1/" + indiv + ")");
			}
			
			System.out.println();
			System.out.println();
		}
	}
	
}
