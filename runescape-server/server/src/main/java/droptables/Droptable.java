package droptables;

import nl.bartpelle.veteres.content.mechanics.NpcDeath;
import nl.bartpelle.veteres.model.GroundItem;
import nl.bartpelle.veteres.model.Tile;
import nl.bartpelle.veteres.model.entity.Npc;
import nl.bartpelle.veteres.model.entity.Player;
import nl.bartpelle.veteres.model.item.Item;

/**
 * Created by Bart on 10/6/2015.
 */
public interface Droptable {
	
	public void reward(Npc npc, Player killer);
	
	default void drop(Npc npc, Player player, Item item) {
		drop(npc.tile(), player, item);
	}
	
	default void drop(Tile tile, Player player, Item item) {
		player.world().spawnGroundItem(new GroundItem(player.world(), item, tile, player.id()));
		NpcDeath.notification(player, item);
	}
	
}
