package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/12/2016. :D
 */
object FletchJavelins {
	
	enum class JavelinData(val item1: Int, val item2: Int, val outcome: Int, val xp: Double, val lvlreq: Int, val itemName: String, val shortName: String) {
		BRONZE_JAVELIN(19584, 19570, 825, 15.0, 3, "bronze arrows", "bronze"),
		IRON_JAVELIN(19584, 19572, 826, 30.0, 17, "iron arrows", "iron"),
		STEEL_JAVELIN(19584, 19574, 827, 75.0, 32, "steel arrows", "steel"),
		MITHRIL_JAVELIN(19584, 19576, 828, 120.0, 47, "mithril arrows", "mithril"),
		ADAMANT_JAVELIN(19584, 19578, 829, 150.0, 62, "adamant arrows", "adamant"),
		RUNE_JAVELIN(19584, 19580, 830, 186.0, 77, "rune arrows", "rune"),
		DRAGON_JAVELIN(19584, 19582, 19484, 225.0, 92, "dragon arrows", "dragon")
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		JavelinData.values().forEach { arrow ->
			repo.onItemOnItem(arrow.item1.toLong(), arrow.item2.toLong(), s@ @Suspendable {
				//Check if the player has a high enough level to do this.
				if (it.player().skills()[Skills.FLETCHING] < arrow.lvlreq) {
					it.messagebox("You need a Fletching level of ${arrow.lvlreq} or above to make that javelin.")
					return@s
				}
				
				//Prompt the player with the # they'd like to make
				var num = 1
				if (it.player().inventory().count(arrow.item1) > 15 && it.player().inventory().count(arrow.item2) > 15) {
					num = it.javelinOptions(Item(arrow.outcome, 150), offsetX = 12)
				}
				
				while (num-- > 0) {
					//Check if the player's ran out of supplies
					if (arrow.item1 !in it.player().inventory() || arrow.item2 !in it.player().inventory()) {
						break
					}
					
					val max = Math.min(15, Math.min(it.player().inventory().count(arrow.item1), it.player().inventory().count(arrow.item2)))
					it.player().inventory() -= Item(arrow.item1, max)
					it.player().inventory() -= Item(arrow.item2, max)
					it.player().inventory() += Item(arrow.outcome, max)
					it.addXp(Skills.FLETCHING, arrow.xp * (max.toDouble() / 15.0))
					it.delay(2)
				}
			})
		}
	}
}
