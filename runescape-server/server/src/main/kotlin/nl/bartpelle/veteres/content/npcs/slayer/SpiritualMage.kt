package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 2/20/2016.
 */

object SpiritualMage {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 7)) {
				if (EntityCombat.attackTimerReady(npc)) {
					// Attack the player
					npc.animate(npc.combatInfo().animations.attack)
					
					// Does NOT splash when miss!
					if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0)) {
						target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(CombatStyle.MAGIC).graphic(get_graphic(npc.id())) // Cannot protect from this.
					} else {
						target.hit(npc, 0, 1).combatStyle(CombatStyle.MAGIC).graphic(Graphic(85, 90, 0)) // Cannot protect from this.
					}
					
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun get_graphic(npc: Int): Graphic {
		return when (npc) {
			3161 -> Graphic(78, 0, 0)
			1610 -> Graphic(78, 0, 0)
			2212 -> Graphic(76, 128, 0)
			2209 -> Graphic(76, 128, 0)
			1611 -> Graphic(76, 128, 0)
			else -> Graphic(77, 96, 0)
		}
	}
}