package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.content.randomEnum
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Situations on 10/31/2015.
 */

object Obelisks { // wild portals

	//Enum to handle wilderness obelisks.
	enum class Obelisk(val id: Int, val x: Int, val z: Int, val h: Int, var active: Boolean) {
		WILDERNESS_13(14829, 3156, 3620, 0, false),
		WILDERNESS_19(14830, 3227, 3667, 0, false),
		WILDERNESS_27(14827, 3035, 3732, 0, false),
		WILDERNESS_35(14828, 3106, 3794, 0, false),
		WILDERNESS_44(14826, 2980, 3866, 0, false),
		WILDERNESS_50(14831, 3307, 3916, 0, false);

		fun tile(): Tile {
			return Tile(x, z)
		}

		fun active(): Boolean {
			return active
		}

		fun setactive(v: Boolean) {
			active = v
		}

        companion object {
            fun forId(id: Int): Obelisk? {
                for (obelisk in Obelisk.values()) {
                    if (obelisk.id == id)
                        return obelisk
                }
                return null
            }
        }
	}

	@Suspendable private fun activateObelisk(s: Script, obelisk: Obelisk) {
		activateObelisk(s, obelisk, randomEnum(Obelisk.values(), obelisk).tile())
	}

	@Suspendable fun activateObelisk(s: Script, obelisk: Obelisk, destTile: Tile) {
		val player = s.player()
		val obj: MapObj = s.player().attrib(AttributeKey.INTERACTION_OBJECT)
		if (obelisk.active()) {
			if (player.attribOr(AttributeKey.DEBUG, false)) {
				player.message("The obelisk is already active!")
			}
			return
		}
		obelisk.active = true
		val x = obelisk.x
		val z = obelisk.z
		// The interacted obelisk coordinates are from the enum at the top of this class.
		// The coordinates below are therefore fixed, and do not change depending on if you clicked the NE/SE/SW/NW pillar.

		//Replace the world object to the temp one
		s.runGlobal(player.world()) @Suspendable {
			val world = player.world()

			val swObelisk = MapObj(Tile(x, z).transform(-2, -2, 0), 14825, obj.type(), obj.rot())
			world.spawnObj(swObelisk)

			val nwObelisk = MapObj(Tile(x, z).transform(-2, +2, 0), 14825, obj.type(), obj.rot())
			world.spawnObj(nwObelisk)

			val seObelisk = MapObj(Tile(x, z).transform(+2, -2, 0), 14825, obj.type(), obj.rot())
			world.spawnObj(seObelisk)

			val neObelisk = MapObj(Tile(x, z).transform(+2, +2, 0), 14825, obj.type(), obj.rot())
			world.spawnObj(neObelisk)

			it.delay(11)

			world.removeObjSpawn(swObelisk)
			world.removeObjSpawn(nwObelisk)
			world.removeObjSpawn(seObelisk)
			world.removeObjSpawn(neObelisk)

			it.delay(1)

			val teleported = LinkedList<Player>()
			world.players().forEachKt({ p ->
				val plrx = p.tile().x
				val plrz = p.tile().z

				if (plrx >= x - 1 && plrx <= x + 1 && plrz >= z - 1 && plrz <= z + 1) {
					if (p.timers().has(TimerKey.TELEBLOCK)) {
						p.message("You're teleblocked and cannot travel with obelisks.")
					} else if (!p.locked() || p.timers().has(TimerKey.SPEAR)) {
						p.stopActions(true)
						p.lock()
						p.animate(3945)
						teleported += p
					}
				}
			})

			it.delay(2)

			teleported.forEach {
				val relative = obelisk.tile() - it.tile()
				it.teleport(destTile - relative)
				it.message("Ancient magic teleports you somewhere in the wilderness.") //Send the playermessage
				it.animate(-1)
				if (!it.timers().has(TimerKey.SPEAR) && PlayerCombat.pendingSpears(it).size == 0) {
					it.unlock()
				}
			}

			obelisk.setactive(false)
		}
	}

	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Obelisk in level 13 wilderness
		r.onObject(14829) @Suspendable {
            when(it.interactionOption()) {
                1 -> Obelisks.activateObelisk(it, Obelisks.Obelisk.WILDERNESS_13)
                2 -> teleportToDestination(it, Obelisks.Obelisk.WILDERNESS_13)
                3 -> chooseDestination(it)
            }
		}

		//Obelisk in level 19 wilderness
		r.onObject(14830) @Suspendable {
            when(it.interactionOption()) {
                1 -> Obelisks.activateObelisk(it, Obelisks.Obelisk.WILDERNESS_19)
                2 -> teleportToDestination(it, Obelisks.Obelisk.WILDERNESS_19)
                3 -> chooseDestination(it)
            }
		}

		//Obelisk in level 27 wilderness
		r.onObject(14827) @Suspendable {
            when(it.interactionOption()) {
                1 -> Obelisks.activateObelisk(it, Obelisks.Obelisk.WILDERNESS_27)
                2 -> teleportToDestination(it, Obelisks.Obelisk.WILDERNESS_27)
                3 -> chooseDestination(it)
            }
		}

		//Obelisk in level 35 wilderness
		r.onObject(14828) @Suspendable {
            when(it.interactionOption()) {
                1 -> Obelisks.activateObelisk(it, Obelisks.Obelisk.WILDERNESS_35)
                2 -> teleportToDestination(it, Obelisks.Obelisk.WILDERNESS_35)
                3 -> chooseDestination(it)
            }
		}

		//Obelisk in level 44 wilderness
		r.onObject(14826) @Suspendable {
            when(it.interactionOption()) {
                1 -> Obelisks.activateObelisk(it, Obelisks.Obelisk.WILDERNESS_44)
                2 -> teleportToDestination(it, Obelisks.Obelisk.WILDERNESS_44)
                3 -> chooseDestination(it)
            }
		}

		//Obelisk in level 50 wilderness
		r.onObject(14831) @Suspendable {
            when(it.interactionOption()) {
                1 -> Obelisks.activateObelisk(it, Obelisks.Obelisk.WILDERNESS_50)
                2 -> teleportToDestination(it, Obelisks.Obelisk.WILDERNESS_50)
                3 -> chooseDestination(it)
            }
		}
	}

    @Suspendable private fun teleportToDestination(it: Script, obelisk: Obelisk) {
        val player = it.player()
        val unlocked = player.attribOr<Boolean>(AttributeKey.OBELISK_REDIRECTION_SCROLL, false)
        if (unlocked) {
            val obeliskDestination = player.attribOr<Int>(AttributeKey.OBELISK_DESTINATION, -1)
            if(obeliskDestination == -1) {
                player.filterableMessage("You need to set a destination before attempting to do this.")
                return
            }

			if(obelisk == Obelisk.forId(obeliskDestination)) {
				player.filterableMessage("Please select a location you're not already at!")
				return
			}

            activateObelisk(it, obelisk, Obelisk.forId(obeliskDestination)!!.tile())
        } else {
            player.filterableMessage("You need learn how to set an obelisk destination before attempting this.")
        }

    }

    @Suspendable private fun chooseDestination(it: Script) {
        val player = it.player()
        val unlocked = player.attribOr<Boolean>(AttributeKey.OBELISK_REDIRECTION_SCROLL, false)
        if (unlocked) {
            val opt = OptionList.display(it, true, "Select an Obelisk to teleport to",
                    "Level 13 Wilderness", "Level 19 Wilderness", "Level 27 Wilderness",
                    "Level 35 Wilderness", "Level 44 Wilderness", "Level 50 Wilderness")
            when(opt) {
                0 -> setDestination(player, Obelisk.WILDERNESS_13, "level 13")
                1 -> setDestination(player, Obelisk.WILDERNESS_19, "level 19")
                2 -> setDestination(player, Obelisk.WILDERNESS_27, "level 27")
                3 -> setDestination(player, Obelisk.WILDERNESS_35, "level 35")
                4 -> setDestination(player, Obelisk.WILDERNESS_44, "level 44")
                5 -> setDestination(player, Obelisk.WILDERNESS_50, "level 50")
            }
        } else {
            player.filterableMessage("You need learn how to set an obelisk destination before attempting this.")
        }
    }

    private fun setDestination(player: Player, obelisk: Obelisk, name: String) {
        player.putattrib(AttributeKey.OBELISK_DESTINATION, obelisk.id)
        player.filterableMessage(Color.DARK_RED.wrap("You have set your obelisk destination to $name wilderness."))
    }

}
