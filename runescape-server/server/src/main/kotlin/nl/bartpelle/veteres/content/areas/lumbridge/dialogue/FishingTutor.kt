package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/7/2016.
 */

object FishingTutor {
	
	val FISHING_TUTOR = 3221
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(FISHING_TUTOR) @Suspendable {
			intro_options(it)
		}
	}
	
	@Suspendable fun intro_options(it: Script) {
		when (it.options("Any advice for an advanced fisher?", "Tell me about different fish.", "Where and what should I fish?", "Goodbye.")) {
			1 -> any_advice(it)
			2 -> different_fish(it)
			3 -> where_and_what(it)
			4 -> goodbye(it)
		}
	}
	
	@Suspendable fun any_advice(it: Script) {
		it.chatPlayer("Any advice for an advanced fisher?", 554)
		it.chatNpc("Go get your net from your bank so you can fish.", FISHING_TUTOR, 567)
		it.chatNpc("Aye, as you get better and better you'll find that you<br>can fish things like Tuna and Swordfish. These be very<br>" +
				"good for combat.", FISHING_TUTOR, 569)
		it.chatNpc("Of course, after that you will get Shark, and if you<br>venture out onto the Trawler from Port Khazard, you<br>" +
				"can then get Manta Ray or Sea Turtle, very good<br>eatin' on one of them.", FISHING_TUTOR, 570)
		it.chatNpc("You can also use a fishing potion if you can get your<br>hands on one and just need that little edge.. ask<br>" +
				"someone who is familiar with herblore.", FISHING_TUTOR, 569)
		it.chatNpc("Now.. quests...", FISHING_TUTOR, 588)
		it.chatNpc("Can't rightly say's there's any I know of that you could<br>handle at the moment, but come back when you're a bit<br>" +
				"more experienced and I'm sure I can give you a clue.", FISHING_TUTOR, 590)
	}
	
	@Suspendable fun different_fish(it: Script) {
		it.chatPlayer("Tell me about different fish.", 554)
		fish_options(it)
	}
	
	@Suspendable fun fish_options(it: Script) {
		when (it.options("Big Net Fish", "Rod and Fly Fishing", "Lobster Pots", "Harpoon Fish", "Tell me about...")) {
			1 -> big_net_fish(it)
			2 -> rod_and_fly(it)
			3 -> lobster_pots(it)
			4 -> harpoon(it)
			5 -> intro_options(it)
		}
	}
	
	@Suspendable fun big_net_fish(it: Script) {
		it.itemBox("Aye, you can net yourself some big fish in Catherby,<br>which is a good place to fish for most things, Gar!", 305)
		it.itemBox("Mackrel and Cod will form the backbone of your catch<br>when big net fishin'.. except for the added extras...", 355)
		it.chatNpc("Some rich rewards for big net fishin', make sure you be<br>using a big net fishing spot though...", FISHING_TUTOR, 568)
		fish_options(it)
	}
	
	@Suspendable fun rod_and_fly(it: Script) {
		it.itemBox("Aye, rod fishin' can be practiced here at these spots, as<br>well as south of Draynor Village and in the Lumbridge<br>river, depending upon your experience. You can get<br>bait at any fishin' shop, there be one in Port Sarim.", 309)
		it.itemBox("With a rod you can catch pike, sardines and herring.<br>Good eating on them.", 351)
		it.itemBox("The art of fly fishin' can be done in rivers, so the<br>Lumbridge river here would suffice.", 309)
		it.itemBox("Aye, you can catch yourself a delicious trout or salmon.", 329)
		fish_options(it)
	}
	
	@Suspendable fun lobster_pots(it: Script) {
		it.itemBox("Arrr, lobster pots can be used from the pier on the<br>island of Karamja.", 301)
		it.itemBox("In Lobster pots you can catch lobsters. They're tasty<br>after thems cooked.", 379)
		it.chatPlayer("Is that all?", 554)
		fish_options(it)
	}
	
	@Suspendable fun harpoon(it: Script) {
		it.itemBox("Arrr, you can also use your harpoon from the pier on<br>the island of Karamja.", 311)
		it.doubleItemBox("With it you can catch Tuna and Swordfish.", 361, 373)
		it.itemBox("....and shark.", 385)
		it.chatNpc("Ahoy, there's also the fishin' guild if ya skilled enough,<br>you can find it south west of Seers' Village.", FISHING_TUTOR, 568)
		fish_options(it)
	}
	
	@Suspendable fun where_and_what(it: Script) {
		it.chatPlayer("Where and what should I fish?", 554)
		it.chatNpc("Tuna and Swordfish can be harpooned - if you're good<br>enough - from the thrivin' fishing village of Catherby, or<br>if you can get in try the Fishin' Guild. Level 35 for<br>Tuna and 50 for Swordfish.", FISHING_TUTOR, 570)
		it.chatNpc("You can also go talk to Murphy and see if you can<br>use his trawler to catch Manta Ray and Sea Turtle if<br>you're really good... Gar!", FISHING_TUTOR, 569)
		intro_options(it)
	}
	
	@Suspendable fun goodbye(it: Script) {
		it.chatPlayer("Goodbye.", 588)
		
	}
}
