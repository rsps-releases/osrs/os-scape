package nl.bartpelle.veteres.content.areas.zeah.wcguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/10/2016.
 */
object Forester {
	
	val FORESTER = 7238
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(FORESTER) @Suspendable {
			when (it.player().world().random(1)) {
				0 -> it.chatNpc("Nice weather we're having today.", FORESTER, 567)
				1 -> it.chatNpc("It's so peaceful here, don't you agree?", FORESTER, 588)
			}
		}
	}
	
}