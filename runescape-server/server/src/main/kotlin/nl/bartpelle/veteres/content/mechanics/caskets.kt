package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.L10n
import java.util.*

/**
 * Created by Situations on 12/11/2015.
 */
object caskets {
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		//Regular mob casket drops
		r.onItemOption1(7956) @Suspendable {
			if (!it.player().world().realm().isPVP) {
				val amount: Int = it.player().world().random(5000..50000)
				it.player().inventory().remove(Item(7956), true)
				it.player().inventory().add(Item(995, amount), true)
				it.player().message("You open the casket and find ${L10n.format(amount.toLong())} gold!")
			}
		}
		
		//Boss casket drops
		r.onItemOption1(6759) @Suspendable {
			if (!it.player().world().realm().isPVP) {
				val amount: Int = it.player().world().random(15000..150000)
				it.player().inventory().remove(Item(6759), true)
				it.player().inventory().add(Item(995, amount), true)
				it.player().message("You open the boss casket and find ${L10n.format(amount.toLong())} gold!")
			}
		}
		
		//Wilderness resource casket drops
		r.onItemOption1(405, s@ @Suspendable {
			if (it.player().world().realm().isPVP) {
				return@s
			}
			val cReward: ArrayList<Item> = arrayListOf(Item(11935, 5), Item(2437, 2), Item(2441, 2), Item(6686, 2), Item(386, 15), Item(3025, 2), Item(537, 2))
			val uncReward: ArrayList<Item> = arrayListOf(Item(11935, 1), Item(11935, 10), Item(2437, 5), Item(2441, 5), Item(6686, 5), Item(386, 30), Item(3025, 5), Item(537, 5))
			val rReward: ArrayList<Item> = arrayListOf(Item(9186, 1), Item(11935, 25), Item(2437, 10), Item(2441, 10), Item(6686, 10), Item(386, 45), Item(3025, 10), Item(537, 10))
			var vrReward: ArrayList<Item> = arrayListOf(Item(1616, 1), Item(11935, 50), Item(2437, 15), Item(2441, 15), Item(6686, 15), Item(386, 60), Item(3025, 15), Item(537, 15))
			var uReward: ArrayList<Item> = arrayListOf(Item(6572, 1))
			
			val chance = it.player().world().random(500)
			var vrRewardID = it.player().world().random(vrReward.size - 1)
			var rRewardID = it.player().world().random(rReward.size - 1)
			var uncRewardID = it.player().world().random(uncReward.size - 1)
			var cRewardID = it.player().world().random(cReward.size - 1)
			var uRewardID = it.player().world().random(uReward.size - 1)
			val c = 300
			val unc = 150
			val rare = 20
			val vrare = 10
			val urare = 1
			
			if (chance == urare) {
				val def = it.player().world().definitions().get(ItemDefinition::class.java, uReward[uRewardID].unnote(it.player().world()).id())
				it.player().inventory().remove(Item(405), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(Item(uReward[uRewardID].id(), uReward[uRewardID].amount()), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().message("You open the wilderness resource casket to find x${uReward[uRewardID].amount()} ${def.name}.")
			} else if (chance <= vrare && !(chance == urare)) {
				val def = it.player().world().definitions().get(ItemDefinition::class.java, vrReward[vrRewardID].unnote(it.player().world()).id())
				it.player().inventory().remove(Item(405), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(Item(vrReward[vrRewardID].id(), vrReward[vrRewardID].amount()), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().message("You open the wilderness resource casket to find x${vrReward[vrRewardID].amount()} ${def.name}.")
			} else if (chance <= rare && !(chance <= vrare)) {
				val def = it.player().world().definitions().get(ItemDefinition::class.java, rReward[rRewardID].unnote(it.player().world()).id())
				it.player().inventory().remove(Item(405), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(Item(rReward[rRewardID].id(), rReward[rRewardID].amount()), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().message("You open the wilderness resource casket to find x${rReward[rRewardID].amount()} ${def.name}.")
			} else if (chance <= unc && !(chance <= rare) && !(chance <= vrare)) {
				val def = it.player().world().definitions().get(ItemDefinition::class.java, uncReward[uncRewardID].unnote(it.player().world()).id())
				it.player().inventory().remove(Item(405), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(Item(uncReward[uncRewardID].id(), uncReward[uncRewardID].amount()), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().message("You open the wilderness resource casket to find x${uncReward[uncRewardID].amount()} ${def.name}.")
			} else if (chance <= c && !(chance <= unc) && !(chance <= rare) && !(chance <= vrare)) {
				val def = it.player().world().definitions().get(ItemDefinition::class.java, cReward[cRewardID].unnote(it.player().world()).id())
				it.player().inventory().remove(Item(405), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(Item(cReward[cRewardID].id(), cReward[cRewardID].amount()), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().message("You open the wilderness resource casket to find x${cReward[cRewardID].amount()} ${def.name}.")
			} else {
				val def = it.player().world().definitions().get(ItemDefinition::class.java, cReward[cRewardID].unnote(it.player().world()).id())
				it.player().inventory().remove(Item(405), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(Item(cReward[cRewardID].id(), cReward[cRewardID].amount()), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().message("You open the wilderness resource casket to find x${cReward[cRewardID].amount()} ${def.name}.")
			}
		})
	}
}