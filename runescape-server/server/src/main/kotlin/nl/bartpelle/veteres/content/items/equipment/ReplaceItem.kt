package nl.bartpelle.veteres.content.items.equipment

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/26/2016.
 */

object ReplaceItem {
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(2513) {
			//Replace the broken dragon chain with the correct one.
			if (it.player().inventory().remove(Item(2513), false).success()) {
				it.player().inventory().add(Item(3140), true)
			}
		}
	}
}
