package nl.bartpelle.veteres.content.minigames.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.attempt
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.NpcSyncInfo
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Bart on 9/10/2016.
 */
object WintertodtGame {
	
	const val TICKS_BETWEEN_GAMES = 100 // 1 minute.
	const val MAX_HEALTH = 3500
	
	const val CYCLE_INTERVAL = 2
	const val DAMAGE_INTERVAL = 1
	
	const val MIN_Z_FOR_DAMAGE = 3988
	const val REQUIRED_POINTS = 500
	
	const val GAME_REGION_ID = 6462
	val SNOW_STORM_TILE = Tile(1627, 4004)
	const val QUIESCENT_STORM = 29309
	const val HOWLING_STORM = 29308
	
	val CONFISCATED_ITEMS = intArrayOf(20695, 20696, 20697, 20698, 20699, 20700, 20701, 20702)
	
	val state = WintertodtGameState()
	@JvmStatic var doubleCrates = false
	@JvmStatic var minigameDisabled = false
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onRegionEnter(GAME_REGION_ID) { onPlayerJoin(it) }
		r.onRegionExit(GAME_REGION_ID) { onPlayerLeave(it) }
		
		r.onWorldInit { registerWintertodt(it.ctx()) }
		
		// Register timer triggers
		r.onWorldTimer(TimerKey.WINTERTODT_COLD) { applyColdDamage(it.ctx()) }
		r.onWorldTimer(TimerKey.WINTERTODT_GAME_START) { startGame(it.ctx()) }
		r.onWorldTimer(TimerKey.WINTERTODT_CYCLE) { processGame(it.ctx()) }
	}
	
	fun registerWintertodt(world: World) {
		if (!world.realm().isPVP) {
			world.timers()[TimerKey.WINTERTODT_CYCLE] = CYCLE_INTERVAL
			world.timers()[TimerKey.WINTERTODT_COLD] = DAMAGE_INTERVAL
			world.timers()[TimerKey.WINTERTODT_GAME_START] = TICKS_BETWEEN_GAMES
			
			// Update all npcs for braziers
			updatePyromancers(world)
			
			// Update states.
			updateSnowstorm(world)
		}
	}
	
	fun applyColdDamage(world: World) {
		world.timers()[TimerKey.WINTERTODT_COLD] = DAMAGE_INTERVAL
		
		if (state.active) {
			state.participants.forEach {
				if (it.tile().z >= MIN_Z_FOR_DAMAGE && world.rollDie(50, 1)) {
					it.hit(null, Math.max(1, world.random((it.skills().xpLevel(Skills.HITPOINTS) / 6) - 1) + 1), 0).block(false)
					it.message("The cold of the Wintertodt seeps into your bones.")
					
					// If the player isn't locked, we interrupt him.
					// if (it.locked().not()) {
					//     it.stopActions(false)
					// }
				}
			}
		}
	}
	
	fun processGame(world: World) {
		world.timers()[TimerKey.WINTERTODT_CYCLE] = CYCLE_INTERVAL
		
		// Ensure all players are valid players. Remove finished players, and players out of map.
		attempt {
			state.participants.removeAll { p ->
				var remove = false
				
				if (p.finished() || p.tile().region() != GAME_REGION_ID) {
					remove = true
					closeInterfaceFor(p)
				}
				
				remove
			}
		}
		
		// Randomly extinguish braziers
		if (state.active) {
			for (brazier in state.allBraziers) {
				brazier.updatePyromancer(world)
				
				if (brazier.getState() == BrazierState.LIT && world.rollDie((state.health + 1500) / 10, 10)) {
					// We use the spawned snowflakes to tell if it's breaking. We don't want to interrupt that.
					val snowFlakes = world.objById(26690, brazier.tile + Tile(1, 1))
					
					if (snowFlakes == null) {
						if (world.rollDie(if (state.health < MAX_HEALTH / 2) 2 else 3, 1)) { // One in two or one in three that the brazier actually explodes.
							breakBrazier(world, brazier)
						} else {
							world.tileGraphic(502, brazier.tile, 115, 0)
							brazier.changeState(BrazierState.STALE, world)
						}
					}
				}
			}
		}
		
		// Randomly spawn the strong magic attack on a random player.
		if (state.active && world.rollDie(25, 1)) {
			val combatants = world.players().entriesList
					.filterNotNull() // Strip all null entries
					.filter { it.tile().region() == GAME_REGION_ID && it.tile().z >= MIN_Z_FOR_DAMAGE } // Must be in zone!
					.toTypedArray()
			
			// Did any player match that?
			if (combatants.isNotEmpty()) {
				val victim = combatants.random()
				
				// Strike the magic.
				spawnMagicAttack(world, victim)
			}
		}
		
		// Chance to attack a mage
		if (state.active) {
			val list = state.allBraziers.toList()
			Collections.shuffle(list)
			
			for (brazier in list) {
				if (brazier.pyromancerAlive && brazier.pyromancer!!.hp() > 0
						&& brazier.pyromancer!!.sync().transmog() < 1 && world.rollDie(30, 1)) {
					attackMage(world, brazier.pyromancer!!)
					break // Only do one per attempt
				}
			}
		}
		
		// Process pyromancers
		attempt {
			if (state.active && state.health > 0 && state.ticksUntilPyromancers-- <= 0) {
				state.ticksUntilPyromancers = 5
				
				state.allBraziers.forEach { brazier ->
					val pyro = brazier.pyromancer
					
					if (pyro != null) {
						if (!brazier.pyromancerAlive && !pyro.sync().hasFlag(NpcSyncInfo.Flag.SHOUT.value)) { // Don't overwrite shouts
							pyro.sync().shout(arrayOf("Mummy!", "Ugh, help me!", "We are doomed.", "My flame burns low.", "I think I'm dying.").random())
						} else if (brazier.getState() == BrazierState.BROKEN) {
							pyro.sync().shout("Fix this brazier!")
						} else if (brazier.getState() == BrazierState.STALE) {
							pyro.sync().shout("Light this brazier!")
						} else if (pyro.world().rollDie(3, 1)) {
							pyro.sync().shout("Yemalo shi cardito!")
						}
						
						// Sometimes, when the weather is just right, you can see the pyromancer dance.
						if (brazier.pyromancerAlive && brazier.getState() == BrazierState.LIT && pyro.world().rollDie(2, 1)) {
							pyro.animate(4432)
						}
					}
				}
			}
		}
		
		if (state.active && state.health > 0) {
			// Compute the damage we do this cycle.
			var damage = 0
			state.allBraziers.forEach {
				if (it.doesDamage()) {
					damage += 5
					
					spawnFlameProjectile(world, it)
				}
			}
			
			// Deal damage or add HP
			if (damage == 0) {
				state.health += 5
				
				if (state.health > MAX_HEALTH) {
					state.health = MAX_HEALTH
				}
			} else {
				state.health -= damage
			}
			
			if (state.health <= 0) {
				state.health = 0
				world.timers()[TimerKey.WINTERTODT_GAME_START] = TICKS_BETWEEN_GAMES
				
				// Dead!!
				state.reinitialize(world)
				state.health = 0
				state.active = false
				
				// Turn all braziers stale and make the pyro's shout
				state.allBraziers.forEach {
					it.changeState(BrazierState.STALE, world)
					it.pyromancer?.sync()?.shout("We can rest for a time.")
					it.pyromancer?.hp(14, 0)
					it.pyromancer?.sync()?.transmog(7371)
					it.pyromancer?.animate(-1)
				}
				
				// Reward players.
				state.participants.forEach {
					it.invokeScript(1426, TICKS_BETWEEN_GAMES) // Show timer until it restarts
					rewardParticipant(it)
				}
			}
		}
		
		// Cycle through all players
		state.participants.forEach { p -> synchronizeFor(p) }
	}
	
	private fun spawnFlameProjectile(world: World, brazier: WintertodtBrazier) {
		val npc = Npc(7373, world, brazier.tile + brazier.projectileCenter)
		world.registerNpc(npc)
		npc.pathQueue().interpolate(1630, 4007)
		npc.executeScript @Suspendable {
			it.delay(8)
			npc.world().unregisterNpc(npc)
		}
		npc.lock()
	}
	
	fun updatePyromancers(world: World) {
		state.allBraziers.forEach { it.updatePyromancer(world) }
	}
	
	fun startGame(world: World) {
		state.reinitialize(world)
		
		updatePyromancers(world)
		updateSnowstorm(world)
	}
	
	fun onPlayerJoin(it: Script) {
		state.addParticipant(it.player)
		clearInventoryFor(it.player) // Make sure we don't carry any items into this game!
	}
	
	fun onPlayerLeave(it: Script) {
		state.removeParticipant(it.player)
		closeInterfaceFor(it.player)
		clearInventoryFor(it.player)
	}
	
	fun rewardParticipant(player: Player) {
		player.stopActions(false) // In case they have an open trade window, which would fuck us up.
		WintertodtGame.clearInventoryFor(player)
		
		val points = player.wintertodtPoints()
		player.clearattrib(AttributeKey.WINTERTODT_POINTS)
		
		if (points > 0) {
			player.skills().__addXp(Skills.FIREMAKING, (points * 100).toDouble())
			player.message("You have gained ${points * 100} Firemaking XP.")
		}
		
		if (points >= REQUIRED_POINTS) {
			// Increase counter and display
			player.modifyNumericalAttribute(AttributeKey.WINTERTODT_COUNT, 1, 0)
			player.modifyNumericalAttribute(AttributeKey.WINTERTODT_TOTAL, points, 0)
			
			// Did we beat our high?
			val high = player.attribOr<Int>(AttributeKey.WINTERTODT_HIGHSCORE, 0)
			if (points > high) {
				player.putattrib(AttributeKey.WINTERTODT_HIGHSCORE, points)
				player.message("You have a new high score: $high.")
			}
			
			val count = player.attrib<Int>(AttributeKey.WINTERTODT_COUNT)
			player.message("Your subdued Wintertodt count is: <col=ff0000>$count</col>.")
			
			// Give the player their crates
			val crates = Math.min(11, points / REQUIRED_POINTS)
			if (crates == 1) {
				player.message("You have gained a supply crate!")
			} else {
				player.message("You have gained $crates supply crates!")
			}
			
			// Imagine getting the pet.. Omg :-)
			if (player.world().rollDie(3500, crates)) {
				unlockPhoenix(player)
			}
			
			player.inventory().addOrDrop(Item(20703, if (doubleCrates) crates * 2 else crates), player)
		} else {
			player.message("You did not earn enough points to be worthy of a gift from the citizens of Kourend this time.")
		}
	}
	
	fun unlockPhoenix(player: Player) {
		
		// NOTE: Jagex fucked up the varbit, covers too many bits and therefore covers the Rocky varbit.
		if (!PetAI.hasUnlocked(player, Pet.PHOENIX)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(Pet.PHOENIX.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.PHOENIX, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(Pet.PHOENIX.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.PHOENIX.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
	fun attackMage(world: World, pyromancer: Npc) {
		val obj = MapObj(pyromancer.tile(), 26690, 10, 0)
		world.spawnObj(obj, false)
		world.executeScript @Suspendable {
			it.delay(4)
			world.tileGraphic(502, pyromancer.tile(), 90, 0)
			pyromancer.hit(null, 8)
			world.removeObj(obj, false)
		}
	}
	
	fun breakBrazier(world: World, brazier: WintertodtBrazier) {
		val base = brazier.tile
		
		world.executeScript @Suspendable {
			val spawned = mutableListOf<MapObj>()
			spawned += world.spawnObj(MapObj(base + Tile(1, 0), 26690, 10, 0), false)
			spawned += world.spawnObj(MapObj(base + Tile(0, 1), 26690, 10, 0), false)
			spawned += world.spawnObj(MapObj(base + Tile(1, 1), 26690, 10, 0), false)
			spawned += world.spawnObj(MapObj(base + Tile(2, 1), 26690, 10, 0), false)
			spawned += world.spawnObj(MapObj(base + Tile(1, 2), 26690, 10, 0), false)
			
			it.delay(4)
			
			for (obj in spawned) {
				world.removeObj(obj, false)
			}
			
			// There exists a small chance, that the game finished just as it's about to explode.
			// That would leave the brazier in a broken state, so we do this extra check here.
			if (state.active) {
				brazier.changeState(BrazierState.BROKEN, world)
				world.tileGraphic(502, brazier.tile, 90, 0)
				
				// Now damage all players in near proximity
				world.players().forEachInAreaKt(Area(base + Tile(1, 1), 2), { victim ->
					victim.message("The brazier is broken and schrapnel damages you.")
					victim.hit(null, world.random(6) + 5, 0).block(false)
				})
			}
		}
	}
	
	fun spawnMagicAttack(world: World, victim: Player) {
		// The center of the snow storm. We know for a fact this tile is reachable and walkable, as a player is on it.
		val base = victim.tile()
		
		world.executeScript @Suspendable {
			val spawned = mutableListOf<MapObj>()
			val splats = mutableListOf<Tile>()
			
			spawned += MapObj(base + Tile(0, 0), 26690, 10, 0)
			spawned += MapObj(base + Tile(1, 1), 26690, 10, 0)
			spawned += MapObj(base + Tile(-1, -1), 26690, 10, 0)
			spawned += MapObj(base + Tile(1, -1), 26690, 10, 0)
			spawned += MapObj(base + Tile(-1, 1), 26690, 10, 0)
			
			// Spawn those objects, making sure you can actually walk there. That's to avoid weird placement. It's OS-Scape, duh.
			for (obj in spawned) {
				if (world.routeExists(base, obj.tile()) && obj.tile().z >= MIN_Z_FOR_DAMAGE) {
					world.spawnObj(obj, false)
					splats += obj.tile()
				}
			}
			
			it.delay(4)
			
			for (splat in splats) {
				world.removeObj(MapObj(splat, 26690, 10, 0), false)
			}
			
			// Spawn the snowflakes and puddles, you know.
			spawned.clear()
			for (splat in splats) {
				spawned += world.spawnObj(MapObj(splat, if (splat == base) 29325 else 29324, 10, 0), false)
			}
			
			// Now damage all players in near proximity
			// The freezing cold attack of the Wintertodt's magic hits you.
			world.players().forEachInAreaKt(Area(base, 1), { victim ->
				victim.message("The freezing cold attack of the Wintertodt's magic hits you.")
				victim.hit(null, world.random(victim.skills().xpLevel(Skills.HITPOINTS)) / 2, 0).block(false)
			})
			
			it.delay(30)
			
			for (obj in spawned) {
				world.removeObj(obj, false)
			}
		}
	}
	
	fun synchronizeFor(player: Player) {
		// If the interface isn't visible yet, send it.
		if (!player.interfaces().visible(396)) {
			openInterfaceFor(player)
		}
		
		player.invokeScript(1421, player.wintertodtPoints(), state.health, if (state.brazierSW.pyromancerAlive) 1 else 0,
				if (state.brazierNW.pyromancerAlive) 1 else 0, if (state.brazierNE.pyromancerAlive) 1 else 0,
				if (state.brazierSE.pyromancerAlive) 1 else 0,
				state.brazierSW.getState().value, state.brazierNW.getState().value,
				state.brazierNE.getState().value, state.brazierSE.getState().value)
	}
	
	fun updateSnowstorm(world: World) {
		if (state.active) {
			world.spawnObj(MapObj(SNOW_STORM_TILE, HOWLING_STORM, 10, 0), false)
		} else {
			world.spawnObj(MapObj(SNOW_STORM_TILE, QUIESCENT_STORM, 10, 0), false)
		}
	}
	
	fun openInterfaceFor(player: Player) {
		player.interfaces().sendWidgetOn(396, Interfaces.InterSwitches.C)
	}
	
	fun closeInterfaceFor(player: Player) {
		player.interfaces().closeById(396)
	}
	
	fun allPyromancers(): List<Npc> {
		return state.allBraziers.map { b -> b.pyromancer!! }
	}
	
	fun Player.wintertodtPoints(): Int {
		return attribOr(AttributeKey.WINTERTODT_POINTS, 0)
	}
	
	fun Player.addWintertodtPoints(num: Int) {
		val oldPoints = wintertodtPoints()
		putattrib(AttributeKey.WINTERTODT_POINTS, oldPoints + num)
		
		// Did we just achieve the 500pt milestone?
		if (oldPoints < REQUIRED_POINTS && oldPoints + num >= REQUIRED_POINTS) {
			message("You have helped enough to earn a supply crate. Further work will go towards better rewards.")
		}
	}
	
	fun clearInventoryFor(player: Player) {
		for (item in CONFISCATED_ITEMS) {
			player.inventory().remove(Item(item, Int.MAX_VALUE), true)
		}
	}
	
}