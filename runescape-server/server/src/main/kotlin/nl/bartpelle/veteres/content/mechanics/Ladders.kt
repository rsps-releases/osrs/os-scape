package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/5/2015.
 */
object Ladders {
	
	@Suspendable fun ladderDown(s: Script, tile: Tile, animate: Boolean) {
		s.player().lockDamageOk()
		if (animate)
			s.player().animate(827)
		s.delay(1)
		s.player().teleport(tile)
		s.player().unlock()
	}
	
	@Suspendable fun ladderUp(s: Script, tile: Tile, animate: Boolean) {
		s.player().lockDamageOk()
		if (animate)
			s.player().animate(828)
		s.delay(1)
		s.player().teleport(tile)
		s.player().unlock()
	}
	
	@Suspendable fun delayedTeleport(s: Script, tile: Tile) {
		s.delay(1)
		s.player().teleport(tile)
	}
	
	@JvmStatic @ScriptMain fun ladder(repo: ScriptRepository) {
		//TODO determine if an object is a ladder rather than storing obj ids. Loop objects, check definition name?
		for (ladders in intArrayOf(14735, 14736, 14737, 12964, 12965, 12966, 16683, 16684, 16679, 11794, 11795, 11790, 11792, 11793, 11801, 11802, 16672, 16673, 25938, 25939, 26118, 26119, 2797, 2796, 17122, 2833, 16671, 24303, 2884)) {
			repo.onObject(ladders) @Suspendable {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				val animate: Boolean = !obj.definition(it.player().world()).name.toLowerCase().contains("staircase")
				if (obj.definition(it.player().world()).options[0].equals("Climb-up")) {
					
					var change: Int = 1
					if (obj.tile().equals(2715, 3472)) { // Camelot agility stairs goes up 2 levels!
						change = 2
					}
					var endtile = Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level + change)
					
					// TODO 2x2 stairs rotation matters as to where you come out in this generic handler.
					/*if (obj.id() == 16671 && obj.rot() == 0) { // Staircase, end up at the right place.
						endtile = obj.tile().transform(2, 0)
						it.waitForTile(it.player().stepTowards(endtile, 10)) // move to where the stairs actually are
						endtile = endtile.transform(0, 0, 1) // set the right height
					}*/
					Ladders.ladderUp(it, endtile, animate)
				} else if (obj.definition(it.player().world()).options[0].equals("Climb-down")) {
					var change: Int = 1
					if (obj.tile().equals(2715, 3472)) { // Camelot agility stairs goes down 2 levels!
						change = 2
					}
					Ladders.ladderDown(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level - change), animate)
				} else if (obj.definition(it.player().world()).options[0].equals("Climb")) {
					val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
					if (option == 1) {
						when (it.options("Climb-up", "Climb-down")) {
							1 -> {
								Ladders.ladderUp(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level + 1), animate)
							}
							2 -> {
								Ladders.ladderDown(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level - 1), animate)
							}
						}
					} else if (option == 2) {
						Ladders.ladderUp(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level + 1), animate)
					} else if (option == 3) {
						Ladders.ladderDown(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level - 1), animate)
					}
				}
			}
		}
	}
	
}