package nl.bartpelle.veteres.content.skills.farming

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-06-15.
 */

object Elstan {
	
	val ELSTAN = 2663
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ELSTAN) @Suspendable { initial_options(it) }
	}
	
	@Suspendable fun initial_options(it: Script) {
		when (it.options("Would you look after my crops for me?", "Can you give me any farming advice?",
				"Can you sell me something?", "I'll come back another time.")) {
			1 -> look_over_crops(it)
			2 -> farming_advice(it)
			3 -> can_you_sell_me_something(it)
			4 -> ill_come_back(it)
		}
	}
	
	@Suspendable fun northwestern_allotment(it: Script) {
		it.chatNpc("You don't have any seeds planted in that patch. Plant<br>some and I might agree to look after it for you.", ELSTAN, 589)
		
	}
	
	@Suspendable fun southeastern_allotment(it: Script) {
		it.chatNpc("You don't have any seeds planted in that patch. Plant<br>some and I might agree to look after it for you.", ELSTAN, 589)
	}
	
	@Suspendable fun look_over_crops(it: Script) {
		it.chatPlayer("Would you look after my crops for me?", 554)
		it.chatNpc("I might - which patch were you thinking of?", ELSTAN, 588)
		when (it.options("The northwestern allotment", "The southeastern allotment")) {
			1 -> northwestern_allotment(it)
			2 -> southeastern_allotment(it)
		}
		
	}
	
	@Suspendable fun farming_advice(it: Script) {
		it.chatPlayer("Can you give me any farming advice?", 554)
		it.chatNpc("Vegetables, hops and flowers need constant watering - if<br>you ignore my advice," +
				" you will sooner or later find<br>yourself in possession of a dead farming patch.", ELSTAN, 590)
		initial_options(it)
	}
	
	@Suspendable fun can_you_sell_me_something(it: Script) {
		it.chatPlayer("Can you sell me something?", 554)
		it.chatNpc("That depends on whether I have it to sell. What is it<br>that you're looking for?", ELSTAN, 589)
		purchase_options(it)
	}
	
	@Suspendable fun purchase_options(it: Script) {
		when (it.options("Some plant cure.", "A bucket of compost.", "A rake.", "(See more items)")) {
			1 -> plant_cure(it)
			2 -> bucket_of_compost(it)
			3 -> rake(it)
			4 -> more_options(it)
		}
	}
	
	@Suspendable fun more_options(it: Script) {
		when (it.options("A watering can.", "A gardening trowel.", "A seed dibber.", "(See previous items)", "Forget it.")) {
			1 -> watering_can(it)
			2 -> trowel(it)
			3 -> dibber(it)
			4 -> purchase_options(it)
			5 -> forget_it(it)
		}
	}
	
	@Suspendable fun forget_it(it: Script) {
		it.chatPlayer("Forget it, you don't have anything I need.", 610)
	}
	
	@Suspendable fun watering_can(it: Script) {
		it.chatPlayer("A watering can.", 588)
		it.chatNpc("A watering can, eh? I might have one spare ... tell you<br>what, I'll sell it to you for 25 coins if you like.", ELSTAN, 589)
		when (it.options("Yes, that sounds like a fair price.", "No thanks, I can get that much cheaper elsewhere.")) {
			1 -> {
				it.chatPlayer("Yes, that sounds like a fair price.", 567)
				if (!it.player().inventory().full()) {
					if (it.player().inventory().remove(Item(995, 25), false).success()) {
						it.player().inventory().add(Item(5531, 1), true)
					}
				} else {
					it.chatNpc("You don't have enough space to buy that.", ELSTAN, 610)
				}
			}
			2 -> no_thanks(it)
		}
	}
	
	@Suspendable fun dibber(it: Script) {
		it.chatPlayer("A seed dibber.", 588)
		it.chatNpc("A seed dibber, eh? I might have one spare ... tell you<br>what, I'll sell it to you for 15 coins if you like.", ELSTAN, 589)
		when (it.options("Yes, that sounds like a fair price.", "No thanks, I can get that much cheaper elsewhere.")) {
			1 -> {
				it.chatPlayer("Yes, that sounds like a fair price.", 567)
				if (!it.player().inventory().full()) {
					if (it.player().inventory().remove(Item(995, 15), false).success()) {
						it.player().inventory().add(Item(5343, 1), true)
					}
				} else {
					it.chatNpc("You don't have enough space to buy that.", ELSTAN, 610)
				}
			}
			2 -> no_thanks(it)
		}
	}
	
	@Suspendable fun trowel(it: Script) {
		it.chatPlayer("A gardening trowel.", 588)
		it.chatNpc("A gardening trowel, eh? I might have one spare ... tell<br>you what, I'll sell it to you for 15 coins if you like.", ELSTAN, 589)
		when (it.options("Yes, that sounds like a fair price.", "No thanks, I can get that much cheaper elsewhere.")) {
			1 -> {
				it.chatPlayer("Yes, that sounds like a fair price.", 567)
				if (!it.player().inventory().full()) {
					if (it.player().inventory().remove(Item(995, 15), false).success()) {
						it.player().inventory().add(Item(676, 1), true)
					}
				} else {
					it.chatNpc("You don't have enough space to buy that.", ELSTAN, 610)
				}
			}
			2 -> no_thanks(it)
		}
	}
	
	@Suspendable fun plant_cure(it: Script) {
		it.chatPlayer("Some plant cure.", 588)
		it.chatNpc("Plant cure, eh? I might have some put aside for myself.<br>Tell you what, I'll sell you some plant cure for 25<br>coins if you like.", ELSTAN, 590)
		when (it.options("Yes, that sounds like a fair price.", "No thanks, I can get that much cheaper elsewhere.")) {
			1 -> {
				it.chatPlayer("Yes, that sounds like a fair price.", 567)
				if (!it.player().inventory().full()) {
					if (it.player().inventory().remove(Item(995, 25), false).success()) {
						it.player().inventory().add(Item(6036, 1), true)
					}
				} else {
					it.chatNpc("You don't have enough space to buy that.", ELSTAN, 610)
				}
			}
			2 -> no_thanks(it)
		}
	}
	
	@Suspendable fun bucket_of_compost(it: Script) {
		it.chatPlayer("A bucket of compost.", 588)
		it.chatNpc("A bucket of compost, eh? I might have one spare ... tell<br>you what, I'll sell it to you for 35 coins if you like.", ELSTAN, 589)
		when (it.options("Yes, that sounds like a fair price.", "No thanks, I can get that much cheaper elsewhere.")) {
			1 -> {
				it.chatPlayer("Yes, that sounds like a fair price.", 567)
				if (!it.player().inventory().full()) {
					if (it.player().inventory().remove(Item(995, 35), false).success()) {
						it.player().inventory().add(Item(6032, 1), true)
					}
				} else {
					it.chatNpc("You don't have enough space to buy that.", ELSTAN, 610)
				}
			}
			2 -> no_thanks(it)
		}
	}
	
	@Suspendable fun rake(it: Script) {
		it.chatPlayer("A rake.", 588)
		it.chatNpc("A rake, eh? I might have one spare ... tell you what, I'll<br>sell it to you for 15 coins if you like.", ELSTAN, 589)
		when (it.options("Yes, that sounds like a fair price.", "No thanks, I can get that much cheaper elsewhere.")) {
			1 -> {
				it.chatPlayer("Yes, that sounds like a fair price.", 567)
				if (!it.player().inventory().full()) {
					if (it.player().inventory().remove(Item(995, 15), false).success()) {
						it.player().inventory().add(Item(952, 1), true)
					}
				} else {
					it.chatNpc("You don't have enough space to buy that.", ELSTAN, 610)
				}
			}
			2 -> no_thanks(it)
		}
	}
	
	@Suspendable fun no_thanks(it: Script) {
		it.chatPlayer("No thanks, I can get that much cheaper elsewhere.", 588)
	}
	
	@Suspendable fun ill_come_back(it: Script) {
		it.chatPlayer("I'll come back another time.", 588)
	}
}
