package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 19/04/2016.
 */
object Oyster {
	
	val OYSTER = 407
	val OYSTER_PERL = 411
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(OYSTER) @Suspendable {
			it.player().inventory().remove(Item(OYSTER), true)
			it.player().inventory().add(Item(OYSTER_PERL, 1), true)
			it.player().message("You open the oyster and find perls inside!")
		}
	}
}