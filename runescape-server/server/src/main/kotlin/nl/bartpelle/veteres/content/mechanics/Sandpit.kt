package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/18/2016.
 */

object Sandpit {
	
	val EMPTY_BUCKET = 3727
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnObject(10814, @Suspendable {
			var amt = it.player().inventory().count(EMPTY_BUCKET)
			
			while (amt-- > 0) {
				if (it.player().inventory().remove(Item(EMPTY_BUCKET, 1), false).success()) {
					it.player().inventory().add(Item(1783, 1), true)
					it.player().animate(895)
					it.player().message("You fill the bucket with sand.")
					it.delay(2)
				}
			}
		})
	}
}
