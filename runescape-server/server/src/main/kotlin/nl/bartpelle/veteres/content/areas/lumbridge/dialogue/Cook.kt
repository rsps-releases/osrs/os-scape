package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.quests.free.CooksAssistant
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 02/26/2015.
 */

object Cook {
	
	val COOK = 4626
	
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(COOK) @Suspendable {
			when (CooksAssistant.stage(it.player())) {
				0 -> regular_dialogue(it)
				1 -> quest_dialogue(it)
				2 -> postQuestDialogue(it)
			}
		}
	}
	
	@Suspendable fun regular_dialogue(it: Script) {
		it.chatNpc("What am I to do?", COOK, 610)
		when (it.options("What's wrong?", "Can you make a cake?", "You don't look very happy.", "Nice hat!")) {
			1 -> {
				it.chatPlayer("What's wrong?", 588)
				it.chatNpc("Oh dear, oh dear, oh dear, I'm in a terrible terrible<br>mess! It's the Duke's birthday today, and I should be<br>making him a lovely big birthday cake.", COOK, 598)
				it.chatNpc("I've forgotten to buy the ingredients. I'll never get<br>them in time now. He'll sack me! What will I do? I have<br>four children and a goat to look after. Would you help<br>me? Please?", COOK, 613)
				assist_cook(it)
			}
			2 -> {
				it.chatPlayer("You're a cook, why don't you bake me a cake?", 567)
				it.chatNpc("*sniff* Don't talk to me about cakes...", COOK, 610)
				it.chatPlayer("What's wrong?", 588)
				it.chatNpc("Oh dear, oh dear, oh dear, I'm in a terrible terrible<br>mess! It's the Duke's birthday today, and I should be<br>making him a lovely big birthday cake.", COOK, 598)
				it.chatNpc("I've forgotten to buy the ingredients. I'll never get<br>them in time now. He'll sack me! What will I do? I have<br>four children and a goat to look after. Would you help<br>me? Please?", COOK, 613)
			}
			3 -> {
				it.chatPlayer("You don't look very happy.", 588)
				it.chatNpc("No, I'm not. The world is caving in around me - I am<br>overcome by dark feelings of impending doom.", COOK, 611)
				when (it.options("What's wrong?", " I'd take the rest of the day off if I were you.")) {
					1 -> {
						it.chatPlayer("What's wrong?")
						it.chatNpc("Oh dear, oh dear, oh dear, I'm in a terrible terrible<br>mess! It's the Duke's birthday today, and I should be<br>making him a lovely big birthday cake.", COOK, 598)
						it.chatNpc("I've forgotten to buy the ingredients. I'll never get<br>them in time now. He'll sack me! What will I do? I have<br>four children and a goat to look after. Would you help<br>me? Please?", COOK, 613)
						assist_cook(it)
					}
					2 -> {
						it.chatPlayer("I'd take the rest of the day off if I were you.", 588)
						it.chatNpc("No, that's the worst thing I could do. I'd get in terrible<br>trouble.", COOK, 611)
						it.chatPlayer("Well maybe you need to take a holiday...", 588)
						it.chatNpc("That would be nice, but the Duke doesn't allow holidays<br>for core staff.", COOK, 611)
						it.chatPlayer("Hmm, why not run away to the sea and start a new<br>life as a Pirate?", 589)
						it.chatNpc("My wife gets sea sick, and I have an irrational fear of<br>eyepatches. I don't see it working myself.", COOK, 611)
						it.chatPlayer("I'm afraid I've run out of ideas.", 588)
						it.chatNpc("I know I'm doomed.", COOK, 610)
						it.chatPlayer("What's wrong?")
						it.chatNpc("Oh dear, oh dear, oh dear, I'm in a terrible terrible<br>mess! It's the Duke's birthday today, and I should be<br>making him a lovely big birthday cake.", COOK, 598)
						it.chatNpc("I've forgotten to buy the ingredients. I'll never get<br>them in time now. He'll sack me! What will I do? I have<br>four children and a goat to look after. Would you help<br>me? Please?", COOK, 613)
						assist_cook(it)
					}
				}
			}
			4 -> {
				it.chatPlayer("Nice hat!", 567)
				it.chatNpc("Err thank you. It's a pretty ordinary cooks hat really.", COOK, 610)
				it.chatPlayer("Still, suits you. The trousers are pretty special too.", 567)
				it.chatNpc("Its all standard cook's issue uniform...", COOK, 610)
				it.chatPlayer("The whole hat, apron, stripey trousers ensemble - it<br>works. It make you looks like a real cook.", 568)
				it.chatNpc("I am a real cook! I haven't got time to be chatting<br>about Culinary Fashion. I am in desperate need of help!", COOK, 615)
				it.chatPlayer("What's wrong?")
				it.chatNpc("Oh dear, oh dear, oh dear, I'm in a terrible terrible<br>mess! It's the Duke's birthday today, and I should be<br>making him a lovely big birthday cake.", COOK, 598)
				it.chatNpc("I've forgotten to buy the ingredients. I'll never get<br>them in time now. He'll sack me! What will I do? I have<br>four children and a goat to look after. Would you help<br>me? Please?", COOK, 613)
				assist_cook(it)
			}
		}
		
	}
	
	@Suspendable fun assist_cook(it: Script) {
		when (it.options("I'm always happy to help a cook in distress.", "I can't right now, maybe later.")) {
			1 -> {
				it.chatPlayer("I'm always happy to help a cook in distress.", 567)
				CooksAssistant.stage(it.player(), 1)
				it.chatNpc("Oh thank you, thank you. I need milk, an egg and<br>flour. I'd be very grateful if you can get them for me.", COOK, 568)
				it.chatPlayer("So where do I find these ingredients then?", 588)
				where_do_I_find_ingredients(it)
			}
			
			2 -> {
				it.chatPlayer("I can't right now, maybe later.")
				it.chatNpc("Fine. I always knew you Adventurer types were callous<br>beasts. Go on your merry way!", COOK, 611)
			}
		}
	}
	
	@Suspendable fun where_do_I_find_ingredients(it: Script) {
		when (it.options("Where do I find some flour?", "How about milk?", "And eggs? Where are they found?", "Actually, I know where to find this stuff.")) {
			1 -> flour(it)
			2 -> milk(it)
			3 -> egg(it)
			4 -> I_got_all_the_info_I_need(it)
		}
	}
	
	@Suspendable fun flour(it: Script) {
		it.chatNpc("There is a Mill fairly close, Go North and then West.<br>Mill Lane Mill is just off the road to Draynor. I<br>usually get my flour from there.", COOK, 569)
		it.chatNpc("Talk to Millie, she'll help, she's a lovely girl and a fine<br>Miller..", COOK, 568)
		where_do_I_find_ingredients(it)
	}
	
	@Suspendable fun milk(it: Script) {
		it.chatNpc("There is a cattle field on the other side of the river,<br>just across the road from the Groats' Farm.", COOK, 568)
		it.chatNpc("Talk to Gillie Groats, she looks after the Dairy cows -<br>she'll tell you everything you need to know about<br>milking cows!", COOK, 569)
		where_do_I_find_ingredients(it)
	}
	
	@Suspendable fun egg(it: Script) {
		it.chatNpc("I normally get my eggs from the Groats' farm, on the<br>other side of the river.", COOK, 568)
		it.chatNpc("But any chicken should lay eggs.", COOK, 567)
		where_do_I_find_ingredients(it)
	}
	
	@Suspendable fun I_got_all_the_info_I_need(it: Script) {
		it.chatPlayer("I've got all the information I need. Thanks.", 567)
		
	}
	
	@Suspendable fun quest_dialogue(it: Script) {
		it.chatNpc("How are you getting on with finding the ingredients?", COOK, 610)
		
		//Does our player have any items in his inventory that the cook needs?
		if (it.player().inventory().has(CooksAssistant.POT_OF_FLOUR) && !CooksAssistant.hasGiven(it, CooksAssistant.POT_OF_FLOUR)) {
			if (it.player().inventory().remove(Item(CooksAssistant.POT_OF_FLOUR), false).success()) {
				it.player().putattrib(AttributeKey.GIVEN_POT_OF_FLOUR, true)
				it.chatPlayer("Here's a pot of flour.")
			}
		}
		if (it.player().inventory().has(CooksAssistant.BUCKET_OF_MILK) && !CooksAssistant.hasGiven(it, CooksAssistant.BUCKET_OF_MILK)) {
			if (it.player().inventory().remove(Item(CooksAssistant.BUCKET_OF_MILK), false).success()) {
				it.player().putattrib(AttributeKey.GIVEN_BUCKET_OF_MILK, true)
				it.chatPlayer("Here's a bucket of milk.")
			}
		}
		if (it.player().inventory().has(CooksAssistant.EGG) && !CooksAssistant.hasGiven(it, CooksAssistant.EGG)) {
			if (it.player().inventory().remove(Item(CooksAssistant.EGG), false).success()) {
				it.player().putattrib(AttributeKey.GIVEN_EGG, true)
				it.chatPlayer("Here's a fresh egg.")
			}
		}
		
		//If our player has given all the items to the cook we..
		if (CooksAssistant.hasGiven(it, CooksAssistant.EGG) && CooksAssistant.hasGiven(it, CooksAssistant.BUCKET_OF_MILK) && CooksAssistant.hasGiven(it, CooksAssistant.POT_OF_FLOUR)) {
			it.chatNpc("You've brought me everything I need! I am saved!<br>Thank you!", COOK, 568)
			it.chatPlayer("So do I get to go to the Duke's Party?", 567)
			it.chatNpc("I'm afraid not, only the big cheeses get to dine with the<br>Duke.", COOK, 611)
			it.chatPlayer("Well, maybe one day I'll be important enough to sit on<br>the Duke's table.", 589)
			it.chatNpc("Maybe, but I won't be holding my breath.", COOK, 588)
			CooksAssistant.completeQuest(it.player())
			return
		}
		
		//If our player has given an item to the cook he says..
		if (CooksAssistant.hasGiven(it, CooksAssistant.EGG) || CooksAssistant.hasGiven(it, CooksAssistant.BUCKET_OF_MILK) || CooksAssistant.hasGiven(it, CooksAssistant.POT_OF_FLOUR)) {
			it.chatNpc("Thanks for ingredients you have got so far, please get<br>the rest quickly.  I'm running out of time! The Duke<br>will throw me into the streets!", COOK, 612)
		} else {
			it.chatPlayer("I haven't got any of them yet, I'm still looking.", 588)
			it.chatNpc("Please get the ingredients quickly.  I'm running out of<br>time! The Duke will throw me into the streets!", COOK, 611)
		}
		
		it.messagebox("You still need to get:<br>${items_left(it, CooksAssistant.BUCKET_OF_MILK) + items_left(it, CooksAssistant.POT_OF_FLOUR) + items_left(it, CooksAssistant.EGG)}")
		
		when (it.options("I'll get right on it.", "Can you remind me how to find these things again?")) {
			1 -> it.chatPlayer("I'll get right on it.", 567)
			2 -> where_do_I_find_ingredients(it)
		}
	}
	
	@Suspendable fun items_left(it: Script, ingredient: Int): String {
		if (!CooksAssistant.hasGiven(it, CooksAssistant.BUCKET_OF_MILK) && ingredient == CooksAssistant.BUCKET_OF_MILK) {
			return " A bucket of milk."
		}
		
		if (!CooksAssistant.hasGiven(it, CooksAssistant.POT_OF_FLOUR) && ingredient == CooksAssistant.POT_OF_FLOUR) {
			return " A pot of flour."
		}
		
		if (!CooksAssistant.hasGiven(it, CooksAssistant.EGG) && ingredient == CooksAssistant.EGG) {
			return " An egg."
		}
		
		return ""
	}
	
	@Suspendable fun postQuestDialogue(it: Script) {
		it.chatNpc("Hello friend, how is the adventuring going?", COOK, 567)
		when (it.options("I am getting strong and mighty.", "I keep on dying.", "Can I use your range?")) {
			1 -> strong_and_mighty(it)
			2 -> keep_on_dying(it)
			3 -> use_your_range(it)
		}
	}
	
	@Suspendable fun strong_and_mighty(it: Script) {
		it.chatPlayer("I am getting strong and mighty. Grrr", 567)
		it.chatNpc("Glad to hear it.", COOK, 567)
	}
	
	@Suspendable fun keep_on_dying(it: Script) {
		it.chatPlayer("I keep on dying.", 610)
		it.chatNpc("Ah well, at least you keep coming back to life!", COOK, 567)
	}
	
	@Suspendable fun use_your_range(it: Script) {
		it.chatPlayer("Can I use your range?", 588)
		it.chatNpc("Go ahead - it's a very good range. It's easier to use<br>than most other ranges.", COOK, 568)
		it.chatNpc("Its called the Cook-o-matic 100. It uses a combination of<br>state of the art temperature regulation and magic.", COOK, 568)
		it.chatPlayer("Will it mean my food will burn less often?", 588)
		it.chatNpc("Well, that's what the salesman told us anyway...", COOK, 567)
		it.chatPlayer("Thanks?", 588)
	}
}

