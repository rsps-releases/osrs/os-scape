package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.veteres.content.TileOffset
import nl.bartpelle.veteres.content.to

/**
 * Created by Jason MacKeigan on 2016-09-01 at 4:07 PM
 *
 * Represents the offset from the center of the map to which the door is located.
 */
enum class DoorPositionOffset(vararg val offsets: TileOffset) {
	EAST(14 to 1, 14 to 0),
	SOUTH(1 to -7, 0 to -7),
	WEST(-13 to 1, -13 to 0)
	;
	
	companion object {
		
		val ALL: List<TileOffset> = DoorPositionOffset.values().flatMap { it.offsets.asIterable() }
		
	}
}