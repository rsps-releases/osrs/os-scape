package nl.bartpelle.veteres.content.events.christmas.antisanta

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 12/22/2015.
 */

object SantaClaus {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		val SANTA_CLAUS = 6508
		r.onNpcOption1(SANTA_CLAUS) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 80) {
				it.chatPlayer("Hey Santa, how's it going?")
				it.chatNpc("Hohoho! Just making the final preparations before heading out to spread more festive cheer. Thanks again for all your help!", SANTA_CLAUS, 7183)
			} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 60 && it.player().varps().varbit(Varbit.XMAS2015_AUBURY_KID) == 0 &&
					it.player().varps().varbit(Varbit.XMAS2015_EAST_KID) == 0 &&
					it.player().varps().varbit(Varbit.XMAS2015_GENSTORE_KID) == 0 &&
					it.player().varps().varbit(Varbit.XMAS2015_RANGESTORE_KID) == 0) {
				
				it.chatPlayer("I've cheered up all of the children!")
				it.chatNpc("Hohoho! It actually worked! We are rid of that menace once and for all!", SANTA_CLAUS, 7183)
				it.chatPlayer("What will you do now?")
				it.chatNpc("I can finally leave the castle and carry out my festive duties. I believe you may have earned a spot on the nice list despite " +
						"everything you had to do...", SANTA_CLAUS, 7176)
				it.chatNpc("Please accept this reward as a personal thank you for saving Christmas!", SANTA_CLAUS, 7176)
				it.player().varps().varbit(Varbit.XMAS2015_STAGE, 80)
				it.player().inventory().addOrDrop(Item(13344), it.player()) // Inverted hat
				it.player().message("You've received a holiday item: <col=ff0000> Inverted santa hat</col>")
			} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 60) {
				it.chatPlayer("What am I supposed to be doing?")
				it.chatNpc("You need to cheer up all the children you upset. Delivering them presents " +
						"from the table over there should be enough to get them all feeling festive again.", SANTA_CLAUS, 7176)
			} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 50) {
				it.chatNpc("Welcome back, what have you discovered?", SANTA_CLAUS, 7176)
				it.chatPlayer("Let's start from the beginning...")
				it.chatPlayer("I found Anti-Santa on the roof of Varrock palace...")
				it.chatPlayer("...and then the frosty critters came to life, and now he's planning on using them to attack the castle!")
				it.chatNpc("Oh my, then it is as I feared... although... There may be something we can do.", SANTA_CLAUS, 7176)
				it.chatPlayer("What are you thinking?")
				it.chatNpc("I imagine the creatures are manifesting themselves through some sort of link to those children you upset.", SANTA_CLAUS, 7176)
				it.chatPlayer("Okay... and?")
				it.chatNpc("If you were to cheer up those same children, perhaps we could turn Anti-Santa's own army against him, he wouldn't stand a chance against all that Christmas cheer.", SANTA_CLAUS, 559)
				it.chatPlayer("It's worth a shot. How could I cheer up the children?")
				it.player().varps().varbit(Varbit.XMAS2015_STAGE, 60)
				
				it.chatNpc("You must restore the spirit of Christmas you took away from them. I have a selection of gifts over there, grab as many as you need and deliver them to the children you upset.", SANTA_CLAUS, 559)
				
				it.chatPlayer("Sure, I'll get to it right away!")
			} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 0) {
				it.chatNpc("Hohoho! Merry Christmas!", SANTA_CLAUS, 7176)
				it.chatPlayer("Happy Christmas Santa! What are you doing here?")
				it.chatNpc("It's too dangerous for me out there while Anti-Santa is on the loose. The White Knights' are keeping me here" +
						" until it's safe. At this rate, I'll miss Christmas altogether!", SANTA_CLAUS, 7176)
				when (it.options("Is there anything I can do to help?", "Bah! Humbug. I haven't got time for this.")) {
					1 -> {
						it.chatPlayer("Is there anything I can do to help?")
						it.chatNpc("Well, now you mention it... If we knew what Anti-Santa was up to, we could get a step ahead and put an end to" +
								" his holiday ruining shenanigans once and for all.", SANTA_CLAUS, 7176)
						when (it.options("Perhaps I could find out what he's planning?", "I really don't have time to get involved in this, goodbye.")) {
							1 -> {
								it.chatPlayer("Perhaps I could find out what he's planning?")
								it.chatNpc("That would certainly be helpful, but it won't be an easy task; Anti-Santa isn't going to just reveal his plans" +
										" to anybody, you'll have to earn his trust first. First, " +
										"however, you'll have to find him!", SANTA_CLAUS, 7176)
								it.chatPlayer("Any ideas on where I should start looking?")
								it.chatNpc("The White Knights' spies have narrowed down the cycle to somewhere within Varrock, you should try looking there.", SANTA_CLAUS, 7176)
								it.player().varps().varbit(Varbit.XMAS2015_STAGE, 10)
								it.chatPlayer("Okay, I'll report back once I know more.")
							}
							2 -> {
								it.chatPlayer("I really don't have time to get involved in this, goodbye.")
							}
						}
					}
					2 -> {
						it.chatPlayer("Bah! Humbug. I haven't got time for this.", 610)
					}
				}
				
			} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 10) {
				it.chatPlayer("What should I be doing again?", 588)
				it.chatNpc("You need to find Anti-Santa, he's located somewhere in<br>Varrock. Once you've found him, you'll need to earn<br>his trust and find out what he's up to.", SANTA_CLAUS, 7176)
				it.chatNpc("Good luck, ${it.player().name()}, we can't let Anti-Santa<br>destroy Christmas!", SANTA_CLAUS, 7176)
			} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 20) {
				it.chatPlayer("I found Anti-Santa. He wants me to fill a vial with children's tears.")
				it.chatNpc("Oh dear! There truly is no limit to his malignance! Unfortunately, I see no choice but for you to go through with it, we must learn more about his plans.", SANTA_CLAUS, 7176)
			}
		}
	}
	
}
