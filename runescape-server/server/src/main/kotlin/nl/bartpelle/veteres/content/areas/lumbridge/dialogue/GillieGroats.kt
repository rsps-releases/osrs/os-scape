package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/4/2015.
 */

object GillieGroats {
	
	val GILLIE_GROATS = 4628
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(GILLIE_GROATS) @Suspendable {
			it.chatPlayer("Hello, I'm Gillie the Milkmaid. What can I do for you?", 554)
			when (it.options("Who are you?", "Can you tell me how to milk a cow?", "I'm fine, thanks.")) {
				1 -> {
					it.chatPlayer("Who are you?", 567)
					it.chatNpc("My name's Gillie Groats. My father is a farmer and I<br>milk the cows for him.", GILLIE_GROATS, 568)
					it.chatPlayer("Do you have any buckets of milk spare?", 554)
					it.chatNpc("I'm afraid not. We need all of our milk to sell to<br>market, but you can milk the cow yourself if you need<br>milk.", GILLIE_GROATS, 569)
					it.chatPlayer("Thanks.", 554)
				}
				2 -> {
					it.chatPlayer("So how do you get milk from a cow then?", 554)
					it.chatNpc("It's very easy. First you need an empty bucket to hold<br>the milk.", GILLIE_GROATS, 568)
					it.chatNpc("Then find a dairy cow to milk - you can't milk just<br>any cow.", GILLIE_GROATS, 568)
					it.chatPlayer("How do I find a dairy cow?", 554)
					it.chatNpc("They are easy to spot - they are dark brown and<br>white, unlike beef cows, which are light brown and white.<br>We " +
							"also tether them to a post to stop them wandering<br>around all over the place.", GILLIE_GROATS, 570)
					it.chatNpc("There are a couple very near, in this field.", GILLIE_GROATS, 567)
					it.chatNpc("Then just milk the cow and your bucket will fill with<br>tasty, nutritious milk.", GILLIE_GROATS, 568)
				}
				3 -> {
					it.chatPlayer("I'm fine, thanks.", 567)
				}
			}
		}
	}
	
}
