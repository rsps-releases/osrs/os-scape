package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands

/**
 * Created by Situations on 1/7/2016.
 */

object Wizard {
	
	val WIZARD = 41594
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(WIZARD) @Suspendable {
			it.chatNpc("Good day to you, traveller. How can I assist you today?", WIZARD, 567)
			when (it.options("Who are you?", "Could you teleport me somewhere?")) {
				1 -> {
					it.chatPlayer("Who are you?")
					it.chatNpc("Well.. I'm a wizards! I'm here to assist an <br>adventurer such as yourself to travel around OS-Scape PVP. Are you interested in traveling anywhere, ${it.player().name()}?", WIZARD, 567)
					when (it.options("Yes please!", "I'd rather walk.", "What do you mean by.. traveling anywhere?")) {
						1 -> {
							it.chatPlayer("Yes please!", 590)
							it.chatNpc("Okay, where would you like to go?", WIZARD, 567)
							pvp_teleports(it)
						}
						2 -> {
							it.chatPlayer("I'd rather walk, thanks for the offer, though.", 571)
							it.chatNpc("Suit yourself!", WIZARD, 590)
						}
						3 -> {
							it.chatPlayer("What do you mean by.. traveling anywhere?")
							it.chatNpc("I can take you to various locations! Unfortunately, I didn't have enough time to learn all the teleportation spells.. but I did learn the ones", WIZARD, 590)
							it.chatNpc("that are most popular around OS-Scape PVP. So what do you say? Would you like me to take you somewhere?", WIZARD, 567)
							when (it.options("Yes please!", "I'd rather walk.")) {
								1 -> {
									it.chatPlayer("Yes please!")
									it.chatNpc("Okay, where would you like to go?", WIZARD, 567)
									pvp_teleports(it)
								}
								2 -> {
									it.chatPlayer("I'd rather walk, thanks for the offer, though.", 571)
									it.chatNpc("Suit yourself!", WIZARD, 590)
								}
							}
						}
					}
				}
				2 -> {
					it.chatPlayer("Could you teleport me somewhere?", 588)
					it.chatNpc("Absolutely! Where would you like to go?", WIZARD, 567)
					pvp_teleports(it)
				}
			}
		}
		
		r.onNpcOption2(WIZARD) @Suspendable { pvp_teleports(it) }
		r.onNpcOption3(WIZARD) @Suspendable { minigame_teleports(it) }
		r.onNpcOption4(WIZARD) @Suspendable { monster_teleports(it) }
	}
	
	@Suspendable fun pvp_teleports(it: Script) {
		when (it.options("Wilderness Teleports", "PvP Teleports", "Safe PVP Teleports")) {
			1 -> {
				when (it.options("Obelisks", "Dragons", "Magebank", "Demonic Ruins", "Graveyard")) {
					1 -> when (it.options("Lvl 35 Obelisk", "Lvl 44 Obelisk", "Lvl 50 Obelisk")) {
						1 -> teleport(it, 3097, 3799, 0)
						2 -> teleport(it, 2979, 3871, 0)
						3 -> teleport(it, 3307, 3916, 0)
					}
					2 -> when (it.options("East Dragons", "West Dragons")) {
						1 -> teleport(it, 3364, 3666, 0)
						2 -> teleport(it, 2983, 3598, 0)
					}
					3 -> teleport(it, 2538, 4716, 0)
					4 -> teleport(it, 3287, 3886, 0)
					5 -> teleport(it, 3160, 3676, 0)
				}
			}
			2 -> {
				when (it.options("Edgeville PVP")) {
					1 -> pvp_teleport(it)
				}
			}
			3 -> {
				when (it.options("Free For All")) {
					1 -> teleport(it, 3327, 4751, 0)
				}
			}
		}
	}
	
	@Suspendable fun minigame_teleports(it: Script) {
		when (it.options("Barrows", "Duel Arena")) {
			1 -> teleport(it, 3565, 3314, 0)
			2 -> teleport(it, 3367 + it.player().world().random(1), 3267 + it.player().world().random(1), 0)
		}
	}
	
	@Suspendable fun monster_teleports(it: Script) {
		when (it.options("Safe Monsters", "Unsafe Monsters")) {
			1 -> safe_bosses(it)
			2 -> unsafe_bosses(it)
		}
	}
	
	@Suspendable fun safe_bosses(it: Script) {
		when (it.options("Corporeal Beast", "Dagannoths", "Zulrah", "Lizardman Shaman", "More")) {
			1 -> teleport(it, 2951 + it.player().world().random(2), 4384 + it.player().world().random(2), 2)
			2 -> teleport(it, 1910, 4367, 0)
			3 -> teleport(it, 2196 + it.player().world().random(1), 3057 + it.player().world().random(1), 0)
			4 -> teleport(it, 1461, 3689, 0)
			5 ->
				when (it.options("Abyssal Demon", "Thermonuclear Smoke Devil", "Godwars", "Cave Krakens")) {
					1 -> teleport(it, 3424 + it.player().world().random(2), 3550 + it.player().world().random(2), 2)
					2 -> teleport(it, 3677, 5775, 0)
					3 -> teleport(it, 2882, 5310, 2)
					4 -> teleport(it, 2458, 9810, 0)
				}
		}
	}
	
	@Suspendable fun unsafe_bosses(it: Script) {
		when (it.options("Callisto", "Chaos Fanatic", "Venenatis", "Crazy Archaeologist", "More")) {
			1 -> pay_for_teleport(it, 50, 3295 + it.player().world().random(2), 3841 + it.player().world().random(2))
			2 -> pay_for_teleport(it, 50, 2984 + it.player().world().random(2), 3831 + it.player().world().random(2))
			3 -> pay_for_teleport(it, 50, 3345 + it.player().world().random(2), 3728 + it.player().world().random(2))
			4 -> pay_for_teleport(it, 50, 2968 + it.player().world().random(2), 3695 + it.player().world().random(2))
			5 -> {
				when (it.options("Scorpia", "Demonic Gorilla's")) {
					1 -> pay_for_teleport(it, 50, 3236 + it.player().world().random(2), 3946 + it.player().world().random(2))
					2 -> pay_for_teleport(it, 50, 3107 + it.player().world().random(2), 3675 + it.player().world().random(2))
				}
			}
		}
	}
	
	@Suspendable fun pay_for_teleport(it: Script, price: Int, x: Int, z: Int) {
		it.itemBox("Caution: This option costs 50 blood money and teleports you into a PVP area.", 13307, 50)
		if (it.options("Yes.", "No.") == 1) {
			if (GameCommands.pkTeleportOk(it.player(), x, z)) {
				if (it.player().inventory().remove(Item(13307, price), false).success()) {
					teleport(it, x, z, 0)
				} else {
					it.player().message("You need $price Blood money to use this teleport.")
				}
			}
		}
	}
	
	@Suspendable fun teleport(it: Script, x: Int, z: Int, level: Int) {
		if (GameCommands.pkTeleportOk(it.player(), x, z)) {
			it.targetNpc()!!.sync().faceEntity(it.player())
			it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
			it.targetNpc()!!.sync().animation(1818, 1)
			it.targetNpc()!!.sync().graphic(343, 100, 1)
			it.delay(1)
			it.player().graphic(1296)
			it.player().animate(3865)
			it.player().lockNoDamage()
			it.delay(3)
			it.player().teleport(x, z, level)
			it.animate(-1)
			it.player().unlock()
		}
	}
	
	@Suspendable fun pvp_teleport(it: Script) {
		it.targetNpc()!!.sync().faceEntity(it.player())
		it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
		it.targetNpc()!!.sync().animation(1818, 1)
		it.targetNpc()!!.sync().graphic(343, 100, 1)
		it.delay(1)
		it.player().graphic(1296)
		it.player().animate(3865)
		it.player().lockNoDamage()
		it.delay(3)
		it.player().teleport(PVPAreas.enter_tile_edge)
		it.player().write(UpdateStateCustom(47))
		it.player().putattrib(AttributeKey.UPDATE_STATE_CUSTOM, true)
		it.message("Talk to <col=FF0000>Tafani</col> in the bank to return to the main world. Rushing & PJing is <col=FF0000>breaking the rules</col> in this area.")
		it.animate(-1)
		it.player().unlock()
	}
}
