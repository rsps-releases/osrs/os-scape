package nl.bartpelle.veteres.content.treasuretrails.rewards

import nl.bartpelle.veteres.content.mechanics.ServerAnnouncements
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets.clueScrollReward
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 2016-11-18.
 */

object HardRewards {
	
	fun generateHardReward(player: Player, source: Int) {
		val commonRewardAmount = player.world().random(3..5)
		for (i in 1..commonRewardAmount) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = CommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			
			if (player.world().realm().isPVP) {
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, player.world().random(reward.amount)), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
			}
		}
		
		//Roll for good items..
		if (player.world().rollDie(2, 1)) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			if (player.world().realm().isPVP) {
				
				//Change coins to blood money
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from a hard clue scroll!")
		}
		
		//If we're on the PVP world, automatically give something from the uncommon table..
		if (player.world().realm().isPVP) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			//Change coins to blood money
			if (reward.reward.id() == 995) {
				player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from a hard clue scroll!")
		}
	}
	
	enum class CommonRewards(val probability: Double, val reward: Item, val amount: IntRange = 1..1) {
		BLOOD_MONEY(100.0, Item(13307), 15000..15000),
	}
	
	enum class UncommonRewards(val probability: Double, val reward: Item) {
		RUNE_FULL_HELM_T(100.0, Item(2627)),
		RUNE_PLATE_BODY_T(100.0, Item(2623)),
		RUNE_PLATE_LEGS_T(100.0, Item(2625)),
		RUNE_PLATE_SKIRT_T(100.0, Item(3477)),
		RUNE_KITE_SHIELD_T(100.0, Item(2629)),
		RUNE_FULL_HELM_G(100.0, Item(2619)),
		RUNE_PLATE_BODY_G(100.0, Item(2615)),
		RUNE_PLATE_LEGS_G(100.0, Item(2617)),
		RUNE_PLATE_SKIRT_G(100.0, Item(2621)),
		RUNE_KITE_SHIELD_G(100.0, Item(3476)),
		GUTHIX_FULL_HELM(90.0, Item(2673)),
		GUTHIX_PLATE_BODY(90.0, Item(2669)),
		GUTHIX_PLATE_LEGS(90.0, Item(2671)),
		GUTHIX_PLATE_SKIRT(90.0, Item(3480)),
		GUTHIX_KITE_SHIELD(90.0, Item(2675)),
		SARADOMIN_FULL_HELM(90.0, Item(2665)),
		SARADOMIN_PLATE_BODY(90.0, Item(2661)),
		SARADOMIN_PLATE_LEGS(90.0, Item(2663)),
		SARADOMIN_PLATE_SKIRT(90.0, Item(3479)),
		SARADOMIN_KITE_SHIELD(90.0, Item(2667)),
		ZAMORAK_FULL_HELM(90.0, Item(2657)),
		ZAMORAK_PLATE_BODY(90.0, Item(2653)),
		ZAMORAK_PLATE_LEGS(90.0, Item(2655)),
		ZAMORAK_PLATE_SKIRT(90.0, Item(3478)),
		ZAMORAK_KITE_SHIELD(90.0, Item(2659)),
		GUTHIX_DRAGONHIDE_COIF(80.0, Item(10382)),
		GUTHIX_DRAGONHIDE_TOP(80.0, Item(10378)),
		GUTHIX_DRAGONHIDE_CHAPS(80.0, Item(10380)),
		GUTHIX_DRAGONHIDE_BRACERS(80.0, Item(10376)),
		SARADOMIN_DRAGONHIDE_COIF(80.0, Item(10390)),
		SARADOMIN_DRAGONHIDE_TOP(80.0, Item(10386)),
		SARADOMIN_DRAGONHIDE_CHAPS(80.0, Item(10388)),
		SARADOMIN_DRAGONHIDE_BRACERS(80.0, Item(10384)),
		ZAMORAK_DRAGONHIDE_COIF(80.0, Item(10374)),
		ZAMORAK_DRAGONHIDE_TOP(80.0, Item(10370)),
		ZAMORAK_DRAGONHIDE_CHAPS(80.0, Item(10372)),
		ZAMORAK_DRAGONHIDE_BRACERS(80.0, Item(10368)),
		ARMADYL_DRAGONHIDE_COIF(80.0, Item(12512)),
		ANCIENT_FULL_HELM(80.0, Item(12466)),
		ANCIENT_PLATE_BODY(80.0, Item(12460)),
		ANCIENT_PLATE_LEGS(80.0, Item(12462)),
		ANCIENT_PLATE_SKIRT(80.0, Item(12464)),
		ANCIENT_KITE_SHIELD(80.0, Item(12468)),
		BANDOS_FULL_HELM(80.0, Item(12486)),
		BANDOS_PLATE_BODY(80.0, Item(12480)),
		BANDOS_PLATE_LEGS(80.0, Item(12482)),
		BANDOS_PLATE_SKIRT(80.0, Item(12484)),
		BANDOS_KITE_SHIELD(80.0, Item(12488)),
		ARMADYL_FULL_HELM(80.0, Item(12476)),
		ARMADYL_PLATE_BODY(80.0, Item(12470)),
		ARMADYL_PLATE_LEGS(80.0, Item(12472)),
		ARMADYL_PLATE_SKIRT(80.0, Item(12474)),
		ARMADYL_KITE_SHIELD(80.0, Item(12478)),
		RUNE_HERALDIC_HELM_H_ONE(70.0, Item(10286)),
		RUNE_HERALDIC_HELM_H_TWO(70.0, Item(10288)),
		RUNE_HERALDIC_HELM_H_THREE(70.0, Item(10290)),
		RUNE_HERALDIC_HELM_H_FOUR(70.0, Item(10292)),
		RUNE_HERALDIC_HELM_H_FIVE(70.0, Item(10294)),
		RUNE_HERALDIC_SHIELD_H_ONE(70.0, Item(7336)),
		RUNE_HERALDIC_SHIELD_H_TWO(70.0, Item(7342)),
		RUNE_HERALDIC_SHIELD_H_THREE(70.0, Item(7348)),
		RUNE_HERALDIC_SHIELD_H_FOUR(70.0, Item(7354)),
		RUNE_HERALDIC_SHIELD_H_FIVE(70.0, Item(7360)),
		ANCIENT_DRAGONHIDE_BOOTS(70.0, Item(19921)),
		BANDOS_DRAGONHIDE_BOOTS(70.0, Item(19924)),
		GUTHIX_DRAGONHIDE_BOOTS(70.0, Item(19927)),
		ARMADYL_DRAGONHIDE_BOOTS(70.0, Item(19930)),
		SARADOMIN_DRAGONHIDE_BOOTS(70.0, Item(19933)),
		ZAMORAK_DRAGONHIDE_BOOTS(70.0, Item(19936)),
		AMULET_OF_GLORY_T(65.0, Item(10362)),
		BLUE_DRAGON_HIDE_BODY_G(60.0, Item(7374)),
		BLUE_DRAGON_HIDE_CHAPS_G(60.0, Item(7382)),
		RED_DRAGON_HIDE_BODY_G(60.0, Item(12327)),
		RED_DRAGON_HIDE_CHAPS_G(60.0, Item(12329)),
		ENCHANTED_ROBE_TOP(50.0, Item(7399)),
		ENCHANTED_ROBE_BOT(50.0, Item(7398)),
		ENCHANTED_ROBE_HAT(50.0, Item(7400)),
		TAN_CAVALIER(40.0, Item(2639)),
		DARK_CAVALIER(40.0, Item(2641)),
		BLACK_CAVALIER(40.0, Item(2643)),
		WHITE_CAVALIER(40.0, Item(12321)),
		RED_CAVALIER(40.0, Item(12323)),
		NAVY_CAVALIER(40.0, Item(12325)),
		PIRATES_HAT(40.0, Item(2651)),
		GUTHIX_STOLE(35.0, Item(10472)),
		SARADOMIN_STOLE(35.0, Item(10470)),
		ZAMORAK_STOLE(35.0, Item(10474)),
		GUTHIX_CROZIER(35.0, Item(10440)),
		SARADOMIN_CROZIER(35.0, Item(10442)),
		ZAMORAK_CROZIER(35.0, Item(10444)),
		GREEN_DRAGON_MASK(30.0, Item(12518)),
		BLUE_DRAGON_MASK(30.0, Item(12520)),
		RED_DRAGON_MASK(30.0, Item(12522)),
		BLACK_DRAGON_MASK(30.0, Item(12524)),
		PITH_HELMET(25.0, Item(12516)),
		GILDED_FULL_HELM(20.0, Item(3486)),
		GILDED_PLATE_BODY(20.0, Item(3481)),
		GILDED_PLATE_LEGS(20.0, Item(3483)),
		GILDED_PLATE_SKIRT(20.0, Item(3485)),
		GILDED_KITE_SHIELD(20.0, Item(3488)),
		GILDED_MEDIUM_HELM(20.0, Item(20146)),
		GILDED_CHAIN_MAIL(20.0, Item(20149)),
		GILDED_SQUARE_SHIELD(20.0, Item(20152)),
		GILDED_TWO_HANDED(20.0, Item(20155)),
		GILDED_SPEAR(20.0, Item(20158)),
		GILDED_HASTA(20.0, Item(20161)),
		EXPLORER_BACKPACK(15.0, Item(12514)),
		RUNE_CANE(15.0, Item(12379)),
		ROBIN_HOOD_HAT(10.0, Item(2581)),
		ZOMBIE_HEAD(5.0, Item(19912)),
		CYCLOPS_HEAD(5.0, Item(19915)),
		NUNCHAKU(5.0, Item(19918)),
		THIRD_AGE_MELEE_HELM(1.0, Item(10350)),
		THIRD_AGE_MELEE_BODY(1.0, Item(10348)),
		THIRD_AGE_MELEE_LEGS(1.0, Item(10346)),
		THIRD_AGE_MELEE_KITE(1.0, Item(10352)),
		THIRD_AGE_RANGE_COIF(1.0, Item(10334)),
		THIRD_AGE_RANGE_BODY(1.0, Item(10330)),
		THIRD_AGE_RANGE_LEGS(1.0, Item(10332)),
		THIRD_AGE_RANGE_VAMBS(1.0, Item(10336)),
		THIRD_AGE_RANGE_HAT(1.0, Item(10342)),
		THIRD_AGE_RANGE_TOP(1.0, Item(10338)),
		THIRD_AGE_RANGE_SKIRT(1.0, Item(10340)),
		THIRD_AGE_RANGE_AMULET(1.0, Item(10344)),
	}
	
}
