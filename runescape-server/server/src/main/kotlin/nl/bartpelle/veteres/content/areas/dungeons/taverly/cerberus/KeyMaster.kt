package nl.bartpelle.veteres.content.areas.dungeons.taverly.cerberus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/7/2016.
 */

object KeyMaster {
	
	val KEY_MASTER = 5870
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(KEY_MASTER) @Suspendable {
			it.chatPlayer("Hello.", 567)
			it.chatNpc("Who goes there?", KEY_MASTER, 554)
			it.chatNpc("This is no place for a human. You need to leave.", KEY_MASTER, 554)
			it.chatPlayer("Why?", 554)
			it.chatNpc("The voices! The voices in my head!", KEY_MASTER, 614)
			it.chatPlayer("You're starting to scare me man...", 596)
			it.chatNpc("I am no man! They changed me and cursed me to<br>remain here...", KEY_MASTER, 615)
			options(it)
		}
	}
	
	@Suspendable fun options(it: Script) {
		when (it.options("What do you mean they changed you?", "What was the curse?", "Goodbye")) {
			1 -> what_do_you_mean(it)
			2 -> what_was_the_curse(it)
			3 -> goodbye(it)
		}
	}
	
	@Suspendable fun what_do_you_mean(it: Script) {
		it.chatNpc("I was once a free man, powerful and wealthy. I owned<br>several apothecaries across Zeah and sold the tastiest<br>potions in the land.", KEY_MASTER, 590)
		it.chatPlayer("What happened?", 554)
		it.chatNpc("One of my greatest inventions, it was going so well. I<br>spent days finding the right herbs, I travelled across all<br>of Zeah to find" +
				" the most exotic weeds. Once I had<br>gathered them all, I put them in a potion and mixed in", KEY_MASTER, 570)
		it.chatNpc("the final ingredient.", KEY_MASTER, 567)
		it.chatPlayer("Zeah? Interesting...", 588)
		it.chatNpc("Yes, yes... The potion tasted delicious but it was missing<br>a tiny something so I added Magic roots to the potion.<br>It started to pulsate and glow!" +
				" I took a sip and I felt<br>like a million gold! A few seconds later my eye sight", KEY_MASTER, 591)
		it.chatNpc("began to blur, my brain was throbbing. I fell and hit<br>my head on my worktop, then it all went black.", KEY_MASTER, 589)
		it.chatPlayer("Ouch! But, you still haven't said who changed you?", 554)
		it.chatNpc("Will you let me finish??", KEY_MASTER, 614)
		it.chatNpc("I woke up screaming in pain. Blue foam streaming out<br>of my mouth, my eye sight worse than before, the only<br>things I could make out were 3 tall figures with" +
				" green<br>banners - they were of the Arceuus Elders. They", KEY_MASTER, 591)
		it.chatNpc("muttered to each other in Archaic Language after<br>trying to get up several times. I lost hope and stared at<br>the sky. I had given up when the tallest of the figures<br>" +
				"bent over and brought his face right to mine and spoke", KEY_MASTER, 591)
		it.chatNpc("very softly 'We can save you, but it will come at a<br>cost'.", KEY_MASTER, 589)
		it.chatPlayer("So they saved you?", 554)
		it.chatNpc("Yes of course they saved me but look at the cost! I<br>have horns coming out of my head and I'm still blind<br>and stuck here... forever alone.", KEY_MASTER, 616)
		options(it)
	}
	
	@Suspendable fun what_was_the_curse(it: Script) {
		it.chatNpc("I have been charged to stay here to prevent the<br>Monstrosity from escaping. Those gates and the<br>winches that operate them are the only thing that stops<br>it breaking free.", KEY_MASTER, 591)
		it.chatPlayer("What monstrosity? What's behind those gates?", 554)
		it.chatNpc("You really do not want to know. Leave this place<br>human.", KEY_MASTER, 615)
		it.chatPlayer("Hey now, I'm no wimp. What's to stop me from just<br>turning the winch to open the gate and going in?", 615)
		it.chatNpc("Me. And of course your quick demise at the mercy of<br>Cerberus, guardian of the river of souls.", KEY_MASTER, 589)
		it.chatPlayer("But I can do it!", 614)
		it.chatNpc("You are obviously passionate at trying. But only those<br>with great skill at slaying these types of beast may<br>enter.", KEY_MASTER, 590)
		it.chatPlayer("I have been charged by the slayer masters to eliminate<br>this type of threat.", 589)
		it.chatNpc("Then you may pass... may your soul not end up<br>consumed and forever condemned to the-", KEY_MASTER, 589)
		it.chatNpc("THE VOICES! AAAHHHHH! Leave this place human!", KEY_MASTER, 614)
		options(it)
	}
	
	@Suspendable fun goodbye(it: Script) {
		it.chatPlayer("Goodbye.", 567)
	}
	
}
