package nl.bartpelle.veteres.content.items.skillcape;

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.script.ScriptRepository

/**
 *
 * Created by Jason MacKeigan on July 4th, 2016 at 11:45AM
 *
 * An enumeration of skill capes where the first element of the
 * vararg is the untrimmed cape, and the second is the trimmed
 * cape.
 */
enum class CapeOfCompletion(val untrimmed: Int, val trimmed: Int, val perk: CapeOfCompletionPerk) {
	ATTACK(9747, 9748, AttackCape),
	STRENGTH(9750, 9751, StrengthCape),
	DEFENCE(9753, 9754, DefenceCape),
	RANGED(9756, 9757, RangedCape),
	PRAYER(9759, 9760, PrayerCape),
	MAGIC(9762, 9763, MagicCape),
	RUNECRAFT(9765, 9766, RunecraftingCape),
	HITPOINT(9768, 9769, HitpointsCape),
	AGILITY(9771, 9772, AgilityCape),
	HERBLORE(9774, 9775, HerbloreCape),
	THIEVE(9777, 9778, ThievingCape),
	CRAFTING(9780, 9781, CraftingCape),
	FLETCHING(9783, 9784, FletchingCape),
	SLAYER(9786, 9787, SlayerCape),
	CONSTRUCTION(9789, 9790, ConstructionCape),
	MINING(9792, 9793, MiningCape),
	SMITHING(9795, 9796, SmithingCape),
	FISHING(9798, 9799, FishingCape),
	COOKING(9801, 9802, CookingCape),
	FIREMAKE(9804, 9805, FiremakingCape),
	WOODCUTTING(9807, 9808, WoodcuttingCape),
	FARMING(9810, 9811, FarmingCape);
	
	fun operating(player: Player): Boolean {
		val equipment = player.equipment()
		
		return equipment.hasAt(EquipSlot.CAPE, untrimmed) || equipment.hasAt(EquipSlot.CAPE, trimmed)
	}
	
	companion object {
		
		@JvmStatic @ScriptMain fun register(repository: ScriptRepository) {
			for (cape in CapeOfCompletion.values()) {
				val perk = cape.perk
				
				for (option in perk.options) {
					repository.onEquipmentOption(option, cape.trimmed, perk.option(option))
					repository.onEquipmentOption(option, cape.untrimmed, perk.option(option))
				}
			}
		}
		
		fun boost(skill: Int, player: Player) {
			if (player.skills().level(skill) < 99) {
				return
			}
			if (player.skills().level(skill) > 99) {
				player.message("Your current level is above the possible boosted level, please wait until it drops.")
				return
			}
			if (skill == 5) {
				if (System.currentTimeMillis() - player.attribOr<Long>(AttributeKey.PRAYERCAPE_BOOST, 0L) < 60000L) {
					player.message("You can only boost your Prayer once every minute.")
					return
				}
				player.putattrib(AttributeKey.PRAYERCAPE_BOOST, System.currentTimeMillis())
			}
			player.skills().setLevel(skill, 100)
			player.message("You have temporarily boosted your level to 100.")
		}
		
	}
}