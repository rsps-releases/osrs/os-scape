package nl.bartpelle.veteres.content.npcs.godwars.armadyl

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 6/11/2016
 */

object Kreearra {
	
	@JvmStatic val ENCAMPMENT = Area(2823, 5295, 2843, 5309)
	@JvmStatic var lastBossDamager: Entity? = null
	
	@JvmStatic fun isMinion(n: Npc): Boolean {
		return n.id() in arrayOf(3164, 3165, 3163)
	}
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target, 50) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10)) {
				if (EntityCombat.attackTimerReady(npc)) {
					val melee_range = EntityCombat.canAttackMelee(npc, target, false)
					// Attack the player
					val roll = npc.world().random(2)
					if (melee_range && roll == 0) {
						attackMelee(npc, target)
					} else if (roll == 1) {
						attackMage(it, npc, target)
					} else {
						attackRange(it, npc, target)
					}
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun attackMage(it: Script, npc: Npc, target: Entity) {
		npc.animate(6980)
		
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc.tile() + Tile(1, 1), target, 1200, 0, 0, 45, 12 * tileDist, 0, 1)
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) { // NOT a mistake, uses range def to compute hits!
			target.hit(npc, npc.world().random(21), 1).delay(delay).combatStyle(CombatStyle.RANGE)
		} else {
			target.hit(npc, 0, 1).delay(delay).combatStyle(CombatStyle.RANGE)
		}
	}
	
	@Suspendable fun attackRange(it: Script, npc: Npc, target: Entity) {
		npc.animate(6980)
		
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc.tile() + Tile(1, 1), target, 1199, 0, 0, 45, 12 * tileDist, 0, 1)
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
			target.hit(npc, npc.world().random(71), 1).delay(delay).combatStyle(CombatStyle.RANGE)
		} else {
			target.hit(npc, 0, 1).delay(delay).combatStyle(CombatStyle.RANGE)
		}
	}
	
	fun attackMelee(npc: Npc, target: Entity) {
		npc.animate(6981)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(npc, 0, 1).combatStyle(CombatStyle.MELEE)
		}
	}
}