package nl.bartpelle.veteres.content.minigames.raids

import nl.bartpelle.veteres.model.entity.Player
import java.util.*

/**
 * A utility class for any raid related content.
 * @author Mack
 */
object RaidsResources {

    private val RAID_PARTIES = HashMap<Int, RaidParty>()
    private val RAID_ADVERTISED_LISTINGS = mutableListOf<RaidParty>()

    fun raidParties(): HashMap<Int, RaidParty> {
        return RAID_PARTIES
    }

    fun raidPartyAdvertisedListings(): MutableList<RaidParty> {
        return RAID_ADVERTISED_LISTINGS
    }

    fun generateId(): Int {
        for (id in 0 until 512) {
            if (RaidsResources.RAID_PARTIES[id] == null) {
                return id
            }
        }

        return -1
    }

    @JvmStatic fun partyById(index: Int): RaidParty? {
        if (index == -1) return null
        if (RAID_PARTIES.containsKey(index))
            return RAID_PARTIES[index]
        return null
    }

    enum class RaidPartyStatus(private val status: Int) {
        DISBANDED(-1),
        LOBBY(0),
        DUNGEON(1);

        fun value(): Int {
            return status
        }
    }
}