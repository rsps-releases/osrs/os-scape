package nl.bartpelle.veteres.content.areas.catherby.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/9/2015.
 */

object Arhein {
	
	val ARHIEN = 1032
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ARHIEN) @Suspendable {
			it.chatNpc("Hi! Would you like to trade?", ARHIEN, 590)
			when (it.options("Yes.", "No thank you.", "Is that your shit?")) {
				1 -> {
					it.chatPlayer("Sure.")
					if (it.player().ironMode() != IronMode.NONE) {
						it.player().world().shop(26).display(it.player())
					} else {
						it.player().world().shop(25).display(it.player())
					}
				}
				2 -> {
					it.chatPlayer("No thanks.")
				}
				3 -> {
					it.chatPlayer("Is that your ship?")
					it.chatNpc("Yes, I use it to make deliveries to my customers up and down the coast. These crates here are all ready for my next trip.", ARHIEN, 590)
					when (it.options("Where do you deliver to?", "Are you rich then?")) {
						1 -> {
							it.chatPlayer("Where do you deliver to?")
							it.chatNpc("Oh, various places up and down the coast. Mostly Karamja and Port Sarim.", ARHIEN, 590)
							when (it.options("I don't suppose I could get a lift anywhere?", "Well, good luck with your business.")) {
								1 -> {
									it.chatPlayer("I don't suppose I could get a lift anywhere?")
									it.chatNpc("Sorry pal, but I'm afraid I'm not quite ready to sail yet.", ARHIEN, 590)
									it.chatNpc("I'm waiting on a big delivery of candles which I need to deliver futher along the coast.", ARHIEN, 590)
								}
								2 -> {
									it.chatPlayer("Well, good luck with your business!")
									it.chatNpc("Thanks buddy!", ARHIEN, 590)
								}
							}
						}
						2 -> {
							it.chatPlayer("Are you rich then?")
							it.chatNpc("Business is going reasonably well... I wouldn't say I was the richest of merchants every, but I'm doing fairly well all things considered.", ARHIEN, 590)
						}
					}
				}
			}
			
		}
		r.onNpcOption2(ARHIEN) @Suspendable {
			if (it.player().ironMode() != IronMode.NONE)
				it.player().world().shop(26).display(it.player())
			else
				it.player().world().shop(25).display(it.player())
			
		}
	}
}