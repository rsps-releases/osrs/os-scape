package nl.bartpelle.veteres.content.minigames.pest_control

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import java.util.*

/**
 * Created by Jason MacKeigan on 2016-07-12 at 4:10 PM
 *
 * A lander is a boat that contains an ever changing list of players that will eventually be put
 * into a new game or be given a higher priority for the next potential game. A lander defines
 * a unique process that is created on world init as it is intended to exist indefinitely.
 */
class Lander(val landerType: LanderType) {
	
	/**
	 * A flag that determines if the process is active or not. This
	 * should always be true however in the case it is not we can
	 * debug why.
	 */
	var processActive: Boolean = true
	
	/**
	 * The number of cycles that are required to pass before the lander
	 * ships off to the island.
	 */
	var waitCycles: Int = DEFAULT_WAIT_CYCLES
	
	/**
	 * The participating members that are active in the boat
	 */
	val members = ArrayList<Player>()
	
	/**
	 * Attempts to add a member to the lander, however if the member cannot
	 * be added, false will be returned.
	 */
	fun addMember(member: Player): Boolean {
		return members.add(member)
	}
	
	/**
	 * Effectively removes a member from the members list if the list contains that reference.
	 * However, if it does not, false is returned instead of true.
	 */
	fun removeMember(member: Player): Boolean {
		return members.remove(member)
	}
	
	/**
	 * Removes a group of members from the lander all at once.
	 */
	fun removeMembers(toRemove: Collection<Player>) {
		members.removeAll(toRemove)
	}
	
	/**
	 * Returns a list of 25 members at maximum and 5 players at minimum in the lander. The
	 * list is sorted by priority so any player that has waited longer than another is given
	 * a higher priority.
	 */
	fun prioritizeMembers(): List<Player> {
		if (members.size < MAXIMUM_MEMBERS) {
			return members
		}
		
		// Get the members that are in the correct region (fixes an issue where people somehow were away),
		// and sort by priority.
		val prioritized = members
				.filter { p -> p.tile().region() == 10537 }
				.sortedBy { it.attribOr<Int>(AttributeKey.PEST_CONTROL_LANDER_PRIORITY, 0) }
		
		return prioritized.subList(0, MAXIMUM_MEMBERS)
	}
	
	/**
	 * Increases the priority of each member in the lander boat.
	 */
	fun increasePriority() {
		members.forEach {
			it.message("You have been given priority level 1 over other players in joining the next game.")
			it.putattrib(AttributeKey.PEST_CONTROL_LANDER_PRIORITY, 1)
		}
	}
	
	/**
	 * The script that acts as the process or pulse of this lander. This
	 * script should always remain alive during the runtime of the program.
	 */
	val process: Function1<Script, Unit> = suspend@ @Suspendable {
		val world = it.ctx<World>()
		
		while (processActive) {
			if (members.size >= MINIMUM_MEMBERS) {
				if (waitCycles == 0) {
					if (!PestControl.shipOff(this, world)) {
						waitCycles = HALF_DEFAULT_WAIT_CYCLES
					} else {
						reset()
					}
				} else if (waitCycles > RESTART_CYCLES_IF_MINIMUM_MEMBERS) {
					waitCycles = RESTART_CYCLES_IF_MINIMUM_MEMBERS
				}
			}
			if (members.isEmpty()) {
				waitCycles = DEFAULT_WAIT_CYCLES
			} else {
				if (waitCycles == 0) {
					waitCycles = HALF_DEFAULT_WAIT_CYCLES
				}
				members.forEach { updateInterface(it) }
				waitCycles--
			}
			it.delay(1)
		}
	}
	
	/**
	 * A function that updates the interface for each player in the members list.
	 */
	fun updateInterface(member: Player) {
		val interfaces = member.interfaces()
		val timeUpdateNecessary = waitCycles % 50 == 0
		val remainingUpdateNecessary = waitCycles % 10 == 0
		
		if (timeUpdateNecessary) {
			interfaces.text(407, 12, "Next Departure: ${parseCycles(waitCycles)}")
		}
		
		if (remainingUpdateNecessary) {
			interfaces.text(407, 13, "Players Ready: ${members.size}")
			interfaces.text(407, 15, "Pest Points: ${member.attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0)}")
			interfaces.text(407, 16, landerType.name.toLowerCase().capitalize())
		}
	}
	
	/**
	 * Parses the cycles from either the game or the boat into a readable string of
	 * text that can be displayed on the interface.
	 */
	private fun parseCycles(cycles: Int): String {
		if (cycles == 0) {
			return "0 seconds"
		}
		val minutes: Int = cycles / 100
		
		val seconds: Int = ((cycles % 100) * .6).toInt()
		
		if (seconds == 0) {
			return "$minutes min"
		}
		if (minutes < 1) {
			return "$seconds seconds"
		}
		return "$minutes min $seconds seconds"
	}
	
	/**
	 * Effectively resets the timer of this lander boat to that
	 * of the default amount of time required to wait.
	 */
	private fun reset() {
		waitCycles = DEFAULT_WAIT_CYCLES
	}
	
	companion object {
		
		/**
		 * The number of cycles that the cycle is set to if the number
		 * of minimum members is met.
		 */
		val RESTART_CYCLES_IF_MINIMUM_MEMBERS = 50
		
		/**
		 * The default number of cycles the lander must wait to pass before
		 * it can ship off a group of players.
		 */
		val DEFAULT_WAIT_CYCLES = 500
		
		/**
		 * Half of the default amount specified.
		 */
		val HALF_DEFAULT_WAIT_CYCLES = DEFAULT_WAIT_CYCLES / 2
		
		/**
		 * The minimum number of members required before a game is started
		 * once the wait time hits zero.
		 */
		val MINIMUM_MEMBERS = 5
		
		/**
		 * The absolute maximum number of members allowed into this lander
		 */
		val MAXIMUM_MEMBERS = 25
		
	}
}