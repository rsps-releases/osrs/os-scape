package nl.bartpelle.veteres.content.areas.dwarvenpass

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/8/2015.
 */

object DwarvenPassway {
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		r.onObject(56) @Suspendable {
			
			val player = it.player()
			if (player.tile().equals(2661, 3500)) { // Staircase back up to taverly
				player.teleport(player.tile().transform(1, 0, 0))
			} else if (player.tile().equals(2662, 3500)) {
				player.teleport(player.tile().transform(-1, 0, 0))
			} else {
				it.player().teleport(Tile(2877, 3482))
			}
		}
		// Stairway down from taverly
		r.onObject(57) @Suspendable { it.player().teleport(Tile(2876, 9878)) }
		// Stairway down from camelot
		r.onObject(55) @Suspendable { it.player().teleport(Tile(2820, 9882)) }
		// Staircase back up to camelot
		r.onObject(54) @Suspendable { it.player().teleport(Tile(2820, 3486)) }
	}
	
}