package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 1/4/2018.
 */
object Vorkath {
	
	// Fireball: 1491
	
	data class VorkatState(var didPoison: Boolean)
	
	@JvmField
	val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		if (npc.state() == null) {
			npc.state(VorkatState(false))
		}
		
		val state = npc.state() as VorkatState
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.attackTimerReady(npc)) {
				EntityCombat.putCombatDelay(npc, 5)
				
				val attackType = npc.world().random(1..6)
				when (attackType) {
					1, 2, 3 -> { // Bomb attack
						npc.animate(7960)
						val targetTile = target.tile()
						val tileDist = npc.tile().distance(targetTile)
						npc.faceTile(targetTile)
						npc.world().spawnProjectile(npc.bounds().center(), targetTile, 1491, 30, 30, 20, 20 * tileDist + 20, 40, 25)
						
						npc.world().executeScript @Suspendable {
							it.delay(tileDist)
							
							it.ctx<World>().players().forEachInAreaKt(Area(targetTile, 1), { p ->
								p.hit(npc, npc.world().random(30) + 20, 0).combatStyle(CombatStyle.GENERIC)
								p.graphic(1466, 20, 0)
							})
						}
					}
					4 -> {
						npc.lockNoAttack()
						
						val zone = Area(target.tile(), 3)
						
						for (i in 1..25) {
							npc.animate(7957)
							val found = arrayOf(false)
							
							npc.world().pvpShuffablePid.forEachInAreaKt(zone, { p ->
								if (!found[0]) {
									npc.world().spawnProjectile(npc, p, 1482, 20, 20, 20, 20, 1, 1)
									npc.face(p)
									p.hit(npc, npc.world().random(15), 1).combatStyle(CombatStyle.GENERIC)
									
									found[0] = true
								}
							})
							
							// If we didn't hit, we just fire at the ground
							if (!found[0]) {
								val rt = zone.randomTile()
								npc.faceTile(rt)
								npc.world().spawnProjectile(npc.bounds().center(), rt, 1482, 20, 20, 20, 20, 1, 1)
								break // Stop firing since we have no targets
							}
							
							it.delay(1)
						}
						
						npc.animate(-1) // Cancel rapidfire animation
						npc.unlock()
					}
					5 -> {
						if (!state.didPoison) {
							state.didPoison = true
							npc.lock()
							
							npc.animate(7960)
							val npcBounds = npc.bounds()
							val poisonBounds = npc.bounds(6)
							val poisons = mutableListOf<MapObj>()
							val poisonTiles = mutableListOf<Tile>()
							
							// Spawn the splashes...
							for (x in poisonBounds.x1..poisonBounds.x2) {
								for (z in poisonBounds.z1..poisonBounds.z2) {
									if (npc.world().random(3) == 2) {
										val tile = Tile(x, z)
										
										// Avoid putting poison underneath the NPC. Looks stupid.
										if (tile !in npcBounds && npc.world().objByType(10, tile) == null) {
											val obj = MapObj(tile, 32000, 10, npc.world().random(3))
											npc.world().spawnObj(obj, false)
											
											poisons += obj
											poisonTiles += tile
											
											// Spawn projectile to that tile
											npc.world().spawnProjectile(npc.bounds().center(), tile, 1483, 30, 0, 20, 50, 30, 25)
										}
									}
								}
							}
							
							// Add world script to damage players on the splashes and remove later.
							npc.world().executeScript @Suspendable {
								it.delay(1)
								var ticks = 50 // Half a minute
								val world = it.ctx<World>()
								
								while (ticks-- > 0) {
									world.players().forEachInAreaKt(poisonBounds, { p ->
										if (p.tile() in poisonTiles) {
											val hit = world.random(8)
											p.hit(null, hit, 0, Hit.Type.POISON)
											npc.heal(hit)
										}
									})
									
									it.delay(1)
								}
								
								// Allow poison move again
								state.didPoison = false
								
								// Remove the poisons :)
								for (poison in poisons) {
									world.removeObj(poison, false)
								}
							}
							
							npc.unlock()
						}
					}
					5 -> { // Melee
						npc.animate(7951)
						target.hit(npc, npc.world().random(20)).combatStyle(CombatStyle.MELEE)
					}
				}
				
				EntityCombat.putCombatDelay(npc, 5)
				npc.face(target) // Go back to facing the target.
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@JvmField
	val hitScript: Function1<Script, Unit> = @Suspendable {
		if (it.npc().world().rollDie(10, 1)) {
			val dmger: Entity? = it.npc().attrib<Player>(AttributeKey.LAST_DAMAGER)
			
			if (dmger != null && dmger is Player && !dmger.finished() && dmger.tile().distance(it.npc().tile()) <= 20) {
				// Display a message if we're not skulled yet.
				if (!Skulling.skulled(dmger)) {
					dmger.message("Vorkath has skulled you!")
				}
				
				// Update skull timer.
				Skulling.assignSkullState(dmger)
			}
		}
	}
	
}

