package nl.bartpelle.veteres.content.minigames.duelingarena

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.DuelArenaScoreboard
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.interfaces.SpellSelect
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.mechanics.Poison
import nl.bartpelle.veteres.content.mechanics.Prayers
import nl.bartpelle.veteres.content.mechanics.Transmogrify
import nl.bartpelle.veteres.content.mechanics.Venom
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.*
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.services.logging.LoggingService
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.QuestTab
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.lang.ref.WeakReference
import java.util.*

/**
 * Created by Jak on 08/02/2016.
 *
 * Note: when advancing between two interfaces, you must modify the hardcoded check in #Interfaces.send(a, b, c, d)
 * That check will STOP the onInterClose hook being called and close the stake rather than just move onto the next stage.
 */
object Staking {
	
	@JvmStatic val logger: Logger = LogManager.getLogger(Staking::class.java)
	
	// A list of tiles where fight with No Movement on are taking place. Stops fights spawning on top of each other
	@JvmStatic val OCCUPIED_TILES: ArrayList<Tile> = ArrayList(12)
	
	// In any of the 6 challenge areas.
	@JvmStatic fun inChallengeArea(l: Tile): Boolean {
		return l.inArea(3318, 3247, 3327, 3247) || l.inArea(3324, 3247, 3328, 3264) || l.inArea(3327, 3262, 3342, 3270) ||
				l.inArea(3342, 3262, 3387, 3280) || l.inArea(3387, 3262, 3394, 3271) || l.inArea(3312, 3224, 3325, 3247) ||
				l.inArea(3326, 3200, 3398, 3267)
	}
	
	// Checks if the armour type we're trying to wear has been disabled.
	@JvmStatic fun equipment_disabled(player: Player, equipmentSlot: Int): Boolean {
		for (i in 0..DuelOptions.SLOTS_TO_VARP_MAP.size - 1) {
			if (DuelOptions.SLOTS_TO_VARP_MAP[i] == equipmentSlot) {
				player.debug("slot=%d  index=%d  slot-varp=%d   real-varp=%d  varp-value=%d", equipmentSlot, i, DuelOptions.SLOTS_TO_VARP_MAP[i], DuelOptions.EQUIPMENT_VARBITS[i],
						player.varps().varbit(DuelOptions.EQUIPMENT_VARBITS[i]))
				if (player.varps().varbit(DuelOptions.EQUIPMENT_VARBITS[i]) == 1) {
					return true
				}
			}
		}
		return false
	}
	
	// If we've previously invited this player.
	fun invited(inviter: Player, invited: Player): Boolean {
		if (inviter.timers().has(TimerKey.DUEL_INVITATION_OPEN)) {
			val partnerId: Int = inviter.attribOr(AttributeKey.REQUESTED_DUEL_PARTNER, -1)
			return partnerId == invited.id()
		}
		
		return false
	}
	
	
	// Obtain the ItemContainer with our stake.
	fun Player.duelOffer(): ItemContainer {
		val offer = attribOr<ItemContainer>(AttributeKey.DUEL_OFFER, null)
		if (offer != null)
			return offer
		
		val container = ItemContainer(world(), 28, ItemContainer.Type.REGULAR)
		putattrib(AttributeKey.DUEL_OFFER, container)
		return container
	}
	
	// Container which shows the items you lost from a stake. Disgarded when closed.
	fun Player.lostItems(): ItemContainer {
		val offer = attribOr<ItemContainer>(AttributeKey.DUEL_LOSS_CONTAINER, null)
		if (offer != null)
			return offer
		
		val container = ItemContainer(world(), 28, ItemContainer.Type.REGULAR)
		putattrib(AttributeKey.DUEL_LOSS_CONTAINER, container)
		return container
	}
	
	// Container holding winnings from opponent's staked items. Given to winner on interface close.
	fun Player.duelWinnings(): ItemContainer {
		val offer = attribOr<ItemContainer>(AttributeKey.DUEL_WINNINGS, null)
		if (offer != null)
			return offer
		
		val container = ItemContainer(world(), 28, ItemContainer.Type.REGULAR)
		putattrib(AttributeKey.DUEL_WINNINGS, container)
		return container
	}
	
	// Locate the instance of our duel partner.
	@JvmStatic fun Player.duelPartner(): Player? {
		return world().playerForId(attribOr<Int>(AttributeKey.DUEL_PARTNER, -1)).orElse(null)
	}
	
	// Finds our partner... if we have one
	@JvmStatic fun partnerFor(player: Player): Player? {
		return player.world().playerForId(player.attribOr<Int>(AttributeKey.DUEL_PARTNER, -1)).orElse(null)
	}
	
	
	// Accept button on either screens.
	@JvmStatic fun acceptDuel(player: Player) {
		val partner = player.duelPartner() ?: return
		
		if ((player.interfaces().visible(482) && !partner.interfaces().visible(482)) ||
				(!player.interfaces().visible(482) && partner.interfaces().visible(482))) {
			return
		} else if ((player.interfaces().visible(481) && !partner.interfaces().visible(481)) ||
				(!player.interfaces().visible(481) && partner.interfaces().visible(481))) {
			return
		} else if ((player.interfaces().visible(476) && !partner.interfaces().visible(476)) ||
				(!player.interfaces().visible(476) && partner.interfaces().visible(476))) {
			return
		}
		
		if (player.timers().has(TimerKey.DUEL_ACCEPT_DELAY)) {
			return
		}
		
		if (player.world().ticksUntilUpdate() > 0 && player.world().ticksUntilUpdate() < 1000) {
			player.message("You can't stake while an update is in progress.")
			return
		}
		
		if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.FUN_WEAPONS)) {
			if (doesnt_have_a_fun_weapon(player)) {
				player.message("<col=FF0000>You don't have a fun weapon to fight with!")
				return
			}
		}
		
		// Magic only
		if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED) &&
				DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MELEE) &&
				!DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC)) {
			if (DuelAntiScams.cantMageinMageOnly(player)) {
				player.message("You don't have many runes for this stake. Get some more.")
				return
			}
		}
		
		// Range only
		if (GameCommands.DUELARENA_ANTISCAM_RANGE_ENABLED && DuelAntiScams.rangeOnlyWithoutRangeGear(player)) {
			return
		}
		
		// Won't ever happen. Varp values are synced up.
		if (DuelAntiScams.different_settings(player, partner)) {
			logger.error("#0: DUEL ARENA: Settings mismatch when trying to accept options. Player {} at {}", player.name(), player.tile().toStringSimple())
			player.message("Your settings don't match!")
			return
		}
		
		player.putattrib(AttributeKey.DUEL_ACCEPTED, true)
		
		val my_extra_option: Int = extra_of(player)
		val partner_extra_option: Int = extra_of(partner)
		
		if (partner.attribOr(AttributeKey.DUEL_ACCEPTED, false)) { // If the other person has accepted, now we have...
			if (player.interfaces().visible(482)) { // First screen
				// Staking.duel_confirm_screen(player)
				DuelOptions.advanceToSecondScreen(player)
			} else if (player.interfaces().visible(481)) { // First screen
				// Staking.duel_confirm_screen(player)
				DuelOptions.advanceToThirdScreen(player)
			} else if (player.interfaces().visible(476)) { // Must be on second screen
				// If extra stake type rules are met...
				if (extra_options_apply(player)) {
					// Extra option are not yet showing.
					if (my_extra_option == -2 && partner_extra_option == -2) {
						player.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, -1)
						partner.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, -1)
						//player.write(InterfaceText(106, 96, "Select your duel type!"))
						//partner.write(InterfaceText(106, 96, "Select your duel type!"))
						player.world().server().scriptExecutor().executeScript(player, DuelExtraSelection.script)
						partner.world().server().scriptExecutor().executeScript(partner, DuelExtraSelection.script)
					} else if (my_extra_option > 0 && partner_extra_option > 0) {
						if (my_extra_option != partner_extra_option) {
							player.interfaces().closeById(476)
							player.message("You didn't choose the same duel type!")
							partner.message("Your partner didn't choose the same duel type!")
							return
						}
						
						// Begin the stake!
						try {
							if (DuelSpecialStakes.extra_duel_crit_not_met(player, partner, my_extra_option)) {
								player.interfaces().closeById(476)
								return
							}
							
							start_duel(player, partner)
						} catch (e: Exception) {
							player.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
							partner.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
							logger.error("#1: Exception starting duel! Player {} at {}", player.name(), player.tile().toStringSimple(), e)
						}
					}
				} else {
					
					// Suspicious stake will trigger a warning before starting.
					if (!DuelAntiScams.suspicious_stake(player)) {
						
						// Begin the stake!
						try {
							start_duel(player, partner)
						} catch (e: Exception) {
							player.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
							partner.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
							logger.error("#2: Exception starting stake! Player {} at {}", player.name(), player.tile().toStringSimple(), e)
						}
					}
				}
			}
		} else if (player.interfaces().visible(482)) { // Screen 1
			player.write(InterfaceText(482, 107, "Waiting for other player..."))
			partner.write(InterfaceText(482, 107, "<col=ff0000>Other player has accepted."))
		} else if (player.interfaces().visible(476)) { // Screen 3
			player.write(InterfaceText(476, 51, "Waiting for other player..."))
			partner.write(InterfaceText(476, 51, "Other player has accepted."))
		} else if (player.interfaces().visible(481)) { // Screen 2
			// Partner hasn't chosen yet but we have.
			if (my_extra_option > 0 && partner_extra_option == -1) {
				// We've chosen our extra option type.
				player.write(InterfaceText(481, 74, "Waiting for other player..."))
				partner.write(InterfaceText(481, 74, "Other player has selected!"))
			} else {
				// A normal stake
				player.write(InterfaceText(481, 74, "Waiting for other player..."))
				partner.write(InterfaceText(481, 74, "Other player has accepted..."))
			}
		}
	}
	
	// Cancel a duel before it starts. NEVER during/after a stake. Ties (after a stake) refunds a different way.
	@JvmStatic fun Player.abortDuel() {
		// Stake has already been closed. Maybe we started the duel! Interface close hook would still call this lol.
		if (attribOr(AttributeKey.IN_STAKE, false)) {
			message("You can't abort mid-stake!")
			return
		}
		
		val partnerId: Int = attribOr(AttributeKey.DUEL_PARTNER, -1)
		if (partnerId == -1) {
			//message("The stake has already been declined.")
			// No partner. Items are returned. Valid.
			return
		}
		val partner = duelPartner()
		if (partner == null) {
			//message("You're not in a stake to decline.")
			return
		}
		val partnersPartner: Player? = partner.duelPartner()
		
		// Give back the staked items.
		returnStakedItems(this)
		returnStakedItems(partner)
		// At this point only, where we are in no shape or form in need of a partner, can we remove this attribute.
		clearattrib(AttributeKey.DUEL_PARTNER)
		partner.clearattrib(AttributeKey.DUEL_PARTNER)
		
		// Don't abort a duel if we're not viewing one. This abort is NOT used after a duel ends.
		if (partner.interfaces().visible(482) || partner.interfaces().visible(481)) { // Avoid double messages.
			// Notify players
			partner.message("Other player declined the duel.")
			message("You decline the duel.")
		}
		
		// Reset staking info
		partner.stopDuel()
		stopDuel()
		
		interfaces().closeMain()
		interfaces().closeById(109)
		partner.interfaces().closeById(109)
		
		if (interfaces().visible(162, 550)) { // Close open chatbox which had anti-scam options on
			interfaces().close(162, 550)
		}
		
		// If we were on the 2nd stage (confirm duel options) then some tabs are closed like spellbook, prayer, inventory etc.
		// Only friends/cc is shown. We need to re-send.
		restoreHiddenTabs(this)
		
		// Close their screen. Items not returned (the above code) since we clear the attribute. We refund for them.
		partner.interfaces().closeMain()
		if (partner.interfaces().visible(162, 550)) { // Close open chatbox which had anti-scam options on
			partner.interfaces().close(162, 550)
		}
		
		restoreHiddenTabs(partner)
		
		stopActions(false)
		partner.stopActions(false)
		
		// Extra extra security, should never happen after previous patch, but what the hell.
		if (partnersPartner != null) {
			if (partnersPartner != this) {
				val fourthParticipant = partnersPartner.duelPartner()?.name() ?: "??"
				logger.error("#11 : partner-mismatch : [{}, {}, {}, {}]", this.name(), partner.name(), partnersPartner.name(), fourthParticipant)
			}
			partnersPartner.interfaces().closeMain()
		}
	}
	
	// Interface Ids which are hidden when you're setting up a stake.
	val CLOSED_CHILDREN = intArrayOf(593, 320, 399, 149, 387, 271, 218, 261, 216)
	// Show all of the children when you start the stake (aka restore the ones you previously made hidden)
	val ALL_CHILDREN = intArrayOf(593, 320, 399, 149, 387, 271, 218, 429, 432, 182, 261, 216, 239)
	
	// All tabs excluding social (friends, clanchat) are hidden during stake setup. This shows them again.
	private fun restoreHiddenTabs(duelee: Player) {
		for (i in ALL_CHILDREN)
			duelee.interfaces().sendWidgetOn(i, Interfaces.InterSwitches.forWidget(i))

		duelee.interfaces().setInterfaceSettings()
	}
	
	// For one player only.
	fun Player.stopDuel() {
		write(DestoryItemContainer(134)) // Destory old containers.
		write(DestoryItemContainer(32902))
		varps().varp(Varp.STAKE_SETTINGS, 0) // Reset the stake settings varp.
	}
	
	// Show the second screen.
	fun duel_confirm_screen(player: Player) {
		// Security
		val partner = player.duelPartner() ?: return
		if (!player.interfaces().visible(482) || !partner.interfaces().visible(482)) {
			return
		}
		
		// Double check we have enough inventory space to hold both our staked items and disallowed equipped items.
		if (!has_room_for_armour_and_stake(player)) {
			player.message("You don't have enough inventory space for the stake and/or duel options.")
			partner.message("Other player does not have enough inventory space for the stake and/or duel options.")
			return
		}
		if (!has_room_for_armour_and_stake(partner)) {
			partner.message("You don't have enough inventory space for the stake and/or duel options.")
			player.message("Other player does not have enough inventory space for the stake and/or duel options.")
			return
		}
		
		val selected_duel_applies: Boolean = extra_options_apply(player)
		
		arrayOf(player, partner).forEach { staker ->
			
			val mypartner = staker.duelPartner() ?: return
			staker.putattrib(AttributeKey.DUEL_ACCEPTED, false)
			
			if (selected_duel_applies) {
				staker.message("<col=7F00FF>This duel qualifies for a selection type-duel! You can choose normal / dds+whip / dds+tent after accepting!")
			}
			
			// Bypass interfaces() internal visible and close hook. We're not Xing out, we're just progressing to next stage.
			staker.write(CloseInterface(staker.interfaces().activeRoot(), staker.interfaces().mainComponent()))
			staker.write(CloseInterface(staker.interfaces().activeRoot(), staker.interfaces().inventoryComponent()))
			staker.interfaces().sendMain(106) // Show 2nd screen
			
			staker.write(InterfaceText(106, 109, mypartner.name()))
			staker.write(InterfaceText(106, 107, "" + mypartner.skills().combatLevel()))
			
			
			// Coin amts
			staker.write(InterfaceText(106, 96, "")) // White text below worn items -- accepted or not
			
			staker.write(InterfaceText(106, 99, "Nothing! <br> joke one<br>3")) // Send nothing otherwise leave empty because the list of items is rendered by clientscripts
			staker.write(InterfaceText(106, 101, "item<br>item2<br>item3<br>item4")) // See above
			
			staker.write(InterfaceText(106, 102, "0gp")) // Our stake worth in GP
			staker.write(InterfaceText(106, 103, "0gp")) // Our stake worth in GP
			
			staker.write(InvokeScript(969, "0 gp", 6946918)) // Top ours
			staker.write(InvokeScript(969, "0 gp", 6946919)) // Bottom theirs
			
			// testing
			//player.interfaces().close(548, 19) // main - solid interface - rs sends but we don't cos it'd close the stake.
			staker.write(DestoryItemContainer(134))
			staker.write(DestoryItemContainer(32902))
			
			// second set of stuff testing - make tabs visible
			for (i in CLOSED_CHILDREN) {
				staker.interfaces().closeById(i)
			}
			// disable clicking some widget childs. Don't need to really bother with these.. they're not vis anyway
			staker.write(InterfaceSettings(548, 46, -1, -1, 0))
			staker.write(InterfaceSettings(548, 47, -1, -1, 0))
			staker.write(InterfaceSettings(548, 48, -1, -1, 0))
			staker.write(InterfaceSettings(548, 49, -1, -1, 0))
			staker.write(InterfaceSettings(548, 50, -1, -1, 0))
			staker.write(InterfaceSettings(548, 51, -1, -1, 0))
			staker.write(InterfaceSettings(548, 32, -1, -1, 0))
			staker.write(InterfaceSettings(548, 33, -1, -1, 0))
			
			staker.write(SetItems(134, -1, 64168, staker.duelOffer())) // Ours
			staker.write(SetItems(134, -2, 60937, mypartner.duelOffer())) // Opponents
			
			staker.write(InvokeScript(InvokeScript.OPEN_TAB, 8))
			//p.write(InvokeScript(InvokeScript.SETVARCS, -1, -1)) // idk but rs does it. Probs autotyper pause/play related!
			
			// Since the text on interfaces are reset when closed, we need to clear some stuff again.
			staker.updateWeaponInterface()
			if (staker.world().realm().isPVP) {
				QuestTab.clearList(staker)
			}
		}
	}
	
	// Do you have room in your inventory to hold equipment which will be taken off when the stake starts?
	fun has_room_for_armour_and_stake(player: Player): Boolean {
		var required: Int = 0
		// Check space for equipment
		for (i in 0..DuelOptions.SLOTS_TO_VARP_MAP.size - 1) {
			if (player.varps().varbit(DuelOptions.EQUIPMENT_VARBITS[i]) == 1) {
				val slot: Int = DuelOptions.SLOTS_TO_VARP_MAP[i]
				val worn: Item = player.equipment().get(slot) ?: continue
				if (!worn.definition(player.world()).stackable()) {
					// You'll need to take this off for the stake.
					required++
				} else if (player.inventory().count(worn.id()).toLong() + worn.amount().toLong() > Integer.MAX_VALUE) {
					// Wearing 2.1b arrows, and you have 2.1b in inventory. You can't unequip that!
					required++
				}
			}
		}
		// Wearing a 2h weapon with shield disabled, it'll need to be taken off.
		val weaponId = player.equipment()[EquipSlot.WEAPON]?.id() ?: -1
		if (equipment_disabled(player, EquipSlot.SHIELD) && weaponId > -1) {
			if (player.world().equipmentInfo().typeFor(weaponId) == 5)
				required++
		}
		// Check space for staked items if we win
		for (i in player.duelOffer().items()) {
			if (i != null) {
				if (i.definition(player.world()).stackable()) { // same as above really
					if (player.inventory().count(i.id()) + i.amount() > Integer.MAX_VALUE) {
						// goes back into our inventory. winnings drop on the ground.
						required++
					}
				} else {
					// Non stackable items need an inventory space to hold.
					required++
				}
			}
		}
		return player.inventory().freeSlots() >= required
	}
	
	// Start a duel.
	fun start_duel(player: Player, partner: Player) {
		// Security
		if (!player.interfaces().visible(476) || !partner.interfaces().visible(476)) {
			return
		}
		
		// Even tho RS doesn't do this at this stage, our server allows spawning which causes exploits filling inventory and stopping items being dequipped.
		// EXTRA SECURITY: Double check we have enough inventory space to hold both our staked items and disallowed equipped items.
		if (!has_room_for_armour_and_stake(player)) {
			player.message("You don't have enough inventory space for the stake and/or duel options.")
			partner.message("Other player does not have enough inventory space for the stake and/or duel options.")
			return
		}
		if (!has_room_for_armour_and_stake(partner)) {
			partner.message("You don't have enough inventory space for the stake and/or duel options.")
			player.message("Other player does not have enough inventory space for the stake and/or duel options.")
			return
		}
		
		val settings = player.varps().varp(Varp.STAKE_SETTINGS)
		
		// Won't ever happen. Always kept in sync.
		if (DuelAntiScams.different_settings(player, partner)) {
			logger.error("#4: DUEL ARENA: Settings mismatch when trying to start stake. Player {} at {}", player.name(), player.tile().toStringSimple())
			return
		}
		
		// Set in stake attrib.
		player.putattrib(AttributeKey.IN_STAKE, true)
		partner.putattrib(AttributeKey.IN_STAKE, true)
		
		player.stopActions(true)
		partner.stopActions(true)
		
		// Save previous settings.
		player.putattrib(AttributeKey.PREVIOUS_DUEL_SETTINGS, settings)
		partner.putattrib(AttributeKey.PREVIOUS_DUEL_SETTINGS, settings)
		
		// Make our opponent show on top of other people
		player.varps().varp(Varp.ATTACK_PRIORITY_PID, partner.index())
		partner.varps().varp(Varp.ATTACK_PRIORITY_PID, player.index())
		
		// Since stake attrib is set, closing these interfaces doesn't invoke giveback items (decline duel)
		player.interfaces().closeById(476)
		partner.interfaces().closeById(476)
		
		// Make sure we have full spec starting, full hp, not boosted hp.
		heal_player(player)
		heal_player(partner)
		player.message("Accepted stake and duel options.")
		partner.message("Accepted stake and duel options.")
		
		if (player.attribOr(AttributeKey.VENGEANCE_ACTIVE, false)) {
			player.clearattrib(AttributeKey.VENGEANCE_ACTIVE)
			player.message("Your Vengeance has been cleared.")
		}
		
		if (partner.attribOr(AttributeKey.VENGEANCE_ACTIVE, false)) {
			partner.clearattrib(AttributeKey.VENGEANCE_ACTIVE)
			partner.message("Your Vengeance has been cleared.")
		}
		player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, 0)
		partner.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, 0)
		
		var cur = System.currentTimeMillis()
		player.putattrib(AttributeKey.STAKE_START, cur)
		partner.putattrib(AttributeKey.STAKE_START, cur)
		
		val weaponId = player.equipment()[EquipSlot.WEAPON]?.id() ?: -1
		val oppWeaponId = partner.equipment()[EquipSlot.WEAPON]?.id() ?: -1
		val noShield: Boolean = equipment_disabled(player, EquipSlot.SHIELD)
		if (noShield) {
			if (weaponId > -1 && player.world().equipmentInfo().typeFor(weaponId) == 5) {
				Equipment.unequip(player, EquipSlot.WEAPON)
				player.message("Your 2h weapon has been unequipped.")
			}
			if (oppWeaponId > -1 && partner.world().equipmentInfo().typeFor(oppWeaponId) == 5) {
				Equipment.unequip(partner, EquipSlot.WEAPON)
				partner.message("Your 2h weapon has been unequipped.")
			}
		}
		
		if (Transmogrify.isTransmogrified(player)) {
			Transmogrify.hardReset(player)
		}
		
		// Take off equipped items that have been disallowed for this stake.
		remove_worn_items(arrayOf(player, partner))
		
		val noMove: Boolean = DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MOVEMENT)
		val obs: Boolean = DuelOptions.ruleToggledOn(player, DuelOptions.OBSTACLES)
		
		// world.random(2) // use random if you wan't to use the 3 arena there are, instead of just the one closest to the main arena.
		// So, we have a problem with people running over other peoples stake to make non-movement stakers misclick
		// Solution: put non-movement in their own arena!
		val arena_idx = if (noMove) 1 else 0
		val arena_tile = get_arena(obs, player.world(), arena_idx, noMove)
		
		player.teleport(arena_tile)
		
		if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_MOVEMENT)) {
			val parttile = get_tile_next_to_opponent(player)
			partner.teleport(parttile)
			OCCUPIED_TILES.add(arena_tile)
			OCCUPIED_TILES.add(parttile)
		} else {
			partner.teleport(get_arena(obs, player.world(), arena_idx, false))
		}
		
		// Start the timer to go '3, 2, 1, go!'
		player.timers().addOrSet(TimerKey.STAKE_COUNTDOWN, 11)
		partner.timers().addOrSet(TimerKey.STAKE_COUNTDOWN, 11)
		player.world().executeScript @Suspendable { s ->
			s.delay(2)
			if (Staking.in_duel(player)) {
				player.sync().shout("3")
				partner.sync().shout("3")
				s.delay(3)
				
				if (Staking.in_duel(player)) {
					player.sync().shout("2")
					partner.sync().shout("2")
					s.delay(3)
					
					if (Staking.in_duel(player)) {
						player.sync().shout("1")
						partner.sync().shout("1")
						s.delay(3)
						
						if (Staking.in_duel(player)) {
							player.sync().shout("Go!")
							partner.sync().shout("Go!")
						}
					}
				}
			}
		}
		
		player.write(PlayMusic(47))
		player.write(InterfaceText(239, 5, "Duel Arena"))
		partner.write(PlayMusic(47))
		partner.write(InterfaceText(239, 5, "Duel Arena"))
		
		restoreHiddenTabs(player)
		restoreHiddenTabs(partner)
		val settings2 = arrayOf(46, 47, 48, 49, 50, 51, 29, 30, 31, 32, 76, 77, 33, 34)
		arrayOf(player, partner).forEach { p ->
			settings2.forEach { id ->
				p.write(InterfaceSettings(548, id, -1, -1, 2))
			}
			p.write(InterfaceSettings(548, 76, 1, 4, 2))
			p.write(InterfaceSettings(548, 77, 1, 4, 2))
			
		}
		
		// Disable SOTD special
		player.timers().cancel(TimerKey.SOTD_DAMAGE_REDUCTION)
		partner.timers().cancel(TimerKey.SOTD_DAMAGE_REDUCTION)
		
		player.write(SetHintArrow(partner, 1))
		partner.write(SetHintArrow(player, 1))
	}
	
	// Finds a tile next to the player. used for no movement stakes.
	fun get_tile_next_to_opponent(player: Player): Tile {
		val possibilities = ArrayList<Tile>()
		// Define possibilities
		val right: Tile = player.tile().transform(1, 0, 0)
		val left: Tile = player.tile().transform(-1, 0, 0)
		val up: Tile = player.tile().transform(0, 1, 0)
		val down: Tile = player.tile().transform(0, -1, 0)
		val check_oc: Boolean = DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MOVEMENT)
		val meleeonly: Boolean = DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC) && DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED)
				&& !DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MELEE)
		
		/*player.debug("tile melee path legitmacy: R:%s, L:%s, U:%s, D:%s", player.touches(player.duelPartner(), right),
				player.touches(player.duelPartner(), left), player.touches(player.duelPartner(), up), player.touches(player.duelPartner(), down))

		player.debug("tile occupied legitmacy: R:%s, L:%s, U:%s, D:%s", Staking.OCCUPIED_TILES.contains(right), Staking.OCCUPIED_TILES.contains(left),
				Staking.OCCUPIED_TILES.contains(up), Staking.OCCUPIED_TILES.contains(down))*/
		
		
		// Check clipping masks at those spots
		if (player.world().clipAt(right) == 0
				&& (!meleeonly || (meleeonly && player.touches(player.duelPartner(), right)))
				&& (!check_oc || (check_oc && !OCCUPIED_TILES.contains(right)))) {
			possibilities.add(right)
		}
		if (player.world().clipAt(left) == 0
				&& (!meleeonly || (meleeonly && player.touches(player.duelPartner(), left)))
				&& (!check_oc || (check_oc && !OCCUPIED_TILES.contains(left)))) {
			possibilities.add(left)
		}
		if (player.world().clipAt(up) == 0
				&& (!meleeonly || (meleeonly && player.touches(player.duelPartner(), up)))
				&& (!check_oc || (check_oc && !OCCUPIED_TILES.contains(up)))) {
			possibilities.add(up)
		}
		if (player.world().clipAt(down) == 0
				&& (!meleeonly || (meleeonly && player.touches(player.duelPartner(), down)))
				&& (!check_oc || (check_oc && !OCCUPIED_TILES.contains(down)))) {
			possibilities.add(down)
		}
		if (possibilities.size == 0) { // Safety.
			// TODO will fail if all 4 are taken (arena might be PACKED) in this case, change initiator coord instead of
			// TODO looking for ones next too
			possibilities.add(right)
		}
		// Return a possibilitiy. Always one due to how you spawn in the arena (tile selection)
		return possibilities[player.world().random(possibilities.size - 1)]
	}
	
	// Area. Add the others if you wish.
	val obstacleAreas = arrayOf(Area(3370, 3250, 3385, 3254),
			Area(3337, 3230, 3353, 3234))
	val normalAreas = arrayOf(Area(3336, 3248, 3354, 3254),
			Area(3368, 3229, 3384, 3234))
	val lobby = Area(3355, 3267, 3379, 3279)
	
	// Gets a random tile in an arena.
	fun get_arena(obstacles: Boolean, world: World, arena_idx: Int, check_occupied: Boolean): Tile {
		if (obstacles) {
			return randomPos(obstacleAreas[arena_idx], world, check_occupied)
		} else {
			return randomPos(normalAreas[arena_idx], world, check_occupied)
		}
	}
	
	// Uses the tiles within the given arena area to select a random tile with available clipping.
	fun randomPos(arena: Area, world: World, check_occupied: Boolean): Tile {
		var randx = arena.x1() + world.random(arena.x2() - arena.x1())
		var randy = arena.z1() + world.random(arena.z2() - arena.z1())
		var target_tile = Tile(randx, randy)
		
		// System.out.println(target_tile)
		while (world.clipAt(target_tile) != 0 || (check_occupied && OCCUPIED_TILES.contains(target_tile))) {
			// Generate random coords until one is unclipped.
			randx = arena.x1() + world.random(arena.x2() - arena.x1())
			randy = arena.z1() + world.random(arena.z2() - arena.z1())
			target_tile = Tile(randx, randy)
		}
		
		return target_tile
	}
	
	// Make both stakers remove any items that go against the rules.
	fun remove_worn_items(opponents: Array<Player>) {
		opponents.forEach { player ->
			for (i in 0..DuelOptions.SLOTS_TO_VARP_MAP.size - 1) {
				if (player.varps().varbit(DuelOptions.EQUIPMENT_VARBITS[i]) == 1) {
					val equipslot: Int = DuelOptions.SLOTS_TO_VARP_MAP[i]
					val worn: Item = player.equipment().get(equipslot) ?: continue
					// Equipment unequip method depends on button interaction and hence is UNRELIABLE! Hardcoded unequipping with security is here instead.
					
					val res = player.inventory().add(worn, false) // Don't force it, allow failure
					
					// Any remaining we couldn't unequip we'll bank
					if (!res.success()) {
						val leftover: Int = res.requested() - res.completed()
						player.bank().add(Item(worn.id(), leftover), true)
						val iname: String = worn.definition(player.world()).name
						player.message("<col=FF0000>%d x %s was banked as you could not unequip it.", leftover, iname)
						logger.error("#12 {} {} could not unequip {} x {} - left-over:{} slot:{} inv-available: {} exploited at {}", player.name(), player.index(),
								res.requested(), iname, leftover, equipslot, player.inventory().freeSlots(), player.tile().toStringSimple())
					}
					
					// Force remove it for the stake
					player.equipment().remove(Item(worn.id(), res.requested()), true)
					
					player.sound(2238)
					// Remove auto-select
					if (equipslot == EquipSlot.WEAPON) {
						SpellSelect.reset(player, true, false)
					}
					//player.debug("Worn item in slot "+equipslot+" removed.")
				}
			}
		}
	}
	
	// Called as soon as Damage is processed and we hit 0hp. NOT when you're about to "respawn" out of the arena.
	// Done instantly so we can compare double deaths etc, in which case the stake should be a tie.
	@JvmStatic fun on_death(player: Player) {
		try {
			// Notes: pretty sure if p2 dies but p1 died first it's still a win. I remember on a stream -> p2 got teleported outside area, was shown winnings, then respawns outside and
			// winnings interface closes -- loot appears in inventory.
			// With PID enabled on RS, unless poison ticks at the same time (pure luck.. hard to test).. there is never a tie.
			// We do support ties though, as poison deaths are actaully frequent when tenting.
			
			// Teleport outside
			if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MOVEMENT)) {
				OCCUPIED_TILES.remove(player.tile())
			}
			if (!lobby.contains(player)) {
				player.teleport(randomPos(lobby, player.world(), false))
			}
			heal_player(player)
			player.clearattrib(AttributeKey.IN_STAKE)
			player.timers().addOrSet(TimerKey.POST_ARENA_DEATH_IMMUNITY, 10)
			player.write(SetHintArrow(null, 1))
			player.clearattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION)
			
			val partner = player.duelPartner() ?: return
			
			// They've already been removed from the arena! They would be the one that did the double death/winnings code logic.
			// They would also make the loser interface show for us.
			if (!partner.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
				return
			}
			partner.write(SetHintArrow(null, 1))
			
			// In case our partner is also dead, give them immunity from losing items for 10 seconds.
			partner.timers().addOrSet(TimerKey.POST_ARENA_DEATH_IMMUNITY, 10)
			
			// Double death!
			if (player.attribOr<Boolean>(AttributeKey.STAKING_DOUBLE_DEATH, false) || partner.attribOr<Boolean>(AttributeKey.STAKING_DOUBLE_DEATH, false)) {
				
				// Note: do NOT remove staking attribute from  DEAD PLAYER. They'll respawn at Home and LOSE ITEMS!
				// In this situation, we only teleport ourselves out, and also refund both parties.
				// When our partner executes death.kt -> they'll do this method, tele outside and heal, but not (dupe/duplicate) refund items/get winnings.
				player.message("The battle was a tie!")
				partner.message("The battle was a tie!")
				returnStakedItems(player)
				returnStakedItems(partner)
				player.clearattrib(AttributeKey.STAKING_DOUBLE_DEATH)
				partner.clearattrib(AttributeKey.STAKING_DOUBLE_DEATH)
				player.clearattrib(AttributeKey.ARENA_DEATH_TICK)
				partner.clearattrib(AttributeKey.ARENA_DEATH_TICK)
				player.clearattrib(AttributeKey.DUEL_PARTNER)
				partner.clearattrib(AttributeKey.DUEL_PARTNER)
				
				// do the "on death" code for an alive player that ended in a tie. Only if duel was a ddos, other double deaths mean exactly that - both died.
				// and the relevent methods are called.
				if (!partner.dead()) {
					heal_player(partner)
					partner.clearattrib(AttributeKey.IN_STAKE)
					if (DuelOptions.ruleToggledOnNotInArena(partner, DuelOptions.NO_MOVEMENT)) {
						OCCUPIED_TILES.remove(partner.tile())
					}
					if (!lobby.contains(partner)) {
						partner.teleport(randomPos(lobby, player.world(), false))
					}
				}
				return
			}
			
			// Record statistics.
			player.putattrib(AttributeKey.STAKES_LOST, player.attribOr<Int>(AttributeKey.STAKES_LOST, 0) + 1)
			player.message("You were defeated. You have now won %d duels and lost %d duels.", player.attribOr(AttributeKey.STAKES_WON, 0), player.attribOr(AttributeKey.STAKES_LOST, 0))
			
			partner.putattrib(AttributeKey.STAKES_WON, partner.attribOr<Int>(AttributeKey.STAKES_WON, 0) + 1)
			partner.message("You have won! You have now won %d duels and lost %d duels.", partner.attribOr(AttributeKey.STAKES_WON, 0), partner.attribOr(AttributeKey.STAKES_LOST, 0))
			
			// Put the result onto the scoreboard
			DuelArenaScoreboard.registerResult(partner, player)
			
			// Log the stake result
			player.world().server().service(LoggingService::class.java, true).ifPresent { log ->
				log.logStake(partner, player, partner.duelOffer(), player.duelOffer())
			}
			
			// Only heal our partner if they're NOT dead. If they're dead, they'll respawn outside anyway, healed, stake attrib removed by themselves
			if (!partner.dead()) {
				heal_player(partner)
				partner.clearattrib(AttributeKey.IN_STAKE)
			}
			// Force them out of the arena so that, if dead, the winnings dropped don't appear in the arena!
			if (DuelOptions.ruleToggledOnNotInArena(partner, DuelOptions.NO_MOVEMENT)) {
				OCCUPIED_TILES.remove(partner.tile())
			}
			if (!lobby.contains(partner)) {
				partner.teleport(randomPos(lobby, player.world(), false))
			}
			/**
			 * EVEN IF DEAD, WE RETURN THE STAKE. ON DEATH (2 TICKS LATER) THEY'LL APPEAR OUTSIDE WITH INTERFACE CLOSED (get winnings)
			 */
			// Give the winners stake back to them.
			returnStakedItems(partner)
			
			// TODO logging for stakes
			/*player.world().server().service(LoggingService::class.java, true).ifPresent { s ->
			s.logTrade(player, partner, player.duelOffer(), partner.duelOffer())
		}*/
			
			// partner is classed as IN A STAKE until the loot is recieved. this means, if they die OUTSIDE THE AREA (double death) they keep their items.
			partner.interfaces().sendMain(110) // Loot
			partner.write(InterfaceText(110, 15, "You have won!"))
			partner.write(InterfaceText(110, 23, player.name()))
			partner.write(InterfaceText(110, 22, "" + player.skills().combatLevel()))
			partner.write(InvokeScript(149, 7208993, 541, 6, 6, 0, -1, "", "", "", "", "")) // rs does it
			partner.write(InterfaceSettings(110, 33, 0, 35, 1024)) // rs does it
			partner.varps().varp(Varp.STAKE_SETTINGS, 0)
			// Inventory will also be sent this game tick when stake is given back to the winner - making the container dirty.
			partner.write(SetItems(541, -1, 63761, player.duelOffer())) // Winnings
			
			player.interfaces().sendMain(119)
			player.write(InterfaceText(119, 14, "You lost!"))
			player.write(InterfaceText(119, 20, partner.name()))
			
			player.write(InterfaceText(119, 19, "" + partner.skills().combatLevel()))
			player.write(InvokeScript(149, 7798817, 541, 6, 6, 0, -1, "", "", "", "", "")) // rs does it
			player.write(InterfaceSettings(119, 33, 0, 35, 1024)) // rs does it
			player.varps().varp(Varp.STAKE_SETTINGS, 0)
			player.write(SetItems(541, -1, 63761, player.duelOffer())) // What we lost :(
			
			val winnings = ItemContainer(partner.world(), 28, ItemContainer.Type.REGULAR)
			winnings.items(player.duelOffer().copy())
			player.lostItems().items(player.duelOffer().copy())
			player.clearattrib(AttributeKey.DUEL_OFFER)
			// Duel offer (lost items) not removed until loser interface closed.
			partner.putattrib(AttributeKey.DUEL_WINNINGS, winnings)
			// Don't clear our duel offer, we'll use it to examine what we lost. When that closes, it can be cleared
			
			// Close chatbox options
			player.interfaces().closeById(219)
			partner.interfaces().closeById(219)
			
			player.clearattrib(AttributeKey.DUEL_PARTNER)
			partner.clearattrib(AttributeKey.DUEL_PARTNER)
		} catch (ex: Exception) {
			player.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
			logger.error("#7: Exception executing death in staking! Player {} at {}", player.name(), player.tile().toStringSimple(), ex)
		}
	}
	
	// Get our staked items back.
	fun returnStakedItems(partner: Player) {
		partner.dupeWatch().exclude()
		val used: Int = partner.duelOffer().occupiedSlots()
		if (used < 1) {
			return
		}
		try {
			for (item in partner.duelOffer().items()) {
				if (item == null)
					continue
				
				// Add to our inv
				val r = partner.inventory().add(item, false)
				
				// If your inventory is full when trying to claim your/opponent winnings. Would happen if armour was disabled, your inventory
				// spaces are used up to hold your gear.
				if (r.success())
					continue
				bank_or_drop_stake_item(partner, item, r, "staked items")
			}
			// Now clear our staked items offer. We're done with it.
			partner.duelOffer().empty()
		} catch (e: Exception) {
			logger.error("#8: DUEL ARENA: MAJOR ERR :: return item fail for Player {} at {}", partner.name(), partner.tile().toStringSimple(), e)
		}
	}
	
	private fun bank_or_drop_stake_item(partner: Player, item: Item, r: ItemContainer.Result, containerName: String) {
		// Bank the remaining ones
		val remaining = r.requested() - r.completed()
		val bankSlot = partner.bank().slotOf(item.id())
		
		if (bankSlot != -1 || partner.bank().freeSlots() > 0) {
			val dummy_container = ItemContainer(partner.world(), arrayOf(Item(item.id(), remaining)), ItemContainer.Type.FULL_STACKING)
			Bank.deposit(remaining, partner, null, Item(item.id(), remaining), dummy_container)
			val dummy_container_postcount = dummy_container.count(item.id())
			val added = remaining - dummy_container_postcount
			// Partial deposits don't work using the .deposit method. It's all or nothing.
			
			// All of the items in the dummy container were successfully banked.
			if (dummy_container_postcount == 0) {
				partner.message("You <col=FF0000>couldn't carry</col> your $containerName. %d x %s was banked.", added, item.name(partner.world()))
				partner.executeScript @Suspendable { it ->
					it.itemBox(String.format("You <col=FF0000>couldn't carry</col> your $containerName. %d x %s was banked.", added, item.name(partner.world())),
							item.id(), added)
				}
			} else {
				// Not ALL of the dummy container items were banked, maybe just some.
				val remainingToDrop = remaining - added
				val gitem = GroundItem(partner.world(), Item(item.id(), remainingToDrop), partner.tile(), partner.id())
				partner.world().spawnGroundItem(gitem).hidden().lifetime(GroundItem.UNTRADABLE_LIFETIME.toLong())
				partner.write(SetHintArrow(gitem.tile()))
				partner.putattrib(AttributeKey.LAST_DEATH_TILE, partner.tile())
				
				// None were banked.
				if (remainingToDrop == remaining) {
					partner.message("You <col=FF0000>couldn't carry</col> your $containerName. %d %s were <col=FF0000>dropped to the floor</col>.", gitem.item().amount(), gitem.item().name(partner.world()))
					partner.executeScript @Suspendable { it ->
						it.itemBox(String.format("You <col=FF0000>couldn't carry</col> your $containerName. %d %s were <col=FF0000>dropped to the floor</col>.", gitem.item().amount(), gitem.item().name(partner.world())),
								gitem.item().id(), gitem.item().amount())
					}
				} else {
					// Some were banked, drop the rest.
					partner.message("You <col=FF0000>couldn't carry</col> your $containerName. %d %s were <col=FF0000>banked</col> and %d were <col=FF0000>dropped</col> to the floor.", added, gitem.item().name(partner.world()), gitem.item().amount())
					partner.executeScript @Suspendable { it ->
						it.itemBox(String.format("You <col=FF0000>couldn't carry</col> your $containerName. %d %s were <col=FF0000>banked</col> and %d were <col=FF0000>dropped</col> to the floor.", added, gitem.item().name(partner.world()), gitem.item().amount()),
								gitem.item().id(), gitem.item().amount())
					}
				}
			}
		} else {
			// No room to try to bank stuff, drop it to the ground.
			val gitem = GroundItem(partner.world(), Item(item.id(), remaining), partner.tile(), partner.id())
			partner.world().spawnGroundItem(gitem).hidden().lifetime(GroundItem.UNTRADABLE_LIFETIME.toLong())
			partner.write(SetHintArrow(gitem.tile()))
			partner.putattrib(AttributeKey.LAST_DEATH_TILE, partner.tile())
			
			partner.message("You <col=FF0000>couldn't carry</col> your $containerName. %d %s were <col=FF0000>dropped to the floor</col>.", gitem.item().amount(), gitem.item().name(partner.world()))
			partner.executeScript @Suspendable { it ->
				it.itemBox(String.format("You <col=FF0000>couldn't carry</col> your $containerName. %d %s were <col=FF0000>dropped to the floor</col>.", gitem.item().amount(), gitem.item().name(partner.world())),
						gitem.item().id(), gitem.item().amount())
			}
		}
	}
	
	// Claim our opponents staked items.
	fun claim_winnings(player: Player) {
		player.dupeWatch().exclude()
		// NOTE: YOU CANNOT USE PARTNER HERE, AS THEY MAY BE NULL. RESULT OF USE = NULLPOINTER = LOSE WINNINGS!
		for (item in player.duelWinnings()) {
			if (item == null)
				continue
			// Add to inv
			val result = player.inventory().add(item, false)
			
			// Not enough space?
			if (result.success())
				continue
			bank_or_drop_stake_item(player, item, result, "winnings")
		}
		player.clearattrib(AttributeKey.DUEL_OFFER) //Bit of safety
		player.clearattrib(AttributeKey.DUEL_WINNINGS) // Bit of safety
	}
	
	// Send the inventory and stake items.
	fun Player.refreshMyStake() {
		val partner = duelPartner() ?: return
		write(SetItems(134, -1, 64168, duelOffer())) // Ours
		partner.write(SetItems(134, -2, 60937, duelOffer())) // Opponents
		write(SetItems(93, -1, 64209, inventory())) // Our inventory
		
		// Update the item container viewing options
		if (DuelOptions.ruleToggledOnNotInArena(this, DuelOptions.SHOW_INVENTORIES)) {
			DuelOptions.updatePartnersInventories(this)
		}
	}
	
	// Examine an item - using item id instead of a container, since duel uses multiple containers.
	@JvmStatic fun examine_message(player: Player, item: Item) {
		val pre: String = if (item.amount() >= 100000) "" + item.amount() + " x " else ""
		player.message(pre + player.world().examineRepository().item(item.id()))
	}
	
	// Register listeners to buttons.
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onTimer(TimerKey.POST_ARENA_DEATH_IMMUNITY) {
			it.player().timers().cancel(TimerKey.POST_ARENA_DEATH_IMMUNITY)
			it.player().clearattrib(AttributeKey.STAKING_DOUBLE_DEATH)
		}
		
		// Accept duel options
		repo.onButton(482, 103) {
			//Staking.advanceToSecondScreen(it.player())
			acceptDuel(it.player())
		}
		
		// Decline duel options
		repo.onButton(482, 104) {
			it.player().interfaces().closeById(482)
		}
		
		// Save duel options preset
		for (child in intArrayOf(110, 114)) {
			repo.onButton(482, child) {
				val preset = it.player().attribOr<Int>(AttributeKey.SAVED_DUEL_SETTINGS, 0)
				if (preset != it.player().varps().varp(Varp.STAKE_SETTINGS)) {
					it.player().putattrib(AttributeKey.SAVED_DUEL_SETTINGS, it.player().varps().varp(Varp.STAKE_SETTINGS))
					it.player().message("Stored preset settings overwritten.")
				} else {
					it.player().message("Your stored duel settings are identical to those you wish to save.")
				}
			}
		}
		
		// Load duel options preset
		for (child in intArrayOf(112, 115)) {
			repo.onButton(482, child) {
				val preset = it.player().attribOr<Int>(AttributeKey.SAVED_DUEL_SETTINGS, 0)
				if (it.player().varps().varp(Varp.STAKE_SETTINGS) == preset) {
					it.player().message("Preset duel settings are identical to those already selected.")
				} else {
					it.player().message("Preset duel settings loaded.")
					it.player().varps().varp(Varp.STAKE_SETTINGS, preset)
					DuelOptions.optionChangedWarning(it.player(), child)
				}
			}
		}
		
		// Load last duel options
		for (child in intArrayOf(111, 113)) {
			repo.onButton(482, child) {
				val last = it.player().attribOr<Int>(AttributeKey.PREVIOUS_DUEL_SETTINGS, 0)
				if (it.player().varps().varp(Varp.STAKE_SETTINGS) == last) {
					it.player().message("Previous duel settings are identical to those already selected.")
				} else {
					it.player().message("Previous duel settings loaded.")
					it.player().varps().varp(Varp.STAKE_SETTINGS, last)
					DuelOptions.optionChangedWarning(it.player(), child)
				}
			}
		}
		
		// Stake options - no food, pots etc
		for (CHILD_ID in 37..62) {
			repo.onButton(482, CHILD_ID) {
				DuelOptions.toggleOptions(it, CHILD_ID)
			}
		}
		
		// Armour allowed toggles.
		for (CHILD_ID in 69..79) {
			repo.onButton(482, CHILD_ID) {
				DuelOptions.toggleArmour(it, CHILD_ID)
			}
		}
		
		repo.onButton(482, 100) {
			it.player().interfaces().closeById(482)
		}
		
		repo.onButton(481, 71) {
			it.player().abortDuel()
		}
		
		repo.onInterfaceClose(482) {
			it.player().abortDuel()
		}
		
		repo.onInterfaceClose(481) {
			it.player().abortDuel()
		}
		
		// Offer buttons
		repo.onButton(109, 0) s@ {
			when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
				1 -> DuelStake.offerItem(it.player(), 1)
				2 -> DuelStake.offerItem(it.player(), 5)
				3 -> DuelStake.offerItem(it.player(), 10)
				4 -> DuelStake.offerItem(it.player(), Int.MAX_VALUE)
				5 -> DuelStake.offerItem(it.player(), it.inputInteger("Enter amount:"))
				10 -> {
					if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
					val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]] ?: return@s
					examine_message(it.player(), item)
				}
			}
		}
		
		// Withdraw
		repo.onButton(481, 18) s@ {
			when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
				1 -> DuelStake.takeItem(it.player(), 1)
				2 -> DuelStake.takeItem(it.player(), 5)
				3 -> DuelStake.takeItem(it.player(), 10)
				4 -> DuelStake.takeItem(it.player(), Int.MAX_VALUE)
				5 -> DuelStake.takeItem(it.player(), it.inputInteger("Enter amount:"))
				10 -> {
					if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
					val item = it.player().duelOffer()[it.player()[AttributeKey.BUTTON_SLOT]] ?: return@s
					examine_message(it.player(), item)
				}
			}
		}
		
		// Examine stake opponent
		repo.onButton(481, 26) s@ {
			val partner = it.player().duelPartner() ?: return@s
			
			if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
			val item = partner.duelOffer()[it.player()[AttributeKey.BUTTON_SLOT]] ?: return@s
			examine_message(it.player(), item)
		}
		
		// Winning examine
		repo.onButton(110, 33) s@ {
			// must use winnings. partner could be null here!
			if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
			val item = it.player().duelWinnings()[it.player()[AttributeKey.BUTTON_SLOT]] ?: return@s
			examine_message(it.player(), item)
		}
		
		// Our loss examine
		repo.onButton(119, 33) s@ {
			if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
			val item = it.player().lostItems()[it.player()[AttributeKey.BUTTON_SLOT]] ?: return@s
			examine_message(it.player(), item)
		}
		
		/**
		 * TIMER(S)
		 */
		repo.onTimer(TimerKey.DUEL_ACCEPT_DELAY) {
			// Enable the accept button once again.
			it.player().write(InterfaceVisibility(482, 4, true))
		}
		
		/**
		 * SECOND SCREEN
		 */
		repo.onInterfaceClose(476) {
			it.player().abortDuel()
			// Close 2nd screen
		}
		repo.onButton(481, 70) {
			// Accept 2nd screen
			acceptDuel(it.player())
		}
		repo.onButton(481, 95) {
			it.player().interfaces().closeById(481)
			// Decline duel
		}
		repo.onButton(476, 80) {
			it.player().abortDuel()
		}
		repo.onButton(476, 78) {
			// Accept 2nd screen
			acceptDuel(it.player())
		}
		repo.onButton(481, 7) {
			it.player().interfaces().closeById(481)
			// Close 2nd screen X button
		}
		
		/**
		 * FORFIET TRAPDOORS
		 */
		val forfeitExits = intArrayOf(3111, 3112, 3113, 3114, 3203)
		for (i in forfeitExits) {
			repo.onObject(i, s@ @Suspendable {
				val player = it.player()
				if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_FORFEIT)) {
					player.message("Forfeit is disabled for this duel.")
					return@s
				}
				val partner = player.duelPartner() ?: return@s
				when (it.optionsTitled("Are you sure you wish to forfeit?", "Yes", "No")) {
					1 -> {
						if (player.dead() || partner.dead() || !player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
							return@s
						}
						// We could be in the middle of counting down. Better stop.
						player.timers().cancel(TimerKey.STAKE_COUNTDOWN)
						partner.timers().cancel(TimerKey.STAKE_COUNTDOWN)
						player.hits.clear()
						partner.hits.clear()
						
						// Leave
						on_death(player)
					}
				}
			})
		}
		
		/**
		 * WINNINGS INTERFACE
		 */
		repo.onButton(110, 17) {
			// Claim
			claim_winnings(it.player())
			it.player().interfaces().closeById(110)
		}
		
		repo.onButton(110, 31) {
			// Close X button
			claim_winnings(it.player())
			it.player().interfaces().closeById(110)
		}
		
		repo.onInterfaceClose(110) {
			claim_winnings(it.player())
		}
		
		// Loser interface. Offer kept alive so you can examine items.
		repo.onInterfaceClose(119) {
			// Clear our offer. Can't do it on death otherwise there are no items to examine in the container.
			it.player().dupeWatch().exclude()
			it.player().lostItems().empty()
		}
		
		repo.onPlayerOption1 s@ {
			// Challenge option
			val player = it.player()
			val otherPlayer = player.attrib<WeakReference<Player>>(AttributeKey.TARGET).get() ?: return@s
			
			if (!player.attribOr<Boolean>(AttributeKey.CHALLENGE_OP, false)) {
				return@s
			}
			
			// Make sure you're not trading yourself.
			if (player == otherPlayer || player.id().equals(otherPlayer.id())) {
				return@s
			}
			
			if (otherPlayer.interfaces().activeMain() != -1) {
				player.message("That player is busy at the moment.")
				return@s
			}
			
			// Are we going to challenge a clan?
			if (player in ClanWars.LOBBY) {
				ClanWars.challenge(player, otherPlayer)
				return@s
			}
			
			if (GameCommands.STAKING_STAFF_ONLY && player.privilege() == Privilege.PLAYER) {
				player.message("The Duel Arena is currently in testing stages and cannot be used.")
				return@s
			}
			
			if (player.world().ticksUntilUpdate() > 0 && player.world().ticksUntilUpdate() < 1000) {
				player.message("You can't stake while an update is in progress.")
				return@s
			}
			if (player.gameTime() < 3000 && !player.privilege().eligibleTo(Privilege.ADMIN) && !otherPlayer.privilege().eligibleTo(Privilege.ADMIN)) {
				player.message("You cannot use the Duel Arena until you've played for 30 minutes.")
				return@s
			}
			if (otherPlayer.gameTime() < 3000 && !player.privilege().eligibleTo(Privilege.ADMIN) && !otherPlayer.privilege().eligibleTo(Privilege.ADMIN)) {
				player.message("This player only recently created their account and cannot duel for 30 minutes.")
				return@s
			}
			if (player.world().realm().isDeadman) {
				player.message("The Duel Arena cannot be used on Deadman worlds.")
				return@s
			}
			
			if (invited(otherPlayer, player)) {
				if (!player.dead() && !otherPlayer.dead() && !player.locked() && !otherPlayer.locked()) {
					player.stopActions(true)
					otherPlayer.stopActions(true)
					
					DuelOptions.initiateDuel(player, otherPlayer)
					DuelOptions.initiateDuel(otherPlayer, player)
				}
			} else {
				player.timers().extendOrRegister(TimerKey.DUEL_INVITATION_OPEN, 50)
				player.putattrib(AttributeKey.REQUESTED_DUEL_PARTNER, otherPlayer.id())
				otherPlayer.write(AddMessage("${player.name()} wishes to duel with you.", AddMessage.Type.DUEL, player.name()))
				player.message("Challenging ${otherPlayer.name()}...")
			}
		}
		
		repo.onTimer(TimerKey.PACKETLOGGING) {
			it.player().logged(false) // Disable logging. This acts as a time out, player has likely finished staking.
		}
	}
	
	// Check if the player to interact with is even in/out of the duel arena. Cannot interact if we're in and they're out.
	@JvmStatic fun not_in_area(entity: Entity, other: Entity, message: String): Boolean {
		if (entity.isPlayer && other.isPlayer) {
			// Establish if our target is even in a stake or not.
			val entityInStake: Boolean = entity.attribOr<Boolean>(AttributeKey.IN_STAKE, false)
			val targetInStake: Boolean = other.attribOr<Boolean>(AttributeKey.IN_STAKE, false)
			
			if ((entityInStake && !targetInStake) || (!entityInStake && targetInStake)) {
				if (message.length > 0) {
					entity.message(message)
				}
				return true
			}
			
			if (entityInStake && targetInStake) {
				if (other != (entity as Player).duelPartner()) {
					entity.message("They are not your opponent!")
					return true
				}
			}
		}
		return false
	}
	
	// If the stake hasn't started. If we're currently counting down.
	@JvmStatic fun stake_not_started(entity: Entity, other: Entity): Boolean {
		if (!entity.isPlayer || !other.isPlayer || !entity.attribOr<Boolean>(AttributeKey.IN_STAKE, false) || !other.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			return false
		}
		return entity.timers().has(TimerKey.STAKE_COUNTDOWN)
	}
	
	// It is actually impossible to logout of a stake unless forced by something like getting Banned.
	@JvmStatic fun on_logout(player: Player) {
		// Check we're in a stake before teleporting out lol.
		if (player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			// Forfeit.
			on_death(player)
		}
		
		// player.logout -> stopActions (closeInterface) -> closes stake sceen, items returned
		// If at this point we STILL have items in our staking inventory, something is very wrong. Give that shit back!
		val count: Int = player.duelOffer().occupiedSlots()
		if (count > 0) {
			returnStakedItems(player)
			logger.error("#6: Player {} still had {} items in staking inv on logout at {}", player.name(), count, player.tile().toStringSimple())
		}
	}
	
	// Heal up when entering / ending a stake.
	@JvmStatic fun heal_player(player: Player) {
		player.face(null) // Reset entity facing
		player.skills().resetStats() //Reset all players stats
		Poison.cure(player) //Cure the player from any poisons
		Venom.cure(2, player)
		player.timers().cancel(TimerKey.FROZEN) //Remove frozen timer key
		player.timers().cancel(TimerKey.STUNNED) //Remove stunned timer key
		player.timers().cancel(TimerKey.TELEBLOCK) //Remove teleblock timer key
		player.timers().cancel(TimerKey.TELEBLOCK_IMMUNITY) //Remove the teleblock immunity timer key
		player.varps().varp(Varp.SPECIAL_ENERGY, 1000) //Set energy to 100%
		player.varps().varp(Varp.SPECIAL_ENABLED, 0) //Disable special attack
		player.timers().cancel(TimerKey.COMBAT_LOGOUT) //Remove combat logout timer key
		player.clearDamagers() //Clear damagers
		player.clearDamageTimes() // Clear damage time tracker
		Prayers.disableAllPrayers(player) //Disable all prayers
		player.putattrib(AttributeKey.RUN_ENERGY, 100.0) //Set the players run energy to 100
		player.hp(100, 0) //Set hitpoints to 100%
		player.clearattrib(AttributeKey.MOST_DAM_TRACKER)
		player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
		player.write(SendWidgetTimer(WidgetTimer.TELEBLOCK, 0))
		player.write(SendWidgetTimer(WidgetTimer.VENGEANCE, 0))
	}
	
	// If the in_stake attribute is set.
	@JvmStatic fun in_duel(player: Entity): Boolean {
		return player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)
	}
	
	// Sets the double death attrib if players died within 2 ticks of each other.
	@JvmStatic fun check_double_death(player: Player) {
		if (in_duel(player)) {
			
			val partner = player.duelPartner() ?: return
			// We didn't die 3 ticks later and then set the double death to true.
			// 1st player died, we're both dead on this tick.
			// player.debug("opp ded %s and this cycle %d op-c: %d", partner.dead(), player.world().cycleCount(), partner.attribOr<Int>(AttributeKey.ARENA_DEATH_TICK, 0))
			
			if (partner.dead() && player.world().cycleCount() == partner.attribOr<Int>(AttributeKey.ARENA_DEATH_TICK, 0) + 3) {
				player.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
				partner.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
				//player.debug("DD!")
			}
		}
	}
	
	// If the player is NOT in a duel, nor is any duel-related interface open.
	@JvmStatic fun screen_closed(player: Player): Boolean {
		return !player.interfaces().visible(481) && !player.interfaces().visible(482) && !player.interfaces().visible(110) && !player.interfaces().visible(119) && !in_duel(player)
	}
	
	// The type of special duel the given player has selected. (-2 if none, -1 if selection screen open, 0-4 for types)
	@JvmStatic fun extra_of(player: Player): Int {
		return player.attribOr<Int>(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, -2)
	}
	
	// The settings selected which match whip/dds only.
	@JvmStatic fun extra_options_apply(player: Player): Boolean {
		// Optional settings: movement, obstacles, forfeit, spec
		var armour_off: Int = 0
		arrayOf(EquipSlot.HEAD, EquipSlot.CAPE, EquipSlot.AMULET, EquipSlot.BODY, EquipSlot.SHIELD, EquipSlot.LEGS, EquipSlot.FEET, EquipSlot.RING, EquipSlot.HANDS).forEach { slot ->
			if (equipment_disabled(player, slot)) {
				armour_off += 1
			}
		}
		val other_rules: Boolean = DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_DRINKS) && DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_FOOD)
				&& DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_PRAYER) && DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC) &&
				DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED)
		// player.debug("armour %d and %s", armour_off, other_rules)
		// Slots which dont matter: ammo & weapon.
		return armour_off == 9 && other_rules
	}
	
	// Item ids of fun weapons.
	val fun_weapon_ids = arrayOf(8650, 8652, 8654, 8656, 8658, 8660, 8662, 8664, 8666, 8668, 8670, 8672, 8274, 8676, 8678, 8680,
			6082, 2460, 2462, 2464, 2466, 2468, 2470, 2472, 2474, 2476, 751, 6541, 10150, 3695, 6773, 6774, 6775, 6776, 6777, 6778,
			6779, 4566, 1419, 10501, 4086, 10487)
	
	// Check if the player posesses a fun weapon. Can't start a fun stake without one!
	@JvmStatic fun doesnt_have_a_fun_weapon(player: Player): Boolean {
		var has_fun: Boolean = false
		fun_weapon_ids.forEach { wep ->
			if (!has_fun) {
				if (player.inventory().contains(wep) || player.equipment().get(EquipSlot.WEAPON)!!.id() == wep) {
					has_fun = true
				}
			}
		}
		return has_fun
	}
}
