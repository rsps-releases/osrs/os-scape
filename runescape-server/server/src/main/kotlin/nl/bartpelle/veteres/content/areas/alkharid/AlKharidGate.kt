package nl.bartpelle.veteres.content.areas.alkharid

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 2/20/2016.
 */
object AlKharidGate {
	
	val BORDER_GUARD = 4287
	val JOKE = true
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(2882) @Suspendable { gateTalk(it) }
		r.onObject(2883) @Suspendable { gateTalk(it) }
	}
	
	@Suspendable fun gateTalk(it: Script) {
		val opt = it.player().attrib<Int>(AttributeKey.INTERACTION_OPTION)
		
		if ((opt == 1 || !it.player().inventory().contains(Item(995, 10))) && !it.player().world().realm().isPVP) {
			it.chatPlayer("Can I come through this gate?", 588)
			it.chatNpc("You must pay a toll of 10 gold coins to pass.", BORDER_GUARD)
			when (it.options("No thank you, I'll walk around.", "Who does my money go to?", "Yes, ok.")) {
				1 -> {
					it.chatPlayer("No thank you, I'll walk around.", 588)
					it.chatNpc("Ok suit yourself.", BORDER_GUARD)
				}
				2 -> {
					it.chatPlayer("Who does my money go to?", 554)
					if (JOKE) {
						it.chatNpc("The money goes to Bart and Nick.", BORDER_GUARD)
						it.chatPlayer("...", 554)
					} else {
						it.chatNpc("The money goes to the city of Al-Kharid.", BORDER_GUARD)
					}
				}
				3 -> {
					it.chatPlayer("Yes, ok.")
					if (it.player().inventory().contains(Item(995, 10))) {
						passThrough(it)
					} else {
						it.chatPlayer("Oh dear I don't actually seem to have enough money.", 610)
					}
				}
			}
		} else {
			passThrough(it)
		}
	}
	
	@Suspendable fun passThrough(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		it.player().lock()
		
		it.message("You pay the guard.")
		it.player().inventory().remove(Item(995, 10), true)
		
		it.player().world().removeObj(MapObj(Tile(3268, 3227), 2882, 0, 0), false)
		it.player().world().removeObj(MapObj(Tile(3268, 3228), 2883, 0, 0), false)
		it.player().world().spawnObj(MapObj(Tile(3267, 3228), 7174, 0, 1), false)
		it.player().world().spawnObj(MapObj(Tile(3267, 3227), 7173, 0, 3), false)
		
		it.sound(69)
		
		if (it.player().tile().x < 3268) {
			it.player().pathQueue().step(3268, obj.tile().z, PathQueue.StepType.FORCED_WALK)
		} else {
			it.player().pathQueue().step(3267, obj.tile().z, PathQueue.StepType.FORCED_WALK)
		}
		
		it.delay(1)
		
		it.runGlobal(it.player().world()) @Suspendable {
			it.delay(1)
			it.ctx<World>().removeObj(MapObj(Tile(3267, 3228), 7174, 0, 1), false)
			it.ctx<World>().removeObj(MapObj(Tile(3267, 3227), 7173, 0, 3), false)
			it.ctx<World>().spawnObj(MapObj(Tile(3268, 3227), 2882, 0, 0), false)
			it.ctx<World>().spawnObj(MapObj(Tile(3268, 3228), 2883, 0, 0), false)
		}
		
		it.player().unlock()
	}
	
}