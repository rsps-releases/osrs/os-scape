package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 06/06/2016.
 */
object RockCake {
	
	val ROCKCAKE = 7510
	val OLD_DWARF = 6254
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOption1(ROCKCAKE) {
			val player = it.player()
			if (Staking.in_duel(player) && Staking.extra_of(player) != -2) {
				player.message("The Rock Cake cannot be used in custom stakes.")
				return@onItemOption1
			}
			if (player.hp() > 1 && !player.timers().has(TimerKey.EAT_ROCKCAKE)) {
				player.hit(null, 1, 0, false).block(false).submit()
				player.timers().extendOrRegister(TimerKey.EAT_ROCKCAKE, 1)
				player.sound(1018)
			}
		}
		repo.onItemOption3(ROCKCAKE) {
			val player = it.player()
			if (Staking.in_duel(player) && Staking.extra_of(player) != -2) {
				player.message("The Rock Cake cannot be used in custom stakes.")
				return@onItemOption3
			}
			if (player.hp() > 2 && !player.timers().has(TimerKey.EAT_ROCKCAKE)) {
				val dmg = (player.hp() + 10) / 10
				player.hit(null, dmg, 0, false).block(false).submit()
				player.timers().extendOrRegister(TimerKey.EAT_ROCKCAKE, 1)
				player.sound(1018)
			}
		}
		repo.onNpcOption1(OLD_DWARF) @Suspendable {
			val player = it.player()
			if (it.options("Trade 100 gold for the cake.", "No thanks.") == 1) {
				if (!would_have_space(player, Item(995, 100), Item(ROCKCAKE))) {
					it.messagebox("You don't have enough inventory space.")
				} else if (!player.inventory().contains(Item(995, 100))) {
					it.messagebox("You don't have enough coins.")
				} else if (player.inventory().remove(Item(995, 100), true).success()) {
					player.inventory().add(Item(ROCKCAKE), true)
				}
			}
		}
	}
	
	private fun would_have_space(player: Player, currency: Item, foritem: Item): Boolean {
		var free = player.inventory().freeSlots()
		if (player.inventory().count(currency.id()) - currency.amount() == 0) {
			free += 1
		}
		return free >= foritem.amount()
	}
}