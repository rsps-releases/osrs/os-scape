package nl.bartpelle.veteres.content.areas.global

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.npcs.bosses.wilderness.WildernessEventController
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.LocationUtilities

/**
 * Created by Bart on 12/6/2015.
 * @author Bart
 * @author Savions
 */
object IronGates {
	
	//1 = north, 2 = east, 3 = south, 0 = west
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		intArrayOf(1727, 1728).forEach { gate ->
			r.onObject(gate) @Suspendable {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				if (obj.tile().z == 3856) {
					openLavaDragonsGate(it)
				} else if (obj.tile().z == 3855) {
					closeLavaDragonsGate(it)
				} else if (obj.tile().z == 9917) {
					openEdgevilleDungeonGate(it)
				} else if (obj.tile().equals(2935, 3451) || obj.tile().equals(2935, 3450)) {
					openTaverlyGate(it)
				} else if (obj.tile().z == 3896 && (obj.tile().x == 3336 || obj.tile().x == 3337)) {//open newgate
					openNewgate(it)
				} else if (obj.tile().z == 3895 && (obj.tile().x == 3336 || obj.tile().x == 3337)) {//close newgate
					closeNewgate(it)
				} else if (obj.tile().z == 3904 && (obj.tile().x == 3225 || obj.tile().x == 3224)) {//open midgate
					openMidgate(it)
				} else if (obj.tile().z == 3903 && (obj.tile().x == 3225 || obj.tile().x == 3224)) {//close midgate
					closeMidgate(it)
				} else if (obj.tile().z == 3904 && (obj.tile().x == 2947 || obj.tile().x == 2948)) {//open icegate
					openIcegate(it)
				} else if (obj.tile().z == 3903 && (obj.tile().x == 2947 || obj.tile().x == 2948)) {//close icegate
					closeIcegate(it)
				} else if (obj.tile().x == 3008 && (obj.tile().z == 3849 || obj.tile().z == 3850)) {//open kbd cage gate
					openKbdcageGate(it)
				} else if (obj.tile().x == 3007 && (obj.tile().z == 3849 || obj.tile().z == 3850)) {//close kbd cage gate
					closeKbdcageGate(it)
				}
			}
		}
		
		intArrayOf(1571, 1572).forEach { gate ->
			r.onObject(gate) @Suspendable {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				if (obj.tile().equals(3104, 9910) || obj.tile().equals(3104, 9909)) {
					closeGateWest(it)
				} else if (obj.tile().equals(3146, 9871) || obj.tile().equals(3146, 9870)) {
					closeGateSouth(it)
				}
			}
		}
		
		//wildy stairs 55 dungeon
		intArrayOf(1568, 1569).forEach { gate ->
			r.onObject(gate) @Suspendable {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				val instanced = MageBankInstance.inDungeon(it.player())
				if (obj.tile().x == 3040 && (obj.tile().z == 10307 || obj.tile().z == 10308) || obj.rot() == 2) {
					open55stairsGate1(it)
				} else if (obj.tile().x == 3041 && (obj.tile().z == 10307 || obj.tile().z == 10308) || obj.rot() == 1) {
					close55stairsGate1(it)
				} else if (obj.tile().x == 3022 && (obj.tile().z == 10311 || obj.tile().z == 10312) || instanced) {
					open55stairsGate2(it)
				} else if (obj.tile().x == 3023 && (obj.tile().z == 10311 || obj.tile().z == 10312) || instanced) {
					close55stairsGate2(it)
				} else if (obj.tile().x == 3044 && (obj.tile().z == 10342 || obj.tile().z == 10341) || instanced) {
					open55stairsGate3(it)
				} else if (obj.tile().x == 3045 && (obj.tile().z == 10342 || obj.tile().z == 10341) || instanced) {
					close55stairsGate3(it)
				} else if (obj.tile().equals(3103, 9910) || obj.tile().equals(3103, 9909)) {//edge dungeon gates 1
					openGateWest(it)
				} else if (obj.tile().equals(3145, 9871) || obj.tile().equals(3145, 9870)) {//edge dungeon gates towards hill giants
					openGateSouth(it)
				} else if (obj.tile().equals(3112, 3514) || obj.tile().equals(3112, 3515)) {
					openEdgevilleJail(it)
				}
			}
		}
	}
	
	
	@Suspendable fun openEdgevilleJail(s: Script) {
		val player = s.player
		s.messagebox("The jail is locked. Engraved in the bars, it reads:<br>PROTECTED BY THE POWER OF SKOTIZO")
		
		if (player.inventory().has(19566)) {
			s.itemBox("You look at the key you carry, which seems to glow from a dark energy. It's the same size as the lock on the door.", 19566)
			
			if (s.optionsTitled("Open the door?", "Yes, open the door.", "No, I'd rather keep my key.") == 1) {
				s.messagebox("You take the key and insert it into the lock. The key seems very fragile, and looks like it can break any moment. It could break if you turn it.")
				
				if (s.optionsTitled("Riskfully open the lock?", "Yes, I want to risk it.", "No, I want to keep my key.") == 1) {
					s.player.animate(832)
					s.player.lock()
					s.delay(2)
					s.player.unlock()
					
					val keySlot = player.inventory().findFirst(19566).first()
					if (player.inventory().remove(Item(19566), true).success()) {
						if (player.world().rollDie(10, 1)) { // Key is a 1 in 10 drop. Making this 1 in 10 means it's 1:100 to get a rare.
							val loot = WildernessEventController.randomRareItem(player.world().realm())
							player.inventory().add(loot, true)
							s.itemBox("As you turn the lock, the key breaks, but the lock opens just before it snaps. Behind the bars you find something mysterious: <col=ff0000>${loot.name(player.world())}</col>.", loot.id())
						} else {
							player.inventory().add(Item(592), true, keySlot)
							s.messagebox("As you turn the lock, the key breaks. The key tooth is stuck, and your key crumbles.")
						}
					}
				}
			}
		} else {
			s.itemBox("You need a key from Skotizo to open this door.", 19566)
		}
	}
	
	fun openGateWest(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3103, 9910), 1568, 0, 2))
		world.removeObj(MapObj(Tile(3103, 9909), 1569, 0, 2))
		world.spawnObj(MapObj(Tile(3104, 9910), 1571, 0, 1))
		world.spawnObj(MapObj(Tile(3104, 9909), 1572, 0, 3))
	}
	
	fun closeGateWest(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3104, 9910), 1571, 0, 1))
		world.removeObj(MapObj(Tile(3104, 9909), 1572, 0, 3))
		world.spawnObj(MapObj(Tile(3103, 9910), 1568, 0, 2))
		world.spawnObj(MapObj(Tile(3103, 9909), 1569, 0, 2))
	}
	
	fun openGateSouth(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3145, 9871), 1568, 0, 2))
		world.removeObj(MapObj(Tile(3145, 9870), 1569, 0, 2))
		world.spawnObj(MapObj(Tile(3146, 9871), 1571, 0, 1))
		world.spawnObj(MapObj(Tile(3146, 9870), 1572, 0, 3))
	}
	
	fun closeGateSouth(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3145, 9871), 1568, 0, 2))
		world.spawnObj(MapObj(Tile(3145, 9870), 1569, 0, 2))
		world.removeObj(MapObj(Tile(3146, 9871), 1571, 0, 1))
		world.removeObj(MapObj(Tile(3146, 9870), 1572, 0, 3))
	}
	
	fun open55stairsGate1(it: Script) {//remember it's a double gate
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1568 //closed -> 1st door
		//now get the 2nd door next to it, also closed
        val instanced = MageBankInstance.inDungeon(player)
        val x = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).x else 3040)
        val z = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).z else 10307)

		val obj2 = player.world().objById(if (primary) 1569 else 1568, x, if (primary) z - 1 else z, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null) {
			player.message("This door is stuck...")
			return
		}
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//new objects, 1 sq south, with new rotations
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(1, 0, 0), obj.id(), obj.type(), if (primary) 1 else 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(1, 0, 0), obj2.id(), obj2.type(), if (primary) 3 else 1)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You open the gate.")
	}
	
	fun close55stairsGate1(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1568
		val instanced = MageBankInstance.inDungeon(player)
		val x = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).x else 3040)
        val z = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).z else 10307)

		val obj2 = player.world().objById(if (primary) 1569 else 1568, x, if (primary) z + 1 else z, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		// Stuck mechanic
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		val cur: Int = player.world().cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(-1, 0, 0), obj.id(), obj.type(), 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(-1, 0, 0), obj2.id(), obj2.type(), 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You close the gate.")
	}
	
	fun open55stairsGate2(it: Script) {//remember it's a double gate
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1568 //closed -> 1st door
        val instanced = MageBankInstance.inDungeon(player)
        val x = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).x else 3022)
        val z = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).z else 10311)

		//now get the 2nd door next to it, also closed
		val obj2 = player.world().objById(if (primary) 1569 else 1568, x, if (primary) z - 1 else z , 0)

		//Shouldn't happen, but you never know!
		if (obj2 == null) {
			player.message("This door is stuck...")
			return
		}
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//new objects, 1 sq south, with new rotations
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(1, 0, 0), obj.id(), obj.type(), if (primary) 1 else 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(1, 0, 0), obj2.id(), obj2.type(), if (primary) 3 else 1)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You open the gate.")
	}
	
	fun close55stairsGate2(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1568
        val instanced = MageBankInstance.inDungeon(player)
        val x = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).x else 3023)
        val z = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).z else 103011)

		val obj2 = player.world().objById(if (primary) 1569 else 1568, x, if (primary) z + 1 else z, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		// Stuck mechanic
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		val cur: Int = player.world().cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(-1, 0, 0), obj.id(), obj.type(), 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(-1, 0, 0), obj2.id(), obj2.type(), 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You close the gate.")
	}
	
	fun open55stairsGate3(it: Script) {//remember it's a double gate
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1568 //closed -> 1st door
		//now get the 2nd door next to it, also closed
        val instanced = MageBankInstance.inDungeon(player)
        val x = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).x else 3044)
        val z = (if (instanced) LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).z else 10341)
        /*val z: Int = when(instanced) {
            true -> {
                if (primary) {
                    LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).transform(0, -1).z
                } else {
                    LocationUtilities.dynamicTileFor(obj.tile(), MageBankInstance.mageBankDungeon).transform(0, 1).z
                }
            }
            else -> {
                if (primary) {
                    10341
                } else {
                    10342
                }
            }
        }*/
		val obj2 = player.world().objById(if (primary) 1569 else 1568, x, z, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null) {
			player.message("This door is stuck... $instanced")
			return
		}
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//new objects, 1 sq south, with new rotations
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(1, 0, 0), obj.id(), obj.type(), if (primary) 1 else 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(1, 0, 0), obj2.id(), obj2.type(), if (primary) 3 else 1)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You open the gate.")
	}
	
	fun close55stairsGate3(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1568
		val obj2 = player.world().objById(if (primary) 1569 else 1568, 3045, if (primary) 10341 else 10342, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		// Stuck mechanic
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		val cur: Int = player.world().cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(-1, 0, 0), obj.id(), obj.type(), 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(-1, 0, 0), obj2.id(), obj2.type(), 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You close the gate.")
	}
	
	fun openKbdcageGate(it: Script) {//remember it's a double gate
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728 //closed -> 1st door
		//now get the 2nd door next to it, also closed
		val obj2 = player.world().objById(if (primary) 1727 else 1728, 3008, if (primary) 3849 else 3850, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null) {
			player.message("This door is stuck...")
			return
		}
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		//player.debug("change count %d", openCycleIds.size)
		
		player.world().removeObj(obj.putattrib(AttributeKey.DOOR_USES, openCycleIds), true)
		player.world().removeObj(obj2.putattrib(AttributeKey.DOOR_USES, openCycleIds), true)
		//new objects, 1 sq south, with new rotations
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(-1, 0, 0), obj.id(), obj.type(), if (primary) 1 else 3).putattrib(AttributeKey.DOOR_USES, openCycleIds))
		player.world().spawnObj(MapObj(obj2.tile().transform(-1, 0, 0), obj2.id(), obj2.type(), if (primary) 3 else 1).putattrib(AttributeKey.DOOR_USES, openCycleIds))
		//player.message("You open the gate.")
	}
	
	fun closeKbdcageGate(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728
		val obj2 = player.world().objById(if (primary) 1727 else 1728, 3007, if (primary) 3849 else 3850, 0)
		
		//Shouldn't happen, but you never know!
		obj2 ?: return
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		
		val cur: Int = player.world().cycleCount()
		//player.debug("pre-update change count %d", openCycleIds.size)
		openCycleIds.removeAll { p -> (p as Int) < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		//player.debug("change count %d", openCycleIds.size)
		
		player.world().removeObj(obj.putattrib(AttributeKey.DOOR_USES, openCycleIds), true)
		player.world().removeObj(obj2.putattrib(AttributeKey.DOOR_USES, openCycleIds), true)
		//1 = north, 2 = east, 3 = south, 0 = west
		player.world().spawnObj(MapObj(obj.tile().transform(1, 0, 0), obj.id(), obj.type(), 0).putattrib(AttributeKey.DOOR_USES, openCycleIds))
		player.world().spawnObj(MapObj(obj2.tile().transform(1, 0, 0), obj2.id(), obj2.type(), 0).putattrib(AttributeKey.DOOR_USES, openCycleIds))
		//player.message("You close the gate.")
	}
	
	fun openIcegate(it: Script) {//remember it's a double gate
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728 //closed -> 1st door
		//now get the 2nd door next to it, also closed
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 2948 else 2947, 3904, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null) {
			player.message("This door is stuck...")
			return
		}
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//new objects, 1 sq south, with new rotations
		player.world().spawnObj(MapObj(obj.tile().transform(0, -1, 0), obj.id(), obj.type(), if (primary) 0 else 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds) //north side of square, or south
		player.world().spawnObj(MapObj(obj2.tile().transform(0, -1, 0), obj2.id(), obj2.type(), if (primary) 2 else 0)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You open the gate.")
	}
	
	fun closeIcegate(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 2948 else 2947, 3903, 0) ?: return
		
		// Stuck mechanic
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		val cur: Int = player.world().cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		player.world().spawnObj(MapObj(obj.tile().transform(0, 1, 0), obj.id(), obj.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(0, 1, 0), obj2.id(), obj2.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You close the gate.")
	}
	
	fun openMidgate(it: Script) {//remember it's a double gate
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728 //closed -> 1st door
		//now get the 2nd door next to it, also closed
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 3225 else 3224, 3904, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null) {
			player.message("This door is stuck...")
			return
		}
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//new objects, 1 sq south, with new rotations
		player.world().spawnObj(MapObj(obj.tile().transform(0, -1, 0), obj.id(), obj.type(), if (primary) 0 else 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds) //north side of square, or south
		player.world().spawnObj(MapObj(obj2.tile().transform(0, -1, 0), obj2.id(), obj2.type(), if (primary) 2 else 0)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You open the gate.")
	}
	
	fun closeMidgate(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 3225 else 3224, 3903, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		// Stuck mechanic
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		val cur: Int = player.world().cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		player.world().spawnObj(MapObj(obj.tile().transform(0, 1, 0), obj.id(), obj.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(0, 1, 0), obj2.id(), obj2.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You close the gate.")
	}
	
	fun openNewgate(it: Script) {//remember it's a double gate
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728 //closed -> 1st door
		//now get the 2nd door next to it, also closed
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 3337 else 3336, 3896, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null) {
			player.message("This door is stuck...")
			return
		}
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		//new objects, 1 sq south, with new rotations
		player.world().spawnObj(MapObj(obj.tile().transform(0, -1, 0), obj.id(), obj.type(), if (primary) 0 else 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds) //north side of square, or south
		player.world().spawnObj(MapObj(obj2.tile().transform(0, -1, 0), obj2.id(), obj2.type(), if (primary) 2 else 0)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You open the gate.")
	}
	
	fun closeNewgate(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 3337 else 3336, 3895, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		// Stuck mechanic
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		val cur: Int = player.world().cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		player.world().spawnObj(MapObj(obj.tile().transform(0, 1, 0), obj.id(), obj.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(0, 1, 0), obj2.id(), obj2.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You close the gate.")
	}
	
	//hardcoded
	@Suspendable fun openEdgevilleDungeonGate(it: Script) {
		val player = it.player()
		val outside = player.tile().z <= 9917
		if (outside && !it.wildernessWarning())
			return
		val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
		if (!obj.interactAble())
			return
		val primary = obj.id() == 1728
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 3131 else 3132, 9917, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		
		it.runGlobal(player.world()) @Suspendable {
			obj.interactAble(false)
			obj2.interactAble(false)
			player.faceObj(obj)
			player.world().removeObj(obj, true)
			player.world().removeObj(obj2, true)
			
			val spawned1 = MapObj(obj.tile(), obj.id(), obj.type(), if (primary) 2 else 0)
			val spawned2 = MapObj(obj2.tile(), obj2.id(), obj2.type(), if (primary) 0 else 2)
			spawned1.interactAble(false)
			spawned2.interactAble(false)
			player.world().spawnObj(spawned1)
			player.world().spawnObj(spawned2)
			
			it.delay(2)
			
			player.world().removeObj(spawned1, true)
			player.world().removeObj(spawned2, true)
			player.world().spawnObj(obj)
			player.world().spawnObj(obj2)
			obj.interactAble(true)
			obj2.interactAble(true)
		}
		player.lock()
		player.pathQueue().interpolate(obj.tile().x, if (outside) 9918 else 9917, PathQueue.StepType.FORCED_WALK)
		it.waitForTile(Tile(obj.tile().x, if (outside) 9918 else 9917))
		it.delay(1)
		it.player().unlock()
	}
	
	fun openLavaDragonsGate(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 3202 else 3201, 3856, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(player.world().cycleCount())
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		player.world().spawnObj(MapObj(obj.tile().transform(0, -1, 0), obj.id(), obj.type(), if (primary) 0 else 2)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(0, -1, 0), obj2.id(), obj2.type(), if (primary) 2 else 0)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You open the gate.")
	}
	
	fun closeLavaDragonsGate(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		val primary = obj.id() == 1728
		val obj2 = player.world().objById(if (primary) 1727 else 1728, if (primary) 3202 else 3201, 3855, 0)
		
		//Shouldn't happen, but you never know!
		if (obj2 == null)
			return
		
		// Stuck mechanic
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		val cur: Int = player.world().cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		if (openCycleIds.size >= 10) {
			player.message("The gate is stuck!")
			return
		}
		openCycleIds.add(cur)
		
		player.world().removeObj(obj, true)
		player.world().removeObj(obj2, true)
		player.world().spawnObj(MapObj(obj.tile().transform(0, 1, 0), obj.id(), obj.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.world().spawnObj(MapObj(obj2.tile().transform(0, 1, 0), obj2.id(), obj2.type(), 3)).putattrib(AttributeKey.DOOR_USES, openCycleIds)
		player.message("You close the gate.")
	}
	
	fun openTaverlyGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(2935, 3451), 1727, 0, 2))
		world.removeObj(MapObj(Tile(2935, 3450), 1728, 0, 2))
		world.spawnObj(MapObj(Tile(2936, 3451), 1571, 0, 1))
		world.spawnObj(MapObj(Tile(2936, 3450), 1572, 0, 3))
	}
	
	fun closeTaverlyGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(2935, 3451), 1727, 0, 2))
		world.spawnObj(MapObj(Tile(2935, 3450), 1728, 0, 2))
		world.removeObj(MapObj(Tile(2936, 3451), 1571, 0, 1))
		world.removeObj(MapObj(Tile(2936, 3450), 1572, 0, 3))
	}
	
}