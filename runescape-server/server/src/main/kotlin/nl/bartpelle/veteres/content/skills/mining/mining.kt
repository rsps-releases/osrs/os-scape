package nl.bartpelle.veteres.content.skills.mining

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.items.InfernalTools
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.io.File
import java.io.PrintStream
import java.text.DecimalFormat
import java.util.*

/**
 * Created by Bart on 8/28/2015.
 *
 * Handles the mining skill, as well as a few utility functions to add mining-like features elsewhere in the game.
 */
class Mining {

    enum class Rock(val ore: Int, val rockName: String, val level: Int, val difficulty: Int, val xp: Double, val respawnTime: Int, val petOdds: Int) {
        CLAY(434, "clay", 1, 15, 5.0, 3, 74160),
        COPPER(436, "copper", 1, 35, 17.5, 5, 74160),
        TIN(438, "tin", 1, 35, 17.5, 5, 74160),
        IRON(440, "iron", 15, 75, 35.0, 10, 74160),
        SILVER(442, "silver", 20, 85, 40.0, 20, 74160),
        COAL(453, "coal", 30, 110, 50.0, 30, 29064),
        GOLD(444, "gold", 40, 180, 65.0, 40, 29664),
        MITHRIL(447, "mithril", 55, 210, 80.0, 60, 14832),
        LOVAKITE(13356, "lovakite", 65, 210, 10.0, 40, 24556),
        ADAMANT(449, "adamant", 70, 310, 95.0, 300, 59328),
        RUNE(451, "rune", 85, 380, 125.0, 1500, 4237),
        JAIL_BLURITE(668, "blurite", 1, 320, 0.0, 3, 1000000) // I can already see the idiots getting pets there..
    }

    enum class Pickaxe(val id: Int, val points: Int, val anim: Int, val level: Int) {
        BRONZE(1265, 5, 625, 1),
        IRON(1267, 9, 626, 1),
        STEEL(1269, 14, 627, 6),
        BLACK(12297, 21, 3866, 11),
        MITHRIL(1273, 26, 629, 21),
        ADAMANT(1271, 30, 628, 31),
        RUNE(1275, 36, 624, 41),
        DRAGON(11920, 42, 7139, 61),
        THIRD_AGE(20014, 42, 7283, 61),
        DRAGON_OR(12797, 42, 642, 61),
        INFERNAL(13243, 42, 4482, 61),
    }

    companion object {
        fun chance(level: Int, type: Rock, pick: Pickaxe): Int {
            val points = ((level - type.level) + 1 + pick.points.toDouble())
            val denom = type.difficulty.toDouble()
            return (Math.min(0.95, points / denom) * 100).toInt()
        }

        fun findPickaxe(player: Player): Pickaxe? {
            Pickaxe.values().sortedWith(Comparator { p1, p2 -> p2.level - p1.level }).forEach {
                if (player.skills().xpLevel(Skills.MINING) >= it.level && (it.id in player.inventory() || it.id in player.equipment())) {
                    return it
                }
            }

            return null
        }

        /**
         * Attempts to roll for a gem. Chances to get a gem are 1/256, or 3/256 when wearing a charged glory amulet.
         */
        fun giveGem(player: Player) {
            val roll = player.world().random(9)
            when (roll) {
                0 -> {
                    player.message("You just found a Diamond!"); player.inventory() += 1617
                }
                in 1..2 -> {
                    player.message("You just found a Ruby!"); player.inventory() += 1619
                }
                in 3..5 -> {
                    player.message("You just found an Emerald!"); player.inventory() += 1621
                }
                in 6..9 -> {
                    player.message("You just found a Sapphire!"); player.inventory() += 1623
                }
            }
        }

        @Suspendable
        fun prospect(s: Script, rock: Rock) {
            val player = s.player()

            player.stopActions(true)
            player.message("You examine the rock for ores...")
            player.sound(2661, 0)
            s.delay(4)
            player.message("This rock contains ${rock.rockName}.")
            player.stopActions(true)
        }

        @Suspendable
        fun interact(s: Script, rockType: Rock, replId: Int = 10796) {
            val option: Int = s.player().attribOr(AttributeKey.INTERACTION_OPTION, 0)

            if (option == 2) {
                prospect(s, rockType)
            } else {
                mine(s, rockType, replId)
            }
        }

        @Suspendable
        fun mine(s: Script, rockType: Rock, replId: Int = 10796) {
            var rock: Rock = rockType
            val player = s.player()
            val obj: MapObj = s.player().attrib(AttributeKey.INTERACTION_OBJECT)
            val pick: Pickaxe? = findPickaxe(player)

            // Any rocks mined in the jail are converted to bluite. This is the ore required to leave the jail.
            // If it was coal, people could mine coal before they are even jailed and instantly escape!
            if (player.attribOr<Int>(AttributeKey.JAILED, 0) == 1) {
                rock = Rock.JAIL_BLURITE

                // Expensive code but stops retards stockpiling ores.
                if (player.bank().count(Rock.JAIL_BLURITE.ore) + player.inventory().count(Rock.JAIL_BLURITE.ore) >= player.attribOr<Int>(AttributeKey.JAIL_ORES_TO_ESCAPE, 0)) {
                    player.message("You don't need any more ores to escape.")
                    return
                }
            }

            // Is the inventory full?
            if (player.inventory().full()) {
                player.sound(2277, 0)
                s.messagebox("Your inventory is too full to hold any more ${rock.rockName}.")
                return
            }

            if (pick == null) {
                player.sound(2277, 0)
                s.messagebox("You need a pickaxe to mine this rock. You do not have a pickaxe which " +
                        "you have the Mining level to use.")
                return
            }

            if (!player.world().realm().isPVP && player.skills()[Skills.MINING] < rock.level && player.attribOr<Int>(AttributeKey.JAILED, 0) == 0) {
                player.sound(2277, 0)
                s.messagebox("You need a Mining level of ${rock.level} to mine this rock.")
                return
            }

            s.delay(1)
            s.message("You swing your pick at the rock.")

            val gemOdds = if (player.equipment().hasAny(1706, 1708, 1710, 1712, 11976, 11978)) 3 else 1

            while (true) {
                player.animate(pick.anim)
                s.delay(1)

                // Check if the rock despawned
                if (!obj.valid(player.world())) {
                    player.animate(-1)
                    return
                }

                // Roll for an uncut
                if (player.world().rollDie(256, gemOdds)) {
                    giveGem(player)
                }

                var odds = Mining.chance(player.skills()[Skills.MINING], rock, pick)

                //For jailing we want to boost the rates of mining otherwise some players will literally be there for days.
                if (player.jailed()) {
                    odds = player.world().random(IntRange(90, 95))
                }

                if (BonusContent.isActive(player, BlessingGroup.PROSPECTOR)) {
                    odds = Math.min(95, (1.3 * odds).toInt())
                }

                if (player.attribOr(AttributeKey.DEBUG, false)) {
                    player.debug("Mining chance: $odds.")
                }

                if (player.world().random(100) <= odds) {
                    s.message("You manage to mine some ${rock.rockName}.")

                    // Woo! A pet! The reason we do this BEFORE the item is because it's... quite some more valuable :)
                    // Rather have a pet than a pesky ore thing, right?
                    val odds = (rock.petOdds.toDouble() * player.mode().skillPetMod()).toInt()
                    if (player.world().rollDie(odds, 1)) {
                        unlockGolem(player)
                    }

                    // Infernal pickaxe logic
                    if (pick == Pickaxe.INFERNAL && InfernalTools.canUse(player, Mining.Pickaxe.INFERNAL.id) && player.world().random(2) == 0 && InfernalTools.active) {
                        player.graphic(580, 155, 0)
                        addBar(s, player, rock)
                    } else {
                        // If the player is in the wilderness we give them two ores, else we give only 1. : (
                        // Also, max cape 10% chance for double ore, up to addy only.
                        val bonusContentActivated = BonusContent.isActive(player, BlessingGroup.PROSPECTOR)
                                && player.world().rollDie(2, 1)

                        // Bonus content gives an additional message
                        if (bonusContentActivated) {
                            player.message("The active Prospector's blessing lets you mine another ore!")
                        }

                        player.inventory() += rock.ore
                    }

                    // Finding blood money while skilling..
                    if (player.world().realm().isOSRune && WildernessLevelIndicator.inWilderness(player.tile())) {
                        if (player.world().rollDie(5, 1)) {
                            if (player.inventory().add(Item(13307, 1), false).success()) {
                                player.message("You find 1 blood money coin inside the ore.")
                            }
                        }
                    }

                    player.sound(3600, 0)
                    player.animate(-1)

                    // The Prospector blessing reduces rock respawn timers by half.
                    var rockTime = if (BonusContent.isActive(player, BlessingGroup.PROSPECTOR))
                        rock.respawnTime / 2
                    else
                        rock.respawnTime

                    // High-level rocks on PvP are faster. Nobody waits on a PvP server.
                    if (player.world().realm().isPVP) {
                        rockTime = when (rock) {
                            Rock.RUNE -> 200 // 2 minutes
                            Rock.ADAMANT -> 50 // 30 seconds
                            Rock.MITHRIL -> 25 // 15 seconds
                            else -> rockTime
                        }
                    }

                    if (player.world().realm().isOSRune) {
                        s.runGlobal(player.world()) @Suspendable {
                            val world = player.world()
                            val spawned = MapObj(obj.tile(), replId, obj.type(), obj.rot())
                            world.spawnObj(spawned)
                            it.delay(Math.max(1, rockTime / 2 - 1))
                            world.removeObjSpawn(spawned)
                        }
                    } else {
                        s.runGlobal(player.world()) @Suspendable {
                            val world = player.world()
                            val rock = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot(), true)
                            val spawned = MapObj(obj.tile(), replId, obj.type(), obj.rot())
                            world.spawnObj(spawned)
                            it.delay(Math.max(1, rockTime - 1))
                            if (rock.custom()) {
                                world.spawnObj(rock)
                            } else {
                                world.removeObjSpawn(spawned)
                            }
                        }
                    }


                    if (!player.jailed()) {
                        s.addXp(Skills.MINING, rock.xp)
                        AchievementAction.processCategoryAchievement(player, AchievementCategory.MINING, rock.ore)
                    }
                    return
                }

                s.delay(3)
            }

        }

        fun unlockGolem(player: Player) {
            if (!PetAI.hasUnlocked(player, Pet.ROCK_GOLEM)) {
                // Unlock the varbit. Just do it, rather safe than sorry.
                player.varps().varbit(Pet.ROCK_GOLEM.varbit, 1)

                // RS tries to add it as follower first. That only works if you don't have one.
                val currentPet = player.pet()
                if (currentPet == null) {
                    player.message("You have a funny feeling like you're being followed.")
                    PetAI.spawnPet(player, Pet.ROCK_GOLEM, false)
                } else {
                    // Sneak it into their inventory. If that fails, fuck you, no pet for you!
                    if (player.inventory().add(Item(Pet.ROCK_GOLEM.item), true).success()) {
                        player.message("You feel something weird sneaking into your backpack.")
                    } else {
                        player.message("Speak to Probita to claim your pet!")
                    }
                }

                player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.ROCK_GOLEM.item).name(player.world())}.")
            } else {
                player.message("You have a funny feeling like you would have been followed...")
            }
        }
    }
}

private fun addBar(s: Script, player: Player, rock: Mining.Rock) {
    val xp = 0.0
    val success = when (rock) {
        Mining.Rock.COPPER, Mining.Rock.TIN -> {
            player.inventory() += 2349
            s.addXp(Skills.SMITHING, 2.5)
            true
        }
        Mining.Rock.IRON -> {
            player.inventory() += 2351
            s.addXp(Skills.SMITHING, 5.0)
            true
        }
        Mining.Rock.SILVER -> {
            player.inventory() += 2355
            s.addXp(Skills.SMITHING, 5.5)
            true
        }
        Mining.Rock.GOLD -> {
            player.inventory() += 2357
            s.addXp(Skills.SMITHING, 9.0)
            true
        }
        Mining.Rock.MITHRIL -> {
            player.inventory() += 2359
            s.addXp(Skills.SMITHING, 12.0)
            true
        }
        Mining.Rock.ADAMANT -> {
            player.inventory() += 2361
            s.addXp(Skills.SMITHING, 15.0)
            true
        }
        Mining.Rock.RUNE -> {
            player.inventory() += 2363
            s.addXp(Skills.SMITHING, 20.0)
            true
        }
        else -> false
    }

    // If we succeeded try to remove the charge
    if (success) {
        // Check equipment first
        val equipped = player.equipment()[EquipSlot.WEAPON]
        if (equipped != null && equipped.id() == Mining.Pickaxe.INFERNAL.id) {
            // Aw yis we found one; remove a charge
            equipped.property(ItemAttrib.CHARGES, equipped.property(ItemAttrib.CHARGES) - 1)
        } else {
            // Loop through the inventory to find one with charges
            for (item in player.inventory()) {
                if (item != null && item.id() == Mining.Pickaxe.INFERNAL.id && item.property(ItemAttrib.CHARGES) > 0) {
                    // Yep, we found it
                    item.property(ItemAttrib.CHARGES, item.property(ItemAttrib.CHARGES) - 1)
                }
            }
        }
    }
}

fun main(args: Array<String>) {
    val type = Mining.Rock.GOLD
    val format = DecimalFormat("###.##")

    var out = PrintStream(File("C:\\Users\\Bart\\Desktop\\mining_" + type.toString().toLowerCase() + ".csv"))
    out.print("lvl")
    for (h in Mining.Pickaxe.values())
        out.print("," + h)
    out.println()

    for (i in 1..99) {
        out.print(i)
        for (h in Mining.Pickaxe.values()) {
            out.print("," + format.format(Mining.chance(i, type, h)) + "%")
        }
        out.println()
    }

    out.flush()
}

data class RegisterableRock(val obj: Int, val type: Mining.Rock, val empty: Int = 7468);

@ScriptMain
fun register_mining(r: ScriptRepository) {
    val rocks = arrayOf(
            RegisterableRock(7487, Mining.Rock.CLAY),
            RegisterableRock(7454, Mining.Rock.CLAY, 7469),
            RegisterableRock(7453, Mining.Rock.COPPER),
            RegisterableRock(7484, Mining.Rock.COPPER, 7469),
            RegisterableRock(7455, Mining.Rock.IRON),
            RegisterableRock(7488, Mining.Rock.IRON, 7469),
            RegisterableRock(7485, Mining.Rock.TIN),
            RegisterableRock(7486, Mining.Rock.TIN, 7469),
            RegisterableRock(7456, Mining.Rock.COAL, 7469),
            RegisterableRock(7489, Mining.Rock.COAL),
            RegisterableRock(7457, Mining.Rock.SILVER),
            RegisterableRock(7490, Mining.Rock.SILVER, 7469),
            RegisterableRock(7491, Mining.Rock.GOLD),
            RegisterableRock(7458, Mining.Rock.GOLD, 7469),
            RegisterableRock(7459, Mining.Rock.MITHRIL),
            RegisterableRock(7492, Mining.Rock.MITHRIL, 7469),
            RegisterableRock(28596, Mining.Rock.LOVAKITE),
            RegisterableRock(28597, Mining.Rock.LOVAKITE, 7469),
            RegisterableRock(7493, Mining.Rock.ADAMANT),
            RegisterableRock(7460, Mining.Rock.ADAMANT, 7469),
            RegisterableRock(7461, Mining.Rock.RUNE),
            RegisterableRock(7494, Mining.Rock.RUNE, 7469)
    )

    rocks.forEach { rock ->
        r.onObject(rock.obj) @Suspendable {
            Mining.interact(it, rock.type, rock.empty)
        }
    }

    intArrayOf(7468, 7469).forEach {
        r.onObject(it) { it.player().sound(2661, 0); it.message("There is no ore currently available in this rock.") }
    }
}