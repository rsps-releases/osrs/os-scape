package nl.bartpelle.veteres.content.npcs.bosses.zulrah

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.npcs.bosses.zulrah.ZulrahConfig.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 3/6/2016.
 */
object Zulrah {
	
	@JvmField public val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		if (target.isPlayer) {
			while ((target as Player).interfaces().visible(229)) // Wait for chatbox close before starting combat
				it.delay(1)
		}
		var rotation = ZulrahPattern.randomPattern()
		var rotationIndex = 0
		var init = true
		
		while (EntityCombat.targetOk(npc, target, 26)) {
			doPatternPhase(it, npc, rotation[rotationIndex++ % rotation.phases.size], target, init)
			init = false
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun doPatternPhase(it: Script, npc: Npc, phase: ZulrahPhase, target: Entity, init: Boolean) {
		
		// Stops agro kicking in mid way through phase and interrupting combat and restarting phases
		npc.timers().extendOrRegister(TimerKey.IN_COMBAT, 30)
		// On init we're already at the first position and facing south.
		if (!init) {
			// Appear...
			val targetPosition = npc.spawnTile() + phase.position.tile
			npc.teleport(targetPosition)
			npc.sync().transmog(phase.form.id)
			npc.faceTile(targetPosition.plus(Tile(2, 2)).plus(phase.position.direction))
			npc.combatInfo(npc.world().combatInfo(phase.form.id)) // Acquire new bonuses/stats for weaknesses
			
			npc.lockDelayDamage()
			npc.faceTile(targetPosition.plus(Tile(2, 2)).plus(phase.position.direction))
			npc.animate(5073) // up
			it.delay(3)
		}
		npc.face(target)
		npc.unlock()
		
		// Does this phase do any kind of combat at all?
		if (!phase.hasConfig(NO_ATTACK)) {
			when (phase.form) {
				ZulrahForm.MELEE -> doMeleePhase(it, npc, target)
				ZulrahForm.RANGE -> {
					// Range 5 times!
					for (i in 1..5) {
						doRangedAttack(it, npc, target)
						it.delay(3)
					}
				}
				ZulrahForm.MAGIC -> {
					// Attack randomly 5 times! Most chance for magic, little chance for range.
					for (i in 1..5) {
						if (npc.world().rollDie(6, 1)) {
							doRangedAttack(it, npc, target)
						} else {
							doMagicAttack(it, npc, target)
						}
						
						it.delay(3)
					}
				}
				ZulrahForm.JAD_MR, ZulrahForm.JAD_RM -> {
					var range = phase.form == ZulrahForm.JAD_RM
					
					// Swap attack styles every hit for 10 hits in total
					for (i in 1..10) {
						if (range) {
							doRangedAttack(it, npc, target, true)
						} else {
							doMagicAttack(it, npc, target, true)
						}
						
						range = !range
						it.delay(3)
					}
				}
			}
		}
		
		// Check for any post-combat stuff
		if (phase.hasConfig(FULL_TOXIC_FUMES)) {
			fillToxicFumes(it, npc, target)
		} else if (phase.hasConfig(SNAKELINGS_CLOUDS_SNAKELINGS)) {
			snakelingsCloudsSnakelings(it, npc, target)
		} else if (phase.hasConfig(EAST_SNAKELINGS_REST_FUMES)) {
			eastSnakelingsRestFumes(it, npc, target)
		} else if (phase.hasConfig(SNAKELING_FUME_MIX)) {
			snakelingFumeMix(it, npc, target)
		}
		
		// Sink down again
		target.stopActions(false)
		target.clearattrib(AttributeKey.TARGET)
		/*if (npc.world().playerForId(npc.attrib<Player>(AttributeKey.OWNING_PLAYER)).get().attrib<WeakReference<Entity>>(AttributeKey.TARGET).get() == npc) {
			npc.world().playerForId(npc.attrib<Player>(AttributeKey.OWNING_PLAYER)).get().stopActions(false)
		}*/
		npc.lockDelayDamage()
		npc.animate(5072)
		npc.face(null)
		it.delay(2)
		npc.unlock()
	}
	
	@Suspendable fun doMeleePhase(it: Script, npc: Npc, target: Entity) {
		npc.face(null)
		
		for (i in 1..2) {
			// Aim..
			npc.animate(5806)
			val square = target.tile()
			npc.faceTile(square)
			
			it.delay(1)
			npc.faceTile(square)
			it.delay(4)
			
			// Strike!
			if (square.area(1).contains(target) && !isMeleeSafespot(npc, target.tile())) {
				target.stun(6)
				target.hit(npc, EntityCombat.randomHit(npc), 0).combatStyle(CombatStyle.GENERIC)
			}
			
			it.delay(2)
			npc.face(target)
			it.delay(2)
			npc.face(null)
		}
	}
	
	@Suspendable fun doRangedAttack(it: Script, npc: Npc, target: Entity, jad: Boolean = false) {
		npc.animate(5069)
		
		val tileDist = npc.tile().transform(2, 2, 0).distance(target.tile())
		npc.world().spawnProjectile(npc, target, 1044, 65, 20, 20, 15 + (12 * tileDist), 15, 10)
		val delay = Math.max(1, (20 + tileDist * 12) / 30)
		val style = if (jad) CombatStyle.GENERIC else CombatStyle.RANGE
		var hit = EntityCombat.randomHit(npc)
		
		// If jad, you must be praying at the moment of firing.
		if (jad && target.varps().varbit(Varbit.PROTECT_FROM_MISSILES) != 0) {
			hit = 0
		}
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.RANGE, 1.0))
			target.hit(npc, hit, delay.toInt()).combatStyle(style) // Cannot protect from this.
		else
			target.hit(npc, 0, delay.toInt()).combatStyle(style) // Cannot protect from this.
	}
	
	@Suspendable fun doMagicAttack(it: Script, npc: Npc, target: Entity, jad: Boolean = false) {
		npc.animate(5069)
		
		val tileDist = npc.tile().transform(2, 2, 0).distance(target.tile())
		npc.world().spawnProjectile(npc, target, 1046, 65, 20, 20, 15 + (12 * tileDist), 15, 10)
		val delay = Math.max(1, (20 + tileDist * 12) / 30)
		var style = if (jad) CombatStyle.GENERIC else CombatStyle.MAGIC
		var hit = EntityCombat.randomHit(npc)
		
		// If jad, you must be praying at the moment of firing.
		if (jad && target.varps().varbit(Varbit.PROTECT_FROM_MAGIC) != 0) {
			hit = 0
		}
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, hit, delay.toInt()).combatStyle(style) // Cannot protect from this.
		else
			target.hit(npc, 0, delay.toInt()).combatStyle(style) // Cannot protect from this.
	}
	
	@Suspendable fun fillToxicFumes(it: Script, npc: Npc, target: Entity) {
		val spawnTile = it.npc().spawnTile()
		
		// Fix facing first
		npc.face(null)
		it.delay(1)
		
		// South-east
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(4, -4))
		spitFume(it, npc, spawnTile + Tile(2, -4), target, 3)
		spitFume(it, npc, spawnTile + Tile(5, -4), target, 3)
		it.delay(3)
		
		// South-west
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-2, -4))
		spitFume(it, npc, spawnTile + Tile(-1, -4), target, 3)
		spitFume(it, npc, spawnTile + Tile(-4, -3), target, 3)
		it.delay(3)
		
		// East
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(6, 2))
		spitFume(it, npc, spawnTile + Tile(6, -1), target, 3)
		spitFume(it, npc, spawnTile + Tile(6, 2), target, 3)
		it.delay(3)
		
		// West
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-4, 2))
		spitFume(it, npc, spawnTile + Tile(-4, 3), target, 3)
		spitFume(it, npc, spawnTile + Tile(-4, 0), target, 3)
		it.delay(3)
	}
	
	@Suspendable fun snakelingsCloudsSnakelings(it: Script, npc: Npc, target: Entity) {
		val spawnTile = it.npc().spawnTile()
		
		// Fix facing first
		npc.face(null)
		it.delay(1)
		
		// Snakelings
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-3, 4))
		createSnakeling(npc, spawnTile + Tile(-3, 4), 4, target)
		it.delay(3)
		npc.faceTile(spawnTile + Tile(-3, 1))
		createSnakeling(npc, spawnTile + Tile(-3, 1), 4, target)
		it.delay(3)
		
		// Fumes
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-4, -3))
		spitFume(it, npc, spawnTile + Tile(-4, -3), target, 3)
		spitFume(it, npc, spawnTile + Tile(-1, -4), target, 3)
		it.delay(3)
		
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(5, -2))
		spitFume(it, npc, spawnTile + Tile(5, -4), target, 3)
		spitFume(it, npc, spawnTile + Tile(6, -1), target, 3)
		it.delay(3)
		
		// Snakelings
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(7, 3))
		createSnakeling(npc, spawnTile + Tile(7, 3), 4, target)
		it.delay(3)
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(7, 6))
		createSnakeling(npc, spawnTile + Tile(7, 6), 4, target)
		it.delay(3)
	}
	
	@Suspendable fun eastSnakelingsRestFumes(it: Script, npc: Npc, target: Entity) {
		val spawnTile = it.npc().spawnTile()
		
		// Fix facing first
		npc.face(null)
		it.delay(1)
		
		// Fumes
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(3, -4))
		spitFume(it, npc, spawnTile + Tile(2, -4), target, 3)
		spitFume(it, npc, spawnTile + Tile(5, -4), target, 3)
		it.delay(3)
		
		// Fumes
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-2, -3))
		spitFume(it, npc, spawnTile + Tile(-4, -3), target, 3)
		spitFume(it, npc, spawnTile + Tile(-1, -4), target, 3)
		it.delay(3)
		
		// Fumes
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-4, 2))
		spitFume(it, npc, spawnTile + Tile(-4, 0), target, 3)
		spitFume(it, npc, spawnTile + Tile(-4, 3), target, 3)
		it.delay(3)
		
		// Snakelings
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(6, -3))
		createSnakeling(npc, spawnTile + Tile(6, -3), 4, target)
		it.delay(3)
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(7, 3))
		createSnakeling(npc, spawnTile + Tile(7, 3), 4, target)
		it.delay(3)
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(7, 6))
		createSnakeling(npc, spawnTile + Tile(7, 6), 4, target)
		it.delay(3)
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(7, 0))
		createSnakeling(npc, spawnTile + Tile(7, 0), 4, target)
		it.delay(3)
	}
	
	@Suspendable fun snakelingFumeMix(it: Script, npc: Npc, target: Entity) {
		val spawnTile = it.npc().spawnTile()
		
		// Fix facing first
		npc.face(null)
		it.delay(1)
		
		// Snakelings
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-3, -2))
		createSnakeling(npc, spawnTile + Tile(-3, -2), 4, target)
		it.delay(3)
		
		// Fumes
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(1, -4))
		spitFume(it, npc, spawnTile + Tile(-1, -4), target, 3)
		spitFume(it, npc, spawnTile + Tile(2, -4), target, 3)
		it.delay(3)
		
		// Snakelings
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-3, 4))
		createSnakeling(npc, spawnTile + Tile(-3, 4), 4, target)
		it.delay(3)
		
		// Fumes
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(5, -4))
		spitFume(it, npc, spawnTile + Tile(5, -4), target, 3)
		spitFume(it, npc, spawnTile + Tile(6, -1), target, 3)
		it.delay(3)
		
		// Snakelings
		npc.animate(5069)
		npc.faceTile(spawnTile + Tile(-3, 1))
		createSnakeling(npc, spawnTile + Tile(-3, 1), 4, target)
		it.delay(3)
	}
	
	@Suspendable fun spitFume(it: Script, npc: Npc, tile: Tile, target: Entity, delay: Int) {
		val obj = MapObj(tile, 11700, 10, 0)
		val world = npc.world()
		val area = tile.transform(1, 1, 0).area(1) // Center, 3x3
		
		// Do a nice graphic.
		//val tileDist = npc.tile().transform(2, 2, 0).distance(tile)
		npc.world().spawnProjectile(npc.tile().transform(2, 2, 0), tile + Tile(1, 1), 1045, 65, 0, 20, 12 + (10 * 6), 18, 10)
		
		world.server().scriptExecutor().executeScript(obj) @Suspendable {
			it.delay(delay)
			world.spawnObj(obj, false)
			
			// Deal damage for 30 ticks before it despawns.
			var ticks = 30
			while (ticks-- > 0) {
				if (area.contains(target)) {
					// Cannot be protected from, appears as venom and deals 1..4 damage.
					target.hit(null, world.random(1..4), 0, false).combatStyle(CombatStyle.GENERIC).type(Hit.Type.VENOM).submit()
					
					if (GameCommands.VENOM_VS_PLAYERS_ON) {
						target.venom(npc)
					}
				}
				if (npc.dead()) break // Fumes despawn when zulrah dies.
				
				it.delay(1)
			}
			
			world.removeObjSpawn(obj, true, false)
		}
	}
	
	fun createSnakeling(npc: Npc, tile: Tile, delay: Int, target: Entity) {
		val world = npc.world()
		
		/*world.spawnProjectile(npc.tile().transform(2, 2, 0), tile, 1047, 65, 0, 20, 12 + (16 * 6), 18, 10)

		world.server().scriptExecutor().executeScript(world) @Suspendable {
			it.delay(delay)
			val snakeling = Npc(2045, world, tile)

			world.registerNpc(snakeling)
			snakeling.respawns(false)
			snakeling.animate(2413)
			snakeling.putattrib(AttributeKey.OWNING_PLAYER, (target as Player).id())
			snakeling.lockNoDamage()
			it.delay(3)
			snakeling.unlock()
			snakeling.attack(target)
		}*/
	}
	
	fun isMeleeSafespot(npc: Npc, tile: Tile): Boolean {
		val spawnTile = npc.spawnTile()
		return tile == spawnTile.transform(-3, 0) || tile == spawnTile.transform(7, 0)
				|| tile == spawnTile.transform(4, -2) || tile == spawnTile.transform(0, -2)
	}
	
}