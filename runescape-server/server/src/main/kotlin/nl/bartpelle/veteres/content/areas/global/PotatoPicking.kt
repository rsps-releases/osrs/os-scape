package nl.bartpelle.veteres.content.areas.global

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/5/2015.
 */

object PotatoPicking {
	
	val POTATO = 312
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(POTATO) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			it.player().lock()
			it.delay(1)
			
			if (!it.player().inventory().full()) {
				it.animate(827)
				it.sound(2581)
				if (it.player().world().rollDie(16, 1)) {
					it.player().inventory().add(Item(5318), false)
					it.message("You pick a potato seed.")
				} else {
					it.player().inventory().add(Item(1942), false)
					it.message("You pick a potato.")
				}
				
				it.runGlobal(it.player().world()) @Suspendable { s ->
					s.ctx<World>().removeObj(obj)
					s.delay(50) // Half a minute
					s.ctx<World>().spawnObj(obj)
				}
			} else {
				it.message("You don't have room for this potato.")
			}
			
			it.player().unlock()
		}
	}
	
}