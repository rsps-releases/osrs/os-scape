package nl.bartpelle.veteres.content.areas.dungeons.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 2/24/2016.
 */

object TzhaarHurRoh {
	
	val HUR_ROH = 2185
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(HUR_ROH) @Suspendable {
			it.chatNpc("Can I help you JalYt-Ket-${it.player().name()}?", HUR_ROH, 554)
			when (it.options("What do you have to trade?", "What did you call me?", "No I'm fine thanks.")) {
				1 -> {
					it.player().world().shop(37).display(it.player())
				}
				2 -> {
					it.chatPlayer("What did you call me?", 614)
					it.chatNpc("Are you not JalYt-Ket?", HUR_ROH, 575)
					
					when (it.options("What's a 'JalYt-Ket'?", "I guess so...", "No I'm not!")) {
						1 -> {
							it.chatPlayer("What's a 'JalYt-Ket'?", 554)
							it.chatNpc("That what you are... you tough and strong no?", HUR_ROH, 575)
							it.chatPlayer("Well yes I suppose I am...", 575)
							it.chatNpc("Then you JalYt-Ket!", HUR_ROH, 567)
							
							when (it.options("What are you then?", "Thanks for explaining it.")) {
								1 -> {
									it.chatPlayer("What are you then?", 554)
									it.chatNpc("Silly JalYt, I am TzHaar-Hur, one of the crafters for<br>this city.", HUR_ROH, 568)
									it.chatNpc("There are the wise TzHaar-Mej who guide us, the<br>mighty TzHaar-Ket who guard us, and the swift<br>TzHaar-Xil who hunt for our food.", HUR_ROH, 590)
								}
								2 -> {
									it.chatPlayer("Thanks for explaining it.", 610)
								}
							}
						}
						2 -> {
							it.chatPlayer("I guess so...", 575)
							it.chatNpc("Well then, no problems.", HUR_ROH, 588)
						}
						3 -> {
							it.chatPlayer("No I'm not!", 614)
							it.chatNpc("What ever you say, crazy JalYt!", HUR_ROH, 592)
						}
					}
				}
				3 -> {
					it.chatPlayer("No I'm fine thanks.", 588)
				}
			}
		}
		
		r.onNpcOption2(HUR_ROH) {
			if (!it.player().world().realm().isPVP) {
				it.player().world().shop(37).display(it.player())
			}
		}
	}
	
}