package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/5/2015.
 */

object GemCutting {
	
	enum class BoltTips(val level: Int, val uncut: Int, val cut: Int, val anim: Int, val amt: Int, val exp: Double) {
		OPAL(11, 1609, 45, 890, 12, 1.5),
		JADE(26, 1611, 9187, 891, 12, 2.0),
		PEARL(41, 411, 46, 886, 6, 3.2),
		RED_TOPAZ(48, 1613, 9188, 892, 12, 3.9),
		SAPPHIRE(56, 1607, 9189, 888, 12, 4.0),
		EMERALD(58, 1605, 9190, 889, 12, 5.5),
		RUBY(63, 1603, 9191, 887, 12, 6.0),
		DIAMOND(65, 1601, 9192, 886, 12, 7.0),
		DRAGON(71, 1615, 9193, 885, 12, 8.2),
		ONYX(73, 6573, 9194, 2717, 24, 9.4)
	}
	
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//For each gem in the players inventory do the following..
		BoltTips.values().forEach { bolt ->
			if (bolt.uncut != -1) {
				val CHISEL = 1755
				
				//If the player uses an uncut gem with a chisel we..
				repo.onItemOnItem(bolt.uncut.toLong(), CHISEL.toLong(), s@ @Suspendable {
					//Check if the player has the required level to cut the gem.
					if (it.player().skills()[Skills.FLETCHING] < bolt.level) {
						it.messagebox("You need a Fletching level of ${bolt.level} or above to do that.")
						return@s
					}
					
					//Prompt the player with the # they'd like to cut
					var num = 1
					if (it.player().inventory().count(bolt.uncut) > 1) { // By default, 1, if we have more, ask how many.
						num = it.itemOptions(Item(bolt.uncut, 150), offsetX = 12)
					}
					
					while (num-- > 0) {
						//Check if the player still has an uncut gem and chisel.
						if (bolt.uncut !in it.player().inventory() || CHISEL !in it.player().inventory()) {
							it.message("You have run out of gems.")
							break
						}
						
						//Remove the uncut, add a cut, animate, send message, add experience and set delay.
						it.player().inventory() -= bolt.uncut
						it.player().inventory() += Item(bolt.cut, bolt.amt)
						it.player().animate(bolt.anim)
						it.message("You use your chisel to fletch small bolt tips.")
						it.addXp(Skills.FLETCHING, bolt.exp * bolt.amt)
						it.delay(4)
					}
				}
				)
			}
		}
	}
}