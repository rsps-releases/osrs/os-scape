package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.items.equipment.ZulrahItems
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.hitorigin.VenomOrigin
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 16/08/2016.
 *
 * Starting cycles is 8.
 * 8 'cycles' which execute every 34 game ticks (20.4 seconds)
 * Cycles go down -- per 34 ticks to 0 then continue forever.
 * Each cycle +2 more damage is dealt:
 * 8=6
 * 7=8
 * 6=10
 * 5=12
 * 4=14
 * 3=16
 * 2=18
 * 1=20 .. then on 1 it never reduces to 0, unless you die/take antivenom potion
 */
object Venom {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onLogin {
			val me: Player = it.player()
			if (me.attribOr<Int>(AttributeKey.VENOM_TICKS, 0) > 0) {
				me.clearattrib(AttributeKey.POISON_TICKS)
				me.timers().register(TimerKey.VENOM, 34)
				me.varps().varp(Varp.HP_ORB_COLOR, 1000000)
			}
		}
		
		repo.onTimer(TimerKey.VENOM) {
			val entity: Entity = it.ctx<Entity>()
			entity.timers().register(TimerKey.VENOM, 34) // restart
			
			val ticks = entity.attribOr<Int>(AttributeKey.VENOM_TICKS, 0)
			if (ticks > 0) {
				entity.putattrib(AttributeKey.VENOM_TICKS, Math.max(1, ticks - 1))
				entity.hit(VenomOrigin(), calcHit(ticks), 0, false).type(Hit.Type.VENOM).block(false).submit()
			} else if (ticks < 0) {
				// Negative value of ticks means we're immune. Increase up until 0 where the venom ends.
				entity.putattrib(AttributeKey.VENOM_TICKS, ticks + 1)
			}
		}
		
		repo.onWorldInit {
			val world: World = it.ctx<World>()
			
			// NVM FUCK IT YOLO LOL
			//if (!world.realm().isPVP) {
			// PvP world is cutting to the chase.. freedom. Venom puts ugly restrictions for deep wild pking.. need for anti-venom.
			// I personally decided it's not worth 'matching 07' in this one area.
			
			// ENABLED FOR ALL
			GameCommands.VENOM_VS_PLAYERS_ON = true
			ZulrahItems.CHARGING_ENABLED = true
			//}
		}
	}
	
	private fun calcHit(ticks: Int): Int {
		return Math.max(6, Math.min(20, 6 + ((8 - ticks) * 2)))
	}
	
	@JvmStatic fun cure(type: Int, e: Entity) {
		cure(type, e, true)
	}
	
	@JvmStatic fun cure(type: Int, e: Entity, msg: Boolean) {
		val venomVal = e.attribOr<Int>(AttributeKey.VENOM_TICKS, 0)
		if (type == 1) { // normal poison cure.
			if (venomVal > 0 && msg) {
				e.message("<col=145A32>The potion cures the venom, however you are still poisoned.")
			}
			e.putattrib(AttributeKey.VENOM_TICKS, 0)
			e.varps().varp(Varp.HP_ORB_COLOR, 0) // Reset and then send poison after
			e.poison(6, msg)
		} else if (type == 2) { // totally removes it, no poison
			if (venomVal > 0 && msg) {
				e.message("<col=145A32>The potion fully cures the venom.")
			}
			e.putattrib(AttributeKey.VENOM_TICKS, 0)
			e.varps().varp(Varp.HP_ORB_COLOR, 0) // Healthy
		} else if (type == 3) { // totally removes, plus immunity
			if (venomVal > 0 && msg) {
				e.message("<col=145A32>The potion cures the venom and provides you with 3 minutes of immunity.")
			} else if (msg) {
				e.message("<col=145A32>It grants you 3 minutes of immunity to venom.")
			}
			e.putattrib(AttributeKey.VENOM_TICKS, -9) // 3 minutes, aka 9 venom cycles of immunity
			e.varps().varp(Varp.HP_ORB_COLOR, 0) // Healthy
		}
	}
	
	@JvmStatic fun venomed(e: Entity): Boolean {
		return e.attribOr<Int>(AttributeKey.VENOM_TICKS, 0) > 0
	}
	
	/**
	 * Will reduce the charges/scales/darts in toxic items and convert to uncharged when it runs out, with a notification.
	 * Does not account for if target has a Serp helm or is a Npc Boss. Entity#venom checks that.
	 * @return True if venom can be applied to the victim.
	 */
	@JvmStatic fun attempt(source: Entity, target: Entity, style: CombatStyle, success: Boolean): Boolean {
		// Npcs don't use this method.
		if (source.isNpc)
			return false
		val wep = source.equipment().get(EquipSlot.WEAPON) ?: return false
		
		// Only venom weps for venom lol. Blowpipe charged, toxic trident used, toxic staff of the dead charged.
		if (wep.id() !in arrayOf(12926, 12899, 12904))
			return false
		
		// Weapon can't venom without scales in!
		val scales = if (wep.id() == 12899) wep.propertyOr(ItemAttrib.CHARGES, 0) else wep.propertyOr(ItemAttrib.ZULRAH_SCALES, 0)
		if (scales == 0)
			return false
		
		// Blowpipe
		if (style == CombatStyle.RANGE) {
			val attack_count: Int = wep.propertyOr(ItemAttrib.CONSECUTIVE_ATTACKS, 0) + 1
			if (attack_count == 3) {
				// Every 3 attacks with the item
				val newscales = Math.max(0, wep.property(ItemAttrib.ZULRAH_SCALES) - 2)
				wep.property(ItemAttrib.ZULRAH_SCALES, newscales)
				if (newscales == 0) {
					source.message("Your blowpipe has <col=FF0000>run out</col> of scales!")
				}
				
				val darts = wep.property(ItemAttrib.DARTS_COUNT)
				val newdarts = Math.max(0, darts - 1)
				// Darts drain on eco world only
				if (!source.world().realm().isPVP && PlayerCombat.notAvas(source as Player) && darts > 0) {
					wep.property(ItemAttrib.DARTS_COUNT, newdarts)
					if (newdarts < 1) {
						wep.property(ItemAttrib.DART_ITEMID, -1)
						source.message("Your blowpipe has <col=FF0000>run out</col> of darts!")
					}
				}
				
				if (newdarts < 1 && newscales < 1) {
					source.message("Your blowpipe has become <col=FF0000>uncharged</col> as it has run out of scales and darts.")
					source.equipment().set(EquipSlot.WEAPON, Item(ZulrahItems.EMPTY_TOXIC_BLOWPIPE))
				}
			}
			wep.property(ItemAttrib.CONSECUTIVE_ATTACKS, if (attack_count == 3) 0 else attack_count)
			
			if (success && source.world().rollDie(4, 1))
				return true
		} else {
			// Toxic trident has a special way of doing charges. Uses the CHARGS attrib instead of SCALES because a charge also consists of coins and 3 types of runes!
			if (wep.id() == 12899) {
				val newcharges = wep.property(ItemAttrib.CHARGES) - 1
				wep.property(ItemAttrib.CHARGES, newcharges)
				
				if (newcharges < 1) {
					source.message("Your trident has become <col=FF0000>uncharged</col> as it has run out of charges.")
					source.equipment().set(EquipSlot.WEAPON, Item(ZulrahItems.TOXIC_TRIDENT_EMPTY))
				}
			} else {
				val newscales = wep.property(ItemAttrib.ZULRAH_SCALES) - 1
				wep.property(ItemAttrib.ZULRAH_SCALES, newscales)
				
				if (newscales < 1) {
					source.message("Your toxic staff of the dead has become <col=FF0000>uncharged</col> as it has run out of scales.")
					source.equipment().set(EquipSlot.WEAPON, Item(ZulrahItems.TOXIC_SOTD_EMPTY))
				}
			}
			if (success && source.world().rollDie(4, 1)) {
				return true
			}
		}
		return false
	}
	
}