package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.events.tournament.EdgevilleTourny
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.items.ItemSets
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 8/24/2015.
 */
object bankobjects { // bank booths, banks
	
	@JvmStatic val bankObjectIds = intArrayOf(6944, 7409, 4483, 11744, 11748, 24101, 24347, 25808, 26707, 27249, 27251,
			27264, 27253, 27291, 18491, 21301, 10058, 6943, 27718, 27719, 27720, 27721, 7478, 10060, 14886, 10562,
			28861, 29321, 19051, 32666)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (bank in bankobjects.bankObjectIds) {
			repo.onObject(bank) @Suspendable { Bank.open(it.player(), it) }
			
			repo.onItemOnObject(bank, s@ @Suspendable {
				val player = it.player()
				var instance_key: InstancedMapIdentifier? = null
				player.world().allocator().active(player.tile()).ifPresent { map -> map.identifier.ifPresent { id -> instance_key = id } }
				val stopped = when (instance_key) {
					InstancedMapIdentifier.TOURNY_PLATEAU -> EdgevilleTourny.UNNOTE_ON_BANKS[1]
					InstancedMapIdentifier.TOURNY_GLADE -> EdgevilleTourny.UNNOTE_ON_BANKS[2]
					InstancedMapIdentifier.TOURNY_EDGE -> EdgevilleTourny.UNNOTE_ON_BANKS[0]
					InstancedMapIdentifier.TOURNY_TURRENTS -> EdgevilleTourny.UNNOTE_ON_BANKS[3]
					else -> false
				}
				if (stopped) {
					it.messagebox("Unnoting items on banks is disabled in this area.")
					return@s
				}
				val itemid: Int = player.attrib(AttributeKey.ITEM_ID)
				val slot: Int = player.attrib(AttributeKey.ITEM_SLOT)
				val def: ItemDefinition = player.world().definitions().get(ItemDefinition::class.java, itemid) ?: return@s
				
				noteLogic(it, itemid, slot, def)
			})
		}
		
	}
	
	@Suspendable fun noteLogic(it: Script, itemid: Int, slot: Int, def: ItemDefinition) {
		val player = it.player()
		
		if ((def.id == 995 || def.id == 13204) && it.player().world().realm().isPVP) {
			it.itemBox("You may not exchange this item in this world.", def.id)
			return
		}
		
		// Coins!
		if (def.id == 13307 || def.id == 995) {
			val coins = def.id
			val tokens = if (coins == 13307) 13215 else 13204
			
			val name = if (coins == 995) "coins" else "blood money"
			val tokensName = if (tokens == 13204) "platinum" else "blood"
			
			if (it.player().inventory().count(coins) < 1000) {
				it.messagebox("You need at least 1,000 $name to exchange them for $tokensName tokens.")
				return
			}
			
			if (it.optionsTitled("Exchange your $name for $tokensName tokens?", "Yes", "No") == 1) {
				val coinCount = it.player().inventory().count(coins)
				val amount = coinCount / 1000
				
				if (it.player().inventory().freeSlots() == 0 && coinCount - (amount * 1000) > 0) {
					it.message("You don't have enough room to carry the $tokensName tokens.")
					return
				}
				if (amount >= 1) {
					if (it.player().inventory().remove(Item(itemid, amount * 1000), false).success()) {
						it.player().inventory().add(Item(tokens, amount), true)
						it.doubleItemBox("The bank exchanges your coins for $tokensName tokens.", Item(itemid, amount * 1000), Item(tokens, amount))
					}
				}
				
			}
			// Plat tokens, Blood tokens
		} else if (def.id == 13204 || def.id == 13215) {
			val tokens = def.id
			val coins = if (tokens == 13215) 13307 else 995
			
			val name = if (coins == 995) "coins" else "blood money"
			val tokensName = if (tokens == 13204) "platinum" else "blood"
			
			if (it.optionsTitled("Exchange your $tokensName tokens for $name?", "Yes", "No") == 1) {
				val amount = it.player().inventory().count(tokens)
				
				if ((amount.toLong() * 1000.toLong()) + it.player().inventory().count(coins).toLong() >= Int.MAX_VALUE) {
					it.itemBox("You don't have enough inventory space to do this.", coins, amount)
					return
				}
				
				if (amount >= 1) {
					if (it.player().inventory().remove(Item(tokens, amount), false).success()) {
						it.player().inventory().add(Item(coins, amount * 1000), true)
						it.doubleItemBox("The bank exchanges your $tokensName tokens for $name.", Item(coins, amount * 1000), Item(tokens, amount))
					}
				}
				
			}
			// Other mechanics.. firstly, check sets.
		} else if (!bankobjects.openset(player, itemid, slot)) {
			
			// If open sets was NOT triggered, continue checking other possible logic:
			// It's linked to a noted item, but isn't a note itself
			if (!offerToNoteExchange(it, itemid)) {
				
				// If the above note logic was not triggered, check _other_ logic:
				if (def.noteModel > 0) { // If the item is a note
					if (it.optionsTitled("Un-note the bank notes?", "Yes", "No") == 1) {
						val avail = player.inventory().freeSlots()
						val count = player.inventory().count(itemid)
						val delete_entire_note_pile = avail >= count - 1
						//player.debug("%d, %d, %s %d", avail, count, delete_entire_note_pile, item)
						if (delete_entire_note_pile) {
							player.inventory().remove(Item(itemid, count), false)
							player.inventory().add(Item(itemid, count).unnote(player.world()), false)
							it.itemBox("The bank exchanges your banknotes for items.", def.notelink)
						} else { // Only delete what we can fit in
							val deleted = player.inventory().remove(Item(itemid, avail), false).success()
							val added = player.inventory().add(Item(itemid, avail).unnote(player.world()), false).success()
							//player.debug("%s, %s", deleted, added)
							it.itemBox("The bank exchanges your banknotes for items.", def.notelink)
						}
					}
				} else {
					// No more possible logic. Nothing to do!
					player.message("Nothing interesting happens.")
				}
			}
		}
	}
	
	@JvmStatic fun openset(player: Player, itemid: Int, slot: Int): Boolean {
		var x = false
		ItemSets.ITEM_SETS.forEach { k, data ->
			if (data.boxedset == itemid) {
				if (data.items.size - 1 > player.inventory().freeSlots()) {
					// -1 cos account for the set itself
					player.message("You don't have enough free space to open this set.")
				} else {
					player.inventory().remove(player.inventory().get(slot), true)
					data.items.forEach { peiceid ->
						player.inventory().add(Item(peiceid, 1), true)
					}
					player.message("You open the set.")
				}
				x = true
			}
		}
		return x
	}
	
	@JvmStatic @Suspendable fun offerToNoteExchange(it: Script, itemid: Int): Boolean {
		val player = it.player()
		val def: ItemDefinition = player.world().definitions().get(ItemDefinition::class.java, itemid) ?: return false
		if (def.notelink > 0 && def.noteModel < 1) {
			if (it.optionsTitled("Note the un-noted items?", "Yes", "No") == 1) {
				val count = player.inventory().count(itemid)
				if (player.inventory().remove(Item(itemid, count), false).success()) {
					player.inventory().add(Item(def.notelink, count), true)
					it.itemBox("You've exchanged $count items for notes.", itemid)
				}
			}
			return true
		}
		return false
	}
	
}


