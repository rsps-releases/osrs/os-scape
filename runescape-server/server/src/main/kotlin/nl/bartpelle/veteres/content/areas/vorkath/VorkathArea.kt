package nl.bartpelle.veteres.content.areas.vorkath

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository

object VorkathArea {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(31990) @Suspendable {
			// You climb over the ice. (bone bury anim) unattackable for ~5 sec then aggro player
			it.player().clearDamagers()
			val area = it.player().world().allocator().allocate(64, 64, Tile(2272, 4052)).get()
			area.set(0, 0, Tile(2240, 4032), Tile(2240 + 64, 4032 + 64), 0, true)
			val vorkat = Npc(8059, it.player().world(), area.bottomLeft() + Tile(29, 30))
			vorkat.spawnDirection(6) // Face south
			it.player().teleport(area.bottomLeft() + Tile(32, 20))
			it.player().world().registerNpc(vorkat)
		}
	}
	
}