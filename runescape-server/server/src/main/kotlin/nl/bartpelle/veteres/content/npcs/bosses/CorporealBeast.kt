package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations & Bart on 1/25/2016.
 */

object CorporealBeast {
	
	val dark_energy_core = 320
	val dark_core_gfx = 1826
	val stat_draining_ranged_gfx = 314
	val high_damage_magic_gfx = 316
	val splashing_magic_gfx = 315
	val splashing_magic_tile_gfx = 317
	val splashing_magic_attack_damage = 30
	val corporeal_beast_animation = 1680
	
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var attacked = false
		var target = EntityCombat.getTarget(it) ?: return@s
		
		//While the target is available to attack we..
		while (EntityCombat.targetOk(npc, target) && npc.hp() > 0 && PlayerCombat.canAttack(npc, target)) {
			//If we're not currently waiting on the attack timer me..
			if (EntityCombat.attackTimerReady(npc)) {
				
				//Check if we're able to melee our opponent.. and if we are, roll a die to smack em in the mouth
				if (EntityCombat.canAttackMelee(npc, target, false) && npc.world().rollDie(3, 1)) {
					melee_attack(npc, target)
					attacked = true
					
					//Check if we attack the player using a stat draining magic attack
				} else if (npc.world().rollDie(2, 1) && !attacked) {
					stat_draining_ranged_attack(it, npc, target)
					attacked = true
					
					//Check if we attack using our AOE splashing magic attack
				} else if (npc.world().rollDie(2, 1) && !attacked) {
					splashing_magic_attack(it, npc, target)
					attacked = true
					
					//Else we send the default attack, the HIGH magic damage attack
				} else if (!attacked) {
					high_magic_damage_attack(npc, target)
				}
				
				// ..and take a quick nap
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			//Apply the post hit logic
			attacked = false
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun stat_draining_ranged_attack(it: Script, npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(2, 2, 0).distance(target.tile())
		val delay = Math.max(1, (15 + (tileDist * 15)) / 30)
		
		//Send the projectile from the NPC -> Player
		npc.world().spawnProjectile(npc, target, stat_draining_ranged_gfx, 40, 25, 15, 15 * tileDist, 15, 10)
		
		//Check to see if we hit the player or not
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.RANGE, 1.0)) {
			target.hit(npc, npc.world().random(55), delay.toInt()).combatStyle(CombatStyle.RANGE)
		} else {
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.RANGE)
		}
		
		//Animate the NPC
		npc.animate(corporeal_beast_animation)
		
		it.runGlobal(target.world()) @Suspendable {
			it.delay(delay)
			
			//Check to see if we apply the 'special' effect.
			if (target.world().rollDie(3, 1)) {
				val reduction = target.world().random(3)
				//Decide if we reduce their magic or prayer.
				if (target.world().rollDie(2, 1)) {
					if (target.skills().level(Skills.MAGIC) < reduction) {
						target.skills().setLevel(Skills.MAGIC, 0)
					} else {
						target.skills().setLevel(Skills.MAGIC, target.skills().level(Skills.MAGIC) - reduction)
					}
					target.message("Your Magic has been slightly drained.")
				} else if (target.skills().level(Skills.PRAYER) > reduction) {
					if (target.skills().level(Skills.PRAYER) < reduction) {
						target.skills().setLevel(Skills.PRAYER, 0)
					} else {
						target.skills().setLevel(Skills.PRAYER, target.skills().level(Skills.PRAYER) - reduction)
					}
					target.message("Your Prayer has been slightly drained.")
				}
			}
		}
	}
	
	fun melee_attack(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, EntityCombat.randomHit(npc))
		} else {
			target.hit(npc, 0) // Uh-oh, that's a miss.
		}
		//Animate the NPC
		npc.animate(npc.attackAnimation())
	}
	
	//This attack is a high damage attack that can deal up to 65 damage - 50 if they're using protection against magic.
	fun high_magic_damage_attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(2, 2, 0).distance(target.tile())
		val delay = Math.max(1, (15 + (tileDist * 15)) / 30)
		val damage = if (target.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1) 50 else 60
		
		//Send the projectile from the NPC -> Player
		npc.world().spawnProjectile(npc, target, high_damage_magic_gfx, 40, 25, 15, 15 * tileDist, 15, 10)
		
		//Check to see if we hit the player or not
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, npc.world().random(damage), delay.toInt()).combatStyle(CombatStyle.GENERIC)
		else
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.GENERIC)
		
		//Animate the NPC
		npc.animate(corporeal_beast_animation)
	}
	
	fun splashing_magic_attack(it: Script, npc: Npc, target: Entity) {
		val x = target.tile().x //The target's x tile
		val z = target.tile().z //The target's z tile
		
		val random_one = target.world().random(1)
		val random_two = target.world().random(2)
		
		//Handle the initial spell
		val initial_splash = Tile(x, z)
		val initial_splash_distance = npc.tile().distance(initial_splash) / 2
		val initial_splash_delay = Math.max(1, (20 + initial_splash_distance * 12) / 30)
		
		//Send the projectile from the NPC -> players tile
		npc.world().spawnProjectile(npc.tile().transform(2, 2, 0), initial_splash, splashing_magic_gfx, 40, 0, initial_splash_delay, 28 * initial_splash_distance, 15, 10)
		
		//Send the tile graphic
		target.world().tileGraphic(splashing_magic_tile_gfx, initial_splash, 1, 28 * initial_splash_distance)
		
		//Animate the NPC
		npc.animate(corporeal_beast_animation, initial_splash_delay)
		
		it.runGlobal(target.world()) @Suspendable {
			//Create a delay before checking if the player is on the initial tile
			it.delay(initial_splash_distance)
			
			//Check to see if the player's on the initial tile
			if (target.tile().inSqRadius(initial_splash, 1) && target.tile().inArea(2973, 4404, 3005, 4365))
				target.hit(npc, target.world().random(splashing_magic_attack_damage)).combatStyle(CombatStyle.GENERIC)
		}
		
		//Handle the first splash
		val splash_one = Tile(initial_splash.x + 2 + random_one, initial_splash.z + 2 + random_one)
		val splash_one_distance = initial_splash.distance(splash_one)
		val splash_one_delay = Math.max(1, (20 + splash_one_distance * 12) / 30)
		
		//Handle the second splash
		val splash_two = Tile(initial_splash.x, initial_splash.z + 2 + random_one)
		val splash_two_distance = initial_splash.distance(splash_two)
		val splash_two_delay = Math.max(1, (20 + splash_two_distance * 12) / 30)
		
		//Handle the third splash
		val splash_three = Tile(initial_splash.x + 2 + random_one, initial_splash.z + random_two)
		val splash_three_distance = initial_splash.distance(splash_three)
		val splash_three_delay = Math.max(1, (20 + splash_three_distance * 12) / 30)
		
		//Handle the fourth splash
		val splash_four = Tile(initial_splash.x + 2 + random_one, initial_splash.z - 2 + random_one)
		val splash_four_distance = initial_splash.distance(splash_four)
		val splash_four_delay = Math.max(1, (20 + splash_four_distance * 12) / 30)
		
		//Handle the fifth splash
		val splash_five = Tile(initial_splash.x - 2 + random_one, initial_splash.z + 2 + random_two)
		val splash_five_distance = initial_splash.distance(splash_five)
		val splash_five_delay = Math.max(1, (20 + splash_five_distance * 12) / 30)
		
		//Handle the sixth splash
		val splash_six = Tile(initial_splash.x - 2 + random_two, initial_splash.z - 2 + random_two)
		val splash_six_distance = initial_splash.distance(splash_six)
		val splash_six_delay = Math.max(1, (20 + splash_six_distance * 12) / 30)
		
		it.runGlobal(target.world()) @Suspendable {
			
			//Create a delay before sending the splash projectiles
			it.delay(initial_splash_distance)
			
			//Send the projectiles
			npc.world().spawnProjectile(initial_splash, splash_one, splashing_magic_gfx, 0, 0, splash_one_delay, 28 * splash_one_distance, 15, 10)
			npc.world().spawnProjectile(initial_splash, splash_two, splashing_magic_gfx, 0, 0, splash_two_delay, 28 * splash_two_distance, 15, 10)
			npc.world().spawnProjectile(initial_splash, splash_three, splashing_magic_gfx, 0, 0, splash_three_delay, 28 * splash_three_distance, 15, 10)
			npc.world().spawnProjectile(initial_splash, splash_four, splashing_magic_gfx, 0, 0, splash_four_delay, 28 * splash_four_distance, 15, 10)
			npc.world().spawnProjectile(initial_splash, splash_five, splashing_magic_gfx, 0, 0, splash_five_delay, 28 * splash_five_distance, 15, 10)
			npc.world().spawnProjectile(initial_splash, splash_six, splashing_magic_gfx, 0, 0, splash_six_delay, 28 * splash_six_distance, 15, 10)
			
			//Send the tile graphic
			target.world().tileGraphic(splashing_magic_tile_gfx, splash_one, 1, 28 * splash_one_distance)
			target.world().tileGraphic(splashing_magic_tile_gfx, splash_two, 1, 28 * splash_two_distance)
			target.world().tileGraphic(splashing_magic_tile_gfx, splash_three, 1, 28 * splash_three_distance)
			target.world().tileGraphic(splashing_magic_tile_gfx, splash_four, 1, 28 * splash_four_distance)
			target.world().tileGraphic(splashing_magic_tile_gfx, splash_five, 1, 28 * splash_five_distance)
			target.world().tileGraphic(splashing_magic_tile_gfx, splash_six, 1, 28 * splash_six_distance)
			
			//If any player on the world is on the tile -> deal damage.
			hitAfterDelay(it, target.world(), splash_one, splashing_magic_attack_damage, npc, splash_one_delay)
			hitAfterDelay(it, target.world(), splash_two, splashing_magic_attack_damage, npc, splash_two_delay)
			hitAfterDelay(it, target.world(), splash_three, splashing_magic_attack_damage, npc, splash_three_delay)
			hitAfterDelay(it, target.world(), splash_four, splashing_magic_attack_damage, npc, splash_four_delay)
			hitAfterDelay(it, target.world(), splash_five, splashing_magic_attack_damage, npc, splash_five_delay)
			hitAfterDelay(it, target.world(), splash_six, splashing_magic_attack_damage, npc, splash_six_delay)
		}
	}
	
	@Suspendable fun hitAfterDelay(it: Script, world: World, tile: Tile, dmg: Int, npc: Npc, delay: Int) {
		it.runGlobal(world) @Suspendable {
			world.players().forEachKt({ p ->
				if (p.tile().inSqRadius(tile, 1) && p.tile().inArea(2973, 4404, 3005, 4365))
					p.hit(npc, p.world().random(dmg), 1).combatStyle(CombatStyle.GENERIC)
			})
		}
	}
}