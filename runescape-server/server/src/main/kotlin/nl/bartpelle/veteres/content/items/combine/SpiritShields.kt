package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 3/14/2016.
 */
object SpiritShields {
	
	val HOLY_ELIXIR = 12833
	val SPIRIT_SHIELD = 12829
	val BLESSED_SPIRIT_SHIELD = 12831
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnItem(HOLY_ELIXIR, SPIRIT_SHIELD) @Suspendable {
			it.player().inventory().remove(Item(HOLY_ELIXIR), true)
			it.player().inventory().remove(Item(SPIRIT_SHIELD), true)
			it.player().inventory().add(Item(BLESSED_SPIRIT_SHIELD), true)
			it.itemBox("The spirit shield glows an eerie holy glow.", BLESSED_SPIRIT_SHIELD)
		}
	}
	
	@Suspendable fun onAnvil(it: Script) {
		val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
		
		if (it.player().skills().xpLevel(Skills.PRAYER) < 90 || it.player().skills().xpLevel(Skills.SMITHING) < 85) {
			it.message("You don't have the skills required to make this. You need 85 Smithing and 90 Prayer.")
			return
		}
		
		it.player().animate(898)
		it.delay(6)
		it.player().animate(898)
		it.delay(6)
		
		if (it.player().inventory().hasAll(BLESSED_SPIRIT_SHIELD, item)) {
			it.player().inventory().remove(Item(BLESSED_SPIRIT_SHIELD), true)
			it.player().inventory().remove(Item(item), true)
			it.player().inventory().add(Item(item - 2), true)
		}
		
		it.itemBox("You successfully combine the " + Item(item).name(it.player().world()) + " with the shield.", item - 2)
	}
	
}