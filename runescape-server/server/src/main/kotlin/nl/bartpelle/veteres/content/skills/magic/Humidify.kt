package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/7/2016.
 */
object Humidify {
	
	val VESSELS = mapOf(
			Pair(229, 227), // Vial of water
			Pair(1925, 1929), // Bucket
			Pair(434, 1761), // Soft clay
			Pair(1935, 1937), // Jug of water
			
			
			// Watering cans
			Pair(5331, 5340),
			Pair(5333, 5340),
			Pair(5334, 5340),
			Pair(5335, 5340),
			Pair(5336, 5340),
			Pair(5337, 5340),
			Pair(5338, 5340),
			Pair(5339, 5340)
	)
	
	val RUNES = arrayOf(Item(9075, 1), Item(555, 3), Item(554, 1))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(218, 101) @Suspendable {
			humidify(it)
		}
	}
	
	@Suspendable fun humidify(it: Script) {
		if (it.player().locked()) {
			return
		}
		
		if (it.player().skills()[Skills.MAGIC] < 68) {
			it.message("Your Magic level is not high enough for this spell.")
			return
		}
		
		// Count vessels..
		if (!hasVessel(it.player)) {
			it.player().message("You have nothing in your inventory that this spell can humidify.")
			return
		}

		if (!MagicCombat.has(it.player(), RUNES, true)) {
			return
		}
		
		it.player().lock()
		it.addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 130.0 else 65.0)
		it.player().animate(6294)
		it.player().graphic(1061, 72, 0)
		it.delay(4)
		
		// Fill vessels :D
		for (i in 0..27) {
			val item = it.player.inventory()[i] ?: continue
			if (VESSELS[item.id()] != null) {
				it.player().inventory()[i] = Item(VESSELS[item.id()]!!)
			}
		}
		
		it.player().unlock()
	}
	
	fun hasVessel(player: Player): Boolean {
		for (item in player.inventory()) {
			if (item != null && VESSELS[item.id()] != null) {
				return true
			}
		}
		
		return false
	}
	
}