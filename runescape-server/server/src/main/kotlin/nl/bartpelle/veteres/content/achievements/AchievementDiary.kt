package nl.bartpelle.veteres.content.achievements

import co.paralleluniverse.fibers.Suspendable
import com.google.common.base.Joiner
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.achievements.pvp.PVPAchievements
import nl.bartpelle.veteres.content.achievements.pvp.PVPDiaryHandler
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Misc
import java.text.DecimalFormat
import java.util.*

/**
 * The central class for managing achievement data.
 * Created by Mack on 11/22/2017.
 */
object AchievementDiary {

	const val ACHIEVEMENT_CAPE = 13069
	const val ACHIEVEMENT_HOOD = 13070
	const val ENABLED = false

	private var diaryHandler: AchievementDiaryHandler? = null
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onButton(399, 1, @Suspendable {
			if (ENABLED) {
				init(it)
			} else {
				it.message("The achievment diaries are currently unavailable.")
			}
		})
	}
	
	@Suspendable fun init(it: Script) {
		displayTasks(it)
	}
	
	@Suspendable fun displayTasks(it: Script) {
		if (!ENABLED) {
			return
		}
		
		when (OptionList.display(it, "Achievement Diary Tasks", listOf("Easy", "Medium", "Hard"))) {
			0 -> {
				displayTasksFor(it.player(), PVPAchievements.EASY.achievements)
			}
			1 -> {
				displayTasksFor(it.player(), PVPAchievements.MEDIUM.achievements)
			}
			2 -> {
				displayTasksFor(it.player(), PVPAchievements.HARD.achievements)
			}
		}
	}

	/**
	 * Checks if the collection contains the achievment.
	 */
    fun contains(player: Player, achievement: Achievement): Boolean {
		return (cachedAchievements(player).containsKey(achievement.id()))
	}

    /**
     * Updates the player's save field key with the updated collection.
     */
    fun recacheAchievements(player: Player, map: MutableMap<String, String>) {
        val saveField = Joiner.on(",").withKeyValueSeparator(":").join(map)
        player.putattrib(AttributeKey.ACHIEVEMENT_TASK_TRACKER, saveField)
    }

    /**
     * Gets the map of achievements the player has started.
     */
    fun cachedAchievements(player: Player): MutableMap<String, String> {
        return (player.attribOr(AttributeKey.ACHIEVEMENT_PROGRESSION_MAP, null))
    }
	
	fun displayTasksFor(player: Player, achievements: Set<Achievement>) {
		QuestGuide.clear(player)
		QuestGuide.addQuestGuideHeader(player, "<u><col=850000>Achievement Diary</col></u>")
		
		val ourCompletion = achievements.stream().filter({a -> a.completed(player)}).toArray().size
		val totalCompletion = (ourCompletion / achievements.size).toDouble().times(100)
		var pointer = 4
		
		QuestGuide.addScrollLine(player, pointer, "<u>Diary Completion: ${Misc.color(totalCompletion)} ${DecimalFormat("##.#").format(totalCompletion)} % complete </col>")
		
		pointer++
		
		for (achievement in achievements) {
			val completion = achievement.displayFormattedProgression(player)
			var achievementText = "-${achievement.description()} [${achievement.taskProgressFor(player)}/${achievement.limit()}] ${Misc.color(completion.toDouble())} $completion% complete</col>"
			
			if (achievement.completed(player)) {
				achievementText = "<str>-${achievement.description()} [${achievement.taskProgressFor(player)}/${achievement.limit()}] ${Misc.color(completion.toDouble())} $completion% complete</col></str>"
			}
			
			QuestGuide.addScrollLine(player, pointer++, achievementText)
		}
		
		QuestGuide.open(player)
	}

	/**
	 * Gets the diary handler based on the sevice id the player is playing on.
	 */
	fun diaryHandler(player: Player): AchievementDiaryHandler {
		if (diaryHandler == null) {
			if (player.world().realm().isPVP) {
				diaryHandler = PVPDiaryHandler()
			} else {
				//TODO otherwise return eco handler
			}
 		}
		return diaryHandler!!
	}
}