package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 23/12/2015.
 */

object DarkWarriorFortress {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(14745) @Suspendable {
			//bottom floor, up
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3036 && obj.tile().z == 3638) {
				//NE
				Ladders.ladderUp(it, it.player().tile().transform(0, 0, 1), true)
			}
			if (obj.tile().x == 3036 && obj.tile().z == 3625) {
				//se
				Ladders.ladderUp(it, it.player().tile().transform(0, 0, 1), true)
			}
			if (obj.tile().x == 3022 && obj.tile().z == 3625) {
				//sw
				Ladders.ladderUp(it, it.player().tile().transform(0, 0, 1), true)
			}
		}
		
		r.onObject(14747) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
			if (option == 1) {
				//TODO chatbox
			} else if (option == 2) {
				//up
				if (obj.tile().x == 3036 && obj.tile().z == 3638) {
					//NE
					Ladders.ladderUp(it, it.player().tile().transform(0, 0, 1), true)
				}
				if (obj.tile().x == 3036 && obj.tile().z == 3625) {
					//se
					Ladders.ladderUp(it, it.player().tile().transform(0, 0, 1), true)
				}
				if (obj.tile().x == 3022 && obj.tile().z == 3625) {
					//sw
					Ladders.ladderUp(it, it.player().tile().transform(0, 0, 1), true)
				}
			} else if (option == 3) {
				//down
				if (obj.tile().x == 3036 && obj.tile().z == 3638) {
					//NE
					Ladders.ladderDown(it, it.player().tile().transform(0, 0, -1), true)
				}
				if (obj.tile().x == 3036 && obj.tile().z == 3625) {
					//se
					Ladders.ladderDown(it, it.player().tile().transform(0, 0, -1), true)
				}
				if (obj.tile().x == 3022 && obj.tile().z == 3625) {
					//sw
					Ladders.ladderDown(it, it.player().tile().transform(0, 0, -1), true)
				}
			}
		}
		
		r.onObject(14746) @Suspendable {
			//this object is the ladder found on the very top floor. goes down.
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3036 && obj.tile().z == 3638) {
				//NE
				Ladders.ladderDown(it, it.player().tile().transform(0, 0, -1), true)
			}
			if (obj.tile().x == 3036 && obj.tile().z == 3625) {
				//se
				Ladders.ladderDown(it, it.player().tile().transform(0, 0, -1), true)
			}
			if (obj.tile().x == 3022 && obj.tile().z == 3625) {
				//sw
				Ladders.ladderDown(it, it.player().tile().transform(0, 0, -1), true)
			}
		}
	}
	
}
