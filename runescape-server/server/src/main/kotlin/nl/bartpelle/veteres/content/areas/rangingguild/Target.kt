package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.combat.CombatSounds
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.EquipmentInfo
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Situations on 2017-03-12.
 */

object Target {
	
	enum class TargetHits(val text: String, val exp: Double, val points: Int, val arrow: IntArray) {
		BULLSEYE("Bulls-Eye!", 50.0, 100, intArrayOf(0)),
		YELLOW("Hit Yellow!", 50.0, 50, intArrayOf(1)),
		RED("Hit Red!", 25.0, 30, intArrayOf(2, 3, 4)),
		BLUE("Hit Blue!", 10.0, 20, intArrayOf(5, 6, 7, 8)),
		BLACK("Hit Black!", 5.0, 10, intArrayOf(9, 10)),
		MISSED("Missed!", 0.0, 0, intArrayOf(11))
	}
	
	val TARGET = 11663
	val shootingArea = Area(2669, 3415, 2674, 3420)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(TARGET, s@ @Suspendable {
			val player = it.player()
			val shotsFired = player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS)
			val insideShootingRange = player.tile().inArea(shootingArea)
			
			//Does our player have any shots left?
			if (shotsFired == 0) {
				when (insideShootingRange) {
					true -> talkToJudge(it, "Sorry, you may only use the targets for the<br>competition, not for practicing.", 589)
					false -> player.message("Maybe you should ask before using those.")
				}
				return@s
			} else if (shotsFired == 11) {
				val finalScore = player.varps().varp(Varp.RANGING_GUILD_TARGET_POINTS)
				player.inventory().addOrDrop(Item(1464, (finalScore * 0.1).toInt()), player)
				player.varps().varp(Varp.RANGING_GUILD_TARGET_POINTS, 0)
				player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS, 0)
				talkToJudge(it, "Well done. Your score is: $finalScore. For that score you will<br>receive ${(finalScore * 0.1).toInt()} Archery tickets.", 589)
			} else {
				if (insideShootingRange) {
					val weapon: Item? = player.equipment().get(3)
					val weaponName: String = weapon!!.definition(player.world()).name.toLowerCase()
					val ammoId = player.equipment()[EquipSlot.AMMO]?.id() ?: -1
					val ammoName = Item(ammoId).definition(player.world())?.name ?: ""
					
					//Does our player have a bow equipped?
					if (weapon == null) {
						talkToJudge(it, "You've no longbow on you to take part in the competition.", 588)
						return@s
					} else if (!(weaponName.contains("longbow") || weaponName.contains("dark bow") || weaponName.contains("seercull") || weaponName.contains("crystal bow"))) {
						talkToJudge(it, "You need a longbow to take part in the competition.", 588)
						return@s
					} else if (!ammoName.equals("Bronze arrow")) {
						talkToJudge(it, "I suggest you use the 10 bronze arrows I gave you.", 588)
						return@s
					} else {
						val target = it.interactionObject()
						val tileDist = player.tile().distance(target.tile())
						var cyclesPerTile = 5
						
						player.lock()
						player.faceObj(target)
						it.animate(EquipmentInfo.attackAnimationFor(player))
						player.graphic(19, 96, 0)
						player.world().spawnSound(player.tile(), CombatSounds.weapon_attack_sounds(it), 0, 10)
						player.world().spawnProjectile(player.tile(), target.tile(), 10, 40, 30, 41, cyclesPerTile * tileDist, 15, 11)
						player.message("You carefully aim at the target...")
						val ammoItem = player.equipment()[EquipSlot.AMMO]!!
						player.equipment()[EquipSlot.AMMO] = Item(ammoItem.id(), ammoItem.amount() - 1)
						it.delay(3)
						displayTargetView(it)
						player.unlock()
					}
				} else {
					player.message("You should probably be behind the hay bales.")
				}
			}
		})
	}
	
	@Suspendable fun talkToJudge(it: Script, message: String, anim: Int) {
		val player = it.player()
		
		it.player().world().npcs().forEachInAreaKt(shootingArea, { judge ->
			if (judge.id() == CompetitionJudge.COMPETITION_JUDGE) {
				player.face(judge)
				judge.faceTile(player.tile())
				it.chatNpc(message, judge.id(), anim)
			}
		})
	}
	
	@Suspendable fun displayTargetView(it: Script) {
		val player = it.player()
		val rangeLevel = player.skills().level(Skills.RANGED)
		var shot = TargetHits.MISSED
		
		//Just made something up.. similar results to 07 tho! : )
		if (player.world().rollDie(120, rangeLevel)) {
			shot = TargetHits.BULLSEYE
		} else if (player.world().rollDie(130, rangeLevel)) {
			shot = TargetHits.RED
		} else if (player.world().rollDie(140, rangeLevel)) {
			shot = TargetHits.BLUE
		} else if (player.world().rollDie(150, rangeLevel)) {
			shot = TargetHits.BLACK
		}
		
		player.interfaces().sendMain(325)
		player.interfaces().text(325, 123, shot.text)
		it.addXp(Skills.RANGED, shot.exp)
		player.varps().varp(Varp.RANGING_GUILD_TARGET_POINTS, player.varps().varp(Varp.RANGING_GUILD_TARGET_POINTS) + shot.points)
		player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS, player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS) + 1)
		player.varps().varp(Varp.RANGING_GUILD_ARROW, shot.arrow.random())
		
	}
}
