package nl.bartpelle.veteres.content.combat

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.combat.melee.MeleeSpecialAttacks
import nl.bartpelle.veteres.content.combat.ranged.RangedSpecialAttacks
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.graphic
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.steroids.RangeStepSupplier
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import java.lang.ref.WeakReference

/**
 * Created by Bart on 3/12/2016.
 */
object SpecialAttackButton {
	
	@JvmStatic @ScriptMain fun spectrigger(repo: ScriptRepository) {
		
		repo.onTimer(TimerKey.SPEAR) @Suspendable {
			val moveList = PlayerCombat.pendingSpears(it.entity())
			val move = moveList.poll()
			if (move == null) {
				it.entity().unlock()
				it.entity().clearattrib(AttributeKey.SPEAR_MOVES)
				it.entity().timers().cancel(TimerKey.SPEAR)
			} else {
				it.entity().graphic(254, 100, 0)
				PlayerCombat.dragonSpearMovement(move, it.entity())
				it.entity().timers()[TimerKey.SPEAR] = 4
			}
		}
		
		repo.onButton(593, 30, s@ @Suspendable {
			if (it.player().dead() || it.player().stunned() || it.player().locked()) {
				return@s
			}
			it.player().varps()[Varp.SPECIAL_ENABLED] = if (it.player().varps()[Varp.SPECIAL_ENABLED] == 1) 0 else 1
			
			if (it.player().varps()[Varp.SPECIAL_ENABLED] == 1) {
				val weapon = it.player().equipment()[EquipSlot.WEAPON]?.id() ?: -1
				
				if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_SPK_ATK)) {
					it.player().message("Special attacks have been disabled for this duel.")
					it.player().varps()[Varp.SPECIAL_ENABLED] = 0
					return@s
				}
				
				when (weapon) {
					35 -> doExcalibur(it)
					11920, 13243, 20014 -> doDragonPickaxe(it, weapon) // Dragon, 3rd age, infernal
					6739, 20011, 13241 -> doDragonHatchet(it, weapon)
					11791, 12902, 12904 -> doStaffOfTheDead(it, false)
					22296 -> doStaffOfTheDead(it, true)
					4153, 12848 -> MeleeSpecialAttacks.queueGraniteMaulSpecial(it.player())
					20849 -> doDragonThrowingAxe(it)
					21028 -> executeDragonHarpoon(it)
					1377 -> doDragonBattleaxe(it)
				}
			} else {
				val weapon = it.player().equipment()[EquipSlot.WEAPON]?.id() ?: -1
				if(weapon == 4153 || weapon == 12848) {
					MeleeSpecialAttacks.queueGraniteMaulSpecial(it.player())
				}
			}
		})
	}

	private fun doDragonBattleaxe(it: Script) {
		if (PlayerCombat.takeSpecialEnergy(it.player(), 100)) {
			it.player().sync().shout("Raarrrrrgggggghhhhhhhh!")
			var drained = 0
			for (i in intArrayOf(0, 1, 4, 6)) {
				drained += Math.ceil(it.player().skills().level(i) / 10.0).toInt()
				it.player().skills().setLevel(i, it.player().skills().level(i) - Math.floor(it.player().skills().level(i) / 10.0).toInt())
			}
			val boost = it.player().skills().level(Skills.STRENGTH) + 10 + (drained / 4) // you can only ever get to 119 lol
			it.player().skills().setLevel(Skills.STRENGTH, if (boost >= 119) 120 else boost) // fuck this formula wiki must be wrong
			it.graphic(246)
			it.animate(1056)
		}
	}

	private fun doDragonHatchet(it: Script, weapon: Int) {
		if (PlayerCombat.takeSpecialEnergy(it.player(), 100)) {
			it.player().sync().shout("Smashing!")
			it.player().skills().alterSkill(Skills.WOODCUTTING, 3)
			//it.animate(if (weapon == 11920) 7138 else 3410) // TODO anim
		}
		it.player().varps()[Varp.SPECIAL_ENABLED] = 0;
	}
	
	private fun doGraniteMaul(it: Script) {
		if (it.player().varps()[Varp.SPECIAL_ENERGY] < 500) {
			it.message("You don't have enough power left.")
			it.player().varps()[Varp.SPECIAL_ENABLED] = 0
			return
		}
		val player: Player = it.player()
		
		// Define our target as last entity we attacked
		val ref: WeakReference<Entity> = player.attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: WeakReference<Entity>(null)
		val target: Entity = ref.get() ?: return
		
		// Check if target is attackable
		if (target.isNpc) {
			if ((target as Npc).combatInfo() == null) {
				player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
				return
			}
		}
		
		if (target.dead() || !PlayerCombat.canAttack(player, target) || player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) != 0) {
			player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
			return
		}
		
		// Make sure our target can be attacked in this location.
		val wereInAttackable = if (target.isNpc) true else player.attribOr<Boolean>(AttributeKey.ATTACK_OP, false)
		val inattackable: Boolean = if (target.isNpc) true else target.attribOr<Boolean>(AttributeKey.ATTACK_OP, false)
		if (!inattackable || !wereInAttackable) {
			player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
			return
		}
		
		var tile = player.tile()
		if (!player.touches(target)) {
			// Don't face before this, facing updates this attribute. Check it first. Its a solid-proof way of ensuring you've clicked the target.
			val facingTarg = player.attribOr<Int>(AttributeKey.LAST_FACE_ENTITY_IDX, -1) == target.index() + (if (target.isPlayer) 32768 else 0)
			
			// Path queue empty means we're not pathing towards the target (which would have been triggered in playercombat)
			// Gmaul doesn't make you move to target, you have to click on ur target to make the move. Gmaul just triggers if in range at the time.
			if (player.frozen() || player.stunned()) {
				player.message("I can't reach that!")
				player.sound(154)
				return
			/*} else if (!facingTarg) {
				// The 07 mechanic jmods fucked up/changed from original 2007 which means you have to click ur target for it to trigger
				// The special button doesn't just auto-attack if you're not standing right next to the target.
				player.message("lolll")
				player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
				return*/
			} else {
				// If the target is an NPC and is a non-stacking one, we must ensure we don't add the step yet.
				// Not frozen? Try to move closer.
				if (target is Npc && target.combatInfo()?.unstacked ?: false) {
					PlayerCombat.moveCloserNoPeek(player, target, tile)
				} else {
					// We need to use predictive movement to see if we'll be in range for melee for 2 steps. However, before this check
					// we ensure we're facing the target (we clicked on them). Gmaul doesn't do the pathing for you, you make your player walk towards before it can activate.
					tile = PlayerCombat.moveCloser(player, target, tile)
					if (!player.touches(target, tile)) {
						// Not there yet
						player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
						return
					}
				}
			}
		}
		player.face(target) // Look in the target's direction. Scary!
		
		// Do the special now we're confirmed in distance etc.
		MeleeSpecialAttacks.executeGmaulSpec(it, target)
	}
	
	private fun doStaffOfTheDead(it: Script, staffOfLight: Boolean) {
		if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_MAGIC)) {
			it.player().message("Magic attacks have been disabled for this duel.")
			it.player().varps()[Varp.SPECIAL_ENABLED] = 0
			return
		}
		
		if (ClanWars.checkRule(it.player(), CWSettings.SPECIAL_ATTACKS)) {
			it.player().message("Staff of the dead has been disabled for this clan war.")
			it.player().varps()[Varp.SPECIAL_ENABLED] = 0
			return
		}
		
		if (ClanWars.checkRule(it.player(), CWSettings.SPECIAL_ATTACKS, 2)) {
			it.player().message("Special attacks have been disabled for this clan war.")
			it.player().varps()[Varp.SPECIAL_ENABLED] = 0
			return
		}
		
		if (ClanWars.checkRule(it.player(), CWSettings.MAGIC, 3)) {
			it.player().message("Magic attacks have been disabled for this clan war.")
			return
		}
		
		//Toxic & Regular Staff of the Dead
		if (PlayerCombat.takeSpecialEnergy(it.player(), 100)) {
			it.player().animate(if(staffOfLight) 7967 else 7083)
			it.player().graphic(if(staffOfLight) 1516 else 1228, 300, 0)
			it.player().timers().addOrSet(TimerKey.SOTD_DAMAGE_REDUCTION, 60)
			it.player().message("<col=3d5d2b>Spirits of deceased evildoers offer you their protection.")
		}
		it.player().varps()[Varp.SPECIAL_ENABLED] = 0
	}
	
	private fun doDragonPickaxe(it: Script, weapon: Int) {
		if (PlayerCombat.takeSpecialEnergy(it.player(), 100)) {
			it.player().sync().shout("Smashing!")
			it.player().skills().alterSkill(Skills.MINING, 3)
			it.animate(if (weapon == 11920) 7138 else 3410)
		}
		it.player().varps()[Varp.SPECIAL_ENABLED] = 0
	}
	
	private fun doExcalibur(it: Script) {
		if (PlayerCombat.takeSpecialEnergy(it.player(), 100)) {
			it.player().skills().alterSkill(Skills.DEFENCE, 8)
			it.animate(1168)
			it.player().graphic(247)
			it.player().timers()[TimerKey.COMBAT_ATTACK] = 4
		}
		it.player().varps()[Varp.SPECIAL_ENABLED] = 0
	}
	
	private fun doDragonThrowingAxe(it: Script) {
		if (it.player().varps()[Varp.SPECIAL_ENERGY] < 250) {
			it.message("You don't have enough power left.")
			it.player().varps()[Varp.SPECIAL_ENABLED] = 0
			return
		}
		if (it.player().timers().has(TimerKey.THROWING_AXE_DELAY)) {
			return
		}
		val player: Player = it.player()
		val ref: WeakReference<Entity> = player.attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: WeakReference<Entity>(null)
		val target: Entity = ref.get() ?: return
		
		// Don't face before this, facing updates this attribute. Check it first. Its a solid-proof way of ensuring you've clicked the target.
		val facingTarg = player.attribOr<Int>(AttributeKey.LAST_FACE_ENTITY_IDX, -1) == target.index() + (if (target.isPlayer) 32768 else 0)
		// You've gotta click your target, this special doesn't magically grab whoever you last attacked, nah nigga you gotta put that effort IN.
		if (!facingTarg) {
			return
		}
		it.player().face(target) // Look in the target's direction. Scary!
		
		// Make sure we can attack the target.
		if (target.isNpc) {
			if ((target as Npc).combatInfo() == null) {
				return
			}
		}
		if (target.dead() || !PlayerCombat.canAttack(player, target) || player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) != 0 || player.stunned()) {
			return
		}
		val wereInAttackable = if (target.isNpc) true else player.attribOr<Boolean>(AttributeKey.ATTACK_OP, false)
		val inattackable: Boolean = if (target.isNpc) true else target.attribOr<Boolean>(AttributeKey.ATTACK_OP, false)
		if (!inattackable || !wereInAttackable) {
			return
		}
		
		var dist = 0
		if (player.sync().secondaryStep() != -1 && target.sync().secondaryStep() != -1)
			dist = 3
		else if (player.sync().secondaryStep() != -1)
			dist = 1
		
		// Thrownaxes, like darts or knives, have a distance of 4.
		val supplier = RangeStepSupplier(player, target, 4 + dist)
		
		// Try move about to get a better working position
		if (!supplier.reached(player.world())) {
			if (player.frozen()) {
				player.message("I can't reach that!")
				player.sound(154)
				return
			}
			player.walkTo(target, PathQueue.StepType.REGULAR)
			player.pathQueue().trimToSize(if (player.pathQueue().running()) 2 else 1)
		}
		
		// Recheck, since we have attempted to walk closer in the above code.
		if (!supplier.reached(player.world(), target)) {
			return
		}
		
		RangedSpecialAttacks.execute_dragon_thrownaxe_special(it, target)
	}

	private fun executeDragonHarpoon(s: Script) {
		val player = s.player()

		if (player.skills().level(Skills.FISHING) < 61) {
			s.message("You need a Fishing level of 61 to use this special attack.")
			s.player().varps()[Varp.SPECIAL_ENABLED] = 0
			return
		}

		if (PlayerCombat.takeSpecialEnergy(player, 100)) {
			s.player().sync().animation(7393, 0)
			s.player().sync().graphic(246, 0, 0)
			s.player().sync().shout("Here fishy fishies!")
			s.message("As you harness the power of the dragon harpoon you feel a slight increase in your ability to catch fish.")
			s.player().skills().setLevel(Skills.FISHING, s.player().skills().level(Skills.FISHING) + 3)
		} else {
			s.message("You don't have enough power left.")
		}

		s.player().varps()[Varp.SPECIAL_ENABLED] = 0
	}
	
}