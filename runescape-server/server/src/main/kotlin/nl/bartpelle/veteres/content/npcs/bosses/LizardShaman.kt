package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 3/31/2016.
 */

object LizardShaman {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			
			if (EntityCombat.canAttackDistant(npc, target, true, 7) && EntityCombat.attackTimerReady(npc)) {
				if (EntityCombat.attackTimerReady(npc)) {
					val random = npc.world().random(5)
					
					when (random) {
						1 -> jump_attack(it, npc, target)
						2 -> cast_spawn_destructive_minions(it, npc, target)
						3 -> green_acidic_attack(it, npc, target)
						else -> {
							if (EntityCombat.canAttackMelee(npc, target, false)) primary_melee_attack(npc, target)
							else primate_ranged_attack(npc, target)
						}
					}
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun primary_melee_attack(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, EntityCombat.randomHit(npc))
		else
			target.hit(npc, 0)
		
		npc.animate(npc.attackAnimation())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun primate_ranged_attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
		
		npc.animate(7193)
		npc.world().spawnProjectile(npc, target, 1291, 120, 30, 50, 12 * tileDist, 14, 5)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE))
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.RANGE)
		else
			target.hit(npc, 0, delay.toInt())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun jump_attack(it: Script, npc: Npc, target: Entity) {
		val jump_destination = target.tile()
		
		npc.animate(7152)
		npc.lockNoDamage()
		it.delay(3)
		npc.hidden(true)
		npc.teleport(jump_destination)
		it.delay(2)
		npc.animate(6946)
		npc.hidden(false)
		npc.face(target)
		npc.unlock()
		
		it.runGlobal(target.world()) @Suspendable {
			target.world().players().forEachKt({ p ->
				if (p.tile().inSqRadius(jump_destination, 1))
					p.hit(npc, p.world().random(25))
			})
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun cast_spawn_destructive_minions(it: Script, npc: Npc, target: Entity) {
		npc.animate(7157)
		it.delay(3)
		
		spawn_destructive_minions(npc, target)
		spawn_destructive_minions(npc, target)
		spawn_destructive_minions(npc, target)
	}
	
	@Suspendable fun spawn_destructive_minions(npc: Npc, target: Entity) {
		val spawn = Npc(6768, npc.world(), Tile(target.tile().x + target.world().random(2),
				target.tile().z + target.world().random(2)))
		
		spawn.respawns(false)
		spawn.noRetaliation(true)
		npc.world().registerNpc(spawn)
		spawn.executeScript(LizardShamonSpawn(target).suicide)
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun green_acidic_attack(it: Script, npc: Npc, target: Entity) {
		val green_acidic_orb = Tile(target.tile().x, target.tile().z)
		val green_acidic_orb_distance = npc.tile().distance(green_acidic_orb)
		val green_acidic_orb_delay = Math.max(2, (900 + green_acidic_orb_distance * 12) / 15)
		val green_acidic_orb_hit_delay = Math.max(1, (30 + green_acidic_orb_distance * 12) / 18)
		
		npc.animate(7193)
		npc.world().spawnProjectile(npc.tile().transform(1, 1, 0), green_acidic_orb, 1293, 90, 0, green_acidic_orb_delay, 12 * green_acidic_orb_distance, 25, 10)
		target.world().tileGraphic(1294, green_acidic_orb, 1, 24 * green_acidic_orb_hit_delay)
		
		it.runGlobal(target.world()) @Suspendable {
			it.delay(green_acidic_orb_hit_delay)
			target.world().players().forEachKt({ p ->
				if (p.tile().inSqRadius(Tile(green_acidic_orb), 1)) {
					p.hit(npc, p.world().random(30)).combatStyle(CombatStyle.MAGIC)
					if (p.world().rollDie(2, 1)) {
						p.poison(10)
					}
				}
			})
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
}