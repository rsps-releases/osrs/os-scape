package nl.bartpelle.veteres.content.treasuretrails.rewards

import nl.bartpelle.veteres.content.mechanics.ServerAnnouncements
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets.clueScrollReward
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 2016-11-19.
 */

object MasterRewards {
	
	fun generateMasterReward(player: Player, source: Int) {
		val commonRewardAmount = player.world().random(3..5)
		for (i in 1..commonRewardAmount) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = CommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			
			if (player.world().realm().isPVP) {
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, player.world().random(reward.amount)), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
			}
		}
		
		//Roll for good items..
		if (player.world().rollDie(2, 1)) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			if (player.world().realm().isPVP) {
				
				//Change coins to blood money
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from a master clue scroll!")
		}
		
		//If we're on the PVP world, automatically give something from the uncommon table..
		if (player.world().realm().isPVP) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			//Change coins to blood money
			if (reward.reward.id() == 995) {
				player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from a master clue scroll!")
		}
	}
	
	enum class CommonRewards(val probability: Double, val reward: Item, val amount: IntRange = 1..1) {
		BLOOD_MONEY(100.0, Item(13307), 25000..25000),
	}
	
	enum class UncommonRewards(val probability: Double, val reward: Item) {
		CHARGE_DRAGON_STONE_JEWELLERY_SCROLL(100.0, Item(20238)),
		DRAGON_DEFENDER_ORNAMENT_KIT(100.0, Item(20143)),
		OCCULT_ORNAMENT_KIT(100.0, Item(20065)),
		TORTURE_ORNAMENT_KIT(100.0, Item(20062)),
		ARMADYL_GODSWORD_ORNAMENT_KIT(90.0, Item(20068)),
		BANDOS_GODSWORD_ORNAMENT_KIT(90.0, Item(20071)),
		SARADOMIN_GODSWORD_ORNAMENT_KIT(90.0, Item(20074)),
		ZAMORAK_GODSWORD_ORNAMENT_KIT(90.0, Item(20077)),
		LESSER_DEMON_MASK(80.0, Item(20020)),
		GREATER_DEMON_MASK(80.0, Item(20023)),
		BLACK_DEMON_MASK(80.0, Item(20026)),
		OLD_DEMON_MASK(80.0, Item(20029)),
		JUNGLE_DEMON_MASK(80.0, Item(20032)),
		ARCEUUS_HOUSE_HOOD(70.0, Item(20113)),
		HOSIDIUS_HOUSE_HOOD(70.0, Item(20116)),
		LOVAKENGJ_HOUSE_HOOD(70.0, Item(20119)),
		PISCARILIUS_HOUSE_HOOD(70.0, Item(20122)),
		SHAYZIEN_HOUSE_HOOD(70.0, Item(20125)),
		SAMURAI_KASA(60.0, Item(20035)),
		SAMURAI_SHIRT(60.0, Item(20038)),
		SAMURAI_GLOVES(60.0, Item(20041)),
		SAMURAI_GREAVES(60.0, Item(20044)),
		SAMURAI_BOOTS(60.0, Item(20047)),
		MUMMYS_HEAD(50.0, Item(20080)),
		MUMMYS_BODY(50.0, Item(20083)),
		MUMMYS_HANDS(50.0, Item(20086)),
		MUMMYS_LEGS(50.0, Item(20089)),
		MUMMYS_FEET(50.0, Item(20092)),
		ANKOU_MASK(40.0, Item(20095)),
		ANKOU_TOP(40.0, Item(20098)),
		ANKOU_GLOVES(40.0, Item(20101)),
		ANKOU_LEGS(40.0, Item(20104)),
		ANKOU_SOCKS(40.0, Item(20107)),
		HOOD_OF_DARKNESS(30.0, Item(20128)),
		ROBE_TOP_OF_DARKNESS(30.0, Item(20131)),
		GLOVES_OF_DARKNESS(30.0, Item(20134)),
		ROBE_BOTTOM_OF_DARKNESS(30.0, Item(20137)),
		BOOTS_OF_DARKNESS(30.0, Item(20140)),
		LEFT_EYE_PATCH(20.0, Item(19724)),
		OBSIDIAN_CAPE_R(20.0, Item(20050)),
		FANCY_TIARA(10.0, Item(20008)),
		HALF_MOON_SPECTACLES(10.0, Item(20053)),
		ALE_OF_THE_GODS(5.0, Item(20056)),
		BUCKET_HELM_G(5.0, Item(20059)),
		BOWL_WIG(5.0, Item(20110)),
		DRAGON_CLAWS(2.0, Item(13652)),
		THIRD_AGE_MELEE_HELM(1.0, Item(10350)),
		THIRD_AGE_MELEE_BODY(1.0, Item(10348)),
		THIRD_AGE_MELEE_LEGS(1.0, Item(10346)),
		THIRD_AGE_MELEE_KITE(1.0, Item(10352)),
		THIRD_AGE_RANGE_COIF(1.0, Item(10334)),
		THIRD_AGE_RANGE_BODY(1.0, Item(10330)),
		THIRD_AGE_RANGE_LEGS(1.0, Item(10332)),
		THIRD_AGE_RANGE_VAMBS(1.0, Item(10336)),
		THIRD_AGE_RANGE_HAT(1.0, Item(10342)),
		THIRD_AGE_RANGE_TOP(1.0, Item(10338)),
		THIRD_AGE_RANGE_SKIRT(1.0, Item(10340)),
		THIRD_AGE_RANGE_AMULET(1.0, Item(10344)),
		THIRD_AGE_CLOAK(1.0, Item(12437)),
		THIRD_AGE_WAND(1.0, Item(12422)),
		THIRD_AGE_BOW(1.0, Item(12424)),
		THIRD_AGE_LONGSWORD(1.0, Item(12426)),
		THIRD_AGE_AXE(1.0, Item(20011)),
		THIRD_AGE_PICKAXE(1.0, Item(20014))
	}
	
}
