package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer
import nl.bartpelle.veteres.util.RSFormatter

/**
 * Created by Bart on 12/8/2015.
 */
class BanTimeSpecifier(val name: String) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(s: Script) {
		when (s.optionsTitled("What do you want to do?", "Set ban length.", "View ban length.", "Nothing.")) {
			1 -> {
				val len = s.inputString("Enter ban length (2 minutes, 1 day, until november, christmas 2016):")
				val date = RSFormatter.parseDate(len)
				
				if (date != null) {
					s.message("Ban has been extended/shortened until: $date.")
					s.player().world().server().service(PgSqlPlayerSerializer::class.java, true).ifPresent({ s ->
						s.banPlayer(name, date.time)
					})
				}
			}
			2 -> {
				s.messagebox("This feature is currently unavailable.")
			}
		}
	}
	
}