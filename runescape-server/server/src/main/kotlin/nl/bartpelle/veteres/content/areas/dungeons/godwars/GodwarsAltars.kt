package nl.bartpelle.veteres.content.areas.dungeons.godwars

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 3/5/2016.
 */
object GodwarsAltars {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		makeAltar(r, 26363, Tile(2925, 5333, 2))
		makeAltar(r, 26366, Tile(2862, 5354, 2))
		makeAltar(r, 26364, Tile(2909, 5265, 0)) // Saradomin
		makeAltar(r, 26365, Tile(2839, 5294, 2)) // Armadyl
	}
	
	fun makeAltar(r: ScriptRepository, obj: Int, teleportTile: Tile) {
		r.onObject(obj) {
			val opt = it.interactionOption()
			if (opt == 1) {
				if (!it.player().timers().has(TimerKey.IN_COMBAT)) {
					if (!it.player().timers().has(TimerKey.GODWARS_ALTAR_LOCK)) {
						it.message("You pray to the gods...")
						it.player().timers().set(TimerKey.GODWARS_ALTAR_LOCK, 1000)
						it.player().skills().replenishSkill(Skills.PRAYER, 99)
						it.animate(645)
						it.message("...and recharged your prayer.")
					} else {
						it.message("You cannot use this altar yet!")
					}
				} else {
					it.message("You cannot do this while under attack.")
				}
			} else {
				it.player().teleport(teleportTile)
			}
		}
	}
	
}