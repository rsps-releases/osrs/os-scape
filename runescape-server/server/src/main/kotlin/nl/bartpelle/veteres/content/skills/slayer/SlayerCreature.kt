package nl.bartpelle.veteres.content.skills.slayer

import java.util.*

/**
 * Created by Bart on 11/10/2015.
 */
enum class SlayerCreature(val uid: Int, val req: Int, val cbreq: Int, vararg val ids: Int) {
	
	/*
	1=Monkeys, 2=Goblins, 3=Rats, 4=Spiders, 5=Birds, 6=Cows, 7=Scorpions, 8=Bats, 9=Wolves, 10=Zombies,
	11=Skeletons, 12=Ghosts, 13=Bears, 14=Hill Giants, 15=Ice Giants, 16=Fire Giants, 17=Moss Giants, 18=Trolls,
	19=Ice Warriors, 20=Ogres, 21=Hobgoblins, 22=Dogs, 23=Ghouls, 24=Green Dragons, 25=Blue Dragons, 26=Red Dragons,
	27=Black Dragons, 28=Lesser Demons, 29=Greater Demons, 30=Black Demons, 31=Hellhounds, 32=Shadow Warriors,
	33=Werewolves, 34=Vampires, 35=Dagannoth, 36=Turoth, 37=Cave Crawlers, 38=Banshees, 39=Crawling Hands, 40=Infernal Mages,
	41=Aberrant Spectres, 42=Abyssal Demons, 43=Basilisks, 44=Cockatrice, 45=Kurask, 46=Gargoyles, 47=Pyrefiends, 48=Bloodveld,
	49=Dust Devils, 50=Jellies, 51=Rockslugs, 52=Nechryael, 53=Kalphite, 54=Earth Warriors, 55=Otherworldly Beings, 56=Elves,
	57=Dwarves, 58=Bronze Dragons, 59=Iron Dragons, 60=Steel Dragons, 61=Wall Beasts, 62=Cave Slimes, 63=Cave Bugs, 64=Shades,
	65=Crocodiles, 66=Dark Beasts, 67=Mogres, 68=Desert Lizards, 69=Fever Spiders, 70=Harpie Bug Swarms, 71=Sea Snakes,
	72=Skeletal Wyverns, 73=Killerwatts, 74=Mutated Zygomites, 75=Icefiends, 76=Minotaurs, 77=Fleshcrawlers, 78=Catablepon,
	79=Ankou, 80=Cave Horrors, 81=Jungle Horrors, 82=Goraks, 83=Suqahs, 84=Brine Rats, 85=Minions of Scabaras, 86=Terror Dogs,
	87=Molanisks, 88=Waterfiends, 89=Spiritual Creatures, 90=Lizardmen, 92=Cave Kraken, 3=Mithril Dragons, 94=Aviansies,
	95=Smoke Devils, 96=TzHaar, 97=TzTok-Jad, 98=Bosses
	 */
	
	GOBLINS(2, 1, 1, 3029, 3030, 3031, 3032, 3033, 3034, 3035, 3036, 657, 659, 663,
			664, 662, 667, 668, 663, 661, 658, 665, 2246, 2245, 2249, 2248),
	RATS(3, 1, 1, 2854, 2864, 2863, 2856, 2859),
	SPIDERS(4, 1, 1, 3023, 3017, 3016),
	CHICKENS(5, 1, 1, 2692, 2693),
	COWS(6, 1, 1, 2805, 2806, 2807, 2808),
	SCORPION(7, 1, 1, 3025, 3024, 6615),
	BATS(8, 1, 10, 2827, 2834),
	WOLF(9, 1, 20, 10, 2490, 2491, 3135, 3136, 106, 107, 108),
	ZOMBIE(10, 1, 1, 42, 55, 56, 57, 58, 43, 44),
	SKELETON(11, 1, 1, 70, 71, 73, 77, 84, 82, 72, 74, 75, 76, 77, 84, 82, 73, 79,
			81, 80, 1541),
	GHOSTS(12, 1, 1, 86, 87, 88, 89, 90),
	BEARS(13, 1, 10, 2838, 2839),
	HILL_GIANTS(14, 1, 20, 2098, 2099, 2100, 2101, 2102, 2103),
	ICE_GIANT(15, 1, 1, 2088, 2087, 2086, 2085, 2089, 7878, 7880, 7879),
	FIRE_GIANTS(16, 1, 65, 2075, 2076, 2077, 2078, 2079, 2080, 2081, 2082, 2083, 2084),
	ICE_WARRIOR(19, 1, 1, 2087, 2841),
	HOBGOBLIN(21, 1, 1, 3049, 3050, 2241),
	GREEN_DRAGONS(24, 1, 65, 260, 261, 262, 263, 264, 7868, 7869, 7870),
	BLUE_DRAGONS(25, 1, 70, 241, 242, 243, 265, 266, 267, 268, 269, 5878, 5879, 5880, 5881, 5882),
	BLACK_DRAGONS(27, 1, 1, 252, 253, 254, 255, 256, 257, 258, 259, 239, 7861, 7862, 7863),
	GREATER_DEMONS(29, 1, 85, 2025, 2026, 2027, 2028, 2029, 3129, 3130, 3131, 3132, 7871, 7872, 7873),
	BLACK_DEMONS(30, 1, 1, 2048, 2049, 2050, 2051, 2052, 1432, 7144, 6357, 7874, 7875, 7876),
	HELLHOUNDS(31, 1, 1, 104, 105, 3133, 5862, 5863, 5866, 7256, 7877),
	WEREWOLVES(33, 1, 1, 2135, 3136),
	DAGANNOTH(35, 1, 1, 3185, 2265, 2266, 2267),
	TUROTH(36, 55, 1, 427, 428, 429, 430),
	CAVE_CRAWLER(37, 10, 15, 406, 407, 408, 409, 7389),
	CRAWLING_HANDS(39, 5, 10, 453, 454, 455, 456, 457, 448, 449, 450, 451, 452, 7388),
	ABERRANT_SPECRES(41, 60, 85, 2, 3, 4, 5, 6, 7, 7402),
	ABYSSAL_DEMON(42, 85, 70, 415, 124, 7410),
	// 43 removed
	COCKATRICE(44, 25, 1, 419),
	KURASK(45, 70, 1, 410, 411),
	GARGOYLE(46, 75, 1, 412, 413, 1543),
	PYREFIEND(47, 30, 1, 433, 434, 435, 436, 3139, 7394),
	BLOODVELDS(48, 50, 65, 484, 485, 486, 487, 7397),
	DUST_DEVILS(49, 65, 80, 423, 424, 7404),
	JELLIES(50, 52, 1, 437, 438, 439, 440, 441, 442, 7399),
	ROCKSLUG(51, 20, 20, 421, 7392),
	NECHRYAEL(52, 80, 1, 8, 7411),
	// no 53-57
	BRONZE_DRAGONS(58, 1, 1, 270, 271),
	IRON_DRAGONS(59, 1, 1, 272, 273),
	STEEL_DRAGONS(60, 1, 1, 274, 275),
	// no 61-65
	DARK_BEASTS(66, 90, 1, 4005, 7409),
	// no 67-71
	WYVERN(72, 72, 70, 467),
	// no 73-74
	ICEFIENDS(75, 1, 1, 3140),
	MINOTAURS(76, 1, 10, 2481, 2483),
	FLESH_CRAWLER(77, 1, 18, 2498),
	// no 78
	ANKOUS(79, 1, 70, 2514, 2515, 2516, 6608, 7864),
	CAVE_HORRORS(80, 58, 70, 3209, 7401),
	LIZARDMEN(90, 1, 45, 6914, 6915, 6916, 6917, 6918, 6919, 6766, 6767),
	CAVE_KRAKENS(92, 87, 80, 493, 494, 496),
	SMOKE_DEVILS(95, 93, 90, 498, 499, 7406),
	TZHARR(96, 1, 1, 2173, 2174, 2175, 2176, 2177, 2178, 2179, 2167, 2168, 2169, 2170, 2171, 2172, 2161, 2162, 2163, 2164, 2165, 2166, 3116, 3118, 3121, 3123, 3125, 3127, 3128),
	
	//Wilderness tasks
	LESSER_DEMONS(28, 1, 1, 2005, 2006, 2007, 2008, 20018, 2025, 7865, 7866, 7867),
	LAVA_DRAGONS(104, 1, 1, 6593),
	BOSSES(98, 1, 1, 2054, 6503, 6504, 6609, 6610, 6611, 6612, 6615, 6618, 6619),
	MAGIC_AXES(91, 1, 1, 2844, 7269),
	FOSSIL_ISLAND_WYVERNS(106, 1, 1, 7795),
    ADAMANT_DRAGONS(108, 1, 1, 8030, 8090),
    REVENANTS(107, 1, 1, 7881, 7931, 7932, 7933, 7934, 7935, 7936, 7937, 7938, 7939, 7940),
	RUNE_DRAGONS(109, 1, 1, 8027, 8031, 8091)
	;
	
	fun matches(id: Int): Boolean {
		return id in ids;
	}
	
	companion object {
		var lookup: Map<Int, SlayerCreature>? = null
		
		@JvmStatic fun lookup(uid: Int): SlayerCreature? {
			if (lookup == null) {
				// Build lookup map now.
				val ltemp = HashMap<Int, SlayerCreature>()
				
				values().forEach {
					ltemp.put(it.uid, it)
				}
				
				lookup = ltemp
			}
			
			return lookup!![uid]
		}
		
		@JvmStatic fun slayerReq(id: Int): Int {
			/*values().forEach {
				if (id in it.ids)
					return it.req
			}*/
			return 1;
		}
	}
}
