package nl.bartpelle.veteres.content.areas.varrock.grandexchange

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-06-23.
 */

object Clerks {
	
	val CLERKS = arrayListOf(2148, 2149, 2150, 2151)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		@Suspendable fun sets(it: Script) {
			it.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
			it.player().interfaces().sendMain(451)
			it.player().interfaces().sendInventory(430)
			it.player().interfaces().setting(451, 2, 0, 100, 1026)
			it.player().interfaces().setting(430, 0, 0, 27, 1026)
			it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(451) }
		}
		
		@Suspendable fun history(it: Script) {
			it.player().grandExchange().openHistory()
			it.onInterrupt {
				it.player().interfaces().closeMain()
			}
		}
		
		@Suspendable fun exchange(it: Script) {
			it.player().grandExchange().display()
		}
		
		@Suspendable fun showoptions(CLERK: Int, it: Script) {
			when (it.options("How do I use the Grand Exchange?", "I'd like to set up trade offers please.", "Can you help me with item sets?", "Show me my trade history.", "I'm fine, thanks.")) {
				1 -> {
					it.chatPlayer("How do I use the Grand Exchange?")
					it.chatNpc("My colleague and I can let you set up trade offers.<br>" +
							"You can offer to Sell items or Buy items.", CLERK)
					
					it.chatNpc("When you want to sell something, you give us the items<br>" +
							"and tell us how much money you want for them.", CLERK)
					
					it.chatNpc("We'll look for someone who wants to buy those items at<br>" +
							"your price, and we'll perform the trade. You can then<br>" +
							"collect the cash here, or at any bank.", CLERK)
					
					it.chatNpc("When you want to buy something, you tell us what<br>" +
							"you want, and give us the cash you're willing to spend on it.", CLERK)
					
					it.chatNpc("We'll look for someone who's selling those items at your<br>" +
							"price, and we'll perform the trade. You can then collect<br>" +
							"the items here, or at any bank, along with any left-over<br>" +
							"cash.", CLERK)
					
					it.chatNpc("Sometimes it takes a while to find a matching trade<br>" +
							"offer. If you change your mind, we'll let you cancel<br>" +
							"your trade offer, and we'll return your unused items<br>" +
							"and cash.", CLERK)
					
					it.chatNpc("That's all the essential information you need to get<br>" +
							"started. Would you like to trade now, or exchange item<br>" +
							"sets?", CLERK)
					
					showoptions(CLERK, it)
				}
				2 -> {
					it.chatPlayer("I'd like to set up trade offers please.")
					exchange(it)
				}
				3 -> {
					it.chatPlayer("Can you help me with item sets?")
					sets(it)
				}
				4 -> {
					it.chatPlayer("Show me my trade history.")
					history(it)
				}
				5 -> it.chatPlayer("I'm fine, thanks.")
			}
		}
		
		for (CLERK in CLERKS) {
			//'sets' option
			r.onNpcOption4(CLERK, ::sets)
			
			r.onNpcOption3(CLERK, ::history)
			
			r.onNpcOption2(CLERK, ::exchange)
			
			r.onNpcOption1(CLERK) @Suspendable {
				it.chatNpc("Welcome to the Grand Exchange.<br>" +
						"Would you like to trade now, or exchange item sets?", CLERK)
				showoptions(CLERK, it)
			}
			
		}
		r.onInterfaceClose(451) @Suspendable { it.player().interfaces().closeById(430) }
	}
}