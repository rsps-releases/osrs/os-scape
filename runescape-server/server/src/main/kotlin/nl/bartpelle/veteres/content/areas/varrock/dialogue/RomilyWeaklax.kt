package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/10/2016.
 */

object RomilyWeaklax {
	
	val ROMILY_WEAKLAX = 6533
	
	enum class Pies(val pie: Int, val reward: Int) {
		APPLE_PIE(2323, 84),
		REDBERRY_PIE(2325, 90),
		MEAT_PIE(2327, 96),
		GARDEN_PIE(7178, 112),
		FISH_PIE(7188, 125),
		ADMIRAL_PIE(7198, 387);
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ROMILY_WEAKLAX) @Suspendable {
			var type_of_pie = it.player().attribOr<Int>(AttributeKey.ROMILY_TYPE_OF_PIE, 0)
			var amount_of_pies = it.player().attribOr<Int>(AttributeKey.ROMILY_AMOUNT_OF_PIES, 0)
			
			it.chatNpc("Hello and welcome to my pie shop, how can I help you?", ROMILY_WEAKLAX, 567)
			if (amount_of_pies == 0) {
				when (it.options("I'd like to buy some pies.", "Do you need any help?", "I'm good thanks.")) {
					1 -> it.player().world().shop(34).display(it.player())
					2 -> {
						it.chatPlayer("Do you need any help?", 554)
						it.chatNpc("Actually I could, you see I'm running out of stock and<br>I don't have time to bake any more pies. Would you be<br>willing to bake me some pies? I'll pay you well for them.", ROMILY_WEAKLAX, 590)
						when (it.options("Sure, what do you need?", "Sorry, I can't help you.")) {
							1 -> {
								it.chatPlayer("Sure, what do you need?", 554)
								
								it.player().putattrib(AttributeKey.ROMILY_TYPE_OF_PIE, Pies.values().random().pie)
								it.player().putattrib(AttributeKey.ROMILY_AMOUNT_OF_PIES, it.player().world().random(10..30))
								
								type_of_pie = it.player().attribOr<Int>(AttributeKey.ROMILY_TYPE_OF_PIE, 0)
								amount_of_pies = it.player().attribOr<Int>(AttributeKey.ROMILY_AMOUNT_OF_PIES, 0)
								
								val def = it.player().world().definitions().get(ItemDefinition::class.java, type_of_pie)
								it.chatNpc("Great, can you bake me $amount_of_pies ${def.name} please.", ROMILY_WEAKLAX, 567)
							}
							2 -> it.chatPlayer("Sorry, I can't help you.", 610)
						}
					}
					3 -> it.chatPlayer("I'm good thanks.", 588)
				}
			} else {
				when (it.options("I'd like to buy some pies.", "I've got those pies you wanted.", "I'm good thanks.")) {
					1 -> it.player().world().shop(34).display(it.player())
					2 -> {
						it.chatPlayer("I've got those pies you wanted.", 554)
						
						//Check to see if our player has the pies he claims he does!
						if (it.player().inventory().contains(Item(type_of_pie))) {
							var amt = it.player().inventory().count(type_of_pie)
							var value = 0
							if (amt > amount_of_pies)
								amt = amount_of_pies
							
							Pies.values().forEach { p ->
								if (p.pie == type_of_pie) {
									value = p.reward
								}
							}
							
							//Attempt to remove the players items, set attribute, and give them gold.
							if (it.player().inventory().remove(Item(type_of_pie, amt), false).success()) {
								amount_of_pies = it.player().attribOr<Int>(AttributeKey.ROMILY_AMOUNT_OF_PIES, 0) - amt
								it.player().putattrib(AttributeKey.ROMILY_AMOUNT_OF_PIES, amount_of_pies)
								it.player().inventory().add(Item(995, amt * value), true)
								
								//Fail safe, so that when the player gets the gold we set the attributes back to 0.
								if (amount_of_pies == 0) {
									it.player().putattrib(AttributeKey.ROMILY_AMOUNT_OF_PIES, 0)
									it.player().putattrib(AttributeKey.ROMILY_TYPE_OF_PIE, 0)
								}
							}
							
							//If our player has given Romily all the pies he requested we..
							if (amount_of_pies == 0) {
								it.chatNpc("Thank you very much!", ROMILY_WEAKLAX, 567)
							} else {
								it.chatNpc("Thank you, if you could bring me the other $amount_of_pies that'd<br>be great!", ROMILY_WEAKLAX, 568)
							}
						} else {
							val def = it.player().world().definitions().get(ItemDefinition::class.java, type_of_pie)
							it.chatNpc("Doesn't look like you have any of the $amount_of_pies ${def.name} <br>I requested.", ROMILY_WEAKLAX, 611)
						}
					}
					3 -> it.chatPlayer("I'm good thanks.", 588)
				}
			}
		}
		r.onNpcOption2(ROMILY_WEAKLAX) @Suspendable { it.player().world().shop(34).display(it.player()) }
	}
}