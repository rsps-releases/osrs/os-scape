package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.TwoFactorSetup
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer

/**
 * Created by Situations on 4/29/2016.
 */

object SecurityGuard {
	
	val SECURITY_GUARD = 5442
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(SECURITY_GUARD) @Suspendable {
			if (it.player().twofactorKey() != null) {
				it.chatNpc("Congratulations ${it.player().name()}, you've already setup two-factor authentication on your account. This means nobody will be able to access your account without<br> your verification code!", SECURITY_GUARD, 588)
				it.chatNpc("Is there anything else I can help you with?", SECURITY_GUARD, 588)
				
				when (it.options("No, thank you.", "Can you disable my two-factor?")) {
					1 -> {
						it.chatPlayer("No, thank you. I'll be on my way.")
						it.chatNpc("Okay, you can see my any time should you need security advice!", SECURITY_GUARD)
					}
					2 -> {
						it.chatPlayer("Can you disable my two-factor authentication?")
						it.chatNpc("Eh.. Well.. I certainly can, but I'm not too keen on it! We prefer you to be secure. However.. If you've lost your phone I can see why you'd want this.", SECURITY_GUARD)
						it.chatNpc("If you're sure you understand the security risk of disabling it, I can provide this service to you.", SECURITY_GUARD)
						
						when (it.options("Yes, disable my two-factor.", "No, I want to be secure.")) {
							1 -> {
								it.chatPlayer("Yes, please disable my two-factor authenticator. I will enable it again as soon as I am able to!")
								it.player.twofactorKey(null)
								it.player().world().server().service(PgSqlPlayerSerializer::class.java, true).ifPresent({ ser ->
									ser.disableTwoFactor(it.player().id() as Int)
								})
								it.chatNpc("Okay, fair enough. I've disabled it for you. Please re-enable it as soon as possible!", SECURITY_GUARD)
							}
						}
					}
				}
			} else {
				it.chatNpc("Hi ${it.player().name()}, are you interested in taking a minute of your valuable time and setup two-factor authentication? It'll prevent unwanted hackers accessing your profile.", SECURITY_GUARD, 588)
				when (it.options("Yes, I want to keep my account safe!", "No thanks.")) {
					1 -> {
						it.chatPlayer("Yes, please help me secure my account!", 588)
						it.chatNpc("Perfect, just follow these simple instructions!", SECURITY_GUARD, 588)
						it.player().stopActions(false)
						it.player().world().server().scriptExecutor().executeScript(it.player(), TwoFactorSetup())
					}
					2 -> {
						it.chatPlayer("No thanks, I'm not interested in securing my profile.", 588)
						it.chatNpc("Okay, but please be aware your account is always at risk without two-factor authentication.", SECURITY_GUARD, 590)
					}
				}
			}
		}
	}
}
