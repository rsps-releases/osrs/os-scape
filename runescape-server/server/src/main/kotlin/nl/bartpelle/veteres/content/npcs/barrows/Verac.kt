package nl.bartpelle.veteres.content.npcs.barrows

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 8/28/2015.
 */

object Verac {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		EntityCombat.defaultMelee(it) { it, target ->
			val special = it.npc().world().rollDie(100, 25)
			if (special || EntityCombat.attemptHit(it.npc(), target, CombatStyle.MELEE)) {
				val hit = target.hit(it.npc(), EntityCombat.randomHit(it.npc()))
				if (special)
					hit.combatStyle(CombatStyle.GENERIC)
			} else {
				target.hit(it.npc(), 0) // Uh-oh, that's a miss.
			}
		}
	}
	
}