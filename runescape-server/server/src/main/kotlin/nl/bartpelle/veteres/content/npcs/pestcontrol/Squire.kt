package nl.bartpelle.veteres.content.npcs.pestcontrol

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.minigames.pest_control.PestControl
import nl.bartpelle.veteres.content.minigames.pest_control.PestControlDifficulty
import nl.bartpelle.veteres.content.minigames.pest_control.PestControlGame
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jason MacKeigan on 2016-08-03 at 1:33 PM
 */
object Squire {
	
	@Suspendable fun dialogueStop(it: Script) {
		it.chatPlayer("I'd better get back to it then.")
	}
	
	@Suspendable fun insideOptionOne(it: Script) {
		val option = it.options("How do I repair things?", "I want to leave.", "I'd better get back to it then.")
		
		when (option) {
			1 -> {
				dialogueRepair(it)
				insideOptionTwo(it)
			}
			2 -> {
				dialogueTravel(it)
			}
			3 -> {
				dialogueStop(it)
			}
		}
	}
	
	@Suspendable fun dialogueInformation(it: Script) {
		it.chatPlayer("What's going on?")
		it.chatNpc("This island is being invaded by outsiders and the Void" +
				"Knight over there is using a ritual to unsummon their" +
				"portals. We must defend the Void Knight at all costs" +
				"however if you get an opening you can destroy the", PestControlGame.SQUIRE_NPC)
		it.chatNpc("portals yourself.", PestControlGame.SQUIRE_NPC)
	}
	
	@Suspendable fun insideOptionTwo(it: Script) {
		val option = it.options("What's going on?", "I want to leave.", "I'd better get back to it then.")
		
		when (option) {
			1 -> {
				dialogueInformation(it)
				insideOptionOne(it)
			}
			2 -> {
				dialogueTravel(it)
			}
			3 -> {
				dialogueStop(it)
			}
		}
	}
	
	@Suspendable fun dialogueTravel(it: Script) {
		it.chatNpc("Away you go then, the lander will take you back.", PestControlGame.SQUIRE_NPC)
		
		val player = it.player()
		val game = PestControl.gameForMember(player)
		
		if (!game.isPresent) {
			it.chatNpc("Odd, the lander is having trouble taking you back!", PestControlGame.SQUIRE_NPC)
			return
		}
		player.teleport(game.get().map.get().exit())
		player.interfaces().closeById(PestControl.GAME_INTERFACE)
	}
	
	@Suspendable fun dialogueRepair(it: Script) {
		it.chatPlayer("How do I repair things?")
		it.chatNpc("There are trees on the island. You'll need to chop them" +
				"down for logs and use a hammer to repair the" +
				"defences. Be careful though, the trees don't grow" +
				"back very fast so your resources are limited!", PestControlGame.SQUIRE_NPC)
	}
	
	@ScriptMain @JvmStatic fun dialogue(repository: ScriptRepository) {
		for (difficulty in PestControlDifficulty.values()) {
			repository.onNpcOption1(PestControlGame.SQUIRE_NPC, suspend@ @Suspendable {
				it.chatNpc("Be quick, we're under attack!", PestControlGame.SQUIRE_NPC)
				val option = it.options("What's going on?", "How do I repair things?", "I want to leave.",
						"I'd better get back to it then.")
				
				when (option) {
					1 -> {
						dialogueInformation(it)
						insideOptionOne(it)
					}
					2 -> {
						dialogueRepair(it)
						insideOptionTwo(it)
					}
					3 -> {
						dialogueTravel(it)
					}
					4 -> {
						dialogueStop(it)
					}
				}
			})
			repository.onNpcOption1(difficulty.squire, suspend@ @Suspendable {
				it.chatNpc("Hi, how can I help you?", difficulty.squire)
				val option = it.options("Who are you?", "What's going on here?", "I'm fine thanks.")
				
				when (option) {
					1 -> {
						it.chatPlayer("Who are you?")
						it.chatNpc("I'm a squire for the Void Knights", difficulty.squire)
						it.chatPlayer("The who?")
						it.chatNpc("The Void Knights, they are great warriors of balance</br> " +
								"who do Guthix's work here in OS-Scape.", difficulty.squire)
						
						val secondOption = it.options("Wow, can I join?", "What kind of work?", "What's 'OS-Scape'",
								"Uh huh, sure.")
						
						when (secondOption) {
							1 -> {
								it.chatPlayer("Wow, can I join?")
								it.chatNpc("Entry is strictly invite only, however we do need help</br>continuing Guthix's work.",
										difficulty.squire)
								val thirdOption = it.options("What kind of work?", "Good luck with that.")
								
								when (thirdOption) {
									1 -> {
										chatOptionOne(it, difficulty.squire)
										
										val fourthOption = it.options("Yeah ok, what's the problem?", "What's 'OS-Scape'?",
												"I'd rather not, sorry.")
										
										when (fourthOption) {
											1 -> {
												it.chatPlayer("Yeah ok, what's the problem?")
												it.chatNpc("Well the order has become quite diminished over the</br>" +
														"years, it's a very long process to learn the skills of a</br>" +
														"Void Knight. Recently there have been breaches into</br>" +
														"our realm from somewhere else, and strange creatures",
														difficulty.squire)
												it.chatNpc("have been pouring through. We can't let that happen,</br>" +
														"and we'd be very greatful if you'd help us.",
														difficulty.squire)
												
												val fifthOption = it.options("How can I help?", "Sorry, but I can't.")
												
												when (fifthOption) {
													1 -> {
														it.chatPlayer("How can I help?")
														it.chatNpc("We send launchers from our outpost to the nearby</br>" +
																"islands. If you go and wait in the lander there that'd</br>" +
																"really help.", difficulty.squire)
													}
												}
											}
											2 -> {
												chatOptionThree(it, difficulty.squire)
											}
											3 -> {
												chatOptionThree(it, difficulty.squire)
											}
										}
									}
								}
							}
							2 -> {
								chatOptionOne(it, difficulty.squire)
								
							}
							3 -> {
								chatOptionThree(it, difficulty.squire)
							}
							4 -> {
								it.chatPlayer("Uh huh, sure.")
							}
						}
					}
					2 -> {
						it.chatPlayer("What's going on here?")
						it.chatNpc("This is where we launch our landers to combat the</br>" +
								"invasion of the nearby islands. You'll see three landers</br>" +
								"one for novice, intermediate and veteran adventures.</br>" +
								"Each has a different difficulty, but they therefore offer", difficulty.squire)
						it.chatNpc("varying rewards.", difficulty.squire)
						it.chatPlayer("And this lander is...?")
						it.chatNpc("The ${difficulty.name.toLowerCase()}.", difficulty.squire)
						it.chatPlayer("So how do they work?")
						it.chatNpc("Simple. We send each lander out every five minutes,</br>" +
								"however we need at least five volunteers or it's a suicide</br>" +
								"mission. The lander can only hold a maximum of</br>" +
								"twenty five people though, so we do send them out", difficulty.squire)
						it.chatNpc("early if they get full. If you'd be willing to help us then</br>" +
								"just wait in the lander and we'll launch it soon as it's</br>" +
								"ready. However you will need a combat level of 40 to</br>" +
								"us this lander.", difficulty.squire)
					}
					3 -> {
						it.chatPlayer("I'm fine thanks.")
					}
				}
			})
		}
	}
	
	@Suspendable fun chatOptionOne(it: Script, id: Int) {
		it.chatPlayer("What kind of work?")
		it.chatNpc("Ah well you see we try to keep OS-Scape as Guthix</br>" +
				"intended, it's very challenging. Actually we've been</br>" +
				"having some problems recently, maybe you could help</br>" +
				"us?", PestControlDifficulty.NOVICE.squire)
	}
	
	@Suspendable fun chatOptionThree(it: Script, id: Int) {
		it.chatPlayer("What 'OS-Scape'?")
		it.chatNpc("It is the name that Guthix gave to this world, so we</br>honour him with its use.", id)
	}
	
}