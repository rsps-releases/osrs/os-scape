package nl.bartpelle.veteres.content.tournament

import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.script.TimerKey

object TournamentInscription {

    fun open(world: World) {
        if(TournamentConfig.tournamentInProgress) {
            System.err.println("[TOURNAMENT] Open inscription attempt while tournament is in progress.")
            return
        }
        if(TournamentConfig.inscriptionsOpen) {
            System.err.println("[TOURNAMENT] Open inscription attempt while inscriptions already open.")
            return
        }
        TournamentConfig.inscriptionsOpen = true
        world.broadcast(Color.DARK_RED.wrap("<img=79> Inscriptions are now open! Join the ${TournamentConfig.tournamentType.name}" +
                " tournament by speaking to the Tournament Manager east of Edgeville Bank or by simply typing ::jointournament."))
        world.timers().register(TimerKey.TOURNAMENT_INSCRIPTION_TIMER, 50)
    }

    fun close(world: World) {
        if(!TournamentConfig.tournamentInProgress) {
            System.err.println("[TOURNAMENT] Close inscription attempt while tournament is in progress.")
            return
        }
        if(!TournamentConfig.inscriptionsOpen) {
            System.err.println("[TOURNAMENT] Close inscription attempt while inscriptions already closed.")
            return
        }
        TournamentConfig.inscriptionsOpen = false
        world.broadcast(Color.DARK_RED.wrap("<img=79> Inscriptions are now closed. Good luck to all the tournament participants!"))
    }
}