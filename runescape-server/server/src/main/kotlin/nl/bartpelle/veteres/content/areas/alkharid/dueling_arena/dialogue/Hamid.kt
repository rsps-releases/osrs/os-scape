package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Hamid {
	
	val HAMID = 3352
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(HAMID) @Suspendable {
			it.chatPlayer("Hi!", 567)
			it.chatNpc("Hello traveller. How can I be of assistance?", HAMID, 588)
			when (it.options("Can you heal me?", "What's a Monk doing in a place such as this?", "Which monastery do you come from?")) {
				1 -> {
					it.chatPlayer("Can you heal me?", 575)
					it.chatNpc("You'd be better off speaking to one of the nurses.", HAMID, 588)
					it.chatNpc("They are so... nice... afterall!", HAMID, 567)
				}
				2 -> {
					it.chatPlayer("What's a Monk doing in a place such as this?", 575)
					it.chatNpc("Well don't tell anyone but I came here because of the<br>nurses!", HAMID, 593)
					it.chatPlayer("Really?", 605)
					it.chatNpc("It beats being stuck in the monastery!", HAMID, 605)
				}
				3 -> {
					it.chatPlayer("Which monastery do you come from?", 575)
					it.chatNpc("I belong to the monastery north of Falador.", HAMID, 588)
					it.chatPlayer("You're a long way from home?", 575)
					it.chatNpc("Yeh. I miss the guys.", HAMID, 610)
				}
			}
		}
	}
	
}
