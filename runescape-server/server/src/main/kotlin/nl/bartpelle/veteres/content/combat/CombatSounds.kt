package nl.bartpelle.veteres.content.combat

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.WeaponType
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 2/8/2016.
 */

object CombatSounds {
	
	fun weapon_attack_sounds(it: Script): Int {
		val weapon = it.player().equipment()[EquipSlot.WEAPON]?.id() ?: -1;
		val equiptype = it.player().world().equipmentInfo().weaponType(weapon)
		
		// Special cases that don't match the below
		when (weapon) {
		//Godswords
			11802, 20368 -> return 3846 //Aramdyl godsword
			11804, 20370 -> return 3846 //Bandos godsword
			11806, 20372 -> return 3846 //Saradomin godsword
			11808, 20374 -> return 3846 //Zamorak godsword
		
		//Wands
			6908 -> return 2563 //Beginner wand
			6910 -> return 2563 //Apprentice wand
			6912 -> return 2563 //Teacher wand
			6914 -> return 2563 //Master wand
			10150 -> return 2563 //Noose wand
			11012 -> return 2563 //Wand
			11012 -> return 2563 //Infused wand
			12422 -> return 2563 //3rd age wand
		
		//Misc items
			4153 -> return 2714 //Granite Maul
			4726 -> return 1328 //Guthan's spear
			4755 -> return 1323 //Verac's flail
			4747 -> return 1332 //Torag's hammers
			4718 -> return 1321 //Dharok's greataxe
			6528 -> return 2520 //Tzhaar-ket-om
		}
		
		// Fallback cases
		when (equiptype) {
			WeaponType.LONGSWORD -> return 2500
			WeaponType.DAGGER -> return 2517
			WeaponType.PICKAXE -> return 2498
			WeaponType.MAGIC_STAFF -> return 2555
			WeaponType.AXE -> return 2498
			WeaponType.MACE -> return 2508
			WeaponType.HAMMER -> return 2567
			WeaponType.CROSSBOW -> return 2695
			WeaponType.BOW -> return 2693
			WeaponType.THROWN -> return 2696
			WeaponType.WHIP -> return 2720
		}
		
		return -1
	}
	
	@JvmStatic fun block_sound(player: Player, hit: Int): Int {
		val shield = player.equipment()[EquipSlot.SHIELD]?.id() ?: -1;
		
		if (shield != -1) {
			return player.world().random(arrayOf(2860, 2861, 2862, 2863))
		}
		
		return 511 // Block without shield
	}
	
	@JvmStatic fun damage_sound(world: World): Int {
		return world.random(arrayOf(518, 509, 510))
	}
	
	// For weapons it's bound to be off of the attack styles interface
	fun weapon_equip_sounds(player: Player, slot: Int, itemid: Int) {
		val other = player.world().random(1) == 1
		var sound = if (other) 2238 else 2244
		val name = Item(itemid).name(player.world()).toLowerCase()
		val metal = name.contains("plate") || name.contains("sword") || name.contains("dagger") || name.contains("hammer")
		val shield = name.contains("shield")
		val chaps = name.contains("chaps") // dhide body aint same sound
		val bow = name.contains("bow")
		if (bow)
			sound = 2244
		else if (metal && slot == 3)
			sound = 2248 // metal 'ting' like platebody wearing
		if (shield)
			sound = 2245
		if (chaps)
			sound = 2241
		if (metal && slot != 3)
			sound = 2242
		when (slot) {
			EquipSlot.HEAD -> 2240
			EquipSlot.HANDS -> 2236
			EquipSlot.FEET -> 2237
		}
		if (slot == 3 && name.contains("dark bow"))
			sound = 3738
		if (name.contains("ava"))
			sound = 3284
		
		player.sound(sound)
	}
}