package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/20/2016.
 */

object MenAndWoman {
	
	val MEN_AND_WOMAN = arrayListOf(3080, 3082, 3087, 3078, 3084, 3083, 3079, 3085, 3086, 3652)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (npc in MEN_AND_WOMAN) {
			r.onNpcOption1(npc) @Suspendable {
				it.chatPlayer("Hello, how's it going?", 567)
				
				val dialogue = it.player().world().random(13)
				
				when (dialogue) {
					1 -> it.chatNpc("I'm busy right now.", npc, 588)
					2 -> {
						it.chatPlayer("Do you wish to trade?", 588)
						it.chatNpc("No, I have nothing I wish to get rid of. If you want to<br>do some trading," +
								" there are plenty of shops and market<br>stalls around though.", npc, 590)
					}
					3 -> {
						it.chatNpc("I'm fine, how are you?", npc, 567)
						it.chatPlayer("Very well thank you.", 567)
					}
					4 -> it.chatNpc("I'm very well thank you.", npc, 567)
					5 -> {
						it.chatNpc("How can I help you?", npc, 588)
						when (it.options("Do you wish to trade?", "I'm in cycle of a quest.", "I'm in cycle of enemies to kill.")) {
							1 -> {
								it.chatPlayer("Do you wish to trade?", 588)
								it.chatNpc("No, I have nothing I wish to get rid of. If you want to<br>do some trading," +
										" there are plenty of shops and market<br>stalls around though.", npc, 590)
							}
							2 -> {
								it.chatPlayer("I'm in cycle of a quest.", 567)
								it.chatNpc("I'm sorry I can't help you there.", npc, 588)
							}
							3 -> {
								it.chatNpc("I've heard there are many fearsome creatures that<br>dwell under the ground...", npc, 589)
								
							}
						}
					}
					6 -> it.chatNpc("Get out of my way, I'm in a hurry!", npc, 614)
					7 -> {
						it.chatNpc("Who are you?", npc, 575)
						it.chatPlayer("I'm a bold adventurer.", 567)
						it.chatNpc("Ah, a very noble profession.", npc, 567)
						
					}
					8 -> it.chatNpc("No, I don't want to buy anything!", npc, 614)
					9 -> {
						it.chatPlayer("I'm in cycle of a quest.", 567)
						it.chatNpc("I'm sorry I can't help you there.", npc, 588)
					}
					10 -> {
						it.chatNpc("Not too bad, but I'm a little worried about the increase<br>of goblins these days.", npc, 589)
						it.chatPlayer("Don't worry, I'll kill them.", 567)
					}
					11 -> it.chatNpc("Hello there! Nice weather we've been having.", npc, 567)
					12 -> it.chatNpc("I think we need a new king. The one we've got isn't<br>very good.", npc, 589)
					13 -> it.chatNpc("I'm a little worried - I've heard there's lots of people<br>going about, killing citizens at random.", npc, 589)
				}
				
			}
		}
	}
	
}
