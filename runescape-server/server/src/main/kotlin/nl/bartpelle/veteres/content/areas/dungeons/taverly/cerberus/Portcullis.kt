package nl.bartpelle.veteres.content.areas.dungeons.taverly.cerberus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/7/2016.
 */

object Portcullis {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(21772) {
			val destination = when (it.interactionObject().tile()) {
				Tile(1239, 1225) -> Tile(1291, 1253) //West
				Tile(1367, 1225) -> Tile(1328, 1253) //East
				Tile(1303, 1289) -> Tile(1309, 1269) //North
				else -> Tile(0, 0)
			}
			
			if (it.interactionOption() == 1) {
				teleport_player(it, destination)
			}
			
		}
	}
	
	@Suspendable fun teleport_player(it: Script, tile: Tile) {
		when (it.optionsTitled("Do you wish to leave?", "Yes, I'm scared.", "Nah, I'll stay.")) {
			1 -> {
				it.player().lock()
				it.player().interfaces().sendWidgetOn(174, Interfaces.InterSwitches.C)
				it.player().invokeScript(951)
				it.delay(2)
				it.player().teleport(tile)
				it.delay(2)
				it.player().invokeScript(948, 0, 0, 0, 255, 50)
				it.player().interfaces().sendWidgetOn(246, Interfaces.InterSwitches.X)
				it.player().unlock()
				it.player().interfaces().closeById(174) // Guess work added (31/8/16) unsure if correct
				it.player().interfaces().closeById(246)
			}
		}
	}
}