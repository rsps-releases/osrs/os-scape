package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.NumberUtils
import java.text.NumberFormat

object BraceletOfEthereum {

    const val CHARGED = 21816
    private const val ETHER = 21820
    private const val UNCHARGED = 21817
    private const val MAX_CHARGES = 16000

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        /**
         * Uncharged
         */
        r.onItemOption3(UNCHARGED) @Suspendable { toggleAbsorption(it) }
        r.onItemOption4(UNCHARGED) @Suspendable { dismantle(it) }

        r.onEquipmentOption(1, UNCHARGED) @Suspendable {
            it.message(Color.DARK_GREEN.wrap("This bracelet is doesn't contain any charges. Use revenant ether on it to charge it."))
        }
        r.onEquipmentOption(2, UNCHARGED) @Suspendable { toggleAbsorption(it) }

        r.onItemOnItem(UNCHARGED, ETHER) { charge(it) }

        /**
         * Charged
         */
        r.onItemOption3(CHARGED) @Suspendable { check(it.player(), it.itemUsed()) }
        r.onItemOption4(CHARGED) @Suspendable { toggleAbsorption(it) }
        r.onItemOption5(CHARGED) @Suspendable { uncharge(it) }

        r.onEquipmentOption(1, CHARGED) @Suspendable { check(it.player(), it.itemUsed()) }
        r.onEquipmentOption(2, CHARGED) @Suspendable { toggleAbsorption(it) }

        r.onItemOnItem(CHARGED, ETHER) { charge(it) }
    }

    @Suspendable private fun toggleAbsorption(it: Script) {
        val player = it.player()
        val absorption = player.attribOr<Boolean>(AttributeKey.ETHEREUM_ABSORPTION, true)
        player.message(Color.DARK_GREEN.wrap("Bracelet of Ethereum absorption is now toggled ${if (absorption) "off" else "on"}."))
        player.putattrib(AttributeKey.ETHEREUM_ABSORPTION, !absorption)
    }

    @Suspendable private fun check(player: Player, bracelet: Item) {
        val ether: String
        val etherAmount = bracelet.property(ItemAttrib.CHARGES)
        ether = if (etherAmount == 0)
            "0.0%, 0 ether"
        else
            NumberUtils.formatOnePlace(etherAmount.toDouble() / MAX_CHARGES * 100.0) + "%, " + NumberFormat.getInstance().format(etherAmount) + " ether"
        player.message("Revenant ether: ${Color.DARK_GREEN.wrap(ether)}")
    }

    @Suspendable private fun charge(it: Script) {
        val player = it.player()

        val braceletSlot = if(it.itemUsed().id() == ETHER) it.itemOnSlot() else it.itemUsedSlot()
        val bracelet = player.inventory()[braceletSlot]
        val etherSlot = player.inventory().slotOf(ETHER)
        val ether = player.inventory()[etherSlot]
        val braceletCharges = player.inventory()[braceletSlot].property(ItemAttrib.CHARGES)

        val chargesToAdd = Math.min(MAX_CHARGES - braceletCharges, ether.amount())
        if (chargesToAdd == 0 && bracelet.property(ItemAttrib.CHARGES) > 0) {
            player.message(Color.DARK_GREEN.wrap("The bracelet can't hold anymore ether."))
            return
        }
        if (player.inventory().remove(Item(ETHER, chargesToAdd), false).success()) {
            if (bracelet.id() == UNCHARGED) {
                player.inventory().replace(braceletSlot, CHARGED)
                player.inventory()[braceletSlot].property(ItemAttrib.CHARGES, braceletCharges + chargesToAdd)
                check(player, player.inventory()[braceletSlot])
            } else {
                player.inventory()[braceletSlot].property(ItemAttrib.CHARGES, braceletCharges + chargesToAdd)
                check(player, player.inventory()[braceletSlot])
            }
        }
    }

    @Suspendable private fun uncharge(it: Script) {
        val player = it.player()

        if(player.inventory().full()) {
            player.message("You don't have enough inventory space to uncharge the bracelet.")
            return
        }
        it.messagebox2("<col=7f0000>Warning!</col><br>Uncharging your Bracelet of Ethereum will result in all the revenant ether being ${Color.DARK_RED.wrap("destroyed")}.")
        if(it.optionsTitled("Uncharge your Bracelet of Ethereum?", "Yes, uncharge the bracelet.", "No, I want to keep my bracelet!") == 1) {
            player.inventory().replace(it.itemUsedSlot(), UNCHARGED)
            it.itemBox("You remove all the charges from your Bracelet of Etherum.", UNCHARGED)
        }
    }

    @JvmStatic @Suspendable fun consumeCharge(player: Player, bracelet: Item): Boolean {
        return if(bracelet.id() == CHARGED) {
            if(bracelet.modifyProperty(ItemAttrib.CHARGES, -1, 0) <= 0) {
                player.message(Color.DARK_GREEN.wrap("Your bracelet has ran out of charges!"))
                player.equipment().replace(EquipSlot.HANDS, UNCHARGED)
            }
            true
        } else false
    }

    @Suspendable private fun dismantle(it: Script) {
        val player = it.player()
        val bracelet = it.itemUsed()

        it.doubleItemBox("Dismantling your bracelet will result in the bracelet being ${Color.DARK_RED.wrap("destroyed")} for 1,000 ether.", bracelet, Item(ETHER, 1000))
        when(it.optionsTitled("Destroy your Bracelet of Ethereum",
                "Yes, destroy the bracelet!",
                "No, I want to keep my bracelet!")) {
            1 -> {
                if(player.inventory().remove(bracelet, false).success()) {
                    player.inventory().add(Item(ETHER, 1000))
                    it.itemBox("You dismantle your bracelet and collect 1,000 revenant ethers.", ETHER, 1000)
                }
            }
        }
    }
}