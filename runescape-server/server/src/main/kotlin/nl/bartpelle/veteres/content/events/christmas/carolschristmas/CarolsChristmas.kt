package nl.bartpelle.veteres.content.events.christmas.carolschristmas

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 2016-12-15.
 */

object CarolsChristmas {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit { it.ctx<World>().removeObj(MapObj(Tile(1254, 3572, 0), 28594, 0, 0), true) }
		r.onRegionExit(12627) {
			it.player().looks().transmog(-1)
			it.player().looks().resetRender()
		}
	}
	
	@JvmStatic fun getStage(player: Player): Int {
		return player.attribOr(AttributeKey.CAROLS_CHRISTMAS, 0)
	}
	
	@JvmStatic fun setStage(player: Player, toSet: Int) {
		player.putattrib(AttributeKey.CAROLS_CHRISTMAS, toSet)
	}
	
	@JvmStatic fun setVarbit(player: Player, toSet: Int) {
		player.varps().varbit(Varbit.CAROL_CHRISTMAS_EVENT, toSet)
	}
	
}
