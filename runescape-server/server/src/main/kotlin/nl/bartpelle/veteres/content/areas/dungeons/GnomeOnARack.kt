package nl.bartpelle.veteres.content.areas.dungeons

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/19/2016.
 */

object GnomeOnARack {
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(8794) @Suspendable { it.chatNpc("Daaaaammnnn ${it.player().name()}, this feels goooood.", 4233, 584) }
	}
}
