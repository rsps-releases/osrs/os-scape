package nl.bartpelle.veteres.content.areas.gnomestronghold

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/18/2015.
 */

object GnomeStronghold {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(16675) {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			when (obj.tile()) {
				Tile(2444, 3414) -> it.player().teleport(2445, 3416, 1)
				Tile(2445, 3434) -> it.player().teleport(2445, 3433, 1)
			}
		}
		r.onObject(16677) {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			when (obj.tile()) {
				Tile(2445, 3415, 1) -> it.player().teleport(2446, 3415, 0)
				Tile(2445, 3434, 1) -> it.player().teleport(2444, 3434, 0)
			}
		}
	}
	
}