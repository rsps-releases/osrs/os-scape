package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Realm
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.PlayerDamageTracker
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

object WildernessEventController {
	
	/**
	 * Boss event data. Contains all the types of boss events that can occur - sequentially - across the server.
	 */
	enum class BossEvent(val npc: Int, val desc: String, val bmPerHit: Int = 0, val lootTable: kotlin.Function2<Player, Boolean, List<Item>>? = null) {
		SKOTIZO(7286, "Skotizo", 5, WildernessEventController::skotizoDropTable),
		TEKTON(7542, "Tekton", 5, WildernessEventController::tektonTable),
		VORKATH(8061, "Vorkath", 5, WildernessEventController::vorkathTable),
		NOTHING(-1, "Nothing"), // Filler
	}
	
	/**
	 * An array of possible boss spawns. Chosen at random when a boss spawns.
	 */
	val POSSIBLE_SPAWNS = arrayOf(
			Tile(3307, 3663),
			Tile(3166, 3832),
			Tile(3070, 3725),
			Tile(3034, 3682)
	)
	
	val ANNOUNCED_RARE_ITEMS = intArrayOf(
			22002,
			21907,
			22111,
			21034,
			21012,
			11284,
			21633,
			21009,
			21028,
			21034,
			21018,
			21021,
			21024,
			21006,
			21015,
			21003
	)
	
	/**
	 * A comparartor that sorts player damages by the most damage done, descending.
	 */
	val DAMAGE_COMP = Comparator<MutableMap.MutableEntry<Int, PlayerDamageTracker>> {
		a, b ->
		b.value.damage() - a.value.damage()
	}
	
	/**
	 * The interval at which server-wide Wilderness events occur.
	 */
	const val BOSS_EVENT_INTERVAL = 2000 // Currently 20 minutes.
	
	/**
	 * The timeframe in which the NPC despawns before the next event. This means that, if for example
	 * the BOSS_EVENT_INTERVAL is 20 minutes and the DESPAWN_MARGIN is 2 minutes, the boss is alive for at most 18 min.
	 */
	const val DESPAWN_MARGIN = 200 // 2 minutes
	
	/**
	 * The active event being ran in the Wilderness.
	 */
	var activeEvent = BossEvent.NOTHING
	
	/**
	 * The rotation of events, executed in sequence.
	 */
	val EVENT_ROTATION = arrayOf(BossEvent.VORKATH, BossEvent.SKOTIZO, BossEvent.TEKTON)

	/**
	 * The NPC reference for the active event.
	 */
	var activeNpc: Optional<Npc> = Optional.empty()
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// When the world starts, fire the boss event timer.
		r.onWorldInit {
			if (it.ctx<World>().realm().isPVP) { // Events only on PvP.
				it.ctx<World>().timers().register(TimerKey.WILDERNESS_BOSS_TIMER, BOSS_EVENT_INTERVAL)

			}
		}
		
		// Register the timer to do an event..
		r.onWorldTimer(TimerKey.WILDERNESS_BOSS_TIMER) {
			startEvent(it.ctx())
			it.ctx<World>().timers().register(TimerKey.WILDERNESS_BOSS_TIMER, BOSS_EVENT_INTERVAL)
		}
		
		// Register timer to forcefully end the current event.
		r.onWorldTimer(TimerKey.WILDERNESS_BOSS_TIMEOUT) {
			terminateActiveEvent(it.ctx(), false)
		}
	}
	
	/**
	 * Terminates the currently active event. If the event is an actual boss spawn, the end is being announced too.
	 */
	fun terminateActiveEvent(world: World, force: Boolean) {
		// The 'NOTHING' event spawns nothing, so no need to despawn anything.
		if (activeEvent != BossEvent.NOTHING) {
			var despawned = false
			
			world.npcs().forEachKt({
				if (it != null && it.id() == activeEvent.npc && (it.hp() > 0 || force)) {
					it.lock()
					it.stopActions(true)
					world.unregisterNpc(it)
					despawned = true
				}
			})

            //The active npc reference.
            this.activeNpc = Optional.empty()
			
			if (despawned) {
				if (!world.realm().isDeadman && !world.server().config().hasPathOrNull("deadman.jaktestserver"))
					world.filterableAnnounce("<col=6a1a18><img=15> ${activeEvent.desc} has despawned.")
			}
		}
	}
	
	/**
	 * Starts the next event in the list of events. Since events always run in sequence, players can predict what the
	 * next event will be and when.
	 */
	fun startEvent(world: World) {
		// First despawn the npc if existing
		terminateActiveEvent(world, true)
		
		val oldId = EVENT_ROTATION.indexOf(activeEvent)
		val newId = (oldId + 1) % EVENT_ROTATION.size
		activeEvent = EVENT_ROTATION[newId]
		
		// Only if it's an actual boss we spawn an NPC.
		if (activeEvent != BossEvent.NOTHING) {
			val boss = Npc(activeEvent.npc, world, POSSIBLE_SPAWNS.random())
			boss.respawns(false)
			boss.walkRadius(5)
			world.registerNpc(boss)

            //Assign the npc reference.
            this.activeNpc = Optional.of(boss)
			
			// Broadcast it
			if (!world.realm().isDeadman) {
				world.filterableAnnounce("<col=6a1a18><img=15> ${activeEvent.desc} has been spotted in level " +
						WildernessLevelIndicator.wildernessLevel(boss.tile()) + " Wilderness! It despawns in 28 minutes. Hurry!")
				
				world.players().forEachKt({ other -> other.write(AddMessage("${activeEvent.desc} has been spotted in level " +
						WildernessLevelIndicator.wildernessLevel(boss.tile()) + " Wilderness!", AddMessage.Type.BROADCAST)) })
			}
			
			// Fire the timer for despawning.
			boss.world().timers().register(TimerKey.WILDERNESS_BOSS_TIMEOUT, BOSS_EVENT_INTERVAL - DESPAWN_MARGIN)
		}
	}
	
	/**
	 * A reusable script that should be bound to every boss that can be killed in the wilderness through this controller.
	 * An npc with this script rewards the attackers with damage based on their input, and rewards the best damager
	 * with a possibly rare reward.
	 */
	@JvmField val bossDeathScript: Function1<Script, Unit> = @Suspendable {
		// Reward dmg * X BM
		if (activeEvent.bmPerHit > 0) {
			rewardDamagersWithBloodmoney(it.npc())
		}
		
		// Give unique rewards
		rewardBestDamagers(it.npc())
		
		if (!it.npc().world().realm().isDeadman && !it.npc().world().server().config().hasPathOrNull("deadman.jaktestserver"))
			it.npc().world().filterableAnnounce("<col=6a1a18><img=50> ${activeEvent.desc} has been killed. It will respawn shortly.")
	}
	
	/**
	 * Rewards all players who have participated damage to the boss with a certain amount of Blood money (or GP).
	 * Only players close enough to the boss during this method call will be rewarded.
	 */
	fun rewardDamagersWithBloodmoney(npc: Npc) {
		npc.damagers().forEach { dmg ->
			val playerId = dmg.key
			
			val player = npc.world().playerForId(playerId)
			if (player.isPresent) {
				// Compute what this player gets
				val hits = dmg.value.damage()
				val bm = hits * activeEvent.bmPerHit
				val coins = hits * (activeEvent.bmPerHit * 250)
				
				val p = player.get()
				
				// Only people nearby are rewarded. This is to avoid people 'poking' the boss to do some damage
				// without really risking being there.
				if (p in npc.bounds(15)) {
					// On our economy worlds, you get coins. Otherwise you get Blood money.
					if (npc.world().realm().isOSRune || npc.world().realm().isRealism) {
						if (p.inventory().add(Item(995, coins), false).failed()) {
							// Didn't work. Add ground item.
							val groundItem = GroundItem(p.world(), Item(995, coins), p.tile(), p.id())
							p.world().spawnGroundItem(groundItem)
						}

						p.message("<col=6a1a18><img=15> You have been given $coins coins for your efforts towards slaying this boss.")
					} else if (npc.world().realm().isPVP) {
						if (p.inventory().add(Item(13307, bm), false).failed()) {
							// Didn't work. Add ground item.
							val groundItem = GroundItem(p.world(), Item(13307, bm), p.tile(), p.id())
							p.world().spawnGroundItem(groundItem)
						}
						
						p.message("<col=6a1a18><img=15> You have been given $bm Blood money for your efforts towards slaying this boss.")
					}
				}
			}
		}
	}
	
	/**
	 * Rewards the top 4 damagers (one best and 3 rest) damagers of the boss with better rewards.
	 */
	fun rewardBestDamagers(npc: Npc) {
		val sorted = npc.damagers().entries.sortedWith(DAMAGE_COMP)
		val best = sorted.take(1)
		val rest = sorted.take(4).drop(1) // Yeah, that looks stupid, but the take(1) above doesn't modify the list.
		val world = npc.world()
		
		// Do we have a top killer?
		if (best.isNotEmpty() && world.playerForId(best[0].key).isPresent) {
			val player = world.playerForId(best[0].key).get()
			val rewards = activeEvent.lootTable!!(player, true)
			
			// Add to inventory or drop to floor.
			for (reward in rewards) {
				if (player.inventory().add(reward, false).failed()) {
					// Didn't work. Add ground item.
					val groundItem = GroundItem(world, reward, player.tile(), player.id())
					world.spawnGroundItem(groundItem)
					player.message("<col=6a1a18><img=15> You have been awarded ${reward.amount()} x ${reward.unnote(player.world()).name(player.world())} for killing this boss. It's at your feet.")
				} else {
					player.message("<col=6a1a18><img=15> You have been awarded ${reward.amount()} x ${reward.unnote(player.world()).name(player.world())} for killing this boss.")
				}
				
				// Is that a rare!?
				if (reward in CLUE_REWARDS || reward.id() in ANNOUNCED_RARE_ITEMS) {
					player.world().broadcast("<col=6a1a18><img=15> ${player.name()} has received a rare item from ${activeEvent.name}: ${reward.unnote(player.world()).name(player.world())}!")
				}
			}
		}
		
		// Do some shittier loot for the other 3 best killers.
		for (damager in rest) {
			if (world.playerForId(damager.key).isPresent) {
				val player = world.playerForId(damager.key).get()
				val reward = activeEvent.lootTable!!(player, false)[0]
				
				if (player.inventory().add(reward, false).failed()) {
					// Didn't work. Add ground item.
					val groundItem = GroundItem(world, reward, player.tile(), player.id())
					world.spawnGroundItem(groundItem)
					player.message("<col=6a1a18><img=15> You have been awarded ${reward.amount()} x ${reward.unnote(player.world()).name(player.world())} for killing ${activeEvent.name}. It's at your feet.")
				} else {
					player.message("<col=6a1a18><img=15> You have been awarded ${reward.amount()} x ${reward.unnote(player.world()).name(player.world())} for killing ${activeEvent.name}.")
				}
			}
		}
	}
	
	fun unlockPet(player: Player, pet: Pet) {
		if (!PetAI.hasUnlocked(player, pet)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(pet.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, pet, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(pet.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(pet.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
	fun skotizoDropTable(player: Player, isBestDamager: Boolean): List<Item> {
		val realm = player.world().realm()
		
		// Does our player get a pet Bloodhound?
		val odds = (100 * player.mode().skillPetMod()).toInt()
		if (isBestDamager && player.world().rollDie(odds, 1)) {
			unlockPet(player, Pet.SKOTOS) // Used to be Bloodhound. Will be reintroduced later.
		}

		// Do we get a nice random item?
		if (isBestDamager) {
			return listOf(randomTektonDrop(player))
		} else {
			return listOf(Item(13307, player.world().random(2000..5000)))
		}
	}
	
	fun tektonTable(player: Player, isBestDamager: Boolean): List<Item> {
		// Does our player get a pet Bloodhound?
		val odds = (100 * player.mode().skillPetMod()).toInt()
		if (isBestDamager && player.world().rollDie(odds, 1)) {
			unlockPet(player, Pet.OLMLET)
		}
		
		// Do we get a nice random item?
		if (isBestDamager) {
			return listOf(randomTektonDrop(player))
		} else {
			return listOf(Item(13307, player.world().random(2000..5000)))
		}
	}
	
	fun randomTektonDrop(player: Player): Item {
		val roll = player.world().random(20)
		
		if (roll == 0) {
			return TEKTON_RARE.random()
		} else if (roll == 1 || roll == 2) {
			return TEKTON_COMMON.random()
		} else {
			return Item(13307, player.world().random(2000..5000)) // Blood money
		}
	}
	
	fun vorkathTable(player: Player, isBestDamager: Boolean): List<Item> {
		// Does our player get a pet Bloodhound?
		val odds = (100 * player.mode().skillPetMod()).toInt()
		if (isBestDamager && player.world().rollDie(odds, 1)) {
			unlockPet(player, Pet.VORKI)
		}
		
		// Do we get a nice random item?
		if (isBestDamager) {
			return listOf(randomVorkathDrop(player))
		} else {
			return listOf(Item(13307, player.world().random(3000..6000)))
		}
	}
	
	fun randomVorkathDrop(player: Player): Item {
		val roll = player.world().random(5)
		
		if (roll == 0) {
			return VORKATH_TABLE.random()
		} else {
			return Item(13307, player.world().random(2000..5000)) // Blood money
		}
	}
	
	val REGULAR_ITEMS_W2 = arrayOf(
			Item(13307, 1350), // Blood money
			Item(4151, 1), // Abyssal whip
			Item(13307, 1400), // Blood money
			Item(10551, 1), // Fighter torso
			Item(6735, 1), // Warriors Ring
			Item(13307, 1450), // Blood money
			Item(10547, 1), // Healing hat
			Item(10548, 1), // Fighter hat
			Item(10549, 1), // Runner hat
			Item(10550, 1), // Ranger hat
			Item(13307, 1500), // Blood money
			Item(4153, 1), // Granite maul
			Item(13307, 1550), // Blood money
			Item(11128, 1), // Obby neck
			Item(13307, 1600), //  Blood money
			Item(299, 10) // Mithril seeds
	)
	
	val CLUE_REWARDS = arrayOf(
			Item(13652), // Dragon claws
			Item(20849, 25), // Dragon thrownaxe
			Item(21000), // Twisted buckler
			Item(21012), // Dragon hunter cbow
			Item(19707), // Eternal glory
			Item(19564), // Royal seed pod
			
			Item(20095), // Ankou mask
			Item(20098), // Ankou top
			Item(20101), // Ankou gloves
			Item(20104), // Ankou leggings
			Item(20107), // Ankou socks
			
			Item(20080), // Mummy head
			Item(20083), // Mummy body
			Item(20086), // Mummy hands
			Item(20089), // Mummy legs
			Item(20092), // Mummy feet
			Item(21034) //Prayer scroll
	)
	
	val REGULAR_ITEMS_W1_3 = arrayOf(
			Item(11237, 50), // Dragon arrowtips
			Item(13440, 90), // Raw anglerfish
			Item(565, 300), // Blood rune
			Item(560, 400), // Death rune
			Item(566, 500), // Soul rune
			Item(1202, 4), // Rune kiteshield
			Item(1202, 4), // Rune platelegs
			Item(1128, 4), // Rune platebody
			Item(220, 10), // Grimy torstol
			Item(212, 10), // Grimy avantoe
			Item(2364, 15), // Runite bar
			Item(9144, 200), // Runite bolts
			Item(9193, 80), // Dragon bolt tips
			Item(9194, 40), // Onyx bolt tips
			Item(1632, 7), // Uncut dragonstones
			Item(2366, 1), // Shield left half
			Item(2362, 40), // Adamant bars
			Item(537, 20), // Dragon bones
			Item(11943, 10), // Lava dragon bones
			Item(454, 150), // Coal
			Item(1514, 25)  // Magic logs
	)
	
	val TEKTON_RARE = arrayOf(
			Item(21018), // Ancestral hat
			Item(21021), // Ancestral top
			Item(21024), // Ancestral bottom
			Item(21006), // Kodai wand
			Item(21015), // Bulwark
			Item(21003),  // Elder maul
			Item(22481),  // Sanguinesti staff
			Item(22324)  // Ghrazi rapier
	)
	
	val TEKTON_COMMON = arrayOf(
			Item(21009), // Dragon sword
			Item(21028), // Dragon harpoon
			Item(21034)  // Dexterous Prayer scroll
	)
	
	val VORKATH_TABLE = arrayOf(
			Item(21880, 1000), // Matt ward
			Item(22002), // Dragonfire ward
			Item(21907), // Vorkath's head
			Item(22111), // Dragonbone necklace
			Item(21034), // Dexterous Prayer scroll
			Item(21012), // Dragon hunter crossbow
			Item(11284), // Dragonfire shield
			Item(21633),  // Ancient wyvern shield
			Item(22326),  // Justiciar Helm
			Item(22327),  // Justiciar body
			Item(22328)  // Justiciar legs
	)
	
	fun randomRegularLoot(realm: Realm): Item = when (realm) {
		Realm.OSRUNE, Realm.REALISM -> REGULAR_ITEMS_W1_3.random()
		else -> REGULAR_ITEMS_W2.random()
	}
	
	fun randomRareItem(realm: Realm): Item = CLUE_REWARDS.random()
	
}