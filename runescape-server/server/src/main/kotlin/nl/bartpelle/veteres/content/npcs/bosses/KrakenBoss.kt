package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.mechanics.NpcDeath
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle
import java.lang.ref.WeakReference
import java.util.*

/**
 * Created by Jak on 19/10/2016.
 */
object KrakenBoss {
	
	// Npc ids
	@JvmStatic val KRAKEN_WHIRLPOOL: Int = 496
	@JvmStatic val TENTACLE_WHIRLPOOL: Int = 5534
	
	@JvmStatic val KRAKEN_NPCID: Int = 494
	@JvmStatic val TENTACLE_NPCID: Int = 5535
	
	// Object id
	val CREVICE = 537
	
	// Relevent tiles
	val ENTER_TILE = Tile(2280, 10022)
	val BOSS_TILE = Tile(2278, 10034)
	
	// Going from bottom left, top left, top right, bottom right
	val TENT_TILES = arrayOf(Tile(2276, 10033), Tile(2276, 10037), Tile(2283, 10037), Tile(2283, 10033))
	val REAL_ROOM_AREA = Area(Tile(2269, 10023), Tile(2302, 10046))
	
	// Obj id
	val EXIT_CREVICE = 538
	
	//Leave cove obj
	val CAVE_EXIT = 30178
	
	// Tile to teleport to when you leave
	val ROOM_EXIT = Tile(2280, 10016)
	
	// Corner of the region we are going to create an Instance of
	val CORNER = Tile(2240, 9984)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		// Enter crevice
		repo.onObject(CREVICE) @Suspendable {
			val obj = it.interactionObject()
			val opt = it.interactionOption()
			when (opt) {
				1 -> { // Enter
					if (PlayerCombat.inCombat(it.player())) {
						it.messagebox("You can't go in here when under attack.")
						it.message("You can't go in here when under attack.")
					} else {
						it.player().teleport(ENTER_TILE)
					}
				}
				2 -> { // Private, instanced
					val currency = if (it.player().world().realm().isPVP) "Blood money" else "coins"
					val currencyId = if (it.player().world().realm().isPVP) 13307 else 995
					val currencyReq = if (it.player().world().realm().isPVP) 1000 else 25000
					it.itemBox("Would you like to pay the $currencyReq $currency fee to enter an instanced area?<br>" +
							"<col=FF0000>Warning: any items dropped on death are permanently lost.<br>" +
							"When you leave, you'll have to pay again to enter.", currencyId, currencyReq)
					if (it.options("Pay $currencyReq to enter.", "Never mind.") == 1) {
						if (it.player().inventory().remove(Item(currencyId, currencyReq), false).success()) {
							it.message("You pay $currencyReq $currency to enter an instance room.")
							enterKrakenInstance(it)
						} else {
							it.messagebox("You didn't have $currencyReq $currency with you. Would you like to take it from your bank?")
							if (it.options("Pay $currencyReq to enter from the bank.", "Never mind.") == 1) {
								if (it.player().bank().remove(Item(currencyId, currencyReq), false).success()) {
									it.message("You pay $currencyReq $currency from your bank to enter an instance room.")
									enterKrakenInstance(it)
								} else {
									it.message("You didn't have $currencyReq $currency in your bank to pay")
								}
							}
						}
					}
				}
				3 -> { // Look inside
					var count = 0
					it.player().world().players().forEachInAreaKt(REAL_ROOM_AREA, {
						count++
					})
					val strEnd = if (count == 1) "" else "s"
					val isAre = if (count == 1) "is" else "are"
					it.messagebox("There $isAre currently $count player$strEnd in the cave.")
				}
			}
		}
		
		//Leaving cove.
		repo.onObject(CAVE_EXIT) @Suspendable{
			it.player().teleport(3088, 3505)
		}
		
		// Leaving
		repo.onObject(EXIT_CREVICE) @Suspendable {
			val player = it.player()
			if (player.world().allocator().active(player.tile()).isPresent) {
				if (it.optionsTitled("Leave the instance? You cannot return.", "Yes, I want to leave.", "No, I'm staying for now.") == 1) {
					it.player().teleport(ROOM_EXIT)
				}
			} else {
				it.player().teleport(ROOM_EXIT)
			}
		}
		
		// Spawn hook
		repo.onNpcSpawn(KRAKEN_WHIRLPOOL) {
			val kraken = it.npc()
			kraken.combatInfo().respawntime = 9
			val world = kraken.world()
			val mapOptional = it.npc().world().allocator().active(kraken.tile())
			
			// Is it spawned in an Instance?
			if (mapOptional.isPresent) {
				val map = mapOptional.get()
				// Tile 4 tentacle whirlpools at the relevent tiles
				for (tile in TENT_TILES) {
					val instanceTile = PVPAreas.resolveBasic(tile, CORNER, map)
					val tentacle = Npc(TENTACLE_WHIRLPOOL, world, instanceTile).spawnDirection(6)
					world.registerNpc(tentacle)
					tentacle.putattrib(AttributeKey.BOSS_OWNER, kraken)
					
					val list = kraken.attribOr<ArrayList<Npc>>(AttributeKey.MINION_LIST, ArrayList<Npc>())
					list.add(tentacle)
					kraken.putattrib(AttributeKey.MINION_LIST, list)
				}
			} else {
				// Must be spawned into normal world
				for (tile in TENT_TILES) {
					val tentacle = Npc(TENTACLE_WHIRLPOOL, world, tile).spawnDirection(6)
					world.registerNpc(tentacle)
					tentacle.putattrib(AttributeKey.BOSS_OWNER, kraken)
					
					val list = kraken.attribOr<ArrayList<Npc>>(AttributeKey.MINION_LIST, ArrayList<Npc>())
					list.add(tentacle)
					kraken.putattrib(AttributeKey.MINION_LIST, list)
				}
			}
		}
	}
	
	private fun enterKrakenInstance(it: Script) {
		val map = it.player().world().allocator().allocate(64, 64, ROOM_EXIT).get()
		
		// Set instances clipping masks for each height.
		map.set(0, 0, CORNER.transform(0, 0), CORNER.transform(64, 64), 0, true)
		map.setMulti(true).setDangerous(true).setIdentifier(InstancedMapIdentifier.KRAKEN_ROOM)
		map.setCreatedFor(it.player())
		
		// Tele the player inside
		val enter = PVPAreas.resolveBasic(ENTER_TILE, CORNER, map)
		it.player().teleport(enter)
		
		// Spawn npcs
		val kraken = Npc(KRAKEN_WHIRLPOOL, it.player().world(), PVPAreas.resolveBasic(BOSS_TILE, CORNER, map)).spawnDirection(6)
		it.player().world().registerNpc(kraken)
		// Kraken has a onSpawn trigger to spawn minions
	}
	
	// Script executed when a Kraken is hit.
	@JvmField public val hit: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		// This hook is only relevent when we're in whirlpool form.
		if (npc.id() != KRAKEN_WHIRLPOOL || npc.sync().transmog() == KRAKEN_NPCID || npc.hidden()) { // Not transformed yet
			return@s
		}
		// Did the damage come through the prayer channels in Entity#takeHit
		val hit = npc.attribOr<Hit>(AttributeKey.LAST_HIT_DMG, null) ?: return@s
		
		// Must be woken by an entity
		if (hit.origin() == null || !(hit.origin() is Entity)) {
			return@s
		}
		val source = hit.origin() as Entity
		
		val minions = npc.attribOr<ArrayList<Npc>>(AttributeKey.MINION_LIST, ArrayList<Npc>())
		// Have all minions been attacked first?
		var awake = 0
		for (minion in minions) {
			if (minion.sync().transmog() == KrakenBoss.TENTACLE_NPCID) {
				awake++
			} else {
				//System.out.println("minion not awake: "+minion.id()+" "+minion.sync().transmog())
			}
		}
		
		//System.out.println("awake: "+awake)
		// Do transform and retaliate
		if (awake == 4) {
			npc.timers().addOrSet(TimerKey.COMBAT_ATTACK, 1)
			npc.putattrib(AttributeKey.TARGET, WeakReference<Entity>(source))
			npc.putattrib(AttributeKey.LAST_TARGET, source)
			
			npc.sync().transmog(KRAKEN_NPCID)
			npc.combatInfo(it.npc().world().combatInfo(KRAKEN_NPCID)) // Quickly replace scripts for retaliation before Java finishes processing.
			npc.animate(7135)
		} else {
			val amt = when (awake) {
				1 -> "other three tentacles"
				2 -> "other two tentacles"
				3 -> "last tentacle"
				else -> "four tentacles"
			}
			source?.message("The $amt need to be disturbed before the Kraken emerges.")
		}
	}
	
	// Script executes for kraken boss combat
	@JvmField public val combat: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, false, 15)) {
				if (EntityCombat.attackTimerReady(npc)) {
					val tileDist = npc.tile().distance(target.tile())
					val delay = Math.max(1, (20 + (tileDist * 12)) / 25)
					
					npc.animate(npc.attackAnimation())
					npc.world().spawnProjectile(npc.tile().transform(1, 0), target, 156, 30, 30, 32, 10 * tileDist, 14, 5)
					
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
						target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC)
					} else {
						target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC) // Uh-oh, that's a miss.
					}
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
					
					// Make the minions focus our target too!
					val minions = npc.attribOr<ArrayList<Npc>>(AttributeKey.MINION_LIST, ArrayList<Npc>())
					for (minion in minions) {
						if (minion.sync().transmog() == KrakenBoss.TENTACLE_NPCID) {
							if (!minion.dead() && minion.attribOr<WeakReference<Entity>>(AttributeKey.TARGET, WeakReference<Entity>(null)) != target) {
								minion.attack(target)
							}
						}
					}
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	// Death script for kraken boss
	@JvmField public val death: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		// Clear context to make sure script can't be interrupted
		it.clearContext()
		
		npc.sync().transmog(KRAKEN_WHIRLPOOL)
		
		// Then do the death anim on all tentacles
		val minions = npc.attribOr<ArrayList<Npc>>(AttributeKey.MINION_LIST, ArrayList<Npc>())
		for (minion in minions) {
			if (!minion.hidden()) { // Already dead maybe from recoil/venom
				minion.stopActions(false)
				NpcDeath.deathReset(minion)
				minion.animate(minion.combatInfo().animations.death)
			}
		}
		// Wait 2 for tents to die
		it.delay(2)
		// Hide them
		for (minion in minions) {
			minion.hidden(true)
			minion.sync().transmog(TENTACLE_WHIRLPOOL) // Set it back to the whirlpool
			minion.combatInfo(minion.world().combatInfo(TENTACLE_WHIRLPOOL))
			minion.hp(minion.maxHp(), 0)
		}
		
		it.delay(4)
		// Transmog kraken info after the drop table is done otherwise it'll look for the wrong table
		npc.combatInfo(npc.world().combatInfo(KRAKEN_WHIRLPOOL))
		
		// Wait some seconds - during this time the default NpcDeath script will respawn the Kraken boss
		it.delay(7)
		
		// Respawn the minions
		for (m in minions) {
			NpcDeath.respawn(m)
		}
	}
}