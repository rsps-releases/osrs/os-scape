package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * @author Mack
 */
object SnowInteractions {

    private const val SNOW_PATCH = 15615
    val fakePartyhats = arrayOf(20900, 20901, 20902, 20903)

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onObject(SNOW_PATCH, @Suspendable {
            if (!it.player().busy()) {
                gatherSnow(it)
            }
        })
    }

    @Suspendable
    fun gatherSnow(it: Script) {
        val snowItem = Item(10501, 3)
        if (!it.player().inventory().has(snowItem) && it.player().inventory().freeSlots() == 0) {
            it.messagebox("You don't have enough room to carry anymore snow!")
            return
        }

        it.message("You begin to collect some snow...")
        it.player().busy(true)

        while (true) {
            it.delay(2)

            if (!it.player().busy()) {
                break
            }
            if (it.player().inventory().add(snowItem).failed()) {
                it.messagebox("You don't have enough room to carry anymore snow!")
                break
            }

            it.player().sync().animation(5067, 0)
            it.message("You manage to collect some snow.")
            it.delay(4)
        }
    }

    val pickupPaperhat: Function1<Script, Unit> = s@ @Suspendable {
        if (it.ctx<Any>() !is Player) return@s
        val player = it.player() ?: return@s

        if (Objects.nonNull(player)) {
            if (player.dead()) return@s

            player.lockDamageOk()
            player.putattrib(AttributeKey.FORCED_HIT, true)
            it.delay(1)
            player.animate(827)
            it.delay(1)
            player.sync().hit(Hit(255, Hit.Type.REGULAR, 0, true))
            player.sync().shout("Ow!")
            it.delay(1)
            player.animate(837)
            player.message("<col=0000FF>You need a Strength level of 120 to pick up the paperhat.</col>")
            it.delay(6)
            player.animate(65535)
            player.clearattrib(AttributeKey.FORCED_HIT)
            player.unlock()
        }
    }
}