package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-11.
 */

object RangingGuildDoorman {
	
	val RANGING_GUILD_DOORMAN = 6057
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(RANGING_GUILD_DOORMAN) @Suspendable {
			val player = it.player()
			val rangeLevel = player.skills().level(Skills.RANGED)
			
			it.chatPlayer("Hello there.", 567)
			it.chatNpc("Greetings. If you are an experienced archer, you may want to visit the guild here...", RANGING_GUILD_DOORMAN, 568)
		}
	}
}
