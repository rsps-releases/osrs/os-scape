package nl.bartpelle.veteres.content.items.boxes

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.Pet.*
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Bart on 3/17/2016.
 */
object PetBox {
	
	val unlockable = arrayOf(
			ABYSSAL_ORPHAN,
			//BABY_DISABLED_MOLE, <--- OBTAINABLE VIA LOYALTY, DO NOT ENABLE
			CALLISTO_CUB,
			HELLPUPPY,
			KALPHITE_PRINCESS,
			PET_CHAOS_ELEMENTAL,
			PET_DAGANNOTH_PRIME,
			PET_DAGANNOTH_REX,
			PET_DAGGANOTH_SUPREME,
			PET_DARK_CORE,
			PET_GENERAL_GRAARDOR,
			PET_KRIL_TSUTSAROTH,
			PET_KRAKEN,
			PET_KREEARRA,
			PET_PENANCE_QUEEN,
			PET_SMOKE_DEVIL,
			PET_ZILYANA,
			PRINCE_BLACK_DRAGON,
			SCORPIAS_OFFSPRING,
			TZREK_JAD,
			VENENATIS_SPIDERLING,
			BEAVER,
			HERON,
			ROCK_GOLEM,
			BLOODHOUND,
			ROCKY,
			GIANT_SQUIRREL,
			TANGLEROOT,
			RIFT_GUARDIAN_FIRE,
			CHOMPY_CHICK,
			OLMLET,
			JAL_NIB_REK,
			PHOENIX,
			NOON,
			SKOTOS,
			VORKI
	)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(8148) @Suspendable {
			it.itemBox("The box you are about to open will give you a random pet you haven't unlocked. The pet is untradable, the box is tradable.", 8871)
			if (it.options("Open the box", "No thank you") == 1) {
				unlockPet(it)
			}
		}
	}
	
	@Suspendable fun unlockPet(it: Script) {
		// Create a shuffled list to begin with
		val list = LinkedList(Arrays.asList(*unlockable))
		Collections.shuffle(list)
		
		// Remove any that we've unlocked (or in inventory, or bank)
		for (pet in Pet.values()) {
			if ((pet.varbit != -1 && it.player().varps().varbit(pet.varbit) == 1)
					|| it.player().inventory().has(pet.item) || it.player().bank().has(pet.item)) {
				list.remove(pet)
			}
		}
		
		// If there's any left, remove the box and add a pet.
		if (list.size > 0 && it.player().inventory().remove(Item(8148), true).success()) {
			val reward = list.first
			it.player().inventory().add(Item(reward.item), true)
			// Set varbit to make sure we're unlocking it at all cost
			if (reward.varbit != -1) {
				it.player().varps().varbit(reward.varbit, 1)
			}
			val more = if (list.size == 1) "no" else ("" + (list.size - 1))
			val s = if (list.size == 2) "" else "s"
			it.message("Congratulations! You have unlocked the pet '${Item(reward.item).name(it.player().world())}'!")
			it.message("You can unlock $more more pet$s.")
			it.itemBox("Congratulations! You have unlocked the pet '${Item(reward.item).name(it.player().world())}'! You can unlock $more more pet$s.", reward.item)
			it.player().world().broadcast("<col=844e0d><img=22> ${it.player().name()} unlocked a pet from a Pet Box: ${Item(reward.item).name(it.player().world())}.")
			
			if (list.size == 1) {
				it.itemBox("Warning: you can no longer unlock pets, as you have collected them all! " +
						"Opening another box will not be possible. You can, however, trade the boxes to other players.", 8148)
			}
		} else {
			it.itemBox("You have no more pets to unlock! You can hold your box until a new pet comes out, or sell it to other players.", 8148)
		}
	}
	
}