package nl.bartpelle.veteres.content.quests.free

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interfaces.questtab.QuestTabButtons
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.content.quests.Quests
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/7/2016.
 */

object CooksAssistant {
	
	val STAGE_VARP = 29
	
	val BUCKET_OF_MILK = 1927
	val POT_OF_FLOUR = 1933
	val EGG = 1944
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		QuestTabButtons.ECO_HANDLERS_FREE[1] = { it: Script ->
			QuestGuide.clear(it.player())
			it.player().interfaces().text(275, 2, "<col=800000>Cook's Assistant")
			
			when (stage(it.player())) {
				0 -> {
					it.player().interfaces().text(275, 4, "<col=000080>I can start this quest by speaking to the <col=800000>Cook<col=000080> in the")
					it.player().interfaces().text(275, 5, "<col=000080><col=800000>Kitchen<col=000080> on the ground floor of <col=800000>Lumbridge Castle<col=000080>.")
				}
				1 -> {
					it.player().interfaces().text(275, 4, "<col=000080>It's the <col=800000>Duke of Lumbridge's<col=000080> birthday and I have to help")
					it.player().interfaces().text(275, 5, "<col=000080>his <col=800000>Cook<col=000080> make him a <col=800000>birthday cake<col=000080>. To do this I need to")
					it.player().interfaces().text(275, 6, "<col=000080>bring him the following ingredients:")
					it.player().interfaces().text(275, 7, findFoundOrGiven(it, BUCKET_OF_MILK, "bucket of milk"))
					it.player().interfaces().text(275, 8, findFoundOrGiven(it, POT_OF_FLOUR, "pot of flour"))
					it.player().interfaces().text(275, 9, findFoundOrGiven(it, EGG, "egg"))
				}
				2 -> {
					it.player().interfaces().text(275, 4, "<str>It was the Duke of Lumbridge's birthday, but his cook had")
					it.player().interfaces().text(275, 5, "<str>forgotten to buy the ingredients he needed to make him a")
					it.player().interfaces().text(275, 6, "<str>cake. I brought the cook an egg, some flour and some milk")
					it.player().interfaces().text(275, 7, "<str>and the cook made a delicious looking cake with them.")
					it.player().interfaces().text(275, 9, "<str>As a reward he now lets me use his high quality range")
					it.player().interfaces().text(275, 10, "<str>which lets me burn things less whenever I wish to cook")
					it.player().interfaces().text(275, 11, "<str>there.")
					it.player().interfaces().text(275, 13, "<col=ff0000>QUEST COMPLETE!")
				}
				
			}
			
			QuestGuide.open(it.player())
		}
	}
	
	fun stage(player: Player): Int = player.varps().varp(STAGE_VARP)
	
	fun stage(player: Player, stage: Int) = player.varps().varp(STAGE_VARP, stage)
	
	fun hasGiven(it: Script, ingredient: Int): Boolean {
		if (it.player().attribOr(AttributeKey.GIVEN_BUCKET_OF_MILK, false) && ingredient == BUCKET_OF_MILK) {
			return true;
		} else if (it.player().attribOr(AttributeKey.GIVEN_POT_OF_FLOUR, false) && ingredient == POT_OF_FLOUR) {
			return true;
		} else if (it.player().attribOr(AttributeKey.GIVEN_EGG, false) && ingredient == EGG) {
			return true;
		}
		return false;
	}
	
	fun findFoundOrGiven(it: Script, ingredient: Int, ingredient_name: String): String {
		val aOrAn: String = if (ingredient == EGG) "an" else "a"
		if (hasGiven(it, ingredient)) {
			return ("<str>I have given the cook $aOrAn $ingredient_name.")
		} else if (it.player().inventory().has(ingredient)) {
			return "<col=000080>I have found $aOrAn <col=800000>$ingredient_name <col=000080>to give to the cook."
		}
		return "<col=000080>I need to find $aOrAn <col=800000>$ingredient_name."
	}
	
	fun completeQuest(player: Player) {
		// Update stages
		stage(player, 2)
		Quests.givePoints(player, 1)
		
		// Give coins, experience and the dialogue
		player.skills().__addXp(Skills.COOKING, 300.0)
		
		QuestGuide.clearCompletion(player)
		QuestGuide.setCompletionItem(player, 1891, 260)
		QuestGuide.setCompletionTitle(player, "You have completed Cook's Assistant!")
		QuestGuide.setCompletionLines(player, "1 Quest Point", "300 Cooking XP")
		QuestGuide.displayCompletion(player)
	}
}
