package nl.bartpelle.veteres.content.areas.dungeons.godwars

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 3/10/2016.
 */


object BossRoomDoors {
	
	val ECUMENICAL_KEY = 11942
	
	fun inGodWars(player: Player): Boolean {
		val t = player.tile()
		return t.x > 2820 && t.x < 2964 && t.z > 5245 && t.z < 5380
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Zamorak
		r.onObject(26505, s@ @Suspendable {
			if (it.interactionOption() == 1) {
				if (it.player().tile().z > 5332) {
					if (enterThroughDoor(it, Varbit.GWD_ZAMORAK_KC, "Zamorak"))
						it.player().teleport(2925, 5331, 2)
				} else if (it.player().tile().z == 5331) {
					it.player().teleport(2925, 5333, 2)
				}
			}
		})
		
		// Bandos
		r.onObject(26503, s@ @Suspendable {
			if (it.interactionOption() == 1) {
				if (it.player().tile().x < 2863) {
					if (enterThroughDoor(it, Varbit.GWD_BANDOS_KC, "Bandos"))
						it.player().teleport(2864, 5354, 2)
				} else if (it.player().tile().x == 2864) {
					it.player().teleport(2862, 5354, 2)
				}
			}
		})
		
		// Saradomin
		r.onObject(26504, s@ @Suspendable {
			if (it.interactionOption() == 1) {
				if (it.player().tile().x >= 2909) {
					if (enterThroughDoor(it, Varbit.GWD_SARADOMIN_KC, "Saradomin"))
						it.player().teleport(2907, 5265, 0)
				} else if (it.player().tile().x == 2907) {
					it.player().teleport(2909, 5265, 0)
				}
			}
		})
		
		// Armadyl
		r.onObject(26502, s@ @Suspendable {
			if (it.interactionOption() == 1) {
				if (it.player().tile().z <= 5294) {
					if (enterThroughDoor(it, Varbit.GWD_ARMADYL_KC, "Armadyl"))
						it.player().teleport(2839, 5296, 2)
				} else if (it.player().tile().z == 5296) {
					it.player().teleport(2839, 5294, 2)
				}
			}
		})
	}
	
	fun enterThroughDoor(it: Script, varbit: Int, name: String): Boolean {
		if (BonusContent.isActive(it.player(), BlessingGroup.UNTAMED)) {
			it.message("The Untamed Blessing provides you free passage through the dungeon door.")
			return true
		}
		val kc = it.player().varps().varbit(varbit)
		var required = kcRequirement(it.player())
		
		// If we have an ecumenial key, we pass for 'free' :)
		if (it.player().inventory().remove(Item(ECUMENICAL_KEY, 1), true).failed()) {
			if (kc < required) {
				it.player().message("You need at least $required follower kills before you may enter this chamber.")
				
				//Ensure the player is not on the PVP world.
				if (!it.player().world().realm().isPVP)
					it.player().message("<col=1e44b3><img=22> You can bypass the kill count with an Ecumenical key, found in ::store!")
				
				return false
			} else {
				it.message("The door devours the life-force of $required followers of $name that you have slain.")
				it.player().varps().varbit(varbit, kc - required)
				
				//Ensure the player is not on the PVP world.
				if (!it.player().world().realm().isPVP)
					it.player().message("<col=1e44b3><img=22> You can bypass the kill count with an Ecumenical key, found in ::store!")
			}
		} else {
			it.message("Your ecumenical key melts into the door.")
		}
		return true
	}
	
	// Based on how much you help us become the dominant empire on planet earth, we reduce the kc req :)
	fun kcRequirement(player: Player): Int {
		val rank = player.donationTier()
		
		if (player.world().realm().isPVP)
			return 0
		
		if (player.mode() == GameMode.REALISM)
			return 15
		
		if (rank == DonationTier.DONATOR)
			return 35
		else if (rank == DonationTier.SUPER_DONATOR)
			return 30
		else if (rank == DonationTier.EXTREME_DONATOR)
			return 25
		else if (rank == DonationTier.LEGENDARY_DONATOR)
			return 20
		else if (rank == DonationTier.MASTER_DONATOR)
			return 15
		else if (rank == DonationTier.GRAND_MASTER_DONATOR)
			return 10
		else if (rank == DonationTier.ULTIMATE_DONATOR)
			return 5
		
		return 40
	}
	
}
