package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import java.lang.ref.WeakReference

/**
 * Created by Bart on 8/12/2015.
 */
@Suspendable class PlayerFollowing {
	@Suspendable companion object {
		@JvmField val script: Function1<Script, Unit> = @Suspendable {
			val ref: WeakReference<Entity> = it.player().attrib(AttributeKey.TARGET);
			
			// Return if we have no ref
			if (ref != null && ref.get() != null) {
				var player: Player = ref.get() as Player
				
				while (ref.get() != null) {
					var last = player.pathQueue().lastStep() ?: break
					
					if (!it.player().frozen() && !player.stunned()) // Frozen check (method does this too, but to avoid the endless msgs)
						it.player().walkTo(last.x, last.z, PathQueue.StepType.REGULAR)
					
					it.player().face(player)
					it.delay(1)
					
					player = ref.get() as Player
				}
			}
		}
	}
}