package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Situations on 10/11/2016.
 */

object AbyssalSireChamber {
	
	val ABYSSAL_SIRE = 5886
	val RESPIRATORY_SYSTEM = 5914
	val TENTACLE_SPAWN_IDS = intArrayOf(5909, 5911, 5910, 5909, 5911, 5910)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcSpawn(ABYSSAL_SIRE) @Suspendable { initAbyssalSireChamber(it.npc()) }
		r.onTimer(TimerKey.ABYSSAL_SIRE_RESPAWN) { it.npc().hidden(false) }
		r.onTimer(TimerKey.ABYSSAL_SIRE_RESET_TIMER) {
			AbyssalSireChamber.cleanAndResetChamber(it.npc())
			
			it.npc().timers().cancel(TimerKey.ABYSSAL_SIRE_RESET_TIMER)
		}
	}
	
	private fun initAbyssalSireChamber(abyssalSire: Npc) {
		val abyssalSireX = abyssalSire.tile().x
		val abyssalSireZ = abyssalSire.tile().z
		
		val tentacleSpawnTiles = arrayListOf(
				//Left side of Abyssal Sire
				Tile(abyssalSireX - 9, abyssalSireZ - 29),
				Tile(abyssalSireX - 7, abyssalSireZ - 20),
				Tile(abyssalSireX - 10, abyssalSireZ - 11),
				
				//Right side of Abyssal Sire
				Tile(abyssalSireX + 8, abyssalSireZ - 29),
				Tile(abyssalSireX + 5, abyssalSireZ - 20),
				Tile(abyssalSireX + 7, abyssalSireZ - 11))
		
		val respiratorySystemSpawnTiles = arrayListOf(
				//Left side of Abyssal Sire
				Tile(abyssalSireX - 10, abyssalSireZ - 21),
				Tile(abyssalSireX - 13, abyssalSireZ - 11),
				
				//Right side of Abyssal Sire
				Tile(abyssalSireX + 15, abyssalSireZ - 12),
				Tile(abyssalSireX + 18, abyssalSireZ - 22)
		)
		
		val tentacleSpawnMap = ArrayList<Npc>()
		val respiratorySystemSpawnMap = ArrayList<Npc>()
		
		//Spawn the Abyssal Sire's tentacles
		for (i in 1..tentacleSpawnTiles.size) {
			val tentacle = Npc(TENTACLE_SPAWN_IDS[i - 1], abyssalSire.world(), tentacleSpawnTiles[i - 1])
			
			//Clip the Tentacle's that are protecting the Respiratory system's
			when (i) {
				1, 4 -> for (i in 0..2) tentacle.world().spawnObj(MapObj(Tile(tentacle.tile().x + 4, tentacle.tile().z + 9 - i), 4451, 10, 3), true)
				3, 6 -> for (i in 0..2) tentacle.world().spawnObj(MapObj(Tile(tentacle.tile().x + 4, tentacle.tile().z - i), 4451, 10, 3), true)
			}
			
			tentacle.respawns(false)
			tentacleSpawnMap.add(tentacle)
			abyssalSire.world().registerNpc(tentacle)
		}
		
		//Spawn the Abyssal Sire's Respiratory system's
		for (spawnTile in respiratorySystemSpawnTiles) {
			val respiratorySystem = Npc(RESPIRATORY_SYSTEM, abyssalSire.world(), spawnTile)
			
			respiratorySystem.respawns(false)
			respiratorySystemSpawnMap.add(respiratorySystem)
			abyssalSire.world().registerNpc(respiratorySystem)
		}
		
		abyssalSire.respawns(false)
		AbyssalSireTentacles.putCombatState(tentacleSpawnMap, AbyssalSireState.STASIS)
		
		AbyssalSireRespiratorySystem.putCombatState(respiratorySystemSpawnMap, AbyssalSireState.STASIS)
		AbyssalSireRespiratorySystem.putOwningAbyssalSire(respiratorySystemSpawnMap, abyssalSire)
		
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.STASIS)
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
		
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_TENTACLES, tentacleSpawnMap)
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_RESPIRATORY_SYSTEMS, respiratorySystemSpawnMap)
	}
	
	fun cleanAndResetChamber(abyssalSire: Npc) {
		val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
		val abyssalSireRespiratorySystem = AbyssalSireRespiratorySystem.getRespiratorySystem(abyssalSire)
		val abyssalSireSpawns = abyssalSire.attribOr<ArrayList<Npc>>(AttributeKey.ABYSSAL_SIRE_SPAWNS, ArrayList<Npc>())
		
		//Unregister Abyssal Sire's Tentacles & clean attributes
		abyssalSireTentacles.forEach { t -> abyssalSire.world().unregisterNpc(t) }
		abyssalSire.clearattrib(AttributeKey.ABYSSAL_SIRE_TENTACLES)
		
		//Unregister Abyssal Sire's Respiratory & clear attributes
		abyssalSireRespiratorySystem.forEach { r -> abyssalSire.world().unregisterNpc(r) }
		abyssalSire.clearattrib(AttributeKey.ABYSSAL_SIRE_RESPIRATORY_SYSTEMS)
		
		//Unregister Abyssal Sire's spawns & clear attributes
		abyssalSireSpawns.forEach { s -> abyssalSire.world().unregisterNpc(s) }
		abyssalSire.clearattrib(AttributeKey.ABYSSAL_SIRE_SPAWNS)
		
		//Respawn the Abyssal Sire respawn, make hidden and start visibility timer
		val abyssalSireRespawn = Npc(5886, abyssalSire.world(), abyssalSire.spawnTile())
		
		abyssalSire.world().registerNpc(abyssalSireRespawn)
		abyssalSire.world().unregisterNpc(abyssalSire)
		abyssalSireRespawn.hidden(true)
		abyssalSireRespawn.timers().addOrSet(TimerKey.ABYSSAL_SIRE_RESPAWN, 10)
	}
}
