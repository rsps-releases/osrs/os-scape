package nl.bartpelle.veteres.content.minigames.wintertodt

import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Bart on 9/10/2016.
 */
class WintertodtGameState {
	
	var health = 0
	val participants = mutableSetOf<Player>()
	var active = false
	var ticksUntilPyromancers = 5
	
	val brazierNE = WintertodtBrazier(FaceDirection.NORTH_EAST, Tile(1638, 4015), Tile(1641, 4018), Tile(2, 2))
	val brazierNW = WintertodtBrazier(FaceDirection.NORTH_WEST, Tile(1620, 4015), Tile(1619, 4018), Tile(0, 2))
	val brazierSW = WintertodtBrazier(FaceDirection.SOUTH_WEST, Tile(1620, 3997), Tile(1619, 3996), Tile(0, 0))
	val brazierSE = WintertodtBrazier(FaceDirection.SOUTH_EAST, Tile(1638, 3997), Tile(1641, 3996), Tile(2, 0))
	val allBraziers = arrayOf(brazierNE, brazierNW, brazierSE, brazierSW)
	
	val brazierTileLookup = mapOf(
			brazierNE.tile to brazierNE,
			brazierNW.tile to brazierNW,
			brazierSW.tile to brazierSW,
			brazierSE.tile to brazierSE
	)
	
	fun addParticipant(player: Player) {
		participants.add(player)
	}
	
	fun removeParticipant(player: Player) {
		participants.remove(player)
	}
	
	fun reinitialize(world: World) {
		allBraziers.forEach { it.changeState(BrazierState.STALE, world) }
		active = true
		health = WintertodtGame.MAX_HEALTH
		ticksUntilPyromancers = 0
	}
	
}