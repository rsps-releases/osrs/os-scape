package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj

/**
 * Created by Situations on 11/17/2015.
 */

object WarriorDoubleDoor {
	
	val WARRIORS_GUILD_TOKENS = 8851
	
	val BRONZE_DEFENDER = 8844
	val IRON_DEFENDER = 8845
	val STEEL_DEFENDER = 8846
	val BLACK_DEFENDER = 8847
	val MITHRIL_DEFENDER = 8848
	val ADDY_DEFENDER = 8849
	val RUNE_DEFENDER = 8850
	val DRAGON_DEFENDER = 12954
	
	
	@JvmStatic fun door(it: Script) {
		//Main floor
		if (it.player().tile().level == 0) {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			it.player().lock()
			
			if (!it.player().tile().equals(obj.tile().transform(0, -1, 0)) && player.tile().x >= 3546) {
				it.player().walkTo(obj.tile(), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(obj.tile())
			} else if (!it.player().tile().equals(obj.tile().transform(0, 1, 0)) && player.tile().z <= 3545) {
				it.player().walkTo(obj.tile().transform(0, -1, 0), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(obj.tile().transform(0, -1, 0))
			}
			
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
				val spawned = MapObj(Tile(obj.tile().x, 3545), obj.id() + 2, obj.type(), if (obj.id() == 24309) 2 else 0)
				world.removeObj(old, false)
				world.spawnObj(spawned)
				it.delay(2)
				world.removeObjSpawn(spawned)
				world.spawnObj(old)
			}
			
			if (player.tile().z >= 3546) {
				it.player().pathQueue().interpolate(obj.tile().x, 3545, PathQueue.StepType.FORCED_WALK)
			} else if (player.tile().z <= 3545) {
				it.player().pathQueue().interpolate(obj.tile().x, 3546, PathQueue.StepType.FORCED_WALK)
			}
			
			it.player().unlock()
		} else if (it.player().tile().level == 2) {
			
			//Second floor
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			it.player().lock()
			
			if (!it.player().tile().equals(obj.tile().transform(-1, 0, 0)) && player.tile().x <= 2846) {
				it.player().walkTo(obj.tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(obj.tile().transform(-1, 0, 0))
			} else if (!it.player().tile().equals(obj.tile().transform(0, 0, 0)) && player.tile().x >= 2847) {
				it.player().walkTo(obj.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(obj.tile().transform(0, 0, 0))
			}
			
			//Does our player have enough tokens or are they wearing the attack cape?
			if ((!it.player().inventory().contains(Item(WARRIORS_GUILD_TOKENS, 100))
					&& !CapeOfCompletion.ATTACK.operating(it.player())) && it.player().tile().x == 2846) {
				it.player().unlock()
				it.itemBox("You don't have enough Warrior Guild Tokens to enter the cyclopes enclosure yet, collect at least 100 then come back.", WARRIORS_GUILD_TOKENS, 100)
				return
			}
			
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
				val spawned = MapObj(Tile(obj.tile().x - 1, obj.tile().z, obj.tile().level), obj.id() + 2, obj.type(), if (obj.id() == 24309) 1 else 3)
				world.removeObj(old, false)
				world.spawnObj(spawned)
				it.delay(2)
				world.removeObjSpawn(spawned)
				world.spawnObj(old)
			}
			
			if (player.tile().x <= 2846) {
				it.player().pathQueue().interpolate(obj.tile().x, player.tile().z, PathQueue.StepType.FORCED_WALK)
			} else if (player.tile().x >= 2847) {
				it.player().pathQueue().interpolate(obj.tile().x - 1, player.tile().z, PathQueue.StepType.FORCED_WALK)
			}
			
			//On the economy world, set the defender to what their defence can wield.
			if (it.player().tile().x == 2846) {
				if (it.player().world().realm().isOSRune) {
					val defence = it.player().skills().xpLevel(Skills.DEFENCE)
					if (defence <= 4)
						set_item(it, IRON_DEFENDER)
					else if (defence <= 9)
						set_item(it, STEEL_DEFENDER)
					else if (defence <= 19)
						set_item(it, BLACK_DEFENDER)
					else if (defence <= 29)
						set_item(it, MITHRIL_DEFENDER)
					else if (defence <= 39)
						set_item(it, ADDY_DEFENDER)
					else if (defence <= 59)
						set_item(it, RUNE_DEFENDER)
					else
						set_item(it, DRAGON_DEFENDER)
				} else {
					if (it.player().inventory().contains(Item(DRAGON_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, DRAGON_DEFENDER))
						set_item(it, DRAGON_DEFENDER)
					else if (it.player().inventory().contains(Item(RUNE_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, RUNE_DEFENDER))
						set_item(it, DRAGON_DEFENDER)
					else if (it.player().inventory().contains(Item(ADDY_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, ADDY_DEFENDER))
						set_item(it, RUNE_DEFENDER)
					else if (it.player().inventory().contains(Item(MITHRIL_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, MITHRIL_DEFENDER))
						set_item(it, ADDY_DEFENDER)
					else if (it.player().inventory().contains(Item(BLACK_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, BLACK_DEFENDER))
						set_item(it, MITHRIL_DEFENDER)
					else if (it.player().inventory().contains(Item(STEEL_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, STEEL_DEFENDER))
						set_item(it, BLACK_DEFENDER)
					else if (it.player().inventory().contains(Item(IRON_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, IRON_DEFENDER))
						set_item(it, STEEL_DEFENDER)
					else if (it.player().inventory().contains(Item(BRONZE_DEFENDER)) || it.player().equipment().hasAt(EquipSlot.SHIELD, BRONZE_DEFENDER))
						set_item(it, IRON_DEFENDER)
					else
						set_item(it, BRONZE_DEFENDER)
				}
				val defid = it.player().attrib<Int>(AttributeKey.WARRIORS_GUILD_CYCLOPS_ROOM_DEFENDER)
				val def = it.player().world().definitions().get(ItemDefinition::class.java, defid)
				it.message("<col=804080>Cyclops' are currently dropping ${def.name.toLowerCase()}s. ${
				if (it.player().world().realm().isOSRune) "This is based on your Defence level." else ""}")
			}
		}
		it.player().unlock()
	}
	
	fun set_item(it: Script, item: Int) {
		it.player().putattrib(AttributeKey.WARRIORS_GUILD_CYCLOPS_ROOM_DEFENDER, item)
	}
}

