package nl.bartpelle.veteres.content.skills.fishing

/**
 * Created by Bart with Sensations on that enum on 12/1/2015.
 */

enum class Fish(val lvl: Int, val item: Int, val xp: Double, val fishName: String, val prefix: String = "a", val petChance: Int) {
	SHRIMP(1, 317, 10.0, "shrimps", "some", 43516),
	KARAMBWANJI(5, 3150, 10.0, "karambwanji", "some", 52800),
	SARDINE(5, 327, 20.0, "sardine", "a", 52800),
	HERRING(10, 345, 30.0, "herring", "a", 52800),
	ANCHOVIES(15, 321, 40.0, "anchovies", "some", 43516),
	MACKEREL(16, 345, 20.0, "mackerel", "a", 38260),
	RAINBOW(38, 10138, 80.0,"rainbow", "a", 26000),
	TROUT(20, 335, 50.0, "trout", "a", 46180),
	COD(23, 341, 45.0, "cod", "a", 38260),
	PIKE(25, 349, 60.0, "pike", "a", 30579),
	SLIMY_EEL(28, 3379, 65.0, "slimy eel", "a", 30579),
	SALMON(30, 331, 70.0, "salmon", "a", 46180),
	TUNA(35, 359, 80.0, "tuna", "a", 12888),
	CAVE_EEL(38, 359, 80.0, "cave eel", "a", 12000),
	LOBSTER(40, 377, 90.0, "lobster", "a", 11612),
	BASS(46, 363, 100.0, "bass", "a", 38260),
	SWORDFISH(50, 371, 100.0, "swordfish", "a", 12888),
	LAVA_EEL(53, 2148, 30.0, "lava eel", "a", 12000),
	MONKFISH(62, 7944, 120.0, "monkfish", "a", 13858),
	KARAMBWAN(65, 3142, 105.0, "karambwan", "a", 17087),
	SHARK(76, 383, 110.0, "shark", "a", 8224),
	SEA_TURTLE(79, 395, 138.0, "sea turtle", "a", 8000), // Trawler
	INFERNAL_EEL(80, 21293, 95.0, "infernal eel", "a", 8224),
	MANTA_RAY(81, 389, 146.0, "manta ray", "a", 8000), // Trawler
	ANGLERFISH(82, 13439, 120.0, "anglerfish", "an", 7500),
	DARK_CRAB(85, 11934, 130.0, "dark crab", "a", 14943),
	SACRED_EEL(87, 13339, 105.0, "sacred eel", "an", 9900)
	;
	
	companion object {
		
		fun getFishItemIds(): IntArray {
			val fishIds = mutableListOf<Int>()
			for (fish in Fish.values()) {
				fishIds.add(fish.item)
			}
			return fishIds.toIntArray()
		}
	}
}