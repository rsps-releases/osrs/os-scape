package nl.bartpelle.veteres.content.areas.clanwars

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.Poison
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.content.mechanics.Venom
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 2/5/2016. #jenkins ci
 */
object FFAClanWars {
	
	// Boundaries of the place where people can fight.
	val FFA_AREA = Area(3264, 4760, 3391, 4863)
	
	// Boundaries of the entire place
	val FFA_MAP = Area(3264, 4736, 3391, 4863)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Portal from lobby to FFA
		r.onObject(26645) {
			if (it.player().world().realm().isDeadman) {
				it.message("The Safe Clanwars portal cannot be access on Deadman worlds.")
			} else {
				it.player().teleport(3327, 4751)
			}
		}

		r.onObject(28925) @Suspendable {
			it.player().teleport(3327, 4751)
		}


		
		// Portal from FFA to lobby
		r.onObject(26646) {
			if (it.player().world().realm().isPVP) {
				it.player().teleport(3099, 3505)
			} else {
				it.player().teleport(3352, 3163)
			}
			
			// Reset the majority of things like RS does
			val player = it.player()
			cureonexit(player)
		}
		
		// Register the enter/exit triggers for the FFA zone
		// Interface 90 is the small skull in the top-right.
		// Interface 199 is the "Step north to fight" banner.
		for (i in intArrayOf(13130, 13131, 13386, 13387)) {
			r.onRegionEnter(i) {
				it.player().interfaces().sendWidgetOn(199, Interfaces.InterSwitches.D)
			}
			
			// When leaving the area, remove the interfaces.
			r.onRegionExit(i) {
				it.player().interfaces().closeById(199)
				if (!FFA_MAP.contains(it.player())) { // You've left the arena.
					cureonexit(it.player())
				}
			}
		}
		
	}
	
	private fun cureonexit(player: Player) {
		
		player.skills().resetStats()
		Poison.cure(player)
		Venom.cure(2, player)
		player.timers().cancel(TimerKey.FROZEN)
		player.timers().cancel(TimerKey.STUNNED)
		player.timers().cancel(TimerKey.TELEBLOCK)
		player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
		player.clearattrib(AttributeKey.LAST_DAMAGER)
		Skulling.unskull(player)
		player.clearDamagers()
		player.clearDamageTimes() // Clear damage time tracker
		player.putattrib(AttributeKey.RUN_ENERGY, 100.0) //Set the players run energy to 100
		player.hp(100, 0)
		player.looks().update()
		if (player.timers().has(TimerKey.RECHARGE_SPECIAL_ATTACK)) {
			player.message("Special attack energy can only be restored every couple of minutes.")
		} else {
			player.varps().varp(Varp.SPECIAL_ENERGY, 1000) //Set energy to 100%
			player.timers().register(TimerKey.RECHARGE_SPECIAL_ATTACK, 150) //Set the value of the timer. Currently 1:30m
		}
	}
	
	@JvmStatic fun inFFAWilderness(entity: Entity): Boolean {
		return FFA_AREA.contains(entity)
	}
	
	@JvmStatic fun inFFAMap(entity: Entity): Boolean {
		return FFA_MAP.contains(entity)
	}
	
}