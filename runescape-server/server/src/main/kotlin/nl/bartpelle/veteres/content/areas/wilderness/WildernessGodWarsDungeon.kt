package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Nick on 8/28/2016.
 */

object WildernessGodWarsDungeon {
	
	val TUNNEL_ENTRANCE = 26766
	val TUNNEL_EXIT = 26767
	
	val JUTTING_WALL = 26768
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(TUNNEL_ENTRANCE) @Suspendable {
			val player = it.player()
			val obj: MapObj = player.attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (obj.tile().equals(3016, 3738)) {
				if (!it.player().tile().equals(obj.tile().transform(1, 2, 0))) {
					it.player().walkTo(obj.tile().transform(1, 2, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(obj.tile().transform(1, 2, 0))
				}
				
				player.lockDelayDamage()
				it.delay(1)
				teleport_player(it, 3065, 10159, 3)
			}
		}
		
		r.onObject(TUNNEL_EXIT) @Suspendable {
			val player = it.player()
			val obj: MapObj = player.attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (obj.tile().equals(3065, 10160)) {
				if (!it.player().tile().equals(obj.tile().transform(0, -1, 0))) {
					it.player().walkTo(obj.tile().transform(0, -1, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(obj.tile().transform(0, -1, 0))
				}
				
				player.lockDelayDamage()
				it.delay(1)
				teleport_player(it, 3017, 3740, 0)
			}
		}
		
		// Doesn't work from south side TODO
		/*r.onObject(JUTTING_WALL) @Suspendable {
			it.player().lockDelayDamage()
			it.player().animate(753)
			it.player().faceTile(Tile(0, it.player().tile().z))
			it.player().pathQueue().clear()
			it.player().pathQueue().interpolate(3066, 10147, PathQueue.StepType.FORCED_WALK)
			it.player().looks().render(756, 756, 756, 756, 756, 756, -1)
			it.delay(3)
			it.waitForTile(Tile(3066, 10147 ))
			it.player().looks().resetRender()
			it.addXp(Skills.AGILITY, 6.0)
			it.player().unlock()
		}*/
	}
	
	@Suspendable fun teleport_player(it: Script, x: Int, z: Int, h: Int) {
		val player = it.player()
		it.animate(2796)
		it.delay(2)
		it.animate(-1)
		player.teleport(x, z, h)
		player.unlock()
	}
	
	@Suspendable fun pass_jutting_wall(it: Script, x: Int, z: Int) {
		
	}
}
