package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.skills.mining.Mining
import nl.bartpelle.veteres.content.skills.woodcutting.Woodcutting
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 12/20/2015.
 */

object WildernessMage {
	
	val MAGE_OF_ZAMORAK = 2581
	
	val OUTER_RING_TELEPORTS = arrayOf(Tile(3019, 4844), Tile(3041, 4855), Tile(3053, 4851), Tile(3060, 4842), Tile(3063, 4832),
			Tile(3061, 4823), Tile(3055, 4814), Tile(3047, 4811), Tile(3038, 4809), Tile(3027, 4810), Tile(3021, 4814),
			Tile(3018, 4819), Tile(3016, 4828), Tile(3016, 4838), Tile(3021, 4847))
	
	val INNER_RING_TELEPORTS = arrayListOf(Tile(3042, 4819), Tile(3029, 4823), Tile(3028, 4824), Tile(3024, 4834), Tile(3028, 4841),
			Tile(3031, 4843), Tile(3038, 4846), Tile(3048, 4842), Tile(3051, 4839), Tile(3054, 4830), Tile(3051, 4823), Tile(3048, 4821))
	
	enum class ObjectType() { BLOCKAGE, PASSAGE, GAP, EYES, BOIL, TENDRILS, ROCKS }
	
	val OBJECT_ROTATIONS = arrayListOf(
			arrayOf(ObjectType.BLOCKAGE, ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP,
					ObjectType.PASSAGE, ObjectType.GAP, ObjectType.EYES, ObjectType.BOIL, ObjectType.TENDRILS, ObjectType.ROCKS),
			arrayOf(ObjectType.ROCKS, ObjectType.BLOCKAGE, ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES,
					ObjectType.GAP, ObjectType.PASSAGE, ObjectType.GAP, ObjectType.EYES, ObjectType.BOIL, ObjectType.TENDRILS),
			arrayOf(ObjectType.TENDRILS, ObjectType.ROCKS, ObjectType.BLOCKAGE, ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL,
					ObjectType.EYES, ObjectType.GAP, ObjectType.PASSAGE, ObjectType.GAP, ObjectType.EYES, ObjectType.BOIL),
			arrayOf(ObjectType.BOIL, ObjectType.TENDRILS, ObjectType.ROCKS, ObjectType.BLOCKAGE, ObjectType.ROCKS, ObjectType.TENDRILS,
					ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP, ObjectType.PASSAGE, ObjectType.GAP, ObjectType.EYES),
			arrayOf(ObjectType.EYES, ObjectType.BOIL, ObjectType.TENDRILS, ObjectType.ROCKS, ObjectType.BLOCKAGE, ObjectType.ROCKS,
					ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP, ObjectType.PASSAGE, ObjectType.GAP),
			arrayOf(ObjectType.GAP, ObjectType.EYES, ObjectType.BOIL, ObjectType.TENDRILS, ObjectType.ROCKS, ObjectType.BLOCKAGE,
					ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP, ObjectType.PASSAGE),
			arrayOf(ObjectType.PASSAGE, ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP,
					ObjectType.BLOCKAGE, ObjectType.GAP, ObjectType.EYES, ObjectType.BOIL, ObjectType.TENDRILS, ObjectType.ROCKS),
			arrayOf(ObjectType.GAP, ObjectType.PASSAGE, ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES,
					ObjectType.GAP, ObjectType.BLOCKAGE, ObjectType.GAP, ObjectType.EYES, ObjectType.BOIL, ObjectType.TENDRILS),
			arrayOf(ObjectType.EYES, ObjectType.ROCKS, ObjectType.PASSAGE, ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL,
					ObjectType.EYES, ObjectType.GAP, ObjectType.BLOCKAGE, ObjectType.GAP, ObjectType.EYES, ObjectType.BOIL),
			arrayOf(ObjectType.BOIL, ObjectType.EYES, ObjectType.ROCKS, ObjectType.PASSAGE, ObjectType.ROCKS, ObjectType.TENDRILS,
					ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP, ObjectType.BLOCKAGE, ObjectType.GAP, ObjectType.EYES),
			arrayOf(ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.TENDRILS, ObjectType.ROCKS, ObjectType.PASSAGE, ObjectType.ROCKS,
					ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP, ObjectType.BLOCKAGE, ObjectType.GAP),
			arrayOf(ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.TENDRILS, ObjectType.ROCKS, ObjectType.PASSAGE,
					ObjectType.ROCKS, ObjectType.TENDRILS, ObjectType.BOIL, ObjectType.EYES, ObjectType.GAP, ObjectType.BLOCKAGE))
	
	val OBJECT_LOCATION = arrayListOf(Tile(3042, 4812), Tile(3027, 4814), Tile(3019, 4822), Tile(3019, 4834), Tile(3021, 4843),
			Tile(3029, 4850), Tile(3039, 4853), Tile(3050, 4850), Tile(3059, 4840), Tile(3061, 4831), Tile(3057, 4822), Tile(3050, 4814))
	
	enum class AltarPortals(val id: Int, val target: Tile) {
		AIR(25378, Tile(2841, 4830)),
		MIND(25379, Tile(2792, 4827)),
		WATER(25376, Tile(2726, 4832)),
		EARTH(24972, Tile(2655, 4830)),
		FIRE(24971, Tile(2574, 4849)),
		BODY(24973, Tile(2521, 4834)),
		COSMIC(24974, Tile(2162, 4833)),
		LAW(25034, Tile(2464, 4818)),
		NATURE(24975, Tile(2400, 4835)),
		CHAOS(24976, Tile(2281, 4837)),
		DEATH(25035, Tile(2208, 4830)),
		ASTRAL(25377, Tile(2156, 3863))
	}
	
	@JvmStatic var TELEBLOCK_STOPS_USE = true
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MAGE_OF_ZAMORAK) @Suspendable {
			it.chatNpc("This is no place to talk!<br>Meet me at the Varrock Chaos Temple!", MAGE_OF_ZAMORAK, 557)
		}
		
		r.onNpcOption2(MAGE_OF_ZAMORAK) @Suspendable {
			if (!it.player().world().realm().isPVP)
				it.player().world().shop(46).display(it.player())
		}
		
		r.onNpcOption3(2581, s@ @Suspendable {
			val player = it.player()
			
			if (TELEBLOCK_STOPS_USE && it.player().timers().has(TimerKey.TELEBLOCK)) {
				it.message("You can't go in here when teleblocked.")
				return@s
			}
			
			DeadmanMechanics.attemptTeleport(it)
			it.player().lockNoDamage()
			it.targetNpc()!!.lock()
			it.targetNpc()!!.sync().faceEntity(it.player())
			it.targetNpc()!!.sync().shout("Veniens! Sallakar! Rinnesset!")
			it.targetNpc()!!.sync().animation(1818, 1)
			it.targetNpc()!!.sync().graphic(343, 100, 1)
			it.delay(2)
			it.targetNpc()!!.unlock()
			Skulling.assignSkullState(player)
			player.skills().setLevel(Skills.PRAYER, 0)
			generateRandomAbyss(player)
			player.teleport(player.world().random(OUTER_RING_TELEPORTS))
			it.animate(-1)
			player.unlock()
		})
		
		AltarPortals.values().forEach { portal -> r.onObject(portal.id) { it.player().teleport(portal.target) } }
		r.onObject(25380) { it.message("How can the blood portal be real if the altar isn't real.") }
		
		for (OBJECTS in intArrayOf(26187, 26188, 26189, 26190, 26191, 26192, 26208, 26250, 26251, 26252, 26253, 26574, 26178)) {
			r.onObject(OBJECTS) @Suspendable { determineObject(it, it.player()) }
		}
	}
	
	@Suspendable private fun determineObject(it: Script, player: Player) {
		val mapLayout = player.varps().varbit(Varbit.ABYSS_MAP)
		
		(0..OBJECT_LOCATION.size - 1).filter { player.tile().inSqRadius(OBJECT_LOCATION[it], 4) }.map {
			OBJECT_ROTATIONS[mapLayout][it]
		}.forEach { interactedObject ->
			when (interactedObject) {
				ObjectType.GAP -> squeezeThroughGap(it, player)
				ObjectType.ROCKS -> mineRocks(it, player)
				ObjectType.EYES -> distractEyes(it, player)
				ObjectType.TENDRILS -> cutTendrils(it, player)
				ObjectType.BOIL -> burnBoil(it, player)
				ObjectType.PASSAGE -> goThroughPassage(it, player)
			}
		}
	}
	
	@Suspendable private fun getDestinationTile(player: Player): Tile {
		(0..OBJECT_LOCATION.size - 1).forEach { tile ->
			if (player.tile().inSqRadius(OBJECT_LOCATION[tile], 4)) {
				return INNER_RING_TELEPORTS[tile]
			}
		}
		return Tile(3048, 4842)
	}
	
	@Suspendable private fun squeezeThroughGap(it: Script, player: Player) {
		player.lock()
		
		player.message("You attempt to squeeze through the narrow gap...")
		it.delay(2)
		player.animate(1331)
		it.delay(1)
		
		if (player.world().rollDie(100, chanceToSucceed(player, Skills.AGILITY))) {
			player.message("...and you manage to crawl through.")
			it.delay(2)
			player.skills().__addXp(Skills.AGILITY, 25.0)
			player.teleport(getDestinationTile(player))
			generateRandomAbyss(player)
			player.animate(-1)
		} else {
			player.animate(1332)
			player.message("...but you are not agile enough to get through the gap.")
		}
		
		it.delay(1)
		player.unlock()
	}
	
	@Suspendable private fun mineRocks(it: Script, player: Player) {
		val pickaxe = Mining.findPickaxe(player)
		
		if (pickaxe == null) {
			player.sound(2277, 0)
			player.message("You need a pickaxe to mine this rock. You do not have a pickaxe which " +
					"you have the Mining level to use.")
			return
		}
		
		player.lock()
		
		player.message("You attempt to mine your way through...")
		player.animate(pickaxe.anim)
		it.delay(5)
		
		if (player.world().rollDie(100, chanceToSucceed(player, Skills.MINING))) {
			player.varps().varbit(Varbit.ABYSS_MAP, 12)
			it.delay(3)
			player.varps().varbit(Varbit.ABYSS_MAP, 13)
			it.delay(2)
			player.message("...and manage to break through the rock.")
			it.delay(1)
			player.skills().__addXp(Skills.MINING, 25.0)
			player.teleport(getDestinationTile(player))
			generateRandomAbyss(player)
			player.animate(-1)
		} else {
			player.animate(-1)
			player.message("...but fail to break-up the rock.")
		}
		
		it.delay(1)
		player.unlock()
	}
	
	@Suspendable private fun distractEyes(it: Script, player: Player) {
		player.lock()
		
		player.message("You use your thieving skills to misdirect the eyes...")
		player.animate(1057)
		it.delay(4)
		
		
		if (player.world().rollDie(100, chanceToSucceed(player, Skills.THIEVING))) {
			player.varps().varbit(Varbit.ABYSS_MAP, 18)
			it.delay(3)
			player.animate(864)
			player.varps().varbit(Varbit.ABYSS_MAP, 19)
			it.delay(2)
			player.message("...and sneak past while they're not looking.")
			it.delay(1)
			player.skills().__addXp(Skills.THIEVING, 25.0)
			player.teleport(getDestinationTile(player))
			generateRandomAbyss(player)
			player.animate(-1)
		} else {
			player.animate(-1)
			player.message("...but fail to distract them.")
		}
		
		it.delay(1)
		player.unlock()
	}
	
	@Suspendable private fun cutTendrils(it: Script, player: Player) {
		val axe: Woodcutting.Woodcutting.Hatchet? = Woodcutting.Woodcutting.findAxe(player)
		
		//Does our player have an axe?
		if (axe == null) {
			player.sound(2277, 0)
			player.message("You do not have an axe which you have the Woodcutting level to use.")
			return
		}
		player.lock()
		
		player.message("You attempt to chop your way through...")
		it.delay(3)
		
		if (player.world().rollDie(100, chanceToSucceed(player, Skills.MINING))) {
			player.animate(axe.anim)
			it.delay(2)
			player.varps().varbit(Varbit.ABYSS_MAP, 14)
			it.delay(3)
			player.varps().varbit(Varbit.ABYSS_MAP, 15)
			it.delay(2)
			player.message("...and manage to cut your way through the tendrils.")
			it.delay(1)
			player.skills().__addXp(Skills.WOODCUTTING, 25.0)
			player.teleport(getDestinationTile(player))
			generateRandomAbyss(player)
			player.animate(-1)
		} else {
			player.animate(-1)
			player.message("...but fail to cut down teh tendrils.")
		}
		
		it.delay(1)
		player.unlock()
	}
	
	
	@Suspendable private fun burnBoil(it: Script, player: Player) {
		player.lock()
		player.message("You attempt to set the blockade on fire...")
		it.delay(4)
		
		if (player.inventory().has(590)) {
			player.animate(733)
			it.delay(4)
			
			if (player.world().rollDie(100, chanceToSucceed(player, Skills.FIREMAKING))) {
				player.varps().varbit(Varbit.ABYSS_MAP, 16)
				it.delay(4)
				player.world().tileGraphic(157, it.interactionObject().tile(), 0, 0)
				player.varps().varbit(Varbit.ABYSS_MAP, 17)
				player.message("...and manage to burn it down and get past.")
				player.animate(-1)
				it.delay(1)
				player.skills().__addXp(Skills.FIREMAKING, 25.0)
				player.teleport(getDestinationTile(player))
				generateRandomAbyss(player)
			} else {
				player.animate(-1)
				player.message("...but fail to break-up the rock.")
			}
		} else {
			player.message("...but you don't have a tinderbox to burn it!")
		}
		
		
		it.delay(1)
		player.unlock()
	}
	
	@Suspendable private fun goThroughPassage(it: Script, player: Player) {
		player.lock()
		it.delay(1)
		player.teleport(getDestinationTile(player))
		generateRandomAbyss(player)
		player.unlock()
	}
	
	@Suspendable private fun chanceToSucceed(player: Player, skill: Int): Int {
		return player.skills()[skill] + 1
	}
	
	@Suspendable private fun generateRandomAbyss(player: Player) {
		player.varps().varbit(Varbit.ABYSS_MAP, player.world().random(11))
	}
	
}