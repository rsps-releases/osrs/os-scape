package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/22/2016.
 */

object RemoveObjects {
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit {
			val world = it.ctx<World>()
			
			world.spawnObj(MapObj(Tile(3375, 3172), 13648, 10, 2), true) // Lectern for teletab crafting
		}
	}
	
	/**
	 * Remove the clipping masks after Edgeville PVP instances are created.. so the instance copies
	 * the unchanged clipping tiles and objects.
	 */
	@JvmStatic fun adjustEdgevilleRealms_PVP_OSR(world: World) {

	}
}
