package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.skills.magic.JewelleryEnchant.EnchantData.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/23/2015.
 */
object JewelleryEnchant {
	
	enum class EnchantData(val item: Int, val product: Int, val anim: Int, val gfx: Int) {
		SAPPHIRE_RING(1637, 2550, 719, 238),
		SAPPHIRE_AMULET(1694, 1727, 719, 114),
		SAPPHIRE_NECKLACE(1656, 3853, 719, 114),
		SAPPHIRE_BRACELET(11071, 11074, 719, -1),
		
		EMERALD_RING(1639, 2552, 719, 238),
		EMERALD_AMULET(1696, 1729, 719, 114),
		EMERALD_NECKLACE(1658, 5521, 719, 114),
		EMERALD_BRACELET(11076, 11079, 719, -1),
		
		RUBY_RING(1641, 2568, 712, 238),
		RUBY_AMULET(1698, 1725, 720, 115),
		RUBY_NECKLACE(1660, 11194, 720, 115),
		RUBY_BRACELET(11085, 11088, 720, -1),
		
		DIAMOND_RING(1643, 2570, 712, 238),
		DIAMOND_AMULET(1700, 1731, 720, 115),
		DIAMOND_NECKLACE(1662, 11090, 720, 115),
		DIAMOND_BRACELET(11092, 11095, 720, -1),
		
		DRAGONSTONE_RING(1645, 2572, 712, 238),
		DRAGONSTONE_AMULET(1702, 1712, 721, 116),
		DRAGONSTONE_NECKLACE(1664, 11105, 721, 116),
		DRAGONSTONE_BRACELET(11115, 11118, 721, -1),
		
		ONYX_RING(6575, 6583, 712, 238),
		ONYX_AMULET(6581, 6585, 721, 452),
		ONYX_NECKLACE(6577, 11128, 721, 452),
		ONYX_BRACELET(11130, 11133, 721, -1),
		
		ZENYTE_RING(19538, 19550, 712, 238),
		ZENYTE_AMULET(19541, 19553, 721, 452),
		ZENYTE_NECKLACE(19535, 19547, 721, 452),
		ZENYTE_BRACELET(19532, 19544, 721, 452),
		;
	}
	
	enum class EnchantSpell(val child: Int, val level: Int, val xp: Double, val runes: Array<Item>, val text: String, vararg val data: EnchantData) {
		SAPPHIRE(6, 7, 17.0, arrayOf(Item(555, 1), Item(564, 1)), "sapphire", SAPPHIRE_RING, SAPPHIRE_AMULET, SAPPHIRE_NECKLACE),
		EMERALD(17, 27, 37.0, arrayOf(Item(556, 3), Item(564, 1)), "emerald", EMERALD_NECKLACE, EMERALD_AMULET, EMERALD_RING),
		RUBY(29, 49, 47.0, arrayOf(Item(554, 5), Item(564, 1)), "ruby", RUBY_NECKLACE, RUBY_AMULET, RUBY_RING),
		DIAMOND(37, 57, 67.0, arrayOf(Item(557, 10), Item(564, 1)), "diamond", DIAMOND_NECKLACE, DIAMOND_AMULET, DIAMOND_RING),
		DRAGONSTONE(52, 68, 78.0, arrayOf(Item(557, 15), Item(555, 15), Item(564, 1)), "dragonstone", DRAGONSTONE_NECKLACE, DRAGONSTONE_AMULET, DRAGONSTONE_RING),
		ONYX(64, 87, 97.0, arrayOf(Item(557, 20), Item(554, 20), Item(564, 1)), "onyx", ONYX_NECKLACE, ONYX_AMULET, ONYX_RING),
		ZENYTE(66, 93, 110.0, arrayOf(Item(566, 20), Item(565, 20), Item(564, 1)), "zenyte", ZENYTE_NECKLACE, ZENYTE_AMULET, ZENYTE_RING, ZENYTE_BRACELET),
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		EnchantSpell.values().forEach { spell ->
			repo.onSpellOnItem(218, spell.child) @Suspendable {
				if (it.player().world().realm().isPVP) {
					it.messagebox("That's a bit pointless is it not..?")
				} else {
					val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]]
					
					val result = spell.data.filter { d -> d.item == item.id() }
					if (result.size > 0) {
						enchant(it, spell, result[0])
					} else {
						it.message("This spell can only be cast on ${spell.text} amulets, necklaces, rings and bracelets, " +
								"and on shapes in the Mage Training Arena.")
					}
				}
			}
		}
	}
	
	@Suspendable fun enchant(it: Script, spell: EnchantSpell, data: EnchantData) {
		if (it.player().skills()[Skills.MAGIC] >= spell.level) {

			if (!MagicCombat.has(it.player(), spell.runes, true)) {
				return
			}

			it.addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) spell.xp * 2 else spell.xp)
			it.player().inventory().remove(Item(data.item, 1), true)
			
			it.animate(data.anim)
			it.player().graphic(data.gfx, 100, 0)
			val result = Item(data.product, 1)
			// Don't give charges to items here.. they'd be untradable and stack in your bank like shit.
			// Unknown if its per item or per player attribute for The Bracelet of Clay and Inoculation Bracelet
			it.player().inventory().add(result, true)
		} else {
			it.message("Your Magic level is not high enough for this spell.")
		}
	}
	
}