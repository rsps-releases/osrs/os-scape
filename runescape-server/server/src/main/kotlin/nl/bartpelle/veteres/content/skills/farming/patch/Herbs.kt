package nl.bartpelle.veteres.content.skills.farming.patch

import co.paralleluniverse.fibers.Suspendable
import com.google.common.collect.ImmutableRangeMap
import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.skills.farming.Farming
import nl.bartpelle.veteres.content.skills.farming.Farming.farmbit
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import java.util.*

/**
 * Created by Bart on 6/4/2016.
 */
object Herbs {
	
	const val HERB_TIMER = 1000
	
	enum class Data(val seed: Item, val product: Int, val level: Int, val diseaseOdds: Double, val plantXp: Double,
	                val harvestXp: Double, val startVal: Int, val endVal: Int) {
		GUAM(Item(5291), 199, 9, 0.13, 11.0, 12.5, 4, 8),
		MARRENTIL(Item(5292), 201, 14, 0.13, 13.5, 15.0, 11, 15),
		TARROMIN(Item(5293), 203, 19, 0.12, 16.0, 18.0, 18, 22),
		HARRALANDER(Item(5294), 205, 26, 0.12, 21.5, 24.0, 25, 29),
		RANARR(Item(5295), 207, 32, 0.11, 27.0, 30.5, 32, 36),
		TOADFLAX(Item(5296), 3049, 38, 0.11, 34.0, 38.5, 39, 43),
		IRIT(Item(5297), 209, 44, 0.10, 43.0, 48.5, 46, 50),
		AVANTOE(Item(5298), 211, 50, 0.10, 54.5, 61.5, 53, 57),
		KWUARM(Item(5299), 213, 56, 0.10, 69.0, 78.0, 68, 72),
		SNAPDRAGON(Item(5300), 3051, 62, 0.09, 87.5, 98.5, 75, 79),
		CADANTINE(Item(5301), 215, 67, 0.09, 106.5, 120.0, 82, 86),
		LANTADYME(Item(5302), 2485, 73, 0.09, 134.5, 151.5, 89, 93),
		DWARF_WEED(Item(5303), 217, 79, 0.09, 170.5, 192.5, 96, 100),
		TORSTOL(Item(5304), 219, 85, 0.08, 199.5, 224.5, 103, 107)
		;
		
		fun range(): Range<Int> = Range.closed(startVal, endVal + 2)
	}
	
	// Range lookup map to facilitate looking up the values to their data objects.
	@JvmStatic val RANGELOOKUP: RangeMap<Int, Data> = ImmutableRangeMap.builder<Int, Data>()
			.apply { Data.values().forEach { data -> put(data.range(), data) } }
			.build()
	
	// Seed lookup map
	@JvmStatic val SEEDLOOKUP: Map<Int, Data> = HashMap<Int, Data>().apply {
		Data.values().forEach { data -> put(data.seed.id(), data) }
	}
	
	@Suspendable fun interactHerb(it: Script, varbit: Farmbit) {
		val varval = it.player().varps().farmbit(varbit)
		val states = HerbInfo.decompose(varval)
		val growing = RANGELOOKUP[states.stage]
		
		// Weeds?
		if (it.interactionOption() == 1) {
			if (varval >= 0 && varval <= 2) {
				Farming.rakeWeeds(it, varbit, "herb")
				return
			}
			
			// Bodied plants?
			if (states.dead) {
				clearDeadCrops(it, varbit)
				return
			}
			
			// Are these grown?
			if (growing != null && states.stage >= growing.endVal && states.stage <= growing.endVal + 2) {
				harvest(it, varbit)
				return
			}
		}
	}
	
	@Suspendable fun harvest(it: Script, varbit: Farmbit) {
		val states = HerbInfo.decompose(it.player().varps().farmbit(varbit))
		val growing = RANGELOOKUP[states.stage] ?: return
		
		// Cannot harvest with a full inventory on RS.
		if (it.player().inventory().full()) {
			it.player().message("You don't have enough inventory space to do that.")
			return
		}
		
		// Do the heavy work, but only if we have a spade.
		if (!it.player().inventory().contains(5329)) {
			it.player().message("You need a pair of secateurs to do that.")
			return
		}
		
		it.player().message("You begin to harvest the herb patch.")
		it.player().lock() // Avoid timers ticking et al
		
		var roll = if (CapeOfCompletion.FARMING.operating(it.player())) 60 else 80
		
		if (BonusContent.isActive(it.player, BlessingGroup.NATURES_GIFT)) {
			roll /= 2
		}
		
		while (true) {
			// Add the item, xp, increase stage and continue.
			it.delay(1)
			it.animate(2282)
			it.delay(2)
			it.addXp(Skills.FARMING, growing.harvestXp)
			it.player().inventory().add(Item(growing.product), true)
			
			// Odds are 1/4 to increase a stage.
			if (it.player().world().rollDie(255, roll)) {
				val value = it.player().varps().farmbit(varbit) + 1
				// Did we clean the patch?
				if (value > growing.endVal + 2) {
					it.player().varps().farmbit(varbit, 3) // Clear patch
					it.player().unlock()
					it.message("The herb patch is now empty.")
					Farming.setTimer(it.player(), varbit.timer, HERB_TIMER)
					tryForHerbPatchPet(it.player())
					break
				} else {
					it.player().varps().farmbit(varbit, value)
				}
			}
			
			// If we're out of inv space, stop the loop.
			if (it.player().inventory().full()) {
				it.player().message("You have run out of inventory space.")
				it.player().unlock()
				break
			}
		}
		
		it.player().unlock()
		
		// You treat the allotment with supercompost.
	}
	
	// Pet unlocking formula for herbs
	@JvmStatic fun tryForHerbPatchPet(player: Player) {
		val plevel = player.skills().level(Skills.FARMING)
		var takeaway: Double = 0.0
		val fullchance = 9000.0 + 11643.0 // Base chance rate + maximum possible extra base rate at level 1 Farming
		for (i in 1..plevel - 1) {
			takeaway += (plevel * 1.2)
		}
		val chance: Double = fullchance - takeaway // The actual chance based on level
		val odds: Int = (chance * player.mode().skillPetMod()).toInt() // Realm modifier
		if (player.world().rollDie(odds, 1)) {
			Allotments.unlockTangleroot(player)
		}
	}
	
	fun grow(it: Script, varbit: Farmbit) {
		val value = it.player().varps().farmbit(varbit)
		val states = HerbInfo.decompose(value)
		val growing = RANGELOOKUP[states.stage]
		
		// Do we need to regrow weeds?
		if (value > 0 && value <= 3) {
			it.player().varps().farmbit(varbit, value - 1)
			Farming.addTimer(it.player(), varbit.timer, HERB_TIMER)
			return
		}
		
		// Do we have a plant to grow or should we grow our weed?
		if (growing != null && states.stage < growing.endVal && !states.dead) {
			// Will it die?
			if (states.diseased) {
				states.dead = true
				it.player().varps().farmbit(varbit, states.compose())
				return // No more growing. It's dead. Get over it :'(
			} else if (states.stage != growing.startVal && willDisease(it.player(), states, growing)) { // Not dead, but will it disease?
				states.diseased = true
				states.stage++
				it.player().varps().farmbit(varbit, states.compose())
			} else { // All good! Grow!
				states.stage++
				it.player().varps().farmbit(varbit, states.compose())
			}
			
			// Unless we're fully grown now, resubmit the timer.
			if (states.stage != growing.endVal) {
				Farming.addTimer(it.player(), varbit.timer, HERB_TIMER)
			}
		}
	}
	
	@Suspendable fun itemOnHerbPatch(it: Script, varbit: Farmbit) {
		val item = it.itemUsedId()
		val value = it.player().varps().farmbit(varbit)
		val allotmentSeed = SEEDLOOKUP[item]
		val isSeed = Farming.ALL_SEEDS.contains(item)
		
		// Are we trying to cure plants?
		if (item == Farming.PLANT_CURE) {
			curePlants(it, varbit)
			return
		}
		
		// Dig up plants?
		if (item == Farming.SPADE) {
			clearDeadCrops(it, varbit)
			return
		}
		
		// Rake it?
		if (item == Farming.RAKE) {
			Farming.rakeWeeds(it, varbit, "herb patch")
			return
		}
		
		// One cannot water herbs
		if (item == Farming.EMPTY_CAN || item in Farming.WATERING_CANS) {
			it.player().message("This patch doesn't need watering.")
			return
		}
		
		// Is it a seed?
		if (!isSeed) {
			it.player().message("Nothing interesting happens.")
			return
		}
		
		// Can we plant this seed here?
		if (isSeed && allotmentSeed == null) {
			it.player().message("You can't plant a " + Item(item).name(it.player().world()).toLowerCase() + " in this patch.")
			return
		}
		
		// Are there weeds in our patch?
		if (value >= 0 && value < 3) {
			it.player().message("This patch needs weeding first.")
			return
		}
		
		// Is this one already occupied?
		if (value > 3) {
			it.messagebox("You can only plant ${Item(item).name(it.player().world()).toLowerCase()}s in an empty patch.")
			return
		}
		
		// Null seed? Leave.
		if (allotmentSeed == null) {
			return
		}
		
		// Are we a real master farmer? :D
		if (it.player().skills().xpLevel(Skills.FARMING) < allotmentSeed.level) {
			it.messagebox("You must be a Level ${allotmentSeed.level} Farmer to plant those.")
			return
		}
		
		// Plant the seeds
		it.player().lock()
		it.player().animate(2291)
		it.delay(3)
		
		var success = false
		if (BonusContent.isActive(it.player, BlessingGroup.NATURES_GIFT) && it.player.world().rollDie(2, 1)) {
			success = true
			it.player.message("The active Nature's Gift blessing lets you conserve your seed.")
		} else {
			success = it.player().inventory().remove(Item(allotmentSeed.seed, 1), false).success()
		}
		
		if (success) {
			val aAn = if (Item(item).name(it.player().world()).toLowerCase()[0] in arrayOf('a', 'e', 'i', 'o', 'u')) "an" else "a"
			it.player().message("You plant $aAn ${Item(item).name(it.player().world()).toLowerCase()} in the herb patch.")
			it.addXp(Skills.FARMING, allotmentSeed.plantXp)
			it.player().varps().farmbit(varbit, allotmentSeed.startVal)
			Farming.setTimer(it.player(), varbit.timer, HERB_TIMER)
		}
		
		it.player().unlock()
	}
	
	@Suspendable fun curePlants(it: Script, varbit: Farmbit) {
		val value = it.player().varps().farmbit(varbit)
		val states = HerbInfo.decompose(value)
		val growing = RANGELOOKUP[states.stage]
		
		if (growing == null) {
			it.player().message("This farming patch is empty.")
			return
		}
		
		if (!states.diseased) {
			it.player().message("This patch doesn't need curing.")
			return
		}
		
		it.player().lock()
		it.player().animate(2288)
		it.player().message("You treat the herb patch with the plant cure.")
		it.delay(4)
		
		if (it.player().inventory().remove(Item(Farming.PLANT_CURE), true).success()) {
			it.player().inventory().add(Item(229), true)
			states.diseased = false
			states.stage--
			it.player().varps().farmbit(varbit, states.compose())
			it.player().message("It is restored to health.")
		}
		
		it.player().unlock()
	}
	
	fun willDisease(player: Player, states: HerbInfo, allotment: Data): Boolean {
		var odds = allotment.diseaseOdds
		
		return false;//player.world().randomDouble() <= odds
	}
	
	@Suspendable fun clearDeadCrops(it: Script, varbit: Farmbit) {
		if (!it.player().inventory().contains(952)) {
			it.player().message("You need a spade to do that.")
			return
		}
		
		val varval = it.player().varps().farmbit(varbit)
		val states = HerbInfo.decompose(varval)
		println(states.stage)
		val growing = RANGELOOKUP[states.stage]
		
		if (growing == null && !states.dead) {
			it.player().message("There aren't any crops in this patch to dig up.")
			return
		}
		
		// Don't dig up healthy crops!
		if (!states.dead) {
			//it.chatPlayer("Dig up these healthy plants? Why would I want to do<br>that?", 576)
			if (it.optionsTitled("Dig up healthy plants?", "Yes, dig them up.", "No, let them grow.") == 2) {
				return
			}
		}
		
		it.player().message("You start digging the farming patch...")
		it.animate(830)
		it.delay(3)
		
		while (true) {
			it.animate(830)
			it.delay(3)
			
			// Random odds
			if (it.player().world().rollDie(255, 80 + it.player().skills().level(Skills.FARMING))) {
				it.player().varps().farmbit(varbit, 3)
				Farming.setTimer(it.player(), varbit.timer, HERB_TIMER)
				break
			}
		}
		
		it.player().message("You have successfully cleared this patch for new crops.")
	}
	
	data class HerbInfo(var state: Int, var stage: Int) {
		fun compose(): Int = state.and(0x1).shl(7).or(stage.and(0b1111111))
		
		var diseased: Boolean
			get() = state == 0x1
			set(v) {
				if (v) state = 0x1 else state = 0x0
			}
		
		var dead: Boolean
			get() = diseased && stage in 42..44
			set(v) {
				if (v) {
					val lookup = RANGELOOKUP[stage] ?: return
					val base = stage - lookup.startVal - 1
					stage = Math.max(42, Math.min(44, base + 42))
					diseased = true
				}
			}
		
		companion object {
			fun decompose(state: Int): HerbInfo = HerbInfo(state.shr(7).and(0x1), state.and(0b1111111))
		}
	}
	
}