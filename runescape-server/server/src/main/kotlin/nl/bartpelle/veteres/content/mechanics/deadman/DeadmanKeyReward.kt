package nl.bartpelle.veteres.content.mechanics.deadman

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.util.Varbit
import java.util.*

/**
 * Created by Jak on 05/11/2016.
 *
 * Handles giving keys to a PKer when they kill someone in Deadman mode.
 *
 * Mechanics: https://titanpad.com/CqfPRaanfB
 * Vid https://www.youtube.com/watch?v=BD8sNqKR-DY&feature=youtu.be&t=82
 *
 * Depending on if you are skulled and/or in a Safezone, EXP lost on death greatly varies between (minimum unknown) - 50%
 *
 * Depending on how many keys the PKer is carrying, keys to be rewarded are filtered by value and given to the killer, or dropped
 * on the floor if they do not have room. When you have 5 keys, your targets bank key and any keys they carried converts instantly to the raw items,
 * which are then dropped. This was the first time in Oldschool history when there would be a good reason for having over 128 ground items on the floor
 * at once. Jagex had to implement a prioritization system dependant on values to sort which items appeared on the top of the loot pile.
 */
object DeadmanKeyReward {
	
	// Called on death
	@JvmStatic fun handle_deadman_death(dead: Player, killer: Player) {
		if (dead.id() == killer.id()) return // Suicide / pvm death
		val safedeath: Boolean = !inDangerous(dead)
		val killerinfo = DeadmanMechanics.infoForPlayer(killer)
		val deadinfo = DeadmanMechanics.infoForPlayer(dead).updateBankKey()
		
		if (safedeath) {
			handle_safezone_death(dead, killer, deadinfo, killerinfo)
		} else {
			handle_dangerous_zone_death(dead, killer, deadinfo, killerinfo)
		}
		handle_exp_loss(dead, killer, deadinfo, killerinfo, safedeath)
		
		if (dead.world().server().config().hasPathOrNull("deadman.jaktestserver")) {
			// Since in test worlds we don't lose items, remove keys from inventory manually
			for (keyid in DeadmanMechanics.KEYS) {
				dead.inventory().remove(Item(keyid, Integer.MAX_VALUE), true)
			}
		}
	}
	
	// Remove experience from dead player
	private fun handle_exp_loss(dead: Player, killer: Player, deadinfo: DeadmanContainers, killerinfo: DeadmanContainers, safedeath: Boolean) {
		if (!safedeath) {
			// TODO
		}
	}
	
	// Give keys to killer when in a dangerous zone.
	private fun handle_dangerous_zone_death(dead: Player, killer: Player, deadinfo: DeadmanContainers, killerinfo: DeadmanContainers) {
		
		var dead_carried = dead.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED)
		val carried = killer.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED)
		val possible_total = carried + dead_carried + 1// Plus one for target's personal bank key.
		
		if (carried != 5 && dead_carried == 0) {
			killer.message("You are awarded the key to the bank of %s.", dead.name())
			giveOrDropDeadBankKey(dead, killer, deadinfo, killerinfo, carried)
		} else if (carried != 5 && dead_carried > 0) {
			// Send a full informational message stating total keys rewarded
			killer.message("You have been awarded $dead_carried bank keys that ${dead.name()} was carrying including the key to their own bank.")
			
			val filteredKeys = DeadmanContainers.filteredKeys(deadinfo.keys) // Possible 5
			
			
			// source: https://www.youtube.com/watch?v=mUWWzzUQsBQ
			// (1) see 4:05 for holding 3, killed 2 (total 6, lost 1)
			// (2) 5:05 for holding 0, killed 5 (total 6, lost 1)
			// (3) 6:33 for holding 4, killed 5 (total 10, lost 5)
			
			val destroyed = possible_total - 5 // Keys we cant carry. Example: Possible total 10 (us 4, them 5, their bank) minus 5 we're carrying = 5 will be destroyed
			val rewarded = dead_carried + 1 // Carried plus their own bank key
			var toAdd = Math.min(5 - carried, rewarded) // 5-3 carried = 2 slots free for keys
			var keycount = carried
			
			val keysToGive: ArrayList<BankKey> = ArrayList<BankKey>(toAdd) // Plus their bank makes 6
			keysToGive.add(createBankKey(dead, deadinfo))
			if (toAdd > 1) { // More than just the dead guys bank key
				for (i in 0..toAdd - 1)
					keysToGive.add(filteredKeys[i])
			}
			
			// Step 1: Add as many keys to inventory as possible until inventory full.
			while (toAdd > 0 && killer.inventory().freeSlots() > 0) {
				val itemId = when (keycount) {
					0 -> 13302 // Key 1
					1 -> 13303 // Key 2
					2 -> 13304 // Key 3
					3 -> 13305 // Key 4
					4 -> 13306 // Key 5
					else -> 13302 // Key 1 default
				}
				val keyitem = Item(itemId)
				val deadkey = keysToGive[keycount - carried]
				if (killer.inventory().add(keyitem, false).success()) {
					killerinfo.keys[keycount] = deadkey
					//killer.debug("Your key #%d value is %d. Total keys now %d", keycount+1, deadkey.value, keycount+1)
					killer.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, keycount + 1)
					killer.looks().update()
					toAdd--
					keycount++
				}
			}
			// Step 2: If there are remaining keys to add, notify the player.
			if (toAdd > 0 && killer.inventory().freeSlots() == 0) {
				killer.message("Your inventory is full, and so the keys have remained where your victim died.")
				
				// Step 3: Drop the remaining keys that should have been added.. up until we have 5 keys. Any more than the 5th is destroyed.
				while (toAdd-- > 0) {
					val itemId = when (keycount) {
						0 -> 13302 // Key 1
						1 -> 13303 // Key 2
						2 -> 13304 // Key 3
						3 -> 13305 // Key 4
						4 -> 13306 // Key 5
						else -> 13302 // Key 1 default
					}
					val gitem = GroundItem(killer.world(), Item(itemId), dead.tile(), killer.id()).pkedFrom(dead.id()).hidden().linkDmmBankKey(keysToGive[keycount - carried])
					killer.world().spawnGroundItem(gitem)
					keycount++
				}
			}
			// Step 4: Notify how many were lost.
			if (keycount == 5) {
				val countText = when (destroyed) {
					1 -> "One"
					2 -> "Two"
					3 -> "Three"
					4 -> "Four"
					5 -> "Five"
					else -> "One"
				}
				killer.message("You have reached the limit of 5 bank keys. $countText more key${
				if (destroyed == 1) "" else "s"
				}, the least valuable, ${
				if (destroyed == 1) "has" else "have"} been destroyed.")
			}
		} else if (carried == 5) {
			// We already have 5, dead player has 0 keys. Drop their bank raw, no key. We can't carry another key. It's not filtered against our current.
			// (Incentive to wait out skull)
			killer.message("You have reached the limit of 5 bank keys.")
			val deadkey = createBankKey(dead, deadinfo)
			for (loot in deadkey.lootcontainer) {
				loot ?: continue
				val gitem = GroundItem(killer.world(), loot, dead.tile(), killer.id()).pkedFrom(dead.id())
				killer.world().spawnGroundItem(gitem)
			}
			deadkey.lootcontainer.empty() // Clear
			
			// Also convert dead players keys into items and drop (yeah there can be like 200 items on the floor at once...)
			// Source: https://www.youtube.com/watch?v=YcJyEw1qRZ8 time 5:12 -> made up of 28 bank + 28 inventory + 13 equipment + 28 key1 + 28 key2 = 125 items (stackables.. 112 options)
			if (dead_carried > 1) {
				for (i in 0..4) {
					val deadkey = deadinfo.keys[i] ?: continue
					for (loot in deadkey.lootcontainer) {
						loot ?: continue
						val gitem = GroundItem(killer.world(), loot, dead.tile(), killer.id()).pkedFrom(dead.id())
						killer.world().spawnGroundItem(gitem)
					}
					deadkey.lootcontainer.empty() // Clear
				}
			}
		}
		//killer.debug("[Danger death] New key total: %d  dead:%d  possible_total:%d", killer.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED), dead_carried, possible_total)
	}
	
	/**
	 * When you have <5 keys and target has none, instantly reward their bank key.
	 * Not as complex as the above system for dealing with the possibility of carrying over 5 keys.
	 */
	private fun giveOrDropDeadBankKey(dead: Player, killer: Player, deadinfo: DeadmanContainers, killerinfo: DeadmanContainers, carried: Int) {
		val itemId = when (carried) {
			0 -> 13302 // Key 1
			1 -> 13303 // Key 2
			2 -> 13304 // Key 3
			3 -> 13305 // Key 4
			4 -> 13306 // Key 5
			else -> 13302 // Key 1 default
		}
		val keyitem = Item(itemId)
		val deadkey = createBankKey(dead, deadinfo)
		if (killer.inventory().add(keyitem, false).success()) {
			killerinfo.keys[carried] = deadkey
			killer.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, carried + 1)
			killer.looks().update()
		} else {
			val gitem = GroundItem(killer.world(), keyitem, dead.tile(), killer.id()).pkedFrom(dead.id()).hidden().linkDmmBankKey(deadkey)
			killer.world().spawnGroundItem(gitem)
			killer.message("Your inventory is full, and so the key has remained where your victim died.")
		}
	}
	
	/**
	 * Drop keys when dying in a Safe zone. This can possibly be intergrated into the above block of code, just forcing items to drop
	 * rather than going into the Killer's inventory.
	 */
	private fun handle_safezone_death(dead: Player, killer: Player, deadinfo: DeadmanContainers, killerinfo: DeadmanContainers) {
		killer.message("%s has died and you have been awarded a key to their bank.", dead.name())
		killer.message("The key remains where your victim died.")
		// TODO filter and destory shitter ones than the 5 you're carrying and drop others
		val gkey = GroundItem(killer.world(), Item(13302), dead.tile(), killer.id()).pkedFrom(dead.id()).hidden().linkDmmBankKey(createBankKey(dead, deadinfo))
		killer.world().spawnGroundItem(gkey)
	}
	
	/**
	 * Create a BankKey from the dead player's bank. Also remove the items from their bank.
	 */
	private fun createBankKey(dead: Player, deadinfo: DeadmanContainers): BankKey {
		val rawItemsKey = deadinfo.bankItemsKey.copy() // This is the key killer will get
		for (loot in rawItemsKey.lootcontainer) {
			loot ?: continue
			//dead.bank().remove(loot, true) // TODO enable on real game
			dead.message("You would have lost ${loot.amount()} x ${loot.definition(dead.world()).name} from your bank on real DMM.")
		}
		deadinfo.bankItemsKey = BankKey(dead.world())
		deadinfo.updateBankKey() // Update dead player's info so their bank key has the items they'll lose when they die next time.
		// Create an empty key
		val rewardedBankKey = BankKey(dead.world())
		// Cycle the lost items, add them to the empty key which the Killer will get as noted items.
		for (loot in rawItemsKey.lootcontainer) {
			loot ?: continue
			rewardedBankKey.lootcontainer.add(loot.note(dead.world()), false)
			rewardedBankKey.value += loot.realPrice(dead.world()) * loot.amount()
		}
		return rewardedBankKey
	}
	
	/**
	 * Is the player in a Dangerous location? This is established every game cycle in WildernessLevelIndicator.
	 */
	@JvmStatic fun inDangerous(player: Player): Boolean {
		return player.attribOr<Int>(AttributeKey.GENERAL_VARBIT1, 0) as Int shr 22 and 3 == 1
	}
}