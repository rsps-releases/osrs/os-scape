package nl.bartpelle.veteres.content.areas.dungeons.monkeymadness2

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/11/2016.
 */
object MonkeyMadnessObjects {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Spawn the gate until we have MM2
		r.onWorldInit {
			it.ctx<World>().spawnObj(MapObj(Tile(1987, 5568), 28656, 0, 3))
			it.ctx<World>().spawnObj(MapObj(Tile(2435, 3520), 28656, 0, 3))
		}
		
		// Cave exit:
		r.onObject(28687) @Suspendable {
			it.player().lock()
			it.delay(1)
			it.player().animate(828)
			it.delay(1)
			it.player().teleport(2026, 5611)
			it.player().unlock()
		}
		
		// Cave entrance:
		r.onObject(28686) @Suspendable {
			if (false && it.player().world().realm().isPVP) {
				it.player().message("Why would you want to go in there?")
			} else {
				it.player().lock()
				it.delay(1)
				it.player().message("You enter the cavern beneath the crash site.")
				it.player().message("Why would you want to go in there?")
				it.delay(1)
				it.player().teleport(2129, 5646)
				it.player().unlock()
			}
		}
		
		// Gnome stronghold exit
		r.onObject(28656) @Suspendable {
			if (it.player().tile().z >= 5568) {
				it.player().teleport(2435, 3519)
			} else {
				if (/*!it.player().world().realm().isPVP && */it.optionsTitled("Leave the Gnome Stronghold?", "Yes.", "No.") == 1) {
					it.player().teleport(1987, 5568)
				}
			}
		}
	}
	
}