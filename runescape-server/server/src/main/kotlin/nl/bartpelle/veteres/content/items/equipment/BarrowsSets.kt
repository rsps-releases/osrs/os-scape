package nl.bartpelle.veteres.content.items.equipment

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot

/**
 * Created by Bart on 10/31/2015.
 */

object BarrowsSets {
	
	@JvmStatic fun hasVerac(player: Player): Boolean {
		val helm = player.equipment()[EquipSlot.HEAD]?.id() ?: return false
		val body = player.equipment()[EquipSlot.BODY]?.id() ?: return false
		val legs = player.equipment()[EquipSlot.LEGS]?.id() ?: return false
		val wep = player.equipment()[EquipSlot.WEAPON]?.id() ?: return false
		
		// Now check for matching ids
		if (helm != 4753 && helm != 4976 && helm != 4977 && helm != 4978 && helm != 4979) {
			return false;
		}
		if (body != 4757 && body != 4988 && body != 4989 && body != 4990 && body != 4991) {
			return false;
		}
		if (legs != 4759 && legs != 4994 && legs != 4995 && legs != 4996 && legs != 4997) {
			return false;
		}
		if (wep != 4755 && wep != 4982 && wep != 4983 && wep != 4984 && wep != 4985) {
			return false;
		}
		
		return true
	}
	
	@JvmStatic fun hasGuthan(player: Player): Boolean {
		val helm = player.equipment()[EquipSlot.HEAD]?.id() ?: return false
		val body = player.equipment()[EquipSlot.BODY]?.id() ?: return false
		val legs = player.equipment()[EquipSlot.LEGS]?.id() ?: return false
		val wep = player.equipment()[EquipSlot.WEAPON]?.id() ?: return false
		
		// Now check for matching ids
		if (helm != 4724 && helm != 4904 && helm != 4905 && helm != 4906 && helm != 4907) {
			return false;
		}
		if (body != 4728 && body != 4916 && body != 4917 && body != 4918 && body != 4919) {
			return false;
		}
		if (legs != 4730 && legs != 4922 && legs != 4923 && legs != 4924 && legs != 4925) {
			return false;
		}
		if (wep != 4726 && wep != 4910 && wep != 4911 && wep != 4912 && wep != 4913) {
			return false;
		}
		
		return true
	}
	
	
}