package nl.bartpelle.veteres.content.areas.zeah.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/11/2016.
 */

object IshTheNavigator {
	
	val ISH_THE_NAVIGATOR = 7378
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ISH_THE_NAVIGATOR) @Suspendable {
			val TALKED_TO_IGNISIA = it.player().attribOr<Int>(AttributeKey.TALKED_TO_IGNISIA, 0)
			
			if (TALKED_TO_IGNISIA == 1) {
				it.chatNpc("Be on your guard friend.", ISH_THE_NAVIGATOR, 592)
				when (it.options("Tell me about the Wintertodt.", "Tell me about yourself.", "What do you think of the Pyromancers?", "I have to go.")) {
					1 -> tell_me_about_wintertodt(it)
					2 -> tell_me_about_yourself(it)
					3 -> what_do_you_think_of_the_pyromancers(it)
					4 -> I_have_to_go(it)
				}
			} else {
				it.chatNpc("Be vigilant friend.", ISH_THE_NAVIGATOR, 592)
				it.chatPlayer("What do you mean?", 554)
				it.chatNpc("Ignisia will explain. Just be careful of what she tells<br>you.", ISH_THE_NAVIGATOR, 593)
			}
		}
	}
	
	@Suspendable fun tell_me_about_wintertodt(it: Script) {
		it.chatPlayer("Tell me about the Wintertodt.", 554)
		it.chatNpc("The Wintertodt terrorised Great Kourend many<br>centuries ago. It destroyed much of this northern land<br>" +
				"over many years before it was finally driven back into<br>this prison.", ISH_THE_NAVIGATOR, 591)
		it.chatPlayer("Why did it attack Kourend?", 554)
		it.chatNpc("No one knows for sure but I have a theory...", ISH_THE_NAVIGATOR, 592)
		it.chatPlayer("Which is?", 554)
		it.chatNpc("The Dark Altar. It felt the Altar's power and wanted to<br>consume it for itself. It wouldn't be the first time...", ISH_THE_NAVIGATOR, 593)
		it.chatPlayer("Interesting...", 575)
		when (it.options("Tell me about yourself.", "What do you think of the Pyromancers?", "I have to go.")) {
			1 -> tell_me_about_yourself(it)
			2 -> what_do_you_think_of_the_pyromancers(it)
			3 -> I_have_to_go(it)
		}
	}
	
	@Suspendable fun tell_me_about_yourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 554)
		it.chatNpc("I'm Ish, one of the navigators of the Piscarilius House.", ISH_THE_NAVIGATOR, 588)
		it.chatPlayer("What brings a navigator up here?", 554)
		it.chatNpc("Lady Piscarilius has sent me here to keep an eye on<br>the situation. She doesn't trust the other houses and" +
				"<br>believes something sinister is going on here.", ISH_THE_NAVIGATOR, 594)
		it.chatPlayer("Like what?", 54)
		it.chatNpc("I don't know, but I intend to find out.", ISH_THE_NAVIGATOR, 592)
		when (it.options("Tell me about the Wintertodt.", "What do you think of the Pyromancers?", "I have to go.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> what_do_you_think_of_the_pyromancers(it)
			3 -> I_have_to_go(it)
		}
	}
	
	@Suspendable fun what_do_you_think_of_the_pyromancers(it: Script) {
		it.chatPlayer("What do you think of the Pyromancers?", 554)
		it.chatNpc("I've heard stories about them and the things they have<br>done over the years. Be careful of them, they can't be<br>trusted.", ISH_THE_NAVIGATOR, 594)
		it.chatPlayer("You seem a bit paranoid.", 554)
		it.chatNpc("Better to be paranoid than a fool. Anyone with any<br>sense knows that the Pyromancers are trouble. They<br>say even Lord Arceuus doesn't trust them.", ISH_THE_NAVIGATOR, 590)
		when (it.options("Tell me about the Wintertodt.", "Tell me about yourself.", "I have to go.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_yourself(it)
			3 -> I_have_to_go(it)
		}
	}
	
	@Suspendable fun I_have_to_go(it: Script) {
		it.chatPlayer("I have to go.", 588)
		it.chatNpc("Stay vigilant friend.", ISH_THE_NAVIGATOR, 592)
	}
	
}
