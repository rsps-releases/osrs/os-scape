package nl.bartpelle.veteres.content.skills.farming.patch

import co.paralleluniverse.fibers.Suspendable
import com.google.common.collect.ImmutableRangeMap
import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.skills.farming.Farming
import nl.bartpelle.veteres.content.skills.farming.Farming.farmbit
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import java.util.*

/**
 * Created by Situations on 2017-02-10.
 */

object Flowers {
	
	const val FLOWER_TIMER = 250
	
	enum class Data(val seed: Item, val product: Int, val level: Int, val plantXp: Double,
	                val harvestXp: Double, val startVal: Int, val endVal: Int) {
		MARIGOLDS(Item(5096), 6010, 3, 8.5, 47.0, 8, 12),
		ROSEMARY(Item(5097), 6014, 11, 12.0, 66.5, 13, 17),
		NASTURTIUM(Item(5098), 6012, 24, 19.5, 111.0, 18, 22),
		WOAD(Item(5099), 1793, 25, 20.5, 115.5, 23, 27),
		LIMPWURT(Item(5100), 225, 30, 21.5, 120.0, 28, 32);
		
		fun range(): Range<Int> = Range.closed(startVal, endVal)
	}
	
	// Range lookup map to facilitate looking up the values to their data objects.
	@JvmStatic val RANGELOOKUP: RangeMap<Int, Data> = ImmutableRangeMap.builder<Int, Data>()
			.apply { Data.values().forEach { data -> put(data.range(), data) } }
			.build()
	
	// Seed lookup map
	@JvmStatic val SEEDLOOKUP: Map<Int, Data> = HashMap<Int, Data>().apply {
		Data.values().forEach { data -> put(data.seed.id(), data) }
	}
	
	@Suspendable fun interactFlower(it: Script, varbit: Farmbit) {
		val varval = it.player().varps().farmbit(varbit)
		val states = FlowerInfo.decompose(varval)
		val growing = RANGELOOKUP[states.stage]
		
		// Weeds?
		if (it.interactionOption() == 1) {
			if (varval >= 0 && varval <= 2) {
				Farming.rakeWeeds(it, varbit, "flower")
				return
			}
			
			// Bodied plants?
			if (states.dead) {
				Herbs.clearDeadCrops(it, varbit)
				return
			}
			
			// Are these grown?
			if (growing != null && states.stage >= growing.endVal && states.stage <= growing.endVal + 2) {
				harvest(it, varbit)
				return
			}
		}
	}
	
	@Suspendable fun harvest(it: Script, varbit: Farmbit) {
		val states = FlowerInfo.decompose(it.player().varps().farmbit(varbit))
		val growing = RANGELOOKUP[states.stage] ?: return
		
		// Cannot harvest with a full inventory on RS.
		if (it.player().inventory().full()) {
			it.player().message("You don't have enough inventory space to do that.")
			return
		}
		
		// Do the heavy work, but only if we have a spade.
		if (!it.player().inventory().contains(5329)) {
			it.player().message("You need a pair of secateurs to do that.")
			return
		}
		
		it.player().lock() // Avoid timers ticking et al
		// Add the item, xp, increase stage and continue.
		it.delay(1)
		it.animate(2292)
		it.delay(2)
		it.addXp(Skills.FARMING, growing.harvestXp)
		it.player().inventory().add(Item(growing.product), true)
		it.player().varps().farmbit(varbit, 3) // Clear patch
		it.player().unlock()
		Farming.setTimer(it.player(), varbit.timer, FLOWER_TIMER)
		tryForFlowerPatchPet(it.player())
		
		it.player().unlock()
		
		// You treat the allotment with supercompost.
	}
	
	// Pet unlocking formula for herbs
	@JvmStatic fun tryForFlowerPatchPet(player: Player) {
		val plevel = player.skills().level(Skills.FARMING)
		var takeaway: Double = 0.0
		val fullchance = 9000.0 + 11643.0 // Base chance rate + maximum possible extra base rate at level 1 Farming
		for (i in 1..plevel - 1) {
			takeaway += (plevel * 1.2)
		}
		val chance: Double = fullchance - takeaway // The actual chance based on level
		val odds: Int = (chance * player.mode().skillPetMod()).toInt() // Realm modifier
		if (player.world().rollDie(odds, 1)) {
			Allotments.unlockTangleroot(player)
		}
	}
	
	fun grow(it: Script, varbit: Farmbit) {
		val value = it.player().varps().farmbit(varbit)
		val states = FlowerInfo.decompose(value)
		val growing = RANGELOOKUP[states.stage]
		
		// Do we need to regrow weeds?
		if (value > 0 && value <= 3) {
			it.player().varps().farmbit(varbit, value - 1)
			Farming.addTimer(it.player(), varbit.timer, FLOWER_TIMER)
			return
		}
		
		// Do we have a plant to grow or should we grow our weed?
		if (growing != null && states.stage < growing.endVal && !states.dead) {
			// Will it die?
			if (states.diseased) {
				states.dead = true
				it.player().varps().farmbit(varbit, states.compose())
				return // No more growing. It's dead. Get over it :'(
			} else if (states.stage != growing.startVal && willDisease(it.player(), states, growing)) { // Not dead, but will it disease?
				states.diseased = true
				states.stage++
				it.player().varps().farmbit(varbit, states.compose())
			} else { // All good! Grow!
				states.stage++
				it.player().varps().farmbit(varbit, states.compose())
			}
			
			// Unless we're fully grown now, resubmit the timer.
			if (states.stage != growing.endVal) {
				Farming.addTimer(it.player(), varbit.timer, FLOWER_TIMER)
			}
		}
	}
	
	@Suspendable fun itemOnFlowerPatch(it: Script, varbit: Farmbit) {
		val item = it.itemUsedId()
		val value = it.player().varps().farmbit(varbit)
		val flowerSeed = SEEDLOOKUP[item]
		val isSeed = Farming.ALL_SEEDS.contains(item)
		
		// Are we trying to cure plants?
		if (item == Farming.PLANT_CURE) {
			curePlants(it, varbit)
			return
		}
		
		// Dig up plants?
		if (item == Farming.SPADE) {
			clearDeadCrops(it, varbit)
			return
		}
		
		// Rake it?
		if (item == Farming.RAKE) {
			Farming.rakeWeeds(it, varbit, "flower patch")
			return
		}
		
		// Is it a seed?
		if (!isSeed) {
			it.player().message("Nothing interesting happens.")
			return
		}
		
		// Can we plant this seed here?
		if (isSeed && flowerSeed == null) {
			it.player().message("You can't plant a " + Item(item).name(it.player().world()).toLowerCase() + " in this patch.")
			return
		}
		
		// Are there weeds in our patch?
		if (value >= 0 && value < 3) {
			it.player().message("This patch needs weeding first.")
			return
		}
		
		// Is this one already occupied?
		if (value > 3) {
			it.messagebox("You can only plant ${Item(item).name(it.player().world()).toLowerCase()}s in an empty patch.")
			return
		}
		
		// Null seed? Leave.
		if (flowerSeed == null) {
			return
		}
		
		//Does our player have a seed dibber?
		if (!it.player().inventory().has(Farming.SEED_DIBBER)) {
			it.messagebox("You need a seed dibber to plant seeds.")
			return
		}
		
		// Are we a real master farmer? :D
		if (it.player().skills().xpLevel(Skills.FARMING) < flowerSeed.level) {
			it.messagebox("You must be a Level ${flowerSeed.level} Farmer to plant those.")
			return
		}
		
		// Plant the seeds
		it.player().lock()
		it.player().animate(2291)
		it.delay(3)
		
		var success = false
		if (BonusContent.isActive(it.player, BlessingGroup.NATURES_GIFT) && it.player.world().rollDie(2, 1)) {
			success = true
			it.player.message("The active Nature's Gift blessing lets you conserve your seed.")
		} else {
			success = it.player().inventory().remove(Item(flowerSeed.seed, 1), false).success()
		}
		
		if (success) {
			val aAn = if (Item(item).name(it.player().world()).toLowerCase()[0] in arrayOf('a', 'e', 'i', 'o', 'u')) "an" else "a"
			it.player().message("You plant $aAn ${Item(item).name(it.player().world()).toLowerCase()} in the flower patch.")
			it.addXp(Skills.FARMING, flowerSeed.plantXp)
			it.player().varps().farmbit(varbit, flowerSeed.startVal)
			Farming.setTimer(it.player(), varbit.timer, FLOWER_TIMER)
		}
		
		it.player().unlock()
	}
	
	
	fun willDisease(player: Player, states: FlowerInfo, allotment: Data): Boolean {
		var odds = 0
		
		return false;//player.world().randomDouble() <= odds
	}
	
	@Suspendable fun curePlants(it: Script, varbit: Farmbit) {
		val value = it.player().varps().farmbit(varbit)
		val states = FlowerInfo.decompose(value)
		val growing = RANGELOOKUP[states.stage]
		
		if (growing == null) {
			it.player().message("This farming patch is empty.")
			return
		}
		
		if (!states.diseased) {
			it.player().message("This patch doesn't need curing.")
			return
		}
		
		it.player().lock()
		it.player().animate(2288)
		it.player().message("You treat the flower patch with the plant cure.")
		it.delay(4)
		
		if (it.player().inventory().remove(Item(Farming.PLANT_CURE), true).success()) {
			it.player().inventory().add(Item(229), true)
			states.diseased = false
			states.stage--
			it.player().varps().farmbit(varbit, states.compose())
			it.player().message("It is restored to health.")
		}
		
		it.player().unlock()
	}
	
	@Suspendable fun clearDeadCrops(it: Script, varbit: Farmbit) {
		if (!it.player().inventory().contains(952)) {
			it.player().message("You need a spade to do that.")
			return
		}
		
		val varval = it.player().varps().farmbit(varbit)
		val states = FlowerInfo.decompose(varval)
		val growing = RANGELOOKUP[states.stage]
		
		if (growing == null && !states.dead) {
			it.player().message("There aren't any crops in this patch to dig up.")
			return
		}
		
		// Don't dig up healthy crops!
		if (!states.dead) {
			//it.chatPlayer("Dig up these healthy plants? Why would I want to do<br>that?", 576)
			if (it.optionsTitled("Dig up healthy plants?", "Yes, dig them up.", "No, let them grow.") == 2) {
				return
			}
		}
		
		it.player().message("You start digging the farming patch...")
		it.animate(830)
		it.delay(3)
		
		while (true) {
			it.animate(830)
			it.delay(3)
			
			// Random odds
			if (it.player().world().rollDie(255, 80 + it.player().skills().level(Skills.FARMING))) {
				it.player().varps().farmbit(varbit, 3)
				Farming.setTimer(it.player(), varbit.timer, FLOWER_TIMER)
				break
			}
		}
		
		it.player().message("You have successfully cleared this patch for new crops.")
	}
	
	
	data class FlowerInfo(var state: Int, var stage: Int) {
		fun compose(): Int = state.and(0x1).shl(7).or(stage.and(0b1111111))
		
		var diseased: Boolean
			get() = state == 0x1
			set(v) {
				if (v) state = 0x1 else state = 0x0
			}
		
		var dead: Boolean
			get() = diseased && stage in 42..44
			set(v) {
				if (v) {
					val lookup = RANGELOOKUP[stage] ?: return
					val base = stage - lookup.startVal - 1
					stage = Math.max(42, Math.min(44, base + 42))
					diseased = true
				}
			}
		
		companion object {
			fun decompose(state: Int): FlowerInfo = FlowerInfo(state.shr(7).and(0x1), state.and(0b1111111))
		}
	}
	
}
