package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 08/04/2017.
 */
object CombatDegrading {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onLogin {
			it.player().timers().register(TimerKey.COMBAT_DEGRADE_CHECK, 9)
		}
		
		repo.onTimer(TimerKey.COMBAT_DEGRADE_CHECK) {
			val player = it.player()
			player.timers().extendOrRegister(TimerKey.COMBAT_DEGRADE_CHECK, 9)
			if (!PlayerCombat.inCombat(player)) {
				return@onTimer
			}
			if (Equipment.venomHelm(player)) {
				val helm: Item = player.equipment().get(EquipSlot.HEAD) ?: return@onTimer
				val scales = helm.property(ItemAttrib.ZULRAH_SCALES)
				helm.property(ItemAttrib.ZULRAH_SCALES, scales - 1)
				if (scales == 1) {
					player.message("<col=145A32>Your serpentine helm has run out of scales.")
					val unchargedId = when (helm.id()) {
						13197 -> 13196 // tanz
						13199 -> 13198 // magma
						else -> 12929 // normal serp
					}
					player.equipment().set(EquipSlot.HEAD, Item(unchargedId))
				}
			}
			// If you want to add barrows degrading in future, it'd go below here.
		}
	}
}