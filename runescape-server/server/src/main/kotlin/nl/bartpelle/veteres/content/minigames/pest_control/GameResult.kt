package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.skript.Script

import nl.bartpelle.veteres.model.AttributeKey

/**
 * Created by Jason MacKeigan on 2016-08-01 at 4:40 PM
 */
/**
 * The result that this pest control game may have. The result determines
 * whether or not the players are able to accept points, as well as the
 * message that is displayed to them after the game.
 */
enum class GameResult() {
	WIN_BY_PORTAL() {
		override fun dialogue(squire: Int, points: Int, script: Script) {
			script.chatNpc("Congratulations! You managed to destroy all the portals! We've awarded you $points" +
					"Void Knight Commendation points. Please also accept these coins as a reward.", squire)
			script.messagebox("You now have ${script.player().attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0)}" +
					" Void Knight Commendation points!")
		}
	},
	WIN_BY_TIME() {
		override fun dialogue(squire: Int, points: Int, script: Script) {
			script.chatNpc("Congratulations! You managed to keep the Knight alive! We've awarded you $points" +
					"Void Knight Commendation points. Please also accept these coins as a reward.", squire)
			script.messagebox("You now have ${script.player().attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0)}" +
					" Void Knight Commendation points!")
		}
	},
	LOSS_BY_DEATH() {
		override fun dialogue(squire: Int, points: Int, script: Script) {
			script.chatNpc("The Void Knight was killed, another of our Order has fallen and that Island is lost.", squire)
		}
	}
	;
	
	abstract fun dialogue(squire: Int, points: Int, script: Script)
}