package nl.bartpelle.veteres.content.skills.herblore

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/29/2015.
 */

object PotionBrewing {
	
	enum class BrewablePotion(val herb: Int, val secondary: Int, val unfinished: Item, val result: Int, val level: Int,
	                          val xp: Double, val putName: String, val ingredName: String) {
		ATTACK_POTION(249, 221, Item(91), 121, 1 /* Actually 3 but we don't have the quest */, 25.0, "Guam Leaf", "eye of newt"),
		ANTIPOISON(251, 235, Item(93), 175, 5, 37.5, "Marrentill", "unicorn horn"),
		RELICYMS_BALM(1534, 1526, Item(4840), 4844, 8, 40.0, "Rogue's Purse", "snake weed"),
		STRENGTH_POTION(253, 225, Item(95), 115, 12, 50.0, "Tarromin", "limpwurt root"),
		SERUM_207(-1, 592, Item(95), 3410, 15, 50.0, "Tarromin", "ashes"),
		RESTORE_POTION(255, 223, Item(97), 127, 22, 62.5, "Harralander", "eggs"),
		ENERGY_POTION(-1, 1975, Item(97), 3010, 22, 62.5, "Harralander", "chocolate dust"),
		DEFENCE_POTION(257, 239, Item(99), 133, 30, 75.0, "Ranarr Weed", "white berries"),
		AGILITY_POTION(2998, 2152, Item(3002), 3034, 34, 80.0, "Toadflax", "toad legs"), //sic
		COMBAT_POTION(-1, 9736, Item(97), 9741, 36, 84.0, "Harralander", "goat horn dust"),
		PRAYER_POTION(-1, 231, Item(99), 139, 38, 87.5, "Ranarr Weed", "snape grass"),
		SUPER_ATTACK(259, 221, Item(101), 145, 45, 100.0, "Irit Leaf", "eye of newt"),
		SUPER_ANTIPOISON(-1, 235, Item(101), 181, 48, 106.3, "Irit Leaf", "unicorn horn"),
		FISHING_POTION(261, 231, Item(103), 151, 50, 112.5, "Avantoe", "snape grass"),
		SUPER_ENERGY(-1, 2970, Item(103), 3018, 52, 117.5, "Avantoe", "Mort Myre fungi"),
		HUNTER_POTION(-1, 10111, Item(103), 10000, 53, 120.0, "Avantoe", "ground kebbit teeth"),
		SUPER_STRENGTH(263, 225, Item(105), 157, 55, 125.0, "Kwuarm", "limpwurt root"),
		WEAPON_POISON(-1, 243, Item(105), 187, 60, 137.5, "Kwuarm", "blue dragon scale"),
		SUPER_RESTORE(3000, 223, Item(3004), 3026, 63, 142.5, "Snapdragon", "eggs"),
		SUPER_DEFENCE(265, 239, Item(107), 163, 66, 150.0, "Cadantine", "white berries"),
		ANTIDOTE_PLUS(2998, 6049, Item(5942), 5943, 68, 155.0, "Toadflax", "yew roots"),
		ANTIFIRE(2481, 241, Item(2483), 2454, 69, 157.5, "Lantadyme", "blue dragon scale"),
		RANGING_POTION(267, 245, Item(109), 169, 72, 162.5, "Dwarf Weed", "wine of Zamorak"), // TODO does this consume the can? dont think so
		MAGIC_POTION(-1, 3138, Item(2483), 3042, 76, 172.5, "Lantadyme", "potato cactus"),
		ZAMORAK_BREW(269, 247, Item(111), 189, 78, 175.0, "Torstol", "jangerberries"),
		ANTIDOTE_PP(259, 6051, Item(5951), 5952, 79, 177.5, "Irit Leaf", "magic roots"),
		SARADOMIN_BREW(-1, 6693, Item(3002), 6687, 81, 180.0, "Toadflax", "crushed nest"),
		EXTENDED_ANTIFIRE_4(-1, 2452, Item(11994, 4), 11951, 84, 110.0, "N/A", "lava scale shards x4"),
		EXTENDED_ANTIFIRE_3(-1, 2454, Item(11994, 3), 11953, 84, 82.5, "N/A", "lava scale shards x1"),
		EXTENDED_ANTIFIRE_2(-1, 2456, Item(11994, 2), 11955, 84, 55.0, "N/A", "lava scale shards x2"),
		EXTENDED_ANTIFIRE_1(-1, 2458, Item(11994, 1), 11957, 84, 27.5, "N/A", "lava scale shards x1"),
		STAMINA_4(-1, 3016, Item(12640, 4), 12625, 77, 104.0, "N/A", "amylase crystal x4"),
		STAMINA_3(-1, 3018, Item(12640, 3), 12627, 77, 76.5, "N/A", "amylase crystal x3"),
		STAMINA_2(-1, 3020, Item(12640, 2), 12629, 77, 51.0, "N/A", "amylase crystal x2"),
		STAMINA_1(-1, 3022, Item(12640, 1), 12631, 77, 25.5, "N/A", "amylase crystal1"),
		ANTI_VENOM_FOUR_DOSE(-1, 5952, Item(12934, 20), 12905, 87, 120.0, "Antidote++", "Zulrah's scales x20"),
		ANTI_VENOM_THREE_DOSE(-1, 5954, Item(12934, 15), 12907, 87, 90.0, "Antidote++", "Zulrah's scales x15"),
		ANTI_VENOM_TWO_DOSE(-1, 5956, Item(12934, 10), 12909, 87, 60.0, "Antidote++", "Zulrah's scales x10"),
		ANTI_VENOM_ONE_DOSE(-1, 5958, Item(12934, 5), 12911, 87, 30.0, "Antidote++", "Zulrah's scales x5"),
		ANTI_VENOM_PLUS_THREE(-1, 12907, Item(269), 12915, 94, 125.0, "Torstol", "torstol"),
		ANTI_VENOM_PLUS_FOUR(-1, 12905, Item(269), 12913, 94, 125.0, "Torstol", "torstol");
		
		open fun requiredUnfinished(): Int = 1
	}
	
	val VIAL_OF_WATER = 227
	val COCONUT_MILK = 5935
	val SCALE = 11992
	val SHARD = 11994
	val CRYSTAL = 12640
	
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		reload(repo)
	}
	
	// So it can be hot reloaded from JS
	@JvmStatic fun reload(repo: ScriptRepository) {
		// ALl potion in the enum above
		BrewablePotion.values().forEach { pot ->
			if (pot.herb != -1) {
				val vial = if (pot == BrewablePotion.ANTIDOTE_PLUS || pot == BrewablePotion.ANTIDOTE_PP) COCONUT_MILK else VIAL_OF_WATER
				repo.onItemOnItem(pot.herb.toLong(), vial.toLong(), s@ @Suspendable {
					
					if (it.player().world().realm().isPVP
							&& !it.player.privilege().eligibleTo(Privilege.ADMIN)) { // Stop people making staminas/super combats and selling to store.
						it.messagebox("You can't use this skill on the PvP server.")
						return@s
					}
					
					if (it.player().skills()[Skills.HERBLORE] < pot.level) {
						it.messagebox("You need level ${pot.level} Herblore to make this potion.")
						return@s
					}
					
					var num = it.itemOptions(Item(pot.unfinished, 150))
					
					while (num-- > 0) {
						if (pot.herb !in it.player().inventory() || vial !in it.player().inventory()) {
							break
						}
						
						if (BonusContent.isActive(it.player, BlessingGroup.SUNS_BOUNTY) && it.player.world().rollDie(2, 1)) {
							it.message("The active Sun's Bounty blessing lets you conserve the herb.")
						} else {
							it.player().inventory() -= pot.herb
						}
						
						it.player().inventory() -= vial
						it.player().inventory() += pot.unfinished
						it.player().sound(2608, 0)
						it.player().animate(363)
						
						if (vial == VIAL_OF_WATER) {
							it.message("You put the ${pot.putName} into the vial of water.")
						} else {
							it.message("You put the ${pot.putName} into the coconut milk.")
						}
						
						it.delay(1)
					}
				})
			}
			
			repo.onItemOnItem(pot.unfinished.id().toLong(), pot.secondary.toLong(), s@ @Suspendable {
				if (it.player().skills()[Skills.HERBLORE] < pot.level) {
					it.messagebox("You need level ${pot.level} Herblore to make this potion.")
					return@s
				}
				
				var num = it.itemOptions(Item(pot.result, 150))
				
				while (num-- > 0) {
					if (pot.unfinished !in it.player().inventory() || pot.secondary !in it.player().inventory()) {
						break
					}
					
					if (BonusContent.isActive(it.player, BlessingGroup.SUNS_BOUNTY) && it.player.world().rollDie(2, 1)) {
						it.message("The active Sun's Bounty blessing lets you conserve the ingredient.")
					} else {
						it.player().inventory() -= pot.secondary
					}
					
					it.player().inventory() -= pot.unfinished
					it.player().inventory() += pot.result
					it.player().sound(2608, 0)
					it.player().animate(363)
					
					it.message("You mix the ${pot.ingredName} into your potion.")
					it.addXp(Skills.HERBLORE, pot.xp)
					
					it.delay(2)
				}
			})
			
			// Adding a secondary ingredient to a vial does nothing nice.
			repo.onItemOnItem(pot.secondary.toLong(), VIAL_OF_WATER.toLong()) {
				it.message("You're not sure what effect this might have.")
			}
			
			// Emptying vials
			repo.onItemOption4(pot.unfinished.id()) {
				val slot: Int = it.player()[AttributeKey.ITEM_SLOT]
				it.player().inventory().remove(Item(pot.unfinished), true, slot)
				it.player().inventory().add(Item(229), true, slot)
				it.message("You empty the vial.")
			}
		}
	}
	
}