package nl.bartpelle.veteres.content.areas.lumbridge

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/5/2015.
 */

object LummyGates {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Opening gates
		intArrayOf(1558, 1560).forEach { o ->
			r.onObject(o) {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				it.sound(67)
				if (obj.tile().equals(3253, 3266) || obj.tile().equals(3253, 3267)) {
					openEastCowGate(it)
				} else if (obj.tile().equals(3236, 3296) || obj.tile().equals(3236, 3295)) {
					openEastChickenGate(it)
				} else if (obj.tile().equals(3241, 3301) || obj.tile().equals(3241, 3302)) {
					openPotatoFieldGate(it)
				} else if (obj.tile().equals(3236, 3284) || obj.tile().equals(3236, 3285)) {
					openEastChickenGate2(it)
				} else if (obj.tile().equals(3198, 3282) || obj.tile().equals(3197, 3282)) {
					openWestCowGate(it)
				} else if (obj.tile().equals(3181, 3289) || obj.tile().equals(3180, 3289)) {
					openWestChickenGate(it)
				} else if (obj.tile().equals(3163, 3290) || obj.tile().equals(3162, 3290)) {
					openWestWheatGate(it)
				} else if (obj.tile().equals(3145, 3291) || obj.tile().equals(3146, 3291)) {
					openWestPotatoGate(it)
				} else if (obj.tile().equals(3261, 3321) || obj.tile().equals(3262, 3321)) {
					openNorthEastPotatoGate(it)
				} else if (obj.tile().equals(3080, 3501) || obj.tile().equals(3079, 3501)) {
					openEdgevilleGate(it)
				}
			}
		}
		intArrayOf(12986, 12987).forEach { o ->
			r.onObject(o) {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				it.sound(67)
				if (obj.tile().equals(3213, 3261) || obj.tile().equals(3213, 3262)) {
					openWestSheepGate(it)
				} else if (obj.tile().equals(3188, 3279) || obj.tile().equals(3189, 3279)) {
					openWestFarmGate(it)
				} else if (obj.tile().equals(3186, 3268) || obj.tile().equals(3186, 3269)) {
					openWestCropGate(it)
				}
			}
		}
		
		// Closing gates
		intArrayOf(1559, 1567).forEach { o ->
			r.onObject(o) {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				it.sound(66)
				if (obj.tile().equals(3251, 3266) || obj.tile().equals(3252, 3266)) {
					closeEastCowGate(it)
				} else if (obj.tile().equals(3237, 3296) || obj.tile().equals(3238, 3296)) {
					closeEastChickenGate(it)
				} else if (obj.tile().equals(3239, 3301) || obj.tile().equals(3240, 3301)) {
					closePotatoFieldGate(it)
				} else if (obj.tile().equals(3237, 3285) || obj.tile().equals(3238, 3285)) {
					closeEastChickenGate2(it)
				} else if (obj.tile().equals(3198, 3281) || obj.tile().equals(3198, 3280)) {
					closeWestCowGate(it)
				} else if (obj.tile().equals(3181, 3288) || obj.tile().equals(3181, 3287)) {
					closeWestChickenGate(it)
				} else if (obj.tile().equals(3163, 3289) || obj.tile().equals(3163, 3288)) {
					closeWestWheatGate(it)
				} else if (obj.tile().equals(3145, 3292) || obj.tile().equals(3145, 3293)) {
					closeWestPotatoGate(it)
				} else if (obj.tile().equals(3261, 3322) || obj.tile().equals(3261, 3323)) {
					closeNorthEastPotatoGate(it)
				} else if (obj.tile().equals(3080, 3500) || obj.tile().equals(3080, 3499)) {
					closeEdgevilleGate(it)
				}
			}
		}
		
		intArrayOf(12988, 12989).forEach { o ->
			r.onObject(o) {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				it.sound(66)
				if (obj.tile().equals(3212, 3261) || obj.tile().equals(3211, 3261)) {
					closeWestSheepGate(it)
				} else if (obj.tile().equals(3188, 3280) || obj.tile().equals(3188, 3281)) {
					closeWestFarmGate(it)
				} else if (obj.tile().equals(3185, 3268) || obj.tile().equals(3184, 3268)) {
					closeWestCropGate(it)
				}
			}
		}
	}
	
	fun openNorthEastPotatoGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3261, 3321), 1558, 0, 1))
		world.removeObj(MapObj(Tile(3262, 3321), 1560, 0, 1))
		world.spawnObj(MapObj(Tile(3261, 3322), 1559, 0, 0))
		world.spawnObj(MapObj(Tile(3261, 3323), 1567, 0, 0))
	}
	
	fun closeNorthEastPotatoGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3261, 3321), 1558, 0, 1))
		world.spawnObj(MapObj(Tile(3262, 3321), 1560, 0, 1))
		world.removeObj(MapObj(Tile(3261, 3322), 1559, 0, 0))
		world.removeObj(MapObj(Tile(3261, 3323), 1567, 0, 0))
	}
	
	fun openWestPotatoGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3145, 3291), 1558, 0, 1))
		world.removeObj(MapObj(Tile(3146, 3291), 1560, 0, 1))
		world.spawnObj(MapObj(Tile(3145, 3292), 1559, 0, 0))
		world.spawnObj(MapObj(Tile(3145, 3293), 1567, 0, 0))
	}
	
	fun closeWestPotatoGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3145, 3291), 1558, 0, 1))
		world.spawnObj(MapObj(Tile(3146, 3291), 1560, 0, 1))
		world.removeObj(MapObj(Tile(3145, 3292), 1559, 0, 0))
		world.removeObj(MapObj(Tile(3145, 3293), 1567, 0, 0))
	}
	
	fun openWestWheatGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3163, 3290), 1558, 0, 3))
		world.removeObj(MapObj(Tile(3162, 3290), 1560, 0, 3))
		world.spawnObj(MapObj(Tile(3163, 3289), 1559, 0, 2))
		world.spawnObj(MapObj(Tile(3163, 3288), 1567, 0, 2))
	}
	
	fun closeWestWheatGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3163, 3290), 1558, 0, 3))
		world.spawnObj(MapObj(Tile(3162, 3290), 1560, 0, 3))
		world.removeObj(MapObj(Tile(3163, 3289), 1559, 0, 2))
		world.removeObj(MapObj(Tile(3163, 3288), 1567, 0, 2))
	}
	
	fun openWestCropGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3186, 3268), 12986, 0, 0))
		world.removeObj(MapObj(Tile(3186, 3269), 12987, 0, 0))
		world.spawnObj(MapObj(Tile(3185, 3268), 12988, 0, 3))
		world.spawnObj(MapObj(Tile(3184, 3268), 12989, 0, 3))
	}
	
	fun closeWestCropGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3186, 3268), 12986, 0, 0))
		world.spawnObj(MapObj(Tile(3186, 3269), 12987, 0, 0))
		world.removeObj(MapObj(Tile(3185, 3268), 12988, 0, 3))
		world.removeObj(MapObj(Tile(3184, 3268), 12989, 0, 3))
	}
	
	fun openWestFarmGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3188, 3279), 12986, 0, 1))
		world.removeObj(MapObj(Tile(3189, 3279), 12987, 0, 1))
		world.spawnObj(MapObj(Tile(3188, 3280), 12988, 0, 0))
		world.spawnObj(MapObj(Tile(3188, 3281), 12989, 0, 0))
	}
	
	fun closeWestFarmGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3188, 3279), 12986, 0, 1))
		world.spawnObj(MapObj(Tile(3189, 3279), 12987, 0, 1))
		world.removeObj(MapObj(Tile(3188, 3280), 12988, 0, 0))
		world.removeObj(MapObj(Tile(3188, 3281), 12989, 0, 0))
	}
	
	fun openWestChickenGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3181, 3289), 1558, 0, 3))
		world.removeObj(MapObj(Tile(3180, 3289), 1560, 0, 3))
		world.spawnObj(MapObj(Tile(3181, 3288), 1559, 0, 2))
		world.spawnObj(MapObj(Tile(3181, 3287), 1567, 0, 2))
	}
	
	fun closeWestChickenGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3181, 3289), 1558, 0, 3))
		world.spawnObj(MapObj(Tile(3180, 3289), 1560, 0, 3))
		world.removeObj(MapObj(Tile(3181, 3288), 1559, 0, 2))
		world.removeObj(MapObj(Tile(3181, 3287), 1567, 0, 2))
	}
	
	fun openEastCowGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3253, 3266), 1558, 0, 0))
		world.removeObj(MapObj(Tile(3253, 3267), 1560, 0, 0))
		world.spawnObj(MapObj(Tile(3252, 3266), 1559, 0, 3))
		world.spawnObj(MapObj(Tile(3251, 3266), 1567, 0, 3))
	}
	
	fun closeEastCowGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3253, 3266), 1558, 0, 0))
		world.spawnObj(MapObj(Tile(3253, 3267), 1560, 0, 0))
		world.removeObj(MapObj(Tile(3252, 3266), 1559, 0, 3))
		world.removeObj(MapObj(Tile(3251, 3266), 1567, 0, 3))
	}
	
	fun openWestCowGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3198, 3282), 1558, 0, 3))
		world.removeObj(MapObj(Tile(3197, 3282), 1560, 0, 3))
		world.spawnObj(MapObj(Tile(3198, 3281), 1559, 0, 2))
		world.spawnObj(MapObj(Tile(3198, 3280), 1567, 0, 2))
	}
	
	fun closeWestCowGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3198, 3282), 1558, 0, 3))
		world.spawnObj(MapObj(Tile(3197, 3282), 1560, 0, 3))
		world.removeObj(MapObj(Tile(3198, 3281), 1559, 0, 2))
		world.removeObj(MapObj(Tile(3198, 3280), 1567, 0, 2))
	}
	
	fun openEastChickenGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3236, 3295), 1560, 0, 2))
		world.removeObj(MapObj(Tile(3236, 3296), 1558, 0, 2))
		world.spawnObj(MapObj(Tile(3237, 3296), 1559, 0, 1))
		world.spawnObj(MapObj(Tile(3238, 3296), 1567, 0, 1))
	}
	
	fun openEastChickenGate2(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3236, 3284), 1560, 0, 2))
		world.removeObj(MapObj(Tile(3236, 3285), 1558, 0, 2))
		world.spawnObj(MapObj(Tile(3237, 3285), 1559, 0, 1))
		world.spawnObj(MapObj(Tile(3238, 3285), 1567, 0, 1))
	}
	
	fun openWestSheepGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3213, 3261), 12986, 0, 0))
		world.removeObj(MapObj(Tile(3213, 3262), 12987, 0, 0))
		world.spawnObj(MapObj(Tile(3212, 3261), 12988, 0, 3))
		world.spawnObj(MapObj(Tile(3211, 3261), 12989, 0, 3))
	}
	
	fun closeWestSheepGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3213, 3261), 12986, 0, 0))
		world.spawnObj(MapObj(Tile(3213, 3262), 12987, 0, 0))
		world.removeObj(MapObj(Tile(3212, 3261), 12988, 0, 3))
		world.removeObj(MapObj(Tile(3211, 3261), 12989, 0, 3))
	}
	
	fun closeEastChickenGate2(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3236, 3284), 1560, 0, 2))
		world.spawnObj(MapObj(Tile(3236, 3285), 1558, 0, 2))
		world.removeObj(MapObj(Tile(3237, 3285), 1559, 0, 1))
		world.removeObj(MapObj(Tile(3238, 3285), 1567, 0, 1))
	}
	
	fun closeEastChickenGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3236, 3295), 1560, 0, 2))
		world.spawnObj(MapObj(Tile(3236, 3296), 1558, 0, 2))
		world.removeObj(MapObj(Tile(3237, 3296), 1559, 0, 1))
		world.removeObj(MapObj(Tile(3238, 3296), 1567, 0, 1))
	}
	
	fun openPotatoFieldGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3241, 3301), 1558, 0, 0))
		world.removeObj(MapObj(Tile(3241, 3302), 1560, 0, 0))
		world.spawnObj(MapObj(Tile(3239, 3301), 1567, 0, 3))
		world.spawnObj(MapObj(Tile(3240, 3301), 1559, 0, 3))
	}
	
	fun closePotatoFieldGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3241, 3301), 1558, 0, 0))
		world.spawnObj(MapObj(Tile(3241, 3302), 1560, 0, 0))
		world.removeObj(MapObj(Tile(3239, 3301), 1567, 0, 3))
		world.removeObj(MapObj(Tile(3240, 3301), 1559, 0, 3))
	}
	
	fun openEdgevilleGate(s: Script) {
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3080, 3501), 1558, 0, 3))
		world.removeObj(MapObj(Tile(3079, 3501), 1560, 0, 3))
		world.spawnObj(MapObj(Tile(3080, 3500), 1559, 0, 2))
		world.spawnObj(MapObj(Tile(3080, 3499), 1567, 0, 2))
	}
	
	fun closeEdgevilleGate(s: Script) {
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3080, 3501), 1558, 0, 3))
		world.spawnObj(MapObj(Tile(3079, 3501), 1560, 0, 3))
		world.removeObj(MapObj(Tile(3080, 3500), 1559, 0, 2))
		world.removeObj(MapObj(Tile(3080, 3499), 1567, 0, 2))
	}
	
	
}