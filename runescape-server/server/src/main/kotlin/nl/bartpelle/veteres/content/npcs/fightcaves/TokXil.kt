package nl.bartpelle.veteres.content.npcs.fightcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 9/18/2016.
 */

object TokXil {
	
	val RANGED_MAX_DAMAGE = 14
	val MELEE_MAX_DAMAGE = 13
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 14) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.canAttackMelee(npc, target, false))
					if (npc.world().rollDie(2, 1))
						meleeAttack(npc, target)
					else rangedAttack(npc, target)
				else
					rangedAttack(npc, target)
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun meleeAttack(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, npc.world().random(MELEE_MAX_DAMAGE))
		else
			target.hit(npc, 0)
		
		npc.animate(npc.attackAnimation())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun rangedAttack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
		
		npc.face(target)
		npc.animate(2633)
		npc.world().spawnProjectile(npc, target, 443, 56, 36, 30, 12 * tileDist, 20, 5)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE))
			target.hit(npc, npc.world().random(RANGED_MAX_DAMAGE), delay.toInt()).combatStyle(CombatStyle.RANGE)
		else
			target.hit(npc, 0, delay.toInt())
	}
}