package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Situations on 2017-03-12.
 */

object CompetitionJudge {
	
	val COMPETITION_JUDGE = 6070
	val BRONZE_ARROW = 882
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(COMPETITION_JUDGE) @Suspendable {
			val player = it.player()
			val shotsLeft = player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS)
			val shotsFired = player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS)
			
			if (shotsFired == 11) {
				val finalScore = player.varps().varp(Varp.RANGING_GUILD_TARGET_POINTS)
				player.inventory().addOrDrop(Item(1464, (finalScore * 0.1).toInt()), player)
				player.varps().varp(Varp.RANGING_GUILD_TARGET_POINTS, 0)
				player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS, 0)
				Target.talkToJudge(it, "Well done. Your score is: $finalScore. For that score you will<br>receive ${(finalScore * 0.1).toInt()} Archery tickets.", 589)
			} else {
				if (shotsLeft == 0) {
					it.chatNpc("Hello there, would you like to take part in the archery<br>competition? It only costs 200 coins to enter.", COMPETITION_JUDGE, 568)
					when (it.options("Sure, I'll give it a go.", "What are the rules?", "No thanks.")) {
						1 -> enterCompetition(it)
						2 -> {
							it.chatPlayer("What are the rules?", 554)
							it.chatNpc("You're given 10 shots at the targets, for each hit you<br>will receive points. At the end you'll be awarded 1 ticket" +
									"<br>for every 10 points.", COMPETITION_JUDGE, 590)
							it.chatNpc("The tickets can be exchanged for goods from our<br>stores. Do you want to give it a go? Only 200 coins.", COMPETITION_JUDGE, 589)
							when (it.options("Sure, I'll give it a go.", "No thanks.")) {
								1 -> enterCompetition(it)
								2 -> it.chatPlayer("No thanks.", 588)
							}
						}
						3 -> it.chatPlayer("No thanks.", 588)
						
					}
				} else {
					it.chatNpc("Hello again, do you need reminding of the rules?", COMPETITION_JUDGE, 588)
					
					if (!player.inventory().has(BRONZE_ARROW)) {
						it.chatPlayer("Well, I actually don't have any more arrows. Could I<br>get some more?", 589)
						it.chatNpc("Ok, but it'll cost you 100 coins.", COMPETITION_JUDGE, 588)
						when (it.options("Sure, I'll take some.", "No thanks.")) {
							1 -> {
								it.chatPlayer("Sure, I'll take some.", 588)
								if (player.inventory().remove(Item(995, 200), false).success()) {
									player.inventory().add(Item(882, 10), true)
									player.message("You pay the judge and he gives you 10 bronze arrows.")
								} else {
									it.chatPlayer("Oops, I don't have enough coins on me...", 610)
									it.chatNpc("Never mind, come back when you've got enough.", 588)
								}
							}
							2 -> it.chatPlayer("No thanks.", 588)
						}
					} else {
						when (it.options("Yes please.", "No thanks, I've got it.", "How am I doing so far?")) {
							1 -> {
								it.chatPlayer("Yes please.", 588)
								it.chatNpc("The rules are very simple:", COMPETITION_JUDGE, 567)
								it.chatNpc("You're given 10 shots at the targets, for each hit you<br>will receive points. At the end you'll be awarded 1 ticket" +
										"<br>for every 10 points.", COMPETITION_JUDGE, 590)
								it.chatNpc("The tickets can be exchanged for goods from our<br>stores. Good luck!", COMPETITION_JUDGE, 589)
							}
							2 -> {
								it.chatPlayer("No thanks, I've got it.", 588)
								it.chatNpc("Glad to hear it, good luck!", COMPETITION_JUDGE, 567)
							}
							3 -> {
								val currentScore = player.varps().varp(Varp.RANGING_GUILD_TARGET_POINTS)
								it.chatNpc("So far your score is: $currentScore. ", COMPETITION_JUDGE, 567)
							}
						}
					}
				}
			}
		}
	}
	
	@Suspendable fun enterCompetition(it: Script) {
		val player = it.player()
		
		it.chatPlayer("Sure, I'll give it a go.", 567)
		it.chatNpc("Great! That will be 200 coins then please.", COMPETITION_JUDGE, 567)
		if (player.inventory().full()) {
			it.chatPlayer("Oops, I don't have enough inventory space...", 610)
			it.chatNpc("Never mind, come back when you've got enough.", 588)
			return
		}
		
		if (player.inventory().remove(Item(995, 200), false).success()) {
			player.inventory().add(Item(882, 10), true)
			player.message("You pay the judge and he gives you 10 bronze arrows.")
			player.varps().varp(Varp.RANGING_GUILD_TARGET_SHOTS, 1)
		} else {
			it.chatPlayer("Oops, I don't have enough coins on me...", 610)
			it.chatNpc("Never mind, come back when you've got enough.", 588)
		}
	}
	
}
