package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 9/6/15.
 */

object AmuletOfGlory {
	
	enum class GloryAmulet(val id: Int, val charges: Int, val result: Int) {
		SIX(11978, 6, 11976),
		FIVE(11976, 5, 1712),
		FOUR(1712, 4, 1710),
		THREE(1710, 3, 1708),
		TWO(1708, 2, 1706),
		ONE(1706, 1, 1704),
		ETERNAL(19707, -1, -1),
		SIX_TRIM(11964, 6, 11966),
		FIVE_TRIM(11966, 5, 10354),
		FOUR_TRIM(10354, 4, 10356),
		THREE_TRIM(10356, 3, 10358),
		TWO_TRIM(10358, 2, 10360),
		ONE_TRIM(10360, 1, 10362)
		;
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		GloryAmulet.values().forEach { g ->
			r.onItemOption4(g.id) @Suspendable { rub(g.id, it, g.result, g.charges) }
			
			r.onEquipmentOption(1, g.id) @Suspendable { doMagic(g.id, Tile(3087, 3496), it, g.result, g.charges, true) } //Edgeville
			r.onEquipmentOption(2, g.id) @Suspendable { doMagic(g.id, Tile(2918, 3176), it, g.result, g.charges, true) } //Karamja
			r.onEquipmentOption(3, g.id) @Suspendable { doMagic(g.id, Tile(3105, 3251), it, g.result, g.charges, true) } //Draynor
			r.onEquipmentOption(4, g.id) @Suspendable { doMagic(g.id, Tile(3293, 3163), it, g.result, g.charges, true) } //Al Kharid
		}
		
		r.onEquipmentOption(1, 1704) {
			it.player().message("The amulet has lost its charge.")
			it.player().message("It will need to be recharged before you can use it again.")
		}
		
		r.onItemOption4(1704) {
			it.player().message("The amulet has lost its charge.")
			it.player().message("It will need to be recharged before you can use it again.")
		}
	}
	
	@Suspendable fun rub(usedId: Int, script: Script, result: Int, currentCharges: Int) {
		script.message("You rub the amulet...")
		
		when (script.optionsTitled("Where would you like to teleport to?", "Edgeville", "Karamja", "Draynor Village", "Al Kharid", "Nowhere")) {
			1 -> doMagic(usedId, Tile(3087, 3496), script, result, currentCharges)
			2 -> doMagic(usedId, Tile(2918, 3176), script, result, currentCharges)
			3 -> doMagic(usedId, Tile(3105, 3251), script, result, currentCharges)
			4 -> doMagic(usedId, Tile(3293, 3163), script, result, currentCharges)
		}
	}
	
	@Suspendable fun doMagic(usedId: Int, targetTile: Tile, it: Script, result: Int, currentCharges: Int, equipment: Boolean = false) {
		if (!Teleports.canTeleport(it.player(), true, TeleportType.ABOVE_20_WILD)) {
			return
		}
		
		DeadmanMechanics.attemptTeleport(it)
		
		val slot: Int = it.player()[AttributeKey.ITEM_SLOT]
		Teleports.basicTeleport(it, targetTile)
		
		if (usedId != 19707) {
			if (currentCharges == 1)
				it.message("You use your amulet's last charge.".col("7F00FF"))
			else if (currentCharges == 2)
				it.message("Your amulet has one charge left.".col("7F00FF"))
			else
				it.message("Your amulet has ${numToStr(currentCharges - 1)} charges left.".col("7F00FF"))
			
			if (equipment) {
				if (it.player().equipment().remove(Item(usedId), true, EquipSlot.AMULET).success())
					it.player().equipment().add(Item(result), true, EquipSlot.AMULET)
			} else {
				if (it.player().inventory().remove(Item(usedId), true, slot).success())
					it.player().inventory().add(Item(result), true, slot)
			}
		}
		
		it.player().timers().cancel(TimerKey.FROZEN)
		it.player().timers().cancel(TimerKey.REFREEZE)
		it.player().write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
	}
	
	fun numToStr(num: Int): String = when (num) {
		2 -> "two"
		3 -> "three"
		4 -> "four"
		5 -> "five"
		6 -> "six"
		7 -> "seven"
		else -> "?"
	}
}