package nl.bartpelle.veteres.content.skills.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.items.combine.SlayerHelm
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 11/14/2015.
 */
object SlayerGem {
	
	val SLAYER_GEM = 4155
	val SLAYER_HELMS = intArrayOf(11864, SlayerHelm.RED_SLAYER_HELM, SlayerHelm.GREEN_SLAYER_HELM, SlayerHelm.BLACK_SLAYER_HELM,
			11865, 19641, 19645, 19649, SlayerHelm.PURPLE_SLAYER_HELM, SlayerHelm.PURPLE_HELM_IMBUE)
	val SLAYER_RING = 11866
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(SLAYER_GEM) { it.message("The activate feature is currently not available.") }
		r.onItemOption2(SLAYER_GEM) { checkSlayerStatus(it.player()) } // Slayer gem
		r.onItemOption3(SLAYER_GEM) { it.message("The partner feature is currently not available.") } // PARTNER OPTION
		r.onItemOption4(SLAYER_GEM) { sendLog(it.player()) } // // LOG OPTION
		
		
		r.onItemOption4(SLAYER_RING) { checkSlayerStatus(it.player()) } // Slayer ring
		r.onEquipmentOption(1, SLAYER_RING) { checkSlayerStatus(it.player()) } // Slayer ring CHECK OPTION
		r.onEquipmentOption(3, SLAYER_RING) { it.message("The master feature is currently not available.") } // MASTER OPTION
		r.onEquipmentOption(4, SLAYER_RING) { it.message("The partner feature is currently not available.") } // PARTNER OPTION
		r.onEquipmentOption(5, SLAYER_RING) { sendLog(it.player()) } // // LOG OPTION
		
		for (i in SLAYER_HELMS) {
			r.onEquipmentOption(3, i) { checkSlayerStatus(it.player()) } // Worn slayer helm
			r.onItemOption3(i) { checkSlayerStatus(it.player()) } // Slayer helm
			r.onEquipmentOption(4, i) { it.message("The partner feature is currently not available.") } // partner
			r.onEquipmentOption(5, i) { sendLog(it.player()) } // log
		}
	}
	
	fun checkSlayerStatus(player: Player) {
		val task: SlayerCreature? = SlayerCreature.lookup(player.attribOr(AttributeKey.SLAYER_TASK_ID, 0))
		val num: Int = player.attribOr(AttributeKey.SLAYER_TASK_AMT, 0)
		
		if (task != null) {
			if (num > 0) {
				player.message("You're assigned to kill ${Slayer.taskName(player, task.uid)}; only $num more to go.")
			} else {
				player.message("You need something new to hunt.")
			}
		} else {
			player.message("You need something new to hunt.")
		}
	}
	
	private val kcs = arrayOf<AttributeKey>(AttributeKey.KC_CRAWL_HAND, AttributeKey.KC_CAVE_BUG, AttributeKey.KC_CAVE_CRAWLER, AttributeKey.KC_BANSHEE,
			AttributeKey.KC_CAVE_SLIME, AttributeKey.KC_ROCKSLUG, AttributeKey.KC_DESERT_LIZARD, AttributeKey.KC_COCKATRICE, AttributeKey.KC_PYREFRIEND,
			AttributeKey.KC_MOGRE, AttributeKey.KC_HARPIE_BUG, AttributeKey.KC_WALL_BEAST, AttributeKey.KC_KILLERWATT, AttributeKey.KC_MOLANISK,
			AttributeKey.KC_BASILISK, AttributeKey.KC_SEASNAKE, AttributeKey.KC_TERRORDOG, AttributeKey.KC_FEVER_SPIDER, AttributeKey.KC_INFERNAL_MAGE,
			AttributeKey.KC_BRINERAT, AttributeKey.KC_BLOODVELD, AttributeKey.KC_JELLY, AttributeKey.KC_TUROTH, AttributeKey.KC_ZYGOMITE, AttributeKey.KC_CAVEHORROR,
			AttributeKey.KC_ABERRANT_SPECTRE, AttributeKey.KC_SPIRITUAL_RANGER, AttributeKey.KC_DUSTDEVIL, AttributeKey.KC_SPIRITUAL_WARRIOR, AttributeKey.KC_KURASK,
			AttributeKey.KC_SKELETAL_WYVERN, AttributeKey.KC_GARGOYLE, AttributeKey.KC_NECHRYAEL, AttributeKey.KC_SPIRITUAL_MAGE, AttributeKey.KC_ABYSSALDEMON,
			AttributeKey.KC_CAVEKRAKEN, AttributeKey.KC_DARKBEAST, AttributeKey.KC_SMOKEDEVIL)
	
	private val monsterNames = arrayOf("Crawling hands", "Cave bugs", "Cave crawlers", "Banshees", "Cave slime", "Rockslugs",
			"Desert lizards", "Cockatrice", "Pyrefiends", "Mogres", "Harpie bug swarm", "Wall beasts", "Killerwatts", "Molanisks",
			"Basilisks", "Sea snakes", "Terror dogs", "Fever spiders", "Infernal mages", "Brine rats", "Bloodvelds", "Jellies",
			"Turoth", "Mutated zygomites", "Cave horrors", "Aberrant spectres", "Spiritual rangers", "Dust devils", "Spiritual warriors",
			"Kurask", "Skeletal wyverns", "Gargoyles", "Nechryael", "Spiritual mages", "Abyssal demons", "Cave kraken", "Dark beasts",
			"Smoke devils")
	
	@Suspendable fun sendLog(player: Player) {
		player.interfaces().sendMain(549)
		
		if (kcs.size != monsterNames.size) {
			player.message("info mismatch"); return
		}
		
		player.write(InvokeScript(1584, monsterNames.joinToString("|"), kcs.joinToString("|", transform = { player.attribOr<Int>(it, 0).toString() }),
				//TODO streaks
				"0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|0|", 39, "Slayer Kill Log"))
	}
	
}