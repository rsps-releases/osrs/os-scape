package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/11/2015.
 */

object VarrockSwordShopkeeper {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (SHOP_KEEPER in intArrayOf(537, 538)) {
			r.onNpcOption1(SHOP_KEEPER) @Suspendable {
				it.chatNpc("Hello, bold adventurer! Can I interest you in some<br>swords?", SHOP_KEEPER, 568)
				when (it.options("Yes, please!", "No, I'm okay for swords right now.")) {
					1 -> {
						if (it.player().ironMode() != IronMode.NONE) {
							it.player().world().shop(12).display(it.player())
						} else {
							it.player().world().shop(5).display(it.player())
						}
					}
					2 -> {
						it.chatPlayer("No, I'm okay for swords right now.")
						it.chatNpc("Come back if you need any.", SHOP_KEEPER)
					}
				}
			}
			r.onNpcOption2(SHOP_KEEPER) {
				if (it.player().ironMode() != IronMode.NONE) {
					it.player().world().shop(12).display(it.player())
				} else {
					it.player().world().shop(5).display(it.player())
				}
			}
		}
	}
	
}
