package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.wilderness.CommandTeleportPrompt
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands
import java.text.NumberFormat

/**
 * Created by Situations on 11/16/2015.
 */

object EdgevilleWizard {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Talk-to the wizards
		for (wizards in intArrayOf()) {
			r.onNpcOption1(wizards) @Suspendable {
				if (!it.player().world().realm().isPVP) {
					it.chatNpc("Good day to you, traveller. How can I assist you today?", wizards, 567)
					when (it.options("Who are you?", "Could you teleport me somewhere?")) {
						1 -> {
							it.chatPlayer("Who are you?")
							it.chatNpc("Well.. I'm a wizard! I'm here to assist an <br>adventurer such as yourself to travel around OS-Scape. Are you interested in traveling anywhere, ${it.player().name()}?", wizards, 567)
							when (it.options("Yes please!", "I'd rather walk.", "What do you mean by.. traveling anywhere?")) {
								1 -> {
									it.chatPlayer("Yes please!", 590)
									it.chatNpc("Okay, where would you like to go?", wizards, 567)
									options(it, wizards)
								}
								2 -> {
									it.chatPlayer("I'd rather walk, thanks for the offer, though.", 571)
									it.chatNpc("Suit yourself!", wizards, 590)
								}
								3 -> {
									it.chatPlayer("What do you mean by.. traveling anywhere?")
									it.chatNpc("Whether it's training your skills, playing a <br>minigame, or slaying a dangerous boss, I can take you there! Unfortunately, I didn't have enough time to learn all the teleportation spells.. but I did learn the ones", wizards, 590)
									it.chatNpc("that are most popular around OS-Scape. So what do you say? Would you like me to take you somewhere?", wizards, 567)
									when (it.options("Yes please!", "I'd rather walk.")) {
										1 -> {
											it.chatPlayer("Yes please!")
											it.chatNpc("Okay, where would you like to go?", wizards, 567)
											options(it, wizards)
										}
										2 -> {
											it.chatPlayer("I'd rather walk, thanks for the offer, though.", 571)
											it.chatNpc("Suit yourself!", wizards, 590)
										}
									}
								}
							}
						}
						2 -> {
							it.chatPlayer("Could you teleport me somewhere?", 588)
							it.chatNpc("Absolutely! Where would you like to go?", wizards, 567)
							options(it, wizards)
						}
					}
				} else {
					it.chatNpc("I don't offer any teleports. Please check back with me tomorrow.", wizards, 567)
				}
			}
			//Skilling teleports
			r.onNpcOption2(wizards) @Suspendable {
				if (!it.player().world().realm().isPVP) {
					skillingTeleport(it, false)
				} else {
					it.chatNpc("I don't offer any teleports. Please check back with me tomorrow.", wizards, 567)
				}
			}
			//Minigame teleports
			r.onNpcOption3(wizards) @Suspendable {
				if (!it.player().world().realm().isPVP) {
					minigameTeleports(it, false)
				} else {
					it.chatNpc("I don't offer any teleports. Please check back with me tomorrow.", wizards, 567)
				}
			}
			//Monster teleports
			r.onNpcOption4(wizards) @Suspendable {
				if (!it.player().world().realm().isPVP) {
					monsterTeleports(it, false)
				} else {
					it.chatNpc("I don't offer any teleports. Please check back with me tomorrow.", wizards, 567)
				}
			}
		}
	}
	
	@Suspendable fun options(it: Script, wizards: Int) {
		when (it.options("Skilling Locations", "Minigame Locations", "Monster Locations")) {
			1 -> {
				it.chatPlayer("What kind of Skilling teleports do you know?")
				it.chatNpc("I know a few! Here, take a look.", wizards, 567)
				skillingTeleport(it, false)
			}
			2 -> {
				it.chatPlayer("What kind of Minigame teleports do you know?")
				it.chatNpc("I know a few! Here, take a look.", wizards, 567)
				minigameTeleports(it, false)
			}
			3 -> {
				it.chatPlayer("What kind of Monster teleports do you know?")
				it.chatNpc("I know a few! Here, take a look.", wizards, 567)
				monsterTeleports(it, false)
			}
		}
	}
	
	@JvmStatic @Suspendable fun home_teleport_options(it: Script) {
		when (it.options("Skilling Locations", "Minigame Locations", "Monster Locations")) {
			1 -> {
				skillingTeleport(it, true)
			}
			2 -> {
				minigameTeleports(it, true)
			}
			3 -> {
				monsterTeleports(it, true)
			}
		}
	}
	
	@Suspendable fun minigameTeleports(it: Script, quick_cast: Boolean) {
		when (it.options("Warriors' Guild", "Barrows")) {
			1 -> teleport(it, 2879, 3546, 0, quick_cast)
			2 -> teleport(it, 3565, 3314, 0, quick_cast)
		}
	}
	
	@Suspendable fun skillingTeleport(it: Script, quick_cast: Boolean) {
		when (it.options("Gnome Stronghold Agility", "Dwarven Mine", "Ardougne Stalls", "Seers' Village Docks", "More Skilling Teleports")) {
			1 -> teleport(it, 2473, 3439, 0, quick_cast)
			2 -> teleport(it, 3016, 3446, 0, quick_cast)
			3 -> teleport(it, 2662 + it.player().world().random(3), 3306 + it.player().world().random(3), 0, quick_cast)
			4 -> teleport(it, 2835 + it.player().world().random(1), 3435 + it.player().world().random(1), 0, quick_cast)
			5 -> {
				when (it.options("Slayer Locations", "Zeah", "Altar Locations", "Piscatoris (Monkfishing)", "Woodcutting Guild")) {
					1 -> {
						when (it.options("Canifis", "Zanaris")) {
							1 -> teleport(it, 3508 + it.player().world().random(2), 3506 + it.player().world().random(2), 0, quick_cast)
							2 -> teleport(it, 2449, 4440, 0, quick_cast)
						}
					}
					2 -> teleport(it, 1782 + it.player().world().random(2), 3688 + it.player().world().random(2), 0, quick_cast)
					3 -> {
						when (it.options("Cosmic altar", "Astral altar", "Nature altar", "Law altar", "Death altar")) {
							1 -> teleport(it, 2410, 4377, 0, quick_cast)
							2 -> teleport(it, 2156, 3863, 0, quick_cast)
							3 -> teleport(it, 2868, 3017, 0, quick_cast)
							4 -> teleport(it, 2860, 3381, 0, quick_cast)
							5 -> teleport(it, 1862, 4639, 0, quick_cast)
						}
					}
					4 -> teleport(it, 2337 + it.player().world().random(2), 3683 + it.player().world().random(2), 0, quick_cast)
					5 -> teleport(it, 1662 + it.player().world().random(2), 3507 + it.player().world().random(2), 0, quick_cast)
				}
			}
		}
	}
	
	@Suspendable fun monsterTeleports(it: Script, quick_cast: Boolean) {
		when (it.options("Basic Teleports", "Dungeon Teleports", "Boss Teleports")) {
			1 -> {
				when (it.options("Crabs", "Yaks", "Goblin Village", "Falador Farm", "Experiments")) {
					1 -> {
						when (it.options("Rock Crabs", "Sand Crabs")) {
							1 -> {
								when (it.options("East Relleka", "West Relleka")) {
									1 -> teleport(it, 2706 + it.player().world().random(2), 3718 + it.player().world().random(2), 0, quick_cast)
									2 -> teleport(it, 2668 + it.player().world().random(2), 3712 + it.player().world().random(2), 0, quick_cast)
								}
							}
							2 -> {
								when (it.options("East Zeah", "West Zeah")) {
									1 -> teleport(it, 1812 + it.player().world().random(2), 3471 + it.player().world().random(2), 0, quick_cast)
									2 -> teleport(it, 1629 + it.player().world().random(2), 3472 + it.player().world().random(2), 0, quick_cast)
								}
							}
						}
					}
					2 -> teleport(it, 2322 + it.player().world().random(3), 3794 + it.player().world().random(3), 0, quick_cast)
					3 -> teleport(it, 2956 + it.player().world().random(1), 3501 + it.player().world().random(1), 0, quick_cast)
					4 -> teleport(it, 3033 + it.player().world().random(1), 3285 + it.player().world().random(1), 0, quick_cast)
					5 -> teleport(it, 3561 + it.player().world().random(2), 9947 + it.player().world().random(2), 0, quick_cast)
				}
			}
			2 -> {
				when (it.options("Fremennik Slayer Dungeon", "Edgeville Dungeon", "Waterfall Dungeon", "Asgarnian Ice Dungeon", "More Dungeon Teleports")) {
					1 -> teleport(it, 2794, 3615, 0, quick_cast)
					2 -> teleport(it, 3096, 9867, 0, quick_cast)
					3 -> teleport(it, 2511, 3463, 0, quick_cast)
					4 -> teleport(it, 3011, 3150, 0, quick_cast)
					5 -> {
						when (it.options("Mos Le'Harmless Caves", "Ancient Cavern", "Taverley", "Smoke Dungeon", "More Dungeon Teleports")) {
							1 -> teleport(it, 3748, 9373, 0, quick_cast)
							2 -> teleport(it, 1741, 5316, 1, quick_cast)
							3 -> teleport(it, 2884, 9798, 0, quick_cast)
							4 -> teleport(it, 3205, 9378, 0, quick_cast)
							5 -> {
								when (it.options("God Wars Dungeon", "Tzhaar Cave", "Stronghold Slayer Cave", "Mourner Tunnels", "More Dungeon Teleports")) {
									1 -> teleport(it, 2914, 3735, 0, quick_cast)
									2 -> teleport(it, 2480, 5167, 0, quick_cast)
									3 -> teleport(it, 2434, 3423, 0, quick_cast)
									4 -> teleport(it, 2026, 4636, 0, quick_cast)
									5 -> {
										when (it.options("Monkey Madness II Dungeon", "Previous Teleports")) {
											1 -> teleport(it, 2435, 3518, 0, quick_cast)
											2 -> monsterTeleports(it, quick_cast)
										}
									}
								}
							}
						}
					}
				}
			}
			3 -> {
				when (it.options("Dagannoth Kings", "Callisto (Wilderness)", "Venenatis (Wilderness)", "Chaos Fanatic (Wilderness)", "More")) {
					1 -> teleportPaid(it, 1910, 4367, 0, 12500)
					2 -> {
						teleportPaid(it, 3279, 3847, 0, 7500)
						it.message("Head east to fight Callisto.")
					}
					3 -> {
						teleportPaid(it, 3324, 3741, 0, 9500)
						it.message("Head east to fight Venenatis.")
					}
					4 -> {
						teleportPaid(it, 2981, 3836, 0, 9000)
						it.message("Head north to fight the Chaos Fanatic.")
					}
					5 -> {
						when (it.options("Crazy Archaeologist (Wilderness)", "Corporeal Beast", "Scorpia (Wilderness)", "Zulrah", "More")) {
							1 -> {
								teleportPaid(it, 2968, 3696, 0, 6500)
								it.message("Head north-east to fight the Crazy Archaeologist.")
							}
							2 -> teleportPaid(it, 2968, 4383, 2, 15000)
							3 -> teleportPaid(it, 3236, 3946, 0, 8000)
							4 -> teleportPaid(it, 2200, 3055, 0, 20000)
							5 -> {
								when (it.options("Lizard Shaman", "God Wars")) {
									1 -> teleportPaid(it, 1461, 3689, 0, 6500)
									2 -> teleport(it, 2914, 3735, 0, false)
								}
							}
						}
					}
				}
			}
			
		}
	}
	
	@Suspendable fun teleportPaid(it: Script, x: Int, z: Int, level: Int, price: Int) {
		if (it.optionsTitled("Pay ${NumberFormat.getInstance().format(price)} gp to teleport?", "Pay.", "Never mind.") == 1) {
			if (it.player().inventory().remove(Item(995, price), false).success()) {
				teleport(it, x, z, level, false)
			} else {
				it.itemBox("You don't have enough coins to teleport.", 1004)
			}
		}
	}
	
	@Suspendable fun teleport(it: Script, x: Int, z: Int, level: Int, quick_cast: Boolean) {
		if (GameCommands.pkTeleportOk(it.player(), x, z)) {
			if (!WildernessLevelIndicator.inWilderness(it.player().tile()) && WildernessLevelIndicator.inWilderness(Tile(x, z, level))) {
				// Graves or Demonic gorillas
				/*if ((x == 3160 && z == 3676) || (x == 3107 && z == 3675)) {
					it.message("<col=FF0000>Warning!</col> The Zamorak Mage at 5 wilderness can no longer be used as an escape if teleblocked.")
					it.messagebox("<col=FF0000>Warning!</col> The Zamorak Mage at 5 wilderness can no longer be used as an escape if teleblocked.")
				}*/
				// Show enter wilderness conformation.
				if (!CommandTeleportPrompt.wantsToEnterWild(it)) {
					return
				}
			}
			if (quick_cast) {
				it.player().lockNoDamage()
				it.delay(1)
				it.player().graphic(1296)
				it.player().animate(3865)
				it.delay(3)
				it.player().teleport(x, z, 0)
				it.animate(-1)
				it.player().unlock()
			} else {
				it.targetNpc()!!.sync().faceEntity(it.player())
				it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
				it.targetNpc()!!.sync().animation(1818, 1)
				it.targetNpc()!!.sync().graphic(343, 100, 1)
				it.delay(1)
				it.player().graphic(1296)
				it.player().animate(3865)
				it.player().lockNoDamage()
				it.delay(3)
				it.player().teleport(x, z, level)
				it.animate(-1)
				it.player().unlock()
			}
		}
	}
}

