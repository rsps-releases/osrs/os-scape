package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 7/29/2017.
 * Meleer that burrows underground to target
 */
object JalImKot {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		val world = npc.world()
		var target = EntityCombat.getTarget(npc) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			
			// the call to canAttackMelee will path this Npc towards its target
			val in_dist = EntityCombat.canAttackMelee(npc, target, true)
			var dug = false
			if (!in_dist) {
				if (world.rollDie(50, 1)) {
					burrow(npc, target, world, it)
					dug = true
				}
			}
			if (in_dist && !dug && EntityCombat.attackTimerReady(npc)) {
				attack(npc, target)
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
				
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun attack(npc: Npc, entity: Entity) {
		
		npc.animate(npc.attackAnimation())
		
		if (EntityCombat.attemptHit(npc, entity, CombatStyle.MELEE)) {
			entity.hit(npc, npc.world().random(npc.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
		} else {
			entity.hit(npc, 0, 1)
		}
	}
	
	/**
	 * Performs the burrow action. This action should only be triggered if they cannot attack their target after X
	 * ticks. Reference to the wiki for more specifics.
	 */
	@Suspendable fun burrow(npc: Npc, entity: Entity, world: World, script: Script) {
		
		npc.animate(7600)
		
		//stop the combat and teleport
		npc.clearattrib(AttributeKey.TARGET)
		npc.stopActions(true)
		npc.lockNoDamage()
		
		script.delay(3)
		// Since we're locked, the default instance deallocation won't interrupt this task.
		if (InfernoContext.get(entity as Player) != null) {
			npc.teleport(entity.tile())
			script.delay(2)
			npc.animate(7601)
			script.delay(2)
			npc.unlock()
			script.delay(1)
			
			//resume attacking
			npc.attack(entity)
		} else {
			// manual unregister
			npc.unlock()
			npc.stopActions(true)
			npc.world().unregisterNpc(npc)
		}
	}
}