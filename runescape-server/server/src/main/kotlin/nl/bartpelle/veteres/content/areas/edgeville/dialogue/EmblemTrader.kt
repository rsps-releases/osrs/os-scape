package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterEmblem
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.Varbit
import java.text.NumberFormat
import java.util.*

/**
 * Created by Situations on 7/20/2016.
 * @author Mack - Revisions and additions.
 */

object EmblemTrader {
	
	const val EMBLEM_TRADER = 315
	const val BLOOD_MONEY = 13307
	const val REVENANT_ETHER = 21820
	const val IMBUED_SARADOMIN_CAPE = 21791
	const val IMBUED_GUTHIX_CAPE = 21793
	const val IMBUED_ZAMORAK_CAPE = 21795
	const val BOUNTY_HUNTER_SHOP_VARP = 1132
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(EMBLEM_TRADER, handler@ @Suspendable {
			it.chatPlayer("Hey..?", 580)
			it.chatNpc("Hello, wanderer.", EMBLEM_TRADER, 588)
			
			var hasEmblem = false
			for (emblem in BountyHunterEmblem.values()) {
				if (it.player().inventory().has(emblem.item())) {
					hasEmblem = true
					break
				}
			}
			
			// Custom piece of code: Revenant Ether can be traded for capes.
			val revenantEther = it.player().inventory().count(REVENANT_ETHER)
			if (revenantEther > 0) {
				it.itemBox("The Emblem Trader takes a slightly obvious look at the Revenant Ether in your inventory.", REVENANT_ETHER, 100)
				it.chatNpc("Revenant Ether, hmm? I see you have some. I've heard a lot about it. Good and bad... I'll take 500<br>of it, and I'll give you one of these capes in return..", EMBLEM_TRADER)
				it.doubleItemBox("The Emblem Trader shows you Imbued God capes - Zamorak, Saradomin and Guthix. For 500 Revenant Ether he'll trade you one.", 21795, 21793)
				
				if (revenantEther >= 500) {
					it.chatNpc("What do you say, ${it.player().name()}, are you interested in exchanging 500 of your Revenant Ether for<br>a cape? You may decide which one, even.", EMBLEM_TRADER)
					
					if (it.optionsTitled("What do you want to do?", "Exchange some Revenant Ether.", "Not now. I want to talk about something else.") == 1) {
						it.chatPlayer("Ok, I'm willing to trade some of my Revenant Ether for an Imbued God cape.")
						it.chatNpc("Certainly then, adventurer. And what may it be?", EMBLEM_TRADER)
						
						val choice = it.optionsTitled("Select a Cape", "Imbued Zamorak cape", "Imbued Saradomin cape", "Imbued Guthix cape", "Nothing, nevermind.")
						if (choice == 4) {
							it.chatPlayer("Nothing, nevermind.")
							it.chatNpc("Sure. If you change your mind, you know where to find me...", EMBLEM_TRADER)
							return@handler
						}
						
						val cape = Item(when (choice) {
							1 -> IMBUED_ZAMORAK_CAPE
							2 -> IMBUED_SARADOMIN_CAPE
							else -> IMBUED_GUTHIX_CAPE
						})
						
						if (it.optionsTitled("Purchase an ${cape.name(it.player().world())}?", "Yes.", "No.") == 1) {
							if (it.player().inventory().full()) {
								it.message("You do not have enough inventory space to purchase a cape.")
								return@handler
							}
							
							if (it.player().inventory().remove(Item(REVENANT_ETHER, 500), false).success()) {
								it.player().inventory().add(cape, true)
								it.chatNpc("Thank you, ${it.player().name()}. I hope it is what you were looking for.", EMBLEM_TRADER)
								return@handler
							}
						}
					}
				}
			}
			
			if (hasEmblem) {
				it.chatNpc("I see you have something valuable on your person. I am interested in certain... ancient emblems, you see. I'll happily take yours of your hands for a handsome fee.", EMBLEM_TRADER)
				
				if (it.optionsTitled("Sell your emblems to the trader?", "Yes", "No") == 1) {
					EmblemTrader.exchange(it)
					it.chatNpc("Thank you. My master in the north will be very pleased.", EMBLEM_TRADER)
				}
			} else {
				it.chatNpc("Don't suppose you've come across any strange... emblems along your journey?", EMBLEM_TRADER)
				it.chatPlayer("Not that I've seen.")
				it.chatNpc("If you do, please do let me know. I'll reward you handsomely.", EMBLEM_TRADER)
			}
			
			initial_options(it)
		})
		
		r.onNpcOption2(EMBLEM_TRADER) @Suspendable { displayShop(it) }
		r.onNpcOption3(EMBLEM_TRADER) @Suspendable { toggleOptions(it) }
		r.onNpcOption4(EMBLEM_TRADER) @Suspendable { toggleOrExtendSkulls(it) }
		
		r.onItemOption1(12846, s@ @Suspendable {
			if (it.player().attribOr(AttributeKey.BOUNTY_HUNTER_TARGET_TELEPORT_UNLOCKED, false)) {
				it.player().message("You have already unlocked the contents this scroll has to offer.")
				return@s
			}
			
			it.itemBox("The 'Teleport to Bounty Target' spell requires <col=FF0000>level 85 Magic</col>, and can be cast only <col=FF0000>within the Wilderness</col>, while you have a Bounty Target. This scroll will be destroyed when it has been read.", 12846)
			if (it.optionsTitled("Are you sure you want to learn the spell?", "Yes, I want to learn the spell.", "No, I'll keep the scroll") == 1) {
				it.player().putattrib(AttributeKey.BOUNTY_HUNTER_TARGET_TELEPORT_UNLOCKED, true)
				it.player().inventory().remove(12846, true)
				it.itemBox("You have learnt the 'Teleport to Bounty Target' spell.", 12846)
			}
		})
	}
	
	@Suspendable fun initial_options(it: Script) {
		val skull_option = if (Skulling.skulled(it.player()))
			"Can you make my PK skull last longer?" else "Can I have a PK skull, please?"
		val streak_option = if (VarbitAttributes.varbiton(it.player(), VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.varbitid)) "I want to see kill streak skulls again." else
			"I don't want to see kill streak overheads."
		
		when (it.options("How do I get Blood Money?", "What rewards have you got?", skull_option, streak_option)) {
			1 -> getBloodMoneyDialogue(it)
			2 -> {
				if (it.player().world().realm().isPVP) {
					it.player().world().shop(52).display(it.player())
				} else {
					when (it.options("Shop 1", "Shop 2")) {
						1 -> it.player().world().shop(1364).display(it.player())
						2 -> it.player().world().shop(1365).display(it.player())
					}
				}
			}
			3 -> toggleOrExtendSkulls(it, true)
			4 -> toggleOptions(it, true)
		}
	}
	
	@Suspendable fun getBloodMoneyDialogue(it: Script) {
		it.chatPlayer("How exactly do I get Blood Money?", 554)
		it.chatNpc("There are many different wilderness activities you can do to acquire Blood Money.", EMBLEM_TRADER)
		it.doubleItemBox("You can Woodcut, Fish, or Thieve, which all have a 20% chance of you finding 1 Blood Money coin.", 1515, 333)
		it.itemBox("You can complete the Wilderness Agility course, which has a 20% chance of you finding 5 Blood Money per lap.", 9771)
		it.itemBox("You can kill monsters inside the wilderness which drop Widerness Resource Caskets.", 405)
		it.itemBox("Or finally, you can kill other players inside the wilderness, which gives 10 Blood money per kill.", 964)
		it.chatNpc("Blood Money can be used to purchase items from my shop. The items I carry often change, so make sure you " +
				"regularly check my stock for items that might be there for a day or two.", EMBLEM_TRADER)
		initial_options(it)
	}
	
	@Suspendable fun toggleOptions(it: Script, dialogue: Boolean = false) {
		val player = it.player()
		val streak_option = if (VarbitAttributes.varbiton(it.player(), VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.varbitid)) "I want to see kill streak skulls again." else
			"I don't want to see kill streak overheads."
		
		if (dialogue) {
			it.chatPlayer(streak_option)
			GameCommands.process(player, "toggleskulls")
		} else {
			val givePotions = player.attribOr<Boolean>(AttributeKey.GIVE_EMPTY_POTION_VIALS, true)
			val potionToggleDialogue = if (givePotions) "Remove vials after finishing a potion" else "Keep vials after finishing a potion"
			val toggleHunterStatus = if (player.attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false) == true) "Toggle bounty hunter on" else "Toggle bounty hunter off"
			val toggleBountyRecord = (if (player.varps().varbit(Varbit.BOUNTY_HUNTER_RECORD_OVERLAY_HIDDEN) == 0) "Hide" else "Show") + " bounty hunter record overlay"
			val pkskulls = if (VarbitAttributes.varbiton(player, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.varbitid)) "on" else "off"
			when (it.options("Toggle kill streak colored skulls <img=26> <img=27> <img=28> <img=29> <img=30> ${pkskulls}", potionToggleDialogue, toggleHunterStatus,
					toggleBountyRecord)) {
				1 -> GameCommands.process(player, "toggleskulls")
				2 -> {
					if (givePotions) {
						player.putattrib(AttributeKey.GIVE_EMPTY_POTION_VIALS, false)
						it.itemBox("You will no longer receive vials when drinking the last dose of a potion.", 229)
					} else {
						player.putattrib(AttributeKey.GIVE_EMPTY_POTION_VIALS, true)
						it.itemBox("You will now receive vials when finishing the last dose of a potion.", 229)
					}
				}
				3 -> {
					if (player.attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false) == true) {
						player.putattrib(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false)
						player.write(UpdateStateCustom(1, 33))
						it.chatNpc("You will now receive targets while being within the wilderness.", EMBLEM_TRADER)
					} else {
						player.putattrib(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, true)
						player.write(UpdateStateCustom(1, 1))
						it.chatNpc("You will no longer receive targets while being within the wilderness.", EMBLEM_TRADER)
					}
				}
				4 -> {
					player.varps().varbit(Varbit.BOUNTY_HUNTER_RECORD_OVERLAY_HIDDEN, if (player.varps().varbit(Varbit.BOUNTY_HUNTER_RECORD_OVERLAY_HIDDEN) == 0) 1 else 0)
					player.message("Bounty Hunter Record overlay has been toggled to: ${(if (player.varps().varbit(Varbit.BOUNTY_HUNTER_RECORD_OVERLAY_HIDDEN) == 0) "On" else "Off")}.")
				}
			}
		}
	}
	
	@Suspendable fun toggleOrExtendSkulls(it: Script, dialogue: Boolean = false) {
		if (Skulling.skulled(it.player())) {
			if (dialogue)
				it.chatPlayer("Can I have a skull, please?", 554)
			
			if (it.optionsTitled("A PK Skull means you drop ALL your items on death.", "Give me a PK skull.", "Cancel.") == 1) {
				Skulling.skull(it.player(), null)
				it.itemBox("You are now skulled.", 964)
			}
		} else {
			if (dialogue)
				it.chatPlayer("Can you make my PK skull last longer?", 554)
			
			if (it.optionsTitled("Extend your PK skull duration?", "Yes", "No") == 1) {
				Skulling.skull(it.player(), null)
				it.itemBox("Your PK skull will now last for the max duration.", 964)
			}
		}
	}
	
	@Suspendable fun exchange(it: Script) {
		val pts = BountyHunterEmblem.totalReward(it.player().inventory())
		
		it.player().inventory().items().forEach { item ->
			if (Objects.nonNull(item) && BountyHunterEmblem.isEmblem(item)) {
				it.player().inventory().remove(item, false)
			}
		}
		it.player().modifyNumericalAttribute(AttributeKey.BOUNTY_HUNTER_POINTS, pts, 0)
		it.itemBox("The trader awards you ${NumberFormat.getInstance().format(pts)} points.", 12746)
	}
	
	/**
	 * Displays the bounty hunter shop with appropriate configs.
	 */
	fun displayShop(it: Script) {
		if (false) {
			it.player().world().shop(52).display(it.player())
			it.player().varps().varp(BOUNTY_HUNTER_SHOP_VARP, it.player().attribOr(AttributeKey.BOUNTY_HUNTER_POINTS, 0))
		}
	}
}