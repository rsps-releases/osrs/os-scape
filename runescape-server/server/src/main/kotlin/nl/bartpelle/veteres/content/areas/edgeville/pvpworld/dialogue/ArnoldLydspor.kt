package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 1/7/2016.
 */

object ArnoldLydspor {
	
	val ARNOLD_LYDSPOR = 4293
	val SUPPLIES = 6
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ARNOLD_LYDSPOR) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(SUPPLIES).display(it.player())
			} else {
				it.player().message("You can only view this shop in the PK server.")
			}
		}
	}
}