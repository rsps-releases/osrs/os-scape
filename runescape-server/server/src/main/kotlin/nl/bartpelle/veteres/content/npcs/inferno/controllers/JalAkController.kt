package nl.bartpelle.veteres.content.npcs.inferno.controllers

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Mack on 8/3/2017.
 */
class JalAkController(ak: Npc, session: InfernoSession, counter: Int): InfernoNpcController {
	
	/**
	 * The npc who is owner of this controller instance.
	 */
	val ak = ak
	
	/**
	 * The active inferno session.
	 */
	val session = session
	
	/**
	 * The offset we use to manipulate tile spawns.
	 */
	val counter = counter
	
	var xPosOff = 0
	
	override fun onSpawn() {
		
		//Every 3 creeps per column we make a new row
		if (counter % 3 == 0) {
			xPosOff = 1
		}
		
		ak.tile(Tile(session.area!!.center().x + xPosOff, session.area!!.center().z + 1 + counter))
		
		ak.attack(session.player)
	}
	
	override fun onDamage() {
		if (!InfernoContext.inSession(session.player())) {
			session.player().message("I don't think I should be doing this...")
			return
		}
		super.onDamage()
	}
	
	override fun triggerPlayerKilledDeath() {
		val player = session.player()
		var pad = 0
		for (i in InfernoContext.JAL_AKREK_MEJ..InfernoContext.JAL_AKREK_KET) {
			pad += 2
			var blobling: Npc = Npc(i, player.world(), Tile(ak.tile().x + pad, ak.tile().z))
			blobling.walkRadius(200)
			blobling.respawns(false)
			blobling.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 100)
			player.world().registerNpc(blobling)
			
			//add to the collection
			session.activeNpcs.add(blobling)
			
			blobling.attack(player)
		}
	}
}