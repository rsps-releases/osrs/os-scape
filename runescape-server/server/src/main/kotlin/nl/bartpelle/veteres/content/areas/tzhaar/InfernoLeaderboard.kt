package nl.bartpelle.veteres.content.areas.tzhaar

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Misc
import java.util.*

/**
 * Created by Mack on 8/20/2017.
 */
object InfernoLeaderboard {
	
	/**
	 * The run time repository storing the best run times.
	 */
	val RUNTIME_REPO: ArrayList<Player> = ArrayList()
	
	/**
	 * The comparator used to sort our collection.
	 */
	val comparator = Comparator<Player> {first, second -> first.attrib<Long>(AttributeKey.INFERNO_RUNTIME).compareTo(second.attrib<Long>(AttributeKey.INFERNO_RUNTIME)) }
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onLogin {
			save(it.player())
		}
	}
	
	/**
	 * Saves the run time to the repository, if available.
	 */
	fun save(player: Player) {
		val temp = RUNTIME_REPO
		if (player.attribOr<Long>(AttributeKey.INFERNO_RUNTIME, 0L) != 0L) {
			for (p in temp) {
				if (player.name() == p.name()) {
					RUNTIME_REPO.remove(p)
					break
				}
			}
			RUNTIME_REPO.add(player)
		}
	}
	
	/**
	 * Displays and stylizes the interface for the local best run times.
	 */
	fun display(player: Player) {
		
		//Clean the interface before we write to it.
		for (i in 2..133) {
			player.interfaces().text(275, i, "")
		}
		
		player.interfaces().text(275, 2, "JalYt Leaderboard of Best Inferno Times (Local)")
		
		var linePtr = 4
		var index = 0
		var colorStyle = ""
		
		//Sort the list
		Collections.sort(RUNTIME_REPO, comparator)
		
		for (p in RUNTIME_REPO) {
			
			if (p.privilege().eligibleTo(Privilege.ADMIN) || player.seniorModerator()) {
				continue
			}
			
			index += 1
			
			//If we hit 100 entries then we stop.
			if (index == 101) break
			
			//Get the color
			if (index == 1) colorStyle = "<col=FF0000>" else if (index == 2) colorStyle = "<col=A9A9A9>" else if (index == 3) colorStyle = "<col=8B4513>" else colorStyle = ""
			
			player.interfaces().text(275, linePtr, "#$index $colorStyle ${p.name()} </col> with a run time of ${Misc.formatLongAsHMS(p.attrib<Long>(AttributeKey.INFERNO_RUNTIME))}.")
			linePtr += 1
		}
		player.interfaces().sendMain(275)
	}
}