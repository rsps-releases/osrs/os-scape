package nl.bartpelle.veteres.content.npcs.pets

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI.petType
import nl.bartpelle.veteres.fs.EnumDefinition
import nl.bartpelle.veteres.fs.VarbitDefinition
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 3/16/2016.
 * Support for alternate metamorphed npcs added by Jak
 */
object ProbitaPetReclaiming {
	
	val PROBITA = 5906
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(PROBITA) @Suspendable {
			it.chatNpc("Welcome to the pet insurance bureau.<br>How can I help you?", PROBITA, 567)
			when (it.options("I've lost a pet. Have you got it?", "Maybe another time.")) {
				1 -> {
					it.chatPlayer("I've lost a pet. Have you got it?", 610)
					showInsurance(it.player())
				}
				2 -> {
					it.chatPlayer("Maybe another time.", 588)
				}
			}
		}
		
		// Insurance bitch: option 2 :$
		r.onNpcOption2(PROBITA) {
			for (fucked in arrayOf(Pet.ROCKY, Pet.PHOENIX)) {
				if (PetAI.hasUnlocked(it.player(), fucked)) {
					val isUIM = it.player().ironMode() == IronMode.ULTIMATE
					if (!it.player().inventory().has(fucked.item) && (isUIM || (!isUIM && !it.player().bank().has(fucked.item)))) {
						if (!it.player().inventory().add(Item(fucked.item), false).success()) {
							if (it.player().bank().add(Item(fucked.item), false).success()) {
								it.message("The <col=FF0000>${Item(fucked.item).definition(it.player().world()).name}</col> pet was added to your bank automatically.")
							} else {
								it.message("You've got no inventory or bank space to reclaim the <col=FF0000>${Item(fucked.item).definition(it.player().world()).name}</col> pet.")
							}
						} else {
							it.message("The <col=FF0000>${Item(fucked.item).definition(it.player().world()).name}</col> pet was added to your inventory automatically.")
						}
					}
				}
			}
			showInsurance(it.player())
		}
		
		// Insurance reclaim option
		r.onButton(148, 14) {
			val enum = it.player().world().definitions().get(EnumDefinition::class.java, 985)
			var realId = enum.getInt(it.buttonSlot())
			
			// We can't access our bank. Skip checking bank for pets.. there shouldn't even be one here there but because of our
			// pickup system, pets used to go to bank on death.. with UIM cannot access :P
			val isUIM = it.player().ironMode() == IronMode.ULTIMATE
			
			val pet = Pet.getPetByItem(realId)!!
			
			// Make sure we actually have that pet unlocked
			if (it.player().varps().varbit(pet.varbit) != 0) {
				// Don't add it if we have it, duh.
				if (!it.player().inventory().has(realId) && (isUIM || (!isUIM && !it.player().bank().has(realId)))) {
					if (it.player().inventory().add(Item(realId), true).failed()) {
						it.message("You don't have enough room in your inventory to do this.")
					} else {
						sendReclaimables(it.player())
					}
				}
			}
		}
	}
	
	fun sendReclaimables(player: Player) {
		// Calculate which items we miss etc
		var have866 = 0
		var have1417 = 0
		val pet = player.pet()
		
		// Include any spawned pet..
		if (pet != null && !pet.finished()) {
			if (pet.petType()!!.varbit != -1) {
				val def = player.world().definitions().get(VarbitDefinition::class.java, pet.petType()!!.varbit)
				if (def.varp == 866) {
					have866 = 1 shl def.startbit
				} else {
					have1417 = 1 shl def.startbit
				}
			}
		}
		
		// We can't access our bank. Skip checking bank for pets.. there shouldn't even be one here there but because of our
		// pickup system, pets used to go to bank on death.. with UIM cannot access :P
		val isUIM = player.ironMode() == IronMode.ULTIMATE
		
		// Any in our bank? Inventory?
		val enum = player.world().definitions().get(EnumDefinition::class.java, 985)
		for ((slot, itemId) in enum.enums()) {
			
			val petItemId = itemId as Int
			val petdef = Pet.getPetByItem(petItemId)
			
			var hasAlternateMetamorphId = false
			if (petdef != null) {
				// Find other forms of pets which have the same varbit
				val otherAlternativeFormPets = mutableSetOf<Pet>()
				for (pet in Pet.values()) {
					if (petdef == pet) { // Don't compare self
						continue;
					}
					if (petdef.varbit == pet.varbit) {
						otherAlternativeFormPets.add(pet)
					}
				}
				
				// Now see if we have any of these alternatives in other containers
				for (pet in otherAlternativeFormPets) {
					if (player.inventory().hasAny(pet.item) || player.bank().hasAny(pet.item)) {
						hasAlternateMetamorphId = true
					}
				}
			} else {
				//player.debug("Interesting.. bad pet! %d", brokenOrRealId)
			}
			
			// If we've got the pet, adjust varp.
			if (player.inventory().has(petItemId) || (!isUIM && player.bank().has(petItemId)) || hasAlternateMetamorphId) {
				val def = player.world().definitions().get(VarbitDefinition::class.java, petdef!!.varbit) ?: continue
				if (def.varp == 866) {
					have866 = have866 or 1.shl(def.startbit)
				} else {
					have1417 = have1417 or 1.shl(def.startbit)
				}
			}
		}
		
		val available866 = player.varps().varp(866) and have866.inv()
		val available1417 = player.varps().varp(1417) and have1417.inv()
		player.invokeScript(202, available866, available1417) // Sets available reclaimables :)
	}
	
	@JvmStatic fun showInsurance(player: Player) {
		sendReclaimables(player)
		player.interfaces().setting(148, 7, 0, 50, 1024)
		player.interfaces().setting(148, 14, 0, 50, 1026)
		player.interfaces().sendMain(148)
		sendReclaimables(player)
	}
}