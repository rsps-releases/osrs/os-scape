package nl.bartpelle.veteres.content.areas.wilderness

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Mack on 11/3/2017.
 */
object WildernessSkillingResources {
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onWorldInit s@ {
			val world = it.ctx<World>()
			
			if (!world.realm().isPVP) {
				return@s
			}
			
			//spawn custom rocks
			world.spawnObj(MapObj(Tile(3022, 3590), 7484, 10, 0), true)
			world.spawnObj(MapObj(Tile(3020, 3587), 7486, 10, 1), true)
			world.spawnObj(MapObj(Tile(3011, 3594), 7484, 10, 0), true)
			world.spawnObj(MapObj(Tile(3010, 3592), 7486, 10, 0), true)
			world.spawnObj(MapObj(Tile(3015, 3597), 7484, 10, 0), true)
			world.spawnObj(MapObj(Tile(3012, 3587), 7486, 10, 0), true)
			world.spawnObj(MapObj(Tile(3009, 3589), 7484, 10, 0), true)
			world.spawnObj(MapObj(Tile(3018, 3598), 7486, 10, 0), true)
			world.spawnObj(MapObj(Tile(3021, 3597), 7488, 10, 0), true)
			world.spawnObj(MapObj(Tile(3020, 3589), 7488, 10, 0), true)
			world.spawnObj(MapObj(Tile(3023, 3587), 7488, 10, 0), true)
			world.spawnObj(MapObj(Tile(3017, 3592), 7459, 10, 0), true)
			world.spawnObj(MapObj(Tile(3017, 3591), 7459, 10, 0), true)
		}
	}
}