package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.SettingsBuilder
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 9/5/2015.
 */
@ScriptMain fun xpdrops(r: ScriptRepository) {
	// Toggle button
	r.onButton(160, 1) {
		when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
			1 -> {
				it.player().varps().varbit(Varbit.XP_DROPS_VISIBLE, if (it.player().varps().varbit(Varbit.XP_DROPS_VISIBLE) == 1) 0 else 1)
				if (it.player().varps().varbit(Varbit.XP_DROPS_VISIBLE) == 1) {
					it.player().interfaces().closeById(122) // Close previous position.
					it.player().interfaces().sendWidgetOn(122, Interfaces.InterSwitches.XP_DROP)
				} else {
					it.player().interfaces().closeById(122)
				}
			}
			2 -> open_xpdrops(it.player())
		}
	}
	
	// Speed toggle
	r.onButton(137, 57) {
		it.player().varps().varbit(Varbit.XP_DROPS_SPEED, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Size toggle
	r.onButton(137, 51) {
		it.player().varps().varbit(Varbit.XP_DROPS_SIZE, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Position toggle
	r.onButton(137, 50) {
		it.player().varps().varbit(Varbit.XP_DROPS_POSITION, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Duration toggle
	r.onButton(137, 52) {
		it.player().varps().varbit(Varbit.XP_DROPS_DURATION, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Progressbar toggle
	r.onButton(137, 54) {
		it.player().varps().varbit(Varbit.XP_DROPS_PROGRESSBAR, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Counter toggle
	r.onButton(137, 53) {
		it.player().varps().varbit(Varbit.XP_DROPS_COUNTER, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Color toggle
	r.onButton(137, 55) {
		it.player().varps().varbit(Varbit.XP_DROPS_COLOR, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Color toggle
	r.onButton(137, 56) {
		it.player().varps().varbit(Varbit.XP_DROPS_GROUP, it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) - 1)
	}
	// Open tracker for skill
	r.onButton(137, 16) {
		var slot = it.player().attrib<Int>(AttributeKey.BUTTON_SLOT) + 1
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_STAT_VIEWING, slot)
		if (onTotalMenu(it))
			slot = 0
		var startVarp = 1228 + slot
		var goalVarp = 1252 + slot
		val start = it.player().varps().varp(startVarp)
		val goal = it.player().varps().varp(goalVarp)
		it.player().varps().varp(Varp.ANON_VARP_1, start)
		it.player().varps().varp(Varp.ANON_VARP_2, goal)
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE, if (goal > 0 && start > 0) 2 else if (start > 0) 1 else 0)
		it.player().debug("%s %s %s, %s %s", slot, startVarp, goalVarp, goal, start)
	}
	// Disable tracker (top stage of menu)
	r.onButton(137, 20) {
		it.player().varps().varp(Varp.ANON_VARP_1, 0)
		it.player().varps().varp(Varp.ANON_VARP_2, 0)
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE, 0)
	}
	// Enable tracker (middle stage of menu)
	r.onButton(137, 24) {
		// varp 262 TODO conflict check!!
		var currentXp: Int = if (onTotalMenu(it)) it.player().skills().totalXp().toInt() else xpOfCurrentStatMenu(it)
		if (onTotalMenu(it))
			currentXp /= 1000
		it.player().varps().varp(Varp.ANON_VARP_1, currentXp)
		it.player().varps().varp(Varp.ANON_VARP_2, -1) // goal -1 .. YES -1 doesnt break varbits cos its a varp
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE, 1)
	}

	// Set buttons on both middle AND bottom sections
	for (btn in arrayListOf(37, 41, 29)) // 29 is btn for overall XP middle section
		r.onButton(137, btn) @Suspendable {
			val totalXpMenu = onTotalMenu(it)
			var curentXp = if (totalXpMenu) it.player().skills().totalXp().toInt() else xpOfCurrentStatMenu(it)
			var xp: Int = when (it.buttonAction()) {
				6 -> Skills.levelToXp(it.inputInteger(if (!totalXpMenu) "Enter a level target to start tracking from."
				else "Set tracker start point in THOUSANDS of XP:"))
				7 -> {
					it.inputInteger(if (!totalXpMenu) if (btn == 37) "Enter experience target to start tracking from." else "Enter experience goal to track towards." else
						"Set goal end point in THOUSANDS of XP:")
				}
				10 -> curentXp // current XP
				else -> 0
			}

			if (totalXpMenu)
				curentXp /= 1000
			val varp = if (btn == 41) Varp.ANON_VARP_2 else Varp.ANON_VARP_1
			if (varp == Varp.ANON_VARP_1 && xp > curentXp) // start xp, capped at current
				xp = curentXp
			if (xp > 4600000 && onTotalMenu(it) && btn == 41) {// Goal total XP
				xp = 4600000 // 4.6b cap
				it.message("Maximum total experience target is 4.6b XP.")
			}
			it.player().varps().varp(varp, xp)

			it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE, if (btn in intArrayOf(32, 37, 41)) 2 else 1)
		}

	// Tracking towards Goal section box activate, bottom of menu
	r.onButton(137, 32) @Suspendable {
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE, 2)
		val onTotalMenu = onTotalMenu(it)
		var currentXp: Int = if (onTotalMenu) it.player().skills().totalXp().toInt() else xpOfCurrentStatMenu(it)
		if (onTotalMenu)
			currentXp /= 1000
		it.player().varps().varp(Varp.ANON_VARP_1, currentXp)
		var goalXp = Skills.levelToXp(it.player().skills().xpLevel(statIdOfCurrentMenu(it)) + 1)
		if (goalXp >= Skills.levelToXp(99))
			goalXp = 200_000_000
		if (onTotalMenu) {
			val xp = it.player().skills().totalXp()
			val theshold = xp / 10
			goalXp = ((xp + theshold) / theshold * theshold).toInt() // 100k above current rounded to 100k.. when ive got 50k xp on mule account
		}
		it.player().varps().varp(Varp.ANON_VARP_2, goalXp) // Goal xp, default the level above current
	}
	// Save
	r.onButton(137, 45) {
		val current: Int = if (onTotalMenu(it)) it.player().skills().totalXp().toInt() else xpOfCurrentStatMenu(it)
		val start = it.player().varps().varp(Varp.ANON_VARP_1)
		val m = if (onTotalMenu(it)) 1000 else 1
		if (start * m > current) {
			it.message("You can't set the start point of your tracker to higher than your current XP in the skill.")
			return@onButton
		}

		var slot = it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_STAT_VIEWING)
		if (onTotalMenu(it))
			slot = 0
		var startVarp = 1228 + slot
		var goalVarp = 1252 + slot


		it.player().varps().varp(startVarp, when (it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE)) {
			1, 2 -> start // Goal varp, -1 for no goal or XP
			else -> 0
		})
		var goal = it.player().varps().varp(Varp.ANON_VARP_2)
		if (goal > 4600000 && onTotalMenu(it))
			goal = 4600000 // 4.6b cap
		it.player().varps().varp(goalVarp, goal) // Starting from XP - attack
		it.player().varps().varp(Varp.ANON_VARP_2, 0)
		it.player().varps().varp(Varp.ANON_VARP_1, 0)
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_STAT_VIEWING, 0) // back to main menu
	}
	// Discard
	r.onButton(137, 44) {
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_STAT_VIEWING, 0) // back to main menu
		it.player().varps().varp(Varp.ANON_VARP_2, 0)
		it.player().varps().varp(Varp.ANON_VARP_1, 0)
		it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE, 0)
	}
}

fun onTotalMenu(it: Script): Boolean {
	return it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_STAT_VIEWING) == 24
}

fun xpOfCurrentStatMenu(it: Script): Int {
	return it.player().skills().xp()[statIdOfCurrentMenu(it)].toInt()
}

fun statIdOfCurrentMenu(it: Script): Int {
	val slot = it.player().varps().varbit(Varbit.XP_DROPS_TRACKER_STAT_VIEWING)
	return when (slot) {
	// 0 is viewing main menu
		1 -> Skills.ATTACK
		2 -> Skills.STRENGTH
		3 -> Skills.RANGED
		4 -> Skills.MAGIC
		5 -> Skills.DEFENCE // defence
		6 -> Skills.HITPOINTS // hp
		7 -> Skills.PRAYER // prayer
		8 -> Skills.AGILITY
		9 -> Skills.HERBLORE
		10 -> Skills.THIEVING
		11 -> Skills.CRAFTING
		12 -> Skills.RUNECRAFTING
		13 -> Skills.MINING
		14 -> Skills.SMITHING
		15 -> Skills.FISHING
		16 -> Skills.COOKING
		17 -> Skills.FIREMAKING
		18 -> Skills.WOODCUTTING
		19 -> Skills.FLETCHING
		20 -> Skills.SLAYER
		21 -> Skills.FARMING
		22 -> Skills.CONSTRUCTION
		23 -> Skills.HUNTER
		// 24 = total
		else -> 0
	}
}

fun open_xpdrops(player: Player) {
	player.write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
	player.interfaces().sendMain(137)
	
	player.interfaces().setting(137, 50, 1, 3, SettingsBuilder().option(0))
	player.interfaces().setting(137, 51, 1, 3, SettingsBuilder().option(0))
	player.interfaces().setting(137, 52, 1, 4, SettingsBuilder().option(0))
	player.interfaces().setting(137, 53, 1, 32, SettingsBuilder().option(0))
	player.interfaces().setting(137, 54, 1, 32, SettingsBuilder().option(0))
	player.interfaces().setting(137, 55, 1, 8, SettingsBuilder().option(0))
	player.interfaces().setting(137, 56, 1, 2, SettingsBuilder().option(0))
	player.interfaces().setting(137, 57, 1, 3, SettingsBuilder().option(0))
	player.interfaces().setting(137, 16, 0, 24, SettingsBuilder().option(0))
	player.varps().varbit(Varbit.XP_DROPS_TRACKER_STAT_VIEWING, 0)
	player.varps().varbit(Varbit.XP_DROPS_TRACKER_TYPE_ACTIVE, 0)
}
