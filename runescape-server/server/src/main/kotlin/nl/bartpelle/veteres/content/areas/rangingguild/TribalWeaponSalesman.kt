package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-11.
 */

object TribalWeaponSalesman {
	
	val TRIBAL_WEAPON_SALESMAN = 6069
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(TRIBAL_WEAPON_SALESMAN) @Suspendable {
			it.chatPlayer("Hello there.", 567)
			it.chatNpc("Greetings, traveller. Are you interested in any throwing weapons?", TRIBAL_WEAPON_SALESMAN, 568)
			when (it.options("Yes I am.", "Not really.")) {
				1 -> {
					it.chatPlayer("Yes I am.", 567)
					it.chatNpc("That's a good thing.", TRIBAL_WEAPON_SALESMAN, 567)
					it.player().world().shop(47).display(it.player())
				}
				2 -> {
					it.chatPlayer("Not really.", 588)
					it.chatNpc("No bother to me.", TRIBAL_WEAPON_SALESMAN, 588)
				}
			}
		}
		r.onNpcOption2(TRIBAL_WEAPON_SALESMAN) @Suspendable {
			it.player().world().shop(47).display(it.player())
		}
	}
}
