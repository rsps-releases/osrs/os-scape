package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle
import java.lang.ref.WeakReference

/**
 * Created by Mack on 8/3/2017.
 */

object JalMejJakCombat {
	@JvmField
	val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		val world = npc.world()
		var target = EntityCombat.getTarget(it) ?: return@s

		npc.face(null)

		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.attackTimerReady(npc)) {
				attack(npc, world, target)
			}

			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}

	@Suspendable
	fun attack(npc: Npc, world: World, player: Entity) {
		val toTile = npc.tile().transform(0, -5)
		val tileDist = npc.tile().distance(toTile)
		val delay = Math.max(1, (50 + (tileDist * 12)) / 35)
		val damageZone = Area(toTile, 3)

		npc.animate(npc.attackAnimation())

		player.world().server().scriptExecutor().executeScript(player.world(), @Suspendable {

			it.delay(1)

			world.spawnProjectile(npc.tile(), toTile, 660, 10, 10, 5, 10 * tileDist, 30, 200)
			world.spawnProjectile(npc.tile(), toTile.transform(1, 0), 660, 10, 5, 0, 10 * tileDist, 30, 200)
			world.spawnProjectile(npc.tile(), toTile.transform(-1, 0), 660, 10, 5, 0, 10 * tileDist, 30, 200)

			world.tileGraphic(659, toTile, 0, delay)
			world.tileGraphic(659, toTile.transform(0, -1), 0, delay)

			world.tileGraphic(659, toTile.transform(1, 0), 0, delay)
			world.tileGraphic(659, toTile.transform(1, -1), 0, delay)

			world.tileGraphic(659, toTile.transform(-1, 0), 0, delay)
			world.tileGraphic(659, toTile.transform(-1, -1), 0, delay)

			if (player.tile().inArea(damageZone)) {
				player.hit(npc, npc.world().random(11), delay).combatStyle(CombatStyle.RANGE)
			}
		})

		player.putattrib(AttributeKey.LAST_DAMAGER, npc)
		player.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, player)
		EntityCombat.putCombatDelay(npc, 6)
	}
}

class JalMejJak(zuk: Npc, session: InfernoSession) {
	
	@JvmField val healingScript: Function1<Script, Unit> = @Suspendable {
		
		val healer = it.npc()
		val tileDist = healer.tile().transform(1, 1, 0).distance(zuk.tile())
		
		while (InfernoContext.inSession(session.player())) {
			
			//Break the script and start the combat script if our last damager is not null.
			if (healer.attribOr<Entity>(AttributeKey.LAST_DAMAGER, null) != null) {
				healer.putattrib(AttributeKey.TARGET, WeakReference<Entity>(session.player()))
				healer.faceTile(healer.tile().transform(0, -4))
				healer.executeScript(JalMejJakCombat.script)
				break
			}
			
			healer.face(zuk)
			healer.world().spawnProjectile(healer, zuk, 660, 10, 36, 41, 10 * tileDist, 10, 5)
			zuk.graphic(444, 250, 5)
			zuk.heal(healer.world().random(12))
			healer.animate(healer.attackAnimation())
			
			if (InfernoContext.shouldUpdateOverlay(session.player(), zuk)) {
				InfernoContext.updateBossOverlay(zuk, session.player())
			}
			
			it.delay(2)
		}
	}
}