package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.veteres.content.TileOffset
import nl.bartpelle.veteres.content.to

/**
 * Created by Jason MacKeigan on 2016-09-03 at 12:56 AM
 */
enum class TopBarricadePositionOffset(vararg val offsets: TileOffset) {
	WEST(-20 to -1),
	SOUTH_WEST(-6 to -14),
	SOUTH_SOUTH_WEST(-19 to -18),
	SOUTH(0 to -17),
	SOUTH_SOUTH_EAST(13 to -14),
	SOUTH_EAST(20 to -21),
	EAST(20 to -5),
	NORTH_EAST(17 to 1)
	;
	
	companion object {
		
		val ALL = TopBarricadePositionOffset.values().flatMap { it.offsets.asIterable() }
	}
}