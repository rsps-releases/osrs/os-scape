package nl.bartpelle.veteres.content.areas.abyssalnexus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/19/2016.
 */

object TheOverseer {
	
	val THE_OVERSEER = 27057
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) { //TODO: There must be an additional option to mention a component he havested
		r.onNpcOption1(THE_OVERSEER) @Suspendable {
			it.chatNpc("Have you harvested any components for me?", THE_OVERSEER, 7117)
			when (it.options("What's this stuff for?", "Tell me about yourself.", "Tell me about the Abyssal Sires.", "No, sorry.")) {
				1 -> whatsThisStuffFor(it)
				2 -> tellMeAboutYourself(it)
				3 -> tellMeAboutTheAbyssalSires(it)
				4 -> noSorry(it)
			}
		}
	}
	
	@Suspendable fun whatsThisStuffFor(it: Script) {
		it.chatPlayer("What's this stuff for?", 554)
		it.chatNpc("I shall use the components to construct a mighty<br>weapon for you. Rest assured, the process will be" +
				"<br>beneficial to both of us.", THE_OVERSEER, 7117)
	}
	
	@Suspendable fun tellMeAboutYourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 554)
		it.chatNpc("Ah... Time does not pass in the Abyss at the same rate<br>that it does in your realm, but nevertheless I have been" +
				"<br>in this place for aeons.", THE_OVERSEER, 7117)
		it.chatNpc("Indeed, when I was last in your realm, the armies of<br>Zamorak were battling the other gods for control, and" +
				"<br>we sought reinforcements from any plane we could<br>reach.", THE_OVERSEER, 7117)
		it.chatNpc("That was how I came here. We broke through to the<br>Abyss, seeking allies for our cause, but instead I ended" +
				"<br>up a prisoner in this place, unable to leave, while the<br>God Wars continued without me.", THE_OVERSEER, 7117)
		when (it.options("Tell me about the Abyssal Sires.", "I'll be off for now.")) {
			1 -> tellMeAboutTheAbyssalSires(it)
			2 -> illBeOffNow(it)
		}
	}
	
	@Suspendable fun tellMeAboutTheAbyssalSires(it: Script) {
		it.chatPlayer("Tell me about the Abyssal Sires.", 554)
		it.chatNpc("The Sires are the engineers of this living pocket<br>universe known to you and I as the Abyss.", THE_OVERSEER, 7117)
		it.chatNpc("At the hands of the 'Chaos God', Zamorak, like myself,<br>they too were cast down and imprisoned in this living" +
				"<br>tomb. ", THE_OVERSEER, 7117)
		it.chatNpc("They worked for centuries attempting to escape the pull<br>of the Abyss but as food resources began to deplete" +
				"<br>they knew that their only chance for survival was to<br>construct this Nexus.", THE_OVERSEER, 7117)
		it.chatNpc("They absorb flesh and bone of their demon kin, weaving<br>it into the fabric of these very walls. Here they have" +
				"<br>remained in deep stasis, asleep.", THE_OVERSEER, 7117)
		it.chatNpc("If you succeed in awakening them, they will no doubt<br>unleash their full fury upon you. Someone of your" +
				"<br>calibre should be able to deal with them though, especially<br>with the shadows on your side...", THE_OVERSEER, 7117)
		it.chatNpc("If you succeed in killing a Sire, you may be fortunate<br>enough to find one of its unsired offspring among its" +
				"<br>remains. Bring the Unsired here, to the Font of<br>Consumption.", THE_OVERSEER, 7117)
		it.chatNpc("By placing the Unsired into the Font, I will be able to<br>consume its flesh and perhaps you will receive something<br>worth having.", THE_OVERSEER, 7117)
		when (it.options("Tell me about yourself.", "I'll be off for now.")) {
			2 -> illBeOffNow(it)
		}
	}
	
	@Suspendable fun illBeOffNow(it: Script) {
		it.chatPlayer("I'll be off for now.", 592)
	}
	
	@Suspendable fun noSorry(it: Script) { //TODO: Checks to see what components the player has already given to the Font of consumption
		it.chatPlayer("No, sorry.", 610)
		it.chatNpc("I still require a spine, a claw and an axon from an<br>Abyssal Sire.", THE_OVERSEER, 7117)
	}
}
