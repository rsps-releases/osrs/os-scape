package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Aabla {
	
	val AABLA = 3341
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(AABLA) @Suspendable {
			it.chatPlayer("Hi!", 567)
			it.chatNpc("Hi. How can I help?", AABLA, 567)
			when (it.options("Can you heal me?", "Do you see a lot of injured fighters?", "Do you come here foten?")) {
				1 -> {
					it.chatPlayer("Can you heal me?", 575)
					if (it.player().hp() == it.player().maxHp()) {
						it.chatNpc("You look healthy to me!", AABLA, 567)
					} else {
						heal_player(it)
					}
				}
				2 -> {
					it.chatPlayer("Do you see a lot of injured fighters?", 575)
					it.chatNpc("Yes I do. Thankfully we can cope with almost anything.<br>Jaraah really is a" +
							" wonderful surgeon, his methods are a<br>little unorthodox but he gets the job done.", AABLA, 590)
					it.chatNpc("I shouldn't tell you this but his nickname is 'The<br>Butcher'.", AABLA, 589)
					it.chatPlayer("That's reassuring.", 575)
				}
				3 -> {
					it.chatPlayer("Do you come here often?", 575)
					it.chatNpc("I work here, so yes!", AABLA, 567)
					it.chatNpc("You're silly!", AABLA, 605)
				}
			}
		}
		r.onNpcOption2(AABLA) @Suspendable {
			if (it.player().hp() == it.player().maxHp()) {
				it.chatNpc("You look healthy to me!", AABLA, 567)
			} else {
				heal_player(it)
			}
		}
	}
	
	fun heal_player(it: Script) {
		it.message("You feel a little better.")
		it.player().heal(2)
		it.sound(166)
		it.targetNpc()!!.sync().animation(881, 1)
	}
	
}
