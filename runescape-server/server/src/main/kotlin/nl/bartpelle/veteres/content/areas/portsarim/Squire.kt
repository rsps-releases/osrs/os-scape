package nl.bartpelle.veteres.content.areas.portsarim

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 17/10/2016.
 */
object Squire {
	
	val SQUIRE = 1770
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(SQUIRE, @Suspendable {
			it.chatNpc("Hi, how can I help you?", SQUIRE)
			when (it.options("Who are you?", "Where does this ship go?", "I'd like to go to your outpost.", "I'm fine thanks")) {
				1 -> {
					it.chatPlayer("Who are you?")
					it.chatNpc("I'm a Squire for the Void Knights.", SQUIRE)
					it.chatPlayer("The who?")
					it.chatNpc("The Void Knights, they are great warriors of balance who do Guthix's work here in Gielinor.", SQUIRE)
					when (it.options("Wow, can I join?", "What kind of work?", "What's 'Gielinor'?", "Uh huh, sure.")) {
						1 -> {
							it.chatPlayer("Wow, can I join?")
							it.chatNpc("Entry is strictly invite only, however we do need help continuing Guthix's work.", SQUIRE)
							when (it.options("What kind of work?", "Good luck with that.")) {
								1 -> work(it)
							}
						}
						2 -> work(it)
						3 -> whatIsGielinor(it)
					}
				}
				2 -> {
					it.chatPlayer("Where does this ship go?")
					it.chatNpc("To the Void Knight outpost. It's a small island just off Karamja.", SQUIRE)
					when (it.options("I'd like to go to your outpost.", "That's nice.")) {
						1 -> {
							travel(it, false)
						}
					}
				}
				3 -> travel(it, true)
			}
		})
		repo.onNpcOption2(SQUIRE) {
			travel(it, true)
		}
	}
	
	@Suspendable private fun travel(it: Script, quickTravel: Boolean) {
		if (!quickTravel) {
			it.chatPlayer("I'd like to go to your outpost.")
			it.chatNpc("Certainly, right this way.", SQUIRE)
		}
		it.player().lock()
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 14) // 15 is from the outpost back to port sarim
		it.player().interfaces().sendMain(299)
		it.player().write(InterfaceText(299, 25, "You sail to the Void Knight outpost."))
		it.delay(11)
		it.player().interfaces().closeMain()
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 0) // reset
		it.player().teleport(Tile(2662, 2676, 1))
		it.player().unlock()
		it.messagebox("The ship arrives at the Void Knight outpost.")
	}
	
	@Suspendable private fun work(it: Script) {
		it.chatPlayer("What kind of work?")
		it.chatNpc("Ah well you see we try to keep Gielinor as Guthix intended, it's very challenging. Actually we've been having some problems recently, maybe you could help us?", SQUIRE)
		when (it.options("Yeah ok, what's the problem?", "What's 'Gielinor'?", "I'd rather not, sorry.")) {
			1 -> {
				it.chatPlayer("Yeah ok, what's the problem?")
				it.chatNpc("Well the order has become quite dimished over the years, it's a very long process to learn the skills of a Void Knight. Recently there have been breaches into our realm" +
						" from somewhere else, and stange creatures", SQUIRE)
				it.chatNpc("Have been pouring through. We can't let that happen, and we'd be very grateful if you'd help us.", SQUIRE)
				when (it.options("How can I help?", "Sorry, but I can't.")) {
					1 -> {
						it.chatPlayer("How can I help?")
						it.chatNpc("We send launchers from our outpost to the nearby islands. If you go and wait in the lander there that'd really help.", SQUIRE)
					}
				}
			}
			2 -> whatIsGielinor(it)
		}
	}
	
	@Suspendable private fun whatIsGielinor(it: Script) {
		it.chatPlayer("What is 'Gielinor'?")
		it.chatNpc("It is the name that Guthix gave to this world, so we can honour him with its use.", SQUIRE)
	}
}