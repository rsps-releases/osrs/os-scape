package nl.bartpelle.veteres.content.combat.ranged

enum class ArrowDrawBack(val arrow: Int, val gfx: Int, val projectile: Int) {
	BRONZE_ARROW(882, 19, 10),
	IRON_ARROW(884, 18, 9),
	STEEL_ARROW(886, 20, 11),
	MITHRIL_ARROW(888, 21, 12),
	ADAMANT_ARROW(890, 22, 13),
	RUNITE_ARROW(892, 24, 15),
	AMETHYST_ARROW(21326, 1385, 15),
	DRAGON_ARROW(11212, 1116, 1120)
}