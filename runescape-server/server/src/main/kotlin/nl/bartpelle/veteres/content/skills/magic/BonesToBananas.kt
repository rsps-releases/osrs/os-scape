package nl.bartpelle.veteres.content.skills.magic

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/22/2015.
 */

object BonesToBananas {
	
	enum class BonesToBananasData(val itemId: Int) {
		REGULAR_BONES(526),
		BURNT_BONES(528),
		BAT_BONES(530),
		WOLF_BONES(2859),
		BIG_BONES(532),
		BABYDRAGON_BONES(534),
		DRAGON_BONES(536),
		ZOGRE_BONES(4812),
		OURG_BONES(4830),
		WYVERN_BONES(6812),
		DAGANNOTH_BONES(6729),
		LAVA_DRAGON_BONES(11943),
	}
	
	object BonesToBananas {
		@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
			BonesToBananasData.values().forEach { bones ->
				repo.onButton(218, 10) {
					//Check to see if the player has a high enough level to cast this spell.
					if (it.player().skills().xpLevel(Skills.MAGIC) < 15) {
						it.message("Your Magic level is not high enough for this spell.")
						return@onButton
					}
					//Getting the players inventory
					val inv = it.player().inventory()
					//We count how many bones there are in the players inventory
					val reg_bones = inv.count(BonesToBananasData.REGULAR_BONES.itemId)
					val burnt_bones = inv.count(BonesToBananasData.BURNT_BONES.itemId)
					val bat_bones = inv.count(BonesToBananasData.BAT_BONES.itemId)
					val wolf_bones = inv.count(BonesToBananasData.WOLF_BONES.itemId)
					val big_bones = inv.count(BonesToBananasData.BIG_BONES.itemId)
					val babydragon_bones = inv.count(BonesToBananasData.BABYDRAGON_BONES.itemId)
					val dragon_bones = inv.count(BonesToBananasData.DRAGON_BONES.itemId)
					val zogre_bones = inv.count(BonesToBananasData.ZOGRE_BONES.itemId)
					val orge_bones = inv.count(BonesToBananasData.OURG_BONES.itemId)
					val wyvern_bones = inv.count(BonesToBananasData.WYVERN_BONES.itemId)
					val dagannoth_bones = inv.count(BonesToBananasData.DAGANNOTH_BONES.itemId)
					val lava_dragon_bones = inv.count(BonesToBananasData.LAVA_DRAGON_BONES.itemId)
					//We calculate the total of the bones
					val total_bones = reg_bones + burnt_bones + bat_bones + wolf_bones + big_bones + babydragon_bones + dragon_bones + zogre_bones + orge_bones + wyvern_bones + dagannoth_bones + lava_dragon_bones
					//If the player has one of any of the bones we..
					if (total_bones >= 1) {
						//Check to see if the player has enough runes to cast the spell
						val runes = arrayOf(Item(557, 2), Item(555, 2), Item(561, 1))
						if (!MagicCombat.has(it.player(), runes, true)) {
							return@onButton
						}
						//Remove any possible bones from the players inventory
						it.player().inventory() -= Item(BonesToBananasData.REGULAR_BONES.itemId, reg_bones)
						it.player().inventory() -= Item(BonesToBananasData.BURNT_BONES.itemId, burnt_bones)
						it.player().inventory() -= Item(BonesToBananasData.BAT_BONES.itemId, bat_bones)
						it.player().inventory() -= Item(BonesToBananasData.WOLF_BONES.itemId, wolf_bones)
						it.player().inventory() -= Item(BonesToBananasData.BIG_BONES.itemId, big_bones)
						it.player().inventory() -= Item(BonesToBananasData.BABYDRAGON_BONES.itemId, babydragon_bones)
						it.player().inventory() -= Item(BonesToBananasData.DRAGON_BONES.itemId, dragon_bones)
						it.player().inventory() -= Item(BonesToBananasData.ZOGRE_BONES.itemId, zogre_bones)
						it.player().inventory() -= Item(BonesToBananasData.OURG_BONES.itemId, orge_bones)
						it.player().inventory() -= Item(BonesToBananasData.WYVERN_BONES.itemId, wyvern_bones)
						it.player().inventory() -= Item(BonesToBananasData.DAGANNOTH_BONES.itemId, dagannoth_bones)
						it.player().inventory() -= Item(BonesToBananasData.LAVA_DRAGON_BONES.itemId, lava_dragon_bones)

						it.player().inventory() += Item(1963, total_bones)
						it.player().animate(722)
						it.player().graphic(141, 100, 0)
						it.addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 51.0 else 25.5)
						it.message("You transform all your bones into bananas.")
					} else {
						it.message("You don't have enough bones to cast this spell.")
					}
				}
			}
		}
	}
}