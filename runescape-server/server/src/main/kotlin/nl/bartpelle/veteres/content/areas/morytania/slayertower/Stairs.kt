package nl.bartpelle.veteres.content.areas.morytania.slayertower

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/7/2015.
 */

object Stairs {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(2118) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3434 && obj.tile().z == 3537) {
				Ladders.ladderDown(it, Tile(3438, it.player().tile().z, it.player().tile().level - 1), false)
			}
		}
		r.onObject(2114) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3434 && obj.tile().z == 3537) {
				Ladders.ladderUp(it, Tile(3433, it.player().tile().z, it.player().tile().level + 1), false)
			}
		}
		r.onObject(2119) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3413 && obj.tile().z == 3540) {
				Ladders.ladderUp(it, Tile(3417, it.player().tile().z, it.player().tile().level + 1), false)
			}
		}
		r.onObject(2120) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3415 && obj.tile().z == 3540) {
				Ladders.ladderDown(it, Tile(3412, it.player().tile().z, it.player().tile().level - 1), false)
			}
		}
	}
}