package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

object Dummy {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcSpawn(2668) @Suspendable {
			val npc = it.npc()
			npc.lockDamageOk()
		}
	}
	
}