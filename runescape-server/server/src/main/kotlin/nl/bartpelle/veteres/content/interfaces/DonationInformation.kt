package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository


/**
 * Created by Situations on 2/18/2016.
 */

object DonationInformation {
	
	val DONATION_MANAGER = 5156
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(DONATION_MANAGER) @Suspendable {
			it.player().world().definitions().get(NpcDefinition::class.java, DONATION_MANAGER).name = "Donation Manager"
			
			it.chatNpc("Hi ${it.player().name()}, how can I help you today?", DONATION_MANAGER, 588)
			when (it.options("<img=4> Donator", "<img=5> Super Donator", "<img=6> Extreme Donator", "<img=7> Legendary Donator", "<img=8> Master Donator")) {
				1 -> {
					donator(it)
				}
				2 -> {
					super_donator(it)
				}
				3 -> {
					extreme_donator(it)
				}
				4 -> {
					legendary_donator(it)
				}
				5 -> {
					master_donator(it)
				}
			}
			when (it.optionsTitled("What would you like to say?", "How can I donate?", "How much have I donated in total?", "Can I view donation benefits?", "Can I see your teleports?")) {
				1 -> {
					it.chatPlayer("How can I donate to OS-Scape?", 554)
					it.chatNpc("You can support the production of OS-Scape by purchasing items using our online store:<br> <col=ff3333>www.store.os-scape.com</col>.", DONATION_MANAGER, 588)
					it.chatNpc("All purchases from our web store are put towards running our project, purchasing advertisements to help the server grow, and rewards for community events.", DONATION_MANAGER, 588)
					it.chatNpc("By purchasing items from our store, you will achieve different ranks, titles, and perks. Would you like to view the different tiers and perks?", DONATION_MANAGER, 554)
					
				}
			}
			
		}
		
		// Sorry, I need this for the clan wars :(
		//r.onButton(92, 6) {
		//    it.player().interfaces().closeById(92)
		//}
	}
	
	
	fun donator(it: Script) {
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
		it.player().interfaces().sendMain(92)
		it.player().write(InterfaceText(92, 2, "<img=4> <col=ffffff><u=ffffff>Donator</u></col>"));
		it.player().invokeScript(554, 1, "<col=ffffff>Contribution:</col> You have donated a <col=009933>total of</col> $${it.player().totalSpent}0. <br> <br>"
				+ determine_requirement_text(it, 5, "$5.00")
				+ "<col=ffffff>Information:</u></col> The members of this user group are OS-Scape supporters. They've offere us a ton of support, so please pass on our appreciation if you see them online! <br><br> "
				+ "<col=ffffff><u=ffffff>Benefits:</u></col><br><br><left>"
				+ "<col=eeeeee>-</col> Announce a message once every 240 seconds.                          <br>"
				+ "<col=eeeeee>-</col> 10% extra blood money from wilderness player kills.                 <br>"
				+ "<col=eeeeee>-</col> 10% extra chance of pick locking doors.                             <br>"
				+ "<col=eeeeee>-</col> 10% extra blood money from a monster that drops it.                 <br>"
				+ "<col=eeeeee>-</col> 5% extra chance of getting a rare PVM drop.                        ")
		it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(85) }
	}
	
	fun super_donator(it: Script) {
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
		it.player().interfaces().sendMain(92)
		it.player().write(InterfaceText(92, 2, "<img=5> <col=ffffff><u=ffffff>Super Donator</u></col>"));
		it.player().invokeScript(554, 1, "<col=ffffff>Contribution:</col> You have donated a <col=009933>total of</col> $${it.player().totalSpent}0. <br> <br>"
				+ determine_requirement_text(it, 100, "$100.00")
				+ "<col=ffffff>Information:</u></col> The members of this user group are Super OS-Scape supporters. They've offere us a ton of support, so please pass on our appreciation if you see them online! <br><br> "
				+ "<col=ffffff><u=ffffff>Benefits:</u></col><br><br><left>"
				+ "<col=eeeeee>-</col> Announce a message once every 180 seconds.                          <br>"
				+ "<col=eeeeee>-</col> 25% extra blood money from wilderness player kills.                 <br>"
				+ "<col=eeeeee>-</col> 15% extra chance of pick locking doors.                             <br>"
				+ "<col=eeeeee>-</col> 25% extra blood money from a monster that drops it.                 <br>"
				+ "<col=eeeeee>-</col> 7% extra chance of getting a rare PVM drop.                        ")
		it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(85) }
	}
	
	fun extreme_donator(it: Script) {
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
		it.player().interfaces().sendMain(92)
		it.player().write(InterfaceText(92, 2, "<img=6> <col=ffffff><u=ffffff>Extreme Donator</u></col>"));
		it.player().invokeScript(554, 1, "<col=ffffff>Contribution:</col> You have donated a <col=009933>total of</col> $${it.player().totalSpent}0. <br> <br>"
				+ determine_requirement_text(it, 250, "$250.00")
				+ "<col=ffffff>Information:</u></col> The members of this user group are Extreme OS-Scape supporters. They've offere us a ton of support, so please pass on our appreciation if you see them online! <br><br> "
				+ "<col=ffffff><u=ffffff>Benefits:</u></col><br><br><left>"
				+ "<col=eeeeee>-</col> Announce a message once every 120 seconds.                          <br>"
				+ "<col=eeeeee>-</col> 50% extra blood money from wilderness player kills.                 <br>"
				+ "<col=eeeeee>-</col> 25% extra chance of pick locking doors.                             <br>"
				+ "<col=eeeeee>-</col> 50% extra blood money from a monster that drops it.                 <br>"
				+ "<col=eeeeee>-</col> 10% extra chance of getting a rare PVM drop.                        ")
		it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(85) }
	}
	
	fun legendary_donator(it: Script) {
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
		it.player().interfaces().sendMain(92)
		it.player().write(InterfaceText(92, 2, "<img=7> <col=ffffff><u=ffffff>Legendary Donator</u></col>"));
		it.player().invokeScript(554, 1, "<col=ffffff>Contribution:</col> You have donated a <col=009933>total of</col> $${it.player().totalSpent}0. <br> <br>"
				+ determine_requirement_text(it, 500, "$500.00")
				+ "<col=ffffff>Information:</u></col> The members of this user group are Legendary OS-Scape supporters. They offer us a ton of support, so please pass on our appreciation if you see them online! <br><br> "
				+ "<col=ffffff><u=ffffff>Benefits:</u></col><br><br><left>"
				+ "<col=eeeeee>-</col> Announce a message once every 60 seconds.                           <br>"
				+ "<col=eeeeee>-</col> 75% extra blood money from wilderness player kills.                 <br>"
				+ "<col=eeeeee>-</col> 50% extra chance of pick locking doors.                             <br>"
				+ "<col=eeeeee>-</col> 75% extra blood money from a monster that drops it.                 <br>"
				+ "<col=eeeeee>-</col> 15% extra chance of getting a rare PVM drop.                        ")
		it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(85) }
	}
	
	fun master_donator(it: Script) {
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
		it.player().interfaces().sendMain(92)
		it.player().write(InterfaceText(92, 2, "<img=8> <col=ffffff><u=ffffff>Master Donator</u></col>"));
		it.player().invokeScript(554, 1, "<col=ffffff>Contribution:</col> You have donated a <col=009933>total of</col> $${it.player().totalSpent}0. <br> <br>"
				+ determine_requirement_text(it, 1000, "$1000.00")
				+ "<col=ffffff>Information:</u></col> The members of this user group are Master OS-Scape supporters. They offer us a ton of support, so please pass on our appreciation if you see them online! <br><br> "
				+ "<col=ffffff><u=ffffff>Benefits:</u></col><br><br><left>"
				+ "<col=eeeeee>-</col> Announce a message once every 25 seconds.                           <br>"
				+ "<col=eeeeee>-</col> Double blood money from wilderness player kills.                    <br>"
				+ "<col=eeeeee>-</col> 75% additional chance of pick locking doors.                        <br>"
				+ "<col=eeeeee>-</col> Double blood money from an monster that drops it.                   <br>"
				+ "<col=eeeeee>-</col> 20% extra chance of getting a rare PVM drop.                        ")
		it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(85) }
	}
	
	fun determine_requirement_text(it: Script, amt: Int, amt_text: String): String {
		if (amt < it.player().totalSpent) {
			return "<str><col=ffffff>Requirement:</col> Must have donated <col=ff3333>at least</col> $amt_text. <br> <br>"
		} else {
			return "<col=ffffff>Requirement:</col> Must have donated <col=ff3333>at least</col> $amt_text. <br> <br>"
		}
		return "<col=ffffff>Requirement:</col> Must have donated <col=ff3333>at least</col> $amt_text. <br> <br>"
	}
}