package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild.GuildEntrance
import nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild.WarriorDoubleDoor
import nl.bartpelle.veteres.content.areas.falador.MiningGuild
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.areas.varrock.CookingGuild
import nl.bartpelle.veteres.content.areas.wilderness.PirateHut
import nl.bartpelle.veteres.content.interactionObject

import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.skills.agility.WildyCourse
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.EquipmentInfo
import org.apache.logging.log4j.LogManager
import java.io.DataInputStream
import java.io.FileInputStream
import java.util.*

/**
 * Created by Bart on 11/26/2015.
 */
object Doors {
	
	private val logger = LogManager.getLogger(EquipmentInfo::class.java)
	
	class DoorPair(val id: Int, val toId: Int, val closed: Boolean, val open: Boolean)
	
	
	var MAP_BY_DOOR: Map<Int, DoorPair>? = null
	
	@JvmStatic val DOORSET: ArrayList<DoorPair> = arrayListOf()
	
	@JvmStatic fun attempt_default_door(it: Script): Boolean {
		val obj = it.interactionObject()
		var done = booleanArrayOf(false)
		DOORSET.forEach { door ->
			if (done[0]) {
				// empty - already handled
			} else if (door.id == obj.id()) {
				if (door.closed) {
					openDoor(it.player().world(), obj, door.toId, it)
					done[0] = true
				} else if (door.open) {
					closeDoor(it.player().world(), obj, door.toId, it)
					done[0] = true
				}
			}
		}
		return done[0]
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		val have_scripts = intArrayOf(WildyCourse.LOWER_GATE, WildyCourse.UPPERGATE_EAST, WildyCourse.UPPERGATE_WEST, CookingGuild.GUILD_DOOR, 11728, 24057,
				24058, 11727, 7426, 24309, 24306, 24318, 2108, 2111, 2112, 2113)
		
		// Read door binary
		val input = DataInputStream(FileInputStream("data/map/doorpairs.bin"))
		logger.info("Loaded " + input.available() / 10 + " door pairs.")
		while (input.available() > 0) {
			val id = input.readInt()
			val toid = input.readInt()
			val closed = input.readBoolean()
			val open = input.readBoolean()
			
			DOORSET.add(DoorPair(id, toid, closed, open))
		}
		
		MAP_BY_DOOR = DOORSET.toList().map { d -> Pair(d.id, d) }.toMap() + DOORSET.toList().map { d -> Pair(d.toId, d) }.toMap()
		
		// Add hooks for all entries.
		for (door in DOORSET) {
			val id = door.id
			val closed = door.closed
			
			val toid = door.toId
			val open = door.open
			
			if (id in have_scripts) {
				// These must have their own hooks.
				continue
			}
			
			r.onObject(id, s@ {
				// You cannot have "return" implementations here as you can only assign one script to a object id. Don't use this hook at all!
				val obj: MapObj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
				
				if (closed) {
					if (!handle_sister_door(it, door, true)) {
						openDoor(it.player().world(), obj, toid, it)
					}
					
				} else if (open) {
					if (!handle_sister_door(it, door, false)) {
						closeDoor(it.player().world(), obj, toid, it)
					}
				} else {
					it.player().message("This door doesn't want to budge!")
					println("Unknown door handler.")
				}
			})
		}
		
		// Override the default stuff above.
		r.onObject(11727) @Suspendable {
			val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
			val instanced = it.player() in MageBankInstance
			
			if (obj.tile().distance(Tile(3041, 3957)) <= 10 || instanced) { //Pirate hut doors
				val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
				if (option == 1)
					PirateHut.PirateHutFirst(it)
				if (option == 2)
					PirateHut.pirateHutSecond(it)
			}
		}
		
		r.onObject(7426) { MiningGuild.door(it) } //Mining guild
		r.onObject(24309) { WarriorDoubleDoor.door(it) } //Warriors guild
		r.onObject(24306) { WarriorDoubleDoor.door(it) } //Warriors guild
		r.onObject(24318) { GuildEntrance.door(it) } //Warriors guild
		
		// Small fence at Neitiznot's Yak (Jak hehe?) enclosure
		r.onObject(21600) { openDoor(it.player.world(), it.interactionObject(), 21601, it) }
		r.onObject(21601) { closeDoor(it.player.world(), it.interactionObject(), 21600, it) }
	}
	
	// Identifies any sister doors (double door pairs) and then opens/closes them both in the valid direction.
	private fun handle_sister_door(it: Script, door1_pairdef: DoorPair, opening: Boolean): Boolean {
		val door1 = it.interactionObject() // Original door
		val doorlink = door1.attribOr<DoubleDoorLink?>(AttributeKey.DOUBLE_DOOR_LINK, null)
		val singleDoor = doorlink == null
		
		if (!opening) { // Trying to close it. Check anti-rag mechanic
			val openCycleIds: MutableList<Int> = door1.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
			door1.putattrib(AttributeKey.DOOR_USES, openCycleIds)
			
			val cur: Int = it.player().world().cycleCount()
			openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
			
			if (openCycleIds.size >= 10) {
				it.player().message("The ${if (singleDoor) "door is" else "doors are"} stuck!")
				return true
			}
			openCycleIds.add(cur)
		}
		
		if (doorlink != null) {
			handle_doubledoor_using_attrib(it, doorlink, opening)
			it.sound(62)
			return true
		} else {
			if (find_double_door_pair(it, door1, door1_pairdef, opening)) {
				val link = door1.attribOr<DoubleDoorLink?>(AttributeKey.DOUBLE_DOOR_LINK, null)
				if (link != null) {
					handle_doubledoor_using_attrib(it, link, opening)
					it.sound(62)
					return true
				}
			}
		}
		return false
	}
	
	private fun find_double_door_pair(it: Script, door1: MapObj, door1_pairdef: DoorPair, opening: Boolean): Boolean {
		var door2: MapObj? = null
		val world = it.player().world()
		val opStr = if (opening) "Open" else "Close"
		
		if (opening) {
			// This door is currently closed, check such relevent positions.
			when (door1.rot()) {
				0, 2 -> {
					// A vertical pair, north/south door
					door2 = it.player().world().allObjsByType(door1.type(), door1.tile().transform(0, -1)).find {
						o ->
						o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
					} // try south
					if (door2 == null)
						door2 = it.player().world().allObjsByType(door1.type(), door1.tile().transform(0, 1)).find {
							o ->
							o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
						} // try north
					
					//val nigger = it.player().world().allObjsByType(door1.type(), door1.tile().transform(0,1))
					//nigger.forEach { o -> it.message("Sub1 %s %s at %s to %s", o.definition(world).name, Arrays.toString(o.definition(world).options), o.tile().toStringSimple(), opStr) }
				}
				1, 3 -> {
					// A horizontal pair, east OR west door
					door2 = it.player().world().allObjsByType(door1.type(), door1.tile().transform(-1, 0)).find {
						o ->
						o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
					} // try west
					if (door2 == null)
						door2 = it.player().world().allObjsByType(door1.type(), door1.tile().transform(1, 0)).find {
							o ->
							o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
						} // try east
				}
			}
		} else {
			// This door is currently OPEN, look for sister also open.
			when (door1.rot()) {
				0, 2 -> {
					// A horizontal pair, east/west door
					door2 = it.player().world().allObjsByType(door1.type(), door1.tile().transform(-1, 0)).find {
						o ->
						o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
					} // try west
					if (door2 == null)
						door2 = it.player().world().allObjsByType(door1.type(), door1.tile().transform(1, 0)).find {
							o ->
							o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
						} // try east
				}
				1, 3 -> {
					// A vertical pair, north/south door
					door2 = it.player().world().allObjsByType(door1.type(), door1.tile().transform(0, -1)).find {
						o ->
						o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
					} // try south
					if (door2 == null)
						door2 = it
								.player().world()
								.allObjsByType(
										door1.type(),
										door1.tile().transform(0, 1)
								)
								.find {
									o ->
									o?.definition(world)?.options != null && o.tile().distance(door1.tile()) < 2 && o.definition(world).options.any { s -> s != null && s.equals(opStr) }
								} // try north
				}
			}
		}
		door2 ?: return false
		//it.player().debug("Sister door found - opts are ${Arrays.toString(door2.definition(it.player().world()).options)} at ${door2.tile().toStringSimple()}")
		
		// Get the doorpair definition of the sister door.
		val door2_pairdef = MAP_BY_DOOR?.get(door2.id()) ?: return false
		
		// Changes to be made.
		var new_door1_dir = -1
		var new_door2_dir = -1
		var new_door1_pos = door1.tile()
		var new_door2_pos = door2.tile()
		
		// Identify what dir/tile the doors move to.
		if (door1.tile().x > door2.tile().x && door1.tile().z == door2.tile().z) {
			// Door1 = East door, horizontal pair
			new_door1_dir = if (opening) 2 else 1
			new_door1_pos = if (opening) door1.tile().transform(0, if (door1.rot() == 3) -1 else 1) else door1.tile().transform(0, -1)
			new_door2_dir = if (opening) 0 else 1
			new_door2_pos = if (opening) door2.tile().transform(0, if (door1.rot() == 3) -1 else 1) else door2.tile().transform(0, -1)
		} else if (door1.tile().x < door2.tile().x && door1.tile().z == door2.tile().z) {
			// Door1 = West door, horizontal pair
			new_door1_dir = if (opening) 0 else 1
			new_door1_pos = if (opening) door1.tile().transform(0, if (door1.rot() == 3) -1 else 1) else door1.tile().transform(0, -1)
			new_door2_dir = if (opening) 2 else 1
			new_door2_pos = if (opening) door2.tile().transform(0, if (door1.rot() == 3) -1 else 1) else door2.tile().transform(0, -1)
		} else if (door1.tile().z > door2.tile().z && door1.tile().x == door2.tile().x) {
			// Door1= North door, vertical pair
			new_door1_dir = if (opening) 1 else 0
			new_door1_pos = if (opening) door1.tile().transform(if (door1.rot() == 2) 1 else -1, 0) else door1.tile().transform(1, 0)
			new_door2_dir = if (opening) 3 else 0
			new_door2_pos = if (opening) door2.tile().transform(if (door1.rot() == 2) 1 else -1, 0) else door2.tile().transform(1, 0)
		} else if (door1.tile().z < door2.tile().z && door1.tile().x == door2.tile().x) {
			// Door1= South door, vertical pair
			new_door1_dir = if (opening) 3 else 0
			new_door1_pos = if (opening) door1.tile().transform(if (door1.rot() == 2) 1 else -1, 0) else door1.tile().transform(1, 0)
			new_door2_dir = if (opening) 1 else 0
			new_door2_pos = if (opening) door2.tile().transform(if (door1.rot() == 2) 1 else -1, 0) else door2.tile().transform(1, 0)
		} else {
			//System.err.println("Door sister mismatch! "+door1.tile().toStringSimple()+" vs "+door2.tile().toStringSimple())
			it.message("That's odd! These doors are not quite right.")
			return false
		}
		
		// Check if something broke
		if (new_door1_dir == -1 || new_door2_dir == -1) { // Shouldn't happen. Return true so single door mechanic doesn't execute.
			it.message("That's odd... they won't budge!")
			return false
		}
		
		// Create store info
		val d1state = DoorState(door1.id(), door1.tile(), door1.rot(), door1_pairdef.toId, new_door1_pos, new_door1_dir)
		val d2state = DoorState(door2.id(), door2.tile(), door2.rot(), door2_pairdef.toId, new_door2_pos, new_door2_dir)
		
		// Store attribs
		door1.putattrib(AttributeKey.DOUBLE_DOOR_LINK, DoubleDoorLink(d1state, d2state))
		door2.putattrib(AttributeKey.DOUBLE_DOOR_LINK, DoubleDoorLink(d2state, d1state))
		return true
	}
	
	private fun handle_doubledoor_using_attrib(it: Script, doorlink: DoubleDoorLink, opening: Boolean) {
		val world = it.player().world()
		val door1 = it.interactionObject()
		val left = doorlink.door1_state
		val right = doorlink.door2_state
		
		// Identify which door the one we're interacting with is, either the left or right of a pair. Also open or closed.
		
		if (door1.id() == left.state1_id && door1.rot() == left.state1_rot) {
			// Maybe the closed state, left door
			door1.replaceWith(MapObj(left.state2_tile, left.state2_id, door1.type(), left.state2_rot), world, true)
			// Replace the sister door
			world.objById(right.state1_id, right.state1_tile)?.replaceWith(MapObj(right.state2_tile, right.state2_id, door1.type(), right.state2_rot), world, true)
			
		} else if (door1.id() == left.state2_id && door1.rot() == left.state2_rot) {
			// Maybe the open state, left door.
			door1.replaceWith(MapObj(left.state1_tile, left.state1_id, door1.type(), left.state1_rot), world, true)
			// Replace sister
			world.objById(right.state2_id, right.state2_tile)?.replaceWith(MapObj(right.state1_tile, right.state1_id, door1.type(), right.state1_rot), world, true)
			
		} else if (door1.id() == right.state1_id && door1.rot() == right.state1_rot) {
			// Maybe the closed state, right door
			door1.replaceWith(MapObj(right.state2_tile, right.state2_id, door1.type(), right.state2_rot), world, true)
			// Replace the sister door
			world.objById(left.state1_id, left.state1_tile)?.replaceWith(MapObj(left.state2_tile, left.state2_id, door1.type(), left.state2_rot), world, true)
			
		} else if (door1.id() == right.state2_id && door1.rot() == right.state2_rot) {
			// Maybe the open state, right door.
			door1.replaceWith(MapObj(right.state1_tile, right.state1_id, door1.type(), right.state1_rot), world, true)
			// Replace sister
			world.objById(left.state2_id, left.state2_tile)?.replaceWith(MapObj(left.state1_tile, left.state1_id, door1.type(), left.state1_rot), world, true)
			
		} else {
			it.message("The door doesn't seem to move.")
		}
	}
	
	fun openDoor(world: World, obj: MapObj, newId: Int, it: Script, inverse: Boolean): MapObj {
		if (obj.id() == 9398 && obj.tile().equals(3098, 3107)) {
			it.message("You need to talk to the Guide to proceed.")
			return obj
		}
		var orientation = obj.rot()
		
		val flipped = obj.definition(world).vflip
		if (flipped) {
			orientation -= 1
		} else {
			orientation += 1
		}
		orientation = orientation and 3
		
		val target = if (inverse) inverseDirectionTile(obj.tile(), orientation) else
			directionTile(obj.tile(), orientation)
		
		val replacement = MapObj(target, newId, obj.type(), orientation)
		
		// Mechanic for gates getting stuck.
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		openCycleIds.add(world.cycleCount())
		replacement.putattrib(AttributeKey.DOOR_USES, openCycleIds)
		
		obj.replaceWith(replacement, world, true)
		it.sound(62)
		return obj
	}
	
	fun openDoor(world: World, obj: MapObj, newId: Int, it: Script): MapObj {
		return openDoor(world, obj, newId, it, true)
	}
	
	fun closeDoor(world: World, obj: MapObj, newId: Int, it: Script, targetCalibration: Boolean, flipFlag: Boolean): MapObj {
		val openCycleIds: MutableList<Int> = obj.attribOr<MutableList<Int>>(AttributeKey.DOOR_USES, mutableListOf<Int>())
		obj.putattrib(AttributeKey.DOOR_USES, openCycleIds)
		
		val cur: Int = world.cycleCount()
		openCycleIds.removeAll { p -> p < (cur - 50) } // Remove old "open times" older than 30 seconds ago
		
		if (openCycleIds.size >= 10) {
			// Stuck message.
			return obj
		}
		openCycleIds.add(cur)
		
		var orientation = obj.rot();
		val face = orientation;
		if (flipFlag) {
			val flipped = obj.definition(world).vflip
			if (flipped) {
				orientation += 1;
			} else {
				orientation -= 1;
			}
		}
		orientation = orientation and 3
		val target = if (targetCalibration) directionTile(obj.tile(), face) else inverseDirectionTile(obj.tile(), face)
		val replacement = MapObj(target, newId, obj.type(), orientation)
		
		obj.replaceWith(replacement, world, true)
		it.sound(60)
		return replacement
	}
	
	fun closeDoor(world: World, obj: MapObj, newId: Int, it: Script) = closeDoor(world, obj, newId, it, true, true)
	
	fun inverseDirectionTile(t: Tile, dir: Int): Tile {
		return when (dir) {
			0 -> t.transform(0, -1, 0) // North -> South
			1 -> t.transform(-1, 0, 0) // NorthEast -> SouthWest
			2 -> t.transform(0, 1, 0) // East -> West
			3 -> t.transform(1, 0, 0) // SouthEast -> NorthWest
			else -> t
		}
	}
	
	fun directionTile(t: Tile, dir: Int): Tile {
		return when (dir) {
			0 -> t.transform(0, 1, 0) // North -> South
			1 -> t.transform(1, 0, 0) // NorthEast -> SouthWest
			2 -> t.transform(0, -1, 0) // East -> West
			3 -> t.transform(-1, 0, 0) // SouthEast -> NorthWest
			else -> t
		}
	}
	
}