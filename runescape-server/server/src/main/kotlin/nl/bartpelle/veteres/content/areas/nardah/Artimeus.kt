package nl.bartpelle.veteres.content.areas.nardah

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 15/06/2016.
 */
object Artimeus {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption4(1350) @Suspendable {
			it.player().world().shop(43).display(it.player())
		}
		repo.onNpcOption1(1350) @Suspendable {
			it.chatNpc("Hello there. Would you like to take a look at my Hunting supplies? You may find my bulk stocks quite appealing.", 1350)
			it.chatPlayer("Show me what you've got!")
			it.player().world().shop(43).display(it.player())
		}
	}
}