package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Ryley Kimmel on 11/27/2016.
 */
object TransformationRing {
	
	enum class Ring(val itemId: Int, vararg val npcIds: Int) {
		RING_OF_STONE(6583, 2188),
		RING_OF_NATURE(20005, 7314),
		RING_OF_COINS(20017, 7315),
		EASTER_RING(7927, 5538, 5539, 5540, 5541, 5542, 5543)
	}
	
	const val LOGOUT_TAB_INTERFACE = 375
	const val UNMORPH = 5
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		Ring.values().forEach { ring ->
			r.onItemOption2(ring.itemId) @Suspendable {
				val player = it.player
				
				player.stopActions(true)
				player.looks().transmog(player.world().random(ring.npcIds))
				player.write(InvokeScript(InvokeScript.OPEN_TAB, 10))
				player.interfaces().sendWidgetOn(LOGOUT_TAB_INTERFACE, Interfaces.InterSwitches.LOGOUT_TAB)
				it.onInterrupt { unmorph(player) }
				it.delay(Integer.MAX_VALUE)
			}
		}
		
		r.onButton(LOGOUT_TAB_INTERFACE, UNMORPH) @Suspendable { unmorph(it.player) }
	}
	
	fun unmorph(player: Player) {
		player.looks().transmog(-1)
		player.interfaces().sendDefaultInterlink(Interfaces.InterSwitches.LOGOUT_TAB)
	}
	
}
