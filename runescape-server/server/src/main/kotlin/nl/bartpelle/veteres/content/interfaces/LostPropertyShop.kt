package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Varps
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow

/**
 * Created by Jak on 18/04/2016.
 */
object LostPropertyShop {
	
	// Note: max items per bit range is 8. Maximum bit to use is 28 within an integer.
	enum class PerduVarbits(val itemid: Int, val shift: Int, val key: AttributeKey) {
		VOIDTOP(8839, 0, AttributeKey.PERDU_VARBIT1),
		VOIDLEGS(8840, 4, AttributeKey.PERDU_VARBIT1),
		VOIDGLOVES(8842, 8, AttributeKey.PERDU_VARBIT1),
		VOIDMAGE(11663, 12, AttributeKey.PERDU_VARBIT1),
		VOIDRANGE(11664, 16, AttributeKey.PERDU_VARBIT1),
		VOIDMELEE(11665, 20, AttributeKey.PERDU_VARBIT1),
		ELITETOP(13072, 24, AttributeKey.PERDU_VARBIT1),
		ELITELEGS(13073, 28, AttributeKey.PERDU_VARBIT1),
		DRAG_DEFENDER(12954, 0, AttributeKey.PERDU_VARBIT2),
		FIRECAPE(6570, 4, AttributeKey.PERDU_VARBIT2),
		NORM_MAXCAPE(13280, 8, AttributeKey.PERDU_VARBIT2),
		FIRE_MAXCAPE(13329, 12, AttributeKey.PERDU_VARBIT2),
		AVA_MAXCAPE(13337, 16, AttributeKey.PERDU_VARBIT2),
		ZAM_MAXCAPE(13333, 20, AttributeKey.PERDU_VARBIT2),
		GUTH_MAXCAPE(13335, 24, AttributeKey.PERDU_VARBIT2),
		SARA_MAXCAPE(13331, 28, AttributeKey.PERDU_VARBIT2),
		HEALER_HAT(10547, 0, AttributeKey.PERDU_VARBIT3),
		FIGHTER_HAT(10548, 4, AttributeKey.PERDU_VARBIT3),
		RUNNER_HAT(10549, 8, AttributeKey.PERDU_VARBIT3),
		RANGER_HAT(10550, 12, AttributeKey.PERDU_VARBIT3),
		TORSO(10551, 16, AttributeKey.PERDU_VARBIT3),
		RUNNER_BOOTS(10552, 20, AttributeKey.PERDU_VARBIT3),
		INFERNO_MAX_CAPE(21285, 24, AttributeKey.PERDU_VARBIT3),
		INFERNO_CAPE(21295, 0, AttributeKey.PERDU_VARBIT4)
		
		// Dragon defender (t) NOT supported here - because it is converted to the normal defender on death. Special case.
		;
		
		companion object {
			fun forItem(id: Int): PerduVarbits? {
				for (pv in PerduVarbits.values()) {
					if (pv.itemid == id)
						return pv
				}
				if (id != 19722) // Drag defender (t) is a special case, does not have it's own varbit save. It converts to the normal defender.
					System.out.println("CRITICAL - no Perdu Varbit for item id " + id);
				return null;
			}
		}
	}
	
	// Maximum bit range covered
	val MAX_BITSIZE = 15
	
	@JvmField val hint_fade_script: Function1<Script, Unit> = s@ @Suspendable {
		it.delay(25) // 15s
		it.player().write(SetHintArrow(-1))
	}
	
	@JvmStatic fun altarSavedProperty(player: Player, itemId: Int, change: Int): Boolean {
		try {
			// Single attribute (32 bits) only supports 8 items (32/8=4 bits per item) can store up to 8 in quantity before overflowing.
			val info = PerduVarbits.forItem(itemId) ?: return false
			val key: AttributeKey = info.key
			val full = player.attribOr<Int>(key, 0)
			var old = full
			
			old = player.attribOr<Int>(key, 0).shr(info.shift).and(MAX_BITSIZE)
			if (old == 8 && change > 0) {
				player.message("You already have " + old + " x " + player.world().definitions().get(ItemDefinition::class.java, itemId)!!.name
						+ " in the lost property shop. It's gone for good now!")
				return false
			} else {
				var updated = Varps.updateValue(full, MAX_BITSIZE, old + change, info.shift)
				player.putattrib(key, updated)
				player.debug("full mask=%d, %d-chunk=%d, updated fullmask=%d (%d + %d = %d)", full, itemId, old, updated, old, change, change + old)
				return true
			}
		} catch (e: Exception) {
			// Gay ass niggers im 10/10 done
			return true
		}
	}
	
	@JvmStatic fun getCount(player: Player, itemId: Int): Int {
		val info: PerduVarbits = PerduVarbits.forItem(itemId) ?: return 0
		return player.attribOr<Int>(info.key, 0).shr(info.shift).and(MAX_BITSIZE)
	}
}