package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 2016-06-23.
 */

object Skotizo {
	
	/**
	 * Ranged attack: Throws rocks at every player Skotizo is visually capable of attacking. (20 damage cap)
	 * Magic/special attack: Grabs rocks from the sky which fall on all targetable players tiles and explode. (40 damage cap)
	 */
	
	
	val CHANT = "GAR MULNO FUL TAGLO!"
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 7) && EntityCombat.attackTimerReady(npc)) {
				if (EntityCombat.attackTimerReady(npc) && !npc.attribOr<Boolean>(AttributeKey.SKOTIZO_JUMP_STATE, false)) {
					val random = npc.world().random(5)
					
					/* when (random) {
						 1 -> jump_attack(it, npc, target)
 
						 else -> {
							 if (EntityCombat.canAttackMelee(npc, target, false)) melee_attack(npc, target)
							 else if (npc.world().rollDie(2, 1)) magic_attack(npc, target) else ranged_attack(npc, target)
						 }
					 }*/
					
					when (random) {
						1 -> ranged_attack(npc, target)
						2 -> magic_attack(npc, target)
						else -> if (EntityCombat.canAttackMelee(npc, target, false)) melee_attack(npc, target)
						else if (npc.world().rollDie(2, 1)) magic_attack(npc, target) else ranged_attack(npc, target)
					}
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@JvmField val hitScript: Function1<Script, Unit> = @Suspendable {
		if (it.npc().world().rollDie(10, 1)) {
			val dmger: Entity? = it.npc().attrib<Player>(AttributeKey.LAST_DAMAGER)
			
			if (dmger != null && dmger is Player && !dmger.finished() && dmger.tile().distance(it.npc().tile()) <= 20) {
				// Display a message if we're not skulled yet.
				if (!Skulling.skulled(dmger)) {
					dmger.message("Skotizo has skulled you!")
				}
				
				// Update skull timer.
				Skulling.assignSkullState(dmger)
			}
		}
	}
	
	
	/**
	 * Handles the melee attack
	 */
	fun melee_attack(npc: Npc, target: Entity) {
		val tile = target.tile()
		
		target.world().players().forEachKt({ player ->
			if (tile.inSqRadius(player.tile(), 2)) {
				if (EntityCombat.attemptHit(npc, player, CombatStyle.MELEE))
					player.hit(npc, EntityCombat.randomHit(npc)) else player.hit(npc, 0)
			}
		})
		
		npc.animate(npc.attackAnimation())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	/**
	 * Handles the magic attack
	 */
	fun magic_attack(npc: Npc, target: Entity) {
		val tile = target.tile()
		
		target.world().players().forEachKt({ player ->
			if (tile.inSqRadius(player.tile(), 3)) {
				val tileDist = npc.tile().transform(3, 3, 0).distance(player.tile())
				val delay = Math.max(1, (30 + (tileDist * 12)) / 30)
				
				npc.world().spawnProjectile(npc, player, 165, 80, 30, 20, 12 * tileDist, 14, 5)
				
				if (EntityCombat.attemptHit(npc, player, CombatStyle.MAGIC)) {
					player.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(166, 90, 0))
				} else {
					player.hit(npc, 0, delay.toInt()).graphic(Graphic(85, 90, 0))
				}
			}
		})
		
		npc.animate(69)
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	/**
	 * Handles the ranged attack
	 */
	fun ranged_attack(npc: Npc, target: Entity) {
		val tile = target.tile()
		
		target.world().players().forEachKt({ player ->
			if (tile.inSqRadius(player.tile(), 3)) {
				val tileDist = npc.tile().transform(3, 3, 0).distance(player.tile())
				val delay = Math.max(1, (30 + (tileDist * 12)) / 30)
				
				npc.world().spawnProjectile(npc, player, 1242, 80, 50, 20, 12 * tileDist, 14, 5)
				
				if (EntityCombat.attemptHit(npc, player, CombatStyle.RANGE)) {
					player.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.RANGE).graphic(Graphic(1243, 80, 0))
				} else {
					player.hit(npc, 0, delay.toInt()).graphic(Graphic(1243, 80, 0))
				}
			}
		})
		
		npc.animate(69)
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	/**
	 * Handles the jump attack
	 */
	
	@Suspendable fun jump_attack(it: Script, npc: Npc, target: Entity) {
		
		npc.lockNoDamage()
		npc.sync().shout(CHANT)
		it.delay(1)
		val jump_destination = Tile(target.tile().x - 1, target.tile().z - 1)
		it.npc().sync().animation(4679, 5)
		it.delay(1)
		npc.putattrib(AttributeKey.SKOTIZO_JUMP_STATE, true)
		npc.teleport(jump_destination)
		npc.clearattrib(AttributeKey.SKOTIZO_JUMP_STATE)
		npc.face(target)
		npc.unlock()
		
		npc.world().players().forEachKt({ p ->
			if (p.tile().inSqRadius(npc.tile(), 2)) {
				
				p.executeScript @Suspendable { s ->
					val direction: Direction = Direction.of(p.tile().x - npc.tile().x,
							p.tile().z - npc.tile().z)
					val tile: Tile = p.tile().transform(direction.x * -3, direction.y * -3)
					val face: FaceDirection = FaceDirection.forTargetTile(npc.tile(), p.tile())
					val movement: ForceMovement = ForceMovement(0, 0, direction.x * -3, direction.y * -3, 20, 50, face)
					
					p.lock()
					p.message("Skotizo lands and the shockwave throws you backwards.")
					p.forceMove(movement)
					p.animate(734)
					s.delay(2)
					p.teleport(tile)
					s.delay(2)
					p.unlock()
					p.hit(p, p.world().random(15))
				}
			}
		})
		
		npc.clearattrib(AttributeKey.SKOTIZO_JUMP_STATE)
		it.delay(4)
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
}
