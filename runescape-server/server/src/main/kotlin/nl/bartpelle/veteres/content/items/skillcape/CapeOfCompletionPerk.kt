package nl.bartpelle.veteres.content.items.skillcape

import nl.bartpelle.skript.Script

/**
 * Created by Jason MacKeigan on 2016-07-04.
 */
abstract class CapeOfCompletionPerk(val options: IntArray) {
	
	abstract fun option(option: Int): Function1<Script, Unit>
	
}