package nl.bartpelle.veteres.content.quests

import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Bart on 4/6/2016.
 */
object Quests {
	
	val QUEST_POINTS_VARP = 101
	
	fun givePoints(player: Player, points: Int) {
		player.varps().varp(QUEST_POINTS_VARP, player.varps().varp(QUEST_POINTS_VARP) + points)
	}
	
	fun points(player: Player): Int = player.varps().varp(QUEST_POINTS_VARP)
	
}