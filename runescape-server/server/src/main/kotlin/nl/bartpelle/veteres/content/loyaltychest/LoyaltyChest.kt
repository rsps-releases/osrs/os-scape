package nl.bartpelle.veteres.content.loyaltychest

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.LoyaltyRewards
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.TimedList
import java.util.concurrent.TimeUnit

object LoyaltyChest {

    private const val INTERFACE_ID = 264
    private const val LOYALTY_CHEST = 12309
    private val recentPlayers = TimedList()
    private val REWARDS = arrayOf(
            Item(13307, 500),
            Item(13307, 1500),
            Item(13307, 2500),
            Item(13307, 4000),
            Item(6199, 2),
            Item(13307, 5500),
            Item(13307, 7000),
            Item(13307, 8500),
            Item(13307, 10000),
            Item(19566, 2)
    )

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onObject(LOYALTY_CHEST) @Suspendable {
            val player = it.player()
            player.faceObj(it.interactionObject())
            when(it.interactionOption()) {
                1 -> openChest(player)
                2 -> sendInformation(it)
            }
        }

        r.onTimer(TimerKey.LOYALTY_CHEST_TIMER) @Suspendable {
            if(it.player().timers().has(TimerKey.LOYALTY_CHEST_SPREE_TIMER))
                it.message(Color.COOL_BLUE.wrap("<img=76> You're able to claim your reward from the Loyalty Chest! Thanks for playing!"))
        }

        r.onTimer(TimerKey.LOYALTY_CHEST_SPREE_TIMER) @Suspendable {
            val player = it.player()
            if(player.interfaces().visible(INTERFACE_ID))
                openChest(player)
            player.putattrib(AttributeKey.LOYALTY_CHEST_DAY_REWARD, 1)
            player.putattrib(AttributeKey.LOYALTY_CHEST_SPREE, 0)
            it.message(Color.COOL_BLUE.wrap("<img=76> You have lost your Loyalty chest spree! Collect from the Loyalty Chest again to begin your spree!"))
        }

        // Claiming rewards
        r.onButton(INTERFACE_ID, 25) @Suspendable { claimReward(it, 1) }
        r.onButton(INTERFACE_ID, 27) @Suspendable { claimReward(it, 2) }
        r.onButton(INTERFACE_ID, 29) @Suspendable { claimReward(it, 3) }
        r.onButton(INTERFACE_ID, 31) @Suspendable { claimReward(it, 4) }
        r.onButton(INTERFACE_ID, 33) @Suspendable { claimReward(it, 5) }
        r.onButton(INTERFACE_ID, 35) @Suspendable { claimReward(it, 6) }
        r.onButton(INTERFACE_ID, 37) @Suspendable { claimReward(it, 7) }
        r.onButton(INTERFACE_ID, 39) @Suspendable { claimReward(it, 8) }
        r.onButton(INTERFACE_ID, 41) @Suspendable { claimReward(it, 9) }
        r.onButton(INTERFACE_ID, 43) @Suspendable { claimReward(it, 10) }
    }

    private fun openChest(player: Player) {
        if (player.interfaces().visible(264))
            player.interfaces().closeMain()
        player.interfaces().sendMain(264)
        player.write(LoyaltyRewards(getDayReward(player), getCurrentSpree(player), getHighestSpree(player), getTotalOpened(player), *REWARDS))
    }

    @Suspendable private fun claimReward(it: Script, requestedDayReward: Int) {
        val player = it.player()
        val dayReward = getDayReward(player)
        val reward = REWARDS[dayReward - 1]

        //Have they already claimed this reward?
        if(requestedDayReward < dayReward) {
            it.itemBox("You have already claimed this reward!", 744)
            return
        }

        //Is the requested reward unlocked yet?
        if(requestedDayReward > dayReward) {
            it.itemBox("You have not unlocked this reward yet!", 744)
            return
        }

        //Check for a free inventory slot
        val requiredSlots = if(reward.definition(player.world()).stackable()) 1 else reward.amount()
        if(player.inventory().freeSlots() < requiredSlots) {
            it.itemBox("You need at least $requiredSlots free inventory slot to claim your loyalty reward.", 744)
            return
        }

        //Are we able to collect the next reward?
        if(player.timers().has(TimerKey.LOYALTY_CHEST_TIMER)) {
            it.itemBox("You have to wait " + Color.COOL_BLUE.wrap(getRemainingTime(player)) + " until you're able to claim this reward.", 744)
            return
        }

        //Prevent abuse from multiple accounts!
        if (recentPlayers.contains(player.ip(), System.currentTimeMillis(), 1440L)) {
            it.itemBox("You have already claimed your reward on another account in the past 24 hours.", 744)
            return
        }

        player.lock()
        player.putattrib(AttributeKey.LOYALTY_CHEST_COUNT, getTotalOpened(player) + 1)
        player.putattrib(AttributeKey.LOYALTY_CHEST_SPREE, getCurrentSpree(player) + 1)
        if(getCurrentSpree(player) > getHighestSpree(player))
            player.putattrib(AttributeKey.LOYALTY_CHEST_HIGHEST_SPREE, getCurrentSpree(player))
        player.putattrib(AttributeKey.LOYALTY_CHEST_DAY_REWARD, if(dayReward == 10) 1 else dayReward + 1)
        player.timers().register(TimerKey.LOYALTY_CHEST_TIMER, 144000) // 24 hours
        player.timers().register(TimerKey.LOYALTY_CHEST_SPREE_TIMER, 216000) // 36 hours
        recentPlayers.add(player.ip(), System.currentTimeMillis())
        player.inventory().addOrDrop(Item(reward.id(), reward.amount()), player)
        openChest(player)
        player.unlock()
        if(dayReward == 10)
            it.itemBox("Congratulations! You've collected Loyalty Rewards for 10 days in a row! Thank you for being such an amazing, loyal player.", reward.id(), reward.amount())
        else
            it.itemBox("Thank you so much for playing ${Color.COOL_BLUE.wrap("OS-Scape")}! We hope you have an amazing day!", reward.id(), reward.amount())
    }

    private fun sendInformation(it: Script) {
        sendScroll(it, "<col=800000>Loyalty Chests",
                "Loyalty Chests are our thank you for playing OS-Scape. Every",
                "24 hours you'll be able to claim a reward. Continually claiming",
                "your rewards will result in a spree. If you don't claim your reward",
                "within 12 hours of it being available, your spree will be reset",
                "and you'll start at the beginning of the reward table. These are",
                "the requirements for advancing reward tables:",
                "",
                "   Thank you for playing OS-Scape."
        )
    }

    private fun getDayReward(player: Player): Int {
        return player.attribOr(AttributeKey.LOYALTY_CHEST_DAY_REWARD, 1)
    }

    private fun getTotalOpened(player: Player): Int {
        return player.attribOr(AttributeKey.LOYALTY_CHEST_COUNT, 0)
    }

    private fun getHighestSpree(player: Player): Int {
        return player.attribOr(AttributeKey.LOYALTY_CHEST_HIGHEST_SPREE, 0)
    }

    private fun getCurrentSpree(player: Player): Int {
        return player.attribOr(AttributeKey.LOYALTY_CHEST_SPREE, 0)
    }

    private fun getRemainingTime(player: Player): String {
        val ms = player.timers().left(TimerKey.LOYALTY_CHEST_TIMER) * 600.toLong()
        val hours = TimeUnit.MILLISECONDS.toHours(ms)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(ms)
        return (if (hours >= 1) hours.toString() + " hour" + (if (hours > 1) "s" else "") + " and " else "") +
                Math.max(minutes - TimeUnit.HOURS.toMinutes(hours), 1) + " minute" +
                if (minutes - TimeUnit.HOURS.toMinutes(hours) > 1) "s" else ""
    }

    private fun sendScroll(it: Script, title: String, vararg lines: String) {
        if (it.player().interfaces().visible(275))
            it.player().interfaces().closeMain()
        it.player().interfaces().text(275, 2, title)
        var childId = 4
        it.player().interfaces().text(275, childId++, "")
        for (s in lines)
            it.player().interfaces().text(275, childId++, s)
        it.player().interfaces().sendMain(275)
    }

}