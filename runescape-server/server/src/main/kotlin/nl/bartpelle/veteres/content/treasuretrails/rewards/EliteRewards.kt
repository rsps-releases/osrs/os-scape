package nl.bartpelle.veteres.content.treasuretrails.rewards

import nl.bartpelle.veteres.content.mechanics.ServerAnnouncements
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets.clueScrollReward
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 2016-11-19.
 */

object EliteRewards {
	
	fun generateEliteReward(player: Player, source: Int) {
		val commonRewardAmount = player.world().random(3..5)
		for (i in 1..commonRewardAmount) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = CommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			
			if (player.world().realm().isPVP) {
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, player.world().random(reward.amount)), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
			}
		}
		
		//Roll for good items..
		if (player.world().rollDie(2, 1)) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			if (player.world().realm().isPVP) {
				
				//Change coins to blood money
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			//Shout our rare reward
			if (reward.probability < 100.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 15.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from an elite clue scroll!")
		}
		
		//If we're on the PVP world, automatically give something from the uncommon table..
		if (player.world().realm().isPVP) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			//Change coins to blood money
			if (reward.reward.id() == 995) {
				player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			//Shout our rare reward
			if (reward.probability < 100.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 15.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from an elite clue scroll!")
		}
	}
	
	enum class CommonRewards(val probability: Double, val reward: Item, val amount: IntRange = 1..1) {
		BLOOD_MONEY(100.0, Item(13307), 20000..20000),
	}
	
	enum class UncommonRewards(val probability: Double, val reward: Item) {
		DRAGON_FULL_HELM_ORNAMENT_KIT(100.0, Item(12538)),
		DRAGON_CHAINBODY_ORNAMENT_KIT(100.0, Item(12534)),
		DRAGON_PLATE_LEGS_ORNAMENT_KIT(100.0, Item(12536)),
		DRAGON_SQ_SHIELD_ORNAMENT_KIT(100.0, Item(12532)),
		DRAGON_SCIMITAR_ORNAMENT_KIT(100.0, Item(20002)),
		LIGHT_INFINITY_COLOUR_KIT(90.0, Item(12530)),
		DARK_INFINITY_COLOUR_KIT(90.0, Item(12528)),
		FURY_ORNAMENT_KIT(90.0, Item(12526)),
		MUSKETEER_HAT(80.0, Item(12351)),
		MUSKETEER_TABARD(80.0, Item(12441)),
		MUSKETEER_PANTS(80.0, Item(12443)),
		DRAGON_CANE(80.0, Item(12373)),
		BRIEFCASE(80.0, Item(12335)),
		SAGACIOUS_SPECTACLES(75.0, Item(12337)),
		TOP_HAT(70.0, Item(12432)),
		MONOCLE(70.0, Item(12353)),
		BIG_PIRATE_HAT(60.0, Item(12355)),
		DEERSTALKER(60.0, Item(12540)),
		BRONZE_DRAGON_MASK(60.0, Item(12363)),
		IRON_DRAGON_MASK(60.0, Item(12365)),
		STEEL_DRAGON_MASK(60.0, Item(12367)),
		MITHRIL_DRAGON_MASK(60.0, Item(12369)),
		BLACK_DRAGON_HIDE_BODY_G(60.0, Item(12381)),
		BLACK_DRAGON_HIDE_CHAPS_G(60.0, Item(12383)),
		BLACK_DRAGON_HIDE_BODY_T(60.0, Item(12385)),
		BLACK_DRAGON_HIDE_CHAPS_T(60.0, Item(12387)),
		RANGERS_TUNIC(50.0, Item(12596)),
		AFRO(50.0, Item(12430)),
		KATANA(50.0, Item(12357)),
		ROYAL_GOWN_TOP(50.0, Item(12393)),
		ROYAL_GOWN_BOTTOM(50.0, Item(12395)),
		ROYAL_GOWN_CROWN(50.0, Item(12397)),
		ROYAL_GOWN_SCEPTRE(50.0, Item(12439)),
		LAVA_DRAGON_MASK(40.0, Item(12371)),
		GILDED_FULL_HELM(30.0, Item(3486)),
		GILDED_PLATE_BODY(30.0, Item(3481)),
		GILDED_PLATE_LEGS(30.0, Item(3483)),
		GILDED_PLATE_SKIRT(30.0, Item(3485)),
		GILDED_KITE_SHIELD(30.0, Item(3488)),
		GILDED_MEDIUM_HELM(30.0, Item(20146)),
		GILDED_CHAIN_MAIL(30.0, Item(20149)),
		GILDED_SQUARE_SHIELD(30.0, Item(20152)),
		GILDED_TWO_HANDED(30.0, Item(20155)),
		GILDED_SPEAR(30.0, Item(20158)),
		GILDED_HASTA(30.0, Item(20161)),
		GILDED_BOOTS(20.0, Item(12391)),
		GILDED_SCIMITAR(20.0, Item(12389)),
		ARCEUUS_HOUSE_SCARF(20.0, Item(19943)),
		HOSIDIUS_HOUSE_SCARF(20.0, Item(19946)),
		LOVAKENGJ_HOUSE_SCARF(20.0, Item(19949)),
		PISCARILIUS_HOUSE_SCARF(20.0, Item(19952)),
		SHAYZIEN_HOUSE_SCARF(20.0, Item(19955)),
		BLACKSMITH_HELM(15.0, Item(19988)),
		BUCKET_HELM(15.0, Item(19991)),
		DARK_TUXEDO_JACKET(10.0, Item(19958)),
		DARK_TUXEDO_CUFFS(10.0, Item(19961)),
		DARK_TUXEDO_TROUSERS(10.0, Item(19964)),
		DARK_TUXEDO_SHOES(10.0, Item(19967)),
		DARK_TUXEDO_TIE(10.0, Item(19970)),
		LIGHT_TUXEDO_JACKET(10.0, Item(19973)),
		LIGHT_TUXEDO_CUFFS(10.0, Item(19976)),
		LIGHT_TUXEDO_TROUSERS(10.0, Item(19979)),
		LIGHT_TUXEDO_SHOES(10.0, Item(19982)),
		LIGHT_TUXEDO_TIE(10.0, Item(19985)),
		RANGER_GLOVES(5.0, Item(19994)),
		HOLY_WRAPS(5.0, Item(19997)),
		THIRD_AGE_MELEE_HELM(1.0, Item(10350)),
		THIRD_AGE_MELEE_BODY(1.0, Item(10348)),
		THIRD_AGE_MELEE_LEGS(1.0, Item(10346)),
		THIRD_AGE_MELEE_KITE(1.0, Item(10352)),
		THIRD_AGE_RANGE_COIF(1.0, Item(10334)),
		THIRD_AGE_RANGE_BODY(1.0, Item(10330)),
		THIRD_AGE_RANGE_LEGS(1.0, Item(10332)),
		THIRD_AGE_RANGE_VAMBS(1.0, Item(10336)),
		THIRD_AGE_RANGE_HAT(1.0, Item(10342)),
		THIRD_AGE_RANGE_TOP(1.0, Item(10338)),
		THIRD_AGE_RANGE_SKIRT(1.0, Item(10340)),
		THIRD_AGE_RANGE_AMULET(1.0, Item(10344)),
		THIRD_AGE_CLOAK(1.0, Item(12437)),
		THIRD_AGE_WAND(1.0, Item(12422)),
		THIRD_AGE_BOW(1.0, Item(12424)),
		THIRD_AGE_LONGSWORD(1.0, Item(12426))
	}
	
}
