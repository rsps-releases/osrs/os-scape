package nl.bartpelle.veteres.content.items.boxes

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 4/17/2016.
 */
object MysteryBoxEco {
	
	const val MYSTERY_BOX = 6199
	
	val MEDIUM_LOW = arrayOf<Item>(Item(11212, 75), // Dragon arrow
			Item(11836), // Bandos boots
			Item(19484, 50), // Dragon javelins
			Item(2579), // Wizard boots
			Item(4087), // Dragon platelegs
			Item(2362, 120), // Adamantite bar
			Item(9244, 400), // Dragon bolts (e)
			Item(12954), // Dragon defender
			Item(4585), // Dragon plateskirt
			Item(2615), // Rune platebody (g)
			Item(1632, 10), // Uncut dragonstone
			Item(2623), // Rune platebody (t)
			Item(2653), // Zamorak platebody
			Item(1618, 60), // Uncut diamonds
			Item(2661), // Saradomin platebody
			Item(2669), // Guthix platebody
			Item(19484, 75), // Dragon javelins
			Item(12626, 50), // Stamina potionms
			Item(2617), // Rune platelegs (g)
			Item(1780, 1250), // Flax
			Item(2625), // Rune platelegs (t)
			Item(1514, 200), // Magic logs
			Item(2655), // Zamorak platelegs
			Item(2663), // Saradomin platelegs
			Item(12696, 40), // Super combat pot
			Item(2671), // Guthix platelegs
			Item(2619), // Rune fullhelm (g)
			Item(1516, 400), // Yew logs
			Item(2627), // Rune fullhelm (t)
			Item(1748, 120), // Black dragonhide
			Item(2657), // Zamorak fullhelm
			Item(2665), // Saradomin fullhelm
			Item(3001, 110), // Snap dragons
			Item(2673), // Guthix fullhelm
			Item(1750, 140), // Red dragonhide
			Item(2621), // Rune kiteshield (g)
			Item(1778, 1000), // Bow string
			Item(2629), // Rune kiteshield (t)
			Item(1754, 260), // Green dragonhide
			Item(2659), // Zamorak kiteshield
			Item(454, 600), // Coal
			Item(2667), // Saradomin kiteshield
			Item(1752, 180), // Blue dragonhide
			Item(2675), // Guthix kiteshield
			Item(3025, 50), // Super restores
			Item(2441, 150), // Super str
			Item(2437, 150), // Super att
			Item(6686, 75), // Sara brews
			Item(12938, 75) // Zul-andra teleports
	)
	
	val MEDIUM = arrayOf<Item>(Item(11235), // Dark bow
			Item(2581), // Robin hood hat
			Item(4151), // Abyssal whip
			Item(384, 800), // Raw shark
			Item(386, 800), // Cooked shark
			Item(3025, 150), // Super restores
			Item(2441, 350), // Super str
			Item(2437, 350), // Super att
			Item(6686, 150), // Sara brews
			Item(2435, 150), // Prayer potion
			Item(11818, 1), // Godsword shard 1
			Item(11820, 1), // Godsword shard 2
			Item(19484, 300), // Dragon javelins
			Item(11822, 1), // Godsword shard 3
			Item(11905, 1), // Trident
			Item(3145, 180), // Karambwans
			Item(10551, 1), // Fighter torso
			Item(8851, 1000), // Warrior guild tokens
			Item(537, 125), // Dragon bones
			Item(10034, 350), // Red chins
			Item(868, 1500), // Rune knives
			Item(12361, 1), // Cat mask
			Item(12428, 1), // Penguin mask
			Item(11840, 1) // Dragon boots
	)
	
	val MEDIUM_HIGH = arrayOf<Item>(Item(6737), // Berserker ring
			Item(2364, 400), // Runite bars
			Item(12883), // Karil's set
			Item(12873), // Guthan set
			Item(12875), // Verac set
			Item(12877), // Dharok set
			Item(12879), // Torag set
			Item(12881), // Ahrim set
			Item(2577), // Ranger boots
			Item(12596), // Ranger tunic
			Item(3140), // Dragon chain
			Item(6733), // Archer ring
			Item(6731), // Seers ring
			Item(13233), // Smouldering stone
			Item(6735) // Warrior ring
	)
	
	val HIGH_LOW = arrayOf<Item>(Item(4566), // Rubber chicken
			Item(995, 3000000), // 3M coins
			Item(12637), // Saradomin halo
			Item(12638), // Zamorak halo
			Item(12639), // Guthix halo
			Item(995, 5000000), // 3M coins
			Item(6585), // Amulet of fury
			Item(6920), // Infinity boots
			Item(10836), // Silly jester hat
			Item(10837), // Silly jester top
			Item(10838), // Silly jester tights
			Item(10839), // Silly jester boots
			Item(13227), // Eternal crystal
			Item(13229), // Pegasian crystal
			Item(13231), // Primordial crystal
			Item(19484, 2000), // Dragon javelins
			Item(10840) // Jester stick
	)
	
	val HIGH = arrayOf<Item>(Item(12436), // Amulet of fury (or)
			Item(12004), // Kraken tentacle
			Item(13442, 600), // Anglerfish
			Item(6914), // Master wand
			Item(995, 6000000), // 6M coins
			Item(6889), // Mage's book
			Item(11771), // Archer ring (i)
			Item(11772), // Warrior ring (i)
			Item(11770), // Seers ring ring (i)
			Item(11773), // Berserker ring (i)
			Item(13036), // Gilded set (lg)
			Item(13038), // Gilded set (sk)
			Item(11284), // DFS
			Item(12833), // Holy elixir
			Item(6571), // Uncut onyx
			Item(13576) // Dragon warhammer
	)
	
	val HIGH_HIGH = arrayOf<Item>(
			Item(12821), // Spectral spirit shield
			Item(12825), // Arcane spirit shield
			Item(10330), // 3rd age range top
			Item(10332), // 3rd age range legs
			Item(10334), // 3rd age range coif
			Item(10336), // 3rd age range vanbraces
			Item(10338), // 3rd age robe top
			Item(10340), // 3rd age robe
			Item(10342), // 3rd age mage hat
			Item(10344), // 3rd age amulet
			Item(10346), // 3rd age platelegs
			Item(10348), // 3rd age platebody
			Item(10350), // 3rd age fullhelm
			Item(10352), // 3rd age kiteshield
			Item(9050), // Pharao's sceptre
			Item(1960), // Pumpkin (noted cos i can see retards eating it)
			Item(1962), // Easter egg (noted cos i can see retards eating it)
			Item(11826), // Arma helm
			Item(11838), // Saradomin sword
			Item(11832), // Bandos chestplate
			Item(11824), // Zammy spear
			Item(11834), // Bandos tassets
			Item(11828), // Arma chestplate
			Item(12474), // Arma skirt
			Item(1053), // Green h'ween
			Item(1055), // Blue h'ween
			Item(1057) // Red h'ween
	)
	
	val EXTREME = arrayOf<Item>(
			Item(12817), // Elysian spirit shield
			Item(13175), // H'ween set
			Item(962), // Xmas cracker
			Item(1050), // Santa hat
			Item(1419), // Scythe
			Item(1037), // Bunny ears
			Item(12422), // 3rd age wand
			Item(12424), // 3rd age bow
			Item(12426), // 3rd age sword
			Item(12437) // 3rd age cloak
	)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		if (!r.server().world().realm().isPVP) {
			r.onItemOption1(MYSTERY_BOX) @Suspendable {
				val items: Array<Item>
				if (it.player().world().rollDie(350, 1)) {
					items = EXTREME
				} else if (it.player().world().rollDie(90, 1)) {
					items = HIGH_HIGH
				} else if (it.player().world().rollDie(35, 1)) {
					items = HIGH
				} else if (it.player().world().rollDie(12, 1)) {
					items = HIGH_LOW
				} else if (it.player().world().rollDie(7, 1)) {
					items = MEDIUM_HIGH
				} else if (it.player().world().rollDie(3, 1)) {
					items = MEDIUM
				} else {
					items = MEDIUM_LOW
				}
				
				//if (forceRare) {
				//    items = MysteryBox.EXTREME
				//    forceRare = false
				//}
				
				val item = items.random()
				if (items === EXTREME || items === HIGH_HIGH) {
					it.player().world().broadcast("<col=1e44b3><img=22> " + it.player().name() + " got a " + item.unnote(it.player().world()).definition(it.player().world()).name + " from a Mystery Box!")
				}
				
				it.player().message("You open the Mystery Box and find... " + item.unnote(it.player().world()).definition(it.player().world()).name + " x " + item.amount() + "!")
				it.player().inventory().remove(Item(MYSTERY_BOX), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(item, true)
				if (item.id() == 11905) { // Full trident
					item.property(ItemAttrib.CHARGES, 2500)
				}
			}
		}
	}
	
}