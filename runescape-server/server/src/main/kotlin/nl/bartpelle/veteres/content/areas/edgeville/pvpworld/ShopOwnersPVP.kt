package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.services.advertisement.VoteRewardService
import nl.bartpelle.veteres.services.financial.BMTPostback
import nl.bartpelle.veteres.util.GameCommands

/**
 * @author Mack
 */
object ShopOwnersPVP {

    private const val MAGIC_WARES_PVP = 7042
    private const val MELEE_WARES_PVP = 7477
    private const val RANGE_WARES_PVP = 2995
    private const val UNTRADABLES_PVP = 7456
    private const val BARROWS_WARES = 913
    private const val CONSUMABLE_WARES = 4921
    private const val JEWELLERY_WARES = 5523
    private const val COSMETIC_WARES = 4802
	private const val VOTING_WARES = 1815
    private const val BLOOD_MONEY_WARES = 7382
    private const val CREDIT_MANAGER = 2713

    @JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
        sr.onNpcOption1(MAGIC_WARES_PVP, {
            if (it.player().world().realm().isPVP) {
                it.player().world().shop(1).display(it.player())
            }
        })
        sr.onNpcOption1(MELEE_WARES_PVP, {
            it.player().world().shop(2).display(it.player())
        })
        sr.onNpcOption1(RANGE_WARES_PVP, {
            it.player().world().shop(3).display(it.player())
        })
        sr.onNpcOption1(UNTRADABLES_PVP, {
            it.player().world().shop(4).display(it.player())
        })
        sr.onNpcOption1(CONSUMABLE_WARES, {
            it.player().world().shop(5).display(it.player())
        })
        sr.onNpcOption1(JEWELLERY_WARES, {
            it.player().world().shop(7).display(it.player())
        })
        sr.onNpcOption1(COSMETIC_WARES, {
            it.player().world().shop(6).display(it.player())
        })
	    sr.onNpcOption1(VOTING_WARES, {
            voteRewards(it)
	    })
        sr.onNpcOption1(BLOOD_MONEY_WARES, {
            it.player().world().shop(57).display(it.player())
        })
	    sr.onNpcOption2(VOTING_WARES, @Suspendable {
		    when (it.optionsTitled("Proceed to Voting Site?", "Yes, I'd like to open the official OS-Scape voting web page.", "No, I don't want to vote right now.")) {
			    1 -> {
				    GameCommands.process(it.player(), "vote")
			    }
			    else -> {
				    it.chatNpc("Well, that's kind of a bummer. Anyhow, visit me when you wish to vote!", VOTING_WARES)
			    }
		    }
	    })
        sr.onNpcOption3(VOTING_WARES, @Suspendable {
            it.player().world().server().service(VoteRewardService::class.java, true).ifPresent({ v -> v.claim(it.player()) })
            it.chatNpc("All done! Any pending votes have now been claimed. If you have any issues claiming voting rewards please contact a staff member.", VOTING_WARES)
        })
        sr.onNpcOption1(CREDIT_MANAGER) @Suspendable {
            when (it.optionsTitled("Proceed to Online Store?", "Yes, I'd like to view the online store.", "No, not right now.")) {
                1 -> {
                    GameCommands.process(it.player(), "store")
                }
                else -> {
                    it.chatNpc("You know where to find me if you change your mind!", CREDIT_MANAGER)
                }
            }
        }
        sr.onNpcOption2(CREDIT_MANAGER) @Suspendable {
            when (it.optionsTitled("Proceed to Credit Purchase Page?", "Yes, I'd like to view the credit purchase page.", "No, not right now.")) {
                1 -> {
                    GameCommands.process(it.player(), "store")
                }
                else -> {
                    it.chatNpc("You know where to find me if you change your mind!", CREDIT_MANAGER)
                }
            }
        }
        sr.onNpcOption3(CREDIT_MANAGER) @Suspendable {
            it.player().world().server().service(BMTPostback::class.java, true).ifPresent({ bmt -> bmt.acquire(it.player().id() as Int) })
            it.chatNpc("All done! Any pending purchases have been claimed. If you have any issues claiming donation rewards please contact a staff member.", CREDIT_MANAGER)
        }
    }

    @Suspendable fun voteRewards(it: Script) {
        it.player().world().shop(69).display(it.player())
    }

}