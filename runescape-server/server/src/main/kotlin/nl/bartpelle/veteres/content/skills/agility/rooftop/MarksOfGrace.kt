package nl.bartpelle.veteres.content.skills.agility.rooftop

import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Bart on 4/5/2016.
 */
object MarksOfGrace {
	
	val MARK_OF_GRACE = 11849
	val MARK_LIFETIME = (10 * 60 * 1000).toLong() // 10 minutes
	
	fun trySpawn(player: Player, tiles: Array<Tile>, rarity: Int, threshold: Int) {
		// Base odds depend on the player's game mode
		var odds = when (player.mode()!!) {
			GameMode.REALISM -> 6
			GameMode.CLASSIC -> 3
			GameMode.LAID_BACK -> 1
		}
		
		// Donator perks grant extra odds
		odds += when (player.donationTier()) {
			DonationTier.ULTIMATE_DONATOR -> 15
			DonationTier.GRAND_MASTER_DONATOR -> 12
			DonationTier.MASTER_DONATOR -> 10
			DonationTier.LEGENDARY_DONATOR -> 8
			DonationTier.EXTREME_DONATOR -> 6
			DonationTier.SUPER_DONATOR -> 4
			DonationTier.DONATOR -> 2
			else -> 0
		}
		
		if (player.skills().level(Skills.AGILITY) > threshold + 20) {
			odds = Math.max(1, (odds * 0.7).toInt()) // You don't want to end up in this :)
		}
		
		// Check for the odds. :)
		if (player.world().rollDie(rarity, odds)) {
			val item = GroundItem(player.world(), Item(MARK_OF_GRACE), tiles.random(), player.id()).lifetime(MARK_LIFETIME)
			player.world().spawnGroundItem(item)
		}
	}
	
}