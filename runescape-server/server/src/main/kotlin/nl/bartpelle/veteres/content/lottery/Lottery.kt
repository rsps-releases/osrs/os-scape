package nl.bartpelle.veteres.content.lottery

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.GameServer
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.services.logging.LoggingService
import nl.bartpelle.veteres.services.lottery.SqlLotteryServer
import nl.bartpelle.veteres.services.sql.PgSqlService
import nl.bartpelle.veteres.services.sql.SqlTransaction
import nl.bartpelle.veteres.util.JSON
import nl.bartpelle.veteres.util.RSFormatter.formatDisplayname
import nl.bartpelle.veteres.util.RSFormatter.formatItemAmount
import nl.bartpelle.veteres.util.WeightedCollection
import org.apache.logging.log4j.LogManager
import java.sql.Connection
import java.sql.ResultSet
import java.util.concurrent.TimeUnit

/**
 * Created by Jonathan on 2/8/2017.
 */
object Lottery {
	
	private val logger = LogManager.getLogger(Lottery::class.java)
	
	
	private var enabled = false
	
	var config: LotteryInstance? = null
	
	private var sqlServer: SqlLotteryServer? = null
	
	private var nextSave = -1L
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		if (sqlServer == null) {
			this.sqlServer = SqlLotteryServer(r.server())
			r.onWorldInit {
				pull()
				
				logger.info("Pulled local lottery information to database")
				
				val world = it.ctx<World>()
				
				world.timers().register(TimerKey.LOTTERY_TICK, 1)
			}
		} else {
			r.server().world().timers().register(TimerKey.LOTTERY_TICK, 1)
		}
		
		r.onWorldTimer(TimerKey.LOTTERY_TICK) {
			val world = it.ctx<World>()
			
			if (!r.server().isDevServer && config != null) {
				if (enabled && config != null) {
					val config = config!!
					if (enabled && System.currentTimeMillis() >= config.enddate) {
						rewardAndReset()
						logger.info("Rewarding lottery winner")
					} else if (enabled && (System.currentTimeMillis() > nextSave)) {
						push()//Lets go ahead and save
						nextSave = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(1)
						logger.info("Pushing local lottery information to database")
					}
				}
			}
			world.timers().register(TimerKey.LOTTERY_TICK, 1)
		}
	}
	
	fun sync(r: ResultSet, s: GameServer) {
		try {
			enabled = false
			
			config = LotteryInstance(r.getInt("world_id"), r.getInt("currency_id"), r.getInt("deposit_modifier"),
					r.getDate("deposit_modifier_expire"), r.getInt("withdraw_modifier"),
					r.getDate("withdraw_modifier_expire"), r.getLong("enddate"), s)
			
			val entrants = mutableMapOf<Int, Long>()
			val e = r.getString("entrants")
			if (e != null) {
				JSON.parseMap(e).forEach {
					entrants.put(it.key, it.value)
				}
			}
			
			config!!.entrants = entrants
			
			enabled = true
		} catch (e: Exception) {
			e.printStackTrace()
		}
	}
	
	fun syncWinners(r: ResultSet) {
		if (config == null) {
			return
		}
		val config = config!!
		try {
			val id = r.getInt("account_id")
			val amt = r.getInt("amt")
			
			val existing = config.winners[id] ?: 0
			
			config.winners.put(id, existing + amt)
		} catch (e: Exception) {
			e.printStackTrace()
		}
	}
	
	fun withdraw(player: Player, winner: Boolean, currentWinnings: Long = 0): Boolean {
		if (!enabled || config == null) {
			player.message("The OSScape Lottery is busy at the moment.")
			return false
		}
		val config = config!!
		
		val id = player.id() as Int
		
		if (!winner && config.winningsFor(id) > 0) {
			player.message("You currently have outstanding lottery winnings. Please claim they before participating in the lottery again.")
			return false
		}
		
		val currentWinnings = if (winner) config.winners[id] ?: 0 else Math.min(currentWinnings, config.entrants.getOrElse(id, { 0 }))
		
		if (currentWinnings <= 0L) {
			player.message(if (winner) "You do not have any unclaimed winnings." else "You do not have any money invested.")
			return false
		}
		
		val coins = config.currency
		val tokens = if (coins == 13307) 13215 else 13204
		
		val coinCount: Long = currentWinnings
		val tokenAmount: Long = coinCount / 1000
		
		if (player.inventory().freeSlots() == 0) {
			player.message("You don't have enough room to carry the ${config.tokensName} tokens.")
			return false
		}
		
		val success: Boolean
		if (tokenAmount > 0) {
			val coinAmount = coinCount - (tokenAmount * 1000)
			success = player.inventory().add(Item(coins, coinAmount.toInt()), false).success() &&
					player.inventory().add(Item(tokens, tokenAmount.toInt()), false).success()
		} else {
			success = player.inventory().add(Item(coins, coinCount.toInt()), false).success()
		}
		
		if (coinCount > 0 && success) {
			if (winner) {
				player.message("You withdrew ${formatItemAmount(currentWinnings)} ${config.currencyName} of lottery winnings. Congratulations!")
				config.winners.put(id, -1)
				sqlServer!!.push()
			} else {
				val lastamount = (config.entrants[id] ?: currentWinnings)
				config.entrants.put(id, lastamount - currentWinnings)
				player.message("You withdrew ${formatItemAmount(currentWinnings)} ${config.currencyName} from the lottery.")
				
				player.world().server().service(LoggingService::class.java, true).ifPresent { s ->
					s.logLottery(id, config.worldId, "withdraw", currentWinnings, lastamount, player.ip(), config.currency)
				}
			}
			return true
		} else {
			player.message("You don't have enough room to carry the $coinCount ${config.currencyName}.")
		}
		return false
	}
	
	fun deposit(player: Player, currentAmount: Long): Boolean {
		if (!enabled || config == null) {
			player.message("The OSScape Lottery is busy at the moment.")
			return false
		}
		val config = config!!
		
		val id = player.id() as Int
		
		if (config.winningsFor(id) > 0) {
			player.message("You currently have outstanding lottery winnings. Please claim they before participating in the lottery again.")
			return false
		}
		
		val coins = config.currency
		val tokens = if (coins == 13307) 13215 else 13204
		
		val currentAmount = Math.min(currentAmount, player.inventory().count(coins).toLong() + player.inventory().count(tokens).toLong() * 1000)
		
		val coinCount: Long = currentAmount
		var tokenAmount: Long = coinCount / 1000
		tokenAmount = Math.min(tokenAmount, player.inventory().count(tokens).toLong())
		
		val success: Boolean
		if (tokenAmount > 0) {
			val coinAmount = coinCount - (tokenAmount * 1000)
			success = player.inventory().remove(Item(coins, coinAmount.toInt()), false).success() &&
					player.inventory().remove(Item(tokens, tokenAmount.toInt()), false).success()
		} else {
			success = player.inventory().remove(Item(coins, coinCount.toInt()), false).success()
		}
		
		if (coinCount > 0 && success) {
			val lastAmount = config.entrants[id] ?: 0
			config.entrants.put(id, lastAmount + currentAmount)
			
			player.message("You invest ${formatItemAmount(currentAmount)} ${config.currencyName} into the lottery. You currently have ${formatItemAmount(config.entrants[id]!!)} invested.")
			
			player.world().server().service(LoggingService::class.java, true).ifPresent { s ->
				s.logLottery(id, config.worldId, "deposit", currentAmount, lastAmount, player.ip(), config.currency)
			}
			return true
		} else {
			player.message("You need to have ${config.currencyName} in order to participate in the lottery.")
		}
		return false
	}
	
	@JvmStatic fun rewardAndReset() {
		if (config == null) {
			return
		}
		val config = config!!
		
		val totalEntrans = config.entrants.size
		var total = config.entrants.values.sum()
		
		if (total > 0) {
			if (totalEntrans > 2 && config.withdrawModifier > 0) //Lets not tax if there's < 3 entrants cuz fags might try to find our hidden tax :)
				total -= (total * (config.withdrawModifier.toDouble() / 100.0)).toInt() //Take tax away because we need to fund the wall
			
			val weightCollection = WeightedCollection<Int>()
			
			config.entrants.filter { it.value > 0 }.forEach { weightCollection.add(it.value.toDouble(), it.key) }
			
			val winnerId = weightCollection.next()
			
			val player = config.server.world().playerForId(winnerId)
			if (player.isPresent) {
				player.get().message("Congratulations! You've won the OSScape Lottery. Please talk to the Financial Advisor to claim your winnings!")
				announce("${formatDisplayname(player.get().name())} won this weeks lottery!")
			} else {
				config.server.service(PgSqlService::class.java, true).ifPresent({ sql ->
					sql.worker().submit(object : SqlTransaction() {
						@Throws(Exception::class)
						override fun execute(connection: Connection) {
							val stmt = connection.prepareStatement("select displayname from accounts WHERE id=?;")
							stmt.setInt(1, winnerId)
							val set = stmt.executeQuery()
							if (set.next())
								announce("${formatDisplayname(set.getString("displayname"))} won this weeks lottery!")
							
							connection.commit()
							connection.close()
						}
					})
				})
			}
			
			config.winners.put(winnerId, total)
		} else announce("There were no entries in this weeks lottery. No winner has been selected.")
		
		config.clearAndReset()
		sqlServer!!.push()
	}
	
	fun toggle() {
		enabled = !enabled
	}
	
	fun announce(msg: String) = config?.server?.world()?.broadcast("<col=844e0d><img=54>Lottery: $msg")
	
	@JvmStatic fun push() = sqlServer!!.push()
	
	@JvmStatic fun pull() = sqlServer!!.pull()
	
	@JvmStatic fun enabled() = enabled
	
	@JvmStatic fun enabled(b: Boolean) {
		enabled = b
	}
	
}