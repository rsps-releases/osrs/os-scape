package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Sitautions on 2017-03-05.
 */

object Bonecrusher {
	
	val BONECRUSHER = 13116
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption3(BONECRUSHER) @Suspendable { it.message("This item has unlimited charges!") }
		r.onItemOption4(BONECRUSHER) @Suspendable { it.message("Now why would you want to do that?") }
	}
	
}
