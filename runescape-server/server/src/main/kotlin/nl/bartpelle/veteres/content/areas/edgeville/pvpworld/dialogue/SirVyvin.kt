package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 3/10/2016.
 */

object SirVyvin {
	
	val SIR_VYVIN = 4736
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(SIR_VYVIN) {
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(1345).display(it.player())
			} else {
				it.messagebox("This shop is only accessible from the PvP world.")
			}
		}
	}
}
