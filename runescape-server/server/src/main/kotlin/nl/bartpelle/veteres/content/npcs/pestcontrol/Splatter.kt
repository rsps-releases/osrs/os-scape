package nl.bartpelle.veteres.content.npcs.pestcontrol

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.pest_control.*
import nl.bartpelle.veteres.fs.MapDefinition
import nl.bartpelle.veteres.fs.ObjectDefinition
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Jason MacKeigan on 2016-09-01 at 4:31 PM
 */
object Splatter : SearchesForBreakable {
	
	/**
	 * Creates the effect of the splatter. The effect forces the spinner to walk to any close
	 * barricade and attempt to blow it up. If none are close, meaning they're all destroyed,
	 * then go try and destroy the doors. If the doors are destroyed then proceed to move the
	 * npc to within attacking distance of the knight and then proceed to attack the knight.
	 */
	@Suspendable override fun search(knight: Npc, script: Script, centerOfMap: Tile) {
		val npc = script.npc()
		
		val mapDefinition = npc.world().definitions().get(MapDefinition::class.java, npc.tile().region())
		
		val breakableBarricade = PestControlNpc.searchForBreakable(npc, centerOfMap, mapDefinition, 10, Breakable.BARRICADE,
				BarricadePositionOffset.ALL)
		
		if (breakableBarricade.isPresent && breakableBarricade.get().id() != Breakable.BARRICADE.stages.last()) {
			PestControlNpc.walkToObject(script, breakableBarricade.get())
			explode(npc, mapDefinition, breakableBarricade.get(), Breakable.BARRICADE)
			return
		}
		
		val topCornerBarricade = PestControlNpc.searchForBreakable(npc, centerOfMap, mapDefinition, 10, Breakable.TOP_CORNER_BARRICADE,
				TopBarricadePositionOffset.ALL)
		
		if (topCornerBarricade.isPresent && topCornerBarricade.get().id() != Breakable.TOP_CORNER_BARRICADE.stages.last()) {
			PestControlNpc.walkToObject(script, topCornerBarricade.get())
			explode(npc, mapDefinition, topCornerBarricade.get(), Breakable.TOP_CORNER_BARRICADE)
			return
		}
		
		val bottomCornerBarricade = PestControlNpc.searchForBreakable(npc, centerOfMap, mapDefinition, 10, Breakable.BOTTOM_CORNER_BARRICADE,
				BottomBarricadePositionOffset.ALL)
		
		if (bottomCornerBarricade.isPresent && bottomCornerBarricade.get().id() != Breakable.BOTTOM_CORNER_BARRICADE.stages.last()) {
			PestControlNpc.walkToObject(script, bottomCornerBarricade.get())
			explode(npc, mapDefinition, bottomCornerBarricade.get(), Breakable.BOTTOM_CORNER_BARRICADE)
			return
		}
		for (breakable in Breakable.DOORS) {
			val breakableDoor = PestControlNpc.searchForBreakable(npc, centerOfMap, mapDefinition, 12, breakable,
					DoorPositionOffset.ALL)
			
			if (breakableDoor.isPresent && breakableDoor.get().id() != breakable.stages.last()) {
				PestControlNpc.walkToObject(script, breakableDoor.get())
				explode(npc, mapDefinition, breakableDoor.get(), breakable)
				return
			}
		}
		
		PestControlNpc.walkToKnight(knight, npc, script, CombatStyle.MELEE)
	}
	
	/**
	 * Explodes the non-playable character and in turn killing them and destroying the breakable
	 * object that is targeted.
	 */
	private fun explode(npc: Npc, mapDefinition: MapDefinition, toBreak: MapObj, breakable: Breakable) {
		val world = npc.world()
		val replacementId = breakable.after(toBreak.id())
		
		if (replacementId == -1) {
			return
		}
		val objectDefinition = world.definitions().get(ObjectDefinition::class.java, replacementId)
		
		if (objectDefinition.modeltypes != null) {
			val spawn = MapObj(toBreak.tile(), replacementId, objectDefinition.modeltypes!!.get(0), toBreak.rot())
			
			world.tileGraphic(1250, toBreak.tile(), 0, 0)
			world.removeObj(toBreak, objectDefinition.projectileClipped || objectDefinition.id == breakable.stages.last())
			world.spawnObj(spawn, objectDefinition.projectileClipped)
		}
		npc.hit(npc, npc.hp())
		npc.graphic(650, 0, 205)
	}
	
}