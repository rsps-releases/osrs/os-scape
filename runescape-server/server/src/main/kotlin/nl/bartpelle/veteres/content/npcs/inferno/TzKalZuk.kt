package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.content.npcs.inferno.controllers.AncestralGlyphController
import nl.bartpelle.veteres.content.npcs.inferno.controllers.TzKalZukController
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jonathan on 6/13/2017.
 * Renditions by Mack on 8/1/2017
 */
object TzKalZuk {
	
	@JvmField
	val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		npc.face(null) //getTarget faces so lets reset that shit
		val controller: TzKalZukController = npc.attrib<TzKalZukController>(AttributeKey.INFERNO_CONTROLLER) ?: return@s
		val ancestralGlyph: Npc = if (target.hasAttrib(AttributeKey.INFERNO_SESSION)) target.attrib<InfernoSession>(AttributeKey.INFERNO_SESSION).ancestralGlyph!! else return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.attackTimerReady(npc)) {
				
				//If the player is not within the session let's break the combat loop.
				if (target.isPlayer) {
					val p = target as Player
					if (!InfernoContext.inSession(p)) {
						break
					}
				}
				
				//Spawn jad
				if (npc.hp() <= 480 && !npc.attribOr<Boolean>(AttributeKey.TZKAL_SPAWNED_JAD, false)) {
					controller.spawnJad()
					npc.putattrib(AttributeKey.TZKAL_SPAWNED_JAD, true)
				}
				
				//Spawn the healers.
				if (npc.hp() <= 240 && !npc.attribOr<Boolean>(AttributeKey.TZKAL_ZUK_SPAWNED_HEALERS, false)) {
					for (i in 1..4) {
						controller.spawnHealers(i)
					}
					npc.putattrib(AttributeKey.TZKAL_ZUK_SPAWNED_HEALERS, true)
				}
				
				if (shouldShieldBlock(ancestralGlyph, target)) {
					shieldBlock(npc, ancestralGlyph)
				} else {
					rangedAttack(npc, target)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	/**
	 * Spawns wall at the beginning of the boss fight.
	 */
	fun spawnWall(player: Player, center: Tile, session: InfernoSession) {
		
		//Remove the roof
		player.varps().varbit(Varbit.INFERNO_BOSS_ROOF, 1)
		
		//Delete the Ancestral Glyph object and other map objects to make space for Zuk's chamber.
		val ancestralGlyphObject = player.world().objById(30338, Tile(center.x - 2, center.z + 19))
		
		player.world().removeObj(ancestralGlyphObject)
		player.world().spawnObj(MapObj(Tile(center.x - 4, center.z + 20), 30346, 10, 3))
		player.world().spawnObj(MapObj(Tile(center.x + 1, center.z + 20), 30346, 10, 3))
		
		player.attrib<InfernoSession>(AttributeKey.INFERNO_SESSION).ancestralGlyph = Npc(7707, session.world, Tile(center.x - 2, center.z + 16))
		
		val glyph: Npc? = player.attrib<InfernoSession>(AttributeKey.INFERNO_SESSION).ancestralGlyph
		
		glyph!!.putattrib(AttributeKey.INFERNO_CONTROLLER, AncestralGlyphController(glyph, session))
		glyph.respawns(false)
		player.world().registerNpc(glyph)
		
		glyph.world().server().scriptExecutor().executeScript(glyph.world(), {
			doWallPath(it, player, glyph, session.area!!.center())
		})
	}
	
	/**
	 * Performs the logic for the wall pathing from east to west.
	 */
	@Suspendable
	fun doWallPath(it: Script, player: Player, wall: Npc, center: Tile) {
		val westBoundary = Tile(center.x - 15, center.z + 16)
		val eastBoundary = Tile(center.x + 11, center.z + 16)
		
		it.delay(2) // initial starting pause
		
		//Move to the west boundary
		wall.pathQueue().interpolate(eastBoundary)
		it.delay(17) // 20 because it starts at the middle
		
		//And the movement loop from boundary -> boundary
		while (InfernoContext.inSession(player) && !wall.dead()) {
			wall.pathQueue().interpolate(westBoundary)
			it.delay(30)
			wall.pathQueue().interpolate(eastBoundary)
			it.delay(30)
			
			if (InfernoContext.inSession(player) && !wall.attribOr<Boolean>(AttributeKey.INFERNO_FINAL_BOSS_MAGE_RANGE_SPAWN, false)) {
				spawnRangeAndMager(player, wall)
				wall.putattrib(AttributeKey.INFERNO_FINAL_BOSS_MAGE_RANGE_SPAWN, true)
			}
		}
	}
	
	@Suspendable
	fun rangedAttack(npc: Npc, target: Entity) {
		
		npc.animate(7566)
		
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 35)
		
		target.world().server().scriptExecutor().executeScript(target.world(), @Suspendable { s ->
			s.delay(1)
			npc.world().spawnProjectile(npc, target, 1375, 50, 36, 41, 10 * tileDist, 10, 5)
			
			target.hit(npc, npc.world().random(npc.combatInfo().maxhit), delay).combatStyle(CombatStyle.RANGE)
		})
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, 6)
	}
	
	/**
	 * The function that sends the projectile to the glyph given the block condition is met.
	 */
	fun shieldBlock(npc: Npc, shield: Npc) {
		
		npc.animate(7566)//7565 def, 7566 attack 7563=spawn death=7562
		
		val tileDist = npc.tile().transform(1, 1, 0).distance(shield.tile())
		
		shield.world().server().scriptExecutor().executeScript(shield.world(), @Suspendable { s ->
			s.delay(1)
			npc.world().spawnProjectile(npc, shield, 1375, 50, 36, 41, 10 * tileDist, 10, 5)
		})
		
		EntityCombat.putCombatDelay(npc, 6)
	}
	
	fun shouldShieldBlock(ancestralGlyph_: Npc?, player: Entity): Boolean {
		if (ancestralGlyph_!!.dead()) return false
		
		val playerX = player.pathQueue().peekNextTile().x
		val playerZ = player.pathQueue().peekNextTile().z
		
		val wallX = ancestralGlyph_.pathQueue().peekNextTile().x
		val wallZ = ancestralGlyph_.pathQueue().peekNextTile().z
		
		if (wallZ - playerZ >= 8) { return false }
		
		if (playerX == wallX) //They are on same x
			return true
		
		if (wallX - playerX >= -3 && wallX - playerX <= 3) {
			return true
		}
		
		return false
	}
	
	/**
	 * Spawns a range and mager on the map periodically during the final boss wave.
	 */
	fun spawnRangeAndMager(player: Player, ancestralGlyph_: Npc?) {
		val area = InfernoContext.activeArea(player)
		val ranger = Npc(InfernoContext.RANGER, player.world(), Tile(area!!.center().x - 8, area.center().z + 6))
		val mager = Npc(InfernoContext.MAGER, player.world(), Tile(area.center().x - 4, area.center().z + 6))
		
		if (!InfernoContext.inSession(player)) return
		
		ranger.walkRadius(0)
		ranger.respawns(false)
		ranger.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 0)
		ranger.combatInfo().attackspeed = 6
		
		mager.walkRadius(0)
		mager.respawns(false)
		mager.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 0)
		mager.combatInfo().attackspeed = 9
		
		player.world().registerNpc(ranger)
		player.world().registerNpc(mager)
		
		InfernoContext.get(player)!!.activeNpcs.add(mager)
		InfernoContext.get(player)!!.activeNpcs.add(ranger)
		
		if (ancestralGlyph_ != null && !ancestralGlyph_.dead()) {
			ranger.attack(ancestralGlyph_)
			mager.attack(ancestralGlyph_)
		} else {
			ranger.attack(player)
			mager.attack(player)
		}
		
		player.message("<col=FF0000>Look out! TzKal-Zuk has spawned a Jal-Xil and a Jal-Zek!")
	}
}