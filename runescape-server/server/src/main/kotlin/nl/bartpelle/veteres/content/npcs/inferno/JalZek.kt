package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 7/31/2017.
 */
object JalZek {
	
	//7611 revive animation
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			
			if (EntityCombat.canAttackDistant(npc, target, true, 6) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.canAttackMelee(npc, target) && npc.world().random(2) == 1) {
					melee(npc, target)
				} else {
					magic(npc, target)
				}
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, 6)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun melee(npc: Npc, target: Entity) {
		
		npc.animate(npc.attackAnimation())
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, npc.world().random(npc.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(npc, 0, 1)
		}
	}
	
	fun magic(npc: Npc, target: Entity) {
		
		npc.animate(npc.attackAnimation())
		
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 35)
		
		npc.world().spawnProjectile(npc.tile() + Tile(1, 1), target, 1376, 40, 34, 45, 12 * tileDist, 0, 1)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
			target.hit(npc, npc.world().random(npc.combatInfo().maxhit), delay).combatStyle(CombatStyle.MAGIC)
		} else {
			target.hit(npc, 0, 1)
		}
	}
}