package nl.bartpelle.veteres.content.minigames.tzhaarfightcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 9/18/2016.
 */

object FightCaveObjects {
	
	val EXIT = 11833
	val ENTRANCE = 11834
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(EXIT) @Suspendable {
			if (TimerKey.TZHAAR_FIGHT_CAVE in it.player().timers()) {
				it.chatNpc("Hey, JalYt, you were in cave only a moment ago.<br>You wait ${getAmountOfTimeMessage(it)} before going in again.", 2180, 615)
				if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {// Allows rapid testing
					it.player().timers().cancel(TimerKey.TZHAAR_FIGHT_CAVE)
					it.message("Note: As an admin, you can skip the 4 minute wait time before restarting the Fight Caves.")
				}
			} else {
				it.player().putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, -1) // Hard set as wave -1 straight away, after 3 ticks when the minigame start it progresses to 1.
				FightCaveGame.initiateFightCaves(it)
			}
		}
		
		r.onObject(ENTRANCE) @Suspendable {
			when (it.interactionOption()) {
				1 -> if (it.optionsTitled("Really leave?", "Yes - really leave.", "No, I'll stay.") == 1) FightCaveGame.leaveFightCave(it.player())
				2 -> FightCaveGame.leaveFightCave(it.player())
			}
		}
	}
	
	@Suspendable fun getAmountOfTimeMessage(it: Script): String {
		val timeLeft = it.player().timers().left(TimerKey.TZHAAR_FIGHT_CAVE)
		if (timeLeft > 300) return "4 minutes"
		else if (timeLeft > 200) return "3 minutes"
		else if (timeLeft > 100) return "2 minutes"
		else return "a minute or so";
	}
}