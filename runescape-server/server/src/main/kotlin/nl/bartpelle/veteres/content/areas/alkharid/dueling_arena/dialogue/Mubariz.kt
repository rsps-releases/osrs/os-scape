package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Mubariz {
	
	val MUBARIZ = 3331
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MUBARIZ) @Suspendable {
			it.chatPlayer("Hi!", 567)
			it.chatNpc("Welcome to the Duel Arena!", MUBARIZ, 567)
			it.chatPlayer("Thanks! I need some information.", 567)
			it.chatNpc("What would you like to know?", MUBARIZ, 588)
			when (it.options("What is this place?", "How do I challenge someone to a duel?",
					"What kind of options are there?", "This place looks really old, where did it come from?")) {
				1 -> {
					it.chatPlayer("What is this place?", 575)
					it.chatNpc("The Duel Arena has six duel arenas where you can<br>fight other players in a controlled environment." +
							" We<br>have our own dedicated hospital where we guarantee to<br>put you back together, even if you lose.", MUBARIZ, 591)
					it.chatNpc("Inbetween the arenas are walkways where you can<br>watch the fights and challenge other players.", MUBARIZ, 589)
					it.chatPlayer("Sounds great. Thanks!", 567)
					it.chatNpc("See you in the arenas!", MUBARIZ, 567)
				}
				2 -> {
					it.chatPlayer("How do I challenge someone to a duel?", 575)
					it.chatNpc("When you go to the arena you'll go up an access ramp<br>to the walkways that overlook the duel arenas." +
							" From the<br>walkways you can watch the duels and challenge other<br>players.", MUBARIZ, 575)
					it.chatNpc("You'll know you're in the right place as you'll have a<br>Duel-with option when you right-click a player.", MUBARIZ, 589)
					it.chatPlayer("I'm there!", 567)
				}
				3 -> {
					it.chatPlayer("What kind of options are there?", 575)
					it.chatNpc("You and your opponent can offer items as a stake. If<br>you win, you receive what your opponent staked, but if<br>you lose," +
							" your opponent will get whatever items you<br>staked.", MUBARIZ, 591)
					it.chatNpc("You can choose to use rules to spice things up a bit.<br>For instance if you both agree to use the 'No Magic'<br>rule then neither" +
							" player can use magic to attack the<br>other player. The fight will be restricted to ranging and", MUBARIZ, 591)
					it.chatNpc("melee only.", MUBARIZ, 588)
					it.chatNpc("The rules are fairly self-evident with lots of different<br>combinations for you to try out!", MUBARIZ, 589)
					it.chatPlayer("Cool! Thanks!", 567)
				}
				4 -> {
					it.chatPlayer("This place looks really old, where did it come from?", 575)
					it.chatNpc("The archaeologists that are excavating the area east of<br>Varrock have been working on this site as well. From<br>these cliffs they" +
							" uncovered this huge building. The<br>experts think it may date back to the second age!", MUBARIZ, 591)
					it.chatNpc("Now that the archaeologists have moved out, a group of<br>warriors, headed by myself, have bought the land and<br>converted it to a set of" +
							" duel arenas. The best fighters<br>from around the world come here to fight!", MUBARIZ, 591)
					it.chatPlayer("I challenge you!", 614)
					it.chatNpc("Ho! Ho! Ho!", MUBARIZ, 605)
				}
			}
		}
	}
	
}
