package nl.bartpelle.veteres.content.skills.slayer

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.skills.slayer.master.Krystilia
import nl.bartpelle.veteres.content.slayerTaskId
import nl.bartpelle.veteres.fs.EnumDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.QuestTab
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 3/10/2016.
 */
object SlayerRewards {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(426, 8) {
			
			if (it.buttonSlot() !in 38..46) {
				it.message("This feature is not available on this world.")
				return@onButton
			}
			when (it.buttonSlot()) {
			
			/**
			 * If the button slot has a //comment it means it's incomplete.
			 */
				0 -> unlock(it, Varbit.GARGOYLE_SMASHER)
				1 -> unlock(it, Varbit.SLUG_SALTER, false) // Slug salter - Automatically salt rock slugs when they're on critical health, if you have salt.
				2 -> unlock(it, Varbit.REPTILE_FREEZER, false) // Reptile freezer - Automatically freeze desert lizards when they're on critical health, if you have ice water.
				3 -> unlock(it, Varbit.SHROOM_SPRAYER, false) // 'Shroom sprayer - Automatically spray mutated zygomites when they're on critical health, if you have fungicide.
				4 -> unlock(it, Varbit.NEED_MORE_DARKNESS)
				5 -> unlock(it, Varbit.SLAYER_UNLOCKED_HELM)
				6 -> unlock(it, Varbit.RING_BLING)
				7 -> unlock(it, Varbit.BROADER_FLETCHING, false) // Broader fletching - Learn to fletch broad arrows (with level 52 Fletching) and broad bolts (with level 55 Fletching).
				8 -> unlock(it, Varbit.ANKOU_VERY_MUCH)
				9 -> unlock(it, Varbit.SUQ_ANOTHER_ONE)
				10 -> unlock(it, Varbit.FIRE_AND_DARKNESS)
				11 -> unlock(it, Varbit.PEDAL_TO_THE_METALS)
				12 -> unlock(it, Varbit.SPIRITUAL_FERVOUR)
				13 -> unlock(it, Varbit.AUGMENT_MY_ABBIES)
				14 -> unlock(it, Varbit.ITS_DARK_IN_HERE)
				15 -> unlock(it, Varbit.GREATER_CHALLENGE)
				16 -> unlock(it, Varbit.I_HOPE_YOU_MITH_ME, false) // I hope you mith me - Duradel and Nieve will be able to assign Mithril Dragons as your task.
				17 -> unlock(it, Varbit.WATCH_THE_BIRDIE, false) // Watch the birdie - Duradel, Nieve and Chaeldar will be able to assign Aviansies as your task.
				18 -> unlock(it, Varbit.HOT_STUFF, false) // Hot stuff - Duradel and Nieve will be able to assign TzHaar as your task. You may also be offered a chance to slay TzTok-Jad.
				19 -> unlock(it, Varbit.LIKE_A_BOSS, false) // Like a boss - Duradel and Nieve will be able to assign boss monsters as your task. They will choose which boss you must kill.
				20 -> unlock(it, Varbit.BLEED_ME_DRY)
				21 -> unlock(it, Varbit.SMELL_YA_LATER)
				22 -> unlock(it, Varbit.BIRDS_OF_A_FEATHER)
				23 -> unlock(it, Varbit.I_REALLY_MITH_YOU)
				24 -> unlock(it, Varbit.HORRORIFIC)
				25 -> unlock(it, Varbit.TO_DUST_YOU_SHALL_RETURN)
				26 -> unlock(it, Varbit.WYVER_NOTHER_ONE)
				27 -> unlock(it, Varbit.GET_SMASHED)
				28 -> unlock(it, Varbit.NECHS_PLEASE)
				29 -> unlock(it, Varbit.KRACK_ON)
				30 -> unlock(it, Varbit.REPTILE_GOT_RIPPED, false) // Reptile got ripped - Duradel, Nieve and Chaeldar will be able to assign you Lizardmen. You need Shayzien House favour to fight lizardmen.
				31 -> unlock(it, Varbit.KING_BLACK_BONNET)
				32 -> unlock(it, Varbit.KALPHITE_KHAT)
				33 -> unlock(it, Varbit.UNHOLY_HELMET)
				34 -> unlock(it, Varbit.SEEING_RED, false) // Durabel and Nieve will be able to assign Red Dragons to your task
				35 -> unlock(it, Varbit.BIGGER_AND_BADDER) //Increase the risk against certain slayer monsters with the chance of a superior version spawning whilst on a slayer task.
				36 -> unlock(it, Varbit.GET_SCABARIGHT_ON_IT, false) // Whenever you get a Scabarie task, it will be bigger
				37 -> unlock(it, Varbit.UNLOCK_DULY_NOTED, false) // Whenever you get a Scabarie task, it will be bigger
				39 -> cancelTask(it)
				40 -> blockTask(it)
				41 -> unblockTask(it, Varbit.BLOCKED_TASK_SLOT_ONE)
				42 -> unblockTask(it, Varbit.BLOCKED_TASK_SLOT_TWO)
				43 -> unblockTask(it, Varbit.BLOCKED_TASK_SLOT_THREE)
				44 -> unblockTask(it, Varbit.BLOCKED_TASK_SLOT_FOUR)
				45 -> unblockTask(it, Varbit.BLOCKED_TASK_SLOT_FIVE)
				46 -> unblockTask(it, Varbit.BLOCKED_TASK_SLOT_SIX)
			}
		}
	}
	
	val disableable = arrayListOf(Varbit.I_HOPE_YOU_MITH_ME, Varbit.WATCH_THE_BIRDIE, Varbit.HOT_STUFF, Varbit.REPTILE_GOT_RIPPED,
			Varbit.LIKE_A_BOSS, Varbit.NEED_MORE_DARKNESS, Varbit.ANKOU_VERY_MUCH, Varbit.SUQ_ANOTHER_ONE, Varbit.FIRE_AND_DARKNESS,
			Varbit.PEDAL_TO_THE_METALS, Varbit.I_REALLY_MITH_YOU, Varbit.SPIRITUAL_FERVOUR, Varbit.BIRDS_OF_A_FEATHER, Varbit.GREATER_CHALLENGE,
			Varbit.ITS_DARK_IN_HERE, Varbit.BLEED_ME_DRY, Varbit.SMELL_YA_LATER, Varbit.HORRORIFIC, Varbit.TO_DUST_YOU_SHALL_RETURN,
			Varbit.WYVER_NOTHER_ONE, Varbit.GET_SMASHED, Varbit.NECHS_PLEASE, Varbit.AUGMENT_MY_ABBIES, Varbit.KRACK_ON, Varbit.BIGGER_AND_BADDER)
	
	val multipliable = listOf(Pair(66, Varbit.NEED_MORE_DARKNESS), Pair(79, Varbit.ANKOU_VERY_MUCH), Pair(83, Varbit.SUQ_ANOTHER_ONE),
			Pair(27, Varbit.FIRE_AND_DARKNESS), Pair(58, Varbit.PEDAL_TO_THE_METALS), Pair(59, Varbit.PEDAL_TO_THE_METALS),
			Pair(60, Varbit.PEDAL_TO_THE_METALS), Pair(89, Varbit.SPIRITUAL_FERVOUR), Pair(90, Varbit.SPIRITUAL_FERVOUR),
			Pair(91, Varbit.SPIRITUAL_FERVOUR), Pair(42, Varbit.AUGMENT_MY_ABBIES), Pair(30, Varbit.ITS_DARK_IN_HERE),
			Pair(29, Varbit.GREATER_CHALLENGE), Pair(48, Varbit.BLEED_ME_DRY), Pair(41, Varbit.SMELL_YA_LATER),
			Pair(94, Varbit.BIRDS_OF_A_FEATHER), Pair(93, Varbit.I_REALLY_MITH_YOU), Pair(80, Varbit.HORRORIFIC),
			Pair(72, Varbit.WYVER_NOTHER_ONE), Pair(46, Varbit.GET_SMASHED), Pair(52, Varbit.NECHS_PLEASE),
			Pair(92, Varbit.KRACK_ON))
	
	val BLOCKED_SLOTS = arrayListOf(Varbit.BLOCKED_TASK_SLOT_ONE, Varbit.BLOCKED_TASK_SLOT_TWO,
			Varbit.BLOCKED_TASK_SLOT_THREE, Varbit.BLOCKED_TASK_SLOT_FOUR, Varbit.BLOCKED_TASK_SLOT_FIVE,
			Varbit.BLOCKED_TASK_SLOT_SIX)
	
	
	fun unlock(it: Script, varbit: Int, enabled: Boolean = true) {
		//Can remove the boolean after all rewards are available.
		if (!enabled) {
			it.message("This reward is currently unavailable. Please try again later.")
			return
		}
		
		if (it.player().varps().varbit(varbit) == 1) {
			disableable.forEach { reward ->
				if (reward == varbit) {
					it.player().varps().varbit(varbit, 0)
					it.message("You have disabled <col=ff0000>${it.player().world().definitions().get(EnumDefinition::class.java, 834).getString(it.buttonSlot())}</col>.")
				}
			}
			return
		}
		
		val pts = it.player().varps().varbit(Varbit.SLAYER_POINTS)
		val required = it.player().world().definitions().get(EnumDefinition::class.java, 836).getInt(it.buttonSlot())
		val name = it.player().world().definitions().get(EnumDefinition::class.java, 834).getString(it.buttonSlot())
		
		if (required > pts) {
			it.message("You need $required points to unlock <col=ff0000>$name</col>.")
		} else {
			it.player().varps().varbit(varbit, 1)
			it.player().varps().varbit(Varbit.SLAYER_POINTS, pts - required)
			it.message("You have unlocked <col=ff0000>$name</col>.")
		}
	}
	
	fun cancelTask(it: Script) {
		val pts = it.player().varps().varbit(Varbit.SLAYER_POINTS)
		val required = 30
		
		if (30 > pts) {
			it.message("You need $required points to cancel your task.")
			
			if (it.player().world().realm().isPVP) {
				it.message("Alternatively, you can cancel your current task for ${Krystilia.BM_CANCEL_FEE} blood money by speaking to me.")
			}
		} else {
			it.player().putattrib(AttributeKey.SLAYER_TASK_ID, 0)
			it.player().putattrib(AttributeKey.SLAYER_TASK_AMT, 0)
			it.player().varps().varbit(Varbit.SLAYER_POINTS, pts - required)
			it.message("You have successfully cancelled your task.")
		}
	}
	
	fun blockTask(it: Script) {
		val pts = it.player().varps().varbit(Varbit.SLAYER_POINTS)
		val player = it.player()
		val required = 100
		
		if (100 > pts) {
			it.message("You need $required points to block your task.")
		} else {
			val assignedTask: SlayerCreature? = SlayerCreature.lookup(it.slayerTaskId())
			
			//Firstly we check to see if the task has already been blocked!
			BLOCKED_SLOTS.forEach { slot ->
				if (player.varps().varbit(slot) == assignedTask!!.uid) {
					player.message("You are already blocking ${QuestTab.capitalizeFirstLetter(assignedTask.toString().replace("_", " ").toLowerCase())}.")
					return
				}
			}
			
			//Next, we check if we have any available slots, if we do, BLOCK! :^ )
			BLOCKED_SLOTS.forEach { slot ->
				if (player.varps().varbit(slot) == 0) {
					it.player().varps().varbit(slot, assignedTask!!.uid)
					it.player().putattrib(AttributeKey.SLAYER_TASK_ID, 0)
					it.player().putattrib(AttributeKey.SLAYER_TASK_AMT, 0)
					it.player().varps().varbit(Varbit.SLAYER_POINTS, pts - required)
					it.message("You have successfully blocked your task.")
					return
				}
			}
		}
	}
	
	fun unblockTask(it: Script, varbit: Int) {
		it.player().varps().varbit(varbit, 0)
	}
	
	@JvmStatic fun isTaskBlocked(player: Player, task: SlayerTaskDef): Boolean {
		BLOCKED_SLOTS.forEach { slot ->
			if (player.varps().varbit(slot) == task.creatureUid)
				return true
		}
		return false
	}
}