package nl.bartpelle.veteres.content.areas.catherby.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/27/2015.
 */
object Harry {
	
	val HARRY = 1045
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption2(HARRY) @Suspendable { it.player().world().shop(21).display(it.player()) }
	}
}