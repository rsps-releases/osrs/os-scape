package nl.bartpelle.veteres.content.events.christmas.antisanta

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 12/22/2015.
 */

object AntiSanta {
	
	val ANTI_SANTA = 6760
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(6794) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 10) {
				it.chatNpc("What are you doing up here? Get out of my sight<br>before I lock you up with the palace guards!", ANTI_SANTA, 7184)
				when (it.options("Wait, I'm not here to cause trouble, I want to help.", "I'll just be on my way then.")) {
					1 -> {
						it.chatPlayer("Wait, I'm not here to cause trouble, I want to help.", 588)
						it.chatNpc("What makes you think I possibly need the help of some<br>mortal fool?", ANTI_SANTA, 7184)
						options(it)
					}
					2 -> {
						it.chatPlayer("I'll just be on my way then.", 588)
					}
				}
			} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) >= 20) {
				if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 50) {
					it.chatPlayer("He's busy preparing his troops. I should report back to Santa on what has occurred.")
				} else if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 40) {
					it.chatNpc("Finally, my army of sorrow brought to life!", ANTI_SANTA, 559)
					it.chatPlayer("Those creatures, they're made from the tears I collected?")
					it.chatNpc("Precisely! The sadness harvested from those tears has manifested into beings of pure sorrow, nothing will be able to stop them!", ANTI_SANTA, 559)
					it.chatPlayer("What are you planning to do with them?")
					it.chatNpc("I will prepare them for battle, and when the time is right, we shall storm the White Knights' Castle!", ANTI_SANTA, 559)
					it.player().inventory() += 13343 // TODO what if full!?
					it.player().varps().varbit(Varbit.XMAS2015_STAGE, 50)
					it.chatNpc("You have proven your inner malevolence and earned this reward.", ANTI_SANTA, 559)
					it.player().message("You've received a holiday item: <col=ff0000> Black santa hat</col>")
				} else if (it.player().inventory().contains(13351)) {
					it.chatNpc("Great work, I knew you'd have it in you! Give me a moment to purify these tears and we can continue.", ANTI_SANTA, 559)
					it.player().inventory() -= 13351
					it.player().inventory() += 13352
					it.messagebox("Anti-Santa takes the vial and absorbs a sorrowful essense from the liquid within.")
					it.chatNpc("I've purified the tears into this sorrowful solution, all you have to do is pour it into the machine over there!", ANTI_SANTA, 559)
				} else if (it.player().inventory().contains(13352)) {
					it.chatNpc("I've purified the tears into this sorrowful solution, all you have to do is pour it into the machine over there!", ANTI_SANTA, 559)
				} else if (!it.player().inventory().contains(13347) && it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 20) {
					it.chatNpc("You need to take a vial from the crate over there<br>before we can continue.", ANTI_SANTA, 7184)
				} else if (it.player().inventory().contains(13348) || it.player().inventory().contains(13349) || it.player().inventory().contains(13350)) {
					it.chatPlayer("I've collected some tears!")
					it.chatNpc("You need to fill the entire vial before we can move on. Go and collect some more!", ANTI_SANTA, 559)
				} else {
					it.chatPlayer("I've got the vial, now what?", 588)
					it.chatNpc("I need you to fill it with the tears of the innocent. The<br>children of Varrock make perfect candidates!", ANTI_SANTA, 7184)
					it.chatPlayer("Making children cry? How is that going to help?", 554)
					it.chatNpc("Let me worry about that, you just bring me back those<br>tears.", ANTI_SANTA, 7184)
					it.chatPlayer("Upsetting children during the holiday season? That's<br>easier said than done.", 555)
					it.chatNpc("Use the presents I've prepared over there, I'm sure<br>they'll get those little brats crying in no time!", ANTI_SANTA, 7184)
				}
			}
		}
	}
	
	@Suspendable fun options(it: Script) {
		when (it.options("I've grown sick of Christmas and all that it embodies.", "Err... I thought it'd be fun to be the bad guy for once.", "Mortal fool? I'm the one who's going to defeat you once and for all!")) {
			1 -> {
				it.chatPlayer("I've grown sick of Christmas and all that it embodies.")
				it.chatNpc("Ah, finally someone else who sees through the cheerful facade! But what exactly do you want to do, mortal?", ANTI_SANTA, 7184)
				options2(it)
			}
			2 -> {
				it.chatPlayer("Err... I thought it'd be fun to be the bad guy for once.", 554)
				it.chatNpc("Hahaha! I don't think you have what it takes to work<br>alongside me!", ANTI_SANTA, 7185)
				it.chatPlayer("Wait, that's not what I meant...", 588)
				options(it)
			}
			3 -> {
				it.chatPlayer("Mortal fool? I'm the one who's going to defeat you once and for all!")
				it.chatNpc("Hahaha! I'd like to see you try!", ANTI_SANTA, 7185)
			}
		}
	}
	
	@Suspendable fun options2(it: Script) {
		when (it.options("How about you just tell me what it is you're planning to do?", "I seek to shatter the illusion of hope this holiday embodies.")) {
			1 -> {
				it.chatPlayer("How about you just tell me what it is you're planning to do?", 589)
				it.chatNpc("Those who serve under me must first prove they are worthy! Now, answer my question.", ANTI_SANTA, 7184)
				options2(it)
			}
			2 -> {
				it.chatPlayer("I seek to shatter the illusion of hope this holiday embodies.", 589)
				it.chatNpc("Maybe there is a flicker of malignancy within you after all! We shall see how you fare on your first assignment!", ANTI_SANTA, 7184)
				it.chatPlayer("My first assignment?", 554)
				it.player().varps().varbit(Varbit.XMAS2015_STAGE, 20)
				it.chatNpc("I'll fill you in on the details once you've got the necessary supplies. You'll need a vial from the crate over there.", ANTI_SANTA, 7184)
			}
		}
	}
	
}
