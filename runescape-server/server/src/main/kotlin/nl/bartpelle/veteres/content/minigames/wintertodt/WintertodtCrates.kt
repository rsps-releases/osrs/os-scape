package nl.bartpelle.veteres.content.minigames.wintertodt

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 9/11/2016.
 */
object WintertodtCrates {
	
	val RARE_REWARDS = intArrayOf(20704, 20706, 20708, 20710, 20712, 20716, 20720)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(20703) {
			val p = it.player
			p.inventory().remove(Item(20703), true)
			
			if (p.world().rollDie(64, 1)) {
				p.inventory().add(Item(RARE_REWARDS.random()), true)
			} else {
				when (p.world().random(9)) {
					in 0..2 -> {
						when (p.world().random(5)) {
							0, 1 -> p.inventory().add(Item(1522, p.world().random(50..150)), true) // Oak logs
							2, 3 -> p.inventory().add(Item(6334, p.world().random(35..60)), true) // Teak logs
							4 -> p.inventory().add(Item(8836, p.world().random(20..50)), true) // Mahogany logs
							5 -> p.inventory().add(Item(1516, p.world().random(20..50)), true) // Yew logs
						}
					}
					in 3..5 -> {
						when (p.world().random(7)) {
							0, 1, 2 -> p.inventory().add(Item(7937, p.world().random(200..400)), true) // Pure essence
							3, 4 -> p.inventory().add(Item(445, p.world().random(20..70)), true) // Gold ore
							5, 6 -> p.inventory().add(Item(450, p.world().random(5..15)), true) // Adamantite ore
							7 -> p.inventory().add(Item(452, p.world().random(1..3)), true) // Runite ore
						}
					}
					6 -> {
						when (p.world().random(9)) {
							0, 1 -> p.inventory().add(Item(5295, p.world().random(1..5)), true) // Ranarr seed
							2, 3 -> p.inventory().add(Item(5300, p.world().random(1..10)), true) // Snapdragon seed
							4, 5 -> p.inventory().add(Item(5314, p.world().random(1..5)), true) // Maple seed
							6 -> p.inventory().add(Item(5315, p.world().random(1..4)), true) // Yew seeds
							7 -> p.inventory().add(Item(5316, p.world().random(1..2)), true) // Magic seed
							8 -> p.inventory().add(Item(5317, p.world().random(1..2)), true) // Spirit seed
							9 -> p.inventory().add(Item(5321, p.world().random(1..3)), true) // Watermelon seed
						}
					}
					in 7..9 -> {
						when (p.world().random(7)) {
							0, 1 -> p.inventory().add(Item(1618, p.world().random(2..10)), true) // Uncut diamond
							2, 3 -> p.inventory().add(Item(13422, p.world().random(5..24)), true) // Saltpetre
							4 -> p.inventory().add(Item(995, p.world().random(5050..8000)), true) // Coins
							5 -> p.inventory().add(Item(384, p.world().random(5..21)), true) // Raw shark
							6 -> p.inventory().add(Item(13574, p.world().random(10..20)), true) // Dynamite
							7 -> p.inventory().add(Item(20718, p.world().random(5..20)), true) // Burnt pages
						}
					}
				}
			}
		}
	}
	
}