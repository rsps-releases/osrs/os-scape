package nl.bartpelle.veteres.content.combat

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion
import nl.bartpelle.veteres.content.mechanics.Poison
import nl.bartpelle.veteres.content.mechanics.Venom
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Carl on 2015-08-14.
 */

object Potions {
	
	enum class Potion(val skill: Int, val base: Int, val percentage: Int, val defaultalgo: Boolean, val message: String, vararg val ids: Int) {
		ATTACK_POTION(Skills.ATTACK, 3, 10, true, "attack potion", 2428, 121, 123, 125),
		SUPER_ATTACK_POTION(Skills.ATTACK, 5, 15, true, "attack potion" /* sic */, 2436, 145, 147, 149),
		STRENGTH_POTION(Skills.STRENGTH, 3, 10, true, "strength potion", 113, 115, 117, 119),
		SUPER_STRENGTH_POTION(Skills.STRENGTH, 5, 15, true, "strength potion", 2440, 157, 159, 161),
		DEFENCE_POTION(Skills.DEFENCE, 3, 10, true, "defence potion", 2432, 133, 135, 137),
		SUPER_DEFENCE_POTION(Skills.DEFENCE, 5, 15, true, "defence potion", 2442, 163, 165, 167),
		MAGIC_POTION(Skills.MAGIC, 4, 0, true, "magic potion", 3040, 3042, 3044, 3046),
		SUPER_MAGIC_POTION(Skills.MAGIC, 2, 12, true, "magic potion", 11726, 11727, 11728, 11729),
		RANGING_POTION(Skills.RANGED, 4, 10, true, "ranging potion", 2444, 169, 171, 173),
		SUPER_RANGING_POTION(Skills.RANGED, 2, 12, true, "ranging potion", 11722, 11723, 11724, 11725),
		SARADOMIN_BREW(-1, -1, -1, false, "the foul liquid", 6685, 6687, 6689, 6691),
		SUPER_RESTORE(-1, -1, -1, false, "super restore potion", 3024, 3026, 3028, 3030),
		RESTORE_POTION(-1, -1, -1, false, "stat restoration potion", 2430, 127, 129, 131),
		COMBAT_POTION(-1, -1, -1, false, "combat potion", 9739, 9741, 9743, 9745),
		SUPER_COMBAT_POTION(-1, -1, -1, false, "super combat potion", 12695, 12697, 12699, 12701),
		PRAYER_POTION(-1, -1, -1, false, "restore prayer potion", 2434, 139, 141, 143),
		ANTIPOISON(-1, -1, -1, false, "antipoison potion", 2446, 175, 177, 179),
		SUPERANTIPOISON(-1, -1, -1, false, "antipoison" /*sic*/, 2448, 181, 183, 185),
		ANTIDOTE_PLUS(-1, -1, -1, false, "antidote plus" /*sic*/, 5943, 5945, 5947, 5949),
		ANTIDOTE_PLUSPLUS(-1, -1, -1, false, "antidote plusplus" /*sic*/, 5952, 5954, 5956, 5958),
		ANTIVENOM(-1, -1, -1, false, "anti-venom", 12905, 12907, 12909, 12911),
		ANTIVENOM_PLUS(-1, -1, -1, false, "anti-venom plus", 12913, 12915, 12917, 12919),
		STAMINA_POTION(-1, -1, -1, false, "stamina potion", 12625, 12627, 12629, 12631),
		SANFEW_SERUM(-1, -1, -1, false, "sanfew serum", 10925, 10927, 10929, 10931),
		ENERGY_POTION(-1, -1, -1, false, "energy potion", 3008, 3010, 3012, 3014),
		SUPER_ENERGY_POTION(-1, -1, -1, false, "super energy potion", 3016, 3018, 3020, 3022),
		ANTIFIRE_POTION(-1, -1, -1, false, "antifire potion", 2452, 2454, 2456, 2458),
		EXTENDED_ANTIFIRE_POTION(-1, -1, -1, false, "extended antifire potion", 11951, 11953, 11955, 11957),
		GUTHIX_REST(-1, -1, -1, false, "guthix rest", 4417, 4419, 4421, 4423),
		ZAMORAK_BREW(-1, -1, -1, false, "the foul liquid", 2450, 189, 191, 193),
		SUPER_ANTIFIRE(-1, -1, -1, false, "super antifire", 21978, 21981, 21984, 21987),
		EXTENDED_SUPER_ANTIFIRE(-1, -1, -1, false, "extended super antifire", 22209, 22212, 22215, 22218),

		// ENUM END . Ids are (4) (3) (2) (1)
		;
		
		fun nextDose(id: Int): Int {
			for (i in 0..ids.size) {
				if (ids[i] == id)
					return ids[i + 1]
			}
			return 0
		}
		
		fun isLastDose(id: Int): Boolean {
			return ids[ids.size - 1] == id
		}
		
		fun dosesLeft(id: Int): Int {
			for (i in 0..ids.size) {
				if (ids[i] == id)
					return ids.size - i - 1
			}
			return 0
		}
	}
	
	@JvmStatic fun get(potion_id: Int): Potion? {
		Potion.values().forEach { pot ->
			pot.ids.forEach { id ->
				if (id == potion_id) {
					return pot
				}
			}
		}
		
		return null
	}
	
	fun consume(it: Script, potion: Potion, id: Int) {
		if (it.player().timers().has(TimerKey.POTION))
			return
		
		if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_DRINKS)) {
			it.player().message("Drinks are disabled for this duel.")
			return
		}
		if (ClanWars.checkRule(it.player(), CWSettings.DRINKS)) {
			it.player().message("Drinks are disabled for this clan war.")
			return
		}
		if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_FOOD)
				&& (potion == Potion.SARADOMIN_BREW || potion == Potion.GUTHIX_REST)) {
			it.player().message("Food is disabled for this duel.")
			return
		}
		if (ClanWars.checkRule(it.player(), CWSettings.FOOD) && (potion == Potion.SARADOMIN_BREW || potion == Potion.GUTHIX_REST)) {
			it.player().message("Food is disabled for this clan war.")
			return
		}
		if (it.player().world().realm().isDeadman && it.player().timers().has(TimerKey.DEADMAN_BANK_COOLDOWN))
			return
		
		it.player().timers().register(TimerKey.POTION, 3)
		it.player().timers().register(TimerKey.FOOD, 3)
		val eatAnim = if (4084 == it.player().equipment()[3]?.id() ?: -1) 1469 else 829
		it.player().animate(eatAnim)
		it.player().sound(2401)
		
		if (potion != Potion.ANTIVENOM && potion != Potion.ANTIVENOM_PLUS && potion != Potion.GUTHIX_REST) {
			it.player().message("You drink some of your " + potion.message + ".")
		}
		
		deductDose(it, potion, id)
	}
	
	val VIAL = 229
	val EMPTY_CUP = 1980
	
	fun deductDose(it: Script, potion: Potion, id: Int) {
		val player = it.player()
		if (potion.isLastDose(id)) {
			val giveEmptyVials = player.attribOr<Boolean>(AttributeKey.GIVE_EMPTY_POTION_VIALS, true)
			
			if (giveEmptyVials)
				player.inventory()[player.attrib(AttributeKey.ITEM_SLOT)] = Item(if (potion == Potion.GUTHIX_REST) EMPTY_CUP else VIAL)
			else
				player.inventory().remove(Item(id, 1), true, player.attrib(AttributeKey.ITEM_SLOT))
			
			if (potion == Potion.ANTIVENOM) {
				player.message("You drink the rest of your antivenom potion.")
			} else if (potion == Potion.ANTIVENOM_PLUS) {
				player.message("You drink the rest of your super antivenom potion.")
			} else if (potion != Potion.GUTHIX_REST) {
				player.message("You have finished your potion.")
			}
		} else {
			player.inventory()[player.attrib(AttributeKey.ITEM_SLOT)] = Item(potion.nextDose(id))
			val left = potion.dosesLeft(id)
			
			var doses = if (left == 1) "dose" else "doses"
			if (potion == Potion.ANTIVENOM) {
				player.message("You drink some of your antivenom potion, leaving $left $doses.")
			} else if (potion == Potion.ANTIVENOM_PLUS) {
				player.message("You drink some of your super antivenom potion, leaving $left $doses.")
			} else if (potion != Potion.GUTHIX_REST) {
				player.message("You have $left $doses of your potion left.")
			}
		}
		
		if (potion.defaultalgo) {
			val change = potion.base.toDouble() + (player.skills().xpLevel(potion.skill).toDouble() * potion.percentage / 100.0)
			player.skills().alterSkill(potion.skill, change.toInt())
		} else if (potion == Potion.SARADOMIN_BREW) {
			val heal = (player.skills().xpLevel(Skills.HITPOINTS) * 0.15).toInt() + 2;
			val def = (player.skills().xpLevel(Skills.DEFENCE) * 0.2).toInt() + 2;
			
			player.heal(heal, 16)
			player.skills().alterSkill(Skills.DEFENCE, def);
			
			val ids = intArrayOf(Skills.ATTACK, Skills.STRENGTH, Skills.MAGIC, Skills.RANGED);
			
			for (i in ids) {
				val lvl = player.skills().xpLevel(i);
				player.skills().alterSkill(i, -(lvl * 0.1).toInt())
			}
		} else if (potion == Potion.ZAMORAK_BREW) {
			val attackIncrease = (player.skills().xpLevel(Skills.ATTACK) * 0.2).toInt() + 2
			val strengthIncrease = (player.skills().xpLevel(Skills.STRENGTH) * 0.12).toInt() + 2
			val prayerIncrease = (player.skills().xpLevel(Skills.PRAYER) * 0.1).toInt()
			
			val defenceDecrease = (player.skills().xpLevel(Skills.DEFENCE) * 0.1).toInt() + 2
			val hpDecrease = (player.hp() * 0.1).toInt() + 2
			
			player.skills().alterSkill(Skills.ATTACK, attackIncrease)
			player.skills().alterSkill(Skills.STRENGTH, strengthIncrease)
			player.skills().replenishSkill(Skills.PRAYER, prayerIncrease)
			player.skills().alterSkill(Skills.DEFENCE, -defenceDecrease)
			player.hit(player, hpDecrease, 0, false).block(false).submit()
			
		} else if (potion == Potion.SUPER_RESTORE) {
			for (i in 0..Skills.SKILL_COUNT - 1) {
				if (i != Skills.HITPOINTS) {
					val current_flat = player.skills().xpLevel(i)
					var restorable = (current_flat * 0.25 + 8).toInt()
					
					if (i == Skills.PRAYER) {
						if (player.inventory().has(6714) && Equipment.wearingMaxCape(player)) { // Max cape holy wrench effect
							if (current_flat > 25 && current_flat <= 85) {
								restorable += 1
							} else if (current_flat > 85) {
								restorable += 2
							}
						}
					}
					player.skills().replenishSkill(i, restorable)
				}
			}
		} else if (potion == Potion.RESTORE_POTION) {
			for (i in intArrayOf(Skills.ATTACK, Skills.DEFENCE, Skills.STRENGTH, Skills.MAGIC, Skills.RANGED)) {
				if (i != Skills.HITPOINTS && i != Skills.PRAYER) {
					val current_flat = player.skills().xpLevel(i)
					var restorable = (current_flat * 0.30 + 10).toInt()
					
					player.skills().replenishSkill(i, restorable)
				}
			}
		} else if (potion == Potion.SUPER_COMBAT_POTION) {
			val curStr = player.skills().xpLevel(Skills.STRENGTH)
			val curAtk = player.skills().xpLevel(Skills.ATTACK)
			val curDef = player.skills().xpLevel(Skills.DEFENCE)
			
			player.skills().alterSkill(Skills.ATTACK, (curAtk * 0.1).toInt() + 10)
			player.skills().alterSkill(Skills.STRENGTH, (curStr * 0.1).toInt() + 10)
			player.skills().alterSkill(Skills.DEFENCE, (curDef * 0.1).toInt() + 10)
		} else if (potion == Potion.COMBAT_POTION) {
			val curStr = player.skills().xpLevel(Skills.STRENGTH)
			val curAtk = player.skills().xpLevel(Skills.ATTACK)
			
			player.skills().alterSkill(Skills.ATTACK, (curAtk * 0.1).toInt() + 3)
			player.skills().alterSkill(Skills.STRENGTH, (curStr * 0.1).toInt() + 3)
		} else if (potion == Potion.PRAYER_POTION) {
			val lv = player.skills().xpLevel(Skills.PRAYER)
			val cur = player.skills().level(Skills.PRAYER)
			val addition = 7 + lv / 4
			var newval = cur + addition
			if (player.inventory().has(6714) && Equipment.wearingMaxCape(player)) { // Max cape holy wrench effect
				if (lv > 25 && lv <= 85) {
					newval += 1
				} else if (lv > 85) {
					newval += 2
				}
			}
			if (newval > lv) {
				player.skills().setLevel(Skills.PRAYER, lv)
			} else {
				player.skills().setLevel(Skills.PRAYER, newval)
			}
		} else if (potion == Potion.ANTIPOISON) {
			if (Venom.venomed(player))
				Venom.cure(1, player)
			else {
				if (player.varps()[Varp.HP_ORB_COLOR] > -1)
					player.filterableMessage("It grants you immunity from poison for 90 seconds.")
				Poison.cureAndImmune(player, 6)
			}
		} else if (potion == Potion.SUPERANTIPOISON) {
			if (Venom.venomed(player))
				Venom.cure(1, player)
			else {
				if (player.varps()[Varp.HP_ORB_COLOR] > -1)
					player.filterableMessage("It grants you immunity from poison for six minutes.")
				Poison.cureAndImmune(player, 23) // Longer immunity is the only difference
			}
		} else if (potion == Potion.ANTIDOTE_PLUS) { // basicaly super super anti poison - doesnt cure venom
			if (player.varps()[Varp.HP_ORB_COLOR] > -1)
				player.filterableMessage("It grants you immunity from poison for nine minutes.")
			Poison.cureAndImmune(player, 35) // 8 mins 45s
		} else if (potion == Potion.ANTIDOTE_PLUSPLUS) {
			if (player.varps()[Varp.HP_ORB_COLOR] > -1)
				player.filterableMessage("It grants you immunity from poison for twelve minutes.")
			Poison.cureAndImmune(player, 48) // 12 mins
		} else if (potion == Potion.ANTIVENOM) {
			Venom.cure(2, player) //  wiki does not specify if it provides immunity!
		} else if (potion == Potion.ANTIVENOM_PLUS) {
			Venom.cure(3, player) // 3 mins venom immunity
			Poison.cureAndImmune(player, 12) // 3 mins poison immunity
		} else if (potion == Potion.STAMINA_POTION) {
			player.varps().varbit(Varbit.STAMINA_POTION, 1)
			player.putattrib(AttributeKey.STAMINA_POTION, 11)
			player.setRunningEnergy(player.attribOr<Double>(AttributeKey.RUN_ENERGY, 100.0) + 20.0, true)
			/*if (player.weight >= 55) {
				player.message("<col=7F00FF>As you weigh over 54kg, the stamina potions' effects will not work to reduce energy lost when moving.")
			}*/
		} else if (potion == Potion.ANTIFIRE_POTION) {
			if (player.attribOr<Int>(AttributeKey.ANTIFIRE_POTION, 0) < 1)
				player.filterableMessage("It grants you partial protection from dragonfire for six minutes.")
			player.putattrib(AttributeKey.ANTIFIRE_POTION, 32)
		} else if (potion == Potion.EXTENDED_ANTIFIRE_POTION) {
			if (player.attribOr<Int>(AttributeKey.ANTIFIRE_POTION, 0) < 1)
				player.filterableMessage("It grants you partial protection from dragonfire for twelve minutes.")
			player.putattrib(AttributeKey.ANTIFIRE_POTION, 62)
		} else if (potion == Potion.SUPER_ANTIFIRE) {
			if (player.attribOr<Int>(AttributeKey.ANTIFIRE_POTION, 0) < 1)
				player.filterableMessage("It grants you complete protection from dragonfire for three minutes.")
			player.putattrib(AttributeKey.ANTIFIRE_POTION, 1010) // 1000 base
		} else if (potion == Potion.EXTENDED_SUPER_ANTIFIRE) {
			if (player.attribOr<Int>(AttributeKey.ANTIFIRE_POTION, 0) < 1)
				player.filterableMessage("It grants you complete protection from dragonfire for six minutes.")
			player.putattrib(AttributeKey.ANTIFIRE_POTION, 1032)
		} else if (potion == Potion.SANFEW_SERUM) {
			// Literally super restore & super antipoison in one.
			// Restore effect:
			for (i in 0..Skills.SKILL_COUNT - 1) {
				if (i != Skills.HITPOINTS) {
					val current_flat = player.skills().xpLevel(i)
					var restorable = (current_flat * 0.25 + 8).toInt()
					
					if (i == Skills.PRAYER) {
						if (player.inventory().has(6714) && Equipment.wearingMaxCape(player)) { // Max cape holy wrench effect
							if (current_flat > 25 && current_flat <= 85) {
								restorable += 1
							} else if (current_flat > 85) {
								restorable += 2
							}
						}
					}
					player.skills().replenishSkill(i, restorable)
				}
			}
			
			if (Venom.venomed(player)) {
				// Venom turns to poison.
				Venom.cure(1, player)
			} else {
				if (player.varps()[Varp.HP_ORB_COLOR] > -1)
					player.filterableMessage("It grants you immunity from poison for six minutes.")
				Poison.cureAndImmune(player, 24)
			}
		} else if (potion == Potion.ENERGY_POTION) {
			player.setRunningEnergy(player.attribOr<Double>(AttributeKey.RUN_ENERGY, 100.0) + 10.0, true)
		} else if (potion == Potion.SUPER_ENERGY_POTION) {
			player.setRunningEnergy(player.attribOr<Double>(AttributeKey.RUN_ENERGY, 100.0) + 20.0, true)
		} else if (potion == Potion.GUTHIX_REST) {
			it.message("You drink the herbal tea.")
			// Source: https://www.youtube.com/watch?v=IskZmEHfFtM
			// Jak: have also tested on tourny worlds 25/8/16
			// Re-examined 25/11/2017 added venom/poison support as of this video.
			if (Venom.venomed(player)) {
				// Venom turns to poison. doesn't cure poison.
				Venom.cure(1, player, false)
				it.message("The tea dilutes the venom.")
			} else if (player.varps().varp(Varp.HP_ORB_COLOR) > 0) {
				it.message("The tea dilutes some of the poison.")
				player.varps().varp(Varp.HP_ORB_COLOR, Math.max(0, player.varps().varp(Varp.HP_ORB_COLOR) - 8))
			}
			player.heal(5, 5)
			player.setRunningEnergy(player.attribOr<Double>(AttributeKey.RUN_ENERGY, 0.0) + 5, true)
			it.message("The tea boosts your hitpoints.")
		}
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onLogin { it.player().timers()[TimerKey.STAT_REPLENISH] = 100 }
		repo.onTimer(TimerKey.STAT_REPLENISH) {
			it.player().skills().replenishStats()
			
			if (it.player().varps().varbit(Varbit.PRESERVE) == 1)
				it.player().timers()[TimerKey.STAT_REPLENISH] = 120
			else
				it.player().timers()[TimerKey.STAT_REPLENISH] = 100
		}
		
		repo.onLogin { it.player().timers()[TimerKey.RAPID_HEAL_TIMER] = 50 }
		repo.onTimer(TimerKey.RAPID_HEAL_TIMER) {
			if (it.player().varps().varbit(Varbit.RAPID_HEAL) == 1 || Equipment.wearingMaxCape(it.player())
					|| CapeOfCompletion.HITPOINT.operating(it.player())) {
				if (!it.player().dead()) {
					it.player().skills().replenishSkill(Skills.HITPOINTS, 1)
				}
			}
			it.player().timers()[TimerKey.RAPID_HEAL_TIMER] = 50
		}
		
		Potion.values().forEach { pot ->
			pot.ids.forEach { id ->
				repo.onItemOption1(id) @Suspendable {
					consume(it, pot, id)
				}
				repo.onItemOption4(id) {
					it.player().inventory()[it.player()[AttributeKey.ITEM_SLOT]] = Item(229)
					it.message("You empty the vial.")
				}
			}
		}
	}
	
}
