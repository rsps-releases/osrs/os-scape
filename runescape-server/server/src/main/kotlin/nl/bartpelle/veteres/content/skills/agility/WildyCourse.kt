package nl.bartpelle.veteres.content.skills.agility

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.AnimateObject
import nl.bartpelle.veteres.net.message.game.command.SetMapBase
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 13/06/2016.
 */
object WildyCourse {
	
	val LOWER_GATE = 23555
	val UPPERGATE_EAST = 23552
	val UPPERGATE_WEST = 23554
	val PIPE = 23137
	val ROPESWING = 23132
	val LADDERDOWN = 14758
	// ladder up is same as barb course
	val STEPPINGSTONE = 23556
	val LOGBALANCE = 23542
	val ROCKS = 23640
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(LOWER_GATE) @Suspendable {
			lowergate(it)
		}
		repo.onObject(UPPERGATE_EAST) @Suspendable {
			uppergate(it)
		}
		repo.onObject(UPPERGATE_WEST) @Suspendable {
			uppergate(it)
		}
		repo.onObject(PIPE, s@ @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (it.player().tile().z > 3940)
				return@s
			
			it.player().lockDelayDamage()
			
			// Go to the right spot if we're not there
			if (!it.player().tile().equals(obj.tile().transform(0, -1, 0))) {
				it.player().walkTo(obj.tile().transform(0, -1, 0), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(obj.tile().transform(0, -1, 0))
			}
			
			it.player().animate(749, 30)
			it.player().forceMove(ForceMovement(0, 0, 0, 3, 33, 126, FaceDirection.NORTH))
			it.delay(3)
			it.player().teleport(obj.tile().x, obj.tile().z + 2)
			it.delay(2)
			it.player().teleport(it.player().tile().transform(0, 6))
			it.delay(1)
			it.player().pathQueue().interpolate(it.player().tile().x, it.player().tile().z + 1)
			it.delay(1)
			it.player().animate(749, 30)
			it.player().forceMove(ForceMovement(0, 0, 0, 3, 33, 126, FaceDirection.NORTH))
			it.delay(3)
			it.player().teleport(3004, 3950)
			it.player().unlock()
			putStage(it.player(), 1)
			it.addXp(Skills.AGILITY, 12.5)
		})
		repo.onObject(ROPESWING, s@ @Suspendable {
			val obj = it.interactionObject()
			if (it.player().tile().z > 3953) {
				it.player().message("You can't swing from here.")
				return@s
			}
			if (!it.player().tile().equals(3005, 3953)) { // Get in position
				it.player().walkTo(3005, 3953, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(3005, 3953))
			}
			it.player().faceTile(it.player().tile().transform(0, 10))
			it.delay(1)
			it.player().lockDelayDamage()
			it.player().write(SetMapBase(it.player(), obj.tile())) // set base
			it.player().write(AnimateObject(obj, 54)) // make rope pull back
			it.player().animate(751) // swing
			val success = Barbarian.successful(it.player())
			if (success) {
				it.player().forceMove(ForceMovement(0, 0, 0, 5, 30, 50, FaceDirection.NORTH)) // move
				it.delay(1)
				it.player().write(AnimateObject(obj, 55)) // make the rope go forwards while we swing
				it.player().teleport(it.player().tile().transform(0, 5))
				putStage(it.player(), 2)
				it.addXp(Skills.AGILITY, 20.0)
			} else {
				it.player().forceMove(ForceMovement(0, 0, 0, 3, 30, 50, FaceDirection.SOUTH))
				it.delay(2)
				it.player().teleport(Tile(3004, 10355))
				it.player().animate(766, 5)
				it.player().hit(null, it.player().world().random(5..7)).block(false)
				it.delay(1)
				it.player().pathQueue().interpolate(3004, 10354, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(3004, 10354))
			}
			it.player().unlock()
		})
		repo.onObject(LADDERDOWN) @Suspendable {
			Ladders.ladderDown(it, it.player().tile().transform(0, 6400), true)
		}
		repo.onObject(STEPPINGSTONE, s@ @Suspendable {
			if (it.player().tile().x < 3002) {
				it.player().message("You can't cross those from here.")
				return@s
			}
			it.player().lockDelayDamage()
			it.player().faceTile(Tile(0, it.player().tile().z))
			for (i in 0..5) {
				it.player().animate(741)
				it.player().forceMove(ForceMovement(0, 0, -1, 0, 5, 35, FaceDirection.WEST))
				it.delay(1)
				it.player().teleport(it.player().tile().transform(-1, 0))
				if (i == 2) {
					// TODO failure here. fall into lava
				}
				if (i != 4) { // No delay after you finish the last tile.
					it.delay(2)
				}
			}
			putStage(it.player(), 4)
			it.addXp(Skills.AGILITY, 20.0)
			it.player().unlock()
		})
		repo.onObject(LOGBALANCE) @Suspendable {
			if (!it.player().tile().equals(3002, 3945)) { // Get in position
				it.player().walkTo(3002, 3945, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(3002, 3945))
			}
			it.delay(1)
			it.player().lockDelayDamage()
			it.message("You walk carefully across the slippery log...")
			it.player().pathQueue().clear()
			val end = Tile(2994, 3945)
			it.player().pathQueue().interpolate(end, PathQueue.StepType.FORCED_WALK)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			val success = Barbarian.successful(it.player())
			if (success) {
				it.waitForTile(end)
				it.player().looks().resetRender()
				putStage(it.player(), 8)
				it.addXp(Skills.AGILITY, 20.0)
				it.message("...You make it safely to the other side.")
			} else {
				it.delay(3)
				it.player().pathQueue().clear()
				it.animate(771)
				it.message("...You loose your footing and fall!")
				it.delay(1)
				it.player().looks().render(772, 772, 772, 772, 772, 772, -1)
				it.player().teleport(2998, 10346)
				it.player().hit(null, it.player().world().random(5..7)).block(false)
				it.player().pathQueue().interpolate(2998, 10345, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2998, 10345))
				it.player().looks().resetRender()
			}
			it.player().unlock()
		}
		repo.onObject(ROCKS) @Suspendable {
			if (!it.player().tile().equals(2995, 3937)) { // Get in position
				it.player().walkTo(2995, 3937, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2995, 3937))
			}
			it.delay(1)
			it.player().lockDelayDamage()
			it.player().animate(740)
			it.player().forceMove(ForceMovement(0, 0, 0, -4, 5, 80, FaceDirection.SOUTH)) // move
			it.delay(3)
			it.player().teleport(Tile(2995, 3933))
			it.player().looks().resetRender()
			it.player().animate(-1)
			if (it.player().attribOr<Int>(AttributeKey.WILDY_COURSE_STATE, 0) == 15) {
				it.player().putattrib(AttributeKey.WILDY_COURSE_STATE, 0)
				it.addXp(Skills.AGILITY, 498.9)
				
				// Woo! A pet!
				val odds = (18000.00 * it.player().mode().skillPetMod()).toInt()
				if (it.player().world().rollDie(odds, 1)) {
					UnlockAgilityPet.unlockGiantSquirrel(it.player())
				}
				
			}
			
			//Finding blood money while skilling..
			if (it.player().world().realm().isOSRune && WildernessLevelIndicator.inWilderness(it.player().tile())) {
				if (it.player().world().rollDie(5, 1)) {
					if (it.player().inventory().add(Item(13307, 5), false).success()) {
						it.player().message("You find 5 blood money coin on the ground.")
					}
				}
			}
			
			it.player().unlock()
		}
	}
	
	@Suspendable @JvmStatic fun uppergate(it: Script) {
		val obj = it.interactionObject()
		val player = it.player()
		if (!it.player().tile().equals(obj.tile())) {
			it.player().walkTo(obj.tile(), PathQueue.StepType.FORCED_WALK)
			it.waitForTile(obj.tile())
		}
		it.player().faceTile(it.player().tile().transform(0, -1))
		it.delay(1)
		// Walk away!
		player.lockDelayDamage()
		
		// Agility animation
		player.pathQueue().clear()
		val end = Tile(2998, 3916)
		player.pathQueue().interpolate(end, PathQueue.StepType.FORCED_WALK)
		player.looks().render(763, 762, 762, 762, 762, 762, -1)
		// The ending doors
		openDoubleDoors(it, player)
		
		// Walk to the end
		it.delay(13)
		openEntranceGate(it, player, MapObj(Tile(2998, 3917), LOWER_GATE, 0, 3))
		it.waitForTile(end)
		player.looks().resetRender()
		player.unlock()
	}
	
	@Suspendable @JvmStatic fun lowergate(it: Script) {
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		if (player.skills().xpLevel(Skills.AGILITY) < 52) {
			player.message("You need a Agility level of 52 to pass this gate.")
			return
		}
		if (obj.valid(player.world()) && obj.interactAble() && obj.tile().equals(2998, 3917)) {
			player.lockDelayDamage()
			
			// Agility animation
			player.pathQueue().clear()
			val end = Tile(2998, 3931)
			player.pathQueue().interpolate(end, PathQueue.StepType.FORCED_WALK)
			player.looks().render(763, 762, 762, 762, 762, 762, -1)
			
			// Handle the door
			openEntranceGate(it, player, obj)
			
			// Finish up agility part
			it.delay(13)

			// The ending doors
			openDoubleDoors(it, player)
			it.waitForTile(end)
			player.looks().resetRender()
			player.unlock()
		}
	}
	
	@Suspendable private fun openEntranceGate(it: Script, player: Player, obj: MapObj) {
		obj.interactAble(false)
		player.world().removeObj(obj, false)
		val newobj = MapObj(Tile(2997, 3917), 1548, obj.type(), 2).interactAble(false)
		player.world().spawnObj(newobj)
		it.delay(2)
		
		// Put the old door back
		player.world().removeObj(newobj)
		player.world().spawnObj(obj)
		obj.interactAble(true)
	}
	
	@Suspendable private fun openDoubleDoors(it: Script, player: Player) {
		val enddoorE = player.world().objById(UPPERGATE_EAST, Tile(2998, 3931))
		var enddoorW: MapObj? = null
		val replacementW = MapObj(Tile(2997, 3930), 1574, 0, 0).interactAble(false)
		val replacementE =  MapObj(Tile(2998, 3930), 1573, 0, 2).interactAble(false)

		if (enddoorE.valid(player.world()) && enddoorE.interactAble()) {
			enddoorE.interactAble(false)
			player.world().removeObj(enddoorE)
			player.world().spawnObj(replacementE)
			
			// Try opening the partnering door
			enddoorW = player.world().objById(UPPERGATE_WEST, Tile(2997, 3931))
			if (enddoorW.valid(player.world()) && enddoorW.interactAble()) {
				enddoorW.interactAble(false)
				player.world().removeObj(enddoorW)
				player.world().spawnObj(replacementW)
			}
		}
		// Open up the doors at the end.. if they're not in use already!
		it.delay(2)
		player.world().removeObj(replacementE)
		player.world().spawnObj(enddoorE)
		enddoorE.interactAble(true)
		if (enddoorW != null) {
			player.world().spawnObj(enddoorW)
			player.world().removeObj(replacementW)
			enddoorW.interactAble(true)
		}
	}

	private fun putStage(player: Player, stageBit: Int): Int {
		var stage = player.attribOr<Int>(AttributeKey.WILDY_COURSE_STATE, 0)
		stage = stage or stageBit
		player.putattrib(AttributeKey.WILDY_COURSE_STATE, stage)
		return stage
	}
}