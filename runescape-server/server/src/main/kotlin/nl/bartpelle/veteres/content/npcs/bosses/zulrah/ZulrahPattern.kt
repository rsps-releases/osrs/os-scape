package nl.bartpelle.veteres.content.npcs.bosses.zulrah

import nl.bartpelle.veteres.content.npcs.bosses.zulrah.ZulrahConfig.*
import nl.bartpelle.veteres.content.npcs.bosses.zulrah.ZulrahForm.*
import nl.bartpelle.veteres.content.npcs.bosses.zulrah.ZulrahPosition.*
import nl.bartpelle.veteres.content.random

/**
 * Created by Bart on 3/6/2016.
 */
class ZulrahPattern(vararg val phases: ZulrahPhase) {
	
	companion object {
		
		val PATTERN_ONE = ZulrahPattern(
				ZulrahPhase(RANGE, CENTER, arrayOf(FULL_TOXIC_FUMES, NO_ATTACK)),
				ZulrahPhase(MELEE, CENTER),
				ZulrahPhase(MAGIC, CENTER),
				ZulrahPhase(RANGE, SOUTH, arrayOf(SNAKELINGS_CLOUDS_SNAKELINGS)),
				ZulrahPhase(MELEE, CENTER),
				ZulrahPhase(MAGIC, WEST),
				ZulrahPhase(RANGE, SOUTH, arrayOf(EAST_SNAKELINGS_REST_FUMES, NO_ATTACK)),
				ZulrahPhase(MAGIC, SOUTH, arrayOf(SNAKELING_FUME_MIX)),
				ZulrahPhase(JAD_RM, WEST, arrayOf(FULL_TOXIC_FUMES)),
				ZulrahPhase(MELEE, CENTER)
		)
		
		
		val PHASES = arrayOf(PATTERN_ONE)
		
		fun randomPattern(): ZulrahPattern {
			return PHASES.random()
		}
	}
	
	operator fun get(i: Int): ZulrahPhase {
		return phases[i];
	}
	
}