package nl.bartpelle.veteres.content.combat.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.item.Item

/**
 * Represents a single magic spell.
 * @author Mack
 */
interface MagicSpell {

    @Suspendable fun able(it: Script, alert: Boolean = false): Boolean

   @Suspendable fun execute(it: Script)

    fun runes(): Array<Item>
}