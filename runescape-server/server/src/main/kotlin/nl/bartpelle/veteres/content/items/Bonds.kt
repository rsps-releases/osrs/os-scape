package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.services.financial.BMTPostback
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer
import java.text.NumberFormat

/**
 * Created by Bart on 2/13/2016.
 */
object Bonds {
	
	const val DEADMAN_INVITE = 13189
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(13190) @Suspendable {
			it.itemBox("These are credits. You can use them to purchase items in the OS-Scape store. Redeeming these grants you one credit each.", 13190)
			
			val redeemable = it.player().inventory().count(13190)
			when (it.options("Redeem one for 1 credit.", "Redeem ${NumberFormat.getInstance().format(redeemable)} for ${NumberFormat.getInstance().format(redeemable)} credit(s).", "Never mind.")) {
				1 -> {
					if (it.player().inventory().remove(Item(13190), false).success()) {
						it.player().world().server().service(BMTPostback::class.java, true).ifPresent { service ->
							service.redeemBond(it.player().id() as Int, 1, true)
						}
					}
				}
				2 -> {
					val redeemed = it.player().inventory().remove(Item(13190, 1000000000), true).completed()
					it.player().world().server().service(BMTPostback::class.java, true).ifPresent { service ->
						service.redeemBond(it.player().id() as Int, redeemed, true)
					}
				}
			}
		}
		
		r.onItemOption1(DEADMAN_INVITE) @Suspendable {
			if (it.player.deadmanParticipant()) {
				it.itemBox("It's a Deadman pass. Since you're already participating, all you can do is sell it to other players. For more information, see https://www.os-scape.com/deadman or ::deadman.", DEADMAN_INVITE)
			} else {
				it.itemBox("It's a Deadman pass. With this pass, you can claim Deadman invitational access on one account, or sell it to other players. For more information, see https://www.os-scape.com/deadman or ::deadman.", DEADMAN_INVITE)
				it.messagebox("To redeem the Deadman pass, right-click it and press 'Redeem'.")
			}
		}
		r.onItemOption2(DEADMAN_INVITE) @Suspendable {
			if (it.player.deadmanParticipant()) {
				it.itemBox("You're already enlisted as participant in the next Deadman invitational. However, you can still sell this pass to other players.", DEADMAN_INVITE);
			} else {
				it.itemBox("Claiming the Deadman pass is permanent - once claimed, it cannot be undone. You will grant this very account, ${it.player.name()}, access to the next Deadman invitational on OS-Scape. Are you sure you want to participate?", DEADMAN_INVITE)
				
				if (it.optionsTitled("Join Deadman?", "Yes, enlist ${it.player.name()} for Deadman.", "No, I have changed my mind. Cancel.") == 1) {
					if (it.player.inventory().remove(Item(DEADMAN_INVITE), true).success()) {
						it.player.world().server().service(PgSqlPlayerSerializer::class.java, true).ifPresent { sql ->
							sql.joinDeadman(it.player)
						}
						
						it.player().deadmanParticipant(true)
						it.itemBox("Your invite to the Deadman invitational has been claimed. Keep an eye out for the announcement on the OS-Scape website. We'll also send you an email when it gets announced.", DEADMAN_INVITE)
					}
				}
			}
		}
	}
	
}