package nl.bartpelle.veteres.content.areas.zeah

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/1/2016.
 */

object HandHolds {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(27362) @Suspendable {
			val handholds: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val west_handholds_down = Tile(1459, 3690)
			val wast_handholds_up = Tile(1455, 3690)
			
			if (handholds.tile() == west_handholds_down) {
				if (!it.player().tile().equals(handholds.tile().transform(1, 0, 0))) {
					it.player().walkTo(handholds.tile().transform(1, 0, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(handholds.tile().transform(1, 0, 0))
				}
				it.player().lock()
				it.player().faceTile(Tile(1462, 3690))
				it.delay(1)
				it.player().animate(1148, 20)
				it.player().forceMove(ForceMovement(0, 0, -6, 0, 25, 135, FaceDirection.EAST))
				it.delay(4)
				it.player().teleport(west_handholds_down.x - 4, west_handholds_down.z)
				it.player().unlock()
			} else if (handholds.tile() == wast_handholds_up) {
				if (!it.player().tile().equals(handholds.tile().transform(-1, 0, 0))) {
					it.player().walkTo(handholds.tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(handholds.tile().transform(-1, 0, 0))
				}
				it.player().lock()
				it.player().faceTile(Tile(1462, 3690))
				it.delay(1)
				it.player().animate(1148, 15)
				it.player().forceMove(ForceMovement(0, 0, 6, 0, 25, 135, FaceDirection.EAST))
				it.delay(4)
				it.player().teleport(wast_handholds_up.x + 4, wast_handholds_up.z)
				it.player().unlock()
			}
		}
	}
	
}
