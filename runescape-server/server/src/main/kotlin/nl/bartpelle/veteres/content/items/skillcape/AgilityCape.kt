package nl.bartpelle.veteres.content.items.skillcape

import nl.bartpelle.skript.Script

/**
 * Created by Jason MacKeigan on 2016-07-05 at 4:01 PM
 */
object AgilityCape : CapeOfCompletionPerk(intArrayOf(1)) {
	
	override fun option(option: Int): Function1<Script, Unit> = {
		
	}
	
}