package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 5/11/2016.
 */

object DemonicGorilla {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		// Repeat while target valid
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			
			var closeAttack = false
			if (npc.world().rollDie(2, 1)) { // 50% chance for a long range attack.
				if (EntityCombat.canAttackMelee(npc, target, true) && EntityCombat.attackTimerReady(npc)) {
					primary_melee_attack(npc, target)
					closeAttack = true
				}
			}
			if (!closeAttack) {
				if (EntityCombat.canAttackDistant(npc, target, true, 7) && EntityCombat.attackTimerReady(npc)) {
					
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					
					val random = npc.world().random(5)
					when (random) {
						1 -> magic_attack(npc, target)
						2 -> ranged_attack(npc, target)
						3 -> magic_attack(npc, target)
						4 -> ranged_attack(npc, target)
						else -> explosive_rock(it, npc, target)
						
					}
				}
			}
			
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun primary_melee_attack(npc: Npc, target: Entity) {
		npc.animate(npc.attackAnimation())
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, EntityCombat.randomHit(npc))
		else
			target.hit(npc, 0)
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
		
	}
	
	@Suspendable fun ranged_attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
		
		npc.animate(7227)
		npc.world().spawnProjectile(npc, target, 1302, 45, 30, 35, 9 * tileDist, 7, 5)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE))
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.RANGE)
		else
			target.hit(npc, 0, delay.toInt())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	
	@Suspendable fun magic_attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
		
		npc.animate(7238)
		npc.world().spawnProjectile(npc, target, 1304, 10, 10, 40, 11 * tileDist, 7, 3)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC))
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(1305)
		else
			target.hit(npc, 0, delay.toInt())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	
	@Suspendable fun explosive_rock(it: Script, npc: Npc, target: Entity) {
		val target_tile = Tile(target.tile().x, target.tile().z)
		npc.animate(7228)
		npc.world().spawnProjectile(target.tile().transform(1, 1, 0), target_tile, 856, 230, 6, 30, 200, 25, 5)
		target.world().tileGraphic(305, target_tile, 5, 230)
		
		it.runGlobal(target.world()) @Suspendable {
			it.delay(8)
			if (target.tile().inSqRadius(Tile(target_tile), 1)) {
				target.hit(npc, target.world().random(35)).combatStyle(CombatStyle.MAGIC)
			}
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
}

