package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Situations on 10/11/2016.
 */

object AbyssalSireSpawn {
	
	val ABYSSAL_SIRE_SPAWN = 5916
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//When the NPC spawns, start the transformation to Scion timer..
		r.onNpcSpawn(ABYSSAL_SIRE_SPAWN) { it.npc().timers().addOrSet(TimerKey.ABYSSAL_SIRE_SPAWN_TRANSFORM, 20) }
		
		//When the timer finishes, start the transformation..
		r.onTimer(TimerKey.ABYSSAL_SIRE_SPAWN_TRANSFORM) s@ {
			val abyssalSireSpawn = it.npc()
			val abyssalSireOwner = abyssalSireSpawn.attribOr<Npc?>(AttributeKey.ABYSSAL_SIRE_SPAWN_OWNER, null)
			if (abyssalSireOwner != null) {
				val abyssalSireSpawns = abyssalSireOwner.attribOr<ArrayList<Npc>>(AttributeKey.ABYSSAL_SIRE_SPAWNS, ArrayList<Npc>())
				
				if (abyssalSireSpawn.alive()) {
					abyssalSireSpawn.world().server().scriptExecutor().executeLater(abyssalSireSpawn, @Suspendable { s ->
						val abyssalSireScion = Npc(5918, abyssalSireSpawn.world(), abyssalSireSpawn.tile())
						val targetedPlayer = abyssalSireSpawn.attribOr<Entity>(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, null)
						
						abyssalSireSpawn.lock()
						abyssalSireSpawn.world().registerNpc(abyssalSireScion)
						
						abyssalSireScion.respawns(false)
						abyssalSireScion.putattrib(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, targetedPlayer)
						
						abyssalSireSpawns.remove(abyssalSireSpawn)
						abyssalSireSpawns.add(abyssalSireScion)
						abyssalSireOwner.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNS, abyssalSireSpawns)
						
						abyssalSireScion.lock()
						abyssalSireSpawn.animate(7123)
						abyssalSireScion.animate(7123)
						
						abyssalSireSpawn.world().unregisterNpc(abyssalSireSpawn)
						abyssalSireScion.face(targetedPlayer)
						abyssalSireScion.walkRadius(20)
						s.delay(4)
						
						abyssalSireScion.unlock()
						abyssalSireScion.attack(targetedPlayer)
					})
				}
			}
		}
	}
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val abyssalSireSpawn = it.npc()
		var targetedPlayer = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(abyssalSireSpawn, targetedPlayer) && PlayerCombat.canAttack(abyssalSireSpawn, targetedPlayer)) {
			abyssalSireSpawn.face(targetedPlayer)
			
			if (EntityCombat.canAttackDistant(abyssalSireSpawn, targetedPlayer, true, 4) && EntityCombat.attackTimerReady(abyssalSireSpawn)) {
				
				//Safety check in case something breaks... unregister our beautiful creature.
				if (!abyssalSireSpawn.timers().has(TimerKey.ABYSSAL_SIRE_SPAWN_TRANSFORM) && !abyssalSireSpawn.locked())
					abyssalSireSpawn.world().unregisterNpc(abyssalSireSpawn)
				
				val randomAttack = abyssalSireSpawn.world().random(7)
				
				when (randomAttack) {
					1 -> {
						if (EntityCombat.canAttackMelee(abyssalSireSpawn, targetedPlayer, true)) {
							meleeAttack(abyssalSireSpawn, targetedPlayer)
						}
					}
					else -> rangedAttack(abyssalSireSpawn, targetedPlayer)
				}
			}
			
			EntityCombat.postHitLogic(abyssalSireSpawn)
			it.delay(1)
			targetedPlayer = EntityCombat.refreshTarget(it) ?: return@s
		}
		
		//If we no longer pass the combat checks, simply unregister the NPC.
		abyssalSireSpawn.world().unregisterNpc(abyssalSireSpawn)
	}
	
	fun rangedAttack(abyssalSireSpawn: Npc, targetedPlayer: Entity) {
		val distanceFromPlayer = abyssalSireSpawn.tile().transform(1, 1).distance(targetedPlayer.tile())
		val hitDelay = Math.max(1, (50 + (distanceFromPlayer * 12)) / 30)
		
		abyssalSireSpawn.animate(abyssalSireSpawn.attackAnimation())
		abyssalSireSpawn.world().spawnProjectile(abyssalSireSpawn, targetedPlayer, 32, 20, 30, 30, 12 * distanceFromPlayer, 14, 5)
		
		if (EntityCombat.attemptHit(abyssalSireSpawn, targetedPlayer, CombatStyle.RANGE))
			targetedPlayer.hit(abyssalSireSpawn, EntityCombat.randomHit(abyssalSireSpawn), hitDelay.toInt()).combatStyle(CombatStyle.RANGE)
		else
			targetedPlayer.hit(abyssalSireSpawn, 0, hitDelay.toInt())
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSireSpawn)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		abyssalSireSpawn.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSireSpawn, abyssalSireSpawn.combatInfo().attackspeed)
	}
	
	fun meleeAttack(abyssalSireSpawn: Npc, targetedPlayer: Entity) {
		if (EntityCombat.attemptHit(abyssalSireSpawn, targetedPlayer, CombatStyle.MELEE))
			targetedPlayer.hit(abyssalSireSpawn, EntityCombat.randomHit(abyssalSireSpawn))
		else
			targetedPlayer.hit(abyssalSireSpawn, 0)
		
		abyssalSireSpawn.animate(abyssalSireSpawn.attackAnimation())
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSireSpawn)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		abyssalSireSpawn.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSireSpawn, abyssalSireSpawn.combatInfo().attackspeed)
	}
	
}
