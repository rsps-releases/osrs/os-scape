package nl.bartpelle.veteres.content.mechanics.deadman.safezones

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jak on 30/10/2016.
 *
 * Items to speed up generating safezones in game.
 * To see them in action, ask. I have a Stream recorded and Youtube example uploaded.
 */
object SafeZoneAdminTools {
	
	val STRANGE_BOOK = 5507
	val BOOK_OF_FOLKLAW = 5508
	val LEATHER_BOOK = 7635
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOption1(STRANGE_BOOK) @Suspendable {
			if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				val areaOption = if (it.player().attrib<Tile>(AttributeKey.ADMIN_TEMPATTRIB_SAFEAREA) == null) "Start area override (bottom left)" else "End area override (top right)"
				when (it.options("Make region safe", "Make chunk safe", "Make tile safe", "Force tile dangerous", areaOption)) {
					1 -> DmmZones.get(it.player().tile().region()).entireRegionSafe = true
					2 -> DmmZones.get(it.player().tile().region()).safeChunks.add(LeveledChunk(it.player().tile().chunk(), it.player().tile().level))
					3 -> DmmZones.get(it.player().tile().region()).safeTiles.add(it.player().tile())
					4 -> DmmZones.get(it.player().tile().region()).dangerousTileOverrides.add(it.player().tile())
					5 -> {
						val corner = it.player().attrib<Tile>(AttributeKey.ADMIN_TEMPATTRIB_SAFEAREA)
						if (corner == null) {
							it.player().putattrib(AttributeKey.ADMIN_TEMPATTRIB_SAFEAREA, it.player().tile())
							it.message("Stored current tile as bottom left corner")
						} else {
							val area = Area(corner, it.player().tile())
							DmmZones.get(it.player().tile().region()).dangerousAreaOverrides.add(Area(area, it.player().tile().level))
							it.message("Stored $area level ${it.player().tile().level}")
							it.player().clearattrib(AttributeKey.ADMIN_TEMPATTRIB_SAFEAREA)
						}
					}
				}
			}
		}
		repo.onItemOption1(BOOK_OF_FOLKLAW) @Suspendable {
			if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				when (it.options("Remove safe region", "Remove safe chunk", "Remove safe tile", "Remove force dangerous tile", "Find area overrides (current tile)")) {
					1 -> DmmZones.get(it.player().tile().region()).entireRegionSafe = false
					2 -> DmmZones.get(it.player().tile().region()).safeChunks.removeAll { area -> area.chunkId == it.player().tile().chunk() && it.player().tile().level == area.level }
					3 -> DmmZones.get(it.player().tile().region()).safeTiles.remove(it.player().tile())
					4 -> DmmZones.get(it.player().tile().region()).dangerousTileOverrides.remove(it.player().tile())
					5 -> {
						val script = it
						val sr = DmmZones.get(it.player().tile().region())
						val it = sr.dangerousAreaOverrides.iterator()
						while (it.hasNext()) {
							val area = it.next()
							if (area.contains(script.player().tile())) {
								script.message("Area override : $area with level ${area.level}")
							}
						}
					}
				}
			}
		}
		repo.onItemOption1(LEATHER_BOOK) @Suspendable {
			if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				when (it.options("Clear ground items", "Show safe tiles", "Debug position", "Hard test")) {
					1 -> {
						// Clear previous
						ArrayList<GroundItem>(it.player().world().groundItems()).forEach { g ->
							// Only despawn the ones close to us. Allows two people to work on diff areas at once.
							if (g.tile().distance(it.player().tile()) <= 64 * 2)
								it.player().world().removeGroundItem(g)
						}
					}
					2 -> {
						// Clear previous
						ArrayList<GroundItem>(it.player().world().groundItems()).forEach { g ->
							// Only despawn the ones close to us. Allows two people to work on diff areas at once.
							if (g.tile().distance(it.player().tile()) <= 64 * 2)
								it.player().world().removeGroundItem(g)
						}
						// Show new
						DmmZones.showSafetiles(it.player().world(), it.player().tile())
					}
					3 -> {
						
						val safeinfo = DmmZones.INSTANCE.SAFE_ZONES.get(it.player().tile().region())
						if (safeinfo != null) {
							val override = safeinfo.dangerousTileOverrides.contains(it.player().tile())
							val overrideArea = safeinfo.dangerousAreaOverrides.filter {
								area ->
								area.level == it.player().tile().level && area.contains(it.player())
							}.firstOrNull()
							it.player().message("%s %s %s", override, overrideArea, safeinfo.entireRegionSafe)
							var safetile = false
							for (safeTile in safeinfo.safeTiles) {
								if (it.player().tile().equals(safeTile)) {
									safetile = true
									break
								}
							}
							var safechunk = false
							for (chunkInfo in safeinfo.safeChunks) {
								val chunkTile = Tile.chunkToTile(chunkInfo.chunkId)
								val chunk = Area(chunkTile, chunkTile.transform(8, 8), chunkInfo.level)
								if (chunk.contains(it.player().tile()) && (chunk.level == 0 || chunk.level != 0 && chunk.level == it.player().tile().level)) {
									it.player().message("Safe chunk: %d %s", chunkInfo.chunkId, chunk)
									// We're in a safe chunk
									safechunk = true
									break
								}
							}
							it.player().message("%s %s %d", safetile, safechunk, it.player().tile().region())
						}
					}
					4 -> {
						val corner = it.player().tile().regionCorner()
						for (xx in 0..64) {
							for (yy in 0..64) {
								val xd = corner.transform(xx, yy, it.player().tile().level)
								if (DmmZones.inDangerousDMM(xd)) {
									DmmZones.spawn(it.player().world(), xd)
								}
							}
						}
					}
				}
			}
		}
	}
}