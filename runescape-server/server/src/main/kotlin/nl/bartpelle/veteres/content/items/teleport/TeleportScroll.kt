package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 3/12/2016.
 */

object TeleportScroll {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOption1(12403) @Suspendable { scrollTeleport(it, it.player(), Tile(3326, 3412), 2) } // Digsite
		repo.onItemOption1(12410) @Suspendable { scrollTeleport(it, it.player(), Tile(2193, 3257), 2) } // Elf camp
		repo.onItemOption1(12404) @Suspendable { scrollTeleport(it, it.player(), Tile(2540, 2924), 2) } // Feldip hills
		repo.onItemOption1(12642) @Suspendable { scrollTeleport(it, it.player(), Tile(3307, 3490), 2) } // Lumberyard
		repo.onItemOption1(12405) @Suspendable { scrollTeleport(it, it.player(), Tile(2100, 3914), 1) } // Lunar isle
		repo.onItemOption1(12406) @Suspendable { scrollTeleport(it, it.player(), Tile(3488, 3288), 2) } // Mort'ton
		repo.onItemOption1(12411) @Suspendable { scrollTeleport(it, it.player(), Tile(3676, 2978), 1) } // Mos le'harmless
		repo.onItemOption1(12402) @Suspendable { scrollTeleport(it, it.player(), Tile(3421, 2917), 2) } // Nardah
		repo.onItemOption1(12407) @Suspendable { scrollTeleport(it, it.player(), Tile(2657, 2660), 2) } // Pest control
		repo.onItemOption1(12408) @Suspendable { scrollTeleport(it, it.player(), Tile(2338, 3649), 2) } // Piscatoris
		repo.onItemOption1(12409) @Suspendable { scrollTeleport(it, it.player(), Tile(2789, 3066), 1) } // Tai bwo wannai
		repo.onItemOption1(12938) @Suspendable { scrollTeleport(it, it.player(), Tile(2197, 3056), 2) } // Zul-andra
		repo.onItemOption1(13249) @Suspendable { scrollTeleport(it, it.player(), Tile(1312, 1250), 1) } // Key master at Cerberus
		repo.onItemOption4(21802) @Suspendable { scrollTeleport(it, it.player(), Tile(3076, 3651), 1) } // Revenant caves
	}
	
	
	@Suspendable fun scrollTeleport(it: Script, player: Player, target: Tile, radius: Int) {
		if (!Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
			return
		}
		DeadmanMechanics.attemptTeleport(it)
		
		val used = player.inventory()[player.attrib(AttributeKey.ITEM_SLOT)]
		player.lockNoDamage()
		player.sound(200, 10)
		player.animate(3864)
		player.graphic(1039, 0, 0)
		it.delay(2)
		player.inventory().remove(Item(used.id(), 1), true)
		it.delay(2)
		if (PVPAreas.inPVPArea(player)) {
			player.teleport(PVPAreas.safetileFor(player)) // Go to inside edge bank, stay in pvp.
		} else {
			player.teleport(player.world().randomTileAround(target, radius))
		}
		player.animate(-1)
		player.graphic(-1)
		player.unlock()
		player.timers().cancel(TimerKey.FROZEN)
		player.timers().cancel(TimerKey.REFREEZE)
		player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
	}
}
