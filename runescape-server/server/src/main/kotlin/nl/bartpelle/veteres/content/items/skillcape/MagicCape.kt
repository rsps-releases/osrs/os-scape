package nl.bartpelle.veteres.content.items.skillcape

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import java.util.concurrent.TimeUnit

/**
 * Created by Jason MacKeigan on 2016-07-05 at 4:01 PM
 */
object MagicCape : CapeOfCompletionPerk(intArrayOf(1, 2)) {
	
	override fun option(option: Int): Function1<Script, Unit> = label@ @Suspendable {
		val player = it.player()
		
		when (option) {
			1 -> CapeOfCompletion.boost(Skills.MAGIC, player)
			2 -> swap(it)
		}
	}
	
	@ScriptMain @JvmStatic fun register(repo: ScriptRepository) {
		repo.onItemOption3(9762) { swap(it) }
		repo.onItemOption3(9763) { swap(it) }
	}
	
	@Suspendable @JvmStatic fun swap(it: Script) {
		val player = it.player()
		val time: Long = player.attribOr(AttributeKey.MAGIC_PERK, 0)
		
		val current = System.currentTimeMillis()
		val delay = if (it.player().donationTier() == DonationTier.NONE) 2 else 1
		
		if (current - time < TimeUnit.HOURS.toMillis(delay.toLong()) && !player.privilege().eligibleTo(Privilege.ADMIN)) {
			player.message("You must wait ${delay - TimeUnit.MILLISECONDS.toHours(current - time)} hours before you can do this again.")
			return
		}
		
		val option = it.options("Regular", "Ancient", "Lunar", "Cancel")
		
		if (option - 1 == player.varps().varbit(Varbit.SPELLBOOK)) {
			player.message("Your spellbook is already on this, you don't want to waste this!")
			return
		}
		
		when (option) {
			1 -> it.player().varps().varbit(Varbit.SPELLBOOK, 0)
			2 -> it.player().varps().varbit(Varbit.SPELLBOOK, 1)
			3 -> it.player().varps().varbit(Varbit.SPELLBOOK, 2)
		}
		
		if (option != 5) {
			player.putattrib(AttributeKey.MAGIC_PERK, current)
			player.animate(6299)
			player.graphic(1062)
		}
	}
	
}