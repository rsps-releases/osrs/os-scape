package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue.Herman

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/4/2015.
 */

object Bob {
	
	val BOB = 505
	
	// NOTE: Item on item for repairs for Bob is in Herman's script hook.
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(BOB) @Suspendable {
			when (it.options("Give me a quest!", "Have you anything to sell?", "Can you repair my items for me?")) {
				1 -> {
					it.chatPlayer("Give me a quest!", 567)
					it.chatNpc("Get yer own!", BOB, 614)
				}
				2 -> {
					it.chatPlayer("Have you anything to sell?", 554)
					it.chatNpc("Yes! I buy and sell axes! Take your pick (or axe)!", BOB, 567)
					it.player().world().shop(2).display(it.player())
				}
				3 -> {
					it.chatPlayer("Can you repair my items for me?", 610)
					it.chatNpc("Of course I'll repair it, though the materials may cost<br>you. Just hand me the item and I'll have a look.", BOB, 572)
				}
			}
		}
		r.onNpcOption2(BOB) { it.player().world().shop(2).display(it.player()) }
		r.onNpcOption3(BOB) @Suspendable {
			if (!Herman.hasBroken(it.player())) {
				it.chatNpc("You don't have any broken barrows pieces.", BOB)
			} else {
				Herman.fixall(it, BOB)
			}
		}
	}
	
}
