package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 3/17/2016.
 */


object Scorpia {
	
	@JvmField val deathScript: Function1<Script, Unit> = @Suspendable {
		it.npc().clearattrib(AttributeKey.SCORPIA_GUARDIANS_SPAWNED)
	}
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		var summoned_guardians = npc.attribOr<Boolean>(AttributeKey.SCORPIA_GUARDIANS_SPAWNED, false)
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackMelee(npc, target, true) && EntityCombat.attackTimerReady(npc)) {
				
				//If Scorpia is below 50% HP & hasn't summoned offspring that heal we..
				if (npc.hp() < 100 && !summoned_guardians) {
					summon_guardian(npc)
					summon_guardian(npc)
					summoned_guardians = true
					npc.putattrib(AttributeKey.SCORPIA_GUARDIANS_SPAWNED, true)
				} else {
					//Else we just attack the player, with a chance of poisoning for 20 damage..
					
					if (target.world().rollDie(4, 1)) {
						target.poison(20)
					}
					
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
						target.hit(npc, EntityCombat.randomHit(npc))
					} else {
						target.hit(npc, 0)
					}
					
					npc.animate(npc.attackAnimation())
				}
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun summon_guardian(scorpia: Npc) {
		val guardian = Npc(6617, scorpia.world(), Tile(scorpia.tile().x + scorpia.world().random(2),
				scorpia.tile().z + scorpia.world().random(2)))
		guardian.respawns(false)
		guardian.noRetaliation(true)
		scorpia.world().registerNpc(guardian)
		
		// Execute script
		guardian.executeScript(ScorpiaGuardian(scorpia).healscript)
	}
	
}