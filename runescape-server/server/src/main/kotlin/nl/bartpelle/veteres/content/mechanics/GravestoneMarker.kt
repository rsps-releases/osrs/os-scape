package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 02/05/2016.
 * Only appears when losing items, not in safe places like arena, CW
 */
@Suspendable object GravestoneMarker {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onTimer(TimerKey.HINT_ARROW_TO_DEATH_TILE) {
			it.player().clearattrib(AttributeKey.LAST_DEATH_TILE)
			it.player().write(SetHintArrow(-1)) // clear this
		}
	}
	
	@JvmField public val script: Function1<Script, Unit> = s@ @Suspendable {
		val player = it.player()!!
		it.clearContext()
		
		val world = player.world()
		removeOldGravestone(player)
		it.onInterrupt { removeOldGravestone(player) }
		
		
		/*if (player.world().objByType(10, player.tile()) != null
				|| player.world().objByType(11, player.tile()) != null
				|| (player.world().floorAt(player.tile()) and 0x4) != 0
				|| player.world().objByType(0, player.tile()) != null) {
			return@s
		}*/
		
		player.timers().addOrSet(TimerKey.HINT_ARROW_TO_DEATH_TILE, 200) // 2 mins
		player.write(SetHintArrow(player.tile()))
		
		/*val obj: MapObj = MapObj(player.tile(), 401, 10, 0)
		world.spawnObj(obj, false)
		player.putattrib(AttributeKey.GRAVESTONE, obj)*/
		// player.debug("step1")
		var cycles = if (WildernessLevelIndicator.inWilderness(player.tile())) 100 else 1000
		while (cycles-- > 0) { // since this gets ticked anyway, we could cancel it earlier than 900s if the object isnt valid anymore
			/*player.debug("step2 %s remain=%d", obj.valid(world), cycles)
			if (!obj.valid(world)) {
				cycles = -1
				break
			}*/
			it.delay(1)
		}
		/*player.debug("step3")
		if (obj.valid(world)) {
			//player.debug("step4")
			world.removeObj(obj, false)
		}*/
		player.write(SetHintArrow(-1))
	}
	
	@JvmStatic fun removeOldGravestone(player: Player) {
		/*al oldGravestone: MapObj? = player.attribOr<MapObj>(AttributeKey.GRAVESTONE, null)
		if (oldGravestone != null) {
			if (oldGravestone.valid(player.world())) {
				player.world().removeObj(oldGravestone) // Make sure retards can't spam death and spawn 100s LOL
				player.message("<col=FF0000>Your old gravestone has disappeared "+player.tile().distance(oldGravestone.tile())+" steps away.")
			}
		}*/
	}
}