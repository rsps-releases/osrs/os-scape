package nl.bartpelle.veteres.content.interfaces.magicbook

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterManager
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.skills.crafting.TanningHide
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.steroids.RangeStepSupplier
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import java.util.*

/**
 * Created by Situations on 3/20/2016.
 */

object LunarSpellbook {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(218, 99, s@ @Suspendable { attempt_home_teleport(it) }) //Home teleport
		// r.onButton(218, 96, s@ @Suspendable { attempt_bake_pie(it)}) //Bake pie
		r.onButton(218, 138) @Suspendable { attemptVengeance(it) } // Vengeance
		
		r.onButton(218, 113) @Suspendable {
			/* val player = it.player()
 
			 if (it.interactionOption() == 1) {
				 castTanLeather(it)
			 } else {
				 val key = AttributeKey.LUNAR_TAN_LEATHER_STATE
 
				 val state = player.attribOr<Int>(key, 0)
 
				 player.putattrib(key, if (state == 0) 1 else 0)
				 player.message("You have setup the spell to now tan ${if (state == 0) "soft" else "hard"} leather.")
			 }*/
		}
		
		//Bounty teleport to target
		r.onButton(218, 127, s@ @Suspendable {
			
			if (!it.player().attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TARGET_TELEPORT_UNLOCKED, false)) {
				it.player().message("You have not unlocked this spell yet.")
				return@s
			}
			
			if (!WildernessLevelIndicator.inWilderness(it.player().tile())) {
				it.player().message("You must be in the Wilderness to use this spell.")
				return@s
			}
			
			if (it.player().attribOr<Player>(AttributeKey.BOUNTY_HUNTER_TARGET, null) != null) {
				
				if (!WildernessLevelIndicator.inWilderness(BountyHunterManager.getTargetFor(it.player())!!.tile())) {
					it.player().message("Your target is currently not in the Wilderness.")
					return@s
				}
				
				cast_spell(it,0,  86, it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).tile(), 45.0, false, Item(563, 1), Item(560, 1), Item(562, 1))
				it.player().varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY))
			} else {
				it.player().message("You currently have no target to teleport to!")
			}
		})
		
		r.onTimer(TimerKey.VENGEANCE_COOLDOWN) {
			it.player().varps().varbit(Varbit.VENGENACE_COOLDOWN, 0)
		}
		
		r.onSpellOnEntity(218, 98) {
			monster_examine(it)
		}
		r.onSpellOnEntity(218, 100) {
			cure_other(it)
		}
		r.onSpellOnEntity(218, 109) {
			stat_spy(it)
		}
		r.onSpellOnEntity(218, 131) {
			energy_transfer(it)
		}
		r.onSpellOnEntity(218, 133) {
			vengeance_other(it)
		}
		
	}
	
	@Suspendable fun monster_examine(it: Script): Boolean {
		return false
	}
	
	@Suspendable fun cure_other(it: Script): Boolean {
		return false
	}
	
	@Suspendable fun stat_spy(it: Script): Boolean {
		return false
	}
	
	@Suspendable fun vengeance_other(it: Script): Boolean {
		if (true) {
			return false
		}
		val player = it.player()
		val target = it.target() ?: return false
		
		if (reachedTarget(it)) {
			player.pathQueue().trimToSize(if (player.pathQueue().running() || target.pathQueue().running()) 2 else 1)
			player.interfaces().closeMain()
			player.interfaces().close(162, 550) // Close chatbox
			if (!target.isPlayer) {
				player.message("This spell only works on other players.")
				return false
			}
			if (it.player().skills().level(Skills.DEFENCE) < 40) {
				it.player().message("You need 40 Defence to use Vengence Other.")
			}
			if (player.skills().level(Skills.MAGIC) < 93) {
				player.message("You need a Magic level of 93 to cast Vengeance Other.")
				return false
			}
			if (Staking.in_duel(target) || ClanWars.inInstance(target)) {
				player.message("Your target is in an area where this spell cannot be used.")
				return false
			}
			if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_MAGIC)) {
				player.message("Magic is disabled for this duel.")
				return false
			}
			if (ClanWars.checkRule(player, CWSettings.MAGIC, 3)) {
				player.message("Magic is disabled for this duel.")
				return false
			}
			if (it.player().timers().has(TimerKey.VENGEANCE_COOLDOWN)) {
				it.message("You can only cast vengeance spells every 30 seconds.")
				return false
			}
			if (target.attribOr(AttributeKey.VENGEANCE_ACTIVE, false)) {
				it.message("That player already has vengeance casted.")
				return false
			}
			val other = target as Player
			if (other.varps()[Varp.ACCEPT_AID] != 1) {
				it.message("That player does not have accept aid enabled.")
				return false
			}
			
			val runes = arrayOf(Item(9075, 3), Item(557, 10), Item(560, 2))
			
			// Begin the spell
			
			val weapon = player.equipment().get(3)?.id() ?: -1
			//Kodai wand has 15% change of negating rune cost
			// Now remove runes. Also, apply the staff of the dead effect here (1/8 chance to not take runes).
			val freeCast = (weapon in intArrayOf(11791, 12902, 12904) && player.world().rollDie(8, 1))
					|| (weapon == 21006 && Math.random() < 0.15)
			
			// There is no message sent when the cost is discarded.
			if (!freeCast) {
				if (!MagicCombat.has(player, runes, true))
					return false
			}
			it.player().timers().register(TimerKey.VENGEANCE_COOLDOWN, 50)
			it.player().varps().varbit(Varbit.VENGENACE_COOLDOWN, 1)
			other.putattrib(AttributeKey.VENGEANCE_ACTIVE, true)
			it.animate(4411)
			other.graphic(725, 100, 0)
			it.player().world().spawnSound(it.player().tile(), 2907, 0, 10)
			it.player().skills().__addXp(Skills.MAGIC, 108.0, false)
			other.filterableMessage("You have the power of Vengeance!")
			return true
		}
		return false
	}
	
	@Suspendable fun reachedTarget(it: Script): Boolean {
		val player = it.player()
		var target = it.target()
		player.pathQueue().clear()
		
		// You don't need to check target here, because the doMagicSpell method does it after all these initial checks.
		while (target != null && !target.dead() && !player.dead()) {
			
			// On 07, the player gets unfrozen when the freezer is at least X tiles away
			PlayerCombat.unfreeze_when_out_of_range(player)
			
			var dist = 0
			if (player.sync().secondaryStep() != -1 && target.sync().secondaryStep() != -1)
				dist = 3
			else if (player.sync().secondaryStep() != -1)
				dist = 1
			
			val attackRange = 10 + dist
			val inrange = player.tile().distance(target.tile()) <= attackRange
			
			// Projectile path finder good?
			val supplier = RangeStepSupplier(player, target, attackRange)
			var reached = supplier.reached(player.world(), target)
			
			// When you cast you stop moving
			if (!reached && !player.frozen() && !player.stunned()) {
				player.walkTo(target, PathQueue.StepType.REGULAR)
				player.pathQueue().trimToSize(if (player.pathQueue().running()) 2 else 1)
			}
			
			// Reassess after walkTo + trimToSize
			reached = supplier.reached(player.world(), target)
			
			// Combat stops if you're in range but projectile won't reach.
			if (!reached && inrange && player.frozen()) {
				player.message("I can't reach that!")
				player.sound(154)
				return false
			} else if (reached) {
				player.pathQueue().trimToSize(if (player.pathQueue().running() || target.pathQueue().running()) 2 else 1)
				return true
			}
			it.delay(1)
			target = it.target()
		}
		return false
	}
	
	@Suspendable fun energy_transfer(it: Script): Boolean {
		if (true) {
			return false
		}
		val player = it.player()
		val target = it.target() ?: return false
		
		if (reachedTarget(it)) {
			player.pathQueue().trimToSize(if (player.pathQueue().running() || target.pathQueue().running()) 2 else 1)
			player.interfaces().closeMain()
			player.interfaces().close(162, 550) // Close chatbox
			if (!target.isPlayer) {
				player.message("This spell only works on other players.")
				return false
			}
			if (player.skills().level(Skills.MAGIC) < 91) {
				player.message("You need a Magic level of 91 to cast Energy Transfer.")
				return false
			}
			if (player.skills().level(Skills.HITPOINTS) <= 10) {
				player.message("You need at least 10 hitpoints to use Energy Transfer.")
				return false
			}
			if (player.varps().varp(Varp.SPECIAL_ENERGY) != 1000) {
				player.message("You need full special energy to use Energy Transfer.")
				return false
			}
			if (Staking.in_duel(target) || ClanWars.inInstance(target)) {
				player.message("Your target is in an area where this spell cannot be used.")
				return false
			}
			if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_MAGIC)) {
				player.message("Magic is disabled for this duel.")
				return false
			}
			if (ClanWars.checkRule(player, CWSettings.MAGIC, 3)) {
				player.message("Magic is disabled for this duel.")
				return false
			}
			var instance_key: InstancedMapIdentifier? = null
			player.world().allocator().active(player.tile()).ifPresent { map -> map.identifier.ifPresent { id -> instance_key = id } }
			if (WildernessLevelIndicator.inAttackableArea(target) && !target.varps().varbitBool(Varbit.MULTIWAY_AREA)) {
				if (instance_key != InstancedMapIdentifier.VARROCK_PVP && instance_key != InstancedMapIdentifier.CAMELOT_PVP) {
					player.message("This spell cannot be used in this dangerous single-way combat area.")
					return false
				}
			}
			val other = target as Player
			if (other.varps()[Varp.ACCEPT_AID] != 1) {
				it.message("That player does not have accept aid enabled.")
				return false
			}
			
			
			val runes = arrayOf(Item(9075, 3), Item(561, 1), Item(563, 2))
			
			// Begin the spell
			
			val weapon = player.equipment().get(3)?.id() ?: -1
			//Kodai wand has 15% change of negating rune cost
			// Now remove runes. Also, apply the staff of the dead effect here (1/8 chance to not take runes).
			val freeCast = (weapon in intArrayOf(11791, 12902, 12904) && player.world().rollDie(8, 1))
					|| (weapon == 21006 && Math.random() < 0.15)
			
			// There is no message sent when the cost is discarded.
			if (!freeCast) {
				if (!MagicCombat.has(player, runes, true))
					return false
			}
			player.hit(null, 10)
			player.varps().varp(Varp.SPECIAL_ENERGY, 0)
			other.varps().varp(Varp.SPECIAL_ENERGY, 1000)
			other.setRunningEnergy(100.0, true)
			it.player().skills().__addXp(Skills.MAGIC, 100.0, false)
			player.message("You sacrifice some health to transfer special and run energy to ${other.name()}.")
			player.animate(4411)
			other.graphic(734, 96, 0)
			return true
		}
		return false
	}
	
	@Suspendable fun castTanLeather(it: Script) {
		val player = it.player()
		
		if (player.skills().level(Skills.MAGIC) < 78) {
			player.message("You need a magic level of 78 to cast this.")
			return
		}
		
		if (TimerKey.LUNAR_SPELL_TAN_HIDE in player.timers()) {
			player.message("You must finish tanning before doing this again.")
			return
		}
		try {
			val skip: TanningHide = if (player.attribOr<Int>(AttributeKey.LUNAR_TAN_LEATHER_STATE, 0) == 0)
				TanningHide.SOFT_LEATHER else TanningHide.HARD_LEATHER
			
			val hide = TanningHide.values().filterNot { it == skip }.first { player.inventory().contains(it.resource) }
			
			try {
				//val cost = SpellCost.TAN_LEATHER.combinations.first { it.available(player) }
				
				val amount = Math.min(player.inventory().count(hide.resource.id()), 5)
				
				//cost.delete(player)
				player.skills().__addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 150.0 else 75.0)
				player.skills().__addXp(Skills.CRAFTING, 15.0 * amount)
				player.inventory().remove(Item(hide.resource.id(), amount), true)
				player.inventory().add(Item(hide.product.id(), amount), true)
				player.message("You tan the ${hide.resource.definition(player.world()).name}.")
				player.timers().register(TimerKey.LUNAR_SPELL_TAN_HIDE, 5)
			} catch (e: NoSuchElementException) {
				player.message("You do not have the appropriate runes and or staves for this.")
			}
		} catch (exception: NoSuchElementException) {
			player.message("You do not have any hide to tan.")
		}
	}


	private val VENG_RUNES: Array<Item> = arrayOf(Item(9075, 4), Item(557, 10), Item(560, 2))
	
	@Suspendable private fun attemptVengeance(it: Script) {
		if (!it.player().locked()) {
			if (it.player().skills().level(Skills.DEFENCE) < 40) {
				it.player().message("You need 40 Defence to use Vengence.")
			} else if (it.player().skills().level(Skills.MAGIC) < 94) {
				it.player().message("Your Magic level is not high enough to use this spell.")
			} else if (it.player().attribOr(AttributeKey.VENGEANCE_ACTIVE, false)) {
				it.player().message("You already have Vengeance casted.")
			} else if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_MAGIC)) {
				it.player().message("Magic is disabled for this duel.")
			} else if (ClanWars.checkRule(it.player(), CWSettings.MAGIC)) {
				it.player().message("Magic is disabled for this clan war.")
			} else if (!it.player().timers().has(TimerKey.VENGEANCE_COOLDOWN)) {
				if (!MagicCombat.has(it.player(), VENG_RUNES, true)) {
					return
				}
				it.player().timers().register(TimerKey.VENGEANCE_COOLDOWN, 50)
				it.player().varps().varbit(Varbit.VENGENACE_COOLDOWN, 1)
				it.player().putattrib(AttributeKey.VENGEANCE_ACTIVE, true)
				it.animate(4410)
				it.player().graphic(726, 100, 0)
				it.player().world().spawnSound(it.player().tile(), 2907, 0, 10)
				it.player().skills().__addXp(Skills.MAGIC, 112.0, false)

				it.player().write(SendWidgetTimer(WidgetTimer.VENGEANCE, 30))
			} else {
				it.message("You can only cast vengeance spells every 30 seconds.")
			}
		}
	}
}