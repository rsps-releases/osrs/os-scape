package nl.bartpelle.veteres.content

import co.paralleluniverse.fibers.Fiber
import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.WaitReason
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.entity.player.Varps
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.*
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.script.TimerRepository
import java.util.*

/**
 * Created by Bart on 7/9/2015.
 */


val dialogueInterruptCall = { s: Script ->
	s.player().interfaces().close(162, 550)
	s.player().invokeScript(101)
	s.player().varps().varbit(5983, 0) // Center contents again
}

val inputInterruptCall = { s: Script ->
	{
		s.player().write(CloseInputDialogue())
		s.player().invokeScript(101)
	}
}

@Suspendable fun Script.blankmessagebox(message: String) {
	player().interfaces().send(229, 162, 550, false)
	player().write(InterfaceText(229, 0, message))
	player().write(InvokeScript(600, 1, 1, 0, 15007744))
	player().write(InterfaceSettings(229, 1, -1, -1, 1))
}

@Suspendable fun Script.blankmessagebox2(message: String) {
	player().interfaces().send(229, 162, 550, false)
	player().write(InterfaceText(229, 0, message))
	player().write(InvokeScript(600, 1, 1, 31, 15007744))
	player().write(InterfaceSettings(229, 1, -1, -1, 1))
}

@Suspendable fun Script.doubleItemBox(message: String, first: Int, second: Int) {
	player().interfaces().send(11, 162, 550, false)
	player().write(InterfaceText(11, 1, message))
	player().write(InterfaceText(11, 3, "Click here to continue"))
	player().write(InterfaceItem(11, 0, first))
	player().write(InterfaceItem(11, 2, second))
	player().write(InterfaceSettings(11, 3, -1, -1, 1))
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)
}

@Suspendable fun Script.doubleItemBox(message: String, first: Item, second: Item) {
	player().interfaces().send(11, 162, 550, false)
	player().write(InterfaceText(11, 1, message))
	player().write(InterfaceText(11, 3, "Click here to continue"))
	player().write(InterfaceItem(11, 0, first))
	player().write(InterfaceItem(11, 2, second))
	player().write(InterfaceSettings(11, 3, -1, -1, 1))
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)
}

@Suspendable fun Script.wildernessWarning(): Boolean {
	if (player().attribOr(AttributeKey.WILDERNESS_WARNING, false))
		return true
	player().interfaces().sendMain(475, false)
	player().write(InterfaceVisibility(475, 13, true))
	player().write(InterfaceText(475, 10, "If you go any further you will enter the Wilderness. This is a very dangerous area where other players can attack you!<br><br>The further north you go the more dangerous it becomes. An indicator at the bottom-right of the screen will show the current level of danger."))
	player().interfaces().setting(475, 1, 0, 1, 1)
	player().interfaces().setting(475, 13, 0, 0, 1)
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	val go_in = if (waitReturnVal == null || waitReturnVal !is Int) false else (waitReturnVal as Int) == 1
	if (go_in) {
		player().putattrib(AttributeKey.WILDERNESS_WARNING, true)
	}
	player().interfaces().closeMain()
	
	return go_in
}

@Suspendable fun Script.itemBox(message: String, item: Int, zoom: Int = 1) {
	player().interfaces().send(193, 162, 550, false)
	player().write(InterfaceText(193, 1, message))
	player().write(InterfaceText(193, 2, "Click here to continue"))
	player().write(InterfaceItem(193, 0, Item(item, zoom)))
	player().write(InterfaceSettings(193, 2, -1, -1, 1))
	player().write(InterfaceSettings(193, 3, -1, -1, 0))
	player().write(InterfaceSettings(193, 4, -1, -1, 0))
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)
}

@Suspendable fun Script.craftingInterface(): Int {
	player().interfaces().sendMain(154, false)
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().closeMain()
	val opt = waitReturnVal as Int
	return opt and 0xFFFF
}

@Suspendable fun Script.itemOptions(item: Item, title: String = "How many would you like to make?", offsetX: Int = 23, offsetY: Int = 207): Int {
	return itemOptions(item, title, offsetX, offsetY, subtext = item.name(player().world()))
}

@Suspendable fun Script.itemOptions(item: Item, title: String = "How many would you like to make?", offsetX: Int = 23, offsetY: Int = 207, subtext: String = "?"): Int {
	player().interfaces().send(270, 162, 550, false)
    player().invokeScript(2046, 0, "$title|${player.world().definitions().get(ItemDefinition::class.java, item.id())?.name ?: ""}|||||||", 9, item.id(), -1, -1, -1, -1, -1, -1, -1, -1, -1, 9)
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)

	return when (player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1)) {
		9 -> 0xFF
		else -> return player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1)
	}
}

@Suspendable fun Script.javelinOptions(item: Item, title: String = "How many sets of 15 do you wish to make?", offsetX: Int = 23, offsetY: Int = 207): Int {
	player().interfaces().send(582, 162, 550, false)
	player().write(InterfaceText(582, 6, title))
	player().write(InterfaceItem(582, 2, item.id(), item.amount()))
	player().write(InterfaceText(582, 5, "<br><br><br><br>${item.name(player().world())}"))
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)
	
	val opt = waitReturnVal as Int
	
	return when (opt) {
		5 -> 1
		4 -> 5
		3 -> 10
		else -> 1
	}
}

@Suspendable fun Script.itemOptions(item: Item, item2: Item, title: String = "What would you like to make?"): Item {
	player().interfaces().send(270, 162, 550, false)
    player().invokeScript(2046, 0, "$title|${player.world().definitions().get(ItemDefinition::class.java, item.id())?.name ?: ""}|${player.world().definitions().get(ItemDefinition::class.java, item2.id())?.name ?: ""}" + "||||||||", 9, item.id(), item2.id(), -1, -1, -1, -1, -1, -1, -1, -1, 9)

	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)

    val amt = if (player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1) == 9) 0xFF else player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1)
	val child = waitReturnVal as Int
	return when (child) {
        14 -> Item(item.id(), amt)
        15 -> Item(item2.id(), amt)
		else -> Item(item.id(), amt)
	}
}

@Suspendable fun Script.itemOptions(item: Item, item2: Item, item3: Item, title: String = "What would you like to make?"): Item {
    player().interfaces().send(270, 162, 550, false)
    player().invokeScript(2046, 0, "$title|${player.world().definitions().get(ItemDefinition::class.java, item.id())?.name ?: ""}" + "|${player.world().definitions().get(ItemDefinition::class.java, item2.id())?.name ?: ""}" + "|${player.world().definitions().get(ItemDefinition::class.java, item3.id())?.name ?: ""}|||||||", 9, item.id(), item2.id(), item3.id(), -1, -1, -1, -1, -1, -1, -1, 9)
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)

    val amt = if (player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1) == 9) 0xFF else player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1)
	val child = waitReturnVal as Int
	return when (child) {
        14 -> Item(item.id(), amt)
        15 -> Item(item2.id(), amt)
        16 -> Item(item3.id(), amt)
		
		else -> Item(item.id(), 1)
	}
}

@Suspendable fun Script.itemOptions(item: Item, item2: Item, item3: Item, item4: Item, item5: Item, title: String = "What would you like to make?", strings: Array<String>? = null, func: (() -> Unit)? = null): Item {
	player().varps().varbit(5983, 1) // Topleft-align contents
	player().interfaces().send(270, 162, 550, false)

    player().invokeScript(2046, 0, "$title|${player.world().definitions().get(ItemDefinition::class.java, item.id())?.name ?: ""}" +
            "|${player.world().definitions().get(ItemDefinition::class.java, item2.id())?.name ?: ""}" +
            "|${player.world().definitions().get(ItemDefinition::class.java, item3.id())?.name ?: ""}" +
            "|${player.world().definitions().get(ItemDefinition::class.java, item4.id())?.name ?: ""}" +
            "|${player.world().definitions().get(ItemDefinition::class.java, item5.id())?.name ?: ""}|||||", 1, item.id(), item2.id(), item3.id(), item4.id(), item5.id(), -1, -1, -1, -1, -1, 1)

	for (i in 14..18)
		player.interfaces().setting(270, i, 1, 1, 1)

	// Post-display logic
	if (func != null) {
		func()
	}
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()

	player().interfaces().close(162, 550)
	player().varps().varbit(5983, 0) // Center contents again

    val amt = if (player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1) == 9) 0xFF else player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1)
	val child = waitReturnVal as Int

	return when (child) {
        14 -> Item(item.id(), amt)
        15 -> Item(item2.id(), amt)
        16 -> Item(item3.id(), amt)
        17 -> Item(item4.id(), amt)
        18 -> Item(item5.id(), amt)

		else -> Item(item.id(), 1)
	}
}

@Suspendable fun Script.itemOptions(item: Item, item2: Item, item3: Item, item4: Item, title: String = "What would you like to make?"): Item {
	player().interfaces().send(270, 162, 550, false)
    player().invokeScript(2046, 0, "$title|${player.world().definitions().get(ItemDefinition::class.java, item.id())?.name ?: ""}" +
            "|${player.world().definitions().get(ItemDefinition::class.java, item2.id())?.name ?: ""}" +
            "|${player.world().definitions().get(ItemDefinition::class.java, item3.id())?.name ?: ""}" +
            "|${player.world().definitions().get(ItemDefinition::class.java, item4.id())?.name ?: ""}" +
            "||||||", 9, item.id(), item2.id(), item3.id(), item4.id(), -1, -1, -1, -1, -1, -1, 9)
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)

    val amt = if (player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1) == 9) 0xFF else player.attribOr<Int>(AttributeKey.MAKE_X_AMT, 1)

	val child = waitReturnVal as Int
	return when (child) {
        14 -> Item(item.id(), amt)
        15 -> Item(item2.id(), amt)
        16 -> Item(item3.id(), amt)
        17 -> Item(item4.id(), amt)
        else -> Item(item.id(), amt)
	}
}

fun Script.sound(id: Int) {
	(context as Player).sound(id)
}

fun Script.animate(anim: Int) {
	(context as Entity).animate(anim)
}

fun Script.graphic(gfx: Int) {
	(context as Entity).graphic(gfx)
}

fun Script.openInterface(id: Int) {
	(context as Player).interfaces().sendMain(id, false)
}

fun Script.openRoot(id: Int) {
	(context as Player).interfaces().sendRoot(id)
}

val Script.player: Player
	get() = ctx<Player>()

fun Script.player(): Player {
	return this.ctx<Player>()
}

fun Script.clearContext() {
	context = 1
}

@Suspendable fun Script.waitForTimer(name: TimerKey) {
	val player = player()
	while (player.timers().has(name))
		delay(1)
}

@Suspendable
fun Script.waitForTile(tile: Tile, entity: Entity? = null) {
	try {
		val player = entity ?: ctx<Entity>()
		while (player.tile().x != tile.x || player.tile().z != tile.z) {
			delay(1)
		}
	} catch (t: Throwable) {
		t.printStackTrace()
	}
}

@Suspendable fun Script.waitForTile(tile: Tile, maxDistance: Int) {
	val entity = ctx<Entity>()
	
	while (entity.tile().distance(tile) > maxDistance) {
		if (entity.pathQueue().empty()) {
			break
		}
		delay(1)
	}
}

@Suspendable fun Script.waitForInterfaceClose(id: Int) {
	val player = player()
	while (player.interfaces().visible(id))
		delay(1)
}

fun Script.examineItemMessage(item: Item) {
	if (item.amount() < 100000)
		player().message(player().world().examineRepository().item(item.id()))
	else
		player().message("${item.amount()} x ${player().world().definitions().get(ItemDefinition::class.java, item.id()).name}.")
}

@Suspendable fun Script.destroyItem(title: String = "Are you sure you want to destroy this item?", name: String, note: String, item: Item): Boolean {
	
	itemBox("$name: $title - $note", item.id(), item.amount())
	return optionsTitled(title, "Yes, destroy the $name.", "No!") == 1
	
	// TODO the interface is blank even when running the script. Guessing some new client feature is missing from r138 onwards.
	/*player().interfaces().send(584, 162, 547, false)
	player().invokeScript(814, name, item.id(), item.amount(), title, note)
	player().interfaces().setting(584, 0, 0, 1, 1)
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 547)
	return if (waitReturnVal == null || waitReturnVal !is Int) false else waitReturnVal as Int == 4*/
}

fun Script.interactionObject(): MapObj {
	return player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
}

fun Script.originalInteractionObject(): MapObj {
	return player().attrib<MapObj>(AttributeKey.ORIGINAL_INTERACTION_OBJECT)
}

fun Script.interactionOption(): Int {
	return player().attrib<Int>(AttributeKey.INTERACTION_OPTION)
}

// Main item used
fun Script.itemUsedId(): Int {
	return player().attrib<Int>(AttributeKey.ITEM_ID)
}

// Main item used
fun Script.itemUsed(): Item {
	return player().attrib<Item>(AttributeKey.FROM_ITEM)
}

// Main item used
fun Script.itemUsedSlot(): Int {
	return player().attrib<Int>(AttributeKey.ITEM_SLOT)
}

// Other item used
fun Script.itemOn(): Item {
	return player().attrib<Item>(AttributeKey.TO_ITEM)
}

// Other item used
fun Script.itemOnId(): Int {
	return player().attrib<Int>(AttributeKey.ALT_ITEM_ID)
}

// Other item used
fun Script.itemOnSlot(): Int {
	return player().attrib<Int>(AttributeKey.ALT_ITEM_SLOT)
}

fun Script.buttonSlot(): Int {
	return player().attrib<Int>(AttributeKey.BUTTON_SLOT)
}

fun Script.buttonAction(): Int {
	return player().attrib<Int>(AttributeKey.BUTTON_ACTION)
}

fun Script.interactionGrounditem(): GroundItem {
	return player().attrib<GroundItem>(AttributeKey.INTERACTED_GROUNDITEM)
}

fun Script.widgetHash(): Int {
	return player().attrib<Int>(AttributeKey.INTERACTED_WIDGET_INFO)
}

operator fun ItemContainer.contains(id: Int): Boolean {
	return has(id)
}

operator fun ItemContainer.contains(item: Item?): Boolean {
	return item != null && count(item.id()) >= item.amount()
}

operator fun Varps.get(i: Int): Int {
	return varp(i)
}

operator fun Varps.set(i: Int, j: Int) {
	varp(i, j)
}

operator fun TimerRepository.set(key: TimerKey, v: Int) {
	register(key, v)
}

operator fun TimerRepository.contains(key: TimerKey): Boolean {
	return has(key)
}

operator fun <T> Entity.get(i: AttributeKey): T {
	return attrib(i)
}

operator fun ItemContainer.minusAssign(item: Item) {
	remove(item, true)
}

operator fun ItemContainer.minusAssign(items: List<Int>) {
	for (item in items)
		remove(Item(item), true)
}

operator fun ItemContainer.minusAssign(item: Int) {
	remove(Item(item), true)
}

operator fun ItemContainer.plusAssign(item: Item) {
	add(item, true)
}

operator fun ItemContainer.plusAssign(item: Int) {
	add(Item(item), true)
}

operator fun Skills.get(i: Int): Int {
	return level(i)
}

fun <E : Enum<E>> randomEnum(enums: Array<E>, vararg not: E): E {
	var set: LinkedList<E> = LinkedList()
	
	set.addAll(enums)
	for (e in not) {
		set.remove(e)
	}
	
	return set[RANDOM.nextInt(set.size)]
}

fun String.red(): String {
	return "<col=ff0000>" + (this);
}

fun String.col(col: String): String {
	return StringBuilder("<col=").append(col).append(">").append(this).append("</col>").toString()
}

fun Script.runGlobal(world: World, scr: (Script) -> Any) {
	world.server().scriptExecutor().executeScript(world, scr)
}

fun Script.slayerTaskAmount(): Int {
	return player().attribOr(AttributeKey.SLAYER_TASK_AMT, 0)
}
fun Script.slayerTaskId(): Int {
	return player().attribOr(AttributeKey.SLAYER_TASK_ID, 0)
}

fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)

val RANDOM = Random()
fun <E : Any?> Array<E>.random(): E = this[RANDOM.nextInt(size)]
fun IntArray.random() = this[RANDOM.nextInt(size)]

@Suspendable fun Script.setLevel(id: Int) {
	
	// Hunter has a way different dialogue
	if (id == Skills.HUNTER) {
		//player().interfaces().send(193, 162, 547, false)
		player().interfaces().setting(132, 2, -1, -1, 1)
		player().write(InterfaceText(193, 2, "Click here to continue"))
		player().interfaces().setting(132, 3, -1, -1, 0)
		player().interfaces().setting(132, 4, -1, -1, 0)
		player().write(InterfaceText(193, 3, ""))
		player().write(InterfaceText(193, 4, ""))
		player().write(InterfaceItem(193, 0, 9951, 400))
		player().write(InterfaceText(193, 1, "<col=000080>Congratulations, you've just advanced a Hunter level.<col=000000><br><br>Your Hunter level is now ${player().skills().xpLevel(id)}."))
	} else {
		val unhide: Int = when (id) {
			Skills.AGILITY -> 3
			Skills.ATTACK -> 5
			Skills.CONSTRUCTION -> 8
			Skills.COOKING -> 11
			Skills.CRAFTING -> 13
			Skills.DEFENCE -> 16
			Skills.FARMING -> 18
			Skills.FIREMAKING -> 20
			Skills.FISHING -> 22
			Skills.FLETCHING -> 24
			Skills.HERBLORE -> 27
			Skills.HITPOINTS -> 29
			Skills.MAGIC -> 31
			Skills.MINING -> 33
			Skills.PRAYER -> 35
			Skills.RANGED -> 37
			Skills.RUNECRAFTING -> 40
			Skills.SLAYER -> 42
			Skills.SMITHING -> 44
			Skills.STRENGTH -> 46
			Skills.THIEVING -> 48
			Skills.WOODCUTTING -> 50
			else -> 1
		}
		
		player().write(
				InterfaceVisibility(233, unhide, false), // Sprite
				InterfaceText(233, 0, "<col=000080>You've successfully set your ${Skills.SKILL_INDEFINITES[id]} ${Skills.SKILL_NAMES[id]} level."),
				InterfaceText(233, 1, "Your ${Skills.SKILL_NAMES[id]} level is now ${player().skills().xpLevel(id)}.")
		)
		
		player().interfaces().send(233, 162, 550, false)
	}
	
	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()
	
	player().interfaces().close(162, 550)
}

fun attempt(callable: Function0<Any?>) {
	try {
		callable()
	} catch (ex: Exception) {
		ex.printStackTrace()
	}
}

@Suspendable fun Script.addXp(id: Int, xp: Double) {
	val leveled = player().skills().__addXp(id, xp)
	/*val leveled = player().skills().__addXp(id, xp)

	if (leveled) {
	if (id != Skills.THIEVING)
		player().animate(-1) // RS resets your anim, but on thieving it breaks...

	// Hunter has a way different dialogue
	if (id == Skills.HUNTER) {
		player().interfaces().send(193, 162, 547, false)
		player().interfaces().setting(132, 2, -1, -1, 1)
		player().write(InterfaceText(193, 2, "Click here to continue"))
		player().interfaces().setting(132, 3, -1, -1, 0)
		player().interfaces().setting(132, 4, -1, -1, 0)
		player().write(InterfaceText(193, 3, ""))
		player().write(InterfaceText(193, 4, ""))
		player().write(InterfaceItem(193, 0, 9951, 400))
		player().write(InterfaceText(193, 1, "<col=000080>Congratulations, you've just advanced a Hunter level.<col=000000><br><br>Your Hunter level is now ${player().skills().xpLevel(id)}."))
	} else {
		val unhide: Int = when (id) {
			Skills.AGILITY -> 3
			Skills.ATTACK -> 5
			Skills.CONSTRUCTION -> 8
			Skills.COOKING -> 11
			Skills.CRAFTING -> 13
			Skills.DEFENCE -> 16
			Skills.FARMING -> 18
			Skills.FIREMAKING -> 20
			Skills.FISHING -> 22
			Skills.FLETCHING -> 24
			Skills.HERBLORE -> 27
			Skills.HITPOINTS -> 29
			Skills.MAGIC -> 31
			Skills.MINING -> 33
			Skills.PRAYER -> 35
			Skills.RANGED -> 37
			Skills.RUNECRAFTING -> 40
			Skills.SLAYER -> 42
			Skills.SMITHING -> 44
			Skills.STRENGTH -> 46
			Skills.THIEVING -> 48
			Skills.WOODCUTTING -> 50
			else -> 1
		}

		player().write(
				HideInterface(233, unhide, false), // Sprite
				InterfaceText(233, 0, "<col=000080>Congratulations, you just advanced ${Skills.SKILL_INDEFINITES[id]} ${Skills.SKILL_NAMES[id]} level."),
				InterfaceText(233, 1, "Your ${Skills.SKILL_NAMES[id]} level is now ${player().skills().xpLevel(id)}.")
		)

		player().interfaces().send(233, 162, 547, false)
	}

	waitReason = WaitReason.DIALOGUE
	interruptCall = dialogueInterruptCall
	Fiber.park()

	player().interfaces().close(162, 547)
}*/
}

class TestVarbits(val start: Int, val end: Int, val size: Int) : Function1<Script, Unit> {
	override fun invoke(it: Script) {
		val player = it.player()
		for (n in start..end) {
			player.varps().varbit(n, size)
			player.message("Varbit %d value now %d", n, size)
		}
	}
}


object ItemOnItem {
	fun slotOf(script: Script, item: Int): Int {
		val player = script.player()
		
		val from = player.attrib<Item>(AttributeKey.FROM_ITEM)
		val to = player.attrib<Item>(AttributeKey.TO_ITEM)
		if (from == null || to == null)
			return -1
		
		if (from.id() == item)
			return player.attrib<Int>(AttributeKey.ITEM_SLOT)
		if (to.id() == item)
			return player.attrib<Int>(AttributeKey.ALT_ITEM_SLOT)
		
		return -1
	}
}