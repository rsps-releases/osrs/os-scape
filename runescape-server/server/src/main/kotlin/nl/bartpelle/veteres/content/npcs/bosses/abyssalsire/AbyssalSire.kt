package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.hitorigin.PoisonOrigin
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 10/11/2016.
 */

object AbyssalSire {
	
	@JvmField
	val hitScript: Function1<Script, Unit> = s@ @Suspendable {
		val abyssalSire = it.npc()
		val targetedPlayer = abyssalSire.attribOr<Entity>(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, null)
		val abyssalSireState = abyssalSire.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.STASIS)
		val abyssalSirePhase = abyssalSire.attribOr<AbyssalSirePhase>(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
		val hit = abyssalSire.attribOr<Hit?>(AttributeKey.LAST_HIT_DMG, null)
		val attacker: Entity? = if (hit != null) hit.origin() as? Entity else null
		
		//If the Abyssal Sire is currently in combat, check respiratory condition & current HP for disorientation
		if (abyssalSireState == AbyssalSireState.COMBAT) {
			// Is our respiratory system okay?
			if (abyssalSirePhase == AbyssalSirePhase.PHASE_ONE) {
				AbyssalSireRespiratorySystem.checkRepositorySystem(abyssalSire)
				
			}
			
			// Is our respiratory system okay?
			if (abyssalSirePhase == AbyssalSirePhase.PHASE_TWO && attacker != null && attacker is Player && !EntityCombat.canAttackMelee(abyssalSire, attacker, true)) {
				abyssalSire.world().executeScript(@Suspendable { s ->
					Teleports.basicTeleport(attacker, abyssalSire.tile())
					
					s.delay(3)
					
					attacker.hit(attacker, abyssalSire.world().random(25..35))
				})
			}
			
			//If we've taken at least 75 damage, disorient & heal to full!
			if (abyssalSirePhase == AbyssalSirePhase.PHASE_ONE && (abyssalSire.maxHp() - abyssalSire.hp()) >= 75) {
				AbyssalSirePhaseOne.disorientAbyssalSire(targetedPlayer, abyssalSire)
				abyssalSire.heal(abyssalSire.maxHp() - abyssalSire.hp())
			}
		}
		
		//Start the Abyssal Sire Chamber Reset timer..
		abyssalSire.timers().addOrSet(TimerKey.ABYSSAL_SIRE_RESET_TIMER, 100)
	}
	
	@JvmField
	val script: Function1<Script, Unit> = s@ @Suspendable {
		val abyssalSire = it.npc()
		var abyssalSireState = abyssalSire.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
		var abyssalSireCombatPhase = abyssalSire.attribOr<AbyssalSirePhase>(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
		val targetedPlayer = abyssalSire.attribOr<Entity>(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, null)
		
		while (EntityCombat.targetOk(abyssalSire, targetedPlayer, 40) && !it.interrupted()) {
			//Check to see if our repository system is still intact!
			if (abyssalSireCombatPhase == AbyssalSirePhase.PHASE_ONE) {
				AbyssalSireRespiratorySystem.checkRepositorySystem(abyssalSire)
			}
			
			//Constantly face the player!
			if (abyssalSireCombatPhase == AbyssalSirePhase.PHASE_TWO && !abyssalSire.locked())
				abyssalSire.face(targetedPlayer)
			
			//Check if we're in phase two and lower than 200 HP.. if so engage phase 3!
			if (abyssalSireCombatPhase == AbyssalSirePhase.PHASE_TWO && abyssalSire.hp() < 200) {
				AbyssalSirePhaseThree.initPhaseThree(abyssalSire, targetedPlayer)
			}
			
			//Check if we're in phase two and lower than 200 HP.. if so engage phase 3!
			if (abyssalSireCombatPhase == AbyssalSirePhase.PHASE_THREE_PART_ONE && abyssalSire.hp() < 139) {
				AbyssalSirePhaseThree.initPhaseThreePartTwo(abyssalSire, targetedPlayer)
			}
			
			if (EntityCombat.attackTimerReady(abyssalSire) && abyssalSireState == AbyssalSireState.COMBAT && !abyssalSire.locked()) {
				abyssalSireCombatPhase = abyssalSire.attribOr<AbyssalSirePhase>(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
				
				when (abyssalSireCombatPhase) {
					AbyssalSirePhase.PHASE_ONE -> AbyssalSirePhaseOne.phaseOneAttack(it, abyssalSire, targetedPlayer)
					AbyssalSirePhase.PHASE_TWO -> AbyssalSirePhaseTwo.phaseTwoAttack(abyssalSire, targetedPlayer)
					AbyssalSirePhase.PHASE_THREE_PART_ONE -> AbyssalSirePhaseThree.phaseOneAttack(it, abyssalSire, targetedPlayer)
					AbyssalSirePhase.PHASE_THREE_PART_TWO -> AbyssalSirePhaseThree.phaseTwoAttack(it, abyssalSire, targetedPlayer)
				}
				
			} else if (abyssalSireState == AbyssalSireState.DISORIENTED) {
				val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
				val abyssalSireRespiratory = AbyssalSireRespiratorySystem.getRespiratorySystem(abyssalSire)
				
				AbyssalSireTentacles.faceSouth(abyssalSireTentacles)
				
				if (abyssalSire.world().rollDie(3, 1))
					AbyssalSireRespiratorySystem.attemptToHeal(abyssalSireRespiratory)
			}
			
			abyssalSireCombatPhase = abyssalSire.attribOr<AbyssalSirePhase>(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
			abyssalSireState = abyssalSire.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
			EntityCombat.postHitLogic(abyssalSire)
			it.delay(1)
		}
	}
	
	@JvmStatic
	fun awakenAbyssalSire(npc: Npc, target: Player) {
		val state = npc.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.STASIS)
		
		if (state === AbyssalSireState.STASIS) {
			AbyssalSirePhaseOne.initPhaseOne(npc)
			npc.putattrib(AttributeKey.LAST_DAMAGER, target)
			npc.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		}
	}
	
}
