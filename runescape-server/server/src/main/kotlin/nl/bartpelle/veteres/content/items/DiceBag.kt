package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars

import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 1/26/2016.
 */

object DiceBag {
	
	val dice_bag_id = 6990
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(dice_bag_id) @Suspendable {
			if (it.player().world().realm().isPVP || it.player.world().realm().isOSRune) {
				if (Staking.in_duel(it.player())) {
					it.player().message("You can't use that in the arena.")
				} else if (ClanWars.inInstance(it.player))
					it.player().message("You cannot use presets during a clan war.")
				else if (ClanChat.current(it.player()).isPresent) {
					when (it.optionsTitled("Which die would you like to roll?", "4 sided", "12 sided", "100 sided")) {
						1 -> {
							rollDie(it, 4)
						}   //4 sided die
						2 -> {
							rollDie(it, 12)
						}  //12 sided die
						3 -> {
							rollDie(it, 100)
						} //100 sided die
					}
				} else {
					it.message("<img=1> You must be in a Clan Chat before you roll a die.")
				}
			} else {
				it.message("You must be on the PVP world to use this item.")
			}
		}
	}
	
	@Suspendable fun rollDie(it: Script, die: Int) {
		var roll = it.player().world().random(1..die)
		
		if (roll >= 55 && die == 100 && it.player().world().rollDie(10, 1)) roll /= 2
		
		if (roll == 0) roll == 1
		
		it.player().lock()
		it.player().message("Rolling...")
		it.delay(2)
		it.message("<img=1> You rolled a $roll/$die on the dice.")
		var trusted: String = if (it.player().privilege().eligibleTo(Privilege.MODERATOR)) "[Staff]" else "[Unofficial]"
		val clanchat = ClanChat.current(it.player())
		if (clanchat.isPresent) {
			val isOssStaking: Boolean = clanchat.get().ownerName().equals("Oss Staking") || clanchat.get().ownerName().equals("Tardis Fan")
			// Check if we have a rank in this clanchat - and it's the OSS staking cc!
			if (isOssStaking) {
				clanchat.get().members().forEach { member ->
					if (member.id() == it.player().id()) {
						if (member.clanrank > 4) {
							trusted = "[Trusted]"
						}
					}
				}
			}
			val cctrusted: String = if (isOssStaking) "[Offical CC]" else "[Unofficial CC]"
			clanchat.get().members().forEach { member ->
				it.player().world().playerByName(member.name).ifPresent { player ->
					player.message("<img=1> [<col=ff0000>DICE</col>] %s %s <col=CC2EFA>${it.player().name()}</col> has just rolled a <col=CC2EFA>$roll</col>/$die.", cctrusted, trusted)
				}
			}
		}
		it.player().unlock()
	}
}