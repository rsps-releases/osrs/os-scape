package nl.bartpelle.veteres.content.events.christmas.antisanta

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 12/22/2015.
 */

object HappyChildren {
	
	val CHILD_1 = 6802
	val CHILD_1_DIAG = 6772
	val CHILD_1_CRYING = 6803
	val CHILD_1_CRYING_DIAG = 6774
	
	val CHILD_2 = 6796
	val CHILD_2_DIAG = 6763
	val CHILD_2_CRYING = 6797
	val CHILD_2_CRYING_DIAG = 6765
	
	val CHILD_3 = 6806
	val CHILD_3_DIAG = 6778
	val CHILD_3_CRYING = 6807
	val CHILD_3_CRYING_DIAG = 6780
	
	val CHILD_4 = 6798
	val CHILD_4_DIAG = 6766
	val CHILD_4_CRYING = 6799
	val CHILD_4_CRYING_DIAG = 6768
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(CHILD_1) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 20) {
				it.chatNpc("Merry Christmas!", CHILD_1_DIAG)
				when (it.options("Merry Christmas.", "What are you so happy about?")) {
					1 -> {
						it.chatPlayer("Merry Christmas.")
					}
					2 -> {
						it.chatPlayer("What are you so happy about?")
						it.chatNpc("It's Christmas! Santa's going to bring me all the presents I wished for!", CHILD_1_DIAG)
						when (it.options("Santa doesn't bring presents to naughty kids like you.", "I'm sure he will, goodbye.")) {
							1 -> {
								it.chatPlayer("Santa doesn't bring presents to naughty kids like you.")
								it.chatNpc("But I've been good!", CHILD_1_DIAG)
								when (it.options("Give Anti-present", "Oh, my bad. Have a happy holiday.")) {
									1 -> {
										it.chatPlayer("That's not what it says on Santa's list, this is the only present you'll be getting this year!")
										it.player().varps().varbit(Varbit.XMAS2015_GENSTORE_KID, 1)
										it.player().inventory() -= 13345
										it.messagebox("The child bursts into tears when they open the present to find it contains a lump of coal.")
									}
									2 -> {
										it.chatPlayer("Oh, my bad. Have a happy holiday.")
									}
								}
							}
							2 -> {
								it.chatPlayer("I'm sure he will, goodbye.")
							}
						}
					}
				}
			} else {
				it.chatNpc("Merry Christmas!", CHILD_1_DIAG)
			}
		}
		
		r.onNpcOption1(CHILD_1_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 60) {
				if (!it.player().inventory().contains(13346)) {
					it.chatPlayer("I'll need a present from Santa if I want to cheer them up...")
				} else {
					it.chatNpc("What do you want?", CHILD_1_CRYING_DIAG)
					when (it.options("Give present", "Nothing, bye.")) {
						1 -> {
							it.chatPlayer("I double-checked with Santa and it seems his helpers made a mistake, you were meant to be on the nice list all along!")
							it.chatNpc("Really?", CHILD_1_CRYING_DIAG)
							it.chatPlayer("Yes, and to make up for it, Santa's asked me to give you this very special present!")
							it.player().inventory() -= 13346
							it.player().varps().varbit(Varbit.XMAS2015_GENSTORE_KID, 0)
							checkKids(it.player())
							it.messagebox("A glow of happiness appears on the child's face as they open the present.")
						}
						2 -> {
							it.chatPlayer("Nothing, bye.")
						}
					}
				}
			} else {
				it.message("The child is too busy crying...")
			}
		}
		r.onNpcOption2(CHILD_1_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_GENSTORE_KID) != 2) {
				it.player().lock()
				it.animate(881)
				it.delay(1)
				it.player().message("You carefully add some of the tears to the vial.")
				it.player().varps().varbit(Varbit.XMAS2015_GENSTORE_KID, 2)
				giveVialOfTears(it)
				it.player().unlock()
			}
		}
		
		r.onNpcOption1(CHILD_2) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) != 20) {
				it.chatNpc("Merry Christmas!", CHILD_2_DIAG)
			} else {
				it.chatNpc("Merry Christmas!", CHILD_2_DIAG)
				when (it.options("Merry Christmas.", "Is that your Christmas wish-list?")) {
					1 -> {
						it.chatPlayer("Merry Christmas.")
					}
					2 -> {
						it.chatPlayer("Is that your Christmas wish-list?")
						it.chatNpc("Yep! I've been good all  year around so I know Santa will be giving me something nice!", CHILD_2_DIAG)
						when (it.options("That's not what I heard...", "I'm sure he will, goodbye.")) {
							1 -> {
								it.chatPlayer("That's not what I heard...")
								it.chatNpc("W-What do you mean?", CHILD_2_DIAG)
								when (it.options("Give Anti-present", "Nothing, goodbye.")) {
									1 -> {
										it.chatPlayer("I heard you're on the naughty list! This is the only present you'll be getting this year!")
										it.player().varps().varbit(Varbit.XMAS2015_RANGESTORE_KID, 1)
										it.player().inventory() -= 13345
										it.messagebox("The child bursts into tears when they open the present to find it contains a lump of coal.")
									}
									2 -> {
										it.chatPlayer("Nothing, goodbye.")
									}
								}
							}
							2 -> {
								it.chatPlayer("I'm sure he will, goodbye.")
							}
						}
					}
				}
			}
		}
		
		r.onNpcOption1(CHILD_2_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 60) {
				// TODO QUEST STAGE CHECK
				if (!it.player().inventory().contains(13346)) {
					it.chatPlayer("I'll need a present from Santa if I want to cheer them up...")
				} else {
					it.chatNpc("You again. *Sob* What do you want?", CHILD_2_CRYING_DIAG)
					when (it.options("Give present", "Nothing, bye.")) {
						1 -> {
							it.chatPlayer("I double-checked with Santa and it seems his helpers made a mistake, you were meant to be on the nice list all along!")
							it.chatNpc("Really?", CHILD_2_CRYING_DIAG)
							it.chatPlayer("Yes, and to make up for it, Santa's asked me to give you this very special present!")
							it.player().inventory() -= 13346
							it.player().varps().varbit(Varbit.XMAS2015_RANGESTORE_KID, 0)
							checkKids(it.player())
							it.messagebox("A glow of happiness appears on the child's face as they open the present.")
						}
						2 -> {
							it.chatPlayer("Nothing, bye.")
						}
					}
				}
			} else {
				it.message("The child is too busy crying...")
			}
		}
		r.onNpcOption2(CHILD_2_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_RANGESTORE_KID) != 2) {
				it.player().lock()
				it.animate(881)
				it.delay(1)
				it.player().message("You carefully add some of the tears to the vial.")
				it.player().varps().varbit(Varbit.XMAS2015_RANGESTORE_KID, 2)
				giveVialOfTears(it)
				it.player().unlock()
			}
		}
		
		r.onNpcOption1(CHILD_3) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) != 20) {
				it.chatNpc("Merry Christmas!", CHILD_3_DIAG)
			} else {
				it.chatNpc("Merry Christmas!", CHILD_3_DIAG)
				when (it.options("Merry Christmas.", "What have you got there, kiddo?")) {
					1 -> {
						it.chatPlayer("Merry Christmas.")
					}
					2 -> {
						it.chatPlayer("What have you got there, kiddo?")
						it.chatNpc("It's my wish-list, I've written down all the presents I would like from Santa!", CHILD_3_DIAG)
						when (it.options("Well, that's a waste of time.", "Good for you, bye.")) {
							1 -> {
								it.chatPlayer("Well, that's a waste of time.")
								it.chatNpc("No it's not! Santa gets all the nice children something from their wish-list!", CHILD_3_DIAG)
								when (it.options("Give Anti-present", "Oh right, sure he does, goodbye.")) {
									1 -> {
										it.chatPlayer("That's true, but it looks like you didn't make it onto his nice list this year, this is the only present you'll be receiving!")
										it.player().varps().varbit(Varbit.XMAS2015_EAST_KID, 1)
										it.player().inventory() -= 13345
										it.messagebox("The child bursts into tears when they open the present to find it contains a lump of coal.")
									}
									2 -> {
										it.chatPlayer("Oh right, sure he goes, goodbye.")
									}
								}
							}
							2 -> {
								it.chatPlayer("Good for you, bye.")
							}
						}
					}
				}
			}
		}
		
		r.onNpcOption1(CHILD_3_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 60) {
				if (!it.player().inventory().contains(13346)) {
					it.chatPlayer("I'll need a present from Santa if I want to cheer them up...")
				} else {
					it.chatNpc("Please leave me alone!", CHILD_3_CRYING_DIAG)
					when (it.options("Give present", "Okay, bye.")) {
						1 -> {
							it.chatPlayer("But I'm here to give you a present from Santa.")
							it.chatNpc("I was on the nuaghty list, remember?", CHILD_3_CRYING_DIAG)
							it.chatPlayer("That was all a mistake! Santa's lists got mixed up, thi is a special present just for you!")
							it.player().inventory() -= 13346
							it.player().varps().varbit(Varbit.XMAS2015_EAST_KID, 0)
							checkKids(it.player())
							it.messagebox("A glow of happiness appears on the child's face as they open the present.")
						}
						2 -> {
							it.chatPlayer("Okay, bye.")
						}
					}
				}
			} else {
				it.message("The child is too busy crying...")
			}
		}
		
		r.onNpcOption2(CHILD_3_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_EAST_KID) != 2) {
				it.player().lock()
				it.animate(881)
				it.delay(1)
				it.player().message("You carefully add some of the tears to the vial.")
				it.player().varps().varbit(Varbit.XMAS2015_EAST_KID, 2)
				giveVialOfTears(it)
				it.player().unlock()
			}
		}
		
		r.onNpcOption1(CHILD_4) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) != 20) {
				it.chatNpc("Merry Christmas!", CHILD_4_DIAG)
			} else {
				it.chatNpc("Merry Christmas!", CHILD_4_DIAG)
				when (it.options("Merry Christmas.", "What are you so happy about?")) {
					1 -> {
						it.chatPlayer("Merry Christmas.")
					}
					2 -> {
						it.chatPlayer("What are you so happy about?")
						it.chatNpc("It's Christmas! Santa's going to bring me all the presents I wished for!", CHILD_4_DIAG)
						when (it.options("Santa doesn't bring presents to naughty kids like you.", "I'm sure he will, goodbye.")) {
							1 -> {
								it.chatPlayer("Santa doesn't bring presents to naughty kids like you.")
								it.chatNpc("But I've been good!", CHILD_4_DIAG)
								when (it.options("Give Anti-present", "Oh, my bad. Have a happy holiday.")) {
									1 -> {
										it.chatPlayer("That's not what it says on Santa's list, this is the only present you'll be getting this year!")
										it.player().varps().varbit(Varbit.XMAS2015_AUBURY_KID, 1)
										it.player().inventory() -= 13345
										it.messagebox("The child bursts into tears when they open the present to find it contains a lump of coal.")
									}
									2 -> {
										it.chatPlayer("Oh, my bad. Have a happy holiday.")
									}
								}
							}
							2 -> {
								it.chatPlayer("I'm sure he will, goodbye.")
							}
						}
					}
				}
			}
		}
		
		r.onNpcOption1(CHILD_4_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 60) {
				if (!it.player().inventory().contains(13346)) {
					it.chatPlayer("I'll need a present from Santa if I want to cheer them up...")
				} else {
					it.chatNpc("Go away!", CHILD_4_CRYING_DIAG)
					when (it.options("Give present", "Sure, bye.")) {
						1 -> {
							it.chatPlayer("I'm sorry about earlier, it turns out you were on the nice list all along!")
							it.chatNpc("I knew it!", CHILD_4_CRYING_DIAG)
							it.chatPlayer("To make up for it, Santa's asked me to give you this very special present!")
							it.player().inventory() -= 13346
							it.player().varps().varbit(Varbit.XMAS2015_AUBURY_KID, 0)
							checkKids(it.player())
							it.messagebox("A glow of happiness appears on the child's face as they open the present.")
						}
						2 -> {
							it.chatPlayer("Sure, bye.")
						}
					}
				}
			} else {
				it.message("The child is too busy crying...")
			}
		}
		
		r.onNpcOption2(CHILD_4_CRYING) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_AUBURY_KID) != 2) {
				it.player().lock()
				it.animate(881)
				it.delay(1)
				it.player().message("You carefully add some of the tears to the vial.")
				it.player().varps().varbit(Varbit.XMAS2015_AUBURY_KID, 2)
				giveVialOfTears(it)
				it.player().unlock()
			}
		}
	}
	
	fun checkKids(p: Player) {
		if (p.varps().varbit(Varbit.XMAS2015_AUBURY_KID) == 0 &&
				p.varps().varbit(Varbit.XMAS2015_EAST_KID) == 0 &&
				p.varps().varbit(Varbit.XMAS2015_GENSTORE_KID) == 0 &&
				p.varps().varbit(Varbit.XMAS2015_RANGESTORE_KID) == 0) {
			p.message("<col=ff0000>You have cheered up all of the children, you should return to Santa.")
		}
	}
	
	
	@Suspendable fun giveVialOfTears(it: Script) {
		if (it.player().inventory().contains(13347)) {
			it.player().inventory() -= 13347
			it.player().inventory() += 13348
		} else if (it.player().inventory().contains(13348)) {
			it.player().inventory() -= 13348
			it.player().inventory() += 13349
		} else if (it.player().inventory().contains(13349)) {
			it.player().inventory() -= 13349
			it.player().inventory() += 13350
		} else if (it.player().inventory().contains(13350)) {
			it.player().inventory() -= 13350
			it.player().inventory() += 13351
		}
	}
	
}
