package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 8/28/2015.
 */

object Spinolyp {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, false, 8) && EntityCombat.attackTimerReady(npc)) {
				val style = if (npc.world().random(1) == 1) CombatStyle.MAGIC else CombatStyle.RANGE
				val gfx = when (style) {
					CombatStyle.MAGIC -> 121 // Water bolt?
					else -> 476 // Ranged
				}
				
				val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
				npc.world().spawnProjectile(npc, target, gfx, 20, 30, 20, 12 * tileDist, 14, 5)
				val delay = Math.max(1, (20 + (tileDist * 12)) / 30)
				
				if (EntityCombat.attemptHit(npc, target, style)) {
					target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(style)
					
					if (npc.world().random().nextBoolean()) {
						target.poison(6)
					}
				} else {
					val hit = target.hit(npc, 0, delay.toInt()).combatStyle(style).graphic(Graphic(85, 90)) // Uh-oh, that's a miss.
					if (style == CombatStyle.MAGIC)
						hit.block(false)
				}
				
				npc.animate(npc.attackAnimation())
				
				// .. and go into sleep mode.
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
}