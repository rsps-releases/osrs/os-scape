package nl.bartpelle.veteres.content.areas.burthorpe

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/7/2016.
 */

object RowBoat {
	
	val ROW_BOAT = 27066
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(ROW_BOAT) {
			when (it.optionsTitled("Do you want to leave the island?", "Yes", "No")) {
				1 -> it.player().teleport(2724, 3589)
			}
		}
	}
}