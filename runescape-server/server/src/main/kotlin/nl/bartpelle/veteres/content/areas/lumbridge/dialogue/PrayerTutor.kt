package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/20/2016.
 */

object PrayerTutor {
	
	val PRAYER_TUTOR = 3223
	
	val BONES = 526
	val BIG_BONES = 532
	val BABYDRAGON_BONES = 534
	val WYVERN_BONES = 6812
	val DRAGON_BONES = 536
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(PRAYER_TUTOR) @Suspendable {
			when (it.options("Any advice for an advanced user?", "Tell me about different bones.", "Goodbye.")) {
				1 -> advice(it)
				2 -> {
					it.chatPlayer("Tell me about different bones.", 554)
					bones(it)
				}
				3 -> it.chatPlayer("Goodbye.", 588)
			}
		}
	}
	
	@Suspendable fun advice(it: Script) {
		it.chatPlayer("Any advice for an advanced user?", 554)
		it.chatNpc("When you've buried enough bones there are some<br>prayers available which will heal you or hurt your" +
				"<br>opponent when you're most in need or even dying. Be<br>sure you put these on a little while before death as it", PRAYER_TUTOR, 570)
		it.chatNpc("takes a while to say the prayer... as, if you're dead you<br>can't say it.", PRAYER_TUTOR, 568)
		it.chatNpc("Of course, if you can get your hands on some Prayer<br>Potions, you won't need to recharge your points at an<br>altar. This can be" +
				" really useful if you're setting off on<br>an adventure which is away from civilisation.", PRAYER_TUTOR, 570)
		it.chatNpc("There is also a way of making prayer easier to learn..<br>investigate Port Phasmatys by travelling through the<br>swamp to the east - " +
				"beware of the vampyres though!", PRAYER_TUTOR, 569)
		it.chatPlayer("Thank you, I'll remember that.", 567)
	}
	
	
	@Suspendable fun bones(it: Script) {
		when (it.options("Basic Bones", "Big Bones", "Babydragon Bones", "More....", "Goodbye.")) {
			1 -> basic_bones(it)
			2 -> big_bones(it)
			3 -> babydragon_bones(it)
			4 -> more_bones(it)
			5 -> it.chatPlayer("Goodbye.", 588)
			
		}
	}
	
	@Suspendable fun more_bones(it: Script) {
		when (it.options("Wyvern Bones", "Dragon Bones", "Back...", "Goodbye.")) {
			1 -> wyvern_bones(it)
			2 -> dragon_bones(it)
			3 -> bones(it)
			4 -> it.chatPlayer("Goodbye.", 588)
		}
	}
	
	
	@Suspendable fun basic_bones(it: Script) {
		it.itemBox("Basic bones are left by many creatures such as goblins,<br>monkeys and that sort of thing. They won't get you" +
				"<br>much when you bury them, but if you do it every time<br>you come across them, it all adds up!", BONES)
		bones(it)
	}
	
	@Suspendable fun big_bones(it: Script) {
		it.itemBox("Big bones you can get by killing things like ogres and<br>giants, them being big things and all. They're quite a" +
				"<br>good boost for your prayer if you are up to fighting<br>the big boys.", BIG_BONES)
		it.itemBox("You can probably find them in caves and such dark<br>dank places.", BIG_BONES)
		bones(it)
	}
	
	@Suspendable fun babydragon_bones(it: Script) {
		it.itemBox("If you're feeling adventurous and up to tackling baby<br>dragons, you can get these Baby Dragon bones which<br>" +
				"are even better than big bones.", BABYDRAGON_BONES)
		bones(it)
	}
	
	@Suspendable fun wyvern_bones(it: Script) {
		it.itemBox("If you have the experience in the slayer skill, you can<br>tackle these formidable beasts and get a substantial boost" +
				"<br>to your prayer.", WYVERN_BONES)
		it.itemBox("People often mistake them for dragons, but dragons<br>have 4 legs, wyverns have only two, or if they do have<br>4, the" +
				" two front legs are so small they can't use them<br>to walk on... or so I've read in the histories.", WYVERN_BONES)
		bones(it)
	}
	
	@Suspendable fun dragon_bones(it: Script) {
		it.itemBox("The greatest challenge for bones will be dragons, these<br>fearsome beasts dwell in several places around the lands,<br>" +
				"I suggest you don't try looking for them until you are<br>fully prepared! However the rewards are worth it.", DRAGON_BONES)
		bones(it)
	}
}
