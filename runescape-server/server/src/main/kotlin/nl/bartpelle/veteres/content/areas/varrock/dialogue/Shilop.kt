package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/11/2015.
 */

object Shilop {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (SHILOP in intArrayOf(3501, 3503)) {
			r.onNpcOption1(SHILOP) @Suspendable {
				it.chatPlayer("Hello again.", 567)
				it.chatNpc("You think you're tough do you?", SHILOP, 614)
				it.chatPlayer("Pardon?", 571)
				it.chatNpc("I can beat anyone up!", SHILOP, 614)
				it.chatNpc("He can you know!", if (SHILOP == 3501) 3503 else 3501, 614)
				it.chatPlayer("Really?", 562)
				it.messagebox("The boy begins to jump around with his fists up. You decide it's best<br>not to kill him just yet.")
			}
		}
	}
	
}
