package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 6/3/2016.
 */

object CyclopsRoom {
	
	val WARRIORS_GUILD_TOKENS = 8851
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit() @Suspendable {
			while (true) {
				val world = it.ctx<World>()
				
				world.players().forEachKt({ p ->
					//If our player is inside the second floor..
					if (insideTopFloorCyclopsRoom(p.tile()) && p.tile().level == 2 && p.tile() != Tile(2847, 3537, 2)) {
						handle_time_spent(p)
					}
				})
				it.delay(2)
			}
		}
	}
	
	fun handle_time_spent(p: Player) {
		var timeSpent = p.attribOr<Int>(AttributeKey.WARRIORS_GUILD_CYCLOPS_ROOM, 0)
		
		if (timeSpent == 60) {
			p.putattrib(AttributeKey.WARRIORS_GUILD_CYCLOPS_ROOM, 0)
			if (p.inventory().remove(Item(WARRIORS_GUILD_TOKENS, 10), false).success()) {
				p.message("<col=804080>10 of your tokens crumble away.")
			} else {
				p.teleport(Tile(2846, 3540, 2))
				p.message("<col=804080>Next time, please leave as soon as your time is up.")
			}
		} else {
			p.putattrib(AttributeKey.WARRIORS_GUILD_CYCLOPS_ROOM, timeSpent + 1)
		}
	}
	
	@JvmStatic fun insideTopFloorCyclopsRoom(tile: Tile): Boolean {
		return tile.inArea(2838, 3543, 2876, 3556) || tile.inArea(2847, 3534, 2876, 3542)
	}
	
}
