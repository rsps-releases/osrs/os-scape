package nl.bartpelle.veteres.content.areas.wilderness.magearena

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/24/2016.
 */

object Kolodion {
	
	val KOLODION = 1603
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(KOLODION) @Suspendable {
			if (it.player().world().realm().isPVP) {
			} else {
				if (it.player().attribOr<Int>(AttributeKey.MAGE_ARENA_PHASE, 0) == 0) {
					it.chatPlayer("Hello there. What is this place?", 588)
					it.chatNpc("I am the great Kolodion, master of battle magic, and<br>this is my battle arena. Top wizards travel from all over<br>OS-Scape to fight here.", KOLODION, 594)
					when (it.options("Can I fight here?", "What's the point of that?", "That's barbaric!")) {
						1 -> can_I_fight_here(it)
						2 -> whats_the_point(it)
						3 -> thats_barbaric(it)
					}
				} else if (it.player().attribOr<Int>(AttributeKey.MAGE_ARENA_PHASE, 1) == 1) {
					it.chatPlayer("Hello, Kolodion.", 588)
					it.chatNpc("Hello, young mage. I see you're not as tought as you<br>thought. Would you like to try again?", KOLODION, 592)
					when (it.options("Yes.", "No.")) {
						1 -> start_miniquest(it)
						2 -> {
							it.chatPlayer("No, I don't think I'm strong enough yet.")
							it.chatNpc("Okay, please do try again!", KOLODION, 588)
						}
					}
				} else if (it.player().attribOr<Int>(AttributeKey.MAGE_ARENA_PHASE, 2) == 2) {
					it.chatPlayer("Hello, Kolodion.", 588)
					it.chatNpc("Hello, young mage. You're a tough one.", KOLODION, 592)
					it.chatPlayer("What now?", 554)
					it.chatNpc("Step into the magic pool. It will take you to a chamber.<br>There, you must decide which god you will represent in<br>the arena.", KOLODION, 594)
					it.chatPlayer("Thanks, Kolodion.", 567)
					it.chatNpc("That's what I'm here for.", KOLODION, 567)
				}
			}
		}
	}
	
	@Suspendable fun can_I_fight_here(it: Script) {
		it.chatPlayer("Can I fight here?", 554)
		it.chatNpc("My arena is open to any high level wizard, but this is<br>no game. Many wizards fall in this arena, never to rise<br>again. The strongest mages have been destroyed.", KOLODION, 594)
		it.chatNpc("If you're sure you want in?", KOLODION, 554)
		when (it.options("Yes indeedy.", "No I don't.")) {
			1 -> yes_indeedy(it)
			2 -> {
				it.chatPlayer("No I don't.", 614)
				it.chatNpc("Your loss.", KOLODION, 588)
			}
		}
	}
	
	@Suspendable fun whats_the_point(it: Script) {
		it.chatPlayer("What's the point of that?", 554)
		it.chatNpc("We learn how to use our magic to its fullest and how<br>to channel the forces of the cosmos into our world...", KOLODION, 589)
		it.chatNpc("But mainly, I just like blasting people into dust.", KOLODION, 567)
		when (it.options("Can I fight here?", "That's barbaric!")) {
			1 -> can_I_fight_here(it)
			2 -> thats_barbaric(it)
		}
	}
	
	@Suspendable fun thats_barbaric(it: Script) {
		it.chatPlayer("That's barbaric!", 571)
		it.chatNpc("Nope, it's magic. But I know what you mean. So do<br>you want to join us?", KOLODION, 568)
		when (it.options("Yes indeedy.", "No I don't.")) {
			1 -> yes_indeedy(it)
			2 -> {
				it.chatPlayer("No I don't.", 614)
				it.chatNpc("Your loss.", KOLODION, 588)
			}
		}
	}
	
	@Suspendable fun yes_indeedy(it: Script) {
		it.chatPlayer("Yes indeedy.", 567)
		it.chatNpc("Good, good. You have a healthy sense of competition.", KOLODION, 567)
		it.chatNpc("Remember, traveller - in my arena, hand-to-hand<br>combat is useless. Your strength will diminish as you<br>enter the arena, but the spells you can learn are<br>amongst the most powerful in all of OS-Scape.", KOLODION, 591)
		it.chatNpc("Before I can accept you in, we must duel.", KOLODION, 588)
		when (it.options("Okay, let's fight.", "No thanks.")) {
			1 -> okay_lets_fight(it)
			2 -> {
				it.chatPlayer("No thanks.", 588)
			}
		}
	}
	
	@Suspendable fun okay_lets_fight(it: Script) {
		it.chatPlayer("Okay, let's fight.", 588)
		it.chatNpc("I must first check that you are up to scratch.", KOLODION, 588)
		it.chatPlayer("You don't need to worry about that.", 588)
		it.chatNpc("Not just any magician can enter - only the most<br>powerful and most feared. Before you can use the<br> of this arena, you must prove yourself against<br>me.", KOLODION, 591)
		start_miniquest(it)
	}
	
	@Suspendable fun start_miniquest(it: Script) {
		//it.player().varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, 1)
		//it.player().putattrib(AttributeKey.MAGE_ARENA_PHASE, 1)
	}
}