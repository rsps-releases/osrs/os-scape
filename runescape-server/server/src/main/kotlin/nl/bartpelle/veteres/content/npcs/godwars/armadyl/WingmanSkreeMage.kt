package nl.bartpelle.veteres.content.npcs.godwars.armadyl

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 6/13/2016
 */

object WingmanSkreeMage {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target, 50) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10)) {
				if (EntityCombat.attackTimerReady(npc)) {
					// Attack the player
					attack(npc, target)
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc, target, 1201, 80, 25, 25, 12 * tileDist, 15, 10)
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		npc.animate(6955)
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC) // Cannot protect from this.
		else
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC) // Cannot protect from this.
	}
	
}