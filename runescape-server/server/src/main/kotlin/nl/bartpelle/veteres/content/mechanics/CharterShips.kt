package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.net.message.game.command.InterfaceVisibility
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.L10n

/**
 * Created by Situations on 4/11/2016.
 */

object CharterShips {
	
	enum class CharterPoint(val desination: Tile, val childId: Int) {
		BRIMHAVEN(Tile(2764, 3238, 1), 26),
		CATHERBY(Tile(2792, 3418, 1), 23),
		MUSA_POINT(Tile(2958, 3158, 1), 25),
		MOS_LEHARMLESS(Tile(3667, 2931, 1), 29),
		PORT_KHAZARD(Tile(2674, 3140, 1), 27),
		PORT_PHASMATYS(Tile(3706, 3503, 1), 22),
		PORT_SARIM(Tile(3038, 3188, 1), 28),
		SHIP_YARD(Tile(2997, 3032, 1), 24),
		PORT_TYRAS(Tile(2142, 3125, 1), 21),
		CRANDOR(Tile(3853, 3235, 0), 30),
		;
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Clicking the 'X' icon on the interface..
		r.onButton(95, 1) @Suspendable { charter(it, CharterPoint.PORT_TYRAS) } //Port Tyras
		r.onButton(95, 2) @Suspendable { charter(it, CharterPoint.PORT_PHASMATYS) } //Port Phasmatys
		r.onButton(95, 3) @Suspendable { charter(it, CharterPoint.CATHERBY) } //Catherby
		r.onButton(95, 4) @Suspendable { charter(it, CharterPoint.SHIP_YARD) } //Shipyard
		r.onButton(95, 5) @Suspendable { charter(it, CharterPoint.MUSA_POINT) } //Karamja
		r.onButton(95, 6) @Suspendable { charter(it, CharterPoint.BRIMHAVEN) } //Brimhaven
		r.onButton(95, 7) @Suspendable { charter(it, CharterPoint.PORT_KHAZARD) } //Port Khazard
		r.onButton(95, 8) @Suspendable { charter(it, CharterPoint.PORT_SARIM) } //Port Sarim
		r.onButton(95, 9) @Suspendable { charter(it, CharterPoint.MOS_LEHARMLESS) } //Mos le'Harmless
		r.onButton(95, 10) @Suspendable { charter(it, CharterPoint.CRANDOR) } //Crandor
		
		//Clicking the 'name' on the interface..
		r.onButton(95, 11) @Suspendable { charter(it, CharterPoint.PORT_TYRAS) } //Port Tyras
		r.onButton(95, 12) @Suspendable { charter(it, CharterPoint.PORT_PHASMATYS) } //Port Phasmatys
		r.onButton(95, 13) @Suspendable { charter(it, CharterPoint.CATHERBY) } //Catherby
		r.onButton(95, 14) @Suspendable { charter(it, CharterPoint.SHIP_YARD) } //Shipyard
		r.onButton(95, 15) @Suspendable { charter(it, CharterPoint.MUSA_POINT) } //Karamja
		r.onButton(95, 16) @Suspendable { charter(it, CharterPoint.BRIMHAVEN) } //Brimhaven
		r.onButton(95, 17) @Suspendable { charter(it, CharterPoint.PORT_KHAZARD) } //Port Khazard
		r.onButton(95, 18) @Suspendable { charter(it, CharterPoint.PORT_SARIM) } //Port Sarim
		r.onButton(95, 19) @Suspendable { charter(it, CharterPoint.MOS_LEHARMLESS) } //Mos le'Harmless
		r.onButton(95, 20) @Suspendable { charter(it, CharterPoint.CRANDOR) } //Crandor
	}
	
	@Suspendable fun sendOptionsInterface(it: Script) {
		setClosestSourcePoint(it)
		it.player().interfaces().sendMain(95)
		val source = it.player().attribOr<CharterPoint>(AttributeKey.CHARTER_SHIP_SOURCE_POINT, null) ?: return
		
		// Hide where we're already at so we can't travel there :P
		it.player().write(InterfaceVisibility(95, source.childId, true))
		
		// After dragon slayer, you can't return to Crandor.
		it.player().write(InterfaceVisibility(95, CharterPoint.CRANDOR.childId, true))
		
		// Unless you've done the quest I believe. We don't always have the mapdata for this anyway so let's keep this off limits.
		it.player().write(InterfaceVisibility(95, CharterPoint.MOS_LEHARMLESS.childId, true))
		
		// Some options are disabled like travelling to the same place you're already at and Quest restrictions
		when (source) {
			CharterPoint.PORT_SARIM -> {
				// Dunno why but yeah. I guess you have to use the karamja ship people specifically, not the charter ship people.
				it.player().write(InterfaceVisibility(95, CharterPoint.MUSA_POINT.childId, true))
			}
		}
	}
	
	@JvmStatic fun setClosestSourcePoint(it: Script) {
		var dist = 100
		val player = it.player()
		var closest: CharterPoint? = null
		CharterPoint.values().forEach { point ->
			if (player.tile().distance(point.desination) < dist) {
				dist = player.tile().distance(point.desination)
				closest = point
			}
		}
		player.debug("Updated closest charter ship source point to: " + closest)
		player.putattrib(AttributeKey.CHARTER_SHIP_SOURCE_POINT, closest)
	}
	
	@Suspendable fun charter(it: Script, desination: CharterPoint) {
		it.player().interfaces().closeMain()
		val sourcePoint: CharterPoint = it.player().attribOr(AttributeKey.CHARTER_SHIP_SOURCE_POINT, null) ?: return
		val name = L10n.formatChatMessage(desination.name).replace("_", " ")
		val cost = costFor(sourcePoint, desination)
		if (cost == 0) {
			it.messagebox("You can't travel there.")
			sendOptionsInterface(it)
			return
		}
		it.messagebox("To sail to $name from here will cost you $cost.<br>Are you sure you want to pay that?")
		when (it.options("Ok", "Choose again", "No")) {
			1 -> travel(it, cost, name, desination)
			2 -> sendOptionsInterface(it)
		}
	}
	
	// Yes this is ugly as fuck, however it's a embedded switch such as 9x9 dimension table
	// http://2007.runescape.wikia.com/wiki/Charter_ship
	private fun costFor(sourcePoint: CharterPoint, desination: CharterPoint): Int {
		return when (sourcePoint) {
			CharterPoint.BRIMHAVEN -> when (desination) {
				CharterPoint.BRIMHAVEN -> 0 // Self
				CharterPoint.CATHERBY -> 480
				CharterPoint.MUSA_POINT -> 200
				CharterPoint.MOS_LEHARMLESS -> 725
				CharterPoint.PORT_KHAZARD -> 1600
				CharterPoint.PORT_PHASMATYS -> 2900
				CharterPoint.PORT_SARIM -> 1600
				CharterPoint.SHIP_YARD -> 400
				CharterPoint.PORT_TYRAS -> 3200
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.CATHERBY -> when (desination) {
				CharterPoint.BRIMHAVEN -> 480
				CharterPoint.CATHERBY -> 0 // Self
				CharterPoint.MUSA_POINT -> 480
				CharterPoint.MOS_LEHARMLESS -> 625
				CharterPoint.PORT_KHAZARD -> 1600
				CharterPoint.PORT_PHASMATYS -> 3500
				CharterPoint.PORT_SARIM -> 1000
				CharterPoint.SHIP_YARD -> 1600
				CharterPoint.PORT_TYRAS -> 3200
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.MUSA_POINT -> when (desination) {
				CharterPoint.BRIMHAVEN -> 200
				CharterPoint.CATHERBY -> 480
				CharterPoint.MUSA_POINT -> 0 // Self
				CharterPoint.MOS_LEHARMLESS -> 1025
				CharterPoint.PORT_KHAZARD -> 1600
				CharterPoint.PORT_PHASMATYS -> 2900
				CharterPoint.PORT_SARIM -> 0 // Quest search check.. free here?
				CharterPoint.SHIP_YARD -> 200
				CharterPoint.PORT_TYRAS -> 3200
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.MOS_LEHARMLESS -> when (desination) {
				CharterPoint.BRIMHAVEN -> 975
				CharterPoint.CATHERBY -> 1750
				CharterPoint.MUSA_POINT -> 225
				CharterPoint.MOS_LEHARMLESS -> 0 // Self
				CharterPoint.PORT_KHAZARD -> 1025
				CharterPoint.PORT_PHASMATYS -> 0 // Quest related?
				CharterPoint.PORT_SARIM -> 325
				CharterPoint.SHIP_YARD -> 225
				CharterPoint.PORT_TYRAS -> 1600
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.PORT_KHAZARD -> when (desination) {
				CharterPoint.BRIMHAVEN -> 1600
				CharterPoint.CATHERBY -> 1600
				CharterPoint.MUSA_POINT -> 400
				CharterPoint.MOS_LEHARMLESS -> 1025
				CharterPoint.PORT_KHAZARD -> 0 // Self
				CharterPoint.PORT_PHASMATYS -> 4100
				CharterPoint.PORT_SARIM -> 1280
				CharterPoint.SHIP_YARD -> 720
				CharterPoint.PORT_TYRAS -> 3200
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.PORT_PHASMATYS -> when (desination) {
				CharterPoint.BRIMHAVEN -> 2900
				CharterPoint.CATHERBY -> 3500
				CharterPoint.MUSA_POINT -> 2900
				CharterPoint.MOS_LEHARMLESS -> 0 // Quest related?
				CharterPoint.PORT_KHAZARD -> 4100
				CharterPoint.PORT_PHASMATYS -> 0
				CharterPoint.PORT_SARIM -> 1300
				CharterPoint.SHIP_YARD -> 0 // Quest related?
				CharterPoint.PORT_TYRAS -> 3200
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.PORT_SARIM -> when (desination) {
				CharterPoint.BRIMHAVEN -> 1600
				CharterPoint.CATHERBY -> 1000
				CharterPoint.MUSA_POINT -> 0 // N/A quest related
				CharterPoint.MOS_LEHARMLESS -> 325
				CharterPoint.PORT_KHAZARD -> 1280
				CharterPoint.PORT_PHASMATYS -> 1300
				CharterPoint.PORT_SARIM -> 0 // Self
				CharterPoint.SHIP_YARD -> 400
				CharterPoint.PORT_TYRAS -> 3200
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.SHIP_YARD -> when (desination) {
				CharterPoint.BRIMHAVEN -> 400
				CharterPoint.CATHERBY -> 1600
				CharterPoint.MUSA_POINT -> 200
				CharterPoint.MOS_LEHARMLESS -> 275
				CharterPoint.PORT_KHAZARD -> 1600
				CharterPoint.PORT_PHASMATYS -> 1100 // N/A quest related.. manually tested to get 1100
				CharterPoint.PORT_SARIM -> 400
				CharterPoint.SHIP_YARD -> 0 // n/A quest related
				CharterPoint.PORT_TYRAS -> 3200
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
			CharterPoint.PORT_TYRAS -> when (desination) {
				CharterPoint.BRIMHAVEN -> 3200
				CharterPoint.CATHERBY -> 3200
				CharterPoint.MUSA_POINT -> 3200
				CharterPoint.MOS_LEHARMLESS -> 1600
				CharterPoint.PORT_KHAZARD -> 3200
				CharterPoint.PORT_PHASMATYS -> 3200
				CharterPoint.PORT_SARIM -> 3200
				CharterPoint.SHIP_YARD -> 3200
				CharterPoint.PORT_TYRAS -> 0 // Self
				CharterPoint.CRANDOR -> 0 // Quest only (Dragon Slayer)
				else -> 100
			}
		// You cannot travel _from_ Crandor. You get there by crashing during the quest Dragon Slayer
			else -> 100
		}
	}
	
	@Suspendable fun travel(it: Script, cost: Int, name: String, destination: CharterPoint) {
		if (it.player().world().realm().isPVP || cost > 0) {
			it.player().lock()
			it.player().interfaces().sendMain(174)
			it.player().invokeScript(951)
			it.delay(5)
			it.player().teleport(destination.desination)
			it.player().message("You pay the fare and sail to $name.")
			it.player().invokeScript(948, 0, 0, 0, 255, 50)
			it.player().interfaces().sendMain(174)
			it.delay(2)
			it.player().interfaces().closeById(174)
			it.player().unlock()
		} else if (cost > 0) {
			it.player().interfaces().closeById(174)
			it.chatPlayer("I don't have the money for that.", 610)
		} else {
			it.player().interfaces().closeById(174)
			it.chatNpc("Apologies, this destination is unavailable.", it.targetNpc()!!.id())
		}
	}
}
