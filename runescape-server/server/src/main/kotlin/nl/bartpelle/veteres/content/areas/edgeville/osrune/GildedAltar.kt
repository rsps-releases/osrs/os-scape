package nl.bartpelle.veteres.content.areas.edgeville.osrune

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-06-20.
 */

object GildedAltar {
	
	val LIT = 13213
	val UNLIT = 13212
	val TINDER_BOX = 590
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(UNLIT, s@ @Suspendable {
			val player = it.player()
			val obj = it.interactionObject()
			val rank = player.donationTier()
			val donatorByIcon = player.calculateBaseIcon() >= 15 && player.calculateBaseIcon() <= 21
			
			//Is our player a donator?
			if (player.privilege() == Privilege.PLAYER && rank == DonationTier.NONE && !player.helper() && !donatorByIcon) {
				player.message("You need to be a donator to use this feature.")
				return@s
			}
			
			//Does our player have a tinderbox?
			if (!player.inventory().has(TINDER_BOX)) {
				it.message("You need a tinderbox to light the incense. ")
				return@s
			}
			
			//3...2...1.. GO!
			player.lock()
			player.animate(3687)
			player.message("You light the incense.")
			it.delay(1)
			player.unlock()
			
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val old = MapObj(obj.tile(), UNLIT, obj.type(), obj.rot())
				val spawned = MapObj(obj.tile(), LIT, obj.type(), obj.rot())
				// world.removeObj(old)
				world.spawnObj(spawned)
				it.delay(300)
				//world.removeObjSpawn(spawned)
				world.spawnObj(old)
			}
		})
		
		r.onObject(LIT, s@ @Suspendable {
			val player = it.player()
			val rank = player.donationTier()
			val donatorByIcon = player.calculateBaseIcon() >= 15 && player.calculateBaseIcon() <= 21
			
			//Is our player a donator?
			if (player.privilege() == Privilege.PLAYER && rank == DonationTier.NONE && !player.helper() && !donatorByIcon) {
				it.messagebox("You need to be a donator to use this feature.")
				return@s
			}
			
			it.messagebox("This incense is already lit! Please try again later.")
		})
	}
}
