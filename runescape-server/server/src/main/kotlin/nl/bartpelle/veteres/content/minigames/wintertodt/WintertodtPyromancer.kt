package nl.bartpelle.veteres.content.minigames.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 9/21/2016.
 */
object WintertodtPyromancer {
	
	val POTIONS = setOf(20699, 20700, 20701, 20702)
	
	@JvmField val hit: Function1<Script, Unit> = @Suspendable {
		if (it.npc().hp() <= 0) {
			it.npc().sync().shout("Arg, it got me!")
			it.npc().sync().transmog(7372)
			it.npc().animate(6295)
			it.npc().hp(14, 0)
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(7371) @Suspendable {
			heal(it)
		}
		
		r.onItemOnNpc(7371) @Suspendable {
			heal(it)
		}
	}
	
	@Suspendable fun heal(it: Script) {
		val npc = it.targetNpc()!!
		val brazier = WintertodtGame.state.allBraziers.find { x -> x.pyromancer == npc }
		
		if (brazier != null && !brazier.pyromancerAlive) {
			val firstMatch = it.player.inventory().findFirst(POTIONS)
			if (firstMatch.first() == -1) {
				it.messagebox("You need a rejuvination potion made from the bruma herb to do<br>that.")
			} else {
				val replacement = when (firstMatch.second().id()) {
					20702 -> 229 // Empty vial
					else -> firstMatch.second().id() + 1
				}
				
				it.player().inventory().set(firstMatch.first(), Item(replacement))
				npc.sync().transmog(7371) // Revert to normal form
				npc.animate(-1) // Reset animation
			}
		} else {
			it.chatNpc("No need! I'm fine thanks.", 7371, 567)
		}
	}
	
}