package nl.bartpelle.veteres.content.events.christmas.carolschristmas

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsedId
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-15.
 */

object CarolsChristmasObjects {
	
	val STEPS = 29919
	val PASSAGE = 29908
	val BELLOWS = 29905
	val ROUGH_BED = 29906
	val STORAGE_CHEST = 29896
	val LOCKED_STORAGE_CHEST = 29899
	val DEFLATED_BOUNCY_BALLS = 29872
	val UNFINISHED_MARIONETTE_CRATE = 29869
	val PAPER_HATS = 29870
	val PARTY_HAT_CRATE = 29871
	val BALL_CRATE = 29873
	val MARIONETTE_CRATE = 29868
	val BATTERED_TEDDY = 20830
	val BLUE_PAINT = 29904
	val RED_PAINT = 29903
	val GREEN_PAINT = 29902
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(STEPS, s@ @Suspendable {
			val player = it.player()
			val res = it.player.interfaces().resizable()
			
			if (CarolsChristmas.getStage(player) <= 3) {
				it.chatPlayer("I should speak to Holly before I wander into an<br>unknown dungeon.", 555)
				return@s
			} else {
				player.lock()
				it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
				it.player.invokeScript(951)
				it.delay(2)
				player.teleport(3171, 5326)
				it.player.invokeScript(948, 0, 0, 0, 255, 50)
				it.message("You climb down the old steps into the Christmas Factory.")
				player.unlock()
			}
		})
		
		r.onObject(PASSAGE) @Suspendable {
			val player = it.player()
			val res = it.player.interfaces().resizable()
			
			player.lock()
			it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
			it.player.invokeScript(951)
			it.delay(2)
			player.teleport(1233, 3568)
			it.player.invokeScript(948, 0, 0, 0, 255, 50)
			it.message("You climb up the old steps to the summit of Mount Quidamortem.")
			player.unlock()
		}
		
		r.onObject(BELLOWS, s@ @Suspendable {
			val player = it.player()
			
			for (ball in intArrayOf(20808, 20810, 20812)) {
				if (player.inventory().has(ball)) {
					if (player.inventory().remove(Item(ball, 1), true).success()) {
						player.inventory().add(Item(ball - 6, 1), true)
						player.animate(896)
						it.itemBox("You carefully operate the bellows to inflate the ball.", ball - 6)
						return@s
					}
				}
			}
		})
		
		r.onItemOnObject(BLUE_PAINT) @Suspendable {
			val item = it.itemUsedId()
			val player = it.player()
			
			when (item) {
				20814 -> {
					if (player.inventory().remove(Item(20814, 1), false).success()) {
						player.inventory().add(Item(20816, 1), true)
						player.animate(896)
						it.itemBox("You dip the marionette in the blue paint.", 20816)
					}
				}
				20822 -> {
					if (player.inventory().remove(Item(20822, 1), false).success()) {
						player.inventory().add(Item(20828, 1), true)
						player.animate(896)
						it.itemBox("You dip the paper hat in the blue paint.", 20828)
					}
				}
				else -> it.player().message("Nothing interesting happens.")
			}
		}
		
		r.onItemOnObject(RED_PAINT) @Suspendable {
			val item = it.itemUsedId()
			val player = it.player()
			
			when (item) {
				20814 -> {
					if (player.inventory().remove(Item(20814, 1), false).success()) {
						player.inventory().add(Item(20820, 1), true)
						player.animate(896)
						it.itemBox("You dip the marionette in the red paint.", 20820)
					}
				}
				20822 -> {
					if (player.inventory().remove(Item(20822, 1), false).success()) {
						player.inventory().add(Item(20824, 1), true)
						player.animate(896)
						it.itemBox("You dip the paper hat in the red paint.", 20824)
					}
				}
				else -> it.player().message("Nothing interesting happens.")
			}
		}
		
		r.onItemOnObject(GREEN_PAINT) @Suspendable {
			val item = it.itemUsedId()
			val player = it.player()
			
			when (item) {
				20814 -> {
					if (player.inventory().remove(Item(20814, 1), false).success()) {
						player.inventory().add(Item(20818, 1), true)
						player.animate(896)
						it.itemBox("You dip the marionette in the green paint.", 20818)
					}
				}
				20822 -> {
					if (player.inventory().remove(Item(20822, 1), false).success()) {
						player.inventory().add(Item(20826, 1), true)
						player.animate(896)
						it.itemBox("You dip the paper hat in the green paint.", 20826)
					}
				}
				else -> it.player().message("Nothing interesting happens.")
			}
		}
		
		r.onObject(ROUGH_BED) @Suspendable {
			if (CarolsChristmas.getStage(it.player()) == 22) {
				if (it.optionsTitled("Take and wear the sheets?", "Yes", "No") == 1) {
					it.player().looks().transmog(3008)
					it.player().looks().render(1639, 1639, 1640, 1640, 1639, 1639, 1639)
					it.messagebox("You throw the sheet over yourself and start to itch. But at least you<br>look slightly ghostly, if a little grubby.")
				}
			} else {
				it.chatPlayer("Eww! Lice. Looks like these workers must be really<br>desperate to sleep here.", 611)
			}
		}
		
		r.onObject(LOCKED_STORAGE_CHEST) @Suspendable {
			it.message("This chest is locked.")
		}
		
		r.onObject(STORAGE_CHEST) @Suspendable {
			if (CarolsChristmas.getStage(it.player()) > 16) {
				it.message("You rummage around in Carol's chest but find nothing else of interest.")
			} else if (CarolsChristmas.getStage(it.player()) != 15) {
				it.messagebox("Carol would be quite angry to find you rooting around in her<br>personal belongings.")
			} else {
				if (it.player().inventory().has(BATTERED_TEDDY) || it.player().bank().has(BATTERED_TEDDY)) {
					it.message("You've already found Carol's Teddy.")
				} else {
					if (it.player().inventory().add(Item(BATTERED_TEDDY, 1), false).success()) {
						it.itemBox("You rummage around in Carol's chest and find an old<br>battered teddy bear among paperwork and other trollish<br>items.", BATTERED_TEDDY)
					}
				}
			}
		}
		
		r.onObject(DEFLATED_BOUNCY_BALLS, s@ @Suspendable {
			if (CarolsChristmas.getStage(it.player()) == 17) {
				when (it.optionsTitled("Which colour would you like to take?", "Red", "Green", "Blue")) {
					1 -> {
						if (it.player().inventory().has(20808)) {
							it.message("You already have a deflated red bouncy ball.")
							return@s
						} else if (it.player().inventory().add(Item(20808, 1), false).success()) {
							it.itemBox("You take a deflated red ball.", 20808)
						}
					}
					2 -> {
						if (it.player().inventory().has(20810)) {
							it.message("You already have a deflated green bouncy ball.")
							return@s
						} else if (it.player().inventory().add(Item(20810, 1), false).success()) {
							it.itemBox("You take a deflated green ball.", 20810)
						}
					}
					3 -> {
						if (it.player().inventory().has(20812)) {
							it.message("You already have a deflated blue bouncy ball.")
							return@s
						} else if (it.player().inventory().add(Item(20812, 1), false).success()) {
							it.itemBox("You take a deflated blue ball.", 20812)
						}
					}
					
				}
			} else {
				it.doubleItemBox("It's a pile of coloured, deflated bouncy balls.", 20808, 20806)
			}
		})
		
		r.onObject(UNFINISHED_MARIONETTE_CRATE, s@ @Suspendable {
			if (CarolsChristmas.getStage(it.player()) == 17) {
				if (it.player().inventory().has(20814)) {
					it.message("You already have an unpainted marionette.")
					return@s
				} else if (it.player().inventory().add(Item(20814, 1), false).success()) {
					it.itemBox("You take an unpainted marionette.", 20814)
				}
			} else {
				it.itemBox("It's a crate of unpainted marionettes.", 20814)
			}
		})
		
		r.onObject(PAPER_HATS, s@ @Suspendable {
			if (CarolsChristmas.getStage(it.player()) == 17) {
				if (it.player().inventory().has(20822)) {
					it.message("You already have a blank paper hat.")
					return@s
				} else if (it.player().inventory().add(Item(20822, 1), false).success()) {
					it.itemBox("You take a piece of shaped paper.", 20822)
				}
			} else {
				it.itemBox("It's a pile of blank paper hats.", 20822)
			}
		})
		
		r.onObject(PARTY_HAT_CRATE) @Suspendable {
			it.chatNpc("No stealing the presents!", 7325, 614)
		}
		
		r.onObject(MARIONETTE_CRATE) @Suspendable {
			it.chatNpc("No stealing the presents!", 7325, 614)
		}
		
		r.onObject(BALL_CRATE) @Suspendable {
			it.chatNpc("No stealing the presents!", 7325, 614)
		}
		
		
	}
}