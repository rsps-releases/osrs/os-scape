package nl.bartpelle.veteres.content.mechanics.cutscene

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.CameraType
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.CameraMovement
import nl.bartpelle.veteres.net.message.game.command.InvokeScript


/**
 * Created by Situations on 2017-01-03.
 */

object Cutscene {
	
	enum class Direction() { NORTH, SOUTH, EAST, WEST }
	
	fun fadeToBlack(player: Player) {
		player.interfaces().sendMain(174)
		player.write(InvokeScript(951))
	}
	
	fun resetCamera(player: Player) {
		player.write(CameraMovement())
	}
	
	fun moveCameraToPlayer(player: Player) {
		player.write(CameraMovement(player.tile(), CameraType.POSITION))
	}
	
	fun moveCameraToNpc(player: Player, npc: Npc) {
		player.write(CameraMovement(npc.tile(), CameraType.POSITION))
	}
	
	fun moveCameraToTile(player: Player, tile: Tile) {
		player.write(CameraMovement(tile, CameraType.POSITION))
	}
	
	fun moveCameraFromPlayer(player: Player, amtToMove: Int, direction: Direction) {
		val x = player.tile().x
		val z = player.tile().z
		
		when (direction) {
			Direction.NORTH -> moveCameraToTile(player, Tile(x, z + amtToMove))
			Direction.SOUTH -> moveCameraToTile(player, Tile(x, z - amtToMove))
			Direction.EAST -> moveCameraToTile(player, Tile(x - amtToMove, z))
			Direction.WEST -> moveCameraToTile(player, Tile(x + amtToMove, z))
		}
	}
	
	fun rotateCameraToPlayer(player: Player) {
		player.write(CameraMovement(player.tile(), CameraType.ROTATION))
	}
	
	fun rotateCameraToNpc(player: Player, npc: Npc) {
		player.write(CameraMovement(npc.tile(), CameraType.ROTATION))
	}
	
	fun rotateCameraToTile(player: Player, tile: Tile) {
		player.write(CameraMovement(tile, CameraType.ROTATION))
	}
	
	fun rotateCameraFromPlayer(player: Player, amtToMove: Int, direction: Direction) {
		val x = player.tile().x
		val z = player.tile().z
		
		when (direction) {
			Direction.NORTH -> rotateCameraToTile(player, Tile(x, z + amtToMove))
			Direction.SOUTH -> rotateCameraToTile(player, Tile(x, z - amtToMove))
			Direction.EAST -> rotateCameraToTile(player, Tile(x - amtToMove, z))
			Direction.WEST -> rotateCameraToTile(player, Tile(x + amtToMove, z))
		}
	}
	
	fun shakeCamera(player: Player, intensity: Int) {
		player.write(CameraMovement(intensity))
	}
	
	fun stopCameraShake(player: Player) {
		player.write(CameraMovement())
	}
	
	fun start(player: Player, fadeToBlack: Boolean) {
		if (!watchingCutscene(player)) {
			player.lock()
			player.putattrib(AttributeKey.CUTSCENE_IN_PROGESS, true)
		} else {
			player.debug("Player is already watching a cutscene.")
		}
	}
	
	fun finish(player: Player, fadeToBlack: Boolean) {
		if (watchingCutscene(player)) {
			player.putattrib(AttributeKey.CUTSCENE_IN_PROGESS, false)
			player.unlock()
		} else {
			player.debug("Player doesn't appear to be watching a cutscene.")
		}
	}
	
	private fun watchingCutscene(player: Player): Boolean {
		return player.attribOr(AttributeKey.CUTSCENE_IN_PROGESS, false)
	}
}