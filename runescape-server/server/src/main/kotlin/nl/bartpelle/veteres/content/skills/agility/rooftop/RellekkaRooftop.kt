package nl.bartpelle.veteres.content.skills.agility.rooftop

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.skills.agility.UnlockAgilityPet
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 5/19/2016.
 */
object RellekkaRooftop {
	
	val MARK_SPOTS = arrayOf(Tile(2624, 3675, 3),
			Tile(2620, 3665, 3),
			Tile(2627, 3652, 3),
			Tile(2642, 3650, 3),
			Tile(2646, 3658, 3),
			Tile(2658, 3675, 3))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Wall climb
		r.onObject(11391, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().skills().level(Skills.AGILITY) >= 80) {
				it.player().lock()
				it.player().faceTile(it.player().tile() + Tile(0, -1))
				it.delay(1)
				it.player().faceTile(it.player().tile() + Tile(0, -1))
				it.player().animate(828, 15)
				it.delay(2)
				it.player().teleport(2626, 3676, 3)
				it.player().animate(-1)
				it.addXp(Skills.AGILITY, 20.0)
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 42, 80)
				it.player().unlock()
			} else {
				it.message("You need at least 80 Agility to attempt this course.")
			}
		})
		
		// Gap leap
		r.onObject(11392, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() + Tile(0, -1))
			it.delay(1)
			it.player().animate(1995, 15)
			it.delay(1)
			it.player().sound(1936)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(0, 0, 0, -4, 8, 50, FaceDirection.SOUTH))
			it.delay(2)
			it.player().teleport(2622, 3668, 3)
			it.delay(1)
			it.addXp(Skills.AGILITY, 30.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 42, 80)
			it.player().unlock()
		})
		
		// Tightrope
		r.onObject(11393, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 3)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.player().pathQueue().interpolate(2626, 3654, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().step(2627, 3654, PathQueue.StepType.FORCED_WALK)
			it.waitForTile(Tile(2626, 3654))
			it.player().looks().resetRender()
			it.delay(1)
			it.addXp(Skills.AGILITY, 40.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 42, 80)
			it.player().unlock()
		})
		
		// Gap jump + tightrope
		r.onObject(11395, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().sound(2468, 20, 1)
			it.player().forceMove(ForceMovement(0, 0, 0, 3, 25, 30, FaceDirection.NORTH))
			it.delay(1)
			it.player().teleport(2629, 3658, 3)
			it.player().sound(2451, 2, 10)
			it.player().animate(752)
			it.player().looks().render(755, 755, 754, 754, 754, 754, -1)
			it.player().faceTile(it.player().tile() + Tile(1, 0))
			it.delay(1)
			it.player().pathQueue().interpolate(2635, 3658, PathQueue.StepType.FORCED_WALK)
			it.delay(6)
			it.player().sound(2495, 0, 4)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.player().pathQueue().interpolate(2639, 3654, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().step(2639, 3653, PathQueue.StepType.FORCED_WALK)
			it.delay(6)
			it.addXp(Skills.AGILITY, 85.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 42, 80)
			it.player().looks().resetRender()
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11396, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().animate(1995, 15)
			it.delay(1)
			it.player().sound(1936)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(0, 0, 0, 4, 8, 50, FaceDirection.NORTH))
			it.delay(2)
			it.player().teleport(2643, 3657, 3)
			it.addXp(Skills.AGILITY, 25.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 42, 80)
			it.player().unlock()
		})
		
		// Tightrope
		r.onObject(11397, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 6)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.player().pathQueue().step(2647, 3663, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().interpolate(2654, 3670, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().step(2655, 3670, PathQueue.StepType.FORCED_WALK)
			it.waitForTile(Tile(2654, 3670))
			it.player().looks().resetRender()
			it.delay(1)
			it.addXp(Skills.AGILITY, 105.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 42, 80)
			it.player().unlock()
		})
		
		// Jump fish pile
		r.onObject(11404, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().animate(2586, 15)
			it.player().sound(2462, 15)
			it.delay(1)
			it.player().animate(2588)
			it.player().teleport(2653, 3676, 0)
			it.delay(1)
			it.addXp(Skills.AGILITY, 475.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 42, 80)
			it.player().pathQueue().step(Direction.West)
			it.delay(1)
			it.player().unlock()
			
			// Woo! A pet!
			val odds = (12000.00 * it.player().mode().skillPetMod()).toInt()
			if (it.player().world().rollDie(odds, 1)) {
				UnlockAgilityPet.unlockGiantSquirrel(it.player())
			}
		})
	}
	
}