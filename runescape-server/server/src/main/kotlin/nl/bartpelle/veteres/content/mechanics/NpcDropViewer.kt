package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import com.google.common.collect.Lists
import droptables.ScalarLootTable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.ClientScriptUtils
import nl.bartpelle.veteres.util.RSFormatter
import java.lang.ref.WeakReference
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

/**
 * @author Mack
 */
object NpcDropViewer {
    
    private const val ENABLED = true
    const val DISPLAY_SEARCH_RESULTS_STAGE = 1
    private const val DISPLAY_TABLE_INFO_STAGE = 2
    private const val ERROR_STAGE = 3
    const val INTERFACE_ID = 310
    
    @JvmStatic
    @ScriptMain
    fun register(sr: ScriptRepository) {
        if (ENABLED) {
            sr.onObject(24452, @Suspendable {
                display(it)
            })
            sr.onInterfaceClose(INTERFACE_ID, @Suspendable {
                clear(it)
                it.player().interfaces().closeMain()
                it.player().invokeScript(299, 1, 1)
            })
            sr.onButton(INTERFACE_ID, 4, @Suspendable {
                when (it.player().attribOr<Int>(AttributeKey.LAST_INTER_310_USE_ID, -1)) {
                    1 -> handleInteraction(it)
                    2 -> UnifiedSettingsViewer.clicked(it)
                    else -> it.message("Nothing interesting happens.")
                }

            })
        }
    }
    
    @Suspendable
    fun display(it: Script) {
        it.player().write(InterfaceText(1337, 0, "Os-Scape Bestiary - Loot Table Lookup"))
        clear(it)
        
        val player = it.player()
        val resultMap = mutableMapOf<String, NpcDefinition>()
        
        if (!player.interfaces().visible(INTERFACE_ID)) {
            player.interfaces().sendMain(INTERFACE_ID)
        }
        
        updateInterfaceHeader(player, "The OSS Bestiary provides a service for players to view drop tables for a specific monster, if available. " +
                "Enter the name of the monster you wish to display.")

        player.putattrib(AttributeKey.LAST_INTER_310_USE_ID, 1)
        val userInput: String = it.inputString("Enter an NPC name to lookup:")
        
        for (id in 0..player.world().definitions().total(NpcDefinition::class.java)) {
            val def = it.player().world().definitions().get(NpcDefinition::class.java, id)
            
            def ?: continue
            if (def.name.toLowerCase().contains(userInput.toLowerCase()) && !resultMap.keys.contains(def.name.toLowerCase())) {
                val npc: WeakReference<Npc> = WeakReference(Npc(def.id, it.player().world(), it.player().tile()))
                if (def.combatlevel < 1) continue
                if (npc.get()?.combatInfo() == null) continue
    
                resultMap.put(def.name.toLowerCase(), def)
            }
        }
        
        when (resultMap.size) {
            0 -> {
                val errorMsg = addRow("<col=FF6262>No monster found from the user search '<col=FFFFFF>$userInput</col><col=FF6262>'.</col>") + addRow("<col=FF6262>Click here to try another search.</col>", true)
    
                displayError(it, errorMsg)
            }
            1 -> {
                displayLootTableInfo(resultMap.values.toTypedArray()[0], it, userInput)
            }
            else -> {
                displayNpcResults(resultMap, it, userInput)
            }
        }
    }
    
    @Suspendable
    private fun displayNpcResults(resultMap: MutableMap<String, NpcDefinition>, it: Script, userSearch: String) {
        var query = ""
        for (def in resultMap.values) {
            val npc: WeakReference<Npc> = WeakReference(Npc(def.id, it.player().world(), it.player().tile()))
            val maxHit = npc.get()?.combatInfo()?.maxhit ?: 0
            val aggro = npc.get()?.combatInfo()?.aggressive ?: false
    
            query += addRow("${def.name} (Cmb - ${def.combatlevel})")
            query += addRow("Max hit: $maxHit, Aggressive: ${if (aggro) "Yes" else "No"}", true)
        }
        
        //Add on the search button to the end of the list.
        query += addRow("<u><img=23><col=DF890F>Click here to submit another search.</col></u", true)
        
        //Cache the query and stage to reference for with interaction.
        it.player().putattrib(AttributeKey.BESTIARY_RESULTS, resultMap)
        setStage(it.player(), DISPLAY_SEARCH_RESULTS_STAGE)

        updateInterfaceHeader(it.player(), addRow("Displaying all monsters containing the search: <col=df780f>${RSFormatter.formatDisplayname(userSearch)}</col>") + addRow("Click on a result below to expand their drop table."))
        refresh(it, query)
    }
    
    @Suspendable
    private fun displayLootTableInfo(def: NpcDefinition, it: Script, userSearch: String) {
        val lootTable = if (ScalarLootTable.forNPC(def.id) == null) Optional.empty() else Optional.of(ScalarLootTable.forNPC(def.id))
        val world = it.player().world()
        val decimal = DecimalFormat("##.#")
        var query = ""
        val tableItems = arrayListOf<DropTableEntry>()

        if (!lootTable.isPresent) {
            val errorMessage = addRow("<col=FF6262>A loot table for '<col=FFFFFF>${def.name}</col><col=FF6262>' could not be found in our system.</col>") +
                    addRow("<col=FF6262>Click here to try another search.</col>", true)
            
            displayError(it, errorMessage)
            return
        }
        
        if (Objects.nonNull(lootTable.get().guaranteed)) {
            Arrays.stream(lootTable.get().guaranteed).forEach({ tableItem ->
                tableItems.add(DropTableEntry(tableItem, tableItem.convert().unnote(world.definitions()).name(world), world.prices().bloodyMoney(tableItem.id), 100.0))
            })
        }
        if (Objects.nonNull(lootTable.get().tables)) {
            Arrays.stream(lootTable.get().tables).filter(Objects::nonNull).forEach({ table ->
                Arrays.stream(table.items).filter(Objects::nonNull).filter({ tableItem -> tableItem.points > 0 }).forEach({ tableItem ->
                    val itemInfo = getTableItemChanceInfo(tableItem, table)
                    val tableInfo = getTableChanceInfo(table, Lists.newArrayList(lootTable.get().tables.toList()))
                    val itemRarity = (itemInfo[0].toDouble() / itemInfo[1].toDouble())
                    val tableRarity = (tableInfo[0].toDouble() / tableInfo[1].toDouble())
                    val rarity = (itemRarity * tableRarity).times(100)
    
                    tableItems.add(DropTableEntry(tableItem, tableItem.convert().unnote(world.definitions()).name(world), world.prices().bloodyMoney(tableItem.id), rarity))
                })
            })
        }
        
        //Safety!
        if (tableItems.isNotEmpty()) {
            tableItems.stream().forEach({ pending ->
                val id = if (pending.tableItem.id == 13307) 13316 else pending.tableItem.id
                val amt = pending.tableItem.amount
                val hash: Long = ((id.toLong()) shl 32) or amt.toLong()

                val dropTableType = when (pending.tableItemRarity) {
                    100.0 -> "<col=AFEEEE>Always</col>"
                    in 50..90 -> "<col=56E156>Common</col>"
                    in 9..49 -> "<col=FFED4C>Uncommon</col>"
                    in 3..8 -> "<col=FF6262>Rare</col>"
                    else -> "<col=8B0000>Very Rare</col>"
                }
                val stylizedMarketValue = when (pending.marketValue) {
                    in 0 until 100000 -> "<col=ffff00>${NumberFormat.getNumberInstance().format(pending.marketValue)}</col>"
                    in 100000 until 10000000 -> "<col=ffffff>${NumberFormat.getNumberInstance().format(pending.marketValue)}</col>"
                    else -> "<col=00ff80>${NumberFormat.getNumberInstance().format(pending.marketValue)}</col>"
                }

                query += addRow(pending.tableItemName + " <mdl=$hash>")
                query += addRow("<mdl=${(13310L) shl(32) or(0)}> $stylizedMarketValue <col=df780f>BM ea.             </col>")
                query += addRow("$dropTableType <col=df780f>(${decimal.format(pending.tableItemRarity)} % chance)</col>", true)
            })
        }
        
        //Add on the search button to the end of the list.
        query += addRow("<u><img=23><col=DF890F>Click here to submit another search.</col></u>", true)
        
        //Cache the search results with the key bound to the player to reference throughout this script.
        it.player().putattrib(AttributeKey.BESTIARY_RESULTS, tableItems)
        setStage(it.player(), DISPLAY_TABLE_INFO_STAGE)
        
        updateInterfaceHeader(it.player(), "Displaying loot table items for: <col=df780f>${RSFormatter.formatDisplayname(def.name)}</col><br>" +
                "Click on an item below to open it directly in the Grand Exchange.")
        refresh(it, query)
    }
    
    @Suspendable
    private fun handleInteraction(it: Script) {
        val screenId = it.player().attribOr<Int>(AttributeKey.BESTIARY_SCREEN_STAGE, -1)
        val clickIndex = it.player().attrib<Int>(AttributeKey.BUTTON_SLOT)
        
        if (screenId == -1) {
            clear(it)
            it.player().interfaces().closeMain()
            return
        }
        
        if (screenId == DISPLAY_SEARCH_RESULTS_STAGE) {
            val results: MutableMap<String, NpcDefinition> = it.player().attribOr<MutableMap<String, NpcDefinition>>(AttributeKey.BESTIARY_RESULTS, mutableMapOf<String, NpcDefinition>())

            //Safety check to prevent erroring.
            if (clickIndex > results.size) return
            
            //Check for clicking on 'search again' button.
            if (clickIndex == results.size) {
                display(it)
                return
            }
            
            displayLootTableInfo(results.values.toTypedArray()[clickIndex], it, results.keys.toTypedArray()[clickIndex])
        } else if (screenId == DISPLAY_TABLE_INFO_STAGE) {
            promptExchangeOption(it, clickIndex)
        } else {
            display(it)
        }
    }
    
    @Suspendable
    private fun promptExchangeOption(it: Script, clickIndex: Int) {
        val player = it.player()
        val tableResults = player.attribOr<ArrayList<DropTableEntry>>(AttributeKey.BESTIARY_RESULTS, emptyList<DropTableEntry>())

        //Safety check to prevent erroring.
        if (clickIndex > tableResults.size) return
        
        //Check for clicking on 'search again' button.
        if (clickIndex == tableResults.size) {
            display(it)
            return
        }

        val tableItem = Item(tableResults[clickIndex].tableItem.id)
        if (!tableItem.unnote(it.player().world()).definition(player.world()).grandexchange || tableItem.realPrice(player.world()) == 0) {
            it.messagebox("The item '${tableResults[clickIndex].tableItemName}' cannot be sold on the Grand Exchange!")
            return
        }
        if (clickIndex > -1 && tableResults.isNotEmpty()) {
            when (it.optionsTitled("Open the Grand Exchange?", "Yes, I'd like to open the Grand Exchange to view this item.", "No, don't open the Grange Exchange.")) {
                1 -> {
                    player.grandExchange().display()
                    player.grandExchange().showBuyOffer(tableResults[clickIndex].tableItem.id)
                }
            }
        }
    }
    
    /**
     * Sends an error message to the interface.
     */
    private fun displayError(it: Script, errorMessage: String) {
        updateInterfaceHeader(it.player(), "Refer to the error message below. If you believe this is a bug, please report this issue to a staff member.")
        it.player().putattrib(AttributeKey.BESTIARY_SCREEN_STAGE, ERROR_STAGE)
        refresh(it, errorMessage)
    }
    
    /**
     * Refreshes the interface sending the appropriate widgets.
     */
    fun refresh(it: Script, query: String) {
        val size = (if (query.contains(ClientScriptUtils.CS2_STR_ARRAY_TERMINATOR)) query.split(ClientScriptUtils.CS2_STR_ARRAY_TERMINATOR).size else 0)
        it.player().invokeScript(627, size - 1, size - 1, query)
    }
    
    /**
     * Returns an array with the weight of the specified loot table we are calcing along with the sum
     * of weights from all tables within the definition.
     */
    fun getTableChanceInfo(num: ScalarLootTable, denom: ArrayList<ScalarLootTable>): Array<Int> {
        var totalTablePoints = 0
        for (table in denom) {
            totalTablePoints += table.points
        }
        return arrayOf(num.points, totalTablePoints)
    }
    
    /**
     * Returns an array with the weight of the table item we are calc'ing along with the sum of weights of all items present in
     * the respective table.
     */
    fun getTableItemChanceInfo(tableItem: ScalarLootTable.TableItem, table: ScalarLootTable): Array<Int> {
        var totalItemPoints = 0
        for (item in table.items) {
            totalItemPoints += item.points
        }
        return (arrayOf(tableItem.points, totalItemPoints))
    }
    
    /**
     * Adds a line break tag to the existing query string to indicate to the cs2 we wish
     * to add a new row. If flagged true, the terminate boolean will add the string terminator for cs2.
     */
    fun addRow(query: String, terminate: Boolean = false): String {
        return query + (if (terminate) ClientScriptUtils.CS2_STR_ARRAY_TERMINATOR else "<br>")
    }
    
    /**
     * Updates the header on the interface.
     */
    fun updateInterfaceHeader(player: Player, text: String) {
        player.interfaces().text(INTERFACE_ID, 3, text)
    }
    
    /**
     * Sends a empty query to clear the interface of all widgets to ensure proper displaying or for proper disposal.
     */
    fun clear(it: Script) {
        refresh(it, "")
        it.player().clearattrib(AttributeKey.BESTIARY_RESULTS)
        it.player().clearattrib(AttributeKey.BESTIARY_SCREEN_STAGE)
    }
    
    /**
     * Sets the stage attribute for the player with the specified stage.
     */
    fun setStage(player: Player, stage: Int) {
        player.putattrib(AttributeKey.BESTIARY_SCREEN_STAGE, stage)
    }
    
    data class DropTableEntry(var tableItem: ScalarLootTable.TableItem, val tableItemName: String, val marketValue: Int, var tableItemRarity: Double)
}