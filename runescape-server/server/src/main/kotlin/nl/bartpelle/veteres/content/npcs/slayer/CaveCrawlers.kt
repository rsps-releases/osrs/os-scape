package nl.bartpelle.veteres.content.npcs.slayer

import nl.bartpelle.skript.Script

/**
 * Created by Bart on 11/18/2015.
 */
object CaveCrawlers {
	
	@JvmField public val healscript: Function1<Script, Unit> = {
		if (it.npc().hp() > 0) {
			it.npc().heal(1)
		}
	}
	
}