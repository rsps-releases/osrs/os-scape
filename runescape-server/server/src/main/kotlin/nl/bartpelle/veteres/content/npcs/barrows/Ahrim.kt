package nl.bartpelle.veteres.content.npcs.barrows

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 8/28/2015.
 */

object Ahrim {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && npc.hp() > 0 && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10)) {
				if (EntityCombat.attackTimerReady(npc)) {
					val tileDist = npc.tile().transform(3, 3, 0).distance(target.tile())
					npc.world().spawnProjectile(npc, target, 156, 30, 30, 20, 12 * tileDist, 14, 5)
					var delay = Math.max(Math.max(1, 20 + tileDist * 12 / 30), 4)
					if (delay > 2)
						delay = 2
					
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
						target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC)
					} else {
						target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC) // Uh-oh, that's a miss.
					}
					
					// .. and go into sleep mode.
					npc.animate(npc.attackAnimation())
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
}