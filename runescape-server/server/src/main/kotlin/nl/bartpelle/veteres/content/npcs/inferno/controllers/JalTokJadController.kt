package nl.bartpelle.veteres.content.npcs.inferno.controllers

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.content.npcs.fightcaves.YtHurkot
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import java.util.*

/**
 * Created by Mack on 8/3/2017.
 */
class JalTokJadController(jad: Npc, session: InfernoSession, offset: Int): InfernoNpcController {
	
	/**
	 * The jad npc representing this controller.
	 */
	val jad = jad
	
	/**
	 * The active inferno session.
	 */
	val session = session
	
	/**
	 * The offset value we use to manipulate tile spawns.
	 */
	var offset = offset
	
	/**
	 * The offset we use to modify the z-pos.
	 */
	var zPosOff = 0
	
	/**
	 * The offset we use to modify the x-pos.
	 */
	var xPos = 0
	
	/**
	 * The collection of healers for the jad spawn.
	 */
	var healers: ArrayList<Npc> = ArrayList()
	
	/**
	 * The collection of appropriate jad spawns.
	 */
	val spawns = arrayListOf(Tile(session.area!!.center().x, session.area!!.center().z), Tile(session.area!!.center().x - 8, session.area!!.center().z), Tile(session.area!!.center().x - 2, session.area!!.center().z - 9))
	
	override fun onSpawn() {
		jad.tile(spawns[offset - 1])
		jad.attack(session.player())
	}
	
	override fun tick(it: Script) {
		var spawnedHealers: Boolean = jad.attribOr(AttributeKey.JAD_SPAWNED_HEALERS, false)
		if (jad.hp() <= 175 && !spawnedHealers) {
			for (i in 1..3) {
				spawnHealers()
			}
			jad.putattrib(AttributeKey.JAD_SPAWNED_HEALERS, true)
		}
	}
	
	fun spawnHealers() {
		val area = InfernoContext.tileSpawns(session.player())
		val healer = Npc(InfernoContext.YT_HURKOT, jad.world(), area[jad.world().random(area.size - 1)])
		
		healer.walkRadius(100)
		healer.respawns(false)
		healer.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 100)
		
		if (InfernoContext.get(session.player()) != null) {
			InfernoContext.get(session.player())!!.activeNpcs.add(healer)
		}
		
		healers.add(healer)
		
		session.player().world().registerNpc(healer)
		healer.executeScript(YtHurkot(jad).healJad)
	}
	
	override fun triggerPlayerKilledDeath() {
		if (!healers.isEmpty()) {
			for (remaining in healers) {
				
				session.player().world().unregisterNpc(remaining)
				
				//Remove the remaining healers from the session collection as well so the player can advance the wave.
				if (session.activeNpcs.contains(remaining)) {
					session.activeNpcs.remove(remaining)
				}
			}
		}
	}
	
	override fun onDamage() {
		if (!InfernoContext.inSession(session.player())) {
			session.player().message("I don't think I should be doing this...")
			return
		}
		super.onDamage()
	}
}