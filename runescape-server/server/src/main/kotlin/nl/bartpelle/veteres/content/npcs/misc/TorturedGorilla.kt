package nl.bartpelle.veteres.content.npcs.misc

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 6/11/2016.
 */
object TorturedGorilla {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		var mage = it.npc().world().rollDie(2, 1)
		
		EntityCombat.boilerplateWrap(it) @Suspendable { npc, target ->
			// Do we switch?
			if (npc.world().rollDie(10, 1)) {
				mage = !mage
			}
			
			val tileDist = npc.tile().distance(target.tile())
			val delay = Math.max(1, (26 + (tileDist * 4)) / 30)
			
			if (mage) {
				npc.animate(7238)
				
				npc.world().spawnProjectile(npc.tile(), target, 1304, 0, 0, 30, 20 + (4 * tileDist), 10, 10)
				if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
					target.hit(npc, EntityCombat.randomHit(npc)).graphic(Graphic(1305, 70, 0)).delay(delay)
				} else {
					target.hit(npc, 0).graphic(Graphic(1305, 70)).delay(delay)
				}
			} else {
				npc.animate(7240)
				
				npc.world().spawnProjectile(npc.tile(), target, 1302, 25, 25, 40, 20 + (4 * tileDist), 10, 10)
				if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
					target.hit(npc, EntityCombat.randomHit(npc)).graphic(Graphic(92, 124, 0)).delay(2)
				} else {
					target.hit(npc, 0).graphic(1303).delay(2)
				}
			}
		}
	}
	
}