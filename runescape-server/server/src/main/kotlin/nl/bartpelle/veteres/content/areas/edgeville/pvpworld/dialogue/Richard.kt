package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 3/10/2016.
 */

object Richard {
	
	val RICHARD = 2200
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(RICHARD) {
			//Team capes
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(9).display(it.player())
			} else {
				it.messagebox("This shop is only accessible from the PvP world.")
			}
		}
		r.onNpcOption2(RICHARD) {
			//Team capes
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(9).display(it.player())
			} else {
				it.messagebox("This shop is only accessible from the PvP world.")
			}
		}
	}
}
