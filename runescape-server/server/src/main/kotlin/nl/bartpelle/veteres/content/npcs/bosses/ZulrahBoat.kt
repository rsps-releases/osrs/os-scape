package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.blankmessagebox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Tuple

/**
 * Created by Situations on 1/31/2016.
 */

object ZulrahBoat {
	
	val sacrificial_boat = 10068 //Where the player starts the boss instance
	val player_start_location = Tile(2268, 3068, 0) //Where the teleport the player
	val princess_zul_gwenwynig = 2033 //Princess Zul-Gwenynig NPC ID
	val snakeling = 2045 //Spanking NPC ID
	val green_zulrah = 2042 //Green Zulrah NPC ID
	val crimson_zulrah = 2043 //Crimson Zulrah NPC ID
	val turquoise_zulrah = 2044 //Turquoise Zulrah NPC IDm
	
	
	//North, south, east, and west spawns for Zulrah
	val zulrah_north_spawn = Tile(2268, 3074, 3)
	val zulrah_south_spawn = Tile(2267, 3064, 3)
	val zulrah_west_spawn = Tile(2258, 3072, 3)
	val zulrah_east_spawn = Tile(2277, 3072, 3)
	
	val zulrah_spawn = Tile(2266, 3072, 0) //Zulrah spawn tile
	
	
	//Handle the initial spawn and first wave of Zulrah
	@Suspendable fun initilize_zulrah(it: Script, player: Player) {
		val zulrah = Npc(green_zulrah, it.player().world(), zulrah_spawn)
		
		zulrah.putattrib(AttributeKey.OWNING_PLAYER, Tuple(it.player().id(), it.player())) //Assign Zulrah to the player
		
		it.player().world().registerNpc(zulrah) //Spawn Zulrah into the game
		zulrah.faceTile(zulrah.tile().transform(2, 0, 0)) //Ensure Zulrah is facing the north tile
		zulrah.animate(5073, 1) //Create the spawn animation for Zulrah
		it.delay(2)
	}
	
	
	//Support for the sacrifical boat.
	@JvmStatic @ScriptMain fun register_boat(r: ScriptRepository) {
		r.onObject(sacrificial_boat) @Suspendable {
			zulrahTravel(it)
		}
	}
	
	@JvmStatic fun testTravel(player: Player) {
		player.world().server().scriptExecutor().executeScript(player, @Suspendable { s ->
			zulrahTravel(s)
		})
	}
	
	@Suspendable @JvmStatic fun zulrahTravel(it: Script) {
		val player = it.player()
		if (it.optionsTitled("Return to Zulrah's shrine?", "Yes.", "No.") == 1) {
			player.lock()
			it.blankmessagebox("The princess rows you to Zulrah's shrine, then hurridly paddles away.")
			//FADE TO BLACK HERE.
			it.delay(2)
			/*
			player.interfaces().setting(600, 1, 1, 31, 15007744) // 31 enables clicking client side
			player.invokeScript(143, 280, 1780)
			player.teleport(player_start_location)
			//FADE FROM BLACK HERE
			it.messagebox("The princess rows you to Zulrah's shrine, then hurridly paddles away.")
			it.delay(2)
			player.interfaces().setting(600, 1, 1, 31, 15007744)
			player.invokeScript(143, 280, 1780)
*/
			val map = player.world().allocator().allocate(64, 64, Tile(2210, 3056)).get()
			val corner = Tile(2240, 3048)
			map.set(0, 0, corner, corner.transform(64, 64), 0, true)
			map.setMulti(true)
			map.setIdentifier(InstancedMapIdentifier.ZULRAH)
			map.setCreatedFor(player)
			
			player.teleport(map.center().transform(-4, -12, 0))
			//player.write(DisplayInstancedMap(map, player))
			val zulrahNpc = Npc(2042, player.world(), map.center().transform(-6, -8, 0))
			zulrahNpc.respawns(false)
			zulrahNpc.putattrib(AttributeKey.OWNING_PLAYER, Tuple(player.id(), player))
			player.world().registerNpc(zulrahNpc)
			zulrahNpc.executeScript @Suspendable { s ->
				zulrahNpc.face(null)
				s.delay(1)
				zulrahNpc.faceTile(zulrahNpc.tile() - Tile(0, 10))
				zulrahNpc.animate(5073) // up
			}
			player.unlock()
			it.messagebox("Welcome to Zulrah's shrine.")
			
			player.world().executeScript @Suspendable { s ->
				while (player.interfaces().visible(229)) // Wait for chatbox close before starting combat
					it.delay(1)
				zulrahNpc.attack(player)
			}
		}
	}
	
	//Support for Princess Zul-Gwenyunig dialogue.
	@JvmStatic @ScriptMain fun princess_zul_gwenwynig(r: ScriptRepository) {
		r.onNpcOption1(princess_zul_gwenwynig) @Suspendable {
			it.chatNpc("You left the shrine. Did Zulrah reject your sacrifice?", princess_zul_gwenwynig)
			when (it.options("I killed Zulrah.", "Remind me how this ritual works.", "I'm off.")) {
				1 -> {
					it.chatPlayer("I killed Zulrah.")
					it.chatNpc("Is that what you think? Board the boat again, and I am sure you will find Zulrah has overcome whatever pitiful attacks you used against him.", princess_zul_gwenwynig)
				}
				2 -> {
					it.chatPlayer("Remind me how this ritual works.")
					it.chatNpc("Once I have brought you to the shrine, I will leave you there as an offering to Zulrah. You cannot use the boat to return.", princess_zul_gwenwynig)
					it.chatNpc("You can use your magical powers to flee from Zulrah's grasp, though that would defeat the point of the sacrifice, and you'll have to try again.", princess_zul_gwenwynig)
					it.chatNpc("When Zulrah inevitably kills you, I may be able to retrieve your dropped possessions. If so, I will hold them here.", princess_zul_gwenwynig)
					it.chatNpc("Now board the boat and prepare to be sacrificed.", princess_zul_gwenwynig)
				}
				3 -> {
					it.chatPlayer("I'm off.")
				}
			}
		}
		r.onNpcOption2(princess_zul_gwenwynig) @Suspendable {
			it.chatNpc("I'm afraid I don't have anything for your to collect. If I had any of your items, but you died before collecting them from me, I'd lose them.", princess_zul_gwenwynig)
		}
	}
}