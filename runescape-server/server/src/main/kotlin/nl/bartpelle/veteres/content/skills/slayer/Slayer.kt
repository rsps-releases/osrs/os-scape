package nl.bartpelle.veteres.content.skills.slayer

import co.paralleluniverse.fibers.Suspendable
import com.google.gson.Gson
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.items.HerbSack
import nl.bartpelle.veteres.fs.EnumDefinition
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

/**
 * Created by Bart on 11/10/2015.
 */
object Slayer {
	
	var masters: Array<SlayerMaster> = arrayOf()
	
	var TURAEL_ID = 1
	var MAZCHNA_ID = 2
	var VANNAKA_ID = 3
	var CHAELDAR_ID = 4
	var NIEVE_ID = 5
	var DURADEL_ID = 6
	var KRYSTILIA_ID = 7
	
	fun master(npc: Int): SlayerMaster? {
		for (master in masters) {
			if (master.npcId == npc) {
				return master
			}
		}
		
		return null
	}
	
	fun loadMasters() {
		// Load all masters from the json file
		masters = Gson().fromJson(String(Files.readAllBytes(Paths.get("data", "list", "slayermasters.json"))), Array<SlayerMaster>::class.java)
		
		// Verify integrity, make sure matches are made.
		masters.forEach { master ->
			master.defs.forEach { taskdef ->
				if (taskdef != null) {
					if (SlayerCreature.lookup(taskdef.creatureUid) == null) {
						throw RuntimeException("could not load slayer task def ${taskdef.creatureUid}: could not resolve uid; master: ${master.npcId}")
					}
				}
			}
		}
	}
	
	@JvmStatic fun creatureMatches(player: Player, id: Int): Boolean {
		val task = SlayerCreature.lookup(player.attribOr(AttributeKey.SLAYER_TASK_ID, 0))
		return task != null && task.matches(id)
	}
	
	fun taskName(player: Player, id: Int): String {
		var name = player.world().definitions().get(EnumDefinition::class.java, 693).getString(id)
		
		//Cheaphax the Bosses enum value for W2 just to be anal-specific.
		if (player.world().realm().isPVP && name.equals("Bosses", true)) {
			return "Wilderness Bosses"
		}
		
		return name
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		loadMasters()
		
		r.onButton(426, 23) @Suspendable {
			val player = it.player()
			val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
			val clickedId: Int = player.attrib(AttributeKey.ITEM_ID)
			if (option == 10) {
				player.message(player.world().examineRepository().item(clickedId))
			} else if (option == 2) {
				// buy 1
				buy(it, player, clickedId, 1)
			} else if (option == 3) {
				// buy 5
				buy(it, player, clickedId, 5)
			} else if (option == 4) {
				// buy 10
				buy(it, player, clickedId, 10)
			}
			//player.debug("item %d", clickedId)
		}
		
}
	
	@Suspendable fun buy(it: Script, player: Player, item: Int, count: Int) {
		var price: Int = Integer.MAX_VALUE
		var reward: Item = Item(item)
		when (item) {
			11866 -> price = 75
			11875 -> {
				price = 35; reward = Item(item, 250)
			}
			4160 -> {
				price = 35; reward = Item(item, 250)
			}
			11865 -> price = 1200
			13116 -> price = 1000
			HerbSack.HERBSACK -> price = 750
			12020 -> price = 750 // Gem bag
		}
		if (item == HerbSack.HERBSACK) {
			// There is no "drop" option on the sack so no worries there. (you destory it)
			if (player.inventory().has(HerbSack.HERBSACK) || player.bank().has(HerbSack.HERBSACK)) {
				player.message("You can only have one herb sack at a time. Multiple sacks do <col=FF0000>NOT</col> provide multiple storage.")
				return
			}
		}
		if (item == 12020) { // Gem bag
			if (player.hasItem(12020)) {
				player.message("You can only own one gem bag at a time.")
				return
			}
		}
		val startCount = count
		var tobuy = count
		while (tobuy > 0) {
			var points = player.varps().varbit(Varbit.SLAYER_POINTS)
			
			if (points - price >= 0) {
				// Only one slayer helm can be bought at a time.
				if (reward.id() == 11865) {
					val silver_helm = player.inventory().count(11864)
					val red_helm = player.inventory().count(19639)
					val green_helm = player.inventory().count(19643)
					val black_helm = player.inventory().count(19647)
					val helm_count = player.inventory().count(11864, 19639, 19643, 19647)
					
					// Only one type of any helm on you.
					if (helm_count == 1 || (helm_count - silver_helm == 0) || (helm_count - green_helm == 0) || (helm_count - black_helm == 0) || (helm_count - red_helm == 0)) {
						for (SLAYER_HELM in intArrayOf(11864, 19639, 19643, 19647)) {
							if (player.inventory().hasAny(SLAYER_HELM)) {
								if (player.inventory().remove(Item(SLAYER_HELM), false).success()) {
									player.varps().varbit(Varbit.SLAYER_POINTS, points - price)
									
									when (SLAYER_HELM) {
										11864 -> player.inventory().add(Item(11865), true)
										19639 -> player.inventory().add(Item(19641), true)
										19643 -> player.inventory().add(Item(19645), true)
										19647 -> player.inventory().add(Item(19649), true)
									}
									break // Only buy one
								}
							}
						}
						break
					} else {
						// Show options if you're carry more than 1 type of helm that can be imbued.
						val helm_options = ArrayList<Int>()
						for (SLAYER_HELM in intArrayOf(11864, 19639, 19643, 19647)) {
							if (player.inventory().hasAny(SLAYER_HELM)) {
								if (!helm_options.contains(SLAYER_HELM)) {
									helm_options.add(SLAYER_HELM)
								}
							}
						}
						if (helm_options.size > 1) {
							val opts: ArrayList<String?> = arrayListOf(null, null, null, null, null)
							val op_ids: ArrayList<Int?> = arrayListOf(null, null, null, null, null)
							var x = 0
							helm_options.forEach { h ->
								op_ids[x] = h
								opts[x++] = "Imbue the ${Item(h).name(player.world())}."
							}
							val choice = it.optionsNullable("Which helm would you like to imbue?", opts[0], opts[1], opts[2], opts[3], opts[4]) - 1
							val helm_id = op_ids[choice]!!
							if (player.inventory().remove(Item(helm_id), false).success()) {
								player.varps().varbit(Varbit.SLAYER_POINTS, points - price)
								when (helm_id) {
									11864 -> player.inventory().add(Item(11865), true)
									19639 -> player.inventory().add(Item(19641), true)
									19643 -> player.inventory().add(Item(19645), true)
									19647 -> player.inventory().add(Item(19649), true)
								}
							}
						}
						break
					}
				} else {
					if (!hasSpaceFor(player, reward)) {
						player.message("You don't have enough room to buy more.")
						break
					}
					
					player.varps().varbit(Varbit.SLAYER_POINTS, points - price)
					player.inventory().add(reward, true)
					tobuy--
				}
			} else if (tobuy != startCount) {
				player.message("You've run out of points! Bought " + ((startCount - tobuy) * reward.amount()) + "/" + (startCount * reward.amount()) + " x " + reward.name(player.world()))
				break
			} else {
				player.message("You don't have enough points to buy this.")
				break
			}
		}
	}
	
	private fun hasSpaceFor(player: Player, item: Item): Boolean {
		val def = player.world().definitions().get(ItemDefinition::class.java, item.id())
		val noted = def.notelink > 0
		val count: Int = player.inventory().count(item.id())
		if (noted || def.stackable()) {
			if (count > 0 && (count + item.amount()) <= Integer.MAX_VALUE) { // Already have a stack
				return true
			}
			return player.inventory().freeSlots() > 0
		} else {
			return player.inventory().freeSlots() >= item.amount()
		}
	}
	
	@JvmStatic fun displayCurrentAssignment(player: Player) {
		val name = taskName(player, player.attribOr(AttributeKey.SLAYER_TASK_ID, 0))
		val num: Int = player.attribOr(AttributeKey.SLAYER_TASK_AMT, 0)
		
		if (num == 0)
			player.interfaces().text(426, 33, "No current assignment")
		else
			player.interfaces().text(426, 33, "$num x $name")
	}
	
	fun tipFor(task: SlayerCreature): String {
		when (task.uid) {
			2 -> return "Goblins can be found at the Goblin Village, Stronghold of Security, and Lumbridge."
			3 -> return "Rats can be found inside the Taverley and Edgeville dungeon, and around Lumbridge."
			4 -> return "Spiders can found inside the Taverley and Edgeville dungeon, and are commonly found " +
					"around Lumbridge, Varrock, and Falador. Antipoison is recommend (only if fighting the" +
					"poisonous variants)"
			5 -> return "Birds are commonly found at the Lumbridge or Falador chicken coops."
			6 -> return "Cows can be slaughtered on the Lumbridge or Falador grazing land."
			7 -> return "Scorpions can be slayed inside the Dwarven mines or around the Karamja Volcano."
			8 -> return "Bats can be killed inside the Taverley dungeon or outside of the Mage Arena. Be" +
					" aware of PKers outside the Mage Arena though, as that is a very popular PVP area."
			9 -> return "Wolves can be killed on the White Wolf Mountain or at the Wilderness Agility" +
					"course. Be aware of PVPers as this is a popular PVP area."
			10 -> return "Zombies can be slayed inside the Edgeville Dungeon. "
			11 -> return "Skeletons can be found inside the Edgeville, Taverley, or Waterfall dungeon."
			12 -> return "Ghosts can be killed inside the Taverley dungeon."
			13 -> return "Bears can be found around the Ardougne mines or low-to-mid level Wilderness." +
					" (Careful for PK'ers!)"
			14 -> return "Hill Giants can be found inside the Edgeville and Taverley dungeon, or northeast" +
					"of the Chaos Temple inside the Wilderness."
			15 -> return "Ice Giants can be found inside the Asgarnian Ice Caves or by the level 44 Obelisk" +
					"inside the wilderness. Beware of PKers though, as this is a very popular PVP area."
			16 -> return "Fire Giants can be found inside the Stronghold Slayer, Brimhaven, and Waterfall dungeon."
			19 -> return "Ice Warriors can be found inside the Asgarnian Ice Caves or by the level 44 Obelisk" +
					"inside the wilderness. Beware of PKers though, as this is a very popular PVP area."
			21 -> return "Hobgoblins can be found inside the Asgarnian Ice and Godwars dungeon."
			24 -> return "Green Dragons can be found North of Goblin Village in 13 Wilderness, between" +
					"the Ruins and Graveyard of Shadows in level 24 Wilderness, west of the Bone Yard," +
					"between the Lava Maze and Hobgoblin mine, and north east of the Chaos Temple. Beware" +
					"of the PKers here though, as they're extremely popular PVP areas. A Anti-dragon shield," +
					" Dragonfire shield or Antifire potion and a good Stab (Abyssal whip or Dragon scimitar " +
					"works too!) or Ranged weapon is recommended."
			25 -> return "Blue Dragons can be found inside the Taverley Dungeon. A Anti-dragon shield," +
					" Dragonfire shield or Antifire potion and a good Stab (Abyssal whip or Dragon scimitar " +
					"works too!) or Ranged weapon is recommended."
			27 -> return "Black Dragons can be found inside the Taverley, or Brimhaven dungeon. A Anti-dragon shield," +
					" Dragonfire shield or Antifire potion and a good Stab (Abyssal whip or Dragon scimitar " +
					"works too!) or Ranged weapon is recommended."
			28 -> return "Lesser demons can be found scattered throughout the wild or near a dungeon close to mage bank."
			29 -> return "Greater Demons can be found inside the Strongold Slayer Cave, at the Brimhaven Dungeon," +
					" or inside the wilderness at the Demonic Ruins. Be aware of PVPers at the Demonic Ruins, though, " +
					"as it is a very popular PVP area."
			30 -> return "Black Demons can be found at the Taverley, Edgeville, or Brimhaven Dungeon. Good gear and/or" +
					" protection from melee, with several prayer potions is highly recommended."
			31 -> return "Hell Hounds can be killed inside the Stronghold Slayer cave, or at the Taverley Dungeon."
			35 -> return "Dagannoths can be killed at the Waterbirth Island."
			36 -> return "Turoths can be killed at the Fremennik Slayer Dungeon. Broad arrows/bolts with a strong " +
					"range weapon is recommended."
			37 -> return "Cave Crawlers can be found at the Fremennik Slayer Dungeon. Antipoision and a weapon using" +
					" slash attacks are extremely recommended."
			39 -> return "Crawling Hands are located at the Slayer Tower."
			41 -> return "Aberrant spectres are found at the Stronghold Slayer Cave, or at the Slayer Tower. A Nose peg" +
					"or Slayer helmet is extremely recommended."
			42 -> return "Abyssal Demons can be found at the Stronghold Slayer cave, or at the Slayer Tower. Good gear " +
					"and weapon is highly recommended when fighting these monsters."
			44 -> return "Cockatrices can be found at the Fremennik Slayer Dungeon. A mirror shield is highly recommended."
			45 -> return "Kurasks can be found at the Fremennik Slayer Dungeon. A leaf-bladed weapon or Broad arrows/bolts are" +
					"highly recommend when fighting these monsters."
			46 -> return "Gargoyles can be found at the Stronghold Slayer Cave or the Slayer Tower. A Rock Hammer is required" +
					"to kill these monsters."
			47 -> return "Pyrefiends can be found at the Fremennik Slayer, or Smoke Dungeon. Magic-resistant armour is recommended."
			48 -> return "Bloodvelds can be killed at the Stronghold Slayer Cave or Slayer Tower. Dragonhide armour and a decent" +
					"weapon is recommend when fighting these monsters."
			49 -> return "Dust Devils can be killed at the Smoke Dungeon."
			50 -> return "Jellies can be found at the Fremennik Slayer Dungeon. Magic-resistant armour is recommended."
			51 -> return "Rockslugs can be found at the Fremennik Slayer Dungeon. A bag of salt is required" +
					"to finish these monsters."
			52 -> return "Nechryaels can be found at the Stronghold Slayer Cave or Slayer Tower."
			58 -> return "Bronze Dragons can be found at the Stronghold Slayer Cave or Brimhaven Dungeon. A Anti-dragon shield," +
					" Dragonfire shield or Antifire potion and a good Stab (Abyssal whip or Dragon scimitar " +
					"works too!) or Fire Bold (or better) is recommended."
			59 -> return "Iron Dragons can be found at the Stronghold Slayer Cave or Brimhaven Dungeon. A Anti-dragon shield," +
					" Dragonfire shield or Antifire potion and a good Stab (Abyssal whip or Dragon scimitar " +
					"works too!) or Fire Bold (or better) is recommended."
			60 -> return "Steel Dragons can be found at the Stronghold Slayer Cave or Brimhaven Dungeon. A Anti-dragon shield," +
					" Dragonfire shield or Antifire potion and a good Stab (Abyssal whip or Dragon scimitar " +
					"works too!) or Fire Bold (or better) is recommended."
			66 -> return "Dark Beasts can be found inside the Mourner Tunnels. Good armour/weapon is recommended when fighting" +
					"these monsters."
			72 -> return "Skeletal Wyverns can be found in the Asgarnian Ice Dungeon. Protect from Range, Mirror Shields, and good" +
					"armour/weapon is suggested when fighting these monsters."
			75 -> return "Icefiends can be found inside the Godwars Dungeon."
			76 -> return "Minotaurs can be found inside the Godwars Dungeon or on the 1st level of the Stronghold of Security."
			77 -> return "Flesh Crawlers can be found on the 2nd level of the Stronghold of Security."
			79 -> return "Ankous can be found in the Stronghold Slayer Cave, on the 4th level of the Stronghold of Security, and at" +
					" the Forgotten Cemetery. Beware of PKers at the Forgotten Cemetary, though. As it's inside the wilderness."
			80 -> return "Cave Horrors can be found at Mos Le'Harmless Caves."
			91 -> return "Magic axes can be found in a hut east of mage bank. A lockpick is required to get in."
			92 -> return "Cave Krakens can be awaken at the Stronghold Slayer Cave. A Magic weapon is preferred, as Ranged his are heavily" +
					"reduced. You're unable to use Melee on these monsters."
			95 -> return "Smoke Devils can be found at the Stronghold Slayer Cave. A Slayer helm or facemask is suggested when" +
					"fighting these monsters."
			96 -> return "Tzhaar monsters can be found inside the Tzhaar Cave."
			98 -> return "A boss task in W2 counts any major wilderness boss you may encounter. Vet'ion, Chaos Fanatic, Callisto and so on are all valid."
			104 -> return "Lava dragons are a strong breed located north-east of black chinchompas. A form of anti-dragon shield is strongly recommended."
			106 -> return "Fossil Island Wyverns aren't located on Fossil Island in this world. They're near an altar close to the KBD entrance."
			else -> return "You don't appear to have a task!"
		}
	}
	
}

fun Interfaces.showSlayerRewards() {
	this.sendMain(426)
	this.setting(426, 8, 0, 45, 2)
	this.setting(426, 23, 0, 5, 1052)
}