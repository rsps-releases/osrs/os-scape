package nl.bartpelle.veteres.content.skills.magic

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.items.equipment.RunePouch
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * @author Jak
 * @date Jan 7 2018
 * POGCHAMP
 */
object CombinationRunes {

    const val LAVA_RUNE = 4699
    const val MUD_RUNE = 4698
    const val MIST_RUNE = 4695
    const val STEAM_RUNE = 4694
    const val DUST_RUNE = 4696
    const val SMOKE_RUNE = 4697

    const val AIR_RUNE = 556
    const val FIRE_RUNE = 554
    const val WATER_RUNE = 555
    const val EARTH_RUNE = 557

    class ComboRune(val combo: Int, val elementals: Array<Int>)

    val COMBO_RUNES = HashMap<Int, ComboRune>()

    @JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
        COMBO_RUNES.put(LAVA_RUNE, ComboRune(LAVA_RUNE, arrayOf(FIRE_RUNE, EARTH_RUNE)))
        COMBO_RUNES.put(MUD_RUNE, ComboRune(MUD_RUNE, arrayOf(EARTH_RUNE, WATER_RUNE)))
        COMBO_RUNES.put(MIST_RUNE, ComboRune(MIST_RUNE, arrayOf(WATER_RUNE, AIR_RUNE)))
        COMBO_RUNES.put(STEAM_RUNE, ComboRune(STEAM_RUNE, arrayOf(FIRE_RUNE, WATER_RUNE)))
        COMBO_RUNES.put(DUST_RUNE, ComboRune(DUST_RUNE, arrayOf(EARTH_RUNE, AIR_RUNE)))
        COMBO_RUNES.put(SMOKE_RUNE, ComboRune(SMOKE_RUNE, arrayOf(FIRE_RUNE, AIR_RUNE)))
    }

    fun numberOfAlternative(id: Int, available: Item): Int {
        when (id) {
            Staves.FIRE_RUNE -> if (available.id() in intArrayOf(LAVA_RUNE, STEAM_RUNE)) return available.amount()
            Staves.AIR_RUNE -> if (available.id() in intArrayOf(MIST_RUNE, DUST_RUNE, SMOKE_RUNE)) return available.amount()
            Staves.EARTH_RUNE -> if (available.id() in intArrayOf(LAVA_RUNE, MUD_RUNE, DUST_RUNE)) return available.amount()
            Staves.WATER_RUNE -> if (available.id() in intArrayOf(MUD_RUNE, STEAM_RUNE)) return available.amount()
        }
        return 0
    }

    fun numberOfAlternative(player: Player, id: Int): Int {
        return when (id) {
            Staves.FIRE_RUNE -> player.inventory().count(LAVA_RUNE, STEAM_RUNE)
            Staves.AIR_RUNE -> player.inventory().count(MIST_RUNE, DUST_RUNE, SMOKE_RUNE)
            Staves.EARTH_RUNE -> player.inventory().count(LAVA_RUNE, MUD_RUNE, DUST_RUNE)
            Staves.WATER_RUNE -> player.inventory().count(MUD_RUNE, STEAM_RUNE)
            else -> 0
        }
    }

    // Note: runepouch boolean will need to be changed to int/enum if >2 sources become available, currently only inventory + RP (staffs don't count - they're unlimited)
    data class ComboRuneSource(val id: Int, val amt: Int, val slot: Int, val runepouch: Boolean) {
        override fun toString(): String { return String.format("combo:${id}x$amt at $slot, rp:$runepouch") }
    }

    class ComboRuneDeduction(val remainingRequired: Array<Item>, val usedComboRunes: Array<ComboRuneSource>)

    @JvmStatic fun findComboRuneUses(player: Player, immutableReqs: Array<Item>): ComboRuneDeduction {

        // Container with the runes needed to cast a spell.
        val requiredContainer = ItemContainer(player.world(), immutableReqs.copyOf(), ItemContainer.Type.REGULAR)

        // Combo runes on our person, from the inventory
        val available = ItemContainer(player.world(), player.inventory().filter { i -> i != null && (i.id() in 4694 .. 4699) }.toTypedArray(), ItemContainer.Type.REGULAR)

        val usedRunes = mutableListOf<ComboRuneSource>()

        findCombosInContainer(requiredContainer, available, usedRunes)

        // Make sure we're carrying the rune pouch before taking into consideration its contents.
        if (player.inventory().has(12791)) {
            findCombosInContainer(requiredContainer, ItemContainer(player.world(), RunePouch.allRunesOf(player).filter { r -> r != null }.toTypedArray(), ItemContainer.Type.REGULAR), usedRunes, true)
        }

        /*val left = Arrays.toString(requiredContainer.items().filter { i -> i != null }.map { i -> i.name(player.world())+"x"+i.amount() }.toTypedArray())
        val used = Arrays.toString(usedRunes.map { r -> Item(r.id).name(player.world())+"x"+r.amt }.toTypedArray())
        val avail = Arrays.toString(available.items().filter { i -> i != null }.map { i -> i.name(player.world())+"x"+i.amount() }.toTypedArray())
        player.message("required: ${Arrays.toString(immutableReqs.map { i -> i.name(player.world())+"x"+i.amount() }.toTypedArray())}")
        player.message("left after combos: $left")
        player.message("combos used: $used".red())
        player.message("remaining available: $avail")*/
        return ComboRuneDeduction(requiredContainer.items(), usedRunes.toTypedArray())
    }

    private fun findCombosInContainer(requiredContainer: ItemContainer, available: ItemContainer, usedRunes: MutableList<ComboRuneSource>, runepouch: Boolean = false) {

        val comboIterator = available.items().iterator()

        // Loop all combo runes available to us, use their pairs to reduce required runes as we go
        lookup@ while (comboIterator.hasNext()) {
            val comboRune = comboIterator.next()
            // we have a combo rune, let's see if it can remove 1 or even 2 required runes
            val comboSet = combo(comboRune) ?: continue

            // find a combo match AND another required match of the same combo
            val ele1 = comboSet.elementals[0]
            val ele2 = comboSet.elementals[1]

            // There will only be 1 match of the elemental rune of THE CURRENT COMBO RUNE BEING CHECKED
            val matchEle1 = requiredContainer.items().toList().stream().filter { r2 -> r2 != null && r2.id() == ele1 }.findFirst()
            if (matchEle1.isPresent) {
                val matched2nd = requiredContainer.items().toList().stream().filter { r2 -> r2 != null && r2.id() == ele2 }.findFirst()
                if (matched2nd.isPresent) {
                    val amt = Math.min(matchEle1.get().amount(), matched2nd.get().amount())
                    requiredContainer.remove(Item(matchEle1.get().id(), amt), false)
                    requiredContainer.remove(Item(matched2nd.get().id(), amt), false)
                    usedRunes.add(ComboRuneSource(comboSet.combo, amt, available.slotOf(comboSet.combo), runepouch))
                    available.remove(Item(comboSet.combo, amt), false)
                    // Met all requirements (unlikely to happen but w.e)
                    if (requiredContainer.size() == 0)
                        break@lookup
                    // Next combo rune check.
                    continue
                }
            }
            val matchEle2 = requiredContainer.items().toList().stream().filter { r2 -> r2 != null && r2.id() == ele2 }.findFirst()
            if (matchEle2.isPresent) {
                val matched2nd = requiredContainer.items().toList().stream().filter { r2 -> r2 != null && r2.id() == ele1 }.findFirst()
                if (matched2nd.isPresent) {
                    val amt = Math.min(matchEle1.get().amount(), matched2nd.get().amount())
                    requiredContainer.remove(Item(matchEle2.get().id(), amt), false)
                    requiredContainer.remove(Item(matched2nd.get().id(), amt), false)
                    usedRunes.add(ComboRuneSource(comboSet.combo, amt, available.slotOf(comboSet.combo), runepouch))
                    available.remove(Item(comboSet.combo, amt), false)
                    if (requiredContainer.size() == 0)
                        break@lookup
                    continue
                }
            }
        }
    }

    private fun combo(i: Item): ComboRune? {
        return COMBO_RUNES[i.id()]
    }
}