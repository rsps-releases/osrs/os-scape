package nl.bartpelle.veteres.content.areas.dungeons.waterfall

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/10/2016.
 */

object WaterfallEntrance {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(2010) @Suspendable {
			it.delay(1)
			it.player().teleport(Tile(2575, 9861))
		}
	}
	
}

