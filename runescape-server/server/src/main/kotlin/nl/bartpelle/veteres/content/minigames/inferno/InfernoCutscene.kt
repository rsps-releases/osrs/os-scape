package nl.bartpelle.veteres.content.minigames.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.areas.tzhaar.TzhaarKetKeh
import nl.bartpelle.veteres.content.blankmessagebox
import nl.bartpelle.veteres.content.mechanics.cutscene.Cutscene
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.ShakeReset
import nl.bartpelle.veteres.net.message.game.command.ShakeScreen

/**
 * Created by Mack on 8/18/2017.
 */
object InfernoCutscene {
	
	/**
	 * The on screen shake data for the cut scene display.
	 */
	val shakeInfo = intArrayOf(4, 4, 4, 4)
	
	/**
	 * Begins the cutscene for The Inferno.
	 */
	fun start(player: Player) {
		
		player.lockNoDamage()
		Cutscene.fadeToBlack(player)
		
		player.world().server().scriptExecutor().executeScript(player, @Suspendable { s ->
			s.delay(3)
			s.player().teleport(Tile(InfernoContext.activeArea(s.player())!!.center().x,  InfernoContext.activeArea(s.player())!!.center().z + 12))
			s.delay(4)
			s.player().interfaces().closeById(174)
//			player.write(ShakeScreen(shakeInfo)) TODO
			s.blankmessagebox("Oh no! TzKal-Zuk's prison is breaking down. This not mean to have happened. There's nothing I can do for you now JalYt!")
			s.delay(2)
//			s.player().write(ShakeReset()) TODO
			s.player().interfaces().close(162, 550)
			s.player().unlock()
			
			//If they're still in the session, then we spawn.
			if (InfernoContext.inSession(player)) {
				InfernoContext.get(player)!!.currentWave().get().spawnWave(player, InfernoContext.get(player)!!)
			}
		})
	}
}