package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Jak on 10/12/2016.
 */
class DelayedLogoutRequest(val player: Player) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(it: Script) {
		var loops = 5
		// Give it 15 seconds for damage to be absorbed
		while (loops-- > 0) {
			player.sync().shout(" - server: user has crashed -")
			it.delay(5)
		}
		
		// immunity disabled
		//player.unlock()
		
		// we expect to be disconnected when our session closes
		//player.putattrib(AttributeKey.LOGOUT, true)
	}
	
	
}