package nl.bartpelle.veteres.content.skills.agility.rooftop

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.skills.agility.UnlockAgilityPet
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 3/19/2015.
 */
object FaladorRooftop {
	
	val MARK_SPOTS = arrayOf(Tile(3046, 3345, 3),
			Tile(3046, 3365, 3),
			Tile(3036, 3363, 3),
			Tile(3015, 3355, 3),
			Tile(3011, 3339, 3),
			Tile(3023, 3334, 3))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Wall climb
		r.onObject(10833, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().skills().xpLevel(Skills.AGILITY) < 50) {
				it.message("You need an Agility level of 50 to attempt this.")
			} else {
				it.player().lock()
				it.player().faceTile(it.player().tile() + Tile(0, 1))
				it.delay(1)
				it.player().animate(828, 15)
				it.delay(2)
				it.player().teleport(3036, 3342, 3)
				it.animate(-1)
				it.addXp(Skills.AGILITY, 8.0)
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
				it.player().unlock()
			}
		})
		
		// Tightrope
		r.onObject(10834, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 5)
			it.player().pathQueue().interpolate(3047, 3343, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().step(3047, 3344, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(3047, 3343))
			it.player().looks().resetRender()
			it.delay(1)
			it.addXp(Skills.AGILITY, 17.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Wall bricks
		r.onObject(10836, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			
			it.player().sound(2468, 20)
			it.player().animate(2583, 20)
			it.player().pathQueue().step(Direction.North, PathQueue.StepType.FORCED_RUN)
			it.player().pathQueue().step(Direction.North, PathQueue.StepType.FORCED_RUN)
			it.player().forceMove(ForceMovement(0, -2, 0, 0, 25, 30, FaceDirection.NORTH))
			it.delay(1)
			
			it.player().teleport(3050, 3351, 2)
			it.player().sound(2459, 35)
			it.player().animate(1118)
			it.delay(1)
			
			it.player().pathQueue().step(Direction.NorthEast, PathQueue.StepType.FORCED_WALK)
			it.player().forceMove(ForceMovement(0, -1, 0, 0, 13, 22, FaceDirection.WEST))
			it.delay(1)
			
			for (i in 1..3) {
				it.player().sound(2459, 35)
				it.player().animate(1118)
				it.player().pathQueue().step(Direction.North, PathQueue.StepType.FORCED_WALK)
				it.player().forceMove(ForceMovement(0, -1, 0, 0, 34, 52, FaceDirection.WEST))
				it.delay(2)
			}
			
			it.player().sound(2459, 35)
			it.player().animate(1120)
			it.player().pathQueue().step(3050, 3357, PathQueue.StepType.FORCED_RUN)
			it.player().forceMove(ForceMovement(1, -2, 0, 0, 34, 60, FaceDirection.WEST))
			it.delay(2)
			it.player().teleport(3050, 3357, 3)
			
			it.addXp(Skills.AGILITY, 45.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 50)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11161, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().animate(741)
			it.player().forceMove(ForceMovement(0, -3, 0, 0, 15, 30, FaceDirection.NORTH).baseTile(Tile(3048, 3361, 3)))
			it.delay(1)
			it.player().teleport(3048, 3361, 3)
			it.addXp(Skills.AGILITY, 20.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Gap jump 2
		r.onObject(11360, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().animate(741)
			it.player().forceMove(ForceMovement(4, 0, 0, 0, 15, 30, FaceDirection.WEST).baseTile(Tile(3041, 3361, 3)))
			it.delay(1)
			it.player().teleport(3041, 3361, 3)
			it.addXp(Skills.AGILITY, 20.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Tightrope
		r.onObject(11361, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 4)
			it.player().pathQueue().interpolate(3033, 3361, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().interpolate(3028, 3356, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().interpolate(3028, 3354, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(3028, 3355))
			it.player().looks().resetRender()
			it.delay(2)
			it.addXp(Skills.AGILITY, 45.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Tightrope
		r.onObject(11364, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().pathQueue().interpolate(3026, 3353, PathQueue.StepType.FORCED_WALK)
			it.delay(2)
			it.player().sound(1935, 5)
			it.player().animate(7134)
			it.player().forceMove(ForceMovement(5, 0, 0, 0, 0, 89, FaceDirection.WEST).baseTile(Tile(3021, 3353, 3)))
			it.delay(3)
			it.player().teleport(3021, 3353, 3)
			it.player().pathQueue().step(3020, 3353, PathQueue.StepType.FORCED_WALK)
			it.player().animate(-1)
			it.addXp(Skills.AGILITY, 40.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11365, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(0, 4, 0, 0, 15, 30, FaceDirection.SOUTH).baseTile(it.player().tile().transform(0, -4)))
			it.delay(1)
			it.player().teleport(it.player().tile().transform(0, -4))
			it.addXp(Skills.AGILITY, 25.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11366, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(2, 0, 0, 0, 15, 30, FaceDirection.WEST).baseTile(it.player().tile().transform(-2, 0)))
			it.delay(1)
			it.player().teleport(it.player().tile().transform(-2, 0))
			it.player().animate(-1)
			it.addXp(Skills.AGILITY, 10.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11367, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(0, 2, 0, 0, 15, 30, FaceDirection.SOUTH).baseTile(it.player().tile().transform(0, -2)))
			it.delay(1)
			it.player().teleport(it.player().tile().transform(0, -2))
			it.player().animate(-1)
			it.addXp(Skills.AGILITY, 10.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11368, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(0, 2, 0, 0, 15, 30, FaceDirection.SOUTH).baseTile(it.player().tile().transform(0, -2)))
			it.delay(1)
			it.player().teleport(it.player().tile().transform(0, -2))
			it.player().animate(-1)
			it.addXp(Skills.AGILITY, 10.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11370, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(-2, 0, 0, 0, 15, 30, FaceDirection.EAST).baseTile(it.player().tile().transform(2, 0)))
			it.delay(1)
			it.player().teleport(it.player().tile().transform(2, 0))
			it.player().animate(-1)
			it.addXp(Skills.AGILITY, 10.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 50, 50)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11371, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(-3, 0, 0, 0, 15, 30, FaceDirection.EAST).baseTile(it.player().tile().transform(3, 0)))
			it.delay(1)
			it.player().teleport(it.player().tile().transform(3, 0))
			it.player().animate(-1)
			it.delay(1)
			it.player().animate(2586, 15)
			it.player().sound(2462, 15)
			it.delay(1)
			it.player().teleport(3029, 3333, 0)
			it.addXp(Skills.AGILITY, 180.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 50)
			it.player().unlock()
			
			// Woo! A pet!
			val odds = (18000.00 * it.player().mode().skillPetMod()).toInt()
			if (it.player().world().rollDie(odds, 1)) {
				UnlockAgilityPet.unlockGiantSquirrel(it.player())
			}
			
		})
	}
	
}
