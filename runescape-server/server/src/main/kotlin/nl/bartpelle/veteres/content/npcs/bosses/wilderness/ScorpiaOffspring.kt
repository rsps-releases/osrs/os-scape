package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 3/26/2016.
 *
 * Scorpia's offspring, the smaller creatures around scorpia. Only attack when the player isn't poisoned.
 */
object ScorpiaOffspring {
	
	@JvmField val aggrocheck: Function1<Entity, Boolean> = { entity ->
		entity.varps().varp(Varp.HP_ORB_COLOR) <= 0
	}
	
}