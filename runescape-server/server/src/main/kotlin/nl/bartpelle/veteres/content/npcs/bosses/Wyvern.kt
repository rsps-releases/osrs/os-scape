package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 11/26/2015.
 */

object Wyvern {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.canAttackMelee(npc, target) && npc.world().rollDie(3, 1)) {
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
						target.hit(npc, EntityCombat.randomHit(npc))
					} else {
						target.hit(npc, 0) // Uh-oh, that's a miss.
					}
					npc.animate(npc.attackAnimation())
				} else if (npc.world().rollDie(3, 1)) {
					iceAttack(it, npc, target)
				} else {
					range(npc, target)
				}
				
				// .. and go into sleep mode.
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	private fun range(npc: Npc, target: Entity) {
		npc.animate(2989)
		npc.graphic(499)
		// Throw a magic projectile
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc, target, 500, 30, 30, 20, 12 * tileDist, 14, 5)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
			target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(CombatStyle.RANGE)
		} else {
			target.hit(npc, 0, 1) // Uh-oh, that's a miss.
		}
	}
	
	
	@Suspendable fun iceAttack(it: Script, npc: Npc, target: Entity) {
		val tileDist = npc.tile().distance(target.tile())
		npc.animate(2988)
		npc.graphic(501)
		target.message("You have been frozen!")
		target.freeze(6, npc)
		target.hit(npc, EntityCombat.randomHit(npc), 2).combatStyle(CombatStyle.GENERIC) // Cannot protect from this.
		it.delay(1)
		npc.world().spawnProjectile(npc, target, 502, 20, 5, 5, 12 * tileDist, 15, 10)
	}
}
