package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.dialogue.EmblemTrader
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.items.MaxCape
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 27/05/2016.
 */
object OtherMaxCapes {
	
	@JvmStatic val NORMAL_MAXCAPE: Int = 13280
	@JvmStatic val HOOD: Int = 13281
	@JvmStatic val FIRECAPE = 6570
	@JvmStatic val AVAS = 10499
	@JvmStatic val GUTHIX = 2413
	@JvmStatic val SARA = 2412
	@JvmStatic val ZAMMY = 2414
	@JvmStatic val ARDY_CAPE_4 = 13124
	@JvmStatic val INFERNAL = 21295
	const val ASSEMBLER = 22109
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		// Fusion
		for (src in intArrayOf(FIRECAPE, AVAS, SARA, GUTHIX, ZAMMY, ARDY_CAPE_4, INFERNAL, EmblemTrader.IMBUED_SARADOMIN_CAPE, EmblemTrader.IMBUED_ZAMORAK_CAPE, EmblemTrader.IMBUED_GUTHIX_CAPE,
				ASSEMBLER)) {
			r.onItemOnItem(NORMAL_MAXCAPE, src) @Suspendable {
				combine(it, src)
			}
			r.onItemOnItem(HOOD, src) @Suspendable {
				combine(it, src)
			}
		}
	}
	
	@Suspendable private fun combine(it: Script, src: Int) {
		val player = it.player()
		val name: String = Item(src).name(player.world())
		it.doubleItemBox("Are you sure you want to combine the Max Cape and a $name? This combines the stats together and cannot be undone." +
				" You <col=FF0000>will lose</col> your $name.", NORMAL_MAXCAPE, src)
		if (it.options("Proceed with infusion.", "Never mind.") == 1) {
			val hasItems = if (player.world().realm().isPVP) it.player().inventory().hasAll(NORMAL_MAXCAPE, src) else it.player().inventory().hasAll(NORMAL_MAXCAPE, HOOD, src)
			if (hasItems) {
				val combo = comboOf(src)
				if (combo.size == 2) { // Safety
					player.inventory().remove(Item(src), true)
					player.inventory().remove(Item(NORMAL_MAXCAPE), true)
					player.inventory().remove(Item(HOOD), true)
					player.inventory().add(Item(combo[0]), true)
					player.inventory().add(Item(combo[1]), true)
					val extra = if (!player.world().realm().isPVP) " and Hood" else ""
					it.doubleItemBox("You fuse the items together to produce a new Max Cape$extra.", combo[0], combo[1])
				}
			} else {
				it.messagebox("You need a Max Cape and the Max Hood to infuse these together.")
			}
		}
	}
	
	// Cape : hood
	private fun comboOf(i: Int): IntArray {
		when (i) {
			FIRECAPE -> return intArrayOf(13329, 13330)
			AVAS -> return intArrayOf(13337, 13338)
			SARA -> return intArrayOf(13331, 13332)
			GUTHIX -> return intArrayOf(13335, 13336)
			ZAMMY -> return intArrayOf(13333, 13334)
			ARDY_CAPE_4 -> return intArrayOf(20760, 20764)
			INFERNAL -> return intArrayOf(MaxCape.INFERNAL_MAX_CAPE, MaxCape.INFERNAL_MAX_HOOD)
			EmblemTrader.IMBUED_SARADOMIN_CAPE -> return intArrayOf(MaxCape.IMBUED_SARADOMIN_MAX_CAPE, MaxCape.IMBUED_SARADOMIN_MAX_HOOD)
			EmblemTrader.IMBUED_GUTHIX_CAPE -> return intArrayOf(MaxCape.IMBUED_GUTHIX_MAX_CAPE, MaxCape.IMBUED_GUTHIX_MAX_HOOD)
			EmblemTrader.IMBUED_ZAMORAK_CAPE -> return intArrayOf(MaxCape.IMBUED_ZAMORAK_MAX_CAPE, MaxCape.IMBUED_ZAMORAK_MAX_HOOD)
			ASSEMBLER -> return intArrayOf(MaxCape.ASSEMBLER_MAX_CAPE, MaxCape.ASSEMBLER_MAX_CAPE_HOOD)
		}
		return intArrayOf()
	}
}