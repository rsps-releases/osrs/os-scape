package nl.bartpelle.veteres.content.minigames.raids

import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */
class RaidParty(private var ownerId: Int, private val ownerName: String, private val world: World, private val chatChannel: ClanChat) {

    private var id = -1
    private var advertised = false
    private var timestamp = 0L
    private var status = -1
    private var members: MutableList<Player> = mutableListOf()

    var partySizePreference: Int = 0
        set(value) {
            field = if (value > 100) 100 else value
        }

    var combatLevelPreferece: Int = 0
        set(value) {
            field = if (value > 126) 126 else value
        }

    var skillLevelPreference: Int = 0
        set(value) {
            field = if (value > 2277) 2277 else value
        }

    fun id(id: Int) {
        this.id = id
    }

    fun status(status: RaidsResources.RaidPartyStatus) {
        this.status = status.value()
    }

    fun syncAdvertise(advert: Boolean, currentTime: Long = 0L) {
        this.timestamp = currentTime
        this.advertised = advert
    }

    fun clearPreferencesFor(player: Player) {
        player.varps().varbit(Varbit.RAID_PARTY_SIZE_PREF, 0)
        player.varps().varbit(Varbit.RAID_SKILL_LVL_PREF, 0)
        player.varps().varbit(Varbit.RAID_CMB_LVL_PREF, 0)
    }

    fun advertised(): Boolean = advertised

    fun creation(): Long = timestamp

    fun id(): Int = id

    fun world(): World = world

    fun status(): Int = status

    fun members(): MutableList<Player> = members

    fun ownerId(): Int = ownerId

    fun ownerName(): String = ownerName

    fun chatChannel(): ClanChat = chatChannel

    override fun toString(): String {
        return String.format("id=%d, ownerId=%d, ownerName=%s, status=%d, memberSize=%d", id, ownerId, ownerName, status, members.size)
    }
}