package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.ItemsOnDeath
import java.util.stream.Collectors

/**
 * Created by Situations on 1/1/2016.
 */

@Suspendable fun ShowItemsKeptOnDeath(it: Script) {
	if (it.player().interfaces().visible(162, 550)) { // Close chatbox
		it.player().interfaces().close(162, 550)
	}
	it.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
	if (it.player().world().realm().isDeadman) {
		it.player().interfaces().sendMain(226)
		it.player().write(InterfaceSettings(226, 16, 0, 27, 2))
		it.player().write(SetItems(554, -1, 63748, DeadmanMechanics.infoForPlayer(it.player()).updateBankKey().bankItemsKey.lootcontainer))
	} else {
		it.player().interfaces().sendMain(4)
		it.player().write(InterfaceSettings(4, 18, 0, 4, 2))
		it.player().write(InterfaceSettings(4, 21, 0, 42, 2))
		ItemsOnDeath.sendIKODScript(it.player())
	}
	val player = it.player()
	// Ripped from ItemsOnDeath
	val lostItems = mutableListOf<Item>()
	player.inventory().copy().filter { i -> i != null }.forEach { lostItems.add(it) }
	player.equipment().copy().filter { i -> i != null }.forEach { lostItems.add(it) }
	val untrades = lostItems.stream().filter({ item ->
						item != null &&
						(item.id() == ItemsOnDeath.DRAG_DEFENDER_TRIM ||
						item.id() == ItemsOnDeath.LOOTING_BAG ||
						!item.rawtradable(player.world()) &&
						!item.lostUntradable(player.world()))
	}).collect(Collectors.toList<Item>())
	val keptIds = mutableListOf<Int>()
	val dropped = mutableListOf<Int>()
	untrades.forEach { u ->
		if (!ItemsOnDeath.isRSUntradeableItem(u)) {
			if (!keptIds.contains(u.id()))
				keptIds.add(u.id())
		} else {
			if (!dropped.contains(u.id()))
				dropped.add(u.id())
		}
	}
	if (keptIds.size > 0) {
		var set1 = ""
		var kx = 0
		keptIds.forEach { k ->
			set1 += Item(k).name(player.world()).col("FF0000")
			if (++kx < keptIds.size)
				set1 += ", "
		}
		player.message("Untradable items always kept: $set1.")
	}
	if (dropped.size > 0) {
		var set1 = ""
		var kx = 0
		dropped.forEach { k ->
			set1 += Item(k).name(player.world()).col("3377ff")
			if (++kx < dropped.size)
				set1 += ", "
		}
		player.message("Untradable items dropped or lost: $set1.")
	}
}

@ScriptMain fun SIKODbutton(repo: ScriptRepository) {
	repo.onButton(387, 21) @Suspendable {
		ShowItemsKeptOnDeath(it)
	}
}