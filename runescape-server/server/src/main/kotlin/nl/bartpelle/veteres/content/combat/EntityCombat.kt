package nl.bartpelle.veteres.content.combat

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.steroids.MeleeStepSupplier
import nl.bartpelle.veteres.model.map.steroids.RangeStepSupplier
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.lang.ref.WeakReference

/**
 * Created by Bart on 10/27/2015.
 */
object EntityCombat {

	@JvmStatic val logger: Logger = LogManager.getLogger(EntityCombat)
	
	// Gorrillas
	@Suspendable fun boilerplateWrap(it: Script, f: (Npc, Entity) -> Unit) {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.attackTimerReady(npc)) {
				f.invoke(npc, target)
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_ATTACK_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				if (target.isPlayer) {
					val targ = target as Player
					targ.interfaces().closeMain()
					targ.interfaces().close(162, 550) // Close chatbox
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return
		}
	}
	
	fun getTarget(script: Script) = getTarget(script.npc())
	
	fun getTarget(npc: Entity): Entity? {
		val ref: WeakReference<Entity> = npc.attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: return null
		val target: Entity? = ref.get()
		
		if (target != null)
			npc.face(target)
		
		// If these conditions fail, we can't attack
		if (target != null && !target.dead() && !npc.dead() && !target.finished()) {
			return target
		}
		
		return null
	}
	
	fun refreshTarget(script: Script): Entity? {
		val ref: WeakReference<Entity> = script.npc().attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: return null
		val target: Entity? = ref.get()
		val npc: Npc = script.npc()
		
		// If these conditions fail, we can't attack
		if (target != null && !target.dead() && !npc.dead() && !npc.finished() && !target.finished()) {
			return target
		}
		
		return null
	}
	
	// While this returns true, the npc will continue aggressing a target.
	@JvmStatic fun targetOk(me: Entity, target: Entity?, maxdist: Int = 16): Boolean {
		val ok = target != null && !target.finished() && !me.finished() && !target.dead() && !me.dead() && me.hp() > 0 && (target.tile().distance(me.tile()) < maxdist
				&& PlayerCombat.bothInFixedRoom(me, target))
		if (!ok) {
			me.stopActions(true)
		}
		
		return ok
	}
	
	fun moveCloser_(player: Entity, target: Entity, tile: Tile): Tile {
		val steps = if (player.pathQueue().running()) 2 else 1
		//val otherSteps = if (target.pathQueue().running()) 2 else 1
		
		//val otherTile = target.pathQueue().peekAfter(otherSteps)?.toTile() ?: target.tile()
		moveCloser(player, target, tile)
		//player.stepTowards(target, otherTile, 25)
		return player.pathQueue().peekAfter(steps - 1)?.toTile() ?: tile
	}
	
	fun canAttackMelee(me: Entity, target: Entity, getCloser: Boolean = false): Boolean {
		var tile = me.tile()
		
		if (!me.touches(target, tile) && !me.frozen() && !me.stunned() && getCloser) {
			me.pathQueue().clear()
			// Not touching? Shit son, go close!
			tile = moveCloser_(me, target, tile)
			
			// On obstructable npcs, this is important to return false. See NpcPreSyncTask: this would say 'yes, you
			// can freely walk' while the check in NpcPreSyncTask would say otherwise. See issue #533.
			if (me is Npc && me.combatInfo()?.unstacked ?: false) {
				return false
			}
		}
		
		return me.touches(target, tile)
	}

	fun cooldownForNpc(entity: Entity, target: Entity) {
        val attacker = entity as Npc
		target.putattrib(AttributeKey.LAST_DAMAGER, attacker)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		attacker.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(attacker, attacker.combatInfo().attackspeed)
	}
	
	// Not the default. used only by verac/dharok as of 18/7/16
	@Suspendable fun defaultMelee(it: Script, function: Function2<Script, Entity, Unit>) {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return
		
		// Cycle while our target is generally valid.
		while (EntityCombat.targetOk(npc, target)) {
			// Firstly, path to the target. Regardless of if we can or cannot hit them, we must get within distance.
			if (EntityCombat.canAttackMelee(npc, target, true)) {
				// Only once we are in combat distance do we attempt to attack, and stop attacking if the target is invalid.
				if (!PlayerCombat.canAttack(npc, target)) {
					return // For consistency, you have to get within melee distance if you're a melee Npc before breaking combat.
				} else if (EntityCombat.attackTimerReady(npc)) {
					npc.animate(npc.attackAnimation())
					function(it, target)
					
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					if (target.isPlayer) {
						val targ = target as Player
						targ.interfaces().closeMain()
						targ.interfaces().close(162, 550) // Close chatbox
					}
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return
		}
	}
	
	fun canAttackDistant(me: Entity, target: Entity, getCloser: Boolean, dist: Int): Boolean {
		val supplier = RangeStepSupplier(me, target, dist)
		if (!supplier.reached(me.world()) && !me.frozen() && !me.stunned() && !me.dead() && getCloser) {
			// Are we in range distance?
			me.walkTo(target, PathQueue.StepType.REGULAR)
			me.pathQueue().trimToSize(1)
		}
		
		return supplier.reached(me.world(), target)
	}
	
	fun attackTimerReady(me: Entity) = TimerKey.COMBAT_ATTACK !in me.timers()
	
	fun putCombatDelay(me: Entity, time: Int) {
		me.timers()[TimerKey.COMBAT_ATTACK] = time
	}
	
	fun attemptHit(me: Entity, target: Entity, style: CombatStyle) = AccuracyFormula.doesHit(me, target, style, 1.0)
	
	fun randomHit(me: Npc): Int {
		val max = me.combatInfo().maxhit
		val hit = me.world().random(max)
		return hit
	}
	
	/**
	 * Be sure to call this every cycle in the combat script REGARDLESS of if an attack took place (might not if in range).
	 * Used in the npc logic core for aggression, retaliation, random walking etc.
	 */
	fun postHitLogic(me: Entity) {
		me.timers()[TimerKey.IN_COMBAT] = 3
	}
	
	// This is the default!
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		npc.stopActions(true)
		var target = getTarget(it) ?: return@s
		if (npc.combatInfo() == null || npc.combatInfo().stats == null) {
			logger.error("Npc id {} '{}' index {} is missing combatInfo or Stats", npc.id(), npc.def().name, npc.index())
			return@s
		}
		val maxDist = 16
		
		while (targetOk(npc, target, maxDist)) {
			// Distance checks first
			if (canAttackMelee(npc, target, true)) {
				// Now validity checks only when in distance.
				if (!PlayerCombat.canAttack(npc, target)) { // For consistency, you have to get within melee distance if you're a melee Npc before breaking combat.
					return@s
				} else if (attackTimerReady(npc)) {
					if (attemptHit(npc, target, CombatStyle.MELEE)) {
						val hit = target.hit(npc, randomHit(npc)).combatStyle(CombatStyle.MELEE)//npc vs entity
						
						// Poison?
						if (hit.damage() > 0 && (npc.combatInfo()?.poisonous() ?: false)) {
							val chance = npc.combatInfo().poisonchance
							if (chance >= 100 || npc.world().rollDie(100, npc.combatInfo().poisonchance)) {
								target.poison(npc.combatInfo().poison)
							}
						}
					} else {
						target.hit(npc, 0).combatStyle(CombatStyle.MELEE) // Uh-oh, that's a miss.
					}
					
					npc.animate(npc.attackAnimation())
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					if (target.isPlayer) {
						val targ = target as Player
						targ.interfaces().closeMain()
						targ.interfaces().close(162, 550) // Close chatbox
					}
					putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			postHitLogic(npc)
			it.delay(1)
			target = refreshTarget(it) ?: return@s
		}
	}
	
	@JvmField val autoretaliate: Function1<Script, Unit> = s@ @Suspendable {
		// The entity that is going to retaliate
		val entity = it.ctx<Entity>()
		val target = entity.attribOr<WeakReference<Entity>>(AttributeKey.TARGET, WeakReference<Entity>(null)).get() ?: return@s
		it.delay(2)
		entity.attack(target)
	}
	
	fun moveCloser(npc: Entity, target: Entity, tile: Tile): Tile {
		/*npc.pathQueue().clear()
		val steps = if (npc.pathQueue().running()) 2 else 1
		val otherSteps = if (target.pathQueue().running()) 2 else 1

		val otherTile = target.pathQueue().peekAfter(otherSteps)?.toTile() ?: target.tile()
		npc.stepTowards(target, otherTile, 25)
		return npc.pathQueue().peekAfter(steps - 1)?.toTile() ?: tile*/
		val supplier = MeleeStepSupplier(npc, target)
		npc.pathQueue().step(supplier.get())
		return npc.pathQueue().peekLastTile()
	}
}