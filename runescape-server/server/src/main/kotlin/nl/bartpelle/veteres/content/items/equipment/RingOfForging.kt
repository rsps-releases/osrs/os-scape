package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/19/2016.
 */

object RingOfForging {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		val RING_OF_FORGING = 2568
		
		r.onEquipmentOption(1, RING_OF_FORGING) @Suspendable {
			val charges = it.player().attribOr<Int>(AttributeKey.RING_OF_FORGING_CHARGES, 140)
			if (charges == 1) {
				it.message("You can smelt one more piece of iron ore before a ring melts.")
			} else {
				it.message("You can smelt $charges more pieces of iron ore before a ring melts.")
			}
		}
	}
}
