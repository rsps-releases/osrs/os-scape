package nl.bartpelle.veteres.content.achievements

import nl.bartpelle.veteres.model.entity.Player
import java.text.DecimalFormat

/**
 * Created by Mack on 11/20/2017.
 */
class Achievement(uuid: Int, category: AchievementCategory = AchievementCategory.CUSTOM, limit: Int, description: String, func: Function1<Player, Boolean>? = null, triggers: IntArray = intArrayOf()) {
	
	/**
	 * The unique identifier of this achievement task.
	 */
	private val id = uuid
	
	/**
	 * The goal count or limit to the task.
	 */
	private val goal = limit
	
	/**
	 * Checks if the player is eligible to start/continue/complete the task.
	 */
	private val progress = func
	
	/**
	 * The category type the achievement falls into.
	 */
	private val type = category
	
	/**
	 * The description to the achievement.
	 */
	private val descr = description
	
	/**
	 * The array of triggers for this achievement.
	 */
	private val keys = triggers
	
	/**
	 * Returns the task unique identifier.
	 */
	fun id(): String {
		return id.toString()
	}
	
	/**
	 * Returns the raw integer value
	 */
	fun rawId(): Int {
		return id
	}
	
	/**
	 * Gets the description for the achievement.
	 */
	fun description(): String {
		return descr
	}
	
	/**
	 * Gets the achievement type defined, if any.
	 */
	fun type(): AchievementCategory {
		return type
	}
	
	/**
	 * Gets the limit for the task.
	 */
	fun limit(): Int {
		return goal
	}
	
	/**
	 * The array of triggers for the achievement.
	 */
	fun triggers(): IntArray {
		return keys
	}
	
	/**
	 * A flag checking if we need to scan for triggers.
	 */
	fun hasTriggers(): Boolean {
		return (keys.isNotEmpty())
	}
	
	/**
	 * Flags whether the player can progress on the given achievement.
	 */
	fun meetsRequirement(player: Player): Boolean {
		if (completed(player)) return false

		return progress?.invoke(player) ?: return true
	}

	/**
	 * Gets the player's current progress on the specified task and stylizes it to
	 * display on screen.
	 */
	fun displayFormattedProgression(player: Player): String {
		val currentProgress = taskProgressFor(player)
        val decimalFormatter = DecimalFormat("##.#")
		if (completed(player)) {
            return "100"
        }
		return (decimalFormatter.format(currentProgress.toDouble().div(goal.toDouble()).times(100)))
	}
	
	/**
	 * A flag checking if the player has met the goal of this achievement.
	 */
	fun completed(player: Player): Boolean {
		return (taskProgressFor(player) >= goal)
	}
	
	/**
	 * Gets the player's map value of the provided key.
	 */
	fun taskProgressFor(player: Player): Int {
		//return (AchievementDiary.cachedAchievements(player).getOrDefault(id.toString(), "0").toInt())
		return 0
	}
	
	companion object {
		
		/**
		 * Creates a new basic achievement.
		 */
		fun create(uuid: Int, category: AchievementCategory = AchievementCategory.CUSTOM, goal: Int, description: String, func: Function1<Player, Boolean>? = null, vararg triggers: Int) : Achievement {
			return (Achievement(uuid, category, goal, description, func, triggers))
		}

		/**
		 * Creates a new achievement with no expectation of having a functional argument within it.
		 */
		fun create(uuid: Int, category: AchievementCategory = AchievementCategory.CUSTOM, goal: Int, description: String, vararg triggers: Int) : Achievement {
			return Achievement(uuid, category, goal, description, null, triggers)
		}
	}
}