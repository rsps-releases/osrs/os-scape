package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.sigmund

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.ItemContainer

/**
 * Created by Mack on 10/10/2017.
 */
class SigmundExchangeContainer(player: Player) {
	
	/**
	 * The container storing all items offered by the player.
	 */
	private val playersOffer = ItemContainer(player.world(), 28, ItemContainer.Type.REGULAR)
	
	/**
	 * The item(s) offered by Sigmund.
	 */
	private val sigmundsOffer = ItemContainer(player.world(), 1, ItemContainer.Type.REGULAR)
	
	/**
	 * Gets Sigmund's item container.
	 */
	fun sigmundsOffer(): ItemContainer {
		return sigmundsOffer
	}
	
	/**
	 * Gets the player's item container.
	 */
	fun playersOffer(): ItemContainer {
		return playersOffer
	}
}