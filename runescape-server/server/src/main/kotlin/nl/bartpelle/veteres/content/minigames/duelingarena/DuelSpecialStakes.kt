package nl.bartpelle.veteres.content.minigames.duelingarena

import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot

/**
 * Created by Jak on 04/11/2016.
 */
object DuelSpecialStakes {
	
	// If you're trying to equip a weapon not allowed in a special stake.. An Abyssal Whip is NOT allowed in a tent+dds stake.
	@JvmStatic fun extra_duel_type_rule_broken(player: Player, weaponId: Int): Boolean {
		if (Staking.in_duel(player)) {
			val type = player.attribOr<Int>(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, -2)
			val whip = if (type == 1) 4151 else 12006
			if (type == 1 || type == 2) {
				// Only do these checks in type 1/2 whip/dds stakes
				if (weaponId == whip) {
					return false
				}
				daggers.forEach { id -> if (weaponId == id) return false }
				// Not a whip/dds. Rule broken!
				return true
			} else if (type == 4) {
				if (weaponId != 12006) {
					return true
				}
			}
			// anything else is fine
		}
		return false
	}
	
	
	// List of dragon daggers.
	val daggers = arrayOf(1215, 1231, 6580, 5698)
	
	// If you're missing items for a special stake.. such as tent+dds only.
	@JvmStatic fun extra_duel_crit_not_met(player: Player, partner: Player, extra_type: Int): Boolean {
		var ok: Boolean = true
		val whip_type = if (extra_type == 1) 4151 else 12006
		
		arrayOf(player, partner).forEach { stakee ->
			val weaponId = stakee.equipment()[EquipSlot.WEAPON]?.id() ?: -1
			//stakee.debug("type "+whip_type+" on duel type "+extra_type+" wep = "+weaponId)
			when (extra_type) {
				1, 2 -> {
					// Checks that we're (1) wearing either dds/tent/abby whip AND have the other required item in our inventory - tent/abby/dds
					if (weaponId == -1) {
						stakee.message("<col=FF0000>You must be wearing a weapon for this type of duel.")
						// You must be wearing one of the right weapons. Saves checking inv for both items.
						ok = false
					} else if (weaponId == whip_type) {
						var has_dds: Boolean = false
						daggers.forEach { id ->
							if (stakee.inventory().contains(id)) {
								has_dds = true
							}
						}
						if (!has_dds) {
							stakee.message("<col=FF0000>You don't have a Dragon dagger to duel with!")
							// You're missing a dds!
							ok = false
						}
					} else if (ok) { // Optimise check
						var wearing_dds: Boolean = false
						daggers.forEach { id ->
							if (weaponId == id) {
								wearing_dds = true
								if (!stakee.inventory().contains(whip_type)) {
									stakee.message("<col=FF0000>You don't have an " + stakee.world().definitions().get(ItemDefinition::class.java, whip_type).name + " to duel with!")
									// You're missing a whip.
									ok = false
								}
							}
						}
						if (ok && !wearing_dds) {
							stakee.message("<col=FF0000>You're not wearing an acceptable weapon for this duel.")
							// You're not wearing an acceptable weapon at all!
							ok = false
						}
					}
				}
			// 3 = normal stake, no restrictions.
				4 -> {
					if (weaponId == -1 || (weaponId != 12006)) {
						stakee.message("<col=FF0000>You must be wearing a Tentacle whip for this type of duel.")
						// You must be wearing one of the right weapons. Saves checking inv for both items.
						ok = false
					}
				}
			}
		}
		if (!ok) {
			player.message("<col=FF0000>You or your partner didn't have the right items equipped/in the inventory.")
			partner.message("<col=FF0000>You or your partner didn't have the right items equipped/in the inventory.")
		}
		return !ok
	}
}