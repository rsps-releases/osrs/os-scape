package nl.bartpelle.veteres.content.interfaces.magicbook

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Tuple

/**
 * Created by Situations on 3/20/2016.
 */

object ArceuusSpellbook {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Teleports
//		r.onButton(218, 141, @Suspendable { attempt_home_teleport(it) }) //Home teleport (1699, 3881)
		r.onButton(218, 143) @Suspendable { cast_spell(it, 3, 6, Tile(3240, 3195, 0), 10.0, false, Item(563, 1), Item(557, 2)) } //Lumbridge Graveyard teleport
		r.onButton(218, 147) @Suspendable { cast_spell(it, 3, 17, Tile(3109, 3350, 0), 16.0, false, Item(563, 1), Item(557, 1), Item(555, 1)) } //Draynor Manor teleport
		r.onButton(218, 152) @Suspendable { cast_spell(it, 3, 28, Tile(2981, 3510, 0), 22.0, false, Item(563, 1), Item(558, 2)) } //Mind Altar teleport
		r.onButton(218, 154) @Suspendable { cast_spell(it, 3, 34, Tile(3211, 3422, 0), 27.0, false, Item(563, 1), Item(556, 1)) } //Respawn teleport
		r.onButton(218, 156) @Suspendable { cast_spell(it, 3, 40, Tile(3431, 3460, 0), 30.0, false, Item(563, 1), Item(556, 2)) } //Salve Graveyard teleport
		r.onButton(218, 158) @Suspendable { cast_spell(it, 3, 48, Tile(3549, 3529, 0), 50.0, false, Item(563, 1), Item(566, 1), Item(557, 1)) } //Fenkenstrain's teleport
		r.onButton(218, 163) @Suspendable { cast_spell(it, 3, 61, Tile(3502, 3292, 0), 68.0, false, Item(563, 2), Item(566, 2)) } //West Ardougne teleport
		r.onButton(218, 166) @Suspendable { cast_spell(it, 3, 65, Tile(3212, 3424, 0), 74.0, false, Item(563, 1), Item(566, 1), Item(561, 1)) } //Harmony Island teleport TODO: COORDS
		r.onButton(218, 168) @Suspendable { cast_spell(it, 3, 71, Tile(2979, 3763, 0), 82.0, true, Item(563, 1), Item(565, 1), Item(566, 1)) } //Cementery teleport
		r.onButton(218, 172) @Suspendable { cast_spell(it, 3, 83, Tile(3564, 3312, 0), 90.0, false, Item(563, 2), Item(565, 1), Item(566, 2)) } //Barrows teleport
		r.onButton(218, 174) @Suspendable { cast_spell(it, 3, 90, Tile(3212, 3424, 0), 100.0, false, Item(563, 2), Item(565, 2), Item(566, 2)) } //Ape Atoll teleport TODO: COORDS
		
		//Reanimate
		r.onSpellOnItem(218, 142, @Suspendable { animate(it, 13447, "Ensouled goblin head", 3, 6.0, 7018, Item(561, 1), Item(559, 2)) }) //Reanimate Goblin
		r.onSpellOnItem(218, 144, @Suspendable { animate(it, 13450, "Ensouled monkey head", 7, 14.0, 7019, Item(561, 1), Item(559, 3)) }) //Reanimate Monkey
		r.onSpellOnItem(218, 145, @Suspendable { animate(it, 13453, "Ensouled imp head", 12, 24.0, 7020, Item(561, 2), Item(559, 3)) }) //Reanimate Imp
		r.onSpellOnItem(218, 146, @Suspendable { animate(it, 13456, "Ensouled minotaur head", 16, 32.0, 7021, Item(561, 2), Item(559, 4)) }) //Reanimate Minotaur
		r.onSpellOnItem(218, 148, @Suspendable { animate(it, 13459, "Ensouled scorpion head", 19, 38.0, 7022, Item(566, 1), Item(561, 1)) }) //Reanimate Scorpion
		r.onSpellOnItem(218, 149, @Suspendable { animate(it, 13462, "Ensouled bear head", 21, 42.0, 7023, Item(566, 1), Item(561, 1), Item(559, 1)) }) //Reanimate Bear
		r.onSpellOnItem(218, 150, @Suspendable { animate(it, 13465, "Ensouled unicorn head", 22, 44.0, 7024, Item(566, 1), Item(561, 1), Item(559, 2)) }) //Reanimate Unicorn
		r.onSpellOnItem(218, 151, @Suspendable { animate(it, 13468, "Ensouled dog head", 26, 52.0, 7025, Item(566, 1), Item(561, 2), Item(559, 2)) }) //Reanimate Dog
		r.onSpellOnItem(218, 153, @Suspendable { animate(it, 13471, "Ensouled chaos druid head", 30, 60.0, 7026, Item(566, 1), Item(561, 2), Item(559, 3)) }) //Reanimate Chaos Druid
		r.onSpellOnItem(218, 155, @Suspendable { animate(it, 13474, "Ensouled giant head", 37, 74.0, 7027, Item(566, 1), Item(561, 2), Item(559, 4)) }) //Reanimate Giant
		r.onSpellOnItem(218, 156, @Suspendable { animate(it, 13477, "Ensouled ogre head", 40, 80.0, 7028, Item(566, 1), Item(561, 3), Item(559, 4)) }) //Reanimate Ogre
		r.onSpellOnItem(218, 158, @Suspendable { animate(it, 13480, "Ensouled elf head", 43, 86.0, 7029, Item(566, 2), Item(561, 2), Item(559, 2)) }) //Reanimate Ogre
		r.onSpellOnItem(218, 159, @Suspendable { animate(it, 13483, "Ensouled troll head", 46, 92.0, 7030, Item(566, 2), Item(561, 2), Item(559, 3)) }) //Reanimate Troll
		r.onSpellOnItem(218, 161, @Suspendable { animate(it, 13486, "Ensouled horror head", 52, 104.0, 7031, Item(566, 2), Item(561, 2), Item(559, 4)) }) //Reanimate Horror
		r.onSpellOnItem(218, 162, @Suspendable { animate(it, 13489, "Ensouled kalphite head", 57, 114.0, 7032, Item(566, 2), Item(561, 3), Item(559, 4)) }) //Reanimate Kalphite
		r.onSpellOnItem(218, 164, @Suspendable { animate(it, 13492, "Ensouled dagannoth head", 62, 124.0, 7033, Item(566, 3), Item(561, 3), Item(559, 4)) }) //Reanimate Dagannoth
		r.onSpellOnItem(218, 165, @Suspendable { animate(it, 13495, "Ensouled bloodveld head", 65, 130.0, 7034, Item(566, 2), Item(565, 1), Item(561, 2)) }) //Reanimate Bloodveld
		r.onSpellOnItem(218, 167, @Suspendable { animate(it, 13498, "Ensouled tzhaar head", 69, 138.0, 7035, Item(566, 2), Item(565, 1), Item(561, 3)) }) //Reanimate TzHaar
		r.onSpellOnItem(218, 169, @Suspendable { animate(it, 13501, "Ensouled demon head", 72, 144.0, 7036, Item(566, 2), Item(565, 1), Item(561, 4)) }) //Reanimate Demon
		r.onSpellOnItem(218, 170, @Suspendable { animate(it, 13504, "Ensouled aviansie head", 78, 156.0, 7037, Item(566, 3), Item(565, 1), Item(561, 4)) }) //Reanimate Aviansie
		r.onSpellOnItem(218, 173, @Suspendable { animate(it, 13507, "Ensouled abyssal head", 85, 170.0, 7038, Item(566, 4), Item(565, 1), Item(561, 4)) }) //Reanimate Abyssal Creature
		r.onSpellOnItem(218, 175, @Suspendable { animate(it, 13510, "Ensouled dragon head", 93, 186.0, 7039, Item(566, 4), Item(565, 2), Item(561, 4)) }) //Reanimate Dragon
	}
	
	//You should finish off your last one first.
	@Suspendable fun animate(it: Script, head: Int, head_name: String, req: Int, mag_exp: Double, reanimated_npc: Int, vararg runes: Item) {
		val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]]
		val old_tile = it.player().tile()
		if (it.player().locked()) return
		it.player().stopActions(true)
		
		//Is our player using this spell on the correct item?
		if (item.id() != head) {
			it.itemBox("This spell cannot reanimate that item.<br> Its intended target is: <col=862013>$head_name</col>.", head)
			return
		}
		
		//Does our player have the required magic level?
		if (it.player().skills().xpLevel(Skills.MAGIC) < req) {
			it.itemBox("You need a Magic level of $req to reanimate that.", head)
			return
		}

/*        //Is our player near the Dark Altar?
        if(!it.player().tile().inArea(1677, 3859, 1745, 3904)) {
            it.itemBox("The creature cannot be reanimated here. The power<br>of the crystals by the Dark Altar will increase the<br>potency of the spell.", head)
            return
        }*/
		
		//Has our player already spawned a monster?
		if (it.player().attribOr(AttributeKey.HAS_REANIMATED_MONSTER, false)) {
			it.player().message("You need to kill the monster you already have before spawning another!")
			return
		}
		
		//Does our player have enough runes?
		if (!MagicCombat.has(it.player(), runes.toList().toTypedArray(), true))
			return
		
		//Time to animate our beast! :-)
		it.player().lock()
		it.player().walkTo(it.player().tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
		it.player().faceTile(Tile(old_tile))
		it.delay(1)
		it.player().skills().__addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) mag_exp * 2 else mag_exp)
		it.animate(7198)
		it.player().graphic(1288)
		it.delay(1)
		it.player().inventory().remove(item, true)
		val spawnedItem = it.player().world().spawnGroundItem(GroundItem(it.player().world(), Item(item), old_tile, it.player().id()))
		it.player().world().spawnProjectile(it.player().tile(), old_tile, 1289, 40, 10, 5, 75, 15, 10)
		it.delay(2)
		it.player().world().tileGraphic(1290, old_tile, 0, 0)
		it.delay(1)
		it.player().world().removeGroundItem(spawnedItem)
		it.delay(5)
		val npc = Npc(reanimated_npc, it.player().world(), old_tile)
		npc.putattrib(AttributeKey.OWNING_PLAYER, Tuple(it.player().id(), it.player()))
		npc.putattrib(AttributeKey.IS_REANIMATED_MONSTER, true)
		it.player().putattrib(AttributeKey.HAS_REANIMATED_MONSTER, true)
		npc.timers().register(TimerKey.REANIMATED_MONSTER_DESPAWN, 60)
		it.player().world().registerNpc(npc)
		npc.face(it.player())
		npc.respawns(false)
		npc.attack(it.player())
		it.player().unlock()
	}
}