package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.achievements.pvp.PVPDiaryHandler
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.skills.mining.Mining
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 11/9/2015.
 */


object ResourceArena {
	
	val MANDRITH = 6599
	val PILES = 13
	
	val ARENA_BOUNDARIES = Area(3174, 3924, 3196, 3944)
	val ALLOWED_EXCHANGE = intArrayOf(440, 453, 444, 447, 449, 451, 1515, 1513, 11936, 11934, 2349, 2351, 2353, 2355, 2357, 2359, 2361, 2363, 451, 13439, 10138)
	
	@Suspendable fun swap(it: Script, original: Int, result: Int) {
		val currency = if (it.player().world().realm().isPVP) 13307 else 995
		val name = if (it.player().world().realm().isPVP) "blood money" else "coins"
		if (it.customOptions("Banknote " + it.player().inventory().count(original) + " ${Item(original).name(it.player().world())}?", "Yes - " + it.player().inventory().count(original) * 50 + " gp", "Cancel") == 1) {
			if (Item(currency, it.player().inventory().count(original) * 50) !in it.player().inventory()) {
				it.chatNpc("Unfortunately, you don't have enough $name right now to do that.", 13, 611)
			} else {
				val num = it.player().inventory().count(original)
				it.player().inventory() -= Item(currency, it.player().inventory().count(original) * 50)
				it.player().inventory() -= Item(original, num)
				it.player().inventory() += Item(result, num)
				
				if (AchievementDiary.diaryHandler(it.player()) is PVPDiaryHandler) {
					AchievementAction.process(it.player(), AchievementDiary.diaryHandler(it.player()).achievementById(it.player(), 23))
				}
				it.itemBox("Piles converts your items to banknotes.", original)
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(MANDRITH) @Suspendable {
			it.chatPlayer("Who are you and what is this place?", 554)
			it.chatNpc("My name is Mandrith.", MANDRITH, 588)
			it.chatNpc("I collect valuable resources and pawn off access to them<br>to foolish adventurers, like yourself.", MANDRITH, 589)
			it.chatNpc("You should take a look inside my arena. Theres an<br>abundance of valuable resources inside.", MANDRITH, 555)
			it.chatPlayer("And I can take whatever I want?", 554)
			it.chatNpc("It's all yours. All I ask is you pay the upfront fee.", MANDRITH, 588)
			it.chatPlayer("Will others be able to kill me inside?", 588)
			it.chatNpc("Yes. These walls will only hold them back for so long.", MANDRITH, 588)
			it.chatPlayer("You'll stop them though, right?", 554)
			it.chatNpc("Haha! For the right price, I won't deny any one access<br>to my arena. Even if their intention is to kill you.", MANDRITH, 568)
			it.chatPlayer("Right...", 571)
			it.chatNpc("My arena holds many treasures that I've acquired at<br>great expense, adventurer. Their bounty can come at a<br>price.", MANDRITH, 590)
			it.chatNpc("One day, adventurer, I will boast ownership of a much<br>larger, much more dangerous arena than this. Take<br>advantage of this offer while it lasts.", MANDRITH, 590)
		}
		
		repo.onNpcOption1(PILES) @Suspendable {
			val name = if (it.player().world().realm().isPVP) "blood money" else "coins"
			it.chatNpc("Hello. I can convert items to banknotes, for 50 $name<br>per item. Just hand me the items you'd like me to<br>convert.", PILES, 590)
			when (it.options("Who are you?", "Thanks")) {
				1 -> {
					it.chatPlayer("Who are you?", 554)
					it.chatNpc("I'm Piles. I lived in Draynor Village when I was<br>young, where I saw three men working in the market,<br>converting items to banknotes.", PILES, 569)
					it.chatNpc("Their names were Niles, Miles and Giles. I'm trying to<br>be like them, so I've changed my name and started this<br>business here.", PILES, 569)
					it.chatPlayer("Thanks", 575)
				}
				2 -> {
					it.chatPlayer("Thanks", 575)
				}
			}
		}
		
		repo.onItemOnNpc(PILES) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			
			if (item in ALLOWED_EXCHANGE) {
				swap(it, item, Item(item).note(it.player().world()).id())
			} else {
				it.chatNpc("Sorry, I wasn't expecting anyone to want to convert<br>that sort of item, so I haven't any banknotes for it.", 13, 611)
			}
		}
		
		repo.onObject(26760) @Suspendable {
			val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
			if (option == 1) {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				val player = it.player()
				val name = if (it.player().world().realm().isPVP) "blood money" else "coins"
				val itemID = if (it.player().world().realm().isPVP) 13307 else 995
				val amount = if (it.player().world().realm().isPVP) 100 else 7500
				val instanced = player in MageBankInstance
				
				it.player().faceObj(obj)
				if (it.player().tile().z == 3945 || instanced && player.tile().z > obj.tile().z) {
					if (player.inventory().count(itemID) < amount) {
						it.message("You do not have enough $name to enter the Arena.")
					} else if (it.customOptions("Pay $amount $name to enter?", "Yes", "No") == 1) {
//						it.player().lock()
						it.player().inventory() -= Item(itemID, amount)
						it.message("You pay $amount $name and enter the resource arena.")
						it.runGlobal(player.world()) @Suspendable { s ->
							val world = player.world()
							val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
							val spawned = MapObj(Tile(obj.tile().x, 3945), 1548, obj.type(), 2)
							world.removeObj(old, false)
							world.spawnObj(spawned)
							s.delay(2)
							world.removeObjSpawn(spawned)
							world.spawnObj(old)
						}
						val targetTile = Tile(player.tile().x, player.tile().transform(0, -1).z)
						it.player().pathQueue().interpolate(targetTile.x, targetTile.z, PathQueue.StepType.FORCED_WALK)
						it.waitForTile(targetTile)
						it.delay(1)
//						it.player().unlock()
					}
				} else if (it.player().tile().z == 3944 || instanced && player.tile().z <= obj.tile().z) {
					if (!it.player().tile().equals(obj.tile().transform(0, 0, 0))) {
						it.player().walkTo(obj.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
						it.waitForTile(obj.tile().transform(0, 0, 0))
					}
					
//					it.player().lock()
					it.runGlobal(player.world()) @Suspendable { s ->
						val world = player.world()
						val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
						val spawned = MapObj(Tile(obj.tile().x, 3945), 1548, obj.type(), 2)
						world.removeObj(old, false)
						world.spawnObj(spawned)
						s.delay(2)
						world.removeObjSpawn(spawned)
						world.spawnObj(old)
					}
					val targetTile = player.tile().transform(0, 1)
					it.player().pathQueue().interpolate(targetTile.x, targetTile.z, PathQueue.StepType.FORCED_WALK)
					it.waitForTile(targetTile)
					it.delay(1)
//					it.player().unlock()
				}
			}
			
			if (option == 2) {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				val world = it.player().world()
                val instanced = it.player() in MageBankInstance
				
				it.player().faceObj(obj)
				if (it.player().tile().z == 3945 || instanced && it.player().tile().z > obj.tile().z) {
					val count = LinkedList<Player>()
					world.players().forEachKt({ p ->
						val plrx = p.tile().x
						val plrz = p.tile().z
						if (plrx in 3174..3196 && plrz >= 3924 && plrz <= 3944) {
							count += p
						}
					})
					if (count.size == 0) {
						it.messagebox("You peek inside the gate and see no adventurers inside the arena.")
					} else {
						it.messagebox("You peek inside the gate and see " + count.size + " adventurer inside the arena.")
					}
				} else if (it.player().tile().z == 3944) {
					it.message("All you see is the barren wasteland of the Wilderness.")
				}
			}
		}
		
		// Runite golem head mining
		repo.onNpcOption1(6601) @Suspendable {
			val head = it.targetNpc()!!
			
			it.delay(1)
			val player = it.player()
			val pick = Mining.findPickaxe(it.player())
			
			if (pick == null) {
				it.player.message("You do not have a pickaxe which you have the Mining level to use.")
			} else {
				if (player.skills()[Skills.MINING] < 85) {
					player.sound(2277, 0)
					it.messagebox("You need a Mining level of 85 to mine this rock.")
				} else {
					it.delay(1)
					it.message("You swing your pick at the rock.")
					
					while (!head.finished()) {
						player.animate(pick.anim)
						it.delay(1)
						
						var odds = Mining.chance(player.skills()[Skills.MINING], Mining.Rock.RUNE, pick)
						
						if (BonusContent.isActive(player, BlessingGroup.PROSPECTOR)) {
							odds = Math.min(95, (1.3 * odds).toInt())
						}
						
						if (player.world().random(100) <= odds) {
							it.message("You manage to mine some runite.")
							
							player.sound(3600, 0)
							player.animate(-1)
							
							player.inventory() += Mining.Rock.RUNE.ore
							it.addXp(Skills.MINING, Mining.Rock.RUNE.xp)
							
							if (AchievementDiary.diaryHandler(player) is PVPDiaryHandler) {
								AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(it.player(), 34))
							}
							
							player.world().unregisterNpc(head)
							break
						} else {
							it.delay(3)
						}
					}
				}
			}
		}
	}
	
}
