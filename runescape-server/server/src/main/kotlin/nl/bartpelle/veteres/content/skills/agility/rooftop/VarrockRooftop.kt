package nl.bartpelle.veteres.content.skills.agility.rooftop

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.skills.agility.UnlockAgilityPet
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/17/2015.
 * Finished by Bart on 3/14/2016
 */
object VarrockRooftop {
	
	val MARK_SPOTS = arrayOf(Tile(3215, 3410, 3),
			Tile(3195, 3416, 1),
			Tile(3193, 3395, 3),
			Tile(3222, 3402, 3),
			Tile(3237, 3406, 3))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Wall climb
		r.onObject(10586, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().skills().xpLevel(Skills.AGILITY) < 30) {
				it.message("You need an Agility level of 30 to attempt this.")
			} else {
				it.player().lock()
				it.player().faceTile(it.player().tile() - Tile(1, 0))
				it.delay(1)
				it.player().animate(828, 15)
				it.delay(2)
				it.player().teleport(3220, 3414, 3)
				it.animate(2585)
				it.delay(2)
				it.player().teleport(3219, 3414, 3)
				it.animate(-1)
				it.addXp(Skills.AGILITY, 12.0)
				it.player().unlock()
				
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
			}
		})
		
		// Clothes line
		r.onObject(10587, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lockNoDamage()
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(741)
			it.player().forceMove(ForceMovement(0, 0, -2, 0, 15, 30, FaceDirection.WEST))
			it.delay(1)
			it.player().teleport(Tile(3212, 3414, 3))
			it.delay(1)
			it.player().sound(2461, 15)
			it.player().animate(741, 15)
			it.player().forceMove(ForceMovement(0, 0, -2, 0, 30, 45, FaceDirection.WEST))
			it.delay(1)
			it.player().teleport(Tile(3210, 3414, 3))
			it.delay(1)
			it.player().sound(2461)
			it.player().animate(741)
			it.player().forceMove(ForceMovement(0, 0, -2, 0, 15, 30, FaceDirection.WEST))
			it.delay(1)
			it.player().teleport(Tile(3208, 3414, 3))
			it.addXp(Skills.AGILITY, 21.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
		})
		
		// Jump down
		r.onObject(10642, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() - Tile(1, 0))
			it.delay(1)
			it.player().sound(2462, 15)
			it.player().animate(2586, 15)
			it.delay(1)
			it.player().teleport(3197, 3416, 1)
			it.player().animate(2588)
			it.delay(1)
			it.player().animate(-1)
			it.addXp(Skills.AGILITY, 17.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
		})
		
		// Swing wall
		r.makeRemoteObject(10777) // Custom walk script
		r.onObject(10777, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().walkTo(3194, 3416, PathQueue.StepType.REGULAR).equals(3194, 3416)) {
				it.waitForTile(Tile(3194, 3416))
				it.player().lock()
				it.player().faceTile(it.player().tile() - Tile(1, 0))
				
				it.delay(1)
				it.player().animate(1995, 15)
				it.player().forceMove(ForceMovement(0, 0, -1, 0, 15, 45, FaceDirection.WEST))
				it.delay(1)
				
				it.player().teleport(3193, 3416, 1)
				it.player().sound(2468, 20)
				it.player().animate(2583, 20)
				it.player().forceMove(ForceMovement(0, 0, -3, -2, 25, 30, FaceDirection.WEST))
				it.delay(1)
				it.player().teleport(3190, 3414, 1)
				
				for (i in 1..5) {
					it.player().sound(2459, 35)
					it.player().animate(1122)
					it.player().forceMove(ForceMovement(0, 0, 0, -1, 34, 52, FaceDirection.WEST))
					it.delay(1)
					it.delay(1)
					it.player().teleport(it.player().tile() - Tile(0, 1))
				}
				
				it.player().looks().render(757, 757, 756, 756, 756, -1)
				it.player().animate(753)
				it.player().faceTile(it.player().tile() + Tile(0, -1))
				it.delay(1)
				it.player().pathQueue().interpolate(3190, 3407, PathQueue.StepType.FORCED_WALK)
				it.player().sound(2451, 0, 2)
				it.delay(1)
				it.player().sound(2451, 0, 2)
				it.delay(1)
				it.player().sound(2468)
				it.player().looks().resetRender()
				it.player().animate(741)
				it.player().forceMove(ForceMovement(0, 0, 2, -1, 5, 30, FaceDirection.EAST))
				it.delay(1)
				it.player().teleport(3192, 3406, 3)
				
				it.addXp(Skills.AGILITY, 25.0)
				it.player().unlock()
				
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
			}
		})
		
		// Jump gap
		r.onObject(10778, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() - Tile(0, 1))
			it.delay(1)
			it.player().sound(2468, 20)
			it.player().animate(2583, 10)
			it.player().forceMove(ForceMovement(0, 0, 0, -3, 25, 30, FaceDirection.SOUTH))
			it.delay(1)
			it.player().teleport(3193, 3399, 3)
			it.animate(2585)
			it.delay(1)
			it.player().forceMove(ForceMovement(0, 0, 0, -1, 17, 26, FaceDirection.SOUTH))
			it.delay(1)
			it.player().teleport(3193, 3398, 3)
			it.addXp(Skills.AGILITY, 9.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
		})
		
		// Jump gap
		r.onObject(10779, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() + Tile(1, 0))
			it.delay(1)
			it.animate(1995)
			it.player().sound(2461, 15)
			it.player().animate(4789, 15)
			it.player().forceMove(ForceMovement(0, 0, 7, 2, 20, 50, FaceDirection.EAST))
			it.delay(1)
			it.player().teleport(3215, 3399, 3)
			it.delay(1)
			
			it.player().sound(2468)
			it.player().animate(2583)
			it.player().faceTile(Tile(3222, 3399, 3))
			it.player().teleport(3217, 3399, 3)
			it.player().forceMove(ForceMovement(0, 0, 2, 0, 5, 10, FaceDirection.EAST).baseTile(Tile(3215, 3399)))
			it.delay(1)
			it.player().faceTile(Tile(3222, 3399, 3))
			it.player().teleport(3217, 3399, 3)
			//it.delay(1)
			
			it.player().animate(2585)
			it.player().forceMove(ForceMovement(0, 0, 1, 0, 18, 27, FaceDirection.EAST))
			it.player().faceTile(Tile(3222, 3399, 3))
			it.delay(1)
			it.player().faceTile(Tile(3222, 3399, 3))
			it.player().animate(-1)
			it.player().teleport(3218, 3399, 3)
			it.addXp(Skills.AGILITY, 22.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
		})
		
		// Jump another gap
		r.onObject(10780, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().sound(2462, 15)
			it.player().animate(2586, 15)
			it.delay(1)
			it.player().teleport(3236, 3403, 3)
			it.animate(2588)
			it.delay(1)
			it.player().animate(-1)
			it.addXp(Skills.AGILITY, 4.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
		})
		
		// Jump up
		r.onObject(10781, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() + Tile(0, 1))
			it.delay(1)
			it.player().sound(1936)
			it.player().animate(1603)
			it.player().forceMove(ForceMovement(0, 0, 0, 2, 8, 50, FaceDirection.NORTH))
			it.delay(1)
			it.player().teleport(3236, 3410, 3)
			it.addXp(Skills.AGILITY, 3.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
		})
		
		// Jump down from the roof
		r.onObject(10817, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() + Tile(0, 1))
			it.delay(1)
			it.player().animate(741)
			it.player().forceMove(ForceMovement(0, 0, 0, 1, 15, 30, FaceDirection.NORTH))
			it.delay(1)
			it.player().teleport(3236, 3416, 3)
			it.delay(1)
			it.player().sound(2462, 15)
			it.player().animate(2586, 15)
			it.delay(1)
			it.player().teleport(3236, 3417, 0)
			it.player().animate(2588)
			it.addXp(Skills.AGILITY, 125.0)
			it.player().unlock()
			
			
			// Woo! A pet!
			val odds = (26000.00 * it.player().mode().skillPetMod()).toInt()
			if (it.player().world().rollDie(odds, 1)) {
				UnlockAgilityPet.unlockGiantSquirrel(it.player())
			}
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 30)
		})
	}
	
}
