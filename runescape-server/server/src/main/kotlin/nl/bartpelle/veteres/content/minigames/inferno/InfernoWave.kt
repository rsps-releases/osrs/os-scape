package nl.bartpelle.veteres.content.minigames.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.npcs.inferno.JalImKot
import nl.bartpelle.veteres.content.npcs.inferno.TzKalZuk
import nl.bartpelle.veteres.content.npcs.inferno.controllers.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import java.util.*

/**
 * Created by Mack on 7/25/2017.
 */
class InfernoWave(vararg var entries: InfernoContext.InfernoWaveEntry) {
	
	/**
	 * The amount of nibblers present in this wave.
	 */
	var nibblers = 0
	
	/**
	 * The amount of blobs present in this wave.
	 */
	var blobs = 0
	
	/**
	 * The amount of meleers present in this wave.
	 */
	var meleers = 0
	
	/**
	 * The amount of magers present in this wave.
	 */
	var magers = 0
	
	/**
	 * The amount of rangers present in this wave.
	 */
	var rangers = 0
	
	/**
	 * The amount of jads present in this wave.
	 */
	var jads = 0
	
	/**
	 * The current npc we are attempting to parse from the array.
	 */
	var npc: Optional<Npc> = Optional.empty()
	
	/**
	 * Spawns the current wave entries onto the map. We also utilize this map to render any on screen visuals
	 * we may need to as the inferno requires a few.
	 */
	@Suspendable fun spawnWave(player: Player, session: InfernoSession) {
		val tiles: ArrayList<Tile> = InfernoContext.tileSpawns(player)
		var buf = 0
		
		for (wave: InfernoContext.InfernoWaveEntry in entries) {
			for (i in 1..wave.count) {
				
				if (!session.active) {
					continue
				}
				
				this.npc = Optional.of(Npc(wave.npcId, player.world(), tiles[i-1]))
				
				if (npc.get().id() == InfernoContext.JALTOK_JAD) {
					jads++
					npc.get().putattrib(AttributeKey.INFERNO_CONTROLLER, JalTokJadController(npc.get(), session, jads))
				}
				
				if (npc.get().id() == InfernoContext.TZKAL_ZUK) {
					npc.get().putattrib(AttributeKey.INFERNO_CONTROLLER, TzKalZukController(npc.get(), session))
				}
				
				if (npc.get().id() == InfernoContext.BLOB) {
					blobs++
					npc.get().putattrib(AttributeKey.INFERNO_CONTROLLER, JalAkController(npc.get(), session, blobs))
				}
				
				if (npc.get().id() == InfernoContext.NIBBLER) {
					nibblers++
					npc.get().putattrib(AttributeKey.INFERNO_CONTROLLER, JalNibController(npc.get(), session, nibblers))
				}
				
				if (npc.get().id() == InfernoContext.RANGER) {
					rangers++
					npc.get().putattrib(AttributeKey.INFERNO_CONTROLLER, JalXilController(npc.get(), session, rangers))
				}
				
				if (npc.get().id() == InfernoContext.MAGER) {
					magers++
					npc.get().putattrib(AttributeKey.INFERNO_CONTROLLER, JalZekController(npc.get(), session, magers))
				}
				
				if (npc.get().id() == InfernoContext.MELEE) {
					meleers++
					npc.get().putattrib(AttributeKey.INFERNO_CONTROLLER, JalImKotController(npc.get(), session, meleers))
				}
				
				//Triple jad wave so let's adjust their speeds to make it reasonable to protect against.
				if (session.wave == 68) {
					buf += 2
					npc.get().combatInfo().attackspeed = (buf + (i * 2))
					EntityCombat.putCombatDelay(npc.get(), npc.get().combatInfo().attackspeed)
				}
				
				//Our actions we wish to handle on spawn of an inferno npc.
				if (npc.get().attribOr<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER, null) != null) {
					npc.get().attrib<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER).onSpawn()
				} else {
					npc.get().attack(player)
				}
				
				npc.get().walkRadius(200)
				npc.get().respawns(false)
				npc.get().putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 100)
				
				player.world().registerNpc(npc.get())
				
				//add to the collection
				session.activeNpcs.add(npc.get())
			}
		}
		jads = 0
		meleers = 0
		rangers = 0
		magers = 0
		nibblers = 0
		blobs = 0
	}
}