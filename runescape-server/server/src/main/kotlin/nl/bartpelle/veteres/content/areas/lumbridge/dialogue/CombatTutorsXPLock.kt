package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 22/12/2015.
 */

object CombatTutorsXPLock {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onLogin {
			if (VarbitAttributes.varbiton(it.player(), Varbit.XPLOCKED)) {
				it.player().message("<col=00FF00>Your experience is LOCKED.")
			}
		}
		
		arrayOf(3216, 3217, 3218).forEach { npcid ->
			
			r.onNpcOption1(npcid, s@ @Suspendable {
				if (it.player().world().realm().isPVP)
					return@s
				
				var locktype: String = "Lock"
				if (VarbitAttributes.varbiton(it.player(), Varbit.XPLOCKED)) {
					locktype = "Unlock"
				}
				val cbxp = if (it.player().world().realm().isOSRune) 500 else it.player().mode().combatXp()
				var xpm: String = "x1"
				if (VarbitAttributes.varbiton(it.player(), Varbit.XP_X1)) {
					xpm = "x" + cbxp + ".";
				}
				when (it.optionsTitled("Experience options", locktype + " experience", "Set XP multipler to " + xpm, "Reset a combat stat")) {
					1 -> {
						if (!VarbitAttributes.varbiton(it.player(), Varbit.XPLOCKED)) {
							VarbitAttributes.set(it.player(), Varbit.XPLOCKED, 1)
							locktype = "locked."
						} else {
							VarbitAttributes.set(it.player(), Varbit.XPLOCKED, 0)
							locktype = "unlocked."
						}
						it.chatNpc("You experience is now " + locktype, npcid)
					}
					2 -> {
						if (it.player().world().realm().isPVP) {
							return@s
						}
						if (VarbitAttributes.varbiton(it.player(), Varbit.XP_X1)) {
							VarbitAttributes.set(it.player(), Varbit.XP_X1, 0)
							xpm = "x" + cbxp + ".";
						} else {
							VarbitAttributes.set(it.player(), Varbit.XP_X1, 1)
							xpm = "x1."
						}
						it.chatNpc("Your experience multiplier is now " + xpm, npcid)
					}
					3 -> reset1(it, npcid)
				}
			})
		}
	}
	
	@Suspendable fun reset1(it: Script, npcid: Int) {
		when (it.options("Attack", "Strength", "Defence", "More")) {
			4 -> reset2(it, npcid)
			1 -> resetId(it, 0, npcid)
			2 -> resetId(it, 2, npcid)
			3 -> resetId(it, 1, npcid)
		}
	}
	
	@Suspendable fun reset2(it: Script, npcid: Int) {
		when (it.options("Range", "Magic", "Hitpoints", "Back")) {
			4 -> reset1(it, npcid)
			1 -> resetId(it, 4, npcid)
			2 -> resetId(it, 6, npcid)
			3 -> resetId(it, 3, npcid)
		}
	}
	
	@Suspendable fun resetId(it: Script, id: Int, npcid: Int) {
		it.chatNpc("What level would you like to reset your " + Skills.SKILL_NAMES[id] + " to?", npcid)
		take(it, it.inputInteger("Reset level to:"), id, npcid)
	}
	
	@Suspendable fun take(it: Script, lvl: Int, id: Int, npcid: Int) {
		if (lvl > it.player().skills().xpLevel(id)) {
			it.chatNpc("You cannot reset your level to higher than it already is.", npcid)
			reset1(it, npcid)
		} else if (lvl < 1) {
			it.chatNpc("Level must be at least 1.", npcid)
			reset1(it, npcid)
		} else if (lvl < 10 && id == 3) {
			it.chatNpc("Hitpoints cannot go under 10.", npcid)
			reset1(it, npcid)
		} else {
			it.chatNpc("Are you sure you want to reset " + Skills.SKILL_NAMES[id] + " to " + lvl + "?", npcid)
			when (it.options("Yes, reset " + Skills.SKILL_NAMES[id] + " to " + lvl + ".", "Never mind.")) {
				1 -> {
					for (item in it.player().equipment().items()) {
						if (item != null) {
							it.chatNpc("You cannot reset levels while wearing equipment", npcid)
							return
						}
					}
					it.player().skills().setXp(id, Skills.levelToXp(lvl).toDouble())
					it.chatNpc("Your " + Skills.SKILL_NAMES[id] + " has been reset to " + lvl + ".", npcid)
				}
				2 -> {
				}
			}
		}
	}
	
}
