package nl.bartpelle.veteres.content.areas.zeah.catacombs

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-02-23.
 */

object RopeLadder {
	
	val ROPE_LADDER = 28894
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(ROPE_LADDER) @Suspendable {
			val player = it.player()
			
			player.lock()
			player.animate(828)
			it.delay(1)
			player.teleport(Tile(1639, 3673))
			player.unlock()
		}
	}
	
}
