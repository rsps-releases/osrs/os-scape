package nl.bartpelle.veteres.content.events.christmas.carolschristmas.factoryworkers

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-15.
 */

object MarionetteToys {
	
	val RED_MARIONETTE = 7508
	val BLUE_MARIONETTE = 7509
	val GREEN_MARIONETTE = 7510
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishBillamsToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(GREEN_MARIONETTE, world, Tile(3164, 5339))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3164, 5335)
						s.delay(3)
						toy.pathQueue().interpolate(3161, 5335)
						s.delay(3)
						toy.pathQueue().interpolate(3161, 5331)
						s.delay(5)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			finishBillamsToy()
			it.onInterrupt { finishBillamsToy() }
		}
		
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishTragacanthsToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(RED_MARIONETTE, world, Tile(3161, 5336))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3161, 5331)
						
						s.delay(5)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			
			finishTragacanthsToy()
			it.onInterrupt { finishTragacanthsToy() }
		}
		
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishTragacanthsToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(BLUE_MARIONETTE, world, Tile(3161, 5339))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3161, 5331)
						
						s.delay(8)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			
			finishTragacanthsToy()
			it.onInterrupt { finishTragacanthsToy() }
		}
		
	}
}