package nl.bartpelle.veteres.content.skills.agility.rooftop

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jonathan on 4/29/2017.
 */
object CanifisRooftop {
	
	val MARK_SPOTS = arrayOf(Tile(3101, 3278, 3),
			Tile(3091, 3275, 3),
			Tile(3093, 3266, 3),
			Tile(3098, 3259, 3))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		/*      // Wall climb
			  r.onObject(10819) @Suspendable {
				  if (it.player().skills().level(Skills.AGILITY) >= 40) {
					  it.player().lockNoDamage()
					  it.delay(1)
					  it.player().animate(1765, 60)
					  it.player().forceMove(ForceMovement(0, -1, 0,0,30,60, FaceDirection.EAST))
					  it.player().forceMove(ForceMovement(2, 0, 0, 0, 60, 115, FaceDirection.EAST))
					  it.delay(5)
					  it.player().teleport(3506, 3492, 2)
					  it.animate(-1)
					  it.player().unlock()
					  it.addXp(Skills.AGILITY, 5.0)
	  
					  //MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 10)
				  } else {
					  it.message("You need at least 40 Agility to attempt this course.")
				  }
			  }
	  
			  // Tightrope
			  r.onObject(10820) @Suspendable {
				  it.player().lockNoDamage()
				  it.delay(1)
				  it.player().animate(1995, 20)
				  it.player().forceMove(ForceMovement(0, -1, 0, 0, 20, 40, FaceDirection.NORTH))
				  it.delay(3)
				  it.player().animate(2586, 15)
				  it.delay(1)
				  it.player().teleport(3503, 3504, 2)
				  it.animate(-1)
				  it.player().unlock()
				  it.addXp(Skills.AGILITY, 5.0)
			  }
	  
			  // Second tightrope
			  r.onObject(10821) @Suspendable {
				  it.player().lockNoDamage()
				  it.delay(1)
				  it.player().animate(2588, 0)
				  it.delay(1)
				  it.player().teleport(3492, 3504, 2)
				  it.animate(-1)
				  it.player().unlock()
				  it.addXp(Skills.AGILITY, 5.0)
			  }
	  
			  // Narrow wall
			  r.onObject(10077) @Suspendable {
				  it.delay(1)
				  it.player().lockNoDamage()
				  it.player().animate(753)
				  it.player().looks().render(757, 757, 756, 756, 756, 756, -1)
				  it.player().pathQueue().clear()
				  it.player().pathQueue().interpolate(3089, 3262, PathQueue.StepType.FORCED_WALK)
				  it.player().pathQueue().interpolate(3088, 3261, PathQueue.StepType.FORCED_WALK)
				  it.waitForTile(Tile(3088, 3261))
				  it.player().looks().resetRender()
				  it.animate(759)
				  it.delay(1)
				  it.player().unlock()
				  it.addXp(Skills.AGILITY, 7.0)
	  
				  MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 10)
			  }
	  
			  // Wall jump
			  r.onObject(10084) @Suspendable {
				  it.delay(1)
				  it.player().lockNoDamage()
				  it.player().pathQueue().step(3088, 3256)
				  it.player().animate(2583, 20)
				  it.player().forceMove(ForceMovement(0, 1, 25, 30))
				  it.delay(1)
				  it.player().animate(2585)
				  it.delay(1)
				  it.player().pathQueue().step(3088, 3255)
				  it.player().forceMove(ForceMovement(0, 1, 17, 26))
				  it.delay(1)
				  it.player().unlock()
				  it.addXp(Skills.AGILITY, 10.0)
	  
				  MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 10)
			  }
	  
			  // Gap jump
			  r.onObject(10085) @Suspendable {
				  it.delay(1)
				  it.player().lockNoDamage()
				  it.player().faceTile(Tile(3096, 3256))
				  it.delay(1)
				  it.player().animate(2586, 15)
				  it.delay(1)
				  it.player().teleport(Tile(3096, 3256, 3))
				  it.player().animate(2588)
				  it.delay(1)
				  it.player().animate(-1)
				  it.player().unlock()
				  it.addXp(Skills.AGILITY, 4.0)
	  
				  MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 10)
			  }
	  
			  // Crate jump
			  r.onObject(10086) @Suspendable {
				  it.delay(1)
				  it.player().lockNoDamage()
				  it.player().animate(2586, 15)
				  it.delay(1)
				  it.player().teleport(Tile(3102, 3261, 1))
				  it.player().animate(2588)
				  it.delay(1)
				  it.player().animate(-1)
				  it.delay(1)
				  it.player().animate(2586, 15)
				  it.delay(1)
				  it.player().teleport(Tile(3103, 3261, 0))
				  it.player().animate(2588)
				  it.delay(1)
				  it.player().animate(-1)
				  it.player().unlock()
				  it.addXp(Skills.AGILITY, 79.0)
	  
				  // Woo! A pet!
				  val odds = (32000.00 * it.player().mode().skillPetMod()).toInt()
				  if (it.player().world().rollDie(odds, 1)) {
					  UnlockAgilityPet.unlockGiantSquirrel(it.player())
				  }
	  
				  MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 10)
			  }*/
	}
	
}