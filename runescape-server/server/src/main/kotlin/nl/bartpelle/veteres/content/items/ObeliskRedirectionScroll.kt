package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

object ObeliskRedirectionScroll {

    private const val SCROLL = 1505

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onItemOption1(SCROLL, s@ @Suspendable {
            val player = it.player()
            val unlocked = player.attribOr<Boolean>(AttributeKey.OBELISK_REDIRECTION_SCROLL, false)
            if(unlocked) {
                it.itemBox("You can make out some faded word on the ancient parchment. It's an archaic invocation of the gods. However there's nothing more for you to learn.", SCROLL)
                return@s
            }

            it.itemBox("You can make out some faded words on the ancient parchment. It appears to be an archaic invocation of the gods! Would you like to absorb its power?", SCROLL)
            if(it.optionsTitled("This will consume the scroll.", "Learn how to set Obelisk Destinations.", "Cancel") == 1) {
                if(player.inventory().remove(SCROLL, false).success()) {
                    player.putattrib(AttributeKey.OBELISK_REDIRECTION_SCROLL, true)
                    player.animate(7403)
                    it.itemBox("You study the scroll and learn how to set Obelisk Destinations.", SCROLL)
                }
            }
        })
    }

}