package nl.bartpelle.veteres.content.areas.falador

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/20/2015.
 */

object CraftingGuild {
	
	val DOOR = 14910
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(DOOR, s@ @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (obj.tile().equals(2933, 3289)) {
				if (it.player().tile().z >= 3289) {
					if (!(it.player().equipment().hasAt(EquipSlot.BODY, 1757) || it.player().equipment().hasAny(9780, 9781))) {
						it.chatNpc("Where's your brown apron? You can't come in here unless you're wearing one.", 5810)
						return@s
					}
				}
				
				it.player().lock()
				it.player().world().removeObj(obj)
				val o = MapObj(Tile(2933, 3288), 7128, obj.type(), 0)
				it.player().world().spawnObj(o)
				it.player().pathQueue().interpolate(2933, if (it.player().tile().z >= 3289) 3288 else 3289)
				
				if (it.player().tile().z >= 3289) {
					it.player().executeScript @Suspendable {
						it.chatNpc("Welcome to the Guild of Master Craftsmen.", 5810)
					}
				}
				
				it.delay(2)
				it.player().world().spawnObj(obj)
				it.player().world().removeObjSpawn(o)
				it.player().unlock()
			}
		})
		
		// Stairs down
		r.onObject(9584) {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (obj.tile().equals(2932, 3282, 1)) {
				it.player().teleport(2932, 3281, 0)
			} else if (obj.tile().equals(3026, 3248, 1)) { //Port Sarim
				it.player().teleport(3025, 3248, 0)
			} else if (obj.tile().equals(3024, 3261, 1)) { //Port Sarim
				it.player().teleport(3024, 3260, 0)
			}
		}
		
		// Stairs up
		r.onObject(9582) {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (obj.tile().equals(2931, 3282, 0)) {
				it.player().teleport(2933, 3282, 1)
			} else if (obj.tile().equals(3026, 3248, 0)) { //Port Sarim
				it.player().teleport(3026, 3247, 1)
			} else if (obj.tile().equals(3023, 3261, 0)) { //Port Sarim
				it.player().teleport(3025, 3261, 1)
			}
		}
	}
	
	
}