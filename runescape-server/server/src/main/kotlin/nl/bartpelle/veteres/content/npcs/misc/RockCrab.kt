package nl.bartpelle.veteres.content.npcs.misc

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.MultiwayCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 2017-01-17.
 */

object RockCrab {
	
	val ROCK_CRABS = intArrayOf(101, 103)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (ROCK_CRAB in ROCK_CRABS) {
			r.onNpcSpawn(ROCK_CRAB) @Suspendable { sleep(it.npc()) }
		}
	}
	
	@JvmField val deathScript: Function1<Script, Unit> = @Suspendable { sleep(it.npc()) }
	
	fun sleep(npc: Npc) {
		npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
			while (true) {
				s.delay(2)
				
				npc.world().players().forEachInAreaKt(npc.tile().area(1), { player ->
					if (!inCombat(player)) {
						when (npc.id()) {
							101 -> npc.sync().transmog(100)
							103 -> npc.sync().transmog(102)
						}
						npc.walkRadius(8)
						npc.attack(player)
					} else if (MultiwayCombat.includes(player)) {
						when (npc.id()) {
							101 -> npc.sync().transmog(100)
							103 -> npc.sync().transmog(102)
						}
						npc.walkRadius(8)
						npc.attack(player)
					}
				})
			}
		})
	}
	
	fun inCombat(entity: Entity): Boolean {
		val lastAttacked = System.currentTimeMillis() - entity.attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0L)
		return entity.timers().has(TimerKey.COMBAT_LOGOUT) || lastAttacked < 4000L
	}
}
