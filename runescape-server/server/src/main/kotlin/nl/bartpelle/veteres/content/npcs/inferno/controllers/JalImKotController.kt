package nl.bartpelle.veteres.content.npcs.inferno.controllers

import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Mack on 8/8/2017.
 */
class JalImKotController(melee: Npc, session: InfernoSession, offset: Int): InfernoNpcController {
	
	/**
	 * The session this npc is involved in.
	 */
	val session = session
	
	/**
	 * The owner of this npc controller
	 */
	val melee = melee
	
	/**
	 * The class pointer variable.
	 */
	var offset = offset
	
	/**
	 * The collection of spawn points for the npc.
	 */
	val spawns = arrayListOf(Tile(session.area!!.center().x - 7, session.area!!.center().z + 5), Tile(session.area!!.center().x + 4, session.area!!.center().z + 5))
	
	override fun onSpawn() {
		melee.tile(spawns[offset - 1])
		melee.attack(session.player())
	}
	
	override fun onDamage() {
		if (!InfernoContext.inSession(session.player())) {
			session.player().message("I don't think I should be doing this...")
			return
		}
		super.onDamage()
	}
}