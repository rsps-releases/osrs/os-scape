/**
 * Created by bart on 9/8/15.
 */
package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by bart on 9/8/15.
 */

object RingOfDueling {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (i in IntRange(2552, 2566).step(2)) { // Step because each id+1 is the noted charge version
			r.onItemOption4(i) @Suspendable { rub_duel_ring(it) }
			r.onEquipmentOption(1, i) @Suspendable { do_duel_ring(Tile(3315, 3235), it, true) }
			r.onEquipmentOption(2, i) @Suspendable { do_duel_ring(Tile(2440, 3090), it, true) }
			r.onEquipmentOption(3, i) @Suspendable { do_duel_ring(Tile(3388, 3160), it, true) }
			
		}
	}
	
	@Suspendable fun rub_duel_ring(script: Script) {
		script.message("You rub the ring...")
		
		when (script.optionsTitled("Where would you like to teleport to?", "Al Kharid Duel Arena.", "Castle Wars Arena.", "Clan Wars Arena.", "Nowhere.")) {
			1 -> do_duel_ring(Tile(3315, 3235), script)
			2 -> do_duel_ring(Tile(2440, 3090), script)
			3 -> do_duel_ring(Tile(3388, 3160), script)
		}
	}
	
	val ONE_CHARGE_RING = 2566
	val FULL_CHARGE = 2552
	
	@Suspendable fun do_duel_ring(target: Tile, it: Script, equipment: Boolean = false) {
		if (!Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
			return
		}
		DeadmanMechanics.attemptTeleport(it)
		
		val slot: Int = it.player()[AttributeKey.ITEM_SLOT]
		val item = (if (equipment) it.player().equipment() else it.player().inventory()) [slot]
		val charges = (7 - (item.id() - FULL_CHARGE) / 2) + 1
		
		Teleports.basicTeleport(it, it.player().world().randomTileAround(target, 2))
		
		if (charges == 1)
			it.message("Your ring of dueling crumbles to dust.".col("7F00FF"))
		else if (charges == 2)
			it.message("Your ring of dueling has one charge left.".col("7F00FF"))
		else
			it.message("Your ring of dueling has ${numToStr(charges - 1)} charges left.".col("7F00FF"))
		
		var result: Item? = Item(item.id() + 2)
		if (item.id() == ONE_CHARGE_RING)
			result = null
		
		if (equipment) {
			it.player().equipment().remove(Item(item.id()), true, EquipSlot.RING)
			it.player().equipment().add(result, true, EquipSlot.RING)
		} else {
			it.player().inventory().remove(Item(item.id()), true, slot)
			it.player().inventory().add(result, true, slot)
		}
		
		it.player().timers().cancel(TimerKey.FROZEN)
		it.player().timers().cancel(TimerKey.REFREEZE)
		it.player().write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
	}
	
	fun numToStr(num: Int): String = when (num) {
		2 -> "two"
		3 -> "three"
		4 -> "four"
		5 -> "five"
		6 -> "six"
		7 -> "seven"
		else -> "?"
	}
}