package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.sigmund

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import java.io.FileReader

/**
 * Created by Mack on 10/11/2017.
 */
object SigmundExchangeConstants {
	
	@JvmStatic var sigmundPrices = mapOf<Int, Int>()
	
	@JvmStatic fun load() {
		try {
			val typeOfHashMap = object : TypeToken<Map<Int, Int>>() {}.type
			sigmundPrices = Gson().fromJson<Map<Int, Int>>(FileReader("data/map/sigmund-pricing.json"), typeOfHashMap)
		} catch (e: Exception) {
			e.printStackTrace()
		}
	}
	
	/**
	 * A flag we use to check if a player has a current session with sigmund.
	 */
	fun hasActiveSession(player: Player): Boolean {
		return (player.attribOr<SigmundExchangeContainer>(AttributeKey.SIGMUND_EXCHANGE_SESSION, null) != null)
	}
	
	/**
	 * Gets the stashed session or returns null.
	 */
	fun getSession(player: Player): SigmundExchangeContainer {
		return player.attribOr<SigmundExchangeContainer>(AttributeKey.SIGMUND_EXCHANGE_SESSION, null)
	}
	
}