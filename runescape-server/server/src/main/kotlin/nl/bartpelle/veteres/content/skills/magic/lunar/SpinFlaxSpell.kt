package nl.bartpelle.veteres.content.skills.magic.lunar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.skills.magic.ProductionSpell
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.TimerKey

/**
 * @author Mack
 */
class SpinFlaxSpell: ProductionSpell(138, 76) {

    private val flax: Int = 1779

    @Suspendable override fun able(it: Script, alert: Boolean): Boolean {
        if (!it.player().inventory().has(flax)) {
            if (alert) it.messagebox("You do not have any flax to perform this spell.")
            return false
        }
        if (it.player().timers().has(TimerKey.NON_COMBAT_SPELL_TIMER)) {
            if (alert) it.message("Your last spell is still in progress!")
            return false
        }
        return super.able(it, alert)
    }

    @Suspendable override fun execute(it: Script) {
        if (!able(it, true)) return

        val player = it.player()
        val flaxCount = it.player().inventory().count(flax)
        val craftingXp = (flaxCount * 5).toDouble()

        player.animate(4413)
        player.graphic(748)

        MagicCombat.has(player, runes(), true)
        player.inventory().remove(Item(flax, if (flaxCount < 5) it.player().inventory().count(flax) else 5), true)

        player.skills().__addXp(Skills.MAGIC, 75.0)
        player.skills().__addXp(Skills.CRAFTING, craftingXp)
        player.timers().register(TimerKey.NON_COMBAT_SPELL_TIMER, 4)
    }

    override fun runes(): Array<Item> {
        return arrayOf(Item(556, 5), Item(9075, 2), Item(561, 2))
    }
}