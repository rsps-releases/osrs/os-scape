package nl.bartpelle.veteres.content.skills.farming

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.mechanics.bankobjects
import nl.bartpelle.veteres.content.skills.farming.patch.Allotments
import nl.bartpelle.veteres.content.skills.farming.patch.Herbs
import nl.bartpelle.veteres.content.skills.herblore.Cleaning
import nl.bartpelle.veteres.content.waitForInterfaceClose
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import java.util.*

/**
 * Created by Jak on 08/06/2016.
 */

object ToolLeprechaun {
	
	val LEPRECHAUN = 0
	val harvestedNotableItems = ArrayList<Int>()
	
	val REMOVE_TOOLS_INTERFACE = 125
	val ADD_TOOLS_INTERFACE = 126
	
	enum class FarmingStorage(val toolID: Int, val max_storage: Int, val varbit: Int, val add_interface_child: Int, val remove_interface_child: Int) {
		FARMING_RAKE(5341, 1, Varbit.FARMING_RAKE, 1, 3),
		SEED_DIBBER(5343, 1, Varbit.SEED_DIBBER, 2, 4),
		ALL_PURPOSE_SPADE(952, 1, Varbit.ALL_PURPOSE_SPADE, 3, 5),
		SECATEURS(5329, 1, Varbit.SECATEURS, 4, 6),
		WATERING_CAN(5331, 1, Varbit.WATERING_CAN, 5, 7),
		GARDENING_TROWEL(676, 1, Varbit.GARDENING_TROWEL, 6, 8),
		EMPTY_BUCKETS(1925, 255, Varbit.EMPTY_BUCKETS, 7, 9),
		NORMAL_COMPOST(6032, 255, Varbit.NORMAL_COMPOST, 8, 10),
		SUPER_COMPOST(6034, 255, Varbit.SUPER_COMPOST, 9, 11);
	}
	
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(LEPRECHAUN) @Suspendable { start_dialogue(it) }
		repo.onNpcOption2(LEPRECHAUN) @Suspendable { open_tools_interface(it) }
		
		FarmingStorage.values().forEach { tool ->
			repo.onButton(ADD_TOOLS_INTERFACE, tool.add_interface_child) @Suspendable {
				when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
					1 -> add_tools(it, tool.toolID, tool.max_storage, tool.varbit, 1)
					2 -> add_tools(it, tool.toolID, tool.max_storage, tool.varbit, 5)
					3 -> add_tools(it, tool.toolID, tool.max_storage, tool.varbit, 10)
					4 -> add_tools(it, tool.toolID, tool.max_storage, tool.varbit, it.inputInteger("Enter amount:"))
				}
			}
		}
		
		FarmingStorage.values().forEach { tool ->
			repo.onButton(REMOVE_TOOLS_INTERFACE, tool.remove_interface_child) @Suspendable {
				when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
					1 -> remove_tools(it, tool.toolID, tool.max_storage, tool.varbit, 1)
					2 -> remove_tools(it, tool.toolID, tool.max_storage, tool.varbit, 5)
					3 -> remove_tools(it, tool.toolID, tool.max_storage, tool.varbit, it.player().inventory().count(tool.toolID))
					4 -> remove_tools(it, tool.toolID, tool.max_storage, tool.varbit, it.inputInteger("Enter amount:"))
				}
			}
		}
		
		for (i in Allotments.Data.values()) {
			harvestedNotableItems.add(i.product)
		}
		for (i in Herbs.Data.values()) {
			harvestedNotableItems.add(i.product)
		}
		for (i in Cleaning.Herb.values()) {
			harvestedNotableItems.add(i.clean)
		}
		
		repo.onItemOnNpc(LEPRECHAUN, s@ @Suspendable {
			val player = it.player()
			val itemid = player.attrib<Int>(AttributeKey.ITEM_ID) ?: return@s
			if (itemid < 0) return@s
			val item = Item(itemid)
			
			// If the item requested IS a banknote..
			for (i in harvestedNotableItems) {
				val def: ItemDefinition = player.world().definitions().get(ItemDefinition::class.java, i)
				if (def.notelink == itemid) {
					it.chatNpc("That IS a banknote!", LEPRECHAUN, 575)
					return@s
				}
			}
			
			//If the item requested can't be noted..
			if (item.note(it.player().world()).id() == itemid) {
				it.chatNpc("Nay, I can't turn that into a banknote.", LEPRECHAUN, 610)
				return@s
			}
			
			//If the item requested is supported..
			for (i in harvestedNotableItems) {
				if (i == itemid) {
					if (bankobjects.offerToNoteExchange(it, itemid)) {
						return@s
					}
				}
			}
			
			//Else the item isn't supported, so send a message..
			it.chatNpc("Nay, I've got no banknotes to exchange for that item.", LEPRECHAUN, 610)
		})
		
	}
	
	@Suspendable fun remove_tools(it: Script, tool: Int, max_storage: Int, varbit: Int, amt: Int) {
		val item = Item(tool)
		val def = it.player().world().definitions().get(ItemDefinition::class.java, item.id())
		
		val aOrAny = if (max_storage > 1) "any" else "a"
		val sOrNo = if (def.name.equals("Bucket")) "s" else ""
		
		//If our player doesn't have the tool requested we..
		if (it.player().varps().varbit(varbit) == 0) {
			it.message("You haven't got $aOrAny ${def.name.toLowerCase()}$sOrNo stored in here.")
			return
		}
		
		//If our player doesn't have enough inventory slots..
		if (it.player().inventory().freeSlots() == 0) {
			it.message("You don't have enough free inventory spots to do this!")
			return
		}
		
		var amt = amt
		
		while (amt-- > 0) {
			if (it.player().varps().varbit(varbit) == 0)
				return
			if (it.player().inventory().freeSlots() == 0)
				return
			it.player().varps().varbit(varbit, it.player().varps().varbit(varbit) - 1)
			it.player().inventory().add(item, true)
		}
		
	}
	
	@Suspendable fun add_tools(it: Script, tool: Int, max_storage: Int, varbit: Int, amt: Int) {
		val item = Item(tool)
		val def = it.player().world().definitions().get(ItemDefinition::class.java, item.id())
		val max = if (max_storage == 1) "one" else "$amt"
		val aOrAny = if (max_storage > 1) "any" else "a"
		
		//If our player already has the max amount stored we..
		if (it.player().varps().varbit(varbit) == max_storage) {
			it.message("You cannot store more than $max ${def.name} in here.")
			return
		}
		
		//Does our player doesn't have the item we..
		if (!it.player().inventory().contains(item)) {
			it.message("You haven't got $aOrAny ${item.name(it.player().world())} to store.")
			return
		}
		
		var amt = amt
		
		while (amt-- > 0) {
			if (it.player().varps().varbit(varbit) == max_storage)
				return
			if (it.player().inventory().remove(item, false).success()) {
				it.player().varps().varbit(varbit, it.player().varps().varbit(varbit) + 1)
			}
		}
		
	}
	
	/**
	 * All the dialogue for the Tool Leprechaun
	 */
	@Suspendable fun start_dialogue(it: Script) {
		it.chatNpc("Ah, 'tis a foine day to be sure! Were yez wantin' me to<br>" +
				"store yer tools, or maybe ye might be wantin' yer stuff<br>back from me?", LEPRECHAUN, 569)
		when (it.optionsTitled("What would you like to say?", "Yes please.", "What can you store?", "What do you do with the tools you're storing?",
				"No thanks, I'll keep hold of my stuff.")) {
			1 -> open_tools_interface(it)
			2 -> what_can_you_store(it)
			3 -> what_do_you_do(it)
			4 -> no_thanks(it)
		}
	}
	
	@Suspendable fun what_can_you_store(it: Script) {
		it.chatPlayer("What can you store?", 588)
		it.chatNpc("We'll hold onto yer rake, yer seed dibber, yer spade,<br>" +
				"yer secateurs, yer waterin' can and yer trowel - but<br>mind it's not one of them fancy trowels only<br>" +
				"archaeologists use!", LEPRECHAUN, 570)
		it.chatNpc("We'll take a few buckets off yer hands too, and even<br>" +
				"yer compost and yer supercompost. There's room in<br>" +
				"our shed for plenty of compost, so bring it on!", LEPRECHAUN, 564)
		it.chatNpc("Also, if you hand me yer farming produce, I might be<br>" +
				"able to change it into banknotes.", LEPRECHAUN, 568)
		it.chatNpc("So... do ye want to be using the store?", LEPRECHAUN, 554)
		when (it.options("Yes please", "What do you do with the tools you're storing?", "No thanks, I'll keep hold of my stuff.")) {
			1 -> open_tools_interface(it)
			2 -> what_do_you_do(it)
			3 -> no_thanks(it)
		}
	}
	
	@Suspendable fun no_thanks(it: Script) {
		it.chatPlayer("No thanks, I'll keep hold of my stuff.", 588)
		it.chatNpc("Ye must be dafter than ye look if ye likes luggin' yer<br>tools everywhere ye goes!", LEPRECHAUN, 606)
	}
	
	@Suspendable fun what_do_you_do(it: Script) {
		it.chatPlayer("What do you do with the tools you're storing?<br>They can't possibly all fit in your pockets!", 555)
		it.chatNpc("We leprechauns have a shed where we keep 'em. It's a<br>magic shed, so ye can get yer items back from any of" +
				"<br>us leprechauns whenever ye want. Saves ye havin' to<br>carry loads of stuff around the country!", LEPRECHAUN, 570)
		it.chatNpc("So... do ye want to be using the store?", LEPRECHAUN, 554)
		when (it.options("Yes please", "What can you store?", "No thanks, I'll keep hold of my stuff.")) {
			1 -> open_tools_interface(it)
			2 -> what_can_you_store(it)
			3 -> no_thanks(it)
		}
	}
	
	@Suspendable fun open_tools_interface(it: Script) {
		
		it.player().interfaces().sendMain(REMOVE_TOOLS_INTERFACE)
		it.player().interfaces().sendInventory(ADD_TOOLS_INTERFACE)
		
		it.onInterrupt {
			it.player().interfaces().closeMain()
			it.player().interfaces().closeById(ADD_TOOLS_INTERFACE)
		}
		
		it.waitForInterfaceClose(REMOVE_TOOLS_INTERFACE)
		it.player().interfaces().closeMain()
		it.player().interfaces().closeById(ADD_TOOLS_INTERFACE)
	}
}