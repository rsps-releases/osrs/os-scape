package nl.bartpelle.veteres.content.minigames.bonuscontent

/**
 * Created by Bart on 12/15/2016.
 */
enum class BlessingGroup(val title: String, val desc: String) {
	
	NONE("None active", "There is currently no active blessing."),
	
	/* == Skills == */
	BLOODLUST("Bloodlust", "Double experience gained when fighting monsters in any combat style."),
	ARCANIST("Arcanist", "Double experience gained when casting non-combat Magic spells."),
	NECROMANCER("Necromancer", "Double experience gained for each bone buried. Only applies to burying."),
	MERCURIAL("Mercurial", "Double experience gained when training Agility."),
	HANDYMAN("Handyman", "Double experience gained when training Crafting."),
	HIGHSTRUNG("Highstrung", "Double experience gained when training Fletching."),
	BLOODSHED("Bloodshed", "Double experience gained when training Slayer."),
	ARTEMIS_GAZE("Artemis' Gaze", "Double experience gained when hunting creatures."),
	IRON_VEINS("Iron Veins", "Double experience gained when mining ores and essence."),
	FORGER("Forger", "Double experience gained when smelting and smithing."),
	TRITONS_GIFT("Triton's Gift", "Double experience gained when catching fish."),
	CHEFS_DELIGHT("Chef's Delight", "Double experience gained when training Cooking."),
	HEARTFIRE("Heartfire", "Double experience gained when training Firemaking."),
	LUMBERJACK("Lumberjack", "Double experience gained when training Woodcutting."),
	GREENFINGERS("Greenfingers", "Double experience gained when training Farming."),
	ALCHEMIST("Alchemist", "Double experience gained when crafting runes."),
	
	/* == Skill Benefits == */
	INFILTRATOR("Infiltrator", "Pickpocketing and stealing from stalls will go unnoticed."),
	SUNS_BOUNTY("Sun's Bounty", "50% chance to conserve a herb or secondary ingredient when brewing potions."),
	NATURES_GIFT("Nature's Gift", "Greatly increased Farming harvest, and 50% chance to conserve seeds."),
	PROSPECTOR("Prospector", "50% chance to mine two ores, increased mining speed and faster rock respawn."),
	GOLDSMITH("Goldsmith", "50% chance to conserve an ore or bar and increased smithing and smelting speed."),
	CLEAR_WATER("Clear Water", "50% chance to catch an extra fish and only the best available fish is caught."),
	CUISINIER("Cuisinier", "No food will be burned when cooking any type of food."),
	PYROMANIAC("Pyromaniac", "50% chance to conserve a log when burning."),
	BRIGHTWOOD("Brightwood", "50% chance to get an extra log when cutting wood."),
	PREDATOR("Predator", "Increased chance to attract prey to your Hunting traps."),
	FABLE("Fable", "Increased chance for superior slayer monsters to spawn, and increased chance to get a superior loot item."),
	RUNESMITH("Runesmith", "Double runes gained from Runecrafting."),
	
	/* == PVM related == */
	UNTAMED("Untamed", "Double boss monster drops, no kill count requirement for God Wars and increased boss pet drop rate."),
	TOMB_RAIDER("Tomb Raider", "Increased barrows loot chance and supplies amount."),
	
	/* == Misc. == */
	OBDURATE("Obdurate", "Run energy does not drain unless in the Wilderness."),
	DEVOTION("Devotion", "Prayer drains at half the normal drain rate unless in the Wilderness.")
	
}