package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.openInterface
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/17/2015.
 */

object InformationScroll {
	
	val INFORMATION_SCROLL = 24908
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(INFORMATION_SCROLL) @Suspendable {
			it.openInterface(412)
		}
	}
	
}
