/*
package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.script.ScriptRepository

*/
/**
 * Created by Bart on 11/1/2015.
 *//*

object WildernessLevelIndicatorRepo {
	
	// Every game cycle, depending on where the player is, show options like "challenge" or "attack"
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		r.onWorldInit() @Suspendable {
			val world = it.ctx<World>()
			fun cycle() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						s.onInterrupt { cycle() }
						
						world.players().forEachKt({ p ->
							WildernessLevelIndicator.areascycle(p)
						})
						if (s != null) // when server is rebuilt at runtime the script becomes invalid.
							s.delay(1)
						else {
							cycle()
						}
					}
				})
			}
			cycle()
			it.onInterrupt { cycle() }
		}
	}
	
}
*/
