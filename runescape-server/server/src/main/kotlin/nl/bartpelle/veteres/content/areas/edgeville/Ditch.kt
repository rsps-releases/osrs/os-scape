package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.events.tournament.EdgevilleTourny
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.wildernessWarning
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands

/**
 * Created by Bart on 8/23/2015.
 */

object Ditch {
	
	val WILDERNESS_DITCH = 23271
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		// Main ditch and also the vertical ditch north of Black Castle
		repo.onObject(WILDERNESS_DITCH, s@ @Suspendable {
			
			val obj = it.interactionObject()
			val lx = obj.tile().x and 63 // only works if instance origin bottom left is exact coord match to the real region otherwise relative position changes
			val lz = obj.tile().z and 63
			val dx = obj.tile().x - it.player().tile().x
			val dz = obj.tile().z - it.player().tile().z
			//it.player().debug("%d %d .. %d %d", lx, lz, dx, dz)
			
			if (it.player().world().allocator().active(it.player().tile()).isPresent) {
				if (!EdgevilleTourny.INVITED.contains(it.player().id())) {
					it.messagebox("You have to be invited by Staff to enter the battle arena.")
					return@s
				}
			}
			if (it.player().world().realm().isPVP && it.player().inventory().count(6685) > GameCommands.brewCap) {
				it.player().message("${it.player().inventory().count(6685)} brews is a little excessive don't you think? The wilderness limit is ${GameCommands.brewCap}.")
				return@s
			}
			
			// Which ditch?
			val mainDitch = obj.rot() == 0 || obj.rot() == 2// lz == 1
			val fromSafe = dz == 1
			
			val verticleObj = obj.rot() == 3 || obj.rot() == 1 // lx == 52
			val fromWild = dx == 1
			
			val moveX = if (mainDitch) 0 else if (fromWild) 3 else -3
			val moveZ = if (mainDitch) {
				if (fromSafe) 3 else -3
			} else 0
			val end = it.player().tile().transform(moveX, moveZ)
			val dir = if (mainDitch) {
				if (fromSafe) FaceDirection.NORTH else FaceDirection.SOUTH
			} else if (fromWild) FaceDirection.EAST else FaceDirection.WEST
			
			// Cannot enter the ditch with Blood Keys.
			if (mainDitch && dir == FaceDirection.NORTH) {
				for (item in it.player.inventory()) {
					if (item != null && BloodChest.isKey(item.id())) {
						it.player.message("You cannot enter the Wilderness with a Blood Key - it's cursed!")
						return@s
					}
				}
			}
			
			// Specific tiles
			
			if (!fromSafe || fromWild || it.wildernessWarning()) {
				// If we're not at the ditch.. we could have opened the wildy warning, done ::mb then clicked "ok, enter wildy"
				if (mainDitch || verticleObj) {
					it.player().lock()
					it.delay(1)
					it.player().sound(2462, 25)
					it.animate(6132)
					it.player().forceMove(ForceMovement(0, 0, moveX, moveZ, 33, 60, dir))
					it.delay(1)
					it.player().pathQueue().interpolate(end.x, end.z)
					it.delay(1)
					it.player().teleport(end.x, end.z)
					it.player().unlock()
					
					if (fromSafe) {
						// Clear damage tracking for this edge pker's next fight.
						// Stops people crying that someone else got loot because someone else did more dmg within last 10 mins.
						it.player().clearDamageTimes() // clear who damaged us for the next fight.
						it.player().clearDamagers()
					}
					
					// If we left the Wilderness with a key, broadcast victory!
					if (mainDitch && dir == FaceDirection.SOUTH) {
						for (item in it.player.inventory()) {
							if (item != null && BloodChest.isKey(item.id())) {
								it.player.world().filterableAnnounce("<col=6a1a18><img=50> " + it.player.name() + " has escaped the Wilderness successfully.")
								break
							}
						}
					}
				}
			}
		})
	}
	
}
