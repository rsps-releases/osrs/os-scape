package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jonathan on 6/3/2017.
 */
object RoyalSeedPod {
	
	val ROYAL_SEED_POD = 19564
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		// Commune
		repo.onItemOption1(ROYAL_SEED_POD, s@ @Suspendable {
			it.player().stopActions(true)
			if (!Teleports.canTeleport(it.player(), true, TeleportType.ABOVE_20_WILD)) return@s
			
			DeadmanMechanics.attemptTeleport(it)
			it.player().graphic(767)
			it.player().animate(4544)
			it.player().lockNoDamage()
			it.delay(3)
			it.player().looks().transmog(716)
			it.delay(1)
			it.player().teleport(if (PVPAreas.inEdgePvp(it.player()) && PVPAreas.edgevillePvpCorner != null) PVPAreas.resolveBasic(Tile(3093, 3495), PVPAreas.edgevillePvpCorner, PVPAreas.edgeville_map) else Tile(3092, 3500))
			it.delay(2)
			it.player().graphic(769)
			it.delay(2)
			it.player().looks().transmog(-1)
			it.player().animate(-1)
			it.player().timers().cancel(TimerKey.FROZEN)
			it.player().timers().cancel(TimerKey.REFREEZE)
			if(!it.player().world().realm().isPVP)
				it.player().skills().alterSkill(Skills.FARMING, -5)
			it.player().unlock()
			if(!it.player().world().realm().isPVP)
				it.messagebox("Plants seem hostile to you for killing seeds from the Grand Tree. <br>Your current farming level has been reduced by 5.")
		})
	}
}