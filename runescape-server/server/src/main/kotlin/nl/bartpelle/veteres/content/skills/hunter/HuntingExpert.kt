package nl.bartpelle.veteres.content.skills.hunter

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 21/09/2016.
 */
object HuntingExpert {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(1504) @Suspendable {
			it.chatNpc("Hello there " + it.player().name() + "! Interested in a bit of Hunter are we?", 1504)
			it.chatPlayer("We sure are! Can give you sell me some supplies?")
			it.chatNpc("I can do more than that my friend. If you use your hunting loot on me, I'll offer you some cash in exchange for your finds.", 1504)
			it.chatPlayer("I'll keep that in mind! So, how about this shop?")
			it.player().world().shop(45).display(it.player())
		}
		repo.onItemOnNpc(1504) @Suspendable {
			val item = it.itemUsed()
			if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				it.message("You can't do that on this World.")
			} else {
				val exchange = when (item.id()) {
					10033 -> 500
					10034 -> 1000
					11959 -> 1800
					else -> 0
				}
				if (exchange > 0) {
					val gold = exchange * item.amount()
					it.doubleItemBox("Exchange the chinchompas for $gold gold?", item, Item(995, gold))
					if (it.options("Exchange for $gold gold.", "No, I'll keep them.") == 1) {
						if (it.player().inventory().remove(item, false).success()) {
							it.player().inventory() += Item(995, gold)
						}
					}
				}
			}
		}
	}
}