package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 08/04/2017.
 */
object RingOfSuffering {
	
	@JvmStatic val RING_OF_SUFFERING = 19550
	@JvmStatic val SUFFERING_I = 19710
	@JvmStatic val SUFFERING_R = 20655
	@JvmStatic val SUFFERING_RI = 20657
	val RECOIL_RING = 2550
	val RECOIL_CHARGE_CAP = 100
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (rings in intArrayOf(RING_OF_SUFFERING, SUFFERING_I, SUFFERING_R, SUFFERING_RI)) {
			for (recoils in 2550..2551) { // unnoted and noted
				repo.onItemOnItem(recoils, rings, s@ @Suspendable {
					val recoilSlot = ItemOnItem.slotOf(it, recoils)
					val sufferSlot = ItemOnItem.slotOf(it, rings)
					val player = it.player()
					val recoil = player.inventory().get(recoilSlot) ?: return@s
					val suffer = player.inventory().get(sufferSlot) ?: return@s
					
					var toAdd = 1
					if (recoil.amount() > 1) {
						toAdd = Math.min(it.inputInteger("How many recoil rings do you want to store? (Maximum: $RECOIL_CHARGE_CAP)"), recoil.amount())
						if (toAdd == 0)
							return@s
					}
					val current = suffer.property(ItemAttrib.CHARGES)
					val maxcharges = RECOIL_CHARGE_CAP * 40
					var avail = maxcharges - current
					if (avail < 40 && current > 0) {
						it.messagebox("You can't store any more charges. The maximum is $maxcharges charges.")
						return@s
					}
					avail /= 40
					if (avail < toAdd) {
						toAdd = Math.min(toAdd, avail)
						it.message("You could only store up to ${toAdd * 40} more charges.")
					} else {
						it.message("You've added ${toAdd * 40} more charges to the ring of suffering.")
					}
					player.inventory().remove(Item(recoil, toAdd), false)
					val norm = suffer.id() == RING_OF_SUFFERING
					if (norm || suffer.id() == SUFFERING_I) {
						if (it.player().inventory().remove(suffer, false, sufferSlot).success()) {
							it.player().inventory().add(Item(if (norm) SUFFERING_R else SUFFERING_RI), true, sufferSlot)
							player.inventory().get(sufferSlot).property(ItemAttrib.CHARGES, current + (toAdd * 40))
						}
					} else {
						suffer.property(ItemAttrib.CHARGES, current + (toAdd * 40))
					}
				})
			}
		}
		
		for (ring in intArrayOf(RING_OF_SUFFERING, SUFFERING_R)) {
			for (currencies in intArrayOf(13307, 995)) {
				repo.onItemOnItem(currencies, ring, s@ @Suspendable {
					val player = it.player()
					val pvp = it.player().world().realm().isPVP
					val cost = if (pvp) "10,000 blood money" else "1 million coins"
					val currency = if (pvp) 13307 else 995
					val amt = if (pvp) 10000 else 1000000
					if (currencies != currency) {
						it.itemBox("The currency required to imbue a ring of suffering is ${Item(currency).name(player.world())}.", currency, amt)
						return@s
					}
					it.doubleItemBox("Would you like to pay $cost to imbue the Ring of Suffering?", Item(currency, amt), Item(ring))
					if (it.optionsTitled("Imbue the ring?", "Yes, imbue the ring for $cost", "Never mind.") == 1) {
						val ringslot = ItemOnItem.slotOf(it, ring)
						val oldring = player.inventory().get(ringslot)
						if (player.inventory().remove(Item(ring), false, ringslot).success() && player.inventory().remove(Item(currency, amt), false).success()) {
							val res = if (oldring.id() == RING_OF_SUFFERING) SUFFERING_I else SUFFERING_RI
							val newring = Item(res).duplicateProperties(oldring)
							player.inventory().add(newring, false, ringslot)
							player.message("You pay $cost to imbue the Ring of Suffering.")
						}
					}
				})
			}
		}
		
		for (rings in intArrayOf(SUFFERING_R, SUFFERING_RI)) {
			repo.onItemOption4(rings, s@ @Suspendable {
				val charges = it.itemUsed()?.property(ItemAttrib.CHARGES)
				it.message("The Ring of Suffering has $charges more recoil charges remaining.")
			})
		}
		
		for (imbued in intArrayOf(SUFFERING_I, SUFFERING_RI)) {
			repo.onItemOption3(imbued, s@ @Suspendable {
				it.message("This rings imbue cannot be removed. It can be traded when imbued.")
			})
		}
		
		
		for (rings in intArrayOf(SUFFERING_R, SUFFERING_RI)) {
			repo.onItemOption5(rings, s@ @Suspendable {
				toggleSetting(it, false)
			})
			
			repo.onEquipmentOption(2, rings) {
				toggleSetting(it, true)
			}
			
			repo.onEquipmentOption(1, rings, {
				val charges = it.player().equipment().get(it.itemUsedSlot())?.property(ItemAttrib.CHARGES)
				it.message("The Ring of Suffering has $charges more recoil charges remaining.")
			})
		}
		
	}
	
	@Suspendable private fun toggleSetting(it: Script, equipment: Boolean) {
		val player = it.player()
		val ring: Item = if (equipment) player.equipment().get(EquipSlot.RING) else player.inventory().get(it.itemUsedSlot())
		var disabled = VarbitAttributes.varbiton(player, Varbit.RING_OF_SUFFERING_RECOIL_DISABLED)
		val question = if (disabled) "on" else "off"
		val toggle: String = "Turn $question the ring's recoil effect."
		val charges = ring.propertyOr(ItemAttrib.CHARGES, 0) / 40
		val uncharge: String = if (charges > 0) "Extract $charges rings of recoil." else ""
		when (it.optionsNullableS("Select an Option", toggle, uncharge, "Never mind.")) {
			toggle -> {
				VarbitAttributes.toggle(player, Varbit.RING_OF_SUFFERING_RECOIL_DISABLED)
				it.message("The recoil ability of your ring of suffering is now: ${if (VarbitAttributes.varbiton(player, Varbit.RING_OF_SUFFERING_RECOIL_DISABLED)) "off" else "on"}.")
			}
			uncharge -> {
				if (player.inventory().add(Item(2551, charges), false).success()) {
					val refund = if (ring.id() == SUFFERING_R) RING_OF_SUFFERING else if (ring.id() == SUFFERING_RI) SUFFERING_I else RING_OF_SUFFERING
					if (equipment)
						player.equipment().set(EquipSlot.RING, Item(refund))
					else
						player.inventory().set(it.itemUsedSlot(), Item(refund))
					val punc = if (charges > 1) "s" else ""
					it.itemBox("You take $charges ring$punc of recoil from the ring.", RING_OF_SUFFERING)
				} else {
					it.message("You have no inventory space to remove the charges.")
				}
			}
		}
	}
}