package nl.bartpelle.veteres.content.items.combine

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 2/21/2016.
 */
object SlayerHelm {
	
	val NOSE_PEG = 4168
	val FACEMASK = 4164
	val GEM = 4155
	val EARMUFFS = 4166
	val BLACK_MASK = 8921
	val BLACK_MASK_IMBUE = 11784
	val SPINY_HELM = 4551
	val SLAYER_HELM = 11864
	val SLAYER_HELM_IMBUE = 11865
	
	val RED_SLAYER_HELM = 19647
	val RED_HELM_IMBUE = 19649
	val GREEN_SLAYER_HELM = 19643
	val GREEN_HELM_IMBUE = 19645
	val BLACK_SLAYER_HELM = 19639
	val BLACK_HELM_IMBUE = 19641
	val PURPLE_SLAYER_HELM = 21264
	val PURPLE_HELM_IMBUE = 21266
	val TURQUOISE_SLAYER_HELM = 21888
    val TURQUOISE_HELM_IMBUE = 21890

    private val ABYSSAL_HEAD = 7979
    private val ABYSSAL_HEAD_WRONG = 7986 // We gave out the wrong ID from the start..
    private val KQ_HEAD = 7981
    private val KBD_HEADS = 7980
	private val DARK_CLAW = 21275
    private val VORKATH_HEAD = 21907
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Combinations
		r.onItemOnItem(NOSE_PEG, FACEMASK) { makeMask(it) }
		r.onItemOnItem(NOSE_PEG, GEM) { makeMask(it) }
		r.onItemOnItem(NOSE_PEG, EARMUFFS) { makeMask(it) }
		r.onItemOnItem(NOSE_PEG, BLACK_MASK) { makeMask(it) }
		r.onItemOnItem(NOSE_PEG, BLACK_MASK_IMBUE) { makeMask(it, true) }
		r.onItemOnItem(NOSE_PEG, SPINY_HELM) { makeMask(it) }
		r.onItemOnItem(FACEMASK, GEM) { makeMask(it) }
		r.onItemOnItem(FACEMASK, EARMUFFS) { makeMask(it) }
		r.onItemOnItem(FACEMASK, FACEMASK) { makeMask(it) }
		r.onItemOnItem(FACEMASK, BLACK_MASK) { makeMask(it) }
		r.onItemOnItem(FACEMASK, BLACK_MASK_IMBUE) { makeMask(it, true) }
		r.onItemOnItem(FACEMASK, SPINY_HELM) { makeMask(it) }
		r.onItemOnItem(GEM, EARMUFFS) { makeMask(it) }
		r.onItemOnItem(GEM, BLACK_MASK) { makeMask(it) }
		r.onItemOnItem(GEM, BLACK_MASK_IMBUE) { makeMask(it, true) }
		r.onItemOnItem(GEM, SPINY_HELM) { makeMask(it) }
		r.onItemOnItem(EARMUFFS, BLACK_MASK) { makeMask(it) }
		r.onItemOnItem(EARMUFFS, BLACK_MASK_IMBUE) { makeMask(it, true) }
		r.onItemOnItem(EARMUFFS, SPINY_HELM) { makeMask(it) }
		r.onItemOnItem(SPINY_HELM, BLACK_MASK) { makeMask(it) }
		r.onItemOnItem(SPINY_HELM, BLACK_MASK_IMBUE) { makeMask(it, true) }
		
		// Colorizing
		r.onItemOnItem(ABYSSAL_HEAD, SLAYER_HELM) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.UNHOLY_HELMET) == 1) {
				it.player().inventory().remove(Item(ABYSSAL_HEAD), true)
				it.player().inventory().remove(Item(SLAYER_HELM), true)
				it.player().inventory().add(Item(RED_SLAYER_HELM), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		r.onItemOnItem(ABYSSAL_HEAD, SLAYER_HELM_IMBUE) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.UNHOLY_HELMET) == 1) {
				it.player().inventory().remove(Item(ABYSSAL_HEAD), true)
				it.player().inventory().remove(Item(SLAYER_HELM_IMBUE), true)
				it.player().inventory().add(Item(RED_HELM_IMBUE), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		r.onItemOnItem(ABYSSAL_HEAD_WRONG, SLAYER_HELM) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.UNHOLY_HELMET) == 1) {
				it.player().inventory().remove(Item(ABYSSAL_HEAD_WRONG), true)
				it.player().inventory().remove(Item(SLAYER_HELM), true)
				it.player().inventory().add(Item(RED_SLAYER_HELM), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		r.onItemOnItem(ABYSSAL_HEAD_WRONG, SLAYER_HELM_IMBUE) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.UNHOLY_HELMET) == 1) {
				it.player().inventory().remove(Item(ABYSSAL_HEAD_WRONG), true)
				it.player().inventory().remove(Item(SLAYER_HELM_IMBUE), true)
				it.player().inventory().add(Item(RED_HELM_IMBUE), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		
		// Colorizing
		r.onItemOnItem(KBD_HEADS, SLAYER_HELM) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KING_BLACK_BONNET) == 1) {
				it.player().inventory().remove(Item(KBD_HEADS), true)
				it.player().inventory().remove(Item(SLAYER_HELM), true)
				it.player().inventory().add(Item(BLACK_SLAYER_HELM), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		r.onItemOnItem(KBD_HEADS, SLAYER_HELM_IMBUE) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KING_BLACK_BONNET) == 1) {
				it.player().inventory().remove(Item(KBD_HEADS), true)
				it.player().inventory().remove(Item(SLAYER_HELM_IMBUE), true)
				it.player().inventory().add(Item(BLACK_HELM_IMBUE), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		
		// Colorizing
		r.onItemOnItem(KQ_HEAD, SLAYER_HELM) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KALPHITE_KHAT) == 1) {
				it.player().inventory().remove(Item(KQ_HEAD), true)
				it.player().inventory().remove(Item(SLAYER_HELM), true)
				it.player().inventory().add(Item(GREEN_SLAYER_HELM), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		r.onItemOnItem(KQ_HEAD, SLAYER_HELM_IMBUE) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KALPHITE_KHAT) == 1) {
				it.player().inventory().remove(Item(KQ_HEAD), true)
				it.player().inventory().remove(Item(SLAYER_HELM_IMBUE), true)
				it.player().inventory().add(Item(GREEN_HELM_IMBUE), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		
		//Colorizing
		r.onItemOnItem(DARK_CLAW, SLAYER_HELM) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KING_BLACK_BONNET) == 1) { //TODO change this to proper varp for building
				it.player().inventory().remove(Item(DARK_CLAW), true)
				it.player().inventory().remove(Item(SLAYER_HELM), true)
				it.player().inventory().add(Item(PURPLE_SLAYER_HELM), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
		r.onItemOnItem(DARK_CLAW, SLAYER_HELM_IMBUE) {
			if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KING_BLACK_BONNET) == 1) { //TODO change this to proper varp for building
				it.player().inventory().remove(Item(DARK_CLAW), true)
				it.player().inventory().remove(Item(SLAYER_HELM_IMBUE), true)
				it.player().inventory().add(Item(PURPLE_HELM_IMBUE), true)
			} else {
				it.player().message("You need to unlock this ability from a Slayer master first!")
			}
		}
        r.onItemOnItem(VORKATH_HEAD, SLAYER_HELM) {
            if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KING_BLACK_BONNET) == 1) { //TODO change this to proper varp for building
                it.player().inventory().remove(Item(VORKATH_HEAD), true)
                it.player().inventory().remove(Item(SLAYER_HELM), true)
                it.player().inventory().add(Item(TURQUOISE_SLAYER_HELM), true)
            } else {
                it.player().message("You need to unlock this ability from a Slayer master first!")
            }
        }
        r.onItemOnItem(VORKATH_HEAD, SLAYER_HELM_IMBUE) {
            if (it.player().world().realm().isPVP || it.player().varps().varbit(Varbit.KING_BLACK_BONNET) == 1) { //TODO change this to proper varp for building
                it.player().inventory().remove(Item(VORKATH_HEAD), true)
                it.player().inventory().remove(Item(SLAYER_HELM_IMBUE), true)
                it.player().inventory().add(Item(TURQUOISE_HELM_IMBUE), true)
            } else {
                it.player().message("You need to unlock this ability from a Slayer master first!")
            }
        }
		
		// Disassembly
		r.onItemOption4(SLAYER_HELM) { disassemble(it, Item(SLAYER_HELM)) }
		r.onItemOption4(SLAYER_HELM_IMBUE) { disassemble(it, Item(SLAYER_HELM_IMBUE), null, true) }
		
		r.onItemOption4(RED_SLAYER_HELM) { disassemble(it, Item(RED_SLAYER_HELM), Item(ABYSSAL_HEAD)) }
		r.onItemOption4(RED_HELM_IMBUE) { disassemble(it, Item(RED_HELM_IMBUE), Item(ABYSSAL_HEAD), true) }
		
		r.onItemOption4(GREEN_SLAYER_HELM) { disassemble(it, Item(GREEN_SLAYER_HELM), Item(KQ_HEAD)) }
		r.onItemOption4(GREEN_HELM_IMBUE) { disassemble(it, Item(GREEN_HELM_IMBUE), Item(KQ_HEAD), true) }
		
		r.onItemOption4(BLACK_SLAYER_HELM) { disassemble(it, Item(BLACK_SLAYER_HELM), Item(KBD_HEADS)) }
		r.onItemOption4(BLACK_HELM_IMBUE) { disassemble(it, Item(BLACK_HELM_IMBUE), Item(KBD_HEADS), true) }
		
		r.onItemOption4(PURPLE_SLAYER_HELM) { disassemble(it, Item(PURPLE_SLAYER_HELM), Item(DARK_CLAW)) }
		r.onItemOption4(PURPLE_HELM_IMBUE) { disassemble(it, Item(PURPLE_HELM_IMBUE), Item(DARK_CLAW), true) }
	}
	
	fun disassemble(it: Script, remove: Item, additional: Item? = null) {
		disassemble(it, remove, additional, false)
	}
	
	fun disassemble(it: Script, remove: Item, additional: Item? = null, imbued: Boolean = false) {
		if (it.player().inventory().freeSlots() >= if (additional == null) 5 else 6) {
			// Delete helm
			it.player().inventory().remove(Item(remove), true)
			
			// Add components
			it.player().inventory().add(Item(NOSE_PEG), true)
			it.player().inventory().add(Item(FACEMASK), true)
			it.player().inventory().add(Item(GEM), true)
			it.player().inventory().add(Item(EARMUFFS), true)
			it.player().inventory().add(Item(if (imbued) BLACK_MASK_IMBUE else BLACK_MASK), true)
			it.player().inventory().add(Item(SPINY_HELM), true)
			
			if (additional != null) {
				it.player().inventory().add(additional, true)
			}
			
			it.message("You disassemble your Slayer helm.")
		} else {
			it.message("You don't have space to disassemble that item.")
		}
	}
	
	fun makeMask(it: Script) {
		makeMask(it, false)
	}
	
	fun makeMask(it: Script, imbued: Boolean = false) {
		// Got the skill unlocked?
		if (!it.player().world().realm().isPVP && it.player().varps().varbit(Varbit.SLAYER_UNLOCKED_HELM) != 1) {
			it.message("You have not unlocked the ability to combine these items.")
			return
		}
		
		if (!it.player().inventory().hasAll(NOSE_PEG, FACEMASK, GEM, EARMUFFS, if (imbued) BLACK_MASK_IMBUE else BLACK_MASK, SPINY_HELM)) {
			it.message("You need a nosepeg, facemask, earmuffs, spiny helmet, enchanted gem and a black mask in your inventory in order to construct a Slayer helm.")
		} else {
			if (it.player().skills().level(Skills.CRAFTING) < 55) {
				it.message("You need a Crafting level of 55 to make a Slayer helm.")
			} else {
				it.player().inventory().remove(Item(NOSE_PEG), true)
				it.player().inventory().remove(Item(FACEMASK), true)
				it.player().inventory().remove(Item(GEM), true)
				it.player().inventory().remove(Item(EARMUFFS), true)
				it.player().inventory().remove(Item(SPINY_HELM), true)
				it.player().inventory().remove(Item(if (imbued) BLACK_MASK_IMBUE else BLACK_MASK), true)
				it.player().inventory().add(Item(if (imbued) SLAYER_HELM_IMBUE else SLAYER_HELM), true)
				
				it.message("You combine the items into a Slayer helm.")
			}
		}
	}
	
}