package nl.bartpelle.veteres.content.combat.ranged

enum class JavelinDrawback(val arrow: Int, val gfx: Int, val projectile: Int) {
	BRONZE_JAVELIN(825, 219, 212),
	IRON_JAVELIN(826, 219, 212),
	STEEL_JAVELIN(827, 219, 212),
	MITHRIL_JAVELIN(828, 219, 212),
	ADAMANT_JAVELIN(829, 219, 212),
	RUNITE_JAVELIN(830, 219, 212),
	//POISONED
	BRONZE_JAVELIN_P1(831, 219, 212),
	IRON_JAVELIN_P1(832, 219, 212),
	STEEL_JAVELIN_P1(833, 219, 212),
	MITHRIL_JAVELIN_P1(834, 219, 212),
	ADAMANT_JAVELIN_P1(835, 219, 212),
	RUNITE_JAVELIN_P1(836, 219, 212),
	//P+
	BRONZE_JAVELIN_P2(5642, 219, 212),
	IRON_JAVELIN_P2(5643, 219, 212),
	STEEL_JAVELIN_P2(5644, 219, 212),
	MITHRIL_JAVELIN_P2(5645, 219, 212),
	ADAMANT_JAVELIN_P2(5646, 219, 212),
	RUNITE_JAVELIN_P2(5647, 219, 212),
	//P++
	BRONZE_JAVELIN_P3(5648, 219, 212),
	IRON_JAVELIN_P3(5649, 219, 212),
	STEEL_JAVELIN_P3(5650, 219, 212),
	MITHRIL_JAVELIN_P3(5651, 219, 212),
	ADAMANT_JAVELIN_P3(5652, 219, 212),
	RUNITE_JAVELIN_P3(5653, 219, 212),
}