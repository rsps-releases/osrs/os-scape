package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/1/2015.
 */

object VelrakTheExplorer {
	
	val VELRAK_THE_EXPLORER = 4925
	val CHARGED_BLACK_MASKS = 8901..8919
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(VELRAK_THE_EXPLORER) @Suspendable {
			it.chatNpc("I know a few places with altars powerful enough to change your spellbook.", VELRAK_THE_EXPLORER, 568)
			when (it.options("Can you show me?", "Good for you.", "Can you discharge a Black mask?")) {
				1 -> {
					it.chatPlayer("Can you show me?")
					it.chatNpc("Which one would you like to see?", VELRAK_THE_EXPLORER, 568)
					when (it.options("Ancient Magick Altar", "Lunar Spells Altar")) {
						1 -> {
							it.chatPlayer("Ancient Magick Altar, please!")
							it.chatNpc("Sure thing.", VELRAK_THE_EXPLORER, 568)
							teleport(it, 3233 + it.player().world().random(1), 9317 + it.player().world().random(1), 0)
						}
						2 -> {
							it.chatPlayer("Lunar Spells Altar, please!")
							it.chatNpc("Sure thing.", VELRAK_THE_EXPLORER, 568)
							teleport(it, 2147 + it.player().world().random(1), 3868 + it.player().world().random(1), 0)
						}
					}
				}
				2 -> {
					it.chatPlayer("Good for you.")
					it.chatNpc("Yeah.. heh.", VELRAK_THE_EXPLORER, 568)
				}
				3 -> {
					it.chatPlayer("Can you discharge a black mask?")
					it.chatNpc("Heh.. Ancient Magicks.. A spell.. Yes, hand me the mask.", VELRAK_THE_EXPLORER)
				}
			}
		}
		
		// Black mask discharging
		r.onItemOnNpc(VELRAK_THE_EXPLORER) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			
			// Can we discharge this?
			if (item !in CHARGED_BLACK_MASKS) {
				it.chatNpc("Ehh.. That's not the kind of Black mask I meant. Hand me a Black mask, and I'll be able to discharge it.", VELRAK_THE_EXPLORER)
			} else {
				it.chatNpc("Ah. Yes, a Black mask. I can discharge it for you.. Shall I?", VELRAK_THE_EXPLORER)
				if (it.options("Yes.", "Nevermind.") == 1) {
					it.chatPlayer("Yes, please. Here you go.")
					if (it.player().inventory().remove(Item(item), true).success()) {
						it.player().inventory().add(Item(8921), true)
					}
					it.itemBox("You hand your Black mask to Velrak, and he discharges it.", 8921)
				}
			}
		}
	}
	
	@Suspendable fun teleport(it: Script, x: Int, z: Int, level: Int) {
		it.delay(1)
		it.targetNpc()!!.sync().faceEntity(it.player())
		it.targetNpc()!!.sync().animation(1818, 1)
		it.targetNpc()!!.sync().graphic(343, 100, 1)
		it.player().graphic(342)
		it.player().animate(1816)
		it.player().graphic(110, 124, 100)
		it.player().lockNoDamage()
		it.delay(3)
		it.player().teleport(x, z, level)
		it.animate(-1)
		it.player().unlock()
	}
	
}
