package nl.bartpelle.veteres.content.events.christmas.carolschristmas.factoryworkers

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-15.
 */

object BouncyBallToys {
	
	val RED_BOUNCY_BALL = 7505
	val BLUE_BOUNCY_BALL = 7506
	val GREEN_BOUNCY_BALL = 7507
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishUlinasToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(GREEN_BOUNCY_BALL, world, Tile(3178, 5345))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3178, 5339)
						s.delay(6)
						toy.pathQueue().interpolate(3175, 5339)
						s.delay(3)
						toy.pathQueue().interpolate(3175, 5337)
						s.delay(2)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			finishUlinasToy()
			it.onInterrupt { finishUlinasToy() }
		}
		
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishCaveDylasToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(RED_BOUNCY_BALL, world, Tile(3181, 5345))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3181, 5339)
						s.delay(9)
						toy.pathQueue().interpolate(3175, 5339)
						s.delay(6)
						toy.pathQueue().interpolate(3175, 5337)
						s.delay(2)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			
			finishCaveDylasToy()
			it.onInterrupt { finishCaveDylasToy() }
		}
		
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishDylansToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(BLUE_BOUNCY_BALL, world, Tile(3181, 5348))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3181, 5339)
						s.delay(10)
						toy.pathQueue().interpolate(3175, 5339)
						s.delay(6)
						toy.pathQueue().interpolate(3175, 5337)
						s.delay(2)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			
			finishDylansToy()
			it.onInterrupt { finishDylansToy() }
		}
		
		
	}
}