package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Mack
 */
object Consumables {

    private const val VOTE_TICKET = 5020

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onItemOption1(VOTE_TICKET, @Suspendable {
            redeemVoteTicket(it)
        })
    }

    @Suspendable private fun redeemVoteTicket(it: Script) {
        it.itemBox("You are about to redeem this vote ticket in exchange for 1 credited vote point to your account. " +
                "This ticket will fade into dust upon use. Do you wish to continue?", VOTE_TICKET)
        when (it.optionsTitled("Redeem ticket?", "Yes, redeem this ticket for 1 vote point.", "No thanks, I'll keep the ticket.")) {
            1 -> {
                if (it.player().inventory().remove(VOTE_TICKET, false).failed()) {
                    return
                }

                it.player().modifyNumericalAttribute(AttributeKey.VOTING_POINTS, 1, 0)
                it.player().message("You have received 1 vote point. You now have a total of ${it.player().attribOr<Int>(AttributeKey.VOTING_POINTS, 0)} vote point(s).")
            }
        }
    }
}