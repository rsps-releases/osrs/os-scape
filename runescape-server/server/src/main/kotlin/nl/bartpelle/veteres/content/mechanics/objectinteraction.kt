package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.tzhaar.InfernoPool
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.items.RottenPotato
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.ChangeMapMarker
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 8/23/2015.
 */
@Suspendable class ObjectInteraction {
	
	@Suspendable companion object {
		
		val ITEM_ON_OBJ = -1
		
		@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
			val obj: MapObj? = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
			it.player().faceObj(obj) // Always face what you interacted with.
			
			// Cannot interact with objects when speared. #OSSRV-100
			if (it.player().timers().has(TimerKey.SPEAR)) {
				return@s
			}
			
			if (obj != null && obj.interactAble()) {
				var reachable: Boolean
				
				if (option == ITEM_ON_OBJ && it.player().privilege().eligibleTo(Privilege.ADMIN)) {
					val itemId = it.player().inventory()[it.player()[AttributeKey.ITEM_SLOT]]?.id() ?: -1
					if (itemId == 5733) {
						RottenPotato.used_on_object(it)
						return@s
					}
				}
				
				if (obj.id() == 11663) {
					it.player().world().server().scriptRepository().triggerObject(it.player(), obj, option)
					return@s
				}

				//occult altar
				if (obj.id() == 29150 && it.player().tile().distance(obj.tile()) <= 2) {
					it.player().world().server().scriptRepository().triggerObject(it.player(), obj, option)
				}
				
				if (obj.id() == InfernoPool.ID) {
					if (it.player().tile().distance(obj.tile()) <= 10) {
						it.player().world().server().scriptRepository().triggerObject(it.player(), obj, option)
						return@s
					}
				}
				
				if ((obj.type() == 10 && obj.tile() == it.player().tile())
						|| it.player().world().server().scriptRepository().isRemoteObject(obj.id())) // We are ontop of it
					reachable = true
				else {
					reachable = if (it.player().looks().trans() == 3008) it.player().walkTo(obj, PathQueue.StepType.FORCED_WALK)
					else it.player().walkTo(obj, PathQueue.StepType.REGULAR)
					it.player().write(ChangeMapMarker(it.player().pathQueue().peekLastTile()))
					
					val lastTile = it.player().pathQueue().peekLast()?.toTile() ?: it.player().tile()
					
					// Are we frozen?
					if (it.player().frozen()) { // Reset the path found above. The lastTile will now be player.tile, and we CAN reach the object.
						if (it.player().tile() != lastTile) {
							it.player().message("A magical force stops you from moving.")
							it.player().sound(154)
							reachable = false
							it.player().pathQueue().clear()
						}
					}

					it.waitForTile(lastTile)
				}
				
				// Did we arrive there?
				if (obj.valid(it.player().world())) { // Might be removed like a lever by the time we arrive there
					if (reachable) {
						//if (it.player().attribOr(AttributeKey.DEBUG, false))
						//    it.message("Obj %d rot %d type %d x %d z %d", obj.id(), obj.rot(), obj.type(), obj.tile().x, obj.tile().z)
						
						if (option == ITEM_ON_OBJ) {
							if (!it.player().world().server().scriptRepository().triggerItemOnObject(it.player(), obj))
								it.message("Nothing interesting happens.")
						} else {
							if (!it.player().world().server().scriptRepository().triggerObject(it.player(), obj, option))
								it.message("Nothing interesting happens.")
						}
					} else if (!it.player().frozen()) {
						//loyalty chest
						if (obj.id() == 12309 && it.player().tile().distance(obj.tile()) <= 2) {
							it.player().world().server().scriptRepository().triggerObject(it.player(), obj, option)
							return@s
						}
						it.message("You can't reach that!")
					}
				}
			}
		}
	}
}