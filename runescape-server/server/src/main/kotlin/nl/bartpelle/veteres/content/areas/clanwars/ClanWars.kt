package nl.bartpelle.veteres.content.areas.clanwars

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.fs.EnumDefinition
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.lang.ref.WeakReference

/**
 * Created by Bart on 5/31/2016.
 */
object ClanWars {
	
	val LOBBY = Area(3352, 3143, 3391, 3176)
	val ACTIVE = mutableSetOf<ClanWar>()
	
	val LOBBY_EXIT_TILE = Tile(3376, 3155) // Middle of the Clan Wars lobby.
	val CW_BANK_CENTER_TILE = Tile(3363, 3169) // Center tile of where players using the jail portal end up at
	
	val JAIL_EXIT_PORTALS = intArrayOf(26738, 26739)
	val FORFIET_PORTALS = intArrayOf(
			26727, 26728,
			26731, 26732,
			26733, 26734,
			26735, 26736,
			26737)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(91, 6) { toggle(it.player(), CWSettings.GAME_END, it.buttonSlot() / 3) }
		r.onButton(91, 10) { toggle(it.player(), CWSettings.ARENA, it.buttonSlot() / 3) }
		r.onButton(91, 14) { toggle(it.player(), CWSettings.STRAGGLERS, it.buttonSlot() / 3) }
		r.onButton(91, 19) { toggle(it.player(), CWSettings.MELEE, it.buttonSlot() / 3 - 1) }
		r.onButton(91, 20) { toggle(it.player(), CWSettings.RANGE, it.buttonSlot() / 3 - 1) }
		r.onButton(91, 21) { toggle(it.player(), CWSettings.MAGIC, it.buttonSlot() / 3 - 1) }
		r.onButton(91, 22) { toggle(it.player(), CWSettings.PRAYER, it.buttonSlot() / 3 - 1) }
		r.onButton(91, 23) { toggle(it.player(), CWSettings.FOOD, it.buttonSlot() / 3 - 1) }
		r.onButton(91, 24) { toggle(it.player(), CWSettings.DRINKS, it.buttonSlot() / 3 - 1) }
		r.onButton(91, 25) { toggle(it.player(), CWSettings.SPECIAL_ATTACKS, it.buttonSlot() / 3 - 1) }
		r.onButton(91, 17) {
			when (it.buttonSlot()) {
				0 -> toggle(it.player(), CWSettings.IGNORE_FREEZING)
				3 -> toggle(it.player(), CWSettings.PJ_TIMER)
				6 -> toggle(it.player(), CWSettings.SINGLE_SPELLS)
				9 -> toggle(it.player(), CWSettings.ALLOW_TRIDENT)
			}
		}
		
		// Accept button on first screen
		r.onButton(91, 27) {
			accept(it.player())
		}
		
		// Accept on second screen
		r.onButton(92, 6) {
			acceptLast(it.player())
		}
		
		// Accept on second screen
		r.onObject(26642) {
			val playerClan = ClanChat.current(it.player())
			
			if (playerClan.isPresent) {
				// Check if any of them has a war ongoing..
				if (hasWar(playerClan.get().ownerId())) {
					get(playerClan.get().ownerId())!!.join(it.player())
				} else {
					it.player().message("Your clan is not in battle!")
				}
			} else {
				it.player().message("You are not in a clan!")
			}
		}
		r.onObject(26643) {
			val playerClan = ClanChat.current(it.player()).get()
			
			// Check if any of them has a war ongoing..
			if (hasWar(playerClan.ownerId())) {
				get(playerClan.ownerId())!!.join(it.player())
			} else {
				it.player().message("Your clan is not in battle!")
			}
		}
		r.onObject(26644) {
			val playerClan = ClanChat.current(it.player()).get()
			
			// Check if any of them has a war ongoing..
			if (hasWar(playerClan.ownerId())) {
				get(playerClan.ownerId())!!.join(it.player())
			} else {
				it.player().message("Your clan is not in battle!")
			}
		}
		
		// Fire 5 tick timer to cycle all the wars
		r.onWorldInit {
			it.ctx<World>().timers().register(TimerKey.CLAN_WARS_CYCLE, 5)
		}
		
		r.onWorldTimer(TimerKey.CLAN_WARS_CYCLE) {
			it.ctx<World>().timers().register(TimerKey.CLAN_WARS_CYCLE, 5)
			
			for (war in ACTIVE) {
				try {
					war.cycle()
				} catch (e: Exception) {
					// TODO log
					e.printStackTrace()
				}
			}
		}
		
		// Exit portals & Jail portals
		for (i in FORFIET_PORTALS + JAIL_EXIT_PORTALS) {
			r.onObject(i) {
				it.player().teleport(it.player().world().randomTileAround(Tile(3364, 3168), 3))
			}
		}
	}
	
	//ONLY USE IF VALUES WILL BE 0 OR 1
	fun toggle(player: Player, setting: Int)
			= toggle(player, setting, if (player.varps().varbit(setting) != 0) 0 else 1)
	
	fun toggle(player: Player, setting: Int, value: Int) {
		var value = value
		val opponent = player.clanPartner() ?: return
		
		// Be sure we have the interface open
		if (!player.interfaces().visible(91) || !opponent.interfaces().visible(91)) {
			return
		}
		
		// Be sure we're both on the first screen...
		if (player.attribOr<Boolean>(AttributeKey.CLANWAR_SECOND_SCREEN, false) ||
				player.attribOr<Boolean>(AttributeKey.CLANWAR_SECOND_SCREEN, false)) {
			return
		}
		
		// Make sure the opponent's opponent is the player.
		if (opponent.clanPartner() != player) {
			return
		}
		
		if (setting == CWSettings.GAME_END) {
			if (value in CWSettings.KING_80..CWSettings.KING_6000 || value in CWSettings.ODDSKULL_100..CWSettings.ODDSKULL_500) {
				player.message("This game mode is not enabled. Your game end has be defaulted to Last team standing.")
				value = CWSettings.LAST_TEAM_STANDING
			}
		}
		if (setting == CWSettings.ARENA) {
			if (value == CWSettings.ETHEREAL) {
				player.message("This arena is not enabled. Your arena has be defaulted to Wasteland.")
				value = CWSettings.WASTELAND
			}
		}
		
		player.varps().varbit(setting, value)
		opponent.varps().varbit(setting, value)
		blockAccept(opponent)
		player.varps().varbit(CWSettings.ACCEPT, 0)
		opponent.varps().varbit(CWSettings.ACCEPT, 0)
	}
	
	fun accept(player: Player) {
		val opponent = player.clanPartner() ?: return
		
		// Be sure we have the interface open
		if (!player.interfaces().visible(91) || !opponent.interfaces().visible(91)) {
			return
		}
		
		// Just to be sure. The client blocks out for 5 seconds but rather safe than sorry.
		if (TimerKey.CLANWARS_MODIFIED in player.timers()) {
			return
		}
		
		player.varps().varbit(CWSettings.ACCEPT, 1)
		
		// Have we both accepted?
		if (opponent.varps().varbitBool(CWSettings.ACCEPT)) {
			val summary = generateSummary(player)
			
			proceedToOverview(player, summary)
			proceedToOverview(player.clanPartner()!!, summary)
		}
	}
	
	fun acceptLast(player: Player) {
		val opponent = player.clanPartner() ?: return
		
		// Be sure we have the interface open
		if (!player.interfaces().visible(92) || !opponent.interfaces().visible(92)) {
			return
		}
		
		player.varps().varbit(CWSettings.ACCEPT, 1)
		
		if (!ClanChat.current(player).isPresent) {
			player.message("You do not appear to be in a clan.")
			abortSecondStage(player)
			return
		}
		
		if (!ClanChat.current(opponent).isPresent) {
			abortSecondStage(opponent)
			return
		}
		
		// Have we both accepted?
		if (opponent.varps().varbitBool(CWSettings.ACCEPT)) {
			ACTIVE.clear()
			val playerClan = ClanChat.current(player).get()
			val enemyClan = ClanChat.current(opponent).get()
			
			// Check if any of them has a war ongoing..
			if (hasWar(playerClan.ownerId())) {
				player.message("Your clan already has a battle in progress on this world. Step through the portals to join it.")
				opponent.message("Your opponent already has a battle in progress on this world.")
				
				reset(player)
				reset(opponent)
				return
			} else if (hasWar(enemyClan.ownerId())) {
				opponent.message("Your clan already has a battle in progress on this world. Step through the portals to join it.")
				player.message("Your opponent already has a battle in progress on this world.")
				
				reset(player)
				reset(opponent)
				return
			}
			
			val war = ClanWar(player.world(), playerClan, enemyClan, player.varps().varp(92)) // TODO why was this 91?
			commenceFor(player, opponent, playerClan, war)
			commenceFor(opponent, player, enemyClan, war)
			
			war.commence(player, opponent)
			ACTIVE.add(war)
		}
	}
	
	fun commenceFor(player: Player, enemy: Player, clan: ClanChat, war: ClanWar) {
		player.interfaces().closeMain()
		
		// Reset states to normal
		player.varps().varbit(CWSettings.ACCEPT, 0)
		player.clearattrib(AttributeKey.CLANWAR_SECOND_SCREEN)
		
		player.message("Your clan is being invited to join you...")
		broadcastInvite(ClanChat.current(player).get(), player)
	}
	
	fun proceedToOverview(player: Player, summary: String) {
		val opponent = player.clanPartner() ?: return
		
		
		// Put in the second phase, and clear the acceptance
		player.varps().varbit(CWSettings.ACCEPT, 0)
		player.putattrib(AttributeKey.CLANWAR_SECOND_SCREEN, true)
		
		// Show the second screen :)
		player.interfaces().sendMain(92)
		player.interfaces().text(92, 2, "Clan Wars Setup: Challenging " + opponent.name())
		
		// Show the settings summarized
		player.invokeScript(554, 0, summary)
		
		// Add proper interrupting! :)
		player.executeScript @Suspendable {
			it.onInterrupt {
				abortSecondStage(it.player)
			}
			
			it.waitForInterfaceClose(92)
			abortSecondStage(it.player)
		}
	}
	
	fun generateSummary(player: Player): String {
		val v = player.varps()
		val defs = player.world().definitions()
		val builder = StringBuilder("<col=ffffff><u=ffffff>The Game:</u></col><br><br>")
		
		// The game:
		builder.append(defs.get(EnumDefinition::class.java, 375).getString(v.varbit(CWSettings.GAME_END)))
		builder.append("<br><br><col=ffffff><u=ffffff>")
		
		// The arena:
		builder.append("The Arena:</u></col> ")
		builder.append(defs.get(EnumDefinition::class.java, 929).getString(v.varbit(CWSettings.ARENA))).append("<br><br><col=ffffff><u=ffffff>")
		
		// The combat:
		builder.append("The Combat:</u></col><br><br><col=eeeeee>")
		
		// Melee:
		builder.append("Melee:</col> ")
		builder.append(defs.get(EnumDefinition::class.java, 914).getString(v.varbit(CWSettings.MELEE))).append("<br><col=eeeeee>")
		
		// Ranging:
		builder.append("Ranging:</col> ")
		builder.append(defs.get(EnumDefinition::class.java, 916).getString(v.varbit(CWSettings.RANGE))).append("<br><col=eeeeee>")
		
		// Magic:
		builder.append("Magic:</col> ")
		builder.append(defs.get(EnumDefinition::class.java, 918).getString(v.varbit(CWSettings.MAGIC))).append("<br><br><col=eeeeee>")
		
		// Prayers:
		builder.append("Prayer:</col> ")
		builder.append(defs.get(EnumDefinition::class.java, 920).getString(v.varbit(CWSettings.PRAYER))).append("<br><br><col=eeeeee>")
		
		// Food:
		builder.append("Food:</col> ")
		builder.append(defs.get(EnumDefinition::class.java, 922).getString(v.varbit(CWSettings.FOOD))).append("<br><col=eeeeee>")
		
		// Drinks:
		builder.append("Drinks:</col> ")
		builder.append(defs.get(EnumDefinition::class.java, 924).getString(v.varbit(CWSettings.DRINKS))).append("<br><br><col=eeeeee>")
		
		// Special attacks
		builder.append("Special attacks:</col><br>")
		builder.append(defs.get(EnumDefinition::class.java, 926).getString(v.varbit(CWSettings.SPECIAL_ATTACKS))).append("<br><br><col=ffffff><u=ffffff>")
		
		// Other settings:
		builder.append("Other settings:</u></col><br><br>")
		var makeline = false
		
		if (v.varbit(CWSettings.IGNORE_FREEZING) == 1) {
			builder.append("Spells such as Bind and Ice Barrage will not prevent their targets from moving. Their damage will be applied normally.")
			makeline = true
		}
		
		if (v.varbit(CWSettings.PJ_TIMER) == 1) {
			if (makeline) builder.append("<br><br>")
			builder.append("In single-way combat areas, players are protected from being attacked for 10 secs after they have been attacking someone else.")
			makeline = true
		}
		
		if (v.varbit(CWSettings.SINGLE_SPELLS) == 1) {
			if (makeline) builder.append("<br><br>")
			builder.append("Multi-target attacks, including chinchompas, will hit only one target, even in multi-way combat areas.")
			makeline = true
		}
		
		if (v.varbit(CWSettings.ALLOW_TRIDENT) == 1) {
			if (makeline) builder.append("<br><br>")
			builder.append("The Trident of the Seas can cast its spell against players.")
		}
		
		return builder.toString()
	}
	
	fun challenge(player: Player, otherPlayer: Player) {
		// Can we invite him? Can WE invite him?
		if (!ClanChat.current(player).isPresent) {
			player.message("You don't appear to be in a clan. This is Clan Wars, mmkay?")
			return
		}
		
		if (!ClanChat.current(otherPlayer).isPresent) {
			player.message("${otherPlayer.name()} doesn't appear to be in a clan.")
			return
		}
		
		val playerChat = ClanChat.current(player).get()
		val otherChat = ClanChat.current(otherPlayer).get()
		
		// You cannot invite someone from your own channel.
		if (otherChat == playerChat) {
			player.message("${otherPlayer.name()} is in your clan!")
			return
		}
		
		// Rank check..
		val playerMayInvite = playerChat.rankFor(player.id() as Int) in arrayOf(127, 5, 6) // Owner, captain general.
		val otherMayBeInvited = otherChat.rankFor(otherPlayer.id() as Int) in arrayOf(127, 5, 6) // Owner, captain general.
		
		if (!playerMayInvite) {
			player.message("Only clan-members with a rank of 'Captain' or higher may send challenges.")
			return
		}
		
		if (!otherMayBeInvited) {
			player.message("Only clan-members with a rank of 'Captain' or higher may accept challenges.")
			return
		}
		
		if (otherPlayer invited player) {
			if (!player.dead() && !otherPlayer.dead() && !player.locked() && !otherPlayer.locked()) {
				player.stopActions(true)
				otherPlayer.stopActions(true)
				
				// Commence!
				player.putattrib(AttributeKey.CLANWARS_TARGET, WeakReference(otherPlayer))
				otherPlayer.putattrib(AttributeKey.CLANWARS_TARGET, WeakReference(player))
				openSettings(player, otherPlayer)
				openSettings(otherPlayer, player)
			}
		} else {
			player.timers().extendOrRegister(TimerKey.CLANWAR_INVITATION_OPEN, 50)
			player.putattrib(AttributeKey.CLANWAR_PARTNER, otherPlayer.id())
			otherPlayer.write(AddMessage("${player.name()} wishes to challenge your clan to a Clan War.", AddMessage.Type.CLAN_WAR, player.name()))
			player.message("Sending Clan Wars challenge...")
		}
	}
	
	fun openSettings(player: Player, opponent: Player) {
		player.varps().varp(91, 0)
		player.varps().varp(92, 0)
		
		player.interfaces().text(91, 2, "Clan Wars Setup: Challenging ${opponent.name()}")
		player.interfaces().sendMain(91)
		
		// Unlock all the buttons *sigh*
		player.interfaces().setting(91, 6, 0, 71, 2)
		player.interfaces().setting(91, 19, 3, 8, 2)
		player.interfaces().setting(91, 20, 3, 8, 2)
		player.interfaces().setting(91, 21, 3, 14, 2)
		player.interfaces().setting(91, 22, 3, 11, 2)
		player.interfaces().setting(91, 23, 3, 8, 2)
		player.interfaces().setting(91, 24, 3, 8, 2)
		player.interfaces().setting(91, 25, 3, 11, 2)
		player.interfaces().setting(91, 14, 0, 5, 2)
		player.interfaces().setting(91, 10, 0, 41, 2)
		player.interfaces().setting(91, 17, 0, 11, 2)
		
		// Stop acceptance
		player.varps().varbit(CWSettings.ACCEPT, 0)
		player.clearattrib(AttributeKey.CLANWAR_SECOND_SCREEN)
		
		// Add proper interrupting! :)
		player.executeScript @Suspendable {
			it.onInterrupt {
				abortFirstStage(it.player)
			}
			
			it.waitForInterfaceClose(91)
			abortFirstStage(it.player)
		}
	}
	
	fun abortFirstStage(player: Player) {
		// Don't proceed if we just went onto the second screen.
		if (player.attribOr(AttributeKey.CLANWAR_SECOND_SCREEN, false)) {
			return
		}
		
		val opponent = player.clanPartner() ?: return
		player.message("You have cancelled the challenge.")
		opponent.message("${player.name()} cancelled the challenge.")
		
		player.interfaces().closeMain()
		opponent.interfaces().closeMain()
		
		reset(player)
		reset(opponent)
	}
	
	fun abortSecondStage(player: Player) {
		// Don't proceed if we closed the second screen (clears this one).
		if (!player.attribOr<Boolean>(AttributeKey.CLANWAR_SECOND_SCREEN, false)) {
			return
		}
		
		val opponent = player.clanPartner() ?: return
		player.message("You have cancelled the challenge.")
		opponent.message("${player.name()} cancelled the challenge.")
		
		player.interfaces().closeMain()
		opponent.interfaces().closeMain()
		reset(player)
		reset(opponent)
	}
	
	fun Player.clanPartner(): Player? = attrib<WeakReference<Player>>(AttributeKey.CLANWARS_TARGET)?.get()
	
	@JvmStatic fun hasWar(clanId: Int) = get(clanId) != null
	
	fun endWar(war: ClanWar) = ACTIVE.remove(war)
	
	@JvmStatic fun inInstance(entity: Entity): Boolean {
		val instance = entity.world().allocator().active(entity.tile())
		if (instance.isPresent) {
			return instance.get().identifier.orElse(null) == InstancedMapIdentifier.CLAN_WARS
		}
		
		return false
	}
	
	@JvmStatic fun get(clanId: Int): ClanWar? =
			ACTIVE.firstOrNull { it.attacker.ownerId() == clanId || it.opponent.ownerId() == clanId }
	
	fun blockAccept(player: Player) {
		player.invokeScript(591) // Shows the 5-second countdown
		player.timers()[TimerKey.CLANWARS_MODIFIED] = 9 // 5.2 seconds, closest to 5 we can get.
	}
	
	fun reset(player: Player) {
		player.clearattrib(AttributeKey.CLANWAR_SECOND_SCREEN)
		player.varps().varbit(CWSettings.ACCEPT, 0)
		player.clearattrib(AttributeKey.CLANWARS_TARGET)
		player.clearattrib(AttributeKey.CLANWAR_PARTNER)
	}
	
	fun displayStatusInterface(player: Player) {
		if (player.interfaces().resizable()) {
			player.interfaces().send(88, 161, 6, true)
		} else {
			player.interfaces().send(88, 548, 14, true)
		}
	}
	
	fun closeStatusInterface(player: Player) {
		player.interfaces().closeById(88)
	}
	
	fun broadcastInvite(channel: ClanChat, not: Player) {
		val worldId = not.world().id()
		for (member in channel.members()) {
			if (member.world == worldId && member.id != not.id()) {
				not.world().playerForId(member.id).ifPresent { it.message("<col=5f005f>Your clan has initiated a battle! Come to the Clan Wars challenge area to join it.") }
			}
		}
	}
	
	@JvmStatic
	@JvmOverloads
	fun checkRule(player: Player, rule: Int, expectedValue: Int = 1): Boolean {
		val clanChat = ClanChat.current(player)
		if (clanChat.isPresent) {
			val clanWars = get(clanChat.get().ownerId())
			if (clanWars != null && inInstance(player)) {
				return clanWars.setting(rule) == expectedValue
			}
		}
		return false
	}
	
	@JvmStatic
	fun endFor(player: Player, message: String, winner: Boolean) {
		player.stopActions(true)
		
		// Don't teleport someone who's not fighting
		if (inInstance(player)) {
			player.teleport(3376, 3155)
		}
		
		player.interfaces().sendMain(89)
		player.invokeScript(592, message, winner.toInt())
		
		Staking.heal_player(player)
		player.setRunningEnergy(100.0, true)
		
		// Close the overlay.
		closeStatusInterface(player)
	}
	
	infix fun Player.invited(invited: Player): Boolean {
		if (timers().has(TimerKey.CLANWAR_INVITATION_OPEN)) {
			val partnerId: Int = attribOr(AttributeKey.CLANWAR_PARTNER, -1)
			return partnerId == invited.id()
		}
		
		return false
	}
	
	fun Boolean.toInt(): Int = if (!this) 0 else 1
	
}