package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/4/2015.
 *
 * Note: the different in shops in stock for iron men is just pots/sharks supplies.
 */

object VarrockGeneralStore {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		for (SHOP_KEEPER in intArrayOf(508, 509)) {
			
			r.onNpcOption1(SHOP_KEEPER) @Suspendable {
				if (!it.player().world().realm().isPVP) {
					it.chatNpc("Can I help you at all?", SHOP_KEEPER, 567)
					when (it.options("Yes please. What are you selling?", "Supplies please.", "No thanks.")) {
						1 -> {
							if (it.player().ironMode() != IronMode.NONE) {
								it.player().world().shop(19).display(it.player())
							} else {
								it.player().world().shop(6).display(it.player())
							}
						}
						2 -> {
							if (it.player().ironMode() != IronMode.NONE) {
								it.player().message("Iron men cannot access the supplies shop.")
							} else {
								it.player().world().shop(28).display(it.player())
							}
						}
						3 -> {
							it.chatPlayer("No thanks.", 588)
						}
					}
				}
			}
			r.onNpcOption2(SHOP_KEEPER) {
				if (!it.player().world().realm().isPVP) {
					if (it.player().ironMode() != IronMode.NONE) {
						it.player().world().shop(19).display(it.player())
					} else {
						it.player().world().shop(6).display(it.player())
					}
				}
			}
			
			r.onNpcOption3(SHOP_KEEPER) {
				if (!it.player().world().realm().isPVP) {
					if (it.player().ironMode() != IronMode.NONE) {
						it.player().message("Iron men cannot access the supplies shop.")
					} else {
						it.player().world().shop(28).display(it.player())
					}
				}
			}
		}
	}
	
}
