package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-12.
 */

object GuildDoor {
	
	val GUILD_DOOR = 11665
	val REPLACEMENT_DOOR = 1539
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(GUILD_DOOR, s@ @Suspendable {
			val player = it.player()
			val door = it.interactionObject()
			
			//Is our player eligible to enter the guild?
			if (player.skills().level(Skills.RANGED) >= 40) {
				if (player.tile().x >= 2658) {
					if (!it.player().tile().equals(door.tile().transform(1, -1, 0))) {
						it.player().walkTo(door.tile().transform(1, -1, 0), PathQueue.StepType.FORCED_WALK)
						it.waitForTile(door.tile().transform(1, -1, 0))
					}
					it.player().lock()
					it.runGlobal(player.world()) @Suspendable {
						val world = player.world()
						val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
						val spawned = MapObj(Tile(2658, 3439), REPLACEMENT_DOOR, door.type(), 1)
						world.removeObj(old, false)
						world.spawnObj(spawned)
						it.delay(3)
						world.removeObjSpawn(spawned)
						world.spawnObj(old)
					}
					it.player().pathQueue().interpolate(2657, 3439, PathQueue.StepType.FORCED_WALK)
					it.waitForTile(Tile(2657, 3439))
					it.delay(1)
					it.player().unlock()
				} else {
					if (!it.player().tile().equals(door.tile().transform(-1, 1, 0))) {
						it.player().walkTo(door.tile().transform(-1, 1, 0), PathQueue.StepType.FORCED_WALK)
						it.waitForTile(door.tile().transform(-1, 1, 0))
					}
					it.player().lock()
					it.runGlobal(player.world()) @Suspendable {
						val world = player.world()
						val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
						val spawned = MapObj(Tile(2658, 3439), REPLACEMENT_DOOR, door.type(), 1)
						world.removeObj(old, false)
						world.spawnObj(spawned)
						it.delay(3)
						world.removeObjSpawn(spawned)
						world.spawnObj(old)
					}
					it.player().pathQueue().interpolate(2659, 3437, PathQueue.StepType.FORCED_WALK)
					it.waitForTile(Tile(2659, 3437))
					it.delay(1)
					it.player().unlock()
				}
			} else {
				invalidRequirements(it)
			}
		})
	}
	
	@Suspendable fun invalidRequirements(it: Script) {
		val player = it.player()
		
		player.world().npcs().forEachInAreaKt(player.tile().area(5), { doorman ->
			if (doorman.id() == RangingGuildDoorman.RANGING_GUILD_DOORMAN) {
				player.face(doorman)
				doorman.faceTile(player.tile())
				it.chatNpc("Hey you can't come in here. You must be a level 40<br>ranger to enter.", doorman.id(), 589)
			}
		})
	}
}