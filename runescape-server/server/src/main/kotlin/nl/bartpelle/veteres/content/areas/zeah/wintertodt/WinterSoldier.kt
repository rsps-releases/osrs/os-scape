package nl.bartpelle.veteres.content.areas.zeah.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/11/2016.
 */

object WinterSoldier {
	
	val WINTER_SOLDIER = 7379
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(WINTER_SOLDIER) @Suspendable {
			val random_chat = it.player().world().random(3)
			
			when (random_chat) {
				0 -> it.chatNpc("I'm hungry...", WINTER_SOLDIER, 610)
				1 -> it.chatNpc("It's so cold...", WINTER_SOLDIER, 610)
				2 -> it.chatNpc("This could be worse I guess. Oh wait... No it can't.", WINTER_SOLDIER, 610)
				3 -> it.chatNpc("Sometimes I wish this thing would just escape. At least<br>we'd have something to do then.", WINTER_SOLDIER, 611)
			}
		}
	}
}