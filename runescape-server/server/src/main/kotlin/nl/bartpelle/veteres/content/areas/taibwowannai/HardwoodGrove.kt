package nl.bartpelle.veteres.content.areas.taibwowannai

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jonathan on 3/13/2017.
 */
object HardwoodGrove {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		@Suspendable fun open(it: Script) {
			with(it.player().world()) {
				val doors = arrayOf(objById(9039, Tile(2817, 3084)),
						objById(9038, Tile(2817, 3083)))
				
				var walkTo = Tile(2816, 3084)
				var target = Tile(2816, 3084)
				
				when (it.interactionObject().id()) {
					9039, 9038 -> if (it.player().tile().x > 2816) {
						walkTo = it.interactionObject().tile()
						target = walkTo.transform(-1, 0)
					} else {
						walkTo = it.interactionObject().tile().transform(-1, 0)
						target = walkTo.transform(1, 0)
					}
					else -> it.player().tile()
				}
				
				it.waitForTile(walkTo)
				if (doors.valid(this) && doors.interactAble()) {
					doors.forEachIndexed { index, it ->
						it.interactAble(false)
						
						val rep = MapObj(it.tile(), it.id(), it.type(), index + index + 1).interactAble(false)
						doors[index] = it.replaceWith(rep, this)
					}
					
					it.player().lockMovement()
					it.player().pathQueue().clear()
					it.player().pathQueue().interpolate(target, PathQueue.StepType.FORCED_WALK)
					
					it.waitForTile(target)
					doors.forEach {
						removeObjSpawn(it).interactAble(true)
					}
					
					it.player().unlock()
				}
			}
		}
		
		repo.onObject(9038, ::open)
		repo.onObject(9039, ::open)
	}
	
	fun Array<MapObj>.interactAble() = all(MapObj::interactAble)
	
	fun Array<MapObj>.valid(w: World) = all { it.valid(w) }
}