package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.hitorigin.PoisonOrigin
import java.util.*

/**
 * Created by Situations on 2017-02-20.
 */

object AbyssalSirePhaseThree {
	
	@JvmStatic
	fun initPhaseThree(abyssalSire: Npc, targetedPlayer: Entity) {
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.SWITCHING_PHASE)
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_THREE_PART_ONE)
		
		abyssalSire.world().executeScript(@Suspendable { s ->
			val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
			val phaseThreeStartTile = Tile(abyssalSire.spawnTile().x, abyssalSire.spawnTile().z - 17)
			
			abyssalSire.sync().transmog(5889)
			abyssalSire.lockNoDamage()
			AbyssalSireTentacles.lockTentacles(abyssalSireTentacles)
			
			abyssalSire.pathQueue().clear()
			abyssalSire.pathQueue().interpolate(phaseThreeStartTile)
			s.waitForTile(phaseThreeStartTile, abyssalSire)
			
			abyssalSire.animate(7096)
			s.delay(3)
			
			abyssalSire.sync().transmog(5891)
			AbyssalSireTentacles.animateTentacles(abyssalSireTentacles, 7114)
			AbyssalSireTentacles.transmogTentacles(abyssalSireTentacles, 5912)
			AbyssalSireTentacles.removeClipping(abyssalSireTentacles)
			
			s.delay(2)
			
			abyssalSire.walkRadius(40)
			
			abyssalSire.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 40)
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
			
			abyssalSire.unlock()
			abyssalSire.noRetaliation(true)
			
			AbyssalSireTentacles.putCombatState(abyssalSireTentacles, AbyssalSireState.COMBAT)
			AbyssalSireTentacles.unlockTentacles(abyssalSireTentacles)
			
			for (i in 0..3) {
				summonSpawn(abyssalSire, targetedPlayer)
			}
			
		})
	}
	
	@JvmStatic
	fun initPhaseThreePartTwo(abyssalSire: Npc, targetedPlayer: Entity) {
		if (abyssalSire.attrib<AbyssalSirePhase>(AttributeKey.ABYSSAL_SIRE_PHASE) == AbyssalSirePhase.PHASE_THREE_PART_TWO) {
			return
		}
		
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.SWITCHING_PHASE)
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_THREE_PART_TWO)
		abyssalSire.world().executeScript(@Suspendable { s ->
			abyssalSire.lockNoDamage()
			
			abyssalSire.animate(7098)
			
			s.delay(2)
			
			abyssalSire.world().players().forEachInAreaKt(abyssalSire.tile().area(10), { p ->
				Teleports.basicTeleport(p, abyssalSire.tile())
			})
			
			s.delay(4)
			
			abyssalSire.animate(7099)
			
			s.delay(4)
			
			abyssalSire.world().players().forEachInAreaKt(abyssalSire.tile().area(3), { p ->
				p.hit(abyssalSire, abyssalSire.world().random(40..80))
			})
			
			s.delay(2)
			
			for (i in 0..abyssalSire.world().random(1..14)) {
				summonSpawn(abyssalSire, targetedPlayer)
			}
			
			abyssalSire.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 40)
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
			val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
			
			AbyssalSireTentacles.putCombatState(abyssalSireTentacles, AbyssalSireState.COMBAT)
			
			abyssalSire.unlock()
			abyssalSire.noRetaliation(true)
		})
	}
	
	@JvmStatic
	fun phaseOneAttack(script: Script, abyssalSire: Npc, targetedPlayer: Entity) {
		if (!abyssalSire.attribOr<Boolean>(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, false)) {
			summonPoisonousFumes(abyssalSire, targetedPlayer)
		}
		/*if (!abyssalSire.attribOr<Boolean>(AttributeKey.ABYSSAL_SIRE_SPAWNING_HEALING, false)) {
			heal(abyssalSire)
<<<<<<< Updated upstream
		}
		
		tentacleSwipe(script, abyssalSire, targetedPlayer)
=======
		}*/
	}
	
	@JvmStatic
	fun phaseTwoAttack(script: Script, abyssalSire: Npc, targetedPlayer: Entity) {
		abyssalSire.animate(7099)
		
		tentacleSwipe(script, abyssalSire, targetedPlayer)
		
		if (abyssalSire.world().random(5) == 1 && !abyssalSire.attribOr<Boolean>(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, false)) {
			summonPoisonousFumes(abyssalSire, targetedPlayer)
		}
	}
	
	private fun tentacleSwipe(script: Script, abyssalSire: Npc, targetedPlayer: Entity) {
		val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
		
		AbyssalSireTentacles.putCombatState(abyssalSireTentacles, AbyssalSireState.COMBAT)
		//Face the Abyssal Sire's tentacles to the player's tile
		AbyssalSireTentacles.facePlayer(abyssalSireTentacles, targetedPlayer)
		
		//Attempt to hit the player if they're within the tentacle's hit radius
		AbyssalSireTentacles.attemptToHitPlayer(script, abyssalSireTentacles, targetedPlayer)
	}
	
	private fun summonSpawn(abyssalSire: Npc, targetedPlayer: Entity) {
		val targetTile = abyssalSire.world().randomTileAround(abyssalSire.tile(), 2)
		val abyssalSireSpawn = Npc(5916, abyssalSire.world(), targetTile)
		val abyssalSireSpawns = abyssalSire.attribOr<ArrayList<Npc>>(AttributeKey.ABYSSAL_SIRE_SPAWNS, ArrayList<Npc>())
		
		abyssalSire.world().executeScript(@Suspendable { s ->
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, true)
			
			abyssalSireSpawn.respawns(false)
			abyssalSireSpawn.walkRadius(40)
			abyssalSireSpawn.putattrib(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, targetedPlayer)
			abyssalSireSpawn.world().registerNpc(abyssalSireSpawn.spawnDirection(6))
			abyssalSireSpawn.animate(4523, 5)
			
			s.delay(2)
			abyssalSireSpawn.attack(targetedPlayer)
			abyssalSireSpawns.add(abyssalSireSpawn)
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNS, abyssalSireSpawns)
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, false)
			abyssalSireSpawn.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWN_OWNER, abyssalSire)
		})
	}
	
	private fun summonPoisonousFumes(abyssalSire: Npc, targetedPlayer: Entity) {
		val targetsTile = targetedPlayer.tile()
		
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, true)
		
		abyssalSire.world().tileGraphic(1275, targetsTile, 0, 0)
		
		abyssalSire.world().executeScript(@Suspendable { s ->
			s.delay(3)
			
			for (i in 0..3) {
				if (targetedPlayer.tile().inSqRadius(targetsTile, 1)) {
					targetedPlayer.hit(PoisonOrigin(), targetedPlayer.world().random(IntRange(6, 28)))
							.type(Hit.Type.POISON).block(false)
					if (targetedPlayer.world().rollDie(3, 1))
						targetedPlayer.poison(8)
				}
				
				s.delay(1)
			}
			
			s.delay(5)
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, false)
		})
	}
	
	private fun heal(abyssalSire: Npc) {
		/*abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_HEALING, true)
		
		abyssalSire.world().executeScript(@Suspendable { s ->
			abyssalSire.heal(abyssalSire.world().random(3..6))
			
			s.delay(abyssalSire.world().random(2..3))
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_HEALING, false)
		})*/
	}
}
