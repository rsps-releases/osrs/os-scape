package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Jak on 08/04/2017.
 */
object EmptyCommandConfirm {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val p: Player = it.player()
		it.messagebox("If you empty your inventory any items that are not worth<br> blood money will be lost forever. Are you sure?")
		if (it.options("Yes, empty and permanently lose any items not worth BM.", "No!") == 1) {
			for (item in p.inventory().items()) {
				if (item == null) {
					continue
				}
				// Don't delete blood money items, fuck that.
				if (p.world().realm().isPVP && p.world().prices().containsKey(item.unnote(p.world()).id())) {
					p.message("Cannot destroy %d x %s as it is worth Blood Money.", item.amount(), item.definition(p.world()).name)
					continue
				}
				p.inventory().remove(item, true)
			}
		}
	}
}