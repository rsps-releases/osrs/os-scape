package nl.bartpelle.veteres.content.skills.thieving

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Jak on 23/08/2016.
 */
object ThievableChests {
	
	data class Loot(val id: Int, val min: Int, val max: Int)
	
	enum class Chests(val level: Int, val xp: Double, val respawnTime: Int, var loot: ArrayList<Any>, var tiles: ArrayList<Tile>) {
		COINS_10(13, 7.8, 7, arrayListOf(Item(995, 10)), arrayListOf(Tile(2671, 3299, 1), Tile(2612, 3314, 1))),
		NATURE(28, 25.0, 8, arrayListOf(Item(995, 3), Item(561)), arrayListOf(Tile(2671, 3301, 1), Tile(2614, 3314, 1))),
		COINS_50(43, 125.0, 50, arrayListOf(Item(995, 50)), arrayListOf(Tile(3188, 3962), Tile(3184, 3962), Tile(3193, 3962))),
		ARROWTIP(47, 150.0, 210, arrayListOf(Item(41, 5)), arrayListOf(Tile(0, 0))),
		DORGESH_KAAN(52, 200.0, 210, arrayListOf(Loot(995, 100, 300), Item(4548), Item(10981), Item(5013), Item(10192)), arrayListOf(Tile(0, 0))),
		BLOOD(59, 250.0, 135, arrayListOf(Item(995, 500), Item(565, 2)), arrayListOf(Tile(2586, 9737), Tile(2586, 9734))),
		PALADIN(72, 500.0, 400, arrayListOf(Item(995, 1000), Item(383), Item(449), Item(1623)), arrayListOf(Tile(2588, 3302), Tile(2588, 3291))),
		DORG_RICH(78, 650.0, 300, arrayListOf(Loot(995, 500, 2500), Item(1623), Item(1621), Item(1619), Item(1617),
				Item(1625), Item(1627), Item(1629), Item(4548), Item(5013), Item(10954), Item(10956), Item(10958),
				Item(2351), Item(10973), Item(10980)), arrayListOf(Tile(0, 0)));
		// ROGUE_CHEST - has its own impl for w1-3
		
		companion object {
			fun forTile(tile: Tile): Chests? {
				for (c in Chests.values()) {
					for (t in c.tiles)
						if (t.equals(tile))
							return c
				}
				return null;
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (chestId in arrayOf(11735, 11736, 11738)) {
			repo.onObject(chestId, s@ @Suspendable {
				val op = it.interactionOption()
				val chest: Chests? = Chests.forTile(it.interactionObject().tile())
				
				if (!Skills.disabled(it.player(), Skills.THIEVING)) {
					return@s
				}
				
				if (chest == null) {
					it.message("This chest cannot be opened.")
					return@s
				}
				
				if (op == 1) {
					it.player().lockDamageOk()
					it.message("You begin to open the chest...")
					it.delay(1)
					it.message("You trigger a trap!")
					it.delay(1)
					it.player().hit(null, it.player().world().random(2..3)).block(false).combatStyle(CombatStyle.GENERIC)
					it.player().unlock()
				} else if (op == 2) {
					it.player().lockDamageOk()
					it.message("You begin to open the chest...")
					it.delay(1)
					if (it.player().world().rollDie(100, 85)) {
						it.message("You successfully disarm the trap.")
						it.delay(1)
						val lootidx = it.player().world().random(chest.loot.size - 1)
						val lootval = chest.loot[lootidx]
						var loot: Item? = null
						if (lootval is Item)
							loot = Item(lootval)
						else if (lootval is Loot)
							loot = Item(lootval.id, it.player().world().random(lootval.min..lootval.max))
						loot ?: return@s
						if (it.player().inventory().add(Item(loot), false).failed()) {
							it.player().world().spawnGroundItem(GroundItem(it.player().world(), loot, it.player().tile(), it.player().id()))
						}
						it.message("You steal %d x %s.", loot.amount(), loot.name(it.player().world()))
					} else {
						it.message("You trigger a trap!")
						it.delay(1)
						it.player().hit(null, it.player().world().random(2..3)).block(false).combatStyle(CombatStyle.GENERIC)
						if (it.player().world().rollDie(100, 15)) {
							it.player().poison(2)
						}
					}
					it.player().unlock()
				}
			})
		}
	}
}