package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.areas.wilderness.CommandTeleportPrompt
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom
import nl.bartpelle.veteres.util.GameCommands

/**
 * Created by Bart on 6/15/2016.
 */

object NewWizardPVP {
	
	data class Teleport(val text: String, val x: Int = 0, val z: Int = 0, val height: Int = 0, val id: Int = -1, val instance: Boolean = false)
	
	//Categories
	var categoryList: Array<Teleport> = buildCategoryList()
	var wildernessCategories: Array<Teleport> = buildWildernessCategories()
	var minigameCategories: Array<Teleport> = buildMinigameCategories()
	var monsterCategories: Array<Teleport> = buildMonsterCategories()
	
	//Teleport Lists
	var obeliskTeleports: Array<Teleport> = buildObeliskTeleports()
	var dragonTeleports: Array<Teleport> = buildDragonTeleports()
	var miscTeleports: Array<Teleport> = buildMiskTeleports()
	var pvpTeleports: Array<Teleport> = buildPvpTeleports()
	
	var safeMonsterTeleports: Array<Teleport> = buildSafeMonsterTeleports()
	var dangerousMonsterTeleports: Array<Teleport> = buildDangerousMonsterTeleports()
	
	
	var computedString: String = ""

/*	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		buildCategories()

		r.onNpcOption1(4159) @Suspendable { wizardCategories(it) }

		r.onNpcOption2(4159) @Suspendable {
			val previousTeleport = it.player().attribOr<NewWizardPVP.Teleport>(AttributeKey.PREVIOUS_TELEPORT, null)

			if (previousTeleport == null) {
				it.messagebox("You haven't teleport anywhere yet.")
			} else {
				if (!previousTeleport.instance)
					teleport(it, previousTeleport.x, previousTeleport.z, previousTeleport.height, false)
				else
					pvp_teleport(it, previousTeleport.id)
			}
		}
	}*/
	
	@JvmStatic fun buildCategories() {
		computedString = categoryList.map { it.text }.joinToString("|")
		categoryList = buildCategoryList()
	}
	
	fun buildCategoryList(): Array<Teleport> = arrayOf(
			Teleport("Wilderness Locations"),
			Teleport("Minigames"),
			Teleport("Monster Locations"))
	
	fun buildWildernessCategories(): Array<Teleport> = arrayOf(
			Teleport("Obelisks"),
			Teleport("Dragons"),
			Teleport("PVP Areas"),
			Teleport("Miscellaneous"))
	
	fun buildObeliskTeleports(): Array<Teleport> = arrayOf(
			Teleport("Lvl 35 Obelisk", 3097, 3799, 0),
			Teleport("Lvl 44 Obelisk", 2979, 3871, 0),
			Teleport("Lvl 50 Obelisk", 3307, 3916, 0))
	
	fun buildDragonTeleports(): Array<Teleport> = arrayOf(
			Teleport("East Dragons", 3364, 3666, 0),
			Teleport("West Dragons", 2983, 3598, 0))
	
	fun buildMiskTeleports(): Array<Teleport> = arrayOf(
			Teleport("Magebank", 2538, 4716, 0),
			Teleport("Demonic Ruins", 3287, 3886, 0),
			Teleport("Graveyard", 3160, 3676, 0))
	
	fun buildPvpTeleports(): Array<Teleport> = arrayOf(
			Teleport("Edgeville PvP - safe teleport", 1, 1, 0, 1, true),
			Teleport("Varrock PvP - dangerous teleport", 1, 0, 0, 2, true),
			Teleport("Canifis PvP - safe teleport", 1, 0, 0, 3, true),
			Teleport("Grand Exchange PvP - safe, optional non +1", 1, 0, 0, 4, true),
			Teleport("Camelot PvP - safe, optional non +1", 1, 0, 0, 5, true))
	
	fun buildMinigameCategories(): Array<Teleport> = arrayOf(
			Teleport("Warriors' Guild", 2879, 3546, 0),
			Teleport("Barrows", 3565, 3314, 0),
			Teleport("Duel Arena", 3367, 3267, 0),
			Teleport("Pest Control", 2659, 2676, 0),
			Teleport("Fight Caves", 2438, 5170, 0))
	
	
	fun buildMonsterCategories(): Array<Teleport> = arrayOf(
			Teleport("Safe Monsters"),
			Teleport("Dangerous Monsters"))
	
	fun buildDangerousMonsterTeleports(): Array<Teleport> = arrayOf(
			Teleport("Callisto", 3295, 3841, 0),
			Teleport("Chaos Fanatic", 2984, 3831, 0),
			Teleport("Venenatis", 3345, 3728, 0),
			Teleport("Crazy Archaeologist", 2968, 3695, 0),
			Teleport("Scorpia", 3236, 3946, 0),
			Teleport("Demonic Gorilla's", 3107, 3675, 0))
	
	fun buildSafeMonsterTeleports(): Array<Teleport> = arrayOf(
			Teleport("Corporeal Beast", 2968, 4383, 2),
			Teleport("Dagannoths", 1910, 4367, 0),
			Teleport("Zulrah", 2196, 3057, 0),
			Teleport("Lizardman Shaman", 1461, 3689, 0),
			Teleport("Abyssal Demon", 3424, 3550, 2),
			Teleport("Thermonuclear Smoke Devil", 2404, 9415, 0),
			Teleport("Godwars", 2882, 5310, 2),
			Teleport("Cave Krakens", 2276, 9988, 0),
			Teleport("Kalphite Queen", 3508, 9493, 0),
			Teleport("Cerberus", 1310, 1237, 0))
	
	@Suspendable fun teleport(it: Script, x: Int, z: Int, level: Int, quick_cast: Boolean) {
		it.player.interfaces().closeMain()
		
		if (GameCommands.pkTeleportOk(it.player(), x, z)) {
			if (!WildernessLevelIndicator.inWilderness(it.player().tile()) && WildernessLevelIndicator.inWilderness(Tile(x, z, level))) {
				// Graves or Demonic gorillas
				/* if ((x == 3160 && z == 3676) || (x == 3107 && z == 3675)) {
					 it.message("<col=FF0000>Warning!</col> The Zamorak Mage at 5 wilderness can no longer be used as an escape if teleblocked.")
					 it.messagebox("<col=FF0000>Warning!</col> The Zamorak Mage at 5 wilderness can no longer be used as an escape if teleblocked.")
				 }*/
				// Show enter wilderness conformation.
				if (!CommandTeleportPrompt.wantsToEnterWild(it)) {
					return
				}
			}
			if (quick_cast) {
				it.player().lockNoDamage()
				it.delay(1)
				it.player().graphic(1296)
				it.player().animate(3865)
				it.delay(3)
				it.player().teleport(x, z, 0)
				it.animate(-1)
				it.player().unlock()
			} else {
				it.targetNpc()!!.sync().faceEntity(it.player())
				it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
				it.targetNpc()!!.sync().animation(1818, 1)
				it.targetNpc()!!.sync().graphic(343, 100, 1)
				it.delay(1)
				it.player().graphic(1296)
				it.player().animate(3865)
				it.player().lockNoDamage()
				it.delay(3)
				it.player().teleport(x, z, level)
				it.animate(-1)
				it.player().unlock()
			}
		}
	}
	
	@Suspendable fun wizardCategories(it: Script) {
		val opt = OptionList.display(it, true, "Select a Category", *(categoryList.map { it.text }.toTypedArray()))
		if (opt != -1) {
			when (opt) {
				0 -> wildernessCategories(it)
				1 -> teleports(it, minigameCategories)
				2 -> monsterCategories(it)
			}
		}
	}
	
	@Suspendable fun wildernessCategories(it: Script) {
		val opt = OptionList.display(it, true, "Select a Category", *(wildernessCategories.map { it.text }.toTypedArray()))
		if (opt != -1) {
			when (opt) {
				0 -> teleports(it, obeliskTeleports)
				1 -> teleports(it, dragonTeleports)
				2 -> teleports(it, pvpTeleports)
				3 -> teleports(it, miscTeleports)
			}
		}
	}
	
	@Suspendable fun monsterCategories(it: Script) {
		val opt = OptionList.display(it, true, "Select a Category", *(monsterCategories.map { it.text }.toTypedArray()))
		if (opt != -1) {
			when (opt) {
				0 -> teleports(it, safeMonsterTeleports)
				1 -> teleports(it, dangerousMonsterTeleports)
			}
		}
	}
	
	@Suspendable fun teleports(it: Script, teleports: Array<NewWizardPVP.Teleport>) {
		val opt = OptionList.display(it, true, "Select a Location", *(teleports.map { it.text }.toTypedArray()))
		if (opt != -1 && opt < teleports.size) {
			val destination = teleports[opt]
			if (destination.x > 0) {
				if (!destination.instance)
					teleport(it, destination.x, destination.z, destination.height, false)
				else
					pvp_teleport(it, destination.id)
				
				it.player().putattrib(AttributeKey.PREVIOUS_TELEPORT, destination)
			} else {
				NewWizardPVP.wizard_teleport(it)
			}
			
		}
	}
	
	@Suspendable fun wizard_teleport(it: Script) {
		val opt = OptionList.display(it, true, "Select a Category", *(categoryList.map { it.text }.toTypedArray()))
		if (opt != -1 && opt < categoryList.size) {
			val destination = categoryList[opt]
			if (destination.x > 0) {
				if (!destination.instance)
					teleport(it, destination.x, destination.z, destination.height, false)
				else
					pvp_teleport(it, destination.id)
				
				it.player().putattrib(AttributeKey.PREVIOUS_TELEPORT, destination)
			} else {
				// when the player clicks an option on this suspendable displayed interface, it'll close it.
				// let's open it again for invalid clicks! :)
				NewWizardPVP.wizard_teleport(it)
			}
			
		}
	}
	
	@Suspendable fun pvp_teleport(it: Script, teleportId: Int) {
		val target: Tile? = when (teleportId) {
			1 -> PVPAreas.enter_tile_edge
			2 -> PVPAreas.enter_tile_varrock
			3 -> PVPAreas.enter_tile_canifis
			4 -> PVPAreas.GE_ENTER_TILE
			5 -> PVPAreas.CAMELOT_PVP_ENTER_TILE
			else -> it.player().tile()
		}
		if (target == null || target.equals(it.player().tile())) {
			it.messagebox("That PvP instance is currently unavailable. Apologies!")
			return
		}
		it.targetNpc()!!.sync().faceEntity(it.player())
		it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
		it.targetNpc()!!.sync().animation(1818, 1)
		it.targetNpc()!!.sync().graphic(343, 100, 1)
		it.delay(1)
		it.player().graphic(1296)
		it.player().animate(3865)
		it.player().lockNoDamage()
		it.delay(3)
		it.player().teleport(target)
		it.player().write(UpdateStateCustom(47))
		it.player().putattrib(AttributeKey.UPDATE_STATE_CUSTOM, true)
		it.message("Talk to <col=FF0000>Tafani</col> in the bank to return to the main world. Rushing & PJing is <col=FF0000>breaking the rules</col> in this area.")
		it.animate(-1)
		it.player().unlock()
	}
	
}