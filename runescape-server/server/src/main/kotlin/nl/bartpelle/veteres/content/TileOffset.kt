package nl.bartpelle.veteres.content

/**
 * Created by Jason MacKeigan on 2016-09-01 at 11:46 PM
 */
class TileOffset(val x: Int, val z: Int)

infix fun Int.to(that: Int) = TileOffset(this, that)