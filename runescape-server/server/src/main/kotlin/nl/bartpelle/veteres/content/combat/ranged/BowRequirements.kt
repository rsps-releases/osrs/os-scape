package nl.bartpelle.veteres.content.combat.ranged

import nl.bartpelle.veteres.model.item.Item

enum class BowRequirements(val weapon: Int, vararg private val shootable: Item) {
	//SHORTBOWS
	SHORT_BOW(841, Item(882), Item(884)),
	OAK_SHORTBOW(843, Item(882), Item(884)),
	WILLOW_SHORTBOW(849, Item(882), Item(884), Item(886)),
	MAPLE_SHORTBOW(853, Item(882), Item(884), Item(886), Item(888), Item(890)),
	YEW_SHORTBOW(857, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892)),
	MAGIC_SHORTBOW(861, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892)),
	MAGIC_SHORTBOW_I(12788, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892)),
	//LONGBOWS
	SHORT_LONGBOW(839, Item(882), Item(884)),
	OAK_LONGBOW(845, Item(882), Item(884)),
	WILLOW_LONGBOW(847, Item(882), Item(884), Item(886)),
	MAPLE_LONGBOW(851, Item(882), Item(884), Item(886), Item(888), Item(890)),
	YEW_LONGBOW(855, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892)),
	MAGIC_LONGBOW(859, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892)),
	//CROSSBOWS
	BRONZE_CROSSBOW(9174, Item(877)),
	IRON_CROSSBOW(9177, Item(877), Item(9140)),
	STEEL_CROSSBOW(9179, Item(877), Item(9140), Item(9141)),
	MITHRIL_CROSSBOW(9181, Item(877), Item(9140), Item(9141), Item(9142)),
	ADAMANT_CROSSBOW(9183, Item(877), Item(9140), Item(9141), Item(9142), Item(9143)),
	RUNE_CROSSBOW(9185, Item(877), Item(9140), Item(9141), Item(9142), Item(9143), Item(9144), Item(9236), Item(9237),
			Item(9238), Item(9239), Item(9240), Item(9241), Item(9242), Item(9243), Item(9244), Item(9245), Item(21932), Item(21934), Item(21936), Item(21938), Item(21940),Item(21942), Item(21944), Item(21946), Item(21948)),
	ARMADYL_CROSSBOW(11785, Item(877), Item(9140), Item(9141), Item(9142), Item(9143), Item(9144), Item(9236),
			Item(9237), Item(9238), Item(9239), Item(9240), Item(9241), Item(9242), Item(9243), Item(9244), Item(9245), Item(21932), Item(21934), Item(21936), Item(21938), Item(21940),Item(21942), Item(21944), Item(21946), Item(21948)),
	HUNTERS_CROSSBOW(10156, Item(10158)),
	KARIL_CROSSBOW(4734, Item(4740)),
	//MISC
	DARK_BOW(11235, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892), Item(11212)),
	DARK_BOW2(12765, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892), Item(11212)), // Dark bow cosmetic
	DARK_BOW3(12766, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892), Item(11212)), // Dark bow cosmetic
	DARK_BOW4(12767, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892), Item(11212)), // Dark bow cosmetic
	DARK_BOW5(12768, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892), Item(11212)), // Dark bow cosmetic
	SEERCULL(6724, Item(882), Item(884), Item(886), Item(888), Item(890), Item(892))
}