package nl.bartpelle.veteres.content.areas.clanwars

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.RANDOM
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.GAME_END
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_10000
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_25
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_6000
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_80
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.MOST_KILLS_120
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.MOST_KILLS_5
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.ODDSKULL_100
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.ODDSKULL_500
import nl.bartpelle.veteres.content.mechanics.Prayers
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.fs.VarbitDefinition
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.model.entity.player.Varps
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.map.MapObj

/**
 * Created by Bart on 6/1/2016.
 */
class ClanWar(val world: World, var attacker: ClanChat, var opponent: ClanChat, val settings: Int) {
	
	var ticksUntilStart = 200
		private set
	var totalTicks = 0
		private set
	
	var area: Area = Area(0)
	
	val map = ClanWarsMap[setting(CWSettings.ARENA)]
	
	val team1 = ClanWarTeam()
	val team2 = ClanWarTeam()
	
	val allParticipants = mutableSetOf<Player>() // All participants
	
	val allJailed = mutableSetOf<Player>() // All participants
	
	val gameType = when (setting(GAME_END)) {
		in KILL_25..KILL_10000 -> ClanWarCriteria.KILL_COUNT
		in KING_80..KING_6000 -> ClanWarCriteria.KING
		in MOST_KILLS_5..MOST_KILLS_120 -> ClanWarCriteria.MOST_KILLS
		in ODDSKULL_100..ODDSKULL_500 -> ClanWarCriteria.ODD_SKULL
		else -> ClanWarCriteria.LAST_TEAM_STANDING
	}
	
	private val barriers = mutableListOf<MapObj>()

	fun commence(attackingPlayer: Player, opponentPlayer: Player) {
		var attackingPlayer = attackingPlayer
		var opponentPlayer = opponentPlayer
		
		// Allocate the map now
		area = allocateMap(map)
		
		// You don't always start on the same sides
		val swap = attackingPlayer.world().random().nextBoolean()
		if (swap) {
			val temp = attackingPlayer
			attackingPlayer = opponentPlayer
			opponentPlayer = temp
			
			val temp2 = attacker
			attacker = opponent
			opponent = temp2
		}
		
		// Move the players to the starting tiles
		attackingPlayer.teleport(area.bottomLeft() + map.startTile1)
		opponentPlayer.teleport(area.bottomLeft() + map.startTile2)
		
		// Align the players to the right team
		attackingPlayer.varps().varbit(CWSettings.ACTIVE_TEAM, 1)
		opponentPlayer.varps().varbit(CWSettings.ACTIVE_TEAM, 2)
		
		createBarrier(attackingPlayer)
		createBarrier(opponentPlayer)
		
		Prayers.disableAllPrayers(attackingPlayer)
		Prayers.disableAllPrayers(opponentPlayer)
	}
	
	fun join(player: Player) {
		val chat = ClanChat.currentId(player)
		
		if (started() && gameType == ClanWarCriteria.LAST_TEAM_STANDING) {
			player.message("You can no longer join because the battle has already started!")
			return
		}
		if (chat == attacker.ownerId()) {
			player.teleport(area.bottomLeft() + map.startTile1)
			player.varps().varbit(CWSettings.ACTIVE_TEAM, 1)
			team1 += player
			Prayers.disableAllPrayers(player)
		} else if (chat == opponent.ownerId()) {
			player.teleport(area.bottomLeft() + map.startTile2)
			player.varps().varbit(CWSettings.ACTIVE_TEAM, 2)
			team2 += player
			Prayers.disableAllPrayers(player)
		} else {
			player.message("Your clan is not in battle!")
		}
	}
	
	/**
	 * Called every 5 ticks, processes all main logic.
	 */
	fun cycle() {
		if (ticksUntilStart > 0) {
			ticksUntilStart -= 5 // Remove 5, since we operate on 5 tick intervals.
			
			// Check if we're ready to start now
			if (ticksUntilStart <= 0) {
				ticksUntilStart = 0
				startFight()
			}
		}
		
		// Recompute participants so we work with accurate counts from this point onwards.
		recomputeParticipants()
		
		// Update the player counts and timer
		updateInfoPanel()
		
		// Check if the war is over.
		if (started()) {
			totalTicks++
			
			val winner = gameType.determineWinner(this)
			
			if (winner != null) {
				// We have a winner: teleport all contestants.
				for (p in allParticipants.plus(allJailed)) {
					p.stopActions(true)
					
					var win = false
					
					// Show the appropriate message.
					val message = when (winner) {
						-1 -> "Both teams ran out of fighters!"
						ClanChat.currentId(p) -> {
							win = true
							"Your team kicked the enemy right out of the arena!"
						}
						else -> "Your team was wiped out.<br>Better luck next time."
					}
					
					ClanWars.endFor(p, message, win)
				}
				
				// Broadcast to the clan chat for those not in the actual war.
				if (winner != -1) {
					val winnerChat = world.chats()[winner]
					winnerChat!!.members().forEach { friend ->
						world.playerForId(friend.id).ifPresent { friendPlayer ->
							if (friendPlayer !in allParticipants) {
								friendPlayer.message("<col=5F005F>Your clan was defeated in battle.")
							}
						}
					}
				}
				
				// Officially end the war.
				ClanWars.endWar(this)
			}
		}
	}
	
	fun startFight() = removeBarrier()
	
	fun createBarrier(player: Player) {
		if (!barriers.isEmpty()) {
			barriers.forEach {
				player.world().spawnObj(it, true)
			}
			return
		}
		
		val starts = mutableListOf(map.barrierStart)
		val ends = mutableListOf(map.barrierEnd)
		
		if (map == ClanWarsMap.CLAN_CUP) {
			starts.add(Tile(32, 8))
			ends.add(Tile(32, 32))
		}
		
		for (i in 0..starts.size - 1) {
			val start = starts[i]
			val end = ends[i]
			val startObject = map.barrierObj
			
			val increaseX = end.x - start.x != 0
			val stepMode = if (increaseX) start.x..end.x else start.z..end.z
			
			for (x in stepMode step 2) {
				val cur = if (increaseX) Tile(x, start.z) else Tile(start.x, x)
				val nxt = if (increaseX) Tile(x + 1, end.z) else Tile(end.x, x + 1)
				
				if (cur.x >= start.x && start.z >= start.z) {
					if (player.world().canWalk(area.relative(cur)) && !map.jail1.contains(area.relative(cur)) && !map.jail2.contains(area.relative(cur))) {
						val obj = MapObj(area.relative(cur), startObject, 10, RANDOM.nextInt(3))
						player.world().spawnObj(obj, true)
						barriers.add(obj)
					}
				}
				
				if (nxt.x <= end.x && nxt.z <= end.z) {
					if (player.world().canWalk(area.relative(nxt)) && !map.jail1.contains(area.relative(nxt)) && !map.jail2.contains(area.relative(nxt))) {
						val obj = MapObj(area.relative(nxt), startObject, 10, RANDOM.nextInt(3))
						player.world().spawnObj(obj, true)
						barriers.add(obj)
					}
				}
			}
		}
		
	}
	
	@Suspendable fun removeBarrier() {
		val destroyObject = map.barrierObj + 1
		
		world.executeScript {
			if (barriers.isNotEmpty()) {
				barriers.forEachIndexed { index, mapObj ->
					barriers[index] = mapObj.replaceWith(destroyObject, world)
				}
				
				it.delay(3)
				
				barriers.forEach {
					world.removeObjSpawn(it, true)
				}
				
				barriers.clear()
			}
		}
	}
	
	fun jail(player: Player) {
		val chat = ClanChat.currentId(player)
		
		if (chat == attacker.ownerId()) {
			player.teleport(area.relative(map.jail1().randomClippedTile(world)))
			team2.kills++
		} else if (chat == opponent.ownerId()) {
			player.teleport(area.relative(map.jail2().randomClippedTile(world)))
			team1.kills++
		}
		
		Staking.heal_player(player)
	}
	
	fun recomputeParticipants() {
		val oldParticipants = setOf(*allParticipants.toTypedArray())
		allParticipants.clear()
		team1.participants.clear()
		team2.participants.clear()
		
		val jails = map.jail1.plus(map.jail2)
		
		world.players().forEachInAreaKt(area, s@ { p ->

			//Ignore players who cant be see.
			if (p.looks().hidden())
				return@s

			val team = p.varps().varbit(CWSettings.ACTIVE_TEAM)
			
			for (jail in jails) {
				if (p in area.relative(jail)) {
					if (team == 1) team1.jail.add(p)
					else team2.jail.add(p)
					
					allJailed.add(p)
					return@s
				}
			}
			
			if (team == 1) team1 += p
			else team2 += p
			
			allParticipants.add(p)
			
			p.putattrib(AttributeKey.IN_CLAN_WAR, true)
		})
		
		// Remove the interface for those who left
		oldParticipants.minus(allParticipants).minus(allJailed).forEach {
			ClanWars.closeStatusInterface(it)
			it.clearattrib(AttributeKey.IN_CLAN_WAR)
		}
	}
	
	fun updateInfoPanel() {
		allParticipants.plus(allJailed).forEach { participant ->
			ClanWars.displayStatusInterface(participant)
			
			participant.varps().varbit(CWSettings.COUNTDOWN_TIMER, ticksUntilStart)
			participant.varps().varbit(CWSettings.TEAM_1_COUNT, team1.size())
			participant.varps().varbit(CWSettings.TEAM_2_COUNT, team2.size())
		}
	}
	
	fun allocateMap(map: ClanWarsMap): Area {
		val regionIds = map.maps
		
		val singleMap = regionIds.first()
		val copyX = singleMap.shr(8) * 64
		val copyZ = singleMap.and(0xFF) * 64
		
		var size = 64
		val startX = copyX
		val startZ = copyZ
		var endX = copyX + 64
		var endZ = copyZ + 64
		
		if (regionIds.size > 1) {
			size = 128
			
			val lastRegion = regionIds.sorted().last() //Get the largest region id is always the last
			val lastX = lastRegion.shr(8) * 64
			val lastZ = lastRegion.and(0xFF) * 64
			
			endX = lastX + 64
			endZ = lastZ + 64
		}
		
		val allocated = world.allocator().allocate(size, size, ClanWars.LOBBY_EXIT_TILE).get()
		allocated.setDangerous(false)
		allocated.setDeallocateOnDeath(false)
		allocated.setDeallocatesOnLogout(false)
		allocated.setMulti(true)
		allocated.setIdentifier(InstancedMapIdentifier.CLAN_WARS)
		allocated.set(0, 0, Tile(startX, startZ), Tile(endX, endZ), 0, true)
		
		return allocated
	}
	
	fun setting(id: Int) = Varps.varbitGet(settings, world.definitions().get(VarbitDefinition::class.java, id))
	
	fun setting(id: Int, expected: Int) = Varps.varbitGet(settings, world.definitions().get(VarbitDefinition::class.java, id)) == expected
	
	fun started() = ticksUntilStart <= 0
	
	operator fun Array<Area>.invoke() = this.random()
	
	operator fun Array<Area>.contains(t: Tile): Boolean {
		forEach {
			if (it.contains(t)) return true
		}
		return false
	}
}