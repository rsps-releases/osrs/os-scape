package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 22/01/2016.
 */
@ScriptMain fun putotherwidgets(repo: ScriptRepository) {
	
	repo.onButton(370, 17, @Suspendable {
		// 'X' button on house options
		it.player().interfaces().closeById(370)
	})
	
	repo.onButton(245, 23) {
		// Kourend favour view tasks
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
		it.player().interfaces().sendMain(243)
	}
	
	repo.onButton(378, 6) {
		// Login screen
		val player: Player = it.player()
		player.clearattrib(AttributeKey.NO_GPI) // Post loginscreen
		// TODO add support to interfaces().sendForMode(xx) for going from 165 -> whatever mode
	}
}

