package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Jason MacKeigan on 2016-09-08 at 4:06 PM
 */
interface SearchesForBreakable {
	
	fun search(knight: Npc, script: Script, centerOfMap: Tile)
	
}