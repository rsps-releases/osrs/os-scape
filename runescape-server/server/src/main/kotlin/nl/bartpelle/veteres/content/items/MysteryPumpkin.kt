package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Mack on 10/9/2017.
 */
object MysteryPumpkin {
	
	const val MYSTERY_PUMPKIN = 12835
	
	val low = arrayListOf(
			Item(1419, 1) //Scythe
	)
	
	val medium = arrayListOf(
			Item(1053, 1),// Green h'ween
			Item(1055, 1),// Blue h'ween
			Item(1057, 1)// Red h'ween
	)
	
	val high = arrayListOf(
			Item(11847) //Black h'ween
	)
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onItemOption2(MYSTERY_PUMPKIN, @Suspendable {
			it.doubleItemBox("This exclusive holiday item will give you a chance at cracking open a range of different items. If you're lucky, you could even walk away with a colored halloween mask! Upon breaking open this item you will lose the pumpkin permanently!", MYSTERY_PUMPKIN, 1057)
			if (it.optionsTitled("Really break open the pumpkin?", "Yes, in the spirit of Halloween!", "No thanks, I'll just hold onto it.") == 1) {
				open(it.player())
			}
		})
	}
	
	/**
	 * Open the pumpkin and calculate our reward container.
	 */
	fun open(player: Player) {
		
		val rewardContainer = ArrayList<Item>()
		
		if (player.world().rollDie(20, 1)) {
			rewardContainer.add(medium[(player.world().random(medium.size - 1))])
		} else if (player.world().rollDie(30, 1)) {
			rewardContainer.add(high[(player.world().random(high.size - 1))])
		} else {
			for (i in 0..1) {
				rewardContainer.add(low[player.world().random(low.size - 1)])
			}
		}
		
		if (player.inventory().freeSlots() < rewardContainer.size) {
			for (item in rewardContainer) {
				player.bank().add(item)
			}
			player.message("Due to lack of available space... your reward items were sent to your bank.")
		} else {
			for (item in rewardContainer) {
				player.inventory().add(item)
			}
		}
		
		if (player.inventory().has(MYSTERY_PUMPKIN)) {
			player.inventory().remove(player.inventory().byId(MYSTERY_PUMPKIN), true)
		}
		player.message("You crack open the mystery pumpkin and pull out your reward.")
	}
}