package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.npcs.Bankers
import nl.bartpelle.veteres.content.skills.slayer.master.Krystilia
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.steroids.RangeStepSupplier
import java.lang.ref.WeakReference

/**
 * Created by Bart on 8/23/2015.
 */
val OPTION_ITEM_ON_NPC = -1

@Suspendable object NpcInteraction {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val ref = it.player().attrib<WeakReference<Npc>>(AttributeKey.TARGET) ?: return@s
		if (ref.get() !is Npc) return@s // Since the script is called by execLater, we can change target before this runs
		val npc: Npc? = ref.get()
		val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
		
		it.onInterrupt { it.player().face(null) }
		
		if (npc != null && !npc.dead()) {
			var reachable = false
			var targetTile: Tile? = null
			var lastTile: Tile = npc.tile()
			var playerTile: Tile = it.player().tile()
			
			it.player().face(npc)
			
			// Intstant trigger no path to needed.
			if (option == OPTION_ITEM_ON_NPC && it.player().attribOr<Int>(AttributeKey.ITEM_ID, -1) == 5733) {
				it.player().world().server().scriptRepository().triggerItemOnNpc(it.player(), npc)
				return@s
			}
			
			val banker = Bankers.isBanker(npc)
			
			while (true) {
				// Do we need to recalculate the path?
				if (targetTile == null || (lastTile.x != npc.tile().x || lastTile.z != npc.tile().z)) {
					reachable = if (it.player().looks().trans() == 3008) it.player().walkTo(npc, PathQueue.StepType.FORCED_WALK)
					else it.player().walkTo(npc, PathQueue.StepType.REGULAR)
					targetTile = it.player().pathQueue().peekLast()?.toTile() ?: it.player().tile()
					
					//it.player().debug("reachable %s last %s", reachable, targetTile!!.toStringSimple())
					if (targetTile != null && targetTile.equals(3096, 3487)) { // South side edge bank, hardcode target tile
						targetTile = Tile(3094, 3489)
						it.player().walkTo(Tile(3094, 3489), PathQueue.StepType.REGULAR)
					}
				}
				
				// Set last tile
				lastTile = npc.tile()
				it.player().face(npc)
				
				// Are we frozen?
				if (it.player().frozen()) {
					// Reset the path found above. The lastTile will now be player.tile, and we CAN reach the object.
					it.player().message("A magical force stops you from moving.")
					it.player().sound(154)
					it.player().pathQueue().clear()
				}
				
				var forceOk: Boolean = false
				
				if (banker) {
					val supplier = RangeStepSupplier(it.player(), npc, 3)
					if (supplier.reached(it.player().world())) { // projectile will reach target from the last possible tile we can access
						if (it.player().tile().equals(targetTile))
							forceOk = true
					}
				}

				// Shops
				if (npc.id() == 7042 || npc.id() == 7477 || npc.id() == 2995 || npc.id() == 913 || npc.id() == 7456 || npc.id() == 4921 || npc.id() == 6481 || npc.id() == 4802) {
					val supplier = RangeStepSupplier(it.player(), npc, 1)
					if (supplier.reached(it.player().world())) { // projectile will reach target from the last possible tile we can access
						if (it.player().tile().equals(targetTile))
							forceOk = true
					}
				}

				// Prison pete
				if (npc.id() == 368) {
					val supplier = RangeStepSupplier(it.player(), npc, 1)
					if (supplier.reached(it.player().world())) { // projectile will reach target from the last possible tile we can access
						if (it.player().tile().equals(targetTile))
							forceOk = true
					}
				}

				if (npc.id() == Krystilia.KRYSTILIA) {
					val supplier = RangeStepSupplier(it.player(), npc, 2)
					if (supplier.reached(it.player().world())) {
						forceOk = true
					}
				}
				
				// Have we reached our path?
				if (targetTile != null && (it.player().touches(npc, reached(it.player(), npc)) || forceOk)) {
					if (it.player().attribOr(AttributeKey.DEBUG, false)) {
						it.player().message("Interacting with npc ${npc.id()}, option ${it.player().attrib<Int>(AttributeKey.INTERACTION_OPTION)}.")
					}
					
					if (reachable || forceOk) {
						if (!it.player().locked()) {
							triggerInteraction(it, option, npc)
						}
					} else {
						it.player().message("I can't reach that!")
					}
					
					it.player().executeScript @Suspendable {
						it.delay(1); it.player().face(null);
					}
					return@s
				} else if (it.player().pathQueue().empty()) {
					it.player().message("I can't reach that!")
					it.player().face(null)
					return@s
				} else {
					it.delay(1) // Wait a bit, try next tick
				}
			}
		}
	}
	
	// List of npc id which you should not face again after interacting with them. These npcs have static face directions.
	// No longer used.. leaving just in case?
	val ignore_facing_policy = intArrayOf(7494, 7496, 7497, 7498, 7499, 7501, 7502, 7503, 7504, 276, 6601, 3343)
	
	@JvmStatic @Suspendable fun triggerInteraction(it: Script, option: Int, npc: Npc) {
		val triggered = when (option) {
			1 -> it.player().world().server().scriptRepository().triggerNpcOption1(it.player(), npc)
			2 -> it.player().world().server().scriptRepository().triggerNpcOption2(it.player(), npc)
			3 -> it.player().world().server().scriptRepository().triggerNpcOption3(it.player(), npc)
			4 -> it.player().world().server().scriptRepository().triggerNpcOption4(it.player(), npc)
			OPTION_ITEM_ON_NPC -> it.player().world().server().scriptRepository().triggerItemOnNpc(it.player(), npc)
			else -> false
		}
		if (!triggered) {
			it.message("Nothing interesting happens.")
		}
	}
	
	fun reached(player: Player, target: Entity): Tile {
		val steps = if (player.pathQueue().running()) 2 else 1
		val otherSteps = if (target.pathQueue().running()) 2 else 1
		
		val otherTile = target.pathQueue().peekAfter(otherSteps)?.toTile() ?: target.tile()
		return player.pathQueue().peekAfter(steps - 1)?.toTile() ?: player.tile()
	}
}