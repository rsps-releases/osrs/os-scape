package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 7/20/2016.
 */
object CallFollower {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(387, 23) { callPet(it) }
	}

	@Suspendable fun callPet(it: Script) {
		if (!it.player.locked()) {
			if (it.player.pet() == null || it.player.pet()!!.finished()) {
				it.player.message("You do not have a follower.")
			} else {
				it.player.pet()!!.teleport(it.player.tile())
				it.player.pet()!!.walkTo(it.player, PathQueue.StepType.REGULAR)
				it.player.pet()!!.graphic(333) // Custom, but otherwise they won't see it.
			}
		}
	}
}