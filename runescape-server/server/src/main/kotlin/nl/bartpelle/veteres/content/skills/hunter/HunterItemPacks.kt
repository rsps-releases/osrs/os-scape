package nl.bartpelle.veteres.content.skills.hunter

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 21/09/2016.
 */
object HunterItemPacks {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOption1(12744) @Suspendable {
			if (it.player().inventory().remove(it.itemUsed(), false).success()) {
				it.player().inventory().add(Item(10026, 100), false)
				it.message("You open the pack to find 100 magic boxes.")
			}
		}
		repo.onItemOption1(12742) @Suspendable {
			if (it.player().inventory().remove(it.itemUsed(), false).success()) {
				it.player().inventory().add(Item(10009, 100), false)
				it.message("You open the pack to find 100 box traps.")
			}
		}
		repo.onItemOption1(12740) @Suspendable {
			if (it.player().inventory().remove(it.itemUsed(), false).success()) {
				it.player().inventory().add(Item(10007, 100), false)
				it.message("You open the pack to find 100 bird snares.")
			}
		}
	}
}