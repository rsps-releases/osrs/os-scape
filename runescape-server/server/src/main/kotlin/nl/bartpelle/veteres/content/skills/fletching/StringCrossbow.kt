package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/26/2015.
 */

object StringCrossbow {
	
	enum class Crossbows(val unstrung: Int, val strung: Int, val lvl: Int, val exp: Double, val anim: Int) {
		BRONZE_CROSSBOW(9454, 9174, 9, 6.0, 6671),
		BLURITE_CROSSBOW(9456, 9176, 24, 16.0, 6672),
		IRON_CROSSBOW(9457, 9177, 39, 22.0, 6673),
		STEEL_CROSSBOW(9459, 9179, 46, 24.0, 6674),
		MITHRIL_CROSSBOW(9461, 9181, 54, 32.0, 6675),
		ADAMANT_CROSSBOW(9463, 9183, 61, 41.0, 6676),
		RUNITE_CROSSBOW(9465, 9185, 69, 50.0, 6677);
	}
	
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		Crossbows.values().forEach { crossbow ->
			val CROSSBOW_STRING = 9438
			repo.onItemOnItem(crossbow.unstrung.toLong(), CROSSBOW_STRING.toLong(), s@ @Suspendable {
				//Check if the player has a high enough level to do this.
				if (it.player().skills()[Skills.FLETCHING] < crossbow.lvl) {
					it.messagebox("You need at least level ${crossbow.lvl} Fletching to do that.")
					return@s
				}
				//Prompt the player with the # they'd like to make
				var num = 1
				if (it.player().inventory().count(crossbow.unstrung) > 1) {
					num = it.itemOptions(Item(crossbow.strung, 150), offsetX = 12)
				}
				while (num-- > 0) {
					//Check if the player's ran out of supplies
					if (crossbow.unstrung !in it.player().inventory() || CROSSBOW_STRING !in it.player().inventory()) {
						it.animate(-1)
						break
					}
					//Remove the supplies, give the player the item, animate, send experience, message, and apply a delay.
					it.player().inventory() -= Item(crossbow.unstrung, 1)
					it.player().inventory() -= Item(CROSSBOW_STRING, 1)
					it.player().inventory() += Item(crossbow.strung, 1)
					it.message("You add a string to the crossbow")
					it.player().animate(crossbow.anim)
					it.addXp(Skills.FLETCHING, crossbow.exp)
					it.delay(2)
				}
			})
		}
	}
}