package nl.bartpelle.veteres.content.areas.dungeons.godwars

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 11/27/2015.
 */

object Obstacles {
	
	val GWD_REGION_IDS = intArrayOf(11602, 11603, 11602, 11346, 11347)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		GWD_REGION_IDS.forEach { regionId ->
			r.onRegionEnter(regionId) @Suspendable {
				it.player().interfaces().sendWidgetOn(406, Interfaces.InterSwitches.T)
			}
			
			r.onRegionExit(regionId) {
				it.player().interfaces().closeById(406)
			}
		}
		
		//Zamorak Bridge
		r.onObject(26518, s@ @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val res = it.player.interfaces().resizable()
			
			
			if (it.player().hp() < 70 || it.player().skills().xpLevel(Skills.HITPOINTS) < 70) {
				it.message("Without at least 70 Hitpoints, you would never survive the icy water.") // TODO huh gib msg
				return@s
			}
			
			if (it.player().tile().z >= 5344) {
				it.player().lock()
				it.delay(1)
				// Go to the right spot if we're not there
				if (!it.player().tile().equals(obj.tile().transform(0, 0, 0))) {
					it.player().walkTo(obj.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(obj.tile().transform(0, 0, 0))
				}
				it.delay(1)
				it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
				it.player.invokeScript(951)
				it.player().teleport(2885, 5343, 2)
				it.player().graphic(68)
				it.player().looks().render(6993, 6993, 6993, 6993, 6993, 6993, 6993)
				it.delay(6)
				it.player().looks().resetRender()
				it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
				it.player.invokeScript(948, 0, 0, 0, 255, 50)
				it.player().teleport(2885, 5332, 2)
				it.message("Dripping, you climb out of the water.")
				it.player().unlock()
			} else {
				it.player().lock()
				it.delay(1)
				// Go to the right spot if we're not there
				if (!it.player().tile().equals(obj.tile().transform(0, 0, 0))) {
					it.player().walkTo(obj.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(obj.tile().transform(0, 0, 0))
				}
				it.delay(1)
				it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
				it.player.invokeScript(951)
				it.player().teleport(2885, 5334, 2)
				it.player().graphic(68)
				it.player().looks().render(6993, 6993, 6993, 6993, 6993, 6993, 6993)
				it.delay(6)
				it.player().looks().resetRender()
				it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
				it.player.invokeScript(948, 0, 0, 0, 255, 50)
				it.player().teleport(2885, 5345, 2)
				if (it.player().skills().level(Skills.PRAYER) > 0) {
					it.message("Dripping, you climb out of the water.")
					it.message("The extreme evil of this area leaves your Prayer drained.")
					it.player().skills().setLevel(Skills.PRAYER, 0)
				} else {
					it.message("Dripping, you climb out of the water.")
				}
				it.player().unlock()
			}
		})
		
		//Ring the gong
		r.onObject(26461, s@ @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (!it.player().inventory().contains(2347) && it.player().world().realm().isRealism) {
				it.player().message("You need a hammer to ring the gong.")
			} else {
				if (it.player().tile().x >= 2851) {
					it.player().lock()
					it.delay(1)
					it.animate(7012)
					it.delay(2)
					
					it.runGlobal(it.player().world()) @Suspendable {
						val bigdoor = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
						it.ctx<World>().removeObj(bigdoor, false)
						it.delay(2)
						it.ctx<World>().spawnObj(bigdoor, false)
					}
					
					it.player().pathQueue().step(2850, 5333, PathQueue.StepType.FORCED_WALK)
					it.player().unlock()
				} else {
					it.player().lock()
					it.runGlobal(it.player().world()) @Suspendable {
						val bigdoor = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
						it.ctx<World>().removeObj(bigdoor, false)
						it.delay(2)
						it.ctx<World>().spawnObj(bigdoor, false)
					}
					
					it.player().pathQueue().step(2851, 5333, PathQueue.StepType.FORCED_WALK)
					it.player().unlock()
				}
			}
		})
		
		//Saradomin: First rope down
		r.onObject(26561) @Suspendable {
			it.delay(1)
			if (it.player().varps().varbit(Varbit.GOD_WARS_SARADOMIN_FIRST_ROPE) == 0) {
				if (it.player().inventory().contains(Item(954, 1))) {
					it.player().varps().varbit(Varbit.GOD_WARS_SARADOMIN_FIRST_ROPE, 1)
					it.player().inventory().remove(Item(954, 1), true)
				} else {
					it.player().message("You aren't carrying a rope with you.")
				}
			} else if (it.player().varps().varbit(Varbit.GOD_WARS_SARADOMIN_FIRST_ROPE) == 1) {
				if (it.player().skills().xpLevel(Skills.AGILITY) < 70) {
					it.player().message("You need an Agility level of 70 to use this shortcut.")
				} else {
					it.player().animate(828)
					it.delay(1)
					it.player().teleport(Tile(2914, 5300, 1))
				}
			} else {
				it.message("Something went wrong, please report what you did to the staff!")
			}
		}
		//Saradomin: First rope up
		r.onObject(26371) @Suspendable {
			it.player().lock()
			it.player().animate(828)
			it.delay(1)
			it.player().teleport(Tile(2912, 5299, 2))
			it.player().unlock()
		}
		
		//Saradomin: Second rope down
		r.onObject(26562) @Suspendable {
			it.delay(1)
			if (it.player().varps().varbit(Varbit.GOD_WARS_SARADOMIN_SECOND_ROPE) == 0) {
				if (it.player().inventory().contains(Item(954, 1))) {
					it.player().varps().varbit(Varbit.GOD_WARS_SARADOMIN_SECOND_ROPE, 1)
					it.player().inventory().remove(Item(954, 1), true)
				} else {
					it.player().message("You aren't carrying a rope with you.")
				}
			} else if (it.player().varps().varbit(Varbit.GOD_WARS_SARADOMIN_SECOND_ROPE) == 1) {
				it.player().animate(828)
				it.delay(1)
				it.player().teleport(Tile(2920, 5274, 0))
			} else {
				it.message("Something went wrong, please report what you did to the staff!")
			}
		}
		//Saradomin: Second rope up
		r.onObject(26375) @Suspendable {
			it.player().lock()
			it.player().animate(828)
			it.delay(1)
			it.player().teleport(Tile(2919, 5276, 1))
			it.player().unlock()
		}
		
		r.onObject(26380) @Suspendable {
			// Temp easy nigger mode; can waste time on this later
			if (it.player().tile().z > 5269) {
				it.player.teleport(2871, 5269, 2)
			} else {
				it.player.teleport(2871, 5279, 2)
			}
			
			/*val MITHRIL_GRAPPLE = 9419

			val weaponId = it.player().equipment()[EquipSlot.WEAPON]?.id() ?: -1;
			val weaponType = it.player().world().equipmentInfo().weaponType(weaponId)
			if (it.player().equipment().contains(MITHRIL_GRAPPLE)) {
				if (weaponType == WeaponType.CROSSBOW) {

				} else {
					it.message("You need a crossbow equipped to do that.")
				}
			} else {
				it.message("You need a mithril grapple-tipped bolt with a rope to do that.")
			}*/
		}
	}
}
