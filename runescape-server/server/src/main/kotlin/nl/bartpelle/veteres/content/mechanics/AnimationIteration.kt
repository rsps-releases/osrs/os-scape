package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.Entity

/**
 * Created by JasonMacKeigan on 2016-07-01.
 *
 * The purpose of this script is to lazily animate the player a certain amount
 * of times by starting at a certain number and incrementing by one until we
 * have met the total amount of iterations required.
 */
class AnimationIteration(entity: Entity, start: Int, iterations: Int) {
	
	val script: Function1<Script, Unit> = suspend@ @Suspendable {
		var animationToAnimate = start
		
		while (animationToAnimate < start + iterations) {
			entity.message("Animating the animation #$animationToAnimate.")
			entity.animate(animationToAnimate)
			animationToAnimate++
			it.delay(2)
		}
	}
}