package nl.bartpelle.veteres.content.areas.portsarim

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 17/10/2016.
 */
object EntranaMonks {
	
	val MONK_IDS = intArrayOf(1165, 1166, 1167)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (npcId in MONK_IDS) {
			repo.onNpcOption1(npcId) @Suspendable {
				it.chatNpc("Do you seek passage to holy Entrana? If so, you must leave your weaponry and armour behind. This is Saradomin's will.", npcId)
				if (it.optionsTitled("What would you like to say?", "No, not right now.", "Yes, okay, I'm ready to go") == 2) {
					it.chatPlayer("Yes, okay, I'm ready to go")
					travel(it, true)
				}
			}
			repo.onNpcOption2(npcId) @Suspendable {
				if (!badItems(it)) {
					travel(it, true)
				}
			}
		}
		
		for (npcid in intArrayOf(1168, 1169, 1170)) {
			repo.onNpcOption1(npcid) @Suspendable {
				it.chatNpc("Do you wish to leave holy Entrana?", npcid)
				if (it.options("Yes, I'm ready to go.", "Not just yet.") == 1) {
					it.chatPlayer("Yes, I'm ready to go.")
					it.chatNpc("Okay, let's board...", npcid)
					travel(it, false)
				}
			}
			
			repo.onNpcOption2(npcid) @Suspendable {
				travel(it, false)
			}
		}
	}
	
	private fun badItems(it: Script): Boolean {
		return false // TODO search for wep types that can be eqipped
	}
	
	@Suspendable private fun travel(it: Script, toEntrana: Boolean) {
		it.player().lock()
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, if (toEntrana) 1 else 2)
		it.player().interfaces().sendMain(299)
		it.player().write(InterfaceText(299, 25, "You sail to " + (if (toEntrana) "Entrana" else "Port Sarim") + "."))
		it.delay(11)
		it.player().interfaces().closeMain()
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 0) // reset
		it.player().teleport(if (toEntrana) Tile(2834, 3331, 1) else Tile(3048, 3230, 1))
		it.player().unlock()
		it.messagebox("The ship arrives at " + (if (toEntrana) "Entrana" else "Port Sarim") + ".")
	}
}