package nl.bartpelle.veteres.content.areas.instances

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.RemoveObjects
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.events.tournament.EdgevilleTourny
import nl.bartpelle.veteres.content.interfaces.TournamentSupplies
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.fs.MapDefinition
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.instance.InstancedMap
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.net.message.game.command.SetPlayerOption
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.GameCommands
import java.util.*

/**
 * Created by Jak on 5/23/2016.
 */

object PVPAreas {
	
	// Translates a real coordinate of an object, using the botleft corner of the real map, to a new tile in the instance.
	@JvmStatic fun resolveBasic(real: Tile, realMapCorner: Tile?, map: InstancedMap?): Tile {
		val xdif = real.x - realMapCorner!!.x
		val zdif = real.z - realMapCorner.z
		return Tile(map!!.x1() + xdif, map.z1() + zdif)
	}
	
	// Generates an area based from an origin tile and given dimensions.
	@JvmStatic fun tileToArea(origin: Tile, width: Int, height: Int): Area {
		return Area(origin, origin.transform(width, height))
	}
	
	@JvmStatic fun realTile(instanced: Tile): Tile {
		// We need to know what instance we're in to know the real region corner
		if (GE_MAP?.contains(instanced) ?: false) {
			val xdif = instanced.x - instanced.regionCorner().x
			val zdif = instanced.z - instanced.regionCorner().z
			return GE_CORNER.transform(xdif, zdif)
		}
		return instanced
	}
	
	/**
	 * Teleports the player to the given tile. Unique as it also sends our custom packet: WorldFlag. This allows the PvP World
	 * skull overlay to be visible, as the client uses this hardcoded variable rather than a varp. Mod Ash + client scripts = pain in the ass
	 */
	@JvmStatic fun tele_into_instance(player: Player, targetTile: Tile) {
		player.world().server().scriptExecutor().executeLater(player, @Suspendable { it ->
			if (GameCommands.PVP1_OFF) {
				it.messagebox("PvP instances are currently disabled.")
			} else {
				Teleports.basicTeleport(it, targetTile)
				it.message("Talk to <col=FF0000>Tafani</col> in the bank to return to the main world. Rushing & PJing is <col=FF0000>breaking the rules</col> in this area.")
				it.player().timers().cancel(TimerKey.FROZEN)
				it.player().timers().cancel(TimerKey.REFREEZE)
				it.player().write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
				it.player().write(UpdateStateCustom(47)) // pvp world
				it.player().putattrib(AttributeKey.UPDATE_STATE_CUSTOM, true)
				if (GE_ENTER_TILE?.equals(targetTile) ?: false) {
					it.itemBox("Warning: The East side of the Grand Exchange is a <col=FF0000>High Risk</col> area. <br><br>The <col=FF0000>Protect Item</col> prayer will <col=FF0000>not work</col> there.", 11802)
				} else if (CAMELOT_PVP_ENTER_TILE?.equals(targetTile) ?: false) {
					it.itemBox("Welcome to Camelot PvP. Towards the <col=FF0000>East</col> is a <col=FF0000>High Risk</col> area. <br><br>The <col=FF0000>Protect Item</col> prayer will <col=FF0000>not work</col> there.", 11802)
				}
			}
		})
	}
	
	/**
	 * Prompts the player with options for which PvP instance to teleport into: Varrock Canifis and Edge
	 */
	@JvmField val showPvpInstanceOptions: Function1<Script, Unit> = s@ @Suspendable {
		val player = it.player()
		when (it.optionsTitled("Which PvP instance do you wish to visit?", "Edgeville bank - safe teleport",
				"Varrock center - dangerous teleport", "Canifis bank - safe teleport",
				"Grand Exchange - safe, optional non +1 area", "Camelot - safe, optional non +1 area")) {
			1 -> {
				GameCommands.process(player, "edgepvp")
			}
			2 -> {
				it.player().world().server().scriptExecutor().executeLater(it.player(), showVarrockPvpTeleportWarning)
			}
			3 -> {
				GameCommands.process(player, "canifispvp")
			}
			4 -> {
				GameCommands.process(player, "gepvp")
			}
			5 -> {
				GameCommands.process(player, "cammypvp")
			}
		}
	}
	
	/**
	 * Shows a warning that teleporting into Varrock PvP puts the player into a dangerous area, then teleports them.
	 */
	@JvmField val showVarrockPvpTeleportWarning: Function1<Script, Unit> = s@ @Suspendable {
		val p: Player = it.player()
		if (GameCommands.PVP1_OFF) {
			it.messagebox("PvP instances are currently disabled.")
		} else {
			if (p.world().realm().isPVP() || p.world().server().isDevServer) {
				if (!GameCommands.PVP2_OFF && varrock_map != null) {
					if (GameCommands.pkTeleportOk(p, enter_tile_varrock)) {
						it.messagebox("This teleport takes you into a <col=FF0000>dangerous zone</col> in the center of Varrock. Are you sure you want to teleport there?")
						if (it.optionsTitled("Teleport into dangerous zone?", "Yes.", "No.") == 1) {
							tele_into_instance(it.player(), it.player().world().randomTileAround(enter_tile_varrock!!, 2))
						}
					}
				}
			}
		}
	}
	
	// Running total of players in various areas - reset and recalculated every server cycle
	@JvmStatic var range0_10: Int = 0
	@JvmStatic var range11_20: Int = 0
	@JvmStatic var range21_30: Int = 0
	@JvmStatic var range31_40: Int = 0
	@JvmStatic var range41_49: Int = 0
	@JvmStatic var range50plus: Int = 0
	@JvmStatic var in_wild_count: Int = 0
	@JvmStatic var players_in_pvp_instances: Int = 0
	
	// Map instances
	@JvmStatic var edgeville_map: InstancedMap? = null
	@JvmStatic var varrock_map: InstancedMap? = null
	@JvmStatic var canifis_map: InstancedMap? = null
	
	// Tile to apppear at when you enter the area
    @JvmStatic var edgevillePvpCorner: Tile? = null
	@JvmStatic var enter_tile_edge: Tile? = null
	@JvmStatic var enter_tile_varrock: Tile? = null
	@JvmStatic var varrockSafe: Tile? = null
	@JvmStatic var enter_tile_canifis: Tile? = null
	
	// The safe arena in this map
	@JvmField var safearea_coords: ArrayList<Area> = arrayListOf()
	
	val INSTANCES_WIZARD = 1787
	
	@JvmStatic var GE_MAP: InstancedMap? = null
	@JvmStatic var GE_ENTER_TILE: Tile? = null
	
	@JvmStatic var GE_CENTRE: Tile? = null
	
	
	// initiate and bind instanced maps
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onRegionExit(12342) {
			val player = it.player()
			if (player.world().realm().isPVP || player.world().server().isDevServer) {
				if (player.attribOr(AttributeKey.GIFT_PRAYERS_PLAYER_OP, false)) {
					player.write(SetPlayerOption(6, false, "null"))
					player.clearattrib(AttributeKey.GIFT_PRAYERS_PLAYER_OP)
					player.message("The gift prayer player option toggles off as you leave Edgeville.")
				}
			}
		}
		
		repo.onNpcOption1(INSTANCES_WIZARD) @Suspendable {
			if (it.player().world().realm().isPVP || it.player().world().server().isDevServer) {
				it.chatPlayer("Hello there!")
				it.chatNpc("Good day " + it.player().name() + ". Would you like to visit some special PvP areas?", INSTANCES_WIZARD)
				it.chatPlayer("What are these places?")
				it.chatNpc("Varrock, Canifis and Edgeville. These places are special because they exist as small pocket-worlds. The only safe area is inside a bank, and you can" +
						" attack anyone within 15 combat levels outside these safe areas.", INSTANCES_WIZARD)
				it.chatPlayer("So these are like little PVP worlds?")
				it.chatNpc("Exactly. If your combat level is over 100, you cannot use range. If you rush or PJ in these areas, you can be jailed by staff. ", INSTANCES_WIZARD)
				it.chatNpc("Would you like to visit one? You'll be given a warning before teleporting, so you know if the area is safe or not.", INSTANCES_WIZARD)
				it.chatPlayer("Let's ave' a look!")
				GameCommands.process(it.player(), "pvp")
			} else {
				it.chatNpc("Leave me be! I've enough trouble taking care of the dead, let alone the living.", INSTANCES_WIZARD)
			}
		}
		repo.onNpcOption2(INSTANCES_WIZARD) @Suspendable {
			if (it.player().world().realm().isPVP || it.player().world().server().isDevServer) {
				it.chatNpc("This will take you into the Edgeville bank, which is a safe zone. Off you go!", INSTANCES_WIZARD)
				GameCommands.process(it.player(), "edgepvp")
			}
		}
		repo.onNpcOption3(INSTANCES_WIZARD) @Suspendable {
			if (it.player().world().realm().isPVP || it.player().world().server().isDevServer) {
				it.chatNpc("This will take you into the Canifis bank, which is a safe zone. Off you go!", INSTANCES_WIZARD)
				GameCommands.process(it.player(), "canifispvp")
			}
		}
		repo.onNpcOption4(INSTANCES_WIZARD) @Suspendable {
			if (it.player().world().realm().isPVP || it.player().world().server().isDevServer) {
				it.chatNpc("This will take you into middle of Varrock, which is <col=FF0000>dangerous</col>. You'll have to the run to the bank or teleport to be safe.", INSTANCES_WIZARD)
				it.player().world().server().scriptExecutor().executeLater(it.player(), showVarrockPvpTeleportWarning)
			}
		}
		
		repo.onWorldInit {
			val world = it.ctx<World>()
			
			// This is a PVP world mechanic only.
			if (!world.realm().isPVP) {
				GameCommands.ANTIRUSH_ON = false
			}
			
			// NOT PVP INSTANCE RELATED - MUST BE DONE!
			registerSpecialMechanics(it)
			addSolidFloorsToGameworld(it)
			
			// Create all instances which you want to retain normal map objects here.
			// Afterwards, you can remove objects from e.g. Edgeville. Future instances that copy these coords will have
			// any custom removed objects also removed.

			setupHelpersZone(it)
			
			if (world.realm().isPVP || world.server().isDevServer) {

				setupEdgevillePvP(it)
				setupVarrockPvP(it)
				setupCanifisPvP(it)
				setupGrandExchangePvP(it)
				setupCamelotPvP(it)
				setupRunningTotalTracker(it)
				
				// Adjust clipping for custom home AFTER instances are setup.
				RemoveObjects.adjustEdgevilleRealms_PVP_OSR(world)
				
				//PkBots.initBots(world)
				
			} else if (it.ctx<World>().realm().isOSRune) {
				setupRunningTotalTracker(it)
				RemoveObjects.adjustEdgevilleRealms_PVP_OSR(world)
			}
		}
	}
	
	@JvmStatic fun setupCamelotPvP(it: Script) {
		val corner = Tile(2688, 3456)
		val world = it.ctx<World>()
		val map = world.allocator().allocate(128, 128, EXIT).get()
		map.set(0, 0, corner.transform(0, 0), corner.transform(128, 128), 0, true)
		map.persist().setDangerous(true).setMulti(false)
		CAMELOT_PVP_ENTER_TILE = resolveBasic(Tile(2757, 3479), corner, map)
		CAMELOT_INSTANCE = map
		world.registerNpc(Npc(TAFANI, world, resolveBasic(Tile(2756, 3480), corner, map)).spawnDirection(Direction.East.ordinal))
		world.spawnObj(MapObj(resolveBasic(Tile(2756, 3479), corner, map), BANK_CHEST, 10, 1))
		map.setIdentifier(InstancedMapIdentifier.CAMELOT_PVP)
		
		val shouter: Npc = Npc(CRIER, world, resolveBasic(Tile(2765, 3476), corner, map)).spawnDirection(Direction.East.ordinal)
		world.registerNpc(shouter)
		shouter.executeScript @Suspendable { s ->
			s.clearContext()
			s.delay(2)
			while (!shouter.finished()) {
				shouter.sync().shout("Careful! You can't use Protect Item around this spot!")
				s.delay(world.random(10..15))
			}
		}
		
		// Fuck trees, wasting perfectly good compact space to PK. Global warming isn't thaat bad right..? /s
		world.removeObj(MapObj(resolveBasic(Tile(2761, 3479), corner, map), 879, 10, 0)) // Fountain
		world.removeObj(MapObj(resolveBasic(Tile(2766, 3479), corner, map), 879, 10, 0)) // Fountain
		world.removeObj(MapObj(resolveBasic(Tile(2771, 3479), corner, map), 879, 10, 0)) // Fountain
		world.removeObj(MapObj(resolveBasic(Tile(2761, 3473), corner, map), 1276, 10, 0)) // Tree
		world.removeObj(MapObj(resolveBasic(Tile(2763, 3474), corner, map), 1276, 10, 0)) // Tree
		world.removeObj(MapObj(resolveBasic(Tile(2762, 3471), corner, map), 1247, 10, 0)) // Tree
		world.removeObj(MapObj(resolveBasic(Tile(2764, 3472), corner, map), 1276, 10, 0)) // Tree
		world.removeObj(MapObj(resolveBasic(Tile(2768, 3474), corner, map), 1276, 10, 0)) // Tree
		
		val cammy_high_risk_sw_corner = Tile(2766, 3469)
		val cammy_high_risk_ne_corner = Tile(2788, 3480)
		CAMMY_HIGH_RISK_AREA_EAST = Area(resolveBasic(cammy_high_risk_sw_corner, corner, map), resolveBasic(cammy_high_risk_ne_corner, corner, map))
		
		val hr_sw_2 = Tile(2775, 3480)
		val hr_ne_2 = Tile(2788, 3490)
		CAMMY_HIGH_RISK_AREA_NORTH = Area(resolveBasic(hr_sw_2, corner, map), resolveBasic(hr_ne_2, corner, map))
		
		// Put some graphical orbs down to showcase the boundary of the high-risk areas
		for (i in CAMMY_HIGH_RISK_AREA_EAST!!.x1 .. CAMMY_HIGH_RISK_AREA_EAST!!.x2) {
			world.spawnObj(MapObj(Tile(i, CAMMY_HIGH_RISK_AREA_EAST!!.z1), 399, 10, 0), false)
			
			if (i < CAMMY_HIGH_RISK_AREA_EAST!!.x1 + 10) // dont place down where the two rectangles overlap xd
				world.spawnObj(MapObj(Tile(i, CAMMY_HIGH_RISK_AREA_EAST!!.z2), 399, 10, 0), false)
		}
		for (i in CAMMY_HIGH_RISK_AREA_EAST!!.z1 .. CAMMY_HIGH_RISK_AREA_EAST!!.z2) {
			world.spawnObj(MapObj(Tile(CAMMY_HIGH_RISK_AREA_EAST!!.x1, i), 399, 10, 0), false)
			world.spawnObj(MapObj(Tile(CAMMY_HIGH_RISK_AREA_EAST!!.x2, i), 399, 10, 0), false)
		}
		
		for (i in CAMMY_HIGH_RISK_AREA_NORTH!!.x1 .. CAMMY_HIGH_RISK_AREA_NORTH!!.x2) {
			
			// dont place down where the two rectangles overlap xd
			//world.spawnObj(MapObj(Tile(i, CAMMY_HIGH_RISK_AREA_NORTH!!.z1), 399, 10, 0), false)
			world.spawnObj(MapObj(Tile(i, CAMMY_HIGH_RISK_AREA_NORTH!!.z2), 399, 10, 0), false)
		}
		for (i in CAMMY_HIGH_RISK_AREA_NORTH!!.z1 .. CAMMY_HIGH_RISK_AREA_NORTH!!.z2) {
			world.spawnObj(MapObj(Tile(CAMMY_HIGH_RISK_AREA_NORTH!!.x1, i), 399, 10, 0), false)
			world.spawnObj(MapObj(Tile(CAMMY_HIGH_RISK_AREA_NORTH!!.x2, i), 399, 10, 0), false)
		}
		
		
		safearea_coords.add(Area(resolveBasic(Tile(2755, 3476), corner, map), resolveBasic(Tile(2760, 3481), corner, map)))
	}
	
	@JvmStatic var CAMELOT_PVP_ENTER_TILE: Tile? = null
	@JvmStatic var CAMELOT_INSTANCE: InstancedMap? = null
	@JvmStatic var CAMMY_HIGH_RISK_AREA_EAST: Area? = null
	@JvmStatic var CAMMY_HIGH_RISK_AREA_NORTH: Area? = null
	
	@JvmStatic var HELPERS_INSTANCE: InstancedMap? = null
	@JvmStatic var HZ_ENTER_COORDS: Tile? = null
	
	@JvmStatic fun in_high_risk_area(player: Player): Boolean {
		if (GE_HIGH_RISK_AREA != null && player.tile().inArea(GE_HIGH_RISK_AREA))
			return true
		if (CAMELOT_INSTANCE != null) {
			if (player.tile().inArea(CAMMY_HIGH_RISK_AREA_EAST) || player.tile().inArea(CAMMY_HIGH_RISK_AREA_NORTH))
				return true
		}
		return false
	}
	
	val BANK_CHEST = 26707
	val PVPREALM_WIZARD = 4159
	val ECOREALM_WIZARD = 4399
	val OSRREALM_WIZARD = 4398
	
	@JvmStatic @Suspendable fun reSetupHZ(world: World) {
		world.executeScript { setupHelpersZone(it) }
	}
	
	@JvmStatic fun setupHelpersZone(it: Script) {
		val corner = Tile(2632, 2952)
		val SIZE = 24
		if (HELPERS_INSTANCE != null) { // Remove old
			HELPERS_INSTANCE!!.setPersists(false)
		}
		
		val world = it.ctx<World>()
		val map = world.allocator().allocate(64, 64, EXIT).get()
		map.set(0, 0, corner.transform(0, 0), corner.transform(SIZE, SIZE), 0, true)
		map.persist().setDangerous(false).setMulti(false)
		HZ_ENTER_COORDS = resolveBasic(Tile(2644, 2960), corner, map)
		HELPERS_INSTANCE = map
		
		val wizard = when (world.realm()) {
			Realm.REALISM -> ECOREALM_WIZARD
			Realm.PVP -> PVPREALM_WIZARD
			Realm.OSRUNE -> OSRREALM_WIZARD
			else -> PVPREALM_WIZARD
		}
		world.registerNpc(Npc(TAFANI, world, resolveBasic(Tile(2642, 2963), corner, map)).spawnDirection(6))
		world.registerNpc(Npc(wizard, world, resolveBasic(Tile(2643, 2963), corner, map)).spawnDirection(6))
		world.spawnObj(MapObj(resolveBasic(Tile(2644, 2963), corner, map), BANK_CHEST, 10, 2))
		
		// Now, since the instance itself is 64x64 (granularity required for generation) BUT we've only applied 24x24 clippings,
		// any walkable area which leads into the rest of the 64x64 region becomes walkable and you can noclip out.
		// To stop this, block any exits on walkable tiles into the great beyond.
		val topLeft = map.bottomLeft().transform(0, SIZE)
		val mapdef = world.definitions().get(MapDefinition::class.java, map.center().region()) ?: return
		for (i in topLeft.x..topLeft.x + 5) {
			mapdef.addFloor(world.definitions(), topLeft.level, i and 63, topLeft.z and 63, true)
		}
	}
	
	val EXIT = Tile(3088, 3504)
	val GE_CORNER = Tile(3136, 3456)
	val GE_HIGH_RISK_CORNER_SW = Tile(3173, 3468)
	val GE_HIGH_RISK_CORNER_NE = Tile(3197, 3516)
	@JvmStatic var GE_HIGH_RISK_AREA: Area? = null
	val CRIER = 277
	
	// Testing from JS
	@JvmStatic fun rebuild(world: World) {
		world.executeScript { s ->
			setupGrandExchangePvP(s)
		}
	}
	
	val WIZARD = 4159
	val TAFANI = 3343
	
	fun setupGrandExchangePvP(it: Script) {
		val world = it.ctx<World>()
		val map = world.allocator().allocate(64, 64, EXIT).get()
		map.set(0, 0, GE_CORNER.transform(0, 0), GE_CORNER.transform(64, 64), 0, true)
		map.persist().setDangerous(true).setMulti(false)
		
		// Real coord
		val enterTile = Tile(3169, 3489)
		GE_ENTER_TILE = resolveBasic(enterTile, GE_CORNER, map)
		
		GE_MAP = map
		map.setIdentifier(InstancedMapIdentifier.GE_PVP)
		
		world.registerNpc(Npc(TAFANI, world, resolveBasic(Tile(3165, 3495), GE_CORNER, map)).spawnDirection(6))
		world.registerNpc(Npc(WIZARD, world, resolveBasic(Tile(3163, 3495), GE_CORNER, map)).spawnDirection(6))
		
		val crierTile = resolveBasic(Tile(3178, 3489), GE_CORNER, map)
		val mapdef = world.definitions().get(MapDefinition::class.java, crierTile.region())
		
		val shouter: Npc = Npc(CRIER, world, crierTile).spawnDirection(3)
		world.registerNpc(shouter)
		shouter.executeScript @Suspendable { s ->
			s.clearContext()
			s.delay(2)
			while (!shouter.finished()) {
				shouter.sync().shout("Careful! You can't use Protect Item around this spot!")
				s.delay(world.random(10..15))
			}
		}
		mapdef?.addFloor(world.definitions(), 0, crierTile.x and 63, crierTile.z and 63, true)

		copynpcspawns(Area(GE_CORNER, GE_CORNER.transform(64, 64)), world, GE_CORNER, map)
		world.spawnObj(MapObj(resolveBasic(Tile(3166, 3495), GE_CORNER, map), 23709, 10, 2)) // Box of Health
		
		GE_HIGH_RISK_AREA = Area(resolveBasic(GE_HIGH_RISK_CORNER_SW, GE_CORNER, map), resolveBasic(GE_HIGH_RISK_CORNER_NE, GE_CORNER, map))
		GE_CENTRE = resolveBasic(Tile(3164, 3490), GE_CORNER, map)
		
		// Safe areas making up the GE
		val rectangles = arrayOf(
				Area(Tile(3159, 3473), Tile(3170, 3506)),
				Area(Tile(3157, 3474), Tile(3171, 3505)),
				Area(Tile(3155, 3475), Tile(3173, 3504)),
				Area(Tile(3153, 3476), Tile(3175, 3503)),
				Area(Tile(3152, 3477), Tile(3176, 3502)),
				Area(Tile(3151, 3478), Tile(3178, 3501)),
				Area(Tile(3150, 3480), Tile(3179, 3498)),
				Area(Tile(3149, 3482), Tile(3180, 3496)),
				Area(Tile(3148, 3484), Tile(3181, 3495))
		)
		for (area in rectangles) {
			val converted = Area(resolveBasic(area.bottomLeft(), GE_CORNER, map), resolveBasic(area.topRight(), GE_CORNER, map))
			// System.out.println("converted safe: "+converted)
			safearea_coords.add(converted)
		}
	}
	
	private fun setupEdgevillePvP(it: Script) {
		val world = it.ctx<World>()
		val map = world.allocator().allocate(64, 64, EXIT).get()
		val corner = Tile(3064, 3472, 0)
        edgevillePvpCorner = corner
		map.set(0, 0, corner, corner.transform(64, 64), 0, true)
		map.persist()
		map.setDangerous(true) // Items are lost on death in here.
		map.setMulti(false)
		map.setIdentifier(InstancedMapIdentifier.EDGE_PVP)
		edgeville_map = map
		enter_tile_edge = map.center().transform(-3, -6, 0)
		safearea_coords.add(Area(map.center().x - 5, map.center().z - 16, map.center().x + 2, map.center().z - 5))
		
		world.registerNpc(Npc(3343, world, map.center().transform(-2, -5, 0)).spawnDirection(6))
		
		// Remove some objects that get in the way.
		world.removeObj(MapObj(map.center().transform(-6, -1, 0), 1276, 10, 0)) // Tree 1 sq north of the bank.
		world.removeObj(MapObj(resolveBasic(Tile(3084, 3502), corner, map), 884, 10, 0)) // The Well
		world.removeObj(MapObj(resolveBasic(Tile(3088, 3509, 0), corner, map), 307, 10, 0)) //Cart
		
		val bank_sw = Tile(3091, 3489)
		copynpcspawns(Area(bank_sw, bank_sw.transform(8, 8)), world, corner, map)
	}
	
	val VARROCK_CORNER = Tile(3136, 3392)
	
	private fun setupVarrockPvP(it: Script) {
		val world = it.ctx<World>()
		val map = world.allocator().allocate(192, 192, EXIT).get()
		val corner = VARROCK_CORNER
		map.set(0, 0, corner.transform(0, 0), corner.transform(192, 192), 0, true)
		map.persist()
		map.setDangerous(true) // Items are lost on death in here.
		map.setMulti(false)
		map.setIdentifier(InstancedMapIdentifier.VARROCK_PVP)
		varrock_map = map
		val botleft2: Tile = Tile(map.x1(), map.z1())
		enter_tile_varrock = resolveBasic(Tile(3212, 3423), corner, map)
		safearea_coords.add(Area(botleft2.x + 44, botleft2.z + 41, botleft2.x + 54, botleft2.z + 55)) // Varrock West big bank
		safearea_coords.add(Area(botleft2.x + 114, botleft2.z + 24, botleft2.x + 121, botleft2.z + 31)) // Varrock East small bank
		copynpcspawns(Area(corner, corner.transform(192, 192)), world, corner, map)
		val healer = Npc(3343, world, resolveBasic(Tile(3180, 3439), corner, varrock_map!!)).spawnDirection(4)
		world.registerNpc(healer)
		world.spawnObj(MapObj(resolveBasic(Tile(3180, 3440), corner, varrock_map!!), 23709, 10, 1))
		varrockSafe = resolveBasic(Tile(3183, 3439), corner, varrock_map!!)
	}
	
	val CANIFIS_CORNER = Tile(3472, 3464)
	
	private fun setupCanifisPvP(it: Script) {
		val world = it.ctx<World>()
		val map = world.allocator().allocate(64, 64, EXIT).get()
		val corner = CANIFIS_CORNER
		map.set(0, 0, corner.transform(0, 0), corner.transform(64, 64), 0, true)
		map.persist()
		map.setDangerous(true) // Items are lost on death in here.
		map.setMulti(false)
		map.setIdentifier(InstancedMapIdentifier.CANIFIS_PVP)
		canifis_map = map
		enter_tile_canifis = Tile(map.x1(), map.z1()).transform(38, 11, 0)
		val botleft3: Tile = Tile(map.x1(), map.z1())
		safearea_coords.add(Area(botleft3.x + 36, botleft3.z + 11, botleft3.x + 43, botleft3.z + 19))
		copynpcspawns(Area(corner, corner.transform(64, 64)), world, corner, map)
		val healer3 = Npc(3343, world, resolveBasic(Tile(3510, 3476), corner, map)).spawnDirection(1)
		world.registerNpc(healer3)
		world.spawnObj(MapObj(resolveBasic(Tile(3513, 3477), corner, map), 23709, 10, 3))
	}
	
	/**
	 * Starts a infinite loop which tracks how many people are in the various PvP instances
	 */
	private fun setupRunningTotalTracker(it: Script) {
		val world: World = it.ctx<World>()
		fun valueupdate() {
			world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
				s.onInterrupt { valueupdate() }
				
				while (world.ticksUntilUpdate() != -1) {
					// Refresh values
					updateRunningTotals(world)
					// Sleep until next cycle
					s.delay(1)
				}
			})
		}
		valueupdate()
	}
	
	/**
	 * Makes some tiles unwalkable in the general game world.
	 */
	private fun addSolidFloorsToGameworld(it: Script) {
		val world = it.ctx<World>()

		// First - some general world stuff. Make non-moving shop npcs have solid tiles - therefore players can't
		// walk on top and be rendered over them - so other players can't even see the shops.
		val mapdef = world.definitions().get(MapDefinition::class.java, Tile(3092, 3511).region()) ?: return

		// Add solid tiles on the north side of kbd cage - haven't checked on RS but probs should be multi not 2 single squares.
		// They are single rn because the chunk id overlaps the single-way cb area inside kbd cage
		mapdef.addFloor(world.definitions(), 0, 3021 and 63, 3855 and 63, true)
		mapdef.addFloor(world.definitions(), 0, 3022 and 63, 3855 and 63, true)
		
		val mb_map = world.definitions().get(MapDefinition::class.java, Tile(2533, 4717).region()) ?: return
		// Gundai banker at MB inside so he can stand on top of people
		mb_map.addFloor(world.definitions(), 0, 2533 and 63, 4717 and 63, true)
	}
	
	/**
	 * NOT RELATED TO PVP INSTANCES AT ALL.
	 *
	 * Very important things which need to be done on server start.
	 */
	private fun registerSpecialMechanics(it: Script) {
		val world = it.ctx<World>()
		
		// Please no lose pets
		Pet.values().forEach { pet ->
			world.prices().set(pet.item, 1)
			
			//Make sure as hell you can't buy a pet using Cheat Engine
			TournamentSupplies.UNPURCHASABLE.add(pet.item)
		}
		
		// Stop buying certain items using Cheat Engine
		for (i in intArrayOf(11905, 11907, 11908, 12899, 12900, 13652, 12924, 12926, 13576, 12902, 12904, 13269,
				13267, 13265, 12931, 12817, 12825, 12821, 13307, 10330, 10332, 10334, 10336, 10338, 10340, 10342,
				10344, 10346, 10348, 10350, 10352, 12422, 12424, 12426, 12437, 6990, 19478, 19481, 19484, 19553,
				19547, 19550, 19544)) {
			TournamentSupplies.UNPURCHASABLE.add(i)
		}
	}
	
	/**
	 * Looks at the game world, identifies Npcs which exist in a certain area and copy them into their equivilent
	 * positions in an instances.
	 */
	private fun copynpcspawns(area: Area, world: World, mapfrom: Tile, map: InstancedMap) {
		world.npcs().forEachInAreaKt(area, { n ->
			val spawnTile = resolveBasic(Tile(n.tile().x, n.tile().z, n.tile().level), mapfrom, map)
			val npc = Npc(n.id(), world, spawnTile)
			npc.spawnDirection(n.spawnDirection())
			npc.walkRadius(n.walkRadius())
			world.registerNpc(npc)
			//System.out.println("instance npc spawn : "+npc.def().name)
		})
	}
	
	/**
	 * Reset and re-update the running totals of people in the wild/pvp areas
	 */
	private fun updateRunningTotals(world: World) {
		// Reset
		in_wild_count = 0
		range0_10 = 0
		range11_20 = 0
		range21_30 = 0
		range31_40 = 0
		range41_49 = 0
		range50plus = 0
		players_in_pvp_instances = 0 // Reset
		
		// Recalc
		if (world.realm().isPVP) {
			world.players().stream().filter { player -> player != null && !player.looks().hidden() && !player.bot() }.forEach { player ->
				val lvl = WildernessLevelIndicator.wildernessLevel(player.tile())
				if (lvl > 0) {
					in_wild_count++ // General population in the wilderness
					
					if (lvl <= 10) {
						range0_10++
					} else if (lvl > 10 && lvl <= 20) {
						range11_20++
					} else if (lvl > 20 && lvl <= 30) {
						range21_30++
					} else if (lvl > 30 && lvl <= 40) {
						range31_40++
					} else if (lvl > 40 && lvl <= 49) {
						range41_49++
					} else if (lvl >= 50) {
						range50plus++
					} else if (WildernessLevelIndicator.inPvpInstanceAttackable(player)) { // If they're not in a wild level they must be in edge pvp
						in_wild_count++
					}
				}
				if (edgeville_map != null && edgeville_map!!.contains(player)) {
					players_in_pvp_instances++
				}
				if (varrock_map != null && varrock_map!!.contains(player)) {
					players_in_pvp_instances++
				}
				if (canifis_map != null && canifis_map!!.contains(player)) {
					players_in_pvp_instances++
				}
			}
		} else if (world.realm().isOSRune) {
			world.players().stream().filter { player -> player != null && !player.looks().hidden() && !player.bot()  }.forEach { player ->
				val lvl = WildernessLevelIndicator.wildernessLevel(player.tile())
				if (lvl > 0) {
					in_wild_count++ // General population in the wilderness
					
					if (lvl <= 10) {
						range0_10++
					} else if (lvl > 10 && lvl <= 20) {
						range11_20++
					} else if (lvl > 20 && lvl <= 30) {
						range21_30++
					} else if (lvl > 30 && lvl <= 40) {
						range31_40++
					} else if (lvl > 40 && lvl <= 49) {
						range41_49++
					} else if (lvl >= 50) {
						range50plus++
					} else if (WildernessLevelIndicator.inPvpInstanceAttackable(player)) { // If they're not in a wild level they must be in edge pvp
						in_wild_count++
					}
				}
			}
		}
	}
	
	/**
	 * A simple string telling you how many people are in certain level ranges
	 */
	@JvmStatic fun breakdown(): String {
		return "below 10: ${range0_10}  | 10-20: ${range11_20} | 20-30: ${range21_30} | 30-40: ${range31_40} | 40-49: ${range41_49} | 50+: ${range50plus}"
	}
	
	/**
	 * If the entity is in a PvP instance area.
	 */
	@JvmStatic fun inPVPArea(entity: Entity): Boolean {
		return (edgeville_map?.contains(entity) ?: false) || (varrock_map?.contains(entity) ?: false) || (canifis_map?.contains(entity) ?: false) || EdgevilleTourny.insideTournament(entity)
				|| (GE_MAP?.contains(entity) ?: false) || WildernessLevelIndicator.PVP_WORLD || (CAMELOT_INSTANCE?.contains(entity) ?: false || MageBankInstance.attackableAreas(entity))
	}
	
	/**
	 * A flag check if the player is in edge pvp.
	 */
	@JvmStatic fun inEdgePvp(entity: Entity): Boolean {
		return (edgeville_map?.contains(entity) ?: false)
	}
	
	/**
	 * Finds a tile which is safe in the instance a Player is in. Always inside banks.
	 */
	@JvmStatic fun safetileFor(player: Player): Tile {
		if (edgeville_map!!.contains(player)) {
			return enter_tile_edge!!
		}
		if (varrock_map!!.contains(player)) {
			return varrockSafe!!
		}
		if (canifis_map!!.contains(player)) {
			return enter_tile_canifis!!
		}
		if (GE_MAP?.contains(player) ?: false) {
			return GE_ENTER_TILE!!
		}
		if (CAMELOT_INSTANCE?.contains(player) ?: false) {
			return CAMELOT_PVP_ENTER_TILE!!
		}
		return EdgevilleTourny.findDeathTileFor(player) ?: EXIT
	}
}