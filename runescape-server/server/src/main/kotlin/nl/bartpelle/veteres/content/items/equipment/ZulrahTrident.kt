package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 22/09/2016.
 * Trident "charges" represent the fire, chaos and death runes. coins not included when uncharging.
 * the zulrah scales attrib then takes care of toxic trident charges.
 */
object ZulrahTrident {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		for (tridentId in ZulrahItems.NORM_TRIDENTS) {
			
			// Fang + any trident (empty, used, full) = TOXIC version
			r.onItemOnItem(tridentId, ZulrahItems.MAGIC_FANG) @Suspendable {
				
				if (tridentId != ZulrahItems.TRIDENT_EMPTY) {
					// This is because toxic tridents requires scales to charge too. You can't apply the scale charges serpately, they're part of a single "charge"
					it.message("You can only attach a magic fang to an Uncharged trident.")
				} else if (it.player().inventory().contains(1755)) {
					if (it.player().inventory().contains(tridentId) && it.player().inventory().contains(ZulrahItems.MAGIC_FANG)) {
						it.animate(3015)
						
						val tridentSlot = ItemOnItem.slotOf(it, tridentId)
						val trident = it.player().inventory()[tridentSlot]
						val fangSlot = ItemOnItem.slotOf(it, ZulrahItems.MAGIC_FANG)
						val fang = it.player().inventory()[fangSlot]
						
						it.player().inventory().remove(trident, true, tridentSlot)
						it.player().inventory().remove(fang, true, fangSlot)
						
						val res = when (tridentId) {
							ZulrahItems.TRIDENT_EMPTY -> ZulrahItems.TOXIC_TRIDENT_EMPTY
						// empty and full version becomes toxic used.
							else -> ZulrahItems.TOXIC_TRIDENT_USED
						}
						val swamp = Item(res)
						swamp.duplicateProperties(trident) // Don't lose the charges!
						
						it.player().inventory().add(swamp, false, it.itemOnSlot())
						it.itemBox("You graft the fang onto the trident.", res)
					}
				} else {
					it.message("You'll need a chisel to do that.")
				}
			}
		}
		
		for (tridentId in intArrayOf(ZulrahItems.TRIDENT_FULL, ZulrahItems.TRIDENT_USED, ZulrahItems.TOXIC_TRIDENT_USED)) {
			
			// Check charges
			r.onItemOption3(tridentId) @Suspendable {
				send_casts_message(it, it.itemUsed())
			}
			
			r.onEquipmentOption(1, tridentId) {
				send_casts_message(it, it.player().equipment().get(EquipSlot.WEAPON))
			}
			
			// Trident (normal OR toxic) uncharging - giving back runes and scales
			r.onItemOption4(tridentId) @Suspendable {
				if (!it.itemUsed().hasProperties()) {
					if (tridentId == ZulrahItems.TOXIC_TRIDENT_USED) {
						it.player().inventory().set(it.itemUsedSlot(), Item(ZulrahItems.TOXIC_TRIDENT_EMPTY))
						it.player().message("You uncharge the toxic trident.")
					} else if (tridentId == ZulrahItems.TRIDENT_FULL || tridentId == ZulrahItems.TRIDENT_USED) {
						it.player().inventory().set(it.itemUsedSlot(), Item(ZulrahItems.TRIDENT_EMPTY))
						it.player().message("You uncharge the trident.")
					} else {
						// Will never happen
						it.message("You cannot uncharge this trident.")
					}
				} else {
					var spacerequired = 3
					// Coins NOT refunded.
					if (it.player().inventory().has(560)) spacerequired -= 1 // Death
					if (it.player().inventory().has(554)) spacerequired -= 1 // Fire
					if (it.player().inventory().has(562)) spacerequired -= 1 // Chaos
					
					val toxic = tridentId == ZulrahItems.TOXIC_TRIDENT_USED
					
					// If you're not carry stackable scales, you'll need 1 more inventory slot space.
					if (toxic && !it.player().inventory().has(ZulrahItems.ZULRAHS_SCALES))
						spacerequired += 1
					
					// No room!
					if (it.player().inventory().freeSlots() < spacerequired) {
						it.messagebox("You need $spacerequired free inventory slots to do this.")
					} else {
						// Calculate runes to refund
						val charges = it.itemUsed().property(ItemAttrib.CHARGES)
						val deathsToAdd = charges
						val chaosToAdd = charges
						val firesToAdd = charges * 5
						val scalesToAdd = charges
						
						// 2.1b scales held already?
						// If you can't hold ANY of these types, totally stop.
						// Don't try to be clever as fuck and work out the maximum amount we can hold, aaaand then adjust the other values
						// If you did that, you've have to update the trident's values too.
						if (it.player().inventory().count(560) + charges > Integer.MAX_VALUE) {
							it.message("You don't have enough room in your inventory to hold all the death runes.")
						} else if (it.player().inventory().count(554) + (charges * 5) > Integer.MAX_VALUE) {
							it.message("You don't have enough room in your inventory to hold all the fire runes.")
						} else if (it.player().inventory().count(562) + charges > Integer.MAX_VALUE) {
							it.message("You don't have enough room in your inventory to hold all the chaos runes.")
						} else if (toxic && it.player().inventory().count(ZulrahItems.ZULRAHS_SCALES) + scalesToAdd > Integer.MAX_VALUE) {
							it.message("You don't have enough room in your inventory to hold all the zulrah scales.")
						} else {
							it.player().inventory().remove(it.itemUsed(), false, it.itemUsedSlot()) // Remove charged
							// Give back what trident?
							val res = if (toxic) ZulrahItems.TOXIC_TRIDENT_EMPTY else ZulrahItems.TRIDENT_EMPTY
							it.player().inventory().add(Item(res), false, it.itemUsedSlot()) // Give uncharged
							
							// Give runes, and a nice message
							val sb: StringBuilder = StringBuilder()
							it.player().inventory() += Item(560, deathsToAdd)
							sb.append("$deathsToAdd death runes, ")
							it.player().inventory() += Item(554, firesToAdd)
							sb.append("$firesToAdd fire runes, ")
							it.player().inventory() += Item(562, chaosToAdd)
							sb.append("and $chaosToAdd chaos runes")
							if (toxic) {
								it.player().inventory() += Item(ZulrahItems.ZULRAHS_SCALES, scalesToAdd)
								sb.append(", in addition to $scalesToAdd scales")
							}
							val removed = sb.toString()
							
							it.message("You remove $removed from the trident, leaving it uncharged.")
						}
					}
				}
			}
		}
		
		// Uncharged toxic trident - check option - only gets to this state if no scales AND no runes
		r.onItemOption3(ZulrahItems.TOXIC_TRIDENT_EMPTY) @Suspendable {
			it.player().message("Your toxic trident has <col=FF0000>no charges nor scales</col> remaining.")
		}
		
		r.onEquipmentOption(1, ZulrahItems.TOXIC_TRIDENT_EMPTY) {
			it.player().message("Your toxic trident has <col=FF0000>no charges nor scales</col> remaining.")
		}
		
		// Check option, no charges.
		r.onItemOption3(ZulrahItems.TRIDENT_EMPTY) @Suspendable {
			it.message("This trident has <col=FF0000>no</col> charges.")
		}
		
		r.onEquipmentOption(1, ZulrahItems.TRIDENT_EMPTY) {
			it.message("This trident has <col=FF0000>no</col> charges.")
		}
		
		// Dismantle uncharged toxic trident (to get magic fang back)
		r.onItemOption4(ZulrahItems.TOXIC_TRIDENT_EMPTY) @Suspendable {
			if (!it.itemUsed().hasProperties()) {
				it.doubleItemBox("This trident is <col=FF0000>uncharged</col>. Would you like to remove the fang?", ZulrahItems.TRIDENT_FULL,
						ZulrahItems.MAGIC_FANG)
				if (it.options("Remove the fang.", "Never mind.") == 1) {
					if (it.player().inventory().freeSlots() < 2) {
						it.player().message("You need two free inventory slots to do this.")
					} else if (it.player().inventory().remove(Item(ZulrahItems.TOXIC_TRIDENT_EMPTY), true).success()) {
						it.player().inventory().add(Item(ZulrahItems.TRIDENT_EMPTY), true)
						it.player().inventory().add(Item(ZulrahItems.MAGIC_FANG), true)
					}
				}
			} else {
				val charges = it.itemUsed().property(ItemAttrib.CHARGES)
				it.message("Your trident has $charges charges remaining. You must remove these before you can dismantle the trident.")
			}
		}
		
		// Trying to charge the NORMAL trident - coins and runes, no scales required.
		for (supply in intArrayOf(995, 554, 562, 560)) {
			for (trident in intArrayOf(ZulrahItems.TRIDENT_USED, ZulrahItems.TRIDENT_EMPTY, ZulrahItems.TRIDENT_FULL)) {
				r.onItemOnItem(supply, trident, s@ @Suspendable {
					
					val tridentSlot = ItemOnItem.slotOf(it, trident)
					val tridentItem = it.player().inventory()[tridentSlot]
					val tridId = tridentItem.id()
					val currentCharges = tridentItem.property(ItemAttrib.CHARGES)
					
					// If the system is disabled, replace empty with used.
					if (trident == ZulrahItems.TRIDENT_EMPTY && !ZulrahItems.CHARGING_ENABLED) {
						if (it.player().inventory().remove(tridentItem, true).success()) {
							it.player().inventory().add(Item(ZulrahItems.TRIDENT_USED), true, tridentSlot)
							it.message("You don't need to charge this item for it to work.")
						}
						return@s
					}
					if (!ZulrahItems.CHARGING_ENABLED) {
						it.message("You don't need to charge this item for it to work.")
						return@s
					}
					// These versions cannot be obtained unless charged into that form.
					if (!tridentItem.hasProperties() && (trident == ZulrahItems.TRIDENT_FULL || trident == ZulrahItems.TOXIC_TRIDENT_USED || tridId == ZulrahItems.TRIDENT_USED)) {
						it.itemBox("This type of trident cannot be charged. You have to charge an uncharge trident first.", trident)
					} else if (trident == ZulrahItems.TRIDENT_FULL) {
						it.itemBox("Your trident is already fully charged.", trident)
					} else {
						val supplyItemSlot = ItemOnItem.slotOf(it, supply)
						val supplyItem = it.player().inventory()[supplyItemSlot]
						
						// Depends on how many runes, coins etc in inventory. 1 death rune, 1 chaos rune, 5 fire runes and 10 coins per cast
						val deathCharge = it.player().inventory().count(560)
						val fireCharge = it.player().inventory().count(554) / 5
						val chaosCharge = it.player().inventory().count(562)
						val coinsCharge = if (it.player().world().realm().isPVP) 2500 else it.player().inventory().count(995) / 10
						var attemptedAdd = Math.min(coinsCharge, Math.min(fireCharge, Math.min(deathCharge, Math.min(deathCharge, 2500))))
						it.player().debug("charging info: %d, %d, %d, %d = %d. Current:%d", deathCharge, fireCharge, chaosCharge, coinsCharge, attemptedAdd, currentCharges)
						
						// Max charges 2.5k
						var chargesToAdd = Math.min(attemptedAdd, 2500 - currentCharges)
						if (currentCharges == 2500) {
							it.message("The trident is already fully charged.")
						} else if (chargesToAdd == 0) {
							it.message("You do not have enough supplies to charge this trident.")
						} else {
							// Confirm quantities
							it.doubleItemBox(
									"You can add $chargesToAdd charges to your staff, requiring $chargesToAdd death, $chargesToAdd chaos, ${chargesToAdd * 5} fire runes and ${10 * chargesToAdd} coins. " +
											"Do you want to continue?", tridentItem, Item(995, coinsCharge))
							// Are you sure?
							if (it.options("Yes, add $chargesToAdd charges.", "Never mind.") == 1) {
								
								// Add charges, animate.
								tridentItem.property(ItemAttrib.CHARGES, tridentItem.property(ItemAttrib.CHARGES) + chargesToAdd)
								it.player().animate(1979)
								it.player().graphic(76, 80, 30)
								val newTotal = tridentItem.property(ItemAttrib.CHARGES)
								
								// Empty tridents become charged/full. Normal 'used' trident becomes full if charges == 2.5k (max)
								val newTrident = tridId == ZulrahItems.TRIDENT_EMPTY || tridId == ZulrahItems.TOXIC_TRIDENT_EMPTY || (newTotal == 2500 && tridId == ZulrahItems.TRIDENT_USED)
								var extra = ""
								var newId = tridId
								if (newTrident) {
									// Replace with a different trident. Maybe full or used
									it.player().inventory().remove(tridentItem, false, tridentSlot)
									newId = when (tridId) {
										ZulrahItems.TOXIC_TRIDENT_EMPTY -> ZulrahItems.TOXIC_TRIDENT_USED
										ZulrahItems.TRIDENT_EMPTY, ZulrahItems.TRIDENT_USED -> if (newTotal == 2500) ZulrahItems.TRIDENT_FULL else ZulrahItems.TRIDENT_USED
										else -> tridId
									}
									// If we're adding a new trident, delete the old (Above) and add the new, duplicating the updated charges property.
									it.player().inventory().add(Item(newId).duplicateProperties(tridentItem), false, tridentSlot)
								}
								// Take away the runnnes
								it.player().inventory() -= Item(554, attemptedAdd * 5)
								it.player().inventory() -= Item(560, attemptedAdd)
								it.player().inventory() -= Item(562, attemptedAdd)
								if (!it.player().world().realm().isPVP)
									it.player().inventory() -= Item(995, attemptedAdd * 10)
								if (newTotal == 2500) { // Be insightful
									extra = "It is now fully charged."
								}
								it.itemBox("You add $chargesToAdd charges to your trident. It now has $newTotal charges. $extra", trident)
							}
						}
					}
				})
			}
		}
		
		// Trying to charge the TOXIC trident - requires zulrah scales to charge too!
		for (supply in intArrayOf(995, 554, 562, 560, ZulrahItems.ZULRAHS_SCALES)) {
			for (trident in intArrayOf(ZulrahItems.TOXIC_TRIDENT_EMPTY, ZulrahItems.TOXIC_TRIDENT_USED)) {
				r.onItemOnItem(supply, trident, s@ @Suspendable {
					
					
					val tridentSlot = ItemOnItem.slotOf(it, trident)
					val tridentItem = it.player().inventory()[tridentSlot]
					val tridId = tridentItem.id()
					val currentCharges = tridentItem.property(ItemAttrib.CHARGES)
					
					// If the system is disabled, replace empty with used.
					if (trident == ZulrahItems.TOXIC_TRIDENT_EMPTY && !ZulrahItems.CHARGING_ENABLED) {
						if (it.player().inventory().remove(tridentItem, true).success()) {
							it.player().inventory().add(Item(ZulrahItems.TOXIC_TRIDENT_USED), true, tridentSlot)
							it.message("You don't need to charge this item for it to work.")
						}
						return@s
					}
					if (!ZulrahItems.CHARGING_ENABLED) {
						it.message("You don't need to charge this item for it to work.")
						return@s
					}
					
					if (!tridentItem.hasProperties() && (trident == ZulrahItems.TRIDENT_FULL || trident == ZulrahItems.TOXIC_TRIDENT_USED || tridId == ZulrahItems.TRIDENT_USED)) {
						it.itemBox("This type of trident cannot be charged. You have to charge an uncharge trident first.", trident)
					} else if (trident == ZulrahItems.TRIDENT_FULL) {
						it.itemBox("Your trident is already fully charged.", trident)
					} else {
						val supplyItemSlot = ItemOnItem.slotOf(it, supply)
						val supplyItem = it.player().inventory()[supplyItemSlot]
						
						// Depends on how many runes, coins etc in inventory. 1 death rune, 1 chaos rune, 5 fire runes and 10 coins per cast
						val deathCharge = it.player().inventory().count(560)
						val fireCharge = it.player().inventory().count(554) / 5
						val chaosCharge = it.player().inventory().count(562)
						val coinsCharge = if (it.player().world().realm().isPVP) 2500 else it.player().inventory().count(995) / 10
						val scalesCharge = it.player().inventory().count(ZulrahItems.ZULRAHS_SCALES)
						var attemptedAdd = Math.min(scalesCharge, Math.min(coinsCharge, Math.min(fireCharge, Math.min(deathCharge, Math.min(deathCharge, 2500)))))
						it.player().debug("charging info: %d, %d, %d, %d, %d = %d. Current:%d", deathCharge, fireCharge, chaosCharge, coinsCharge, scalesCharge, attemptedAdd, currentCharges)
						
						// Max charges 2.5k
						var chargesToAdd = Math.min(attemptedAdd, 2500 - currentCharges)
						if (currentCharges == 2500) {
							it.message("The trident is already fully charged.")
						} else if (chargesToAdd == 0) {
							it.message("You do not have enough supplies to charge this trident.")
						} else {
							// Confirm quantities
							it.doubleItemBox(
									"You can add $chargesToAdd charges to your staff, requiring $chargesToAdd death, $chargesToAdd chaos, ${chargesToAdd * 5} fire runes, $chargesToAdd scales and ${10 * chargesToAdd} coins. " +
											"Do you want to continue?", tridentItem, Item(995, coinsCharge))
							// Are you sure?
							if (it.options("Yes, add $chargesToAdd charges.", "Never mind.") == 1) {
								
								// Add charges, animate.
								tridentItem.property(ItemAttrib.CHARGES, tridentItem.property(ItemAttrib.CHARGES) + chargesToAdd)
								it.player().animate(1979)
								it.player().graphic(76, 80, 30)
								val newTotal = tridentItem.property(ItemAttrib.CHARGES)
								
								// Empty tridents become charged/full. Normal 'used' trident becomes full if charges == 2.5k (max)
								val newTrident = tridId == ZulrahItems.TRIDENT_EMPTY || tridId == ZulrahItems.TOXIC_TRIDENT_EMPTY || (newTotal == 2500 && tridId == ZulrahItems.TRIDENT_USED)
								var extra = ""
								var newId = tridId
								if (newTrident) {
									// Replace with a different trident. Maybe full or used
									it.player().inventory().remove(tridentItem, false, tridentSlot)
									newId = when (tridId) {
										ZulrahItems.TOXIC_TRIDENT_EMPTY -> ZulrahItems.TOXIC_TRIDENT_USED
										ZulrahItems.TRIDENT_EMPTY, ZulrahItems.TRIDENT_USED -> if (newTotal == 2500) ZulrahItems.TRIDENT_FULL else ZulrahItems.TRIDENT_USED
										else -> tridId
									}
									// If we're adding a new trident, delete the old (Above) and add the new, duplicating the updated charges property.
									it.player().inventory().add(Item(newId).duplicateProperties(tridentItem), false, tridentSlot)
								}
								// Take away the runnnes
								it.player().inventory() -= Item(554, attemptedAdd * 5)
								it.player().inventory() -= Item(560, attemptedAdd)
								it.player().inventory() -= Item(562, attemptedAdd)
								if (!it.player().world().realm().isPVP)
									it.player().inventory() -= Item(995, attemptedAdd * 10)
								it.player().inventory() -= Item(ZulrahItems.ZULRAHS_SCALES, attemptedAdd)
								if (newTotal == 2500) { // Be insightful
									extra = "It is now fully charged."
								}
								it.itemBox("You add $chargesToAdd charges to your trident. It now has $newTotal charges. $extra", trident)
							}
						}
					}
				})
			}
		}
	}
	
	private fun send_casts_message(it: Script, get: Item) {
		if (!get.hasProperties())
			it.player().message("The trident is <col=FF0000>uncharged</col>.")
		else {
			val sb = StringBuilder()
			sb.append("Your trident has <col=FF0000>${((get.property(ItemAttrib.CHARGES) as Int).toDouble() / 2500).toFloat() * 100}%</col> charges (${get.property(ItemAttrib.CHARGES)} casts) remaining.")
			it.player().message(sb.toString())
		}
	}
}