package nl.bartpelle.veteres.content.areas.global

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 2/5/2016.
 */
object PollBooth {
	
	val POLL_BOOTH = 26813
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(POLL_BOOTH) @Suspendable {
			it.player.write(AddMessage("https://www.os-scape.com/polls", AddMessage.Type.URL))
			//it.chatPlayer("I don't think Bart or Nick really care about my opinions...")
			//it.messagebox("You scribble down your thoughts and put them in the booth anyway.")
		}
	}
	
}