package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 1/5/2016.
 */
object PvPCombining {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// PVP world item combining:
		combine(r, 12804, 11838, 12809) //Sardomin's blessed sword
		combine(r, 4151, 12769, 12774) // Frozen abyssal whip
		combine(r, 4151, 12771, 12773) // Volcanic abyssal whip
		combine(r, 12757, 11235, 12766) // Blue bow mix
		combine(r, 12759, 11235, 12765) // Green bow mix
		combine(r, 12761, 11235, 12767) // Yellow bow mix
		combine(r, 12763, 11235, 12768) // White bow mix
		combine(r, 12802, 11926, 12807) // Odium ward
		combine(r, 12802, 11924, 12806) // Malediction ward
		combine(r, 6585, 12526, 12436) // Amulet of fury (or)
		combine(r, 12849, 4153, 12848) // Granite maul (or)
		combine(r, 12530, 6918, 12419) // Light infinity
		combine(r, 12530, 6916, 12420) // Light infinity
		combine(r, 12530, 6924, 12421) // Light infinity
		combine(r, 12528, 6918, 12457) // Dark infinity
		combine(r, 12528, 6916, 12458) // Dark infinity
		combine(r, 12528, 6924, 12459) // Dark infinity
		combine(r, 12954, 20143, 19722) // Dragon defender
		combine(r, 4587, 20002, 20000) // Dragon scimitar
		combine(r, 11920, 12800, 12797) // Dragon pickaxe
		combine(r, 19553, 20062, 20366) // Amulet of torture
		combine(r, 12002, 20065, 19720) // Occult necklace
		combine(r, 11802, 20068, 20368) // Armadyl godsword
		combine(r, 11804, 20071, 20370) // Bandos godsword
		combine(r, 11806, 20074, 20372) // Saradomin godsword
		combine(r, 11808, 20077, 20374) // Zamorak godsword
		combine(r, 11791, 13256, 22296) //Staff of light
		combine(r, 21892, 22236, 22242) //Dragon platebody (or)
		combine(r, 21895, 22239, 22244) //Dragon kiteshield (or)
		combine(r, 11840, 22231, 22234) //Dragon boots (or)
		combine(r, 19547, 22246, 22249) //Necklace of anguish (or)
		
		// Economy world item combining:
		combineEco(r, 11286, 1540, 11283) // Dragonfire shield
	}
	
	fun combine(r: ScriptRepository, item1: Int, item2: Int, result: Int) {
		r.onItemOnItem(item1.toLong(), item2.toLong()) @Suspendable {
			
			if (Staking.screen_closed(it.player()) && it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
				it.doubleItemBox("Combining these items will be irreversible. Are you sure you want to combine them?", item1, item2)
				if (it.optionsTitled("Proceed with combining?", "Yes.", "No.") == 1) {
					if (it.player().inventory().contains(item1) && it.player().inventory().contains(item2)) {
						it.player().inventory() -= item1
						it.player().inventory() -= item2
						it.player().inventory() += result
						it.itemBox("You combine the ${Item(item1).definition(r.server().definitions()).name} and " +
								"${Item(item2).definition(r.server().definitions()).name} into the ${Item(result).definition(r.server().definitions()).name}.", result)
					}
				}
			}
		}
	}
	
	fun combine(r: ScriptRepository, kit: Int, item1: Int, item2: Int, item3: Int, result1: Int, result2: Int, result3: Int) {
		for (infinity in intArrayOf(item1, item2, item3)) {
			r.onItemOnItem(kit.toLong(), infinity.toLong()) @Suspendable {
				if (Staking.screen_closed(it.player()) && it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
					it.doubleItemBox("Combining these items will be irreversible. Are you sure you want to combine them?", kit, item1)
					if (it.optionsTitled("Proceed with combining?", "Yes.", "No.") == 1) {
						
						if (it.player().inventory().contains(item1) && it.player().inventory().contains(item2) && it.player().inventory().contains(item3)
								&& it.player().inventory().contains(kit)) {
							it.player().inventory() -= kit
							it.player().inventory() -= item1
							it.player().inventory() -= item2
							it.player().inventory() -= item3
							it.player().inventory() += result1
							it.player().inventory() += result2
							it.player().inventory() += result3
						} else {
							it.player().message("You don't have the required supplies to do this.")
						}
					}
				}
			}
		}
	}
	
	fun combineEco(r: ScriptRepository, item1: Int, item2: Int, result: Int) {
		r.onItemOnItem(item1.toLong(), item2.toLong()) @Suspendable {
			if (Staking.screen_closed(it.player())) {
				it.doubleItemBox("Combining these items will be irreversible. Are you sure you want to combine them?", item1, item2)
				if (it.optionsTitled("Proceed with combining?", "Yes.", "No.") == 1) {
					it.player().inventory() -= item1
					it.player().inventory() -= item2
					it.player().inventory() += result
					it.itemBox("You combine the ${Item(item1).definition(r.server().definitions()).name} and " +
							"${Item(item2).definition(r.server().definitions()).name} into the ${Item(result).definition(r.server().definitions()).name}.", result)
				}
			}
		}
	}
	
}