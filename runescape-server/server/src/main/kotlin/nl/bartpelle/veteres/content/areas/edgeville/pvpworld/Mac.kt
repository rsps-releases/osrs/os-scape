package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.nardah.Zahur
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Mack
 */
object Mac {

	//NPC dialogue ID's used during the chat
	val MAC = 6481
	val MAXCAPE = 13280
	val MAXHOOD = 13281

	// We're 3 skills short atm haha!
	@JvmStatic var TOTAL_LEVEL_FOR_MAXED = 2178 + 1 // 22 99's and 1 last level you can't level up (construction)

	//Register the NPC option 1
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MAC, @Suspendable { it.player().world().shop(8).display(it.player()) })
		r.onNpcOption2(MAC, {
            if(it.player().world().realm().isPVP) {
                Zahur.decantAllPotions(it)
            } else {
                it.player().world().shop(8).display(it.player())
            }
		})
	}

	@Suspendable fun initiate(it: Script) {
		displayOptions(it)
	}

	@Suspendable fun displayOptions(it: Script) {
		when (it.options("View Shop", "Can I buy a 'Mac's cape'?", "Why are you so dirty?", "Bye.")) {
			1-> {
				if (it.player().world().realm().isPVP) {
					it.player().world().shop(8).display(it.player())
				}
			}
			2 -> {
				it.chatPlayer("Can I buy a 'Mac's cape'?")

				if (!success(it)) {
					return
				}

				onSuccess(it)
			}
			3 -> {
				it.chatPlayer("Why are you so dirty?")
				it.chatNpc("Because bathing is xp waste.", MAC)
				it.chatPlayer("Interesting...")
			}
		}
	}

	@Suspendable fun op1(it: Script) {
		it.chatPlayer("Who are you?")
		it.chatNpc("I am Mac, an experienced traveler who roams the lands offering my services to those who are deemed fit.", MAC)
		it.chatPlayer("What services are you referring to?")
		it.chatNpc("I can sell you one of my signature capes so long as you meet the requirements.", MAC)
		verifyRequirements(it)
	}

	@Suspendable fun verifyRequirements(it: Script) {
		when (it.options("Yes, I'd like to buy a cape.", "No thanks, I have more training to do.")) {
			1 -> {
				it.chatPlayer("Yes, I'd like to buy a cape.")

				if (!success(it)) {
					return
				}

				onSuccess(it)
			}
			2 -> {
				it.chatPlayer("No thanks, I have more training to do.")
				it.chatNpc("Well, better get to it then.", MAC)
			}
		}
	}

	@Suspendable fun onSuccess(it: Script) {
		it.chatNpc("Hmm... very well. You seem like a suitable adventurer to wear my cape. However, this is not cheap", MAC)
		it.chatNpc("cape. To purchase one of my crafted signature capes it'll cost ye 150,000 blood money.", MAC)
		when (it.optionsTitled("Pay Mac the fee?", "Yes, I understand. Take my blood money.", "On second thought, I don't think I can afford that.")) {
			1 -> {
				it.chatPlayer("Yes, I understand. Take my blood money.")
				if (it.player().inventory().has(Item(13307)) && it.player().inventory().byId(13307).amount() >= 150_000) {
					it.player().inventory().remove(Item(13307, 150_000), true)
					it.player().inventory().addOrDrop(Item(MAXCAPE), it.player())
					it.player().inventory().addOrDrop(Item(MAXHOOD), it.player())
					it.doubleItemBox("Mac carefully removes a cape and hood from his bag. The cape is heavy and made from fine cloth.", MAXCAPE, MAXHOOD)
					it.chatNpc("Here you are. Hold onto it dearly as these capes are not easy to make.", MAC)
				} else {
					it.chatNpc("Sorry, but it appears as if you do not have enough blood money to afford this cape.", MAC)
				}
			}
			2 -> {
				it.chatPlayer("On second thought, I don't think I can afford that.")
			}
		}
	}

	@Suspendable fun success(it: Script): Boolean {
		var counter = 0
		val message = StringBuilder("Sorry but you require a skill level of 99 in ")
		for (skillId in 0 until Skills.SKILL_COUNT) {
			if (it.player().skills().xpLevel(skillId) < 99) {
//				message.append(Skills.SKILL_NAMES[skillId]).append(", ")
				counter++
			}
		}
		if (counter > 0) {
			val pluralOr = if (counter == 1) "skill" else "skills"
			message.append("$counter more $pluralOr. Better get to it.")
			it.chatNpc(message.toString(), MAC)
			return false
		}

		return true
	}

	@JvmStatic fun totallevel(player: Player): Int {
		var total = 0
		for (i in 0..22) {
			total += player.skills().xpLevel(i)
		}
		return total
	}
}
