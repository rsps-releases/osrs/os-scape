package nl.bartpelle.veteres.content.minigames.duelingarena

import nl.bartpelle.veteres.content.minigames.duelingarena.Staking.duelPartner
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.AddReceivedPrivateMessage
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 04/11/2016.
 */
object DuelAntiScams {
	
	// A boolean to assess whether a stake should probably be a custom one, but not enough rules were triggered
	// to make it custom. Maybe someone is scasmming.
	@JvmStatic fun suspicious_stake(player: Player): Boolean {
		var armour_off: Int = 0
		arrayOf(EquipSlot.HEAD, EquipSlot.CAPE, EquipSlot.AMULET, EquipSlot.BODY, EquipSlot.SHIELD, EquipSlot.LEGS, EquipSlot.FEET, EquipSlot.RING, EquipSlot.HANDS).forEach { slot ->
			if (Staking.equipment_disabled(player, slot)) {
				armour_off += 1
			}
		}
		var activeUsualRules = 0
		arrayOf(DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_DRINKS), DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_FOOD),
				DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_PRAYER), DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC),
				DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED)).forEach { b -> if (b) activeUsualRules++ }
		
		if (armour_off >= 7 && activeUsualRules >= 3) {
			val partner = player.duelPartner() ?: return false
			player.world().server().scriptExecutor().executeScript(player, DuelArenaScamWarning(player, armour_off, activeUsualRules))
			partner.world().server().scriptExecutor().executeScript(partner, DuelArenaScamWarning(partner, armour_off, activeUsualRules))
			return true
		}
		return false
	}
	
	
	fun cantMageinMageOnly(player: Player): Boolean {
		val book = player.varps().varbit(Varbit.SPELLBOOK)
		//var hasRunes: Boolean = false
		var runeCount: Int = 0 // need at least 2 tbf
		for (i in player.inventory().items()) {
			if (i == null) continue
			else if (i.id() in (554..565) || i.id() == 9075) {
				runeCount++
			}
		}
		if (book == 0) { // modern
			// TODO best way to determine damaging spells and check all reqs. what about a worn staff?? hasRunes will check that!
		} else if (book == 1) { // ancients
			
		}
		return runeCount < 2
	}
	
	
	// Names of ranging weapons. We check these if the stake is range-only. Stops people getting scammed if they don't have ANY form of range wep.
	val okPrimaries = arrayOf("dart", "knif", "kniv", "javelin", "thrownaxe", "toktz-xil-ul", "blowpipe", "crystal bow", "chinchompa")
	
	// Are we trying to start a range-only stake w/o ranging equipment? are you a spastic? or blind? You're getting scammed!
	fun rangeOnlyWithoutRangeGear(player: Player): Boolean {
		val world = player.world()
		if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC) && DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MELEE) &&
				!DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED)) {
			if (Staking.equipment_disabled(player, EquipSlot.WEAPON)) {
				player.message("How can you do a range stake without a weapon?")
				return true
			}
			val wep: Item? = player.equipment().get(3)
			if (wep != null) {
				val wepname: String = wep.definition(world).name.toLowerCase()
				var hasOkWep: Boolean = false
				for (s in okPrimaries) {
					if (wepname.contains(s)) {
						hasOkWep = true
						break
					}
				}
				if (!hasOkWep) { // Not a ammo-requiring item
					val ammo: Item? = player.equipment().get(EquipSlot.AMMO)
					if (ammo != null) {
						// check for correct wep
						val ammoname: String = ammo.definition(world).name.toLowerCase()
						if (ammoname.contains("bolt")) {
							if (!wepname.contains("crossbow")) {
								player.message("You don't have range gear for this range stake.")
								return true
							}
						} else if (ammoname.contains("arrow")) {
							if (!wepname.contains("bow") && !wepname.contains("crossbow")) { // can't use a crossbow with arrows
								player.message("You don't have range gear for this range stake.")
								return true
							}
						}
					} else {
						// cut code from inventoryHasRangeGear
						var hasArrows: Boolean = false
						var hasBolts: Boolean = false
						for (i in player.inventory().items()) {
							if (i == null) continue
							val itemname: String = i.definition(world).name.toLowerCase()
							if (itemname.contains("bolt")) {
								hasBolts = true
							} else if (itemname.contains("arrow")) {
								hasArrows = true
							}
						}
						if (wepname.contains("crossbow") && !hasBolts) {
							player.message("You don't have bolts.")
							return true
						} else if (wepname.contains("bow") && !wepname.contains("crossbow") && !hasArrows) {
							player.message("You don't have arrows.")
							return true
						}
					}
				}
				return false
			} else {
				// Check inventory for stuff.
				var hasPrimary: Boolean = false
				for (s in okPrimaries) {
					if (hasPrimary) break
					for (i in player.inventory().items()) {
						if (i == null) continue
						else if (i.definition(world).name.toLowerCase().contains(s)) {
							hasPrimary = true
							break
						}
					}
				}
				if (!hasPrimary) {
					// Check crossbows and ammo
					if (!inventoryHasRangeGear(player, world)) {
						player.message("You don't have range gear.")
						return true
					}
				}
				return false
			}
		}
		return false
	}
	
	// Do we have ranging equipment for a range-only stake?
	fun inventoryHasRangeGear(player: Player, world: World): Boolean {
		// Find out stuff
		var hasArrows: Boolean = false
		var hasBolts: Boolean = false
		var hasBow: Boolean = false
		var hasCrossbow: Boolean = false
		for (i in player.inventory().items()) {
			if (i == null) continue
			val itemname: String = i.definition(world).name.toLowerCase()
			if (itemname.contains("bolt")) {
				hasBolts = true
			} else if (itemname.contains("arrow")) {
				hasArrows = true
			} else if (itemname.contains("crossbow")) {
				hasCrossbow = true
			} else if (itemname.contains("bow") && !itemname.contains("crossbow")) {
				hasBow = true
			} else if (itemname.contains("crystal bow")) { // No ammo needed
				return true
			}
		}
		// Check inv for ammo and wep
		val nocb: Boolean = (hasCrossbow && !hasBolts) || (hasBolts && !hasCrossbow) || (!hasBolts && !hasCrossbow)
		val nobow: Boolean = (hasBow && !hasArrows) || (hasArrows && !hasBow) || (!hasArrows && !hasArrows)
		// return not missing crossbow OR not missing bow (if you have either, you're good to go)
		return !nocb || !nobow
	}
	
	
	// Are our varp values out of sync with our opponents? Should never happen.
	fun different_settings(player: Player, partner: Player): Boolean {
		val settings = player.varps().varp(Varp.STAKE_SETTINGS)
		// Security. Far as I'm concerned this is absolutely impossible, but video evidence slightly proves otherwise...
		// https://vid.me/nfvR
		if (settings != partner.varps().varp(Varp.STAKE_SETTINGS)) {
			player.message("Your rules are not the same!")
			partner.message("Your rules are not the same!")
			
			// Notify moderators
			player.world().players().forEachKt({ p ->
				if (p != null && p.privilege().eligibleTo(Privilege.MODERATOR)) {
					p.write(AddReceivedPrivateMessage("DupeWatch", 2, 0, "WARNING: duel arena glitching: " + player.name() + " & " + partner.name() + "; see chatbox!"))
					p.message("<col=ff0000>[DUPEWATCH] " + player.name() + " vs " + partner.name() + " tried dueling when their options are NOT equal! Tell Jak!")
				}
			})
			return true
		}
		return false
	}
	
	
	// Our settings will never be out of sync.
	@JvmStatic fun bug_abused_rules(player: Entity, entity: Entity): Boolean {
		if (player.isPlayer && Staking.in_duel(player) && entity.isPlayer && Staking.in_duel(entity)) {
			val p: Player = player as Player
			val partner = p.duelPartner() ?: return true
			
			if (p.varps().varp(Varp.STAKE_SETTINGS) != entity.varps().varp(Varp.STAKE_SETTINGS)) {
				
				p.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
				partner.putattrib(AttributeKey.STAKING_DOUBLE_DEATH, true)
				
				p.clearattrib(AttributeKey.ARENA_DEATH_TICK)
				partner.clearattrib(AttributeKey.ARENA_DEATH_TICK)
				
				// FORFIET
				Staking.on_death(p)
				return true
			}
		}
		return false
	}
}