package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveGame
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.GameCommands
import java.lang.ref.WeakReference

/**
 * Created by Jak on 10/01/2016.
 */
@Suspendable object ModeratePlayerOptions {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val mod = it.player()
		mod.timers().extendOrRegister(TimerKey.COMBAT_IMMUNITY, 50) // 30s
		
		// Checks for ranks PLUS modop is actually enabled, otherwise it might be some other displayed player option for another system.
		if (mod.privilege().eligibleTo(Privilege.MODERATOR)) {
			
			if (it.player().attribOr<Boolean>(AttributeKey.MODOPS, false)) {
				
				val other: Player = it.player().attrib<WeakReference<Player>>(AttributeKey.TARGET).get() ?: return@s
				showModMenu(it, other)
			} else {
				// How'd you do this haha?
				it.message("Huh?")
			}
		} else if (mod.helper()) {
			if (it.player().attribOr<Boolean>(AttributeKey.MODOPS, false)) {
				val other: Player = it.player().attrib<WeakReference<Player>>(AttributeKey.TARGET).get() ?: return@s
				showHelperMenu(it, other)
			} else {
				// How'd you do this haha?
				it.message("Huh??")
			}
		}
	}
	
	@Suspendable private fun showHelperMenu(it: Script, opp: Player) {
		val name: String = opp.name()
		when (it.optionsTitled("Helper actions on : " + name, "Jail options...", "Mute options...", "Never mind")) {
			1 -> showJailOptions(it, opp)
			2 -> showMuteOptions(it, opp)
		}
	}
	
	@Suspendable private fun showMuteOptions(it: Script, opp: Player) {
		val name: String = opp.name()
		when (it.optionsTitled("Mute actions on : " + name, "Mute", "Unmute", "Setmute (altar the duration of the mute)", "Back")) {
			1 -> mute(it, opp)
			2 -> unmute(it, opp)
			3 -> setmute(it, opp)
			4 -> if (it.player().helper()) showHelperMenu(it, opp) else showModMenu(it, opp)
		}
	}
	
	@Suspendable private fun showJailOptions(it: Script, opp: Player) {
		val name: String = opp.name()
		when (it.optionsTitled("Jail actions on : " + name, "Jail", "Unjail", "Back")) {
			1 -> jail(it, opp)
			2 -> unjail(it, opp)
			3 -> if (it.player().helper()) showHelperMenu(it, opp) else showModMenu(it, opp)
		}
	}
	
	@Suspendable fun showModMenu(it: Script, opp: Player) {
		val name: String = opp.name()
		val ip = if (it.player().privilege().eligibleTo(Privilege.ADMIN)) "(" + opp.ip() + ")" else ""
		
		val title = "Mod actions on $name $ip"
		
		if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {
			when (it.optionsTitled(title, "Punish " + name, "View " + name, "Put player into TzTok-Jad fight caves", "Never mind")) {
				1 -> showModPunishments(it, opp)
				2 -> view(it, opp)
				3 -> {
					val wave = it.inputInteger("What wave will ${opp.name()} start on?")
					if (wave in 1..63 && it.options("Restart caves on wave $wave for ${opp.name()}", "Nvm") == 1) {
						opp.putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, wave)
						opp.executeScript { s -> FightCaveGame.initiateFightCaves(s, false) }
					}
				}
			// 3 is never mind. does nothing.
			}
		} else {
			when (it.optionsTitled(title, "Punish " + name, "Never mind")) {
				1 -> showModPunishments(it, opp)
			// 2 is never mind. does nothing.
			}
		}
	}
	/*
	@Suspendable fun modMiscMenu(it: Script, opp: Player) {
		when (it.optionsTitled("Misc actions on ${opp.name()}", "Refund Preserve", "Refund Augury", "Refund Rigour", "Restart fight caves on on wave [x]")) {
			1 -> refundPrayer(it, opp, Varbit.UNLOCK_PRESERVE, PrayerScroll.PRAYER_SCROLL)
			2 -> refundPrayer(it, opp, Varbit.UNLOCK_AUGURY, PrayerScroll.PRAYER_SCROLL)
			3 -> refundPrayer(it, opp, Varbit.UNLOCK_RIGOUR, PrayerScroll.PRAYER_SCROLL)
			4 -> {
				val wave = it.inputInteger("What wave will ${opp.name()} start on?")
				if (wave in 1..63 && it.options("Restart caves on wave $wave for ${opp.name()}", "Nvm") == 1) {
					opp.putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, wave)
					opp.executeScript { s -> FightCaveGame.initiateFightCaves(s, false) }
				}
			}
		}
	}*/
	
	@JvmStatic fun refundPrayer(it: Script, opp: Player, varbit: Int, refundItem: Int) {
		if (!(it.player().privilege().eligibleTo(Privilege.ADMIN) || it.player().seniorModerator())) {
			it.message("Refunding raids prayers is a Senior mod+ action.")
		} else if (opp.varps().varbitBool(varbit)) {
			if (opp.inventory().add(Item(refundItem), false).success()) {
				opp.varps().varbit(varbit, 0)
				it.message("${opp.name()}'s raids prayer was revoked and a Prayer Scroll was added to their inventory.".col("FF0000"))
				opp.message("${it.player().name()} revoked a raids prayer and refunded a Prayer Scroll to your inventory.".col("FF0000"))
			} else {
				it.message("${opp.name()} did not have a free space to hold a Prayer scroll.".col("FF0000"))
			}
		} else {
			it.message("${opp.name()} did not have that raids prayer unlocked.".col("FF0000"))
		}
	}
	
	@Suspendable fun showModPunishments(it: Script, opp: Player) {
		val name: String = opp.name()
		when (it.optionsTitled("Punish $name.", "Mute options...", "Ban types...", "Kick " + name, "Jail options...", "Back")) {
			1 -> showMuteOptions(it, opp)
			2 -> showBanOptions(it, opp)
			3 -> kick(it, opp)
			4 -> showJailOptions(it, opp)
			5 -> if (it.player().helper()) showHelperMenu(it, opp) else showModMenu(it, opp)
		}
	}
	
	@Suspendable fun showBanOptions(it: Script, opp: Player) {
		val name: String = opp.name()
		when (it.optionsTitled("Punish $name.", "Normal ban " + name, "UUID ban " + name, "Set ban duration", "Back")) {
			1 -> ban(it, opp)
			2 -> macban(it, opp)
			3 -> setban(it, opp)
			4 -> if (it.player().helper()) showHelperMenu(it, opp) else showModMenu(it, opp)
		}
	}
	
	fun view(it: Script, opp: Player) {
		GameCommands.process(it.player(), "view " + opp.name())
	}
	
	fun jail(it: Script, opp: Player) {
		GameCommands.process(it.player(), "jail " + opp.name())
	}
	
	fun unjail(it: Script, opp: Player) {
		GameCommands.process(it.player(), "unjail " + opp.name())
	}
	
	fun mute(it: Script, opp: Player) {
		GameCommands.process(it.player(), "mute " + opp.name())
	}
	
	fun unmute(it: Script, opp: Player) {
		GameCommands.process(it.player(), "unmute " + opp.name())
	}
	
	fun setmute(it: Script, opp: Player) {
		GameCommands.process(it.player(), "setmute " + opp.name())
	}
	
	fun ban(it: Script, opp: Player) {
		GameCommands.process(it.player(), "ban " + opp.name())
	}
	
	fun setban(it: Script, opp: Player) {
		GameCommands.process(it.player(), "setban " + opp.name())
	}
	
	fun macban(it: Script, opp: Player) {
		GameCommands.process(it.player(), "macban " + opp.name())
	}
	
	fun kick(it: Script, opp: Player) {
		GameCommands.process(it.player(), "kick " + opp.name())
	}
}