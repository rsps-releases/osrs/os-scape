package nl.bartpelle.veteres.content.npcs.pestcontrol

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.pest_control.Pest
import nl.bartpelle.veteres.content.minigames.pest_control.PortalType
import java.util.function.Consumer

/**
 * Created by Jason MacKeigan on 2016-07-14 at 5:04 PM
 */
object Portal {
	
	@JvmField val deathScript: Function1<Script, Unit> = suspend@ @Suspendable {
		val npc = it.npc()
		
		val portal = PortalType.values().first { it.shieldedNpcId == npc.id() }
		
		val position = npc.tile().transform(portal.xOffset * -1, portal.yOffset * -1)
		
		npc.world().npcs().forEach(Consumer {
			if (it != null && it.tile().equals(position.x, position.z)) {
				it.heal(50)
			}
		})
		
		val closePlayers = npc.closePlayers(8).filterNotNull()
		
		npc.closeNpcs(8).filterNotNull().filter { Pest.SPINNER.ids.contains(it.id()) }.forEach {
			closePlayers.forEach {
				it.poison(5)
			}
		}
	}
	
}