package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 31/10/2016.
 */
object ArdyCape {
	
	val ARDY_MAXCAPE = 20760
	val ARDY_CLOAK_4 = 13124
	val MONASTERY = Tile(2606, 3221)
	val ARDY_FARM = Tile(2664, 3375)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		for (cape in intArrayOf(ARDY_CLOAK_4, ARDY_MAXCAPE)) {
			repo.onItemOption3(cape) @Suspendable { ardyCapeTeleport(it) }
			repo.onEquipmentOption(1, cape) @Suspendable {
				if (Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
					Teleports.basicTeleport(it, MONASTERY, 3872, Graphic(1237, 92)) // Kandarin Monastery
				}
			}
			repo.onEquipmentOption(2, cape) @Suspendable {
				if (Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
					Teleports.basicTeleport(it, ARDY_FARM, 3872, Graphic(1237, 92)) // Ardy farm
				}
			}
		}
	}
	
	@JvmStatic @Suspendable fun ardyCapeTeleport(it: Script) {
		var target = it.player().tile()
		when (it.options("Kandarin Monastery", "Ardougne Farm")) {
			1 -> target = MONASTERY // Kandarin Monastery
			2 -> target = ARDY_FARM // Ardy farm
		}
		if (Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
			Teleports.basicTeleport(it, target, 3872, Graphic(1237, 92))
		}
	}
}