package nl.bartpelle.veteres.content.items

import nl.bartpelle.veteres.content.skills.mining.Mining
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.ItemAttrib

/**
 * Created by Bart on 3/26/2016.
 */
object InfernalTools {
	
	/**
	 * Flag used to check if the item passive is active or not.
	 */
	val active = false
	
	fun canUse(player: Player, id: Int): Boolean {
		val equipped = player.equipment()[EquipSlot.WEAPON]
		if (equipped != null && equipped.id() == Mining.Pickaxe.INFERNAL.id) {
			return true
		} else {
			// Loop through the inventory to find one with charges
			for (item in player.inventory()) {
				if (item != null && item.id() == Mining.Pickaxe.INFERNAL.id && item.property(ItemAttrib.CHARGES) > 0) {
					return true
				}
			}
		}
		
		return false
	}
	
}
