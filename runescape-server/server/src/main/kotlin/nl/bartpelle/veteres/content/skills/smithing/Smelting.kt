package nl.bartpelle.veteres.content.skills.smithing

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.skills.smithing.Smithing.ADAMANT_BAR
import nl.bartpelle.veteres.content.skills.smithing.Smithing.BRONZE_BAR
import nl.bartpelle.veteres.content.skills.smithing.Smithing.IRON_BAR
import nl.bartpelle.veteres.content.skills.smithing.Smithing.MITHRIL_BAR
import nl.bartpelle.veteres.content.skills.smithing.Smithing.RUNITE_BAR
import nl.bartpelle.veteres.content.skills.smithing.Smithing.STEEL_BAR
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceItem
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 10/3/2015.
 */
object Smelting {
	
	val FURNACES = intArrayOf(24009, 16469, 26300, 11978)
	val BARS = intArrayOf(2349, 2351, 2355, 2353, 2357, 2359, 2361, 2363)
	val RING_OF_FORGING = 2568
	
	@JvmStatic @ScriptMain fun smeltingTriggers(r: ScriptRepository) {
		// Furnace interaction triggers
		for (obj in FURNACES) {
			r.onObject(obj) @Suspendable { smeltDialogue(it) }
		}
		
		// Stray anvil
		r.onObject(2097) {
			val player = it.player()
			
			for (bar in BARS) {
				if (player.inventory().contains(bar)) {
					val type = when (bar) {
						BRONZE_BAR -> 1
						IRON_BAR -> 2
						STEEL_BAR -> 3
						MITHRIL_BAR -> 4
						ADAMANT_BAR -> 5
						RUNITE_BAR -> 6
						else -> 0
					}
					
					it.player().varps().varbit(Varbit.SMITHING_BAR_TYPE, type)
					
					if (type != 0)
						it.player().interfaces().sendMain(312)
					
					return@onObject
				}
			}
			it.messagebox("You should select an item from your inventory and use it on the anvil.")
		}
	}
	
	@JvmStatic @Suspendable fun smeltDialogue(s: Script) {
		if (s.player().world().realm().isPVP) {
			s.messagebox("That's a bit pointless is it not...?")
			return
		}
		s.player().write(
				InterfaceItem(311, 4, 2349, 150),
				InterfaceItem(311, 5, 9467, 150),
				InterfaceItem(311, 6, 2351, 150),
				InterfaceItem(311, 7, 2355, 150),
				InterfaceItem(311, 8, 2353, 150),
				InterfaceItem(311, 9, 2357, 150),
				InterfaceItem(311, 10, 2359, 150),
				InterfaceItem(311, 11, 2361, 150),
				InterfaceItem(311, 12, 2363, 150)
		)
		
		val (inter, child) = s.displayDialogueInterface(311, 162, 550, false)
		val opt = (child - 13) / 4
		val sub = (child - 13) % 4
		
		when (opt) {
			0 -> smelt(s, 2349, sub) // Bronze
			2 -> smelt(s, 2351, sub) // Iron
			3 -> smelt(s, 2355, sub) // Silver
			4 -> smelt(s, 2353, sub) // Steel
			5 -> smelt(s, 2357, sub) // Gold
			6 -> smelt(s, 2359, sub) // Mithril
			7 -> smelt(s, 2361, sub) // Adamant
			8 -> smelt(s, 2363, sub) // Rune
		}
	}
	
	@Suspendable private fun smelt(s: Script, bar: Int, count: Int) {
		// Turn a child into a number :)
		var num = when (count) {
			3 -> 1
			2 -> 5
			1 -> 10
			0 -> s.inputInteger("Enter amount:")
			else -> 0
		}
		
		if (!checkRequirements(s, s.player(), bar)) {
			return
		}
		
		// Smelt until we run out of resources, or until we are finished.
		while (num-- > 0) {
			// Check for ores..
			when (bar) {
				2349 -> { // Bronze bar
					if (436 !in s.player().inventory() || 438 !in s.player().inventory()) {
						s.message("You have run out of ore to smelt into bronze.")
						return
					}
				}
				2351 -> { // Iron bar
					if (440 !in s.player().inventory()) {
						s.message("You have run out of iron ore to smelt.")
						return
					}
				}
				2355 -> { // Silver bar
					if (442 !in s.player().inventory()) {
						s.message("You have run out of silver ore to smelt.")
						return
					}
				}
				2353 -> { // Steel bar
					if (440 !in s.player().inventory()) {
						s.message("You have run out of iron ore with which to make steel.")
						return
					}
					if (Item(453, 2) !in s.player().inventory()) {
						s.message("You don't have enough coal to make any more steel.")
						return
					}
				}
				2357 -> { // Gold bar
					if (444 !in s.player().inventory()) {
						s.message("You have run out of gold ore to smelt.")
						return
					}
				}
				2359 -> { // Mithril bar
					if (447 !in s.player().inventory()) {
						s.message("You have run out of mithril ore with which to make mithril.") // TODO msg
						return
					}
					if (Item(453, 4) !in s.player().inventory()) {
						s.message("You don't have enough coal to make any more mithril.") // TODO msg
						return
					}
				}
				2361 -> { // Adamant bar
					if (449 !in s.player().inventory()) {
						s.message("You have run out of adamantite ore with which to make adamant.") // TODO msg
						return
					}
					if (Item(453, 6) !in s.player().inventory()) {
						s.message("You don't have enough coal to make any more adamant.") // TODO msg
						return
					}
				}
				2363 -> { // Rune bar
					if (451 !in s.player().inventory()) {
						s.message("You have run out of runite ore with which to make rune.") // TODO msg
						return
					}
					if (Item(453, 8) !in s.player().inventory()) {
						s.message("You don't have enough coal to make any more rune.") // TODO msg
						return
					}
				}
			}
			
			// Show start message..
			when (bar) {
				2349 -> s.message("You smelt the copper and tin together in the furnace.")
				2351 -> s.message("You smelt the iron in the furnace.")
				2353 -> s.message("You place the iron and two heaps of coal into the furnace.")
				2355 -> s.message("You place a lump of silver in the furnace.")
				2357 -> s.message("You place a lump of gold in the furnace.")
				2359 -> s.message("You place the mithril and four heaps of coal into the furnace.")
				2361 -> s.message("You place the adamant and six heaps of coal into the furnace.") // TODO adamant/adamantite?
				2363 -> s.message("You place the runite and eight heaps of coal into the furnace.") // TODO rune/runite?
			}
			
			s.animate(899)
			s.delay(4)
			
			val goldsmithTriggered = BonusContent.isActive(s.player, BlessingGroup.GOLDSMITH)
					&& s.player.world().rollDie(2, 1)
			
			// Show end message..
			when (bar) {
				2349 -> { // Bronze
					if (!goldsmithTriggered) {
						s.player().inventory() -= 436
						s.player().inventory() -= 438
					}
					
					s.player().inventory().addOrDrop(Item(bar), s.player)
					s.message("You retrieve a bar of bronze.")
					s.addXp(Skills.SMITHING, 7.5)
				}
				2351 -> { // Iron 50% chance
					if (!goldsmithTriggered) {
						s.player().inventory() -= 440
					}
					
					// By default, roll a die with true or false
					var success = s.player().world().random().nextBoolean()
					
					// If we have a ring, remove a charge and set 'success' to true.
					if (s.player().equipment().has(RING_OF_FORGING)) {
						val charges = s.player().attribOr<Int>(AttributeKey.RING_OF_FORGING_CHARGES, 140) - 1
						
						if (charges <= 0) {
							s.player().putattrib(AttributeKey.RING_OF_FORGING_CHARGES, 140)
							s.player().equipment().remove(Item(RING_OF_FORGING), true)
							s.message("Your Ring Of Forging has melted.")
						} else {
							s.player().putattrib(AttributeKey.RING_OF_FORGING_CHARGES, charges)
						}
						
						success = true
					}
					
					// Add a bar or make the player cry about losing one precious metal piece :'(
					if (success) {
						s.message("You retrieve a bar of iron.")
						s.player().inventory().addOrDrop(Item(bar), s.player)
						s.addXp(Skills.SMITHING, 12.5)
					} else {
						s.message("The ore is too impure and you fail to refine it.")
					}
				}
				2355 -> { // Silver
					if (!goldsmithTriggered) {
						s.player().inventory() -= 442
					}
					
					s.player().inventory().addOrDrop(Item(bar), s.player)
					s.message("You retrieve a bar of silver from the furnace.")
					s.addXp(Skills.SMITHING, 13.7)
				}
				2353 -> { // Steel
					if (!goldsmithTriggered) {
						s.player().inventory() -= 440
					}
					
					s.player().inventory() -= Item(453, 2)
					s.player().inventory().addOrDrop(Item(bar), s.player)
					s.message("You retrieve a bar of steel.")
					s.addXp(Skills.SMITHING, 17.5)
				}
				2357 -> { // Gold
					if (!goldsmithTriggered) {
						s.player().inventory() -= 444
					}
					
					s.player().inventory().addOrDrop(Item(bar), s.player)
					s.message("You retrieve a bar of gold from the furnace.")
					s.addXp(Skills.SMITHING, if (s.player().equipment().hasAt(EquipSlot.HANDS, 776)) 56.2 else 22.5)
				}
				2359 -> { // Mithril
					if (!goldsmithTriggered) {
						s.player().inventory() -= 447
					}
					
					s.player().inventory() -= Item(453, 4)
					s.player().inventory().addOrDrop(Item(bar), s.player)
					s.message("You retrieve a bar of mithril.") // todo msg
					s.addXp(Skills.SMITHING, 30.0)
				}
				2361 -> { // Adamant
					if (!goldsmithTriggered) {
						s.player().inventory() -= 449
					}
					
					s.player().inventory() -= Item(453, 6)
					s.player().inventory().addOrDrop(Item(bar), s.player)
					s.message("You retrieve a bar of adamant.") // todo msg
					s.addXp(Skills.SMITHING, 37.5)
				}
				2363 -> { // Rune
					if (!goldsmithTriggered) {
						s.player().inventory() -= 451
					}
					
					s.player().inventory() -= Item(453, 8)
					s.player().inventory().addOrDrop(Item(bar), s.player)
					s.message("You retrieve a bar of rune.") // todo msg
					s.addXp(Skills.SMITHING, 50.0)
				}
			}
			
			// Message for the Goldsmith effect
			if (goldsmithTriggered) {
				s.message("You manage to conserve the ore in the process!")
			}
			
			// Wait some
			s.delay(1)
		}
	}
	
	@Suspendable private fun checkRequirements(s: Script, player: Player, bar: Int): Boolean {
		return when (bar) {
			2363 -> { // Runite
				if (player.skills()[Skills.SMITHING] < 85) {
					s.messagebox("You need a smithing level of at least 85 to smelt runite ore.")
					return false
				}
				
				if (451 in player.inventory() && Item(453, 8) in player.inventory())
					return true
				else {
					s.messagebox("You need one runite ore and eight coal to make a runite bar.")
					return false
				}
			}
			2361 -> { // Adamantite
				if (player.skills()[Skills.SMITHING] < 70) {
					s.messagebox("You need a smithing level of at least 70 to smelt adamantite ore.")
					return false
				}
				
				if (449 in player.inventory() && Item(453, 6) in player.inventory())
					return true
				else {
					s.messagebox("You need one adamantite ore and six coal to make an adamantite  bar.")
					return false
				}
			}
			2359 -> { // Mithril
				if (player.skills()[Skills.SMITHING] < 50) {
					s.messagebox("You need a smithing level of at least 50 to smelt mithril ore.")
					return false
				}
				
				if (447 in player.inventory() && Item(453, 4) in player.inventory())
					return true
				else {
					s.messagebox("You need one mithril ore and four coal to make a mithril bar.")
					return false
				}
			}
			2357 -> { // Gold
				if (player.skills()[Skills.SMITHING] < 40) {
					s.messagebox("You need a smithing level of at least 40 to smelt gold ore.")
					return false
				}
				
				if (444 in player.inventory())
					return true
				else {
					s.messagebox("You don't have any gold ore to smelt.")
					return false
				}
			}
			2353 -> { // Steel
				if (player.skills()[Skills.SMITHING] < 30) {
					s.messagebox("You need a smithing level of at least 30 to smelt steel.")
					return false
				}
				
				if (440 in player.inventory() && Item(453, 2) in player.inventory())
					return true
				else {
					s.messagebox("You need one iron ore and two coal to make steel.")
					return false
				}
			}
			2355 -> { // Silver
				if (player.skills()[Skills.SMITHING] < 20) {
					s.messagebox("You need a smithing level of at least 20 to smelt silver ore.")
					return false
				}
				
				if (442 in player.inventory())
					return true
				else {
					s.messagebox("You don't have any silver ore to smelt.")
					return false
				}
			}
			2351 -> { // Iron
				if (player.skills()[Skills.SMITHING] < 15) {
					s.messagebox("You need a smithing level of at least 15 to smelt iron ore.")
					return false
				}
				
				if (440 in player.inventory())
					return true
				else {
					s.messagebox("You don't have any iron ore to smelt.")
					return false
				}
			}
			2349 -> { // Bronze
				if (436 in player.inventory() && 438 in player.inventory())
					return true
				else {
					s.messagebox("You need both tin and copper ore to make bronze.")
					return false
				}
			}
			else -> false
		}
	}
}
