package nl.bartpelle.veteres.content.events

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.InterfaceVisibility
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

object BloodyVolcano {

    private const val BOULDER = 31037
    private const val BLOOD_FRAGMENT = 21532

    var wildernessKillsRequired = 100
    var boulder: MapObj? = null
    var boulderStability = 10000

    var players = ArrayList<Player>(500)

    private val VOLCANO_BOUNDS = Area(3333, 3914, 3390, 3975, -1)
    private val SPAWNS = arrayOf(
            Tile(3366, 3936),
            Tile(3353, 3934),
            Tile(3374, 3937),
            Tile(3361, 3924))

    private var ACTIVE = randomVolcanoSpawn()

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onWorldInit @Suspendable {
            while (true) {
                if (wildernessKillsRequired > 0 || boulder != null) {
                    it.delay(1)
                    continue
                }
                if(boulderStability <= 0) { // Just in case...
                    removeBoulder(it.ctx<World>(), true)
                    continue
                }
                awakenVolcano(it.ctx<World>())
                it.delay(10)
            }
        }

        r.onWorldTimer(TimerKey.ACTIVE_VOLCANO_DESPAWN) @Suspendable {
            removeBoulder(it.ctx<World>(), false)
        }

        r.onObject(BOULDER) @Suspendable {
            mine(it)
        }
    }

    private fun randomVolcanoSpawn(): Tile {
        val generator = Random()
        val randomIndex = generator.nextInt(SPAWNS.size)
        return SPAWNS[randomIndex]
    }

    private fun awakenVolcano(world: World) {
        ACTIVE = randomVolcanoSpawn()
        addBoulder(world)
        world.timers().register(TimerKey.ACTIVE_VOLCANO_DESPAWN, 2000) // 20 minutes to mine it
        world.broadcast("<col=6a1a18><img=15> The Volcano in level 53 wilderness has become active due to the amount of bloodshed!")
    }

    private fun addBoulder(world: World) {
        val spawnedBoulder = world.spawnObj(MapObj(Tile(ACTIVE.x, ACTIVE.z), BOULDER, 10, 0, false))
        boulder = spawnedBoulder
        boulderStability = 10000
    }

    private fun removeBoulder(world: World, success: Boolean) {
        if (boulder != null) {
            world.removeObj(boulder)
            boulder = null
            wildernessKillsRequired = 100
            players.clear()
            if (success) {
                world.broadcast("<col=6a1a18><img=15> The Volcano has been subdued! Well done everyone!")

                val eventParticipants = hashMapOf<Player, Int>()
                world.players().forEachInAreaKt(VOLCANO_BOUNDS, { p ->
                    eventParticipants[p] = p.attribOr<Int>(AttributeKey.BLOODY_FRAGMENTS, 0)
                })

                val result = eventParticipants.entries.sortedBy { it.value }.sortedByDescending { it.value }
                BloodChest.dropKeyFromVolcano(result.first().key, WildernessLevelIndicator.wildernessLevel(result.first().key.tile()), result.first().key.tile())
                world.players().forEachInAreaKt(VOLCANO_BOUNDS, { p ->
                    p.putattrib(AttributeKey.BLOODY_FRAGMENTS, 0)
                })
            } else {
                world.broadcast("<col=6a1a18><img=15> The Volcano has erupted! Help subdue it next time for blood money!")
            }
        }
    }

    private fun removeShards(amt: Int) {
        boulderStability -= amt
        if (boulderStability <= 0)
            boulderStability = 0
    }

    @JvmStatic fun boulderColor(): String {
        if (boulderStability > 6000)
            return "<col=00FF00>"
        else if (boulderStability > 2000)
            return "<col=ffff00>"
        return "<col=FF0000>"
    }

    @Suspendable private fun mine(it: Script) {
        val player = it.player()
        if (player.inventory().full() && !player.inventory().has(BLOOD_FRAGMENT)) {
            it.messagebox("Your inventory is too full to do this.")
            return
        }
        player.faceObj(it.interactionObject())
        it.delay(1)
        player.animate(627)
        player.filterableMessage("You swing your pick at the boulder.")
        it.delay(player.world().random(3..8))
        while (true) {
            if (player.inventory().full() && !player.inventory().has(BLOOD_FRAGMENT)) {
                it.messagebox("You can't hold anymore fragments in your inventory.")
                return
            }
            val random = player.world().random(15..30)
            player.animate(627)
            player.inventory().add(Item(BLOOD_FRAGMENT, (random.toDouble() * player.donationTier().modifiers()[DonatorBoost.BLOODY_VOLCANO.ordinal]).toInt()), true)
            removeShards(random)
            player.putattrib(AttributeKey.BLOODY_FRAGMENTS, player.attribOr<Int>(AttributeKey.BLOODY_FRAGMENTS, 0) + random)
            player.filterableMessage("You manage to mine $random fragments.")
            if (boulderStability <= 0) {
                removeBoulder(player.world(), true)
                player.animate(-1)
                break
            }
            if (boulder == null) {
                player.animate(-1)
                break
            }
            if (player.world().rollDie(10, 1)) {
                fallingLava(it, player.tile())
                break
            }
            it.delay(player.world().random(3..8))
        }
    }

    @Suspendable private fun fallingLava(it: Script, tile: Tile) {
        val world = it.player().world()
        world.executeScript @Suspendable {
            it.delay(1)
            world.players().forEachInAreaKt(VOLCANO_BOUNDS, { p ->
                if(p.tile().equals(tile.x, tile.z))
                    p.filterableMessage(Color.DARK_RED.wrap("You see something break off the boulder.."))
            })
            it.delay(3)
            world.tileGraphic(1406, tile, 0, 0)
            world.players().forEachInAreaKt(VOLCANO_BOUNDS, { p ->
                if(p.tile().equals(tile.x, tile.z)) {
                    p.hit(p, p.world().random(10..30))
                    p.animate(-1)
                    p.filterableMessage(Color.DARK_RED.wrap("A piece of flying lava hits you."))
                } else {
                    p.filterableMessage(Color.DARK_RED.wrap("You dodge the flying lava.. close one."))
                }
            })
        }
    }

    @JvmStatic fun entered(player: Player) {
        val volcanoNotification = player.attribOr<Boolean>(AttributeKey.BLOODY_FRAGMENT_UPDATE, false)
        if (!volcanoNotification) {
            BloodyVolcano.players.add(player)
            player.interfaces().sendWidgetOn(611, Interfaces.InterSwitches.C)
            player.write(InterfaceVisibility(611, 15, true))
            player.putattrib(AttributeKey.BLOODY_FRAGMENT_UPDATE, true)
        }
        var message = ""
        if (!player.world().timers().has(TimerKey.ACTIVE_VOLCANO_DESPAWN))
            player.interfaces().text(611, 8, "Subdued")
        else {
            if (message.isEmpty())
                message = player.world().timers().asMinutesLeft(TimerKey.ACTIVE_VOLCANO_DESPAWN)
            if (message.isEmpty())
                message = player.world().timers().asSeconds(TimerKey.ACTIVE_VOLCANO_DESPAWN)
        }

        player.interfaces().text(611, 8, message)
        player.interfaces().text(611, 9, "Fragments")
        player.interfaces().text(611, 10, "" + player.attribOr(AttributeKey.BLOODY_FRAGMENTS, 0) as Int)
        player.interfaces().text(611, 11, "Boulder Stability")
        player.interfaces().text(611, 12, BloodyVolcano.boulderColor() + (BloodyVolcano.boulderStability * .01).toInt() + "%")
        player.interfaces().text(611, 13, "Total Players")
        player.interfaces().text(611, 14, "" + BloodyVolcano.players.size)
    }

    @JvmStatic fun exited(player: Player) {
        val volcanoNotification = player.attribOr<Boolean>(AttributeKey.BLOODY_FRAGMENT_UPDATE, false)
        if (volcanoNotification) {
            BloodyVolcano.players.remove(player)
            player.putattrib(AttributeKey.BLOODY_FRAGMENTS, 0)
            player.interfaces().closeById(611)
            player.putattrib(AttributeKey.BLOODY_FRAGMENT_UPDATE, false)
        }
    }

}