package nl.bartpelle.veteres.content.npcs.raids

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

object Tekton {
	
	@JvmField
	val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.attackTimerReady(npc)) {
				EntityCombat.putCombatDelay(npc, 2) // We already waste 2 ticks in the rest of the code. Total 4.
				
				val targetTile = target.tile()
				npc.face(null) // Stop facing the target
				it.delay(1)
				npc.animate(npc.world().random(7482..7484))
				
				for (p in npc.world().players().entriesList) {
					if (p != null && p.tile().hash30() == targetTile.hash30()) {
						if (EntityCombat.attemptHit(npc, p, CombatStyle.MELEE)) {
							if (target.varps().varbit(Varbit.PROTECT_FROM_MELEE) == 1) {
								p.hit(npc, p.world().random(20), 1).combatStyle(CombatStyle.GENERIC)
							} else {
								p.hit(npc, p.world().random(50), 1).combatStyle(CombatStyle.GENERIC)
							}
						} else {
							p.hit(npc, 0, 1)
						}
						
						p.putattrib(AttributeKey.LAST_DAMAGER, npc)
						p.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
						npc.putattrib(AttributeKey.LAST_TARGET, p)
					}
				}
				
				npc.face(target) // Go back to facing the target.
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@JvmField
	val hitScript: Function1<Script, Unit> = @Suspendable {
		if (it.npc().world().rollDie(10, 1)) {
			val dmger: Entity? = it.npc().attrib<Player>(AttributeKey.LAST_DAMAGER)
			
			if (dmger != null && dmger is Player && !dmger.finished() && dmger.tile().distance(it.npc().tile()) <= 20) {
				// Display a message if we're not skulled yet.
				if (!Skulling.skulled(dmger)) {
					dmger.message("Tekton has skulled you!")
				}
				
				// Update skull timer.
				Skulling.assignSkullState(dmger)
			}
		}
	}
	
}