package nl.bartpelle.veteres.content.skills.cooking

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Cook
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.quests.free.CooksAssistant
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/20/2015.
 */
object Cooking {
	
	enum class Cookable(val lvl: Int, val exp: Double, val raw: Int, var cooked: Int, val burnt: Int, val itemname: String, val plural: String = "", val offset: Int = 3) {
		//Raw Fish
		RAW_SHRIMPS(1, 30.0, 317, 315, 7954, "a shrimp", "shrimps"),
		RAW_KARAMBWANJI(1, 15.0, 3150, 3151, 592, "a karambwanji", "karambwanji"),
		RAW_SARDINE(1, 40.0, 327, 325, 369, "a sardine", "sardines"),
		RAW_HERRING(5, 50.0, 345, 347, 357, "a herring", "herring"),
		RAW_ANCHOVIES(1, 30.0, 321, 319, 323, "an anchovy", "anchovies"),
		RAW_KARAMBWAN(1, 190.0, 3142, 3144, 3148, "a karambwan", "karambwan"),
		RAW_MACKEREL(10, 60.0, 345, 347, 357, "a mackerel", "mackerels"),
		RAW_TROUT(15, 70.0, 335, 333, 343, "a trout", "trouts"),
		RAW_COD(18, 75.0, 341, 339, 343, "a cod", "cods"),
		RAW_PIKE(20, 80.0, 349, 351, 343, "a pike", "pikes"),
		RAW_SLIMY_EEL(28, 95.0, 3379, 3381, 3383, "a slimy eel", "slimy eels"),
		RAW_SALMON(25, 90.0, 331, 329, 343, "a salmon", "salmons"),
		RAW_TUNA(30, 100.0, 359, 361, 367, "a tuna", "tunas"),
		RAW_CAVE_EEL(28, 115.0, 359, 361, 367, "a cave eel", "cave eels"),
		RAW_LOBSTER(40, 120.0, 377, 379, 381, "a lobster", "lobsters"),
		RAW_BASS(43, 130.0, 363, 365, 367, "a bass", "basses"),
		RAW_SWORDFISH(45, 140.0, 371, 373, 375, "a swordfish", "swordfishes"),
		RAW_LAVA_EEL(53, 30.0, 2148, 2149, 3383, "a lava eel", "lava eels"),
		RAW_MONKFISH(62, 150.0, 7944, 7946, 7948, "a monkfish", "monkfishes"),
		RAW_SHARK(80, 210.0, 383, 385, 387, "a shark", "sharks"),
		RAW_SEA_TURTLE(82, 211.3, 395, 397, 399, "a sea turtle", "sea turtles"),
		RAW_ANGLERFISH(84, 230.0, 13439, 13441, 13443, "an anglerfish", "anglerfish"),
		RAW_DARK_CRAB(85, 215.0, 11934, 11936, 11938, "a dark crab", "dark crabs", 15),
		RAW_MANTA_RAY(91, 216.2, 389, 391, 393, "a manta ray", "manta rays"),
		
		//Raw Meat
		RAW_MEAT(1, 30.0, 2132, 2142, 2146, "a piece of meat", "meat"),
		RAW_RAT_MEAT(1, 30.0, 2134, 2142, 2146, "a piece of rat meat", "rat meat"),
		RAW_YAK_MEAT(1, 30.0, 10816, 2142, 2146, "a piece of yak meat", "yak meat"),
		RAW_BEAR_MEAT(1, 30.0, 2136, 2142, 2146, "a piece of bear meat", "bear meat"),
		RAW_CHICKEN(1, 30.0, 2138, 2140, 2144, "a chicken", "chickens", 10),
		RAW_RABBIT(1, 30.0, 3226, 3228, 7222, "a rabbit", "rabbits"),
		CRAB_MEAT(21, 100.0, 7518, 7521, 7520, "a piece of crab meat", "crab meat"),
		
		//Pies
		REDBERRY_PIE(10, 78.0, 2321, 2325, 2329, "a redberry pie"),
		PIE_MEAT(20, 104.0, 2317, 2327, 2329, "a meat pie"),
		MUD_PIE(29, 128.0, 2319, 7170, 2329, "a mud pie"),
		APPLE_PIE(30, 130.0, 7168, 2323, 2329, "an apple pie"),
		GARDEN_PIE(34, 128.0, 7186, 7188, 2329, "a garden pie"),
		FISH_PIE(47, 164.0, 7186, 7188, 2329, "a fish pie"),
		ADMIRAL_PIE(70, 210.0, 7196, 1798, 2329, "an admiral pie"),
		WILD_PIE(85, 240.0, 7206, 7208, 2329, "a wild pie"),
		SUMMER_PIE(95, 260.0, 7216, 7218, 2329, "a summer pie"),
		
		//Pizza
		PLAIN_PIZZA(35, 143.0, 2287, 2289, 2305, "a plain pizza"),
		
		//Cake
		ISHCAKE(31, 100.0, 7529, 7530, 7531, "a fishcake"),
		CAKE(40, 180.0, 1889, 1891, 1903, "a cake"),
		
		//Random
		SWEET_CORN(28, 104.0, 5986, 5988, 5990, "a piece of sweet corn"),
		SEAWEED(1, 0.0, 401, 1781, 1781, "soda ash")
		;
		
		companion object {
			fun get(raw: Int): Cookable? {
				Cookable.values().forEach {
					if (it.raw == raw)
						return it
				}
				return null
			}
		}
	}
	
	fun cookingChance(player: Player, type: Cookable): Int {
		val points: Int = 60
		val diff = player.skills()[Skills.COOKING] - type.lvl
		return Math.min(100, points + diff)
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//Range
		intArrayOf(26181, 4488, 26185, 8712, 12269, 11851, 26180, 12969, 114, 4489, 7183, 29300).forEach { range ->
			repo.onItemOnObject(range, @Suspendable {
				val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				val food = Cookable.get(item)
				
				if (food != null) {
					//Check to see if the player has the level required to cook the food
					if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) { // Stop people cooking dark crabs and selling back.
						it.messagebox("You can't use this skill on the PvP server.")
					} else if (range == 114 && CooksAssistant.stage(it.player()) != 3) {
						it.chatNpc("Hey, who said you could use that?", Cook.COOK, 614)
					} else if (it.player().skills()[Skills.COOKING] < food.lvl) {
						it.messagebox("You need a cooking level of ${food.lvl} to cook ${food.itemname}.")
					} else {
						startCooking(it, food, obj)
					}
				}
			})
		}
	}
	
	@Suspendable fun startCooking(it: Script, food: Cookable, obj: MapObj) {
		var dry_out_meat = false
		val SINEW = 9436
		//If we're using raw beef or bear meat we..
		if (food.raw == 2132 || food.raw == 2136) {
			if (it.options("Dry the meat into sinew.", "Cook the meat") == 1) {
				dry_out_meat = true
			}
		}
		// Either 1 if we only have one, else ask.
		var amt = 1
		if (it.player().inventory().count(food.raw) > 1) {
			amt = it.itemOptions(Item(food.raw, 175), "How many would you like to cook?", offsetX = food.offset)
		}
		
		while (amt-- > 0 && obj.valid(it.player().world())) {
			if (it.player().inventory().remove(Item(food.raw), true).failed()) {
				it.message("You don't have any more ${food.plural} to cook.")
				return
			}
			if (obj.id() == 26185 || obj.id() == 29300) {
				it.animate(897)
			} else {
				it.animate(896)
			}
			
			// Cooking skillcape stops burning all food.
			if (it.player().equipment().hasAny(9801, 9802, 10658)
					|| Equipment.wearingMaxCape(it.player())
					|| it.player().world().rollDie(100, cookingChance(it.player(), food))
					|| food == Cookable.SEAWEED
					|| CapeOfCompletion.COOKING.operating(it.player())
					|| BonusContent.isActive(it.player, BlessingGroup.CUISINIER)) {
				if (!dry_out_meat) {
					it.player().inventory().add(Item(food.cooked), true)
				} else {
					it.player().inventory().add(Item(SINEW), true)
				}
				
				val msg = when (food) {
					Cookable.RAW_LOBSTER -> "You roast a lobster."
					Cookable.PIE_MEAT -> "You successfully bake a tasty meat pie."
					Cookable.REDBERRY_PIE -> "You successfully bake a delicious redberry pie."
					Cookable.SEAWEED -> "You burn the seaweed to soda ash."
/*                    Cookable.RAW_BEAR_MEAT_TO_SINEW -> "You dry a piece of bear meat and extract the sinew."
                    Cookable.RAW_MEAT_TO_SINEW -> "You dry a piece of beef and extract the sinew."*/
					else -> "You successfully cook ${food.itemname}."
				}
				it.player().message(msg)
				
				it.addXp(Skills.COOKING, food.exp)
			} else {
				it.player().inventory().add(Item(food.burnt), true)
				it.player().message("You accidentally burn ${food.itemname.replaceFirst("an ", "the ").replaceFirst("a ", "the ")}.")
			}
			
			it.delay(3)
		}
	}
	
}