package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 09/07/2016.
 */
class KickCommandWarning(val other: Player) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(it: Script) {
		
		// Last time we were attacked
		val lastAttacked = System.currentTimeMillis() - other.attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0.toLong())
		val lastAttack = System.currentTimeMillis() - other.attribOr<Long>(AttributeKey.LAST_ATTACK_TIME, 0.toLong())
		val incb: Boolean = lastAttack < 10000 || lastAttacked < 10000 || other.timers().has(TimerKey.COMBAT_LOGOUT)
		val lvl: Int = WildernessLevelIndicator.wildernessLevel(other.tile())
		val inw = lvl > 0
		val mod = it.player()
		val world = mod.world()
		
		if (inw && incb) {
			it.messagebox(other.name() + " is <col=FF0000>in combat</col> and at <col=FF0000>level " + lvl + " wilderness</col>. Confirm kick?")
			if (it.optionsTitled("Kick <col=FF0000>" + other.name() + "</col>?", "Kick " + other.name() + ".", "nvm") == 1) {
				other.logout()
				it.player().message("Kicked: <col=7F00FF>" + other.name() + ".")
			} else {
				return
			}
		} else if (incb && !inw) {
			it.messagebox(other.name() + " is <col=FF0000>in combat</col>. Confirm kick?")
			if (it.optionsTitled("Kick <col=FF0000>" + other.name() + "</col>?", "Kick " + other.name() + ".", "nvm") == 1) {
				other.logout()
				it.player().message("Kicked: <col=7F00FF>" + other.name() + ".")
			} else {
				return
			}
		} else if (!incb && inw) {
			it.messagebox(other.name() + " is at <col=FF0000>level " + lvl + " wilderness</col>. Confirm kick?")
			if (it.optionsTitled("Kick <col=FF0000>" + other.name() + "</col>?", "Kick " + other.name() + ".", "nvm") == 1) {
				other.logout()
				it.player().message("Kicked: <col=7F00FF>" + other.name() + ".")
			} else {
				return
			}
		}
		val ot = other.tile().transform(0, 0)
		val pt = mod.tile().transform(0, 0)
		
		world.tileGraphic(110, pt, 110, 0)
		world.tileGraphic(110, ot, 110, 0)
		world.spawnProjectile(pt, ot, 143, 25, 10, 0, 60, 10, 10)
		it.clearContext()
		it.delay(2)
		
		world.tileGraphic(110, ot, 110, 0)
		world.spawnProjectile(ot, pt, 143, 25, 10, 0, 60, 10, 10)
		it.delay(2)
		world.tileGraphic(110, pt, 110, 0)
		world.spawnProjectile(pt, ot, 143, 25, 10, 0, 60, 10, 10)
		it.delay(2)
		
		world.tileGraphic(110, ot, 110, 0)
		world.spawnProjectile(ot, pt, 143, 25, 10, 0, 60, 10, 10)
		it.delay(2)
		world.tileGraphic(110, pt, 110, 0)
		world.spawnProjectile(pt, ot, 143, 25, 10, 0, 60, 10, 10)
		it.delay(2)
	}
}