package nl.bartpelle.veteres.content.areas.karamja

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.ChangeMapVisibility
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Situations on 4/10/2016.
 */

object CustomsOfficer {
	
	val CUSTOMS_OFFICER = 3648
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(CUSTOMS_OFFICER) @Suspendable {
			it.chatNpc("Can I help you?", CUSTOMS_OFFICER, 588)
			when (it.options("Can I journey on this ship?", "Does Karmaja have unusual customs then?")) {
				1 -> {
					it.chatPlayer("Can I journey on this ship?", 554)
					it.chatNpc("Hey, I know you, you work at the plantation.", CUSTOMS_OFFICER, 588)
					it.chatNpc("I don't think you'll try smuggling anything, you just<br>need to pay a boarding charge of 30 coins.", CUSTOMS_OFFICER, 589)
					when (it.options("Ok.", "Oh, I'll not bother then.")) {
						1 -> {
							it.chatPlayer("Ok.", 588)
							travel(it, false)
						}
					}
				}
			}
		}
		r.onNpcOption2(CUSTOMS_OFFICER) @Suspendable { travel(it, true) }
	}
	
	@Suspendable fun travel(it: Script, quick_travel: Boolean) {
		if (it.player().inventory().remove(Item(995, 30), false).success()) {
			it.player().lock()
			it.player().interfaces().sendMain(299)
			it.player().write(ChangeMapVisibility(2))
			it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 6)
			it.message("You pay 30 coins and board the ship.")
			it.delay(8)
			it.player().unlock()
			it.player().interfaces().closeById(299)
			it.player().write(ChangeMapVisibility(0))
			it.player().teleport(Tile(3032, 3217, 1))
			it.messagebox("The ship arrives at Port Sarim.")
		} else {
			if (quick_travel)
				it.message("You do not have enough coins to pay passage, you need 30.")
			else
				it.chatPlayer("Oh dear, I don't seem to have enough money.", 610)
		}
	}
}