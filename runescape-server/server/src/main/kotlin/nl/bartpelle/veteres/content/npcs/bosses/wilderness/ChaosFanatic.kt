package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 1/25/2016.
 */

object ChaosFanatic {
	
	val SHOUT = arrayOf("Burn!", "WEUGH!", "Develish Oxen Roll!", "All your wilderness are belong to them!",
			"AhehHeheuhHhahueHuUEehEahAH", "I shall call him squidgy and he shall be my squidgy!")
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 5)) {
				if (EntityCombat.attackTimerReady(npc)) {
					//Shout!
					npc.sync().shout(SHOUT[target.world().random(5)])
					
					//Send the explosives!
					if (target.world().rollDie(20, 1)) //5% chance the npc sends explosives
						explosives(it, npc, target)
					
					if (target.world().rollDie(30, 1)) //3.3% chance of getting disarmed
						disarm(target)
					
					// Attack the player
					attack(npc, target)
					
					// ..and take a quick nap
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, 2)
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc, target, 554, 40, 25, 35, 12 * tileDist, 15, 10)
		val delay = Math.max(1, (20 + tileDist * 12) / 30)
		npc.animate(811)
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC)
	}
	
	fun explosives(it: Script, npc: Entity, target: Entity) {
		val x = target.tile().x //The target's x tile
		val z = target.tile().z //The target's z tile
		
		//Handle the first explosive
		val explosive_one = Tile(x + target.world().random(2), z)
		val explosive_one_distance = npc.tile().distance(explosive_one)
		val explosive_one_delay = Math.max(1, (20 + explosive_one_distance * 12) / 30)
		
		//Handle the second explosive
		val explosive_two = Tile(x, z + target.world().random(2))
		val explosive_two_distance = npc.tile().distance(explosive_two)
		val explosive_two_delay = Math.max(1, (20 + explosive_two_distance * 12) / 30)
		
		//Handle the third explosive
		val explosive_three = Tile(x, z + target.world().random(2))
		val explosive_three_distance = npc.tile().distance(explosive_three)
		val explosive_three_delay = Math.max(1, (20 + explosive_three_distance * 12) / 30)
		
		//Send the projectiles
		npc.world().spawnProjectile(npc.tile(), explosive_one, 551, 50, 0, explosive_one_delay, 24 * explosive_one_distance, 35, 10)
		npc.world().spawnProjectile(npc.tile(), explosive_two, 551, 50, 0, explosive_two_delay, 24 * explosive_two_distance, 35, 10)
		npc.world().spawnProjectile(npc.tile(), explosive_three, 551, 50, 0, explosive_three_delay, 24 * explosive_three_distance, 35, 10)
		
		//Send the tile graphic
		target.world().tileGraphic(157, explosive_one, 1, 24 * explosive_one_distance)
		target.world().tileGraphic(157, explosive_two, 1, 24 * explosive_two_distance)
		target.world().tileGraphic(552, explosive_three, 1, 24 * explosive_three_distance)
		
		it.runGlobal(target.world()) @Suspendable {
			//Create a delay before checking if the player is on the explosive tile
			it.delay(6)
			//For each player in the world we..
			val target_x = target.tile().x
			val target_z = target.tile().z
			//Check to see if the player's tile is the same as the first explosive..
			if (target_x == explosive_one.x && target_z == explosive_one.z)
				target.hit(npc, target.world().random(15)).combatStyle(CombatStyle.MAGIC)
			//Check to see if the player's tile is the same as the second explosive..
			if (target_x == explosive_two.x && target_z == explosive_two.z)
				target.hit(npc, target.world().random(15)).combatStyle(CombatStyle.MAGIC)
		}
	}
	
	fun disarm(target: Entity) {
		if (target.inventory().add(target.equipment().get(EquipSlot.WEAPON), false).success()) {
			target.equipment().set(EquipSlot.WEAPON, null)
			target.message("The fanatic disarms you!")
		}
	}
}