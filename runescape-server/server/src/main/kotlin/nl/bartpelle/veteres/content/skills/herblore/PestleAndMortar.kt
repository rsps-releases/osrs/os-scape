package nl.bartpelle.veteres.content.skills.herblore

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/16/2016.
 */

object PestleAndMortar {
	
	val PESTLE_AND_MORTAR = 233
	
	enum class CrushableContents(val before: Int, val after: Int, val message: String) {
		UNICORN_HORN(237, 235, "You grind the unicorn horn to dust."),
		CHOCOLATE_BAR(1973, 1975, "You grind the chocolate to dust."),
		KEBBIT_TEETH(10109, 10111, "You grind the kebbit teeth to dust."),
		BIRD_NEST(5075, 6693, "You grind the bird's nest down."),
		DESERT_GOAT_HORN(9735, 9736, "You grind the goat's horn to dust."),
		CHARCOAL(973, 704, "You grind the charcoal to a powder."),
		ASHES(592, 8865, "You grind down the ashes."),
		BLUE_DRAGON_SCALE(243, 241, "You grind the dragon scale to dust."),
		LAVA_SCALE(11992, 11994, "")
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		CrushableContents.values().forEach { c ->
			if (c.before != -1) {
				r.onItemOnItem(c.before.toLong(), PESTLE_AND_MORTAR.toLong(), s@ @Suspendable {
					
					//Is the player on the PVP world?
					if (it.player().world().realm().isPVP) {
						it.messagebox("That's a bit pointless, isn't it?")
						return@s
					}
					
					var num = it.player().inventory().count(c.before)
					
					while (num-- > 0) {
						
						//If the items required aren't in the players inventory, break the while..
						if (c.before !in it.player().inventory() || PESTLE_AND_MORTAR !in it.player().inventory()) {
							break
						}
						
						it.player().inventory() -= c.before
						
						if (c.before != 11992) {
							it.message(c.message)
							it.player().inventory() += c.after
						} else {
							val amt = it.player().world().random(3..6)
							
							it.message("You grind the lava dragon scale into $amt shards.")
							it.player().inventory() += Item(c.after, amt)
						}
						it.player().animate(364)
						it.delay(2)
					}
					
				})
			}
		}
	}
}
