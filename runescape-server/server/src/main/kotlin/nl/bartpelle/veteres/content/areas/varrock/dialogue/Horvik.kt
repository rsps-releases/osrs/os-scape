package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.areas.edgeville.osrune.Ilfeen
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue.Herman
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.text.NumberFormat
import java.util.*

/**
 * Created by Situations on 11/30/2015.
 */

object Horvik {

	enum class ItemUpgrading(val regularId: Int, val upgradeID: Int, val bmUpgradeCost: Int, val itemName: String, val upgradedItemName: String) {
		VOID_TOP(8839, 13072, 3000, "Void Top", "Elite Void Top"),
		VOID_BOTTOM(8840, 13073, 3000, "Void Bottom", "Elite Void Bottom"),
		MAGIC_SHORT(861, 12788, 200, "Magic Shortbow", "Magic Shortbow(i)"),
		RING_OF_WEALTH(2572, 12785, 500, "Ring of Wealth", "Ring of Wealth(i)"),
		SEERS_RING(6731, 11770, 2500, "Seers Ring", "Seers Ring(i)"),
		ARCHERS_RING(6733, 11771, 2500, "Archer Ring", "Archer Ring(i)"),
		WARRIOR_RING(6735, 11772, 1000, "Warrior Ring", "Warrior Ring(i)"),
		BERSERKER_RING(6737, 11773, 1000, "Berserker Ring", "Berserker Ring(i)"),
		SLAYER_HELMET(11864, 11865, 2000, "Slayer Helmet", "Slayer Helmet(i)"),
		TYRANNICAL_RING(12603, 12691, 1500, "Tyrannical Ring", "Tyrannical Ring(i)"),
		TREASONOUS_RING(12605, 12692, 1500, "Treasonous Ring", "Treasonous Ring(i)"),
		RING_OF_THE_GODS(12601, 13202, 1500, "Ring of the Gods", "Ring of the Gods(i)"),
		BLACK_SLAYER_HELMET(19639, 19641, 2000, "Black Slayer Helmet", "Black Slayer Helmet(i)"),
		GREEN_SLAYER_HELMET(19643, 19645, 2000, "Green Slayer Helmet", "Green Slayer Helmet(i)"),
		RED_SLAYER_HELMET(19647, 19649, 2000, "Red Slayer Helmet", "Red Slayer Helmet(i)"),
		RING_OF_SUFFERING(19550, 19710, 2000, "Ring of Suffering", "Ring of Suffering(i)"),
		GUTHIX_CAPE(2413, 21793, 5000, "Guthix Cape", "Imbued Guthix Cape"),
		ZAMORAK_CAPE(2414, 21795, 5000, "Zamorak Cape", "Imbued Zamorak Cape"),
		SARADOMIN_CAPE(2412, 21791, 5000, "Saradomin Cape", "Imbued Saradomin Cape")
	}
	
	@JvmStatic val HORVIK = 535
	
	@JvmStatic val ZAMORAKIAN_SPEAR = 11824
	@JvmStatic val ZAMORAKIAN_HASTA = 11889
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(HORVIK) @Suspendable {
			if(it.player().world().realm().isPVP) {
				if (!Herman.hasBroken(it.player())) {
					it.chatNpc("You don't have any broken barrows pieces.", HORVIK)
				} else {
					Herman.fixall(it, HORVIK)
				}
			} else {
				it.chatNpc("Hello, do you need any help?", HORVIK, 589)
				val player = it.player()
				val hasta: Boolean = player.inventory().has(ZAMORAKIAN_HASTA)
				val spear: Boolean = player.inventory().has(ZAMORAKIAN_SPEAR)

				if (hasta && spear) {
					when (it.options("No thanks. I'm just looking around.", "Do you want to trade?", "Can you turn my spear into a hasta?",
							"Can you revert my Hasta into a Spear?")) {
						1 -> {
							op1(it)
						}
						2 -> {
							op2(it)
						}
						3 -> {
							opSpear(it)
						}
						4 -> opHasta(it)
					}
				} else if (!hasta && spear) {
					when (it.options("No thanks. I'm just looking around.", "Do you want to trade?", "Can you turn my spear into a hasta?")) {
						1 -> {
							op1(it)
						}
						2 -> {
							op2(it)
						}
						3 -> {
							opSpear(it)
						}
					}
				} else if (hasta && !spear) {
					when (it.options("No thanks. I'm just looking around.", "Do you want to trade?", "Can you revert my Hasta into a Spear?")) {
						1 -> {
							op1(it)
						}
						2 -> {
							op2(it)
						}
						3 -> {
							opHasta(it)
						}
					}
				} else if (!hasta && !spear) {
					when (it.options("No thanks. I'm just looking around.", "Do you want to trade?")) {
						1 -> {
							op1(it)
						}
						2 -> {
							op2(it)
						}
					}
				}
			}
		}
		r.onItemOnNpc(HORVIK, s@ @Suspendable {
            val itemUsed = it.itemUsed()
			for(upgrade in ItemUpgrading.values()) {
                if(upgrade.regularId == itemUsed.id()) {
                    it.chatNpc("Are you sure you'd like me to upgrade your ${upgrade.itemName} for ${upgrade.bmUpgradeCost}?", HORVIK)
                    when(it.options("Yes, upgrade my item.", "No.")) {
                        1 -> {
                            if(!it.player().inventory().has(13307) || it.player().inventory().count(13307) < upgrade.bmUpgradeCost) {
                                it.chatNpc("You don't have enough blood money for me to upgrade that.", HORVIK)
                                return@s
                            }

                            it.player().inventory().remove(Item(13307, upgrade.bmUpgradeCost), true)
                            it.player().inventory().remove(Item(upgrade.regularId, 1), true)
                            it.player().inventory().add(Item(upgrade.upgradeID))
                            it.chatNpc("I've upgraded your ${upgrade.itemName}. Please let me know if you want to upgrade any more items!", HORVIK)
                        }
                    }
                    return@s
                }
			}
		})

		r.onNpcOption2(HORVIK) {
			if(it.player().world().realm().isPVP) {
				val upgradeInfo = ArrayList<String>()
				upgradeInfo.add("To upgrade/imbue an item, simply use the required item on")
				upgradeInfo.add("Horvik. These are the items he's able to assist you with:")
				upgradeInfo.add("")
				for(upgrade in ItemUpgrading.values())
					upgradeInfo.add(upgrade.itemName + " -> " + upgrade.upgradedItemName + "  <img=40> <col=800000> "
							+ NumberFormat.getInstance().format(upgrade.bmUpgradeCost))
				val upgradeInfoArray = upgradeInfo.toTypedArray()
				sendScroll(it, "<col=800000>Item Upgrades", *upgradeInfoArray)
			} else {
				op2(it)
			}
		}

		r.onNpcOption3(HORVIK) @Suspendable {
			if (!it.player().world().realm().isPVP) {
				it.chatPlayer("Hey ya.", 588)

				if (it.player().inventory().contains(Item(Ilfeen.CRYSTAL_SEED)))
					Ilfeen.has_seed(it, HORVIK) else Ilfeen.no_seed(it, HORVIK)

			} else if (it.player().world().realm().isPVP) {
				if (!it.player().inventory().contains(Item(Ilfeen.CRYSTAL_SEED))) {
					Ilfeen.no_seed(it, HORVIK)
				} else {
					val opt = it.optionsTitled("Which would you like Horvik to enchant?", "Crystal bow", "Crystal shield", "Crystal halberd")
					if (opt != 4) {
						val resultStr = when (opt) {
							1 -> "bow"
							2 -> "shield"
							3 -> "halberd"
							else -> "unknown"
						}
						val resItem = when (opt) {
							1 -> 4212
							2 -> 4224
							else -> 13091
						}
						val cost = when (opt) {
							1 -> 10000
							2 -> 3500
							else -> 5500
						}
						if (it.player().inventory().count(13307) < cost) {
							it.chatNpc("You don't have $cost blood money to enchant this crystal, " + it.player().name() + ".", HORVIK)
						} else if (it.optionsTitled("Pay Horvik $cost blood money for a Crystal $resultStr?", "Yes", "No") == 1) {
							val ok = it.player().inventory().remove(Item(Ilfeen.CRYSTAL_SEED), false).success() && it.player().inventory().remove(Item(13307, cost), true).success()
							if (ok) {
								it.targetNpc()!!.animate(713)
								it.player().inventory().add(Item(resItem), false)
								when (resItem) {
									4212 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_BOW_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_BOW_ENCHANTS, 0))
									4224 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_SHIELD_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_SHIELD_ENCHANTS, 0))
									13091 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_HALBERD_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_HALBERD_ENCHANTS, 0))
								}
								it.doubleItemBox("Horvik uses an elven enchantment to weave the seed into a $resultStr.", Ilfeen.CRYSTAL_SEED, resItem)
								val extra = if (it.player().world().rollDie(20, 1)) "You're one sexy minx." else ""
								it.chatPlayer("Thanks a lot, Horvik. $extra")
							}
						}
					}
				}
			}
		}

		//Imbued ring uncharging
		for(i in 11770..11773) {
			r.onItemOption4(i, @Suspendable {
				if (it.customOptions("Are you sure you wish to do this?", "Yes, remove the imbue from my ring.", "No, I'll keep my imbued ring.") == 1) {
					if (it.player().inventory().count(i) > 0 && it.player().inventory().freeSlots() > 0) {
						it.player().inventory() -= i
						when (i) {
                            11770 -> it.player().inventory() += 6731
                            11771 -> it.player().inventory() += 6733
                            11772 -> it.player().inventory() += 6735
                            11773 -> it.player().inventory() += 6737
                        }
						it.animate(713)
					} else {
						it.player().message("You don't have enough room to do that.")
					}
				}
			})
		}
	}
	
	@Suspendable fun op1(it: Script) {
		it.chatPlayer("No thanks. I'm just looking around.")
		it.chatNpc("Well, come and see me if you're ever in need of armour!", HORVIK, 589)
	}
	
	@Suspendable fun op2(it: Script) {
		if (!it.player().world().realm().isPVP) {
			if (it.player().ironMode() != IronMode.NONE) {
				it.player().world().shop(15).display(it.player())
			} else {
				it.player().world().shop(20).display(it.player())
			}
		}
	}
	
	@Suspendable fun opSpear(it: Script) {
		it.chatPlayer("Can you turn my spear into a hasta?", 567)
		if (it.player().inventory().contains(ZAMORAKIAN_SPEAR)) {
			if (it.player().world().realm().isPVP) {
				if (it.player().inventory().contains(Item(ZAMORAKIAN_SPEAR))) {
					it.player().inventory().remove(Item(ZAMORAKIAN_SPEAR), true)
					it.player().inventory().add(Item(ZAMORAKIAN_HASTA), true)
				}
				it.doubleItemBox("Horvik does the conversion for free!", ZAMORAKIAN_SPEAR, ZAMORAKIAN_HASTA)
			} else {
				it.chatNpc("Sure! For the cheap price of 2,000,000 gold coins!", HORVIK, 567)
				when (it.options("Yes", "No")) {
					1 -> {
						it.chatPlayer("Okay, you've got a deal.")
						if (it.player().inventory().contains(Item(995, 2000000))) {
							if (it.player().inventory().contains(Item(ZAMORAKIAN_SPEAR))) {
								it.player().inventory().remove(Item(ZAMORAKIAN_SPEAR), true)
								it.player().inventory().remove(Item(995, 2000000), true)
								it.player().inventory().add(Item(ZAMORAKIAN_HASTA), true)
							} else {
								it.chatPlayer("I don't have a spear to give you right now, sorry!")
							}
						} else {
							it.chatPlayer("I don't have the coins right now, sorry!")
						}
					}
					2 -> {
						it.chatPlayer("Sorry, I'm way too cheap for that.", 588)
						it.chatNpc("I suggest purchasing some gold at store.os-scape.com - heh.", HORVIK, 567)
					}
				}
			}
		} else {
			it.chatNpc("You don't appear to have a Zamorakian spear.", HORVIK, 610)
			it.chatPlayer("Oh yeah.. sorry.")
		}
	}
	
	@Suspendable fun opHasta(it: Script) {
		it.chatPlayer("Can you revert my hasta into a spear?", 567)
		if (it.player().inventory().contains(ZAMORAKIAN_HASTA)) {
			if (it.player().world().realm().isPVP) {
				if (it.player().inventory().contains(Item(ZAMORAKIAN_HASTA))) {
					it.player().inventory().remove(Item(ZAMORAKIAN_HASTA), true)
					it.player().inventory().add(Item(ZAMORAKIAN_SPEAR), true)
				}
				it.doubleItemBox("Horvik does the conversion for free!", ZAMORAKIAN_HASTA, ZAMORAKIAN_SPEAR)
			} else {
				it.chatNpc("Sure! For the cheap price of 2,000,000 gold coins!", HORVIK, 567)
				when (it.options("Yes", "No")) {
					1 -> {
						it.chatPlayer("Okay, you've got a deal.")
						if (it.player().inventory().contains(Item(995, 2000000))) {
							if (it.player().inventory().contains(Item(ZAMORAKIAN_HASTA))) {
								it.player().inventory().remove(Item(ZAMORAKIAN_HASTA), true)
								it.player().inventory().remove(Item(995, 2000000), true)
								it.player().inventory().add(Item(ZAMORAKIAN_SPEAR), true)
							} else {
								it.chatPlayer("I don't have a hasta to give you right now, sorry!")
							}
						} else {
							it.chatPlayer("I don't have the coins right now, sorry!")
						}
					}
					2 -> {
						it.chatPlayer("Sorry, I'm way too cheap for that.", 588)
						it.chatNpc("I suggest purchasing some gold at store.os-scape.com!", HORVIK, 567)
					}
				}
			}
		} else {
			it.chatNpc("You don't appear to have a Zamorakian hasta.", HORVIK, 610)
			it.chatPlayer("Oh yeah.. sorry.")
		}
	}

	fun sendScroll(it: Script, title: String, vararg lines: String) {
		if (it.player().interfaces().visible(275))
			it.player().interfaces().closeMain()
		it.player().interfaces().text(275, 2, title)
		var childId = 4
		it.player().interfaces().text(275, childId++, "")
		for (s in lines)
			it.player().interfaces().text(275, childId++, s)
		it.player().interfaces().sendMain(275)
	}
	
}
