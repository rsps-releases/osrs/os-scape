package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 11/22/2015.
 */

object Alchemy {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//High alchemy
		repo.onSpellOnItem(218, 35, s@ @Suspendable {
			if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				it.messagebox2("That's pretty pointless isn't it?")
				return@s
			}
			var warning = it.player().attribOr<Int>(AttributeKey.ALCHEMY_WARNING, 0)
			val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]]
			val def = it.player().world().definitions().get(ItemDefinition::class.java, item.unnote(it.player().world()).id())
			
			while (it.player().timers().has(TimerKey.ALCH_TIMER))
				it.delay(1)
			
			if (!it.player().timers().has(TimerKey.ALCH_TIMER)) {
				it.player().invokeScript(InvokeScript.OPEN_TAB, 6);
				if (it.player().skills()[Skills.MAGIC] >= 55) {
					if (item.id() != 995) {
						if (def.cost < 30000) {
							highAlch(it, item.id())
						} else {
							if (warning == 1) {
								highAlch(it, item.id())
							} else {
								it.itemBox("The item you are trying to alch is considered <col=6f0000>valuable</col>.<br>Are you absolutely sure you want to alch it?", item.unnote(it.player().world()).id())
								if (it.options("Alch it.", "No, don't alch it.") == 1) {
									highAlch(it, item.id())
								}
							}
						}
					} else {
						it.message("Coins are already made of gold.")
					}
				} else {
					it.message("Your Magic level is not high enough for this spell.")
				}
			}
		})
		//Low alchemy
		repo.onSpellOnItem(218, 14, s@ @Suspendable {
			if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				it.messagebox2("That's pretty pointless isn't it?")
				return@s
			}
			var warning = it.player().attribOr<Int>(AttributeKey.ALCHEMY_WARNING, 0)
			val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]]
			val def = it.player().world().definitions().get(ItemDefinition::class.java, item.unnote(it.player().world()).id())
			//Check to see if the player is currently alching
			
			while (it.player().timers().has(TimerKey.ALCH_TIMER))
				it.delay(1)
			
			if (!it.player().timers().has(TimerKey.ALCH_TIMER)) {
				it.player().invokeScript(InvokeScript.OPEN_TAB, 6);
				if (it.player().skills()[Skills.MAGIC] > 21) {
					if (item.id() != 995) {
						if (def.cost < 30000) {
							lowAlch(it, item.id())
						} else {
							if (warning == 1) {
								lowAlch(it, item.id())
							} else {
								it.itemBox("The item you are trying to alch is considered <col=6f0000>valuable</col>.<br>Are you absolutely sure you want to alch it?", item.unnote(it.player().world()).id())
								if (it.options("Alch it.", "No, don't alch it.") == 1) {
									lowAlch(it, item.id())
								}
							}
						}
					} else {
						it.message("Coins are already made of gold.")
					}
				} else {
					it.message("Your Magic level is not high enough for this spell.")
				}
			}
		})
		//High Alchemy toggle warning
		repo.onButton(218, 14) @Suspendable {
			it.itemBox("This toggle will prevent or allow a warning from<br>appearing when attempting to alch and item worth over<br>30,000 coins. Do you want to toggle this On/Off?", 561)
			if (it.options("On", "Off") == 1) {
				putWarning(it.player(), 0)
			} else {
				putWarning(it.player(), 1)
			}
		}
		//Low Alchemy toggle warning
		repo.onButton(218, 35) @Suspendable {
			it.itemBox("This toggle will prevent or allow a warning from<br>appearing when attempting to alch and item worth over<br>30,000 coins. Do you want to toggle this On/Off?", 561)
			if (it.options("On", "Off") == 1) {
				putWarning(it.player(), 0)
			} else {
				putWarning(it.player(), 1)
			}
		}
	}
	
	@Suspendable fun highAlch(it: Script, itemid: Int) {
		it.player().stopActions(false)
		val slot: Int = it.player()[AttributeKey.BUTTON_SLOT] ?: return
		if (slot < 0) return
		val item = it.player().inventory()[slot] ?: return
		val note = item.unnote(it.player().world()) ?: return
		val def = note.definition(it.player().world()) ?: return
		val runes = arrayOf(Item(554, 5), Item(561, 1))
		
		if (!MagicCombat.has(it.player(), runes, true)) {
			return
		}
		it.player().animate(713)
		it.player().graphic(113, 100, 0)
		it.player().inventory() -= Item(item.id(), 1)
		it.player().inventory() += Item(995, (def.cost * 0.60).toInt())
		it.player().timers().set(TimerKey.ALCH_TIMER, 5)
		it.addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 130.0 else 65.0)
		it.player().sound(97)
	}
	
	@Suspendable fun lowAlch(it: Script, itemid: Int) {
		it.player().stopActions(false)
		val slot: Int = it.player()[AttributeKey.BUTTON_SLOT] ?: return
		if (slot < 0) return
		val item = it.player().inventory()[slot] ?: return
		val def = item.unnote(it.player().world()).definition(it.player().world())
		val runes = arrayOf(Item(554, 3), Item(561, 1))
		
		if (!MagicCombat.has(it.player(), runes, true)) {
			return
		}
		it.player().animate(712)
		it.player().graphic(112, 100, 0)
		it.player().inventory() -= Item(item.id(), 1)
		it.player().inventory() += Item(995, (def.cost * 0.4).toInt())
		it.player().timers().set(TimerKey.ALCH_TIMER, 5)
		it.addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 62.0 else 31.0)
		it.player().sound(98)
	}
	
	fun putWarning(player: Player, stageBit: Int): Int {
		player.putattrib(AttributeKey.ALCHEMY_WARNING, stageBit)
		return stageBit
	}
}