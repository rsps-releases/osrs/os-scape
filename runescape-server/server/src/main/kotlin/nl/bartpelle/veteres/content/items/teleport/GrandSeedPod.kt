package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 22/05/2017.
 */
object GrandSeedPod {
	
	val GRAND_SEED_POD = 9469
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		// Launch
		repo.onItemOption1(GRAND_SEED_POD, s@ @Suspendable {
			it.player().stopActions(true)
			if (!Teleports.canTeleport(it.player(), true, TeleportType.ABOVE_20_WILD)) {
				return@s
			}
			DeadmanMechanics.attemptTeleport(it)
			it.message("The seed pod is released high into the air and the pod bursts into a pyrotechnic like display.<br>A pilot will be with you shortly.")
			it.player().graphic(768, 0, 15)
			it.player().animate(4547, 15)
			it.player().lockNoDamage()
			// val npc = Npc(6089, it.player().world(), Tile(it.player().tile().transform(-5, 0,0)))
			// npc.respawns(false)
			//npc.sync().shout("Glider for ${it.player().name()}!!!") // belongs to the gnome npc
			// npc.walkTo( Tile(it.player().tile().transform(-10, 0,0)), PathQueue.StepType.FORCED_WALK)
			it.delay(7)
			// npc.sync().shout("Hello, ${it.player().name()}! Let's go!") // belongs to the gnome npc
			it.message("Your glider has arrived.")
			it.delay(2)
			it.player().unlock()
			it.player().inventory().remove(Item(GRAND_SEED_POD), false)
			it.player().teleport(Tile(3092, 3500))
			//it.player().world().npcs().remove(npc)
			it.player().skills().__addXp(Skills.FARMING, 100.0)
		})
		
		// Squash
		repo.onItemOption3(GRAND_SEED_POD, s@ @Suspendable {
			it.player().stopActions(true)
			if (!Teleports.canTeleport(it.player(), true, TeleportType.ABOVE_20_WILD)) {
				return@s
			}
			DeadmanMechanics.attemptTeleport(it)
			it.message("You squash the seed pod.")
			it.player().graphic(767)
			it.player().animate(4544)
			it.player().lockNoDamage()
			it.delay(3)
			it.player().looks().transmog(716)
			it.delay(4)
			it.player().teleport(Tile(3092, 3500))
			it.delay(1)
			it.player().graphic(769)
			it.player().inventory().remove(Item(GRAND_SEED_POD), false)
			it.delay(2)
			it.player().looks().transmog(-1)
			it.delay(2)
			it.player().skills().alterSkill(Skills.FARMING, -5)
			it.player().unlock()
			it.messagebox("Plants seem hostile to you for killing seeds from the Grand Tree. <br>Your current farming level has been reduced by 5.")
		})
	}
}