package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Player
import java.util.concurrent.TimeUnit

/**
 * Created by Mack on 10/19/2017.
 */
object Skulling {
	
	private const val SKULL_TIMER_MINS = 20
	
	/**
	 * If the player meets the criteria to be skulled, we skull them.
	 * The skull register timer is 20 mins.
	 * <b>Note</b> that the combating parameter is indicating if the skulling action is deriving from a combat
	 * context or if we're just giving the player a skull status with no player interaction.
	 */
	@JvmStatic
	fun skull(attacker: Player, target: Entity?) {
		if (target == null || attacker.dead() || !target.isPlayer) {
			return
		}
		
		val target = target as Player
		
		PlayerCombat.trackPvpAggression(attacker, target)
		
		val theyHaveAttacked = getHistoryFor(target).containsKey(attacker.id().toString()) //Means target has recently attacked attacker
		val weHaveAttacked = getHistoryFor(attacker).containsKey(target.id().toString()) //Means attacker has recently attacked target
		
		if (theyHaveAttacked) {
			checkForRemoval(target, attacker)
		} else if (weHaveAttacked) {
			checkForRemoval(attacker, target)
		} else {
			updateAttackHistory(attacker, target)
			
			//if (!skulled(attacker)) {
				assignSkullState(attacker)
			//}
		}
	}
	
	/**
	 * Assigns the skull state to the specified player.
	 */
	@JvmStatic
	fun assignSkullState(player: Player) {
		player.putattrib(AttributeKey.SKULL_CYCLES, SKULL_TIMER_MINS)
		player.looks().update()
	}
	
	/**
	 * Performs the unskulling action on the specified player.
	 */
	@JvmStatic
	fun unskull(player: Player) {
		player.clearattrib(AttributeKey.SKULL_CYCLES)
		player.clearattrib(AttributeKey.SKULL_ENTRIES_TRACKER)
		player.looks().update()
	}
	
	/**
	 * Decrements the amount of skull cycles remaining.
	 */
	@JvmStatic
	fun decrementSkullCycle(player: Player) {
		if (player.attribOr<Int>(AttributeKey.SKULL_CYCLES, 0) - 1 <= 0) {
			unskull(player)
		}
		player.modifyNumericalAttribute(AttributeKey.SKULL_CYCLES, -1, 0)
	}
	
	/**
	 * A flag indicating if the player is/should still be considered skulled.
	 */
	@JvmStatic
	fun skulled(player: Player)
			= (player.attribOr<Int>(AttributeKey.SKULL_CYCLES, 0) > 0)
	 
	/**
	 * Updates the specified player's history map.
	 */
	private fun updateAttackHistory(attacker: Player, target: Player) {
		val map = getHistoryFor(attacker)
		map.put(target.id().toString(), System.currentTimeMillis().toString())
		attacker.putattrib(AttributeKey.SKULL_ENTRIES_TRACKER, map)
	}
	
	/**
	 * Checks for the removal of the cached id in the map.
	 */
	private fun checkForRemoval(attacker: Player, target: Player) {
		val map = getHistoryFor(attacker)
		val cachedTime = map[target.id().toString()]?.toLong() ?: 0
		if (TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - cachedTime) >= 20) {
			map.remove(target.id().toString())
			updateAttackHistory(attacker, target)
		}
	}
	
	/**
	 * Gets the history for the specified player.
	 */
	private fun getHistoryFor(player: Player): MutableMap<String, String> =
			player.attribOr(AttributeKey.SKULL_ENTRIES_TRACKER, mutableMapOf<String, String>())
}