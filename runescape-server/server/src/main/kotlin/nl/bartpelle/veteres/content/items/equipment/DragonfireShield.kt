package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Jak on 23/05/2017.
 * Edits by Mack on 7/19/2017.
 */
object DragonfireShield {

    private val DRAGONFIRE_SHIELD_TYPES = DragonfireShieldType.values()
    private const val MAX_CHARGE_COUNT = 50

    @JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
        for (type in DragonfireShieldType.values()) {
            repo.onItemOption3(type.charged()) {
                displayChargeCount(it, true)
            }
            repo.onItemOption4(type.charged()) @Suspendable {
                emptyShield(it, type)
            }
            repo.onItemOnItem(13307, type.uncharged(), {
                if (it.player().world().realm().isPVP) {
                    bloodMoneyCharge(it, type.uncharged(), type)
                }
            })
            repo.onItemOnItem(13307, type.charged(), {
                bloodMoneyCharge(it, type.charged(), type)
            })
            repo.onItemOption3(type.uncharged()) {
                displayChargeCount(it, true)
            }
            repo.onEquipmentOption(1, type.uncharged()) {
                displayChargeCount(it, false)
            }
            repo.onEquipmentOption(1, type.charged(), s@ @Suspendable {
                executeSpecialAttack(it, type)
            })
            repo.onEquipmentOption(2, type.charged()) {
                displayChargeCount(it, false)
            }
        }
    }

    @Suspendable private fun executeSpecialAttack(it: Script, type: DragonfireShieldType) {
        val player = it.player()

        if (!player.privilege().eligibleTo(Privilege.ADMIN) && player.timers().has(TimerKey.DRAGONFIRE_SPECIAL)) {
            it.message("Your shield is still on cooldown from its last use.")
        } else {
            val shield: Item = it.player().equipment().get(EquipSlot.SHIELD)
            val target: Entity = it.target() ?: return

            if (target != player && !it.player().dead() && PlayerCombat.inCombat(player)
                    && PlayerCombat.canAttack(it.player(), target) && EntityCombat.canAttackDistant(player, target, false, 10)) {

                player.face(target)

                if (shield.modifyProperty(ItemAttrib.CHARGES, -1, 0) <= 0) {
                    player.equipment().replace(EquipSlot.SHIELD, type.uncharged())
                    player.message("Your shield has degraded upon using its final charge.")
                }

                // Allow timers to fire before player event
                player.timers().extendOrRegister(TimerKey.COMBAT_ATTACK, 4)
                player.timers().extendOrRegister(TimerKey.IN_COMBAT, 4)
                player.timers().register(TimerKey.DRAGONFIRE_SPECIAL, 200)

                if (shield.id() == DragonfireShieldType.WYVERN.charged()) {
                    wyvernSpecial(it, target)
                } else {
                    dragonfireSpecial(it, target)
                }
            }
        }
    }

    @Suspendable private fun emptyShield(it: Script, type: DragonfireShieldType) {
        val player = it.player()

        it.doubleItemBox("Upon empty this shield it will lose defensive strength along with any remaining charges it has. You may recharge the shield by absorbing dragon's breath or using <col=FF0000>blood money</col> on it.", type.charged(), type.uncharged())
        if (it.optionsTitled("Uncharge the shield?", "Yes.", "No.") == 1) {
            if (player.inventory().remove(Item(type.charged(), 1), false).success()) {
                player.inventory().add(Item(type.uncharged()), false)
                player.animate(type.unchargeAnimationId())
                player.message("You vent the shield's remaining charges harmlessly into the air.")
            }
        }
    }

    @Suspendable private fun bloodMoneyCharge(it: Script, shieldId: Int, type: DragonfireShieldType) {
        val player = it.player()

        it.doubleItemBox("Using blood money on the shield will result in it being fully charged however, you will lose your money in the process. The shield will become untradable while gaining defensive strength. The cost is 500 blood money.", type.charged(), 13314)
        if (it.optionsTitled("Charge the shield?", "Yes, I will sacrifice 500 blood money to charge the shield.", "No, I wish to keep my blood money.") == 1) {
            if (player.inventory().remove(Item(13307, 500), false).failed()) {
                player.message("You do not have enough blood money to fully charge this shield.")
            } else {
                val slot = player.inventory().slotOf(shieldId)

                if (player.inventory()[slot].property(ItemAttrib.CHARGES) == MAX_CHARGE_COUNT) {
                    player.message("This shield cannot store anymore charges!")
                    return
                }

                player.inventory().replace(slot, type.charged())
                player.inventory()[slot].property(ItemAttrib.CHARGES, MAX_CHARGE_COUNT)
                player.message("The shield glows brightly as it consumes the sacrifice.")
            }
        }
    }

    @Suspendable private fun wyvernSpecial(it: Script, target: Entity) {
        val player = it.player()
        val dmg = it.player().world().random(25)

        player.animate(7700)

        if (dmg > 0) {
            target.freeze(25, player)
        }

        it.delay(3)
        val hit = target.hit(player, dmg, 1)
        target.graphic(367)

        if (target is Npc) {
            hit.addXp(CombatStyle.MAGIC)
        } else {
            player.skills().__addXp(Skills.DEFENCE, (hit.damage() * 4).toDouble())
        }
    }

    @Suspendable private fun dragonfireSpecial(it: Script, target: Entity) {
        val player = it.player()

        player.animate(6696)
        player.graphic(1165)

        it.delay(3)
        player.world().spawnProjectile(player, target, 1166, 31, 16, 0, 30, 2, 0)

        val hit = target.hit(player, it.player().world().random(25), 1)

        if (target is Npc) {
            hit.addXp(CombatStyle.MAGIC)
        } else {
            player.skills().__addXp(Skills.DEFENCE, (hit.damage() * 4).toDouble())
        }
    }

    /**
     * A function for displaying the amount of charges the player's shield currently has, otherwise zero.
     */
    private fun displayChargeCount(it: Script, inventory_click: Boolean) {
        val shield = if (!inventory_click) it.player().equipment().get(EquipSlot.SHIELD) else it.itemUsed()
        shield ?: return
        val charges = shield.property(ItemAttrib.CHARGES)
        val pluralOr = if (charges > 1) "charges" else "charge"

        it.itemBox("Your Dragonfire shield has $charges $pluralOr remaining. You may recharge it by using <col=FF0000>blood money</col> or absorbing dragon's breath.", shield.id())
        it.message("Your Dragonfire shield has $charges $pluralOr remaining.")
    }

    /**
     * A function dedicated to charging our current shield and swapping the shield when needed.
     * TODO: Handle proper charging for eco world.
     */
    @JvmStatic fun charge(player: Player) {
        player.equipment().get(EquipSlot.SHIELD) ?: return
        val shieldType= getTyped(player)

        if (shieldType != null) {
            if (player.equipment()[EquipSlot.SHIELD].id() != shieldType.charged()) {
                player.equipment().replace(EquipSlot.SHIELD, shieldType.charged())
            }

            player.equipment()[EquipSlot.SHIELD].property(ItemAttrib.CHARGES, MAX_CHARGE_COUNT)
            player.timers().extendOrRegister(TimerKey.COMBAT_ATTACK, 3)

            player.graphic(if (shieldType == DragonfireShieldType.WYVERN) 1399 else 1164)
            player.animate(6695)

            player.message("Your dragonfire shield glows more brightly.")
        }
    }

    /**
     * A flag checking if the player meets the conditions to be charging their dragonfire shield.
     */
    @JvmStatic fun canCharge(player: Player): Boolean {
        val shield = player.equipment().get(EquipSlot.SHIELD) ?: return false

        return (getTyped(player) != null) && shield.property(ItemAttrib.CHARGES) < MAX_CHARGE_COUNT
    }

    fun getTyped(player: Player): DragonfireShieldType? {
        for (type in DRAGONFIRE_SHIELD_TYPES) {
            if (player.equipment()[EquipSlot.SHIELD] != null) {
                if (player.equipment()[EquipSlot.SHIELD].id() == type.charged() || player.equipment()[EquipSlot.SHIELD].id() == type.uncharged()) {
                    return type
                }
            }
        }

        return null
    }

    enum class DragonfireShieldType(private val uncharged: Int, private val charged: Int, private val unchargeAnimationId: Int) {
        REGULAR(11284, 11283, 6700),
        WARD(22003, 22002, 65535),
        WYVERN(21634, 21633, 65535);

        fun charged(): Int {
            return charged
        }

        fun uncharged(): Int {
            return uncharged
        }

        fun unchargeAnimationId(): Int {
            return unchargeAnimationId
        }
    }
}