package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsedId
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.WeightedCollection

object AbyssalSireFountain {
	
	private val UNSIRED = 13273
	
	@JvmStatic
	@ScriptMain
	fun register(r: ScriptRepository) {
		r.onItemOnObject(27029) @Suspendable {
			val player = it.player()
			val item: Int = it.itemUsedId()
			when (item) {
				UNSIRED -> {
					if (player.inventory().has(UNSIRED)) {
						player.animate(827)
						player.inventory().remove(UNSIRED, true)
						it.itemBox("You place the Unsired into the Font of Consumption...", UNSIRED)
						
						val reward = UnsiredRewards.rewards.next()
						player.inventory().addOrDrop(reward, player)
						it.itemBox("The Font consumes the Unsired and returns you a reward.", reward.id(), reward.amount())
					}
				}
				else -> it.player().message("Nothing interesting happens.")
			}
		}
	}
	
	private enum class UnsiredRewards(val item: Item, val chance: Double) {
		
		ABYSSAL_ORPHAN(Item(13262), 3.91),
		ABYSSAL_HEAD(Item(7979), 7.81),
		ABYSSAL_WHIP(Item(4151), 9.37),
		JAR_OF_MIASMA(Item(13277), 10.16),
		ABYSSAL_DAGGER(Item(13265), 20.31),
		ABYSSAL_BLUDGEON(Item(13263), 20.31),
		BLOOD_MONEY(Item(13307, 10_000), 28.13);
		
		companion object {
			val rewards = WeightedCollection<Item>()
			
			init {
				values().forEach {
					rewards.add(it.chance, it.item)
				}
			}
		}
		
	}
	
}