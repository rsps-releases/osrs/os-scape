package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/8/2015.
 */

object Hans {
	
	val HANS = 3077
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(HANS) @Suspendable {
			it.chatNpc("Hello. What are you doing here?", HANS, 590)
			when (it.options("I'm looking for whoever is in charge of this place!", "I have come to kill everyone in this castle!", "I don't know. I'm lost. Where am I?", "Can you tell me who long I've been here?", "Nothing.")) {
				1 -> {
					it.chatPlayer("I'm looking for whoever is in charge of this place.")
					it.chatNpc("Who, Bart and Situations? They're probably busy with something.. I'd try talking to a player moderator.", HANS, 590)
				}
				2 -> {
					it.chatPlayer("I have come to kill everyone in this castle!")
					it.chatNpc("Oh no, help! HELP!", HANS, 590)
				}
				3 -> {
					it.chatPlayer("I don't know. I'm lost. Where am I?")
					it.chatNpc("You are in Lumbridge Castle.", HANS, 590)
				}
				4 -> {
					it.chatPlayer("Can you tell me how long I've been here?")
					it.chatNpc("Ahhh, I see all the newcomers arriving in Lumbridge, fresh-faced and eager for adventure. I remember you..", HANS, 590)
					getTime(it)
				}
				5 -> {
					it.chatPlayer("Nothing.")
				}
			}
			
		}
		r.onNpcOption2(HANS) @Suspendable {
			getTime(it)
		}
	}

	@JvmStatic @Suspendable fun getTime(it: Script) {
		it.player().stopActions(false)
		val time: Int = (it.player().gameTime().toDouble() * 0.6).toInt()
		val days: Int = (time / 86400)
		val hours: Int = (time / 3600) - (days * 24)
		val minutes: Int = (time / 60) - (days * 1440) - (hours * 60)
		val dayText = if (days == 0) "" else " " + days + " day${if (days > 1) "s" else ""},"
		it.chatNpc("You've spent$dayText " + hours + " hour${if (hours > 1) "s" else ""} and " + minutes + " minute${if (minutes > 1) "s" else ""} playing OS-Scape.", HANS, 590)
	}
	
	@JvmStatic fun getTimeForQuestTab(p: Player): String {
		val time: Int = (p.gameTime().toDouble() * 0.6).toInt()
		val days: Int = (time / 86400)
		val hours: Int = (time / 3600) - (days * 24)
		val minutes: Int = (time / 60) - (days * 1440) - (hours * 60)
		return "$days:$hours:$minutes"
	}
	
	@JvmStatic fun getTimeDHS(p: Player): String {
		val time: Int = (p.gameTime().toDouble() * 0.6).toInt()
		val days: Int = (time / 86400)
		val hours: Int = (time / 3600) - (days * 24)
		val minutes: Int = (time / 60) - (days * 1440) - (hours * 60)
		var m = ""
		if (days > 0)
			m += "${days}d"
		if (hours > 0)
			m += "${hours}h"
		if (days < 0 && hours < 0)
			m += "${minutes}m"
		return m
	}
	
	
}
