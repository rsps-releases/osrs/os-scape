package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.Player

/**
 * @author Mack
 */
class VarbitIteration(private val player: Player, start: Int, end: Int) {

    val script: Function1<Script, Unit> = @Suspendable {
        var counter = start

        while(counter++ < end) {

            player.varps().varbit(counter, 1)
            player.message("Set varbit $counter with value 1.")
            it.delay(1)
        }
    }
}