package nl.bartpelle.veteres.content.areas.global

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 2/23/2016.
 */
object BankChests {
	
	// Pair(closedId, openId)
	val CHESTS: Array<Pair<Int, Int>> = arrayOf((Pair(3193, 3194)))
	val _CHESTS = intArrayOf()
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		CHESTS.forEach { chest ->
			// Register open options
			/*r.onObject(chest.first) @Suspendable {
				val obj = it.interactionObject()
				it.player().lock()
				it.player().animate(536)
				it.sound(52)
				it.delay(1)
				it.player().world().spawnObj(MapObj(obj.tile(), chest.second, obj.type(), obj.rot()))
				it.player().unlock()
			}*/
			
			// Register the bank interacting
			r.onObject(30267) @Suspendable {
				Bank.open(it.player(), it)
			}
			r.onObject(chest.second) @Suspendable {
				val obj = it.interactionObject()
				val opt = it.interactionOption()
				
				if (opt == 1) {
					Bank.open(it.player(), it)
				} else {
					// Closing
					/*it.player().lock()
					it.player().animate(536)
					it.sound(51)
					it.delay(1)
					it.player().world().spawnObj(MapObj(obj.tile(), chest.first, obj.type(), obj.rot()))
					it.player().unlock()*/
				}
			}
		}
	}
	
}