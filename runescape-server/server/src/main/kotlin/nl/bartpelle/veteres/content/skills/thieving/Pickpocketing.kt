package nl.bartpelle.veteres.content.skills.thieving

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion
import nl.bartpelle.veteres.content.items.teleport.ArdyCape
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.CombatStyle
import java.lang.ref.WeakReference
import java.util.*

/**
 * Created by Bart on 10/25/2015.
 */

object Pickpocketing {

	enum class PickpocketableNpc(val level: Int, val xp: Double, val identifier: String, val stunTimer: Int,
	                             val damage: Int, val petOdds: Int, val loot: ArrayList<Item>, vararg val npcs: Int) {
		MAN(1, 8.0, "Man's", 5, 1, 52000, arrayListOf(Item(13307, 2)), 3078, 3079, 3080, 3101, 3038, 3082, 3652),
		FEMALE(1, 8.0, "Female's", 5, 1, 49000, arrayListOf(Item(13307, 3)), 3083, 3084, 3085),
		FARMER(10, 14.5, "Farmer's", 5, 1, 43500, arrayListOf(Item(13307, 10)), 3086, 3087),
		HAM_MEMBER(15, 18.5, "H.A.M member's", 4, 1, 43500, arrayListOf(Item(882, 20), Item(1351, 1), Item(1265, 1), Item(1349, 1),
				Item(1267, 1), Item(886, 20), Item(1353, 1), Item(1207, 1), Item(1129, 1), Item(4302, 1),
				Item(4298, 1), Item(4300, 1), Item(4304, 1), Item(4306, 1), Item(4308, 1), Item(4310, 1), Item(995, 21),
				Item(319, 1), Item(2138, 1), Item(453, 1), Item(440, 1), Item(1739, 1), Item(314, 5), Item(1734, 6),
				Item(1733, 1), Item(1511, 1), Item(686, 1), Item(697, 1), Item(1625, 1), Item(1627, 1), Item(199, 1), Item(201, 1),
				Item(203, 1)), 2540, 2541),
		WARRIOR_WOMAN(25, 26.0, "Warrior woman's", 5, 2, 39000, arrayListOf(Item(995, 18)), 3100),
		AL_KHARID_WARRIOR(25, 26.0, "Al-Kharid warrior's", 5, 2, 37000, arrayListOf(Item(995, 18)), 3103),
		ROGUE(32, 35.3, "Rogue's", 5, 2, 34500, arrayListOf(Item(995, 25)), 2884),
		CAVE_GOBLIN(36, 40.0, "Cave goblin's", 5, 1, 31000, arrayListOf(Item(995, 11), Item(995, 28), Item(995, 32),
				Item(995, 48), Item(2384, 1), Item(10964, 1), Item(10960, 1), Item(10961, 1), Item(10963, 1),
				Item(4537, 1), Item(4548, 1), Item(590, 1), Item(595, 1), Item(10981, 1), Item(1939, 1), Item(440, 1),
				Item(440, 2), Item(440, 3), Item(440, 4)), 2268, 2269, 2270, 2271, 2272, 2273, 2274, 2275, 2276, 2278, 2279,
				2280, 2281, 2282, 2283, 2284, 2285),
		MASTER_FARMER(38, 43.0, "Master farmer's", 5, 3, 27540, arrayListOf(Item(5318, 1), Item(5319, 1), Item(5324, 3), Item(5323, 2),
				Item(5321, 2), Item(5305, 4), Item(5307, 2), Item(5308, 2), Item(5306, 3), Item(5309, 2), Item(5310, 1), Item(5311, 1),
				Item(5101, 1), Item(5102, 1), Item(5103, 1), Item(5104, 1), Item(5105, 1), Item(5106, 1), Item(5096, 1), Item(5097, 1),
				Item(5098, 1), Item(5099, 1), Item(5100, 1), Item(5291, 1), Item(5292, 1), Item(5293, 1), Item(5294, 1), Item(5295, 1),
				Item(5296, 1), Item(5297, 1), Item(5298, 1), Item(5299, 1), Item(5300, 1), Item(5301, 1), Item(5302, 1), Item(5303, 1),
				Item(5304, 1), Item(5280, 1), Item(5281, 1)), 3257, 3258),
		GUARD(40, 46.8, "Guard's", 5, 2, 23000, arrayListOf(Item(995, 30)), 1546, 1547, 1548, 1549, 3010, 3011, 3245),
		DESERT_BANDIT(53, 79.5, "Desert bandit's", 5, 5, 23000, arrayListOf(Item(995, 30)), 690, 695),
		KNIGHT(55, 79.5, "Knight's", 5, 3, 19000, arrayListOf(Item(995, 50)), 3108, 4022),
		MENAPHITE_THUG(65, 137.5, "Menaphite thug's", 5, 5, 17500, arrayListOf(Item(995, 60)), 3549, 3550),
		PALADIN(70, 151.75, "Paladin's", 5, 3, 12000, arrayListOf(Item(995, 80)), 3104, 3105),
		GNOME(75, 198.5, "Gnome's", 5, 1, 11540, arrayListOf(Item(995, 300)), 5130, 6094, 6095, 6096),
		HERO(80, 125.0, "Hero's", 6, 4, 9700, arrayListOf(Item(13307, 25)), 3106)

		;

		companion object {

			fun getNpcIds(): IntArray {
				val npcIds = mutableListOf<Int>()
				for (npc in PickpocketableNpc.values()) {
					for (id in npc.npcs) {
						npcIds.add(id)
					}
				}
				return npcIds.toIntArray()
			}
		}
	}

	fun thievingChance(player: Player, type: PickpocketableNpc): Int {
		val thievingLevel = player.skills()[Skills.THIEVING];
		val requiredLevel = type.level;
		val minimum = 45 //start with a 45% success chance
		val slope = 2
		var chance = increasedChanceValue(player) + minimum;
		if (thievingLevel > requiredLevel) {
			chance += (thievingLevel - requiredLevel) * slope;
		}
		if (CapeOfCompletion.THIEVE.operating(player)) {
			chance = (chance * 1.10).toInt()
		}
		if (player.inventory().hasAny(ArdyCape.ARDY_CLOAK_4, ArdyCape.ARDY_MAXCAPE) || player.equipment().hasAny(ArdyCape.ARDY_CLOAK_4, ArdyCape.ARDY_MAXCAPE)) {
			chance = (chance * 1.10).toInt()
		}
		return chance
	}

	val GLOVES_OF_SILENCE = 10075

	fun increasedChanceValue(player: Player): Int {
		var chance = 0
		if (player.equipment().get(EquipSlot.HANDS) != null && player.equipment().get(EquipSlot.HANDS).id() == GLOVES_OF_SILENCE)
			chance += 12
		if (Equipment.wearingMaxCape(player)) {
			chance += 10
		}
		if (player.equipment().get(EquipSlot.CAPE) != null && player.equipment().get(EquipSlot.CAPE).id() == 15349) // must be a r667 item
			chance += 15

		return chance
	}


	@Suspendable fun steal(def: PickpocketableNpc, npcId: Int, it: Script) {
		if (Skills.disabled(it.player(), Skills.THIEVING)) {
			return
		}
		// Check level requirement
		if (!it.player.world().realm().isPVP && it.player().skills()[Skills.THIEVING] < def.level) {
			it.player().sound(2277, 0)
			it.player().message("You need a Thieving level of ${def.level} to pickpocket the ${def.identifier.replace("'s", "")}.")
			return
		}

		//Is the players inventory full?
		if (it.player().inventory().full()) {
			it.player().sound(2277, 0)
			it.messagebox("Your inventory is too full to hold any more.")
			return
		}


		it.player().lockDamageOk()
		it.player().message("You attempt to pick the ${def.identifier} pocket.")

		// Did we fail? Oh no!
		var chance = thievingChance(it.player(), def)

		// Cap the success rate at 95%
		if (chance > 95)
			chance = 95

		//s.message("Chance to thieve is $chance%.")
		if (it.player().world().random(100) >= chance && !BonusContent.isActive(it.player, BlessingGroup.INFILTRATOR)) {
			val ref: WeakReference<Entity> = it.player().attrib(AttributeKey.TARGET)
			it.player().message("You fail to pick the ${def.identifier} pocket.")
			it.player().animate(-1)

			if (ref.get() != null) {
				val npc: Entity = ref.get()!!

				npc.stopActions(true)
				npc.sync().shout("What do you think you're doing?")
				npc.pathQueue().clear()
				it.delay(1)
				npc.face(it.player())
				npc.animate(422)

				it.player().hit(npc, def.damage).combatStyle(CombatStyle.GENERIC)
				it.player().stun(def.stunTimer)
				val player = it.player()
				it.player().executeScript @Suspendable { s ->
					s.clearContext()
					s.delay(def.stunTimer)
					player.graphic(-1)
				}
				it.player().sound(2727, 0)
				it.player().sound(518, 20)
				it.player().unlock()

				it.clearContext()
				it.delay(2)
				npc.face(null)
			}

			return
		}

		it.player().animate(881)
		it.player().sound(2581, 0)
		it.delay(1)
		it.player().message("You pick the ${def.identifier} pocket.")

		// Woo! A pet!
		val odds = (def.petOdds.toDouble() * it.player().mode().skillPetMod()).toInt()
		if (it.player().world().rollDie(odds, 1)) {
			unlockRaccoon(it.player())
		}

		//Random chance of getting a casket on OSRune : )
		if (it.player().world().realm().isOSRune && it.player().world().rollDie(15, 1)) {
			if (it.player().inventory().add(Item(7956, 1), false).success()) {
				it.message("You find a chest in the ${def.identifier} pocket.")
			}
		}

		if (def.loot.size > 0) {
			val randomId = it.player().world().random(def.loot.size - 1)

			var item: Item = def.loot[randomId]
			// 1/30 for HAM member to give easy clue casket
			if (def == PickpocketableNpc.HAM_MEMBER && it.player().world().random(30) == 1) {
				item = Item(20546)
			}

			if (item.id() == 995) {
				// Realism faggot
				if (it.player().ironMode() != IronMode.NONE) {
					it.player().inventory() += item
				} else {
					// Give non ironmen a bit more cash, juiccy
					if (item.id() == 995)
						it.player().inventory() += Item(item.id(), Math.min(1000, item.amount() * 10))
				}
			} else {
				it.player().inventory() += item
			}

		}

		it.addXp(Skills.THIEVING, def.xp)

		AchievementAction.processCategoryAchievement(it.player(), AchievementCategory.THIEVING, npcId)

		it.player().unlock()
	}

	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		PickpocketableNpc.values().forEach { n ->
			n.npcs.forEach {
				id ->
				r.onNpcOption2(id, @Suspendable {
					steal(n, id, it)
				})
			}
		}
	}

	fun unlockRaccoon(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.ROCKY)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(Pet.ROCKY.varbit, 1)

			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.ROCKY, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(Pet.ROCKY.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}

			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.ROCKY.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}

}
