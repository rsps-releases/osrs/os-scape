package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import io.netty.buffer.Unpooled
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.crypto.IsaacRand
import nl.bartpelle.veteres.io.RSBuffer
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.instance.InstancedMap
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.action.PlayerAction2
import nl.bartpelle.veteres.util.*
import java.lang.ref.WeakReference
import java.util.*

/**
 * @Author Jak/Shadowy
 * @date Created on Nov 22 2017
 */
object PkBots {
	
	val LOOTER_BOT_SPAWNS_VARROCK = arrayListOf<Tile>(
			// v centre
			Tile(3209, 3423),
			Tile(3215, 3224),
			Tile(3218, 3428),
			Tile(3212, 3434),
			Tile(3203, 3429),
			// west bank/side
			Tile(3198, 3433),
			Tile(3196, 3443),
			Tile(3187, 3452),
			Tile(3175, 3456),
			Tile(3177, 3443),
			Tile(3178, 3428),
			Tile(3182, 3412),
			// south from centre path
			Tile(3210, 3413),
			Tile(3211, 3392),
			// east
			Tile(3228, 3430),
			Tile(3246, 3432),
			Tile(3263, 3430)
	)
	
	val LOOTER_BOT_SPAWNS_CANIFIS = arrayListOf<Tile>(
			Tile(3505, 3484),
			Tile(3499, 3491),
			Tile(3491, 3493),
			Tile(3487, 3481)
	)
	
	@JvmStatic val PURE_PKERS_VARROCK = arrayListOf<Tile>(
			Tile(3208, 3426),
			Tile(3210, 3432),
			Tile(3217, 3433),
			Tile(3222, 3427)
	)
	
	@JvmStatic
	val LOOTER_BOTS = arrayListOf<Player>()
	@JvmStatic
	val PKER_BOTS = arrayListOf<Player>()
	@JvmStatic
	val FOLLOWER_BOTS = arrayListOf<FollowerBots>()
	
	val GITEMS = arrayListOf<Item>(Item(892, 5),
			Item(526, 1), Item(526, 1), Item(526, 1),
			Item(526, 1), Item(526, 1), Item(526, 1), Item(526, 1), Item(526, 1),
			Item(526, 1), Item(526, 1), Item(526, 1), Item(526, 1), Item(526, 1), Item(526, 1),
			Item(229, 1),
			Item(149, 1), Item(161, 1), Item(159, 1), Item(385, 3),
			Item(3144, 1), Item(890, 10), Item(555, 40), Item(560, 20), Item(565, 10),
			Item(9075, 10), Item(558, 100), Item(8013, 2),
			Item(9244, 2))
	
	@JvmStatic
	fun initPkers(world: World) {
		initPurePker(world, PURE_PKERS_VARROCK, PVPAreas.VARROCK_CORNER, PVPAreas.varrock_map!!)
	}
	
	@JvmStatic
	fun initPurePker(world: World, spawn: ArrayList<Tile>, corner: Tile, map: InstancedMap) {
		spawn.forEach { l ->
			var loc = PVPAreas.resolveBasic(l, corner, map)
			loc = loc.randomize(1)
			val bot = Player(null, name(world), world, loc, IsaacRand(), IsaacRand())
			GameCommands.process(bot, "nh")
			GameCommands.process(bot, "spec")
			bot.varps().varp(Varp.RUNNING_ENABLED, 1)
			val actions = LastBotActions()
			actions.spawnTile = loc
			bot.putattrib(AttributeKey.BOT_INFO, actions)
			bot.looks().randomize()
			
			if (world.registerPlayer(bot)) {
				PKER_BOTS.add(bot)
				println("Pure pker spawned: ${bot.name()}")
				
				// Give looter stuff to do, move and talk
				bot.executeScript @Suspendable {
					it.clearContext()
					it.delay(2)
					bot.executeScript(purePkerScript(bot, world).script)
				}
			}
		}
	}
	
	@JvmStatic
	val MAGIC_WEP: ArrayList<String> = arrayListOf("staff", "wand")
	@JvmStatic
	val RANGE_WEP: ArrayList<String> = arrayListOf("bow", "crossbow", "chinch", "dart", "knife", "karil")
	
	@Suspendable
	class purePkerScript(bot: Player, world: World) {
		val script: Function1<Script, Any> = @Suspendable {
			it.clearContext() // uninterruptable
			while (bot.active()) {
				//bot.sync().shout("hi lol")
				if (bot.dead()) {
					it.delay(10)
					GameCommands.process(bot, "nh") // restore
					GameCommands.process(bot, "spec")
					continue
				}
				val inf = info(bot)
				val hp = bot.skills().level(3)
				val hpLvl = bot.skills().xpLevel(3)
				val pray = bot.skills().level(5)
				val range = bot.skills().level(4)
				val mage = bot.skills().level(6)
				val staticRange = bot.skills().xpLevel(4)
				val food = bot.inventory().findFirst(385)
				val brew = bot.inventory().findFirst(6685)
				val restore = bot.inventory().findFirst(3024)
				val rangepot = bot.inventory().findFirst(2444)
				val dds = bot.inventory().findFirst(5698)
				val hides = bot.inventory().findFirst(2497)
				val staff = bot.inventory().findFirst(4675)
				val bow = bot.inventory().findFirst(9185)
				val avas = bot.inventory().findFirst(10499)
				val cape = bot.inventory().findFirst(2412)
				val robe = bot.inventory().findFirst(6108)
				
				// Either attacking somebody (following them) or died (spawn point)
				if (PlayerCombat.inCombat(bot)) {
					var target: Player? = PlayerCombat.getTarget(bot) as Player?
					if (target == null) {
						target = PlayerCombat.lastAttacker(bot) as Player?
					}
					if (hp < 30) {
						if (food.second() != null) {
							bot.world().server().scriptRepository().triggerItemOption1(bot, food.second().id(), food.first())
							if (brew.second() != null) {
								bot.world().server().scriptRepository().triggerItemOption1(bot, brew.second().id(), brew.first())
								it.delay(1)
								if (bot.skills().level(3) > 55 && restore.second() != null) {
									if (restore.second() != null) {
										bot.world().server().scriptRepository().triggerItemOption1(bot, restore.second().id(), restore.first())
									}
								}
							}
						}
					} else if (hp < 55) {
						if (food.second() != null) {
							bot.world().server().scriptRepository().triggerItemOption1(bot, food.second().id(), food.first())
						}
					}
					if (range <= staticRange && rangepot.second() != null && hp >= 55) {
						bot.world().server().scriptRepository().triggerItemOption1(bot, rangepot.second().id(), rangepot.first())
					}
					if (restore.second() != null && pray < 25) {
						bot.world().server().scriptRepository().triggerItemOption1(bot, restore.second().id(), restore.first())
					}
					//bot.sync().shout(if (target != null) target.name() else "in cb")
					if (target != null) {
						val oppWep = target.equipment().get(3)
						var style: CombatStyle = CombatStyle.MELEE
						if (oppWep != null) {
							val n: String = oppWep.name(bot.world()).toLowerCase()
							for (nn in MAGIC_WEP)
								if (n.contains(nn))
									style = CombatStyle.MAGIC
							for (nn in RANGE_WEP)
								if (n.contains(nn))
									style = CombatStyle.RANGE
						}
						//bot.sync().shout("$oppWep $style")
						
						if (dds.first() == -1) { // wearing dds, speced or running at target
							if (target.hp() < 50 && bot.varps().varp(Varp.SPECIAL_ENERGY) >= 250 && (!bot.frozen() || (bot.frozen() && bot.tile().nextTo(target.tile())))) {
								// spec again - will be healthy, close to opp/unfrozen, and have spec
								spec(bot, target)
								bot.sync().shout("1")
							} else {
								// back to brid
								switch(bot, arrayListOf(hides, bow, avas))
								clickTarget(bot, target)
								bot.sync().shout("2")
							}
						}
						if ((!bot.frozen() || bot.tile().nextTo(target.tile())) && target.hp() < 60 && bot.varps().varp(Varp.SPECIAL_ENERGY) >= 250) {
							switch(bot, arrayListOf(dds, hides, cape))
							spec(bot, target)
							bot.sync().shout("3 ${bot.varps().varp(Varp.SPECIAL_ENERGY)}")
						}
						else if (style == CombatStyle.RANGE) {
							switch(bot, arrayListOf(staff, robe, cape))
							cast(bot, target, if (mage >= 94) 71 else 69)
						} else {
							switch(bot, arrayListOf(hides, bow, avas))
							clickTarget(bot, target)
						}
						
						if (Misc.secsSince(inf.lastPraySwitch) > 5) {
							val btn = when (style) {
								CombatStyle.MELEE -> 18
								CombatStyle.RANGE -> 17
								CombatStyle.MAGIC -> 16
								CombatStyle.GENERIC -> 18
							}
							bot.putattrib(AttributeKey.INTERACTION_OPTION, 1)
							bot.world().server().scriptRepository().triggerButton(bot, 541, btn, -1, 1, -1)
							inf.lastPraySwitch = System.currentTimeMillis()
						}
					}
					it.delay(1)
					continue
				} else {
					// Only walk about when zone & spawn not null
					if (inf.spawnTile != null && Misc.secsSince(inf.lastMove) > 20 + world.random(10)) {
						val spawn = inf.spawnTile
						// Not in spawn radius
						val radius: Int = 8
						if (!spawn!!.area(radius).contains(bot.tile())) {
							bot.sync().shout("i appear to have wondered")
							inf.lastFollowChat = System.currentTimeMillis()
							inf.lastIdleChat = System.currentTimeMillis()
							inf.lastMove = System.currentTimeMillis()
							Teleports.teleportContextless(it, bot, spawn, 714, Graphic(111, 92))
							continue
						} else {
							var target: Tile = bot.tile()
							for (i in 1..100) {
								target = bot.tile().randomize(radius)
								if (inf.spawnTile!!.area(radius).contains(target))
									break
							}
							inf.lastMove = System.currentTimeMillis()
							bot.walkTo(target, PathQueue.StepType.REGULAR)
						}
					}
					if (Misc.secsSince(inf.lastIdleChat) > 20 + world.random(10)) {
						inf.lastIdleChat = System.currentTimeMillis()
						bot.sync().shout(BOT_IDLE_CHATS[world.random(BOT_IDLE_CHATS.size - 1)])
					}
					if (hp < hpLvl) {
						GameCommands.process(bot, "nh") // restore
						GameCommands.process(bot, "heal")
						GameCommands.process(bot, "spec")
					}
					it.delay(1)
				}
			}
		}
	}
	
	@JvmStatic fun spec(bot: Player, target: Player) {
		if (!bot.varps().varpBool(Varp.SPECIAL_ENABLED)) {
			bot.putattrib(AttributeKey.INTERACTION_OPTION, 1)
			bot.world().server().scriptRepository().triggerButton(bot, 593, 30, -1, 1, -1)
		}
		clickTarget(bot, target)
	}
	
	@JvmStatic fun switch(bot: Player, switches: ArrayList<Tuple<Int, Item>>) {
		switches.forEach { s ->
			if (s.first() > -1) {
				wear(bot, s)
			}
		}
	}
	
	@JvmStatic
	fun wear(bot: Player, equip: Tuple<Int, Item>) {
		Equipment.equip(bot, equip.first(), bot.inventory().get(equip.first())!!, true)
		bot.world().server().scriptRepository().triggerItemOption2(bot, equip.second().id(), equip.first())
	}
	
	@JvmStatic fun cast(bot: Player, target: Player, btn: Int) {
		bot.putattrib(AttributeKey.TARGET, WeakReference<Entity>(target))
		bot.putattrib(AttributeKey.INTERACTION_OPTION, 1)
		bot.putattrib(AttributeKey.INTERACTED_WIDGET_INFO, intArrayOf(218, btn))
		bot.world().server().scriptExecutor().executeLater(bot, SpellOnEntity.script)
	}
	
	@JvmStatic fun clickTarget(bot: Player, target: Player) {
		val kill = PlayerAction2()
		kill.decode(
				RSBuffer(Unpooled.buffer(3))
						.writeLEShortA(target.index())
						.writeByte(1),
				null, 225, 3, bot)
		kill.process(bot)
	}
	
	@JvmStatic fun initGlobalLooterBots(world: World) {
	
		initLootersInInstances(world, LOOTER_BOT_SPAWNS_VARROCK, PVPAreas.VARROCK_CORNER, PVPAreas.varrock_map!!)
		initLootersInInstances(world, LOOTER_BOT_SPAWNS_CANIFIS, PVPAreas.CANIFIS_CORNER, PVPAreas.canifis_map!!)
	}
	
	@JvmStatic fun initLootersInInstances(world: World, spawnLocs: ArrayList<Tile>, instanceCorner: Tile, instance: InstancedMap) {
		
		spawnLocs.forEach { l ->
			var loc = PVPAreas.resolveBasic(l, instanceCorner, instance)
			loc = loc.randomize(2)
			val bot = Player(null, name(world), world, loc, IsaacRand(), IsaacRand())
			val actions = LastBotActions()
			actions.spawnTile = loc
			bot.putattrib(AttributeKey.BOT_INFO, actions)
			bot.looks().randomize()
			for (i in 0 .. 6)
				bot.skills().setXp(i, (if (i == 3) 10 else 0) + Skills.levelToXp(bot.world().random(30)).toDouble())
			
			if (world.registerPlayer(bot)) {
				LOOTER_BOTS.add(bot)
				println("Looter spawned: ${bot.name()}")
				
				// Give looter stuff to do, move and talk
				bot.executeScript @Suspendable {
					it.delay(2)
					bot.executeScript(looterScript(bot, world).script)
				}
			}
		}
	}
	
	@Suspendable class looterScript(bot: Player, world: World) {
		val script: Function1<Script, Any> = @Suspendable {
			it.clearContext() // uninterruptable
			while (bot.active()) {
				if (bot.dead()) {
					it.delay(1)
					continue
				}
				
				// Either attacking somebody (following them) or died (spawn point)
				if (PlayerCombat.inCombat(bot)) {
					it.delay(1)
					continue
				}
				val inf = info(bot)
				if (inf.spawnTile != null) {
					val spawn = inf.spawnTile!!
					// Not in spawn radius
					val radius = 7
					if (!spawn.area(radius).contains(bot.tile())) {
						bot.sync().shout("i appear to have wondered")
						Teleports.teleportContextless(it, bot, spawn, 714, Graphic(111, 92))
						continue
					} else {
						var target: Tile = bot.tile()
						for (i in 1..100) {
							target = bot.tile().randomize(radius)
							if (inf.spawnTile!!.area(radius).contains(target))
								break
						}
						bot.walkTo(target, if (world.random(2) == 2) PathQueue.StepType.FORCED_RUN else PathQueue.StepType.REGULAR)
					}
				}
				it.delay(8 + world.random(10)) // just longer nothing special
				bot.sync().shout(BOT_IDLE_CHATS[world.random(BOT_IDLE_CHATS.size - 1)])
				it.delay(8 + world.random(15))
			}
		}
	}
	
	@JvmStatic fun initFollowerBots(world: World) {
		initFollowerBots(world, PVPAreas.varrockSafe!!, 7, PVPAreas.varrock_map!!)
		initFollowerBots(world, PVPAreas.enter_tile_canifis!!, 7, PVPAreas.canifis_map!!)
		initFollowerBots(world, PVPAreas.CAMELOT_PVP_ENTER_TILE!!, 7, PVPAreas.CAMELOT_INSTANCE!!)
		initFollowerBots(world, PVPAreas.GE_ENTER_TILE!!, 7, PVPAreas.GE_MAP!!)
	}
	
	@JvmStatic fun pvpFollowersInZone(player: Player): Optional<FollowerBots> {
		return Optional.ofNullable(PkBots.FOLLOWER_BOTS.stream().filter { fb -> fb.zone != null && fb.zone.contains(player) && fb.bots.isNotEmpty()}.findFirst().orElse(null))
	}
	
	@JvmStatic fun pvpFollowersOnMe(player: Player): Optional<FollowerBots> {
		return Optional.ofNullable(PkBots.FOLLOWER_BOTS.stream().filter { fb -> fb.target != null && fb.target!!.equals(player) }.findFirst().orElse(null))
	}
	
	class FollowerBots(val zone: Area?) {
		var target: Player? = null
		val bots: ArrayList<Player> = arrayListOf()
	}
	
	class LastBotActions {
		var lastMove: Long = System.currentTimeMillis() - 10_000
		var lastFollowChat: Long = System.currentTimeMillis() - 10_000
		var spawnTile: Tile? = null
		var lastIdleChat: Long = System.currentTimeMillis() - 10_000
		var owner: FollowerBots? = null
		var lastPraySwitch: Long = System.currentTimeMillis() - 10_000
		var botScriptRunning = true
		public var isPermFollower = false
	}
	
	@JvmStatic fun initFollowerBots(world: World, spawn: Tile, count: Int, zone: Area?): FollowerBots {
		val fb = FollowerBots(zone)
		FOLLOWER_BOTS.add(fb)
		for (i in 0 .. count) {
			val loc = spawn.randomize(2)
			val bot = Player(null, name(world), world, loc, IsaacRand(), IsaacRand())
			bot.varps().varp(Varp.RUNNING_ENABLED, 1)
			val actions = LastBotActions()
			actions.spawnTile = loc
			actions.owner = fb
			bot.putattrib(AttributeKey.BOT_INFO, actions)
			bot.looks().randomize()
			for (i in 0..6)
				bot.skills().setXp(i, (if (i == 3) 10 else 0) + Skills.levelToXp(bot.world().random(30)).toDouble())
			if (world.registerPlayer(bot)) {
				fb.bots.add(bot)
				println("FollowerBot spawned: ${bot.name()} to $fb")
				bot.executeScript( @Suspendable {
					it.delay(2) // initial wait a sec
					bot.executeScript(followerScript(bot, world, fb).script)
				})
			}
		}
		return fb
	}
	
	@Suspendable class followerScript(bot: Player, world: World, fb: FollowerBots) {
		val script: Function1<Script, Any> = @Suspendable {
			it.clearContext() // uninterruptable
			while (bot.active() && info(bot).botScriptRunning) {
				//bot.sync().shout("$fb ${if (fb.target == null) "idle" else fb.target!!.name()}")
				if (bot.dead()) {
					it.delay(1)
					continue
				}
				
				// Either attacking somebody (following them) or died (spawn point)
				if (PlayerCombat.inCombat(bot)) {
					it.delay(1)
					continue
				}
				val inf = info(bot)
				
				// Safety
				if (fb.target != null && !fb.target!!.active()) {
					fb.target = null
				}
				if (fb.target == null && fb.zone == null) {
					info(bot).botScriptRunning = false
					bot.executeScript @Suspendable {
						it.clearContext()
						bot.animate(4069, 16)
						bot.sync().shout("bye")
						it.delay(2)
						bot.graphic(678)
						bot.animate(4071)
						it.delay(2)
						bot.logout()
					}
					break
				}
				
				// No target - random walk
				if (fb.target == null) {
					// Only walk about when zone & spawn not null
					if (inf.spawnTile != null && fb.zone != null && Misc.secsSince(inf.lastMove) > 20 + world.random(10)) {
						val spawn = inf.spawnTile
						// Not in spawn radius
						val radius: Int = 2
						if (!spawn!!.area(radius).contains(bot.tile())) {
							bot.sync().shout("i appear to have wondered")
							inf.lastFollowChat = System.currentTimeMillis()
							inf.lastIdleChat = System.currentTimeMillis()
							inf.lastMove = System.currentTimeMillis()
							Teleports.teleportContextless(it, bot, spawn, 714, Graphic(111, 92))
							continue
						} else {
							var target: Tile = bot.tile()
							for (i in 1..100) {
								target = bot.tile().randomize(radius)
								if (inf.spawnTile!!.area(radius).contains(target))
									break
							}
							inf.lastMove = System.currentTimeMillis()
							bot.walkTo(target, PathQueue.StepType.REGULAR)
						}
					}
					if (Misc.secsSince(inf.lastIdleChat) > 20 + world.random(10)) {
						inf.lastIdleChat = System.currentTimeMillis()
						bot.sync().shout(BOT_IDLE_CHATS[world.random(BOT_IDLE_CHATS.size - 1)])
					}
					it.delay(1)
				} else if (PlayerCombat.getTarget(bot) != fb.target) {
					// Start following
					bot.face(fb.target)
					PlayerCombat.setTarget(bot, fb.target!!)
					world.server().scriptExecutor().executeScript(bot, PlayerFollowing.script)
					it.delay(1)
				} else {
					
					val pker: Player = fb.target!!
					// The pker we're following died, guess he must be a shitter.
					if (pker.dead()) {
						bot.stopActions(true)
						bot.sync().shout(BAD_PK_REACTION[world.random(BAD_PK_REACTION.size - 1)])
						it.delay(world.random(5.. 8))
						fb.target = null
					} else {
						var opTarget: Entity? = PlayerCombat.getTarget(pker)
						// Pker killed their target.
						if (opTarget == null) { // could be null if we bolted then ran off, which nullifies target
							opTarget = PlayerCombat.lastTarget(pker)
						}
						if (opTarget != null && opTarget.dead()) {
							if (!inf.isPermFollower) {
								// only stop actions (following the pker) if not a perm follower (spawned by admin cmd)
								bot.stopActions(true)
							}
							reaction(bot, pker.name())
							it.delay(world.random(5.. 8))
						}
						// Opp was recently in combat / has a non-dead target - chill chat
						else if (((opTarget != null && !opTarget.dead()) || PlayerCombat.inCombat(pker)) && Misc.secsSince(inf.lastFollowChat) > 10 + world.random(10)) {
							inf.lastFollowChat = System.currentTimeMillis()
							bot.sync().shout(BOT_FOLLOWING_CHATS[world.random(BOT_FOLLOWING_CHATS.size - 1)])
							it.delay(1)
						} else {
							it.delay(1)
						}
					}
				}
			}
		}
	}
	
	fun reaction(bot: Player, focus: String) {
		var str: String = BOT_PK_REACTIONS[bot.world().random(BOT_PK_REACTIONS.size - 1)]
		if (str.contains("[x]")) {
			str = str.replace("[x]", focus)
			var effect = 0
			var color = 0
			if (str.startsWith("flash2:")) {
				color = 7
			} else if (str.startsWith("flash1:")) {
				color = 6
			} else if (str.startsWith("red:")) {
				color = 1
			} else if (str.startsWith("white:")) {
				color = 5
			} else if (str.startsWith("cyan:")) {
				color = 3
			} else if (str.startsWith("green:")) {
				color = 2
			} else if (str.startsWith("purple:")) {
				color = 4
			} else if (str.startsWith("yellow:")) {
				color = 0
			}
			if (str.startsWith("wave:")) {
				effect = 1
			} else if (str.startsWith("wave2:")) {
				effect = 2
			} else if (str.startsWith("shake:")) {
				effect = 3
			} else if (str.startsWith("scroll:")) {
				effect = 4
			} else if (str.startsWith("slide:")) {
				effect = 5
			} else if (str.startsWith("flash3:")) {
				effect = 8
			} else if (str.startsWith("flash2:")) {
				effect = 7
			} else if (str.startsWith("flash1:")) {
				effect = 6
			} else if (str.startsWith("glow1:")) {
				effect = 9
			} else if (str.startsWith("glow2:")) {
				effect = 10
			} else if (str.startsWith("glow3:")) {
				effect = 11
			}
			val chatMessage = ChatMessage(str, effect, color, bot.calculateBaseIcon(), false)
			bot.sync().publicChatMessage(chatMessage)
		} else {
			bot.sync().shout(str)
		}
	}
	
	@JvmStatic fun info(bot: Player): LastBotActions {
		return bot.attrib<LastBotActions>(AttributeKey.BOT_INFO)
	}
	
	@JvmStatic fun spawnGitems(world: World) {
		spawnGrounditemsInInstance(world, LOOTER_BOT_SPAWNS_VARROCK, GITEMS, PVPAreas.VARROCK_CORNER, PVPAreas.varrock_map!!)
		spawnGrounditemsInInstance(world, LOOTER_BOT_SPAWNS_CANIFIS, GITEMS, PVPAreas.CANIFIS_CORNER, PVPAreas.canifis_map!!)
	}
	
	@JvmStatic fun spawnGrounditemsInInstance(world: World, tiles: ArrayList<Tile>, gitems: ArrayList<Item>, corner: Tile, map: InstancedMap) {
		tiles.forEach { l ->
			var loc = PVPAreas.resolveBasic(l, corner, map)
			// 3 times for each item in the array, randomize spawn on a walkable tile
			for (i in 1..4) {
				val item = gitems[world.random(gitems.size - 1)]
				var pos: Tile = loc
				for (i in 1..100) {
					pos = loc.transform(-5 + world.random(10), -5 + world.random(10))
					if (world.canWalk(pos)) // not solid
						break
				}
				world.spawnGroundItem(GroundItem(world, Item(item.id(), 1 + world.random(item.amount())), pos, null)).respawns(true).respawnTimer(50)
			}
		}
	}
	
	val BOT_NAME_PREFIXS = arrayListOf<String>("meme", "dank", "oldie", "botguy", "zelda", "xb", "dane", "sand", "arena", "rwter")
	val BAD_PK_REACTION = arrayListOf<String>("omfg", "dead", "l0l", "rofl", "shitter", "whale", "l0000000000000000l", "whale lmfao", "LMFAO")
	val BOT_IDLE_CHATS = arrayListOf<String>("b0aty is bad", "streaming is shit", "streaming money l0l $$", "selling house tabs",
			"looters are gay", "die lol", "not cancer btw", "12 btw", "hey guys its luke amenity here and welcome to a new video")
	val BOT_FOLLOWING_CHATS = arrayListOf<String>("seen reddit latest post?", "yt channel?", "yt rank this guy", "wish i was in 2010", "wheres hd graphics lol", "oss ftw", "os-scape is the best",
			"str lvl?", "spec?", "fight then", "gl som1", "my mains better", "free entertainment trihard", "neighbors wifi", "stolen laptop", "what team?")
	val BOT_PK_REACTIONS = arrayListOf<String>("l00000000000000000l", "omfg", "omfg", "omfg!", "k0", "dead", "k0 l0l", "flash2:you got owned by [x]!", "pked lmfao", "sick pker",
			"bodied", "cya", "byeeee", "rofl", "gf", "gff", "gg", "owned", "gaame", "gone", "ded", "dead", "lol!", "lmao!!!", "lmao£$%£^£")
	
	private fun name(world: World): String =
			BOT_NAME_PREFIXS[world.random(BOT_NAME_PREFIXS.size - 1)] + "${world.random(10 .. 999)}"
}