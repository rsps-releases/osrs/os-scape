package nl.bartpelle.veteres.content.skills.crafting

import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Jason MacKeigan on 2016-08-23 at 5:31 PM
 */
enum class TanningHide(val resource: Item, val product: Item) {
	SOFT_LEATHER(Item(1739), Item(1741)),
	HARD_LEATHER(Item(1739), Item(1743)),
	GREEN_DRAGON_HIDE(Item(1753), Item(1745)),
	BLUE_DRAGON_HIDE(Item(1751), Item(2505)),
	RED_DRAGON_HIDE(Item(1749), Item(2507)),
	BLACK_DRAGON_HIDE(Item(1747), Item(2509))
	;
}