package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemOnSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Mack
 */
object ItemCombination {

    //Combinables
    private val VORKATH_HEAD = 21907
    private val ACCUMULATOR = 10499
    private val BANDOS_BOOTS = 11836
    private val BLACK_TOURMALINE_CORE = 21730

    //Combinable products
    private val AVA_ASSEMBLER = 22109
    private val GUARDIAN_BOOTS = 21733

    const val DRAGON_METAL_SHARD = 22097
    const val DRAGON_METAL_SLICE = 22100
    const val DRAGON_METAL_LUMP = 22103
    const val DRAGON_SQUARE_SHIELD = 1187
    const val DRAGON_CHAINBODY = 3140

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onItemOnItem(VORKATH_HEAD, ACCUMULATOR, @Suspendable {
            combineAssembler(it)
        })
        sr.onItemOnItem(BANDOS_BOOTS, BLACK_TOURMALINE_CORE, @Suspendable {
            combineGuardianBoots(it)
        })
        for (i in intArrayOf(DRAGON_METAL_SHARD, DRAGON_METAL_SLICE))
            sr.onItemOnItem(DRAGON_SQUARE_SHIELD, i, @Suspendable {
                createDragonKiteshield(it)
            })
        for (i in intArrayOf(DRAGON_METAL_SHARD, DRAGON_METAL_LUMP ))
            sr.onItemOnItem(DRAGON_CHAINBODY, i, @Suspendable {
                createDragonPlatebody(it)
            })
    }

    @Suspendable private fun createDragonPlatebody(it: Script) {
        if (!it.player().inventory().hasAny(DRAGON_METAL_SHARD, DRAGON_METAL_LUMP, DRAGON_CHAINBODY)) {
            it.message("You're missing some of the items to create the dragon platebody.")
        }
        if (it.optionsTitled("Create the Dragon Platebody?", "Yes", "No") != 1)
            return
        it.player().inventory().remove(DRAGON_CHAINBODY, false)
        it.player().inventory().remove(DRAGON_METAL_LUMP, false)
        it.player().inventory().remove(DRAGON_METAL_SHARD, false)
        it.player().inventory().add(Item(21892), false, it.itemOnSlot())
        it.itemBox("You combine the shard, lump and chainbody to create a Dragon Platebody.", 21892)
    }

    @Suspendable private fun createDragonKiteshield(it: Script) {
        if (!it.player().inventory().hasAny(DRAGON_METAL_SHARD, DRAGON_METAL_SLICE, DRAGON_SQUARE_SHIELD)) {
            it.message("You're missing some of the items to create the dragon kiteshield.")
        }
        if (it.optionsTitled("Create the Dragon Kiteshield?", "Yes", "No") != 1)
            return
        it.player().inventory().remove(DRAGON_SQUARE_SHIELD, false)
        it.player().inventory().remove(DRAGON_METAL_SLICE, false)
        it.player().inventory().remove(DRAGON_METAL_SHARD, false)
        it.player().inventory().add(Item(21895), false, it.itemOnSlot())
        it.itemBox("You combine the shard, slice and square shield to create a Dragon Kiteshield.", 21895)
    }

    @Suspendable private fun combineAssembler(it: Script) {
        val player = it.player()

        if (it.optionsTitled("Create Ava's Assembler?", "Yes, sacrifice Vorkath's head.", "No, not right now.") == 1) {
            player.inventory().remove(VORKATH_HEAD, true)
            player.inventory().remove(ACCUMULATOR, true)
            player.inventory().add(Item(AVA_ASSEMBLER), true)
            it.message("You carefully attach Vorkath's head to the device and create the assembler.")
        }
    }

    @Suspendable private fun combineGuardianBoots(it: Script) {
        val player = it.player()

        if (it.optionsTitled("Create Guardian Boots?", "Yes, I'd like to merge the core with the boots.", "No, not right now.") == 1) {
            player.inventory().remove(BANDOS_BOOTS, true)
            player.inventory().remove(BLACK_TOURMALINE_CORE, true)
            player.inventory().add(Item(GUARDIAN_BOOTS), true)
            it.message("You merge the black tourmaline core with the boots to create a pair of guardian boots.")
        }
    }
}