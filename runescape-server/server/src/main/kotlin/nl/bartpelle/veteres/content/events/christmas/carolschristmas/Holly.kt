package nl.bartpelle.veteres.content.events.christmas.carolschristmas

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-14.
 */

object Holly {
	
	val DRAYNOR_HOLLY = 7517
	val ISLAND_HOLLY = 7493
	
	val ORACLE = 821
	
	val DRAYNOR_ISLAND_TELEPORT = Tile(3020, 3276)
	val ISLAND_TELEPORT = Tile(1254, 3558)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(DRAYNOR_HOLLY) @Suspendable {
			if (CarolsChristmas.getStage(it.player()) < 3) {
				introductionDialogue(it)
			} else if (CarolsChristmas.getStage(it.player()) == 24) {
				it.chatNpc("Quit hanging around ${it.player().name()}! Go and speak to<br>Diango!", ISLAND_HOLLY, 615)
			} else if (CarolsChristmas.getStage(it.player()) == 25) {
				it.chatPlayer("Good news! Diango says he can help! He's happy to hire<br>your entire team.", 568)
				it.chatNpc("Thanks so much for your help. I was worried we would<br>ruin Chrismas entirely! Here, take these gifts as a<br>present from us to you.", ISLAND_HOLLY, 569)
				CarolsChristmas.setStage(it.player(), 26)
				
				it.player().inventory().addOrDrop(Item(20834), it.player()) // Sack of presents
				it.player().message("You've received a holiday item: <col=ff0000> Sack of presents</col>")
				
				it.player().inventory().addOrDrop(Item(20836), it.player()) // Giant present
				it.player().message("You've received a holiday item: <col=ff0000> Giant present</col>")
				
				//Disabled the snowglobe because I can already see the abuse
/*              it.player().inventory().addOrDrop(Item(20832), it.player()) // Snow globe
                it.player().message("You've received a holiday item: <col=ff0000> Snow globe</col>")*/
				
			} else if (CarolsChristmas.getStage(it.player()) == 26) {
				it.chatNpc("Thanks for all of your help ${it.player.name()}. We're really<br>looking forward to working with Diango!", ISLAND_HOLLY, 568)
			} else {
				teleportToFactoryDialogue(it)
			}
		}
		
		r.onNpcOption1(ISLAND_HOLLY)
		@Suspendable {
			if (CarolsChristmas.getStage(it.player()) <= 3) {
				when (it.options("So where is this boss of yours then?", "Why is your factory on this mountain?", "Can you teleport me back to Port Sarim?")) {
					1 -> whereIsThisBoss(it)
					2 -> {
						it.chatPlayer("Why is your factory on this mountain?", 554)
						it.chatNpc("Don't ask me I just work here...", ISLAND_HOLLY, 588)
						when (it.options("So where is this boss of yours then?", "Can you teleport me back to Port Sarim?")) {
							1 -> whereIsThisBoss(it)
							2 -> teleportBackToPortSarim(it)
						}
					}
					3 -> teleportBackToPortSarim(it)
				}
			} else if (CarolsChristmas.getStage(it.player()) == 12) {
				carolWontBudge(it)
			} else if (CarolsChristmas.getStage(it.player()) == 15) {
				whatShouldIBeDoing(it)
			} else if (CarolsChristmas.getStage(it.player()) == 16) {
				focusOnBusinessNow(it)
			} else if (CarolsChristmas.getStage(it.player()) == 18) {
				didntLikePresent(it)
			} else if (CarolsChristmas.getStage(it.player()) == 19) {
				okayIllGoTalkToOracle(it)
			} else if (CarolsChristmas.getStage(it.player()) == 20) {
				it.chatNpc("You must go and tell Carol what's going to happen!", ISLAND_HOLLY, 571)
			} else if (CarolsChristmas.getStage(it.player()) == 21) {
				it.chatPlayer("Carol won't believe me that Ulina is going to die! What<br>should we do?", 615)
				it.chatNpc("I guess it is a little farfetched, especially coming from a<br>human... Maybe you can " +
						"turn yourself a ghost or<br>something?", ISLAND_HOLLY, 577)
				it.chatPlayer("I did see some beds in the factory, I could use one of<br>the sheets!", 589)
				CarolsChristmas.setStage(it.player(), 22)
				it.chatNpc("Good idea but you must hurry ${it.player().name()}, we don't<br>know how long Ulina has left!", ISLAND_HOLLY, 615)
			} else if (CarolsChristmas.getStage(it.player()) == 22) {
				it.chatPlayer("What should I be doing again?", 554)
				it.chatNpc("You should be looking for a bed sheet to make yourself<br>look like a ghost " +
						"so you can convince Carol that Ulina is<br>in danger!", ISLAND_HOLLY, 590)
				it.chatPlayer("Okay, thanks.", 567)
			} else if (CarolsChristmas.getStage(it.player()) == 23) {
				it.chatPlayer("It worked! Carol has gone! You're all free!!", 567)
				it.chatNpc("Gone? What!? We were always free, we just wanted<br>better working conditions! " +
						"You idiot... how are we going<br>to make the presents now!?", ISLAND_HOLLY, 573)
				it.chatPlayer("But I thought you hated Carol? Can't you work without<br>a boss?", 611)
				it.chatNpc("We can make presents but now we have no one to<br>manage deliveries and shipping... you've ruined<br>Christmas!", ISLAND_HOLLY, 616)
				it.chatPlayer("Wait! What about Diango? He's always making presents<br>and might be able to help you out?", 589)
				it.chatNpc("You had better hope so ${it.player().name()}. Go and speak to<br>Diango and let me know as soon as possible!", ISLAND_HOLLY, 615)
				CarolsChristmas.setStage(it.player(), 24)
				when (it.options("Can I get a teleport back to Port Sarim?", "Ok, I'll go find Diango.")) {
					1 -> teleportPlayer(it, it.player().world().randomTileAround(DRAYNOR_ISLAND_TELEPORT, 1))
					2 -> it.chatPlayer("Ok, I'll go find Diango!", 590)
				}
			} else if (CarolsChristmas.getStage(it.player()) == 24) {
				it.chatNpc("Quit hanging around ${it.player().name()}! Go and speak to<br>Diango!", ISLAND_HOLLY, 615)
			} else if (CarolsChristmas.getStage(it.player()) == 25) {
				it.chatPlayer("Good news! Diango says he can help! He's happy to hire<br>your entire team.", 568)
				it.chatNpc("Thanks so much for your help. I was worried we would<br>ruin Chrismas entirely! Here, take these gifts as a<br>present from us to you.", ISLAND_HOLLY, 569)
				CarolsChristmas.setStage(it.player(), 26)
			} else if (CarolsChristmas.getStage(it.player()) == 26) {
				it.chatNpc("Thanks for all of your help ${it.player.name()}. We're really<br>looking forward to working with Diango!", ISLAND_HOLLY, 568)
			} else {
				it.chatPlayer("Where can I find Carol again?", 554)
				it.chatNpc("You'll find Carol in the factory to the North West, just<br>descend the steps to go inside.", ISLAND_HOLLY, 589)
			}
		}
		
		r.onNpcOption2(DRAYNOR_HOLLY)
		@Suspendable {
			if (CarolsChristmas.getStage(it.player()) < 3)
				introductionDialogue(it)
			else if (CarolsChristmas.getStage(it.player()) == 24)
				it.chatNpc("Quit hanging around ${it.player().name()}! Go and speak to<br>Diango!", ISLAND_HOLLY, 615)
			else
				teleportPlayer(it, it.player().world().randomTileAround(ISLAND_TELEPORT, 1))
		}
		
		r.onNpcOption2(ISLAND_HOLLY)
		@Suspendable {
			teleportPlayer(it, it.player().world().randomTileAround(DRAYNOR_ISLAND_TELEPORT, 1))
		}
		
		r.onNpcSpawn(DRAYNOR_HOLLY)
		@Suspendable {
			val npc: Npc = it.npc()
			
			fun faceIvy() {
				npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
					s.onInterrupt { faceIvy() }
					while (true) {
						s.delay(npc.world().random(30))
						npc.faceTile(Tile(3021, 3278))
					}
				})
			}
			
			it.onInterrupt { faceIvy() }
			faceIvy()
		}
	}
	
	@Suspendable fun introductionDialogue(it: Script) {
		it.player().world().definitions().get(NpcDefinition::class.java, 7517).name = "Holly"
		
		it.chatPlayer("Hello, you look a bit worse for wear, are you okay?", 554)
		it.chatNpc("We're okay just very tired, our boss has been working<br>us very hard this year.", DRAYNOR_HOLLY, 611)
		it.chatPlayer("Working hard on what?", 554)
		it.chatNpc("We work in a factory making Christmas presents, we<br>used to love our work but this year our boss has<br>" +
				"turned into a corporate machine and is making us work<br>crazy hours.", DRAYNOR_HOLLY, 613)
		it.chatPlayer("If your job is that bad can't you just quit?", 554)
		it.chatNpc("If we don't make the presents it will ruin Christmas!<br>We just need a better boss.", DRAYNOR_HOLLY, 589)
		when (it.options("Perhaps I should speak to your boss.", "You'd better get back to work then!")) {
			1 -> {
				it.chatPlayer("Perhaps I should speak to your boss and teach her a<br>thing or two about management.", 568)
				it.chatPlayer("Where is this factory of yours?", 554)
				it.chatNpc("That would be great! We're based on a mountain to the<br>far west of here but you can teleport there with us.", DRAYNOR_HOLLY, 568)
				CarolsChristmas.setStage(it.player(), 3)
				CarolsChristmas.setVarbit(it.player(), 3)
				teleportToFactoryDialogue(it)
			}
			2 -> it.chatPlayer("You'd better get back to work then!", 605)
		}
	}
	
	@Suspendable fun teleportToFactoryDialogue(it: Script) {
		it.player().world().definitions().get(NpcDefinition::class.java, 7517).name = "Holly"
		
		it.chatNpc("Would you like to teleport to the factory now?", DRAYNOR_HOLLY, 554)
		when (it.optionsTitled("Teleport to the Christmas Factory?", "Yes", "No")) {
			1 -> teleportPlayer(it, it.player().world().randomTileAround(ISLAND_TELEPORT, 1))
			2 -> {
				it.chatPlayer("Not yet.", 588)
				it.chatNpc("Come back and speak to us when you are ready.", DRAYNOR_HOLLY, 588)
			}
		}
	}
	
	@Suspendable fun teleportBackToPortSarim(it: Script) {
		it.chatPlayer("Can you teleport me back to Port Sarim?", 554)
		it.chatNpc("Certainly, we'll come with you.", ISLAND_HOLLY, 567)
		teleportPlayer(it, it.player().world().randomTileAround(DRAYNOR_ISLAND_TELEPORT, 1))
	}
	
	@Suspendable fun whereIsThisBoss(it: Script) {
		it.chatPlayer("So where is this boss of yours then?", 554)
		it.chatNpc("Before you speak to Carol there is one other small<br>detail you should probably know about her...", ISLAND_HOLLY, 589)
		it.chatNpc("Carol is a Mountain Troll.", ISLAND_HOLLY, 610)
		it.chatPlayer("A Troll!? You want me to stand up to a mountain<br>troll!?", 572)
		it.chatNpc("Carol isn't like most trolls, she's from a group of more<br>intelligent trolls.", ISLAND_HOLLY, 568)
		it.chatNpc("Besides, you're a strong adventurer... please you've got<br>to help us... we're basically slaves!", ISLAND_HOLLY, 611)
		when (it.options("Fine, I'll go and talk to her.", "Nope, I'm out!")) {
			1 -> {
				it.chatPlayer("Fine, I'll go and talk to her... Where can I find Carol?", 567)
				CarolsChristmas.setStage(it.player(), 10)
				it.chatNpc("Thank you! You'll find Carol in the factory to the<br>North West, just descend the steps to go inside.", ISLAND_HOLLY, 568)
			}
			2 -> it.chatPlayer("Nope, I'm out!", 614)
		}
	}
	
	@Suspendable fun carolWontBudge(it: Script) {
		it.chatPlayer("I tried speaking to Carol but she just won't budge. Do<br>you have any ideas?", 611)
		it.chatNpc("Perhaps we should try and ignite Carol's Christmas<br>spirit... help her to see that work and targets isn't what<br>" +
				"Christmas is about.", ISLAND_HOLLY, 590)
		it.chatPlayer("That might work, how do you propose we do that? Do<br>Trolls even care about Christmas?", 568)
		it.chatNpc("Carol once spoke about her favourite gift from a<br>previous Christmas, if you could find that and give it to<br>" +
				"her it should remind her that Christmas is a time of joy.", ISLAND_HOLLY, 556)
		CarolsChristmas.setStage(it.player(), 15)
		it.chatPlayer("Good idea, where should I start looking?", 554)
		it.chatNpc("We have storage chests in the factory, you should start<br>there.", ISLAND_HOLLY, 589)
		it.chatPlayer("Okay, thanks.", 567)
	}
	
	@Suspendable fun whatShouldIBeDoing(it: Script) {
		it.chatPlayer("What should I be doing again?", 554)
		it.chatNpc("You're going to cycle for Carol's favourite Christmas<br>gift and give it to her.", ISLAND_HOLLY, 589)
		it.chatPlayer("Okay, thanks.", 567)
	}
	
	@Suspendable fun focusOnBusinessNow(it: Script) {
		it.chatPlayer("I gave Carol her old gift but she says that was the past<br>and she is focused on business now.", 611)
		it.chatNpc("We'll have to try something else, perhaps a new gift<br>could make her feel differently.", ISLAND_HOLLY, 589)
		it.chatPlayer("Where could we get a Christmas gift?", 554)
		CarolsChristmas.setStage(it.player(), 17)
		generateCarolsToy(it.player())
		it.chatNpc("We're making them in factory! You'll have to find out<br>what kind of gift would be suitable though!", ISLAND_HOLLY, 589)
		it.chatPlayer("How can I do that? I don't think Carol will just tell me<br>what she wants.", 555)
		it.chatNpc("The other staff probably know her best, you'll have to<br>ask them for the information.", ISLAND_HOLLY, 589)
		it.chatPlayer("Okay I'll try that, thanks.", 567)
	}
	
	@JvmStatic @Suspendable fun generateCarolsToy(player: Player) {
		val randomToy = player.world().random(2)
		
		when (randomToy) {
			0 -> { //Ball
				val randomColor = player.world().random(2)
				
				when (randomColor) {
					0 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 0)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 0)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20806)
					}
					1 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 0)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 1)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20802)
					}
					2 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 0)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 2)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20804)
					}
				}
			}
			1 -> { //Partyhat
				val randomColor = player.world().random(2)
				
				when (randomColor) {
					0 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 1)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 0)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20828)
					}
					1 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 1)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 1)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20824)
					}
					2 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 1)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 2)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20826)
					}
				}
			}
			2 -> { //Marionette
				val randomColor = player.world().random(2)
				
				when (randomColor) {
					0 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 2)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 0)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20816)
					}
					1 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 2)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 1)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20820)
					}
					2 -> {
						player.putattrib(AttributeKey.CAROLS_TOY, 2)
						player.putattrib(AttributeKey.CAROLS_TOY_COLOR, 2)
						player.putattrib(AttributeKey.CAROLS_REQUIRED_TOY, 20818)
					}
				}
			}
		}
	}
	
	@Suspendable fun didntLikePresent(it: Script) {
		it.chatPlayer("I made a Carol a new present and she liked it but it<br>still hasn't convinced her.", 611)
		it.chatNpc("Blasted Trolls... What do you think we should do?", ISLAND_HOLLY, 610)
		it.chatPlayer("Well, reminding Carol of the past and improving her<br>present didn't work... we need to convince her the" +
				"<br>future will be better if she changes her ways.", 590)
		it.chatNpc("Carol is business minded so she should always be looking<br>ahead, this might work!", ISLAND_HOLLY, 589)
		it.chatPlayer("The only problem is we have no way of seeing the<br>future...", 611)
		it.chatNpc("Indeed we can't see the future ${it.player().name()}, but I know<br>someone who can... the Oracle!", ISLAND_HOLLY, 568)
		it.chatPlayer("Where can I find this Oracle?", 554)
		it.chatNpc("The Oracle resides atop Ice Mountain, between<br>Edgeville and Falador.", ISLAND_HOLLY, 568)
		when (it.options("Okay, I will go and speak to the Oracle.", "That's too far, go yourself.")) {
			1 -> okayIllGoTalkToOracle(it)
			2 -> {
				it.chatPlayer("That's too far, go yourself.", 588)
				it.chatNpc("I must stay with Ivy, ${it.player().name()}. Please visit the<br>Oracle and save us from this torment!", ISLAND_HOLLY, 611)
				when (it.options("Okay, I will go and speak to the Oracle.", "What's in it for me?")) {
					1 -> okayIllGoTalkToOracle(it)
					2 -> {
						it.chatPlayer("What's in it for me?", 554)
						it.chatNpc("The warm feelings that arise from doing a good deed,<br>${it.player().name()}." +
								" Where is your Christmas spirit?", ISLAND_HOLLY, 589)
						it.chatNpc("And I'm sure we could arrange for some presents from<br>the factory for you.", ISLAND_HOLLY, 589)
						when (it.options("Okay, I will go and speak to the Oracle.", "Nah, see ya!")) {
							1 -> okayIllGoTalkToOracle(it)
							2 -> it.chatPlayer("Nah, see ya!", 605)
						}
					}
				}
			}
			
		}
	}
	
	@Suspendable fun okayIllGoTalkToOracle(it: Script) {
		CarolsChristmas.setStage(it.player(), 19)
		it.chatPlayer("Okay I will go and speak to the Oracle.", 567)
		it.chatNpc("Hello? Is this thing working?", ORACLE, 554)
		it.chatPlayer("What? Who? I'm hearing voices!", 571)
		it.chatNpc("That will be the Oracle I'm guessing.", ISLAND_HOLLY, 605)
		it.chatNpc("Ah good, it's working. You needed to speak to me?", ORACLE, 567)
		it.chatPlayer("Oracle, I seek your wisdom to save Christmas.", 588)
		it.chatNpc("Do not speak ${it.player().name()}. I foresaw your request and I<br>have what you seek. Ulina is going to DIE!", ORACLE, 589)
		it.chatPlayer("Thank you Oracle!", 571)
		it.chatPlayer("Holly! The Oracle showed me what's going to happen!<br>Ulina is going to die!", 572)
		it.chatNpc("Oh no! You must tell Carol, even she won't want this!", ISLAND_HOLLY, 571)
		CarolsChristmas.setStage(it.player(), 20)
	}
	
	@Suspendable fun teleportPlayer(it: Script, tile: Tile) {
		it.targetNpc()!!.sync().faceEntity(it.player())
		it.targetNpc()!!.sync().animation(1818, 1)
		it.targetNpc()!!.sync().graphic(343, 100, 1)
		it.delay(1)
		it.player().graphic(1296)
		it.player().animate(3865)
		it.player().lockNoDamage()
		it.delay(3)
		it.player().teleport(tile)
		it.animate(-1)
		it.player().unlock()
	}
	
	
}
