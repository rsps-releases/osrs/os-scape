package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.graphic
import nl.bartpelle.veteres.content.mechanics.GroundItemAction
import nl.bartpelle.veteres.content.mechanics.SnowInteractions
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.services.logging.LoggingService

/**
 * Created by Jak on 10/05/2016.
 */
object SpellOnGitem {
	
	val MODERN_SPELLBOOK = 218 // all spellbooks have same parent
	val TELEGRAB = 20
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onSpellOnGroundItem(MODERN_SPELLBOOK, TELEGRAB, s@ @Suspendable {
			val player = it.player()
			val gitem: GroundItem = it.player().attrib(AttributeKey.INTERACTED_GROUNDITEM)
			player.faceTile(gitem.tile())
			
			if (gitem.item().id() in SnowInteractions.fakePartyhats) {
				player?.message("<col=0000FF>You need a Magic level of 120 to grab the paperhat.</col>")
				return@s
			}
			while (player.tile().distance(gitem.tile()) > 7) {
				it.player().walkTo(gitem.tile().x, gitem.tile().z, PathQueue.StepType.REGULAR)
				it.delay(1)
			}
			player.pathQueue().clear()
			player.faceTile(gitem.tile())
			if (gitem.valid(player.world()) && GroundItemAction.interact_legally(it, gitem)) {
				// Start the grab
				it.sound(3006)
				it.animate(723)
				it.graphic(142)
				it.delay(1)
				val tileDist = player.tile().distance(gitem.tile())
				player.world().spawnProjectile(player.tile(), gitem.tile(), 143, 10, 0, 45, (9 * tileDist), 10, 10)
				
				// Retreieve 3t later
				it.delay(3)
				player.world().spawnSound(player.tile(), 3007, 0, 2)
				
				if (gitem.valid(player.world()) && player.inventory().add(gitem.item(), false).success()) {
					if (!it.player().world().removeGroundItem(gitem)) {
						it.player.inventory().remove(gitem.item(), true)
						return@s
					}
					
					// Success!
					it.player().sound(2582, 0)
					
					if (it.player().world().realm().isDeadman) {
						if (gitem.item().id() in DeadmanMechanics.KEYS) {
							DeadmanMechanics.pickupKey(it, gitem)
						}
					}
					
					// Blood chest key pickup!
					if (gitem.item().id() in BloodChest.BLOODY_KEYS) {
						BloodChest.takeKey(it, it.player, gitem)
					}
					
					// Does this ground item respawn?
					if (gitem.respawns()) {
						it.player().world().server().scriptExecutor().executeScript(it.player().world()) @Suspendable {
							it.delay(gitem.respawnTimer())
							it.ctx<World>().spawnGroundItem(gitem)
						}
					}
					
					// Clear the hint arrow when we go back and pick stuff up. We don't need to see it anymore.
					if (it.player().attribOr<Tile?>(AttributeKey.LAST_DEATH_TILE, null) == gitem.tile()) {
						it.player().clearattrib(AttributeKey.LAST_DEATH_TILE)
						it.player().write(SetHintArrow(-1)) // clear this
					}
					
					var irrelevant = false
					val value = gitem.item().amount() * (if (it.player().world().realm().isPVP) it.player().world().prices().getOrElse(gitem.item().unnote(it.player().world()).id(), 0) else
						gitem.item().realPrice(it.player().world()))
					if (value > 0) {
						// Update our risked total if looting in the wildy.
						it.player().putattrib(AttributeKey.RISKED_WEALTH, it.player().attribOr<Int>(AttributeKey.RISKED_WEALTH, 0) + value)
						if (gitem.respawns()) { //Is our item a world spawned item?
							irrelevant = true
						} else if (gitem.owner() != null && gitem.owner() as Int in intArrayOf(3875, 7620, 3877, 29426, 206487, 37272, 5835, 37274, 51779)) { //Does it belong to a player?
							irrelevant = true
						}
					} else {
						// No wealth, pointless to log.
						irrelevant = true
					}
					
					// Exclude dupewatch from our own loot. If someones drop trading - flag it!
					if (gitem.ownerMatches(it.player())) {
						it.player().dupeWatch().exclude()
					} else if (!irrelevant) { // drop parties will spam the log
						// THIS IS A DROP TRADE!
						
						it.player().world().server().service(LoggingService::class.java, true).ifPresent { s ->
							s.logDropTrade(it.player(), gitem)
						}
					}
				}
			}
		})
	}
}