package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 23/12/2015.
 */

object KbdCage {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(18987) @Suspendable {
			Ladders.ladderDown(it, Tile(3069, 10255), true)
		}
		r.onObject(18988) @Suspendable {
			Ladders.ladderUp(it, Tile(3016, 3849), true)
		}
		r.onObject(1816) @Suspendable {
			//down stairs from kbd cage
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			//Check to see if the player is teleblocked
			if (!it.player().timers().has(TimerKey.TELEBLOCK)) {
				it.player().lockNoDamage()
				it.player().faceTile(it.player().tile().transform(0, -2, 0))
				it.delay(1)
				it.animate(2140)
				it.message("You pull the lever...")
				it.runGlobal(player.world()) @Suspendable {
					it.delay(1)
					val world = player.world()
					val spawned = MapObj(obj.tile(), 88, obj.type(), obj.rot())
					world.spawnObj(spawned)
					it.delay(5)
					world.removeObjSpawn(spawned)
				}
				it.delay(2)
				it.animate(714)
				it.player().graphic(111, 110, 0)
				it.delay(4)
				player.teleport(2271, 4680)
				it.animate(-1)
				it.player().unlock()
				
				it.message("...And teleport into the Dragon's Lair.")
			} else {
				it.player().teleblockMessage()
			}
		}
		
		r.onObject(1817) @Suspendable {
			//inside kbd area back to the wilderness
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			it.player().lockNoDamage()
			it.player().faceTile(it.player().tile().transform(0, -2, 0))
			it.delay(1)
			it.animate(2140)
			it.message("You pull the lever...")
			it.runGlobal(player.world()) @Suspendable {
				it.delay(1)
				val world = player.world()
				val spawned = MapObj(obj.tile(), 88, obj.type(), obj.rot())
				world.spawnObj(spawned)
				it.delay(5)
				world.removeObjSpawn(spawned)
			}
			it.delay(2)
			it.animate(714)
			it.player().graphic(111, 110, 0)
			it.delay(4)
			player.teleport(3067, 10253)
			it.animate(-1)
			it.player().unlock()
			
			it.message("...And teleport out of the Dragon's Lair.")
		}
	}
	
}
