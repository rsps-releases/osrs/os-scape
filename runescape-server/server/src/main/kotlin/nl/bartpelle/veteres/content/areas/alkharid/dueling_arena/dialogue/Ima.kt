package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Ima {
	
	val IMA = 3346
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(IMA) @Suspendable {
			val random = it.player().world().random(6)
			
			if (random == 1) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("My favourite fighter is Mubariz!", IMA, 567)
				it.chatPlayer("The guy at the information kiosk?", 575)
				it.chatNpc("Yeah! He rocks!", IMA, 567)
				it.chatPlayer("Takes all sorts, I guess.", 562)
			}
			if (random == 1) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Can't you see I'm watching the duels?", IMA, 614)
				it.chatPlayer("I'm sorry!", 571)
			}
			if (random == 2) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Well. This beats doing the shopping!", IMA, 567)
			}
			if (random == 3) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Knock knock!", IMA, 567)
				it.chatPlayer("Who's there?", 588)
				it.chatNpc("Boo.", IMA, 567)
				it.chatPlayer("Boo who?", 575)
				it.chatNpc("Don't cry, it's just me!", IMA, 605)
			}
			if (random == 4) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Hmph.", IMA, 610)
			}
			if (random == 5) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("My son just won his first duel!", IMA, 567)
				it.chatPlayer("Congratulations!", 567)
				it.chatNpc("He ripped his opponent in half!", IMA, 567)
				it.chatPlayer("That's gotta hurt!, 571")
				it.chatNpc("He's only 10 as well!", IMA, 567)
				it.chatPlayer("You gotta start 'em young!", 567)
			}
		}
	}
	
}
