package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

object Skull {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Nothing - class was still in artifact so this class kills the old code.
	}
	
}