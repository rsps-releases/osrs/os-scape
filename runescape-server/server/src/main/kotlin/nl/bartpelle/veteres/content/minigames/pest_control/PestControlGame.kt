package nl.bartpelle.veteres.content.minigames.pest_control

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.mechanics.Poison
import nl.bartpelle.veteres.content.minigames.Minigame
import nl.bartpelle.veteres.content.npcs.pestcontrol.Ravager
import nl.bartpelle.veteres.content.npcs.pestcontrol.Splatter
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Realm
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceVisibility
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import java.util.*
import java.util.logging.Logger

/**
 * Created by Jason MacKeigan on 2016-07-12 at 6:45 PM
 *
 * An instance of this class represents a singular pest control game with a group of players
 * that originated from one of the available lander boats.
 **/
class PestControlGame(val difficulty: PestControlDifficulty, landerType: LanderType, val world: World, val members: List<Player>) : Minigame {
	
	/**
	 * A value representing whether or not the game is in debug mode.
	 */
	val debug = true
	
	/**
	 * The allocated map for our game, if any could be created.
	 */
	val map = world.allocator().allocate(64, 64, landerType.outside)
	
	/**
	 * A value that when reduced to zero determines that the process is complete.
	 */
	var gameCycles = DEFAULT_GAME_CYCLES
	
	/**
	 * The state of the game being either active or inactive. A game is not active
	 * if the game has been completed regardless of succession, or if there are no
	 * members within the game anymore.
	 */
	var active = true
	
	/**
	 * Creates the sequence in which we must kill the portals once they fall. By
	 * default we pass a Random object however in the future we can pass a more
	 * secure random if we so choose to do so.
	 */
	val portalSequence = PortalSequence.randomSequence(Random())
	
	/**
	 * The portal or portals that have had their shields dropped.
	 */
	val shieldlessPortals = mutableListOf<PortalType>()
	
	/**
	 * The position in the sequence that we currently are.
	 */
	var portalSequencePosition = 0
	
	/**
	 * The void knight that exists onto in this mini game.
	 */
	val knight = Npc(difficulty.voidKnight, world, Tile(0, 0))
	
	/**
	 * The squire the player will talk to.
	 */
	val squire = Npc(SQUIRE_NPC, world, Tile(0, 0))
	
	/**
	 * The non-playable characters in this mini game that are pests.
	 * This does not include portals or the void knight.
	 */
	val pests = mutableMapOf<Pest, MutableCollection<Npc>>()
	
	/**
	 * The non-playable characters in this mini-game that are
	 * portals.
	 */
	val portals = mutableMapOf<PortalType, Npc>()
	
	/**
	 * Starts the game by updating the map and moving all members to said map.
	 */
	fun start() {
		val map = this.map.get()
		
		map.set(0, 0, MAP_TILE, MAP_TILE.transform(64, 64), 0, true)
		map.setMulti(true)
		map.setEntitiesAgressiveInArea(true)
		map.setDeallocateOnDeath(false)
		map.setIdentifier(InstancedMapIdentifier.PEST_CONTROL)
		map.setDangerous(false)
		map.setDeallocatesOnLogout(false)
		for (player in members) {
			player.teleport(map.center().transform(3 - player.world().random(3), 17 + player.world().random(5)))
			player.varps().varbit(Varbit.MULTIWAY_AREA, 1)
			//player.write(DisplayInstancedMap(map, player))
			player.interfaces().sendWidgetOn(PestControl.GAME_INTERFACE, Interfaces.InterSwitches.T)
			player.executeScript @Suspendable {
				it.chatNpc("You must defend the Void Knight while the portals are<br>unsummoned. " +
						"The ritual takes twenty minutes though,<br>so you can help out by destroying them yourselves!<br>" +
						"Now GO GO GO!", SQUIRE_NPC)
			}
		}
		spawnRequired()
		updateInterface()
		
		world.server().scriptExecutor().executeScript(world) { process(it) }
	}
	
	/**
	 * Effectively resets or normalizes the attributes of the member
	 * in this mini game to their default state.
	 */
	private fun resetAttributes() {
		for (member in members) {
			member.putattrib(AttributeKey.TEMPORARY_PEST_CONTROL_POINTS, 0)
			member.putattrib(AttributeKey.PEST_CONTROL_LANDER_PRIORITY, 0)
			member.heal(member.maxHp())
			member.skills().replenishSkill(Skills.PRAYER, 99)
			member.varps()[Varp.SPECIAL_ENERGY] = 1000
			member.setRunningEnergy(100.0, true)
			member.varps().varbit(Varbit.MULTIWAY_AREA, 0)
			Poison.cure(member)
		}
	}
	
	/**
	 * Processes pivotal information within the pest control mini game such as map existance,
	 * . The process is available so long as this game is active.
	 */
	@Suspendable private fun process(it: Script) {
		while (active) {
			// every 4.8 seconds we check if the map is existant.
			if (gameCycles % MAP_EXISTANCE_INTERVAL == 0 && gameCycles != DEFAULT_GAME_CYCLES) {
				if (!world.allocator().active(map.get().center()).isPresent
						|| members.filterNotNull().filter { map.get().contains(it.tile()) }.count() == 0) {
					stop()
					moveAll()
					PestControl.removeGame(difficulty)
					world.unregisterAll(map.get())
					Logger.getGlobal().warning("[PestControlGame.kt] Map not active or no players existing in area.")
					break
				}
			}
			
			val result = result()
			if (result.isPresent && !waitBeforeStopping()) {
				stop()
				complete(result.get())
				moveAll()
				resetAttributes()
				PestControl.removeGame(difficulty)
				world.unregisterAll(map.get())
				world.allocator().deallocateQuietly(map.get())
				Logger.getGlobal().info("[PestControlGame.kt] Game has ended with result ${result.get().name}.")
				break
			}
			
			// after 30 seconds or 15 seconds into the game, the next shield is dropped.
			if (gameCycles == DEFAULT_GAME_CYCLES - 25 || gameCycles % DROP_SHIELD_INTERVAL == 0) {
				dropNextShield()
			}
			
			// every 9.6 seconds the void knight potentially spawns a new pest.
			if (gameCycles % SPAWN_KNIGHT_PEST_INTERVAL == 0) {
				if (spawnKnightPestRequired()) {
					spawnKnightPest()
				}
			}
			
			// every 4.8 seconds a new pest is spawned.
			if (gameCycles % SPAWN_PORTAL_PEST_INTERVAL == 0) {
				spawnPortalPest()
			}
			
			// every 7.2 seconds active the spinner effect.
			if (gameCycles % SPINNER_EFFECT_INTERVAL == 0) {
				spinnerEffect()
			}
			
			// every 1.8 seconds update the game interface for each member of the lobby.
			if (gameCycles % INTERFACE_PROCESS_INTERVAL == 0) {
				updateInterface()
			}
			
			gameCycles--
			it.delay(1)
		}
	}
	
	/**
	 * Calculates the result of the game. In the case that no result
	 * is obtained, an Optional empty is returned to indicate so.
	 */
	private fun result(): Optional<GameResult> {
		return if (knight.dead()) {
			Optional.of(GameResult.LOSS_BY_DEATH)
		} else if (gameCycles == 0) {
			Optional.of(GameResult.WIN_BY_TIME)
		} else if (portalDeathCount() == 4) {
			Optional.of(GameResult.WIN_BY_PORTAL)
		} else {
			Optional.empty()
		}
	}
	
	/**
	 * Determines if we should wait a bit before stopping the mini-game.
	 * This bit of code ensures that all members in the game are alive and
	 * well and have finished their dying task.
	 */
	private fun waitBeforeStopping(): Boolean {
		return members.filter { it.dead() }.isNotEmpty()
	}
	
	/**
	 * Completes the mini game in either a successful or unsuccessful manner
	 * depending on the result of the game.
	 */
	private fun complete(result: GameResult) {
		when (result) {
			GameResult.WIN_BY_TIME, GameResult.WIN_BY_PORTAL -> {
				for (member in members) {
					if (!map.get().contains(member.tile())) {
						continue
					}
					
					val temp = member.attribOr<Int>(AttributeKey.TEMPORARY_PEST_CONTROL_POINTS, 0)
					
					if (temp > REQUIRED_TEMP_POINTS) {
						member.putattrib(AttributeKey.PEST_CONTROL_POINTS, member.attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0)
								+ difficulty.pointsGranted)
						
						if (member.world().realm() == Realm.PVP) {
							val amount = when (difficulty) {
								PestControlDifficulty.NOVICE -> 100
								PestControlDifficulty.INTERMEDIATE -> 150
								PestControlDifficulty.VETERAN -> 200
							}
							member.inventory().addOrDrop(Item(13307, amount), member)
							member.message("You have been rewarded ${amount} blood money for assisting the squire.")
						} else {
							member.inventory().addOrDrop(Item(995, member.skills().combatLevel() * 10), member)
						}
						member.executeScript @Suspendable {
							result.dialogue(difficulty.squire, difficulty.pointsGranted, it)
						}
					} else {
						member.executeScript @Suspendable {
							it.messagebox("The knights noticed your lack of zeal in that battle and have not</br>" +
									"presented you with any points.")
						}
					}
				}
			}
			GameResult.LOSS_BY_DEATH -> {
				members.forEach {
					if (!map.get().contains(it.tile())) {
						return
					}
					it.executeScript @Suspendable {
						result.dialogue(difficulty.squire, difficulty.pointsGranted, it)
					}
				}
			}
		}
	}
	
	/**
	 * The number of portals that are effectively dead in the mini game.
	 */
	private fun portalDeathCount(): Int = portals.values.filterNotNull().filter { it.dead() }.count()
	
	/**
	 * Teleports all members within the mini-game to the exit area of the map.
	 * If a player is not in the area but is a member, they are not moved.
	 */
	private fun moveAll() {
		for (member in members) {
			val mapInstance = map.get()
			
			if (mapInstance.contains(member.tile())) {
				member.teleport(mapInstance.exit())
				member.interfaces().closeById(PestControl.GAME_INTERFACE)
			}
		}
	}
	
	/**
	 * Effectively stops the mini game by setting the state of active to
	 * that of false. When this variable is false the process function
	 * terminates
	 */
	private fun stop() {
		active = false
	}
	
	/**
	 * Spawns the necessary non-playable characters required for the mini-game.
	 */
	private fun spawnRequired() {
		for (type in PortalType.values()) {
			val npc = Npc(type.shieldedNpcId, world, map.get().center().transform(type.xOffset, type.yOffset))
			
			portals.put(type, npc)
			world.registerNpc(npc)
			npc.combatInfo().stats.hitpoints = difficulty.portalHealth
			npc.heal(npc.maxHp())
			npc.respawns(false)
		}
		squire.teleport(map.get().center().transform(-1, 15))
		world.registerNpc(squire)
		knight.teleport(map.get().center())
		world.registerNpc(knight)
	}
	
	/**
	 * Spawns a pest near the knight and forces that pest to attack the knight.
	 */
	private fun spawnKnightPest() {
		val pest = Pest.values().filter { it != Pest.SPINNER }.toTypedArray().random()
		val random = Random()
		val tile = map.get().center().transform(-2 + random.nextInt(5), -2 + random.nextInt(5))
		val npc = Npc(pest.getRandom(random, difficulty), world, tile)
		
		world.registerNpc(npc)
		npc.respawns(false)
		npc.attack(knight)
		npc.graphic(654)
	}
	
	/**
	 * Spawns a non-playable character, or pest, best each portal.
	 */
	private fun spawnPortalPest() {
		for ((type, portal) in portals) {
			if (portal.dead()) {
				continue
			}
			val localMinions = portal.closeNpcs(14).filterNotNull()
			
			if (localMinions.size < MINIMUM_PORTAL_SPAWNS) {
				val random = Random()
				
				val world = portal.world()
				
				for (counter in localMinions.size..MINIMUM_PORTAL_SPAWNS) {
					val pest = Pest.values().random()
					val pestId = pest.getRandom(random, difficulty)
					val pestNpc = Npc(pestId, world, world.randomTileAround(portal.tile(), 2))
					val pestCollection = pests[pest] ?: mutableListOf()
					
					world.registerNpc(pestNpc)
					pestNpc.walkRadius(6)
					pestNpc.respawns(false)
					pestNpc.graphic(654)
					pestNpc.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 30)
					
					pestCollection.add(pestNpc)
					pests.put(pest, pestCollection)
					
					if (pest == Pest.SPLATTER) {
						pestNpc.executeScript { Splatter.search(knight, it, map.get().center()) }
					}
					if (pest == Pest.RAVAGER) {
						pestNpc.executeScript { Ravager.search(knight, it, map.get().center()) }
					}
				}
			}
		}
	}
	
	/**
	 * Actives the heal effect of all spinners in the local
	 * area of each portal by 25.
	 */
	private fun spinnerEffect() {
		portals.values.filter { !it.dead() }.forEach {
			val portal = it
			val spinners = it.closeNpcs(64, 5).filterNotNull().filter { Pest.SPINNER.ids.contains(it.id()) }
			
			spinners.forEach {
				portal.heal(25)
			}
		}
	}
	
	/**
	 * Determines if the knight should be spawned based on whether or not
	 * the npcs in the area amount to less. If they do, it is required that
	 * a knight is spawned, otherwise false.
	 */
	private fun spawnKnightPestRequired(): Boolean {
		return knight.closeNpcs(5, 6).size < 5
	}
	
	/**
	 * Updates the interface displayed to the player, visually changing
	 * the values displayed on that interface.
	 */
	private fun updateInterface() {
		for (member in members) {
			val interfaces = member.interfaces()
			
			interfaces.text(PestControl.GAME_INTERFACE, 2, parseCycles(gameCycles))
			interfaces.text(PestControl.GAME_INTERFACE, 3, (if (knight.hp() < 70) "<col=ff0000>" else "<col=00ff00>") + knight.hp().toString())
			
			val points = member.attribOr<Int>(AttributeKey.TEMPORARY_PEST_CONTROL_POINTS, 0)
			interfaces.text(PestControl.GAME_INTERFACE, 4, (if (points < 50) "<col=ff0000>" else "<col=00ff00>") + points.toString())
			
			for (portal in PortalType.values()) {
				if (portals[portal]!!.hp() == 0) {
					interfaces.text(PestControl.GAME_INTERFACE, 14 + portal.interfaceOrder, "<col=ff0000>0")
				} else {
					interfaces.text(PestControl.GAME_INTERFACE, 14 + portal.interfaceOrder, "<col=00ff00>" + portals[portal]!!.hp().toString())
				}
				
				member.write(InterfaceVisibility(PestControl.GAME_INTERFACE, portal.shieldComponent, portal in shieldlessPortals))
			}
		}
	}
	
	/**
	 * Attempts to drop the shield on the next portal in the portal sequence. If
	 * there are no more shields to drop, the function terminates.
	 */
	private fun dropNextShield() {
		val next = portalSequence.next(portalSequencePosition)
		if (next == portalSequence.last() && shieldlessPortals.contains(next)) {
			return
		}
		
		world.npcs().forEachInAreaKt(map.get(), {
			if (it != null && it.id() == next.shieldedNpcId) {
				it.sync().transmog(next.shieldlessNpcId)
			}
		})
		
		val message = when (next) {
			PortalType.BLUE -> "The blue, eastern portal shield has dropped!"
			PortalType.RED -> "The red, south-western portal shield has dropped!"
			PortalType.YELLOW -> "The yellow, south-eastern portal shield has dropped!"
			PortalType.PURPLE -> "The purple, western portal shield has dropped!"
			else -> ""
		}
		
		members.forEach { it.message(message) }
		knight.sync().shout(message)
		shieldlessPortals.add(next)
		portalSequencePosition++
	}
	
	/**
	 * Parses the cycles from either the game or the boat into a readable string of
	 * text that can be displayed on the interface.
	 */
	private fun parseCycles(cycles: Int): String {
		if (cycles == 0) {
			return "0 seconds"
		}
		val minutes: Int = cycles / 100
		
		if (minutes > 0) {
			return "$minutes min"
		}
		val seconds: Int = ((cycles % 100) * .6).toInt()
		
		return "$seconds seconds"
	}
	
	companion object {
		
		/**
		 * The number of cycles that must pass before we check if the map still exists
		 * in the world.
		 */
		val MAP_EXISTANCE_INTERVAL = 8
		
		/**
		 * The number of cycles that must pass before the next shield is dropped.
		 */
		val DROP_SHIELD_INTERVAL = 50
		
		/**
		 * The number of cycles between each spinner effect process.
		 */
		val SPINNER_EFFECT_INTERVAL = 12
		
		/**
		 * The number of cycles that are between each spawn of pests.
		 */
		val SPAWN_KNIGHT_PEST_INTERVAL = 16
		
		/**
		 * The number of cycles that are between each interface update.
		 */
		val INTERFACE_PROCESS_INTERVAL = 3
		
		/**
		 * The number of cycles that must pass before another pest is spawned for each portal.
		 */
		val SPAWN_PORTAL_PEST_INTERVAL = 8
		
		/**
		 * The map region tile used to generate the instanced map.
		 */
		val MAP_TILE = Tile(2624, 2560)
		
		/**
		 * The number of cycles that must pass before the game ends.
		 */
		val DEFAULT_GAME_CYCLES = 2000
		
		/**
		 * The minimum number of spawns that can be around a portal before it will spawn no more.
		 */
		val MINIMUM_PORTAL_SPAWNS = 5
		
		/**
		 * The amount of points the player must obtain within each game through combat
		 * to obtain points at the end.
		 */
		val REQUIRED_TEMP_POINTS = 50
		
		/**
		 * The id of the squire npc that is in the game.
		 */
		val SQUIRE_NPC = 2949
	}
}
