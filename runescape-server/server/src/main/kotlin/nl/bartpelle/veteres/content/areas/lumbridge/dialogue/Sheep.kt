package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/4/2016.
 */

object Sheep {
	
	val SHEARS = 1735
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//All of our beautiful sheep
		r.onNpcOption1(2794) { sheer(it, 2794, 2697) }
		r.onNpcOption1(2795) { sheer(it, 2795, 2698) }
		r.onNpcOption1(2796) { sheer(it, 2796, 2699) }
		r.onNpcOption1(2800) { sheer(it, 2800, 2789) }
		r.onNpcOption1(2801) { sheer(it, 2801, 2790) }
		r.onNpcOption1(2802) { sheer(it, 2802, 2791) }
	}
	
	@Suspendable fun sheer(it: Script, sheep: Int, replace: Int) {
		val player = it.player()
		val npc = it.targetNpc()!!
		
		if (player.inventory().has(SHEARS)) {
			player.lock()
			it.animate(893)
			it.sound(761)
			it.delay(2)
			it.sound(762)
			npc.sync().shout("Baa!")
			if (player.world().rollDie(3, 1)) {
				player.message("The sheep manages to get away from you.")
			} else {
				player.message("You get some wool.")
				player.inventory().add(Item(1737), false)
				it.runGlobal(player.world()) @Suspendable {
					npc.sync().transmog(replace)
					it.delay(30)
					npc.sync().transmog(sheep)
				}
			}
			player.unlock()
		} else {
			it.player().message("You need a set of shears to do this.")
		}
	}
}