package nl.bartpelle.veteres.content.quests

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.InterfaceItem
import nl.bartpelle.veteres.net.message.game.command.InterfaceText

/**
 * Created by Bart on 4/6/2016.
 */
object QuestGuide {
	
	const val QUEST_GUIDE_INTERFACE = 275
	
	@JvmStatic fun clear(player: Player) {
		for (i in 4..133) {
			player.write(InterfaceText(QUEST_GUIDE_INTERFACE, i, ""))
		}
	}
	
	@JvmStatic fun open(player: Player) {
		player.interfaces().sendMain(QUEST_GUIDE_INTERFACE)
	}
	
	@JvmStatic fun clearCompletion(player: Player) {
		for (i in 8..14) {
			player.write(InterfaceText(277, i, ""))
		}
	}
	
	fun addQuestGuideHeader(player: Player, header: String) {
		player.interfaces().text(QUEST_GUIDE_INTERFACE, 2, header)
	}
	
	fun addScrollLine(player: Player, lineIndex: Int, text: String) {
		player.interfaces().text(QUEST_GUIDE_INTERFACE, lineIndex, text)
	}
	
	fun setCompletionItem(player: Player, item: Int, zoom: Int = 260) {
		player.write(InterfaceItem(277, 3, item, zoom))
	}
	
	fun setCompletionTitle(player: Player, title: String) {
		player.interfaces().text(277, 2, title)
	}
	
	fun setCompletionLines(player: Player, vararg messages: String) {
		var l = 8
		for (str in messages) {
			player.interfaces().text(277, l++, str)
		}
	}
	
	fun displayCompletion(player: Player) {
		player.interfaces().sendMain(277)
	}
	
}