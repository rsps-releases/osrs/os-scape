package nl.bartpelle.veteres.content.minigames.pest_control

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.Doors
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.fs.ObjectDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jason MacKeigan on 2016-07-12 at 6:06 PM
 *
 * This singleton manages main interactions with the pest control mini game however
 * it does not represent the mini game itself.
 */
object PestControl {
	
	/**
	 * This temporary value determines if this content is enabled or disabled.
	 */
	var enabled = true
	
	/**
	 * A single mapping of each lander type to a single instance of it's respective
	 * lander boat instance.
	 */
	val landers = mapOf(
			PestControlDifficulty.NOVICE to Lander(LanderType.NOVICE),
			PestControlDifficulty.INTERMEDIATE to Lander(LanderType.INTERMEDIATE),
			PestControlDifficulty.VETERAN to Lander(LanderType.VETERAN))
	
	/**
	 * A single mapping of each difficulty to a single instance of a pest control game.
	 * If the value of the key returns Optional#empty() then it is taken that there is
	 * no active game for that lander type.
	 */
	val games = mutableMapOf<PestControlDifficulty, Optional<PestControlGame>>(
			PestControlDifficulty.NOVICE to Optional.empty(),
			PestControlDifficulty.INTERMEDIATE to Optional.empty(),
			PestControlDifficulty.VETERAN to Optional.empty()
	)
	
	/**
	 * The identification of the parent interface displayed whilst inside the boat.
	 */
	val BOAT_INTERFACE = 407
	
	/**
	 * The identification value of the parent interface displayed whilst inside the game.
	 */
	val GAME_INTERFACE = 408
	
	/**
	 * The id for the shop or reward interface.
	 */
	val REWARD_INTERFACE = 267
	
	/**
	 * The function that appends actions to each of the gankplanks in the pest control
	 * area. The first action adds the member to the boat, and the second removes them.
	 */
	@JvmStatic @ScriptMain fun registerGangplanks(repository: ScriptRepository) {
		if (!enabled) {
			return
		}
		for (landerType in LanderType.values()) {
			repository.onObject(landerType.gankplankIn, s@ @Suspendable {
				val player = it.player()
				val lander = landers[landerType.difficulty]!!
				
				if (player.skills().combatLevel() >= landerType.combatRequirement) {
					// There is a bug that allows double registering in the lander: we remove first.
					lander.removeMember(player)
					
					if (lander.addMember(player)) {
						player.teleport(landerType.inside)
						player.interfaces().sendWidgetOn(BOAT_INTERFACE, Interfaces.InterSwitches.T)
					}
				} else {
					player.message("You need a combat level of ${landerType.combatRequirement} to use this lander.")
				}
			})
			
			repository.onObject(landerType.gankplankOut, {
				val lander = landers[landerType.difficulty]!!
				val player = it.player()
				
				lander.removeMember(player)
				player.teleport(landerType.outside)
				player.interfaces().closeById(BOAT_INTERFACE)
				player.putattrib(AttributeKey.PEST_CONTROL_LANDER_PRIORITY, 0)
			})
		}
	}
	
	@JvmStatic @ScriptMain fun registerDoors(repository: ScriptRepository) {
		repository.onObject(14233, suspend@ {
			open(it, 14235, 14234, 14236)
		})
		
		repository.onObject(14234, suspend@ {
			close(it, 14236, 14233, 14235)
		})
	}
	
	@JvmStatic @ScriptMain fun registerObjectClick(repository: ScriptRepository) {
		for (breakable in Breakable.values()) {
			for (stage in breakable.stages) {
				repository.onItemOnObject(stage, suspend@ {
					val player = it.player()
					val previousStage = breakable.before(stage)
					
					if (previousStage == -1 || stage == breakable.stages.last()) {
						player.message("This does not need to be repaired.")
						return@suspend
					}
					val inventory = player.inventory()
					val log = Item(1511)
					val hammer = Item(2347)
					if (log !in inventory) {
						player.message("You need a log to do this, there may be some nearby.")
						return@suspend
					}
					if (hammer !in inventory) {
						player.message("You need a hammer to do this.")
						return@suspend
					}
					val world = player.world()
					val interacted = player.attrib<MapObj>(AttributeKey.INTERACTION_OBJECT) ?: return@suspend
					val interactedDefinition = interacted.definition(world)
					val newObjectDefinition = world.definitions().get(ObjectDefinition::class.java, previousStage)
					val newObject = MapObj(interacted.tile(), previousStage, newObjectDefinition.modeltypes.get(0), interacted.rot())
					
					inventory.remove(log, false)
					world.removeObj(interacted, interactedDefinition.projectileClipped || interactedDefinition.id == breakable.stages.last())
					world.spawnObj(newObject, newObjectDefinition.projectileClipped)
					player.message("You repair the broken ${interactedDefinition.name}.")
					player.modifyNumericalAttribute(AttributeKey.TEMPORARY_PEST_CONTROL_POINTS, +5, 0)
				})
			}
		}
	}
	
	private fun close(it: Script, uninteracted: Int, interactedOpen: Int, uninteracedOpen: Int) {
		val player = it.player()
		val world = player.world()
		val interacted = player.attrib<MapObj>(AttributeKey.INTERACTION_OBJECT) ?: return
		val uninteractedObject = world.anyObjInDistance(uninteracted, interacted.tile(), 2) ?: return
		
		for (breakable in Breakable.DOORS) {
			if (world.spawnedObjs().any {
				it.tile().distance(interacted.tile()) < 4
						&& breakable.allButFirst.contains(it.id())
			}) {
				player.message("You cannot close a door if one of them is damaged or broken.")
				return
			}
			//println(Arrays.toString(breakable.allButFirst))
		}
		Doors.closeDoor(player.world(), interacted, interactedOpen, it)
		Doors.closeDoor(player.world(), uninteractedObject, uninteracedOpen, it, false, true)
	}
	
	private fun open(it: Script, uninteracted: Int, interactedClose: Int, uninteracedClose: Int) {
		val player = it.player()
		val world = player.world()
		val interacted = player.attrib<MapObj>(AttributeKey.INTERACTION_OBJECT) ?: return
		val uninteractedObject = world.anyObjInDistance(uninteracted, interacted.tile(), 2) ?: return
		
		for (breakable in Breakable.DOORS) {
			if (world.spawnedObjs().any {
				it.tile().distance(interacted.tile()) < 4
						&& breakable.allButFirst.contains(it.id())
			}) {
				player.message("You cannot open a door if one of them is damaged or broken.")
				return
			}
		}
		Doors.openDoor(world, interacted, interactedClose, it)
		Doors.openDoor(world, uninteractedObject, uninteracedClose, it, false)
	}
	
	/**
	 * Effectively registers each process for each lander object that we require.
	 */
	@JvmStatic @ScriptMain fun registerProcesses(repository: ScriptRepository) {
		if (!enabled) {
			return
		}
		repository.onWorldInit {
			val world = it.ctx<World>()
			
			for (lander in landers.values) {
				it.runGlobal(world, lander.process)
			}
		}
	}
	
	@ScriptMain @JvmStatic fun registerRewardInterface(repository: ScriptRepository) {
		repository.onNpcOption2(1755, { PestControlRewardInterface.open(it.player()) })
		
		for (section in PestControlRewardInterface.Section.values()) {
			for (reward in section.rewards) {
				repository.onButton(REWARD_INTERFACE, reward.buttonId(), {
					PestControlRewardInterface.select(it.player(), section, reward)
				})
			}
		}
		
		repository.onButton(REWARD_INTERFACE, 146, {
			val player = it.player()
			
			val selected: Optional<PestControlRewardInterface.Reward> = player.attribOr(
					AttributeKey.PEST_CONTROL_REWARD_SELECTED, Optional.empty<PestControlRewardInterface.Reward>())
			
			if (selected.isPresent) {
				selected.ifPresent {
					PestControlRewardInterface.confirm(player, it)
				}
			} else {
				it.player().message("You must select an option from the interface first.")
			}
		})
	}
	
	/**
	 * Attempt to ship off a group of members of the lander to the island.
	 * If a game is currently active or the map cannot be created false
	 * is returned, otherwise true.
	 *
	 * It should be noted that the ArrayList constructor is used to ensure
	 * the game members and lobby members aren't linked in any way.
	 */
	fun shipOff(lander: Lander, world: World): Boolean {
		val landerType = lander.landerType
		val difficulty = landerType.difficulty
		val members = ArrayList<Player>(lander.prioritizeMembers())
		val game = PestControlGame(landerType.difficulty, landerType, world, members)
		
		if (games[difficulty]!!.isPresent) {
			return false
		}
		
		if (!game.map.isPresent) {
			return false
		}
		
		lander.removeMembers(members)
		lander.increasePriority()
		games.put(difficulty, Optional.of(game))
		game.start()
		
		return true
	}
	
	/**
	 * Removes the game instance from the mapping to establish that an active
	 * game is no present. We do so by signifying that the mapping contains an
	 * empty value.
	 */
	fun removeGame(difficulty: PestControlDifficulty) {
		games.put(difficulty, Optional.empty())
	}
	
	fun gameForMember(member: Player): Optional<PestControlGame> {
		for ((difficulty, game) in games) {
			if (game.isPresent) {
				val pestGame = game.get()
				
				if (pestGame.members.contains(member) && pestGame.map.isPresent) {
					val map = pestGame.map.get()
					
					if (map.contains(member)) {
						return game
					}
				}
			}
		}
		return Optional.empty()
	}
	
}