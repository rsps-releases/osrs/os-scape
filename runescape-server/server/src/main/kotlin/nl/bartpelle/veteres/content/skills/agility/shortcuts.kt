package nl.bartpelle.veteres.content.skills.agility

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.ChangeMapMarker
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.LocationUtilities

/**
 * Created by Bart on 8/29/2015.
 */
@ScriptMain fun agility_shortcuts(r: ScriptRepository) {
	// Al Kharid mine shortcut exit
	r.onObject(16550) @Suspendable {
		if (it.player().skills().level(Skills.AGILITY) < 38) {
			it.message("You need an agility level of 38 to negotiate these rocks.")
		} else {
			it.player().lock()
			it.delay(1)
			it.player().looks().render(738, 737, 737, 737, 737, 737, -1)
			it.player().sound(2244, 10, 4)
			it.player().pathQueue().clear()
			it.player().pathQueue().interpolate(3305, 3315, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().sound(2244, 10, 4)
			it.delay(1)
			it.player().sound(2244, 10, 4)
			it.delay(2)
			it.player().looks().resetRender()
			it.player().pathQueue().interpolate(3306, 3315, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().unlock()
		}
	}
	
	// Fence between lumb and varrock
	r.onObject(16518) @Suspendable {
		val player = it.player()
		if (player.tile().equals(3240, 3334)) {
			player.teleport(player.tile().transform(0, 1, 0))
		} else if (player.tile().equals(3240, 3335)) {
			player.teleport(player.tile().transform(0, -1, 0))
		}
	}
	
	// Fally crumbled wall
	r.onObject(24222) @Suspendable {
		val player = it.player()
		if (player.tile().equals(2936, 3355)) {
			player.teleport(player.tile().transform(-1, 0, 0))
		} else if (player.tile().equals(2935, 3355)) {
			player.teleport(player.tile().transform(1, 0, 0))
		}
	}
	
	// Rope swing orges outside yanille west
	r.onObject(23570) @Suspendable {
		val player = it.player()
		if (player.tile().equals(2511, 3096)) {
			player.teleport(Tile(2511, 3089))
		} else if (player.tile().equals(2511, 3089)) {
			player.teleport(Tile(2511, 3096))
		}
	}
	
	// Fally wall climb, inside to up
	r.onObject(17049) @Suspendable {
		val player = it.player()
		if (player.tile().equals(3033, 3389)) {
			player.teleport(Tile(3032, 3389, 1))
		}
		// Fally wall climb, down to up outside
		else if (player.tile().z == 3390) {
			player.teleport(Tile(3033, 3389, 1))
		}
	}
	
	// Fally wall climb, up to down inside
	r.onObject(17052) @Suspendable {
		val player = it.player()
		if (player.tile().equals(3032, 3389, 1)) {
			player.teleport(Tile(3033, 3389, 0))
		}
	}
	
	// Fally wall climb, up to down outside
	r.onObject(17051) @Suspendable {
		val player = it.player()
		if (player.tile().equals(3033, 3389, 1)) {
			player.teleport(Tile(3033, 3390, 0))
		}
	}
	
	// Fally wall climb south side to up
	r.onObject(17050) @Suspendable {
		val player = it.player()
		if (player.tile().equals(3032, 3388, 0)) {
			player.teleport(Tile(3032, 3389, 1))
		}
	}
	
	// Fred farmer Stile jump lumbridge
	r.onObject(12982) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		if (player.tile().equals(3197, 3276)) { // Verify it's the right object.
			if (player.tile().z == 3276 || player.tile().z == 3275) {
				// Verify loc.
				player.teleport(Tile(3197, 3278))
			} else if (player.tile().equals(3197, 3278)) {
				player.teleport(Tile(3197, 3275))
			}
		}
	}
	
	// Edge dungeon bars
	r.onObject(23566) @Suspendable {
		edge_dungeon_monkeybars(it)
	}
	
	// Watchtower hole under wall
	r.onObject(16520) @Suspendable {
		val player = it.player()
		if (player.tile().equals(2575, 3112)) {
			player.teleport(player.tile().transform(0, -5, 0))
		} else if (player.tile().equals(2575, 3107)) {
			player.teleport(player.tile().transform(0, 5, 0))
		}
	}
	
	// Watchtower wall climb
	r.onObject(20056) @Suspendable {
		val player = it.player()
		if (player.tile().equals(2548, 3119)) {
			player.teleport(2548, 3118, 1)
		}
	}
	
	// in/out of railing surrounding mcgoobers woods with the dogs in object 56 handled else where
	
	// Log balance west of seers coal trucks
	r.onObject(23274) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		if (obj.tile().equals(2602, 3477)) { //; Verify it's the right object haha.
			if (player.tile().x >= 2602) {
				player.teleport(2598, 3477)
			} else if (player.tile().x <= 2599) {
				player.teleport(2603, 3477)
			}
		}
	}
	
	// GE under wall tunnel
	r.onObject(16529, s@ @Suspendable {
		val player = it.player()
		val obj = it.interactionObject()
		
		if (player.world().realm().isPVP) {
			it.messagebox("This shortcut has been disabled in this world.")
			return@s
		}
		if (!player.tile().equals(obj.tile().transform(-1, 0, 0))) {
			player.walkTo(obj.tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
			it.waitForTile(obj.tile().transform(-1, 0, 0))
		}
		player.lock()
		it.delay(1)
		it.sound(2452)
		player.faceObj(obj)
		player.animate(2589, 0)
		player.forceMove(ForceMovement(0, 0, 1, 0, 0, 50, FaceDirection.EAST))
		it.delay(2)
		player.teleport(Tile(player.tile().x + 1, player.tile().z))
		player.animate(2590, 0)
		player.forceMove(ForceMovement(0, 0, 3, -3, 0, 100, FaceDirection.EAST))
		it.delay(3)
		player.teleport(Tile(player.tile().x + 3, player.tile().z - 3))
		player.animate(2591, 0)
		player.forceMove(ForceMovement(0, 0, 1, 0, 15, 33, FaceDirection.EAST))
		it.delay(1)
		player.teleport(Tile(player.tile().x + 1, player.tile().z))
		player.unlock()
	})
	
	// GE under wall
	r.onObject(16530) @Suspendable {
		val player = it.player()
		val obj = it.interactionObject()
		
		if (!player.tile().equals(obj.tile().transform(1, 0, 0))) {
			player.walkTo(obj.tile().transform(1, 0, 0), PathQueue.StepType.FORCED_WALK)
			it.waitForTile(obj.tile().transform(1, 0, 0))
		}
		
		player.lock()
		it.delay(1)
		player.faceObj(obj)
		player.animate(2589, 0)
		player.forceMove(ForceMovement(0, 0, -1, 0, 0, 50, FaceDirection.WEST))
		it.delay(2)
		player.teleport(Tile(player.tile().x - 1, player.tile().z))
		player.animate(2590, 0)
		player.forceMove(ForceMovement(0, 0, -3, 3, 0, 100, FaceDirection.WEST))
		it.delay(3)
		player.teleport(Tile(player.tile().x - 3, player.tile().z + 3))
		player.animate(2591, 0)
		player.forceMove(ForceMovement(0, 0, -1, 0, 15, 33, FaceDirection.WEST))
		it.delay(1)
		player.teleport(Tile(player.tile().x - 1, player.tile().z))
		player.unlock()
	}
	
	// Brimhaven dungeon - vine
	r.onObject(26880) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2672, 9583)) {
			if (player.tile().equals(2673, 9583)) {
				player.teleport(Tile(2670, 9583, 2))
			}
		}
	}
	
	// Brimhaven dungeon - vine
	r.onObject(26882) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2670, 9583, 2)) {
			if (player.tile().equals(2670, 9583, 2)) {
				player.teleport(Tile(2673, 9583, 0))
			}
		}
	}
	
	// Brimhaven dungeon - pipe
	r.onObject(21727) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		// South side
		if (obj.tile().equals(2698, 9493)) {
			if (player.tile().equals(2698, 9492)) {
				player.teleport(Tile(2698, 9500))
			}
		}
		
		// North side
		if (obj.tile().equals(2698, 9498)) {
			if (player.tile().equals(2698, 9500)) {
				player.teleport(Tile(2698, 9492))
			}
		}
	}
	
	// Brimhaven dungeon - log balance
	r.onObject(20884) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2686, 9506)) {
			if (player.tile().equals(2687, 9506)) {
				player.teleport(player.tile().transform(-5, 0, 0))
			} else if (player.tile().equals(2682, 9506)) {
				player.teleport(player.tile().transform(5, 0, 0))
			}
		}
	}
	
	// Eagles peak rock climb
	r.onObject(19849) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2322, 3501)) {
			if (player.tile().equals(2322, 3502)) {
				player.teleport(2324, 3497)
			} else if (player.tile().equals(2324, 3497)) {
				player.teleport(2322, 3502)
			}
		}
	}
	
	// Falador hole under wall
	r.onObject(16528) @Suspendable {
		val player = it.player()
		if (player.tile().equals(2948, 3313)) {
			player.teleport(player.tile().transform(0, -4, 0))
		} else if (player.tile().equals(2948, 3309)) {
			player.teleport(player.tile().transform(0, 4, 0))
		}
	}
	
	// Stepping stones east side of draynor manor
	r.onObject(16533) @Suspendable {
		val player = it.player()
		if (player.tile().equals(3154, 3363)) {
			player.teleport(player.tile().transform(-5, 0, 0))
		} else if (player.tile().equals(3149, 3363)) {
			player.teleport(player.tile().transform(5, 0, 0))
		}
	}
	
	// White wolf mountain grapple to catherby
	r.onObject(17042) @Suspendable {
		val player = it.player()
		if (player.tile().equals(2869, 3430)) {
			player.teleport(2866, 3429)
		}
	}
	
	// Cairn isle rock slide climb
	r.onObject(2231) @Suspendable {
		val player = it.player()
		if (player.tile().x == 2795) {
			player.teleport(player.tile().transform(-4, 0, 0))
		}
	}
	
	// East ardy log balance
	r.onObject(16548) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2601, 3336)) {
			if (player.tile().equals(2602, 3336)) {
				player.teleport(player.tile().transform(-4, 0, 0))
			} else if (player.tile().equals(2598, 3336)) {
				player.teleport(player.tile().transform(4, 0, 0))
			}
		}
	}
	
	// Grand tree climb shortcut north side from waterfall
	r.onObject(16535) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2489, 3520)) {
			if (player.tile().equals(2489, 3521)) {
				player.teleport(2486, 3515)
			}
		}
	}
	
	// Grand tree climb shortcut north side from waterfall
	r.onObject(16534) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2487, 3515)) {
			if (player.tile().equals(2486, 3515)) {
				player.teleport(2489, 3521)
			}
		}
	}
	
	// Rock climb al kharid mining pit
	r.onObject(16549) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(3305, 3315)) {
			if (player.tile().distance(obj.tile()) < 2) {
				player.teleport(3302, 3315)
			}
		}
	}
	
	// Yanille south wall climb, outside up
	r.onObject(17047) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (player.tile().equals(2556, 3072)) {
			player.teleport(Tile(2556, 3073, 1))
		}
		if (obj.tile().equals(2556, 3075)) {
			player.teleport(2556, 3074, 1)
		}
	}
	
	// Yanille south wall climb, up to outside
	r.onObject(17048) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		if (obj.tile().equals(2556, 3072)) {
			if (player.tile().equals(2556, 3073, 1)) {
				player.teleport(Tile(2556, 3072))
			}
		}
		if (obj.tile().equals(2556, 3075)) {
			if (player.tile().equals(2556, 3074, 1)) {
				player.teleport(Tile(2556, 3075))
			}
		}
	}
	
	// Crevice shortcut falador mines
	r.onObject(16543) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		// East side
		if (obj.tile().equals(3034, 9806)) {
			if (player.tile().equals(3035, 9806)) {
				player.teleport(Tile(3028, 9806))
			}
		}
		// West side
		if (obj.tile().equals(3029, 9806)) {
			if (player.tile().equals(3028, 9806)) {
				player.teleport(Tile(3035, 9806))
			}
		}
	}
	
	// draynor under wall
	r.onObject(19036) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		// East side
		if (obj.tile().equals(3069, 3260)) {
			player.teleport(Tile(3065, 3260))
		}
	}
	
	// draynor under wall
	r.onObject(19032) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		// West side
		if (obj.tile().equals(3066, 3260)) {
			player.teleport(Tile(3070, 3260))
		}
	}
	
	// zanaris wall
	r.onObject(17002) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		if (obj.tile().equals(2400, 4404)) {
			player.teleport(player.tile().transform(0, -2, 0))
		}
		if (obj.tile().equals(2400, 4402)) {
			player.teleport(player.tile().transform(0, 2, 0))
		}
		
		if (obj.tile().equals(2415, 4403)) {
			player.teleport(player.tile().transform(0, -2, 0))
		}
		if (obj.tile().equals(2415, 4401)) {
			player.teleport(player.tile().transform(0, 2, 0))
		}
		
		if (obj.tile().equals(2408, 4396)) {
			player.teleport(player.tile().transform(0, -2, 0))
		}
		if (obj.tile().equals(2408, 4394)) {
			player.teleport(player.tile().transform(0, 2, 0))
		}
	}
	
	// 55 stairs wildy shortcut
	r.onObject(19043, s@ @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
        val instanced = MageBankInstance.inDungeon(player)

		val targetTileNorth = Tile(3048, 10336)
        val targetTileSouth = Tile(3046, 10326)

        val creviceTileSouth = Tile(3046, 10327)
        val creviceTileNorth = Tile(3048, 10335)


        val instancedTileNorth = LocationUtilities.dynamicTileFor(targetTileNorth, MageBankInstance.mageBankDungeon)
        val instancedTileSouth = LocationUtilities.dynamicTileFor(targetTileSouth, MageBankInstance.mageBankDungeon)
		if (obj.tile() == creviceTileSouth || obj.tile() == LocationUtilities.dynamicTileFor(creviceTileSouth, MageBankInstance.mageBankDungeon)) {
			if (player.skills().xpLevel(Skills.AGILITY) < 46) {
				player.message("You need a Agility level of 46 to use this obstacle.")
				return@s
			}
			player.lockDelayDamage()
			it.animate(2796)
			it.delay(1)
			it.animate(-1)

			player.teleport(if (instanced) instancedTileNorth else targetTileNorth)
			player.unlock()
		}
		if (obj.tile() == creviceTileNorth || obj.tile() == LocationUtilities.dynamicTileFor(creviceTileNorth, MageBankInstance.mageBankDungeon)) {
			if (player.skills().xpLevel(Skills.AGILITY) < 46) {
				player.message("You need a Agility level of 46 to use this obstacle.")
				return@s
			}
			player.lockDelayDamage()
			it.animate(2796)
			it.delay(1)
			it.animate(-1)
			player.teleport(if (MageBankInstance.inDungeon(player)) instancedTileSouth else targetTileSouth)
			player.unlock()
		}
	})
	
	// yanille dungeon monkey bars
	r.onObject(23567) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2598, 9489) || obj.tile().equals(2597, 9489)) {
			player.teleport(player.tile().transform(0, 7, 0))
		}
		if (obj.tile().equals(2598, 9494) || obj.tile().equals(2597, 9494)) {
			player.teleport(player.tile().transform(0, -7, 0))
		}
	}
	
	// yanille dungeon pipe
	r.onObject(23140) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2576, 9506)) {
			player.teleport(2572, 9506)
		}
		if (obj.tile().equals(2573, 9506)) {
			player.teleport(2578, 9506)
		}
	}
	
	// yanille balance log
	r.onObject(23548) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2580, 9519)) {
			player.teleport(2580, 9512)
		}
		if (obj.tile().equals(2580, 9512)) {
			player.teleport(2580, 9520)
		}
	}
	
	// edge dungeon pipe
	r.onObject(16511) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(3150, 9906)) {
			player.teleport(3149, 9906)
		}
		if (obj.tile().equals(3149, 9906)) {
			player.teleport(3155, 9906)
		}
	}
	
	r.makeRemoteObject(16545)
	
	// Trollhiem shortcut to wildy
	r.onObject(16545) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		var walkToNormally = false
		
		if (obj.tile() in arrayListOf(Tile(2947, 3678), Tile(2923, 3673), Tile(2922, 3672), Tile(2917, 3672), Tile(2916, 3672)))
			walkToNormally = true
		
		// From objectinteraction.kt
		if (walkToNormally) {
			walkToNormally = it.player().walkTo(obj, PathQueue.StepType.REGULAR) // is this actually possible?
			it.player().write(ChangeMapMarker(it.player().pathQueue().peekLastTile()))
			val lastTile = it.player().pathQueue().peekLast()?.toTile() ?: it.player().tile()
			it.waitForTile(lastTile)
		}
		
		// Theres ones are not remote.
		if (walkToNormally) {
			// West
			if (obj.tile().equals(2916, 3672)) {
				player.teleport(player.tile().transform(4, 0, 0))
			}
			if (obj.tile().equals(2917, 3672)) {
				player.teleport(player.tile().transform(-4, 0, 0))
			}
			
			// Middle
			if (obj.tile().equals(2922, 3672)) {
				player.teleport(player.tile().transform(4, 1, 0))
			}
			if (obj.tile().equals(2923, 3673)) {
				player.teleport(player.tile().transform(-4, -1, 0))
			}
			
			// East to wildy
			if (obj.tile().equals(2947, 3678)) {
				it.messagebox("This shortcut leads into <col=FF0000>level 20 Wilderness.</col> Are you sure you want to enter?")
				if (it.options("Yes, enter the Wilderness.", "Heck no!") == 1) {
					player.teleport(2950, 3681)
				}
			}
		}
		// This one is remote.
		if (obj.tile().equals(2949, 3680)) {
			// It is actually 1 way according to the Wiki. Can't be used as a wilderness escape.
			if (it.player().walkTo(2950, 3681, PathQueue.StepType.REGULAR).equals(2950, 3681)) {
				it.waitForTile(Tile(2950, 3681))
				it.messagebox("You can't climb this hill. It's too steep!")
				/*it.player().lock()
				it.player().walkTo(it.player().tile().transform(-1, 0), PathQueue.StepType.FORCED_WALK)
				it.delay(1)
				it.player().teleport(Tile(2946, 3678))
				it.player().unlock()*/
			}
		}
	}
	
	// Trollhiem - GWD boulder enterance
	r.onObject(26415) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2898, 3716)) {
			if (player.tile().z == 3716) {
				player.teleport(player.tile().transform(0, 3, 0))
			}
			if (player.tile().z == 3719) {
				player.teleport(player.tile().transform(0, -3, 0))
			}
		}
	}
	
	// Trollhiem - GWD crevice enterence
	r.onObject(26382) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2900, 3713)) {
			player.teleport(2904, 3720)
		}
		if (obj.tile().equals(2904, 3719)) {
			player.teleport(2899, 3713)
		}
	}
	
	// Fremennick slayer dungeon
	r.onObject(16544) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2774, 10003)) {
			if (player.tile().x == 2775) {
				player.teleport(player.tile().transform(-2, 0, 0))
			} else if (player.tile().x == 2773) {
				player.teleport(player.tile().transform(2, 0, 0))
			}
		}
		
		if (obj.tile().equals(2769, 10002)) {
			if (player.tile().x == 2770) {
				player.teleport(player.tile().transform(-2, 0, 0))
			} else if (player.tile().x == 2768) {
				player.teleport(player.tile().transform(2, 0, 0))
			}
		}
	}
	
	// Fremennick slayer dungeon
	r.onObject(16539) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2734, 10008)) {
			if (player.tile().x == 2735) {
				player.teleport(player.tile().transform(-5, 0, 0))
			} else if (player.tile().x == 2730) {
				player.teleport(player.tile().transform(5, 0, 0))
			}
		}
	}
	
	// Temple east side of varrock shortcut
	r.onObject(16552) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(3424, 2476)) {
			if (player.tile().x == 3423) {
				player.teleport(player.tile().transform(1, 0, 0))
			} else if (player.tile().x == 3424) {
				player.teleport(player.tile().transform(-1, 0, 0))
			}
		}
	}
	
	// Temple east side of varrock shortcut
	r.onObject(16999) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(3426, 3478)) {
			player.teleport(3424, 3476)
		}
	}
	
	// Temple east side of varrock shortcut
	r.onObject(16998) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(3425, 3476)) {
			player.teleport(3426, 3478)
		}
	}
	
	// Taverly dungeon pipe
	r.onObject(16509) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (it.player().skills().level(Skills.AGILITY) < 70) {
			it.message("You need at least level 70 Agility to use this shortcut.")
		} else {
			if (obj.tile().equals(2887, 9799)) {
				player.teleport(2892, 9799)
			}
			if (obj.tile().equals(2890, 9799)) {
				player.teleport(2886, 9799)
			}
		}
	}
	
	// Taverly strange floor
	r.onObject(16510) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		
		if (obj.tile().equals(2879, 9813)) {
			if (player.tile().equals(2880, 9813)) {
				player.teleport(player.tile().transform(-2, 0, 0))
			} else if (player.tile().equals(2878, 9813)) {
				player.teleport(player.tile().transform(2, 0, 0))
			}
		}
	}
	
	// Black dragon taverly upstairs to down
	r.onObject(17384) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		if (obj.tile().equals(2842, 3424)) {
			Ladders.ladderUp(it, Tile(2842, 9825), true)
		} else if (obj.tile().equals(2562, 3356)) { // Chaos druid tower dungeon to down
			Ladders.ladderDown(it, Tile(2563, 9756), true)
		}
	}
	
	// Kalphite lair shortcut
	r.onObject(16465) @Suspendable {
		val player = it.player()
		val obj = it.player().attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
		if (obj.tile().equals(3501, 9510)) {
			player.teleport(3506, 9505, 2)
		}
		if (obj.tile().equals(3506, 9506)) {
			player.teleport(3500, 9510, 2)
		}
	}
}

@Suspendable fun edge_dungeon_monkeybars(it: Script) {
	val player = it.player()
	if (player.tile().z == 9963) {
		player.teleport(player.tile().transform(0, 7, 0))
	} else if (player.tile().z == 9970) {
		player.teleport(player.tile().transform(0, -7, 0))
	}
}