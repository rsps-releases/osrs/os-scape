package nl.bartpelle.veteres.content.areas.dungeons.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.wildernessWarning
import nl.bartpelle.veteres.model.Tile

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 2/24/2016.
 */

object TzhaarHurTel {
	
	val HUR_TEL = 2183
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(HUR_TEL) @Suspendable {
			it.chatNpc("Can I help you JalYt-Ket-${it.player().name()}?", HUR_TEL, 554)
			when (it.options("What did you call me?", "No I'm fine thanks.")) {
				1 -> {
					it.chatPlayer("What did you call me?", 614)
					it.chatNpc("Are you not JalYt-Ket?", HUR_TEL, 575)
					
					when (it.options("What's a 'JalYt-Ket'?", "I guess so...", "No I'm not!")) {
						1 -> {
							it.chatPlayer("What's a 'JalYt-Ket'?", 554)
							it.chatNpc("That what you are... you tough and strong no?", HUR_TEL, 575)
							it.chatPlayer("Well yes I suppose I am...", 575)
							it.chatNpc("Then you JalYt-Ket!", HUR_TEL, 567)
							
							when (it.options("What are you then?", "Thanks for explaining it.")) {
								1 -> {
									it.chatPlayer("What are you then?", 554)
									it.chatNpc("Silly JalYt, I am TzHaar-Hur, one of the crafters for<br>this city.", HUR_TEL, 568)
									it.chatNpc("There are the wise TzHaar-Mej who guide us, the<br>mighty TzHaar-Ket who guard us, and the swift<br>TzHaar-Xil who hunt for our food.", HUR_TEL, 590)
								}
								2 -> {
									it.chatPlayer("Thanks for explaining it.", 610)
								}
							}
						}
						2 -> {
							it.chatPlayer("I guess so...", 575)
							it.chatNpc("Well then, no problems.", HUR_TEL, 588)
						}
						3 -> {
							it.chatPlayer("No I'm not!", 614)
							it.chatNpc("What ever you say, crazy JalYt!", HUR_TEL, 592)
						}
					}
				}
				2 -> {
					it.chatPlayer("No I'm fine thanks.", 588)
				}
			}
		}
		
		r.onNpcOption2(HUR_TEL) {
			val npc = it.targetNpc()
			if (npc.world().realm().isPVP) {
				it.chatNpc("Sorry, JalYt, my shop is not open in this world.", HUR_TEL)
			} else {
				if (npc.spawnTile() == (Tile(2491, 5070))) {
					it.player().world().shop(51).display(it.player())
				} else {
					it.player().world().shop(30).display(it.player())
				}
			}
		}
	}
	
}