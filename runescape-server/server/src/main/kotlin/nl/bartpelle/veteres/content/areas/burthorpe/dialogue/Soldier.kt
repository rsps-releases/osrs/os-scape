package nl.bartpelle.veteres.content.areas.burthorpe.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/17/2016.
 */

object Soldier {
	
	val EATING_SOLDIERS = arrayListOf(4089, 4090, 4091)
	val ANIMATIONS = arrayListOf(422, 423, 425)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Soldiers training
		r.onNpcOption1(4086) { it.message("The soldier is busy training.") }
		r.onNpcOption1(4087) { it.message("The soldier is busy training.") }
		
		//Soldiers eating
		for (SOLDIERS in EATING_SOLDIERS) {
			r.onNpcOption1(SOLDIERS) { it.message("The soldier is busy eating.") }
			
			r.onNpcSpawn(SOLDIERS) @Suspendable {
				val npc: Npc = it.npc()
				fun eatFood() {
					npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
						s.onInterrupt { eatFood() }
						while (true) {
							s.delay(npc.world().random(5))
							npc.animate(1145)
						}
					})
				}
				it.onInterrupt { eatFood() }
				eatFood()
			}
		}
		
		//Soldiers punching the bag
		r.onNpcSpawn(4087) @Suspendable {
			val npc: Npc = it.npc()
			fun punchBag() {
				npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
					s.onInterrupt { punchBag() }
					while (true) {
						s.delay(npc.world().random(6..7))
						npc.animate(ANIMATIONS[npc.world().random(1)])
					}
				})
			}
			it.onInterrupt { punchBag() }
			punchBag()
		}
	}
}
