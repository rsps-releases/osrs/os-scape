package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands
import java.text.NumberFormat

/**
 * Created by Bart on 6/15/2016.
 */

object NewWizardEcoAndRealism {
	
	data class Teleport(val text: String, val x: Int = 0, val z: Int = 0, val height: Int = 0, val ID: Int = -1, val cost: Int = 0)
	
	var list: Array<Teleport> = buildList()
	var computedString: String = ""
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Compute the string to send
		computedString = list.map { it.text }.joinToString("|")
		list = buildList()
		
		for (wizard in intArrayOf(4398, 4399, 4400)) {
			r.onNpcOption1(wizard) @Suspendable { wizard_teleport(it) }
			
			r.onNpcOption2(wizard) @Suspendable {
				val teleport_ID = it.player().attribOr<Int>(AttributeKey.PREVIOUS_TELEPORT, -1)
				
				if (teleport_ID == -1) {
					it.messagebox("You haven't teleport anywhere yet.")
				} else {
					val opt = list.filter { it.ID == teleport_ID } [0]
					
					priceCheck(it, opt.x, opt.z, opt.height, false, opt.cost)
				}
			}
		}
	}
	
	fun buildList(): Array<Teleport> = arrayOf(
			Teleport("<col=ff>Please note:"),
			Teleport("<col=00><img=15> = Dangerous Teleport"),
			Teleport("<col=00><img=35> = Paid Teleport"),
			Teleport(""), Teleport(""), Teleport(""),
			Teleport(""), Teleport(""), Teleport(""),
			
			Teleport("<col=880000><u=000000>Monster Teleports"),
			Teleport("<col=004080>Basic Mobs"),
			Teleport("East Relleka Rock Crabs", 2706, 3718, ID = 1),
			Teleport("West Relleka Rock Crabs", 2668, 3712, ID = 2),
			Teleport("East Zeah Sand Crabs", 1812, 3471, ID = 3),
			Teleport("West Zeah Sand Crabs", 1629, 3472, ID = 4),
			Teleport("Yaks", 2322, 3794, ID = 5),
			Teleport("Goblin Village", 2956, 3501, ID = 6),
			Teleport("Falador Farm", 3033, 3285, ID = 7),
			Teleport("Experiments", 3561, 9947, ID = 8),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Popular Dungeons"),
			Teleport("Fremennik Slayer Dungeon", 2794, 3615, ID = 9),
			Teleport("Edgeville Dungeon", 3096, 9867, ID = 10),
			Teleport("Waterfall Dungeon", 2511, 3463, ID = 11),
			Teleport("Asgarnian Ice Dungeon", 3011, 3150, ID = 12),
			Teleport("Mos Le'Harmless Cave", 3748, 9373, ID = 13),
			/*Teleport("Ancient Cavern", 1741, 5316, 1, ID = 14),*/
			Teleport("Taverley Dungeon", 2884, 9798, ID = 15),
			Teleport("Smoke Dungeon", 3205, 9378, ID = 16),
			Teleport("God Wars Dungeon", 2914, 3735, ID = 17),
			Teleport("Tzhaar Cave", 2480, 5167, ID = 18),
			Teleport("Stronghold Slayer Cave", 2434, 3423, ID = 19),
			Teleport("Mourner Tunnels", 2026, 4636, ID = 20),
			Teleport("Monkey Madness II Dungeon", 2435, 3518, ID = 21),
			Teleport("Waterbirth Island", 2524, 3739, ID = 93),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Bosses"),
			Teleport("<img=35> Dagannoth Kings", 1910, 4367, ID = 22, cost = 12500),
			Teleport("<img=15> <img=35> Callisto", 3279, 3847, ID = 23, cost = 7500),
			Teleport("<img=15> <img=35> Venenatis", 3324, 3741, ID = 24, cost = 9500),
			Teleport("<img=15> <img=35> Chaos Fanatic", 2981, 3836, ID = 25, cost = 9000),
			Teleport("<img=15> <img=35> Crazy Archaeologist", 2968, 3696, ID = 26, cost = 6500),
			Teleport("<img=35> Corporeal Beast", 2968, 4383, 2, ID = 27, cost = 15000),
			Teleport("<img=35> Lizard Shaman", 1461, 3689, ID = 28, cost = 6500),
			Teleport("God Wars", 2914, 3735, ID = 29),
			Teleport("<img=15> <img=35> Scorpia", 3236, 3946, ID = 32, cost = 8000),
			Teleport("<img=35> Zulrah", 2200, 3055, ID = 33, cost = 20000),
			Teleport("<img=35> Kalphite Queen", 3508, 9493, ID = 82, cost = 4000),
			Teleport("<img=35> Cerberus", 1310, 1237, ID = 83, cost = 10000),
			
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=880000><u=000000>Minigame Teleports"),
			Teleport("Warriors' Guild", 2879, 3546, ID = 30),
			Teleport("Barrows", 3565, 3314, ID = 31),
			Teleport("Dueling Arena", 3315, 3235, ID = 84),
			Teleport("Pest Control", 2659, 2676, ID = 85),
			Teleport("Fight Caves", 2438, 5170, ID = 86),
			Teleport("Wintertodt", 1631, 3943, ID = 87),
			
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=880000><u=000000>Skilling Teleports"),
			Teleport("<col=004080>Agility Courses"),
			Teleport("Gnome Stronghold Agility Course", 2480, 3438, ID = 34),
			Teleport("Draynor Village Rooftop Course", 3106, 3279, ID = 35),
			Teleport("Al-Kharid Rooftop Course", 3274, 3198, ID = 36),
			Teleport("Varrock Rooftop Course", 3223, 3415, ID = 37),
			Teleport("Barbarian Outpost Agility Course", 2552, 3556, ID = 38),
			Teleport("Falador Rooftop Course", 3037, 3338, ID = 39),
			Teleport("<img=15> <img=35> Wilderness Course", 2997, 3907, ID = 40, cost = 7500),
			Teleport("Seers' Village Rooftop Course", 2729, 3485, ID = 41),
			Teleport("Relleka Rooftop Course", 2626, 3679, ID = 42),
			Teleport("Ardougne Rooftop Course", 2671, 3297, ID = 88),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Thieving Stalls"),
			Teleport("Varrock Tea", 3267, 3411, ID = 43),
			Teleport("Ardougne Stalls", 2664, 3306, ID = 44),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Pickpocketable NPCs"),
			Teleport("Lumbridge Men & Women", 3234, 3219, ID = 45),
			Teleport("Lumbridge Cow Farmer", 3195, 3289, ID = 46),
			Teleport("Draynor Master Farmer", 3078, 3250, ID = 47),
			Teleport("Varrock Guards", 3173, 3428, ID = 48),
			Teleport("Ardougne Paladins & Heroes", 2652, 3307, ID = 49),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Fishing & Cooking"),
			Teleport("Catherby Pier", 2835, 3435, ID = 50),
			Teleport("Draynor's Shoreline", 3088, 3230, ID = 51),
			Teleport("The Rogues' Den", 3046, 4970, 1, ID = 52),
			Teleport("The Cooking Guild", 3141, 3441, ID = 53),
			Teleport("The Fishing Guild", 2611, 3392, ID = 94),
			Teleport("Piscatoris' Coastline", 2334, 3696, ID = 54),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Mining & Smithing"),
			Teleport("The Dwarven Mines", 3016, 3446, ID = 55),
			Teleport("Varrock Anvils", 3187, 3426, ID = 56),
			Teleport("Lumbridge Furnace", 3224, 3254, ID = 57),
			Teleport("Varrock SW Rocks", 3183, 3371, ID = 58),
			Teleport("Varrock SE Rocks", 3285, 3371, ID = 59),
			Teleport("<img=15> <img=35> Wilderness Runite Rocks", 3066, 3881, ID = 60, cost = 8000),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Farming Patches"),
			Teleport("Camelot Allotment & Herbs", 2806, 3464, ID = 61),
			Teleport("Ardougne Allotment & Herbs", 2664, 3374, ID = 62),
			Teleport("Falador Allotment & Herbs", 3053, 3303, ID = 63),
			Teleport("Canifis Allotment & Herbs", 3600, 3523, ID = 64),
			Teleport("Zeah Allotment & Herbs", 1821, 3480, ID = 65),
			Teleport(""), Teleport(""), Teleport(""),
			
			Teleport("<col=004080>Runecrafting Altars"),
			Teleport("Cosmic Altar", 2410, 4377, ID = 66),
			Teleport("Astral Altar", 2156, 3863, ID = 67),
			Teleport("Nature Altar", 2868, 3017, ID = 68),
			Teleport("Law Altar", 2860, 3381, ID = 69),
			Teleport("Death Altar", 1862, 4639, ID = 70),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Slayer Masters"),
			Teleport("Turael - Burthorpe Master (<img=14> 3+)", 2928, 3535, ID = 71),
			Teleport("Mazchna - Canifis Master (<img=14> 20+)", 3508, 3506, ID = 72),
			Teleport("Vannaka - Edgeville Dungeon Master (<img=14> 40+)", 3145, 9914, ID = 73),
			Teleport("Chaeldar - Zanaris Master (<img=14> 70+)", 2449, 4440, ID = 74),
			Teleport("Nieve - Gnome Stronghold Master (<img=14> 85+)", 2434, 3421, ID = 75),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Woodcutting Areas"),
			Teleport("Draynor Willow Trees", 3090, 3237, ID = 76),
			Teleport("Camelot Maple Trees", 2726, 3503, ID = 77),
			Teleport("Edgeville Yew Trees", 3087, 3477, ID = 78),
			Teleport("Camelot Yew Tree", 2711, 3463, ID = 79),
			Teleport("Camelot Magic Trees", 2693, 3421, ID = 80),
			Teleport("Woodcutting Guild", 1662, 3507, ID = 81),
			Teleport("Hardwood Grove", 2819, 3083, ID = 82),
			
			Teleport(""), Teleport(""), Teleport(""),
			Teleport("<col=004080>Hunter spots"),
			Teleport("Piscatoris", 2336, 3584, ID = 89),
			Teleport("Feldip Hills (west)", 2534, 2916, ID = 90),
			Teleport("Feldip Hills (east)", 2595, 2911, ID = 91),
			Teleport("Al Kharid desert (east)", 3381, 3083, ID = 92)
	)
	
	@Suspendable fun wizard_teleport(it: Script) {
		val opt = OptionList.display(it, false, "Select a Location", *(list.map { it.text }.toTypedArray()))
		if (opt != -1) {
			val destination = list[opt]
			if (destination.x > 0) {
				priceCheck(it, destination.x, destination.z, destination.height, true, destination.cost)
				it.player().putattrib(AttributeKey.PREVIOUS_TELEPORT, destination.ID)
			} else {
				// when the player clicks an option on this suspendable displayed interface, it'll close it.
				// let's open it again for invalid clicks! :)
				wizard_teleport(it)
			}
			
		}
	}
	
	@Suspendable fun priceCheck(it: Script, x: Int, z: Int, level: Int, quick_cast: Boolean, cost: Int) {
		it.player.interfaces().closeMain()
		
		if (cost != 0) {
			if (it.optionsTitled("Pay ${NumberFormat.getInstance().format(cost)} gp to teleport?", "Pay.", "Never mind.") == 1) {
				if (it.player().inventory().remove(Item(995, cost), false).success()) {
					teleport(it, x, z, level, quick_cast, cost)
				} else if (it.player().bank().remove(Item(995, cost), false).success()) {
					teleport(it, x, z, level, quick_cast, cost)
				} else {
					it.itemBox("You don't have enough coins to teleport.", 1004)
				}
			}
		} else {
			teleport(it, x, z, level, quick_cast, cost)
		}
	}
	
	@Suspendable fun teleport(it: Script, x: Int, z: Int, level: Int, quick_cast: Boolean, cost: Int) {
		if (GameCommands.pkTeleportOk(it.player(), x, z)) {
			if (quick_cast) {
				it.player().lockNoDamage()
				it.delay(1)
				it.player().graphic(1296)
				it.player().animate(3865)
				it.delay(3)
				it.player().teleport(x, z, level)
				it.animate(-1)
				it.player().unlock()
			} else {
				it.player().lockNoDamage()
				it.targetNpc()!!.lock()
				it.targetNpc()!!.sync().faceEntity(it.player())
				it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
				it.targetNpc()!!.sync().animation(1818, 1)
				it.targetNpc()!!.sync().graphic(343, 100, 1)
				it.delay(1)
				it.targetNpc()!!.unlock()
				it.player().graphic(1296)
				it.player().animate(3865)
				it.delay(3)
				it.player().teleport(x, z, level)
				it.animate(-1)
				it.player().unlock()
			}
		}
	}
}