package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jak on 08/11/2016.
 */
object MagicCarpets {
	
	enum class CarpetPosition(val boardingTile: Tile, val faceBeforeFly: Tile, val walkOffDirection: Tile) {
		SHANTAY_PASS(Tile(3308, 3110), Tile(0, -1), Tile(0, 1)),
		UZER(Tile(3469, 3113), Tile(0, -1), Tile(-1, 0)),
		BEDABIN_CAMP(Tile(3180, 3045), Tile(0, -1), Tile(-1, 0)),
		POLLNIVNERACH_NORTH(Tile(3349, 3003), Tile(1, 0), Tile(0, 1)),
		POLLNIVNERACH_SOUTH(Tile(3351, 2942), Tile(0, -1), Tile(1, 0)),
		NARDAH(Tile(3401, 2916), Tile(-1, 0), Tile(0, -1)),
		MENAPOHS(Tile(3245, 2813), Tile(0, 1), Tile(-1, 0)),
		SOPHANEM(Tile(3285, 2813), Tile(0, 1), Tile(1, 0))
		;
	}
	
	enum class CarpetDestinations(val actual: CarpetPosition, val destinations: ArrayList<CarpetPosition>, val cost: Int, vararg val places: String) {
		AL_KHARID(CarpetPosition.SHANTAY_PASS, arrayListOf(CarpetPosition.UZER, CarpetPosition.BEDABIN_CAMP, CarpetPosition.POLLNIVNERACH_NORTH), 200, "Uzer", "Bedabin camp", "Pollnivneach"),
		UZER(CarpetPosition.UZER, arrayListOf(CarpetPosition.SHANTAY_PASS), 200, "Shantay Pass", "Cancel"),
		BEDABIN_CAMP(CarpetPosition.BEDABIN_CAMP, arrayListOf(CarpetPosition.SHANTAY_PASS), 200, "Shantay Pass", "Cancel"),
		POLLNIVNERACH_NORTH(CarpetPosition.POLLNIVNERACH_NORTH, arrayListOf(CarpetPosition.SHANTAY_PASS), 200, "Shantay Pass", "Cancel"),
		POLLNIVNERACH_SOUTH(CarpetPosition.POLLNIVNERACH_SOUTH, arrayListOf(CarpetPosition.NARDAH, CarpetPosition.SOPHANEM, CarpetPosition.MENAPOHS), 200, "Nardah", "Sophanem", "Menaphos", "Cancel"),
		NARDAH(CarpetPosition.NARDAH, arrayListOf(CarpetPosition.POLLNIVNERACH_SOUTH), 200, "Pollnivneach", "Cancel"),
		MENAPHOS(CarpetPosition.MENAPOHS, arrayListOf(CarpetPosition.SOPHANEM), 200, "Sophanem", "Cancel"),
		SOPHANEM(CarpetPosition.SOPHANEM, arrayListOf(CarpetPosition.POLLNIVNERACH_SOUTH, CarpetPosition.MENAPOHS), 200, "Pollnivneach", "Menaphos", "Cancel"),
		;
	}
	
	@JvmStatic @ScriptMain fun regiser(repo: ScriptRepository) {
		for (npcId in 17..22) {
			repo.onNpcOption2(npcId) {
				magicCarpet(it)
			}
		}
	}
	
	@Suspendable fun magicCarpet(it: Script) {
		val from = CarpetDestinations.values().toList().sortedBy { spot -> it.player().tile().distance(spot.actual.boardingTile) }[0]
		startFlying(it, from.actual, from, it.optionsTitled("Carpet rides costs ${from.cost} coins.", *from.places))
	}
	
	@Suspendable fun startFlying(it: Script, from: CarpetPosition, fromTargets: CarpetDestinations, targetId: Int) {
		val endName: String = fromTargets.places[targetId - 1]
		if (endName.equals("Cancel")) {
			return
		}
		if (it.player().inventory().count(995) < 200) {
			it.messagebox("Carpet rides cost 200 coins.")
			return
		}
		val end: Tile = fromTargets.destinations[targetId - 1].boardingTile
		it.player().debug("Travel target: %d %s %s %s", targetId, from, end, endName)
		if (it.player().inventory().remove(Item(995, 200), false).success()) {
			// Fly!
			it.targetNpc()!!.sync().shout("Enjoy the ride, ${it.player().name()}!")
			it.player().lockDamageOk()
			if (!it.player().tile().equals(from.boardingTile)) {
				it.player().walkTo(from.boardingTile, PathQueue.StepType.FORCED_WALK)
				while (!it.player().tile().equals(from.boardingTile)) {
					if (it.player().tile().nextTo(from.boardingTile)) {
						it.player().unclippedStep(from.boardingTile, PathQueue.StepType.FORCED_WALK)
						it.delay(1)
						break
					}
					it.delay(1)
				}
			}
			it.delay(1) // Start up
			it.player().faceTile(it.player().tile().transform(from.faceBeforeFly.x, from.faceBeforeFly.z))
			it.delay(1)
			it.player().animate(2266)
			it.delay(1)
			it.player().varps().varp(499, 1)
			it.player().looks().render(6936, 6936, 6936, 6936, 6936, 6936, 6936)
			it.player().animate(2262)
			var loops = 5
			while (loops-- > 0) {
				it.player().animate(2262)
				it.delay(1)
			}
			// TODO force walk travelling here .. force walk does the anims not player update mask
			it.player().teleport(end)
			it.delay(1)
			it.player().animate(2263) // end
			it.delay(1)
			it.player().animate(-1)
			it.player().varps().varp(499, 0)
			it.player().walkTo(end.transform(from.walkOffDirection.x, from.walkOffDirection.z), PathQueue.StepType.FORCED_WALK)
			it.player().looks().resetRender()
			it.player().unlock()
		}
	}
}