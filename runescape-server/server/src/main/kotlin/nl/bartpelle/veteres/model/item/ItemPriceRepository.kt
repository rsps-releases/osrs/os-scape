package nl.bartpelle.veteres.model.item

import co.paralleluniverse.strands.Strand
import com.google.gson.Gson
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.services.sql.PgSqlService
import nl.bartpelle.veteres.services.sql.SqlTransaction
import org.apache.logging.log4j.LogManager
import java.nio.file.Files
import java.nio.file.Paths
import java.sql.Connection

/**
 * Created by Jonathan on 4/21/2017.
 */
class ItemPriceRepository(private val service: PgSqlService, private val world: World?) {
	
	private val logger = LogManager.getLogger(ItemPriceRepository::class.java)
	
	private lateinit var baseBloodMoneyValues: Array<BloodMoneyPriceDef>
	private lateinit var PRICES: MutableMap<Int, Int>
	private lateinit var BLOOD_MONEY_PRICES: MutableMap<Int, Int>
	
	init {
		try {
			baseBloodMoneyValues = populateBaseBloodMoneyValues()
			PRICES = mutableMapOf()
			BLOOD_MONEY_PRICES = mutableMapOf()
			
			reload(true)
		} catch (t: Throwable) {
			t.printStackTrace()
		}
	}
	
	@JvmOverloads
	fun reload(pause: Boolean = false) {
		var loaded = false
		service.worker().submit(object : SqlTransaction() {
			override fun execute(connection: Connection) {
				val stamp = System.currentTimeMillis()
				val s = connection.prepareStatement("SELECT id, bm_value, exchange_price, exchange_price_w3 FROM item_definitions;")
				val set = s.executeQuery()
				while (set.next()) {
					val id = set.getInt("id")
					val price = if (world == null || world.realm().isRealism) set.getInt("exchange_price") else set.getInt("exchange_price_w3")
					val bmPrice = set.getInt("bm_value")
					
					PRICES.put(id, price)
					BLOOD_MONEY_PRICES.put(id, bmPrice)
				}
				logger.info("Loaded ${PRICES.size} regular prices and ${BLOOD_MONEY_PRICES.size} blood money prices in ${System.currentTimeMillis() - stamp}ms!")
				loaded = true
				connection.close()
			}
		})
		
		if (pause) while (!loaded) Strand.sleep(250) //Other stuff depends that this shit is FULLY loaded
	}
	
	private fun populateBaseBloodMoneyValues(): Array<BloodMoneyPriceDef> {
		val blob = Gson().fromJson(String(Files.readAllBytes(Paths.get("data", "list", "bloodmoney_base_values.json"))), Array<BloodMoneyPriceDef>::class.java)
		return (blob ?: emptyArray())
	}
	
	@JvmOverloads
	fun containsKey(id: Int, forceCoins: Boolean = false): Boolean = if ((world == null || world.realm().isPVP) && !forceCoins) BLOOD_MONEY_PRICES.containsKey(id) else PRICES.containsKey(id)
	
	@JvmOverloads operator fun get(id: Int, forceCoins: Boolean = false): Int = if ((world == null || world.realm().isPVP) && !forceCoins) BLOOD_MONEY_PRICES.getOrDefault(id, 0) else PRICES.getOrDefault(id, 1)
	
	@JvmOverloads
	fun set(id: Int, amount: Int, forceCoins: Boolean = false) = if ((world == null || world.realm().isPVP)&& !forceCoins) BLOOD_MONEY_PRICES.put(id, amount) else PRICES.put(id, amount)
	
	@JvmOverloads
	fun getOrElse(id: Int, defaultValue: Int, forceCoins: Boolean = false): Int = if ((world == null || world.realm().isPVP) && !forceCoins) BLOOD_MONEY_PRICES.getOrDefault(id, defaultValue) else PRICES.getOrDefault(id, defaultValue)
	
	fun bloodyMoney(id: Int): Int = BLOOD_MONEY_PRICES.getOrDefault(id, 0)
	
	fun coins(id: Int): Int = PRICES.getOrDefault(id, 1)
	
	data class BloodMoneyPriceDef(val id: Int, val value: Int, var persistent: Boolean = false)
}