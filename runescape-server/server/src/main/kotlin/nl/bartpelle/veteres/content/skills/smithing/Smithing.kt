package nl.bartpelle.veteres.content.skills.smithing

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.items.combine.DragonSquare
import nl.bartpelle.veteres.content.items.combine.SpiritShields
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 11/21/2015.
 */
object Smithing {
	
	val BRONZE_BAR = 2349
	val IRON_BAR = 2351
	val STEEL_BAR = 2353
	val MITHRIL_BAR = 2359
	val ADAMANT_BAR = 2361
	val RUNITE_BAR = 2363
	
	enum class Smithable(val group: Int, val result: Item, val itemamt: Int, val bar: Item, val baramt: Int, val child: Int, val level: Int, val xp: Double, val txt: String, val full: String) {
		//Bronze data
		BRONZE_DAGGER(1, Item(1205), 1, Item(BRONZE_BAR), 1, 2, 1, 12.5, "a dagger", "a Bronze Dagger"),
		BRONZE_SWORD(1, Item(1139), 1, Item(BRONZE_BAR), 1, 3, 4, 12.5, "a sword", "a Bronze Sword"),
		BRONZE_SCIMITAR(1, Item(1321), 1, Item(BRONZE_BAR), 2, 4, 3, 25.0, "a scimitar", "a Bronze Scimitar"),
		BRONZE_LONG_SWORD(1, Item(1291), 1, Item(BRONZE_BAR), 2, 5, 3, 25.0, "a long sword", "a Bronze Long Sword"),
		BRONZE_2H_SWORD(1, Item(1307), 1, Item(BRONZE_BAR), 3, 6, 3, 37.0, "a 2-hand sword", "a Bronze 2-Hand Sword"),
		BRONZE_AXE(1, Item(1351), 1, Item(BRONZE_BAR), 1, 7, 1, 12.5, "an axe", "a Bronze Axe"),
		BRONZE_MACE(1, Item(1422), 1, Item(BRONZE_BAR), 1, 8, 1, 12.5, "a mace", "a Bronze Mace"),
		BRONZE_WARHAMMER(1, Item(1337), 1, Item(BRONZE_BAR), 3, 9, 1, 37.5, "a warhammer", "a Bronze Warhammer"),
		BRONZE_BATTLEAXE(1, Item(1375), 1, Item(BRONZE_BAR), 3, 10, 2, 37.5, "a battleaxe", "a Bronze Battleaxe"),
		BRONZE_CHAINBODY(1, Item(1103), 1, Item(BRONZE_BAR), 3, 12, 3, 37.5, "a chainbody", "a Bronze Chainbody"),
		BRONZE_PLATELEGS(1, Item(1075), 1, Item(BRONZE_BAR), 3, 13, 3, 37.5, "platelegs", "Bronze Platelegs"),
		BRONZE_PLATE_SKIRT(1, Item(1087), 1, Item(BRONZE_BAR), 3, 14, 16, 37.5, "a plateskirt", "a Bronze Plateskirt"),
		BRONZE_PLATE_BODY(1, Item(1117), 1, Item(BRONZE_BAR), 5, 15, 18, 62.5, "a platebody", "a Bronze Platebody"),
		BRONZE_NAILS(1, Item(4819), 15, Item(BRONZE_BAR), 1, 16, 4, 12.5, "15 bronze nails", "15 Bronze Nails"),
		BRONZE_MEDIUM_HELM(1, Item(1139), 1, Item(BRONZE_BAR), 1, 17, 3, 12.5, "a medium helm", "a Bronze Medium Helm"),
		BRONZE_FULL_HELM(1, Item(1155), 1, Item(BRONZE_BAR), 2, 18, 7, 25.0, "a full helm", "a Bronze Full Helm"),
		BRONZE_SQUARE_SHIELD(1, Item(1173), 1, Item(BRONZE_BAR), 2, 19, 8, 12.5, "a square shield", "a Bronze Square Shield"),
		BRONZE_KITE_SHIELD(1, Item(1189), 1, Item(BRONZE_BAR), 3, 20, 12, 12.5, "a kiteshield", "a Bronze Kiteshield"),
		BRONZE_DART_TIPS(1, Item(819), 10, Item(BRONZE_BAR), 1, 23, 4, 12.5, "10 dart tips", "10 Bronze Dart Tips"),
		BRONZE_ARROWTIPS(1, Item(39), 15, Item(BRONZE_BAR), 1, 24, 5, 12.5, "15 arrow tips", "15 Bronze Arrow Tips"),
		BRONZE_THROWING_KNIVES(1, Item(864), 5, Item(BRONZE_BAR), 1, 25, 7, 12.5, "5 throwing knives", "5 Bronze Throwing Knives"),
		BRONZE_WIRE(1, Item(1794), 1, Item(BRONZE_BAR), 1, 26, 4, 12.5, "a wire", "a Bronze Wire"),
		BRONZE_BOLTS(1, Item(9375), 10, Item(BRONZE_BAR), 1, 28, 3, 12.5, "10 bolts", "10 Bronze Bolts"),
		BRONZE_LIMBS(1, Item(9420), 1, Item(BRONZE_BAR), 1, 29, 6, 12.5, "limbs", "Bronze Limbs"),
		BRONZE_JAVELINS(1, Item(19570), 15, Item(BRONZE_BAR), 1, 30, 6, 12.5, "five javelin heads", "bronze javelin heads"),
		
		//Iron data
		IRON_DAGGER(2, Item(1203), 1, Item(IRON_BAR), 1, 2, 15, 25.0, "a dagger", "an Iron Dagger"),
		IRON_SWORD(2, Item(1279), 1, Item(IRON_BAR), 1, 3, 19, 25.0, "a sword", "an Iron Sword"),
		IRON_SCIMITAR(2, Item(1323), 1, Item(IRON_BAR), 2, 4, 20, 50.0, "a scimitar", "an Iron Scimitar"),
		IRON_LONG_SWORD(2, Item(1293), 1, Item(IRON_BAR), 2, 5, 21, 50.0, "a long sword", "an Iron Long Sword"),
		IRON_2H_SWORD(2, Item(1309), 1, Item(IRON_BAR), 3, 6, 29, 75.0, "a 2-hand sword", "an Iron 2-Hand Sword"),
		IRON_AXE(2, Item(1349), 1, Item(IRON_BAR), 1, 7, 16, 25.0, "an axe", "an Iron Axe"),
		IRON_MACE(2, Item(1420), 1, Item(IRON_BAR), 1, 8, 17, 25.0, "a mace", "an Iron Mace"),
		IRON_WARHAMMER(2, Item(1335), 1, Item(IRON_BAR), 3, 9, 24, 75.0, "a warhammer", "an Iron Warhammer"),
		IRON_BATTLEAXE(2, Item(1363), 1, Item(IRON_BAR), 3, 10, 25, 75.0, "a battleaxe", "an Iron Battleaxe"),
		IRON_CHAINBODY(2, Item(1101), 1, Item(IRON_BAR), 3, 12, 26, 75.0, "a chainbody", "an Iron Chainbody"),
		IRON_PLATELEGS(2, Item(1067), 1, Item(IRON_BAR), 3, 13, 31, 75.0, "platelegs", "Iron Platelegs"),
		IRON_PLATE_SKIRT(2, Item(1081), 1, Item(IRON_BAR), 3, 14, 31, 75.0, "a plateskirt", "an Iron Plateskirt"),
		IRON_PLATE_BODY(2, Item(1115), 1, Item(IRON_BAR), 5, 15, 33, 125.0, "a platebody", "an Iron Platebody"),
		IRON_NAILS(2, Item(4820), 15, Item(IRON_BAR), 1, 16, 19, 25.0, "15 nails", "15 Iron Nails"),
		IRON_MEDIUM_HELM(2, Item(1137), 1, Item(IRON_BAR), 1, 17, 18, 25.0, "a medium helm", "an Iron Medium Helm"),
		IRON_FULL_HELM(2, Item(1153), 1, Item(IRON_BAR), 2, 18, 22, 50.0, "a full helm", "an Iron Full Helm"),
		IRON_SQUARE_SHIELD(2, Item(1175), 1, Item(IRON_BAR), 2, 19, 23, 50.0, "a square shield", "an Iron Square Shield"),
		IRON_KITE_SHIELD(2, Item(1191), 1, Item(IRON_BAR), 3, 20, 27, 75.0, "a kiteshield", "an Iron Kiteshield"),
		OIL_LANTERN_FRAME(2, Item(4540), 1, Item(IRON_BAR), 1, 21, 26, 25.0, "an oil lantern frame", "an Oil Lantern Frame"),
		IRON_DART_TIPS(2, Item(820), 10, Item(IRON_BAR), 1, 23, 19, 25.0, "10 dart tips", "10 Iron Dart Tips"),
		IRON_ARROWTIPS(2, Item(40), 15, Item(IRON_BAR), 1, 24, 20, 25.0, "15 arrow tips", "15 Iron Arrow Tips"),
		IRON_THROWING_KNIVES(2, Item(863), 5, Item(IRON_BAR), 1, 25, 22, 25.0, "5 throwing knives", "5 Iron Throwing Knives"),
		IRON_SPIT(2, Item(7225), 1, Item(IRON_BAR), 1, 26, 17, 25.0, "a iron spit", "an Iron Spit"),
		IRON_BOLTS(2, Item(9377), 10, Item(IRON_BAR), 1, 28, 18, 25.0, "10 bolts", "10 Iron Bolts"),
		IRON_LIMBS(2, Item(9423), 1, Item(IRON_BAR), 1, 29, 23, 25.0, "limbs", "Iron Limbs"),
		IRON_JAVELINS(2, Item(19572), 15, Item(IRON_BAR), 1, 30, 21, 25.0, "five javelin heads", "iron javelin heads"),
		
		//Steel data
		STEEL_DAGGER(3, Item(1207), 1, Item(STEEL_BAR), 1, 2, 30, 37.5, "a dagger", "a Steel Dagger"),
		STEEL_SWORD(3, Item(1281), 1, Item(STEEL_BAR), 1, 3, 34, 37.5, "a sword", "a Steel Sword"),
		STEEL_SCIMITAR(3, Item(1325), 1, Item(STEEL_BAR), 2, 4, 35, 75.0, "a scimitar", "a Steel Scimitar"),
		STEEL_LONG_SWORD(3, Item(1295), 1, Item(STEEL_BAR), 2, 5, 36, 75.0, "a long sword", "a Steel Long Sword"),
		STEEL_2H_SWORD(3, Item(1311), 1, Item(STEEL_BAR), 3, 6, 44, 112.5, "a 2-hand sword", "a Steel 2-Hand Sword"),
		STEEL_AXE(3, Item(1353), 1, Item(STEEL_BAR), 1, 7, 31, 37.5, "an axe", "a Steel Axe"),
		STEEL_MACE(3, Item(1424), 1, Item(STEEL_BAR), 1, 8, 32, 37.5, "a mace", "a Steel Mace"),
		STEEL_WARHAMMER(3, Item(1339), 1, Item(STEEL_BAR), 3, 9, 39, 112.5, "a warhammer", "a Steel Warhammer"),
		STEEL_BATTLEAXE(3, Item(1365), 1, Item(STEEL_BAR), 3, 10, 40, 112.5, "a battle axe", "a Steel Battleaxe"),
		STEEL_CHAINBODY(3, Item(1105), 1, Item(STEEL_BAR), 3, 12, 41, 112.5, "a chainbody", "a Steel Chainbody"),
		STEEL_PLATELEGS(3, Item(1069), 1, Item(STEEL_BAR), 3, 13, 46, 112.5, "platelegs", "Steel Platelegs"),
		STEEL_PLATE_SKIRT(3, Item(1083), 1, Item(STEEL_BAR), 3, 14, 46, 112.5, "a plateskirt", "a Steel Plateskirt"),
		STEEL_PLATE_BODY(3, Item(1119), 1, Item(STEEL_BAR), 5, 15, 48, 187.5, "a platebody", "a Steel Platebody"),
		STEEL_NAILS(3, Item(1539), 15, Item(STEEL_BAR), 1, 16, 34, 37.5, "15 nails", "15 Steel Nails"),
		STEEL_MEDIUM_HELM(3, Item(1141), 1, Item(STEEL_BAR), 1, 17, 33, 37.5, "a medium helm", "a Steel Medium Helm"),
		STEEL_FULL_HELM(3, Item(1157), 1, Item(STEEL_BAR), 2, 18, 37, 75.0, "a full helm", "a Steel Full Helm"),
		STEEL_SQUARE_SHIELD(3, Item(1177), 1, Item(STEEL_BAR), 2, 19, 38, 75.0, "a square shield", "a Steel Square Shield"),
		STEEL_KITE_SHIELD(3, Item(1193), 1, Item(STEEL_BAR), 3, 20, 42, 112.5, "a kiteshield", "a Steel Kiteshield"),
		BULLSEYE_LANTERN(3, Item(4544), 1, Item(STEEL_BAR), 1, 21, 49, 37.5, "a bullseye lantern", "a Bullseye Lantern"),
		STEEL_DART_TIPS(3, Item(821), 10, Item(STEEL_BAR), 1, 23, 37, 37.5, "10 dart tips", "10 Steel Dart Tips"),
		STEEL_ARROWTIPS(3, Item(41), 15, Item(STEEL_BAR), 1, 24, 35, 37.5, "15 arrow tips", "15 Steel Arrow Tips"),
		STEEL_THROWING_KNIVES(3, Item(865), 5, Item(STEEL_BAR), 1, 25, 37, 37.5, "5 throwing knives", "5 Steel Throwing Knives"),
		STUDS(3, Item(2370), 1, Item(STEEL_BAR), 1, 26, 36, 37.5, "Steel studs", "Steel Studs"),
		STEEL_BOLTS(3, Item(9378), 10, Item(STEEL_BAR), 1, 28, 33, 37.5, "10 bolts", "10 Steel Bolts"),
		STEEL_LIMBS(3, Item(9425), 1, Item(STEEL_BAR), 1, 29, 36, 37.5, "limbs", "Steel Limbs"),
		STEEL_JAVELINS(3, Item(19574), 15, Item(STEEL_BAR), 1, 30, 36, 37.5, "five javelin heads", "steel javelin heads"),
		
		//Mithril data
		MITHRIL_DAGGER(4, Item(1209), 1, Item(MITHRIL_BAR), 1, 2, 50, 50.0, "a dagger", "a Mithril Dagger"),
		MITHRIL_SWORD(4, Item(1285), 1, Item(MITHRIL_BAR), 1, 3, 54, 50.0, "a sword", "a Mithril Sword"),
		MITHRIL_SCIMITAR(4, Item(1329), 1, Item(MITHRIL_BAR), 2, 4, 55, 100.0, "a scimitar", "a Mithril Scimitar"),
		MITHRIL_LONG_SWORD(4, Item(1299), 1, Item(MITHRIL_BAR), 2, 5, 56, 100.0, "a long sword", "a Mithril Long Sword"),
		MITHRIL_2H_SWORD(4, Item(1315), 1, Item(MITHRIL_BAR), 3, 6, 64, 150.0, "a 2-hand sword", "a Mithril 2-Hand Sword"),
		MITHRIL_AXE(4, Item(1355), 1, Item(MITHRIL_BAR), 1, 7, 51, 50.0, "an axe", "a Mithril Axe"),
		MITHRIL_MACE(4, Item(1428), 1, Item(MITHRIL_BAR), 1, 8, 52, 50.0, "a mace", "a Mithril Mace"),
		MITHRIL_WARHAMMER(4, Item(1343), 1, Item(MITHRIL_BAR), 3, 9, 59, 150.0, "a warhammer", "a Mithril Warhammer"),
		MITHRIL_BATTLEAXE(4, Item(1369), 1, Item(MITHRIL_BAR), 3, 10, 60, 150.0, "a battle axe", "a Mithril Battleaxe"),
		MITHRIL_CHAINBODY(4, Item(1109), 1, Item(MITHRIL_BAR), 3, 12, 61, 150.0, "a chainbody", "a Mithril Chainbody"),
		MITHRIL_PLATELEGS(4, Item(1071), 1, Item(MITHRIL_BAR), 3, 13, 66, 150.0, "platelegs", "Mithril Platelegs"),
		MITHRIL_PLATE_SKIRT(4, Item(1085), 1, Item(MITHRIL_BAR), 3, 14, 66, 150.0, "a plateskirt", "a Mithril Plateskirt"),
		MITHRIL_PLATE_BODY(4, Item(1121), 1, Item(MITHRIL_BAR), 5, 15, 68, 250.0, "a platebody", "a Mithril Platebody"),
		MITHRIL_NAILS(4, Item(4822), 15, Item(MITHRIL_BAR), 1, 16, 54, 50.0, "15 nails", "15 Mithril Nails"),
		MITHRIL_MEDIUM_HELM(4, Item(1143), 1, Item(MITHRIL_BAR), 1, 17, 53, 50.0, "a medium helm", "a Mithril Medium Helm"),
		MITHRIL_FULL_HELM(4, Item(1159), 1, Item(MITHRIL_BAR), 2, 18, 57, 100.0, "a full helm", "a Mithril Full Helm"),
		MITHRIL_SQUARE_SHIELD(4, Item(1181), 1, Item(MITHRIL_BAR), 2, 19, 58, 100.0, "a square shield", "a Mithril Square Shield"),
		MITHRIL_KITE_SHIELD(4, Item(1197), 1, Item(MITHRIL_BAR), 3, 20, 62, 150.0, "a kiteshield", "a Mithril Kiteshield"),
		MITHRIL_DART_TIPS(4, Item(822), 10, Item(MITHRIL_BAR), 1, 23, 54, 50.0, "10 dart tips", "10 Mithril Dart Tips"),
		MITHRIL_ARROWTIPS(4, Item(42), 15, Item(MITHRIL_BAR), 1, 24, 55, 50.0, "15 arrow tips", "15 Mithril Arrow Tips"),
		MITHRIL_THROWING_KNIVES(4, Item(866), 5, Item(MITHRIL_BAR), 1, 25, 57, 50.0, "5 throwing knives", "5 Mithril Throwing Knives"),
		MITHRIL_BOLTS(4, Item(9379), 10, Item(MITHRIL_BAR), 1, 28, 53, 50.0, "10 bolts", "10 Mithril Bolts"),
		MITHRIL_LIMBS(4, Item(9427), 1, Item(MITHRIL_BAR), 1, 29, 56, 50.0, "limbs", "Mithril Limbs"),
		MITHRIL_GRAPPLE_TIP(4, Item(9416), 1, Item(MITHRIL_BAR), 1, 26, 59, 50.0, "grapple tip", "Mithril Grapple Tip"),
		MITHRIL_JAVELINS(4, Item(19576), 15, Item(MITHRIL_BAR), 1, 30, 56, 50.0, "five javelin heads", "mithril javelin heads"),
		
		//Adamant data
		ADAMANT_DAGGER(5, Item(1211), 1, Item(ADAMANT_BAR), 1, 2, 70, 62.5, "a dagger", "an Adamant Dagger"),
		ADAMANT_SWORD(5, Item(1287), 1, Item(ADAMANT_BAR), 1, 3, 74, 62.5, "a sword", "an Adamant Sword"),
		ADAMANT_SCIMITAR(5, Item(1331), 1, Item(ADAMANT_BAR), 2, 4, 75, 125.0, "a scimitar", "an Adamant Scimitar"),
		ADAMANT_LONG_SWORD(5, Item(1301), 1, Item(ADAMANT_BAR), 2, 5, 76, 125.0, "a long sword", "an Adamant Long Sword"),
		ADAMANT_2H_SWORD(5, Item(1317), 1, Item(ADAMANT_BAR), 3, 6, 84, 162.5, "a 2-hand sword", "an Adamant 2-Hand Sword"),
		ADAMANT_AXE(5, Item(1357), 1, Item(ADAMANT_BAR), 1, 7, 71, 62.5, "an axe", "an Adamant Axe"),
		ADAMANT_MACE(5, Item(1430), 1, Item(ADAMANT_BAR), 1, 8, 72, 62.5, "a mace", "an Adamant Mace"),
		ADAMANT_WARHAMMER(5, Item(1345), 1, Item(ADAMANT_BAR), 3, 9, 79, 187.5, "a warhammer", "an Adamant Warhammer"),
		ADAMANT_BATTLEAXE(5, Item(1371), 1, Item(ADAMANT_BAR), 3, 10, 80, 187.5, "a battle axe", "an Adamant Battleaxe"),
		ADAMANT_CHAINBODY(5, Item(1111), 1, Item(ADAMANT_BAR), 3, 12, 81, 187.5, "a chainbody", "an Adamant Chainbody"),
		ADAMANT_PLATELEGS(5, Item(1073), 1, Item(ADAMANT_BAR), 3, 13, 86, 187.5, "platelegs", "Adamant Platelegs"),
		ADAMANT_PLATE_SKIRT(5, Item(1091), 1, Item(ADAMANT_BAR), 3, 14, 86, 187.5, "a plateskirt", "an Adamant Plateskirt"),
		ADAMANT_PLATE_BODY(5, Item(1123), 1, Item(ADAMANT_BAR), 5, 15, 88, 312.5, "a platebody", "an Adamant Platebody"),
		ADAMANT_NAILS(5, Item(4823), 15, Item(ADAMANT_BAR), 1, 16, 74, 62.5, "15 nails", "15 Adamant Nails"),
		ADAMANT_MEDIUM_HELM(5, Item(1145), 1, Item(ADAMANT_BAR), 1, 17, 73, 62.5, "a medium helm", "an Adamant Medium Helm"),
		ADAMANT_FULL_HELM(5, Item(1161), 1, Item(ADAMANT_BAR), 2, 18, 77, 125.0, "a full helm", "an Adamant Full Helm"),
		ADAMANT_SQUARE_SHIELD(5, Item(1183), 1, Item(ADAMANT_BAR), 2, 19, 78, 125.0, "a square shield", "an Adamant Square Shield"),
		ADAMANT_KITE_SHIELD(5, Item(1199), 1, Item(ADAMANT_BAR), 3, 20, 82, 187.5, "a kiteshield", "an Adamant Kiteshield"),
		ADAMANT_DART_TIPS(5, Item(823), 10, Item(ADAMANT_BAR), 1, 23, 74, 62.5, "10 dart tips", "10 Adamant Dart Tips"),
		ADAMANT_ARROWTIPS(5, Item(43), 15, Item(ADAMANT_BAR), 1, 24, 75, 62.5, "15 arrow tips", "15 Adamant Arrow Tips"),
		ADAMANT_THROWING_KNIVES(5, Item(867), 5, Item(ADAMANT_BAR), 1, 25, 77, 62.5, "5 throwing knives", "5 Adamant Throwing Knives"),
		ADAMANT_BOLTS(5, Item(9380), 10, Item(ADAMANT_BAR), 1, 28, 73, 62.5, "10 bolts", "10 Adamant Bolts"),
		ADAMANT_LIMBS(5, Item(9429), 1, Item(ADAMANT_BAR), 1, 29, 76, 62.5, "limbs", "Adamant Limbs"),
		ADAMANT_JAVELINS(5, Item(19578), 15, Item(ADAMANT_BAR), 1, 30, 76, 62.5, "five javelin heads", "adamant javelin heads"),
		
		//Runite data
		RUNITE_DAGGER(6, Item(1213), 1, Item(RUNITE_BAR), 1, 2, 85, 75.0, "a dagger", "a Runite Dagger"),
		RUNITE_SWORD(6, Item(1289), 1, Item(RUNITE_BAR), 1, 3, 89, 75.0, "a sword", "a Runite Sword"),
		RUNITE_SCIMITAR(6, Item(1333), 1, Item(RUNITE_BAR), 2, 4, 90, 150.0, "a scimitar", "a Runite Scimitar"),
		RUNITE_LONG_SWORD(6, Item(1303), 1, Item(RUNITE_BAR), 2, 5, 91, 150.0, "a long sword", "a Runite Long Sword"),
		RUNITE_2H_SWORD(6, Item(1319), 1, Item(RUNITE_BAR), 3, 6, 99, 162.5, "a 2-hand sword", "a Runite 2-Hand Sword"),
		RUNITE_AXE(6, Item(1359), 1, Item(RUNITE_BAR), 1, 7, 86, 75.0, "an axe", "a Runite Axe"),
		RUNITE_MACE(6, Item(1432), 1, Item(RUNITE_BAR), 1, 8, 87, 75.0, "a mace", "a Runite Mace"),
		RUNITE_WARHAMMER(6, Item(1347), 1, Item(RUNITE_BAR), 3, 9, 94, 225.0, "a warhammer", "a Runite Warhammer"),
		RUNITE_BATTLEAXE(6, Item(1373), 1, Item(RUNITE_BAR), 3, 10, 95, 225.0, "a battle axe", "a Runite Battleaxe"),
		RUNITE_CHAINBODY(6, Item(1113), 1, Item(RUNITE_BAR), 3, 12, 96, 225.0, "a chainbody", "a Runite Chainbody"),
		RUNITE_PLATELEGS(6, Item(1079), 1, Item(RUNITE_BAR), 3, 13, 99, 225.0, "platelegs", "Runite Platelegs"),
		RUNITE_PLATE_SKIRT(6, Item(1093), 1, Item(RUNITE_BAR), 3, 14, 99, 225.0, "a plateskirt", "a Runite Plateskirt"),
		RUNITE_PLATE_BODY(6, Item(1127), 1, Item(RUNITE_BAR), 5, 15, 99, 375.0, "a platebody", "a Runite Platebody"),
		RUNITE_NAILS(6, Item(4824), 15, Item(RUNITE_BAR), 1, 16, 89, 75.0, "15 nails", "15 Runite Nails"),
		RUNITE_MEDIUM_HELM(6, Item(1147), 1, Item(RUNITE_BAR), 1, 17, 88, 75.0, "a medium helm", "a Runite Medium Helm"),
		RUNITE_FULL_HELM(6, Item(1163), 1, Item(RUNITE_BAR), 2, 18, 92, 150.0, "a full helm", "a Runite Full Helm"),
		RUNITE_SQUARE_SHIELD(6, Item(1185), 1, Item(RUNITE_BAR), 2, 19, 93, 150.0, "a square shield", "a Runite Square Shield"),
		RUNITE_KITE_SHIELD(6, Item(1201), 1, Item(RUNITE_BAR), 3, 20, 97, 225.0, "a kiteshield", "a Runite Kiteshield"),
		RUNITE_DART_TIPS(6, Item(824), 10, Item(RUNITE_BAR), 1, 23, 89, 75.0, "10 dart tips", "10 Runite Dart Tips"),
		RUNITE_ARROWTIPS(6, Item(44), 15, Item(RUNITE_BAR), 1, 24, 90, 75.0, "15 arrow tips", "15 Runite Arrow Tips"),
		RUNITE_THROWING_KNIVES(6, Item(868), 5, Item(RUNITE_BAR), 1, 25, 92, 75.0, "5 throwing knives", "5 Runite Throwing Knives"),
		RUNITE_BOLTS(6, Item(9381), 10, Item(RUNITE_BAR), 1, 28, 88, 75.0, "10 bolts", "10 Runite Bolts"),
		RUNITE_LIMBS(6, Item(9431), 1, Item(RUNITE_BAR), 1, 29, 91, 75.0, "limbs", "Runite Limbs"),
		RUNITE_JAVELINS(6, Item(19580), 15, Item(RUNITE_BAR), 1, 30, 91, 75.0, "five javelin heads", "rune javelin heads");
		
		companion object {
			fun get(group: Int, child: Int): Smithable? {
				Smithable.values().forEach {
					if (it.group == group && it.child == child)
						return it
				}
				return null
			}
		}
	}
	
	fun metalName(bar: Smithable): String {
		return when (bar.group) {
			1 -> "bronze"
			2 -> "iron"
			3 -> "steel"
			4 -> "mithril"
			5 -> "adamantite"
			6 -> "runite"
			else -> "something"
		}
	}
	
	@Suspendable fun smith(s: Script, bar: Smithable, amt: Int) {
		s.player().interfaces().closeMain()
		
		if (s.player().skills().level(Skills.SMITHING) < bar.level) {
			s.messagebox("You need a Smithing level of ${bar.level} to make ${bar.full}.")
			return
		}
		
		var left = amt
		while (left-- > 0) {
			if (s.player().inventory().count(bar.bar.id()) < bar.baramt) {
				s.messagebox("You don't have enough ${metalName(bar)} bars to make any more.")
				return
			}
			
			s.player().animate(898)
			s.delay(4)
			
			var barAmount = bar.baramt
			if (barAmount > 1 && BonusContent.isActive(s.player, BlessingGroup.GOLDSMITH) && s.player.world().rollDie(2, 1)) {
				barAmount--
			}
			
			if (s.player().inventory().remove(Item(bar.bar, barAmount), false).success()) {
				s.player().inventory().addOrDrop(Item(bar.result, bar.itemamt), s.player)
				
				if (barAmount != bar.baramt) {
					s.message("You hammer the ${metalName(bar)} and make ${bar.txt}, conserving a bar in the process.")
				} else {
					s.message("You hammer the ${metalName(bar)} and make ${bar.txt}.")
				}
				
				s.addXp(Skills.SMITHING, bar.xp)
			} else {
				return // Just in case.
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Register all the children, we only need to do it for one group since the group is resolved when the player actually smiths
		Smithable.values().filter { bar -> bar.group == 1 }.forEach { bar ->
			r.onButton(312, bar.child) @Suspendable {
				// Resolve the one we need
				val smithable = Smithable.get(it.player().varps().varbit(Varbit.SMITHING_BAR_TYPE), bar.child)
				if (smithable != null) {
					// Grab num
					when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
						1 -> smith(it, smithable, 1)
						2 -> smith(it, smithable, 5)
						3 -> smith(it, smithable, 10)
						4 -> smith(it, smithable, it.inputInteger("Enter amount:"))
						10 -> {
							it.player().message(it.player().world().examineRepository().item(bar.result.id()))
						}
					}
				}
			}
		}
		
		// Anvil object
		r.onItemOnObject(2097, s@ @Suspendable {
			/*if (it.player().world().realm().isPVP) {
				it.messagebox("That's a bit pointless is it not...?")
				return@s
			}*/
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			
			// Spirit shields are handled elsewhere
			if (item == 12819 || item == 12823 || item == 12827) {
				SpiritShields.onAnvil(it)
				return@s
			}
			
			if (2347 !in it.player().inventory()) {
				it.messagebox("You need a hammer to work the metal with.")
				return@s
			}
			if (item == 2366 || item == 2368) {
				DragonSquare.on_anvil(it)
				return@s
			}
			
			
			val type = when (item) {
				BRONZE_BAR -> 1
				IRON_BAR -> 2
				STEEL_BAR -> 3
				MITHRIL_BAR -> 4
				ADAMANT_BAR -> 5
				RUNITE_BAR -> 6
				else -> 0
			}
			
			it.player().varps().varbit(Varbit.SMITHING_BAR_TYPE, type)
			
			if (type != 0) {
				it.player().interfaces().sendMain(312)
			} else {
				it.message("Nothing interesting happens.")
			}
		})
	}
	
	
}