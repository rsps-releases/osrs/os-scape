package nl.bartpelle.veteres.content.achievements

import com.google.common.base.Joiner
import com.google.common.base.Splitter
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*
import java.util.stream.Stream

/**
 * Created by Mack on 11/20/2017.
 */
object AchievementAction {
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onLogin {
			init(it.player())
		}
	}

	/**
	 * Processes a single achievement task. This is for more case-specific achievements such as interacting with an object or
	 * npc etc.
	 */
	fun process(player: Player, achievement: Achievement?) {
		if (!AchievementDiary.ENABLED) return
		val task = achievement ?: return
		if (task.completed(player)) return

		incrementCountFor(player, task)
	}
	
	/**
	 * This method is intended to be used to update/check all achievements under a category, but some achievements
	 * require more specific implementation so we default to another method.
	 * @see #processAchievement
	 */
	fun processCategoryAchievement(player: Player, category: AchievementCategory, vararg triggers: Int) {
		if (!AchievementDiary.ENABLED) return
		val container = AchievementDiary.diaryHandler(player).achievementsByKey(category)
		val scanTriggers = triggers.isNotEmpty()
		container.filter({task -> !task.completed(player)}).forEach({ task ->
			if (scanTriggers && task.hasTriggers()) {
				val process = Arrays.stream(triggers).anyMatch { trigger -> task.triggers().contains(trigger)}
				
				if (process) process(player, task)
			} else {
				process(player, task)
			}
		})
	}

    /**
     * Creates any data the player may need regarding achievements.
     */
	private fun init(player: Player) {
		if (AchievementDiary.ENABLED) {
			player.putattrib(AttributeKey.ACHIEVEMENT_PROGRESSION_MAP, Splitter.on(",").trimResults().omitEmptyStrings().withKeyValueSeparator(":").split(player.attribOr(AttributeKey.ACHIEVEMENT_TASK_TRACKER, "")).toMutableMap())
		}
	}

    /**
     * If all preconditions are met, we increase the achievement progression counter for the player.
     */
	fun incrementCountFor(player: Player, task: Achievement, amount: Int = 1) {
		val tracker = AchievementDiary.cachedAchievements(player)
		var updateRequired = false
		val meetsRequirement = task.meetsRequirement(player)
		var completed = false

        if (meetsRequirement && !AchievementDiary.contains(player, task)) {
			player.message("<col=804080>You have made progression towards a new achievement diary task. View the quest tab for more info.")
			tracker.put(task.id(), amount.toString())
			updateRequired = true
			
			if (task.completed(player)) {
				completed = true
			}
		} else if (meetsRequirement) {
			var progression = tracker.getValue(task.id()).toInt()
			
			progression += amount
			tracker.put(task.id(), progression.toString())
			updateRequired = true
			
			if (task.completed(player)) {
				completed = true
			}
		}
		if (completed) {
			if (task.completed(player)) {
				val rewardAmount = when(task.rawId()) {
                    in 11..23 -> player.world().random(IntRange(2000, 5000))
                    in 24..39 -> player.world().random(IntRange(3000, 10_000))
                    else -> player.world().random(IntRange(250, 1000))
                }

				player.message("<col=804080>Congratulations! You have completed an achievement diary task. View the quest tab for more info.</col>")
				if (!player.inventory().addOrDrop(Item(13307, rewardAmount), player).success()) {
					player.message("<col=804080>Your reward was dropped on the floor due to no available inventory space.</col>")
				}
			}
		}
		if (updateRequired) {
			AchievementDiary.recacheAchievements(player, tracker)
		}
	}
}