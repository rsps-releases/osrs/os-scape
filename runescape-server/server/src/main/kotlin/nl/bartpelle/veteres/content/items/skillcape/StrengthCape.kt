package nl.bartpelle.veteres.content.items.skillcape

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Jason MacKeigan on 2016-07-04.
 */
object StrengthCape : CapeOfCompletionPerk(intArrayOf(1, 2)) {
	
	val TILE_LOCATION = Tile(2877, 3546)
	
	override fun option(option: Int): Function1<Script, Unit> = @Suspendable {
		val player = it.player()
		
		when (option) {
			1 -> {
				CapeOfCompletion.boost(Skills.STRENGTH, player)
			}
			2 -> {
				if (Teleports.canTeleport(player, true, TeleportType.GENERIC)) {
					Teleports.basicTeleport(it, TILE_LOCATION)
				}
			}
		}
	}
}