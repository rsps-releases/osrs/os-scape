package nl.bartpelle.veteres.content.npcs.dragons

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.items.equipment.DragonfireShield
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */
object AdamantDragon {

    @JvmField val script: Function1<Script, Unit> = script@ @Suspendable {
        val dragon = it.npc()
        var target = EntityCombat.getTarget(dragon) ?: return@script

        while (EntityCombat.targetOk(dragon, target) && dragon.hp() > 0 && PlayerCombat.canAttack(dragon, target)) {
            if (EntityCombat.canAttackDistant(dragon, target, true, 8) && EntityCombat.attackTimerReady(dragon)) {
                val tileDist = dragon.tile().distance(target.tile())
                val delay = Math.max(1, (20 + (tileDist) * 12) / 30)
                val rand = dragon.world().random(4)

                when (rand) {
                    1 -> doDragonBreath(it, target, tileDist, delay)
                    2 -> doRangedAttack(it, target, tileDist, delay)
                    3 -> doMagicBlast(it, target, tileDist, delay)
                    else -> {
                        if (EntityCombat.canAttackMelee(dragon, target, false)) {
                            doMelee(dragon, target)
                        } else {
                            when(dragon.world().random(3)) {
                                1 -> doDragonBreath(it, target, tileDist, delay)
                                2 -> doRangedAttack(it, target, tileDist, delay)
                                3 -> doMagicBlast(it, target, tileDist, delay)
                            }
                        }
                    }
                }
                EntityCombat.cooldownForNpc(dragon, target)
            }

            EntityCombat.postHitLogic(dragon)
            target = EntityCombat.refreshTarget(it) ?: break
            it.delay(1)
        }
    }

    @Suspendable private fun doMagicBlast(it: Script, target: Entity, tileDist: Int, delay: Int) {
        val npc = it.npc()

        npc.animate(81)
        it.delay(1)

        npc.world().spawnProjectile(npc, target, 165, 40, 36, 10 * tileDist, 25, 20, 255)

        if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
            target.hit(npc, npc.world().random(26), delay).combatStyle(CombatStyle.MAGIC)
        } else {
            target.hit(npc, 0, delay).combatStyle(CombatStyle.MAGIC)
        }
    }

    @Suspendable private fun doRangedAttack(it: Script, target: Entity, tileDist: Int, delay: Int) {
        val npc = it.npc()
        val targetTile = target.tile()

        npc.animate(81)
        it.delay(1)

        npc.world().spawnProjectile(npc.tile().transform(1, 1), targetTile, 1486, 40, 36, 10 * tileDist, 25, 20, 0)

        it.delay(delay + 1)

        if (target.tile() == targetTile) {
            target.hit(npc, 8, 0, Hit.Type.POISON)
        }

        npc.world().tileGraphic(1487, targetTile, 0, 0)
        npc.world().spawnProjectile(targetTile, targetTile.transform(1, 0), 1486, 40, 36, 10 * tileDist, 25, 20, 255)
        it.delay(1)

        if (target.tile() == targetTile) {
            target.hit(npc, 8, 0, Hit.Type.POISON).combatStyle(CombatStyle.RANGE)
        }

        it.delay(1)

        if (inBlastTile(target, targetTile.area(1))) {
            target.hit(npc, 4, 0, Hit.Type.POISON).combatStyle(CombatStyle.RANGE)
        }

        it.delay(1)
        npc.world().tileGraphic(1487, targetTile.transform(1, 0), 0, 0)
        it.delay(1)

        if (inBlastTile(target, targetTile.area(1))) {
            target.hit(npc, 4, 0, Hit.Type.POISON).combatStyle(CombatStyle.RANGE)
        }
    }

    @Suspendable private fun doDragonBreath(it: Script, target: Entity, tileDist: Int, delay: Int) {
        val npc = it.npc()
        var max = 50.0

        npc.animate(81)
        it.delay(1)

        npc.world().spawnProjectile(npc, target, 54, 40, 36, 10 * tileDist, 46, 20, 255)

        if (target is Player) {
            if (DragonfireShield.getTyped(target) != null || target.equipment().hasAt(EquipSlot.SHIELD, 1540)) {
                max *= 0.30
                target.message("Your shield absorbs most of the dragon fire!")
            }
            if (target.attribOr<Int>(AttributeKey.ANTIFIRE_POTION, 0) > 0) {
                max *= 0.30
                target.message("Your potion protects you from the heat of the dragon's breath!")
            }
            if (target.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1) {
                max *= 0.60
                target.message("Your prayer absorbs most of the dragon's breath!")
            }
        }

        if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
            target.hit(npc, target.world().random(50), delay).combatStyle(CombatStyle.MAGIC)
        } else {
            target.hit(npc, 0, delay).combatStyle(CombatStyle.MAGIC)
        }

        if (target is Player && DragonfireShield.canCharge(target)) {
            DragonfireShield.charge(target)
        }
    }

    @Suspendable private fun doMelee(npc: Npc, target: Entity) {
        npc.animate(npc.attackAnimation())

        if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
            target.hit(npc, target.world().random(npc.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
        } else {
            target.hit(npc, 0, 1).combatStyle(CombatStyle.MELEE)
        }
    }

    private fun inBlastTile(target: Entity, area: Area): Boolean {
        return (target.tile().inArea(area))
    }
}