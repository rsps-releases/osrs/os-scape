package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.npcs.bosses.wilderness.WildernessEventController
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * @author Mack
 */
object BurningAmulet {

    /**
     * The collection of burning amulet ids.
     */
    private val BURNING_AMULETS = arrayListOf(21166, 21169, 21171, 21173, 21175)

    /**
     * The collection of teleport tile targets.
     */
    private val TELEPORT_TILES = arrayListOf(
            Tile(3039, 3691, 0),
            Tile(3232, 3637, 0),
            Tile(3027, 3842, 0)
            )

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        for (amulet in BURNING_AMULETS) {
            sr.onItemOption4(amulet, @Suspendable {
                teleportMenu(it, false)
            })
            sr.onEquipmentOption(1, amulet, @Suspendable {
                teleport(it, TELEPORT_TILES[1], true)
            })
            sr.onEquipmentOption(2, amulet, @Suspendable {
                teleport(it, TELEPORT_TILES[0], true)
            })
            sr.onEquipmentOption(3, amulet, @Suspendable {
                teleport(it, TELEPORT_TILES[2], true)
            })
        }
    }

    /**
     * Handles the displaying of teleport options.
     */
    @Suspendable private fun teleportMenu(it: Script, equipment: Boolean) {
        var options = arrayOf("Chaos Temple<col=FF0000> (Level 15 Wild)</col>", "Bandit Camp<col=FF0000> (Level 17 Wilderness)</col>", "Lava Maze<col=FF0000> (Level 41 Wild)</col>")
        val eventActive = WildernessEventController.activeEvent != WildernessEventController.BossEvent.NOTHING
        val wildEventNpc = if (WildernessEventController.activeNpc.isPresent) WildernessEventController.activeNpc.get() else null
        val wildEventTile = if (eventActive && Objects.nonNull(wildEventNpc)) wildEventNpc?.tile() else null
        val wildEventLevel = if (wildEventTile != null) WildernessLevelIndicator.wildernessLevel(wildEventTile).toString() else "N/A"

        if (eventActive) {
            options = arrayOf(
                    "Chaos Temple<col=FF0000> (Level 15 Wild)</col>",
                    "Bandit Camp<col=FF0000> (Level 17 Wild)</col>",
                    "Lava Maze<col=FF0000> (Level 41 Wild)</col>",
                    WildernessEventController.activeEvent.desc +"<col=FF0000> (Level $wildEventLevel Wild)</col>")
        }
        when (it.options(*options)) {
            1 -> {
                teleport(it, TELEPORT_TILES[1], equipment)
            }
            2 -> {
                teleport(it, TELEPORT_TILES[0], equipment)
            }
            3 -> {
                teleport(it, TELEPORT_TILES[2], equipment)
            }
            4 -> {
                if (!eventActive || Objects.isNull(wildEventNpc) || !WildernessEventController.activeNpc.isPresent) {
                    it.message("The Wilderness Event has ended. Wait for another to commence!")
                    return
                }

                if (wildEventTile != null) {
                    teleport(it, wildEventTile, equipment)
                }
            }
        }
    }

    /**
     * Executes the teleport action.
     */
    @Suspendable private fun teleport(it: Script, teleportTile: Tile, equipment: Boolean) {
        if (!Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
            return
        }
        val slot: Int = it.player()[AttributeKey.ITEM_SLOT]
        var amulet = if (equipment) it.player().equipment()[EquipSlot.AMULET] else it.player().inventory()[slot]

        Teleports.basicTeleport(it, teleportTile)

        amulet = degrade(amulet)
        if (equipment) it.player().equipment().replace(EquipSlot.AMULET, if (amulet == null) -1 else amulet.id()) else it.player().inventory().replace(slot, if (amulet == null) -1 else amulet.id())
        it.player().message("<col=7F00FF>Your amulet has ${chargeCount(amulet)} remaining.</col>")

        if (amulet == null) {
            it.player().message("<col=7F00FF>It crumbles into dust...</col>")
        }
    }

    /**
     * Degrades the item by morphing the object to the new id.
     */
    private fun degrade(amulet: Item): Item? {
        return when {
            amulet.id() != 21175 -> {
                val mod = if (amulet.id() == 21166) 3 else 2
                Item(amulet.id() + mod, 1)
            }
            else -> null
        }
    }

    /**
     * Supplies the charge count based on which amulet id the
     * player is using.
     */
    private fun chargeCount(amulet: Item?): String {
        return when (amulet?.id()) {
            21166 -> "five charges"
            21169 -> "four charges"
            21171 -> "three charges"
            21173 -> "two charges"
            21175 -> "one charge"
            else -> "no charges"
        }
    }
}