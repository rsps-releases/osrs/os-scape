package nl.bartpelle.veteres.content.npcs.pestcontrol

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Jason MacKeigan on 2016-08-31 at 5:48 PM
 */
object Defiler {
	
	@JvmField val combatScript: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		val world = npc.world()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10) && EntityCombat.attackTimerReady(npc)) {
				if (EntityCombat.canAttackMelee(npc, target, false) && npc.world().rollDie(2, 1)) {
					melee(npc, target)
				} else {
					range(npc, target)
				}
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun melee(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, EntityCombat.randomHit(npc))
		else
			target.hit(npc, 0)
	}
	
	fun range(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
		
		npc.animate(npc.attackAnimation())
		npc.world().spawnProjectile(npc, target, 657, 50, 30, 50, 12 * tileDist, 14, 5)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE))
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.RANGE)
		else
			target.hit(npc, 0, delay.toInt())
	}
	
}