package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 01/05/2016.
 */
object CommandTeleportPrompt {
	
	@JvmStatic @Suspendable fun prompt(player: Player, x: Int, z: Int, level: Int) {
		if (!WildernessLevelIndicator.inWilderness(player.tile())) {
			player.executeScript @Suspendable {
				// Graves or Demonic gorillas
				/*if ((x == 3160 && z == 3676) || (x == 3107 && z == 3675)) {
					it.message("<col=FF0000>Warning!</col> The Zamorak Mage at 5 wilderness can no longer be used as an escape if teleblocked.")
					it.messagebox("<col=FF0000>Warning!</col> The Zamorak Mage at 5 wilderness can no longer be used as an escape if teleblocked.")
				}*/
				if (wantsToEnterWild(it)) {
					player.interfaces().closeMain() // Could have bank open and take food out when you enter wild
					player.teleport(x, z, level)
					player.timers().cancel(TimerKey.FROZEN)
					player.timers().cancel(TimerKey.REFREEZE)
					player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
				}
			}
		}
	}
	
	@JvmStatic @Suspendable fun wantsToEnterWild(it: Script): Boolean {
		return it.optionsTitled("Enter the Wilderness?", "Yes, into the <col=FF0000>Wilderness</col>!", "No.") == 1
	}
}