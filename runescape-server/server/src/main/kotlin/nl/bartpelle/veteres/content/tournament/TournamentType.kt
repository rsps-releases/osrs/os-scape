package nl.bartpelle.veteres.content.tournament

enum class TournamentType {
    PVP,
    DMM,
    LMS
}