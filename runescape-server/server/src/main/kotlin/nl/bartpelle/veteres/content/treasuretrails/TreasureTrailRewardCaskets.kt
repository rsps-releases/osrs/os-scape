package nl.bartpelle.veteres.content.treasuretrails

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.treasuretrails.rewards.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository
import java.text.NumberFormat

/**
 * Created by Situations on 2016-11-05.
 */

object TreasureTrailRewardCaskets {
	
	val EASY_CASKET = 20546
	val MEDIUM_CASKET = 20545
	val HARD_CASKET = 20544
	val ELITE_CASKET = 20543
	val MASTER_CASKET = 19836
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(EASY_CASKET) { openRewardCasket(it, TreasureTrailClueLevel.EASY, AttributeKey.EASY_CLUE_SCROLL, "easy", Item(EASY_CASKET)) }
		r.onItemOption1(MEDIUM_CASKET) { openRewardCasket(it, TreasureTrailClueLevel.MEDIUM, AttributeKey.MEDIUM_CLUE_SCROLL, "medium", Item(MEDIUM_CASKET)) }
		r.onItemOption1(HARD_CASKET) { openRewardCasket(it, TreasureTrailClueLevel.HARD, AttributeKey.HARD_CLUE_SCROLL, "hard", Item(HARD_CASKET)) }
		r.onItemOption1(ELITE_CASKET) { openRewardCasket(it, TreasureTrailClueLevel.ELITE, AttributeKey.ELITE_CLUE_SCROLL, "elite", Item(ELITE_CASKET)) }
		r.onItemOption1(MASTER_CASKET) { openRewardCasket(it, TreasureTrailClueLevel.MASTER, AttributeKey.MASTER_CLUE_SCROLL, "master", Item(MASTER_CASKET)) }
	}
	
	@JvmStatic fun openRewardCasket(it: Script, rewardLevel: TreasureTrailClueLevel, clueScrollAttribute: AttributeKey, clueScrollType: String, casket: Item) {
		val player = it.player()
		val clueScrollsCompleted = player.attribOr<Int>(clueScrollAttribute, 0)
		val sOrNo = if (clueScrollsCompleted >= 1) "s" else ""
		
		if (player.inventory().remove(Item(casket), false).success()) {
			//Lock the player
			player.lock()
			
			// Generate the clue scroll reward
			when (rewardLevel) {
				TreasureTrailClueLevel.EASY -> EasyRewards.generateEasyReward(player, 1)
				TreasureTrailClueLevel.MEDIUM -> MediumRewards.generateMediumReward(player, 1)
				TreasureTrailClueLevel.HARD -> HardRewards.generateHardReward(player, 1)
				TreasureTrailClueLevel.ELITE -> EliteRewards.generateEliteReward(player, 1)
				TreasureTrailClueLevel.MASTER -> MasterRewards.generateMasterReward(player, 1)
			}
			
			//If it's a master clue, roll for a pet
			if (rewardLevel == TreasureTrailClueLevel.MASTER) {
				if (player.world().rollDie(2500, 1))
					unlockBloodhound(player)
			}
			
			// Send our player some achievement messages
			player.message("Well done, you've completed the Treasure Trail!")
			player.message("<col=3300ff>You have completed ${clueScrollsCompleted + 1} $clueScrollType Treasure Trail$sOrNo.</col>")
			player.message("<col=3300ff>Your treasure is worth around ${NumberFormat.getInstance().format(clueScrollValue(player))}!</col>")
			
			//Add the reward to the players inventory, or drop it on the ground
			player.clueScrollReward().forEach { item ->
				if (item != null) {
					
					if (player.inventory().add(Item(item.id(), item.amount()), false).failed()) {
						val groundItem = GroundItem(player.world(), Item(item.id(), item.amount()), player.tile(), player.id())
						player.world().spawnGroundItem(groundItem)
					}
				}
			}
			
			//Display the interface
			player.interfaces().sendMain(364)
			player.write(SetItems(112, 364, 1, player.clueScrollReward()))
			player.putattrib(clueScrollAttribute, clueScrollsCompleted + 1)
			
			//Clear the clue scroll reward and unlock the player
			player.clearattrib(AttributeKey.CLUE_SCROLL_REWARD)
			player.unlock()
		}
	}
	
	// Calculate what the clue scroll's reward value is
	private fun clueScrollValue(player: Player): Int {
		var rewardTotal = 0
		
		player.clueScrollReward().forEach { reward ->
			if (reward != null) {
				rewardTotal += reward.realPrice(player.world()) * reward.amount()
			}
		}
		
		return rewardTotal
	}
	
	// Obtain the ItemContainer with our reward
	fun Player.clueScrollReward(): ItemContainer {
		val offer = attribOr<ItemContainer>(AttributeKey.CLUE_SCROLL_REWARD, null)
		if (offer != null)
			return offer
		
		val container = ItemContainer(world(), 28, ItemContainer.Type.FULL_STACKING)
		putattrib(AttributeKey.CLUE_SCROLL_REWARD, container)
		return container
	}
	
	fun unlockBloodhound(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.BLOODHOUND)) {
			player.varps().varbit(Pet.BLOODHOUND.varbit, 1)
			
			val currentPet = player.pet()
			
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.BLOODHOUND, false)
			} else {
				if (player.inventory().add(Item(Pet.BLOODHOUND.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.BLOODHOUND.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
	fun unlockOlmlet(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.OLMLET)) {
			player.varps().varbit(Pet.OLMLET.varbit, 1)
			
			val currentPet = player.pet()
			
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.OLMLET, false)
			} else {
				if (player.inventory().add(Item(Pet.OLMLET.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.OLMLET.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
}
