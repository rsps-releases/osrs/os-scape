package nl.bartpelle.veteres.content.skills.farming

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 2016-06-15.
 */

object CompostBin {
	
	val FALADOR_BIN = 7836
	val WEEDS = 6055
	val BUCKET = 1925
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnObject(FALADOR_BIN, s@ @Suspendable {
			val item = it.itemUsed()
			var amt = it.player().inventory().count(WEEDS)
			
			if (item.id() == WEEDS) {
				if (it.player().timers().has(TimerKey.COMPOST_BIN)) {
					return@s
				}
				
				if (compostBinFull(it)) {
					it.messagebox("The compost bin is too full to put anything else in it.")
					return@s
				}
				
				if (it.player().inventory().count(WEEDS) - it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS) > 15) {
					amt = 15 - it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS)
				}
				
				it.player().lock()
				val removed = it.player().inventory().remove(Item(WEEDS, amt), true).completed()
				if (removed > 0) {
					it.player().animate(832)
					it.player().message("You fill the compost bin with weeds.")
					it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS, it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS) + removed)
					it.delay(1)
				}
				it.player().unlock()
			} else if (item.id() == BUCKET) {
				if (!readyToCollect(it)) {
					it.message("Nothing interesting happens.")
					return@s
				}
				
			} else {
				it.player().message("Nothing interesting happens.")
			}
		})
		
		r.onObject(FALADOR_BIN) @Suspendable {
			val option = it.interactionOption()
			var can_collect = it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS)
			val compost_to_collect = it.player().attribOr<Int>(AttributeKey.COMPOST_BIN, 0)
			
			if (option == 1) {
				if (compostBinFull(it)) {
					if (compost_to_collect == 0) {
						if (!it.player().timers().has(TimerKey.COMPOST_BIN) && can_collect == 0) {
							it.player().lock()
							it.player().animate(810)
							Farming.setTimer(it.player(), TimerKey.COMPOST_BIN, 20)
							it.player().putattrib(AttributeKey.COMPOST_BIN, 15)
							it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_TYPE, 17)
							it.player().unlock()
						} else if (compost_to_collect == 15) {
							it.player().message("gundrilla#1")
						}
					} else {
						it.messagebox("The vegetation hasn't finished rotting yet.")
					}
				}
			}
			if (option == 5) {
				if (it.optionsTitled("Dump the entire contents of the bin?", "Yes, throw it all away.", "No, keep it.") == 1) {
					it.player().lock()
					it.player().animate(832)
					it.delay(1)
					it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS, 0)
					it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_TYPE, 0)
					it.player().putattrib(AttributeKey.COMPOST_BIN, 0)
					it.player().unlock()
				}
			}
		}
		
		r.onTimer(TimerKey.COMPOST_BIN) @Suspendable {
			it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS, 14)
			it.message("The compost has finished.")
		}
	}
	
	@Suspendable fun compostBinFull(it: Script): Boolean {
		if (it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS) == 15)
			return true else return false
	}
	
	@Suspendable fun readyToCollect(it: Script): Boolean {
		if (it.player().varps().varbit(Varbit.FALADOR_COMPOST_BIN_FULLNESS) == 21)
			return true else return false
	}
}