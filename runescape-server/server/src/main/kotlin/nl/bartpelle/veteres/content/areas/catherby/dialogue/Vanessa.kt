package nl.bartpelle.veteres.content.areas.catherby.dialogue

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/4/2016.
 */

object Vanessa {
	
	val VANESSA = 502
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption2(VANESSA) { it.player().world().shop(41).display(it.player()) }
	}
	
}