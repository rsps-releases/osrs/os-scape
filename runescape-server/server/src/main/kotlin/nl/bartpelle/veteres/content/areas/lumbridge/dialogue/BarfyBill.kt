package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/20/2016.
 */

object BarfyBill {
	
	val BARFY_BILL = 1326
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(BARFY_BILL) @Suspendable {
			it.chatPlayer("Hello there.", 588)
			it.chatNpc("Oh! Hello there.", BARFY_BILL)
			when (it.options("Who are you?", "Can you teach me about Canoeing?")) {
				1 -> who_are_you(it)
			}
		}
	}
	
	@Suspendable fun who_are_you(it: Script) {
		it.chatPlayer("Who are you?", 588)
		it.chatNpc("My name is Ex Sea Captain Barfy Bill.", BARFY_BILL, 588)
		it.chatPlayer("Ex sea captain?", 554)
		it.chatNpc("Yeah, I bought a lovely ship and was planning to make<br>a fortune running her as a merchant vessel.", BARFY_BILL, 611)
		it.chatPlayer("Why are you not still sailing?", 554)
		it.chatNpc("Chronic sea sickness.  My first, and only, voyage was<br>spent dry heaving over the rails.", BARFY_BILL, 611)
		it.chatNpc("If I had known about the sea sickness I could have<br>saved myself a lot of money.", BARFY_BILL, 589)
		it.chatPlayer("What are you up to now then?", 575)
		it.chatNpc("Well my ship had a little fire related problem.<br>Fortunately it was well insured.", BARFY_BILL, 593)
		it.chatNpc("Anyway, I don't have to work anymore so I've taken to<br>canoeing on the river.", BARFY_BILL, 589)
		it.chatNpc("I don't get river sick!", BARFY_BILL, 567)
		it.chatNpc("Would you like to know how to make a canoe?", BARFY_BILL, 554)
		when (it.options("Yes", "No")) {
			1 -> teach_me(it)
			2 -> it.chatPlayer("No thanks, not right now.", 588)
		}
	}
	
	@Suspendable fun teach_me(it: Script) {
		it.chatPlayer("Could you teach me about canoes?", 554)
		
		//Does our player have the required skills?
		if (it.player().skills().level(Skills.WOODCUTTING) < 12) {
			it.chatNpc("Well, you don't look like you have the skill to make a<br>canoe.", BARFY_BILL, 589)
			it.chatNpc("You need to have at least level 12 woodcutting.", BARFY_BILL, 588)
			it.chatNpc("Once you are able to make a canoe it makes travel<br>along the river much quicker!", BARFY_BILL, 589)
		} else {
			it.chatNpc("It's really quite simple. Just walk down to that tree on<br>the bank and chop it down.", BARFY_BILL, 589)
			it.chatNpc("When you have done that you can shape the log<br>further with your axe to make a canoe.", BARFY_BILL, 589)
			it.chatNpc("Be careful if<br>you travel into the Wilderness though. I've heard tell of great evil in that blasted wasteland.", BARFY_BILL, 589)
			it.chatPlayer("Thanks for the warning Bill.", 588)
		}
	}
}
