package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Jason MacKeigan on 2016-09-28 at 7:34 PM
 */
class InterfaceIteration(player: Player, start: Int, iterations: Int) {
	
	val script: Function1<Script, Unit> = suspend@ @Suspendable {
		var interfacesToDraw = start
		
		while (interfacesToDraw++ < start + iterations) {
			player.message("Drawing interface #$interfacesToDraw.")
			player.interfaces().sendMain(interfacesToDraw)
			it.delay(2)
		}
	}
}