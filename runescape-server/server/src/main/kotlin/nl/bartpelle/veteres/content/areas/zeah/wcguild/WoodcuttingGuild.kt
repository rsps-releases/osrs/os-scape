package nl.bartpelle.veteres.content.areas.zeah.wcguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.skills.woodcutting.Woodcutting
import nl.bartpelle.veteres.content.skills.woodcutting.Woodcutting.Woodcutting.Hatchet
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/9/2016.
 */
object WoodcuttingGuild {
	
	val AREA_EAST = Area(1608, 3487, 1655, 3517)
	val AREA_WEST = Area(1564, 3477, 1600, 3503)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Guild entrances
		r.onObject(28851) @Suspendable { enterGuild(it) }
		r.onObject(28852) @Suspendable { enterGuild(it) }
		
		// Rope ladders up
		r.onObject(28857) @Suspendable {
			it.player().lock()
			it.delay(1)
			it.player().animate(828)
			it.delay(1)
			val obj = it.interactionObject()
			if (it.player().tile().x >= 1571) { // East side
				it.player().teleport(obj.tile().transform(-1, 0, 1))
			} else { // West side <gang symbol here>
				it.player().teleport(obj.tile().transform(1, 0, 1))
			}
			it.player().unlock()
		}
		
		// Rope ladders down
		r.onObject(28858) @Suspendable {
			it.player().lock()
			it.delay(1)
			it.player().animate(827)
			it.delay(1)
			val obj = it.interactionObject()
			if (it.player().tile().x >= 1571) { // East side
				it.player().teleport(obj.tile().transform(1, 0, -1))
			} else { // West side <gang symbol here>
				it.player().teleport(obj.tile().transform(-1, 0, -1))
			}
			it.player().unlock()
		}
		
		// Dungeon exit
		r.onObject(28856) @Suspendable {
			it.player().lock()
			it.delay(1)
			it.animate(828)
			it.delay(1)
			it.player().teleport(1606, 3508)
			it.player().unlock()
		}
		
		// Dungeon entrance
		r.onObject(28855) {
			it.player().teleport(1596, 9900)
		}
		
		// Ent log cutting
		r.onNpcOption1(6595) @Suspendable {
			val trunk = it.targetNpc()!!
			
			it.delay(1)
			
			val axe = Woodcutting.Woodcutting.findAxe(it.player())
			
			if (axe == null) {
				it.player.message("You do not have an axe which you have the Woodcutting level to use.")
			} else {
				while (!trunk.finished()) {
					it.player().animate(cutAnimationFor(axe))
					it.delay(3)
					
					// 35% chance per 3 ticks to cut a log from the trunk
					if (it.player.world().random(100) <=
							Woodcutting.Woodcutting.chance(it.player.skills()[Skills.WOODCUTTING],
									Woodcutting.Woodcutting.Tree.ENTTRUNK, axe)) {
						it.addXp(Skills.WOODCUTTING, 25.0)
						it.player().inventory().add(Item(getEntLog(it.player)), true)
					}
				}
			}
		}
	}
	
	fun cutAnimationFor(axe: Hatchet): Int = when (axe) {
		Hatchet.INFERNAL -> 2116
		Hatchet.DRAGON -> 3292
		Hatchet.RUNE -> 3285
		Hatchet.ADAMANT -> 3286
		Hatchet.MITHRIL -> 3287
		Hatchet.BLACK -> 3288
		Hatchet.STEEL -> 3289
		Hatchet.IRON -> 3290
		Hatchet.BRONZE -> 3291
		else -> 3285
	}
	
	fun getEntLog(player: Player): Int {
		if (player.skills().level(Skills.WOODCUTTING) >= 75 && player.world().rollDie(10, 4)) {
			return 1514;
		} else if (player.skills().level(Skills.WOODCUTTING) > 60 && player.world().rollDie(10, 5)) { // NOT a typo, 61 gives you yews.
			return 1516;
		} else if (player.skills().level(Skills.WOODCUTTING) >= 40 && player.world().rollDie(10, 4)) {
			return 1518;
		} else if (player.world().rollDie(10, 5)) {
			return 1520;
		}
		
		return 1522 // Oak
	}
	
	@Suspendable fun enterGuild(it: Script) {
		if (it.player().skills().xpLevel(Skills.WOODCUTTING) < 60) {
			it.messagebox("You need a Woodcutting level of 60 to access this guild.")
		} else {
			it.player().lock()
			
			it.player().world().removeObj(MapObj(Tile(1657, 3505), 28851, 0, 2), false)
			it.player().world().removeObj(MapObj(Tile(1657, 3504), 28852, 0, 2), false)
			it.player().world().spawnObj(MapObj(Tile(1658, 3505), 28853, 0, 1), false)
			it.player().world().spawnObj(MapObj(Tile(1658, 3504), 28854, 0, 3), false)
			
			if (it.player().tile().x >= 1658) {
				it.player().pathQueue().step(1657, it.player.tile().z)
			} else {
				it.player().pathQueue().step(1658, it.player.tile().z)
			}
			it.delay(2)
			
			it.player().world().spawnObj(MapObj(Tile(1657, 3505), 28851, 0, 2), false)
			it.player().world().spawnObj(MapObj(Tile(1657, 3504), 28852, 0, 2), false)
			it.player().world().removeObj(MapObj(Tile(1658, 3505), 28853, 0, 1), false)
			it.player().world().removeObj(MapObj(Tile(1658, 3504), 28854, 0, 3), false)
			
			it.player().unlock()
			
			it.chatNpc("Welcome to the woodcutting guild, adventurer.", 7235)
		}
	}
	
}