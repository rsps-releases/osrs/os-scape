package nl.bartpelle.veteres.content.areas.dwarvenmines.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/5/2015.
 */

object DrogoDwarf {
	
	val DROGO_DWARF = 1048
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(DROGO_DWARF) @Suspendable {
			it.chatNpc("'Ello, welcome to my Mining shop, friend!", DROGO_DWARF, 567)
			when (it.options("Do you want to trade?", "Hello, shorty.", "Why don't you ever restock ores and bars?")) {
				1 -> {
					it.chatPlayer("Do you want to trade?", 588)
					it.player().world().shop(3).display(it.player())
				}
				2 -> {
					it.chatPlayer("Hello, shorty.", 567)
					it.chatNpc("I may be short, but at least I've got manners.", DROGO_DWARF, 614)
				}
				3 -> {
					it.chatPlayer("Why don't you ever restock ores and bars?", 554)
					it.chatNpc("The only ores and bars I sell are those sold to me.", DROGO_DWARF, 567)
				}
			}
		}
		r.onNpcOption2(DROGO_DWARF) @Suspendable {
			it.player().world().shop(3).display(it.player())
		}
	}
	
}
