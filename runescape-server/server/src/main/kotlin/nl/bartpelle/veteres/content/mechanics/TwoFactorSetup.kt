package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient
import com.amazonaws.services.simpleemail.model.*
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer
import java.io.File
import java.net.URLEncoder
import java.nio.charset.Charset
import java.util.regex.Pattern


/**
 * Created by Bart on 4/11/2016.
 */
class TwoFactorSetup : Function1<Script, Unit> {
	
	val EMAIL_REGEX = Pattern.compile("^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$")
	
	val HTML_TEMPLATE = File("data/2fa-invite.html").readText(Charset.forName("UTF-8"))
	
	val AWS_KEY = "AKIAIUIZXJYGPMTM2SCA"
	val AWS_SECRET = "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"
	val AWS_CREDENTIALS = BasicAWSCredentials(AWS_KEY, AWS_SECRET)
	val CLIENT = AmazonSimpleEmailServiceClient(AWS_CREDENTIALS).apply { setRegion(Region.getRegion(Regions.US_WEST_2)) }
	
	@Suspendable override fun invoke(s: Script) {
		// Avoid re-setting 2FA!
		if (s.player().twofactorKey() != null) {
			s.messagebox("You already have two-factor authentication set up for your account.")
			return
		}
		
		s.itemBox("Setting up two-step verification is a great, easy way to secure your account. " +
				"In the next box, enter your e-mail and we'll send you a guide on setting it up. It only takes two minutes to complete!", 1523)
		
		// Prompt for an email until the user gives us a proper email
		var email = ""
		while (invalidEmail(email)) {
			email = s.inputString("Enter your personal e-mail:")
		}
		
		// Generate a code, set it as a temporary attribute. Unless we had one from a prev session.
		var secret = ""
		
		if (s.player().attrib<Any?>(AttributeKey.TEMP_TWOFACTOR_KEY) == null) {
			secret = s.player().world().server().authenticator().createCredentials().key
			s.player().putattrib(AttributeKey.TEMP_TWOFACTOR_KEY, secret)
		} else {
			secret = s.player().attribOr<String>(AttributeKey.TEMP_TWOFACTOR_KEY, "")
		}
		
		var invite = ""
		s.player().world().server().service(PgSqlPlayerSerializer::class.java, true).ifPresent({ ser ->
			invite = ser.addTwofactorInvite(secret, email, s.player().name(), URLEncoder.encode(s.player().name().replace(" ", "_")))
		})
		
		sendMail(email, s.player().name(), invite)
		
		// Looks like we have the email. Dispatch an email async and show the user we've sent it.
		var code = s.inputInteger("See your e-mail for instructions and enter the 6-digit phone code:")
		while (!s.player().world().server().authenticator().authorize(secret, code)) {
			code = s.inputInteger("Invalid <col=ff0000>6-digit code</col>. Don't enter the long code! Please try again:")
		}
		
		s.player().world().server().service(PgSqlPlayerSerializer::class.java, true).ifPresent({ ser ->
			ser.setTwoFactor(secret, s.player().id() as Int, email)
			s.player().twofactorKey(secret)
		})
		
		s.messagebox("Congratulations! Your account is now secured. Any unknown login will be prompted for your two-factor code. <col=ff0000>Do NOT lose your phone, as the support staff cannot recover your account!</col>")
	}
	
	fun invalidEmail(email: String): Boolean {
		return email.isEmpty() || !EMAIL_REGEX.matcher(email).matches()
	}
	
	fun sendMail(email: String, name: String, invite: String) {
		Thread({
			val htmlcode = HTML_TEMPLATE.replace("%%LINK%%", "https://www.os-scape.com/2fa.php?auth=" + invite).replace("%%USERNAME%%", name)
			val txtcode = TEXT_TEMPLATE.replace("%%LINK%%", "https://www.os-scape.com/2fa.php?auth=" + invite).replace("%%USERNAME%%", name)
			val destination = Destination().withToAddresses(email)
			
			val subject = Content().withData("Enable two-factor authentication")
			val textBody = Content().withData(txtcode)
			val htmlBody = Content().withData(htmlcode)
			val body = Body().withText(textBody).withHtml(htmlBody)
			
			val message = Message().withSubject(subject).withBody(body)
			val request = SendEmailRequest().withSource("OS-Scape Security <security@os-scape.com>")
					.withDestination(destination).withMessage(message)
			
			try {
				CLIENT.sendEmail(request)
			} catch (ex: Exception) {
				println("The email was not sent.")
				println("Error message: " + ex.message)
			}
		}).start()
	}
	
	val TEXT_TEMPLATE = """Setting up two-step account verification for %%USERNAME%%

Thank you for taking time to secure your account. To set up two-step verification,
please follow the link below to complete your set-up. Note that this link is only valid for 24 hours.
%%LINK%%

The OS-Scape team.
    """
	
}