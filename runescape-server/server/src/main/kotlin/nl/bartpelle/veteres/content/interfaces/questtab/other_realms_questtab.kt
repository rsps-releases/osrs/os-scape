package nl.bartpelle.veteres.content.interfaces.questtab

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Hans
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Misc
import nl.bartpelle.veteres.util.QuestTab
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 09/11/2016.
 */
object other_realms_questtab {
	
	fun setupForRealm(r: ScriptRepository) {
		r.onButton(Interfaces.QUEST_TAB, 2) {
			// Minigames
			if (it.player().privilege().eligibleTo(Privilege.ADMIN))
				when (it.options("Uptime", "Wildy count")) {
					1 -> QuestTabButtons.displayServerUptime(it)
					2 -> QuestTabButtons.displayWildernessBreakdown(it) // (admins)
				}
			
		}
		r.onButton(Interfaces.QUEST_TAB, 7) @Suspendable {
			// Deadman hook
			val slot = it.buttonSlot()
			when (slot) {
				0 -> { // Server uptime button
					QuestTabButtons.displayServerUptime(it)
				}
				1 -> { // Player playtime
					Hans.getTime(it)
				}
				2 -> { // Players online
					it.player().stopActions(false)
					val count = Misc.playerCount(it.player().world())
					it.messagebox("There ${if (count == 1) "is" else "are"} currently $count player${if (count == 1) "" else "s"} online.")
				}
				3 -> { // Wilderness active
					QuestTabButtons.displayWildernessBreakdown(it)
				}
				5 -> { // Voting points
					it.player().stopActions(false)
					it.itemBox("You currently have <col=f9fe11><shad>${it.player().attribOr<Int>(AttributeKey.VOTING_POINTS, 0)}</col></shad> " +
							"voting point${if (it.player().attribOr<Int>(AttributeKey.VOTING_POINTS, 0) == 0) "" else "s"}. For more points, type ::vote!", 10943)
				}
				6 -> { // Total spent
					it.player().stopActions(false)
					if (it.player().donationTier().ordinal + 1 == 8) {
						it.messagebox("You have currently spent <col=009933>$${it.player().totalSpent().toInt()}</col>. This is the highest donation tier there is!")
					} else {
						it.messagebox("You have currently spent <col=009933>$${it.player().totalSpent().toInt()}</col>, " +
								"to upgrade to the next donator tier you need to spend an additional " +
								"<col=ff3333>$${Math.ceil(DonationTier.values()[it.player().donationTier().ordinal + 1].required() - it.player().totalSpent).toInt()}</col>. You also have ${it.player().credits()} credit(s). These are available at ::store.")
					}
				}
				8 -> { // Total kils
					it.player().sync().shout("<img=34> I have ${it.player().varps().varp(Varp.KILLS)} kills.")
				}
				9 -> { // Total deaths
					it.player().sync().shout("<img=13> I have ${it.player().varps().varp(Varp.DEATHS)} deaths.")
				}
				10 -> { // KDR
					it.player().sync().shout("<img=14> I have a k/d ratio of ${QuestTab.getKdr(it.player())}.")
				}
			}
		}

	}
	
}