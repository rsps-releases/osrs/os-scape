package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.blankmessagebox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.services.logging.SqlLoggingService
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer
import java.util.concurrent.TimeUnit

typealias LinkResult = PgSqlPlayerSerializer.RefundVerificationResult

object RefundHandlerNpc {
	
	const val ID = 503
	@JvmStatic public var enabled = true;
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(503, s@ @Suspendable {
			it.chatNpc("Hey there!", ID)

			if (!enabled) {
				it.chatPlayer("Never mind. I see you're busy.")
				return@s
			}

			//it.chatPlayer("Hi, new phone who dis?")
			it.chatPlayer("Hi. I'd like to claim my refunds.")
			
			if (it.player().hasRefundAccount()) {
				processRefund(it)
			} else {
				processAccountSetup(it)
			}
		})
	}
	
	@Suspendable fun processRefund(it: Script) {
		if (it.player().refundCreditsRemaining() > 0) {
			if (!it.player().canClaimRefund()) {
				it.chatNpc("I'm afraid it's not exactly time yet! Please come back in ${getRemainingTime(it.player())}.", ID)
				return
			}

			it.player().lock()
			it.blankmessagebox("Just a second - we're processing your transaction...")
			
			val claimable = if (it.player().refundCreditsPerChunk() > it.player().refundCreditsRemaining())
								it.player().refundCreditsRemaining()
							else
								it.player().refundCreditsPerChunk()
			
			val sql = it.player().world().server().service(PgSqlPlayerSerializer::class.java, true).get()
			var result: Boolean? = null
			sql.claimCreditChunk(it.player(), claimable, { result = it })
			
			while (result == null) {
				it.delay(1)
			}

			it.player().unlock()
			
			if (result!!) {
				it.player().removeRefundCredits(claimable)
				it.player().updateLastRefundClaim()

				// Log the claim to the database for later inspections.
				it.player().world().server().service(SqlLoggingService::class.java, true).ifPresent { log ->
					log.logRefundClaim(it.player(), claimable, it.player().linkedRefundAccount(), it.player().refundCreditsRemaining())
				}

				it.player().bank().add(Item(13190, claimable))
				it.chatNpc("There we go! We've added $claimable credit(s) to your bank. You can claim your next chunk in 20 hours.", ID)
			} else {
				it.chatNpc("Oops, something went wrong there. Please try again later.", ID)
			}
		} else {
			it.chatNpc("You don't have any credits pending, I'm afraid!", ID)
		}
	}
	
	@Suspendable fun processAccountSetup(it: Script) {
		val sql = it.player().world().server().service(PgSqlPlayerSerializer::class.java, true).get()
		
		it.chatNpc("Okay, let's see.. We'll need to get your old account back first. What was your " +
				"username (not your display name, but the name you would log in with!)", ID)
		
		val username = it.inputString("Enter your log-in name before the reset:")
		
		it.chatNpc("Okay, now enter your old password. Make sure nobody is watching!", ID)
		
		val password = it.inputString("Please carefully enter your old password:")

		it.player().lock()
		it.blankmessagebox("We're verifying your account details, please wait...")
		
		var result: LinkResult? = null
		sql.verifyAndLinkRefundDetails(it.player(), username, password, { result = it; null })
		
		while (result == null) {
			it.delay(1)
		}
		it.player().unlock()
		
		when (result) {
			LinkResult.ALREADY_LINKED -> {
				it.chatNpc("That account has already been claimed by someone else. I hope you're not trying to claim someone else's account..?", ID)
			}
			LinkResult.ERROR -> {
				it.chatNpc("Something didn't exactly go right. Can you try again, please?", ID)
			}
			LinkResult.INVALID_USER -> {
				it.chatNpc("That username doesn't seem to exist, or has never used the OS-Scape store. Remember that " +
						"only things you have paid for can be reclaimed. Credits you have purchased from other players are not refunded.", ID)
			}
			LinkResult.INVALID_PASS -> {
				it.chatNpc("That doesn't seem to be the right password. Please try again.", ID)
			}
			LinkResult.SUCCESS -> {
				it.chatNpc("Great! That's all done - you'll be able to claim ${it.player().refundCreditsRemaining()} " +
						"credits in total, ${it.player().refundCreditsPerChunk()} per 20 hours.", ID)
				it.chatNpc("You can talk to me to claim them every 20 hours.", ID)
				it.chatPlayer("Why can't you give them all at once?")
				it.chatNpc("If we'd hand it out all at once, the economy would take a great hit in a very short time.", ID)
				it.chatPlayer("Ok, understood!")
			}
		}
	}

	private fun getRemainingTime(player: Player): String {
		val ms = player.timeUntilNextRefund()
		val hours = TimeUnit.MILLISECONDS.toHours(ms)
		val minutes = TimeUnit.MILLISECONDS.toMinutes(ms)
		return (if (hours >= 1) hours.toString() + " hour" + (if (hours > 1) "s" else "") + " and " else "") +
				Math.max(minutes - TimeUnit.HOURS.toMinutes(hours), 1) + " minute" +
				if (minutes - TimeUnit.HOURS.toMinutes(hours) > 1) "s" else ""
	}
	
}