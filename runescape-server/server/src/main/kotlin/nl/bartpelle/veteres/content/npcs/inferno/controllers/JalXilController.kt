package nl.bartpelle.veteres.content.npcs.inferno.controllers

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Mack on 8/8/2017.
 */
class JalXilController(xil: Npc, session: InfernoSession, offset: Int): InfernoNpcController {
	
	/**
	 * The session this npc is involved in.
	 */
	val session = session
	
	/**
	 * The xil npc owner of this controller.
	 */
	val xil = xil
	
	/**
	 * The class pointer variable.
	 */
	var offset = offset
	
	/**
	 * The collection of spawns for the rangers.
	 */
	val spawns = arrayListOf(Tile(session.area!!.center().x + 5, session.area!!.center().z - 6), Tile(session.area!!.center().x - 5, session.area!!.center().z - 6))
	
	override fun onSpawn() {
		xil.tile(spawns[offset - 1])
		xil.attack(session.player())
	}
	
	override fun onDamage() {
		if (!InfernoContext.inSession(session.player())) {
			session.player().message("I don't think I should be doing this...")
			return
		}
		super.onDamage()
	}
}