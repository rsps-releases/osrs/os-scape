package nl.bartpelle.veteres.content.areas.zeah.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/11/2016.
 */

object CaptainKalt {
	
	val CAPTAIN_KALT = 7377
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(CAPTAIN_KALT) @Suspendable {
			val TALKED_TO_IGNISIA = it.player().attribOr<Int>(AttributeKey.TALKED_TO_IGNISIA, 0)
			
			if (TALKED_TO_IGNISIA == 1) {
				it.chatNpc("Hello there.", CAPTAIN_KALT, 588)
				when (it.options("Tell me about the Wintertodt.", "Tell me about yourself.", "Tell me how I'm doing.", "See you later.")) {
					1 -> tell_me_about_the_wintertodt(it)
					2 -> tell_me_about_yourself(it)
					3 -> tell_me_how_im_doing(it)
					4 -> see_you_later(it)
				}
			} else {
				it.chatNpc("Hello there.", CAPTAIN_KALT, 567)
				it.chatPlayer("Hello. What's going on here then?", 554)
				it.chatNpc("You should speak to Ignisia, she'll fill you in.", CAPTAIN_KALT, 588)
			}
		}
		
		r.onNpcOption2(CAPTAIN_KALT) @Suspendable {
			val TALKED_TO_IGNISIA = it.player().attribOr<Int>(AttributeKey.TALKED_TO_IGNISIA, 0)
			
			if (TALKED_TO_IGNISIA == 1) {
				val TOTAL_GAMES = it.player().attribOr<Int>(AttributeKey.WINTERTODT_COUNT, 0)
				val TOTAL_POINTS = it.player().attribOr<Int>(AttributeKey.WINTERTODT_TOTAL, 0)
				val HIGHEST_POINTS = it.player().attribOr<Int>(AttributeKey.WINTERTODT_HIGHSCORE, 0)
				val sOrNos = if (TOTAL_GAMES == 1) "" else "s"
				it.messagebox3("You have subdued the Wintertodt $TOTAL_GAMES time$sOrNos.<br>Your lifetime score: $TOTAL_POINTS<br>Your highest score: $HIGHEST_POINTS")
			} else {
				it.chatNpc("You should speak to Ignisia, she'll fill you in.", CAPTAIN_KALT, 588)
			}
		}
	}
	
	@Suspendable fun tell_me_about_the_wintertodt(it: Script) {
		it.chatPlayer("Tell me about the Wintertodt.", 554)
		it.chatNpc("Ignisia claims it's some kind of evil spirit.", CAPTAIN_KALT, 610)
		it.chatPlayer("You don't believe her?", 554)
		it.chatNpc("Nah, I believe in things I can stab and kill. This is all<br>" +
				"just a storm in a teacup. I mean, how can wind and<br>snow be evil? My toad is more of a threat!", CAPTAIN_KALT, 590)
		it.chatPlayer("Your... toad?", 554)
		it.chatNpc("Yeah, just a bit of fun, I have a pet - he's around here<br>somewhere, assuming that troublesome cat hasn't eaten<br>him.", CAPTAIN_KALT, 569)
		it.chatPlayer("That's a bit weird, but okay.", 575)
		it.chatNpc("No weirder than being scared of snow.", CAPTAIN_KALT, 567)
		when (it.options("Tell me about yourself.", "Tell me how I'm doing.", "See you later.")) {
			1 -> tell_me_about_yourself(it)
			2 -> tell_me_how_im_doing(it)
			3 -> see_you_later(it)
		}
	}
	
	@Suspendable fun tell_me_about_yourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 554)
		it.chatNpc("I'm Captain Kalt, proud member of the Shayzien<br>Guard.", CAPTAIN_KALT, 568)
		it.chatPlayer("How long have you been a guard?", 554)
		it.chatNpc("Man and boy I've worn the uniform. Earned Captain a<br>few years back and never looked back. Not too happy" +
				"<br>about babysitting delusional wizards though. But Orders<br>is Orders.", CAPTAIN_KALT, 570)
		when (it.options("Tell me about the Wintertodt.", "Tell me how I'm doing.", "See you later.")) {
			1 -> tell_me_about_the_wintertodt(it)
			2 -> tell_me_how_im_doing(it)
			3 -> see_you_later(it)
		}
	}
	
	@Suspendable fun tell_me_how_im_doing(it: Script) {
		val HIGHEST_POINTS = it.player().attribOr<Int>(AttributeKey.WINTERTODT_HIGHSCORE, 0)
		val TOTAL_POINTS = it.player().attribOr<Int>(AttributeKey.WINTERTODT_COUNT, 0)
		
		it.chatPlayer("Tell me how I'm doing.", 554)
		it.chatNpc("You appear to have tried a little.", CAPTAIN_KALT, 588)
		it.messagebox3("You have subdued the Wintertodt once.<br>Your lifetime score: $TOTAL_POINTS<br>Your highest score: $HIGHEST_POINTS")
		when (it.options("Tell me about the Wintertodt.", "Tell me about yourself.", "See you later.")) {
			1 -> tell_me_about_the_wintertodt(it)
			2 -> tell_me_about_yourself(it)
			3 -> see_you_later(it)
		}
	}
	
	@Suspendable fun see_you_later(it: Script) {
		it.chatPlayer("See you later.", 588)
		it.chatNpc("Goodbye.", CAPTAIN_KALT, 567)
	}
}
