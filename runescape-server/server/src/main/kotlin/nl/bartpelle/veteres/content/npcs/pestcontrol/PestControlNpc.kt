package nl.bartpelle.veteres.content.npcs.pestcontrol

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.TileOffset
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.minigames.pest_control.Breakable
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.fs.MapDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Jason MacKeigan on 2016-09-06 at 2:19 PM
 */
object PestControlNpc {
	
	/**
	 * Forces the selected non-playable character to walk to the knight non-playable character. This
	 * function will attempt to walk to the knight fifteen times, after that the script is
	 * suspended.
	 */
	@Suspendable fun walkToKnight(knight: Npc, selected: Npc, script: Script, style: CombatStyle) {
		var walkAttempts = 0
		
		val distance = when (style) {
			CombatStyle.MELEE -> {
				1
			}
			CombatStyle.MAGIC -> {
				6
			}
			CombatStyle.RANGE -> {
				8
			}
			else -> {
				1
			}
		}
		while (knight.tile().distance(selected.tile()) > distance && walkAttempts++ < 15) {
			val target = EntityCombat.getTarget(selected)
			
			if (target != null || selected.dead()) {
				return
			}
			selected.walkTo(knight, PathQueue.StepType.REGULAR)
			script.delay(2)
		}
		
		if (EntityCombat.getTarget(selected) == null && EntityCombat.targetOk(selected, knight, distance)) {
			selected.attack(knight)
		}
	}
	
	/**
	 * Searches the area for a breakable object within distance of the mob. The function
	 * will return only a broken object if there are no available complete or half broken
	 * breakable objects nearby. The function doesn't always choose the closest breakable
	 * object, the result is heavily dependent on the order of [offsets].
	 */
	fun searchForBreakable(npc: Npc, centerOfMap: Tile, mapDefinition: MapDefinition, distanceExclusive: Int,
	                       breakable: Breakable, offsets: List<TileOffset>): Optional<MapObj> {
		val world = npc.world()
		
		outer@
		for (offset in offsets) {
			val tile = centerOfMap.transform(offset.x, offset.z)
			
			if (tile.distance(npc.tile()) > distanceExclusive) {
				continue
			}
			for (id in breakable.stages) {
				val globalObject = world.getSpawnedObj(id, tile)
				
				if (globalObject.isPresent) {
					if (globalObject.get().id() == breakable.stages.last()) {
						continue@outer
					}
					return globalObject
				}
			}
			for (id in breakable.stages) {
				val nonGlobalObject = Optional.ofNullable(
						mapDefinition.objById(tile.level, tile.x.and(63), tile.z.and(63), id))
				
				if (nonGlobalObject.isPresent) {
					if (nonGlobalObject.get().id() == breakable.stages.last()) {
						continue@outer
					}
					return nonGlobalObject
				}
			}
		}
		return Optional.empty()
	}
	
	/**
	 * Attempts to walk the mob to the specific object. The mob has their last player
	 * in combat removed if one is present. Their facing entity is also updated to
	 * null before walking.
	 */
	@Suspendable fun walkToObject(script: Script, objectToWalkTo: MapObj) {
		val npc = script.npc()
		
		npc.clearattrib(AttributeKey.TARGET)
		npc.sync().faceEntity(null)
		npc.lockNoDamage()
		npc.walkTo(objectToWalkTo.tile(), PathQueue.StepType.REGULAR)
		script.waitForTile(objectToWalkTo.tile(), 1)
		npc.unlock()
	}
	
}