package nl.bartpelle.veteres.content.minigames.pest_control

import java.util.*

/**
 * Created by Jason MacKeigan on 2016-07-19 at 2:14 PM
 *
 * The pests, which are non-playable characters, that are involved in the pest control mini game.
 */
enum class Pest(val ids: IntRange, val difficultyRanges: Map<PestControlDifficulty, IntRange>) {
	BRAWLER(1734..1738, mapOf(PestControlDifficulty.NOVICE to 0..1, PestControlDifficulty.INTERMEDIATE to 2..3,
			PestControlDifficulty.VETERAN to 3..4)),
	
	DEFILER(1724..1733, mapOf(PestControlDifficulty.NOVICE to 0..3, PestControlDifficulty.INTERMEDIATE to 4..7,
			PestControlDifficulty.VETERAN to 7..9)),
	
	RAVAGER(1704..1708, mapOf(PestControlDifficulty.NOVICE to 0..1, PestControlDifficulty.INTERMEDIATE to 2..3,
			PestControlDifficulty.VETERAN to 3..4)),
	
	SHIFTER(1694..1703, mapOf(PestControlDifficulty.NOVICE to 0..3, PestControlDifficulty.INTERMEDIATE to 4..7,
			PestControlDifficulty.VETERAN to 8..9)),
	
	SPINNER(1709..1713, mapOf(PestControlDifficulty.NOVICE to 0..1, PestControlDifficulty.INTERMEDIATE to 1..2,
			PestControlDifficulty.VETERAN to 3..4)),
	
	SPLATTER(1689..1693, mapOf(PestControlDifficulty.NOVICE to 0..1, PestControlDifficulty.INTERMEDIATE to 2..3,
			PestControlDifficulty.VETERAN to 3..4)),
	
	TORCHER(1714..1723, mapOf(PestControlDifficulty.NOVICE to 0..3, PestControlDifficulty.INTERMEDIATE to 4..7,
			PestControlDifficulty.VETERAN to 7..9));
	
	/**
	 * Retrieves a random non-playable character identification value that is
	 * relative to the pest control game difficulty.
	 */
	fun getRandom(random: Random, difficulty: PestControlDifficulty): Int {
		val range = difficultyRanges[difficulty]!!
		
		return ids.first + (range.first + random.nextInt(range.endInclusive - range.first))
	}
	
	companion object {
		
		/**
		 * A mapping of all pests to their respective id values.
		 */
		val IDS = Pest.values().flatMap { it.ids }
		
	}
}