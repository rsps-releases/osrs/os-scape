package nl.bartpelle.veteres.content.areas.wilderness

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 08/01/2016.
 */

object MageBankInside {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption2(1600) {
			Bank.open(it.player(), it)
		}
	}
	
}
