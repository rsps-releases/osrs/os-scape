package nl.bartpelle.veteres.content.areas.motherlode

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 4/6/2016.
 */
object MotherlodeMines {
	
	val PAYDIRT_NPC = 6564
	val PAYDIRT_ITEM = 12011
	val WATER_BUBBLES = 2016
	val ROLLING_WHEEL = 26671
	val FIXED_STRUT = 26669
	val BROKEN_STRUT = 26670
	val ENTER_CAVE = 26654
	val EXIT_TUNNEL = 26655
	val CRATES = 357
	
	val PROSPECTOR_PERCY = 6562
	
	val PICKAXE_CRATES = arrayListOf(Tile(3753, 5659), Tile(3755, 5665), Tile(3758, 5671))
	val HAMMER_CRATES = arrayListOf(Tile(3752, 5664), Tile(3755, 5660), Tile(3756, 5668), Tile(3752, 5674))
	
	val BRONZE_PICKAXE = 1265
	val HAMMER = 2347
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		registerObjects(r)
		registerNpcs(r)
		registerLogic(r)
	}
	
	fun registerObjects(r: ScriptRepository) {
		r.onObject(BROKEN_STRUT) @Suspendable {
			it.player().lock()
			it.player().animate(3971, 10)
			it.player().sound(1786, 10)
			it.delay(1)
			it.player().animate(-1)
			fixWheel(it.interactionObject())
			it.player().unlock()
		}
		
		r.onObject(ENTER_CAVE) @Suspendable {
			it.player().message("The cave is too small to crawl through.")
			/*     val cave: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				 if (!it.player().tile().equals(cave.tile().transform(1, 2, 0))) {
					 it.player().walkTo(cave.tile().transform(1, 2, 0), PathQueue.StepType.FORCED_WALK)
					 it.waitForTile(cave.tile().transform(1, 2, 0))
				 }
				 it.player().lock()
				 it.delay(1)
				 it.animate(2796)
				 it.delay(1)
				 it.player().teleport(3728, 5692)
				 it.animate(-1)
				 it.player().unlock()*/
		}
		
		r.onObject(EXIT_TUNNEL) @Suspendable {
			val cave: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (!it.player().tile().equals(cave.tile().transform(0, -1, 0))) {
				it.player().walkTo(cave.tile().transform(0, -1, 0), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(cave.tile().transform(0, -1, 0))
			}
			it.player().lock()
			it.delay(1)
			it.animate(2796)
			it.delay(1)
			it.player().teleport(3060, 9766)
			it.animate(-1)
			it.player().unlock()
		}
		
		r.onObject(CRATES) @Suspendable {
			val crate: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			//Does the crate contain a pickaxe?
			for (crates in PICKAXE_CRATES) {
				if (crates == crate.tile()) {
					if (it.player().inventory().has(BRONZE_PICKAXE)) {
						it.message("You cycle the crate but find nothing.")
					} else {
						if (it.player().inventory().add(Item(BRONZE_PICKAXE), false).success()) {
							it.itemBox("You've found a bronze pickaxe. How handy.", BRONZE_PICKAXE)
						}
					}
				}
			}
			
			//Does the crate contain a hammer?
			for (crates in HAMMER_CRATES) {
				if (crates == crate.tile()) {
					if (it.player().inventory().has(HAMMER)) {
						it.message("You cycle the crate but find nothing.")
					} else {
						if (it.player().inventory().add(Item(HAMMER), false).success()) {
							it.itemBox("You've found a hammer. How handy.", HAMMER)
						}
					}
				}
			}
		}
		
	}
	
	fun registerNpcs(r: ScriptRepository) {
		r.onNpcOption1(PROSPECTOR_PERCY) @Suspendable {
			it.chatNpc("Git back ter work, ye young varmint! There's treasure<br>in them walls, and it's not gonna mine itself while ye<br>" +
					"stand here yappin'.", PROSPECTOR_PERCY, 616)
			when (it.optionsTitled("What would you like to say?", "How do I mine here?", "Would you like to trade?", "Tell me about yourself.", "Can I go up the ladder to mine there?",
					"I'll leave you alone.")) {
				1 -> how_do_I_mine(it)
				2 -> would_you_like_to_trade(it)
				3 -> tell_me_about_yourself(it)
				4 -> can_I_go_up_the_ladder(it)
				5 -> ill_leave_you_alone(it)
			}
		}
	}
	
	fun how_do_I_mine(it: Script) {
		it.chatPlayer("How do I mine here?", 554)
		it.chatNpc("Git ahold of yer pickaxe, find a vein of ore, and set to<br>work. If ye got a bit of skill, ye'll have a pocket o' pay-<br>dirt in no time.", PROSPECTOR_PERCY, 564)
		it.chatNpc("I've built me a contraption to wash the pay-dirt. Just<br>drop yer pay-dirt in the hopper, an' wait fer it at the<br>other end.", PROSPECTOR_PERCY, 590)
		it.chatNpc("I won't charge ye fer usin' my contraption, but ye'd<br>better fix it yerself when it breaks. A good whack with a<br>hammer usually settles it.", PROSPECTOR_PERCY, 564)
		it.chatNpc("Now will ye be gettin' to work now, or are ye gonna<br>keep yappin' like a doggone galoot?", PROSPECTOR_PERCY, 615)
		when (it.optionsTitled("What would you like to say?", "Would you like to trade?", "Tell me about yourself.", "Can I go up the ladder to mine there?", "I'll leave you alone.")) {
			1 -> would_you_like_to_trade(it)
			2 -> tell_me_about_yourself(it)
			3 -> can_I_go_up_the_ladder(it)
			4 -> ill_leave_you_alone(it)
		}
	}
	
	fun would_you_like_to_trade(it: Script) {
		it.chatPlayer("Would you like to trade?", 554)
		it.chatNpc("If ye've found yerself some golden nuggets in this 'ere<br>mine, I'll do you a swap, yeah.", PROSPECTOR_PERCY, 593)
		it.player().world().shop(32).display(it.player())
	}
	
	fun tell_me_about_yourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 567)
		it.chatNpc("Why, I'm Percy. Prospector Percy, the roughest,<br>toughest, gruffest miner in the land. I've been pannin'<br>fer gold since I were a young-un, " +
				"and here's where<br>I've struck it lucky.", PROSPECTOR_PERCY, 565)
		when (it.optionsTitled("What would you like to say?", "Excuse me, but what language are you speaking?", "You discovered this mine?")) {
			1 -> excuse_me(it)
			2 -> you_discovered_this_mine(it)
		}
	}
	
	fun excuse_me(it: Script) {
		it.chatPlayer("Excuse me, but what language are you speaking?")
		it.chatNpc("Don't ye give me any of yer lip, ye dern varmint!<br>Young'uns these days got no respect.", PROSPECTOR_PERCY, 615)
		it.chatPlayer("Do go on.", 592)
		it.chatNpc("This here's the richest seam of ore I've found in all my<br>days. After I built a contraption for washing the pay-<br>dirt, the dwarves let me run things down here.", PROSPECTOR_PERCY, 590)
		it.chatNpc("Now, have ye any more idjit questions, or are ye ready<br>to do some real work?", PROSPECTOR_PERCY, 555)
		when (it.optionsTitled("What would you like to say?", "How do I mine here?", "Would you like to trade?", "Can I go up the ladder to mine there?", "I'll leave you alone.")) {
			1 -> how_do_I_mine(it)
			2 -> would_you_like_to_trade(it)
			3 -> can_I_go_up_the_ladder(it)
			4 -> ill_leave_you_alone(it)
		}
	}
	
	fun you_discovered_this_mine(it: Script) {
		it.chatPlayer("You discovered this mine?", 554)
		it.chatNpc("This here's the richest seam of ore I've found in all my<br>days. After I built a contraption for washing the pay-<br>dirt, the dwarves let me run things down here.", PROSPECTOR_PERCY, 590)
		it.chatNpc("Now, have ye any more idjit questions, or are ye ready<br>to do some real work?", PROSPECTOR_PERCY, 555)
		when (it.optionsTitled("What would you like to say?", "How do I mine here?", "Would you like to trade?", "Can I go up the ladder to mine there?", "I'll leave you alone.")) {
			1 -> how_do_I_mine(it)
			2 -> would_you_like_to_trade(it)
			3 -> can_I_go_up_the_ladder(it)
			4 -> ill_leave_you_alone(it)
		}
	}
	
	fun can_I_go_up_the_ladder(it: Script) {
		it.chatPlayer("Can I go up the ladder to mine there?", 554)
		it.chatNpc("Ye'll need level 72 Mining first. An' don't think ye can<br>fool me with yer potions and fancy stat-boosts. Get yer<br>level up for real.", PROSPECTOR_PERCY, 564)
		when (it.optionsTitled("What would you like to say?", "How do I mine here?", "Would you like to trade?", "Tell me about yourself.", "I'll leave you alone.")) {
			1 -> how_do_I_mine(it)
			2 -> would_you_like_to_trade(it)
			3 -> tell_me_about_yourself(it)
			4 -> ill_leave_you_alone(it)
		}
	}
	
	fun ill_leave_you_alone(it: Script) {
		it.chatPlayer("I'll leave you alone.", 571)
		it.chatNpc("Dern straight ye will.", PROSPECTOR_PERCY, 562)
	}
	
	fun registerLogic(r: ScriptRepository) {
		r.onWorldInit @Suspendable {
			while (true) {
				updateWaterflow(it.ctx())
				it.delay(5)
			}
		}
	}
	
	fun updateWaterflow(world: World) {
		if (wheelsActive(world)) {
			// Send flushy flushy
		} else {
			// Send a dead stream
		}
	}
	
	fun fixWheel(obj: MapObj) {
		
	}
	
	fun breakFlow() {
		
	}
	
	fun repairFlow() {
		
	}
	
	fun wheelsActive(world: World): Boolean {
		return world.objByType(10, 3742, 5669, 0).id() == FIXED_STRUT ||
				world.objByType(10, 3742, 5663, 0).id() == FIXED_STRUT
	}
	
}