package nl.bartpelle.veteres.content.events

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.journal.main.Hotspot
import nl.bartpelle.veteres.util.journal.main.Hotspot.ACTIVE

object Hotspot {

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onWorldInit @Suspendable {
            val swapTicks = 20 * 100 //20 minutes
            while (true) {
                val next: Hotspot = Hotspot.randomHotspot()
                if (next === ACTIVE) {
                    it.delay(1) //Let's not risk hogging the current thread.
                    continue
                }
                ACTIVE = next
                it.ctx<World>().broadcast("<col=6a1a18><img=15> " + next.name + " is the new hotspot! Killing players here will give double blood money for the next 20 minutes!")
                it.delay(swapTicks)
            }
        }
    }
}
