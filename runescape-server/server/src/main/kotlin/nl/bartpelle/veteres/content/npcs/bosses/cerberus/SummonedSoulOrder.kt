package nl.bartpelle.veteres.content.npcs.bosses.cerberus

import java.util.*

/**
 * Created by Jason MacKeigan on 2016-07-07 at 12:45 PM
 *
 * Represents the order that a group of summoned soul non-playable characters
 * attack.
 */
enum class SummonedSoulOrder(val position: Int) : Comparator<SummonedSoulOrder> {
	FIRST(0), SECOND(1), THIRD(2);
	
	/**
	 * Returns the next possible different SummonedSoulOrder object in the ordered
	 * list, otherwise the last element in the enumeration is returned.
	 */
	fun next(): SummonedSoulOrder {
		return if (position < OVERFLOW_POSITION) ordered[position + 1] else THIRD
	}
	
	override fun compare(o1: SummonedSoulOrder, o2: SummonedSoulOrder): Int {
		return Integer.compare(o1.position, o2.position)
	}
	
	companion object {
		
		/**
		 * A List of sorted SummonedSoulOrder elements.
		 */
		val ordered = SummonedSoulOrder.values().sorted()
		
		/**
		 * The value that determines when the position value is greater
		 * than the last remaining position.
		 */
		const val OVERFLOW_POSITION = 2
	}
}

