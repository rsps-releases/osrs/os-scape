package nl.bartpelle.veteres.content.skills.herblore

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.ItemOnItem
import nl.bartpelle.veteres.content.itemOptions
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 10/04/2017.
 */
object HerbTar {
	
	enum class Tars(val herb: Int, val result: Int, val xp: Double, val reqlevel: Int) {
		GUAM(249, 10142, 30.0, 19),
		MARRENTILL(251, 10143, 42.5, 31),
		TARROMIN(253, 10144, 55.0, 39),
		HARRALANDER(255, 10145, 67.5, 44)
	}
	
	val SWAMP_TAR = 1939
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		Tars.values().forEach { tar ->
			repo.onItemOnItem(tar.herb, SWAMP_TAR) @Suspendable {
				val player = it.player()
				val herbslot = ItemOnItem.slotOf(it, tar.herb)
				val herb = player.inventory()[herbslot]
				val herbname = herb.name(player.world()).replace("leaf", "leaves")
				if (!player.inventory().has(PestleAndMortar.PESTLE_AND_MORTAR)) {
					player.message("You need a pestle and mortar to mix ${herbname.toLowerCase()} with swamp tar.")
				} else if (player.skills().xpLevel(Skills.HERBLORE) < tar.reqlevel) {
					player.message("You need a Herblore level of ${tar.reqlevel} to make ${herbname.split(" ")[0]} tar.")
				} else {
					var count = player.inventory().count(tar.herb)
					if (count > 1) {
						count = it.itemOptions(Item(tar.herb, 175), "How many $herbname would you like to use?", offsetX = 6, subtext = "Make ${herbname.split(" ")[0]} tar.")
					}
					while (count-- > 0) {
						player.animate(5249)
						it.delay(3)
						player.inventory().remove(Item(SWAMP_TAR, 15), false)
						player.inventory().remove(Item(tar.herb), false)
						player.inventory().add(Item(tar.result, 15), false)
						player.message("You mix the ${herb.name(player.world())} into the swamp tar.")
						player.skills().__addXp(Skills.HERBLORE, tar.xp)
					}
					player.animate(-1)
					player.graphic(-1)
				}
			}
		}
	}
}