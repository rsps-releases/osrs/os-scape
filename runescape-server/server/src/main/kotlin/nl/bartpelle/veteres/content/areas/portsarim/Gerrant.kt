package nl.bartpelle.veteres.content.areas.portsarim

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/27/2015.
 */

object Gerrant {
	
	val GERRANT = 1027
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption2(GERRANT) @Suspendable {
			it.player().world().shop(22).display(it.player())
		}
	}
}