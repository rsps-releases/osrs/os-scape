package nl.bartpelle.veteres.content.areas.dungeons.godwars

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 3/10/2016.
 */

object GwdEntrance {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(26419) @Suspendable {
			it.delay(1)
			
			//Check to see if the player has a rope or a grapple
			if (it.player().varps().varbit(Varbit.GOD_WARS_DUNGEON) == 0) {
				if (it.player().inventory().contains(Item(954, 1))) {
					it.player().varps().varbit(Varbit.GOD_WARS_DUNGEON, 1)
					it.player().inventory().remove(Item(954, 1), true)
				} else {
					it.player().message("You aren't carrying a rope with you.")
				}
			} else if (it.player().varps().varbit(Varbit.GOD_WARS_DUNGEON) == 1) {
				it.player().animate(828)
				it.delay(1)
				it.player().teleport(Tile(2882, 5311, 2))
			} else {
				it.message("Something went wrong, please report what you did to the staff!")
			}
		}
		
		//Rope to leave the dungeon
		r.onObject(26370) @Suspendable {
			it.player().animate(828)
			it.delay(1)
			it.player().teleport(Tile(2916, 3746, 0))
		}
	}
	
}
