package nl.bartpelle.veteres.content.skills.magic.lunar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.skills.magic.ProductionSpell
import nl.bartpelle.veteres.content.skills.magic.TeleportSpell
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Mack
 */
object LunarSpellBook {

    private val lunarProductionSpells: Array<ProductionSpell> = arrayOf(
            StringJewelrySpell(),
            SpinFlaxSpell(),
            SuperglassMakeSpell()
    )

    private val lunarTeleports: Array<TeleportSpell> = arrayOf(
            TeleportSpell(122, "modern", 85, 65.0, TeleportType.GENERIC, 714, 111, Tile(2604, 3400), arrayOf(Item(555, 10), Item(9075, 3), Item(563, 3))),
            TeleportSpell(110, "modern", 75, 53.0, TeleportType.GENERIC, 714, 111, Tile(2519, 3571), arrayOf(Item(554, 3), Item(9075, 2), Item(563, 2))),
            TeleportSpell(102, "modern", 69, 66.0, TeleportType.GENERIC, 714, 111, Tile(2113, 3915), arrayOf(Item(557, 2), Item(9075, 2), Item(563, 1)))
    )

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        for (production in lunarProductionSpells) {
            sr.onButton(218, production.arguments[0], @Suspendable {
                if (!it.player().world().realm().isPVP) {
                    production.execute(it)
                }
            })
        }
        for (teleport in lunarTeleports) {
            sr.onButton(218, teleport.button, @Suspendable {
                if (!it.player().world().realm().isPVP) {
                    teleport.execute(it)
                }
            })
        }
    }
}