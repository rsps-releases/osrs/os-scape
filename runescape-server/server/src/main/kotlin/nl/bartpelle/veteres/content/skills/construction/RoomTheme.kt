package nl.bartpelle.veteres.content.skills.construction

enum class RoomTheme(val level: Int, val cost: Int) {

    WOOD(1, 5000),
    STONE(10, 5000),
    WHITEWASHED(20, 7500),
    FREMENNIK(30, 10000),
    TROPICAL(40, 15000),
    FANCY(50, 25000),
    DEATHLY(1, 35000),

}