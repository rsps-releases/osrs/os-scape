package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/1/2015.
 */

object MasterSmithingTutor {
	
	val MASTER_SMITHING_TUTOR = 3225
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MASTER_SMITHING_TUTOR) @Suspendable {
			it.chatPlayer("Any advice for an advanced smith?")
			it.chatNpc("As you get better you'll find you will be able to smith Mithril and eventually Adamantite and even Runite. This can be very lucrative but very expensive on the coal front. It may be worth stockpiling coal for a", MASTER_SMITHING_TUTOR, 589)
			it.chatNpc("while before attempting these difficult metals or even sticking to goold old reliable iron by the bucket load.", MASTER_SMITHING_TUTOR, 589)
		}
	}
	
}
