package nl.bartpelle.veteres.content.areas.clanwars

/**
 * Created by Bart on 6/1/2016.
 */
object CWSettings {
	
	const val GAME_END = 4270
	const val MELEE = 4271
	const val RANGE = 4272
	const val MAGIC = 4273
	const val PRAYER = 4274
	const val FOOD = 4275
	const val DRINKS = 4276
	const val SPECIAL_ATTACKS = 4277
	const val STRAGGLERS = 4278
	const val IGNORE_FREEZING = 4279
	const val PJ_TIMER = 4280
	const val ALLOW_TRIDENT = 4281
	const val SINGLE_SPELLS = 4282
	const val ARENA = 4283
	const val ACCEPT = 4285
	const val COUNTDOWN_TIMER = 4286
	const val TEAM_1_COUNT = 4287
	const val TEAM_2_COUNT = 4288
	const val ACTIVE_TEAM = 4289
	
	// Settings for game end:
	const val LAST_TEAM_STANDING = 0
	const val KILL_25 = 1
	const val KILL_50 = 2
	const val KILL_100 = 3
	const val KILL_200 = 4
	const val KILL_500 = 5
	const val KILL_1000 = 6
	const val KILL_5000 = 7
	const val KILL_10000 = 8
	const val KING_80 = 9
	const val KING_250 = 10
	const val KING_750 = 11
	const val KING_1500 = 12
	const val KING_2500 = 13
	const val KING_4000 = 14
	const val KING_6000 = 15
	const val MOST_KILLS_5 = 16
	const val MOST_KILLS_10 = 17
	const val MOST_KILLS_20 = 18
	const val MOST_KILLS_60 = 19
	const val MOST_KILLS_120 = 20
	const val ODDSKULL_100 = 21
	const val ODDSKULL_300 = 22
	const val ODDSKULL_500 = 23
	
	// Settings for arena:
	const val WASTELAND = 0
	const val PLATEAU = 1
	const val SYLVAN_GLADE = 2
	const val FORSAKEN_QUARRY = 3
	const val TURRETS = 4
	const val CLAN_CUP = 5
	const val SOGGY_SWAMP = 6
	const val GHASTLY_SWAMP = 7
	const val NORTHLEACH_QUELL = 8
	const val GRIDLOCK = 9
	const val ETHEREAL = 10
	const val CLASSIC = 11
	
	// Stragglers
	const val KILL_EM_ALL = 0
	const val IGNORE_5 = 1
	
}