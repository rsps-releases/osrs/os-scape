package nl.bartpelle.veteres.content.quests.free

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interfaces.questtab.QuestTabButtons
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.content.quests.Quests
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 4/6/2016.
 */
object DoricsQuest {
	
	val STAGE_VARP = 31
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		QuestTabButtons.ECO_HANDLERS_FREE[3] = {
			QuestGuide.clear(it.player())
			it.player().interfaces().text(275, 2, "<col=800000>Doric's Quest")
			
			when (stage(it.player())) {
				0 -> {
					it.player().interfaces().text(275, 4, "<col=000080>I can start this quest by speaking to <col=800000>Doric<col=000080> who is <col=800000>North of")
					it.player().interfaces().text(275, 5, "<col=800000>Falador")
					it.player().interfaces().text(275, 7, "<col=000080>There aren't any requirements but <col=800000>Level 15 Mining<col=000080> will help")
				}
				10 -> {
					it.player().interfaces().text(275, 4, "<str>I have spoken to <col=800000>Doric")
					it.player().interfaces().text(275, 6, "<col=000080>I need to collect some items and bring them to <col=800000>Doric")
					
					if (hasClay(it.player())) {
						it.player().interfaces().text(275, 7, "<str>6 Clay")
					} else {
						it.player().interfaces().text(275, 7, "<col=800000>6 Clay")
					}
					
					if (hasCopper(it.player())) {
						it.player().interfaces().text(275, 8, "<str>4 Copper Ore")
					} else {
						it.player().interfaces().text(275, 8, "<col=800000>4 Copper Ore")
					}
					
					if (hasIron(it.player())) {
						it.player().interfaces().text(275, 9, "<str>2 Iron Ore")
					} else {
						it.player().interfaces().text(275, 9, "<col=800000>2 Iron Ore")
					}
				}
				100 -> {
					it.player().interfaces().text(275, 4, "<str>I have spoken to <col=800000>Doric")
					it.player().interfaces().text(275, 6, "<str>I have collected some Clay, Copper Ore, and Iron Ore")
					it.player().interfaces().text(275, 8, "<str>Doric rewarded me for all my hard work")
					it.player().interfaces().text(275, 9, "<str>I can now use Doric's Anvils whenever I want")
					it.player().interfaces().text(275, 10, "<col=ff0000>QUEST COMPLETE!")
				}
			}
			
			QuestGuide.open(it.player())
		}
	}
	
	fun showCompletion(player: Player) {
		// Song: 152, 6325
	}
	
	fun stage(player: Player): Int = player.varps().varp(STAGE_VARP)
	
	fun stage(player: Player, stage: Int) = player.varps().varp(STAGE_VARP, stage)
	
	fun hasOres(player: Player): Boolean {
		return hasClay(player) && hasCopper(player) && hasIron(player)
	}
	
	fun hasClay(player: Player): Boolean = player.inventory().count(434) >= 6
	fun hasCopper(player: Player): Boolean = player.inventory().count(436) >= 4
	fun hasIron(player: Player): Boolean = player.inventory().count(440) >= 2
	
	fun takeOresAndComplete(player: Player) {
		// Remove ores from the inventory
		player.inventory().remove(Item(434, 6), true)
		player.inventory().remove(Item(436, 4), true)
		player.inventory().remove(Item(440, 2), true)
		
		// Update stages
		stage(player, 100)
		Quests.givePoints(player, 1)
		
		// Give coins, experience and the dialogue
		player.inventory().add(Item(995, 180), true)
		player.skills().__addXp(Skills.MINING, 1300.0)
		
		QuestGuide.clearCompletion(player)
		QuestGuide.setCompletionItem(player, 1269, 260)
		QuestGuide.setCompletionTitle(player, "You have completed Doric's Quest!")
		QuestGuide.setCompletionLines(player, "1 Quest Point", "1300 Mining XP", "180 coins", "Use of Doric's Anvils")
		QuestGuide.displayCompletion(player)
	}
	
}