package nl.bartpelle.veteres.content.items

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 20/04/2016.
 */
object HerbSack {
	
	@JvmStatic val HERBSACK = 13226
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(HERBSACK) { fillsack(it) } // Fill
		r.onItemOption3(HERBSACK) { emptysack(it) } // Empty
		r.onItemOption4(HERBSACK) { checksack(it) } // Check
	}
	
	// The item ids of grimy herbs allowed to be stored (total 14)
	val GRIMY = intArrayOf(199, 201, 203, 205, 207, 209, 211, 213, 215, 217, 219, 2485, 3049, 3051)
	
	// Fill the herb sack with grimy herbs from our inventory.
	fun fillsack(it: Script) {
		val player = it.player()
		GRIMY.forEach { grimy ->
			val inv = player.inventory().count(grimy)
			if (inv > 0) {
				var added = 0
				while (sackcount(player, grimy) < 30) {
					player.inventory().remove(Item(grimy), false)
					added++
					adjust_sack_stock(player, grimy, 1)
					if (player.inventory().count(grimy) == 0) {
						break
					}
				}
				player.message("You add " + added + " x " + player.world().definitions().get(ItemDefinition::class.java, grimy).name.toLowerCase() + " to your herb sack.")
			}
		}
	}
	
	// Empty the sack until we run out of inventory space.
	fun emptysack(it: Script) {
		val player = it.player()
		for (grimy: Int in GRIMY) {
			if (player.inventory().freeSlots() == 0) {
				player.message("You don't have any more space to hold any more.")
				break
			}
			if (sackcount(player, grimy) > 0) {
				var taken = 0
				while (player.inventory().freeSlots() > 0) {
					adjust_sack_stock(player, grimy, -1)
					player.inventory().add(Item(grimy, 1), false)
					taken++
					if (sackcount(player, grimy) == 0) {
						break
					}
				}
				player.message("You take " + taken + " x " + player.world().definitions().get(ItemDefinition::class.java, grimy).name.toLowerCase() + " out of the herb sack.")
			}
		}
	}
	
	// Tell the player how many herbs the sack has.
	fun checksack(it: Script) {
		val player = it.player()
		player.message("Your herb sack contains the following herbs:")
		GRIMY.forEach { grimy ->
			player.message("  " + sackcount(player, grimy) + " x " + player.world().definitions().get(ItemDefinition::class.java, grimy).name.toLowerCase() + ".")
		}
	}
	
	// How many of the given herb are in the sack currently.
	fun sackcount(player: Player, herbId: Int): Int {
		var key: AttributeKey = AttributeKey.HERBSACK1
		var shift = 0
		when (herbId) {
			199 -> {
				key = AttributeKey.HERBSACK1; /* no shift */
			} // 1st 6bits
			201 -> {
				key = AttributeKey.HERBSACK1; shift = 6
			} // 2nd 6-bit set
			203 -> {
				key = AttributeKey.HERBSACK1; shift = 12
			} // 3rd 6-bit set
			205 -> {
				key = AttributeKey.HERBSACK1; shift = 18
			} // 4th 6-bit set
			207 -> {
				key = AttributeKey.HERBSACK1; shift = 24
			} // 5th 6-bit set
			209 -> {
				key = AttributeKey.HERBSACK2; /* no shift */
			}
			211 -> {
				key = AttributeKey.HERBSACK2; shift = 6
			}
			213 -> {
				key = AttributeKey.HERBSACK2; shift = 12
			}
			215 -> {
				key = AttributeKey.HERBSACK2; shift = 18
			}
			217 -> {
				key = AttributeKey.HERBSACK2; shift = 24
			}
			219 -> {
				key = AttributeKey.HERBSACK3; /* no shift */
			}
			2485 -> {
				key = AttributeKey.HERBSACK3; shift = 6
			}
			3049 -> {
				key = AttributeKey.HERBSACK3; shift = 12
			}
			3051 -> {
				key = AttributeKey.HERBSACK3; shift = 18
			}
		}
		return player.attribOr<Int>(key, 0).shr(shift).and(31)
	}
	
	// Brilliant name?
	fun adjust_sack_stock(player: Player, herbId: Int, amount: Int) {
		var key: AttributeKey = AttributeKey.HERBSACK1
		var shift = 0
		when (herbId) {
			199 -> {
				key = AttributeKey.HERBSACK1; /* no shift */
			} // 1st 6bits
			201 -> {
				key = AttributeKey.HERBSACK1; shift = 6
			} // 2nd 6-bit set
			203 -> {
				key = AttributeKey.HERBSACK1; shift = 12
			} // 3rd 6-bit set
			205 -> {
				key = AttributeKey.HERBSACK1; shift = 18
			} // 4th 6-bit set
			207 -> {
				key = AttributeKey.HERBSACK1; shift = 24
			} // 5th 6-bit set
			209 -> {
				key = AttributeKey.HERBSACK2; /* no shift */
			}
			211 -> {
				key = AttributeKey.HERBSACK2; shift = 6
			}
			213 -> {
				key = AttributeKey.HERBSACK2; shift = 12
			}
			215 -> {
				key = AttributeKey.HERBSACK2; shift = 18
			}
			217 -> {
				key = AttributeKey.HERBSACK2; shift = 24
			}
			219 -> {
				key = AttributeKey.HERBSACK3; /* no shift */
			}
			2485 -> {
				key = AttributeKey.HERBSACK3; shift = 6
			}
			3049 -> {
				key = AttributeKey.HERBSACK3; shift = 12
			}
			3051 -> {
				key = AttributeKey.HERBSACK3; shift = 18
			}
		}
		val newAmt: Int = amount + sackcount(player, herbId)
		if (newAmt > 30) {
			// Should never happen, but for safety. This would overload the varbit.
			return
		}
		player.putattrib(key, updateValue(player.attribOr<Int>(key, 0), 31, newAmt, shift))
	}
	
	// Update a integer value with a new value contained within certain bits.
	fun updateValue(old: Int, b: Int, newval: Int, shift: Int): Int {
		//System.out.printf("old=%d, max_val=%d, new-val=%d, shift=%d", old, b, newval, shift) // debug
		val BITSIZE: Int = b.shl(shift)
		return (old.and(BITSIZE.inv())).or(BITSIZE.and(newval.shl(shift)))
		// old (1) & ~15 -> changes 0001 to 0000
		// now OR the other bits and the new value: 8 (maximum) and 1 << 0
		// old | 1 => updated
	}
	
}