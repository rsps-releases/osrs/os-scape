package nl.bartpelle.veteres.content.areas.alkharid.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Camel {
	
	val CAMELS = intArrayOf(5951, 2835)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (camel in CAMELS) {
			r.onNpcOption1(camel) @Suspendable {
				when (it.options("Ask the camel about its dung.", "Say something unpleasant.", "Neither - I'm a polite person.")) {
					1 -> {
						it.chatPlayer("I'm sorry to bother you, but could<br> you spare a little dung?", 567)
						it.messagebox("The camel didn't seem to appreciate that question.")
					}
					2 -> {
						it.chatPlayer("I wonder if that camel has flees..", 554)
						it.message("The camel spits at you, and you jump back hurriedly.")
					}
					3 -> it.message("You decide not to be rude.")
				}
			}
		}
	}
}