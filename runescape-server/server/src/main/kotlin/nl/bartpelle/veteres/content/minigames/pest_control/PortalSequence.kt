package nl.bartpelle.veteres.content.minigames.pest_control

import java.util.*

/**
 * Created by Jason MacKeigan on 2016-07-12 at 4:03 PM
 *
 * Each element of the enumeration represents an individual unique sequence of portals
 * that are ordered by default based on the required sequence.
 */
enum class PortalSequence(vararg val portals: PortalType) {
	BLUE_SEQUENCE_ONE(PortalType.BLUE, PortalType.RED, PortalType.YELLOW, PortalType.PURPLE),
	BLUE_SEQUENCE_TWO(PortalType.BLUE, PortalType.PURPLE, PortalType.RED, PortalType.YELLOW),
	PURPLE_SEQUENCE_ONE(PortalType.PURPLE, PortalType.BLUE, PortalType.YELLOW, PortalType.RED),
	PURPLE_SEQUENCE_TWO(PortalType.PURPLE, PortalType.YELLOW, PortalType.BLUE, PortalType.RED),
	YELLOW_SEQUENCE_ONE(PortalType.YELLOW, PortalType.RED, PortalType.PURPLE, PortalType.BLUE),
	YELLOW_SEQUENCE_TWO(PortalType.YELLOW, PortalType.PURPLE, PortalType.RED, PortalType.BLUE);
	
	/**
	 * Retrieves the next portal in the sequence.
	 */
	fun next(position: Int): PortalType {
		return if (position > portals.size - 1) last() else portals[position]
	}
	
	/**
	 * Retrieves the last portal in the sequence.
	 */
	fun last(): PortalType = portals[portals.size - 1]
	
	companion object {
		
		/**
		 * Retrieves a random sequence from the enumerator using the
		 * Random nextInt(int) function defined by the parameter.
		 */
		fun randomSequence(random: Random): PortalSequence {
			return values().get(random.nextInt(values().size - 1))
		}
	}
}