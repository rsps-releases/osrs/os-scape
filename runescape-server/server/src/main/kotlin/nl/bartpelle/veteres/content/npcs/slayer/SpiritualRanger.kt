package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 6/11/2016.
 */

object SpiritualRanger {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 7)) {
				if (EntityCombat.attackTimerReady(npc)) {
					// Attack the player
					npc.animate(npc.combatInfo().animations.attack)
					
					if (npc.id() != 2242)
						npc.graphic(18, 90, 0)
					
					npc.world().spawnProjectile(npc.tile(), target, get_projectile(npc.id()), get_height(npc.id()), 33, get_delay(npc.id()), 5 * npc.tile().distance(target.tile()), 11, 105)
					
					// Does NOT splash when miss!
					if (AccuracyFormula.doesHit(npc, target, CombatStyle.RANGE, 1.0)) {
						target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(CombatStyle.RANGE) // Cannot protect from this.
					} else {
						target.hit(npc, 0, 1).combatStyle(CombatStyle.RANGE) // Cannot protect from this.
					}
					
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun get_projectile(npc: Int): Int {
		return when (npc) {
			2211 -> 9
			3160 -> 9
			2242 -> 1197
			else -> 9
		}
	}
	
	@Suspendable fun get_height(npc: Int): Int {
		return when (npc) {
			2211 -> 45
			2242 -> 30
			else -> 1197
		}
	}
	
	@Suspendable fun get_delay(npc: Int): Int {
		return when (npc) {
			2211 -> 40
			3160 -> 40
			2242 -> 20
			else -> 40
		}
	}
}