package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.LocationUtilities

/**
 * Created by Situations on 11/9/2015.
 */

object MageBankLever {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//Outside magebank
		repo.onObject(5959) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			//Check to see if the player is teleblocked
			player.faceTile(obj.tile())
			if (!player.timers().has(TimerKey.TELEBLOCK)) {
//				DeadmanMechanics.attemptTeleport(it)
				player.lock()
				it.delay(1)
				it.animate(2140)
				it.message("You pull the lever...")
				
				it.runGlobal(player.world()) @Suspendable {
					val world = player.world()
					val spawned = MapObj(obj.tile(), 5961, obj.type(), obj.rot())
					world.spawnObj(spawned)
					it.delay(6)
					world.removeObjSpawn(spawned)
				}
				
				it.delay(2)
				it.animate(714)
				player.graphic(111, 110, 0)
				it.delay(4)
                var targetTile = Tile(2539, 4712)
                if (MageBankInstance.mageBank!!.contains(player)) {
                   targetTile = LocationUtilities.dynamicTileFor(targetTile, MageBankInstance.mageBankSafe)
                }
				player.teleport(targetTile)
				it.animate(-1)
				player.unlock()
				it.message("...And teleport into the mage's cave.")
				if (!player.world().realm().isDeadman) // So we can tele straight away
					player.clearattrib(AttributeKey.LAST_WAS_ATTACKED_TIME)
			} else {
				it.player().teleblockMessage()
			}
		}
		//Inside magebank.. to outside
		repo.onObject(5960, s@ @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			if (player.privilege() != Privilege.ADMIN) {
				if (player.inventory().count(6685) > 18) {
					player.message("" + player.inventory().count(6685) + " brews is a little excessive don't you think?")
					return@s
				}
			}
			
			//Check to see if the player is teleblocked
			if (!player.timers().has(TimerKey.TELEBLOCK)) {
				player.lockNoDamage()
				player.faceTile(obj.tile())
				it.delay(1)
				it.animate(2140)
				it.message("You pull the lever...")
				
				it.runGlobal(player.world()) @Suspendable {
					it.delay(1)
					val world = player.world()
					val spawned = MapObj(obj.tile(), 5961, obj.type(), obj.rot())
					world.spawnObj(spawned)
					it.delay(5)
					world.removeObjSpawn(spawned)
				}
				
				it.delay(2)
				it.animate(714)
				player.graphic(111, 110, 0)
				it.delay(4)
				var targetTile = Tile(3090, 3956)
				if (MageBankInstance.mageBankSafe!!.contains(player)) {
					targetTile = LocationUtilities.dynamicTileFor(targetTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)
				}
				player.teleport(targetTile)
				it.animate(-1)
				player.unlock()
				it.message("...And teleport out of the mage's cave.")
			} else {
				it.player().teleblockMessage()
			}
		})
	}
	
}
