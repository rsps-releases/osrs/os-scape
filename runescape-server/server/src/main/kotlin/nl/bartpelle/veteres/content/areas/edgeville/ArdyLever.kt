package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/9/2015.
 */

object ArdyLever {
	
	val LEVER = 1814
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(LEVER, s@ @Suspendable {
			
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			it.player().faceTile(Tile(2560, 3311))
			if (player.privilege() != Privilege.ADMIN) {
				if (player.inventory().count(6685) > 18) {
					player.message("" + player.inventory().count(6685) + " brews is a little excessive don't you think?")
					return@s
				}
			} else {
				player.message("As an admin you bypass pk-tele restrictions.")
			}
			
			it.player().lockNoDamage()
			it.delay(1)
			it.animate(2140)
			it.message("You pull the lever...")
			
			it.runGlobal(player.world()) @Suspendable {
				it.delay(1)
				val world = player.world()
				val spawned = MapObj(obj.tile(), 88, obj.type(), obj.rot())
				world.spawnObj(spawned)
				it.delay(5)
				world.removeObjSpawn(spawned)
			}
			
			it.delay(2)
			it.animate(714)
			it.player().graphic(111, 110, 0)
			it.delay(4)
			player.teleport(3154, 3924)
			it.animate(-1)
			it.player().unlock()
			it.message("...And teleport into the wilderness.")
		})
	}
	
}
