package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.itemUsedId

import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.skills.farming.Farming
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 7/18/2016.
 */
object WaterContainers {
	
	// Fountains
	val VARROCK_EAST_FOUNTAIN = 5125
	val VARROCK_CENTRAL_FOUNTAIN = 7143
	val VARROCK_PALACE_FOUNTAIN = 3641
	val FALADOR_FOUNTAIN = 24102
	val GLOBAL_FOUNTAIN = 879
	
	// Sinks
	val EDGEVILLE_SINK = 12279
	val COOKING_BUILD_SINK = 1763
	val FALADOR_SINK = 873
	val PORT_SARIM_SINK = 874
	val FALADOR_FARM_SINK = 8699
	val CRAFTING_GUILD_SINK = 9684
	val LUMBRIDGE_COOK_SINK = 14868
	val LUMBRIDGE_FARM_SINK = 12974
	val SEERS_VILLAGE_SINK = 25929
	
	// Wells
	val GLOBAL_WELL = 884
	
	// Empty items
	val EMPTY_VIAL = 229
	val EMPTY_BUCKET = 1925
	val EMPTY_BOWL = 1923
	val EMPTY_JUG = 1935
	
	// Collections
	val FOUNTAINS = intArrayOf(VARROCK_EAST_FOUNTAIN, VARROCK_CENTRAL_FOUNTAIN, VARROCK_PALACE_FOUNTAIN,
			FALADOR_FOUNTAIN, GLOBAL_FOUNTAIN)
	val SINKS = intArrayOf(EDGEVILLE_SINK, COOKING_BUILD_SINK, FALADOR_SINK, PORT_SARIM_SINK, FALADOR_FARM_SINK,
			CRAFTING_GUILD_SINK, LUMBRIDGE_COOK_SINK, LUMBRIDGE_FARM_SINK, SEERS_VILLAGE_SINK)
	val WELLS = intArrayOf(GLOBAL_WELL)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Items on fountains
		for (fountain in FOUNTAINS) {
			r.onItemOnObject(fountain) @Suspendable {
				val item = it.itemUsedId()
				val slot: Int = it.player().attrib(AttributeKey.ITEM_SLOT)
				
				when (item) {
					EMPTY_VIAL -> fillContainers(it, item, 227, "You fill the vial from the fountain.")
					EMPTY_BUCKET -> fillContainers(it, item, 1929, "You fill the bucket from the fountain.")
					EMPTY_BOWL -> fillContainers(it, item, 1921, "You fill the bowl from the fountain.")
					EMPTY_JUG -> fillContainers(it, item, 1937, "You fill the jug from the fountain.")
					in Farming.WATERING_CANS -> it.player().message("The watering can is already full.")
					Farming.EMPTY_CAN -> {
						it.player().animate(832)
						if (it.player().inventory().remove(Item(Farming.EMPTY_CAN), true, slot).success()) {
							it.player().inventory().add(Item(5340), true, slot)
						}
						it.player().message("You fill the watering can from the fountain.")
					}
					else -> it.player().message("Nothing interesting happens.")
				}
			}
		}
		
		// Items on sinks
		for (fountain in SINKS) {
			r.onItemOnObject(fountain) @Suspendable {
				val item = it.itemUsedId()
				val slot: Int = it.player().attrib(AttributeKey.ITEM_SLOT)
				when (item) {
					EMPTY_VIAL -> fillContainers(it, item, 227, "You fill the vial from the sink.")
					EMPTY_BUCKET -> fillContainers(it, item, 1929, "You fill the bucket from the sink.")
					EMPTY_BOWL -> fillContainers(it, item, 1921, "You fill the bowl from the sink.")
					EMPTY_JUG -> fillContainers(it, item, 1937, "You fill the jug from the sink.")
					in Farming.WATERING_CANS -> it.player().message("The watering can is already full.")
					Farming.EMPTY_CAN -> {
						it.player().animate(832)
						if (it.player().inventory().remove(Item(Farming.EMPTY_CAN), true, slot).success()) {
							it.player().inventory().add(Item(5340), true, slot)
						}
						it.player().message("You fill the watering can from the sink.")
					}
					else -> it.player().message("Nothing interesting happens.")
				}
			}
		}
		
		// Items on wells
		for (fountain in WELLS) {
			r.onItemOnObject(fountain) @Suspendable {
				val item = it.itemUsedId()
				val slot: Int = it.player().attrib(AttributeKey.ITEM_SLOT)
				when (item) {
					EMPTY_VIAL -> it.message("If I drop my vial down there, I don't think I'm likely to get it back.")
					EMPTY_BUCKET -> fillContainers(it, item, 1929, "You fill the bucket from the well.")
					EMPTY_BOWL -> it.message("If I drop my bowl down there, I don't think I'm likely to get it back.")
					EMPTY_JUG -> it.message("If I drop my jug down there, I don't think I'm likely to get it back.")
					in Farming.WATERING_CANS -> it.player().message("The watering can is already full.")
					Farming.EMPTY_CAN -> it.message("If I drop my watering can down there, I don't think I'm likely to get it back.")
					else -> it.player().message("Nothing interesting happens.")
				}
			}
		}
	}
	
	@Suspendable fun fillContainers(it: Script, item: Int, result: Int, text: String) {
		it.message(text)
		
		while (true) {
			if (item !in it.player.inventory()) {
				break
			}
			
			it.player.animate(832)
			it.player.inventory().remove(Item(item), true)
			it.player.inventory().add(Item(result), true)
			it.delay(1)
		}
	}
	
}