package nl.bartpelle.veteres.content.areas.karamja

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.mechanics.CharterShips
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/11/2016.
 */

object TraderCrewmember {
	
	val TRADER_CREWMEMBER = intArrayOf(1331, 1333, 1334, 1328) //Male, female
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (Crewmember in TRADER_CREWMEMBER) {
			r.onNpcOption1(Crewmember) @Suspendable {
				CharterShips.setClosestSourcePoint(it)
				it.chatNpc("Can I help you?", Crewmember, 554)
				when (it.options("Yes, who are you?", "Yes, I would like to charter a ship.")) {
					1 -> who_are_you(it, Crewmember)
					2 -> charter_ship(it, Crewmember)
				}
			}
			r.onNpcOption2(Crewmember) @Suspendable {
				if (!it.player().world().realm().isPVP)
					it.player().world().shop(36).display(it.player())
			}
			r.onNpcOption3(Crewmember) @Suspendable {
				CharterShips.setClosestSourcePoint(it)
				CharterShips.sendOptionsInterface(it)
			}
		}
	}
	
	@Suspendable fun who_are_you(it: Script, Crewmember: Int) {
		it.chatPlayer("Yes, who are you?", 554)
		it.chatNpc("I'm one of Trader Stan's crew; we are all part of the<br>largest fleet of trading and sailing vessels to ever sail<br>the seven seas.", Crewmember, 569)
		it.chatNpc("If you want to get to a port in a hurry then you can<br>charter one of our ships to take you there - if the price<br>is right...", Crewmember, 590)
		it.chatPlayer("So, where exactly can I go with your ships?", 554)
		it.chatNpc("We run ships from Port Phasmatys over to Port<br>Tyras, stopping at Port Sarim, Catherby, Brimhaven,<br>Karamja, the Shipyard and Port Khazard.", Crewmember, 590)
		it.chatNpc("We might dock at Mos Le'Harmless once in a while, as<br>well, if you catch my meaning...", Crewmember, 593)
		it.chatPlayer("Wow, that's a lot of ports. I take it you have some<br>exotic stuff to trade?", 589)
		it.chatNpc("We certainly do! We have access to items bought and<br>sold from around the world.", Crewmember, 568)
		it.chatNpc("Would you like to take a look?", Crewmember, 554)
		when (it.options("Yes", "No", "Isn't it tricky to sail about in those clothes?")) {
			1 -> {
				it.chatPlayer("Yes.", 588)
				it.player().world().shop(36).display(it.player())
			}
			2 -> no(it, Crewmember)
			3 -> tricky_to_sail(it, Crewmember)
		}
	}
	
	@Suspendable fun tricky_to_sail(it: Script, Crewmember: Int) {
		it.chatPlayer("Isn't it tricky to sail about in those clothes?", 554)
		it.chatNpc("Tricky? Tricky!", Crewmember, 571)
		it.chatNpc("With all due credit, Trader Stan is a great employer,<br>but he insists we wear the latest in high fashion even<br>when sailing.", Crewmember, 590)
		it.chatNpc("Do you have even the slightest idea how tricky it is to<br>sail in this stuff?", Crewmember, 589)
		it.chatNpc("Some of us tried tearing it and arguing that it was too<br>fragile to wear when on a boat, but he just had it<br>enchanted to re-stitch itself.", Crewmember, 594)
		it.chatNpc("It's hard to hate him when we know how much he shells<br>out on this gear, but if I fall overboard because of this<br>getup one more time, I'm going to quit.", Crewmember, 612)
		it.chatPlayer("Wow, that's kind of harsh.", 588)
	}
	
	@Suspendable fun no(it: Script, Crewmember: Int) {
		it.chatPlayer("No.", 588)
		it.chatNpc("Well, that's fine; would you like to charter a ship<br>instead?", Crewmember, 589)
		if (it.options("Yes, I would like to charter a ship.", "No.") == 1) {
			it.chatPlayer("Yes, I would like to charter a ship.", 588)
			it.chatNpc("Certainly sir, where would you like to go?", Crewmember, 588)
			CharterShips.sendOptionsInterface(it)
		}
	}
	
	@Suspendable fun charter_ship(it: Script, Crewmember: Int) {
		it.chatPlayer("Yes, I would like to charter a ship.", 588)
		it.chatNpc("Certainly sir, where would you like to go?", Crewmember, 588)
		CharterShips.sendOptionsInterface(it)
	}
}
