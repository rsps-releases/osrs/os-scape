package nl.bartpelle.veteres.content.skills.hunter

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Tuple
import java.util.*

/**
 * Created by Jak on 15/06/2016.
 * Note: the Hunter AttributeKey, in the case of bird hunting, only uses one bit. Chinchompa hunting used ~4 bits to
 * determine the type of loot/chinchompa you were hunting in addition to entity interaction, but birds have different
 * object ids which you can use instead to determine what type of bird is being hunted, and therefore the loot.
 */
object BirdSnaring {
	
	val SNARE_ITEM = 10006
	val SNARE_OBJECT = 9345
	val SNARE_OBJECT_FALLING = 9346
	val SNARE_OBJECT_FAILED = 9344
	
	val ANIM_DISMANTLE_TRAP = 5207
	val ANIM_SETUP_TRAP = 5208 // Same as chin setup anim - looks like firelighting
	
	// Npc id, trapping object, caught object
	enum class BIRDS(val npc: Int, val trapping: Int, val caught: Int, val xp: Double, val feather: Int, val reqlevel: Int) {
		SWIFT(5549, 9349, 9373, 34.0, 10088, 1),
		WARBLERS(5551, 9376, 9377, 47.0, 10090, 5),
		LONGTAILS(5552, 9378, 9379, 61.0, 10091, 9),
		TWITCHES(5550, 9374, 9375, 64.5, 10089, 11),
		WAGTAILS(5548, 9347, 9348, 95.0, 10087, 19);
		
		companion object {
			fun birdForNpc(npcid: Int): BIRDS? {
				for (bird in BIRDS.values()) {
					if (bird.npc == npcid)
						return bird
				}
				return null
			}
			
			fun birdForCaughtObject(objid: Int): BIRDS? {
				for (bird in BIRDS.values()) {
					if (bird.caught == objid)
						return bird
				}
				return null
			}
		}
	}
	
	// Hooks
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOption1(SNARE_ITEM) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.HUNTER)) {
				it.player().stopActions(true)
				laytrap(it)
				it.player().clearattrib(AttributeKey.INTERACTION_OBJECT)
			}
		}
		repo.onGroundItemOption2(SNARE_ITEM) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.HUNTER)) {
				it.player().stopActions(true)
				laytrap_onground(it)
				it.player().clearattrib(AttributeKey.INTERACTED_GROUNDITEM)
			}
		}
		repo.onObject(SNARE_OBJECT) @Suspendable {
			val option = it.interactionOption()
			if (option == 1) {
				dismantle(it)
			} else if (option == 2) {
				investigate(it)
			}
		}
		repo.onObject(SNARE_OBJECT_FAILED) @Suspendable {
			dismantle(it)
		}
		BIRDS.values().forEach { bird ->
			repo.onObject(bird.caught) @Suspendable {
				collect(it)
			}
		}
		BIRDS.values().forEach { bird ->
			Chinchompas.HUNTER_NPCS.add(bird.npc)
			repo.onNpcSpawn(bird.npc) @Suspendable {
				val npc = it.npc()
				it.clearContext()
				while (npc.index() != -1) {
					BirdSnaring.birdlogic(it, npc)
				}
			}
		}
	}
	
	/*
	 * MAIN LOGIC
	 */
	
	@Suspendable private fun laytrap_onground(it: Script) {
		val item = it.interactionGrounditem()
		val world = it.player().world()
		if (!it.player().tile().equals(item.tile())) {
			it.player().walkTo(item.tile(), PathQueue.StepType.FORCED_WALK)
			it.waitForTile(item.tile())
		}
		if (cannotLay(it)) {
			return
		}
		it.animate(ANIM_SETUP_TRAP)
		it.delay(2)
		if (item.valid(world) && world.objByType(10, item.tile()) == null) {
			if (world.removeGroundItem(item)) {
				val trap = world.spawnObj(MapObj(item.tile(), SNARE_OBJECT, 10, 0))
				trap.putattrib(AttributeKey.OWNING_PLAYER, Tuple(it.player().id(), it.player()))
				traps(it.player()).add(trap)
				addtimeout(world, it.player(), trap)
				it.delay(1)
				it.player().walkTo(item.tile().transform(-1, 0), PathQueue.StepType.FORCED_WALK)
				it.delay(1)
				it.player().faceTile(item.tile())
			}
		}
	}
	
	val BONES = 526
	val BIRD_MEAT = 9978
	
	@Suspendable private fun collect(it: Script) {
		val obj = it.interactionObject()
		if (obj.owner(it.player().world()) != it.player) {
			it.message("This trap does not belong to you.")
			return
		}
		val bird = BIRDS.birdForCaughtObject(obj.id()) ?: return
		var req = 3 // Trap, meat, bones
		if (it.player().inventory().count(bird.feather) == 0) req += 1 // We need extra space to hold feathers
		if (it.player().inventory().freeSlots() < req) {
			it.player().message("You don't have enough inventory space to carry the loot.")
			return
		}
		it.player().lockDelayDamage()
		it.player().faceObj(obj)
		it.animate(ANIM_DISMANTLE_TRAP)
		it.delay(2)
		if (obj.valid(it.player().world())) {
			it.player().world().removeObj(obj)
			traps(it.player()).remove(obj)
			
			it.player().skills().__addXp(Skills.HUNTER, bird.xp)
			it.player().inventory().add(Item(SNARE_ITEM), true)
			it.player().inventory().add(Item(bird.feather), true)
			it.player().inventory().add(Item(BIRD_MEAT), true)
			it.player().inventory().add(Item(BONES), true)
			it.message("You dismantle the trap.")
		}
		it.player().unlock()
	}
	
	@Suspendable private fun investigate(it: Script) {
		it.player().message("This trap is empty.")
	}
	
	@Suspendable private fun dismantle(it: Script) {
		val obj = it.interactionObject()
		if (obj.owner(it.player().world()) != it.player) {
			it.message("This trap does not belong to you.")
			return
		}
		val player = it.player()
		val world = player.world()
		player.animate(ANIM_DISMANTLE_TRAP)
		player.faceObj(obj)
		it.delay(2)
		if (obj.valid(world)) {
			world.removeObj(obj)
			traps(player).remove(obj)
			player.inventory().add(Item(SNARE_ITEM), false)
			player.animate(-1)
		}
	}
	
	@Suspendable private fun laytrap(it: Script) {
		val world = it.player().world()
		if (cannotLay(it)) {
			return
		}
		if (it.player().inventory().remove(it.itemUsed(), true, it.itemUsedSlot()).success()) {
			val item = world.spawnGroundItem(GroundItem(world, Item(SNARE_ITEM), it.player().tile(), it.player().id()))
			it.animate(ANIM_SETUP_TRAP)
			it.delay(2)
			if (item.valid(world) && world.objByType(10, item.tile()) == null) {
				if (world.removeGroundItem(item)) {
					val trap = world.spawnObj(MapObj(item.tile(), SNARE_OBJECT, 10, 0))
					trap.putattrib(AttributeKey.OWNING_PLAYER, Tuple(it.player().id(), it.player()))
					traps(it.player()).add(trap)
					addtimeout(world, it.player(), trap)
					it.delay(1)
					it.player().walkTo(item.tile().transform(-1, 0), PathQueue.StepType.FORCED_WALK)
					it.delay(1)
					it.player().faceTile(item.tile())
				}
			}
		}
	}
	
	// Is this tile available to place a trap?
	private fun cannotLay(it: Script): Boolean {
		if (it.player().world().objByType(10, it.player().tile()) != null) {
			it.message("There is already something placed here.")
			return true
		}
		if (it.player().attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			it.player().message("You can't do that in here.")
			return true
		}
		if (activeBirdSnares(it.player()) >= Chinchompas.maxActiveTraps(it.player())) {
			it.player().message("You can't place any more traps.")
			it.player().debug("%d active.", activeBirdSnares(it.player()))
			return true
		}
		return false
	}
	
	// How many snares are setup right now?
	private fun activeBirdSnares(player: Player): Int {
		var active = 0
		traps(player).forEach { trap ->
			// Snares which failed/have loot don't count as active.
			if (trap.valid(player.world()) && trap.id() == BirdSnaring.SNARE_OBJECT) {
				active++
			}
		}
		return active
	}
	
	// The list of traps as mapobjects
	private fun traps(player: Player): ArrayList<MapObj> {
		val map = player.attribOr<ArrayList<MapObj>>(AttributeKey.BIRDSNARE_LIST, java.util.ArrayList<MapObj>())
		player.putattrib(AttributeKey.BIRDSNARE_LIST, map)
		return map
	}
	
	// What actually happens each bird AI cycle
	@JvmStatic @Suspendable fun birdlogic(it: Script, npc: Npc) {
		val world = npc.world()
		val bird = BIRDS.birdForNpc(npc.id()) ?: return
		//npc.sync().shout("weeee")
		it.delay(4 + world.random(2))
		
		var targTile: Tile? = null
		var trap: MapObj? = null
		
		// Locate and path to trap
		for (obj in npc.world().spawnedObjs()) {
			if (obj.id() == SNARE_OBJECT && obj.tile().distance(npc.tile()) <= 5) {
				val owner: Player = obj.owner(world) ?: continue
				if (owner.skills().xpLevel(Skills.HUNTER) < bird.reqlevel) continue
				if (obj.attribOr<Int>(AttributeKey.HUNTER_INFO, 0).and(1) == 0) {
					// flag that an NPC is pathing towards this box. don't like 5 in 1 box ;)
					obj.putattrib(AttributeKey.HUNTER_INFO, 1)
					// Don't random walk during this stage.
					npc.putattrib(AttributeKey.HUNTER_INFO, 1)
					npc.faceObj(obj)
					npc.walkTo(obj.tile(), PathQueue.StepType.FORCED_WALK)
					targTile = obj.tile() // Has to be the exact tile, not closest.
					if (owner.privilege().eligibleTo(Privilege.ADMIN)) {
						npc.sync().shout("TARGET IDENTIFIED")
						owner.message("Npc %d tiles away pathing to trap...", npc.tile().distance(obj.tile()))
						owner.write(SetHintArrow(npc))
					}
					trap = obj
					break
				}
			}
		}
		
		while (targTile != null && trap != null && !npc.dead() && !npc.frozen() && trap.valid(world)) {
			//npc.sync().shout("what is dis?")
			if (!npc.tile().equals(targTile)) {
				// Must be walking towards.
				it.delay(1)
			} else {
				// We've reached the trap, hover above for a sec
				it.delay(1)
				
				// Now face north before decending
				npc.lockNoDamage()
				npc.faceTile(npc.tile().transform(0, 1))
				it.delay(1)
				
				if (!trap.valid(world)) {
					npc.unlock()
					break
				}
				// Bird flys down onto the trap
				npc.animate(5171)
				it.delay(1)
				trap.interactAble(false)
				val owner: Player = trap.owner(world) ?: break
				val success = sucessfulBirdCatch(npc, owner)
				
				if (success) {
					if (!trap.valid(world)) {
						npc.unlock()
						break
					}
					// npc.sync().shout("caught")
					
					// Respawn npc
					npc.hidden(true)
					npc.teleport(npc.spawnTile())
					npc.faceTile(Chinchompas.tileForDir(npc))
					npc.hp(npc.maxHp(), 0) // Heal up to full hp
					npc.animate(-1) // Reset death animation
					npc.clearDamagers() //Clear damagers
					npc.clearDamageTimes()
					npc.clearattrib(AttributeKey.TARGET)
					npc.putattrib(AttributeKey.HUNTER_INFO, 0)
					
					// Reset npc
					it.runGlobal(world) @Suspendable { s ->
						s.clearContext()
						s.delay(8)
						npc.hidden(false)
						npc.unlock()
					}
					
					// Despawn trap
					world.removeObj(trap)
					trap.clearattrib(AttributeKey.OWNING_PLAYER)
					traps(owner).remove(trap)
					
					val falling: MapObj = world.spawnObj(MapObj(trap.tile(), bird.trapping, trap.type(), trap.rot()))
					// Start falling
					it.runGlobal(world) @Suspendable { s ->
						s.clearContext()
						s.delay(1)
						world.removeObj(falling)
						
						// Spawn successfully trapped bird object
						val fallen = world.spawnObj(MapObj(falling.tile(), bird.caught, falling.type(), falling.rot()))
						fallen.putattrib(AttributeKey.OWNING_PLAYER, Tuple(owner.id(), owner))
						traps(owner).add(fallen)
						addtimeout(world, owner, fallen)
					}
				} else {
					//npc.sync().shout("escaped")
					npc.animate(5172)
					// Reset npc hunting info
					npc.putattrib(AttributeKey.HUNTER_INFO, 0)
					npc.unlock()
					if (!trap.valid(world)) {
						break
					}
					// Despawn trap
					world.removeObj(trap)
					trap.clearattrib(AttributeKey.OWNING_PLAYER)
					traps(owner).remove(trap)
					
					// Spawn falling, failed trap
					val falling: MapObj = world.spawnObj(MapObj(trap.tile(), SNARE_OBJECT_FALLING, trap.type(), trap.rot()))
					
					// Spawn failed
					it.runGlobal(world) @Suspendable { s ->
						s.clearContext()
						s.delay(2)
						world.removeObj(falling)
						val fallen = world.spawnObj(MapObj(falling.tile(), SNARE_OBJECT_FAILED, falling.type(), falling.rot()))
						fallen.putattrib(AttributeKey.OWNING_PLAYER, Tuple(owner.id(), owner))
						world.spawnObj(fallen)
						traps(owner).add(fallen)
						addtimeout(world, owner, fallen)
					}
				}
				return
			}
		}
		if (trap != null) {
			trap.putattrib(AttributeKey.HUNTER_INFO, 0) // Set trap uninteracted with
			npc.putattrib(AttributeKey.HUNTER_INFO, 0) // Allow new target finding
		}
	}
	
	@JvmStatic val TRAP_TIMEOUT = 500
	
	private fun addtimeout(world: World, owner: Player, fallen: MapObj) {
		fallen.putattrib(AttributeKey.MAPOBJ_UUID, MapObj.INCREMENTING_MAPOBJ_UUID++)
		world.server().scriptExecutor().executeScript(world, @Suspendable { s ->
			s.delay(TRAP_TIMEOUT)
			var attempts = 0
			
			while (inuse(fallen, world)) {
				s.delay(5)
				attempts++
				if (attempts >= 3) {
					break
				}
			}
			
			if (fallen.valid(world, true)) {
				world.removeObj(fallen)
				traps(owner).remove(fallen)
				fallen.clearattrib(AttributeKey.HUNTER_INFO)
				fallen.clearattrib(AttributeKey.OWNING_PLAYER)
				world.spawnGroundItem(GroundItem(world, Item(SNARE_ITEM), fallen.tile(), owner.id()))
			}
		})
	}
	
	// Don't expire during this period.
	private fun inuse(fallen: MapObj, world: World): Boolean {
		// The owner is picking it up!
		val owner: Player? = fallen.owner(world)
		if (owner != null) {
			val objtarg: MapObj? = owner.attribOr(AttributeKey.INTERACTION_OBJECT, null)
			if (objtarg != null && objtarg == fallen) {
				return true
			}
		}
		// Check if an npc is interacting with this trap
		if (fallen.attribOr<Int>(AttributeKey.HUNTER_INFO, 0).and(1) == 1) {
			println("Literally in fucking use?")
			return true
		}
		return false
	}
	
	@JvmStatic fun onLogout(player: Player) {
		traps(player).forEach { trap ->
			if (trap.valid(player.world())) {
				player.world().removeObj(trap)
				trap.clearattrib(AttributeKey.HUNTER_INFO)
				trap.clearattrib(AttributeKey.OWNING_PLAYER)
				player.world().spawnGroundItem(GroundItem(player.world(), Item(SNARE_ITEM), trap.tile(), player.id()))
			}
		}
		traps(player).clear()
	}
	
	private fun sucessfulBirdCatch(npc: Npc, player: Player): Boolean {
		var successThreshold = 30
		val mylvl = player.skills().level(Skills.HUNTER)
		val entry = BIRDS.birdForNpc(npc.id()) ?: return false
		val req: Int = entry.reqlevel
		val boosts = Chinchompas.boosts(player)
		val baseMaxLevel = 99
		val levelGap: Int = baseMaxLevel - req
		val overExperienced = Math.max(1, mylvl - req)
		val percentageGapSucess: Double = (100 - successThreshold) / levelGap.toDouble() // 70 / 46 diff = 1.5% more success per level
		val overSuccess = ((overExperienced * percentageGapSucess) * boosts).toInt()
		successThreshold += overSuccess // Maximum gain should be 70, which combined with threshold of 30 = 100% success rate.
		
		// Max success 95%
		// I'm sorry jak, this code is unreadable so I literally put my logic where it fits.
		val roll = npc.world().random(if (BonusContent.isActive(player, BlessingGroup.PREDATOR)) 85 else 100)
		player.debug("Rolled %d/%d. Success threshold:%d.  Cap %d%%, lowest %d%%. Max base level:%d.  Gap:%d.  %% success per level:%f.  Over-level:%d Boosts=%f Over-sucess:%d",
				roll, 100, successThreshold, 95, 30, baseMaxLevel, levelGap, percentageGapSucess, overExperienced, boosts, overSuccess)
		if (successThreshold > 95) successThreshold = 95
		return roll < successThreshold
	}
}