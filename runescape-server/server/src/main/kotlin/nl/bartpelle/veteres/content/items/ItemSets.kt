package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.EnumDefinition
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jak on 24/01/2016.
 */

object ItemSets {
	
	data class ItemSet(val boxedset: Int, val items: Array<Int>)
	
	@JvmStatic val SET_SIZES = intArrayOf(
			4, 3, 3, 3, 3, 4, 4, 4,
			4, 4, 4, 3, 3, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4,
			4, 6, 3, 3
	)
	
	@JvmStatic var ITEM_SETS: HashMap<Int, ItemSet> = HashMap()
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		reloadSets(repo.server().world())
		
		// Pack box set available on interface
		repo.onButton(451, 2, s@ {
			val slot = it.player().attrib<Int>(AttributeKey.BUTTON_SLOT)
			
			println(slot)
			if (ITEM_SETS[slot] != null) {
				if (slot < SET_SIZES.size) {
					it.message("Set size: ${SET_SIZES[slot]}")
					itemSetsToItems(it, ITEM_SETS[slot]!!.boxedset,
							ITEM_SETS[slot]!!.items[0],
							ITEM_SETS[slot]!!.items[1],
							ITEM_SETS[slot]!!.items[2],
							if (SET_SIZES[slot] >= 4) ITEM_SETS[slot]!!.items[3] else -1,
							if (SET_SIZES[slot] >= 5) ITEM_SETS[slot]!!.items[4] else -1,
							if (SET_SIZES[slot] >= 6) ITEM_SETS[slot]!!.items[5] else -1)
				} else {
					it.message("Bad set slot")
				}
			} else {
				it.message("Bad set.")
			}
		})
		
		// Unpack box set in inventory
		repo.onButton(430, 0, s@ {
			val slot = it.player().attrib<Int>(AttributeKey.BUTTON_SLOT)
			val item = it.player().attrib<Int>(AttributeKey.ITEM_ID)
			
			ITEM_SETS.forEach { map ->
				val set = map.value
				if (set.boxedset == item) {
					val size = set.items.size
					itemsToItemSets(it, set.items.size, set.boxedset, set.items[0], set.items[1], set.items[2],
							if (size >= 4) set.items[3] else -1,
							if (size >= 5) set.items[4] else -1,
							if (size >= 6) set.items[5] else -1)
				}
			}
			
		})
	}
	
	@JvmStatic fun reloadSets(world: World) {
		ITEM_SETS.clear()
		
		val list = world.definitions().get(EnumDefinition::class.java, 1033)
		list.enums().forEach { entry ->
			val set = entry.value as Int
			val components = world.definitions().get(EnumDefinition::class.java, set)
			
			val componentList = ArrayList<Int>()
			components.enums().forEach { component ->
				if (component.key != -1) {
					componentList.add(component.value as Int)
				}
			}
			
			ITEM_SETS[entry.key] = ItemSet(components.getInt(-1), componentList.toTypedArray())
			val name = world.definitions().get(ItemDefinition::class.java, components.getInt(-1)).name ?: "?"
			//System.out.printf("Item set from enum: %d (%s) => %s%n", components.getInt(-1), name, Arrays.toString(componentList.toTypedArray()))
		}
		
		// Not on the 07 interface. Add to the array so it is possible to open them via using on bank or "Open" item option
		ITEM_SETS[9668] = ItemSet(9668, arrayOf(5574, 5575, 5576)) // initiate
		ITEM_SETS[9670] = ItemSet(9670, arrayOf(9672, 9674, 9678)) // proselyte m
		ITEM_SETS[9666] = ItemSet(9666, arrayOf(9672, 9674, 9676)) // proselyte f
	}
	
	@Suspendable fun itemSetsToItems(it: Script, boxed_set: Int, item1: Int, item2: Int, item3: Int, item4: Int = -1, item5: Int = -1, item6: Int = -1) {
		
		var hasAll = it.player().inventory().hasAll(item1, item2, item3)
		if (item6 != -1) {
			hasAll = it.player().inventory().hasAll(item1, item2, item3, item4, item5, item6)
		} else if (item5 != -1) {
			hasAll = it.player().inventory().hasAll(item1, item2, item3, item4, item5)
		} else if (item4 != -1) {
			hasAll = it.player().inventory().hasAll(item1, item2, item3, item4)
		}
		
		if (hasAll) {
			it.player().inventory().remove(Item(item1), true)
			it.player().inventory().remove(Item(item2), true)
			it.player().inventory().remove(Item(item3), true)
			if (item4 != -1)
				it.player().inventory().remove(Item(item4), true)
			if (item5 != -1)
				it.player().inventory().remove(Item(item5), true)
			if (item6 != -1)
				it.player().inventory().remove(Item(item6), true)
			
			it.player().inventory().add(Item(boxed_set), true)
		} else {
			val sb = StringBuilder()
			val len = if (item6 != -1) 5 else if (item5 != -1) 4 else if (item4 != -1) 3 else 2
			for (i in 0..len) {
				val id = intArrayOf(item1, item2, item3, item4, item5, item6)[i]
				val name = "<col=ff0000>${it.player().world().definitions().get(ItemDefinition::class.java, id)?.name ?: "?"}</col>"
				sb.append(name)
				if (i < len)
					sb.append(", ")
			}
			it.player().message("This set consists of:")
			it.player().message(sb.toString() + ".")
		}
	}
	
	@Suspendable fun itemsToItemSets(it: Script, amount_of_items: Int, boxed_set: Int, item1: Int, item2: Int, item3: Int, item4: Int = -1,
	                                 item5: Int = -1, item6: Int = -1) {
		if (it.player().inventory().freeSlots() > amount_of_items - 1) {
			if (it.player().inventory().remove(Item(boxed_set), false).success()) {
				it.player().inventory().add(Item(item1), true)
				it.player().inventory().add(Item(item2), true)
				it.player().inventory().add(Item(item3), true)
				if (item4 != -1)
					it.player().inventory().add(Item(item4), true)
				if (item5 != -1)
					it.player().inventory().add(Item(item5), true)
				if (item6 != -1)
					it.player().inventory().add(Item(item6), true)
			}
		} else {
			it.player().message("You need at least ${amount_of_items - 1} free spaces to unpack this set.")
		}
	}
	
}