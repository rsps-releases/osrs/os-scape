package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.combat.magic.MagicSpell
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * A template for any spell that may take resources to give a finished product.
 * @author Mack
 */
abstract class ProductionSpell(vararg val arguments: Int): MagicSpell {

    private val args = arguments

    @Suspendable override fun able(it: Script, alert: Boolean): Boolean {
        val player = it.player()
        val levelReq = args[1]

        if (player.skills().levels()[Skills.MAGIC] < args[1]) {
            it.messagebox("You need a Magic level of $levelReq to cast this spell.")
            return false
        }

        if (!MagicCombat.has(player, runes(), true)) return false

        return true
    }
}