package nl.bartpelle.veteres.content.areas.neitiznot

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 19/04/2016.
 */
object NeitTrader {
	
	val ANNE_ISAAKSON = 1887
	val GUNNAR_HOLDSTORM = 1886
	
	val YAK_HIDE = 10818
	val YAK_HAIR = 10814
	val RAW_YAK_MEAT = 10816
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (i in intArrayOf(ANNE_ISAAKSON, GUNNAR_HOLDSTORM)) {
			r.onNpcOption1(i) @Suspendable {
				it.chatNpc("I'll buy Yak hide, meat and hair from you. Just use it on me.", i)
			}
			r.onItemOnNpc(i, s@ @Suspendable {
				
				val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
				val slot: Int = it.player().attrib(AttributeKey.ITEM_SLOT)
				if (item < 0 || slot < 0 || slot > 27)
					return@s
				val def = it.player().world().definitions().get(ItemDefinition::class.java, item) ?: return@s
				val count = it.player().inventory().count(item)
				val price = count * (if (item == YAK_HAIR) 1000 else if (item == YAK_HIDE) 1250 else 1500)
				
				
				if (item == YAK_HIDE || item == YAK_HAIR || item == RAW_YAK_MEAT) {
					if (it.options("Sell " + count + " " + def.name + " for " + price + " gold coins.", "Never mind") == 1) {
						if (it.player().inventory().remove(Item(item, count), true).success()) {
							it.player().inventory().add(Item(995, price), true)
							it.itemBox(it.targetNpc()!!.def()!!.name + " hands you " + price + " gold in exchange for " + count + " " + def.name.toLowerCase() + ".", 995, price)
							it.chatPlayer("Thanks!")
						}
					}
				}
			})
		}
		
	}
}