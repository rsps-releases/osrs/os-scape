package nl.bartpelle.veteres.content.tutorial

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.util.Varp

/**
 * @author Mack
 */
object Tutorial {

    val script: Function1<Script, Unit> = @Suspendable {
        main(it)
    }

    @Suspendable fun main(it: Script) {
        it.player().looks().hide(true)
        it.player().lock()

        when (it.options("Select game mode", "View the tutorial", "Read info on game modes")) {
            1 -> promptGameModeSelection(it)
            2 -> initTutorial(it)
            3 -> discussGameModes(it)
        }
    }

    @Suspendable private fun initTutorial(it: Script) {
        it.chatNpc("Hello, ${it.player().name()}. Welcome to Os-Scape! During this brief tutorial I'll be passing you along"+
                "to other NPCs you should become familiarized with. They'll describe will quickly talk about what service or use they provide.",4306)
        it.chatNpc("After the tutorial concludes you will be asked to select which game mode you wish to play on. Without further delay, let us begin!", 4306)

        it.player().teleport(3080, 3509)
        it.chatNpc("Here is the primary shopping centre. You may find an abundance of items and resources from the merchants here"+
        " however, there are various shops scattered throughout the world.", 514)

        it.player().teleport(3109, 3514)
        it.chatNpc("Here is all of the Slayer masters within Os-Scape. Slayer is a great source of resources and allow you to obtain "+
                "higher-tier items as you scale up to a higher level.", 3307)
        it.chatNpc("Speak to these instructors to find out which one is best for you.", 3307)

        it.player().teleport(3088, 3507)
        it.chatNpc("I am the expert of transportation when it comes to navigating around Os-Scape. While you may unlock and obtain various ways to travel "+
                "around the world, I provide a way for players to", 4718)
        it.chatNpc("travel to common and useful areas. Come see me to view where you can go!", 4718)
    }

    @Suspendable private fun promptGameModeSelection(it: Script) {
        when (it.options("Select <img=2><col=7f0000>Iron man</col> mode.", "Select <img=34><col=606060>Realism</col> mode.", "Select Classic mode.", "Return to main menu.")) {
            1 -> displayIronModes(it)
            2 -> confirmMode(it, 4, "Realism mode")
            3 -> confirmMode(it, 5, "Classic mode")
            4 -> main(it)
        }
    }

    @Suspendable private fun discussGameModes(it: Script) {
        when (it.options("Tell me about <img=2><col=7f0000>Iron man</col> mode.", "Tell me about <img=34><col=606060>Realism</col> mode.", "Tell me about Classic mode.", "Return to main menu.")) {
            1 -> talkIronModes(it)
            2 -> talkRealismMode(it)
            3 -> talkClassicMode(it)
            4 -> main(it)
        }
    }

    @Suspendable private fun displayIronModes(it: Script) {
        when (it.options("Select <img=2><col=7f0000>Iron man</col> mode.", "Select <img=3><col=00007f>Ultimate iron man</col> mode.", "Select <img=10><col=b10000>Hardcore iron man</col> mode.", "Return to main menu.")) {
            1 -> confirmMode(it, 1, "Iron man")
            2 -> confirmMode(it, 2, "Ultimate iron man")
            3 -> confirmMode(it, 3, "Hardcord iron man")
            4 -> main(it)
        }
    }

    @Suspendable private fun confirmMode(it: Script, mode: Int, rankText: String) {
        when (it.optionsTitled("Play as $rankText?", "Yes, I wish to play in the $rankText mode.", "No, I wish to return to main menu.")) {
            1 -> {
                when (mode) {
                    1 -> it.player().ironMode(IronMode.REGULAR)
                    2 -> it.player().ironMode(IronMode.ULTIMATE)
                    3 -> it.player().ironMode(IronMode.HARDCORE)
                    4 -> it.player().mode(GameMode.REALISM)
                    5 -> it.player().mode(GameMode.CLASSIC)
                }

                //Mark the player as finished.
                it.player().varps().varp(Varp.TUTORIAL_STAGE, 1000)
                it.message("You have successfully selected the $rankText mode. Best of luck on your adventure!")

                it.player().unlock()
                it.player().looks().hide(false)
                it.player().interfaces().sendMain(269)
            }
            2 -> main(it)
        }
    }

    @Suspendable private fun talkIronModes(it: Script) {
        it.doubleItemBox("The iron man modes are designed for players who wish to take on a whole new challenge."+
                " When you play as an <col=7f0000>Iron Man</col>, you do everything for yourself."+
                " The use of shops, trading with other players and", 12810, 12811)
        it.doubleItemBox(" any other form of assistance is not allowed for iron men." + " As an <col=7f0000>Iron Man</col>, you choose to have these restrictions imposed on you,"+
                "so everyone knows you're doing it properly.", 12810, 12811)
        it.doubleItemBox("As an <col=00007f>Ultimate iron man</col> you adhere to all of the restrictions as a normal <col=7f0000>Iron Man</col>"+
                " with the addition of not being able to use the bank system. Also, upon death of an <col=00007f>Ultimate iron man</col>, you will lose any and all items on death.", 12813, 12814)
        it.doubleItemBox("And last, but certainly not least, comes the <col=b10000>Hardcore iron man</col>. This mode follows the restrictions of a regular <col=7f0000>Iron Man</col>. However "+
                "you only have one life. When dying you are deranked to a regular", 20792, 20794)
        it.doubleItemBox("<col=7f0000>Iron Man</col> and your status on the <col=b10000>Hardcore iron man</col> highscore table will be locked along with your name having a slash across it.", 20792, 20794)
        main(it)
    }

    @Suspendable private fun talkRealismMode(it: Script) {
        it.doubleItemBox("The realism mode is for our more dedicated players. With a rate of <col=00007f>${GameMode.REALISM.combatXp()} combat exp</col> modifier and <col=00007f>${GameMode.REALISM.skillXp()} skilling exp</col> modifier, this mode is "+
                "for dedicated players who seek a long term challenge. You can view your standing with other <img=34><col=606060>Realism</col> players on the highscore table.", 1291, 1171)
        main(it)
    }

    @Suspendable private fun talkClassicMode(it: Script) {
        it.doubleItemBox("The Classic mode is our traditional playing style. The experience rates with this mode come with a <col=00007f>${GameMode.CLASSIC.combatXp()} combat exp</col> modifier and a <col=00007f>${GameMode.CLASSIC.skillXp()} skilling exp</col> modifier.", 1323, 1191)
        main(it)
    }
}