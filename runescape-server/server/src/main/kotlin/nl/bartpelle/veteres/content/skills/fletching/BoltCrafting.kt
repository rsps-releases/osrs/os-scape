package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/5/2015.
 */

object BoltCrafting {
	
	enum class bolts(val level: Int, val exp: Double, val unfBolt: Int, val boltTips: Int, val tipped: Int, val amt: Int) {
		OPAL(11, 1.6, 877, 45, 879, 10),
		JADE(26, 2.4, 9139, 9187, 9335, 10),
		PEARL(41, 3.0, 9140, 46, 880, 10),
		RED_TOPAZ(48, 4.0, 9141, 9188, 9336, 10),
		SAPPHIRE(56, 4.0, 9142, 9189, 9337, 10),
		EMERALD(55, 4.0, 9142, 9190, 9338, 10),
		RUBY(63, 6.3, 9143, 9191, 9339, 10),
		DIAMOND(65, 7.0, 9143, 9192, 9340, 10),
		DRAGON(71, 8.2, 9144, 9193, 9341, 10),
		ONYX(73, 10.0, 9144, 9194, 9342, 10)
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//For each bolt in the players inventory do the following..
		bolts.values().forEach { bolt ->
			
			//If the player uses the bolt with the bolt tip we..
			repo.onItemOnItem(bolt.unfBolt.toLong(), bolt.boltTips.toLong(), s@ @Suspendable {
				//Check if the players has the required level.
				if (it.player().skills()[Skills.FLETCHING] < bolt.level) {
					it.messagebox("You need at least level ${bolt.level} Fletching to do that.")
					return@s
				}
				//Prompt the player with the # they'd like to make
				var num = 1
				if (it.player().inventory().count(bolt.unfBolt) >= 10) {
					num = it.itemOptions(Item(bolt.tipped, 150), offsetX = 5)
				}
				
				while (num-- > 0) {
					//Check if the player still has unfinished bolts and/or bolt tips
					if (bolt.unfBolt !in it.player().inventory() || bolt.boltTips !in it.player().inventory()) {
						break
					}
					var amt = 10
					if (it.player().inventory().count(bolt.unfBolt) >= 10 && it.player().inventory().count(bolt.boltTips) >= 10) {
						amt = 10
					} else if (it.player().inventory().count(bolt.unfBolt) < 10) {
						if (it.player().inventory().count(bolt.unfBolt) > it.player().inventory().count(bolt.boltTips)) {
							amt = it.player().inventory().count(bolt.boltTips)
						} else {
							amt = it.player().inventory().count(bolt.unfBolt)
						}
					} else if (it.player().inventory().count(bolt.boltTips) < 10) {
						if (it.player().inventory().count(bolt.boltTips) > it.player().inventory().count(bolt.unfBolt)) {
							amt = it.player().inventory().count(bolt.unfBolt)
						} else {
							amt = it.player().inventory().count(bolt.boltTips)
						}
					}
					
					//Remove the unfinished bolts and bolt tips, add the tipped bolts.
					it.player().inventory() -= Item(bolt.unfBolt, amt)
					it.player().inventory() -= Item(bolt.boltTips, amt)
					it.player().inventory() += Item(bolt.tipped, amt)
					//Add the fletching experience, send message, and add delay
					it.addXp(Skills.FLETCHING, bolt.exp * amt)
					it.message("You fletch $amt bolts.")
					it.delay(2)
				}
			})
		}
	}
}