package nl.bartpelle.veteres.content.areas.karamja.objects

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/11/2016.
 */

object CaveEntrance {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(11835) @Suspendable {
			it.player().lock()
			it.player().teleport(Tile(2480, 5175))
			it.player().unlock()
		}
	}
}