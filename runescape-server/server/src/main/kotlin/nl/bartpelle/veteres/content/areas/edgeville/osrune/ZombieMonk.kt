package nl.bartpelle.veteres.content.areas.edgeville.osrune

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-06-25.
 */

object ZombieMonk {
	
	val ZOMBIE_MONK = 560
	val BONES_ALLOWED = intArrayOf(526, 532, 536, 6729)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ZOMBIE_MONK) @Suspendable {
			if (it.player().skills().xpLevel(Skills.PRAYER) < 70) {
				it.chatPlayer("Who are you?", 588)
				it.chatNpc("Brraaaiiiinnnssss.", ZOMBIE_MONK, 590)
				it.messagebox("The Zombie monk doesn't seem to be interested in talking. Try again when you have at least 70 Prayer.")
			} else {
				it.chatPlayer("Who are you?", 588)
				it.chatNpc("It's not who I am now.. it's who I once was.", ZOMBIE_MONK, 590)
				when (it.options("And.. who is that?", "Cool.")) {
					1 -> {
						it.chatPlayer("And.. who is that exactly?", 588)
						it.chatNpc("I used to be a Saradominist at the monastary in Entrana. Now I'm chained to the altar with one purpose: to sell these malodorous ensouled heads.", ZOMBIE_MONK, 588)
						when (it.options("Cool.", "Can I buy one?")) {
							1 -> it.chatPlayer("Uhh.. cool.", 590)
							2 -> {
								it.chatPlayer("Can I buy one?", 588)
								it.chatNpc("Yes, but I only take bones as currency.", ZOMBIE_MONK, 580)
								it.player().world().shop(1354).display(it.player())
							}
						}
					}
					2 -> {
						it.chatPlayer("That's.. cool.", 590)
					}
				}
			}
		}
		r.onNpcOption2(ZOMBIE_MONK) @Suspendable {
			if (it.player().skills().xpLevel(Skills.PRAYER) < 70) {
				it.messagebox("You need at least 70 prayer to access this shop.")
			} else {
				it.player().world().shop(1354).display(it.player())
			}
		}
		r.onItemOnNpc(ZOMBIE_MONK) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			
			if (item in BONES_ALLOWED) swap(it, item, Item(item).note(it.player().world()).id())
			else it.chatNpc("What do you expect me to do with this?", ZOMBIE_MONK, 611)
		}
	}
	
	@Suspendable fun swap(it: Script, original: Int, result: Int) {
		val num = it.player().inventory().count(original)
		
		it.player().inventory() -= Item(original, num)
		it.player().inventory() += Item(result, num)
		it.itemBox("The Zombie Monk converts your bones to banknotes.", original)
	}
	
	@JvmStatic @Suspendable fun getPriceOf(itemid: Int): Int {
		if (itemid == 13447) {
			return 8
		} //Reanimate Goblin
		if (itemid == 13450) {
			return 12
		} //Reanimate Monkey
		if (itemid == 13453) {
			return 17
		} //Reanimate Imp
		if (itemid == 13456) {
			return 21
		} //Reanimate Minotaur
		if (itemid == 13459) {
			return 27
		} //Reanimate Scorpion
		if (itemid == 13462) {
			return 28
		} //Reanimate Bear
		if (itemid == 13465) {
			return 29
		} //Reanimate Unicorn
		if (itemid == 13468) {
			return 30
		} //Reanimate Dog
		if (itemid == 13471) {
			return 34
		} //Reanimate Chaos Druid
		if (itemid == 13474) {
			return 12
		} //Reanimate Giant
		if (itemid == 13477) {
			return 13
		} //Reanimate Ogre
		if (itemid == 13480) {
			return 45
		} //Reanimate Elf
		if (itemid == 13483) {
			return 46
		} //Reanimate Troll
		if (itemid == 13486) {
			return 49
		} //Reanimate Horror
		if (itemid == 13489) {
			return 52
		} //Reanimate Kalphite
		if (itemid == 13492) {
			return 2
		} //Reanimate Dagannoth
		if (itemid == 13495) {
			return 62
		} //Reanimate Bloodveld
		if (itemid == 13498) {
			return 65
		} //Reanimate TzHaar
		if (itemid == 13501) {
			return 69
		} //Reanimate Demon
		if (itemid == 13504) {
			return 73
		} //Reanimate Aviansie
		if (itemid == 13507) {
			return 77
		} //Reanimate Abyssal Creature
		if (itemid == 13510) {
			return 6
		} //Reanimate Dragon
		
		return 9999
	}
	
	@JvmStatic @Suspendable fun getCurrencyID(itemid: Int): Int {
		if (itemid == 13474) {
			return 533
		} //Reanimate Giant
		if (itemid == 13477) {
			return 533
		} //Reanimate Ogre
		if (itemid == 13492) {
			return 6730
		} //Reanimate Dagannoth
		if (itemid == 13510) {
			return 537
		} //Reanimate Dragon
		return 527
	}
}