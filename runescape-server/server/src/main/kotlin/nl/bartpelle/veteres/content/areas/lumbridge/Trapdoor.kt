package nl.bartpelle.veteres.content.areas.lumbridge

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/6/2016.
 */

object Trapdoor {
	
	val TRAP_DOOR = 14880
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(TRAP_DOOR) @Suspendable {
			val trapdoor: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (trapdoor.tile() == Tile(3209, 3216)) {
				Ladders.ladderDown(it, Tile(3210, 9616), true)
			}
		}
		
		r.onObject(5492) @Suspendable {
			// H.A.M trapdoor to down
			val trapdoor: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (trapdoor.tile() == Tile(3166, 3252)) {
				Ladders.ladderDown(it, Tile(3149, 9652), true)
			}
		}
		
		r.onObject(5493) @Suspendable {
			// H.A.M ladder to surface in lumbridge
			val trapdoor: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (trapdoor.tile() == Tile(3149, 9653)) {
				Ladders.ladderUp(it, Tile(3166, 3251), true)
			}
		}
	}
}
