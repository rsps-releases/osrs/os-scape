package nl.bartpelle.veteres.content.npcs.fightcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveGame
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveMonsters
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 9/18/2016.
 */

object TztokJad {
	
	val RANGED_MAX_DAMAGE = 96
	val MAGIC_MAX_DAMAGE = 96
	val MELEE_MAX_DAMAGE = 98
	
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 12) && EntityCombat.attackTimerReady(npc)) {
				val attackRoll = npc.world().random(3)
				var spawnedHealers = npc.attribOr<Boolean>(AttributeKey.JAD_SPAWNED_HEALERS, false)
				
				//Do we spawn the healers?
				if (npc.hp() < 130 && !spawnedHealers) {
					for (i in 0..3)
						spawnHealers(npc, target)
					npc.putattrib(AttributeKey.JAD_SPAWNED_HEALERS, true)
				}
				
				//Select an attack style based on our random roll..
				when (attackRoll) {
					1 -> rangedAttack(npc, target)
					2 -> mageAttack(npc, target)
					else -> {
						if (EntityCombat.canAttackMelee(npc, target, false) && npc.world().rollDie(3, 1)) {
							meleeAttack(npc, target)
						} else {
							if (npc.world().rollDie(2, 1))
								rangedAttack(npc, target)
							else
								mageAttack(npc, target)
						}
					}
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun meleeAttack(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, npc.world().random(MELEE_MAX_DAMAGE))
		else
			target.hit(npc, 0)
		
		npc.animate(npc.attackAnimation())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun rangedAttack(npc: Npc, target: Entity) {
		npc.face(target)
		npc.animate(2652)
		
		target.world().server().scriptExecutor().executeScript(target.world(), @Suspendable { s ->
			s.delay(3)
			target.graphic(451)
			s.delay(2)
			
			if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE))
				target.hit(npc, npc.world().random(RANGED_MAX_DAMAGE), 0).combatStyle(CombatStyle.RANGE).graphic(Graphic(157, 80))
			else
				target.hit(npc, 0, 0).graphic(Graphic(157, 80))
		})
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun mageAttack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 35)
		
		npc.face(target)
		npc.animate(2656)
		
		target.world().server().scriptExecutor().executeScript(target.world(), @Suspendable { s ->
			s.delay(2)
			npc.graphic(447, 525, 23)
			npc.world().spawnProjectile(npc, target, 448, 130, 36, 35, 8 * tileDist, 10, 5)
			npc.world().spawnProjectile(npc, target, 449, 130, 36, 38, 8 * tileDist, 10, 5)
			npc.world().spawnProjectile(npc, target, 450, 130, 36, 41, 8 * tileDist, 10, 5)
			if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC))
				target.hit(npc, npc.world().random(MAGIC_MAX_DAMAGE), delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(157, 80))
			else
				target.hit(npc, 0, delay.toInt()).graphic(Graphic(157, 80))
		})
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, 8)
	}
	
	@Suspendable fun spawnHealers(tztokJad: Npc, target: Entity) {
		val tiles = FightCaveGame.spawnTiles(target as Player)
		if (tiles.size == 0) return
		val monster = Npc(FightCaveMonsters.YT_HURKOT, tztokJad.world(), tiles[tztokJad.world().random(4)])
		
		monster.walkRadius(100)
		monster.respawns(false)
		monster.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 100)
		tztokJad.world().registerNpc(monster)
		monster.executeScript(YtHurkot(tztokJad).healJad)
	}
}
