package nl.bartpelle.veteres.content.skills.magic.lunar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.skills.magic.ProductionSpell
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * @author Mack
 */
class SuperglassMakeSpell: ProductionSpell(117, 77) {

    private val resources = arrayOf(401, 1781, 6958, 10978, 21504)

    @Suspendable override fun able(it: Script, alert: Boolean): Boolean {
        val hasIngredients: Boolean = Arrays.stream(resources).anyMatch({r -> it.player().inventory().has(r)})

        if (!hasIngredients) {
            if (alert) it.messagebox("You do not have any item which can be blown into glass.")
            return false
        }
        return super.able(it, alert)
    }

    @Suspendable override fun execute(it: Script) {
        if (!able(it, true)) return

        val player = it.player()

        player.animate(4412)
        player.graphic(730)

        MagicCombat.has(player, runes(), true)

        player.skills().__addXp(Skills.MAGIC, 78.0)
        player.skills().__addXp(Skills.CRAFTING, 4.0)
        player.timers().register(TimerKey.NON_COMBAT_SPELL_TIMER, 6)
    }

    override fun runes(): Array<Item> {
        return arrayOf(Item(9075, 2), Item(556, 10), Item(554, 6))
    }
}