package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/19/2016.
 */

object RingOfRecoil {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		val RING_OF_RECOIL = 2550
		
		//Checking how many charges we have left.
		r.onEquipmentOption(1, RING_OF_RECOIL) @Suspendable {
			val charges = it.player().attribOr<Int>(AttributeKey.RING_OF_RECOIL_CHARGES, 40)
			if (charges == 1) {
				it.message("You can inflict one more point of damage before a ring will shatter.")
			} else {
				it.message("You can inflict $charges more points of damage before a ring will shatter.")
			}
		}
		
		//Breaking our ring
		r.onItemOption4(RING_OF_RECOIL) @Suspendable {
			val charges = it.player().attribOr<Int>(AttributeKey.RING_OF_RECOIL_CHARGES, 40)
			if (charges == 40) {
				it.itemBox("The ring is fully charged.<br>There would be no point in breaking it.", RING_OF_RECOIL)
			} else {
				if (it.optionsTitled("Status: $charges damage point left.", "Break the ring.", "Cancel.") == 1) {
					it.player().inventory().remove(Item(RING_OF_RECOIL), true)
					it.player().putattrib(AttributeKey.RING_OF_RECOIL_CHARGES, 40)
					it.itemBox("The ring shatters. Your next ring of recoil will start<br>afresh from 40 damage points.", RING_OF_RECOIL)
				}
			}
		}
	}
}
