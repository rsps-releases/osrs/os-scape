package nl.bartpelle.veteres.content.treasuretrails

/**
 * Created by Situations on 2016-11-04.
 */

enum class TreasureTrailClueLevel {
	//How challenging is our clue scroll? :-)
	UNKNOWN,
	EASY, MEDIUM, HARD, ELITE, MASTER
}
