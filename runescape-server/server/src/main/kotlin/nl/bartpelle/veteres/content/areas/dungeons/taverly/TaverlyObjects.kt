package nl.bartpelle.veteres.content.areas.dungeons.taverly

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/23/2016.
 */

object TaverlyObjects {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(2623) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().equals(2924, 9803)) {
				if (it.player().tile().x == 2924) {
					it.player().teleport(it.player().tile().transform(-1, 0))
				} else {
					it.player().teleport(it.player().tile().transform(1, 0))
				}
			}
		}
	}
}