package nl.bartpelle.veteres.content.areas.nardah

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.Mac
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.skills.herblore.Cleaning
import nl.bartpelle.veteres.content.skills.herblore.PotionMixing
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Bart on 3/12/2017.
 */
object Zahur {
	
	const val ZAHUR = 4753
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(ZAHUR) @Suspendable {
			it.chatNpc("Hi, I clean herbs for a fee of 200 coins per herb. I<br>can clean all the herbs in your inventory at once, or<br>you can show me a herb for me to clean it individually.", ZAHUR, 569)
			it.chatNpc("I can also combine your potion vials to try and make<br>the potions fit into fewer vials. That service is free. So,<br>what would you like today?", ZAHUR, 569)
			
			showOpts(it, "Please clean all my herbs.", "Why would I pay for you to clean herbs?", "Please combine my potions.", "Who's that kid outside your shop?", "Maybe another time.")
		}
		repo.onNpcOption2(ZAHUR) @Suspendable {
			decantAllPotions(it)
		}
		repo.onNpcOption3(ZAHUR) @Suspendable {
			cleanAllHerbs(it)
		}
	}
	
	@Suspendable fun showOpts(it: Script, vararg opts: String) {
		when (it.optionsS("Select an Option", *opts)) {
			"Please clean all my herbs" -> cleanAllHerbs(it)
			"Why would I pay for you to clean herbs?" -> {
				it.chatPlayer("Why would I pay for you to clean herbs? I can<br>advance my skills in Herblore by cleaning them myself.", 555)
				it.chatNpc("This is true, but many of my customers wish to sell the<br>herbs which they can't clean to other adventurers. They<br>might hope for a better price for this.", ZAHUR, 569)
				
				showOpts(it, "Please clean all my herbs.", "Please combine my potions.", "Who's that kid outside your shop?", "Maybe another time.")
			}
			"Please combine my potions." -> decantAllPotions(it)
			"Who's that kid outside your shop?" -> {
				it.chatPlayer("Who's that kid outside your shop?", 554)
				it.chatNpc("I do not know where he came from. He came to<br>Nardah recently, and said his business might help my<br>customers, so I let him have some space out there.", ZAHUR, 577)
				it.chatNpc("He seems very lost in the world, but I hope his work<br>will be a success for him.", ZAHUR, 611)
				it.chatNpc("Now, would you like me to clean your herbs or<br>combine your potions?", ZAHUR, 555)
				
				showOpts(it, "Why would I pay for you to clean herbs?", "Please clean all my herbs.", "Please combine my potions.", "Maybe another time.");
			}
			"Maybe another time." -> it.chatPlayer("Maybe another time.", 592)
		}
	}
	
	@Suspendable fun cleanAllHerbs(it: Script) {
		if (it.optionsTitled("Clean all your herbs at 200 coins each?", "Yes", "No") == 1) {
			var found = false
			
			for (slot in 0..27) {
				val item = it.player.inventory()[slot] ?: continue
				
				// Is this a grimy herb?
				for (grimy in Cleaning.Herb.values()) {
					if (grimy.grimy == item.id()) {
						// Remove the 200 GP, or tell the player they're broke as shit.
						if (it.player.inventory().remove(Item(995, 200), false).failed()) {
							it.chatNpc("You don't have enough money for me to clean any<br>more herbs.", ZAHUR, 589)
							return
						}
						
						// Clean the herb, respecting the slot it's in.
						it.player.inventory()[slot] = Item(grimy.clean, 1)
						found = true // Indicate we did clean. Used for the message down there.
					}
				}
			}
			
			if (found) {
				it.chatNpc("There, all done.", ZAHUR, 567)
			} else {
				it.chatNpc("I didn't find anything that I could clean.", ZAHUR, 610)
			}
		}
	}
	
	@JvmStatic @Suspendable fun decantAllPotions(it: Script) {
		var cleaned = 0
		
		// Two maps, because these cannot be combined into the same thing.
		val totals = HashMap<PotionMixing.PotDoses, Long>()
		val totalsNoted = HashMap<PotionMixing.PotDoses, Long>()
		var totalVials = 0L
		var totalNotedVials = 0L
		
		for (slot in 0..27) {
			val item = it.player.inventory()[slot] ?: continue
			val extracted = extractPotionDoses(it.player, item) ?: continue
			
			// Add the doses to the totals.
			if (extracted.noted) {
				totalsNoted.compute(extracted.pot, { x, out -> extracted.doses + (out ?: 0) })
				totalNotedVials += item.amount()
			} else {
				totals.compute(extracted.pot, { x, out -> extracted.doses + (out ?: 0) })
				totalVials++
			}
			
			// Remove the item! (yes yes deprecated I know what I'm doing)
			it.player.inventory()[slot] = null
		}
		
		if (totals.isEmpty() && totalsNoted.isEmpty()) {
			it.chatNpc("I don't think you've got anything that I can combine.", if(it.player().world().realm().isPVP) Mac.MAC else ZAHUR, 610)
		} else {
			// Add the items to the inventory now.
			totalsNoted.forEach { k, v ->
				val full = Math.min(Int.MAX_VALUE.toLong(), v / 4).toInt() // Number of full potions to add
				val leftover = v.mod(4).toInt()
				
				it.player.inventory().addOrDrop(Item(k.four, full).note(it.player.world()), it.player)
				
				// If we have leftovers add that potion too.
				if (leftover > 0) {
					val leftoverId = k[leftover - 1]
					it.player.inventory().addOrDrop(Item(leftoverId).note(it.player.world()), it.player)
				}
				
				// Add leftover vials
				val vialsUsed = full + if (leftover > 0) 1 else 0
				if (totalNotedVials - vialsUsed > 0) {
					val vialsToAdd = Math.min(Int.MAX_VALUE.toLong(), totalNotedVials - vialsUsed).toInt()
					it.player.inventory().addOrDrop(Item(230, vialsToAdd), it.player)
				}
			}
			
			// And in turn the unnnoted items too. We do noted first since a stack of X noted is likely more
			// valuable than unnoted. Pretty much risk policy. Safety first. Love sex, love Durex.
			totals.forEach { k, v ->
				val full = Math.min(Int.MAX_VALUE.toLong(), v / 4).toInt() // Number of full potions to add
				val leftover = v.mod(4).toInt()
				
				it.player.inventory().addOrDrop(Item(k.four, full), it.player)
				
				// If we have leftovers add that potion too.
				if (leftover > 0) {
					val leftoverId = k[leftover - 1]
					it.player.inventory().addOrDrop(Item(leftoverId), it.player)
				}
				
				// Add leftover vials
				val vialsUsed = full + if (leftover > 0) 1 else 0
				if (totalVials - vialsUsed > 0) {
					val vialsToAdd = (totalVials - vialsUsed).toInt()
					it.player.inventory().addOrDrop(Item(229, vialsToAdd), it.player)
				}
			}

			if(it.player().world().realm().isPVP)
				it.chatNpc("There you go, I've decanted all your potions!", Mac.MAC)
			else
				it.chatNpc("There, all done.", ZAHUR, 567)
		}
	}
	
	fun extractPotionDoses(p: Player, item: Item): PotionDose? {
		// Track noting - they don't end up getting combined. Two unnoted doses and two noted doses does NOT mix!
		val noted = item.definition(p.world()).noted()
		val id = item.unnote(p.world()).id()
		
		// Iterate through potion values, see if there is any match here.
		for (potion in PotionMixing.PotDoses.values()) {
			if (id in potion) {
				val doses = potion.numDoses(id)
				return PotionDose(potion, noted, (doses * item.amount()).toLong())
			}
		}
		
		return null
	}
	
	data class PotionDose(val pot: PotionMixing.PotDoses, val noted: Boolean, var doses: Long);
	
}