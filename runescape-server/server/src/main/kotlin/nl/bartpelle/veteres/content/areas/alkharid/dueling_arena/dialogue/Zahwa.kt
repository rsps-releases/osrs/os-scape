package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Zahwa {
	
	val ZAHWA = 3345
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ZAHWA) @Suspendable {
			it.chatPlayer("Hi!", 567)
			it.chatNpc("I could've 'ad 'im!", ZAHWA, 614)
			it.chatPlayer("Er...", 571)
			it.chatNpc("I was robbed!", ZAHWA, 614)
			it.chatPlayer("Right.", 562)
			it.chatNpc("It was rigged I tell you!", ZAHWA, 614)
			it.chatPlayer("Uh huh.", 562)
			it.chatNpc("Leave me alone!", ZAHWA, 614)
		}
	}
	
}
