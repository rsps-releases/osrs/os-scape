package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.items.LootingBag
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.model.map.steroids.RouteFinder
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.services.logging.LoggingService

/**
 * Created by Bart on 8/23/2015.
 */
@Suspendable class GroundItemAction {
	
	@Suspendable companion object {
		@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
			
			val gitem: GroundItem? = it.player().attrib(AttributeKey.INTERACTED_GROUNDITEM)
			val INTERACT_TYPE: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
			
			it.player().stopActions(true)
			it.player().animate(-1)
			
			if (gitem != null) {
				if (it.player.tile().level != gitem.tile().level) {
					return@s
				}
				// If whoever froze you is out of viewport, you get unfrozen when interacting with items.
				PlayerCombat.unfreeze_when_out_of_range(it.player())

				// Warn before we take a Blood Chest key!
				if (gitem.item().id() in BloodChest.BLOODY_KEYS) {
					it.player().faceTile(gitem.tile())
					it.itemBox("The key you're about to pick up is a <col=c12717>Blood Key</col>. " +
							"Infused from the blood of your victim, its tale tells him who takes it becomes cursed. " +
							"You cannot <col=c12717>teleport</col>. You'll become <col=c12717>skulled</col>. You're considered in a <col=c12717>multiway-combat area</col>. This all lasts until you <col=c12717>cross the Wilderness ditch</col>.", 7302)
					
					if (it.optionsTitled("Raise the horns?", "Let them come. Take the key.", "I'm unprepared. Leave the key.") == 2) {
						return@s
					}
				}
				
				// These logic variables are checked early if the ground item is adjacent and you're frozen, otherwise they are evaluated again after adjecent item check.
				var legal: Boolean = false
				var canInteract: Boolean = false
				var fromObject = false
				
				// Stop duplicate messages from walkTo method in Player class
				val nextTo: Boolean = it.player().tile().nextTo(gitem.tile())
				if (!it.player().tile().equals(gitem.tile()) && !nextTo) {
					if (it.player().stunned()) {
						it.player().message("You're stunned!")
						it.player().pathQueue().clear()
						it.player().faceTile(gitem.tile())
						return@s
					} else if (it.player().frozen()) {
						it.player().message("A magical force stops you from moving.")
						it.player().pathQueue().clear()
						it.player().faceTile(gitem.tile())
						return@s
					}
				} else if (nextTo && it.player().frozen() && INTERACT_TYPE == 3) {
					legal = RouteFinder.isLegal(it.player().world(), gitem.tile(), Direction.orthogonal(it.player().tile().x - gitem.tile().x, it.player().tile().z - gitem.tile().z), 1)
					if (legal) {
						fromObject = true
						canInteract = interact_legally(it, gitem)
						if (canInteract) {
							pickupItem(it, gitem, fromObject)
							return@s
							// This handles picking up, not item on item of adjcent items! Or ground op 2 ('light' logs)
							// Those require you to be on the exact tile, where the remaining below logic applies.
						}
					}
					// If it's not adjacent, all other logic is below;
				}
				
				// If we're not already on the gitem tile, do some checks...
				if (!it.player().tile().equals(gitem.tile())) {
					// Calculate the closest possible tile we can get (maybe it's stuck on an object?)
					val pathEnd = it.player().walkTo(gitem.tile().x, gitem.tile().z, PathQueue.StepType.REGULAR)
					
					it.waitForTile(pathEnd)
					
					// If the item's on the table, taking logic becomes slightly different
					if (pathEnd != gitem.tile() && !fromObject) {
						// Value (fromObject) initiazlied as false, re-evaluate logic!
						legal = RouteFinder.isLegal(it.player().world(), gitem.tile(), Direction.orthogonal(it.player().tile().x - gitem.tile().x, it.player().tile().z - gitem.tile().z), 1)
						if (!legal || it.player().tile().distance(gitem.tile()) > 1) {
							it.player().message("You can't reach that!")
							it.player().faceTile(gitem.tile())
							return@s
						} else {
							fromObject = true
						}
					}
				}
				
				if (!canInteract) { // Value (canInteract) initiazlied as false, re-evaluate logic!
					if (!interact_legally(it, gitem)) {
						return@s
					}
				}
				
				// Did we arrive there?
				val sameH: Boolean = Tile.sameH(it.player(), gitem)
				if (sameH && it.player().world().groundItemValid(gitem)) {
					
					when (INTERACT_TYPE) {
						2 -> it.player().world().server().scriptRepository().triggerGroundItemOption2(it.player(), gitem)
						3 -> pickupItem(it, gitem, fromObject)
						-1 -> it.player().world().server().scriptRepository().triggerInvItemOnGroundItem(it.player(), gitem)
					}
				} else {
					it.player().debug("Invalid ground item. " + sameH)
				}
			}
		}
		
		@JvmStatic fun interact_legally(it: Script, gitem: GroundItem): Boolean {
			val different_owner: Boolean = !gitem.ownerMatches(it.player())
			// Ironman checks (if it's not a respawning item)
			if (it.player().ironMode() != IronMode.NONE && !gitem.respawns()) {
				if (different_owner && gitem.pkedFrom() != it.player().id()) { // Owner different? It could be pked!
					it.player().message("You're an Iron Man, so you can't take items that other players have dropped.")
					return false
				} else if (gitem.pkedFrom() != null && gitem.pkedFrom() != it.player().id()) {
					it.player().message("You're an Iron Man, so you can't take items that other players have dropped.")
					return false
				}
			}
			
			if (different_owner && Staking.in_duel(it.player())) {
				it.player().message("You can't pickup other players items in the duel arena.")
				return false
			}
			if (different_owner && ClanWars.inInstance(it.player())) {
				it.player().message("You can't pickup other players items in a clan wars.")
				return false
			}
			if (different_owner && gitem.isHidden) {
				// You're trying to pick up a hidden item, no thanks drop trader.
				return false
			}
			if (gitem.item().id() == 12020) { // Gem bag
				if (it.player().hasItem(12020)) {
					it.player().message("You can only own one gem bag at a time.")
					return false
				}
			}
			return true
		}
		
		@Suspendable fun pickupItem(it: Script, item: GroundItem, fromObject: Boolean) {
			if (!it.player.world().groundItemValid(item))
				return


            var looted = false
            val lootingBagOpened = LootingBag.lootbagOpen(it.player())

            // Add to looting bag if open.
            if (lootingBagOpened && LootingBag.deposit(it, item.item(), item.item().amount(), item)) {
                looted = true
            }

            // If it hasn't been looted yet then let's try inventory?
            if (!looted && it.player().inventory().add(item.item(), false).failed()) {
                if (it.player().world().groundItemValid(item)) {
                    it.player().message("You don't have enough inventory space to hold that item.")
                }
                return
            }

            // If we've made it here then it added to the inventory.
            if (!looted) {
                looted = true
            }
            val container = if (lootingBagOpened) it.player().lootingBag() else it.player().inventory()
            if (looted) {
                if (!it.player().world().removeGroundItem(item)) {
                    container.remove(item.item(), true)
                    return
                }

                // Clear the hint arrow when we go back and pick stuff up. We don't need to see it anymore.
                if (it.player().attribOr<Tile?>(AttributeKey.LAST_DEATH_TILE, null) == item.tile()) {
                    it.player().clearattrib(AttributeKey.LAST_DEATH_TILE)
                    it.player().write(SetHintArrow(-1)) // clear this
                }
                var irrelevant = false
                val value = item.item().amount() * (if (it.player().world().realm().isPVP) it.player().world().prices().getOrElse(item.item().unnote(it.player().world()).id(), 0) else
                    item.item().realPrice(it.player().world()))
                if (value > 0) {
                    // Update our risked total if looting in the wildy.
                    it.player().putattrib(AttributeKey.RISKED_WEALTH, it.player().attribOr<Int>(AttributeKey.RISKED_WEALTH, 0) + value)
                    if (item.respawns()) { //Is our item a world spawned item?
                        irrelevant = true
                    } else if (item.owner() != null && item.owner() as Int in intArrayOf(3875, 7620, 3877, 29426, 206487, 37272, 5835, 37274, 51779)) { //Does it belong to a player?
                        irrelevant = true
                    }
                } else {
                    // No wealth, pointless to log.
                    irrelevant = true
                }

                // Exclude dupewatch from our own loot. If someones drop trading - flag it!
                if (item.ownerMatches(it.player())) {
                    it.player().dupeWatch().exclude()
                } else if (!irrelevant) { // drop parties will spam the log
                    // THIS IS A DROP TRADE!

                    it.player().world().server().service(LoggingService::class.java, true).ifPresent { s ->
                        s.logDropTrade(it.player(), item)
                    }
                }

                it.player().sound(2582, 0)

                if (it.player().world().realm().isDeadman) {
                    if (item.item().id() in DeadmanMechanics.KEYS) {
                        DeadmanMechanics.pickupKey(it, item)
                    }
                }

                // Blood chest key pickup!
                if (item.item().id() in BloodChest.BLOODY_KEYS) {
                    BloodChest.takeKey(it, it.player, item)
                }

                if (fromObject) {
                    it.player().faceTile(item.tile())
                    it.animate(832)
                }

                // Does this ground item respawn?
                if (item.respawns()) {
                    it.player().world().server().scriptExecutor().executeScript(it.player().world()) @Suspendable {
                        it.delay(item.respawnTimer())
                        it.ctx<World>().spawnGroundItem(item)
                    }
                }
            }
		}
	}
	
}