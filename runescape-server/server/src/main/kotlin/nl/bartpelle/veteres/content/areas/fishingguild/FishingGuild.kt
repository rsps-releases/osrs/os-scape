package nl.bartpelle.veteres.content.areas.fishingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 21/05/2017.
 */
object FishingGuild {
	
	val GUILD_DOOR = 20925
	val ROACHARY = 1299
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		r.onObject(GUILD_DOOR, s@ @Suspendable {
			val change = if (it.player().tile().z >= 3394) -1 else 1
			if (change == 1 && it.player().skills().level(Skills.FISHING) < 68) {
				it.messagebox("You do not meet the level 68 Fishing requirement to enter the Guild.")
				return@s
			}
			val obj = it.interactionObject()
			obj.replaceWith(MapObj(obj.tile(), obj.id(), obj.type(), 0), it.player().world()).interactAble(false)
			it.player().pathQueue().interpolate(it.player().tile().x, it.player().tile().z + change, PathQueue.StepType.FORCED_WALK)
			it.player().lockDelayDamage()
			it.delay(1)
			obj.replaceWith(MapObj(obj.tile(), obj.id(), obj.type(), 3), it.player().world()).interactAble(true)
			it.player().unlock()
			it.message("You ${if (change == -1) "leave" else "enter"} the Fishing Guild.")
		})
		
		
		r.onNpcOption1(ROACHARY) {
			if (!it.player().world().realm().isPVP)
				it.player().world().shop(50).display(it.player())
		}
		r.onNpcOption2(ROACHARY) {
			if (!it.player().world().realm().isPVP)
				it.player().world().shop(50).display(it.player())
		}
	}
}