package nl.bartpelle.veteres.content.areas.zeah

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.openInterface
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-06-10.
 */

object Statue {
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(27785) @Suspendable {
			when (it.interactionOption()) {
				1 -> readStatue(it)
				2 -> investigateState(it)
			}
		}
	}
	
	@Suspendable fun readStatue(it: Script) {
		val player = it.player()
		
		player.lock()
		player.faceObj(it.interactionObject())
		player.animate(2171, 0)
		it.delay(1)
		player.invokeScript(InvokeScript.SETVARCS, 4404769, 0)
		it.openInterface(116)
		player.write(
				//Title
				InterfaceText(116, 4, "Kourend the Magnificent"),
				
				//Stanza 1
				InterfaceText(116, 6, "Kourend the magnificent,"),
				InterfaceText(116, 7, "Kourend the resplendent,"),
				InterfaceText(116, 8, "Kourend, the most powerful"),
				InterfaceText(116, 9, "of the nations in the world."),
				
				//Stanza 2
				InterfaceText(116, 11, "Kourend is our citadel,"),
				InterfaceText(116, 12, "Kourend is our homeland."),
				InterfaceText(116, 13, "We will live and die for Kourend,"),
				InterfaceText(116, 14, "for the city of our birth."),
				
				//Stanza 3
				InterfaceText(116, 16, "From the rugged mountains,"),
				InterfaceText(116, 17, "to the foaming seas,"),
				InterfaceText(116, 18, "here we make our home in Kourend,"),
				InterfaceText(116, 19, "all together, all in peace."),
				
				//Stanza 4
				InterfaceText(116, 21, "Kourend is a monument,"),
				InterfaceText(116, 22, "Kourend is a triumph;"),
				InterfaceText(116, 23, "it shall stand here forever"),
				InterfaceText(116, 24, "in the shadow of our King."))
		
		player.unlock()
		it.onInterrupt { player.interfaces().closeMain(); player.interfaces().closeById(116) }
	}
	
	@Suspendable fun investigateState(it: Script) {
		val player = it.player()
		val res = it.player().interfaces().resizable()
		
		player.lock()
		player.faceObj(it.interactionObject())
		it.message("You investigate what looks like hinges on the plaque and find it opens.")
		player.animate(827)
		it.delay(1)
		it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
		it.player().invokeScript(951)
		it.delay(2)
		player.teleport(Tile(1666, 10050))
		it.message("You climb down the hole.")
		it.delay(2)
		it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
		it.player.invokeScript(948, 0, 0, 0, 255, 50)
		player.unlock()
	}
}