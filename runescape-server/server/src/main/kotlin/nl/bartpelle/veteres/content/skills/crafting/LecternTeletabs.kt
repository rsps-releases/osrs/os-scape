package nl.bartpelle.veteres.content.skills.crafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jak on 16/08/2016.
 */
object LecternTeletabs {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(13648) @Suspendable {
			it.player().interfaces().sendMain(79)
		}
		repo.onButton(79, 9) @Suspendable { makeResult(it, 8021, 87) } // oynx
		repo.onButton(79, 4) @Suspendable { makeResult(it, 8017, 27) } // emerald
		repo.onButton(79, 8) @Suspendable { makeResult(it, 8020, 68) } // dstone
		repo.onButton(79, 12) @Suspendable { makeResult(it, 8008, 31) } // lumb tab
		repo.onButton(79, 3) @Suspendable { makeResult(it, 8016, 7) } // sapphire
		repo.onButton(79, 6) @Suspendable { makeResult(it, 8018, 49) } // ruby
		repo.onButton(79, 7) @Suspendable { makeResult(it, 8019, 57) } // diamond
		repo.onButton(79, 13) @Suspendable { makeResult(it, 8009, 37) } // fally
		repo.onButton(79, 11) @Suspendable { makeResult(it, 8007, 25) } // varrock
		repo.onButton(79, 16) @Suspendable { makeResult(it, 8012, 58) } // watchtower
		repo.onButton(79, 15) @Suspendable { makeResult(it, 8011, 51) } // ardy
		repo.onButton(79, 14) @Suspendable { makeResult(it, 8010, 45) } // cammy
		repo.onButton(79, 17) @Suspendable { makeResult(it, 8013, 40) } // house
		repo.onButton(79, 5) @Suspendable { makeResult(it, 8014, 15) } // bananas
		repo.onButton(79, 10) @Suspendable { makeResult(it, 8015, 60) } // peaches
	}
	
	val SOFTCLAY = 1761
	
	@Suspendable private fun makeResult(it: Script, itemid: Int, req: Int) {
		it.player().stopActions(true) // interrupt previous making
		if (it.player().skills().level(Skills.MAGIC) < req) {
			it.message("You don't have the required Magic level to do this.")
			return
		}
		if (it.player().inventory().count(SOFTCLAY) < 1) {
			it.message("You need soft clay to craft teleport tablets.")
			return
		}
		val requiredItems = forId(itemid)
		if (requiredItems.size == 0) { // must need something
			it.message("Fishy cx. Unable to craft tab - please report this.")
			return
		}
		for (i in requiredItems) {
			if (!it.player().inventory().contains(i)) {
				it.message("You need %d x %s to make this.", i.amount(), i.name(it.player().world()))
				return
			}
		}
		val btn = it.buttonAction()
		var amt = if (btn == 1) 1 else if (btn == 2) 5 else if (btn == 3) Integer.MAX_VALUE else it.inputInteger("How many would you like to craft?")
		while (amt > 0) {
			if (!it.player().inventory().has(SOFTCLAY)) {
				it.message("You've run out of soft clay.")
				it.animate(-1)
				break
			}
			var fail = false // nested loop watch it lad
			for (i in requiredItems) {
				if (!it.player().inventory().contains(i)) {
					it.message("You've run out of %ss.", i.name(it.player().world()))
					it.animate(-1)
					fail = true
					break
				}
			}
			if (fail) break
			it.animate(713)
			it.delay(4)
			it.player().inventory() -= SOFTCLAY
			for (i in requiredItems) {
				it.player().inventory() -= i
			}
			it.player().inventory() += itemid
			amt--
		}
	}
	
	val FIRE = 554
	val WATER = 555
	val AIR = 556
	val EARTH = 557
	val MIND = 558
	val BODY = 559
	val DEATH = 560
	val NAT = 561
	val CHAOS = 562
	val LAW = 563
	val COSMIC = 564
	val BLOOD = 565
	val SOUL = 566
	
	private fun forId(itemid: Int): ArrayList<Item> {
		var r: ArrayList<Item> = ArrayList()
		when (itemid) {
			8007 -> r = arrayListOf(Item(FIRE), Item(AIR, 3), Item(LAW))
			8008 -> r = arrayListOf(Item(EARTH), Item(AIR, 3), Item(LAW))
			8009 -> r = arrayListOf(Item(WATER), Item(AIR, 3), Item(LAW))
			8010 -> r = arrayListOf(Item(AIR, 5), Item(LAW))
			8011 -> r = arrayListOf(Item(WATER, 2), Item(LAW, 2))
			8012 -> r = arrayListOf(Item(EARTH, 2), Item(LAW, 2))
			8013 -> r = arrayListOf(Item(EARTH), Item(AIR), Item(LAW))
			8014 -> r = arrayListOf(Item(EARTH, 2), Item(WATER, 2), Item(NAT))
			8015 -> r = arrayListOf(Item(EARTH, 4), Item(WATER, 4), Item(NAT, 2))
		}
		return r
	}
}