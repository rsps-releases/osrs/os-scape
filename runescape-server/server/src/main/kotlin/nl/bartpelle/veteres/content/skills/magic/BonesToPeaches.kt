package nl.bartpelle.veteres.content.skills.magic

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/22/2015.
 */
object BonesToPeaches {
	
	enum class BonesToPeachesData(val itemId: Int) {
		REGULAR_BONES(526),
		BURNT_BONES(528),
		BAT_BONES(530),
		WOLF_BONES(2859),
		BIG_BONES(532),
		BABYDRAGON_BONES(534),
		DRAGON_BONES(536),
		ZOGRE_BONES(4812),
		OURG_BONES(4830),
		WYVERN_BONES(6812),
		DAGANNOTH_BONES(6729),
		LAVA_DRAGON_BONES(11943),
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onButton(218, 41) {
			if (it.player().skills().xpLevel(Skills.MAGIC) < 60) {
				it.message("Your Magic level is not high enough for this spell.")
				return@onButton
			}

			// Count all the bones we have, mapping it to one grand total
			val inv = it.player().inventory()
			val totalBones = inv.count(*(BonesToPeachesData.values().map { it -> it.itemId }.toTypedArray()));

			if (totalBones >= 1) {

				//Check to see if the player has enough runes to cast the spell
				val runes = arrayOf(Item(557, 4), Item(555, 4), Item(561, 2))
				if (!MagicCombat.has(it.player(), runes, true)) {
					return@onButton
				}
				// Try to remove as many bones as we can
				BonesToPeachesData.values().forEach { btpd ->
					it.player().inventory().remove(Item(btpd.itemId, Integer.MAX_VALUE), true)
				}

				// Add peaches, animate, graphic, and xp.
				it.player().inventory() += Item(6883, totalBones)
				it.player().animate(722)
				it.player().graphic(141, 100, 0)
				it.addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 71.0 else 35.5)
				it.message("You transform all your bones into peaches.")
			} else {
				it.message("You don't have enough bones to cast this spell.")
			}
		}

	}
	
}