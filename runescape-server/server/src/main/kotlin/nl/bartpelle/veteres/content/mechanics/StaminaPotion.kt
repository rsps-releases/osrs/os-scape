package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 12/31/2015.
 */

object StaminaPotion {
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		r.onLogin { it.player().timers().register(TimerKey.STAMINA_POTION, 10) }
		r.onTimer(TimerKey.STAMINA_POTION) {
			var ticks = it.player().attribOr<Int>(AttributeKey.STAMINA_POTION, 0)
			if (ticks > 0) {
				ticks -= 1
				if (ticks == 3) {
					it.player().message("<col=8F4808>Your stamina potion is about to expire.")
				}
				if (ticks == 0) {
					it.player().message("<col=8F4808>Your stamina potion has expired.")
					it.player().sound(2672, 3)
					it.player().clearattrib(AttributeKey.STAMINA_POTION)
					it.player().varps().varbit(Varbit.STAMINA_POTION, 0)
				} else
					it.player().putattrib(AttributeKey.STAMINA_POTION, ticks)
			}
			it.player().timers().register(TimerKey.STAMINA_POTION, 19)
		}
	}
}