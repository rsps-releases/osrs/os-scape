package nl.bartpelle.veteres.content.npcs.bosses.cerberus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors

/**
 * Created by Jason MacKeigan on 2016-07-07 at 12:20 PM
 *
 * The Cerberus mob has a combat script that dictates how it acts in combat. The monster
 * has a series of attacks. An area of effect attack, an attack that spawns mobs in the area
 * to attack for it as well as basic attacks in magic, melee and range.
 */
object Cerberus {
	
	/**
	 * The summoned soul non-playable character identification values.
	 */
	val SUMMONED_SOUL_IDS = SummonedSoulType.values().map { it.id }
	
	/**
	 * The default animation to the displayed is none can be found, an odd
	 * but always possible situation to occur!
	 */
	val DEFAULT_ANIMATION = 4489
	
	/**
	 * The default project to be used in the case of something going wrong, however the projectile
	 * will not be displayed if this projectile value is used.
	 */
	val DEFAULT_PROJECTILE = -1
	
	/**
	 * A mapping of all potential animations used by each style of cerberus. The naming follows
	 * conventional final naming because the object is declared in a static-final manner. The
	 * mapping is also immutable.
	 */
	val ANIMATIONS = mapOf<CombatStyle, Int>(CombatStyle.MAGIC to 4490, CombatStyle.RANGE to 4489,
			CombatStyle.MELEE to 4489)
	
	/**
	 * A mapping of all potential projectiles used by each style of cerberus.
	 */
	val projectiles = mapOf<CombatStyle, Int>(CombatStyle.MAGIC to 1242, CombatStyle.RANGE to 1245)
	
	val CERB_NO_ATK_OPTION = 5863
	val CERB_WITH_AKT_OPTION = 5862
	
	/**
	 * The script used to define Cerberus's attack sequences.
	 */
	@JvmField val script: Function1<Script, Unit> = suspend@ @Suspendable {
		val npc = it.npc()
		val world = npc.world()
		var target = EntityCombat.getTarget(npc) ?: return@suspend
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			val cerberusRegion = CerberusRegion.valueOfRegion(npc.tile().region())
			
			if (cerberusRegion.isPresent) {
				if (npc.sync().transmog() == CERB_NO_ATK_OPTION) {
					addFlameIfNotPresent(npc.world(), cerberusRegion.get())
					npc.sync().transmog(CERB_WITH_AKT_OPTION)
					it.delay(1)
				}
				val close = npc.world().players().count(cerberusRegion.get().area)
				
				if (close == 0) {
					removeFlameIfPresent(world, cerberusRegion.get())
					reset(it, npc)
					break
				}
			}
			
			val canMelee = EntityCombat.canAttackMelee(npc, target, true)
			
			if (EntityCombat.attackTimerReady(npc)) {
				val lastConsecutiveAttack = npc.attribOr<Long>(AttributeKey.CERBERUS_LAST_ROTATION, 0L)
				
				if (System.currentTimeMillis() - lastConsecutiveAttack > TimeUnit.SECONDS.toMillis(50)) {
					npc.putattrib(AttributeKey.CERBERUS_LAST_ROTATION, System.currentTimeMillis())
				}
				
				val consecutiveAttackActive = npc.attribOr<Boolean>(AttributeKey.CERBERUS_COMBAT_STYLE_ROTATION_ACTIVE, false)
				val style = if (consecutiveAttackActive) nextConsecutiveAttack(npc) else randomStyle(canMelee)
				
				attack(npc, target, style, consecutiveAttackActive, canMelee)
				npc.putattrib(AttributeKey.LAST_COMBAT_STYLE, style)
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			
			val potentialTarget = EntityCombat.refreshTarget(it)
			
			if (potentialTarget != null) {
				target = potentialTarget
				continue
			}
			if (cerberusRegion.isPresent) {
				val close = npc.world().players().count(cerberusRegion.get().area)
				
				if (close == 0) {
					removeFlameIfPresent(world, cerberusRegion.get())
					reset(it, npc)
				}
			}
			
			return@suspend
		}
	}
	
	fun removeFlameIfPresent(world: World, cerberusRegion: CerberusRegion) {
		cerberusRegion.flames.forEach {
			Optional.ofNullable(world.objById(23105, it)).ifPresent {
				world.removeObj(it)
			}
		}
	}
	
	@ScriptMain @JvmStatic fun register(repository: ScriptRepository) {
		repository.onNpcSpawn(CERB_WITH_AKT_OPTION, suspend@ @Suspendable {
			val cerberus = it.npc()
			val world = cerberus.world()
			val region = cerberus.tile().region()
			val cerberusRegion = CerberusRegion.valueOfRegion(region)
			
			if (!cerberusRegion.isPresent)
				return@suspend
			
			
			val area = cerberusRegion.get().area
			val playersInArea = world.players().count(area)
			
			if (playersInArea > 0) {
				addFlameIfNotPresent(world, cerberusRegion.get())
			} else {
				reset(it, cerberus)
				removeFlameIfPresent(world, cerberusRegion.get())
			}
		})
	}
	
	fun addFlameIfNotPresent(world: World, cerberusRegion: CerberusRegion) {
		cerberusRegion.flames.forEach {
			val tileObject = Optional.ofNullable(world.objById(23105, it))
			
			if (!tileObject.isPresent) {
				world.spawnObj(MapObj(it, 23105, 10, 0))
			}
		}
	}
	
	@Suspendable fun reset(it: Script, npc: Npc) {
		npc.sync().transmog(CERB_NO_ATK_OPTION)
		npc.clearattrib(AttributeKey.TARGET)
		npc.stopActions(false)
		npc.lockNoDamage()
		npc.walkTo(npc.spawnTile(), PathQueue.StepType.REGULAR)
		npc.faceTile(npc.spawnTile())
		it.waitForTile(npc.spawnTile())
		npc.unlock()
		npc.faceTile(npc.spawnTile().transform(0, -1))
	}
	
	/**
	 * A function used to define each individual attack sequence.
	 */
	fun attack(npc: Npc, target: Entity, style: CombatStyle, consecutiveAttackActive: Boolean, canMelee: Boolean) {
		// Make sure we don't melee when we can't melee.
		if (!canMelee && style == CombatStyle.MELEE) {
			return
		}
		
		val animation = ANIMATIONS.getOrElse(style, { DEFAULT_ANIMATION })
		
		val projectile = projectiles.getOrElse(style, { DEFAULT_PROJECTILE })
		
		if (consecutiveAttackActive) {
			if (style == CombatStyle.MELEE) {
				npc.putattrib(AttributeKey.CERBERUS_COMBAT_STYLE_ROTATION_ACTIVE, false)
			}
			hit(npc, target, style, projectile, animation)
			return
		}
		if (npc.hp() >= 400) {
			hit(npc, target, style, projectile, animation)
			return
		}
		val roll = npc.world().random(if (npc.hp() < 200) 80 else if (npc.hp() < 400) 90 else 100)
		
		when (roll) {
			in 0..19 -> {
				val key = AttributeKey.CERBERUS_SUMMONED_SOUL_ATTACK_UNAVAILABLE
				
				if (npc.hp() < 400 && !npc.attribOr<Boolean>(key, false)) {
					npc.animate(4494)
					npc.sync().shout("Aaarrrooooooo")
					
					val npcs = npc.world().npcs().stream().filter {
						it != null && it.spawnTile().distance(npc.spawnTile()) < 25 && SUMMONED_SOUL_IDS.contains(it.id())
					}.collect(Collectors.toList<Npc>())
					
					npc.putattrib(key, true)
					npc.executeScript(SummonedSoulAttackScript(target, npcs).script)
					return
				}
			}
		/*      in 0..80 -> {
				  if (npc.hp() < 200) {
					  npc.animate(4494)
					  npc.sync().shout("Grrrrrrrrrrrrrr")
					  npc.executeScript(CerberusAOEAttackScript(target).script)
					  return
				  }
			  }*/
		}
		
		hit(npc, target, style, projectile, animation)
	}
	
	/**
	 * Hits the target, animates the origin of the attack, and displays a projectile.
	 */
	private fun hit(npc: Npc, target: Entity, style: CombatStyle, projectile: Int, animation: Int) {
		var delay = 0
		
		if (projectile != -1) {
			npc.world().spawnProjectile(npc.tile().transform(1, 1), target, projectile, 60, 15, 25, 20 * npc.tile().distance(
					target.tile()), 15, 10)
			
			delay = 3
			npc.animate(animation)
			
			target.hit(npc, if (EntityCombat.attemptHit(npc, target, style)) EntityCombat.randomHit(npc) else 0, delay)
					.combatStyle(style).applyProtection().graphic(Graphic(1244, 60))
			
		} else {
			npc.animate(animation)
			target.hit(npc, if (EntityCombat.attemptHit(npc, target, style)) EntityCombat.randomHit(npc) else 0, 0)
					.combatStyle(style).applyProtection()
		}
	}
	
	/**
	 * Calculates the next attack which is always in the same order.
	 */
	private fun nextConsecutiveAttack(npc: Npc): CombatStyle {
		val last = npc.attribOr<CombatStyle>(AttributeKey.CERBERUS_COMBAT_STYLE_ROTATION_ACTIVE, CombatStyle.MAGIC)
		
		return when (last) {
			CombatStyle.MAGIC -> return CombatStyle.RANGE
			CombatStyle.RANGE -> return CombatStyle.MELEE
			else -> CombatStyle.MAGIC
		}
	}
	
	/**
	 * Generates a random style where each style has an (fairly) equal chance
	 * of being returned.
	 */
	private fun randomStyle(canMelee: Boolean): CombatStyle {
		val random = Random().nextInt(100)
		
		if (random < 33)
			return CombatStyle.MAGIC
		else if (random < 66)
			return CombatStyle.RANGE
		else if (!canMelee)  // If we can't melee, default to mage unless any of the previous odds did a style.
			return CombatStyle.MAGIC
		
		return CombatStyle.MELEE
	}
}