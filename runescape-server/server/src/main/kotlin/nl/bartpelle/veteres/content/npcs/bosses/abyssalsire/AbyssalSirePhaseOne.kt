package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.hitorigin.PoisonOrigin
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Situations on 10/27/2016.
 */

object AbyssalSirePhaseOne {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onTimer(TimerKey.ABYSSAL_SIRE_DISORIENTATION) {
			val abyssalSire = it.npc()
			val abyssalSirePhase = abyssalSire.attribOr<AbyssalSirePhase>(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
			
			//If the Abyssal Sire disorientation timer goes off and Abyssal Sire is currently in the first combat phase, re-initiate the phase..
			if (abyssalSirePhase == AbyssalSirePhase.PHASE_ONE)
				initPhaseOne(abyssalSire)
		}
	}
	
	@JvmStatic fun initPhaseOne(abyssalSire: Npc) {
		val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
		val abyssalSireRespiratorySystem = AbyssalSireRespiratorySystem.getRespiratorySystem(abyssalSire)
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.SWITCHING_PHASE)
		
		abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
			abyssalSire.lockDamageOk()
			AbyssalSireTentacles.lockTentacles(abyssalSireTentacles)
			
			abyssalSire.animate(4528)
			abyssalSire.sync().transmog(5887)
			
			s.delay(1)
			
			AbyssalSireTentacles.animateTentacles(abyssalSireTentacles, 7114)
			AbyssalSireTentacles.transmogTentacles(abyssalSireTentacles, 5912)
			AbyssalSireTentacles.removeClipping(abyssalSireTentacles)
			
			s.delay(7)
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
			
			AbyssalSireTentacles.putCombatState(abyssalSireTentacles, AbyssalSireState.COMBAT)
			AbyssalSireRespiratorySystem.putCombatState(abyssalSireRespiratorySystem, AbyssalSireState.COMBAT)
			
			s.delay(3)
			
			abyssalSire.unlock()
			AbyssalSireTentacles.unlockTentacles(abyssalSireTentacles)
			
			s.delay(2)
			
			abyssalSire.executeScript(AbyssalSire.script)
		})
	}
	
	fun phaseOneAttack(script: Script, abyssalSire: Npc, targetedPlayer: Entity) {
		val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
		val abyssalSireRespiratory = AbyssalSireRespiratorySystem.getRespiratorySystem(abyssalSire)
		
		//Face the Abyssal Sire's tentacles to the player's tile
		AbyssalSireTentacles.facePlayer(abyssalSireTentacles, targetedPlayer)
		
		//Attempt to hit the player if they're within the tentacle's hit radius
		AbyssalSireTentacles.attemptToHitPlayer(script, abyssalSireTentacles, targetedPlayer)
		
		//Attempt to heal the Respiratory System's if they've been damages
		if (abyssalSire.world().rollDie(3, 1))
			AbyssalSireRespiratorySystem.attemptToHeal(abyssalSireRespiratory)
		
		//If they player is in Abyssal Sire's combat radius, roll for an attack
		val combatArea = Area(abyssalSire.tile().x, abyssalSire.tile().z - 10,
				abyssalSire.tile().x + 5, abyssalSire.tile().z - 7)
		
		val extendedTiles = arrayOf(Tile(abyssalSire.tile().x + 1, abyssalSire.tile().z - 6),
				Tile(abyssalSire.tile().x + 2, abyssalSire.tile().z - 6),
				Tile(abyssalSire.tile().x + 3, abyssalSire.tile().z - 6),
				Tile(abyssalSire.tile().x + 4, abyssalSire.tile().z - 6))
		
		if (targetedPlayer.tile().inArea(combatArea) || onExtendedTiles(targetedPlayer, extendedTiles)) {
			val spawningMinion = abyssalSire.attribOr<Boolean>(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, false)
			val spawningFumes = abyssalSire.attribOr<Boolean>(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, false)
			
			//If the Abyssal Sire isn't currently spawning a minion or poisonous fumes, roll to attack!
			if (!spawningFumes && !spawningMinion) {
				if (abyssalSire.world().rollDie(2, 1))
					summonSpawn(abyssalSire, targetedPlayer, combatArea)
				
				//Else, if the Abyssal Sire isn't currently spawning fumes, try and spawn one!
				else summonPoisonousFumes(abyssalSire, targetedPlayer)
			}
		}
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSire)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		abyssalSire.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSire, abyssalSire.combatInfo().attackspeed)
	}
	
	private fun summonSpawn(abyssalSire: Npc, targetedPlayer: Entity, areaToSpawn: Area) {
		val targetTile = Tile(areaToSpawn.x1() + abyssalSire.world().random(3),
				areaToSpawn.z1() + abyssalSire.world().random(4))
		val tileDistance = abyssalSire.tile().transform(2, 2, 0).distance(targetTile)
		val abyssalSireSpawn = Npc(5916, abyssalSire.world(), targetTile)
		val abyssalSireSpawns = abyssalSire.attribOr<ArrayList<Npc>>(AttributeKey.ABYSSAL_SIRE_SPAWNS, ArrayList<Npc>())
		
		abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
			abyssalSire.lockDamageOk()
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, true)
			
			abyssalSire.animate(4530)
			abyssalSire.world().spawnProjectile(abyssalSire.tile().transform(2, 2), targetTile, 1274, 90, 0, 80, 16 * tileDistance, 14, 5)
			
			s.delay(Math.max(1, (90 + (tileDistance * 16)) / 30))
			abyssalSireSpawn.respawns(false)
			abyssalSireSpawn.walkRadius(40)
			abyssalSireSpawn.putattrib(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, targetedPlayer)
			abyssalSireSpawn.world().registerNpc(abyssalSireSpawn.spawnDirection(6))
			abyssalSireSpawn.animate(4523, 5)
			
			s.delay(2)
			abyssalSireSpawn.attack(targetedPlayer)
			abyssalSireSpawns.add(abyssalSireSpawn)
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNS, abyssalSireSpawns)
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, false)
			abyssalSireSpawn.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWN_OWNER, abyssalSire)
			
			abyssalSire.unlock()
		})
	}
	
	private fun summonPoisonousFumes(abyssalSire: Npc, targetedPlayer: Entity) {
		val targetsTile = targetedPlayer.tile()
		
		abyssalSire.lockDamageOk()
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, true)
		
		abyssalSire.animate(4531)
		abyssalSire.world().tileGraphic(1275, targetsTile, 0, 0)
		
		abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
			s.delay(2)
			
			for (i in 0..3) {
				if (targetedPlayer.tile().inSqRadius(targetsTile, 1)) {
					targetedPlayer.hit(PoisonOrigin(), targetedPlayer.world().random(IntRange(6, 28))).type(Hit.Type.POISON).block(false)
					if (targetedPlayer.world().rollDie(3, 1))
						targetedPlayer.poison(8)
				}
				
				s.delay(1)
			}
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, false)
			abyssalSire.unlock()
		})
	}
	
	private fun onExtendedTiles(targetedPlayer: Entity, extendedTiles: Array<Tile>): Boolean {
		extendedTiles.forEach { tile -> if (tile == targetedPlayer.tile()) return true }
		return false
	}
	
	@Suspendable fun attemptToDisorient(targetedPlayer: Entity, abyssalSire: Npc, chance: Int) {
		if (abyssalSire.world().rollDie(100, chance)) disorientAbyssalSire(targetedPlayer, abyssalSire)
	}
	
	@Suspendable fun disorientAbyssalSire(targetedPlayer: Entity, abyssalSire: Npc) {
		val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
		val abyssalSireRespiratory = AbyssalSireRespiratorySystem.getRespiratorySystem(abyssalSire)
		val abyssalSireState = abyssalSire.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
		
		if (abyssalSireState != AbyssalSireState.SWITCHING_PHASE) {
			abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
				abyssalSire.lockNoDamage()
				AbyssalSireTentacles.lockTentacles(abyssalSireTentacles)
				
				AbyssalSireRespiratorySystem.putCombatState(abyssalSireRespiratory, AbyssalSireState.DISORIENTED)
				AbyssalSireTentacles.putCombatState(abyssalSireTentacles, AbyssalSireState.DISORIENTED)
				abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.DISORIENTED)
				
				AbyssalSireTentacles.animateTentacles(abyssalSireTentacles, 7112)
				
				s.delay(4)
				abyssalSire.sync().transmog(5886)
				AbyssalSireTentacles.transmogTentacles(abyssalSireTentacles, 5913)
				s.delay(4)
				
				AbyssalSireTentacles.faceSouth(abyssalSireTentacles)
				targetedPlayer.message("The Sire has been disorientated temporarily.")
				abyssalSire.timers().addOrSet(TimerKey.ABYSSAL_SIRE_DISORIENTATION, 35)
				abyssalSire.unlock()
			})
		}
	}
}