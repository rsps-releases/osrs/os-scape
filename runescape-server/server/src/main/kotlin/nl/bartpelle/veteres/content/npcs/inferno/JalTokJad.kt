package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Mack on 7/30/2017.
 */
object JalTokJad {

	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		val world = npc.world()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		if (npc.attribOr<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER, null) != null) {
			npc.attrib<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER).tick(it)
		}
		
		while(PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 15) && EntityCombat.attackTimerReady(npc)) {
				
				when(world.random(3)) {
					1 -> {
						magic(npc, target, world)
					}
					2 -> {
						range(npc, target, world)
					}
					else -> {
						
						if (EntityCombat.canAttackMelee(npc, target)) {
							melee(npc, target, world)
						} else if (world.rollDie(2, 1)) {
							range(npc, target, world)
						} else {
							magic(npc, target, world)
						}
					}
				}
			}
		
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun melee(npc: Npc, target: Entity, world: World) {
		
		npc.animate(7590)
		
		if (target.isPlayer && target.varps().varbit(Varbit.PROTECT_FROM_MELEE) == 1) {
			target.hit(npc, 0, 1)
		} else {
			target.hit(npc, world.random(npc.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	fun magic(npc: Npc, target: Entity, world: World) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 35)
		
		npc.animate(7592)
		
		target.world().server().scriptExecutor().executeScript(target.world(), @Suspendable { s->
			
			s.delay(2)
			npc.graphic(447, 525, 23)
			world.spawnProjectile(npc, target, 448, 130, 36, 35, 8 * tileDist, 10, 5)
			world.spawnProjectile(npc, target, 449, 130, 36, 38, 8 * tileDist, 10, 5)
			world.spawnProjectile(npc, target, 450, 130, 36, 41, 8 * tileDist, 10, 5)
			
			if (target.isPlayer && target.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1) {
				target.hit(npc, 0, delay)
			} else {
				target.hit(npc, world.random(70), delay).combatStyle(CombatStyle.MAGIC)
			}
		})
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	fun range(npc: Npc, target: Entity, world: World) {
		
		npc.animate(7593)
		
		target.world().server().scriptExecutor().executeScript(target.world(), @Suspendable {
			
			it.delay(3)
			target.graphic(451)
			it.delay(2)
			
			if (target.isPlayer && target.varps().varbit(Varbit.PROTECT_FROM_MISSILES) == 1) {
				target.hit(npc, 0, 0)
			} else {
				target.hit(npc, world.random(70), 0).combatStyle(CombatStyle.RANGE)
			}
		})
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
}