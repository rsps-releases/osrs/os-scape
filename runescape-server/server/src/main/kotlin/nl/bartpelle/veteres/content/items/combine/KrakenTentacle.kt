package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/16/2015.
 */
object KrakenTentacle {
	
	@JvmStatic @ScriptMain fun krakenTentacle(repo: ScriptRepository) {
		repo.onItemOnItem(4151, 12004) @Suspendable { makeWhip(it) } // tent on norm whip
		
		repo.onItemOption4(12006) {
			// Check charges
			it.player().message("Nothing interesting happens.")
		}
		
		repo.onItemOption5(12006, s@ @Suspendable {
			val player = it.player()
			// Don't use commands during staking!
			if (player.interfaces().visible(106) || player.interfaces().visible(482) || player.interfaces().visible(110) || player.interfaces().visible(119)) {
				return@s
			}
			
			if (it.player().world().realm().isPVP) {
				it.messagebox2("<col=7f0000>Warning!</col><br>Reverting the tentacle to a whip does NOT return the kraken tentacle, only the whip. Are you sure?")
				
				if (it.customOptions("Are you sure you wish to do this?", "Yes, revert the tentacle to a abyssal whip.", "No, I'll keep my tentacle.") == 1) {
					if (it.player().inventory().count(12006) > 0) {
						// Apply inv change
						it.player().inventory() -= 12006
						it.player().inventory() += 4151
						// Do some nice graphics
						it.animate(713)
					}
				}
			} else {
				it.messagebox("This option is only available on the PvP server.")
			}
		})
	}
	
	@Suspendable private fun makeWhip(it: Script) {
		it.messagebox2("<col=7f0000>Warning!</col><br>The tentacle will gradually consume your whip and destroy it. You<br>won't be able to get the whip out again.<br>The combined item is not tradeable.")
		
		if (it.customOptions("Are you sure you wish to do this?", "Yes, let the tentacle consume the whip.", "No, I'll keep my whip.") == 1) {
			if (it.player().inventory().count(4151) > 0 && it.player().inventory().count(12004) > 0) {
				// Apply inv change
				it.player().inventory() -= 4151
				it.player().inventory() -= 12004
				it.player().inventory() += 12006
				// Do some nice graphics
				it.animate(713)
			}
			
		}
	}
	
}
