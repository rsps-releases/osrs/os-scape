package nl.bartpelle.veteres.content.minigames.raids

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.ClientScriptUtils
import nl.bartpelle.veteres.util.Misc
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */
object RaidInterfaces {

    private const val RECRUITING_BOARD_OBJ = 29776
    const val PARTY_MEMBERS_LAYER = 500
    const val RECRUITING_BOARD_LAYER = 499
    const val RAID_PARTY_SETUP_LAYER = 507

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onObject(RECRUITING_BOARD_OBJ, {
            if (it.player().world().realm().isOSRune) @Suspendable {
                if (it.interactionOption() == 1) {
                    reloadRecruitingBoard(it.player())
                }
            }
        })
        sr.onButton(RECRUITING_BOARD_LAYER, 3, @Suspendable {
            if (it.player().world().realm().isOSRune) {
                onRecruitBoardButton(it)
            }
        })
        sr.onButton(RECRUITING_BOARD_LAYER, 13, @Suspendable {
            attemptJoinParty(it)
        })
        sr.onButton(RAID_PARTY_SETUP_LAYER, 3, @Suspendable {
            onPartyManageButton(it)
        })
    }

    @Suspendable private fun onRecruitBoardButton(it: Script) {
        val player = it.player()

        when(player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1)) {
            0 -> reloadRecruitingBoard(player)
            1 -> createRaidParty(it)
            else -> player.interfaces().closeMain()
        }
    }

    @Suspendable private fun onPartyManageButton(it: Script) {
        val player = it.player()
        val raidParty = RaidsResources.partyById(player.attribOr<Int>(AttributeKey.RAID_PARTY, -1)) ?: return

        when(player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1)) {
            0 -> {
                if (it.optionsTitled("Leave ${raidParty.ownerName()}'s party?", "Yes, I'd like to be removed from this raid party.", "No, I'd like to stay in this raid party.") == 1) {
                    raidParty.chatChannel().leave(player, false)
                }
            }
            1 -> {
                if (raidParty.advertised()) {
                    if (it.optionsTitled("De-list party?", "Yes, I'd like to remove my party from public listing.", "No, I'd like to keep my party public.") == 1) {
                        raidParty.removeListing()
                        reloadPartySetupBoard(it)
                        player.message("<col=FF0000>Your raid party has successfully been de-listed.</col>")
                    }
                } else {
                    raidParty.addListing()
                    reloadPartySetupBoard(it)
                    player.message("<col=FF0000>Your raid party is now being publicly listed. Anyone who wishes may join.</col>")
                }
            }
            2 -> {
                if (it.optionsTitled("Disband party?", "Yes, I'd like to disband my current party.", "No, I wish to keep my party.") == 1) {
                    raidParty.disband()
                    reloadRecruitingBoard(player)
                }
            }
            3 -> {
                it.delay(1)
                reloadPartySetupBoard(it)
            }
            4 -> {
                it.delay(1)
                reloadRecruitingBoard(player)
            }
            5 -> {
                raidParty.partySizePreference = it.inputInteger("Enter party size preference:")
                player.varps().varbit(Varbit.RAID_PARTY_SIZE_PREF, raidParty.partySizePreference)
            }
            6 -> {
                raidParty.combatLevelPreferece = it.inputInteger("Enter combat level preference:")
                player.varps().varbit(Varbit.RAID_CMB_LVL_PREF, raidParty.combatLevelPreferece)
            }
            7 -> {
                raidParty.skillLevelPreference = it.inputInteger("Enter skill level preference:")
                player.varps().varbit(Varbit.RAID_SKILL_LVL_PREF, raidParty.skillLevelPreference)
            }
            else -> player.interfaces().closeMain()
        }
    }

    @Suspendable private fun createRaidParty(it: Script) {
        val player = it.player()
        val channel = player.world().chats()[(player.attrib<Int>(AttributeKey.CLAN_CHANNEL))]
        val currentPartyId = player.attribOr<Int>(AttributeKey.RAID_PARTY, -1)

        it.delay(1)

        when(currentPartyId) {
            -1 -> {
                if (channel == null) {
                    it.messagebox("You must be in a chat-channel to create a party.")
                    return
                } else if (channel.raidParties().stream().anyMatch({party -> party.status() == RaidsResources.RaidPartyStatus.LOBBY.value()})) {
                    it.messagebox("A raid party has already been created in your current chat-channel and is waiting to be started.")
                    return
                } else if (channel.rankFor(player.id() as Int) < 7) {
                    it.messagebox("You must be ranked as a Captain or above to setup a party in this chat-channel.")
                    return
                }

                val party = RaidParty(player.id() as Int, player.name(), player.world(), channel)

                party.create(RaidsResources.generateId(), channel)
                reloadPartySetupBoard(it)
            }
            else -> reloadPartySetupBoard(it)
        }
    }

    private fun displayRaidPartyInfo(player: Player, party: RaidParty) {
        val partyOwner = party.world().playerForId(party.ownerId())
        val editorFlag = party.ownerId() == player.id()
        val advertised = party.advertised()

        player.interfaces().sendMain(RAID_PARTY_SETUP_LAYER)

        player.invokeScript(1524, "Raiding Party of ${partyOwner.get().name()} (${party.members().size})", if (editorFlag) 2 else 1, if (advertised) 1 else 0, 15)
        player.varps().varbit(Varbit.RAID_PARTY_SIZE_PREF, party.partySizePreference)
        player.varps().varbit(Varbit.RAID_CMB_LVL_PREF, party.combatLevelPreferece)
        player.varps().varbit(Varbit.RAID_SKILL_LVL_PREF, party.skillLevelPreference)

        for (idx in 0 until party.members().size) {

            val plr = party.members()[idx]
            var data = if (plr.id() == partyOwner.get().id()) "<col=ffffff>${plr.name()}</col>|" else "<col=9f9f9f>${plr.name()}</col>|"

            data += ClientScriptUtils.addRow(plr.skills().combatLevel().toString())
            data += ClientScriptUtils.addRow(plr.skills().totalLevel().toString())

            for (skill in 0 until plr.skills().levels().size) {
                data += ClientScriptUtils.addRow(plr.skills().levels()[skill].toString())
            }

            player.invokeScript(1517, idx, data)
        }
    }

    @Suspendable fun reloadRecruitingBoard(player: Player) {
        player.interfaces().setting(RECRUITING_BOARD_LAYER, 13, 0, 39, 1)
        player.interfaces().sendMain(RECRUITING_BOARD_LAYER)

        val limit = if (RaidsResources.raidParties().size > 39) RaidsResources.raidParties().size else 39
        for (idx in 0..limit) {
            val party = RaidsResources.partyById(idx)
            if (party == null || !party.advertised()) {
                player.invokeScript(1566, idx, "")
                continue
            }

            player.invokeScript(1566, idx, "<col=ffffff>${party.ownerName()}</col>|${party.members().size}|${party.partySizePreference}|${party.combatLevelPreferece}|${party.skillLevelPreference}|${Misc.longToTick(party.creation())}")
        }
    }

    @Suspendable private fun reloadPartySetupBoard(it: Script) {
        val player = it.player()

        val party = RaidsResources.partyById(player.attribOr<Int>(AttributeKey.RAID_PARTY, -1))

        if (party == null) {
            it.messagebox("This party has been disbanded.")
            return
        }

        displayRaidPartyInfo(player, party)
    }

    @Suspendable private fun attemptJoinParty(it: Script) {
        val player = it.player()
        val slot = player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1)

        if (slot == -1) return

        val selectedParty = RaidsResources.raidPartyAdvertisedListings()[slot]
        val currentParty = RaidsResources.partyById(player.attribOr(AttributeKey.RAID_PARTY, -1))

        if (currentParty != null && selectedParty.id() != currentParty.id()) {
            it.messagebox("You must leave your current party before joining another.")
        } else if (currentParty == null) {
            if (it.optionsTitled("Join ${selectedParty.ownerName()}'s party?", "Yes, I'd like to join the raid party.", "No, I want to look for another raid party.") == 1) {
                if (selectedParty.skillLevelPreference > player.skills().totalLevel()) {
                    it.messagebox("You require a skill level of ${selectedParty.skillLevelPreference} to join this party.")
                    return
                }
                if (selectedParty.combatLevelPreferece > player.skills().combatLevel()) {
                    it.messagebox("You require a combat level of ${selectedParty.combatLevelPreferece} to join this party.")
                    return
                }
                if (!selectedParty.chatChannel().join(player)) {
                    return
                }

                displayRaidPartyInfo(player, selectedParty)
            }
        } else {
            displayRaidPartyInfo(player, selectedParty)
        }
    }

    @Suspendable fun updatePartySessionLayer(player: Player) {
        player.interfaces().sendWidgetOn(PARTY_MEMBERS_LAYER, Interfaces.InterSwitches.QUEST_TAB)

        player.interfaces().invokeScript(1548)
    }
}