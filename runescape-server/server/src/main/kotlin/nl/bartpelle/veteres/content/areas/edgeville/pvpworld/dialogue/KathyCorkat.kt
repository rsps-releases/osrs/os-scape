package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/29/2015.
 */

object KathyCorkat {
	
	val KATHY_CORKAT = 4298
	
	val RARES_SHOP = 3
	val MISCELLANEOUS_SHOP_2 = 1349
	val MISCELLANEOUS_SHOP_3 = 1351
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(KATHY_CORKAT) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(RARES_SHOP).display(it.player())
			} else {
				it.player().message("You can only view this shop in the PK server.")
			}
		}
	}
}