package nl.bartpelle.veteres.content.mechanics.deadman

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.examineItemMessage
import nl.bartpelle.veteres.content.waitForInterfaceClose
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jak on 05/11/2016.
 */
object DeadmanSafeDepositBox {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		// Access Safe DMM Deposit Box relevent DMM Wizard Bankers
		for (i in intArrayOf(6485, 6510, 6514, 6515, 6516, 6517, 6518, 6519, 6703)) {
			repo.onNpcOption4(i) @Suspendable {
				val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
				it.player().interfaces().sendMain(230)
				it.player().interfaces().sendInventory(232)
				it.player().write(InterfaceSettings(230, 5, 0, 9, 1180734))
				it.player().write(InterfaceSettings(232, 2, 0, 27, 1180734))
				it.player().write(SetItems(563, -1, 63739, dmminfo.protectedItems.lootcontainer))
				it.player().write(SetItems(93, -1, 64209, it.player().inventory()))
				it.onInterrupt { it.player().interfaces().closeById(230) }
				it.waitForInterfaceClose(230)
				it.player().interfaces().closeById(230)
			}
		}
		
		repo.onInterfaceClose(230) {
			// Deposit box close, close the inventory too.
			it.player().interfaces().closeById(232)
		}
		
		repo.onButton(230, 9) {
			// Deposit all inventory
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val toRemove = ArrayList<Item>(10)
			for (i in it.player().inventory().items()) {
				i ?: continue
				val size = getProtectedBoxCount(dmminfo)
				if (size >= 10)
					break
				val toAdd = Math.min(10 - size, i.amount())
				val added = Item(i.id(), toAdd)
				dmminfo.protectedItems.lootcontainer.add(added, true)
				toRemove.add(added)
			}
			for (removed in toRemove) {
				it.player().inventory().remove(removed, true)
			}
			it.player().write(SetItems(563, -1, 63739, dmminfo.protectedItems.lootcontainer))
			it.player().write(SetItems(93, -1, 64209, it.player().inventory()))
		}
		
		repo.onButton(230, 11) {
			// Deposit all equipment
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val toRemove = ArrayList<Item>(10)
			for (i in it.player().equipment().items()) {
				i ?: continue
				var size = 0
				for (protected in dmminfo.protectedItems.lootcontainer) {
					protected ?: continue
					size += protected.amount()
				}
				if (size >= 10)
					break
				val toAdd = Math.min(10 - size, i.amount())
				val added = Item(i.id(), toAdd)
				dmminfo.protectedItems.lootcontainer.add(added, true)
				toRemove.add(added)
			}
			for (removed in toRemove) {
				it.player().equipment().remove(removed, true)
			}
			it.player().write(SetItems(563, -1, 63739, dmminfo.protectedItems.lootcontainer))
			it.player().equipment().makeDirty()
		}
		
		repo.onButton(230, 5, s@ @Suspendable {
			// Withdraw item from Protected Box
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val button = it.buttonAction()
			val slot = it.buttonSlot()
			val item: Item = dmminfo.protectedItems.lootcontainer[slot] ?: return@s
			if (button == 10) { // Examine
				it.examineItemMessage(item)
				return@s
			}
			val toTake = when (button) {
				1 -> 1
				2 -> 5
				3 -> 10
				4 -> it.inputInteger("How many do you wish to withdraw?")
				5 -> Integer.MAX_VALUE
				else -> 1
			}
			val taken = Item(item.id(), Math.min(item.amount(), toTake))
			if (it.player().inventory().add(taken, false).success()) {
				dmminfo.protectedItems.lootcontainer.remove(taken, true)
				it.player().write(SetItems(563, -1, 63739, dmminfo.protectedItems.lootcontainer))
				it.player().inventory().makeDirty()
			}
		})
		
		repo.onButton(232, 2, s@ @Suspendable {
			// Add item to protected box
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val button = it.buttonAction()
			val slot = it.buttonSlot()
			val item: Item = it.player().inventory()[slot] ?: return@s
			if (button == 10) { // Examine
				it.examineItemMessage(item)
				return@s
			}
			val toAdd = when (button) {
				1 -> 1
				2 -> 5
				3 -> 10
				4 -> it.inputInteger("How many do you wish to store?")
				5 -> Integer.MAX_VALUE
				else -> 1
			}
			val free = 10 - getProtectedBoxCount(dmminfo)
			val taken = Item(item.id(), Math.min(item.amount(), Math.min(free, toAdd)))
			if (dmminfo.protectedItems.lootcontainer.add(taken, false).success()) {
				it.player().inventory().remove(taken, true)
				it.player().write(SetItems(563, -1, 63739, dmminfo.protectedItems.lootcontainer))
				it.player().inventory().makeDirty()
			}
		})
	}
	
	/**
	 * Obtain how many items are in your Protected Box. You can have a total of 10, and even if stackable each single item counts as 1.
	 * For example, 10 runes will fill your Protection box and no more items can be added.
	 */
	@JvmStatic fun getProtectedBoxCount(dmminfo: DeadmanContainers): Int {
		var size = 0
		for (protected in dmminfo.protectedItems.lootcontainer) {
			protected ?: continue
			size += protected.amount()
		}
		return size
	}
}