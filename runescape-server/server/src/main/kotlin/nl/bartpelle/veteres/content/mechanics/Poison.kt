package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.hitorigin.PoisonOrigin
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 11/18/2015.
 */
object Poison {
	
	fun determineHit(poisonticks: Int): Int {
		return poisonticks / 5 + 1
	}
	
	@JvmStatic fun ticksForDamage(damage: Int): Int {
		return damage * 5 - 4
	}
	
	@JvmStatic fun cure(player: Player) {
		player.varps()[Varp.HP_ORB_COLOR] = 0
	}
	
	@JvmStatic fun cureAndImmune(player: Player, immunityTicks: Int) {
		player.varps()[Varp.HP_ORB_COLOR] = -immunityTicks
	}
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		r.onLogin { it.player().timers().register(TimerKey.POISON, 25) }
		r.onNpcCreate { it.npc().timers().register(TimerKey.POISON, 25) }
		
		/**
		 * Note that Venom has its own timer to track both immunity and remaining cycles, whereas poison
		 * uses the Varp value.
		 */
		r.onTimer(TimerKey.POISON) {
			val ctx: Entity = it.ctx<Entity>()
			
			ctx.timers().extendOrRegister(TimerKey.POISON, 25) // restart
			
			if (Venom.venomed(ctx))
				return@onTimer
			
			if (ctx.isPlayer) {
				// Grab value. More than 0 means we're poisoned for X ticks, lower than X means we're immune.
				val poisonticks = it.player().varps()[Varp.HP_ORB_COLOR]
				
				if (poisonticks > 0) {
					if(!Staking.in_duel(it.player()))
						it.player().hit(PoisonOrigin(), Math.min(20, determineHit(poisonticks)), 0, false).type(Hit.Type.POISON).block(false).submit()
					it.player().varps()[Varp.HP_ORB_COLOR] = poisonticks - 1 // reduce as normal
				} else if (poisonticks < 0) {
					it.player().varps()[Varp.HP_ORB_COLOR] = poisonticks + 1 // increment it back to 0 from negative (from being immune)
				}
				
			} else {
				// Grab value. More than 0 means we're poisoned for X ticks, lower than X means we're immune.
				val poisonticks = it.npc().attribOr<Int>(AttributeKey.POISON_TICKS, 0)
				
				if (poisonticks > 0) {
					it.npc().hit(PoisonOrigin(), Math.min(20, determineHit(poisonticks))).type(Hit.Type.POISON).block(false)
					it.npc().putattrib(AttributeKey.POISON_TICKS, poisonticks - 1)
				} else if (poisonticks < 0) {
					it.npc().putattrib(AttributeKey.POISON_TICKS, poisonticks + 1)
				}
				
			}
		}
	}
	
}