package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 23/12/2015.
 */

object PirateShip {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(272) @Suspendable {
			//up to deck
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3018 && obj.tile().z == 3957) {
				Ladders.ladderUp(it, Tile(3018, 3958, 1), true)
			}
		}
		r.onObject(273) @Suspendable {
			//down to hold
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3018 && obj.tile().z == 3957) {
				Ladders.ladderDown(it, Tile(3018, 3958, 0), true)
			}
		}
		r.onObject(245) @Suspendable {
			//ship ladder z1 to z2
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if ((obj.tile().x == 3019 && obj.tile().z == 3959) || (obj.tile().x == 3017 && obj.tile().z == 3959)) {
				it.player().teleport(it.player().tile().transform(0, 2, 1))
			}
		}
		r.onObject(246) @Suspendable {
			//down to deck z1
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if ((obj.tile().x == 3019 && obj.tile().z == 3959) || (obj.tile().x == 3017 && obj.tile().z == 3959)) {
				it.player().teleport(it.player().tile().transform(0, -2, -1))
			}
		}
	}
	
}
