package nl.bartpelle.veteres.content.skills.slayer

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.get

import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.red
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 12/9/2016.
 */
object ImbuedHeart {
	
	val IMBUED_HEART = 20724
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(IMBUED_HEART) {
			if (it.player.timers().has(TimerKey.IMBUED_HEART_COOLDOWN)) {
				val ticks = it.player.timers().left(TimerKey.IMBUED_HEART_COOLDOWN)
				
				if (ticks >= 100) {
					val minutes = ticks / 100
					it.message("The heart is still drained of its power. Judging by how it feels, it will be ready in around $minutes minutes.")
				} else {
					val seconds = ticks / 10 * 6
					it.message("The heart is still drained of its power. Judging by how it feels, it will be ready in around $seconds seconds.")
				}
			} else {
				if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_DRINKS)) {
					it.player().message("Stat-boosting items are disabled for this duel.")
				} else if (ClanWars.checkRule(it.player(), CWSettings.DRINKS)) {
					it.player().message("Drinks are disabled for this clan war.")
				} else {
					it.player.graphic(1316)
					it.player.timers().register(TimerKey.IMBUED_HEART_COOLDOWN, 700)
					
					// This boost will only increment.
					val boost = 1 + (it.player.skills().xpLevel(Skills.MAGIC) / 10)
					if (it.player.skills()[Skills.MAGIC] == it.player.skills().xpLevel(Skills.MAGIC)) {
						it.player.skills().alterSkill(Skills.MAGIC, boost)
					}
				}
			}
		}
		
		r.onTimer(TimerKey.IMBUED_HEART_COOLDOWN) {
			it.player.message("Your imbued heart has regained its magical power.".red())
		}
	}
	
}