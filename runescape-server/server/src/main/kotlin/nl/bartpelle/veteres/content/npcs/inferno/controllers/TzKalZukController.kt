package nl.bartpelle.veteres.content.npcs.inferno.controllers

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.tzhaar.TzhaarKetKeh
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.mechanics.cutscene.Cutscene
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.content.npcs.inferno.JalMejJak
import nl.bartpelle.veteres.content.npcs.inferno.TzKalZuk
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces

/**
 * Created by Mack on 8/3/2017.
 */
class TzKalZukController(zuk: Npc, session: InfernoSession): InfernoNpcController {
	
	/**
	 * The active inferno session.
	 */
	val session = session
	
	/**
	 * The npc who is owner of this controller instance.
	 */
	val zuk = zuk
	
	/**
	 * The starting tile for the healer spawns.
	 */
	val startingTile = Tile(zuk.tile().x - 10, zuk.tile().z - 3)
	
	/**
	 * The collection of healers for the Zuk boss.
	 */
	var healers: ArrayList<Npc> = ArrayList()
	
	override fun onSpawn() {
		zuk.spawnDirection(6)
		zuk.tile(Tile(session.area!!.center().x - 4,  session.area!!.center().z + 18))
		TzKalZuk.spawnWall(session.player(), session.area!!.center(), session)
		displayBossOverlay(zuk, session.player())
//		zuk.putattrib(AttributeKey.UNPROTECTABLE_ATTACKS, true)
		zuk.attack(session.player)
	}
	
	override fun onDamage() {
		if (!InfernoContext.inSession(session.player())) {
			session.player().message("I don't think I should be doing this...")
			return
		}
		super.onDamage()
	}
	
	override fun triggerPlayerKilledDeath() {
		
		//Prevent any remaining npcs doing damage to our winner.
		session.player().lock()
		
		if (!session.activeNpcs.isEmpty()) {
			
			for (remaining in session.activeNpcs) {
				session.player().world().unregisterNpc(remaining)
			}
		}
		
//		zuk.clearattrib(AttributeKey.UNPROTECTABLE_ATTACKS)
		
		//end session
		session.end(true)
	}
	
	fun spawnJad() {
		val player = session.player()
		val area = InfernoContext.activeArea(player)
		val jad: Npc = Npc(InfernoContext.JALTOK_JAD, player.world(), Tile(area!!.center().x + 3, area.center().z + 4))
		val ancestralGlyph: Npc = if (session.player().hasAttrib(AttributeKey.INFERNO_SESSION)) session.player().attrib<InfernoSession>(AttributeKey.INFERNO_SESSION).ancestralGlyph!! else return
		
		jad.walkRadius(0)
		jad.respawns(false)
		jad.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 0)
		
		session.activeNpcs.add(jad)
		player.world().registerNpc(jad)
		
		if (!ancestralGlyph.dead()) {
			jad.attack(ancestralGlyph)
		} else {
			jad.attack(session.player())
		}
		
		player.message("<col=FF0000>Look out! The TzKal-Zuk has summoned a JalTok-Jad!")
	}
	
	fun spawnHealers(index: Int) {
		val healer = Npc(InfernoContext.JAL_MEJJAK, zuk.world(), Tile(startingTile.x + (index * 3), session.area!!.center().z + 18))
		
		healer.walkRadius(0)
		healer.respawns(false)
		healer.face(zuk)
		
		if (InfernoContext.get(session.player()) != null) {
			InfernoContext.get(session.player())!!.activeNpcs.add(healer)
		}
		
		healers.add(healer)
		session.player().world().registerNpc(healer)
		healer.animate(2864)
		
		healer.executeScript(JalMejJak(zuk, session).healingScript)
	}
	
	/**
	 * Displays the boss' current and max hitpoints on the overlay.
	 */
	fun displayBossOverlay(npc: Npc, player: Player) {
		player.varps().varbit(5653, npc.hp())
		player.varps().varbit(5654, npc.combatInfo().stats.hitpoints)
		player.interfaces().sendWidgetOn(InfernoContext.TZKAL_ZUK_OVERLAY, Interfaces.InterSwitches.T)
	}
}