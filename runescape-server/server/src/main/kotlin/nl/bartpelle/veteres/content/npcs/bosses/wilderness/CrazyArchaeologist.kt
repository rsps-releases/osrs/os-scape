package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 1/28/2016.
 */

object CrazyArchaeologist {
	
	val misc_shout = arrayOf("I'm Bellock - respect me!", "Get off my site!", "No-one messes with Bellock's dig!", "These ruins are mine!",
			"Taste my knowledge!", "You belong in a museum!")
	val special_shout = "Rain of knowledge!"
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		//While the target is available to attack we..
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			//Check to see if we're able to melee the target..
			if (EntityCombat.canAttackMelee(npc, target, false) && EntityCombat.attackTimerReady(npc)) {
				
				//Send the explosive books!
				if (target.world().rollDie(20, 1)) { // 5% chance the target sends explosive books
					//Animate the NPC
					npc.animate(3353)
					
					it.delay(1)
					special_attack(it, npc, target)
					
					//..take a nap
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, 3)
					
					//Send the post hit logic
					EntityCombat.postHitLogic(npc)
					it.delay(1)
					target = EntityCombat.refreshTarget(it) ?: return@s
				}
				
				//Attack the player
				melee_attack(npc, target)
				
				//..and take a quick nap
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, 3)
				
				//Else we check if we can range the target..
			} else if (EntityCombat.canAttackDistant(npc, target, true, 5) && EntityCombat.attackTimerReady(npc)) {
				
				//Send the explosive books!
				if (target.world().rollDie(20, 1)) { // 5% chance the target sends explosive books
					//Animate the NPC
					npc.animate(3353)
					
					it.delay(1)
					special_attack(it, npc, target)
					
					//..take a nap
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, 3)
					
					//Send the post hit logic
					EntityCombat.postHitLogic(npc)
					it.delay(1)
					target = EntityCombat.refreshTarget(it) ?: return@s
				}
				
				//Attack the player
				ranged_attack(npc, target)
				
				//..and take a quick nap
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, 3)
			}
			//Send the post hit logic
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	//Handle the melee attack
	fun melee_attack(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, EntityCombat.randomHit(npc))
		} else {
			target.hit(npc, 0) // Uh-oh, that's a miss.
		}
		//Shout!
		npc.sync().shout(misc_shout[target.world().random(5)])
		
		//Animate the NPC
		npc.animate(npc.attackAnimation())
	}
	
	//Handle the ranged attack
	fun ranged_attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().distance(target.tile())
		val delay = Math.max(1, (20 + tileDist * 12) / 30)
		
		//Shout!
		npc.sync().shout(misc_shout[target.world().random(5)])
		
		//Send the projectile and animate the NPC
		npc.world().spawnProjectile(npc, target, 1259, 25, 25, 35, 12 * tileDist, 15, 10)
		npc.animate(3353)
		
		//Determine the damage dealt to the target
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.RANGE, 1.0))
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.RANGE)
		else
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.RANGE)
		
		//Roll a die to see if we send the target graphic
		if (target.world().rollDie(3, 1))
			target.graphic(305, 0, 24 * tileDist)
	}
	
	//Handle the special attack
	fun special_attack(it: Script, npc: Npc, target: Entity) {
		val x = target.tile().x //The target's x tile
		val z = target.tile().z //The target's z tile
		
		//Handle the first explosive
		val explosive_book_one = Tile(x + target.world().random(2) + 1, z + target.world().random(2) + 1)
		val explosive_book_one_distance = npc.tile().distance(explosive_book_one)
		val explosive_book_one_delay = Math.max(1, (20 + explosive_book_one_distance * 12) / 30)
		
		//Handle the second explosive
		val explosive_book_two = Tile(x + target.world().random(2), z + target.world().random(2) + 1)
		val explosive_book_two_distance = npc.tile().distance(explosive_book_two)
		val explosive_book_two_delay = Math.max(1, (20 + explosive_book_two_distance * 12) / 30)
		
		//Handle the third explosive
		val explosive_book_three = Tile(x + target.world().random(1), z)
		val explosive_book_three_distance = npc.tile().distance(explosive_book_three)
		val explosive_book_three_delay = Math.max(1, (20 + explosive_book_three_distance * 12) / 30)
		
		//Shout!
		npc.sync().shout(special_shout)
		
		//Send the projectiles
		npc.world().spawnProjectile(npc.tile(), explosive_book_one, 1260, 50, 0, explosive_book_one_delay, 24 * explosive_book_one_distance, 35, 10)
		npc.world().spawnProjectile(npc.tile(), explosive_book_two, 1260, 50, 0, explosive_book_two_delay, 24 * explosive_book_two_distance, 35, 10)
		npc.world().spawnProjectile(npc.tile(), explosive_book_three, 1260, 50, 0, explosive_book_three_delay, 24 * explosive_book_three_distance, 35, 10)
		
		//Send the tile graphic
		target.world().tileGraphic(157, explosive_book_one, 1, 24 * explosive_book_one_distance)
		target.world().tileGraphic(157, explosive_book_two, 1, 24 * explosive_book_two_distance)
		target.world().tileGraphic(157, explosive_book_three, 1, 24 * explosive_book_three_distance)
		
		it.runGlobal(target.world()) @Suspendable {
			//Create a delay before checking if the player is on the explosive tile
			it.delay(explosive_book_one_distance)
			//For each player in the world we..
			//Check to see if the player's tile is the same as the first explosive book..
			if (target.tile().inSqRadius(Tile(explosive_book_one), 1))
				target.hit(npc, target.world().random(23)).combatStyle(CombatStyle.MAGIC)
			//Check to see if the player's tile is the same as the second explosive book..
			if (target.tile().inSqRadius(Tile(explosive_book_two), 1))
				target.hit(npc, target.world().random(23)).combatStyle(CombatStyle.MAGIC)
			//Check to see if the player's tile is the same as the third explosive book..
			if (target.tile().inSqRadius(Tile(explosive_book_three), 1))
				target.hit(npc, target.world().random(23)).combatStyle(CombatStyle.MAGIC)
		}
		
		//Grab the coordinates of the tile our ricochets come from..
		val explosive_x = explosive_book_two.x //The x coordinates of explosive book two
		val explosive_z = explosive_book_two.z //The z coordinates of explosive book two
		
		//Handle the first ricochet
		val ricochet_explosive_book_one = Tile(explosive_x + 2, explosive_z + 1 + target.world().random(2))
		val ricochet_explosive_book_one_distance = explosive_book_two.distance(ricochet_explosive_book_one)
		val ricochet_explosive_book_one_delay = Math.max(1, (20 + ricochet_explosive_book_one_distance * 12) / 30)
		
		//Handle the second ricochet
		val ricochet_explosive_book_two = Tile(explosive_x + 1, explosive_z + target.world().random(2) + 2)
		val ricochet_explosive_book_two_distance = explosive_book_two.distance(ricochet_explosive_book_two)
		val ricochet_explosive_book_two_delay = Math.max(1, (20 + ricochet_explosive_book_two_distance * 12) / 30)
		
		it.runGlobal(target.world()) @Suspendable {
			//Create a delay before sending the ricochet explosives
			it.delay(explosive_book_two_distance)
			
			//Send the projectiles
			npc.world().spawnProjectile(explosive_book_two, ricochet_explosive_book_one, 1260, 0, 0, ricochet_explosive_book_one_delay, 50 * ricochet_explosive_book_one_distance, 50, 20)
			npc.world().spawnProjectile(explosive_book_two, ricochet_explosive_book_two, 1260, 0, 0, ricochet_explosive_book_two_delay, 50 * ricochet_explosive_book_two_distance, 50, 20)
			
			//Send the tile graphic
			target.world().tileGraphic(157, ricochet_explosive_book_one, 1, 50 * ricochet_explosive_book_one_distance)
			target.world().tileGraphic(157, ricochet_explosive_book_two, 1, 50 * ricochet_explosive_book_two_distance)
			
			//Create a delay before checking if the player is on the explosive tile
			it.delay(ricochet_explosive_book_one_delay)
			
			//Check to see if the player's tile is the same as the first explosive book..
			if (target.tile().inSqRadius(Tile(ricochet_explosive_book_one), 1))
				target.hit(npc, target.world().random(23)).combatStyle(CombatStyle.MAGIC)
			//Check to see if the player's tile is the same as the second explosive book..
			if (target.tile().inSqRadius(Tile(ricochet_explosive_book_two), 1))
				target.hit(npc, target.world().random(23)).combatStyle(CombatStyle.MAGIC)
		}
	}
}