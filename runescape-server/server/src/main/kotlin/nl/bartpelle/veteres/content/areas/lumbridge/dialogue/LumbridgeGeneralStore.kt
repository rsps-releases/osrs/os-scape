package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/4/2015.
 */

object LumbridgeGeneralStore {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (keeper in intArrayOf(507, 506)) {
			r.onNpcOption1(keeper) @Suspendable {
				it.chatNpc("Can I help you at all?", keeper, 567)
				when (it.options("Yes please. What are you selling?", "No thanks.")) {
					1 -> {
						if (it.player().ironMode() != IronMode.NONE) {
							it.player().world().shop(1).display(it.player())
						} else {
							it.player().world().shop(23).display(it.player())
						}
					}
					2 -> {
						it.chatPlayer("No thanks.", 588)
					}
				}
			}
			r.onNpcOption2(keeper) {
				if (it.player().ironMode() != IronMode.NONE) {
					it.player().world().shop(1).display(it.player())
				} else {
					it.player().world().shop(23).display(it.player())
				}
			}
		}
	}
	
}
