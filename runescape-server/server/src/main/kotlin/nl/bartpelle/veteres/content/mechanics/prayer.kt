package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.fs.VarbitDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.EquipmentInfo
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import java.util.*

/**
 * Created by Bart on 8/24/2015.
 */


val QUICKPRAY_REPO: HashMap<Int, QuickPrayInfo> = HashMap()

@ScriptMain fun prayers(repo: ScriptRepository) {
	repo.onLogin { it.player().timers()[TimerKey.PRAYER_TICK] = 1 } // Drain 1 tick later.
	
	repo.onTimer(TimerKey.PRAYER_TICK) {
		// Drain this tick, and reset the timer to 1 to drain next tick. Forever and ever and evvvvvvvver
		it.player().timers().extendOrRegister(TimerKey.PRAYER_TICK, 1)
		
		// Dont drain if dead, or no prayers on.
		if (it.player().dead() || it.player().varps().varbit(Varbit.REGULAR_PRAYERS) == 0 ||
				it.player().world().cycleCount() <= it.player().attribOr<Int>(AttributeKey.PRAYER_ON_TICK, it.player().world().cycleCount())) {
			it.player().putattrib(AttributeKey.PRAYERINCREMENT, 0) // reset
			return@onTimer
		}
		//it.player().message("on:%s now:%s drain:%s", it.player().attrib<Int>(AttributeKey.PRAYER_ON_TICK), it.player().world().cycleCount(), it.player().attrib<Int>(AttributeKey.PRAYERINCREMENT))
		var drain: Double = PrayerDrain.compute(it.player())
		if (drain > 0) {
			val pray = EquipmentInfo.prayerBonuses(it.player(), it.player().world().equipmentInfo())
			//it.player().debug("drain: %f  bonus:%d  saved:%f", drain, pray, if (pray < 1) 0.0 else (drain / (1 + (0.0333 * pray))))
			if (pray > 0) {
				// slows the drain rate by 3.33% of the regular drain rate of the Prayer(s)
				drain /= 1 + (0.0333 * pray)
			}
			
			if (it.player().skills().level(Skills.PRAYER) > 0) {
				if (!VarbitAttributes.varbiton(it.player(), Varbit.INFPRAY)) {
					val totalDrains: Double = it.player().attribOr<Double>(AttributeKey.PRAYERINCREMENT, 0.0)
					it.player().putattrib(AttributeKey.PRAYERINCREMENT, totalDrains + drain)
					if (totalDrains > 1.0) {
						it.player().putattrib(AttributeKey.PRAYERINCREMENT, totalDrains - 1)
						it.player().skills().setLevel(Skills.PRAYER, Math.max(0, it.player().skills().level(Skills.PRAYER) - 1))
					}
				}
			}
			
			if (it.player().skills().level(Skills.PRAYER) < 1) {
				// Cant get smited when dead dead, you must be smited before like RS.
				Prayers.disableAllPrayers(it.player())
				it.message("You have run out of prayer points, you must recharge at an altar.")
			}
		}
	}
	
	// Prayer toggles
	QUICKPRAY_REPO.clear()
	registerWithSound(repo, 541, 4, 0, Varbit.THICK_SKIN, 1, "Thick Skin", 2690, Varbit.ROCK_SKIN, Varbit.STEEL_SKIN, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 5, 1, Varbit.BURST_OF_STRENGTH, 4, "Burst of Strength", 2688, Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_WILL,
			Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_WILL, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 6, 2, Varbit.CLARITY_OF_THOUGHT, 7, "Clarity of Thought", 2664, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.MYSTIC_WILL,
			Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_WILL, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 22, 18, Varbit.SHARP_EYE, 8, "Sharp Eye", 2685, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH,
			Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_LORE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY,
			Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 23, 19, Varbit.MYSTIC_WILL, 9, "Mystic Will", 2670, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH,
			Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_LORE, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY,
			Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 7, 3, Varbit.ROCK_SKIN, 10, "Rock Skin", 2684, Varbit.THICK_SKIN, Varbit.STEEL_SKIN, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 8, 4, Varbit.SUPERHUMAN_STRENGTH, 13, "Superhuman Strength", 2689, Varbit.BURST_OF_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_WILL,
			Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_WILL, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 9, 5, Varbit.IMPROVED_REFLEXIS, 16, "Improved Reflexis", 2662, Varbit.CLARITY_OF_THOUGHT, Varbit.INCREDIBLE_REFLEXES, Varbit.MYSTIC_WILL,
			Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_WILL, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 10, 6, Varbit.RAPID_RESTORE, 19, "Rapid Restore", 2679)
	registerWithSound(repo, 541, 11, 7, Varbit.RAPID_HEAL, 22, "Rapid Heal", 2678)
	registerWithSound(repo, 541, 12, 8, Varbit.PROTECT_ITEM, 25, "Protect Item", 1982)
	registerWithSound(repo, 541, 24, 20, Varbit.HAWK_EYE, 26, "Hawk Eye", 2666, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH,
			Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_LORE, Varbit.EAGLE_EYE, Varbit.SHARP_EYE, Varbit.CHIVALRY, Varbit.PIETY,
			Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 25, 21, Varbit.MYSTIC_LORE, 27, "Mystic Lore", 2668, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH,
			Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_WILL, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY,
			Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 13, 9, Varbit.STEEL_SKIN, 28, "Steel Skin", 2687, Varbit.ROCK_SKIN, Varbit.THICK_SKIN, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 14, 10, Varbit.ULTIMATE_STRENGTH, 31, "Ultimate Strength", 2691, Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH, Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT,
			Varbit.MYSTIC_LORE, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 15, 11, Varbit.INCREDIBLE_REFLEXES, 34, "Incredible Reflexes", 2667, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT,
			Varbit.MYSTIC_LORE, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 16, 12, Varbit.PROTECT_FROM_MAGIC, 37, "Protect from Magic", 2675, Varbit.PROTECT_FROM_MELEE, Varbit.PROTECT_FROM_MISSILES, Varbit.RETRIBUTION, Varbit.REDEMPTION, Varbit.SMITE)
	registerWithSound(repo, 541, 17, 13, Varbit.PROTECT_FROM_MISSILES, 40, "Protect from Missiles", 2677, Varbit.PROTECT_FROM_MAGIC, Varbit.PROTECT_FROM_MELEE, Varbit.RETRIBUTION, Varbit.REDEMPTION, Varbit.SMITE)
	registerWithSound(repo, 541, 18, 14, Varbit.PROTECT_FROM_MELEE, 43, "Protect from Melee", 2676, Varbit.PROTECT_FROM_MISSILES, Varbit.PROTECT_FROM_MAGIC, Varbit.RETRIBUTION, Varbit.REDEMPTION, Varbit.SMITE)
	registerWithSound(repo, 541, 26, 22, Varbit.EAGLE_EYE, 44, "Eagle Eye", 2665, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH,
			Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_LORE, Varbit.HAWK_EYE, Varbit.SHARP_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 27, 23, Varbit.MYSTIC_MIGHT, 45, "Mystic Might", 2669, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH,
			Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_LORE, Varbit.MYSTIC_WILL, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 19, 15, Varbit.RETRIBUTION, 46, "Retribution", 2682, Varbit.PROTECT_FROM_MAGIC, Varbit.PROTECT_FROM_MELEE, Varbit.PROTECT_FROM_MISSILES, Varbit.REDEMPTION, Varbit.SMITE)
	registerWithSound(repo, 541, 20, 16, Varbit.REDEMPTION, 49, "Redemption", 2680, Varbit.PROTECT_FROM_MAGIC, Varbit.PROTECT_FROM_MELEE, Varbit.PROTECT_FROM_MISSILES, Varbit.RETRIBUTION, Varbit.SMITE)
	registerWithSound(repo, 541, 21, 17, Varbit.SMITE, 52, "Smite", 2686, Varbit.PROTECT_FROM_MELEE, Varbit.PROTECT_FROM_MAGIC, Varbit.PROTECT_FROM_MISSILES, Varbit.RETRIBUTION, Varbit.REDEMPTION)
	registerWithSound(repo, 541, 32, 28, Varbit.PRESERVE, 55, "Preserve", 3825)
	registerWithSound(repo, 541, 28, 25, Varbit.CHIVALRY, 60, "Chivalry", 3826, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES,
			Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.THICK_SKIN, Varbit.ROCK_SKIN, Varbit.STEEL_SKIN, Varbit.MYSTIC_LORE,
			Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 29, 26, Varbit.PIETY, 70, "Piety", 3825, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES,
			Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.THICK_SKIN, Varbit.ROCK_SKIN, Varbit.STEEL_SKIN, Varbit.MYSTIC_LORE,
			Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.AUGURY, Varbit.RIGOUR)
	registerWithSound(repo, 541, 30, 24, Varbit.RIGOUR, 74, "Rigour", 3825, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES,
			Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.THICK_SKIN, Varbit.ROCK_SKIN, Varbit.STEEL_SKIN, Varbit.MYSTIC_LORE,
			Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY)
	registerWithSound(repo, 541, 31, 27, Varbit.AUGURY, 77, "Augury", 3825, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES,
			Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.THICK_SKIN, Varbit.ROCK_SKIN, Varbit.STEEL_SKIN, Varbit.MYSTIC_LORE,
			Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.RIGOUR)
}

fun register(repo: ScriptRepository, b: Int, c: Int, slot: Int, varbit: Int, level: Int, name: String, vararg toggle: Int) {
	registerWithSound(repo, b, c, slot, varbit, level, name, -1, -1, *toggle)
}

fun registerWithSound(repo: ScriptRepository, b: Int, c: Int, slot: Int, varbit: Int, level: Int, name: String, soundOn: Int, vararg toggle: Int) {
	// Register the button
	repo.onButton(b, c) {
		// Remap button IDs when using old positions.
		val useOld = VarbitAttributes.varbiton(it.player(), VarbitAttributes.VarbitInfo.USE_OLD_PRAYER_POS.varbitid)
		if (useOld) {
			if (c == 26) { // Eagle eye to rigour..
				togglePrayer(it, Varbit.RIGOUR, 74, "Rigour", 3825, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES,
						Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.THICK_SKIN, Varbit.ROCK_SKIN, Varbit.STEEL_SKIN, Varbit.MYSTIC_LORE,
						Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY) // Toggle Rigour
				return@onButton
			} else if (c == 30) { // Rigour to eagle eye..
				togglePrayer(it, Varbit.EAGLE_EYE, 44, "Eagle Eye", 2665, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS,
						Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH,
						Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.MYSTIC_LORE, Varbit.HAWK_EYE,
						Varbit.SHARP_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR) // Toggle Eagle Eye
				return@onButton
			} else if (c == 27) { // Mystic Might to Augury
				togglePrayer(it, Varbit.AUGURY, 77, "Augury", 3825, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES,
						Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH, Varbit.ULTIMATE_STRENGTH, Varbit.THICK_SKIN, Varbit.ROCK_SKIN, Varbit.STEEL_SKIN, Varbit.MYSTIC_LORE,
						Varbit.MYSTIC_WILL, Varbit.MYSTIC_MIGHT, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.RIGOUR) // Toggle Augury
				return@onButton
			} else if (c == 31) { // Augury to Mystic Might
				togglePrayer(it, Varbit.MYSTIC_MIGHT, 45, "Mystic Might", 2669, Varbit.CLARITY_OF_THOUGHT, Varbit.IMPROVED_REFLEXIS, Varbit.INCREDIBLE_REFLEXES, Varbit.BURST_OF_STRENGTH, Varbit.SUPERHUMAN_STRENGTH,
						Varbit.ULTIMATE_STRENGTH, Varbit.MYSTIC_LORE, Varbit.MYSTIC_WILL, Varbit.SHARP_EYE, Varbit.EAGLE_EYE, Varbit.HAWK_EYE, Varbit.CHIVALRY, Varbit.PIETY, Varbit.AUGURY, Varbit.RIGOUR) // Toggle Mystic Might
				return@onButton
			}
		}
		
		togglePrayer(it, varbit, level, name, soundOn, *toggle)
	}
	
	// Enlist data class for quick pray toggling
	var disable = IntArray(toggle.size)
	for (i in 0..toggle.size - 1) {
		disable += repo.server().definitions().get(VarbitDefinition::class.java, toggle[i]).startbit
	}
	
	QUICKPRAY_REPO.put(slot, QuickPrayInfo(slot, varbit, level, name, disable))
}

data class QuickPrayInfo(val slot: Int, val bitval: Int, val level: Int, val name: String, val disableVals: IntArray) {
	
}

@ScriptMain @Suspendable fun QuickPrayerOrb(repo: ScriptRepository) {
	repo.onButton(160, 14, s@ @Suspendable {
		val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
		it.player().interfaces().closeMain()
		if (option == 1) {
			if (it.player().skills().level(Skills.PRAYER) < 1) {
				it.player().message("You need to recharge your Prayer at an altar.")
				return@s
			}
			if (it.player().hp() < 0 || it.player().locked()) {
				it.player().varps().sync(375) // QP on/off --  Set it back in the client.
				return@s
			}
			val current = it.player().varps().varp(Varp.QUICK_PRAYER_SELECTED)
			val on = it.player().varps().varbit(Varbit.QUICK_PRAYERS_ON) != 0
			
			// If it is on, we either put it all off if the prayers are identical now, or add/remove the missing ones.
			if (on) {
				it.player().varps().varp(Varp.PRAYERS_ACTIVE, 0)
				it.player().varps().varbit(Varbit.QUICK_PRAYERS_ON, 0)
			} else {
				if (current == 0) {
					it.player().varps().varbit(Varbit.QUICK_PRAYERS_ON, 0)
					it.player().message(" You haven't selected any quick-prayers.")
				} else {
					if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_PRAYER)) {
						it.player().message("Prayer is disabled for this duel.")
						return@s
					}
					if (ClanWars.checkRule(it.player(), CWSettings.PRAYER, 2)) {
						it.player().message("Prayer is disabled for this clan war.")
						return@s
					}
					if (it.player().varps().varp(Varp.PRAYERS_ACTIVE) == 0) {
						it.player().putattrib(AttributeKey.PRAYER_ON_TICK, it.player().world().cycleCount())
					}
					it.player().varps().varp(Varp.PRAYERS_ACTIVE, current)
					it.player().varps().varbit(Varbit.QUICK_PRAYERS_ON, 1)
					
					Prayers.adjust_quick_prayers_for_level(it.player())
				}
				
			}
			
			it.player().looks().update()
		}
		if (option == 2) { // Hide/set
			it.player().interfaces().sendWidgetOn(77, Interfaces.InterSwitches.PRAYER_TAB)
			it.player().interfaces().setting(548, 49, -1, -1, 2)
			it.player().interfaces().setting(77, 4, 0, 28, 2)
			it.player().invokeScript(InvokeScript.OPEN_TAB, 5)
		}
	})
	repo.onButton(77, 5) {
		// Probs show qp setup
		it.player().interfaces().sendWidgetOn(541, Interfaces.InterSwitches.PRAYER_TAB)
	}
	repo.onButton(77, 4) {
		val option: Int = it.player().attrib(AttributeKey.BUTTON_SLOT)
		var current = it.player().varps().varp(Varp.QUICK_PRAYER_SELECTED)
		var has = (current and (1 shl option)) != 0
		val info = QUICKPRAY_REPO.get(option)!!
		
		if (it.player().skills().xpLevel(Skills.PRAYER) < info.level) {
			it.message("You need a Prayer level of ${info.level} to use ${info.name}.")
		} else {
			// Disable some prayers if we're turning them on
			if (!has) {
				disableQuickPray(it.player(), *info.disableVals)
			}
			
			current = it.player().varps().varp(Varp.QUICK_PRAYER_SELECTED)
			has = (current and (1 shl option)) != 0
			
			if (has) {
				// If it's set, unset it
				val newval = current and (1 shl option).inv() // Dumb ass kotlin without | or ~ or & uhhhhhhhhhgggggggggg
				it.player().varps().varp(Varp.QUICK_PRAYER_SELECTED, newval)
			} else {
				// If not, we add the value
				val newval = current or (1 shl option)
				it.player().varps().varp(Varp.QUICK_PRAYER_SELECTED, newval)
			}
		}
	}
}

fun disableQuickPray(player: Player, vararg bits: Int) {
	bits.forEach { bit ->
		val current = player.varps().varp(Varp.QUICK_PRAYER_SELECTED)
		val newval = current and (1 shl bit).inv()
		player.varps().varp(Varp.QUICK_PRAYER_SELECTED, newval)
	}
}

@Suspendable fun togglePrayer(it: Script, varbit: Int, levelreq: Int, prayername: String, soundOn: Int, vararg disable: Int) {
	if (it.player().dead() || it.player().hp() < 1) {
		it.player().varps().sync(Varp.PRAYERS_ACTIVE)
		return
	}
	// on 07 you can activte a prayer mid teleport and it only activates after the task that locked you is finished performing.
	if (it.player().locked()) {
		var lockedFor = 0
		while (it.player().locked() && lockedFor++ < 10) {
			it.delay(1)
		}
		// Nothing lasts this long, not agility obstacles or teleports.
		if (lockedFor >= 10) {
			it.player().varps().sync(Varp.PRAYERS_ACTIVE)
			return
		}
	}
	if (DuelOptions.ruleToggledOn(it.player(), DuelOptions.NO_PRAYER)) {
		it.player().varps().sync(Varp.PRAYERS_ACTIVE)
		it.player().message("Prayer is disabled for this duel.")
		return
	}
	
	if (ClanWars.checkRule(it.player(), CWSettings.PRAYER, 2)) {
		it.player().varps().sync(Varp.PRAYERS_ACTIVE)
		it.player().message("Prayer is disabled for this clan war.")
		return
	}
	
	if (it.player().skills().xpLevel(Skills.PRAYER) < levelreq) {
		it.messagebox("You need a <col=000080>Prayer</col> level of ${levelreq} to use <col=000080>${prayername}.")
		// Client-side defence check for activation means you don't have to re-sync
		return
	}
	if (varbit == Varbit.PIETY && it.player().skills().xpLevel(Skills.DEFENCE) < 70) {
		it.player().message("You need 70 Defence to use Piety.")
		// Client-side defence check for activation means you don't have to re-sync
		return
	}
	if (varbit == Varbit.CHIVALRY && it.player().skills().xpLevel(Skills.DEFENCE) < 65) {
		it.player().message("You need 65 Defence to use Chivalry.")
		// Client-side defence check for activation means you don't have to re-sync
		return
	}
	if (varbit == Varbit.PROTECT_ITEM) {
		if (it.player().attribOr<Boolean>(AttributeKey.NON_PLUS1_AREA, false)) {
			it.message("The Protect Item prayer cannot be used in High Risk areas.")
			return
		}
		if (it.player().world().realm().isDeadman) {
			it.messagebox("The Protect item prayer cannot be used on Deadman worlds.")
			return
		}
	}
	
	if (varbit == Varbit.RIGOUR && it.player().varps().varbit(Varbit.UNLOCK_RIGOUR) == 0) {
		it.messagebox("You have to learn how to use <col=000080>Rigour</col> before activating it.")
		return
	}
	
	if (varbit == Varbit.RIGOUR && it.player().skills().xpLevel(Skills.DEFENCE) < 70) {
		it.player().message("You need 70 Defence to use Rigour.")
		return
	}
	
	if (varbit == Varbit.AUGURY && it.player().varps().varbit(Varbit.UNLOCK_AUGURY) == 0) {
		it.messagebox("You have to learn how to use <col=000080>Augury</col> before activating it.")
		return
	}
	
	if (varbit == Varbit.AUGURY && it.player().skills().xpLevel(Skills.DEFENCE) < 70) {
		it.player().message("You need 70 Defence to use Augury.")
		return
	}
	
	if (varbit == Varbit.PRESERVE && it.player().varps().varbit(Varbit.UNLOCK_PRESERVE) == 0) {
		it.messagebox("You have to learn how to use <col=000080>Preserve</col> before activating it.")
		return
	}
	
	/*if (varbit == Varbit.PROTECT_FROM_MAGIC || varbit == Varbit.PROTECT_FROM_MELEE || varbit == Varbit.PROTECT_FROM_MISSILES) {
		if (Tile.pvp(script.player()) && script.player().skills().combatLevel() > 100) {
			// Mains
			script.player().varps().sync(Varp.PRAYERS_ACTIVE)
			if (script.player().timers().has(TimerKey.IN_COMBAT)) {
				script.player().message("<col=9A2EFE>You can't use protection prayers in Edgeville PvP.")
			} else {
				script.messagebox("You can't use protection prayers in Edgeville PvP!")
			}
			return
		}
	}*/
	
	if (varbit == Varbit.PROTECT_FROM_MELEE || varbit == Varbit.PROTECT_FROM_MAGIC || varbit == Varbit.PROTECT_FROM_MISSILES
			|| varbit == Varbit.SMITE || varbit == Varbit.RETRIBUTION || varbit == Varbit.REDEMPTION) {
		if (it.player().timers().has(TimerKey.OVERHEADS_BLOCKED)) {
			it.message("You cannot use overhead prayers right now.")
			it.player().varps().sync(Varp.PRAYERS_ACTIVE)
			return
		}
		if (ClanWars.checkRule(it.player(), CWSettings.PRAYER)) {
			it.player().message("Prayers with overhead icons are not permitted in this battle.")
			it.player().varps().sync(Varp.PRAYERS_ACTIVE)
			return
		}
		
	}
	if (varbit == Varbit.PROTECT_FROM_MELEE || varbit == Varbit.PROTECT_FROM_MAGIC || varbit == Varbit.PROTECT_FROM_MISSILES) {
		val map = it.player().world().allocator().active(it.player().tile())
		if (map.isPresent) with(map.get()) {
			if (identifier.isPresent && identifier.get() == InstancedMapIdentifier.EDGE_PVP) {
				it.player().message("Protection prayers are <col=FF0000>disallowed</col> in Edgeville PVP.")
				it.player().varps().sync(Varp.PRAYERS_ACTIVE)
				return
			}
		}
	}
	
	if (it.player().skills().xpLevel(Skills.PRAYER) >= levelreq && it.player().skills().level(Skills.PRAYER) > 0) {
		// No prayers currently active and we're gonna turn one on, note the tick we're starting prayer.
		if (it.player().varps().varp(Varp.PRAYERS_ACTIVE) == 0) {
			it.player().putattrib(AttributeKey.PRAYER_ON_TICK, it.player().world().cycleCount())
		}
		val state = it.player().varps().varbit(varbit)
		it.player().varps().varbit(varbit, if (state == 1) 0 else 1)
		
		// Play a sound
		if (state == 0 && soundOn != -1) {
			it.player().sound(soundOn)
		} else if (state == 1) {
			it.player().sound(2663)
		}
		
		for (v in disable) // Disable the ones we must toggle off
			it.player().varps().varbit(v, 0)
	} else {
		it.player().varps().varbit(varbit, 0)
	}
	it.player().interfaces().closeMain()
	it.player().looks().update()
}

object Prayers {
	@JvmStatic fun disableAllPrayers(player: Player) {
		player.varps().varbit(Varbit.REGULAR_PRAYERS, 0) // Jagex optimisation: this varbit covers the entire varp 83.. bits 0-30! Set to 0 to turn all off.
		player.varps().varbit(Varbit.QUICK_PRAYERS_ON, 0) // Disable quick prayer active
		player.looks().update()
	}
	
	// Disable some saved quick prayers if you've changed your stats (or died in deadman lol) and can no longer activate them!
	@JvmStatic fun adjust_quick_prayers_for_level(player: Player) {
		val pray = player.skills().xpLevel(Skills.PRAYER)
		val def = player.skills().xpLevel(Skills.DEFENCE)
		QUICKPRAY_REPO.values.forEach { qp ->
			if (player.varps().varbit(qp.bitval) == 1 && pray < qp.level) {
				player.varps().varbit(qp.bitval, 0) // Turn that bitch off
			}
			
			if (player.varps().varbit(qp.bitval) == 1) {
				if (qp.bitval == Varbit.PROTECT_FROM_MELEE || qp.bitval == Varbit.PROTECT_FROM_MAGIC || qp.bitval == Varbit.PROTECT_FROM_MISSILES
						|| qp.bitval == Varbit.SMITE || qp.bitval == Varbit.RETRIBUTION || qp.bitval == Varbit.REDEMPTION) {
					if (player.timers().has(TimerKey.OVERHEADS_BLOCKED)) {
						player.message("You cannot use overhead prayers right now.")
						player.varps().varbit(qp.bitval, 0)
						return@forEach
					}
					if (ClanWars.checkRule(player, CWSettings.PRAYER)) {
						player.message("Prayers with overhead icons are not permitted in this battle.")
						player.varps().varbit(qp.bitval, 0)
						return@forEach
					}
					
				}
				if (qp.bitval == Varbit.PROTECT_FROM_MELEE || qp.bitval == Varbit.PROTECT_FROM_MAGIC || qp.bitval == Varbit.PROTECT_FROM_MISSILES) {
					val map = player.world().allocator().active(player.tile()).orElse(null)
					if (map != null && map.identifier.isPresent && map.identifier.get() == InstancedMapIdentifier.EDGE_PVP) {
						player.message("Protection prayers are <col=FF0000>disallowed</col> in Edgeville PVP.")
						player.varps().varbit(qp.bitval, 0)
						return@forEach
					}
				}
			}
		}
		// Defence requirement checks are seperate
		if (player.varps().varbit(Varbit.PIETY) == 1 && def < 70) {
			player.varps().varbit(Varbit.PIETY, 0)
		}
		if (player.varps().varbit(Varbit.CHIVALRY) == 1 && def < 65) {
			player.varps().varbit(Varbit.CHIVALRY, 0)
		}
		if (player.varps().varbit(Varbit.AUGURY) == 1 && def < 70) {
			player.varps().varbit(Varbit.AUGURY, 0)
		}
		if (player.varps().varbit(Varbit.RIGOUR) == 1 && def < 70) {
			player.varps().varbit(Varbit.RIGOUR, 0)
		}
		if (player.varps().varbit(Varbit.PROTECT_ITEM) == 1 && player.attribOr<Boolean>(AttributeKey.NON_PLUS1_AREA, false)) {
			player.varps().varbit(Varbit.PROTECT_ITEM, 0)
		}
		
		if (player.varps().varbit(Varbit.RIGOUR) == 1 && player.varps().varbit(Varbit.UNLOCK_RIGOUR) == 0) {
			player.varps().varbit(Varbit.RIGOUR, 0)
		}
		
		if (player.varps().varbit(Varbit.AUGURY) == 1 && player.varps().varbit(Varbit.UNLOCK_AUGURY) == 0) {
			player.varps().varbit(Varbit.AUGURY, 0)
		}
		
		if (player.varps().varbit(Varbit.PRESERVE) == 1 && player.varps().varbit(Varbit.UNLOCK_PRESERVE) == 0) {
			player.varps().varbit(Varbit.PRESERVE, 0)
		}
		
		if (player.varps().varp(Varp.PRAYERS_ACTIVE) == 0) { //Disable quick prayer if no more prayers are on
			player.varps().varbit(Varbit.QUICK_PRAYERS_ON, 0)
		}
		
		// TODO the value shifted away from our saved CURRENT QP CONFIG
	}
}