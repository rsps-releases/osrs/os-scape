package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.QuestTab
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import java.util.*

/**
 * Created by Situations on 1/1/2016.
 */

object WildernessStatisticsBoard {
	
	
	@Suspendable private fun display(it: Script, player: Array<String?>, kdr: Array<String?>) {
		it.player().invokeScript(InvokeScript.SETVARCS, 16750623, 225)
		it.player().interfaces().sendMain(380)
		it.player().write(InterfaceText(380, 2, "Local K/D Ratios"))
		it.player().invokeScript(498, player.joinToString(" <br>"))
		it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(380) }
	}
	
	@JvmStatic @ScriptMain @Suspendable fun register(repo: ScriptRepository) {
		repo.onObject(26756) @Suspendable {
			val options: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
			
			if (options == 1) {
				val listOfPlayers = generateList(it.player())
				
				listOfPlayers.sortedBy { QuestTab.getKdr(it) }
				
				display(it, listOfPlayers.map {
					"<col=ffff00>".plus(it.name()).plus(":</col>   ").plus(it.varps().varp(Varp.KILLS)).
							plus(" Kills  |  ").plus(it.varps().varp(Varp.DEATHS)).plus(" Deaths  |  ").plus(QuestTab.getKdr(it)).plus(" K/D Ratio")
				}.toTypedArray<String?>(),
						listOfPlayers.map { QuestTab.getKdr(it) }.toTypedArray<String?>())
				
			}
			if (options == 2) {
				if (it.player().world().realm().isPVP) {
					when (it.options("Toggle Kills/Deaths Overlay", "Reset K/D")) {
						1 -> {
							it.player().varps().varbit(Varbit.KDR_OVERLAY, if (it.player().varps().varbitBool(Varbit.KDR_OVERLAY)) 0 else 1)
							it.message("K/D Ratio overlay - ${if (it.player().varps().varbitBool(Varbit.KDR_OVERLAY)) "enabled" else "disabled"}.")
						}
						2 -> {
							if (it.optionsTitled("Are you sure you want to do this?", "Yes, reset my K/D and Kill streak for 1,000 Blood Money.", "No.") == 1) {
								if (it.player().inventory().remove(Item(13307, 1000), false).success()) {
									// Store into all time record
									it.player().putattrib(AttributeKey.ALLTIME_KILLS, it.player().attribOr<Int>(AttributeKey.ALLTIME_KILLS, 0) + it.player().varps().varp(Varp.KILLS))
									it.player().putattrib(AttributeKey.ALLTIME_DEATHS, it.player().attribOr<Int>(AttributeKey.ALLTIME_DEATHS, 0) + it.player().varps().varp(Varp.DEATHS))
									// Reset current
									it.player().varps().set(Varp.KILLS, 0)
									it.player().varps().set(Varp.DEATHS, 0)
									it.player().putattrib(AttributeKey.KILLSTREAK, 0)
									it.player().saveHighscores()
									it.message("You have reset your K/D and Kill streak for 1000 Blood Money.")
								} else {
									it.message("You do not have enough blood money to do this.")
								}
							}
						}
					}
				} else {
					when (it.options("Toggle K/D", "Nothing")) {
						1 -> {
							it.player().varps().varbit(Varbit.KDR_OVERLAY, if (it.player().varps().varbitBool(Varbit.KDR_OVERLAY)) 0 else 1)
							it.message("K/D Ratio overlay - ${if (it.player().varps().varbitBool(Varbit.KDR_OVERLAY)) "enabled" else "disabled"}.")
						}
					}
				}
			}
		}
	}
	
	@Suspendable private fun generateList(player: Player): ArrayList<Player> {
		val listOfPlayers = ArrayList<Player>()
		
		player.world().players().forEachInAreaKt(Area(3066, 3473, 3110, 3519), { p ->
			if (listOfPlayers.size < 22)
				listOfPlayers.add(p)
		})
		
		return listOfPlayers
	}
}
