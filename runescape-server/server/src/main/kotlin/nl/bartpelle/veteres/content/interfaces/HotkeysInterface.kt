package nl.bartpelle.veteres.content.interfaces

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 10/27/2015.
 */
enum class Hotkeyable(val varbit: Int, val button: Int) {
	COMBAT(Varbit.COMBAT_HOTKEY, 22),
	SKILLS(Varbit.SKILLS_HOTKEY, 28),
	QUESTS(Varbit.QUESTS_HOTKEY, 34),
	INVENTORY(Varbit.INVENTORY_HOTKEY, 40),
	EQUIPMENT(Varbit.EQUIPMENT_HOTKEY, 46),
	PRAYERS(Varbit.PRAYERS_HOTKEY, 52),
	MAGIC(Varbit.MAGIC_HOTKEY, 58),
	FRIENDS(Varbit.FRIENDS_HOTKEY, 64),
	IGNORES(Varbit.IGNORES_HOTKEY, 70),
	LOGOUT(Varbit.LOGOUT_HOTKEY, 76),
	SETTINGS(Varbit.SETTINGS_HOTKEY, 82),
	EMOTES(Varbit.EMOTES_HOTKEY, 88),
	CLANCHAT(Varbit.CLANCHAT_HOTKEY, 94),
	MUSIC(Varbit.MUSIC_HOTKEY, 101)
}

object Hotkeys {
	
	//Method called to disable a conflicting hotkey binding.
	fun disableConflicting(player: Player, value: Int) {
		Hotkeyable.values().forEach {
			if (player.varps().varbit(it.varbit) == value)
				player.varps().varbit(it.varbit, 0)
		}
	}
	
	//Method called to toggle hotkey bindings.
	fun toggle(player: Player, thing: Hotkeyable, value: Int) {
		disableConflicting(player, value)
		player.varps().varbit(thing.varbit, value)
	}
	
	//Method used to set the varbit value.
	fun set(player: Player, thing: Hotkeyable, value: Int) {
		player.varps().varbit(thing.varbit, value)
	}
	
	//Registering the buttons used for the hoykey interface.
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		//This handles the toggle arrows on the options.
		Hotkeyable.values().forEach { h ->
			r.onButton(121, h.button) {
				it.player().varps().varbit(4690, h.ordinal)
			}
		}
		
		//This handles selecting the key binding.
		r.onButton(121, 112) {
			val vbit = it.player().varps().varbit(4690)
			val slot: Int = it.player()[AttributeKey.BUTTON_SLOT]
			Hotkeyable.values().forEach { h ->
				if (h.ordinal == vbit) {
					toggle(it.player(), h, slot)
				}
			}
		}
		
		//Restore defaults button.
		r.onButton(121, 104) {
			val player = it.player()
			
			set(player, Hotkeyable.COMBAT, 5)
			set(player, Hotkeyable.SKILLS, 0)
			set(player, Hotkeyable.QUESTS, 0)
			set(player, Hotkeyable.INVENTORY, 1)
			set(player, Hotkeyable.EQUIPMENT, 2)
			set(player, Hotkeyable.PRAYERS, 3)
			set(player, Hotkeyable.MAGIC, 4)
			set(player, Hotkeyable.FRIENDS, 8)
			set(player, Hotkeyable.IGNORES, 9)
			set(player, Hotkeyable.LOGOUT, 0)
			set(player, Hotkeyable.SETTINGS, 10)
			set(player, Hotkeyable.EMOTES, 11)
			set(player, Hotkeyable.CLANCHAT, 7)
			set(player, Hotkeyable.MUSIC, 12)
		}
		
		// Toggle Esc closes interfaces.
		r.onButton(121, 103) {
			it.player().varps().varbit(Varbit.ESC_CLOSES_INTERFACES, if (it.player().varps().varbit(Varbit.ESC_CLOSES_INTERFACES) == 0) 1 else 0)
		}
	}
	
}