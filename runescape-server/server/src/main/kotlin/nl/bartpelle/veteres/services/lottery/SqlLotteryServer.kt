package nl.bartpelle.veteres.services.lottery

import nl.bartpelle.veteres.GameServer
import nl.bartpelle.veteres.content.lottery.Lottery
import nl.bartpelle.veteres.services.sql.PgSqlService
import nl.bartpelle.veteres.services.sql.PgSqlWorker
import nl.bartpelle.veteres.services.sql.SqlTransaction
import java.sql.Connection

/**
 * Created by Jonathan on 2/8/2017.
 */
class SqlLotteryServer(private val server: GameServer) {
	
	private val sqlService by lazy { server.service(PgSqlService::class.java, false).get() }
	private val worker by lazy {
		val w = PgSqlWorker(sqlService)
		Thread(w).start()
		w
	}
	
	fun pull() {
		worker.submit(object : SqlTransaction() {
			override fun execute(connection: Connection) {
				try {
					val sql = connection.prepareStatement("SELECT * FROM lottery where world_id = ?")
					
					sql.setInt(1, server.serviceId())
					
					val results = sql.executeQuery()
					
					Lottery.enabled(false)
					
					while (results.next())
						Lottery.sync(results, server)
					
					connection.commit()
				} catch (e: Exception) {
					connection.rollback()
					throw e
				}
			}
		})
		
		worker.submit(object : SqlTransaction() {
			override fun execute(connection: Connection) {
				try {
					val sql = connection.prepareStatement("SELECT * FROM lottery_winners where world_id = ?")
					
					sql.setInt(1, server.serviceId())
					
					val results = sql.executeQuery()
					
					while (results.next()) Lottery.syncWinners(results)
					
					connection.commit()
				} catch (e: Exception) {
					connection.rollback()
					throw e
				}
			}
		})
	}
	
	fun push() {
		if (Lottery.config == null)
			return
		
		val config = Lottery.config!!
		
		worker.submit(object : SqlTransaction() {
			override fun execute(connection: Connection) {
				try {
					val sql = connection.prepareStatement("UPDATE lottery SET entrants = ?::JSONB, enddate = ? WHERE world_id = ?")
					
					sql.setString(1, config.toJson())
					sql.setLong(2, config.enddate)
					sql.setInt(3, config.worldId)
					
					sql.executeUpdate()
					
					connection.commit()
				} catch (e: Exception) {
					connection.rollback()
					throw e
				}
			}
		})
		
		worker.submit(object : SqlTransaction() {
			override fun execute(connection: Connection) {
				try {
					config.winners.forEach { e ->
						if (e.value <= 0) {
							val sql = connection.prepareStatement("delete from lottery_winners where osrs.public.lottery_winners.account_id = ?;")
							sql.setInt(1, e.key)
							sql.executeUpdate()
							connection.commit()
						} else {
							val sql = connection.prepareStatement("SELECT * FROM insertorupdatewinner(?,?,?);")
							sql.setInt(1, e.key)
							sql.setInt(2, server.serviceId())
							sql.setLong(3, e.value)
							sql.executeQuery()
							connection.commit()
						}
					}
				} catch (e: Exception) {
					connection.rollback()
					throw e
				}
			}
		})
	}
	
}
