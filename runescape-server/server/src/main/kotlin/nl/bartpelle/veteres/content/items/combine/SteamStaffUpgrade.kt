package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 07/06/2016.
 */
object SteamStaffUpgrade {
	
	val KIT = 12798
	val normal_battlestaff = 11787
	val mystic_battlestaff = 11789
	val cosmetic_battlestaff = 12795
	val cosmetic_mysticstaff = 12796
	
	val raw = intArrayOf(normal_battlestaff, mystic_battlestaff)
	val result = intArrayOf(cosmetic_battlestaff, cosmetic_mysticstaff)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (i in 0..raw.size - 1) {
			repo.onItemOnItem(KIT, raw[i]) @Suspendable {
				// Apply upgrade
				val player = it.player()
				val name: String = player.world().definitions().get(ItemDefinition::class.java, raw[i])!!.name
				it.doubleItemBox("Do you want to apply this upgrade to your $name?", KIT, raw[i])
				if (it.options("Yes, apply the upgrade.", "Never mind.") == 1) {
					if (player.inventory().hasAll(KIT, raw[i])) {
						if (player.inventory().remove(Item(KIT), true).success() && player.inventory().remove(Item(raw[i]), true).success()) {
							player.inventory().add(Item(result[i]), true)
							it.itemBox("You apply the upgrade to obtain the cosmetic version.", result[i])
						}
					}
				}
			}
			repo.onItemOption5(result[i]) @Suspendable {
				// "Revert"
				val player = it.player()
				it.doubleItemBox("Do you want to revert the item to its normal form and get the kit back?", result[i], KIT)
				if (it.options("Yes.", "No.") == 1) {
					if (player.inventory().has(result[i]) && player.inventory().freeSlots() >= 1) {
						if (player.inventory().remove(Item(result[i]), true).success()) {
							player.inventory().add(Item(KIT), true)
							player.inventory().add(Item(raw[i]), true)
						}
					}
				}
			}
		}
	}
}