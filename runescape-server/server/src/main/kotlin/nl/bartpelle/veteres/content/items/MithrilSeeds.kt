package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 1/26/2016.
 */

object MithrilSeeds {
	
	//Item ID
	val MITHRIL_SEEDS = 299
	
	//Enum to handle all the flower data: organized from highest -> lowest chance
	enum class Flowers(val item: Int, val obj: Int, val chance: Double) {
		BLACK(2476, 2988, 99.7),
		WHITE(2474, 2987, 99.4),
		ORANGE(2470, 2985, 85.2),
		PURPLE(2468, 2984, 71.0),
		YELLOW(2466, 2983, 56.8),
		BLUE(2464, 2982, 42.6),
		RED(2462, 2981, 28.4),
		MIXED(2460, 2980, 14.2),
		ASSORTED(2472, 2986, 0.00);
	}
	
	//Register clicking the item
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(MITHRIL_SEEDS, s@ @Suspendable {
			if (it.player().tile().region() == 13103) {
				it.messagebox("You can't plant seeds in this arena.")
				return@s
			}
			val chance = it.player().world().randomDouble() * 100 //Generate a random double to use as our 'chance'
			if (chance >= Flowers.BLACK.chance) {
				plant_seed(it, Flowers.BLACK.item, Flowers.BLACK.obj)
			} else if (chance >= Flowers.WHITE.chance) {
				plant_seed(it, Flowers.WHITE.item, Flowers.WHITE.obj)
			} else if (chance >= Flowers.ORANGE.chance) {
				plant_seed(it, Flowers.ORANGE.item, Flowers.ORANGE.obj)
			} else if (chance >= Flowers.PURPLE.chance) {
				plant_seed(it, Flowers.PURPLE.item, Flowers.PURPLE.obj)
			} else if (chance >= Flowers.YELLOW.chance) {
				plant_seed(it, Flowers.YELLOW.item, Flowers.YELLOW.obj)
			} else if (chance >= Flowers.BLUE.chance) {
				plant_seed(it, Flowers.BLUE.item, Flowers.BLUE.obj)
			} else if (chance >= Flowers.RED.chance) {
				plant_seed(it, Flowers.RED.item, Flowers.RED.obj)
			} else if (chance >= Flowers.MIXED.chance) {
				plant_seed(it, Flowers.MIXED.item, Flowers.MIXED.obj)
			} else if (chance >= Flowers.ASSORTED.chance) {
				plant_seed(it, Flowers.ASSORTED.item, Flowers.ASSORTED.obj)
			}
		})
		r.onItemOption1(13654, @Suspendable { //All the cheaters with this item will be exposed. :) - Mack
			it.player().sync().shout("Hey, everyone! I just tried cheating with a nest box. Report me!")
		})
	}
	
	@JvmStatic @Suspendable fun plant_seed(it: Script, item: Int, obj: Int) {
		//Check to see if we're able to plant a flower
		if (it.player().world().objByType(10, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
				|| it.player().world().objByType(11, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null) {
			it.message("You can't plant a seed here.")
			return
		}
		// In deadman mode, you can plant flowers in special places like banks etc.
		if (!it.player().world().realm().isDeadman && (it.player().world().floorAt(it.player().tile()) and 0x4) != 0) {
			it.message("You can't plant a seed here.")
			return
		}
		
		if (it.player().attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			it.message("You can't plant flowers in here.")
			return
		}
		
		var targTile = it.player().tile().transform(-1, 0, 0)
		var legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(-1, 0), 1)
		if (!legal) {
			targTile = it.player().tile().transform(1, 0, 0)
			legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(1, 0), 1)
			if (!legal) {
				return // No valid move to go!
			}
		}
		
		//Lock the player
		it.player().lockDamageOk()
		
		//Send the player a message
		it.message("You open the small mithril case.")
		it.message("You drop a seed by your feet.")
		
		//Remove a seed from the players inventory
		it.player().inventory().remove(Item(MITHRIL_SEEDS, 1), true)
		
		//Create the object
		val obj = spawn_flower(it, item, obj, targTile)
	}
	
	@Suspendable fun spawn_flower(it: Script, item: Int, obj: Int, walkTo: Tile) {
		val player = it.player()
		val flower: MapObj = MapObj(it.player().tile(), obj, 10, 0)
		val world = player.world()
		
		world.spawnObj(flower)
		
		//Move the player, create a delay, face the object and unlock the player.
		it.player().putattrib(AttributeKey.IGNORE_FREEZE_MOVE, true)
		it.player().walkTo(walkTo, PathQueue.StepType.FORCED_WALK)
		it.delay(1)
		it.player().clearattrib(AttributeKey.IGNORE_FREEZE_MOVE)
		it.player().faceObj(flower)
		it.player().unlock()
		
		// Despawn after 60 ticks
		it.runGlobal(player.world()) @Suspendable {
			it.delay(60)
			world.removeObj(flower)
		}
		
		//Prompt the player with some options.
		when (it.options("Pick the flowers.", "Leave the flowers.")) {
			1 -> {
				it.player().animate(827)
				world.removeObj(flower)
				it.player().inventory().add(Item(item, 1), false)
			}
		}
	}
	
}