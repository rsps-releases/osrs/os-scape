package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 22/09/2016.
 */
object ZulrahSerpHelm {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		/**
		 * CREATION & DISMANTLING
		 */
		
		//Creating our beautiful serpentine helms..
		r.onItemOnItem(ZulrahItems.SERPENTINE_VISAGE, ZulrahItems.CHISEL) @Suspendable {
			if (it.player().skills().level(Skills.CRAFTING) < 52) {
				it.player().message("You need at least level 52 Crafting to do this.")
			} else {
				if (it.player().inventory().has(ZulrahItems.SERPENTINE_VISAGE) && it.player().inventory().has(ZulrahItems.CHISEL)) {
					it.player().inventory() -= ZulrahItems.SERPENTINE_VISAGE
					it.player().inventory() += ZulrahItems.SERPENTINE_HELM_UNCHARGED
					it.animate(3015)
					it.addXp(Skills.CRAFTING, 120.0)
					it.itemBox("You adapt the visage to fit on a human head.", ZulrahItems.SERPENTINE_HELM_UNCHARGED)
				}
			}
		}
		
		// Dismantle serp helm
		r.onItemOption4(ZulrahItems.SERPENTINE_HELM_UNCHARGED) @Suspendable {
			it.doubleItemBox("Dismantling your serpentine helmet will destory it and give you 20,000 zulrah scales in return. Are you sure?", Item(ZulrahItems.SERPENTINE_HELM_UNCHARGED), Item(ZulrahItems.ZULRAHS_SCALES, 20000))
			if (it.optionsTitled("Dismantle the helm?", "Yes", "No") == 1) {
				if (it.player().inventory().remove(Item(ZulrahItems.SERPENTINE_HELM_UNCHARGED), false).success()) {
					it.player().inventory() += Item(ZulrahItems.ZULRAHS_SCALES, 20000)
				}
			}
		}
		
		/**
		 * CHARGING & UNCHARGING
		 */
		
		// Check charges
		for (helmId in intArrayOf(ZulrahItems.SERPENTINE_HELM, ZulrahItems.MAGMA_HELM, ZulrahItems.TANZANITE_HELM)) {
			
			// Check charges
			r.onItemOption3(helmId) {
				ZulrahStaffOfDead.check_scales_message(it, it.itemUsed(), 11000)
			}
			
			r.onEquipmentOption(1, helmId) {
				ZulrahStaffOfDead.check_scales_message(it, it.player().equipment().get(EquipSlot.HEAD), 11000)
			}
			
			// Scales on used (partly full)
			r.onItemOnItem(ZulrahItems.ZULRAHS_SCALES, helmId) {
				if (!ZulrahItems.CHARGING_ENABLED) {
					it.message("You don't need to charge this item for it to work.")
					return@onItemOnItem
				}
				val helmSlot = ItemOnItem.slotOf(it, helmId)
				val helm = it.player().inventory()[helmSlot]
				val scalesSlot = ItemOnItem.slotOf(it, ZulrahItems.ZULRAHS_SCALES) // opposite way around
				val scales = it.player().inventory()[scalesSlot]
				val amtToAdd = Math.min(11000 - helm.property(ItemAttrib.ZULRAH_SCALES), scales.amount())
				if (amtToAdd == 0 && helm.property(ItemAttrib.ZULRAH_SCALES) > 0) {
					it.message("Your serpentine helmet is already fully charged.")
					return@onItemOnItem
				}
				if (it.player().inventory().remove(Item(ZulrahItems.ZULRAHS_SCALES, amtToAdd), false).success()) {
					val chargedHelm = helm
					chargedHelm.property(ItemAttrib.ZULRAH_SCALES, helm.property(ItemAttrib.ZULRAH_SCALES) + amtToAdd)
					val newtotal = chargedHelm.property(ItemAttrib.ZULRAH_SCALES)
					it.message("You add $amtToAdd scales to your serpentine helmet. It now has $newtotal charges.")
				}
			}
			
			// 'Uncharge' - take the scales out.
			r.onItemOption5(helmId, @Suspendable {
				if (!it.itemUsed().hasProperties()) {
					val newId = when (helmId) {
						ZulrahItems.MAGMA_HELM -> ZulrahItems.MAGMA_HELM_UNCHARGED
						ZulrahItems.TANZANITE_HELM -> ZulrahItems.TANZANITE_HELM_UNCHARGED
						else -> ZulrahItems.SERPENTINE_HELM_UNCHARGED
					}
					it.player().inventory().set(it.itemUsedSlot(), Item(newId))
					it.player().message("You uncharge the helmet.")
					
				} else {
					val spacerequired = if (it.player().inventory().has(ZulrahItems.ZULRAHS_SCALES)) 0 else 1
					if (it.player().inventory().freeSlots() < spacerequired) {
						it.messagebox("You need $spacerequired free inventory slots to do this.")
					} else {
						val scalesToAdd = Math.min(Integer.MAX_VALUE - it.player().inventory().count(ZulrahItems.ZULRAHS_SCALES), it.itemUsed().property(ItemAttrib.ZULRAH_SCALES))
						if (scalesToAdd == 0) { // 2.1b scales held already.
							it.message("You don't have enough room in your inventory to do this.")
						} else {
							val charged = it.itemUsed()
							charged.property(ItemAttrib.ZULRAH_SCALES, charged.property(ItemAttrib.ZULRAH_SCALES) - scalesToAdd)
							
							if (charged.property(ItemAttrib.ZULRAH_SCALES) == 0) {
								
								val emptyHelmId = if (helmId == ZulrahItems.MAGMA_HELM) ZulrahItems.MAGMA_HELM_UNCHARGED else if (helmId == ZulrahItems.SERPENTINE_HELM)
									ZulrahItems.SERPENTINE_HELM_UNCHARGED else ZulrahItems.TANZANITE_HELM_UNCHARGED
								
								// Give an uncharge blowpipe to the player, as it has nothing in it now.
								it.player().inventory().remove(charged, false, it.itemUsedSlot())
								it.player().inventory().add(Item(emptyHelmId), false, it.itemUsedSlot())
							}
							it.player().inventory() += Item(ZulrahItems.ZULRAHS_SCALES, scalesToAdd)
							it.message("You remove the $scalesToAdd zulrah scales from the serpentine helmet.")
						}
					}
				}
			})
			
			// Revert charged mutagen helms to normal tradable helms, no mutagen. Keep charges if there are any.
			r.onItemOption4(helmId, s@ @Suspendable {
				val slot = it.itemUsedSlot()
				if (helmId != ZulrahItems.SERPENTINE_HELM) {
					it.doubleItemBox("This will convert the colored helmet into the normal serpentine helm and you will <col=ff0000>lose the mutagen applied.</col> Are you sure?",
							helmId, ZulrahItems.SERPENTINE_HELM)
					if (it.options("Yes, revert the helm and lose the mutagen.", "Never mind.") == 1) {
						if (it.player().inventory().get(slot)?.id() == helmId) {
							if (!it.player().inventory().get(slot).hasProperties()) {
								it.player().inventory().set(slot, Item(ZulrahItems.SERPENTINE_HELM_UNCHARGED))
							} else {
								val scales = it.itemUsed().property(ItemAttrib.ZULRAH_SCALES)
								it.player().inventory().set(slot, Item(ZulrahItems.SERPENTINE_HELM).property(ItemAttrib.ZULRAH_SCALES, scales))
							}
							// Do some nice graphics
							it.animate(713)
						}
					}
				}
			})
		}
		
		
		for (helmId in intArrayOf(ZulrahItems.SERPENTINE_HELM_UNCHARGED, ZulrahItems.MAGMA_HELM_UNCHARGED, ZulrahItems.TANZANITE_HELM_UNCHARGED)) {
			// Scales on empty
			r.onItemOnItem(ZulrahItems.ZULRAHS_SCALES, helmId) {
				
				val helmSlot = ItemOnItem.slotOf(it, helmId)
				val helm = it.player().inventory()[helmSlot]
				val scalesSlot = ItemOnItem.slotOf(it, ZulrahItems.ZULRAHS_SCALES) // opposite way around
				val scales = it.player().inventory()[scalesSlot]
				val chargedId = if (helm.id() == ZulrahItems.MAGMA_HELM_UNCHARGED) ZulrahItems.MAGMA_HELM else if (helm.id() == ZulrahItems.SERPENTINE_HELM_UNCHARGED)
					ZulrahItems.SERPENTINE_HELM else ZulrahItems.TANZANITE_HELM
				
				// If the system is disabled, replace empty with used.
				if (!ZulrahItems.CHARGING_ENABLED) {
					if (it.player().inventory().remove(helm, true).success()) {
						it.player().inventory().add(Item(chargedId), true, helmSlot)
						it.message("You don't need to charge this item for it to work.")
					}
					return@onItemOnItem
				}
				val amtToAdd = Math.min(11000 - helm.property(ItemAttrib.ZULRAH_SCALES), scales.amount())
				if (amtToAdd == 0) {
					it.message("Your serpentine helmet is already fully charged.")
					return@onItemOnItem
				}
				it.player().inventory() -= helm
				if (it.player().inventory().remove(Item(ZulrahItems.ZULRAHS_SCALES, amtToAdd), false).success()) {
					val chargedStaff = Item(chargedId)
					chargedStaff.property(ItemAttrib.ZULRAH_SCALES, helm.property(ItemAttrib.ZULRAH_SCALES) + amtToAdd)
					val newtotal = chargedStaff.property(ItemAttrib.ZULRAH_SCALES)
					it.player().inventory().add(chargedStaff, true, it.itemOnSlot())
					it.message("You add $amtToAdd scales to your serpentine helmet. It now has $newtotal charges.")
				}
			}
		}
		
		/**
		 * RESTORE & APPLY MUTAGENS
		 */
		
		// Restore magma helm
		r.onItemOption4(ZulrahItems.MAGMA_HELM_UNCHARGED, @Suspendable {
			val slot = it.itemUsedSlot()
			it.doubleItemBox("This will convert the magma helm into the normal serpentine helm helm and you will <col=ff0000>lose the mutagen applied.</col> Are you sure?",
					ZulrahItems.MAGMA_HELM_UNCHARGED, ZulrahItems.MAGMA_MUTAGEN)
			if (it.options("Yes, revert the helm and lose the mutagen.", "Never mind.") == 1) {
				if (it.player().inventory().get(slot)?.id() == ZulrahItems.MAGMA_HELM_UNCHARGED) {
					it.player().inventory().set(slot, Item(ZulrahItems.SERPENTINE_HELM_UNCHARGED))
					// Do some nice graphics
					it.animate(713)
				}
			}
		})
		
		// Restore tanz helm
		r.onItemOption4(ZulrahItems.TANZANITE_HELM_UNCHARGED, @Suspendable {
			val slot = it.itemUsedSlot()
			it.doubleItemBox("This will revert the tanzanite helm into the normal serpentine helm and you will <col=ff0000>lose the mutagen applied.</col> Are you sure?",
					ZulrahItems.TANZANITE_HELM_UNCHARGED, ZulrahItems.TANZANITE_MUTAGEN)
			if (it.options("Yes, revert the helm and lose the mutagen.", "Never mind.") == 1) {
				if (it.player().inventory().get(slot)?.id() == ZulrahItems.TANZANITE_HELM_UNCHARGED) {
					it.player().inventory().set(slot, Item(ZulrahItems.SERPENTINE_HELM_UNCHARGED))
					// Do some nice graphics
					it.animate(713)
				}
			}
		})
		
		//Using our X mutagen on our serpentine helmet.
		for (mutagenId in intArrayOf(ZulrahItems.MAGMA_MUTAGEN, ZulrahItems.TANZANITE_MUTAGEN)) {
			for (helmId in intArrayOf(ZulrahItems.SERPENTINE_HELM, ZulrahItems.SERPENTINE_HELM_UNCHARGED)) {
				
				r.onItemOnItem(helmId, mutagenId, s@ @Suspendable {
                    val player = it.player()

                    //Check charges
                    if (player.inventory()[it.itemOnSlot()].property(ItemAttrib.ZULRAH_SCALES) > 0 || player.inventory()[it.itemUsedSlot()].property(ItemAttrib.ZULRAH_SCALES) > 0) {
                        it.message("You must empty the helmet's charges before fusing.")
                        return@s
                    }
					
					// Give back the right helmet, charged or uncharged.
					val mutagenHelmId = if (helmId == ZulrahItems.SERPENTINE_HELM)
						if (mutagenId == ZulrahItems.MAGMA_MUTAGEN) ZulrahItems.MAGMA_HELM else ZulrahItems.TANZANITE_HELM
					else if (mutagenId == ZulrahItems.MAGMA_MUTAGEN) ZulrahItems.MAGMA_HELM_UNCHARGED else ZulrahItems.TANZANITE_HELM_UNCHARGED
					
					it.doubleItemBox("The mutagen will change the colour of your helm and<br> <col=ff0000>make it untradable</col>. You will be able to reverse the" +
							"<br>change, but will <col=ff0000>not</col> get the mutagen back.", helmId, mutagenId)
					if (it.optionsTitled("Do you wish to proceed?", "Yes", "No") == 1) {
						if (it.player().inventory().contains(helmId) && it.player().inventory().contains(mutagenId)) {
							it.player().inventory() -= helmId
							it.player().inventory() -= mutagenId
							it.player().inventory() += mutagenHelmId
							it.itemBox("The mutagen induces mutation in your helm.", mutagenHelmId)
						}
					}
				})
			}
		}
		
	}
}