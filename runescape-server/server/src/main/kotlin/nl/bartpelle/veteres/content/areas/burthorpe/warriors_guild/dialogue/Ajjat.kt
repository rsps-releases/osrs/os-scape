package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/17/2015.
 */

object Ajjat {
	
	val AJJAT = 2460
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(AJJAT) @Suspendable {
			it.chatNpc("Greetings, fellow warrior. I am Ajjat, former black<br>knight and now training officer here in the warrior<br>guild.", AJJAT, 590)
			when (it.options("Can you tell me about skillcapes, please?", "Black Knight? Why are you here?", "What's the Dummy Room all about?", "May I claim my tokens please?", "Bye!")) {
				1 -> {
					it.chatPlayer("Can you tell me about skillcapes, please?", 554)
					it.chatNpc("Skillcapes, also known as Capes of Accomplishment, are<br>reserved for the elite of the elite. Only a person who<br>has truly mastered a skill can buy one, and even then<br>a skillcape can only be bought from one who is", AJJAT, 591)
					it.chatNpc("recognised as the highest skilled in the land at any<br>particular skill. I have the privilege of being the person<br>that controls access to the Skillcape of Attack.", AJJAT, 591)
				}
				2 -> {
					it.chatPlayer("Black Knight? Why are you here?", 554)
					it.chatNpc("Indeed I was, however their... methods... did not match<br>with my ideals.. so I left. Harrallak, recognising my<br>talent as a warrior, took me in and offered me a job<br>here.", AJJAT, 591)
					it.chatPlayer("Hmm... well if Harrallak trusts you, I guess I can.", 588)
				}
				3 -> {
					it.chatPlayer("What's the Dummy Room all about?", 554)
					it.chatNpc("Ahh yes, the dummies. Another ingenious invention of<br>the noble Dwarf Gamfred. They're mechanical you see<br>and pop up out of the floor. You have to hit them with<br>the correct attack mode before they disappear again.", AJJAT, 570)
					it.chatPlayer("So how do I tell which one is which?", 554)
					it.chatNpc("There are two different ways. One indication is their<br>colour, the other is the pose and weapons they are<br>holding, for instance, the one holding daggers you will<br>need to hit with a piercing attack.", AJJAT, 570)
					it.chatNpc("In the room you will find a poster on the wall which<br>can help you recognise each different dummy.", AJJAT, 568)
					it.chatPlayer("That sounds ingenious!")
					it.chatNpc("Indeed, you may find that you need several weapons to<br>be successful 100% of the time, but keep trying. The<br>weapons shop upstairs may help you there.", AJJAT, 569)
				}
				4 -> {
					it.chatPlayer("May I claim my tokens please?", 554)
					//it.chatNpc("Of course! Here you go, you've earned 30 tokens!", Ajjat, 569)
					//it.chatPlayer("Thanks!", 567)
					it.chatNpc("I'm afraid you have not earned any tokens yet. Try<br>some of the activities around the guild to earn some.", AJJAT, 611)
					it.chatPlayer("Ok, I'll go see what I can find.", 588)
				}
				5 -> {
					it.chatPlayer("Bye!", 567)
					it.chatNpc("Farewell warrior. Stay away from the dark side.", AJJAT, 588)
				}
			}
		}
	}
	
}
