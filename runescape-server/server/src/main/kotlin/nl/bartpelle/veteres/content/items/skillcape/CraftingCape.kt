package nl.bartpelle.veteres.content.items.skillcape

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jason MacKeigan on 2016-07-04.
 */
object CraftingCape : CapeOfCompletionPerk(intArrayOf(1, 2)) {
	val TARGET_TILE = Tile(2931, 3285)
	
	override fun option(option: Int): Function1<Script, Unit> = @Suspendable {
		val player = it.player()
		
		when (option) {
			1 -> {
				CapeOfCompletion.boost(Skills.CRAFTING, player)
			}
			2 -> {
				if (Teleports.canTeleport(player, true, TeleportType.GENERIC)) {
					Teleports.basicTeleport(it, TARGET_TILE)
				}
			}
		}
	}
	
	@JvmStatic fun register(repo: ScriptRepository) {
		for (i in intArrayOf(9780, 9781)) {
			repo.onItemOption3(i) @Suspendable {
				if (Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
					Teleports.basicTeleport(it, TARGET_TILE)
				}
			}
		}
	}
}