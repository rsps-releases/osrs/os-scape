package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.Mac
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.items.teleport.ArdyCape
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 11/05/2016.
 */
object MaxCape { // Max cape

	const val INFERNAL_MAX_CAPE = 21285
	const val INFERNAL_MAX_HOOD = 21282
	const val IMBUED_SARADOMIN_MAX_CAPE = 21776
	const val IMBUED_SARADOMIN_MAX_HOOD = 21778
	const val IMBUED_ZAMORAK_MAX_CAPE = 21780
	const val IMBUED_ZAMORAK_MAX_HOOD = 21782
	const val IMBUED_GUTHIX_MAX_CAPE = 21784
	const val IMBUED_GUTHIX_MAX_HOOD = 21786
	const val ASSEMBLER_MAX_CAPE = 21898
	const val ASSEMBLER_MAX_CAPE_HOOD = 21900
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (i in Equipment.MAX_CAPES) {
			repo.onItemOption3(i, @Suspendable {
				// "Teleports"
				val player = it.player()
				if (!MaxCape.hasTotalLevel(player)) {
					player.message("You don't meet the requirements to wear this cape.")
				} else {
					if (i == ArdyCape.ARDY_MAXCAPE)
						ArdyCape.ardyCapeTeleport(it)
					else
						options1(it)
				}
			})
			
			repo.onItemOption4(i, s@ @Suspendable {
				// "Features"
				val player = it.player()
				if (!MaxCape.hasTotalLevel(player)) {
					player.message("You don't meet the requirements to wear this cape.")
					return@s
				}
				features(it)
			})
		}
		
		repo.onEquipmentOption(1, 13342) @Suspendable { maxCapeTeleport(it, 2879, 3546, 0) } // Warr
		repo.onEquipmentOption(2, 13342) @Suspendable { maxCapeTeleport(it, 2597, 3410, 0) } // Fishing
		repo.onEquipmentOption(3, 13342) @Suspendable { maxCapeTeleport(it, 2933, 3287, 0) } // Craft
		repo.onEquipmentOption(4, 13342) @Suspendable { chinopts(it) } // Chins
		repo.onEquipmentOption(5, 13342) @Suspendable { it.player().message("This feature is coming soon.") } // POH
		repo.onEquipmentOption(6, 13342) @Suspendable { it.player().message("This feature is coming soon.") } // Spellbook
		repo.onEquipmentOption(7, 13342) @Suspendable { features(it) } // Features
	}
	
	@Suspendable private fun features(it: Script) {
		val player = it.player()
		when (it.options("Pestle and Mortar", "Spellbook Swap", "Grapple", "Toggle Ring of Life effect")) {
			1 -> {
				if (player.inventory().freeSlots() > 0) {
					player.inventory().add(Item(233, 1), false)
					it.itemBox("You find a Pestle and Mortar.", 233)
				} else {
					player.message("Your inventory is full.")
				}
			}
			2, 3 -> {
				player.message("This feature is coming soon.")
			}
			4 -> {
				VarbitAttributes.toggle(player, VarbitAttributes.VarbitInfo.MAXCAPE_ROL_ON.varbitid)
				it.messagebox("Your max cape's Ring of Life effect is now " + (
						if (VarbitAttributes.varbiton(player, Varbit.MAXCAPE_ROL_ON)) "on." else "off."))
			}
		}
	}
	
	@JvmStatic @Suspendable fun options1(it: Script) { // Embedded function pogchamp
		when (it.options("Warriors' Guild", "Crafting Guild", "POH", "Fishing Guild", "Chinchompa spots")) {
			1 -> maxCapeTeleport(it, 2879, 3546, 0)
			2 -> maxCapeTeleport(it, 2933, 3287, 0)
			3 -> it.player().message("This feature is coming soon.")
			4 -> maxCapeTeleport(it, 2597, 3410, 0)
			5 -> chinopts(it)
		}
	}
	
	@JvmStatic @Suspendable fun chinopts(it: Script) {
		when (it.options("Grey chins.", "Red chins.", "Black chins.", "Never mind")) {
			1 -> maxCapeTeleport(it, 2342, 3595, 0)
			2 -> maxCapeTeleport(it, 2556, 2933, 0)
			3 -> maxCapeTeleport(it, 3146, 3772, 0)
			4 -> options1(it)
		}
	}
	
	@Suspendable @JvmStatic fun maxCapeTeleport(it: Script, x: Int, y: Int, z: Int) {
		if (it.player().locked()) return
		
		it.player().stopActions(true)
		
		if (!Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
			return
		}
		
		val player: Player = it.player()
		val tile: Tile = Tile(x, y, z)
		val unsafe: Boolean = WildernessLevelIndicator.inWilderness(tile)
		
		if (player.privilege() != Privilege.ADMIN) {
			if (unsafe && player.inventory().count(6685) > GameCommands.brewCap) {
				player.message("" + player.inventory().count(6685) + " brews is a little excessive don't you think? The wilderness limit is ${GameCommands.brewCap}.")
				return
			}
		} else {
			player.message("As an admin you bypass pk-tele restrictions.")
		}
		
		
		if (unsafe) {
			if (it.optionsTitled("Teleport into the wilderness?", "Yes", "No") != 1) {
				return
			}
		}
		Teleports.basicTeleport(it, it.player().world().randomTileAround(tile, 1))
	}
	
	fun hasTotalLevel(player: Player): Boolean {
		return Mac.totallevel(player) >= Mac.TOTAL_LEVEL_FOR_MAXED
	}
}