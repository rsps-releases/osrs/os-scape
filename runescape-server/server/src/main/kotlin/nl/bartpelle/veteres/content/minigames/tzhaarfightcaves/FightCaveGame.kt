package nl.bartpelle.veteres.content.minigames.tzhaarfightcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.achievements.pvp.PVPDiaryHandler
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.instance.InstancedMap
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Situations on 9/17/2016.
 */

object FightCaveGame {
	
	@Suspendable @JvmStatic fun initiateFightCaves(it: Script, loggingIn: Boolean = false) {
		val player = it.player()
		
		//Just in case! : )
		player.lock()
		player.clearDamagers() // Fixes #841
		
		//Create the unique instanced area
		val fightCave = player.world().allocator().allocate(64, 64, Tile(2368, 5056)).get()
		fightCave.setCreatedFor(player)
		fightCave.set(0, 0, Tile(2368, 5056), Tile(2368 + 64, 5056 + 64), 0, true)
		fightCave.setMulti(true)
		fightCave.setIdentifier(InstancedMapIdentifier.FIGHT_CAVE)
		
		//Teleport the player inside the instanced area
		if (!loggingIn) {
			player.teleport(fightCave.center().transform(+11, +26, 0))
			//player.write(DisplayInstancedMap(fightCave, player))
			player.pathQueue().clear()
			player.walkTo(fightCave.center(), PathQueue.StepType.REGULAR)
		} else {
			player.teleport(fightCave.bottomLeft().transform(player.tile().x - 2368, player.tile().z - 5056)) // The corner of the real place, minus where we're at inside it.
			//player.write(DisplayInstancedMap(fightCave, player))
		}
		// Set as true so you can't teleport out etc before the waves start.
		player.putattrib(AttributeKey.TZHAAR_MINIGAME, true)
		
		//Send our pretty dialogue
		player.executeScript @Suspendable {
			it.chatNpc("You're on your own now JalYt, prepare to fight for<br>your life!", 2180, 615)
		}
		
		///Start sending the waves!
		player.world().server().scriptExecutor().executeScript(player.world(), @Suspendable { s ->
			if (loggingIn) {
				s.delay(4)
				if (fightCave.contains(player)) { // 4 ticks later, did we leave? stop null pointers.
					if (getCurrentWave(player) == -1) // Only situtation where the wave needs checking
						player.putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, 1)
					FightCaveMonsters.spawnWaveMonsters(player)
					sendWaveMessage(player)
					startSendingWaves(player)
				}
			} else {
				s.delay(3)
				if (fightCave.contains(player)) { // 3 ticks later, did we leave? stop null pointers.
					startSendingWaves(player)
				}
			}
		})
		
		player.unlock()
	}
	
	fun spawnTiles(player: Player): ArrayList<Tile> {
		val fightCave = activeArea(player) ?: return ArrayList()
		val monsterSpawn = ArrayList<Tile>()
		
		monsterSpawn.add(fightCave.center()) //Center
		monsterSpawn.add(fightCave.center().transform(+6, -17)) //South
		monsterSpawn.add(fightCave.center().transform(+20, -7)) //South East
		monsterSpawn.add(fightCave.center().transform(-21, -17)) //South West
		monsterSpawn.add(fightCave.center().transform(-19, +19)) //North West
		
		return monsterSpawn
	}
	
	@Suspendable private fun startSendingWaves(player: Player) {
		player.world().server().scriptExecutor().executeScript(player.world(), @Suspendable { s ->
			var active = player.attribOr<Boolean>(AttributeKey.TZHAAR_MINIGAME, false)
			
			while (active) {
				//Is the player still in the instance?
				if (activeArea(player) != null && !player.tile().inArea(activeArea(player))) {
					leaveFightCave(player)
				} else if (areaEmpty(player)) { //If there are no monsters to kill, send the next wave!
					s.delay(3)
					if (player.attribOr<Boolean>(AttributeKey.TZHAAR_MINIGAME, false)) // check again after the delay
						advanceToNextWave(player)
				}
				
				//Delay and check to see if the player is still playing the minigame
				s.delay(3)
				active = player.attribOr<Boolean>(AttributeKey.TZHAAR_MINIGAME, false)
			}
		})
	}
	
	fun activeArea(player: Player): InstancedMap? {
		return player.world().allocator().active(player.tile()).orElse(null)
	}
	
	fun areaEmpty(player: Player): Boolean {
		val active = player.world().allocator().active(player.tile())
		if (active.isPresent) {
			val map = active.get()
			var num = 0
			
			player.world().npcs().forEachInAreaKt(map, { num++ })
			
			return num == 0
		}
		
		return true
	}
	
	@Suspendable fun advanceToNextWave(player: Player) {
		if (player.world().realm().isPVP) {
			//If on the PVP world, let the players fight Tztok-Jad
			player.putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, 63)
		} else {
			//Increase the players wave by 1! -1 means pre-wave-1 when we've just entered the caves.
			player.putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, if (getCurrentWave(player) == -1) 1 else getCurrentWave(player) + 1)
		}
		
		//If the logout timer isn't activated, send the next wave!
		val logoutWarningActivated = player.attribOr<Boolean>(AttributeKey.TZHAAR_LOGOUT_WARNING, false)
		if (!logoutWarningActivated) {
			sendWaveMessage(player)
			FightCaveMonsters.spawnWaveMonsters(player)
		} else {
			player.message("<col=FF0000>The Fight Cave has been paused. You may now log out.")
			player.putattrib(AttributeKey.TZHAAR_MINIGAME, false)
		}
	}
	
	@Suspendable fun leaveFightCave(player: Player, completedFightCaves: Boolean = false) {
		//Teleport the player outside the Fight Cave and apply the cool down timer
		player.timers().addOrSet(TimerKey.TZHAAR_FIGHT_CAVE, 500)
		
		//If the player successfully completed all the waves we..
		if (completedFightCaves) {
			
			//Add the tokkul to the players inventory or drop it on the ground..
			val tokkulReward = calculateTokkulReward(getCurrentWave(player))
			if (player.inventory().add(Item(6529, tokkulReward), false).failed()) {
				val groundItem = GroundItem(player.world(), Item(6529, tokkulReward), player.tile(), player.id())
				player.world().spawnGroundItem(groundItem)
			}
			
			if (AchievementDiary.diaryHandler(player) is PVPDiaryHandler) {
				AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(player, 33))
			}
			
			//Send our pretty dialogue
			player.executeScript {
				it.chatNpc("You have defeated TzTok-Jad, I am most impressed!<br> Please accept this gift.<br>Give cape back to me if you not want it.", 2180, 615)
			}
		} else {
			if (player.world().realm().isPVP || getCurrentWave(player) < 2) {
				player.executeScript {
					it.chatNpc("Well I suppose you tried... better luck next time.", 2180, 610)
				}
			} else {
				player.inventory().add(Item(6529, calculateTokkulReward(getCurrentWave(player))), true)
				player.executeScript {
					it.chatNpc("Well done in the cave, here take TokKul as reward.", 2180, 588)
				}
			}
		}
		
		//Clear the monster wave array
		player.world().unregisterAll(activeArea(player))
		player.teleport(player.world().randomTileAround(Tile(2438, 5170), 1))
		
		if (completedFightCaves) {
			
			//Add the fire cape into the players inventory or drop it on the ground..
			player.inventory().addOrDrop(Item(6570, 1), player)
		}
		
		//Set the player attribute keys back to normal
		player.clearattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE) // Make sure you clear this AFTER rewarding the player haha.
		player.clearattrib(AttributeKey.TZHAAR_LOGOUT_WARNING)
		player.clearattrib(AttributeKey.TZHAAR_MINIGAME)
	}
	
	@JvmStatic @Suspendable fun attemptLogoutFightCaves(player: Player) {
		player.message("<col=FF0000>Your logout request has been received. The minigame will be paused at" +
				" the end of this wave.")
		player.message("<col=FF0000>If you try to log out before that, you will have to repeat this wave.")
		player.putattrib(AttributeKey.LOGOUT, false)
		player.putattrib(AttributeKey.TZHAAR_LOGOUT_WARNING, true)
	}
	
	@Suspendable fun calculateTokkulReward(lastCompletedWave: Int): Int {
		if (lastCompletedWave == 63)
			return 8032
		
		val x = if (lastCompletedWave < 1) 1 else lastCompletedWave
		return 2 * (x * x) + 6 * x + 4
	}
	
	@JvmStatic @Suspendable fun onLogout(player: Player) {
		//Set player attribute keys to their appropriate settings
		player.putattrib(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, Math.min(63, Math.max(-1, getCurrentWave(player))))
		player.clearattrib(AttributeKey.TZHAAR_LOGOUT_WARNING)
		player.clearattrib(AttributeKey.TZHAAR_MINIGAME)
		
		//Clear the monster wave array
		val area = activeArea(player)
		if (area != null) {
			area.identifier.ifPresent { key ->
				if (key == InstancedMapIdentifier.FIGHT_CAVE) {
					player.world().unregisterAll(activeArea(player))
				}
			}
		}
	}
	
	@Suspendable fun getCurrentWave(player: Player): Int {
		val currentWave = player.attribOr<Int>(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, 0)
		return currentWave
	}
	
	@Suspendable fun sendWaveMessage(player: Player) {
		player.message("<col=FF0000>Wave: ${getCurrentWave(player)}")
	}
	
}

