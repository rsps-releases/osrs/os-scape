package nl.bartpelle.veteres.content.areas.clanwars

import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.IGNORE_5
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_100
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_1000
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_10000
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_200
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_25
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_50
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_500
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KILL_5000
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_1500
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_250
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_2500
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_4000
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_6000
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_750
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.KING_80
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.MOST_KILLS_10
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.MOST_KILLS_120
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.MOST_KILLS_20
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.MOST_KILLS_5
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.MOST_KILLS_60
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.ODDSKULL_100
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.ODDSKULL_300
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.ODDSKULL_500
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings.STRAGGLERS
import java.util.concurrent.TimeUnit

/**
 * Created by Bart on 10/16/2016.
 */
data class ClanWarCriteria(val check: (ClanWar) -> Int?) {
	
	companion object {
		val LAST_TEAM_STANDING = ClanWarCriteria { war: ClanWar ->
			when {
				war.team1.isEmpty() -> war.opponent.ownerId()
				war.team2.isEmpty() -> war.attacker.ownerId()
				else -> CHECK_EXTRA.determineWinner(war)
			}
		}
		
		val KILL_COUNT = ClanWarCriteria { war: ClanWar ->
			val endKills = when (war.setting(CWSettings.GAME_END)) {
				KILL_25 -> 25
				KILL_50 -> 50
				KILL_100 -> 100
				KILL_200 -> 200
				KILL_500 -> 500
				KILL_1000 -> 1000
				KILL_5000 -> 5000
				KILL_10000 -> 10000
				else -> 0
			}
			
			when {
				war.team1.kills >= endKills -> war.attacker.ownerId()
				war.team2.kills >= endKills -> war.opponent.ownerId()
				else -> CHECK_EXTRA.determineWinner(war)
			}
		}
		
		val KING = ClanWarCriteria { war: ClanWar ->
			val endPoints = when (war.setting(CWSettings.GAME_END)) {
				KING_80 -> 80
				KING_250 -> 250
				KING_750 -> 750
				KING_1500 -> 1500
				KING_2500 -> 2500
				KING_4000 -> 4000
				KING_6000 -> 6000
				else -> 0
			}
			
			when {
				war.team1.kingPoints >= endPoints -> war.attacker.ownerId()
				war.team2.kingPoints >= endPoints -> war.opponent.ownerId()
				else -> CHECK_EXTRA.determineWinner(war)
			}
		}
		
		val MOST_KILLS = ClanWarCriteria { war: ClanWar ->
			val endTime = when (war.setting(CWSettings.GAME_END)) {
				MOST_KILLS_5 -> 5
				MOST_KILLS_10 -> 10
				MOST_KILLS_20 -> 20
				MOST_KILLS_60 -> 60
				MOST_KILLS_120 -> 120
				else -> 0
			}
			
			val currentMillis = war.totalTicks * 600
			
			if (currentMillis >= TimeUnit.MINUTES.toMillis(endTime.toLong())) {
				when {
					war.team1.kills > war.team2.kills -> war.attacker.ownerId()
					war.team1.kills < war.team2.kills -> war.opponent.ownerId()
					else -> CHECK_EXTRA.determineWinner(war)
				}
			} else {
				CHECK_EXTRA.determineWinner(war)
			}
		}
		
		val ODD_SKULL = ClanWarCriteria { war: ClanWar ->
			val endPoints = when (war.setting(CWSettings.GAME_END)) {
				ODDSKULL_100 -> 100
				ODDSKULL_300 -> 300
				ODDSKULL_500 -> 500
				else -> 0
			}
			
			when {
				war.team1.oddskullPoints >= endPoints -> war.attacker.ownerId()
				war.team2.oddskullPoints >= endPoints -> war.opponent.ownerId()
				else -> CHECK_EXTRA.determineWinner(war)
			}
		}
		
		val CHECK_EXTRA = ClanWarCriteria { war: ClanWar ->
			val min = if (war.setting(STRAGGLERS, IGNORE_5)) 5 else 0
			when {
				war.team1.size() <= min && war.team2.size() <= min -> -1 //TIE
				war.team1.size() > min && war.team2.size() <= min -> war.attacker.ownerId()
				war.team2.size() > min && war.team1.size() <= min -> war.opponent.ownerId()
				else -> null
			}
		}
		
	}
	
	fun determineWinner(war: ClanWar) = check(war)
	
}