package nl.bartpelle.veteres.content.tournament

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.npcs.slayer.CallFollower
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SendTournamentFighters
import nl.bartpelle.veteres.script.ScriptRepository
import java.text.NumberFormat

object TournamentObjects {

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onObject(TournamentConfig.CRUSHED_SPEARS, s@ @Suspendable {
            val player = it.player()
            if (TournamentConfig.tournamentDisabled) {
                player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently in development. Please try again later."))
                return@s
            }
            if (player.tile().inArea(TournamentConfig.WAITING_AREA)) {
                if (it.optionsTitled("Are you sure you want to leave the tournament?", "Yes, leave the tournament.", "No, keep me in the tournament!") == 1) {
                    climbBrokenSpears(it, true)
                }
            } else {
               // if(Tournament.joinRequirementCheck(player))
                    climbBrokenSpears(it, false)
            }
        })

        r.onObject(TournamentConfig.TOURNAMENT_REWARDS, s@ @Suspendable {
            val player = it.player()
            val unclaimedBM = player.attribOr<Int>(AttributeKey.UNCLAIMED_TOURNAMENT_BM, 0)

            if (TournamentConfig.tournamentDisabled) {
                player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently in development. Please try again later."))
                return@s
            }

            if(unclaimedBM <= 0) {
                it.itemBox("You don't have any tournament rewards to claim. Please compete in an upcoming tournament to earn rewards.", 13307, 10_000)
                return@s
            }

            if(player.inventory().full() && !player.inventory().has(13307)) {
                it.message(Color.DARK_RED.wrap("<img=79> You need at least 1 inventory slot in order to claim your tournament reward."))
                return@s
            }

            if (it.optionsTitled("Would you like to claim your tournament rewards?", "Yes, collect ${NumberFormat.getInstance().format(unclaimedBM)} blood money.", "No, leave it.") == 1) {
                player.lock()
                player.inventory().add(Item(13307, unclaimedBM), true)
                player.putattrib(AttributeKey.UNCLAIMED_TOURNAMENT_BM, 0)
                player.unlock()
                it.message(Color.DARK_RED.wrap("<img=79> You claim ${NumberFormat.getInstance().format(unclaimedBM)} blood money."))
                it.itemBox("You claim ${NumberFormat.getInstance().format(unclaimedBM)} blood money. Gain more rewards by competing in additional tournaments.", 13307, unclaimedBM)
            }
        })
        r.onObject(TournamentConfig.TOURNAMENT_ORB, s@ @Suspendable {
            val player = it.player()
            if (TournamentConfig.tournamentDisabled) {
                player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently in development. Please try again later."))
                return@s
            }
            player.interfaces().sendMain(383)
            player.write(SendTournamentFighters("Situations", "Bart"))
        })
        r.onObject(TournamentConfig.TOURNAMENT_BOARD, s@ @Suspendable {
            val player = it.player()
            if (TournamentConfig.tournamentDisabled) {
                player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently in development. Please try again later."))
                return@s
            }
        })
    }

    @Suspendable private fun climbBrokenSpears(it: Script, leaving: Boolean) {
        val player = it.player()
        if (TournamentConfig.tournamentDisabled) {
            player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently in development. Please try again later."))
            return
        }
        player.lockDelayDamage()
        player.animate(839)
        if(leaving) {
            player.forceMove(ForceMovement(2, 0, 0, 0, 60, 0, FaceDirection.EAST))
            it.delay(2)
            player.teleport(player.tile().transform(2, 0))
            Tournament.leave(player)
        } else {
            player.forceMove(ForceMovement(0, 0, -2, 0, 0, 60, FaceDirection.WEST))
            it.delay(2)
            player.teleport(player.tile().transform(-2, 0))
            Tournament.join(player, false)
        }
        CallFollower.callPet(it)
        player.unlock()
    }
}