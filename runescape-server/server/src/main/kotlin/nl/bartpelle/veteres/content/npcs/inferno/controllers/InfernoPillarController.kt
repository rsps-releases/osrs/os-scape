package nl.bartpelle.veteres.content.npcs.inferno.controllers

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoPillars
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.map.MapObj

/**
 * Created by Mack on 8/6/2017.
 */
class InfernoPillarController(pillar: Npc, session: InfernoSession): InfernoNpcController {
	
	/**
	 * The session the pillars are involved in.
	 */
	val session = session
	
	/**
	 * The pillar being interacted with.
	 */
	val pillar = pillar
	
	override fun tick(it: Script) {
//		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}
	
	override fun onSpawn() {
//		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}
	
	override fun triggerNpcKilledDeath() {
		
		session.clearPillar(pillar)
		
		if (!session.activeNpcs.isEmpty()) {
			session.activeNpcs.filter { npc -> npc.id() == InfernoContext.NIBBLER }.forEach{session.handleNpcDeath(it)}
		}
		
		//Will inflict 50 damage if player is within distance of the tile.
		if (session.player().tile().inArea(Area(pillar.tile(), 1))) {
			session.player().hit(pillar, 50, 1)
		}
	}
	
	override fun onDamage() {
		val toUpdate: MapObj? = session.activePillars[pillar]
		val player = session.player()
		var varbit = 0
		
		if (toUpdate!!.id() == InfernoPillars.FIRST.objectId()) {
			varbit = InfernoPillars.FIRST.varbit()
		} else if (toUpdate.id() == InfernoPillars.SECOND.objectId()) {
			varbit = InfernoPillars.SECOND.varbit()
		} else if (toUpdate.id() == InfernoPillars.THIRD.objectId()) {
			varbit = InfernoPillars.THIRD.varbit()
		}
		
		//We send to varbit to update the object appearance. The value
		//has to be as if we're sending the pillars max hp. Basically, the higher the varbit value the more decayed the appearance.
		if (varbit > 0) {
			player.varps().varbit(varbit, pillar.maxHp() - pillar.hp())
		}
	}
}