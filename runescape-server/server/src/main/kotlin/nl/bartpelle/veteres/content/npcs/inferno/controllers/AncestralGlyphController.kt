package nl.bartpelle.veteres.content.npcs.inferno.controllers

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Mack on 8/7/2017.
 */
class AncestralGlyphController(npc: Npc, session: InfernoSession): InfernoNpcController {
	
	val session = session
	val wall = npc
	var warnings = BooleanArray(3)
	
	override fun tick(it: Script) {
	
	}
	
	override fun onSpawn() {
		
	}
	
	override fun triggerNpcKilledDeath() {
		for (current in session.activeNpcs) {
			
			if (current.id() == InfernoContext.ANCESTRAL_GLYPH || current.id() == 7709 || current.id() == 7710 || current.id() == InfernoContext.JAL_MEJJAK) {
				continue
			}
			
			current.attack(session.player())
		}
		
		session.player().message("The ancestral glyph has fallen!")
	}
	
	override fun onDamage() {
		if (!warnings[0] && wall.hp() <= 450) {
			session.player().message("The ancestral glyph has 75% of its life remaining!")
			warnings[0] = true
		}
		if (!warnings[1] && wall.hp() <= 300) {
			session.player().message("The ancestral glyph has 50% of its life remaining!")
			warnings[1] = true
		}
		if (!warnings[2] && wall.hp() <= 150) {
			session.player().message("The ancestral glyph has 25% of its life remaining!")
			warnings[2] = true
		}
	}
}