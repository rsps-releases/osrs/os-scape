package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 7/30/2017.
 */
object JalXil {
	
	//melee 7604
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		val world = npc.world()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while(PlayerCombat.canAttack(npc, target)) {
			
			if (EntityCombat.canAttackDistant(npc, target, true, 8) && EntityCombat.attackTimerReady(npc)) {
				if (EntityCombat.canAttackMelee(npc, target) && npc.world().random(2) == 1) {
					melee(npc, target, world)
				} else {
					range(npc, target, world)
				}
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun melee(npc: Npc, target: Entity, world: World) {
		
		npc.animate(7604)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, world.random(npc.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(npc, 0, 1)
		}
	}
	
	fun range(npc: Npc, target: Entity, world: World) {
	
		npc.animate(npc.attackAnimation())
		
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc.tile() + Tile(1, 1), target, 1376, 40, 34, 45, 12 * tileDist, 0, 1)
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
			target.hit(npc, world.random(npc.combatInfo().maxhit), delay).combatStyle(CombatStyle.RANGE)
		} else {
			target.hit(npc, 0, delay)
		}
	}

}
