package nl.bartpelle.veteres.content.minigames.raids

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */

@Suspendable fun RaidParty.addMember(toAdd: Player) {
    toAdd.putattrib(AttributeKey.RAID_PARTY, id())
    members().add(toAdd)
}

fun RaidParty.removeMember(toRemove: Player) {
    if (toRemove.interfaces().visible(RaidInterfaces.RAID_PARTY_SETUP_LAYER) || toRemove.interfaces().visible(RaidInterfaces.RECRUITING_BOARD_LAYER)) {
        RaidInterfaces.reloadRecruitingBoard(toRemove)
    }
    toRemove.clearattrib(AttributeKey.RAID_PARTY)
    members().remove(toRemove)
}

fun RaidParty.disband() {
    members().stream().forEach({member ->
        if (member.interfaces().visible(RaidInterfaces.RAID_PARTY_SETUP_LAYER) || member.interfaces().visible(RaidInterfaces.RECRUITING_BOARD_LAYER)) {
            RaidInterfaces.reloadRecruitingBoard(member)
        }

        clearPreferencesFor(member)
        member.message("<col=FF0000>Your raid party has been disbanded.</col>")
    })
    delete()
}

fun RaidParty.delete() {
    members().forEach({p->
        p.clearattrib(AttributeKey.RAID_PARTY)
    })

    chatChannel().raidParties().remove(RaidsResources.partyById(id()))
    members().clear()
    RaidsResources.raidParties().remove(id())
}

fun RaidParty.create(id: Int, clanChannel: ClanChat) {
    id(id)
    status(RaidsResources.RaidPartyStatus.LOBBY)

    for(plr in clanChannel.toPlayerTypedArray()) {
        if (plr.attribOr<Int>(AttributeKey.RAID_PARTY, -1) > -1) {
            continue
        }

        clearPreferencesFor(plr)
        plr.putattrib(AttributeKey.RAID_PARTY, id())
        members().add(plr)
        plr.message("<col=804080>A raid party has been created by ${ownerName()}. Head over to the lobby if you wish to participate!</col>")
    }

    clanChannel.raidParties().add(this)
    RaidsResources.raidParties().put(id, this)
}

fun RaidParty.addListing() {
    RaidsResources.raidPartyAdvertisedListings().add(this)
    syncAdvertise(true, System.currentTimeMillis())
}

fun RaidParty.removeListing() {
    if (!RaidsResources.raidPartyAdvertisedListings().contains(this)) {
        return
    }

    RaidsResources.raidPartyAdvertisedListings().remove(this)
    syncAdvertise(false, 0L)
}