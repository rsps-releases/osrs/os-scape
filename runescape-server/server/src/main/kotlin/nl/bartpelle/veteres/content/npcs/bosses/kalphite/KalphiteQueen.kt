package nl.bartpelle.veteres.content.npcs.bosses.kalphite

import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Jason MacKeigan (http://www.github.com/JasonMacKeigan)
 */
object KalphiteQueen {
	
	/**
	 * The area that is the kalphite lair
	 */
	val area: Area = Area(3460, 9475, 3520, 9520)
	
	/**
	 * A mapping of all kalphite queen forms with their respective animations
	 */
	val animations: Map<Int, Map<CombatStyle, Int>> = mapOf(
			6500 to mapOf(CombatStyle.MAGIC to 6240, CombatStyle.RANGE to 6240, CombatStyle.MELEE to 6241),
			6501 to mapOf(CombatStyle.MAGIC to 6234, CombatStyle.RANGE to 6234, CombatStyle.MELEE to 6235))
	
	/**
	 * Returns a random combat style where it is more likely to obtain
	 * a style of Magic or Range, appose to Melee.
	 */
	fun randomizeStyle(): CombatStyle {
		val random = Random().nextInt(100)
		
		if (random < 50) {
			return CombatStyle.MAGIC
		} else if (random < 80) {
			return CombatStyle.RANGE
		}
		
		return CombatStyle.MELEE
	}
	
}
