package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 7/27/2017.
 */
object JalAkRekKet {
	
	@JvmField val script: Function1<Script, Unit> =  s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 1) && EntityCombat.attackTimerReady(npc)) {
				
				attack(npc, target)
				
				//Send to cooldown.
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun attack(npc: Npc, target: Entity) {
		npc.animate(npc.attackAnimation())
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, npc.world().random(18), 1).delay(npc.combatInfo().attackspeed).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(npc, 0, 1)
		}
	}
}