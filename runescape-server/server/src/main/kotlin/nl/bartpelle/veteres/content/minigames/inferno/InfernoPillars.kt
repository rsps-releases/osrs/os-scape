package nl.bartpelle.veteres.content.minigames.inferno

import java.util.*

/**
 * Created by Mack on 8/6/2017.
 */
enum class InfernoPillars(objectId: Int, varbit: Int) {
	
	FIRST(30353, 5655),
	SECOND(30354, 5656),
	THIRD(30355, 5657),
	
	;//end of enum
	
	/**
	 * The object identifier of the pillar.
	 */
	var objectId = objectId
	
	/**
	 * The varbit for the pillar definition.
	 */
	var varbit = varbit
	
	/**
	 * The list of definitions.
	 */
	val definitions: ArrayList<InfernoPillars> = ArrayList()
	
	/**
	 * The supplier for the object Id
	 */
	fun objectId(): Int {
		return objectId
	}
	
	/**
	 * The supplier for the varbit used to update the object appearance.
	 */
	fun varbit(): Int {
		return varbit
	}
}