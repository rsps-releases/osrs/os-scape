package nl.bartpelle.veteres.content.npcs.bosses.kalphite

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle
import java.lang.ref.WeakReference

/**
 * Created by Jason MacKeigan on 2016-06-29.
 */
object KalphiteQueenFirstForm {
	
	@JvmField val script: Function1<Script, Unit> = suspend@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@suspend
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 8) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.canAttackMelee(npc, target) && npc.world().rollDie(4, 1)) {
					attack(npc, target, CombatStyle.MELEE, it)
				} else {
					attack(npc, target, KalphiteQueen.randomizeStyle(), it)
				}
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@suspend
		}
		
	}
	
	@JvmField val death: Function1<Script, Unit> = suspend@ @Suspendable {
		val npc = it.npc()
		npc.respawns(false)
		npc.sync().transmog(6501) // you have to transmog - not spawn another NPC! otherwise you can't replicate the exact 365 degree angle of the respawn animation.
		npc.combatInfo(it.npc().world().combatInfo(6501))
		npc.def(npc.world().definitions().get(NpcDefinition::class.java, 6501))
		npc.lockNoDamage()
		npc.graphic(1055)
		npc.animate(6270)
		npc.heal(npc.maxHp())
		val targ: Entity? = npc.attribOr<WeakReference<Entity?>>(AttributeKey.TARGET, WeakReference<Entity>(null)).get()
		it.delay(14)
		val form2 = Npc(6501, npc.world(), npc.tile())
		form2.spawnDirection(npc.attribOr(AttributeKey.FACING_DIRECTION, 6)).respawns(false)
		npc.hidden(true)
		npc.world().registerNpc(form2)
		if (targ != null) {
			form2.face(targ)
			form2.attack(targ)
			form2.clone_damage(npc)
		}
	}
	
	fun attack(npc: Npc, target: Entity, style: CombatStyle, script: Script) {
		val maxHit = npc.combatInfo().maxhit
		
		val attackAnimation = KalphiteQueen.animations.get(npc.id())?.get(style) ?: return
		
		npc.animate(attackAnimation)
		
		when (style) {
			CombatStyle.MELEE -> {
				target.hit(npc, if (EntityCombat.attemptHit(npc, target, style)) npc.world().random(maxHit) else 0)
			}
			CombatStyle.RANGE -> {
				npc.world().players().forEachInAreaKt(KalphiteQueen.area, { player ->
					val distance = npc.tile().distance(player.tile())
					
					val delay = Math.max(1, (15 + (distance * 15)) / 30)
					
					npc.world().spawnProjectile(npc, player, 473, 45, 30, 35, distance * 11, 15, 10)
					player.hit(npc, npc.world().random(maxHit), delay).combatStyle(style).applyProtection()
				})
			}
			CombatStyle.MAGIC -> {
				npc.graphic(278)
				npc.world().players().forEachInAreaKt(KalphiteQueen.area, { player ->
					val distance = npc.tile().distance(player.tile())
					
					val delay = Math.max(1, (15 + distance * 15) / 30)
					
					npc.world().spawnProjectile(npc, player, 280, 45, 30, 60, distance * 11, 15, 10)
					player.hit(npc, npc.world().random(maxHit), 2 + delay).combatStyle(style).graphic(281)
				})
			}
		}
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
}