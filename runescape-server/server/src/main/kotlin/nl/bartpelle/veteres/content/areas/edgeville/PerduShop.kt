package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 17/04/2016.
 */

object PerduShop {
	
	val PERDU = 7456
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(PERDU, @Suspendable {
			chatPerdu(it)
		})
		repo.onNpcOption2(PERDU, @Suspendable {
			chatPerdu(it)
		})
	}
	
	@Suspendable private fun chatPerdu(it: Script) {
		it.message("It doesn't look like he's got anything for sale.")
		
		// NOTE - discontinued 11/12/2016
		// The purpose of Perdu was to provide a half-price untradeable that had despawned.. maybe because you couldn't get back to it in time.
		// However since items can despawn when you are logged out, and therefore we would be unable to remember that accross logout, the system is
		// only helpful some of the time.
		
		/*val currency = if (it.player().world().realm().isPVP) "Blood money" else "coins"
		it.chatNpc("Hey there! Just to let you know, I'm phasing my shop out. You have until October 24th to re-buy your untradeables for a discount, then I'm closing shop.", PERDU)
		when (it.options("How comes?", "Ok. I'd like to see the shop please.")) {
			1 -> {
				it.chatPlayer("How comes?")
				it.chatNpc("When you drop an untradeable item on death, it'll now be broken. Use some $currency on it in order to repair it.", PERDU)
				it.chatPlayer("Ok, thanks Perdu.")
			}
			2 -> it.player().world().shop(1352).display(it.player());
		}*/
		
	}
}