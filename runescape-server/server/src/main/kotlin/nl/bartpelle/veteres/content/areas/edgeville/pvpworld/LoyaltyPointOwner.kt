package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Mack on 10/12/2017.
 */
object LoyaltyPointOwner {
	
	/**
	 * The npc id constant.
	 */
	const val ID = 5169
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onNpcOption1(ID, @Suspendable {
			displayOptions(it)
		})
		sr.onNpcOption2(ID, {
			it.player().world().shop(53).display(it.player())
		})
	}
	
	@Suspendable fun displayOptions(it: Script) {
		when (it.options("How do I earn loyalty points?", "I'd like to view your shop, please.", "Nothing")) {
			1 -> {
				it.chatNpc("Loyalty points are a form of reward earned automatically based on your activity on our community forum. If you need more information feel free to reference to our forum.", ID)
			}
			2 -> {
				it.player().world().shop(53).display(it.player())
			}
		}
	}
}