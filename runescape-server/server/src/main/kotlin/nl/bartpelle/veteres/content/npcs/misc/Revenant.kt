package nl.bartpelle.veteres.content.npcs.misc

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.items.equipment.BraceletOfEthereum
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.util.CombatStyle

object Revenant {

    @JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
        val npc = it.npc()
        var target = EntityCombat.getTarget(it) ?: return@s

        while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
            if (EntityCombat.canAttackDistant(npc, target, true, 5) && EntityCombat.attackTimerReady(npc)) {
                if (npc.hp() < npc.maxHp() / 2 && npc.world().rollDie(3, 1)) {
                    npc.graphic(1221)
                    npc.heal(npc.maxHp() / 3)
                } else if (EntityCombat.canAttackMelee(npc, target, false) && npc.world().random(2) == 1)
                    meleeAttack(npc, target)
                else if (npc.world().rollDie(2, 1))
                    rangedAttack(npc, target)
                else
                    magicAttack(npc, target)
                target.putattrib(AttributeKey.LAST_DAMAGER, npc)
                target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
                npc.putattrib(AttributeKey.LAST_TARGET, target)
                EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
            }

            EntityCombat.postHitLogic(npc)
            it.delay(1)
            target = EntityCombat.refreshTarget(it) ?: return@s
        }
    }

    @Suspendable private fun meleeAttack(npc: Npc, target: Entity) {
        if (!braceletOfEthereumEffect(target) && EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
            target.hit(npc, EntityCombat.randomHit(npc))
        else
            target.hit(npc, 0)
        npc.animate(npc.attackAnimation())
    }

    @Suspendable private fun rangedAttack(npc: Npc, target: Entity) {
        val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
        val delay = Math.max(1, (50 + (tileDist * 12)) / 30)

        npc.animate(npc.attackAnimation())
        npc.world().spawnProjectile(npc, target, 206, 30, 31, 31, 12 * tileDist, 15, 10)

        if (!braceletOfEthereumEffect(target) && EntityCombat.attemptHit(npc, target, CombatStyle.RANGE))
            target.hit(npc, EntityCombat.randomHit(npc), delay).combatStyle(CombatStyle.RANGE)
        else
            target.hit(npc, 0, delay)
    }

    @Suspendable private fun magicAttack(npc: Npc, target: Entity) {
        val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
        val delay = Math.max(1, (50 + (tileDist * 12)) / 30)

        npc.animate(npc.attackAnimation())
        npc.world().spawnProjectile(npc, target, 1415, 43, 31, 31, 12 * tileDist, 15, 10)

        if (!braceletOfEthereumEffect(target) && EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
            target.hit(npc, EntityCombat.randomHit(npc), delay).combatStyle(CombatStyle.MAGIC).graphic(Graphic(1454, 124, 0))
        } else {
            target.hit(npc, 0, delay).combatStyle(CombatStyle.MAGIC).graphic(Graphic(85, 124, 0))
        }
    }

    private fun braceletOfEthereumEffect(target: Entity): Boolean {
        if(target.isPlayer) {
            val player = target as Player
            val bracelet = player.equipment().get(EquipSlot.HANDS)
            val absorptionToggledOn = player.attribOr<Boolean>(AttributeKey.ETHEREUM_ABSORPTION, true)
            if (bracelet != null && bracelet.id() == BraceletOfEthereum.CHARGED && absorptionToggledOn) {
                BraceletOfEthereum.consumeCharge(player, bracelet)
                return true
            }
        }
        return false
    }

}