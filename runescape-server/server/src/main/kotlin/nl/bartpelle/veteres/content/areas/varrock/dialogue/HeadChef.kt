package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/10/2016.
 */

object HeadChef {
	
	val HEAD_CHEF = 2658
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(HEAD_CHEF) @Suspendable {
			it.chatNpc("Hello, welcome to the Cooking Guild.  Only accomplished<br>chefs and cooks are allowed in here.  Feel free to use<br>any of our facilities.", HEAD_CHEF, 569)
			it.chatPlayer("Thanks, bye.", 588)
		}
	}
}
