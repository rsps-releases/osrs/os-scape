package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.mechanics.MultiwayCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 8/28/2015.
 */
object Venenatis {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10)) {
				if (EntityCombat.attackTimerReady(npc)) {
					// Determine if we do a special hit, or a regular hit.
					if (npc.world().rollDie(14, 1)) {
						hurlWeb(npc, target)
					}
					
					// Determine if we're going to melee or mage
					if (EntityCombat.canAttackMelee(npc, target) && npc.world().rollDie(5, 1)) { // 1 on 5 chance to melee
						if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
							target.hit(npc, EntityCombat.randomHit(npc))
						} else {
							target.hit(npc, 0) // Uh-oh, that's a miss.
						}
						
						npc.animate(npc.attackAnimation())
					} else {
						// Grab all players in a radius and do our magic projectile on them.
						npc.world().players().forEachInAreaKt(npc.bounds(6), { enemy ->
							
							//If the target is currently in multi we..
							if (MultiwayCombat.includes(enemy.tile())) {
								magicAttack(npc, enemy)
							} else if (enemy == target) {
								magicAttack(npc, enemy)
							}
						})
						
						// Do an animation..
						npc.animate(5322)
					}
					
					if (npc.world().rollDie(20, 1)) {
						drainPrayer(npc, target)
					}
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun magicAttack(npc: Npc, target: Entity) {
		// Throw a magic projectile
		val tileDist = npc.tile().transform(3, 3, 0).distance(target.tile())
		npc.world().spawnProjectile(npc, target, 165, 30, 30, 20, 12 * tileDist, 14, 5)
		val delay = Math.max(1, (20 + (tileDist * 12)) / 30)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC)
		} else {
			target.hit(npc, 0, delay.toInt()) // Uh-oh, that's a miss.
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	fun hurlWeb(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(3, 3, 0).distance(target.tile())
		npc.world().spawnProjectile(npc, target, 1254, 20, 5, 5, 12 * tileDist, 15, 10)
		val delay = Math.max(1, (20 + (tileDist * 12)) / 30)
		target.message("Venenatis hurls her web at you, sticking you to the ground.")
		target.stun(6, false)
		target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.GENERIC) // Cannot protect from this.
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	fun drainPrayer(npc: Npc, target: Entity) {
		if (target.isPlayer) {
			val tileDist = npc.tile().transform(3, 3, 0).distance(target.tile())
			npc.world().spawnProjectile(target, npc, 171, 25, 25, 5, 12 * tileDist, 10, 10)
			
			val player = target as Player
			val curpray = player.skills().level(Skills.PRAYER)
			val add = curpray / 5 + 1
			val drain = 10 + add // base 10 drain + 20% of current prayer + 1. Example 50 prayer becomes 30. Best tactic to keep prayer low.
			player.skills().alterSkill(Skills.PRAYER, if (drain > curpray) -curpray else -drain)
			
			if (curpray > 0) {
				target.message("Your prayer was drained!")
			}
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcSpawn(6610) {
			it.npc().putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 30)
		}
	}
	
}