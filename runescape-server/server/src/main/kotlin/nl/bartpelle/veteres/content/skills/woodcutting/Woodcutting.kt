package nl.bartpelle.veteres.content.skills.woodcutting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.areas.zeah.wcguild.WoodcuttingGuild
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.items.InfernalTools
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.skills.firemaking.LogLighting
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 8/28/2015.
 */

object Woodcutting {
	
	class Woodcutting {
		enum class Tree(val logs: Int, val treeName: String, val level: Int, val difficulty: Int, val xp: Double, val respawnTime: Int, val single: Boolean, val petOdds: Int) {
			REGULAR(1511, "logs", 1, 55, 25.0, 75, true, 31764),
			ACHEY(2862, "achey logs", 1, 55, 25.0, 75, true, 31764),
			OAK(1521, "oak logs", 15, 95, 37.5, 15, false, 36114),
			WILLOW(1519, "willow logs", 30, 140, 67.5, 10, false, 28928),
			TEAK(6333, "teak logs", 35, 140, 85.0, 10, false, 28928),
			JUNIPER(13355, "juniper logs", 42, 150, 35.0, 30, false, 36000),
			MAPLE(1517, "maple logs", 45, 180, 100.0, 60, false, 22191),
			MAHOGANY(6332, "mahogany logs", 50, 200, 125.0, 80, false, 16541),
			YEW(1515, "yew logs", 60, 225, 175.0, 100, false, 14501),
			MAGIC(1513, "magic logs", 75, 375, 250.0, 100, false, 7232),
			REDWOOD(19669, "redwood logs", 90, 460, 380.0, 200, false, 6200),
			ENTTRUNK(-1, "ent trunk", -1, 250, 0.0, 0, false, 0) // Used for algo only
		}
		
		enum class Hatchet(val id: Int, val points: Int, val anim: Int, val level: Int) {
			BRONZE(1351, 13, 879, 1),
			IRON(1349, 15, 877, 1),
			STEEL(1353, 18, 875, 6),
			BLACK(1361, 21, 873, 11),
			MITHRIL(1355, 26, 871, 21),
			ADAMANT(1357, 30, 869, 31),
			RUNE(1359, 35, 867, 41),
			DRAGON(6739, 42, 2846, 61),
			THIRD_AGE(20011, 42, 7264, 61),
			INFERNAL(13241, 45, 2117, 61),
		}
		
		companion object {
			fun chance(level: Int, type: Tree, axe: Hatchet): Int {
				val points = ((level - type.level) + 1 + axe.points.toDouble())
				val denom = type.difficulty.toDouble()
				return (Math.min(0.95, points / denom) * 100).toInt()
			}
			
			fun findAxe(player: Player): Hatchet? {
				Hatchet.values().apply { reverse() }.forEach { it ->
					if (player.skills().xpLevel(Skills.WOODCUTTING) >= it.level && (it.id in player.inventory() || it.id in player.equipment())) {
						return it
					}
				}
				
				return null
			}
			
			@Suspendable fun cut(s: Script, tree: Tree, trunkObjectId: Int) {
				val player = s.player()
				val obj: MapObj = s.player().attrib(AttributeKey.INTERACTION_OBJECT)
				val axe: Hatchet? = findAxe(player)
				
				if (Skills.disabled(player, Skills.WOODCUTTING)) {
					return
				}
				
				//Does our player have an axe?
				if (axe == null) {
					player.sound(2277, 0)
					s.message("You do not have an axe which you have the Woodcutting level to use.")
					return
				}
				
				//Does our player have the required woodcutting level?
				if (!player.world().realm().isPVP && player.skills()[Skills.WOODCUTTING] < tree.level) {
					player.sound(2277, 0)
					s.message("You need a Woodcutting level of ${tree.level} to chop down this tree.")
					return
				}
				
				//Does out player have enough inventory space?
				if (player.inventory().full()) {
					player.sound(2277, 0)
					s.message("Your inventory is too full to hold any more logs.")
					return
				}
				
				s.delay(1)
				s.message("You swing your axe at the tree.")
				
				while (true) {
					if (player.inventory().full()) {
						player.sound(2277, 0)
						s.message("Your inventory is too full to hold any more logs.")
						player.animate(-1)
						return
					}
					
					player.animate(axe.anim)
					s.delay(2)
					
					// Check if the tree despawned
					if (!obj.valid(player.world())) {
						player.animate(-1)
						return
					}
					
					var chestChance = 80
					if (Equipment.wearingMaxCape(player) || CapeOfCompletion.WOODCUTTING.operating(player)) {
						chestChance += 8 // 10% increased chance of nests
					}
					if (player.world().rollDie(1000, chestChance)) { // 0.8% chance - I made this value up
						// TODO bird nest
					}
					
					var level = player.skills()[Skills.WOODCUTTING]
					if (player in WoodcuttingGuild.AREA_EAST || player in WoodcuttingGuild.AREA_WEST)
						level += 7; // +7 invisible boost in WC guild!
					
					if (player.world().random(100) <= chance(level, tree, axe)) {
						s.message("You get some ${tree.treeName}.")
						
						val lumberjackTriggered = BonusContent.isActive(player, BlessingGroup.BRIGHTWOOD)
								&& player.world().rollDie(2, 1)
						
						if (lumberjackTriggered) {
							s.message("The active Brightwood blessing lets you cut off an extra branch!")
						}
						
						// Woo! A pet! The reason we do this BEFORE the item is because it's... quite some more valuable :)
						// Rather have a pet than a clumsy log thing, right?
						val odds = (tree.petOdds.toDouble() * player.mode().skillPetMod()).toInt()
						if (player.world().rollDie(odds, 1)) {
							unlockBeaver(player)
						}
						
						// If we're using the infernal axe, we have 1/3 odds to burn the log and get 50% FM xp.
						if (axe === Hatchet.INFERNAL && player.world().rollDie(30, 10) && tree.logs > 0) {
							val log = LogLighting.LightableLog.logForId(tree.logs)
							
							if (log != null) {
								player.graphic(580, 50, 0)
								player.skills().__addXp(Skills.FIREMAKING, (log.xp * LogLighting.pyromancerOutfitBonus(player)) / 2)
							}
						} else {
							//If the player is in the wilderness we give them two logs, else we give only 1. : (
							if (WildernessLevelIndicator.inAttackableArea(player) && InfernalTools.active || lumberjackTriggered)
								player.inventory() += Item(tree.logs, 2)
							else
								player.inventory() += tree.logs
							
							AchievementAction.processCategoryAchievement(player, AchievementCategory.WOODCUTTING, tree.logs)
						}
						
						if (tree.single || player.world().random(10) == 3) {
							player.sound(3600, 0)
							player.animate(-1)
							
							s.runGlobal(player.world()) @Suspendable {
								val world = player.world()
								val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
								val spawned = MapObj(obj.tile(), trunkObjectId, obj.type(), obj.rot())
								old.replaceWith(spawned, world)
								it.delay(tree.respawnTime - 1)
								world.spawnObj(old)
								spawned.replaceWith(old, world)
							}
							
							//Finding blood money while skilling..
							if (player.world().realm().isOSRune && WildernessLevelIndicator.inWilderness(player.tile())) {
								if (player.world().rollDie(5, 1)) {
									if (player.inventory().add(Item(13307, 1), false).success()) {
										player.message("You find 1 blood money coin inside the water.")
									}
								}
							}
							s.addXp(Skills.WOODCUTTING, tree.xp) // Xp as last, it can spawn a dialogue
							return
						}
						
						s.addXp(Skills.WOODCUTTING, tree.xp) // Xp as last, it can spawn a dialogue
					}
					
					s.delay(2)
				}
			}
			
			fun unlockBeaver(player: Player) {
				if (!PetAI.hasUnlocked(player, Pet.BEAVER)) {
					// Unlock the varbit. Just do it, rather safe than sorry.
					player.varps().varbit(Pet.BEAVER.varbit, 1)
					
					// RS tries to add it as follower first. That only works if you don't have one.
					val currentPet = player.pet()
					if (currentPet == null) {
						player.message("You have a funny feeling like you're being followed.")
						PetAI.spawnPet(player, Pet.BEAVER, false)
					} else {
						// Sneak it into their inventory. If that fails, fuck you, no pet for you!
						if (player.inventory().add(Item(Pet.BEAVER.item), true).success()) {
							player.message("You feel something weird sneaking into your backpack.")
						} else {
							player.message("Speak to Probita to claim your pet!")
						}
					}
					
					player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.BEAVER.item).name(player.world())}.")
				} else {
					player.message("You have a funny feeling like you would have been followed...")
				}
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register_woodutting(r: ScriptRepository) {
		r.onObject(1278) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REGULAR, 1342) }
		r.onObject(1276) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REGULAR, 1342) }
		r.onObject(2091) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REGULAR, 1342) } // Evergreen
		r.onObject(1286) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REGULAR, 1351) } // Dead tree
		r.onObject(1282) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REGULAR, 1347) } // Dead tree
		r.onObject(1383) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REGULAR, 1358) } // Dead tree
		r.onObject(1289) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REGULAR, 1353) } // Dead tree
		r.onObject(2023) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.ACHEY, 1355) }
		r.onObject(1751) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.OAK, 1356) }
		r.onObject(1750) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.WILLOW, 9711) }
		r.onObject(1756) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.WILLOW, 9711) }
		r.onObject(1758) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.WILLOW, 9711) }
		r.onObject(1760) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.WILLOW, 9711) }
		r.onObject(9036) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.TEAK, 9037) }
		r.onObject(27499) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.JUNIPER, 27500) }
		r.onObject(1759) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.MAPLE, 9712) }
		r.onObject(1753) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.YEW, 9714) }
		r.onObject(1754) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.YEW, 9714) }
		r.onObject(7483) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.MAGIC, 9713) }
		r.onObject(1761) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.MAGIC, 9713) }
		r.onObject(1762) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.MAGIC, 9713) }
		r.onObject(29668) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REDWOOD, 29669) }
		r.onObject(29670) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.REDWOOD, 29671) }
		r.onObject(9034) @Suspendable { Woodcutting.cut(it, Woodcutting.Tree.MAHOGANY, 9035) }
	}
	
}
