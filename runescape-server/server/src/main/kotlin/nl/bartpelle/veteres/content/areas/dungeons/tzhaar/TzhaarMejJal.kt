package nl.bartpelle.veteres.content.areas.dungeons.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.itemUsedId
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/19/2016.
 */

object TzhaarMejJal {

	const val ALLOW_EXCHANGE_FOR_TOKKUL = false
	val TZHAAR_MEJ_JAL = 2180
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(TZHAAR_MEJ_JAL) @Suspendable {
			openingDialogue(it)
		}
		
		r.onNpcOption2(TZHAAR_MEJ_JAL) @Suspendable {
			it.chatPlayer("I'd like to exchange my fire cape.")
			if (it.player().inventory().contains(Item(6570, 1))) {
				val dialogueSet = if (ALLOW_EXCHANGE_FOR_TOKKUL) arrayOf("Bargain for TzRek-Jad.", "No, keep it.", "Sell my cape for 8,000 Tokkul.") else arrayOf("Bargain for TzRek-Jad.", "No, keep it.")
				when (it.options(*dialogueSet)) {
					1 -> {
						bargain(it)
					}
					3 -> {
						if (ALLOW_EXCHANGE_FOR_TOKKUL) {
							if (it.optionsTitled("Sacrifice your firecape for 8,000 TokKul?", "Yes, I know I won't get my cape back.", "No, I like my cape!") == 1) {
								if (it.player().inventory().remove(Item(6570, 1), false).success()) {
									it.player().inventory().add(Item(6529, 8000), true)
									it.chatNpc("Here TokKul. Thanks for cape.", TZHAAR_MEJ_JAL, TZHAAR_MEJ_JAL)
								}
							}
						}
					}
				}
			} else {
				it.chatNpc("Do not waste my time, JalYt-${it.player().name()}. You have no cape on you.", TZHAAR_MEJ_JAL)
			}
		}
		
		r.onItemOnNpc(TZHAAR_MEJ_JAL) {
			// Bargain fire cape
			if (it.itemUsedId() == 6570) {
				bargain(it)
			}
		}
	}
	
	@Suspendable private fun bargain(it: Script) {
		if (it.optionsTitled("Sacrifice your firecape for a chance at TzRek-Jad?", "Yes, I know I won't get my cape back.", "No, I like my cape!") == 1) {
			if (it.player().inventory().remove(Item(6570, 1), false).success()) {
				if (it.player().world().rollDie(200, 1)) {
					unlockTzrekJad(it.player())
					it.chatNpc("You lucky. Better train him good else TzTok-Jad find you, JalYt.", TZHAAR_MEJ_JAL, 567)
				} else {
					it.chatNpc("You not lucky. Maybe next time, JalYt.", TZHAAR_MEJ_JAL, 567)
				}
			}
		}
	}
	
	@Suspendable fun openingDialogue(it: Script) {
		it.chatNpc("You want help JalYt-Ket-${it.player().name()}?", TZHAAR_MEJ_JAL, 592)
		when (it.options("What is this place?", "What did you call me", "No I'm fine thanks.")) {
			1 -> whatIsThisPlace(it)
			2 -> whatDidYouCallMe(it)
			3 -> noImFineThanks(it)
		}
	}
	
	@Suspendable fun whatIsThisPlace(it: Script) {
		it.chatPlayer("What is this place?", 554)
		it.chatNpc("This is the fight cave, TzHaar-Xil made it for practice,<br>but many JalYt come here to fight too.<br>" +
				"Just enter the cave and make sure you're prepared.", TZHAAR_MEJ_JAL, 590)
		when (it.options("Are there any rules?", "Ok thanks.")) {
			1 -> {
				it.chatPlayer("Are there any rules?", 554)
				it.chatNpc("Rules? Survival is the only rule in there.", TZHAAR_MEJ_JAL, 575)
				when (it.options("Do I win anything?", "Sounds good.")) {
					1 -> {
						it.chatPlayer("Do I win anything?", 554)
						it.chatNpc("You ask a lot of questions.<br>Might give you TokKul if you last long enough.", TZHAAR_MEJ_JAL, 589)
						it.chatPlayer("...", 588)
						it.chatNpc("Before you ask, TokKul is like your Coins.", TZHAAR_MEJ_JAL, 610)
						it.chatNpc("Gold is like you JalYt, soft and easily broken, we use<br>hard rock forged in fire like TzHaar!", TZHAAR_MEJ_JAL, 615)
						
					}
					2 -> it.chatPlayer("Sounds good.", 567)
				}
			}
			2 -> it.chatPlayer("Ok thanks.", 567)
		}
	}
	
	@Suspendable fun whatDidYouCallMe(it: Script) {
		it.chatPlayer("What did you call me?", 614)
		it.chatNpc("Are you not JalYt-Ket?", TZHAAR_MEJ_JAL, 575)
		when (it.options("What's a 'JalYt-Ket'?", "I guess so...", "No I'm not!")) {
			1 -> {
				it.chatPlayer("What's a 'JalYt-Ket'?", 554)
				it.chatNpc("That what you are... you tough and strong no?", TZHAAR_MEJ_JAL, 575)
				it.chatPlayer("Well yes I suppose I am...", 575)
				it.chatNpc("Then you JalYt-Ket!", TZHAAR_MEJ_JAL, 567)
				when (it.options("What are you then?", "Thanks for explaining it.")) {
					1 -> {
						it.chatPlayer("What are you then?", 554)
						it.chatNpc("Foolish JalYt, I am TzHaar-Mej, one of the mystics of<br>this city.", TZHAAR_MEJ_JAL, 56)
						when (it.options("What other types are there?", "Ah ok then.")) {
							1 -> {
								it.chatPlayer("What other types are there?", 554)
								it.chatNpc("There are the mighty TzHaar-Ket who guard us, the<br>swift TzHaar-Xil who hunt for our food, and the skilled" +
										"<br>TzHaar-Hur who craft our homes and tools.", TZHAAR_MEJ_JAL, 590)
							}
							2 -> it.chatPlayer("Ah ok then.", 588)
						}
					}
					2 -> it.chatPlayer("Thanks for explaining it.", 610)
				}
			}
			2 -> {
				it.chatPlayer("I guess so...", 575)
				it.chatNpc("Well then, no problems.", TZHAAR_MEJ_JAL, 588)
			}
			3 -> {
				it.chatPlayer("No I'm not!", 614)
				it.chatNpc("What ever you say, crazy JalYt!", TZHAAR_MEJ_JAL, 592)
			}
		}
	}
	
	@Suspendable fun noImFineThanks(it: Script) {
		it.chatPlayer("No I'm fine thanks.", 588)
	}
	
	fun unlockTzrekJad(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.TZREK_JAD)) {
			player.varps().varbit(Pet.TZREK_JAD.varbit, 1)
			
			val currentPet = player.pet()
			if (currentPet == null) {
				PetAI.spawnPet(player, Pet.TZREK_JAD, false)
			} else {
				if (player.inventory().add(Item(Pet.TZREK_JAD.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.TZREK_JAD.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
}
