package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.util.GameCommands

/**
 * Created by Jak on 22/08/2016.
 */
class ConfirmLoadNormPreset(val name: String) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(it: Script) {
		if (it.optionsTitled("Confirm preset load", "Yes - load the $name setup.", "Never mind.") == 1) {
			when (name) {
				"hybrid" -> GameCommands.process(it.player(), "hybrid")
				"main" -> GameCommands.process(it.player(), "main")
				"nh" -> GameCommands.process(it.player(), "nh")
				"zerker" -> GameCommands.process(it.player(), "zerker")
			}
		}
	}
}