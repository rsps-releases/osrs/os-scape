package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.mechanics.Prayers
import nl.bartpelle.veteres.content.openInterface
import nl.bartpelle.veteres.content.setLevel
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by bart on 7/19/15.
 */

object SkillGuideResources {
	
	/**
	 * A list of buttons that players cannot use to set their stats.
	 */
	val BLACKLISTED_BUTTONS = arrayListOf(12, 15, 16, 17, 19, 22)
}

@Suspendable fun display(script: Script, skill: Int, skill_id: Int, child: Int) {
	var skillGuide = false
	
/*	for (button in SkillGuideResources.BLACKLISTED_BUTTONS) {
		if (child == button) {
			skillGuide = true
		}
	}
	*/
	if (script.player().world().realm().isPVP) {
		if (skillGuide) {
			displaySkillGuide(script, skill)
		} else {
			changeLevel(script, skill_id)
		}
	} else {
		displaySkillGuide(script, skill)
	}
}

fun displaySkillGuide(script: Script, skill: Int) {
	script.openInterface(214)
	script.player().varps().varbit(4371, skill)
	script.player().varps().varbit(4372, 0)
}

@Suspendable fun changeLevel(it: Script, skill_id: Int) {
	val admin = it.player().privilege().eligibleTo(Privilege.ADMIN)
	if (admin || (!WildernessLevelIndicator.inWilderness(it.player().tile()) && !WildernessLevelIndicator.inAttackableArea(it.player()))) {
		if (admin || it.player().tile().inArea(3044, 3455, 3114, 3524)) {
			
			it.player().stopActions(false)
			
			var num = it.inputInteger("What would you like to set your ${Skills.SKILL_NAMES[skill_id]} level to?")
			if (skill_id == 3 && num < 10) {
				num = 10
			}

			if (!it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				for (item in it.player().equipment().items()) {
					if (item != null) {
						it.itemBox("You cannot change your levels while having equipment on.", 6570)
						return
					}
				}
			}
			
			// Turn off prayers
			Prayers.disableAllPrayers(it.player())
			
			it.player().skills().setXp(skill_id, Skills.levelToXp(Math.min(99, num)).toDouble())
			it.player().skills().update()
			it.player().skills().recalculateCombat()
			it.setLevel(skill_id)
		} else {
			it.player().message("You can't change levels here.")
		}
	}
}

@Suspendable fun registerSkillGuideButton(repo: ScriptRepository, inter: Int, child: Int, skill: Int, skill_id: Int) {
	repo.onButton(inter, child, s@ @Suspendable {
		if (!it.player().privilege().eligibleTo(Privilege.ADMIN)) {
			for (button in SkillGuideResources.BLACKLISTED_BUTTONS) {
				if (child == button) {
					it.message("You are unable to set your own level for this skill. It must be trained.")
					return@s
				}
			}
		}
		display(it, skill, skill_id, child)
	})
}

@ScriptMain fun skillguides(repo: ScriptRepository) {
	registerSkillGuideButton(repo, 320, 1, 1, 0) // Attack
	registerSkillGuideButton(repo, 320, 2, 2, 2) // Strength
	registerSkillGuideButton(repo, 320, 3, 5, 1) // Defence
	registerSkillGuideButton(repo, 320, 4, 3, 4) // Ranged
	registerSkillGuideButton(repo, 320, 5, 7, 5) // Prayer
	registerSkillGuideButton(repo, 320, 6, 4, 6) // Magic
	registerSkillGuideButton(repo, 320, 7, 12, 20) // Runecrafting
	registerSkillGuideButton(repo, 320, 8, 22, 22) // Construction
	registerSkillGuideButton(repo, 320, 9, 6, 3) // Hitpoints
	registerSkillGuideButton(repo, 320, 10, 8, 16) // Agility
	registerSkillGuideButton(repo, 320, 11, 9, 15) // Herblore
	registerSkillGuideButton(repo, 320, 12, 10, 17) // Thieving
	registerSkillGuideButton(repo, 320, 13, 11, 12) // Crafting
	registerSkillGuideButton(repo, 320, 14, 19, 9) // Fletching
	registerSkillGuideButton(repo, 320, 15, 20, 18) // Slayer
	registerSkillGuideButton(repo, 320, 16, 23, 21) // Hunter
	registerSkillGuideButton(repo, 320, 17, 13, 14) // Mining
	registerSkillGuideButton(repo, 320, 18, 14, 13) // Smithing
	registerSkillGuideButton(repo, 320, 19, 15, 10) // Fishing
	registerSkillGuideButton(repo, 320, 20, 16, 7) // Cooking
	registerSkillGuideButton(repo, 320, 21, 17, 11) // Firemaking
	registerSkillGuideButton(repo, 320, 22, 18, 8) // Woodcutting
	registerSkillGuideButton(repo, 320, 23, 21, 19) // Farming
	
	for (tab in 11..22) {
		repo.onButton(214, tab) @Suspendable { it.player().varps().varbit(4372, tab - 11) }
	}
	
	/*repo.onObject(1286) @Suspendable {
		it.chatPlayer("Hello there.", 588)
		it.chatNpc("What brings you to our holy monument?", 5045, 554)

		when(it.options("Who are you?", "I'm in search of a quest.", "Did you build this?")) {
			1 -> {
				it.chatPlayer("Who are you?", 554)
				it.chatNpc("We are the druids of Guthix. We worship our god at<br>our famous stone circles. " +
						"You will find them located<br>throughout these lands.", 5045, 590)

				when (it.options("What about the stone circle full of dark wizards?", "So what's so good about Guthix?", "Well, I'll be on my way now.")) {
					1 -> {
						it.chatPlayer("What about the stone circle full of dark wizards?", 554)
						it.chatNpc("That used to be OUR stone circle. Unfortunately,<br>many many years ago, " +
								"dark wizards cast a wicked spell<br>upon it so that they could corrupt its power " +
								"for their<br>own evil ends.", 5045, 591)
						it.chatNpc("When they cursed the rocks for their rituals they made<br>them useless to us and our magics. " +
								"We require a brave<br>adventurer to go on a quest for us to help purify the<br>circle of Varrock.", 5045, 591)

						when (it.options("Ok, I will try and help.", "No, that doesn't sound very interesting.", "So... is there anything in this for me?")) {
							2 -> {
								it.chatPlayer("No, that doesn't sound very interesting.", 575)
								it.chatNpc("I will not try and change your mind adventurer. Some<br>day when you have " +
										"matured you may reconsider your<br>position. We will wait until then.", 5045, 590)
							}
						}
					}
					3 -> {
						it.chatPlayer("Well, I'll be on my way now.")
						it.chatNpc("Goodbye adventurer. I feel we shall meet again.", 5045)
					}
				}
			}
			2 -> {}
			3 -> {
				it.chatPlayer("Did you build this?", 554)
				it.chatNpc("What, personally? No, of course I didn't. However, our<br>" +
						"forefathers did. The first Druids of Guthix built many<br>stone circles across these " +
						"lands over eight hundred<br>years ago.", 5045, 591)
				it.chatNpc("Unfortunately we only know of two remaining, and of<br>those only one is usable by us anymore.", 611)
			}
		}
	}*/
}