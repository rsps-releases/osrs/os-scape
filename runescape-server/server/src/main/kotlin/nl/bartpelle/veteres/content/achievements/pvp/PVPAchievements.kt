package nl.bartpelle.veteres.content.achievements.pvp

import com.google.common.collect.Sets
import nl.bartpelle.veteres.content.achievements.Achievement
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.npcs.bosses.wilderness.SkotizoController
import nl.bartpelle.veteres.content.skills.fishing.Fish
import nl.bartpelle.veteres.content.skills.hunter.Chinchompas
import nl.bartpelle.veteres.content.skills.mining.Mining
import nl.bartpelle.veteres.content.skills.thieving.Pickpocketing
import nl.bartpelle.veteres.content.skills.woodcutting.Woodcutting
import java.util.stream.IntStream

enum class PVPAchievements(set: Set<Achievement>) {
    EASY(Sets.newHashSet(
            Achievement.create(0, AchievementCategory.CUSTOM, 10, "Thieve 10 times from the chest in Rogue's Den"),
            Achievement.create(1, AchievementCategory.MINING, 50, "Mine 50 rocks in the Wilderness", *IntStream.range(Mining.Rock.CLAY.ore, Mining.Rock.COAL.ore + 1).toArray()),
            Achievement.create(2, AchievementCategory.WOODCUTTING, 50, "Cut 50 normal trees in the Wilderness", Woodcutting.Woodcutting.Tree.REGULAR.logs),
            Achievement.create(3, AchievementCategory.HUNTER, 50, "Hunt 50 chinchompas in the Wilderness", Chinchompas.GREYCHIN_ITEM, Chinchompas.REDCHIN_ITEM, Chinchompas.BLACKCHIN_ITEM),
            Achievement.create(4, AchievementCategory.SLAYER, 5, "Complete 5 Wilderness Slayer tasks"),
            Achievement.create(5, AchievementCategory.PVM, 25, "Kill 25 Mage Arena mages", 1610, 1611, 1612),
            Achievement.create(6, AchievementCategory.PVM, 25, "Kill 25 Wilderness Bosses", 6611, 6612, 6609, 6610, 6619, 6618, 6615, 2054),
            Achievement.create(7, AchievementCategory.BOUNTY_HUNTER, 5, "Kill 5 Bounty Hunter Targets"),
            Achievement.create(8, AchievementCategory.PVM, 25, "Kill 25 Revenants", 7452, SkotizoController.SKOTIZO_ID, 7881, 7932, 7940, 7936, 7935, 7939, 7937, 7938, 7934, 7931, 7933),
            Achievement.create(9, AchievementCategory.PVP, 10, "Kill 10 Players within the Wilderness"),
            Achievement.create(10, AchievementCategory.BOUNTY_HUNTER, 5, "Kill 5 Bounty Hunter Targets")
    )),
    MEDIUM(Sets.newHashSet(
            Achievement.create(11, AchievementCategory.CUSTOM, 50, "Thieve 50 times from the chest in Rogue's Den"),
            Achievement.create(12, AchievementCategory.HUNTER, 100, "Hunt 100 chinchompas in the Wilderness", Chinchompas.GREYCHIN_ITEM, Chinchompas.REDCHIN_ITEM, Chinchompas.BLACKCHIN_ITEM),
            Achievement.create(13, AchievementCategory.WOODCUTTING, 100, "Cut 100 yew trees in the Resource Area", {it.tile().inArea(nl.bartpelle.veteres.content.areas.wilderness.ResourceArena.ARENA_BOUNDARIES) }, Woodcutting.Woodcutting.Tree.YEW.logs),
            Achievement.create(14, AchievementCategory.MINING, 275, "Mine 275 rocks in the Wilderness", *IntStream.range(Mining.Rock.CLAY.ore, Mining.Rock.COAL.ore + 1).toArray()),
            Achievement.create(15, AchievementCategory.FISHING, 100, "Catch 100 angler fish in the Resource Area", { it.tile().inArea(nl.bartpelle.veteres.content.areas.wilderness.ResourceArena.ARENA_BOUNDARIES) }, Fish.ANGLERFISH.item),
            Achievement.create(16, AchievementCategory.SLAYER, 12, "Complete 12 Wilderness Slayer tasks"),
            Achievement.create(17, AchievementCategory.PVM, 100, "Kill 100 Mage Arena mages", 1610, 1611, 1612),
            Achievement.create(18, AchievementCategory.BOUNTY_HUNTER, 15, "Kill 15 Bounty Hunter Targets"),
            Achievement.create(19, AchievementCategory.PVM, 1, "Complete 1 Skotizo kill with most damage", 7286),
            Achievement.create(20, AchievementCategory.PVM, 55, "Kill 55 Ancient Wyverns", 7795),
            Achievement.create(21, AchievementCategory.THIEVING, 50, "Thieve 50 times from Heros in the Wilderness", 3106),
            Achievement.create(22, AchievementCategory.PVM, 100, "Kill 100 Hellhounds in the Wilderness", 104, 105),
            Achievement.create(23, AchievementCategory.CUSTOM, 1, "Use Piles to note items in Resource Area")
    )),
    HARD(Sets.newHashSet(
            Achievement.create(24, AchievementCategory.MINING, 500, "Mine 500 rocks in the Wilderness", *IntStream.range(Mining.Rock.CLAY.ore, Mining.Rock.COAL.ore + 1).toArray()),
            Achievement.create(25, AchievementCategory.WOODCUTTING, 150, "Cut 150 magic trees in the Wilderness", Woodcutting.Woodcutting.Tree.MAGIC.logs),
            Achievement.create(26, AchievementCategory.CUSTOM, 1, "Redeem 1 Bloody Key"),
            Achievement.create(27, AchievementCategory.PVM, 50, "Kill 50 Demonic Gorillas", 7144),
            Achievement.create(28, AchievementCategory.SLAYER, 20, "Complete 20 Wilderness Slayer tasks"),
            Achievement.create(29, AchievementCategory.KILLSTREAK, 1, "Obtain a killstreak of 10 in the Wilderness", 10),
            Achievement.create(30, AchievementCategory.FISHING, 500, "Catch 500 fish in the Wilderness", *Fish.getFishItemIds()),
            Achievement.create(31, AchievementCategory.THIEVING, 300, "Thieve 300 times from any npc in Wilderness", *Pickpocketing.PickpocketableNpc.getNpcIds()),
            Achievement.create(32, AchievementCategory.CUSTOM, 1, "Purchase a Slayer Helmet from Krystilia"),
            Achievement.create(33, AchievementCategory.CUSTOM, 5, "Complete 5 Fight Cave sessions"),
            Achievement.create(34, AchievementCategory.PVM, 15, "Kill 15 Runite Golems in the Resource Area", { it.tile().inArea(nl.bartpelle.veteres.content.areas.wilderness.ResourceArena.ARENA_BOUNDARIES) }, 6600),
            Achievement.create(35, AchievementCategory.PVP, 50, "Kill 50 Players within the Wilderness"),
            Achievement.create(36, AchievementCategory.MINING, 25, "Mine 25 Runite rocks in the Wilderness", Mining.Rock.RUNE.ore),
            Achievement.create(37, AchievementCategory.PVM, 1, "Complete 1 Tekton kill with most damage", 7542),
            Achievement.create(38, AchievementCategory.CUSTOM, 1, "Purchase a Rune Pouch from the Bounty Shop"),
            Achievement.create(39, AchievementCategory.CUSTOM, 1, "Complete 1 round of The Inferno")
    ));

    private val achievementSet = set

    val achievements: Set<Achievement>
        get() = this.achievementSet

    companion object {

        fun byDifficulty(difficulty: PVPAchievements) : Set<Achievement> {
            return (difficulty.achievements)
        }
    }
}