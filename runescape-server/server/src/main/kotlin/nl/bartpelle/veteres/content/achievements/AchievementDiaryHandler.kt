package nl.bartpelle.veteres.content.achievements

import nl.bartpelle.veteres.model.entity.Player

interface AchievementDiaryHandler {

    fun achievements(): MutableSet<Achievement>

    fun achievementsByKey(key: AchievementCategory): Set<Achievement>

    /**
     * Returns a stream containing all tasks that haven't been flagged as complete.
     */
    fun getIncompleteTasks(player: Player) = achievements().filter({ task -> !task.completed(player)})

    /**
     * Finds an achievement definition respective to the uid specified in the argument.
     */
    fun achievementById(player: Player, id: Int): Achievement? {
        val task = getIncompleteTasks(player).stream().filter({task -> task.rawId() == id}).findAny()
        return if (task.isPresent) task.get() else null
    }

    /**
     * A flag checking if the player has completed all achievements.
     */
    fun completedAllAchievements(player: Player): Boolean {
        return (achievements().stream().noneMatch({task -> !task.completed(player)}))
    }
}