package nl.bartpelle.veteres.content.areas.falador

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/23/2016.
 */

object Dwarves {
	
	val DWARF_ONE = 3361
	val DWARF_TWO = 3362
	val DWARF_THREE = 3363
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(DWARF_ONE) @Suspendable {
			it.chatNpc("Welcome to the Mining Guild.<br>Can I help you with anything?", DWARF_ONE, 568)
			when (it.options("What have you got in the Guild?", "What do you dwarves do with the ore you mine?", "No thanks, I'm fine.")) {
				1 -> what_have_you_got(it, DWARF_ONE)
				2 -> what_dwarves_do(it, DWARF_ONE)
				3 -> no_thanks(it)
			}
		}
		
		r.onNpcOption1(DWARF_TWO) @Suspendable {
			it.chatNpc("Welcome to the Mining Guild.<br>Can I help you with anything?", DWARF_TWO, 568)
			when (it.options("What have you got in the Guild?", "What do you dwarves do with the ore you mine?", "No thanks, I'm fine.")) {
				1 -> what_have_you_got(it, DWARF_TWO)
				2 -> what_dwarves_do(it, DWARF_TWO)
				3 -> no_thanks(it)
			}
		}
		
		r.onNpcOption1(DWARF_THREE) @Suspendable {
			it.chatNpc("Welcome to the Mining Guild.<br>Can I help you with anything?", DWARF_THREE, 568)
			dwarf_three_options(it)
		}
	}
	
	@Suspendable fun dwarf_three_options(it: Script) {
		when (it.options("What have you got in the Guild?", "What do you dwarves do with the ore you mine?", "Can you tell me about your skillcape?", "No thanks, I'm fine.")) {
			1 -> what_have_you_got(it, DWARF_THREE)
			2 -> what_dwarves_do(it, DWARF_THREE)
			3 -> about_your_skillcape(it)
			4 -> no_thanks(it)
		}
	}
	
	@Suspendable fun what_dwarves_do(it: Script, npc: Int) {
		it.chatPlayer("What do you dwarves do with the ore you mine?", 554)
		it.chatNpc("What do you think? We smelt it into bars, smith the<br>metal to make armour and weapons, then we exchange<br>them for goods and services.", npc, 607)
		it.chatPlayer("I don't see many dwarves<br>selling armour or weapons here.", 593)
		it.chatNpc("No, this is only a mining outpost. We dwarves don't<br>much like to settle in human cities. Most of the ore is<br>carted off to Keldagrim, the great dwarven city.<br>They've got a special blast furnace up there - it makes", npc, 570)
		it.chatNpc("smelting the ore so much easier. There are plenty of<br>dwarven traders working in Keldagrim. Anyway, can I<br>help you with anything else?", npc, 569)
		when (it.options("What have you got in the guild?", "No thanks, I'm fine.")) {
			1 -> what_have_you_got(it, npc)
			2 -> no_thanks(it)
		}
	}
	
	@Suspendable fun what_have_you_got(it: Script, npc: Int) {
		it.chatPlayer("What have you got in the guild?", 554)
		it.chatNpc("Ooh, it's WONDERFUL! There are lots of coal rocks,<br>and even a few mithril rocks in the guild,<br>all exclusively for people with at least level 60 mining.<br>There's no better mining site anywhere near here.", npc, 570)
		if (it.player().skills().level(Skills.MINING) < 60) {
			it.chatPlayer("So you won't let me go in there?", 610)
			it.chatNpc("Sorry, but rules are rules. Do some more training<br>first. Can I help you with anything else?", npc, 611)
		} else {
			it.chatPlayer("It's a good thing I have level ${it.player().skills().level(Skills.MINING)} Mining.", 567)
			it.chatNpc("Yes, that's pretty good. Did you want anything else?", npc, 567)
		}
		when (it.options("What do you dwarves do with the ore you mine?", "No thanks, I'm fine.")) {
			1 -> what_dwarves_do(it, npc)
			2 -> no_thanks(it)
			
		}
	}
	
	@Suspendable fun about_your_skillcape(it: Script) {
		it.chatPlayer("Can you tell me about your skillcape?", 554)
		it.chatNpc("Sure, this is a Skillcape of Mining. It shows my stature<br>as a master miner! It has all sorts of uses including a<br>skill boost to my Mining skill and a chance of mining<br>extra ores. When you get to level 99 come and talk to", DWARF_THREE, 591)
		it.chatNpc("me and I'll sell you one. Is there anything else I can<br>help you with?", DWARF_THREE, 589)
		when (it.options("Yes.", "No.")) {
			1 -> {
				it.chatNpc("So what can I help you with, then?", DWARF_THREE, 567)
				dwarf_three_options(it)
			}
			2 -> {
				it.chatPlayer("No.", 588)
				it.chatNpc("Okay, bye then.", DWARF_THREE, 588)
			}
		}
	}
	
	@Suspendable fun no_thanks(it: Script) {
		it.chatPlayer("No thanks, I'm fine.", 562)
	}
}

