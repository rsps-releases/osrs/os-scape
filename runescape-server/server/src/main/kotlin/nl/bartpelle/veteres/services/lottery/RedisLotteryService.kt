package nl.bartpelle.veteres.services.lottery

import com.typesafe.config.Config
import nl.bartpelle.veteres.GameServer
import nl.bartpelle.veteres.content.lottery.Lottery
import nl.bartpelle.veteres.services.redis.RedisService
import org.apache.logging.log4j.LogManager
import redis.clients.jedis.JedisPubSub
import java.util.concurrent.Executors
import java.util.concurrent.ForkJoinPool

/**
 * Created by Jonathan on 2/8/2017.
 */
class RedisLotteryService : JedisPubSub(), LottoService {
	
	private lateinit var server: GameServer
	private lateinit var redisService: RedisService
	private val fjp = ForkJoinPool()
	
	override fun setup(server: GameServer, serviceConfig: Config) {
		this.server = server
	}
	
	override fun toggle() = fjp.execute { redisService.doOnPool { j -> j.publish(TOGGLE, "Toggle") } }
	
	override fun onMessage(channel: String, message: String) {
		try {
			logger.debug("Lotto event: {}.", message)
			when (channel) {
				TOGGLE -> Lottery.toggle()
			}
		} catch (e: Exception) {
			e.printStackTrace()
		}
	}
	
	override fun start(): Boolean {
		val service = server.service(RedisService::class.java, false)
		if (!service.isPresent) {
			logger.error("Cannot use Redis Lotto service if the Redis service is disabled")
			return false
		}
		
		redisService = service.get()
		
		if (redisService.isAlive) {
			val executor = Executors.newFixedThreadPool(1)
			executor.submit { redisService.doOnPool { j -> j.subscribe(this, TOGGLE) } }
			
			logger.info("Redis Lotto provider ready to dispatch and receive messages.")
		} else {
			logger.error("Cannot use Redis Lotto service if the Redis service failed to start")
			return false
		}
		return true
	}
	
	override fun stop() = true
	
	override fun isAlive() = true
	
	fun server() = server
	
	companion object {
		
		private val logger = LogManager.getLogger()
		
		private val TOGGLE = "toggle"
		
	}
	
}
