package nl.bartpelle.veteres.content.items.skillcape

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Business on 2016-07-04.
 */
object FishingCape : CapeOfCompletionPerk(intArrayOf(1, 2)) {
	
	val TELEPORT_LOCATION = Tile(2611, 3393)
	
	override fun option(option: Int): Function1<Script, Unit> = @Suspendable {
		val player = it.player()
		
		when (option) {
			1 -> {
				CapeOfCompletion.boost(Skills.FISHING, player)
			}
			2 -> {
				if (Teleports.canTeleport(player, true, TeleportType.GENERIC)) {
					Teleports.basicTeleport(it, TELEPORT_LOCATION)
				}
			}
		}
	}
	
}