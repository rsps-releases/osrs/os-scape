package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 09/11/2016.
 */
object PlankMake {
	
	enum class Planks(val logId: Int, val plankId: Int, val cost: Int) {
		NORMAL(1511, 960, 70), OAK(1521, 8778, 175),
		TEAK(6333, 8780, 350), MAHOGANY(6332, 8782, 1050);
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onSpellOnItem(218, 125, s@ @Suspendable {
			val player = it.player()
			
			if (player.skills().level(Skills.MAGIC) < 86) {
				it.messagebox("You need 86 magic to cast this spell.")
				return@s
			}
			
			val item = player.inventory()[player[AttributeKey.BUTTON_SLOT]] ?: return@s
			val runes = arrayOf(Item(9075, 2), Item(557, 15), Item(561, 1))
			var plankType: Planks? = null
			
			Planks.values().forEach { plank ->
				if (plank.logId == item.id()) {
					plankType = plank
				}
			}
			
			plankType ?: return@s
			
			//Is our player currently facing a cool-down timer?
			if (player.timers().has(TimerKey.PLANK_MAKE))
				return@s
			
			//Does the player have enough gold?
			if (player.inventory().count(995) < plankType!!.cost) {
				it.message("It costs ${plankType!!.cost} coins to make this plank.")
				return@s
			}
			
			var make = player.attribOr<Boolean>(AttributeKey.PLANK_MAKE_WARNING, false)
			if (!make) {
				val opt = it.optionsTitled("Spend ${plankType!!.cost} coins to make ${plankType.toString().toLowerCase()} planks?",
						"Yes.", "Yes, and don't show this again.", "Cancel")
				
				if (opt != 3) {
					make = true
				}
				if (opt == 2) {
					player.putattrib(AttributeKey.PLANK_MAKE_WARNING, true)
				}
			}
			if (make) {

				// Do we have the required runes?
				if (!MagicCombat.has(player, runes, true)) {
					return@s
				}
				if (player.inventory().remove(Item(item.id(), 1), false).success()) {
					player.timers().extendOrRegister(TimerKey.PLANK_MAKE, 3)
					player.inventory().add(Item(plankType!!.plankId, 1), false, it.buttonSlot())
					player.inventory().remove(Item(995, plankType!!.cost), false)
					player.skills().__addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 180.0 else 90.0)
					it.sound(3617)
					it.animate(6298)
					player.graphic(1063, 100, 0)
				}
			}
		})
	}
}