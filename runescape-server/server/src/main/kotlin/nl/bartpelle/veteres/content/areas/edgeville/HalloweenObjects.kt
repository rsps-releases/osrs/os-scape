package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/21/2016.
 */

//Halloween objects : )

object HalloweenObjects {
	
	val SHOW_HALLOWEEN_OBJECTS = false
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit @Suspendable {
			val world: World = it.ctx<World>()
			it.delay(1)
			
			if (SHOW_HALLOWEEN_OBJECTS) {
				//Spider webs
				world.spawnObj(MapObj(Tile(3091, 3499), 25443, 10, 0), false)
				world.spawnObj(MapObj(Tile(3098, 3498), 25443, 10, 1), false)
				world.spawnObj(MapObj(Tile(3094, 3488), 25443, 10, 2), false)
				world.spawnObj(MapObj(Tile(3091, 3492), 25443, 10, 3), false)
				world.spawnObj(MapObj(Tile(3098, 3512), 25443, 10, 1), false)
				
				//Rowboat
				world.spawnObj(MapObj(Tile(3103, 3480), 29648, 10, 0), false)
				
				//Skulls
				world.spawnObj(MapObj(Tile(3105, 3497), 658, 10, 1), false)
				world.spawnObj(MapObj(Tile(3107, 3496), 658, 10, 1), false)
				world.spawnObj(MapObj(Tile(3106, 3501), 658, 10, 1), false)
				
				//Objects
				world.spawnObj(MapObj(Tile(3108, 3498), 13182, 10, 3), true)
				world.spawnObj(MapObj(Tile(1640, 4827), 25592, 10, 3), true)
			}
		}
	}
}
