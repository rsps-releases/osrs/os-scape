package nl.bartpelle.veteres.content.npcs.fossilisland

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 9/8/2017.
 */
object AncientWyvern {
	
	val MAGIC_ATTACK = 7657
	val ALT_MELEE_ATTACK = 7658
	
	@JvmField val combat: Function1<Script, Unit> = s@ @Suspendable {
		
		val wyvern = it.npc()
		var target = EntityCombat.getTarget(wyvern) ?: return@s
		
		while(EntityCombat.targetOk(wyvern, target) && PlayerCombat.canAttack(wyvern, target)) {
			if (EntityCombat.canAttackDistant(wyvern, target, true, 6) && EntityCombat.attackTimerReady(wyvern)) {
				
				when (wyvern.world().random(2)) {
					1 -> {
						doMagic(wyvern, target)
					}
					else -> {
						if (EntityCombat.canAttackMelee(wyvern, target, false)) {
							if (wyvern.world().random(2) == 1) {
								doMelee(wyvern, target)
							} else {
								doTailWhip(wyvern, target)
							}
						} else {
							doMagic(wyvern, target)
						}
					}
				}
			}
			
			EntityCombat.postHitLogic(wyvern)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun doMagic(wyvern: Npc, target: Entity) {
		
		val tileDist = wyvern.tile().distance(target.tile())
		val delay = Math.max(1, (30 * tileDist + 10) / 30)
	
		wyvern.animate(MAGIC_ATTACK)
		
		wyvern.world().spawnProjectile(wyvern, target, 136, 90, 45, 10 * tileDist, 55, 14, 0)
		target.graphic(137, 80, 30 * tileDist + 3)
		
		if (EntityCombat.attemptHit(wyvern, target, CombatStyle.MAGIC)) {
			target.hit(wyvern, wyvern.world().random(25), delay).combatStyle(CombatStyle.MAGIC)
		} else {
			target.hit(wyvern, 0, delay).combatStyle(CombatStyle.MAGIC)
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, wyvern)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		wyvern.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(wyvern, wyvern.combatInfo().attackspeed)
	}
	
	@Suspendable fun doTailWhip(wyvern: Npc, target: Entity) {
		
		wyvern.animate(wyvern.attackAnimation())
		
		if (EntityCombat.attemptHit(wyvern, target, CombatStyle.MELEE)) {
			target.hit(wyvern, wyvern.world().random(wyvern.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(wyvern, 0, 1).combatStyle(CombatStyle.MELEE)
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, wyvern)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		wyvern.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(wyvern, wyvern.combatInfo().attackspeed)
	}
	
	@Suspendable fun doMelee(wyvern: Npc, target: Entity) {
	
		wyvern.animate(ALT_MELEE_ATTACK)
		
		if (EntityCombat.attemptHit(wyvern, target, CombatStyle.MELEE)) {
			target.hit(wyvern, wyvern.world().random(wyvern.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(wyvern, 0, 1).combatStyle(CombatStyle.MELEE)
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, wyvern)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		wyvern.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(wyvern, wyvern.combatInfo().attackspeed)
	}
}