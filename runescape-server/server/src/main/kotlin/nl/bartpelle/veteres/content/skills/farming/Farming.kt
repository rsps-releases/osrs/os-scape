package nl.bartpelle.veteres.content.skills.farming

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.skills.farming.patch.Allotments
import nl.bartpelle.veteres.content.skills.farming.patch.Farmbit
import nl.bartpelle.veteres.content.skills.farming.patch.Flowers
import nl.bartpelle.veteres.content.skills.farming.patch.Herbs
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.entity.player.Varps
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Bart on 4/24/2016.
 *
 * Base stub for farming to handle misc. stuff in it.
 */
object Farming {
	
	const val PLANT_CURE = 6036
	const val SPADE = 952
	const val RAKE = 5341
	const val SEED_DIBBER = 5343
	
	val WATERING_CANS = 5333..5340
	val EMPTY_CAN = 5331
	
	// Seed lookup map
	@JvmStatic val ALL_SEEDS: Set<Int> = HashSet<Int>().apply {
		Allotments.Data.values().forEach { data -> add(data.seed.id()) }
		Herbs.Data.values().forEach { data -> add(data.seed.id()) }
		Flowers.Data.values().forEach { data -> add(data.seed.id()) }
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Register all the timers and triggers
		Farmbit.ALLOTMENTS.forEach { fb ->
			r.onObject(fb.obj) @Suspendable {
				when (it.interactionOption()) {
					1 -> Allotments.interactAllotment(it, fb)
					4 -> displayGuide(it, 21, 0)
				}
			}
			r.onItemOnObject(fb.obj) @Suspendable { Allotments.itemOnAllotment(it, fb) }
			
			// Register the growth timer
			r.onTimer(fb.timer) {
				Allotments.grow(it, fb)
			}
		}
		Farmbit.HERBS.forEach { fb ->
			r.onObject(fb.obj) @Suspendable {
				when (it.interactionOption()) {
					1 -> Herbs.interactHerb(it, fb)
					4 -> displayGuide(it, 21, 6)
				}
			}
			
			r.onItemOnObject(fb.obj) @Suspendable { Herbs.itemOnHerbPatch(it, fb) }
			
			// Register the growth timer
			r.onTimer(fb.timer) {
				Herbs.grow(it, fb)
			}
		}
		
		Farmbit.FLOWERS.forEach { fb ->
			r.onObject(fb.obj) @Suspendable {
				when (it.interactionOption()) {
					1 -> Flowers.interactFlower(it, fb)
					4 -> displayGuide(it, 21, 5)
				}
			}
			
			//Items on object
			r.onItemOnObject(fb.obj) @Suspendable { Flowers.itemOnFlowerPatch(it, fb) }
			
			// Register the growth timer
			r.onTimer(fb.timer) { Flowers.grow(it, fb) }
		}
		
		// Register the unique farmbit regions
		val uniqueRegions = Farmbit.values().map { fb -> fb.visibleRegion }.toHashSet()
		for (region in uniqueRegions) {
			r.onRegionEnter(region) {
				synchRegion(it.player())
			}
		}
	}
	
	@Suspendable fun displayGuide(it: Script, skill: Int, varbit: Int) {
		it.openInterface(214)
		it.player().varps().varbit(4371, skill)
		it.player().varps().varbit(4372, varbit)
	}
	
	@Suspendable fun rakeWeeds(it: Script, varbit: Farmbit, name: String) {
		if (!it.player().inventory().contains(5341)) {
			it.player().message("You need a rake to weed a farming patch.")
			return
		}
		
		val value = it.player().varps().farmbit(varbit)
		
		// Does this patch need weeding?
		if (value > 2) {
			it.player().message("This $name doesn't need weeding right now.")
			return
		}
		
		it.delay(1)
		
		while (it.player().varps().farmbit(varbit) < 3) {
			it.player().animate(2273)
			it.delay(3)
			
			// Did we succeed?
			if (it.player().world().rollDie(255, 100 + it.player().skills().level(Skills.FARMING))) {
				it.player().varps().farmbit(varbit, it.player().varps().farmbit(varbit) + 1)
				it.addXp(Skills.FARMING, 4.0) // Yeah, you get 4 xp... #worth
				it.player().inventory().add(Item(6055), true) // Free weed :)
				
				Farming.setTimer(it.player(), varbit.timer)
			}
		}
		
		it.animate(-1)
	}
	
	fun synchRegion(player: Player) {
		val region = player.tile().region()
		
		Farmbit.values().forEach { farmbit ->
			if (region == farmbit.visibleRegion) {
				player.varps().varp(farmbit.varp, player.attribOr<Int>(farmbit.attrib, 0))
				return
			}
		}
	}
	
	fun addTimer(player: Player, timerKey: TimerKey?, time: Int = 1000) {
		player.timers().addOrSet(timerKey, time)
	}
	
	fun setTimer(player: Player, timerKey: TimerKey?, time: Int = 1000) {
		player.timers().register(timerKey, time)
	}
	
	fun Varps.farmbit(farmbit: Farmbit): Int {
		val value = player().attribOr<Int>(farmbit.attrib, 0)
		return value.shr(farmbit.bitStart).and(0xFF)
	}
	
	fun Varps.farmbit(farmbit: Farmbit, v: Int) {
		val value = player().attribOr<Int>(farmbit.attrib, 0)
		val newval = value.and(0xFF.shl(farmbit.bitStart).inv()).or(v.shl(farmbit.bitStart))
		player().putattrib(farmbit.attrib, newval)
		
		// Synch that one. Only updates if in the correct region.
		synch(player(), farmbit)
	}
	
	fun synch(player: Player, farmbit: Farmbit, force: Boolean = false) {
		if (force || player.tile().region() == farmbit.visibleRegion) {
			player.varps().varp(farmbit.varp, player.attribOr<Int>(farmbit.attrib, 0))
		}
	}
	
}