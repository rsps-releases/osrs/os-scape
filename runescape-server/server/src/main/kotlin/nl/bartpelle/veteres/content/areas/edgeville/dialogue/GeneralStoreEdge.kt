package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/29/2015.
 */

object GeneralStoreEdge {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (keeper in intArrayOf(514, 515)) {
			r.onNpcOption1(keeper) @Suspendable {
				it.chatNpc("Can I help you at all?", keeper, 567)
				when (it.options("Yes please. What are you selling?", /*"Supplies please.", */"No thanks.")) {
					1 -> {
						if (it.player().ironMode() != IronMode.NONE) {
							it.player().world().shop(19).display(it.player())
						} else {
							it.player().world().shop(6).display(it.player())
						}
					}
					/*2 -> {
						if (it.player().ironMode() != IronMode.NONE) {
							it.player().message("Iron men cannot access the supplies shop.")
						} else {
							it.player().world().shop(28).display(it.player())
						}
					}*/
					2 -> {
						it.chatPlayer("No thanks.", 588)
					}
				}
			}
			r.onNpcOption2(keeper) {
				if (it.player().ironMode() != IronMode.NONE) {
					it.player().world().shop(19).display(it.player())
				} else {
					it.player().world().shop(6).display(it.player())
				}
			}
			
			r.onNpcOption3(keeper) {
				// Supplies shop - removed in Macks shop overhall
				/*if (it.player().ironMode() != IronMode.NONE) {
					it.player().message("Iron men cannot access the supplies shop.")
				} else {
					it.player().world().shop(28).display(it.player())
				}*/
			}
		}
	}
	
}
