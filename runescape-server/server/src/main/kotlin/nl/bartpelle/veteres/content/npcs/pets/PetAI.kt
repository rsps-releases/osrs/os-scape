package nl.bartpelle.veteres.content.npcs.pets

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.itemOptions
import nl.bartpelle.veteres.content.itemUsedSlot
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Tuple
import nl.bartpelle.veteres.util.Varp
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

/**
 * Created by Bart on 2/17/2016.
 */

object PetAI {
	
	@JvmStatic val logger: Logger = LogManager.getLogger(PetAI::class.java)
	
	val SMART_AI = true
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		it.clearContext() // Avoid interruption!
		val player: Player = npc.attrib<Tuple<Int, Player>>(AttributeKey.OWNING_PLAYER)?.second() ?: return@s
		
		var lastTile: Tile? = null
		while (!player.finished() && !npc.finished() && !npc.dead()) {
			if (lastTile == null || lastTile != player.tile()) {
				lastTile = player.tile()
				
				if (SMART_AI) {
					if (player.tile().distance(npc.tile()) > 6) {
						npc.teleport(player.tile())
					}
					
					npc.walkTo(player, PathQueue.StepType.REGULAR)
					npc.face(player)
				}
			}
			
			it.delay(1)
		}
		
		npc.world().unregisterNpc(npc)
	}
	
	fun spawnPet(player: Player, pet: Pet, removeItem: Boolean = true) {
		if (player.pet() != null && !player.pet()!!.finished()) {
			player.message("You already have a follower!")
			return
		}
		
		if (!removeItem || player.inventory().remove(Item(pet.item), true).success()) {
			// Set varbit to make sure we're unlocking it at all cost
			if (pet.varbit != -1) {
				player.varps().varbit(pet.varbit, 1)
			}
			
			val petNpc = Npc(pet.npc, player.world(), player.tile())
			petNpc.walkRadius(-1) // Allow walking all across the map
			petNpc.putattrib(AttributeKey.OWNING_PLAYER, Tuple(player.id(), player))
			petNpc.putattrib(AttributeKey.PET_TYPE, pet)
			//petNpc.sync().combatLevel(420)
			
			player.world().registerNpc(petNpc)
			player.putattrib(AttributeKey.ACTIVE_PET, petNpc)
			player.putattrib(AttributeKey.ACTIVE_PET_ITEM_ID, pet.item)
			// A special varp that allows the "Transmog" right-click option to become available on this local Npc.
			player.varps().varp(Varp.MY_PET_NPC_INDEX, petNpc.index())
			petNpc.executeScript(script)
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		Pet.values().forEach { pet ->
			// Register item dropping to make the pets appear..
			r.onItemOption5(pet.item) {
				spawnPet(it.player(), pet)
			}

			// Dropping broken placeholders!
			if (pet.phold > 0) {
				r.onItemOption5(pet.phold) {
					val def = Item(pet.phold).definition(it.player().world())
					// That's a placeholder retard.
					if (def.placeheld > 0 && def.pheld14401 == 14401) {
						// Resolve the placeholder to a real pet.
						val realPet: Pet? = Pet.values().filter { p -> p.phold == pet.phold }.firstOrNull()
						if (realPet != null) {
							val slot = it.itemUsedSlot()
							it.player().inventory().remove(Item(pet.phold, Integer.MAX_VALUE), true) // Remove all broken
							it.player().inventory().add(Item(pet.item), true, slot) // Give fixed
							spawnPet(it.player(), realPet) // Must have fixed in inventory to spawn
						} else {
							logger.error("Null pet resolved from placeholder {}", pet.phold)
						}
					}
				}
			}

			if (pet.name.toLowerCase().contains("hell") && pet.item != 13247) {
				r.onNpcOption1(pet.npc) {
					collectPet(it.player())
				}
			} else {

				// Add the option to pick up your npc :)
				r.onNpcOption2(pet.npc) @Suspendable {
					if (pet == Pet.KALPHITE_PRINCESS || pet == Pet.KALPHITE_PRINCESS_2 || pet == Pet.VETION_JR_ORANGE || pet == Pet.VETION_JR_PURPLE)
						metamorph(it.player())
					else if (pet == Pet.OLMLET)
						metamorphOlmlet(it)
					else
						collectPet(it.player())
				}

				r.onNpcOption3(pet.npc) {
					if (pet == Pet.PET_SNAKELING_BLUE || pet == Pet.PET_SNAKELING_GREEN || pet == Pet.PET_SNAKELING_RED || pet == Pet.BABY_CHINCHOMPA_YELLOW
							|| pet == Pet.BABY_CHINCHOMPA_RED || pet == Pet.BABY_CHINCHOMPA_GREY || pet == Pet.BABY_CHINCHOMPA_BLACK)
						metamorph(it.player())
					else
						collectPet(it.player())
				}

				r.onItemOnPlayer(pet.item, s@ @Suspendable {
					val player = it.player()
					val opp: Player = it.target() as Player
					if (player.privilege().eligibleTo(Privilege.ADMIN)) {
						val petname = Item(pet.item).name(player.world())
						if (it.optionsTitled("Revoke pet: '$petname' from ${opp.name()}?", "Yes", "No.") == 1) {
							if (opp.varps().varbitBool(pet.varbit)) {
								collectPet(opp)
								var deleted: Int = opp.inventory().remove(Item(pet.item), true).completed()
								deleted += opp.inventory().remove(Item(pet.metaItemId), true).completed()
								deleted += opp.bank().remove(Item(pet.item), true).completed()
								deleted += opp.bank().remove(Item(pet.metaItemId), true).completed()
								if (deleted > 0) {
									opp.varps().varbit(pet.varbit, 0)
									it.message("${opp.name()} has had a pet revoked: $petname.".col("FF0000"))
									opp.message("${player.name()} has revoked a pet from you: $petname.".col("FF0000"))
									logger.info("${player.name()} revoked pet from ${opp.name()}: '$petname'".col("FF0000"))
								} else {
									it.message("${opp.name()} did not have pet id: ${pet.item} ($petname) so it couldn't be revoked.".col("FF0000"))
								}
							} else {
								it.message("${opp.name()} did not have the pet unlocked: $petname.".col("FF0000"))
							}
						}
					} else {
						player.message("Pets cannot be gifted.")
					}
				})
			}
		}
	}
	
	// Does not use pnpc, it fully changes the npc type and definition.
	fun metamorph(player: Player, intoPet: Pet? = null) {
		val pet = player.pet()
		if (pet != null) {
			val petEnum: Pet = Pet.getByNpc(if (pet.sync().transmog() > 0) pet.sync().transmog() else pet.id()) ?: return
			var trans = intoPet?.item ?: petEnum.metaItemId
			// 1/10k chance that metamorph turns a chin into a golden chin.
			if ((petEnum == Pet.BABY_CHINCHOMPA_BLACK || petEnum == Pet.BABY_CHINCHOMPA_GREY || petEnum == Pet.BABY_CHINCHOMPA_RED) && player.world().rollDie(10000, 1)) {
				trans = Pet.BABY_CHINCHOMPA_YELLOW.item
			}
			// Despawn and spawn alternative pet id
			if (trans > -1) {
				pet.sync().transmog(Pet.Companion.getPetByItem(trans)!!.npc)
				pet.putattrib(AttributeKey.PET_TYPE, Pet.Companion.getPetByItem(trans)!!)
			}
		}
	}

	@Suspendable fun metamorphOlmlet(it: Script) {
		val opt = it.itemOptions(Item(22376), Item(22378), Item(22380), Item(22382), Item(22384))
	}
	
	fun Player.pet(): Npc? {
		val pet: Npc? = this.attribOr(AttributeKey.ACTIVE_PET, null)
		
		// Only consider the pet valid if it's non-null and isn't finished.
		if (pet != null && !pet.finished()) {
			return pet
		}
		
		return null
	}
	
	fun Npc.petType(): Pet? {
		return this.attribOr(AttributeKey.PET_TYPE, null)
	}
	
	@JvmStatic fun hasUnlocked(player: Player, pet: Pet): Boolean {
		if (pet.varbit == -1) {
			return false
		}
		
		return player.varps().varbit(pet.varbit) != 0
	}
	
	@JvmStatic fun collectPet(player: Player) {
		val pet = player.pet()
		
		if (pet != null) {
			player.faceTile(pet.tile())
			val petType = pet.petType()!!
			
			// Try to add it to either your inventory or your bank.
			if (player.inventory().add(Item(petType.item), false).failed()) {
				player.bank().add(Item(petType.item), false)
				player.message("Since your inventory was full, your ${Item(petType.item).name(player.world())} was deposited into your bank.".col("FF0000"))
			}
			player.clearattrib(AttributeKey.ACTIVE_PET_ITEM_ID)
			player.clearattrib(AttributeKey.ACTIVE_PET)
			player.world().unregisterNpc(pet)
		}
	}
	
	@JvmStatic fun despawnOnLogout(player: Player) {
		val pet = player.pet()
		
		if (pet != null) {
			player.faceTile(pet.tile())
			val petType = pet.petType()!!
			player.putattrib(AttributeKey.ACTIVE_PET_ITEM_ID, petType.item)
			player.clearattrib(AttributeKey.ACTIVE_PET)
			player.world().unregisterNpc(pet)
		}
	}
	
	@JvmStatic fun spawnOnLogin(player: Player) {
		val petItemId = player.attribOr<Int>(AttributeKey.ACTIVE_PET_ITEM_ID, -1)
		if (petItemId != -1) {
			var petType: Pet? = Pet.getPetByItem(petItemId)
			val def = Item(petItemId).definition(player.world())
			// That's a placeholder retard.
			if (def.placeheld > 0 && def.pheld14401 == 14401) {
				// Resolve the placeholder to a real pet.
				val realPet: Pet? = Pet.values().filter { p -> p.phold == petItemId }.firstOrNull()
				if (realPet != null)
					petType = realPet
			}
			if (petType == null) {
				// Notify we're fucked
				// Stop making us look bad u nigger
				// player.world().broadcast("<col=FF0000>BUG! Please tell a developer! Invalid pet item: "+petItemId, Privilege.MODERATOR)
			} else {
				spawnPet(player, petType, false)
			}
		}
	}
	
}