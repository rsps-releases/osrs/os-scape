package nl.bartpelle.veteres.content.areas.global

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/6/2015.
 */

object WheatPicking {
	
	val WHEAT = intArrayOf(15506, 15507)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (wheat in WHEAT) {
			r.onObject(wheat) @Suspendable {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				
				it.player().lock()
				it.delay(1)
				
				if (!it.player().inventory().full()) {
					it.animate(827)
					it.sound(2581)
					it.player().inventory().add(Item(1947), false)
					it.message("You pick some grain.")
					it.runGlobal(it.player().world()) @Suspendable { s ->
						s.ctx<World>().removeObj(obj)
						s.delay(50) // Half a minute
						s.ctx<World>().spawnObj(obj)
					}
				} else {
					it.message("You don't have room for grain.")
				}
				it.player().unlock()
			}
		}
	}
}