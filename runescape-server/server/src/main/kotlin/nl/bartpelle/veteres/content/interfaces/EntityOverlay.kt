package nl.bartpelle.veteres.content.interfaces

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */
object EntityOverlay {
	
	private const val INTERFACE_ID = 596

	@JvmStatic @ScriptMain
	fun register(sr: ScriptRepository) {
		sr.onTimer(TimerKey.IN_COMBAT, s@ {
            if (it.context is Npc) return@s

            dispose(it.player())
		})
	}
	
	@JvmStatic
	fun update(attacker: Player, target: Entity?) {
		
		if (target != null) {
			val dirty = flagDirty(attacker, target)
			val hp = target.hp()
			val maxHp = target.maxHp()

			if (dirty) {
				val targetName = when (target) {
					is Player -> {
						var icon = target.privilege().ordinal
						if (icon == 0 || target.seniorModerator()) {
							icon = target.calculateBaseIcon()
						}

						target.name()
					}
					is Npc -> target.def().name +" (Cmb - ${target.def().combatlevel})"
					else -> "???"
				}

				attacker.interfaces().text(INTERFACE_ID, 4, targetName)
				attacker.interfaces().sendWidgetOn(INTERFACE_ID, Interfaces.InterSwitches.C)
			}

			//Check to lessen stress on network. No need to update widget if nothing changed.
			if (attacker.varps().varbit(Varbit.TARGET_ENTITY_HP) != target.hp()) {
				sendHpVarb(attacker, hp, maxHp)
			}
		}
	}
	
	/**
	 * A flag checking if the overlay is enabled.
	 */
	@JvmStatic
	fun enabled(entity: Entity?): Boolean {
		entity ?: return false
		if (entity.isNpc) return false

		return (entity.varps().varbit(Varbit.HP_OVERLAY_TOGGLED) == 1)
	}
	
	/**
	 * Conditions which we flag the interface as dirty and require a full update.
	 */
	private fun flagDirty(attacker: Player, target: Entity?): Boolean{
		val targetUid = when (target) {
			is Player -> target.id()
			is Npc -> target.index()
			else -> -2
		}
		
		return attacker.varps().varbit(Varbit.HP_OVERLAY_ENTITY_UID) != targetUid
	}
	
	/**
	 * Sends the varbit id & value with the specified current hp and max hp values of the target, if any.
	 * Default the max hp varbit value to 1 to prevent arithmetic exception (can't divide by 0!)
	 */
	private fun sendHpVarb(player: Player, current: Int = 0, maxHp: Int = 1) {
		player.varps().varbit(Varbit.TARGET_ENTITY_HP, current)
		player.varps().varbit(Varbit.TARGET_ENTITY_MAX_HP, maxHp)
	}
	
	/**
	 * Sets the info on disposal.
	 */
	private fun dispose(player: Player?) {
        if (player != null) {
            player.interfaces().closeById(INTERFACE_ID)
            player.varps().varbit(Varbit.HP_OVERLAY_ENTITY_UID, -1)
            player.interfaces().text(INTERFACE_ID, 4, "???")
            sendHpVarb(player)
        }
	}
}