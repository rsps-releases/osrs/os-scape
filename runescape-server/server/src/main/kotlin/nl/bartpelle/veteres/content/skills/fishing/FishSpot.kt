package nl.bartpelle.veteres.content.skills.fishing

/**
 * Created by Bart on 12/1/2015.
 */
enum class FishSpot(val id: Int, val types: Array<FishSpotType>) {
	NET_BAIT(1518, arrayOf(FishSpotType.NET, FishSpotType.BAIT)),
	CAGE_HARPOON(1519, arrayOf(FishSpotType.CAGE, FishSpotType.HARPOON)),
	NET_HARPOON(1520, arrayOf(FishSpotType.BIG_NET, FishSpotType.HARPOON_SHARK)),
	FLY_FISHING(1527, arrayOf(FishSpotType.FLY, FishSpotType.BAIT)),
	DARK_CRAB(1536, arrayOf(FishSpotType.DARK_CRAB)),
	MONKFISH(4316, arrayOf(FishSpotType.MONKFISH, FishSpotType.HARPOON_SHARK)),
	ANGLERFISH(6825, arrayOf(FishSpotType.ANGLERGISH)),
	INFERNAL_EEL(7676, arrayOf(FishSpotType.INFERNAL_EEL, FishSpotType.INFERNAL_EEL)),
}