package nl.bartpelle.veteres.content.skills.crafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/18/2016.
 */

object GlassBlowing {
	
	val GLASSBLOWING_PIPE = 1785
	val MOLTEN_GLASS = 1775
	val INTERFACE = 542
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Handle using blowpipe -> molten glass
		r.onItemOnItem(GLASSBLOWING_PIPE, MOLTEN_GLASS) @Suspendable { it.player().interfaces().sendMain(INTERFACE) }
		
		//Handle interface options: Beer glass
		r.onButton(INTERFACE, 120) @Suspendable { craftItem(it, 1, 17.5, 1919, "a beer glass", 1) }
		r.onButton(INTERFACE, 119) @Suspendable { craftItem(it, 1, 17.5, 1919, "a beer glass", 5) }
		r.onButton(INTERFACE, 118) @Suspendable { craftItem(it, 1, 17.5, 1919, "a beer glass", 10) }
		r.onButton(INTERFACE, 117) @Suspendable {
			craftItem(it, 1, 17.5, 1919, "a beer glass",
					amt = it.inputInteger("Enter amount:"))
		}
		
		//Candle lantern
		r.onButton(INTERFACE, 124) @Suspendable { craftItem(it, 4, 19.0, 4527, "a candle lantern", 1) }
		r.onButton(INTERFACE, 123) @Suspendable { craftItem(it, 4, 19.0, 4527, "a candle lantern", 5) }
		r.onButton(INTERFACE, 122) @Suspendable { craftItem(it, 4, 19.0, 4527, "a candle lantern", 10) }
		r.onButton(INTERFACE, 121) @Suspendable {
			craftItem(it, 4, 19.0, 4527, "a candle lantern",
					amt = it.inputInteger("Enter amount:"))
		}
		
		//Oil lamp
		r.onButton(INTERFACE, 128) @Suspendable { craftItem(it, 12, 25.0, 4522, "an oil lamp", 1) }
		r.onButton(INTERFACE, 127) @Suspendable { craftItem(it, 12, 25.0, 4522, "an oil lamp", 5) }
		r.onButton(INTERFACE, 126) @Suspendable { craftItem(it, 12, 25.0, 4522, "an oil lamp", 10) }
		r.onButton(INTERFACE, 125) @Suspendable {
			craftItem(it, 12, 25.0, 4522, "an oil lamp",
					amt = it.inputInteger("Enter amount:"))
		}
		
		//Vial
		r.onButton(INTERFACE, 112) @Suspendable { craftItem(it, 33, 35.0, 229, "a vial", 1) }
		r.onButton(INTERFACE, 111) @Suspendable { craftItem(it, 33, 35.0, 229, "a vial", 5) }
		r.onButton(INTERFACE, 110) @Suspendable { craftItem(it, 33, 35.0, 229, "a vial", 10) }
		r.onButton(INTERFACE, 109) @Suspendable {
			craftItem(it, 33, 35.0, 229, "a vial",
					amt = it.inputInteger("Enter amount:"))
		}
		
		//Fish bowl
		r.onButton(INTERFACE, 136) @Suspendable { craftItem(it, 42, 42.5, 6667, "a fish bowl", 1) }
		r.onButton(INTERFACE, 135) @Suspendable { craftItem(it, 42, 42.5, 6667, "a fish bowl", 5) }
		r.onButton(INTERFACE, 134) @Suspendable { craftItem(it, 42, 42.5, 6667, "a fish bowl", 10) }
		r.onButton(INTERFACE, 133) @Suspendable {
			craftItem(it, 42, 42.5, 6667, "a fish bowl",
					amt = it.inputInteger("Enter amount:"))
		}
		
		//Orb
		r.onButton(INTERFACE, 116) @Suspendable { craftItem(it, 46, 52.5, 567, "an orb", 1) }
		r.onButton(INTERFACE, 115) @Suspendable { craftItem(it, 46, 52.5, 567, "an orb", 5) }
		r.onButton(INTERFACE, 114) @Suspendable { craftItem(it, 46, 52.5, 567, "an orb", 10) }
		r.onButton(INTERFACE, 113) @Suspendable {
			craftItem(it, 46, 52.5, 567, "an orb",
					amt = it.inputInteger("Enter amount:"))
		}
		
		//Lantern lens
		r.onButton(INTERFACE, 132) @Suspendable { craftItem(it, 49, 55.0, 4542, "a lantern lens", 1) }
		r.onButton(INTERFACE, 131) @Suspendable { craftItem(it, 49, 55.0, 4542, "a lantern lens", 5) }
		r.onButton(INTERFACE, 130) @Suspendable { craftItem(it, 49, 55.0, 4542, "a lantern lens", 10) }
		r.onButton(INTERFACE, 129) @Suspendable {
			craftItem(it, 49, 55.0, 4542, "a lantern lens",
					amt = it.inputInteger("Enter amount:"))
		}
		
		//Light orb
		r.onButton(INTERFACE, 144) @Suspendable { craftItem(it, 87, 70.0, 10973, "a light orb", 1) }
		r.onButton(INTERFACE, 143) @Suspendable { craftItem(it, 87, 70.0, 10973, "a light orb", 5) }
		r.onButton(INTERFACE, 142) @Suspendable { craftItem(it, 87, 70.0, 10973, "a light orb", 10) }
		r.onButton(INTERFACE, 141) @Suspendable {
			craftItem(it, 87, 70.0, 10973, "a light orb",
					amt = it.inputInteger("Enter amount:"))
		}
	}
	
	@Suspendable fun craftItem(it: Script, level_req: Int, experience: Double, item_id: Int, item_name: String, amt: Int) {
		it.player().stopActions(true)
		
		var amt = amt
		it.player().interfaces().closeById(INTERFACE)
		
		if (it.player().skills().level(Skills.CRAFTING) < level_req) {
			it.messagebox("You need a Crafting skill of $level_req to make $item_name}")
			return
		}
		
		if (!it.player().inventory().contains(Item(MOLTEN_GLASS, 1))) {
			it.message("You need molten glass to craft this item!")
			return
		}
		
		while (amt-- > 0) {
			if (it.player().inventory().remove(Item(MOLTEN_GLASS), true).failed()) {
				return
			}
			it.player().animate(884)
			it.player().inventory().add(Item(item_id), true)
			it.addXp(Skills.CRAFTING, experience)
			it.delay(2)
		}
	}
}
