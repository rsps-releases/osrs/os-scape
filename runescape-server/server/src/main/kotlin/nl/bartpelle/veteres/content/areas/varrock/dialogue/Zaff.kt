package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/30/2015.
 */

object Zaff {
	
	val ZAFF = 532
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ZAFF) @Suspendable {
			it.chatNpc("Would you like to buy or sell some magic supplies? Or is there something else you need?", ZAFF, 589)
			when (it.options("Yes, please.", "No, thank you.")) {
				1 -> {
					shops(it)
				}
				2 -> {
					it.chatPlayer("No, thank you.")
					it.chatNpc("Well, 'stick' you head in again if you change your mind.", ZAFF)
				}
			}
		}
		r.onNpcOption2(ZAFF) {
			shops(it)
		}
	}

	fun shops(it: Script) {
		if (!it.player().world().realm().isPVP) {
			if (it.player().ironMode() != IronMode.NONE) {
				it.player().world().shop(14).display(it.player())
			} else {
				it.player().world().shop(11).display(it.player())
			}
		}
	}
	
}
