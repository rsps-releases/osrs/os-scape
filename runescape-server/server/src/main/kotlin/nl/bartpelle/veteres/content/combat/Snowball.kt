package nl.bartpelle.veteres.content.combat

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.steroids.RangeStepSupplier
import nl.bartpelle.veteres.net.message.game.command.SetPlayerOption
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.EquipmentInfo
import nl.bartpelle.veteres.util.Varp
import java.lang.ref.WeakReference

/**
 * Created by Jak on 31/10/2016.
 * @author Mack - Modifications
 */
object Snowball {

    private const val SNOWBALL = 10501

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onPlayerOption5({
            if (it.player().equipment()[3] != null && it.player().equipment()[3].id() == SNOWBALL) {
                script.invoke(it)
            }
        })
    }
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		
		val ref: WeakReference<Entity> = it.player().attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: WeakReference<Entity>(null)
		val target: Entity? = ref.get() ?: return@s
		val player: Player = it.player()
		player.face(target)

        if (target != null && target.isNpc) {
            player.message("I don't think they'd find this too amusing...")
            return@s
        }

		while (target != null && !player.dead()) {
			
			player.equipment()[3] ?: break // Ran out of snowballs

			//PlayerCombat.unfreeze_when_out_of_range(player)
			// Begin by clearing the path queue
			player.pathQueue().clear()
			
			player.face(target)
			player.varps().varp(Varp.ATTACK_PRIORITY_PID, target.index())
			
			val supplier = RangeStepSupplier(player, target, 9)
			if (!supplier.reached(player.world()) && !player.frozen() && !player.stunned()) {
				// Are we in range distance?
				player.walkTo(target, PathQueue.StepType.REGULAR)
				player.pathQueue().trimToSize(if (player.pathQueue().running()) 2 else 1)
			}
			
			// Are we stunned?
			if (player.stunned()) {
				break
			}
			
			// Can we shoot?
			if (supplier.reached(player.world())) {
				
				// Wait for cool down
				if (!player.timers().has(TimerKey.COMBAT_ATTACK)) {
					
					// Throw
					player.timers()[TimerKey.COMBAT_ATTACK] = 4
					player.animate(5063)
					player.graphic(862, 0, 50)
					if (target.isPlayer)
						target.animate(EquipmentInfo.blockAnimationFor(target as Player), 50)
					player.equipment().remove(Item(10501, 1), true, 3)
					if (player.equipment().count(10501) < 1) {
						player.write(SetPlayerOption(5, false, "null"))
					}
					player.world().spawnProjectile(player, target, 861, 30, 22, 62, 50, 15, 11)
				}
			}
			it.delay(1)
		}
	}
}