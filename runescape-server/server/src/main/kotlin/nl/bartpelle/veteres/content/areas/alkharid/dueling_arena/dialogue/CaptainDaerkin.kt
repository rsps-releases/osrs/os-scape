package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object CaptainDaerkin {
	
	val CAPTAIN_DAERKIN = 2570
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(CAPTAIN_DAERKIN) @Suspendable {
			it.chatNpc("Hello old chap.", CAPTAIN_DAERKIN, 567)
			it.chatPlayer("What are you doing here? Shouldn't you be looking<br>after your glider?", 589)
			it.chatNpc("I'm pretty much retired these days old fellow. My test<br>piloting days are over. I'm just relaxing here and<br>" +
					"enjoying the primal clash between man and man.", CAPTAIN_DAERKIN, 569)
			it.chatPlayer("You're watching the duels then. Are you going to<br>challenge someone yourself?", 589)
			it.chatNpc("I do find the duels entertaining to watch, but I suspect<br>that actually being involved would be a lot less fun for<br>" +
					"me. I'm a lover, not a fighter!", CAPTAIN_DAERKIN, 569)
			it.chatPlayer("Errm, I suppose you are.", 588)
		}
	}
	
}
