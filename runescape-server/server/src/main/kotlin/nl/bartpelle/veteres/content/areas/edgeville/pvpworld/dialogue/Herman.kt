package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Bob
import nl.bartpelle.veteres.content.itemUsedId
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 11/29/2015.
 * Broken stuff added by Jak 24/1/2016
 */

object Herman {
	
	val HERMAN = 4300
	val BARROWS_SHOP = 5
	
	var BROKENDATA_REPO: ArrayList<BarrowsData> = ArrayList()
	
	data class BarrowsData(val fixed: Int, val broken: Int, val name: String, val equipSlot: Int)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		r.onNpcOption2(HERMAN) @Suspendable {
			if (it.player().world().realm().isPVP) {
				
				if (!hasBroken(it.player())) {
					it.chatNpc("You don't have any broken barrows pieces.", Bob.BOB)
				} else {
					fixall(it, Bob.BOB)
				}
			} else {
				it.player().message("You can only repair barrows on the PK server.")
			}
		}
		
		r.onNpcOption1(HERMAN, s@ @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(BARROWS_SHOP).display(it.player())
			} else {
				it.player().message("You can only view this shop on the PK server.")
			}
		})
		
		r.onNpcOption3(Bob.BOB, @Suspendable {
			if (it.player().world().realm().isPVP) {
				
				if (!hasBroken(it.player())) {
					it.chatNpc("You don't have any broken barrows pieces.", Bob.BOB)
				} else {
					fixall(it, Bob.BOB)
				}
			}
		})
		
		// Item on herman & bob
		for (i in arrayOf(HERMAN, Bob.BOB)) {
			r.onItemOnNpc(i, s@ @Suspendable {
				var valid = false
				for (data in BROKENDATA_REPO) {
					if (data.broken == it.itemUsedId()) {
						valid = true
						break
					}
				}
				if (!valid) {
					it.chatNpc("I can't repair that.", it.targetNpc()!!.id())
				} else {
					val count = brokenCount(it.player())
					var broken: BarrowsData? = null
					for (data in BROKENDATA_REPO) {
						if (data.broken == it.itemUsedId()) {
							broken = data
							break
						}
					}
					broken ?: return@s
					val cost = if (it.player().world().realm().isPVP) costToFixPVP(broken) else costToFixEco(broken)
					val currency = if (it.player().world().realm().isPVP) 13307 else 995
					val currencyName = if (it.player().world().realm().isPVP) "blood money" else "coins"
					val name = Item(broken.fixed).definition(it.player().world()).name
					val totalcost = costToFixAll(it.player())
					if (count == 1) {
						if (it.options("Pay $cost $currencyName to repair $name?", "Never mind") == 1) {
							repair(it, broken, currency, currencyName)
						}
					} else {
						when (it.options("Repair $name for $cost $currencyName", "Repair all for $totalcost $currencyName.", "Never mind.")) {
							1 -> {
								repair(it, broken, currency, currencyName)
							}
							2 -> fixall(it, Bob.BOB)
						}
					}
				}
			})
		}
		
		// Register all info
		BROKENDATA_REPO.clear()
		BROKENDATA_REPO.add(BarrowsData(4753, 4980, "Verac helm", EquipSlot.HEAD))
		BROKENDATA_REPO.add(BarrowsData(4755, 4986, "Verac flail", EquipSlot.WEAPON))
		BROKENDATA_REPO.add(BarrowsData(4757, 4992, "Verac brassard", EquipSlot.BODY))
		BROKENDATA_REPO.add(BarrowsData(4759, 4998, "Verac plateskirt", EquipSlot.LEGS))
		BROKENDATA_REPO.add(BarrowsData(4745, 4956, "Torag helm", EquipSlot.HEAD))
		BROKENDATA_REPO.add(BarrowsData(4747, 4962, "Torag hammers", EquipSlot.WEAPON))
		BROKENDATA_REPO.add(BarrowsData(4749, 4968, "Torag platebody", EquipSlot.BODY))
		BROKENDATA_REPO.add(BarrowsData(4751, 4974, "Torag platelegs", EquipSlot.LEGS))
		BROKENDATA_REPO.add(BarrowsData(4716, 4884, "Dharok helm", EquipSlot.HEAD))
		BROKENDATA_REPO.add(BarrowsData(4718, 4890, "Dharok greataxe", EquipSlot.WEAPON))
		BROKENDATA_REPO.add(BarrowsData(4720, 4896, "Dharok platebody", EquipSlot.BODY))
		BROKENDATA_REPO.add(BarrowsData(4722, 4902, "Dharok platelegs", EquipSlot.LEGS))
		BROKENDATA_REPO.add(BarrowsData(4708, 4860, "Ahrim helm", EquipSlot.HEAD))
		BROKENDATA_REPO.add(BarrowsData(4710, 4866, "Ahrim staff", EquipSlot.WEAPON))
		BROKENDATA_REPO.add(BarrowsData(4712, 4872, "Ahrim robetop", EquipSlot.BODY))
		BROKENDATA_REPO.add(BarrowsData(4714, 4878, "Ahrim robeskirt", EquipSlot.LEGS))
		BROKENDATA_REPO.add(BarrowsData(4732, 4932, "Karil coif", EquipSlot.HEAD))
		BROKENDATA_REPO.add(BarrowsData(4734, 4938, "Karil crossbow", EquipSlot.WEAPON))
		BROKENDATA_REPO.add(BarrowsData(4736, 4944, "Karil leathertop", EquipSlot.BODY))
		BROKENDATA_REPO.add(BarrowsData(4738, 4950, "Karil leatherskirt", EquipSlot.LEGS))
		BROKENDATA_REPO.add(BarrowsData(4724, 4908, "Guthan helm", EquipSlot.HEAD))
		BROKENDATA_REPO.add(BarrowsData(4726, 4914, "Guthan warspear", EquipSlot.WEAPON))
		BROKENDATA_REPO.add(BarrowsData(4728, 4920, "Guthan platebody", EquipSlot.BODY))
		BROKENDATA_REPO.add(BarrowsData(4730, 4926, "Guthan chainskirt", EquipSlot.LEGS))
	}
	
	@Suspendable fun repair(it: Script, broken: BarrowsData, currency: Int, currencyName: String) {
		val player = it.player()
		val cost = if (it.player().world().realm().isPVP) costToFixPVP(broken) else costToFixEco(broken)
		if (player.inventory().count(currency) < cost) {
			it.chatNpc("You've not got enough $currencyName left to pay the the remaining repairs.", it.targetNpc()!!.id())
		} else {
			if (player.inventory().remove(Item(currency, cost), false).success()) {
				player.inventory().remove(Item(broken.broken, 1), true)
				player.inventory().add(Item(broken.fixed, 1), true)
			}
		}
	}
	
	private fun costToFixAll(player: Player): Any {
		var total = 0
		for (item in player.inventory()) {
			item ?: continue
			BROKENDATA_REPO.forEach { data ->
				if (item.id() == data.broken) {
					total += if (player.world().realm().isPVP) costToFixPVP(data) else costToFixEco(data)
				}
			}
		}
		return total
	}
	
	private fun costToFixEco(broken: BarrowsData): Int {
		val cost = when (broken.equipSlot) {
			3 -> 100000
			EquipSlot.BODY -> 90000
			EquipSlot.LEGS -> 80000
			else -> 60000 // Weapon
		}
		// Let's make it 50% of 07.. cos we're nice like that.
		return (cost / 2).toInt()
	}
	
	private fun costToFixPVP(broken: BarrowsData): Int {
		// All pieces cost 500 each to fix.
		return 500
	}
	
	@JvmStatic fun brokenForFixed(itemid: Int): Int {
		var broken: Int = itemid
		BROKENDATA_REPO.forEach { data ->
			if (data.fixed == itemid) {
				broken = data.broken
			}
		}
		return broken
	}
	
	fun hasBroken(player: Player): Boolean {
		return brokenCount(player) > 0
	}
	
	fun brokenCount(player: Player): Int {
		var count: Int = 0
		player.inventory().items().forEach { item ->
			if (item != null) {
				BROKENDATA_REPO.forEach { data ->
					if (data.broken == item.id()) {
						count++
					}
				}
			}
		}
		return count
	}
	
	@Suspendable fun fixall(it: Script, npcId: Int) {
		val player = it.player()
		var totalCost = 0
		// If we run out of money, no need to continue this code block.
		var canPay = booleanArrayOf(true)
		val currency = if (it.player().world().realm().isPVP) 13307 else 995
		
		player.inventory().items().filter { i -> i != null }.forEach { item ->
			BROKENDATA_REPO.forEach { data ->
				if (canPay[0]) {
					if (data.broken == item.id()) {
						var cost = if (it.player().world().realm().isPVP) costToFixPVP(data) else costToFixEco(data)
						if (cost > 0) {
							if (player.inventory().count(currency) < cost) {
								canPay[0] = false
							}
						}
						if (canPay[0]) {
							if (player.inventory().remove(Item(currency, cost), false).success()) {
								totalCost += cost
								player.inventory().remove(Item(item.id(), 1), true)
								player.inventory().add(Item(data.fixed, 1), true)
							}
						}
					}
				}
			}
		}
		
		// If true, all pieces were fixed no problem.
		if (canPay[0]) {
			it.chatNpc("I've fixed all the broken pieces for $totalCost ${Item(currency).name(player.world())}.", Bob.BOB)
			
		} else if (totalCost != 0) {
			// Only some were fixed.
			val repaired = totalCost / 100
			it.chatNpc("You couldn't pay for all the repairs. ${repaired} item${if (repaired > 1) "s" else ""} were repaired for $totalCost ${Item(currency).name(player.world())}.", Bob.BOB)
			
		} else if (totalCost == 0) {
			// None were fixed.
			it.chatNpc("You couldn't afford to have any pieces repaired.", Bob.BOB)
		}
	}
	
}