package nl.bartpelle.veteres.services.lottery

import nl.bartpelle.veteres.services.Service

/**
 * Created by Jonathan on 2/8/2017.
 */
interface LottoService : Service {
	
	fun toggle()
	
}
