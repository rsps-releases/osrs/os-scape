package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.lottery.Lottery
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.RSFormatter.formatItemAmount

/**
 * Created by Jonathan on 2/8/2017.
 */
object FinancialAdvisor {
	
	private val warren = 3310
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(warren, s@ @Suspendable {
			if (!Lottery.enabled()) {
				it.chatNpc("The OSScape Lottery is not available at the moment. Please try again later.", warren)
				return@s
			}
			
			it.chatNpc("Good day, how may I help you?", warren)
			when (it.options("What is the OSScape lottery?", "I'd like to deposit money into the lottery.", "I'd like to withdraw money from the lottery.", "Claim Rewards", "Never mind")) {
				1 -> {
					it.chatNpc("The OSScape lottery is very similar to how lotteries work in real life. You put money (and only money) into the lottery, " +
							"and the more you put in the higher your chance of winning.", warren)
					it.chatNpc("The winner will be announced weekly every Saturday at 8:00 PM GMT. You can talk to me whenever you'd like to " +
							"deposit money, withdraw, or claim your winnings.", warren)
				}
				2 -> deposit(it)
				3 -> withdraw(it)
				4 -> claim(it)
			}
		})
		
		r.onNpcOption2(warren, s@ @Suspendable { deposit(it) })
		
		r.onNpcOption3(warren, s@ @Suspendable { withdraw(it) })
		
		r.onNpcOption4(warren, s@ @Suspendable { claim(it) })
	}
	
	@Suspendable fun deposit(it: Script) {
		val player = it.player()
		
		if (!Lottery.enabled()) {
			player.message("The OSScape Lottery is busy at the moment.")
			return
		}
		
		val amount = it.inputInteger("Amount to deposit (${formatItemAmount(Lottery.config?.amountFor(player.id() as Int) ?: 0)} already invested)?")
		
		if (amount > 0)
			Lottery.deposit(player, amount.toLong())
	}
	
	@Suspendable fun withdraw(it: Script) {
		val player = it.player()
		
		if (!Lottery.enabled()) {
			player.message("The OSScape Lottery is busy at the moment.")
			return
		}
		
		val amount = it.inputInteger("Amount to withdraw (${formatItemAmount(Lottery.config?.amountFor(player.id() as Int) ?: 0)} already invested)?")
		
		if (amount > 0)
			Lottery.withdraw(player, false, amount.toLong())
	}
	
	@Suspendable fun claim(it: Script) {
		val player = it.player()
		
		if (!Lottery.enabled()) {
			player.message("The OSScape Lottery is busy at the moment.")
			return
		}
		
		if (Lottery.config?.winningsFor(player.id() as Int) ?: 0 > 0) {
			Lottery.withdraw(player, true)
		} else {
			it.chatNpc("You don't currently have any outstanding lottery winnings.", warren)
		}
	}
	
}