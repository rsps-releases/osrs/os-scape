package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository

object ReportButton {

    @JvmStatic @ScriptMain
    fun register(r: ScriptRepository) {
        r.onButton(162, 26) @Suspendable {
            it.player().write(AddMessage("http://forum.os-scape.com/index.php?forums/report-center.25/", AddMessage.Type.URL))
            it.message("A webpage should have opened with the url: http://forum.os-scape.com/index.php?forums/report-center.25/")
        }
    }

}