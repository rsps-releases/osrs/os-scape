package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 21/05/2017.
 */
object TicketSalesman {
	
	val SALESMAN = 6071
	val SALESMAN_SHOP_INTER = 278
	val INV_INTER = 512
	val STOCK = arrayOf(Item(47, 30), Item(1133), Item(892, 50), Item(1169), Item(1135), Item(829, 20))
	val PRICES = intArrayOf(140, 150, 2000, 100, 2400, 200)
	val TICKET_ID = 1464
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(SALESMAN) @Suspendable {
			it.player().interfaces().sendMain(SALESMAN_SHOP_INTER)
			it.player().interfaces().sendInventory(INV_INTER)
			it.player().write(SetItems(228, SALESMAN_SHOP_INTER, 89, ItemContainer(it.player().world(), STOCK, ItemContainer.Type.FULL_STACKING)))
			it.player().write(SetItems(93, 512, 0, it.player().inventory()))
			it.player().interfaces().setting(SALESMAN_SHOP_INTER, 89, 0, 27, 1086)
		}
		repo.onInterfaceClose(SALESMAN_SHOP_INTER) {
			it.player().interfaces().closeById(INV_INTER) // Inventory interface
		}
		
		repo.onButton(SALESMAN_SHOP_INTER, 89) {
			val cost = PRICES[it.buttonSlot()]
			val item = STOCK[it.buttonSlot()]
			val player = it.player()
			val opcode = it.buttonAction() // Uses special packet.. probs left over from 317! the interface is just super outdated.
			if (opcode == 200) {
				if (player.inventory().count(TICKET_ID) < cost) {
					it.message("${item.name(player.world())} costs $cost archery tickets. You don't have enough.")
				} else if (player.inventory().add(item, false).success()) {
					player.inventory().remove(Item(TICKET_ID, cost), false)
					it.message("You've spent $cost archery tickets on ${item.name(player.world())}.")
					it.player().write(SetItems(93, 512, 0, it.player().inventory()))
				}
			} else if (opcode == 123) {
				it.message("${item.name(player.world())} costs $cost archery tickets.")
			}
		}
	}
	
}