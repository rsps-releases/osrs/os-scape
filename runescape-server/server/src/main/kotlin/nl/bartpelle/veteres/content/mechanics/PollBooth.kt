package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue.Sbott

import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 1/1/2016.
 */

object PollBooth {
	@Suspendable fun showPolls(player: Player) {
		val slot = player.attrib<Int>(AttributeKey.BUTTON_SLOT)
		player.interfaces().sendMain(310)
		player.invokeScript(621, "Achievement Diary Menu", "Building...")
		player.invokeScript(627, 3, 2, "<col=df780f>Best server in history of RSPS</col>|Testing|Testing", "3rd December - 10th January 2016")
	}
	
	@JvmStatic @ScriptMain fun interfaceButtons(repo: ScriptRepository) {
//		repo.onButton(310, 4) @Suspendable {
//			val hash = 22609922
//			it.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
//			it.player().interfaces().sendMain(345)
//			it.player().message((hash.shr(16).and(0xFFFF)).toString())
//			it.player().invokeScript(603, "Christmas 2015", 22609922, 22609932, 22609931, 22609930, "Building...")
//			it.player().invokeScript(609, "Intersting pol. XD", 16750624, 0, 495, 495, 12, 5, 22609931)
//			it.player().invokeScript(609, "Votes: 33,447", 16777215, 1, 496, 496, 12, 5, 22609931)
//			//it.player().invokeScript(624, -1, -1, "Question 1|Would you like the holiday event described in the 'Christmas 2015' dev blog?|Hyperlink clickable thing|http://www.os-scape.com|Yu|No|", 2 /* num options? */, 1 /* selected opt */, 30754, 2693, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
//			it.player().invokeScript(619, 0xffff, 1, "Question 1|Would you like the holiday event described in the 'Christmas 2015' dev blog?|Hyperlink clickable thing|http://www.os-scape.com|Yu|No|Yes|no|igger|lips")
//			it.player().invokeScript(609, "", 16750624, 0, 495, 495, 12, 5, 22609931)
//			it.player().invokeScript(618, 22609931, 22609932, 22609930, 1)
//			it.player().invokeScript(604, 22609928, 22609929, "History")
//			it.player().write(InterfaceText(345, 3, "This poll has closed."))
//			it.player().invokeScript(604, 22609926, 22609927, "")
//			it.player().invokeScript(604, 22609924, 22609925, "")
//		}
	}
}