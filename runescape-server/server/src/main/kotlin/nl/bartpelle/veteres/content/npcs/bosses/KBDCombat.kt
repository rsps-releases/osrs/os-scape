package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 8/28/2015.
 */

object KBDCombat {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcSpawn(239) {
			it.npc().putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 30)
		}
	}
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		//Combat will continue until the target is unreachable.
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			//Can we attack yet?
			if (EntityCombat.attackTimerReady(npc)) {
				val melee_range = EntityCombat.canAttackMelee(npc, target, false)
				if (melee_range && target.world().random(2) == 0) {
					melee(npc, target)
				} else {
					val attack = target.world().random(3)
					when (attack) {
						0 -> fire(npc, target)
						1 -> poison(npc, target)
						2 -> freezing(npc, target)
						3 -> shocking(npc, target)
					}
				}
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			//Prepare for next cycle.
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun melee(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, EntityCombat.randomHit(npc))
		else
			target.hit(npc, 0)
		
		npc.animate(npc.attackAnimation())
	}
	
	@Suspendable fun fire(npc: Npc, target: Entity) {
		npc.animate(81)
		npc.world().spawnProjectile(npc, target, 393, 35, 25, 35, 30, 15, 10)
		
		// This stuff deals a fuckton of damage on weebs without a shield.
		var max = 65
		if (protected(target))
			max = 0
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, npc.world().random(max), 2).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, 2).combatStyle(CombatStyle.RANGE)
	}
	
	@Suspendable fun poison(npc: Npc, target: Entity) {
		npc.animate(81)
		npc.world().spawnProjectile(npc, target, 394, 35, 25, 35, 30, 15, 10)
		
		// This stuff deals a fuckton of damage on weebs without a shield.
		var max = 65
		if (protected(target))
			max = 10
		
		// This attack can poison the player too
		if (target.world().rollDie(6, 1)) {
			target.poison(8)
		}
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, npc.world().random(max), 2).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, 2).combatStyle(CombatStyle.RANGE)
	}
	
	@Suspendable fun freezing(npc: Npc, target: Entity) {
		npc.animate(81)
		npc.world().spawnProjectile(npc, target, 395, 35, 25, 35, 30, 15, 10)
		
		// This stuff deals a fuckton of damage on weebs without a shield.
		var max = 65
		if (protected(target))
			max = 10
		
		// This attack can poison the player too
		if (target.world().rollDie(6, 1)) {
			target.freeze(6, npc)
		}
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, npc.world().random(max), 2).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, 2).combatStyle(CombatStyle.RANGE)
	}
	
	@Suspendable fun shocking(npc: Npc, target: Entity) {
		npc.animate(81)
		npc.world().spawnProjectile(npc, target, 396, 35, 25, 35, 30, 15, 10)
		
		// This stuff deals a fuckton of damage on weebs without a shield.
		var max = 65
		if (protected(target))
			max = 10
		
		// This attack can poison the player too
		if (target.world().rollDie(6, 1) && target is Player) {
			for (i in intArrayOf(Skills.ATTACK, Skills.STRENGTH, Skills.DEFENCE, Skills.MAGIC, Skills.RANGED)) {
				target.skills().alterSkill(i, -2)
			}
			target.message("You're shocked and weakened!")
		}
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0))
			target.hit(npc, npc.world().random(max), 2).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, 2).combatStyle(CombatStyle.RANGE)
	}
	
	fun protected(target: Entity, message: Boolean = true): Boolean {
		if (!target.isPlayer) {
			return false
		}
		
		// Find any kind of dragonfire shield
		val item = (target as Player).equipment().get(EquipSlot.SHIELD)?.id() ?: -1
		if (item == 11283 || item == 11284 || item == 1540) {
			return true
		}
		
		if (message) {
			target.message("You are horribly burnt by the dragon's fiery breath!")
		}
		
		return false
	}
	
}