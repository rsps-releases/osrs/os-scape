package nl.bartpelle.veteres.content.npcs.godwars.bandos

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.model.Entity

/**
 * Created by Situations on 7/4/2016.
 */

object AggressionCheck {
	
	val BANDOS_PROTECTION_EQUIPMENT = arrayListOf(11061, 11804, 11832, 11834,
			11836, 12265, 12267, 12269, 12271, 12273, 12275, 12480, 12482, 12484,
			12486, 12488, 12498, 12500, 12502, 12504, 12608, 19924)
	
	@JvmField val script: Function1<Entity, Boolean> = s@ @Suspendable { entity -> determine_aggression(entity) }
	
	@Suspendable fun determine_aggression(entity: Entity): Boolean {
		BANDOS_PROTECTION_EQUIPMENT.forEach { armour ->
			if (entity.equipment().has(armour)) {
				return false
			}
		}
		return true
	}
}
