package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Fiber
import co.paralleluniverse.fibers.Suspendable
import com.google.common.collect.Iterables
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.skript.WaitReason
import nl.bartpelle.veteres.content.Help
import nl.bartpelle.veteres.content.dialogueInterruptCall
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository




/**
 * Created by Bart on 6/16/2016.
 */
object OptionList {

	class DisplayOptions(val title: String, val options: List<String>) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			val opt = display(it, false, title, *(options.toTypedArray()))
			val test = Iterables.get(Help.SCROLL_MAP.entries, opt)
			it.player().sendScroll(test.key, *test.value)
		}
	}
	
	@Suspendable fun display(it: Script, title: String, options: List<String>): Int {
		return display(it, false, title, *(options.toTypedArray()))
	}
	
	@Suspendable fun display(it: Script, keyboardTeleport: Boolean, title: String, vararg options: String): Int {
		it.player.interfaces().sendMain(187)
		it.player.interfaces().setting(187, 3, 0, 200, 1)
		it.player.invokeScript(217, title, options.joinToString("|"), if (keyboardTeleport) 1 else 0)
		it.player().invokeScript(1105, 1)

		it.waitReason = WaitReason.DIALOGUE
		it.interruptCall = dialogueInterruptCall
		Fiber.park()
		
		if (keyboardTeleport)
			it.player().write(InvokeScript(1105)) // Re-enables use of the keyboard for normal chatbox chat.
		it.player().write(InvokeScript(1105, 1))
		it.player.interfaces().closeMain()
		return if (it.waitReturnVal == null || it.waitReturnVal !is Int) -1 else it.waitReturnVal as Int
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onInterfaceClose(187) {
			it.player().invokeScript(101)
			it.player().invokeScript(1105, 1)
		}
	}
	
	
}