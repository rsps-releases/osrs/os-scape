package nl.bartpelle.veteres.content.areas.alkharid.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceItem
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Ellis {
	
	val ELLIS = 3231
	val POSSIBLE_ITEMS = intArrayOf(1739, 7801, 6287, 1753, 1751, 1749, 1747)
	val GOLD = 995
	
	enum class Items(val raw: Int, val product: Int, val standard_price: Int, val sbott_price: Int) {
		LEATHER(1739, 1741, 1, 2),
		HARD_LEATHER(1739, 1743, 3, 5),
		SNAKE_HIDE_TAI(7801, 6289, 20, 45),
		SNAKE_HIDE_TEMPLE(6287, 6289, 15, 25),
		GREEN_DRAGONHIDE(1753, 1745, 20, 45),
		BLUE_DRAGONHIDE(1751, 2505, 20, 45),
		RED_DRAGONHIDE(1749, 2507, 20, 45),
		BLACK_DRAGONHIDE(1747, 2509, 20, 45);
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ELLIS) @Suspendable { tanner_chat(it) }
		r.onNpcOption1(5809) @Suspendable { tanner_chat(it) } // Crafting guild tanner
		r.onNpcOption2(ELLIS) @Suspendable { leather_tanning(it) }
		r.onNpcOption2(5809) @Suspendable { leather_tanning(it) } // Crafting guild tanner
		
		//Soft leather - 1, 5, x, all
		r.onButton(324, 148) @Suspendable { tan(it, 1739, 1741, 1, 1) }
		r.onButton(324, 140) @Suspendable { tan(it, 1739, 1741, 1, 5) }
		r.onButton(324, 132) @Suspendable { tan(it, 1739, 1741, 1, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 124) @Suspendable { tan(it, 1739, 1741, 1, 27) }
		
		//Hard leather - 1, 5, x, all
		r.onButton(324, 149) @Suspendable { tan(it, 1739, 1743, 3, 1) }
		r.onButton(324, 141) @Suspendable { tan(it, 1739, 1743, 3, 5) }
		r.onButton(324, 133) @Suspendable { tan(it, 1739, 1743, 3, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 125) @Suspendable { tan(it, 1739, 1743, 3, 27) }
		
		//Snakeskin - 1, 5, x, all
		r.onButton(324, 150) @Suspendable { tan(it, 7801, 6289, 20, 1) }
		r.onButton(324, 142) @Suspendable { tan(it, 7801, 6289, 20, 5) }
		r.onButton(324, 134) @Suspendable { tan(it, 7801, 6289, 20, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 126) @Suspendable { tan(it, 7801, 6289, 20, 27) }
		
		//Snakeskin - 1, 5, x, all
		r.onButton(324, 151) @Suspendable { tan(it, 6287, 6289, 15, 1) }
		r.onButton(324, 143) @Suspendable { tan(it, 6287, 6289, 15, 5) }
		r.onButton(324, 135) @Suspendable { tan(it, 6287, 6289, 15, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 127) @Suspendable { tan(it, 6287, 6289, 15, 27) }
		
		//Green d'hide - 1, 5, x, all
		r.onButton(324, 152) @Suspendable { tan(it, 1753, 1745, 20, 1) }
		r.onButton(324, 144) @Suspendable { tan(it, 1753, 1745, 20, 5) }
		r.onButton(324, 136) @Suspendable { tan(it, 1753, 1745, 20, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 128) @Suspendable { tan(it, 1753, 1745, 20, 27) }
		
		//Blue d'hide - 1, 5, x, all
		r.onButton(324, 153) @Suspendable { tan(it, 1751, 2505, 20, 1) }
		r.onButton(324, 145) @Suspendable { tan(it, 1751, 2505, 20, 5) }
		r.onButton(324, 137) @Suspendable { tan(it, 1751, 2505, 20, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 129) @Suspendable { tan(it, 1751, 2505, 20, 27) }
		
		//Red d'hide - 1, 5, x, all
		r.onButton(324, 154) @Suspendable { tan(it, 1749, 2507, 20, 1) }
		r.onButton(324, 146) @Suspendable { tan(it, 1749, 2507, 20, 5) }
		r.onButton(324, 138) @Suspendable { tan(it, 1749, 2507, 20, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 130) @Suspendable { tan(it, 1749, 2507, 20, 27) }
		
		//Black d'hide - 1, 5, x, all
		r.onButton(324, 155) @Suspendable { tan(it, 1747, 2509, 20, 1) }
		r.onButton(324, 147) @Suspendable { tan(it, 1747, 2509, 20, 5) }
		r.onButton(324, 139) @Suspendable { tan(it, 1747, 2509, 20, amt = it.inputInteger("Enter amount:")) }
		r.onButton(324, 131) @Suspendable { tan(it, 1747, 2509, 20, 27) }
	}
	
	@Suspendable private fun tanner_chat(it: Script) {
		var contains_hide = false
		it.chatNpc("Greetings friend. I am a manufacturer of leather.", it.targetNpc()!!.id(), 590)
		POSSIBLE_ITEMS.forEach { hide ->
			if (it.player().inventory().contains(hide)) {
				contains_hide = true
			}
		}
		if (contains_hide) {
			it.chatNpc("I see you have brought me some hide.<br>Would you like me to tan them for you?", it.targetNpc()!!.id(), 569)
			when (it.optionsTitled("What would you like to say?", "Yes please.", "No thanks.")) {
				1 -> {
					it.chatPlayer("Yes please.", 554)
					leather_tanning(it)
				}
				2 -> {
					it.chatPlayer("No thanks.", 554)
					it.chatNpc("Very well, sir, as you wish.", it.targetNpc()!!.id(), 562)
				}
			}
		} else {
			when (it.optionsTitled("What would you like to say?", "Can I buy some leather then?", "Leather is rather weak stuff.")) {
				1 -> {
					it.chatPlayer("Can I buy some leather then?", 554)
					it.chatNpc("I make leather from animal hides. Bring me some cowhides and one gold coin per hide, and I'll tan them into soft leather for you.", it.targetNpc()!!.id(), 590)
					it.chatPlayer("Thanks!", 562)
				}
				2 -> {
					it.chatPlayer("Leather is rather weak stuff.", 588)
					it.chatNpc("Normal leather may be quite weak, but it's very cheap - I make it from cowhides for only 1 gp per hide - and it's so easy to craft that anyone can work with it.", it.targetNpc()!!.id(), 569)
					it.chatNpc("Alternatively you could try hard leather. It's not so easy to craft, but I only charge 3 gp per cowhide to prepare it, and it makes much sturdier armour.", it.targetNpc()!!.id(), 569)
					it.chatNpc("I can also tan snake hides and dragonhides, suitable for crafting into the highest quality armour for rangers.", it.targetNpc()!!.id(), 568)
					it.chatPlayer("Thanks, I'll bear it in mind.", 562)
				}
			}
		}
	}
	
	@Suspendable fun tan(it: Script, raw: Int, product: Int, cost: Int, amt: Int) {
		var amt = amt
		var cost = cost
		var raw_item = Item(raw)
		var product_item = Item(product)
		var tanned = 0
		
		val raw_name = raw_item.definition(it.player().world()).name.toLowerCase()
		val product_name = product_item.definition(it.player().world()).name.toLowerCase()
		
		if (amt >= 27) {
			amt = it.player().inventory().count(raw)
		}
		
		if (!it.player().inventory().contains(raw)) {
			it.message("You don't have any ${raw_name}s to tan.")
			return
		}
		
		if (!it.player().inventory().contains(Item(995, cost))) {
			it.message("You haven't got enough coins to pay for ${product_name}s.")
			return
		}
		
		it.player().interfaces().closeById(324)
		
		while (amt-- > 0) {
			if (!it.player().inventory().contains(raw) || !it.player().inventory().contains(Item(995, cost))) {
				return
			}
			
			if (it.player().inventory().remove(Item(raw), true).failed() ||
					it.player().inventory().remove(Item(995, cost), true).failed()) {
				return
			}
			
			it.player().inventory().add(Item(product), true)
			tanned += 1
		}
		
		if (tanned > 1) {
			it.message("The tanner tans ${tanned} ${raw_name}s for you.")
		} else {
			it.message("The tanner tans your ${raw_name}")
		}
	}
	
	@JvmStatic @Suspendable fun leather_tanning(it: Script) {
		// Cannot do this while locked.
		if (it.player().locked())
			return
		
		// Has introduced dupes previously..
		it.player().stopActions(false)
		
		it.player().interfaces().sendMain(324)
		it.player().write(InterfaceText(324, 108, get_color(it, 1739, 1) + "Soft Leather"),
				InterfaceText(324, 116, get_color(it, 1739, 1) + "1 coins"),
				InterfaceItem(324, 100, 1739, 250),
				
				InterfaceText(324, 109, get_color(it, 1739, 3) + "Hard leather"),
				InterfaceText(324, 117, get_color(it, 1739, 3) + "3 coins"),
				InterfaceItem(324, 101, 1739, 250),
				
				InterfaceText(324, 110, get_color(it, 7801, 20) + "Snakeskin"),
				InterfaceText(324, 118, get_color(it, 7801, 20) + "20 coins"),
				InterfaceItem(324, 102, 7801, 250),
				
				InterfaceText(324, 111, get_color(it, 6287, 15) + "Snakeskin"),
				InterfaceText(324, 119, get_color(it, 6287, 15) + "15 coins"),
				InterfaceItem(324, 103, 6287, 250),
				
				InterfaceText(324, 112, get_color(it, 1753, 20) + "Green d'hide"),
				InterfaceText(324, 120, get_color(it, 1753, 20) + "20 coins"),
				InterfaceItem(324, 104, 1753, 250),
				
				InterfaceText(324, 113, get_color(it, 1751, 20) + "Blue d'hide"),
				InterfaceText(324, 121, get_color(it, 1751, 20) + "20 coins"),
				InterfaceItem(324, 105, 1751, 250),
				
				InterfaceText(324, 114, get_color(it, 1749, 20) + "Red d'hide"),
				InterfaceText(324, 122, get_color(it, 1749, 20) + "20 coins"),
				InterfaceItem(324, 106, 1749, 250),
				
				InterfaceText(324, 115, get_color(it, 1747, 20) + "Black d'hide"),
				InterfaceText(324, 123, get_color(it, 1747, 20) + "20 coins"),
				InterfaceItem(324, 107, 1747, 250))
	}
	
	@Suspendable fun get_color(it: Script, raw: Int, cost: Int): String {
		if (!it.player().inventory().contains(raw) || !it.player().inventory().contains(Item(GOLD, cost))) {
			return "<col=f80000>"
		}
		return "<col=00c8f8>"
	}
}