package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 10/24/2016.
 */

object AberrantSpectre {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 6)) {
				if (EntityCombat.attackTimerReady(npc)) {
					val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
					val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
					
					npc.animate(npc.combatInfo().animations.attack)
					npc.world().spawnProjectile(npc.tile().transform(1, 1), target, 335, 60, 33, 40, 12 * tileDist, 11, 105)
					
					if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0)) {
						target.hit(npc, EntityCombat.randomHit(npc), delay).combatStyle(CombatStyle.MAGIC).graphic(Graphic(336, 260))
					} else {
						target.hit(npc, 0, delay).combatStyle(CombatStyle.MAGIC).graphic(Graphic(336, 260))
					}
					
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
}