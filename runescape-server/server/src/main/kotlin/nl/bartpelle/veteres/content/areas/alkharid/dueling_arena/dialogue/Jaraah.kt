package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Jaraah {
	
	val JARAAH = 3344
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(JARAAH) @Suspendable {
			it.chatPlayer("Hi!", 567)
			it.chatNpc("What? Can't you see I'm busy?!", JARAAH, 614)
			when (it.options("Can you heal me?", "You must see some gruesome things?", "Why do they call you 'The Butcher'?")) {
				1 -> {
					it.chatPlayer("Can you heal me?", 575)
					if (it.player().hp() == it.player().maxHp()) {
						it.chatNpc("You look healthy to me!", JARAAH, 567)
					} else {
						heal_player(it)
					}
				}
				2 -> {
					it.chatPlayer("You must see some gruesome things?", 575)
					it.chatNpc("It's a gruesome business and with the tools they give<br>me it gets more gruesome before it gets better!", JARAAH, 615)
				}
				3 -> {
					it.chatPlayer("Why do they call you 'The Butcher'?", 575)
					it.chatNpc("'The Butcher'?", JARAAH, 605)
					it.chatNpc("Ha!", JARAAH, 614)
					it.chatNpc("Would you like me to demonstrate?", JARAAH, 614)
					it.chatPlayer("Er...I'll give it a miss, thanks.", 571)
				}
			}
		}
		r.onNpcOption2(JARAAH) @Suspendable {
			if (it.player().hp() == it.player().maxHp()) {
				it.chatNpc("You look healthy to me!", JARAAH, 567)
			} else {
				heal_player(it)
			}
		}
	}
	
	fun heal_player(it: Script) {
		it.message("You feel a little better.")
		it.player().heal(2)
		it.sound(166)
		it.targetNpc()!!.sync().animation(881, 1)
	}
	
}
