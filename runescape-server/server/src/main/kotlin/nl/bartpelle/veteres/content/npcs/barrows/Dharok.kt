package nl.bartpelle.veteres.content.npcs.barrows

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 8/28/2015.
 */

object Dharok {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		EntityCombat.defaultMelee(it) { it, target ->
			if (EntityCombat.attemptHit(it.npc(), target, CombatStyle.MELEE)) {
				target.hit(it.npc(), randomHit(it.npc()))
			} else {
				target.hit(it.npc(), 0) // Uh-oh, that's a miss.
			}
		}
	}
	
	fun randomHit(me: Npc): Int {
		val hp = me.hp().toDouble()
		val max = me.maxHp().toDouble()
		val mult = Math.max(0.0, ((max - hp) / max) * 100.0) + 100.0
		val m = me.combatInfo().maxhit * (mult / 100)
		return me.world().random(m.toInt())
	}
	
}