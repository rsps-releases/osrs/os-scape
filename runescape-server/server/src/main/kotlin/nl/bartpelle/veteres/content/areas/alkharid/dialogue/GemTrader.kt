package nl.bartpelle.veteres.content.areas.alkharid.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object GemTrader {
	
	val GEM_TRADER = 526
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(GEM_TRADER) @Suspendable {
			it.chatNpc("Good day to you traveller. Would you be interested in<br>buying some gems?", GEM_TRADER, 567)
			when (it.options("Yes please.", "No thank you.")) {
				1 -> it.player().world().shop(9).display(it.player())
				2 -> it.chatPlayer("No thanks.", 588)
			}
		}
		r.onNpcOption2(GEM_TRADER) { it.player().world().shop(9).display(it.player()) }
	}
}
