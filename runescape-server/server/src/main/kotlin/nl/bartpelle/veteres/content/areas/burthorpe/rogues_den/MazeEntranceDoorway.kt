package nl.bartpelle.veteres.content.areas.burthorpe.rogues_den

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/19/2016.
 */

object MazeEntranceDoorway {
	
	val MAZE_ENTRANCE_DOORWAY = 7256
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(MAZE_ENTRANCE_DOORWAY) @Suspendable {
			val player = it.player()
			
			val inventory_space = it.player().inventory().freeSlots()
			val equipment_space = it.player().equipment().freeSlots()
			
			//Does our player have the gem?
			if (player.inventory().contains(Item(BrianORichard.MYSTIC_JEWEL))) {
				
				//Does our player have any items in their inventory/equipment?
				if (inventory_space == 27 && equipment_space == 14) {
					val door: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
					
					if (it.player().tile().z <= 4991) {
						if (!it.player().tile().equals(door.tile().transform(0, 0, 0))) {
							it.player().walkTo(door.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
							it.waitForTile(door.tile().transform(0, 0, 0))
						}
						it.player().lock()
						it.runGlobal(player.world()) @Suspendable {
							val world = player.world()
							val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
							val spawned = MapObj(Tile(3056, 4991, door.tile().level), 7254, door.type(), 1)
							world.removeObj(old, false)
							world.spawnObj(spawned)
							it.delay(2)
							world.removeObjSpawn(spawned)
							world.spawnObj(old)
						}
						player.pathQueue().interpolate(3056, 4992, PathQueue.StepType.FORCED_WALK)
						it.waitForTile(Tile(3056, 4992))
						it.delay(1)
						player.unlock()
					} else {
						it.message("The door won't open!")
					}
				} else {
					it.chatNpc("Tut tut tut, now you know you're not allowed to take<br>anything except that jewel in with you.", BrianORichard.BRIAN_O_RICHARD, 576)
				}
			} else {
				it.chatNpc("And where do you think you're going? A little too eager<br>I think. Come and talk to me before you go wandering<br>around in there.", BrianORichard.BRIAN_O_RICHARD, 577)
			}
		}
	}
}
