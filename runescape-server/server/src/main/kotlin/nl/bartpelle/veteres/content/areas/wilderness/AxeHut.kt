package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.LocationUtilities

/**
 * Created by Situations on 11/10/2015.
 */

object AxeHut { // Axe hut
	
	val OPENED_GATE = 1548 // TODO find the right one with no right-click options on RS. ID changed from 7148
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		repo.onObject(11726) @Suspendable {
			val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			if (option == 1) { // "Open"
				open(it, obj, player)
			} else if (option == 2) { // pick-lock
				picklock(it, obj, player)
			}
		}
	}
	
	@Suspendable private fun picklock(it: Script, obj: MapObj, player: Player) {
		val instanced = player in MageBankInstance
		val northDoorTile = Tile(3191, 3963)
        val southDoorTile = Tile(3190, 3957)

		if (WildernessLevelIndicator.inside_axehut(player.tile())) {
			player.message("The door is already unlocked.")
			return
		}
		// Not on the target tile. Cos of doors, yakno.
		if (!player.tile().equals(obj.tile())) {
			player.pathQueue().interpolate(obj.tile().x, obj.tile().z, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
		}
		
		if (!player.world().realm().isPVP && player.skills().xpLevel(Skills.THIEVING) < 37) {
			player.message("You need a Thieving level of 37 to pick lock this door.")
			return
		}
		
		// North side
		if (player.tile().z == 3963 || instanced && obj.tile() == LocationUtilities.dynamicTileFor(northDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)) {
			//Check if the player has a lockpick
			if (player.inventory().has(1523)) {
				player.message("You attempt to pick the lock.")
				
				//Create a chance to picklock the door
				if (player.world().random(100) >= 50) {
					
					it.runGlobal(player.world()) @Suspendable { s ->
						val world = player.world()
						val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
						val spawned = MapObj(Tile(3191, 3962), OPENED_GATE, obj.type(), 0)
						spawned.interactAble(false)
						world.removeObj(old, false)
						world.spawnObj(spawned)
						s.delay(2)
						world.removeObjSpawn(spawned)
						world.spawnObj(old)
					}
					//Move the player outside of the axe hut
					player.pathQueue().interpolate(player.tile().x, player.tile().transform(0, -1).z, PathQueue.StepType.FORCED_WALK)
					player.message("You manage to pick the lock.")
					//Add thieving experience for a successful lockpick
					if(!player.world().realm().isPVP)
						player.skills().__addXp(Skills.THIEVING, 22.0)
					return
				} else {
					player.message("You fail to pick the lock.")
				}
			} else {
				player.message("You need a lockpick for this lock.")
			}
			player.faceTile(obj.tile())
		} else if (player.tile().z == 3957 || instanced && obj.tile() == LocationUtilities.dynamicTileFor(southDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)) {
			// South side
			//Check if the player has a lockpick
			if (player.inventory().has(1523)) {
				
				player.message("You attempt to pick the lock.")
				//Create a chance to picklock the door
				if (player.world().random(100) >= 50) {
					
					it.runGlobal(player.world()) @Suspendable { s ->
						val world = player.world()
						val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
						val spawned = MapObj(Tile(3190, 3958), OPENED_GATE, obj.type(), 2)
						spawned.interactAble(false)
						world.removeObj(old, false)
						world.spawnObj(spawned)
						s.delay(2)
						world.removeObjSpawn(spawned)
						world.spawnObj(old)
					}
					
					//Move the player outside of the axe hut
					player.pathQueue().interpolate(player.tile().x, player.tile().transform(0, 1).z, PathQueue.StepType.FORCED_WALK)
					player.message("You manage to pick the lock.")
					//Add thieving experience for a successful lockpick
					if(!player.world().realm().isPVP)
					player.skills().__addXp(Skills.THIEVING, 22.0)
					return
				} else {
					//Send the player a message
					player.message("You fail to pick the lock.")
				}
			} else {
				player.message("You need a lockpick for this lock.")
			}
			player.faceTile(player.tile().transform(0, 2))
		} else if (player.tile().z == 3958 || player.tile().z == 3962 || instanced) {
			//Send the player a message
			player.message("The door is already unlocked.")
		}
	}
	
	@Suspendable private fun open(it: Script, obj: MapObj, player: Player) {
		when {
			player.tile() == obj.tile() -> // You're outside
				player.message("This door is locked.")
			player.tile().z > obj.tile().z -> { // Go out the south door
				player.message("You go through the door.")

				//Replace the object with an open door
				it.runGlobal(player.world()) @Suspendable { s ->
					val world = player.world()
					val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
					val spawned = MapObj(Tile(3190, 3958), OPENED_GATE, obj.type(), 2)
					spawned.interactAble(false)
					world.removeObj(old, false)
					world.spawnObj(spawned)
					s.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}

				//Move the player outside of the axe hut
				player.pathQueue().interpolate(player.tile().x, player.tile().transform(0, -1).z, PathQueue.StepType.FORCED_WALK)
			}

			player.tile().z < obj.tile().z -> {
				player.message("You go through the door.")

				//Replace the object with an open door
				it.runGlobal(player.world()) @Suspendable { s ->
					val world = player.world()
					val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
					val spawned = MapObj(Tile(3191, 3962), OPENED_GATE, obj.type(), 0)
					world.removeObj(old, false)
					world.spawnObj(spawned)
					spawned.interactAble(false)
					s.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}

				//Move the player outside of the axe hut
				player.pathQueue().interpolate(player.tile().x, player.tile().transform(0, 1).z, PathQueue.StepType.FORCED_WALK)
			}
		}
	}
	
}
