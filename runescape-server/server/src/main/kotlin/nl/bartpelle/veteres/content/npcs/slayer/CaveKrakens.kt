package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 11/18/2015.
 */
object CaveKrakens {
	
	@JvmField public val onhit: Function1<Script, Unit> = {
		it.npc().sync().transmog(492)
	}
	
	@JvmField public val combat: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, false, 15)) {
				if (EntityCombat.attackTimerReady(npc)) {
					val tileDist = npc.tile().distance(target.tile())
					npc.world().spawnProjectile(npc, target, 162, 15, 30, 20, 4 * tileDist, 14, 5)
					val delay = Math.max(1, (4 + tileDist * 4) / 30)
					
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
						target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC)
					} else {
						target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC) // Uh-oh, that's a miss.
					}
					
					// .. and go into sleep mode.
					npc.animate(npc.attackAnimation())
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
}