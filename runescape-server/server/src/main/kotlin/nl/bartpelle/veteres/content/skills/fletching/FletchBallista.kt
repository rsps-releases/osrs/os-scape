package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/12/2016.
 */

object FletchBallista {
	
	const val MONKEY_TAIL = 19610
	const val SPRING = 19601
	const val LIMBS = 19592
	const val LIGHT_FRAME = 19586
	const val HEAVY_FRAME = 19589
	
	const val INCOMPLETE_LIGHT = 19595
	const val INCOMPLETE_HEAVY = 19598
	
	const val UNSTRUNG_LIGHT = 19604
	const val UNSTRUNG_HEAVY = 19607
	
	const val LIGHT_BALLISTA = 19478
	const val HEAVY_BALLISTA = 19481
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Failure options
		r.onItemOnItem(MONKEY_TAIL, SPRING) {
			it.player().message("You should add a ballista spring to an incomplete ballista before attaching the tail.")
		}
		r.onItemOnItem(MONKEY_TAIL, LIGHT_FRAME) {
			it.player().message("The frame must first be combined with a pair of ballista limbs.")
		}
		r.onItemOnItem(SPRING, LIGHT_FRAME) {
			it.player().message("The frame must first be combined with a pair of ballista limbs.")
		}
		r.onItemOnItem(MONKEY_TAIL, LIMBS) {
			it.player().message("The limbs must first be combined with a ballista frame.")
		}
		r.onItemOnItem(SPRING, LIMBS) {
			it.player().message("The limbs must first be combined with a ballista frame.")
		}
		r.onItemOnItem(MONKEY_TAIL, INCOMPLETE_LIGHT) {
			it.player().message("You should add a ballista spring before attaching the tail.")
		}
		
		// Adding the limbs to a frame
		registerCraft(r, 15.0, 30, LIMBS, LIGHT_FRAME, INCOMPLETE_LIGHT)
		registerCraft(r, 30.0, 72, LIMBS, HEAVY_FRAME, INCOMPLETE_HEAVY)
		
		// Adding the spring to an unfinished frame
		registerCraft(r, 15.0, 30, SPRING, INCOMPLETE_LIGHT, UNSTRUNG_LIGHT)
		registerCraft(r, 30.0, 72, SPRING, INCOMPLETE_HEAVY, UNSTRUNG_HEAVY)
		
		// Finishing the ballista
		registerCraft(r, 300.0, 30, MONKEY_TAIL, UNSTRUNG_LIGHT, LIGHT_BALLISTA)
		registerCraft(r, 600.0, 72, MONKEY_TAIL, UNSTRUNG_HEAVY, HEAVY_BALLISTA)
	}
	
	fun registerCraft(r: ScriptRepository, xp: Double, level: Int, item1: Int, item2: Int, result: Int) {
		r.onItemOnItem(item1, item2) @Suspendable {
			if (it.player().skills().xpLevel(Skills.FLETCHING) >= level) {
				it.player().animate(7172)
				it.player().inventory().remove(Item(item1), true)
				it.player().inventory().remove(Item(item2), true)
				it.player().inventory().add(Item(result), true)
				it.addXp(Skills.FLETCHING, xp)
				it.delay(4)
				it.player().animate(-1)
			} else {
				it.messagebox("You need a Fletching level of $level to do this.")
			}
		}
	}
	
}