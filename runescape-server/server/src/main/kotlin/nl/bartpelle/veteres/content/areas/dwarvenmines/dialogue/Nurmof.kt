package nl.bartpelle.veteres.content.areas.dwarvenmines.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/5/2015.
 */

object Nurmof {
	
	val NURMOF = 1301
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(NURMOF) @Suspendable {
			it.chatNpc("Greetings and welcome to my pickaxe shop. Do you<br>want to buy my premium quality pickaxes?", NURMOF, 568)
			when (it.options("Yes, please.", "No, thank you.", "Are your pickaxes better than other pickaxes, then?")) {
				1 -> {
					it.player().world().shop(4).display(it.player())
				}
				2 -> {
					it.chatPlayer("No, thank you.", 588)
				}
				3 -> {
					it.chatPlayer("Are your pickaxes better than other pickaxes, then?", 588)
					it.chatNpc("Of course they are! My pickaxes are made of higher<br>" +
							"grade metal than your ordinary bronze pickaxes,<br>" +
							"allowing you to mine that ore just a little bit faster than<br>normal.", NURMOF, 570)
				}
			}
		}
		
		r.onNpcOption2(NURMOF) {
			it.player().world().shop(4).display(it.player())
		}
	}
	
}
