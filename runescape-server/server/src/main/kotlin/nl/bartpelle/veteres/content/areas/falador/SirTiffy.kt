package nl.bartpelle.veteres.content.areas.falador

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.mechanics.bankobjects
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 06/06/2016.
 */
object SirTiffy {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(4687) @Suspendable {
			it.chatNpc("Welcome back Knight. Do you want to see my wares?", 4687)
			if (it.options("Yes.", "Never mind.") == 1) {
				it.player().world().shop(40).display(it.player())
			}
		}
		
		for (i in intArrayOf(9666, 9668, 9670)) {
			repo.onItemOption1(i) {
				val slot: Int = it.player().attrib(AttributeKey.ITEM_SLOT)
				bankobjects.openset(it.player(), i, slot)
			}
		}
	}
}