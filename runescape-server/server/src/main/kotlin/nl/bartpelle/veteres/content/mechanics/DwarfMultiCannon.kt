package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jonathan on 6/5/2017.
 */
object DwarfMultiCannon {
	
	private val BASE = Item(6, 1)
	private val STAND = Item(8, 1)
	private val BARREL = Item(10, 1)
	private val FURNACE = Item(12, 1)
	
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(BASE.id(), s@ @Suspendable {
			val player = it.player()
			
			if (!player.inventory().hasAll(BASE, STAND, BARREL, FURNACE)) {
				player.message("You don't have all the cannon components!")
				return@s
			}
			if (player.hasAttrib(AttributeKey.DWARF_MULTI_CANNON)) {
				player.message("You cannot construct more than one cannon at a time.");
				player.message("If you have lost your cannon, go and see the dwarf cannon engineer.");
				return@s
			}
			
			player.lock()
			player.animate(827)
			player.inventory().remove(BASE, false)
			player.world().spawnObj(MapObj(player.tile().transform(0, 1), 7, 10, 1))
			player.message("You place the cannon base on the ground.")
			it.delay(2)
			player.animate(827)
			player.inventory().remove(STAND, false)
			player.world().spawnObj(MapObj(player.tile().transform(0, 1), 8, 10, 1))
			player.message("You add the stand.")
			it.delay(2)
			player.animate(827)
			player.inventory().remove(BARREL, false)
			player.world().spawnObj(MapObj(player.tile().transform(0, 1), 9, 10, 1))
			player.message("You add the barrels.")
			it.delay(2)
			player.animate(827)
			player.inventory().remove(FURNACE, false)
			val cannon = Cannon(player.tile().transform(0, 1), 6, 10, 1)
			player.world().spawnObj(cannon)
			player.putattrib(AttributeKey.DWARF_MULTI_CANNON, cannon)
			player.message("You add the furnace.")
			player.unlock()
		})
		
		r.onObject(6, s@ @Suspendable {
			val player = it.player()
			val cannon = it.interactionObject() as? Cannon ?: return@s
			
			if (!cannon.isOwner(player)) {
				player.message("This is not your cannon.")
				return@s
			}
			if (it.interactionOption() == 1) {
				//FIRE
			} else if (it.interactionOption() == 2) {
				//PICKUP
				var slots = 4
				if (cannon.cannonBalls > 0 && !player.inventory().has(2))
					slots += 1
				
				if (player.inventory().freeSlots() < slots) {
					player.message("You don't have enough space in your inventory.")
					return@s
				}
				player.message("You pick up the cannon")
				player.inventory().add(BASE, false)
				player.inventory().add(STAND, false)
				player.inventory().add(BARREL, false)
				player.inventory().add(FURNACE, false)
				player.inventory().add(Item(2, cannon.cannonBalls), false)
				player.world().removeObj(cannon)
				player.clearattrib(AttributeKey.DWARF_MULTI_CANNON)
			}
		})
	}
	
	private class Cannon(tile: Tile, id: Int, type: Int, rot: Int) : MapObj(tile, id, type, rot) {
		
		var cannonBalls = 0
		var owner: Int? = null
		
		fun isOwner(player: Player) = player.id() as Int == owner
		
	}
	
}