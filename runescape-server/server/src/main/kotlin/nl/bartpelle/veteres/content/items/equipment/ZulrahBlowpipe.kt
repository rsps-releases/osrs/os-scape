package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.combat.ranged.DartDrawback
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 22/09/2016.
 */
object ZulrahBlowpipe {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		// Check charges
		r.onItemOption3(ZulrahItems.USED_TOXIC_BLOWPIPE) @Suspendable {
			ZulrahStaffOfDead.check_scales_message(it, it.itemUsed(), 16000)
		}
		r.onEquipmentOption(1, ZulrahItems.USED_TOXIC_BLOWPIPE) {
			ZulrahStaffOfDead.check_scales_message(it, it.player().equipment().get(EquipSlot.WEAPON), 16000)
		}
		
		// Dismantle an empty blowpipe
		r.onItemOption4(ZulrahItems.EMPTY_TOXIC_BLOWPIPE) @Suspendable {
			it.doubleItemBox("Dismantling your blowpipe will destory it and give you 20,000 zulrah scales in return. Are you sure?", Item(ZulrahItems.EMPTY_TOXIC_BLOWPIPE), Item(ZulrahItems.ZULRAHS_SCALES, 20000))
			if (it.optionsTitled("Dismantle the blowpipe?", "Yes", "No") == 1) {
				if (it.player().inventory().remove(Item(ZulrahItems.EMPTY_TOXIC_BLOWPIPE), false).success()) {
					it.player().inventory() += Item(ZulrahItems.ZULRAHS_SCALES, 20000)
				}
			}
		}
		
		// Unload ammo
		r.onItemOption4(ZulrahItems.USED_TOXIC_BLOWPIPE, s@ @Suspendable {
			if (it.player().world().realm().isPVP) {
				if (!it.itemUsed().hasProperties()) {
					it.player().inventory().set(it.itemUsedSlot(), Item(12924))
					it.player().message("You unload the blowpipe.")
				} else {
					it.player().message("This blowpipe cannot be <col=FF0000>unloaded</col>.")
				}
			} else {
				if (!it.itemUsed().hasProperties()) {
					it.doubleItemBox("This blowpipe is <col=FF0000>uncharged</col>. Would you revert it to a magic fang? A chisel is required to recraft a blowpipe.", ZulrahItems.USED_TOXIC_BLOWPIPE, ZulrahItems.TANZANITE_FANG)
					if (it.optionsTitled("Revert the blowpipe to a fang?", "Yes", "No") == 1) {
						if (it.player().inventory().remove(Item(ZulrahItems.USED_TOXIC_BLOWPIPE), false).success()) {
							it.player().inventory() += Item(ZulrahItems.TANZANITE_FANG, 1)
						}
					}
					return@s
				}
				
				val dartid = it.itemUsed().propertyOr(ItemAttrib.DART_ITEMID, -1)
				if (dartid == -1) {
					it.message("There are no darts in the blowpipe.")
					return@s
				}
				val spacerequired = if (it.player().inventory().has(dartid)) 0 else 1
				if (it.player().inventory().freeSlots() < spacerequired) {
					it.messagebox("You need $spacerequired free inventory slots to do this.")
				} else if (it.itemUsed().property(ItemAttrib.DARTS_COUNT) < 1) {
					it.messagebox("This blowpipe has no darts to uncharge.")
				} else {
					val dartsToAdd = Math.min(Integer.MAX_VALUE - it.player().inventory().count(dartid), it.itemUsed().property(ItemAttrib.DARTS_COUNT))
					if (dartsToAdd == 0) { // 2.1b darts held already.
						it.message("You don't have enough room in your inventory to do this.")
					} else {
						val chargedBlowpipe = it.itemUsed()
						chargedBlowpipe.property(ItemAttrib.DARTS_COUNT, chargedBlowpipe.property(ItemAttrib.DARTS_COUNT) - dartsToAdd)
						
						it.player().debug("counts: darts %s scales %s", chargedBlowpipe.property(ItemAttrib.DARTS_COUNT), chargedBlowpipe.property(ItemAttrib.DARTS_COUNT))
						if (chargedBlowpipe.property(ItemAttrib.DARTS_COUNT) < 1 && chargedBlowpipe.property(ItemAttrib.ZULRAH_SCALES) < 1) {
							// Give an uncharge blowpipe to the player, as it has nothing in it now.
							it.player().inventory().set(it.itemUsedSlot(), Item(ZulrahItems.EMPTY_TOXIC_BLOWPIPE))
						}
						if (chargedBlowpipe.property(ItemAttrib.DARTS_COUNT) < 1) {
							chargedBlowpipe.property(ItemAttrib.DART_ITEMID, -1)
						}
						it.player().inventory() += Item(dartid, dartsToAdd)
						it.message("You remove the $dartsToAdd darts from the blowpipe, leaving it uncharged.")
					}
				}
			}
		})
		
		// Uncharge blowpipe
		r.onItemOption5(ZulrahItems.USED_TOXIC_BLOWPIPE) @Suspendable {
			if (!it.itemUsed().hasProperties()) {
				it.player().inventory().set(it.itemUsedSlot(), Item(12924))
				it.player().message("You uncharge the blowpipe.")
			} else {
				val spacerequired = if (it.player().inventory().has(ZulrahItems.ZULRAHS_SCALES)) 0 else 1
				if (it.player().inventory().freeSlots() < spacerequired) {
					it.messagebox("You need $spacerequired free inventory slots to do this.")
				} else if (it.itemUsed().property(ItemAttrib.ZULRAH_SCALES) == 0) {
					it.messagebox("This blowpipe has no scales to uncharge.")
				} else {
					val scalesToAdd = Math.min(Integer.MAX_VALUE - it.player().inventory().count(ZulrahItems.ZULRAHS_SCALES), it.itemUsed().property(ItemAttrib.ZULRAH_SCALES))
					if (scalesToAdd == 0) { // 2.1b scales held already.
						it.message("You don't have enough room in your inventory to do this.")
					} else {
						val chargedBlowpipe = it.itemUsed()
						chargedBlowpipe.property(ItemAttrib.ZULRAH_SCALES, chargedBlowpipe.property(ItemAttrib.ZULRAH_SCALES) - scalesToAdd)
						
						it.player().debug("counts: darts %s scales %s", chargedBlowpipe.property(ItemAttrib.DARTS_COUNT), chargedBlowpipe.property(ItemAttrib.ZULRAH_SCALES))
						if (chargedBlowpipe.property(ItemAttrib.DARTS_COUNT) < 1 && chargedBlowpipe.property(ItemAttrib.ZULRAH_SCALES) < 1) {
							// Give an uncharge blowpipe to the player, as it has nothing in it now.
							it.player().inventory().set(it.itemUsedSlot(), Item(ZulrahItems.EMPTY_TOXIC_BLOWPIPE))
						}
						it.player().inventory() += Item(ZulrahItems.ZULRAHS_SCALES, scalesToAdd)
						it.message("You remove the $scalesToAdd zulrah scales from the blowpipe, leaving it uncharged.")
					}
				}
			}
		}
		
		// Scales on empty
		r.onItemOnItem(ZulrahItems.ZULRAHS_SCALES, ZulrahItems.EMPTY_TOXIC_BLOWPIPE) {
			
			val pipeSlot = ItemOnItem.slotOf(it, ZulrahItems.EMPTY_TOXIC_BLOWPIPE)
			val pipe = it.player().inventory()[pipeSlot]
			val scalesSlot = ItemOnItem.slotOf(it, ZulrahItems.ZULRAHS_SCALES) // opposite way around
			val scales = it.player().inventory()[scalesSlot]
			
			// If the system is disabled, replace empty with used.
			if (pipe.id() == ZulrahItems.EMPTY_TOXIC_BLOWPIPE && !ZulrahItems.CHARGING_ENABLED) {
				if (it.player().inventory().remove(pipe, true).success()) {
					it.player().inventory().add(Item(ZulrahItems.USED_TOXIC_BLOWPIPE), true, pipeSlot)
					it.message("You don't need to charge this item for it to work.")
				}
				return@onItemOnItem
			}
			if (!ZulrahItems.CHARGING_ENABLED) {
				it.message("You don't need to charge this item for it to work.")
				return@onItemOnItem
			}
			val amtToAdd = Math.min(16000 - pipe.property(ItemAttrib.ZULRAH_SCALES), scales.amount())
			if (amtToAdd == 0) {
				it.message("Your blowpipe is already fully charged.")
				return@onItemOnItem
			}
			it.player().inventory() -= pipe
			if (it.player().inventory().remove(Item(ZulrahItems.ZULRAHS_SCALES, amtToAdd), false).success()) {
				val chargedPipe = Item(ZulrahItems.USED_TOXIC_BLOWPIPE)
				chargedPipe.property(ItemAttrib.ZULRAH_SCALES, pipe.property(ItemAttrib.ZULRAH_SCALES) + amtToAdd)
				val newtotal = chargedPipe.property(ItemAttrib.ZULRAH_SCALES)
				it.player().inventory().add(chargedPipe, true, it.itemOnSlot())
				it.message("You add $amtToAdd scales to your blowpipe. It now has $newtotal charges.")
			}
		}
		
		// Scales on used (partly full)
		r.onItemOnItem(ZulrahItems.ZULRAHS_SCALES, ZulrahItems.USED_TOXIC_BLOWPIPE) {
			if (!ZulrahItems.CHARGING_ENABLED) {
				it.message("You don't need to charge this item for it to work.")
				return@onItemOnItem
			}
			val pipeSlot = ItemOnItem.slotOf(it, ZulrahItems.USED_TOXIC_BLOWPIPE)
			val pipe = it.player().inventory()[pipeSlot]
			val scalesSlot = ItemOnItem.slotOf(it, ZulrahItems.ZULRAHS_SCALES) // opposite way around
			val scales = it.player().inventory()[scalesSlot]
			val amtToAdd = Math.min(16000 - pipe.property(ItemAttrib.ZULRAH_SCALES), scales.amount())
			if (amtToAdd == 0 && pipe.property(ItemAttrib.ZULRAH_SCALES) > 0) {
				it.message("Your blowpipe is already fully charged.")
				return@onItemOnItem
			}
			if (it.player().inventory().remove(Item(ZulrahItems.ZULRAHS_SCALES, amtToAdd), false).success()) {
				val chargedPipe = pipe
				chargedPipe.property(ItemAttrib.ZULRAH_SCALES, pipe.property(ItemAttrib.ZULRAH_SCALES) + amtToAdd)
				val newtotal = chargedPipe.property(ItemAttrib.ZULRAH_SCALES)
				it.message("You add $amtToAdd scales to your blowpipe. It now has $newtotal charges.")
			}
		}
		
		for (dart in DartDrawback.values()) {
			for (bp in intArrayOf(ZulrahItems.USED_TOXIC_BLOWPIPE, ZulrahItems.EMPTY_TOXIC_BLOWPIPE)) {
				r.onItemOnItem(dart.dart, bp) {
					if (it.player().world().realm().isPVP) {
						it.message("Blowpipes do not need loading with ammo to work.")
					} else {
						if (!ZulrahItems.CHARGING_ENABLED) {
							it.message("You don't need to charge this item for it to work.")
							return@onItemOnItem
						}
						if (!ZulrahItems.BLOWPIPE_REQUIRES_DARTS) {
							it.message("The blowpipe only needs Zulrah scales, not darts to work.")
							return@onItemOnItem
						}
						val pipeSlot = ItemOnItem.slotOf(it, bp)
						val pipe = it.player().inventory()[pipeSlot]
						val dartsSlot = ItemOnItem.slotOf(it, dart.dart) // opposite way around
						val darts = it.player().inventory()[dartsSlot]
						
						val storedId = pipe.propertyOr(ItemAttrib.DART_ITEMID, -1)
						if (dart.dart != storedId && storedId != -1) {
							it.message("The blowpipe has a different type of Dart stored in it. Remove that first before adding a new dart.")
							return@onItemOnItem
						}
						
						val amtToAdd = Math.min(16000 - pipe.property(ItemAttrib.DARTS_COUNT), darts.amount())
						if (amtToAdd == 0 && pipe.property(ItemAttrib.DARTS_COUNT) > 0) {
							it.message("Your blowpipe is already full with darts.")
							return@onItemOnItem
						}
						if (it.player().inventory().remove(Item(dart.dart, amtToAdd), false).success()) {
							val chargedPipe = pipe
							chargedPipe.property(ItemAttrib.DART_ITEMID, dart.dart)
							chargedPipe.property(ItemAttrib.DARTS_COUNT, pipe.property(ItemAttrib.DARTS_COUNT) + amtToAdd)
							val newtotal = chargedPipe.property(ItemAttrib.DARTS_COUNT)
							if (bp == ZulrahItems.EMPTY_TOXIC_BLOWPIPE)
								it.player().inventory().set(pipeSlot, Item(ZulrahItems.USED_TOXIC_BLOWPIPE).duplicateProperties(chargedPipe))
							it.message("You add $amtToAdd darts to your blowpipe. It now has $newtotal darts.")
						}
					}
				}
			}
		}
		
		//Creating the blowpipe from a Tanzanite fang..
		r.onItemOnItem(ZulrahItems.TANZANITE_FANG, ZulrahItems.CHISEL) @Suspendable {
			if (it.player().skills().level(Skills.FLETCHING) < 53) {
				it.player().message("You need at least level 53 Fletching to do this.")
			} else {
				if (it.player().inventory().has(ZulrahItems.TANZANITE_FANG) && it.player().inventory().has(ZulrahItems.CHISEL)) {
					it.player().inventory() -= ZulrahItems.TANZANITE_FANG
					it.player().inventory() += ZulrahItems.EMPTY_TOXIC_BLOWPIPE
					it.animate(3015)
					it.addXp(Skills.FLETCHING, 120.0)
					it.itemBox("You carve the fang and turn it into a powerful<br>blowpipe.", ZulrahItems.EMPTY_TOXIC_BLOWPIPE)
				}
			}
		}
	}
}