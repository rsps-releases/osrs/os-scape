package nl.bartpelle.veteres.content.areas.zeah.wcguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/10/2016.
 */
object Kai {
	
	const val KAI = 7239
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(KAI) @Suspendable {
			it.chatPlayer("Hello! Who are you?", 554)
			it.chatNpc("Hi there, my name is Kai and I'm the keeper of this<br>here ent habitat.", KAI, 568)
			
			when (it.options("What exactly is an ent?", "That's nice, see you around.")) {
				1 -> {
					it.chatPlayer("What exactly is an ent?", 554)
					it.chatNpc("Good question! Ents are ancient beings that resemble<br>trees, we believe they draw their energy directly from<br>nature.", KAI, 569)
					
					when (it.options("Where did they come from?", "That's nice, see you around.")) {
						1 -> {
							it.chatPlayer("Where did they come from?", 554)
							it.chatNpc("Another good question! We're not sure exactly, we just<br>know they're a very old race.", KAI, 568)
							it.chatNpc("I have heard of similar creatures roaming a desolate<br>part of the eastern continent, though.", KAI, 589)
							
							when (it.options("Interesting... can I fight them?", "Thanks for the information, see you around.")) {
								1 -> {
									it.chatPlayer("Interesting... can I fight them?", 554)
									it.chatNpc("Of course! There's no shortage of them residing within<br>the dungeon, and they're a fantastic source of wood.", KAI, 606)
									it.chatPlayer("Thanks!", 567)
								}
								2 -> {
									it.chatPlayer("Thanks for the information, see you around.", 588)
								}
							}
						}
						2 -> {
							it.chatPlayer("That's nice, see you around.", 588)
						}
					}
				}
				2 -> {
					it.chatPlayer("That's nice, see you around.", 588)
				}
			}
		}
	}
	
}