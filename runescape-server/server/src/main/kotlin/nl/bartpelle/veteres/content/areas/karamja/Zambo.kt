package nl.bartpelle.veteres.content.areas.karamja

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/11/2016.
 */

object Zambo {
	
	val ZAMBO = 1037
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ZAMBO) @Suspendable {
			it.chatNpc("Hey, are you wanting to try some of my fine wines<br>and spirits? All brewed locally on Karamja island.", ZAMBO, 555)
			when (it.options("Yes please.", "No, thank you.")) {
				1 -> it.player().world().shop(35).display(it.player())
				2 -> it.chatPlayer(" No, thank you.", 588)
			}
		}
		r.onNpcOption2(ZAMBO) @Suspendable { it.player().world().shop(35).display(it.player()) }
	}
}
