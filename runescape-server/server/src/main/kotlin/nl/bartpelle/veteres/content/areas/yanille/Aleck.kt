package nl.bartpelle.veteres.content.areas.yanille

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 15/06/2016.
 */
object Aleck {
	
	val ALECK = 1501
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption4(ALECK) @Suspendable {
			it.player().world().shop(42).display(it.player())
		}
		
		repo.onNpcOption1(1501) @Suspendable {
			it.chatPlayer("Hello.")
			it.chatNpc("Hello, hello, and a most warm welcome to my Hunter Emporium. We have everything the discerning Hunter could need.", ALECK)
			it.chatNpc("Would you like me to show you our range of equipment? Or was there something specific you were after?", ALECK)
			when (it.options("Ok, let's see what you've got!", "I'm not interested, thanks.", "Who's that guy over there?")) {
				1 -> {
					it.chatPlayer("Ok, let's see what you've got!")
					it.player().world().shop(42).display(it.player())
				}
				2 -> {
					it.chatPlayer("I'm not interested, thanks.")
					it.chatNpc("Well, if you do ever find yourself in need of the finest Hunter equipment available, then you know where to come.", ALECK)
				}
				3 -> {
					it.chatPlayer("Who's that guy over there?")
					it.chatNpc("Him? I think he might be crazy. Either that or he's seeking attention.", ALECK)
					it.chatNpc("He keeps trying to sell me these barmy looking weapons he's invented. I can't see them working, personally.", ALECK)
				}
			}
		}
	}
}