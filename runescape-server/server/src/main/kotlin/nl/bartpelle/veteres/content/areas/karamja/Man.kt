package nl.bartpelle.veteres.content.areas.karamja

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/10/2016.
 */

object Man {
	
	val MAN = 3082
	val SECOND_MAN = 3652
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MAN) @Suspendable {
			val random = it.player().world().random(6)
			
			if (random == 1) {
				it.chatNpc("So... many... trees...", MAN, 610)
			} else if (random == 2) {
				it.chatNpc("Too many monkeys - I can't stand it!", MAN, 571)
			} else if (random == 3) {
				it.chatNpc("I should have stayed in Port Sarim.", MAN, 610)
			} else if (random == 4) {
				it.chatNpc("Why don't I have a 'Teleport Home to Lumbridge'<br>button like everyone else?", MAN, 615)
			} else if (random == 5) {
				it.chatNpc("Snakes! Snakes! Ohhhh, there are snakes!", MAN, 571)
			} else if (random == 6) {
				it.chatNpc("I can't take the heat!", MAN, 610)
			}
			
			r.onNpcOption1(SECOND_MAN) @Suspendable {
				val random = it.player().world().random(4)
				if (random == 1) {
					it.chatPlayer("Hello, how's it going?", 567)
					it.chatNpc("How can I help you?", SECOND_MAN, 588)
					when (it.options("Do you wish to trade?", "I'm in cycle of a quest.", "I'm in cycle of enemies to kill.")) {
						1 -> wish_to_trade(it)
						2 -> search_of_a_quest(it)
						3 -> enemies_to_kill(it)
					}
				} else if (random == 2) {
					it.chatPlayer("Hello, how's it going?", 567)
					it.chatNpc("Do I know you? I'm in a hurry!", SECOND_MAN, 575)
				} else if (random == 3) {
					it.chatPlayer("Hello, how's it going?", 567)
					it.chatNpc("I'm a little worried - I've heard there's lots of people<br>going about, killing citizens at random.", SECOND_MAN, 589)
				} else if (random == 4) {
					it.chatPlayer("Hello, how's it going?", 567)
					it.chatNpc("Not too bad thanks.", SECOND_MAN, 567)
				} else if (random == 5) {
					it.chatPlayer("Hello, how's it going?", 567)
					it.chatNpc("None of your business.", SECOND_MAN, 614)
				}
			}
		}
	}
	
	@Suspendable fun wish_to_trade(it: Script) {
		it.chatPlayer("Do you wish to trade?", 588)
		it.chatNpc("No, I have nothing I wish to get rid of. If you want to<br>do some trading, there are plenty of shops and market<br>stalls around though.", SECOND_MAN, 590)
	}
	
	@Suspendable fun search_of_a_quest(it: Script) {
		it.chatPlayer("I'm in cycle of a quest.", 567)
		it.chatNpc("I'm sorry I can't help you there.", 588)
	}
	
	@Suspendable fun enemies_to_kill(it: Script) {
		it.chatPlayer("I'm in cycle of enemies to kill.", 588)
		it.chatNpc("I've heard there are many fearsome creatures that<br>dwell under the ground...", SECOND_MAN, 589)
	}
}
