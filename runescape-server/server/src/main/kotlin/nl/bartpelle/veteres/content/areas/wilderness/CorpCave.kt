package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 23/12/2015.
 */

object CorpCave {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(679) @Suspendable {
			// from inside to outside to wildy
			it.player().teleport(Tile(3206, 3681))
		}
		r.onObject(678, s@ @Suspendable {
			// from inside to outside to wildy
			if (it.interactionObject().tile().equals(3201, 3679)) {
				if (it.player().timers().has(TimerKey.TELEBLOCK)) {
					it.message("You can't go in here when teleblocked.")
					return@s
				}
				it.player().teleport(Tile(2965, 4382, 2))
			}
		})
		r.onObject(677) @Suspendable {
			// Actual corp enterance
			if (it.player().tile().x == 2970) {
				it.player().teleport(it.player().tile().transform(4, 0, 0))
				it.player().interfaces().sendWidgetOn(13, Interfaces.InterSwitches.D)
				it.player().varps().varp(Varp.CORP_BEAST_DAMAGE, 0)
			} else if (it.player().tile().x == 2974) {
				it.player().teleport(it.player().tile().transform(-4, 0, 0))
				it.player().interfaces().closeById(13)
				it.player().varps().varp(Varp.CORP_BEAST_DAMAGE, 0)
			}
		}
	}
	
}
