package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Realm
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.PlayerDamageTracker
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Bart on 8/5/2016.
 */
object SkotizoController {
	
	const val SKOTIZO_ID = 7286
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
	
	}
	
}