package nl.bartpelle.veteres.content.npcs.fightcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 9/18/2016.
 */

object KetZek {
	
	val MAGIC_MAX_DAMAGE = 49
	val MELEE_MAX_DAMAGE = 54
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 14) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.canAttackMelee(npc, target, false))
					if (npc.world().rollDie(2, 1))
						meleeAttack(npc, target)
					else magicAttack(npc, target)
				else
					magicAttack(npc, target)
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun meleeAttack(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, npc.world().random(MELEE_MAX_DAMAGE))
		else
			target.hit(npc, 0)
		
		npc.animate(npc.attackAnimation())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun magicAttack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
		
		npc.face(target)
		npc.animate(2647)
		npc.world().spawnProjectile(npc.tile().transform(2, 2), target, 445, 125, 36, 30, 12 * tileDist, 20, 5)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC))
			target.hit(npc, npc.world().random(MAGIC_MAX_DAMAGE), delay.toInt()).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, delay.toInt())
	}
}