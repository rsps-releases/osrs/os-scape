package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.ItemOnItem
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 16/10/2016.
 */
object BrokenUntradeables {
	
	enum class BrokenUntradeablesMap(val brokenId: Int, val fixedItemId: Int, val bmRepairCost: Int = 1, val coinsRepairCost: Int = 1) {
		FIRECAPE(20445, 6570, 20, 1),
		FIRE_MAXCAPE(20447, 13329, 20, 1),
		INFERNAL(21287, 21295, 40, 1),
		INFERNAL_MAXCAPE(21289, 21285, 40, 1),

		BRONZE_DEFENDER(20449, 8844, 0, 1),
		IRON_DEFENDER(20451, 8845, 0, 1),
		STEEL_DEFENDER(20453, 8846, 0, 1),
		BLACK_DEFENDER(20455, 8847, 0, 1), // Trihard Cx
		MITHRIL_DEFENDER(20457, 8848, 0, 1),
		ADAMANT_DEFENDER(20459, 8849, 0, 1),
		RUNE_DEFENDER(20461, 8850, 0, 20),
		DRAGON_DEFENDER(20463, 12954, 20, 1),
		AVERNIC_DEFENDER(22441, 22322, 500, 1),

		VOID_TOP(20465, 8839, 50, 1),
		VOID_ELITE_TOP(20467, 13072, 50, 1),
		VOID_BOTTOMS(20469, 8840, 50, 1),
		VOID_ELITE_BOTTOMS(20471, 13073, 50, 1),
		VOID_MACE(20473, 8841, 1, 1), // Mace not implemented into game.
		VOID_GLOVES(20475, 8842, 50, 1),
		VOID_MAGE_HELM(20477, 11663, 50, 1),
		VOID_RANGE_HELM(20479, 11664, 50, 1),
		VOID_MELEE_HELM(20481, 11665, 50, 1),
		
		DECORATIVE_SWORD(20483, 4508, 1, 1),
		DECORATIVE_TOP_GOLD(20485, 4509, 1, 1),
		DECORATIVE_LEGS_GOLD(20487, 4510, 1, 1),
		DECORATIVE_HELM_GOLD(20489, 4511, 1, 1),
		DECORATIVE_SHIELD_GOLD(20491, 4512, 1, 1),
		DECORATIVE_SKIRT_GOLD(20493, 11895, 1, 1),
		DECORATIVE_ROBETOP(20495, 11896, 250, 1),
		DECORATIVE_ROBESKIRT(20497, 11897, 250, 1),
		DECORATIVE_HAT(20499, 11898, 250, 1),
		DECORATIVE_RANGE_BODY(20501, 11899, 250, 1),
		DECORATIVE_CHAPS(20503, 11900, 250, 1),
		DECORATIVE_QUIVER(20505, 11901, 250, 1),
		
		FIGHTER_HAT(20507, 10548, 25, 1),
		RANGER_HAT(20509, 10550, 25, 1),
		HEALER_HAT(20511, 10547, 25, 1),
		FIGHTER_TORSO(20513, 10551, 25, 1),
		PENANCE_SKIRT(20515, 10555, 25, 1),
		
		SARA_HALO(20537, 12637, 50, 1),
		ZAMMY_HALO(20539, 12638, 50, 1),
		GUTHIX_HALO(20541, 12639, 50, 1)
		;
		
		companion object {
			fun forItemId(id: Int): BrokenUntradeablesMap? {
				for (entry in BrokenUntradeablesMap.values()) {
					if (entry.fixedItemId == id) {
						return entry
					}
				}
				return null
			}
			
			fun forBrokenId(id: Int): BrokenUntradeablesMap? {
				for (entry in BrokenUntradeablesMap.values()) {
					if (entry.brokenId == id) {
						return entry
					}
				}
				return null
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (entry in BrokenUntradeablesMap.values()) {
			
			repo.onItemOption1(entry.brokenId) @Suspendable {
				attempt_repairs(it, entry)
			}
			
			// Blood money repair
			repo.onItemOnItem(13307, entry.brokenId) @Suspendable {
				if (!it.player().world().realm().isPVP) {
					it.messagebox("You can only repair this item with Coins on this world.")
				} else {
					attempt_repairs(it, entry)
				}
			}
			
			// Coin repair
			repo.onItemOnItem(995, entry.brokenId) @Suspendable {
				if (it.player().world().realm().isPVP) {
					it.messagebox("You can only repair this item with coins on this world.")
				} else {
					attempt_repairs(it, entry)
				}
			}
		}
	}
	
	@Suspendable private fun attempt_repairs(it: Script, entry: BrokenUntradeables.BrokenUntradeablesMap) {
		val currencyId = if (it.player().world().realm().isPVP) 13307 else 995
		val currencyName = if (it.player().world().realm().isPVP) "Blood Money" else "coins"
		
		if (multipleBrokenItems(it.player())) {
			when (it.optionsTitled("What would you like to do?", "Fix All", "Fix ${Item(entry.fixedItemId).name(it.player().world())}", "Cancel")) {
				1 -> {
					BrokenUntradeablesMap.values().forEach { item ->
						if (it.player().inventory().has(item.brokenId)) {
							for (i in 1..it.player().inventory().count(item.brokenId)) {
								val cost = if (it.player().world().realm().isPVP) item.bmRepairCost else item.coinsRepairCost
								if (cost == 0 || it.player().inventory().remove(Item(currencyId, cost), false).success()) {
									// Be cool and delete the item at the correct slot, and give back in that slot, rather than first instance in inventory.
									val brokenSlot = ItemOnItem.slotOf(it, item.brokenId)
									it.player().inventory().remove(Item(item.brokenId), true, brokenSlot)
									it.player().inventory().add(Item(item.fixedItemId), false, brokenSlot)
									if (cost == 0)
										it.message("${Item(item.fixedItemId).name(it.player().world())} was repaired for free.")
									else
										it.message("You've paid $cost $currencyName to repair your ${Item(item.fixedItemId).name(it.player().world())}.")
								}
							}
						}
					}
				}
				2 -> repair_single_item(it, entry)
			}
		} else {
			repair_single_item(it, entry)
		}
	}
	
	@Suspendable private fun multipleBrokenItems(player: Player): Boolean {
		var brokenItems = 0
		
		BrokenUntradeablesMap.values().forEach { b ->
			if (player.inventory().has(b.brokenId))
				for (i in 1..player.inventory().count(b.brokenId))
					brokenItems++
			
		}
		
		return brokenItems >= 2
	}
	
	@Suspendable private fun repair_single_item(it: Script, entry: BrokenUntradeablesMap) {
		val pvp = it.player().world().realm().isPVP
		val cost = if (pvp) entry.bmRepairCost else (Item(entry.fixedItemId).definition(it.player().world()).cost * 0.5).toInt()
		val item_name = Item(entry.fixedItemId).name(it.player().world())
		val currencyId = if (it.player().world().realm().isPVP) 13307 else 995
		val currencyName = if (it.player().world().realm().isPVP) "Blood money" else "coins"
		var itemSlot = it.player().inventory().slotOf(entry.brokenId)

		if (cost > 0 && !it.player().inventory().contains(Item(currencyId, cost))) {
			it.doubleItemBox("You need $cost $currencyName to repair this item.", Item(currencyId, cost),
					it.player().inventory()[itemSlot])
			return
		}
		if (cost > 0 && it.optionsTitled("Pay $cost $currencyName to fix the $item_name?", "Yes", "No") != 1)
			return

		// since script was delayed items could have moved, update slot index
		itemSlot = it.player().inventory().slotOf(entry.brokenId)
		
		if (itemSlot < 0) return
		
		if (cost > 0 && !it.player().inventory().contains(Item(currencyId, cost))) {
			it.doubleItemBox("You need $cost $currencyName to repair this item.", Item(currencyId, cost),
					it.player().inventory()[itemSlot])
			return
		}
		if (cost == 0 || it.player().inventory().remove(Item(currencyId, cost), false).success()) {
			// Be cool and delete the item at the correct slot, and give back in that slot, rather than first instance in inventory.
			val brokenSlot =  it.player().inventory().slotOf(entry.brokenId)
			it.player().inventory().remove(Item(entry.brokenId), true, brokenSlot)
			it.player().inventory().add(Item(entry.fixedItemId), false, brokenSlot)
			if (cost == 0)
				it.message("$item_name was repaired for free.")
			else
			it.message("You've paid $cost $currencyName to repair your $item_name.")
		}
	}
}