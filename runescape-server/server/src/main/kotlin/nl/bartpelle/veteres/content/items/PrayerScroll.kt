package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.red
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 2017-01-08.
 */

object PrayerScroll {
	
	val PRAYER_SCROLL = 21034
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(PRAYER_SCROLL) @Suspendable {
			it.itemBox("You can make out some faded words on the ancient parchment. It appears to be an archaic invocation " +
					"of the gods! What would you like to learn?", PRAYER_SCROLL)
			when (it.options("Rigour", "Augury", "Preserve", "Cancel")) {
				1 -> unlockPrayer(it, Varbit.UNLOCK_RIGOUR)
				2 -> unlockPrayer(it, Varbit.UNLOCK_AUGURY)
				3 -> unlockPrayer(it, Varbit.UNLOCK_PRESERVE)
			}
		}
	}
	
	@Suspendable private fun unlockPrayer(it: Script, varbit: Int) {
		val player = it.player()
		val prayerName = when (varbit) {
			Varbit.UNLOCK_RIGOUR -> "Rigour"
			Varbit.UNLOCK_AUGURY -> "Augury"
			else -> "Preserve"
		}
		
		if (player.varps().varbit(varbit) != 1) {
			if (player.inventory().remove(Item(PRAYER_SCROLL, 1), false).success()) {
				player.varps().varbit(varbit, 1)
				player.message("You've unlocked the prayer: ${prayerName.red()}.")
				it.itemBox("You study the scroll and learn a new prayer: ${prayerName.red()}.", PRAYER_SCROLL)
			} else {
				player.message("You need a prayer scroll to unlock this prayer!")
			}
		} else {
			player.message("You've already unlocked this prayer.")
		}
		
	}
	
}
