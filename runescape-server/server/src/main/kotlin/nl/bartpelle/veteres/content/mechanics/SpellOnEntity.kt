package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj

/**
 * Created by Jak on 10/05/2016.
 */
@Suspendable object SpellOnEntity {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
		val player: Player = it.player()
		
		
		it.onInterrupt { it.player().face(null) }
		
		if (option == 1) { // Spell on player
			val data: IntArray = player.attrib<IntArray>(AttributeKey.INTERACTED_WIDGET_INFO)
			player.world().server().scriptRepository().triggerSpellOnEntity(player, data[0], data[1])
		} else if (option == 2) { // Spell on NPC
			val data: IntArray = player.attrib<IntArray>(AttributeKey.INTERACTED_WIDGET_INFO)
			player.world().server().scriptRepository().triggerSpellOnEntity(player, data[0], data[1])
		} else if (option == 3) { // Spell on object
			val obj: MapObj = player.attrib<MapObj>(AttributeKey.INTERACTION_OBJECT)
			if (obj.valid(player.world())) {
				val data: IntArray = player.attrib<IntArray>(AttributeKey.INTERACTED_WIDGET_INFO)
				player.world().server().scriptRepository().triggerSpellOnObject(player, data[0], data[1], obj)
			}
		} else if (option == 4) { // Spell on gitem
			val gitem: GroundItem = player.attrib<GroundItem>(AttributeKey.INTERACTED_GROUNDITEM)
			if (player.world().groundItemValid(gitem)) {
				val data: IntArray = player.attrib<IntArray>(AttributeKey.INTERACTED_WIDGET_INFO)
				player.world().server().scriptRepository().triggerSpellOnGroundItem(player, data[0], data[1], gitem)
			}
		}
	}
}