package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.util.L10n
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Jak on 20/09/2016.
 * Use ::modlog to open. ::pt to test (js command)
 */
object PunishmentLog {
	
	// a record within the log.
	class Punishment(val uuid: Int, val offenderName: String, val punisherName: String, val punishment: String, var reason: String, val time: String)
	
	// The log
	val OFFENCE_LIST = ArrayList<Punishment>()
	
	// We need a way to reference punishments after adding them to altar the punishment reason at a later time.
	var INCREMENTING_PUNISHMENT_UUID = 0
	
	// Display the log.
	@JvmStatic fun displayLog(player: Player) {
		// Clear the interface
		QuestGuide.clear(player)
		
		// Text to render onto the quest list interface
		val text = ArrayList<String>()
		text.add("<img=1> Punishment Log (time is GMT+1)")
		text.add("")

		// List history
		if (OFFENCE_LIST.size == 0) {
			text.add("(no offences!)")
		} else {
			for (i in 0..Math.min(30, OFFENCE_LIST.size - 1)) {
				val o = OFFENCE_LIST.get(i)
				text.add("" + o.uuid + ". [" + o.time + "] <col=" + COL + ">" + o.offenderName + "</col> was " + o.punishment + " by <col=" + COL + ">" + o.punisherName + "</col>")
				text.add("For reason: " + o.reason)
				text.add("")
			}
		}
		
		// Draw that text
		for (i in 2..2 + text.size - 1) {
			player.interfaces().text(275, i, text[i - 2])
		}
		
		// Display
		QuestGuide.open(player)
	}
	
	val DATETIME_FORMAT = "dd/MM HH:mm"
	val COL = "9900ff"
	
	// Append a punishment
	@JvmStatic fun append(offender: String, punisher: Player, reason: String, punishment: String): Int {
		val crown = if (punisher.helper()) "<img=36>" else if (punisher.privilege().eligibleTo(Privilege.ADMIN)) "<img=1>" else "<img=0>"
		val punisherName = punisher.name()
		OFFENCE_LIST.add(0, Punishment(INCREMENTING_PUNISHMENT_UUID++, L10n.formatChatMessage(offender), crown + L10n.formatChatMessage(punisherName), punishment, reason, SimpleDateFormat(DATETIME_FORMAT).format(Date())))
		return INCREMENTING_PUNISHMENT_UUID - 1
	}
	
	// Find a punishment via it's ID then set the reason
	@JvmStatic fun updateReason(uuid: Int, reason: String) {
		val log: Punishment? = OFFENCE_LIST.find { offence -> offence.uuid == uuid }
		if (log != null) {
			log.reason = reason
		}
	}
	
	// Find a offence entry for its ID
	@JvmStatic fun forUuid(uuid: Int): Punishment? {
		return OFFENCE_LIST.find { offence -> offence.uuid == uuid }
	}
	
	// Set a reason for a punishment directly
	@JvmStatic fun updateReason(log: Punishment, reason: String) {
		log.reason = reason
	}
	
	// Populate with dummy stuff
	@JvmStatic fun test() {
		INCREMENTING_PUNISHMENT_UUID = 0
		OFFENCE_LIST.clear()
		OFFENCE_LIST.add(0, Punishment(INCREMENTING_PUNISHMENT_UUID++, "Shad2", "Tardis Fan", "banned", "DDoS", SimpleDateFormat(DATETIME_FORMAT).format(Date())))
		OFFENCE_LIST.add(0, Punishment(INCREMENTING_PUNISHMENT_UUID++, "Shad1", "Tardis Fan", "muted", "Spamming", SimpleDateFormat(DATETIME_FORMAT).format(Date())))
	}
	
	// Represents a Reason to be displayed on the from-selection prompt
	class Reason(val reason: String, val id: Int)
	
	// The list of pre-established punishment reasons
	var reasonsList: Array<Reason> = generate()
	
	// Quickie constructor
	fun generate(): Array<Reason> = arrayOf(
			Reason("Spamming / Offensive language", 1),
			Reason("DDoS", 2),
			Reason("Scamming", 3),
			Reason("Hacking", 4),
			Reason("RWT", 5),
			Reason("Advertising", 6),
			Reason("Punishment Evasion", 7),
			Reason("Threats / Privacy violation", 8)
	)


	@JvmField val addDamper: Function1<Script, Unit> = s@ @Suspendable {
		val word = it.inputString("Type a new word to damper")
		if (word.length > 0) {
			Censor.damperedWords.add(word)
			it.player().message("Added dampered word: $word.")
		}
	}

	@JvmField val deleteDamper: Function1<Script, Unit> = s@ @Suspendable {
		val word = it.inputString("Type a word to remove as dampered")
		if (word.length > 0) {
			if (Censor.damperedWords.removeIf { d -> d.toLowerCase().equals(word.toLowerCase()) })
				it.player().message("Removed dampered word: $word.")
		}
	}
}