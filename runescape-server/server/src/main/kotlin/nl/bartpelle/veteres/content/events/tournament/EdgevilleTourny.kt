package nl.bartpelle.veteres.content.events.tournament

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.fs.MapDefinition
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.instance.InstancedMap
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.net.message.game.command.SetPlayerOption
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Jak on 25/08/2016.
 */
object EdgevilleTourny {
	
	@JvmStatic var OPEN_TO_PLAYERS = true // Tourny instances in general
	@JvmStatic var OPEN_TO_STAFF = true // Testing
	
	@JvmStatic val OPEN_AREAS = booleanArrayOf(false, false, false, false, false) // For each instance
	@JvmStatic var ACCESSABLE_AREAS = booleanArrayOf(false, false, false, false, false)
	@JvmStatic val PJ_TIMER_LENGTH = longArrayOf(4500L, 4500L, 4500L, 4500L, 4500L)
	@JvmStatic var UNNOTE_ON_BANKS = booleanArrayOf(false, false, false, false, false)
	@JvmStatic val PLR_COUNT_INSIDE = intArrayOf(0, 0, 0, 0, 0) // For each instance
	@JvmStatic val EVENT_INSTANCES = arrayOfNulls<InstancedMap>(5) // For each instance
	
	@JvmStatic val ENTER = arrayOfNulls<Tile>(5) // For each instance
	@JvmStatic val DEATH = arrayOfNulls<Tile>(5) // For each instance
	@JvmStatic val BOTTOM_LEFTS = arrayOfNulls<Tile>(5) // For each instance
	
	val DOM_EXIT = Tile(3108, 3506)
	val DOM_TILE = Tile(3110, 3501)
	
	/**
	 * A list of Player instances which have been invited by Staff to be allowed access past the Wilderness Ditch in the Edgeville Tournament Instace
	 */
	@JvmStatic val INVITED = ArrayList<Any>(64)
	
	val DOM = 1120 // Npc ID
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		// Chat to Dom
		repo.onNpcOption1(DOM) @Suspendable {
			val basicStaff = it.player().privilege().eligibleTo(Privilege.MODERATOR) || it.player().helper()
			it.chatNpc("Hello there, " + it.player().name() + "! I'm here to provide<br> access to special areas, used for server<br>Events and Tournaments.", DOM)
			if ((it.player().privilege() == Privilege.PLAYER && !OPEN_TO_PLAYERS) || (basicStaff && !OPEN_TO_STAFF)) {
				it.chatNpc("The areas are currently closed and cannot be accessed though. When an Event is hosted these areas may open up for use.", DOM)
				it.chatPlayer("Thanks for the info. See you around!")
			} else {
				if (basicStaff) {
					it.chatNpc("As a staff member, I can offer you customization <br>options as well as teleports into the arenas.", DOM)
					when (it.options("Customize arenas", "Teleports", "Set pj-timer length", "Toggle un-noting on banks")) {
						1 -> {
							showTournamentToggleOptions(it)
						}
						2 -> offerDomTeleports(it)
						3 -> pjTimerSet(it)
						4 -> unnotingOnBanksSet(it)
					}
				}
			}
		}
		
		// Quick teleports on Dom
		repo.onNpcOption2(DOM) @Suspendable {
			val basicStaff = it.player().privilege().eligibleTo(Privilege.MODERATOR) || it.player().helper()
			if (it.player().privilege() == Privilege.PLAYER && !OPEN_TO_PLAYERS) {
				it.chatNpc("Event instanced areas are currently not open for use.", DOM)
			} else if (basicStaff && !OPEN_TO_STAFF) {
				it.chatNpc("Event instanced areas are currently closed, even for staff.", DOM)
			} else {
				offerDomTeleports(it)
			}
		}
		
		// On world init, setup instances
		repo.onWorldInit {
			val world: World = it.ctx<World>()
			setupEdgevilleInstance(world)
			setupPlateauInstance(world)
			setupGladeInstance(world)
			setupTurrentsInstance(world)
			
			// Make the Tile which Dom stands on unwalkable by players.
			if (world.realm().isPVP) {
				world.definitions().get(MapDefinition::class.java, DOM_TILE.region())?.addFloor(world.definitions(), 0, DOM_TILE.x and 63, DOM_TILE.z and 63, true)
			}
		}
		
		// Exit portal (disabled cos clan wars)
		/*repo.onObject(26728) {
			if (it.interactionObject().tile().equals(3291, 4988) || it.interactionObject().tile().equals(
					PVPAreas.resolveBasic(Tile(3291, 4988), BOTTOM_LEFTS[1]?: return@onObject, EVENT_INSTANCES[1]?: return@onObject))) // Exit
				teleportToTile(it, DOM_EXIT)
			PLR_COUNT_INSIDE[1] -= 1
		}*/
		
		// The "invite" option on players available to staff.
		repo.onPlayerOption7 s@ {
			val basicStaff = it.player().privilege().eligibleTo(Privilege.MODERATOR) || it.player().helper()
			if (basicStaff) {
				val target: Entity = it.target() ?: return@s
				if (target.isPlayer) {
					val other = target as Player
					val inside = INVITED.contains(other.id())
					val type = if (inside) "Uninvite " else "Invite "
					if (it.options(type + "<col=FF0000>" + other.name() + "</col> into the battle ground.", "Never mind.") == 1) {
						if (inside) {
							INVITED.remove(other.id())
							other.message("<col=FF0000>" + it.player().name() + "</col> has revoked your access into the battle grounds.")
							it.player().message("You've revoked ${target.name().col("FF0000")}'s access to fight in the battle grounds.")
						} else {
							INVITED.add(other.id())
							other.message("<col=FF0000>" + it.player().name() + "</col> has given you access into the battle grounds.")
							it.player().message("You've granted ${target.name().col("FF0000")}'s access to fight in the battle grounds.")
						}
					}
				}
			}
		}
	}

	@Suspendable private fun pjTimerSet(it: Script) {
		val arenaId = it.optionsTitled("Apply to which arena? ", "Edge", "Plateau", "Sylvan Glade", "Turrets") - 1
		val len = it.inputInteger("Change pj timer (millisecs) default 4500 (=7.5 rounds to 8 ticks). Currently ${PJ_TIMER_LENGTH[arenaId]}")
		if (arenaId in 0 .. PJ_TIMER_LENGTH.size - 1)
			PJ_TIMER_LENGTH[arenaId] = len.toLong()
		it.messagebox("Arena $arenaId pj timer now $len.")
	}

	@Suspendable private fun unnotingOnBanksSet(it: Script) {
		val arenaId = it.optionsTitled("Apply to which arena? ", "Edge", "Plateau", "Sylvan Glade", "Turrets") - 1
		UNNOTE_ON_BANKS[arenaId] = !UNNOTE_ON_BANKS[arenaId]
		it.messagebox("Arena $arenaId un-note on banks now ${if (UNNOTE_ON_BANKS[arenaId]) "blocked" else "enabled"}.")
	}

	// Instance generation
	private fun setupPlateauInstance(world: World) {
		val exit = Tile(3092, 3500)
		BOTTOM_LEFTS[1] = Tile(3264, 4928)
		val botleft = BOTTOM_LEFTS[1] ?: return
		val spot1 = world.allocator().allocate(64, 64, exit).get()
		spot1.set(0, 0, botleft.transform(0, 0), botleft.transform(64, 64), 0, true)
		spot1.persist().setDangerous(true).setMulti(false).setIdentifier(InstancedMapIdentifier.TOURNY_PLATEAU)
		EVENT_INSTANCES[1] = spot1
		ENTER[1] = PVPAreas.resolveBasic(Tile(3292, 4984), botleft, spot1)
		DEATH[1] = ENTER[1]
		PVPAreas.safearea_coords.add(PVPAreas.tileToArea(PVPAreas.resolveBasic(Tile(3290, 4983), botleft, spot1), 7, 5))
		world.spawnObj(MapObj(PVPAreas.resolveBasic(Tile(3295, 4987), botleft, spot1), 26707, 10, 2))
	}
	
	// Instance generation
	private fun setupGladeInstance(world: World) {
		val exit = Tile(3092, 3500)
		BOTTOM_LEFTS[2] = Tile(3392, 4992)
		val corner = BOTTOM_LEFTS[2] ?: return
		val map = world.allocator().allocate(64, 64, exit).get()
		map.set(0, 0, corner.transform(0, 0), corner.transform(64, 64), 0, true)
		map.persist().setDangerous(true).setMulti(false).setIdentifier(InstancedMapIdentifier.TOURNY_GLADE)
		EVENT_INSTANCES[2] = map
		ENTER[2] = PVPAreas.resolveBasic(Tile(3418, 5004), corner, map)
		DEATH[2] = ENTER[2]
	}
	
	// Instance generation
	private fun setupTurrentsInstance(world: World) {
		val exit = Tile(3092, 3500)
		BOTTOM_LEFTS[3] = Tile(3136, 5000)
		val corner = BOTTOM_LEFTS[3] ?: return
		val map = world.allocator().allocate(128, 128, exit).get()
		map.set(0, 0, corner.transform(0, 0), corner.transform(128, 128), 0, true)
		map.persist().setDangerous(true).setMulti(false).setIdentifier(InstancedMapIdentifier.TOURNY_TURRENTS)
		EVENT_INSTANCES[3] = map
		ENTER[3] = PVPAreas.resolveBasic(Tile(3161, 5055), corner, map)
		DEATH[3] = ENTER[3]
	}
	
	// Instance generation
	@JvmStatic fun setupEdgevilleInstance(world: World) {
		val exit = Tile(3092, 3500)
		BOTTOM_LEFTS[0] = Tile(3064, 3472)
		val corner = BOTTOM_LEFTS[0] ?: return
		val map = world.allocator().allocate(64 * 2, 64 * 2, exit).get()
		map.set(0, 0, corner.transform(0, 0), corner.transform(64, 16 * 6), 0, true)
		map.persist().setDangerous(true).setMulti(false).setIdentifier(InstancedMapIdentifier.TOURNY_EDGE)
		EVENT_INSTANCES[0] = map
		ENTER[0] = PVPAreas.resolveBasic(Tile(3087, 3516), corner, map)
		DEATH[0] = ENTER[0]
		PVPAreas.safearea_coords.add(PVPAreas.tileToArea(PVPAreas.resolveBasic(Tile(3062, 3502), corner, map), 64, 22))
		world.spawnObj(MapObj(PVPAreas.resolveBasic(Tile(3090, 3516), corner, map), 26707, 10, 3))
		world.registerNpc(Npc(DOM, world, PVPAreas.resolveBasic(Tile(3084, 3518), corner, map)))
	}
	
	// Ability to open/close areas, with instance size (player count) included.
	@Suspendable fun showTournamentToggleOptions(it: Script) {
		when (it.options("Open / Close areas", "Toggle access")) {
			1 -> openOrClose(it)
			2 -> toggleAccess(it)
		}
	}
	
	@Suspendable fun toggleAccess(it: Script) {
		val action1 = arrayOf(if (ACCESSABLE_AREAS[0]) "Stop access for " else "Open access for ",
				if (ACCESSABLE_AREAS[1]) "Stop access for " else "Open access for ",
				if (ACCESSABLE_AREAS[2]) "Stop access for " else "Open access for ",
				if (ACCESSABLE_AREAS[3]) "Stop access for " else "Open access for ",
				if (ACCESSABLE_AREAS[4]) "Stop access for " else "Open access for ")
		
		val pCounts = arrayOf(" (" + PLR_COUNT_INSIDE[0] + " players)", " (" + PLR_COUNT_INSIDE[1] + " players)", " (" + PLR_COUNT_INSIDE[2] + " players)", " (" + PLR_COUNT_INSIDE[3] + " players)", "(" + PLR_COUNT_INSIDE[3] + " players)")
		
		val end = arrayOf(if (ACCESSABLE_AREAS[0]) pCounts[0] else "", if (ACCESSABLE_AREAS[1]) pCounts[1] else "", if (ACCESSABLE_AREAS[2]) pCounts[2] else "", if (ACCESSABLE_AREAS[3]) pCounts[3] else "", if (ACCESSABLE_AREAS[3]) pCounts[4] else "")
		
		val choice = it.options(action1[0] + "Edgeville" + end[0],
				action1[1] + "Clanwars arena 1 (Plateau)" + end[1],
				action1[2] + "Clanwars arena 2 (Sylvan Glade)" + end[2],
				action1[3] + "Clanwars arena 3 (Turrets)" + end[3],
                action1[4] + "Mage Bank" + end[4])
		
		if (choice == 3 || choice == 4) {
			it.messagebox("Unfortunately this arena is not currently functional.")
			showTournamentToggleOptions(it)
			return
		}
		
		// Toggle open
		if (ACCESSABLE_AREAS[choice - 1])
			ACCESSABLE_AREAS[choice - 1] = false
		else
			ACCESSABLE_AREAS[choice - 1] = true
		
		val place = when (choice) {
			1 -> "Edgeville"
			2 -> "Clanwars 1 (Plateau)"
			3 -> "Clanwars 2 (Sylvan Glade)"
			4 -> "Clanwars 3 (Turrets)"
            5 -> "Mage Bank"
			else -> ""
		}
		
		it.chatNpc(place + " is now " + (if (ACCESSABLE_AREAS[choice - 1]) "accessible" else "not accessible") + " for people to enter and compete." +
				" The area is still open, those in it are still there.", DOM)
	}
	
	@Suspendable fun openOrClose(it: Script) {
		val action1 = arrayOf(if (OPEN_AREAS[0]) "Close " else "Open ",
				if (OPEN_AREAS[1]) "Close " else "Open ",
				if (OPEN_AREAS[2]) "Close " else "Open ",
				if (OPEN_AREAS[3]) "Close " else "Open ",
                if (OPEN_AREAS[4]) "Close " else "Open ")
		
		val pCounts = arrayOf(" (" + PLR_COUNT_INSIDE[0] + " players)", " (" + PLR_COUNT_INSIDE[1] + " players)", " (" + PLR_COUNT_INSIDE[2] + " players)", " (" + PLR_COUNT_INSIDE[3] + " players)", " (" + PLR_COUNT_INSIDE[4] + " players)")
		
		val end = arrayOf(if (OPEN_AREAS[0]) pCounts[0] else "", if (OPEN_AREAS[1]) pCounts[1] else "", if (OPEN_AREAS[2]) pCounts[2] else "", if (OPEN_AREAS[3]) pCounts[3] else "", if (OPEN_AREAS[4]) pCounts[4] else "")
		
		val choice = it.options(action1[0] + "Edgeville" + end[0],
				action1[1] + "Clanwars arena 1 (Plateau)" + end[1],
				action1[2] + "Clanwars arena 2 (Sylvan Glade)" + end[2],
				action1[3] + "Clanwars arena 3 (Turrets)" + end[3],
                action1[4] + "Mage Bank" + end[4])
		
		if (choice == 3 || choice == 4) {
			it.messagebox("Unfortunately this arena is not currently functional.")
			showTournamentToggleOptions(it)
			return
		}
		
		// If the area is currently open, so we'll be closing it.
		val closing = OPEN_AREAS[choice - 1]
		
		// Toggle open
		if (!closing) {
			OPEN_AREAS[choice - 1] = true
			ACCESSABLE_AREAS[choice - 1] = true
			statusMessage(it, choice)
		}
		
		if (closing) {
			it.messagebox("Closing this area will kick all the players out<br> and teleport them to Dom Onion at the Edgeville furnace. " +
					"<br>Any items on the floor will be spawned next to Dom")
			it.messagebox("so players can retieve them. Are you SURE you <br>want to kick eveyone out?")
			if (it.options("Yes, kick everyone and close the area.", "Whoopsie, nope!") == 1) {
				var instance: InstancedMap = EVENT_INSTANCES[choice - 1] ?: return
				it.player().world().players().forEachInAreaKt(instance, { other ->
					other.world().server().scriptExecutor().executeScript(other, @Suspendable { s ->
						teleportToTile(s, Tile(3108, 3500))
						PLR_COUNT_INSIDE[choice - 1] -= 1
						other.message("<col=FF0000>This arena has been closed, and you've been teleported back to Dominic.")
						other.write(UpdateStateCustom(1)) // pvp world
						other.putattrib(AttributeKey.UPDATE_STATE_CUSTOM, true)
					})
				})
				val removed = ArrayList<GroundItem>()
				ArrayList<GroundItem>(it.player().world().groundItems()).forEach { gitem ->
					if (gitem.tile().inArea(instance)) {
						// teleport the gitem to a place where the player can run back for it! next to dominic
						if (it.player().world().removeGroundItem(gitem)) {
							removed.add(gitem)
						}
					}
				}
				if (removed.size > 0) {
					removed.forEach { gitem ->
						it.player().world().spawnGroundItem(GroundItem(it.player().world(), Item(gitem.item()), DOM_TILE, gitem.owner()))
						val owner = it.player().world().playerForId(gitem.owner())
						if (owner.isPresent) {
							owner.get().write(SetHintArrow(gitem.tile()))
							owner.get().message("%d x %s was moved out of the arena to under Dominic at the Edgeville furnace.", gitem.item().amount(), gitem.item().name(it.player().world()))
						}
					}
				}
				OPEN_AREAS[choice - 1] = false
				ACCESSABLE_AREAS[choice - 1] = false
				statusMessage(it, choice)
			}
		}
		
		showTournamentToggleOptions(it)
	}
	
	@Suspendable fun statusMessage(it: Script, choice: Int) {
		
		val place = when (choice) {
			1 -> "Edgeville"
			2 -> "Clanwars 1 (Plateau)"
			3 -> "Clanwars 2 (Sylvan Glade)"
			4 -> "Clanwars 3 (Turrets)"
            5 -> "Mage Bank"
			else -> ""
		}
		it.chatNpc(place + " is now " + (if (OPEN_AREAS[choice - 1]) "open" else "closed") + ".", DOM)
	}
	
	/**
	 * Offer teleport options into arena, with text strikethrough if the arena is closed.
	 */
	@Suspendable fun offerDomTeleports(it: Script) {
		it.chatNpc("These areas are currently open for use. I can offer you teleports to them. Which one do you wish to visit? <col=FF0000>Warning: They are all dangerous, " +
				"and you will lose items on death.</col>", DOM)
		val s = arrayOf(if (OPEN_AREAS[0]) "" else "<str>", if (OPEN_AREAS[1]) "" else "<str>", if (OPEN_AREAS[2]) "" else "<str>", if (OPEN_AREAS[3]) "" else "<str>", if (OPEN_AREAS[4]) "" else "<str>")
		val choice = it.options(s[0] + "Edgeville", s[1] + "Clanwars arena 1 (Plateau)", s[2] + "Clanwars arena 2 (Sylvan Glade)", s[3] + "Clanwars arena 3 (Turrets)", s[4] + "Mage Bank")
		val idx = choice - 1
		if (idx < 0 || idx >= OPEN_AREAS.size) return // Invalid
		
		if (!OPEN_AREAS[idx]) {
			it.chatNpc("This arena is currently closed.", DOM)
			offerDomTeleports(it)
		} else {
			if (ACCESSABLE_AREAS[idx]) {
				it.player().putattrib(AttributeKey.UPDATE_STATE_CUSTOM, true)
				it.player().write(UpdateStateCustom(47)) // pvp world
				teleportToTile(it, PVPAreas.resolveBasic(ENTER[idx] ?: return, EVENT_INSTANCES[idx]?.bottomLeft() ?: return, EVENT_INSTANCES[idx] ?: return))
				PLR_COUNT_INSIDE[idx] += 1
				val basicStaff = it.player().privilege().eligibleTo(Privilege.MODERATOR) || it.player().helper()
				if (basicStaff) {
					it.player().write(SetPlayerOption(7, false, "Events"))
				}
			} else {
				it.messagebox("This area is open but currently inaccessable. The tournament is probably in progress.")
			}
		}
	}
	
	/**
	 * Teleport a player somewhere
	 */
	@Suspendable private fun teleportToTile(it: Script, tile: Tile) {
		it.player().lockNoDamage()
		it.player().animate(714);
		it.player().graphic(111, 92, 0)
		it.delay(3)
		it.player().teleport(tile)
		it.player().animate(-1)
		it.player().timers().cancel(TimerKey.FROZEN)
		it.player().timers().cancel(TimerKey.REFREEZE)
		it.player().write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
		it.player().unlock()
	}
	
	/**
	 * Is the given entity inside one of our Tournament instances?
	 */
	@JvmStatic fun insideTournament(entity: Entity): Boolean {
		return inTournament(entity.tile())
	}
	
	/**
	 * Are we in an instance?
	 */
	@JvmStatic fun inTournament(t: Tile): Boolean {
		return EVENT_INSTANCES[0]?.contains(t) ?: false || EVENT_INSTANCES[1]?.contains(t) ?: false || EVENT_INSTANCES[2]?.contains(t) ?: false || EVENT_INSTANCES[3]?.contains(t) ?: false
	}
	
	/**
	 * Side spots are INSIDE the instance, but in a safe zone. not outside in the normal game world.
	 */
	@JvmStatic fun findDeathTileFor(player: Player): Tile? {
		if (EVENT_INSTANCES[0]?.contains(player) ?: false) {
			return DEATH[0]
		} else if (EVENT_INSTANCES[1]?.contains(player) ?: false) {
			return DEATH[1]
		} else if (EVENT_INSTANCES[2]?.contains(player) ?: false) {
			return DEATH[2]
		} else if (EVENT_INSTANCES[3]?.contains(player) ?: false) {
			return DEATH[3]
		}
		return null
	}
}