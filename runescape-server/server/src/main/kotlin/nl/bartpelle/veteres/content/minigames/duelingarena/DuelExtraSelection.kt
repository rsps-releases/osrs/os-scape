package nl.bartpelle.veteres.content.minigames.duelingarena

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Jak on 29/02/2016.
 */
@Suspendable class DuelExtraSelection {
	
	@Suspendable companion object {
		public val script: Function1<Script, Unit> = @Suspendable {
			opts1(it)
			
		}
		
		@Suspendable private fun opts1(it: Script) {
			val player: Player = it.player()
			
			when (it.options("Abyssal whip & dds only.", "Tentacle whip & dds only.", "Normal stake (any 1h weapon).", "Tentacle whip only.")) {
				1 -> {
					player.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, 1)
					player.message("You selected duel type: Abyssal Whip & DDS only.")
					Staking.acceptDuel(player)
				}
				2 -> {
					player.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, 2)
					player.message("You selected duel type: Tentacle Whip & DDS only.")
					Staking.acceptDuel(player)
				}
				3 -> {
					player.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, 3)
					player.message("You selected duel type: Normal stake.")
					Staking.acceptDuel(player)
				}
				4 -> {
					player.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, 4)
					player.message("You selected duel type: Tentacle whip only.")
					Staking.acceptDuel(player)
				}
			}
		}
	}
}