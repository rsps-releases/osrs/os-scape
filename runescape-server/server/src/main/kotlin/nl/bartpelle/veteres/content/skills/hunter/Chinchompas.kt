package nl.bartpelle.veteres.content.skills.hunter

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Tuple
import java.util.*

/**
 * Created by Jak on 13/12/2015.
 *
 */
object Chinchompas {
	
	@JvmStatic var CHIN_HUNTING_ENABLED: Boolean = true
	
	val BOX_TRAP_ITEM = 10008
	val BOX_TRAP_OBJECT = 9380

	val GREY_CHIN_NPC = 2910
	val RED_CHIN_NPC = 2911
	val BLACK_CHIN_NPC = 2912
	
	val GREYCHIN_ITEM = 10033
	val REDCHIN_ITEM = 10034
	val BLACKCHIN_ITEM = 11959

	// Other npcs added on server init
	@JvmStatic val HUNTER_NPCS = arrayListOf(2910, 2911, 2912)
	
	val LAYTRAP_ANIM = 5208
	val CHIN_ESCAPE_ANIM = 5185
	
	val CLOSING_TRAP = 9381
	val SHAKINGTRAP = 9384 // 721 at black chins
	val CLOSED_TRAP = 9385
	// In order of N E S W
	val GREY_TRAPPED = intArrayOf(9386, 9387, 9388, 9389)
	val RED_TRAPPED = intArrayOf(9390, 9391, 9392, 9393)
	val BLACK_TRAPPED = intArrayOf(2025, 2026, 2028, 2029)
	
	@JvmStatic val TRAP_TIMEOUT = 1000 // ticks
	
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		// Place box trap down
		r.onItemOption1(BOX_TRAP_ITEM, s@ @Suspendable {
			if (!CHIN_HUNTING_ENABLED) return@s
			val player = it.player()
			if (Skills.disabled(player, Skills.HUNTER)) {
				return@s
			}
			if (failsPrechecks(player)) {
				return@s
			}
			player.animate(LAYTRAP_ANIM) // setup trap anim
			
			it.delay(3)
			player.inventory().remove(Item(BOX_TRAP_ITEM), true, it.itemUsedSlot())
			val box = placeBox(player)
			box.putattrib(AttributeKey.OWNING_PLAYER, Tuple(player.id(), player))
			updateBoxes(player, box)
			player.clearattrib(AttributeKey.INTERACTION_OBJECT)
			
			it.delay(1)
			player.walkTo(player.tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
			player.faceObj(box)
		})
		
		// Re-lay box trap
		r.onGroundItemOption2(BOX_TRAP_ITEM, s@ @Suspendable {
			if (!CHIN_HUNTING_ENABLED) return@s
			val player = it.player()
			
			if (Skills.disabled(player, Skills.HUNTER)) {
				return@s
			}
			if (failsPrechecks(player)) {
				return@s
			}
			
			player.animate(LAYTRAP_ANIM) // setup trap anim
			it.delay(3)
			
			// Spawn a new one
			val grounditem: GroundItem = player.attrib(AttributeKey.INTERACTED_GROUNDITEM)
			player.world().removeGroundItem(grounditem)
			val box = placeBox(player)
			box.putattrib(AttributeKey.OWNING_PLAYER, Tuple(player.id(), player))
			updateBoxes(player, box)
			player.clearattrib(AttributeKey.INTERACTION_OBJECT)
			
			it.delay(1)
			player.walkTo(player.tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
			player.faceObj(box)
		})
		
		// Any object option of a box trap
		r.onObject(BOX_TRAP_OBJECT, s@ @Suspendable {
			val obj = it.originalInteractionObject()
			if (obj.owner(it.player().world()) != it.player) {
				it.message("This trap does not belong to you.")
				return@s
			}
			val player = it.player()
			val world = player.world()
			val interact_type: Int = player.attrib(AttributeKey.INTERACTION_OPTION)
			
			if (interact_type == 1) { // Dismantle
				player.animate(LAYTRAP_ANIM)
				it.delay(2)
				if (obj.valid(world)) {
					world.removeObj(obj)
					boxtraps(player).remove(obj)
					player.inventory().add(Item(BOX_TRAP_ITEM), false)
					player.animate(-1)
				}
			} else if (interact_type == 2) { // Investigat
				player.message("This trap is empty.")
			}
		})
		
		// The cycle for npcs finding and getting stuck in traps
		intArrayOf(GREY_CHIN_NPC, RED_CHIN_NPC, BLACK_CHIN_NPC).forEach { chinNpcId ->
			r.onNpcSpawn(chinNpcId) @Suspendable {
				val npc = it.npc()
				it.clearContext()
				while (npc.index() != -1) {
					chinchompaNpcCycle(it, npc)
				}
			}
		}
		
		// Loot the shaking trap with a catch in it.
		r.onObject(SHAKINGTRAP, s@ @Suspendable {
			val obj = it.interactionObject()
			if (obj.owner(it.player().world()) != it.player) {
				it.message("This trap does not belong to you.")
				return@s
			}
			it.player().lockDelayDamage()
			it.player().animate(5212)
			it.delay(2)
			if (obj.valid(it.player().world())) {
				it.player().world().removeObj(obj)
				boxtraps(it.player()).remove(obj)
				val loottype = obj.attribOr<Int>(AttributeKey.HUNTER_INFO, 0).shr(1).and(7)
				var loot = if (loottype == 1) GREYCHIN_ITEM else if (loottype == 2) REDCHIN_ITEM else BLACKCHIN_ITEM
				
				// Req space is 1 (the box trap) plus either blood money or stackable chinchompas
				var req = 1 + if (it.player().inventory().count(loot) > 0) 0 else 1
				if (it.player().inventory().freeSlots() < req) {
					it.player().message("You don't have enough inventory space to carry the loot.")
				} else {
					it.player().skills().__addXp(Skills.HUNTER, if (loot == GREYCHIN_ITEM) 198.25 else if (loot == REDCHIN_ITEM) 265.0 else 315.0)
					val qty = 1
					it.player().inventory().add(Item(loot, qty), true)
					
					AchievementAction.processCategoryAchievement(it.player(), AchievementCategory.HUNTER, loot)
					
					it.player().inventory() += BOX_TRAP_ITEM
					tryforpet(it.player(), if (loot == GREYCHIN_ITEM) Pet.BABY_CHINCHOMPA_GREY else if (loot == REDCHIN_ITEM) Pet.BABY_CHINCHOMPA_RED else Pet.BABY_CHINCHOMPA_BLACK)
				}
				it.message("You dismantle the trap.")
			}
			it.player().unlock()
		})
		
		// Pick up a failed trap
		r.onObject(CLOSED_TRAP, s@ @Suspendable {
			val obj = it.interactionObject()
			if (obj.owner(it.player().world()) != it.player) {
				it.message("This trap does not belong to you.")
				return@s
			}
			it.player().lockDelayDamage()
			it.player().animate(5212)
			it.delay(2)
			if (obj.valid(it.player().world())) {
				if (it.player().inventory().freeSlots() < 1) {
					it.player().message("You don't have enough inventory space to carry the loot.")
				} else {
					it.player().inventory() += BOX_TRAP_ITEM
					it.player().world().removeObj(obj)
					boxtraps(it.player()).remove(obj)
					it.message("You dismantle the trap.")
				}
			}
			it.player().unlock()
		})
		
	}
	
	private fun tryforpet(player: Player, pet: Pet) {
		val chance = if (player.world().realm().isPVP) 5000 else if (pet == Pet.BABY_CHINCHOMPA_GREY) 131395 else if (pet == Pet.BABY_CHINCHOMPA_RED) 98373 else 82758
		if (player.world().rollDie((chance * player.mode().skillPetMod()).toInt(), 1)) {
			if (!PetAI.hasUnlocked(player, pet)) {
				// Unlock the varbit. Just do it, rather safe than sorry.
				player.varps().varbit(pet.varbit, 1)
				
				// RS tries to add it as follower first. That only works if you don't have one.
				val currentPet = player.pet()
				if (currentPet == null) {
					player.message("You have a funny feeling like you're being followed.")
					PetAI.spawnPet(player, pet, false)
				} else {
					// Sneak it into their inventory. If that fails, fuck you, no pet for you!
					if (player.inventory().add(Item(pet.item), true).success()) {
						player.message("You feel something weird sneaking into your backpack.")
					} else {
						player.message("Speak to Probita to claim your pet!")
					}
				}
				
				player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(pet.item).name(player.world())}.")
			} else {
				player.message("You have a funny feeling like you would have been followed...")
			}
		}
	}

	@JvmStatic fun hunterNpc(id: Int): Boolean {
		return id in HUNTER_NPCS
	}
	
	@Suspendable @JvmStatic fun chinchompaNpcCycle(it: Script, npc: Npc) {
		val world = npc.world()
		val requirement: Int = if (npc.id() == GREY_CHIN_NPC) 53 else if (npc.id() == RED_CHIN_NPC) 63 else 73
		// Let's only check for close traps every few ticks...
		//npc.sync().shout("pew pew")
		it.delay(4 + npc.world().random(2))
		
		var targTile: Tile? = null
		var trap: MapObj? = null
		
		// Let's locate a close box trap, and if the player is lucky, become interested in it...
		for (obj in npc.world().spawnedObjs()) {
			if (obj.id() == BOX_TRAP_OBJECT && obj.tile().distance(npc.tile()) <= 3) {
				val owner: Player = obj.owner(world) ?: continue
				if (owner.skills().xpLevel(Skills.HUNTER) < requirement) continue
				if (obj.attribOr<Int>(AttributeKey.HUNTER_INFO, 0).and(1) == 0) {
					obj.putattrib(AttributeKey.HUNTER_INFO, 1) // flag that an NPC is pathing towards this box. don't like 5 in 1 box ;)
					npc.putattrib(AttributeKey.HUNTER_INFO, 1) // Don't random walk during this stage.
					npc.faceObj(obj)
					//npc.sync().shout("target identified (I'm an attack helicopter)")
					targTile = npc.stepTowards(obj.tile(), 10)
					trap = obj
					break
				}
			}
		}
		
		// Should we be interested in a trap, either escape/get caught once we reach it.
		while (targTile != null && trap != null && !npc.dead() && !npc.frozen() && trap.valid(world)) {
			if (!npc.tile().equals(targTile)) {
				// Must be walking towards.
				it.delay(1)
			} else {
				// We've reached the trap
				it.delay(1)
				npc.lockNoDamage()
				val owner: Player = trap.owner(world) ?: break
				if (sucessfulChinchompaCatch(npc, owner)) { // Success
					if (trap.valid(world)) {
						world.removeObj(trap)
						trap.clearattrib(AttributeKey.OWNING_PLAYER)
						boxtraps(owner).remove(trap)
						
						// Get the right side
						var idx = 0
						if (npc.tile().equals(trap.tile().transform(0, 1))) { // North
							idx = 0
						} else if (npc.tile().equals(trap.tile().transform(0, -1))) { // South
							idx = 2
						} else if (npc.tile().equals(trap.tile().transform(1, 0))) { // East
							idx = 1
						} else if (npc.tile().equals(trap.tile().transform(-1, 0))) { // West
							idx = 3
						} else {
							//System.out.println("bad side- "+(npc.tile().x - trap.tile().x)+", "+(npc.tile().z-trap.tile().z))
						}
						
						// Equivilent of dying.. reset the npc
						npc.hidden(true)
						npc.teleport(npc.spawnTile())
						npc.faceTile(tileForDir(npc))
						npc.hp(npc.maxHp(), 0) // Heal up to full hp
						npc.animate(-1) // Reset death animation
						npc.clearDamagers() //Clear damagers
						npc.clearDamageTimes()
						npc.clearattrib(AttributeKey.TARGET)
						npc.putattrib(AttributeKey.HUNTER_INFO, 0)
						// Respawn it 5 seconds later
						it.runGlobal(world) @Suspendable { s ->
							s.clearContext()
							s.delay(8)
							npc.hidden(false)
							npc.unlock()
						}
						
						// Add a timeout to the capture
						it.runGlobal(world, @Suspendable { s ->
							s.clearContext()
							// Syntax requires !! as smart casting from possible null doesnt work in this context of a suspendable action
							val trap_array = if (npc.id() == GREY_CHIN_NPC) GREY_TRAPPED[idx] else if (npc.id() == RED_CHIN_NPC) RED_TRAPPED[idx] else BLACK_TRAPPED[idx]
							val closing = MapObj(trap!!.tile(), trap_array, trap!!.type(), trap!!.rot())
							world.spawnObj(closing)
							boxtraps(owner).add(closing)
							s.delay(2)
							if (closing.valid(world)) {
								boxtraps(owner).remove(closing)
								world.removeObj(closing)
								
								// Place the full trap
								val captured = MapObj(closing.tile(), SHAKINGTRAP, closing.type(), closing.rot())
								
								val loottype: Int = if (npc.id() == GREY_CHIN_NPC) 1 else if (npc.id() == RED_CHIN_NPC) 2 else 3
								captured.putattrib(AttributeKey.HUNTER_INFO, loottype.shl(1))
								captured.putattrib(AttributeKey.OWNING_PLAYER, Tuple(owner.id(), owner))
								captured.putattrib(AttributeKey.MAPOBJ_UUID, MapObj.INCREMENTING_MAPOBJ_UUID++)
								
								boxtraps(owner).add(captured)
								world.spawnObj(captured)
								
								trap!!.clearattrib(AttributeKey.HUNTER_INFO)
								// Timeout the result
								s.delay(TRAP_TIMEOUT) // 5 minutes to collect a successful catch
								while (true) {
									var sleep = false
									// The owner is picking it up! Or an NPC is pathing towards it.
									val trapOwner: Player? = captured.owner(world)
									if (trapOwner != null) {
										val ownerTarg: MapObj? = trapOwner.attribOr(AttributeKey.INTERACTION_OBJECT, null)
										if (ownerTarg != null && ownerTarg == captured) {
											sleep = true
										}
									}
									if (captured.attribOr<Int>(AttributeKey.HUNTER_INFO, 0).and(1) == 1) {
										sleep = true
									}
									if (sleep) {
										s.delay(5)
										//System.out.println("let's wait a bit... 1")
									} else {
										break
									}
								}
								if (captured.valid(world, true)) {
									world.removeObj(captured)
									boxtraps(owner).remove(captured)
									captured.clearattrib(AttributeKey.HUNTER_INFO)
									captured.clearattrib(AttributeKey.OWNING_PLAYER)
									world.spawnGroundItem(GroundItem(world, Item(BOX_TRAP_ITEM), captured.tile(), owner.id()))
									//System.out.println("h1")
								}
							}
						})
						
					}
				} else { // Failure
					if (trap.valid(world)) {
						// Remove the open trap
						trap.interactAble(false)
						npc.animate(CHIN_ESCAPE_ANIM)
						it.delay(1)
						world.removeObj(trap)
						
						trap.clearattrib(AttributeKey.OWNING_PLAYER)
						
						boxtraps(owner).remove(trap)
						
						// Now show the closing trap and failed
						it.runGlobal(world, @Suspendable { s ->
							s.clearContext()
							val closing = MapObj(trap!!.tile(), CLOSING_TRAP, trap!!.type(), trap!!.rot())
							world.spawnObj(closing)
							boxtraps(owner).add(closing)
							
							s.delay(1)
							npc.unlock()
							npc.putattrib(AttributeKey.HUNTER_INFO, 0)
							
							if (closing.valid(world)) {
								world.removeObj(closing)
								boxtraps(owner).remove(closing)
								
								val closed = MapObj(closing.tile(), CLOSED_TRAP, closing.type(), closing.rot())
								closed.putattrib(AttributeKey.OWNING_PLAYER, Tuple(owner.id(), owner))
								closed.putattrib(AttributeKey.MAPOBJ_UUID, MapObj.INCREMENTING_MAPOBJ_UUID++)
								boxtraps(owner).add(closed)
								world.spawnObj(closed)
								trap!!.clearattrib(AttributeKey.HUNTER_INFO)
								
								// Timeout the result
								s.delay(TRAP_TIMEOUT)
								while (true) {
									var sleep = false
									// The owner is picking it up! Or an NPC is pathing towards it.
									val trapOwner: Player? = closed.owner(world)
									if (trapOwner != null) {
										val ownerTarg: MapObj? = trapOwner.attribOr(AttributeKey.INTERACTION_OBJECT, null)
										if (ownerTarg != null && ownerTarg == closed) {
											sleep = true
										}
									}
									if (closed.attribOr<Int>(AttributeKey.HUNTER_INFO, 0).and(1) == 1) {
										sleep = true
									}
									if (sleep) {
										s.delay(5)
										//System.out.println("let's wait a bit... 2")
									} else {
										break
									}
								}
								if (closed.valid(world, true)) {
									world.removeObj(closed)
									boxtraps(owner).remove(closed)
									closed.clearattrib(AttributeKey.HUNTER_INFO)
									closed.clearattrib(AttributeKey.OWNING_PLAYER)
									world.spawnGroundItem(GroundItem(world, Item(BOX_TRAP_ITEM), closed.tile(), owner.id()))
									//System.out.println("h2")
								}
							}
						})
						
						
						// npc.sync().shout("not today 4head")
					}
				}
				return // We made it to the target. Job done.
			}
		}
		// We failed to make it to the target for some reason.. make sure the trap can once again be used by other targets.
		if (trap != null) {
			trap.putattrib(AttributeKey.HUNTER_INFO, 0)
			npc.putattrib(AttributeKey.HUNTER_INFO, 0)
		}
	}
	
	private fun sucessfulChinchompaCatch(npc: Npc, player: Player): Boolean {
		var successThreshold = 30
		val mylvl = if (player.world().realm().isPVP && player.skills().level(Skills.HUNTER) < 50) 50 else player.skills().level(Skills.HUNTER)
		val req: Int = if (npc.id() == GREY_CHIN_NPC) 53 else if (npc.id() == RED_CHIN_NPC) 63 else 73
		val boosts = boosts(player)
		val baseMaxLevel = 99
		val levelGap: Int = baseMaxLevel - req
		val overExperienced = Math.max(1, mylvl - req)
		val percentageGapSucess: Double = (100 - successThreshold) / levelGap.toDouble() // 70 / 46 diff = 1.5% more success per level
		val overSuccess = ((overExperienced * percentageGapSucess) * boosts).toInt()
		successThreshold += overSuccess // Maximum gain should be 70, which combined with threshold of 30 = 100% success rate.
		
		// Max success 95%
		// The last time I saw code like this, the Ariane 5 Cluster crashed midway through the takeoff.
		val roll = npc.world().random(if (BonusContent.isActive(player, BlessingGroup.PREDATOR)) 85 else 100)
		player.debug("Rolled %d/%d. Success threshold:%d.  Cap %d%%, lowest %d%%. Max base level:%d.  Gap:%d.  %% success per level:%f.  Over-level:%d Boosts=%f Over-sucess:%d",
				roll, 100, successThreshold, 95, 30, baseMaxLevel, levelGap, percentageGapSucess, overExperienced, boosts, overSuccess)
		if (successThreshold > 95) successThreshold = 95
		return roll < successThreshold
	}
	
	// Lowers the base max level, NOT equivilent to a percentage boost. This is custom.
	@JvmStatic fun boosts(player: Player): Double {
		var boost = 1.0
		if (player.equipment().hasAny(9948, 9949) || Equipment.wearingMaxCape(player)) // Lower the base level by 10 when wearing achievement cape.
			boost += 0.1
		val current = player.skills().level(Skills.HUNTER)
		val real = player.skills().xpLevel(Skills.HUNTER)
		if (current > real) // Lower the base level by difference in over-level
			boost += (current - real) / 100
		return boost
	}
	
	@JvmStatic fun tileForDir(npc: Npc): Tile { // TODO correct directions
		return when (npc.spawnDirection()) {
			1 -> npc.tile().transform(0, 0)
			else -> npc.tile().transform(0, 0)
		}
	}
	
	@JvmStatic fun onLogout(player: Player) {
		// Change box trap objects to ground items
		boxtraps(player).forEach { trap ->
			//player.debug("trap at %s valid %s", trap.tile(), trap.valid(player.world()))
			if (trap.valid(player.world())) {
				player.world().removeObj(trap)
				player.world().spawnGroundItem(GroundItem(player.world(), Item(BOX_TRAP_ITEM), trap.tile(), player.id()))
			}
		}
		//player.debug("traps size pre-clear %d", boxtraps(player).size)
		boxtraps(player).clear()
	}
	
	/**
	 * Prechecks before allowing hunter. Level checks, existing traps on the same spot, amt of traps already set up.
	 */
	fun failsPrechecks(player: Player): Boolean {
		if (!player.world().realm().isPVP && player.skills().level(Skills.HUNTER) < 2) {
			player.message("You don't have the required level for that.")
			return true
		}
		if (player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			player.message("You can't do that in here.")
			return true
		}
		// Check tile
		if (player.world().objByType(10, player.tile().x, player.tile().z, player.tile().level) != null) {
			player.message("You can't place a trap here.")
			return true
		}
		if (activeTraps(player) >= maxActiveTraps(player)) {
			player.message("You can't place any more traps.")
			return true
		}
		return false
	}
	
	/**
	 * Appends a MapObj trap instance to the Arraylist attribute.
	 */
	@Suspendable fun updateBoxes(player: Player, box: MapObj) {
		val boxes = boxtraps(player)
		boxes.add(box)
		player.putattrib(AttributeKey.BOXTRAPS_LIST, boxes)
	}
	
	/**
	 * Uses the ArrayList attribute mapped to the player to check how many tracked traps are set
	 */
	@Suspendable fun activeTraps(player: Player): Int {
		var active = 0
		boxtraps(player).forEach { trap ->
			if (trap.id() == BOX_TRAP_OBJECT) {
				active++
			}
		}
		return active
	}
	
	private fun boxtraps(player: Player): ArrayList<MapObj> {
		val map = player.attribOr<ArrayList<MapObj>>(AttributeKey.BOXTRAPS_LIST, java.util.ArrayList<MapObj>())
		player.putattrib(AttributeKey.BOXTRAPS_LIST, map)
		return map
	}
	
	/**
	 * Calculates maximum possible traps you can spawn depending on hunter level.
	 */
	@Suspendable fun maxActiveTraps(player: Player): Int {
		val hunterLevel: Int = player.skills().level(Skills.HUNTER)
		if (hunterLevel >= 80) return 5
		else if (hunterLevel >= 60) return 4
		else if (hunterLevel >= 40) return 3
		else if (hunterLevel >= 20) return 2
		else return 1
		//Not sure how to use WHEN statement with the greater/less than operators.
	}
	
	/**
	 * Registers a Box Trap object on the ground. x seconds later, it'll revert to a trap ground item, if it's still valid.
	 * Trap may already be unregistered (by logging or picking up)
	 */
	@Suspendable fun placeBox(player: Player): MapObj {
		val obj: MapObj = MapObj(player.tile(), BOX_TRAP_OBJECT, 10, 0)
		val world = player.world()
		player.world().server().scriptExecutor().executeScript(world, @Suspendable { s ->
			world.spawnObj(obj)
			obj.putattrib(AttributeKey.OWNING_PLAYER, Tuple(player.id(), player))
			obj.putattrib(AttributeKey.MAPOBJ_UUID, MapObj.INCREMENTING_MAPOBJ_UUID++)
			// Timeout the result
			s.delay(TRAP_TIMEOUT)
			while (true) {
				var sleep = false
				// The owner is picking it up! Or an NPC is pathing towards it.
				val owner: Player? = obj.owner(world)
				if (owner != null) {
					val objtarg: MapObj? = owner.attribOr(AttributeKey.INTERACTION_OBJECT, null)
					if (objtarg != null && objtarg == obj) {
						sleep = true
					}
				}
				if (obj.attribOr<Int>(AttributeKey.HUNTER_INFO, 0).and(1) == 1) {
					sleep = true
				}
				if (sleep) {
					s.delay(5)
					//System.out.println("let's wait a bit... 3")
				} else {
					break
				}
			}
			if (obj.valid(world, true)) {
				world.removeObj(obj)
				boxtraps(player).remove(obj)
				obj.clearattrib(AttributeKey.HUNTER_INFO)
				obj.clearattrib(AttributeKey.OWNING_PLAYER)
				world.spawnGroundItem(GroundItem(world, Item(BOX_TRAP_ITEM), obj.tile(), player.id()))
				//System.out.println("h3")
			}
		})
		return obj
	}
}