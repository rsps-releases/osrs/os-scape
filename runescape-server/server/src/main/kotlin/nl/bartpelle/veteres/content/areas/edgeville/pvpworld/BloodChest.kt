package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.achievements.pvp.PVPDiaryHandler
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailClueLevel
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets.clueScrollReward
import nl.bartpelle.veteres.content.treasuretrails.rewards.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 2016-12-09.
 */


object BloodChest {
	
	val BLOOD_CHEST = 14986
	val BLOODY_KEYS = intArrayOf(3606, 3608, 7297, 7299, 7302)
	val KEY_NAMES = mapOf(3606 to "easy", 3608 to "medium", 7297 to "hard", 7299 to "elite", 7302 to "master")
	
	val NIGEL = 6093
	val NIGEL_SHOUTS = arrayOf("Get away from my chest", "What do you think you're doing", "Don't touch that", "Get out of here")
	
	var playersWithKeys = mutableListOf<Player>()
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(BLOOD_CHEST) @Suspendable {
			val player = it.player()
			
			if (startedBloodySurvivalMinigame(player)) {
				if (hasRewardKey(player)) {
					when (getRewardKey(player)) {
						3606 -> openRewardCasket(it, TreasureTrailClueLevel.EASY, AttributeKey.EASY_CLUE_SCROLL, "easy", Item(3606))
						3608 -> openRewardCasket(it, TreasureTrailClueLevel.MEDIUM, AttributeKey.MEDIUM_CLUE_SCROLL, "medium", Item(3608))
						7297 -> openRewardCasket(it, TreasureTrailClueLevel.HARD, AttributeKey.HARD_CLUE_SCROLL, "hard", Item(7297))
						7299 -> openRewardCasket(it, TreasureTrailClueLevel.ELITE, AttributeKey.ELITE_CLUE_SCROLL, "elite", Item(7299))
						7302 -> openRewardCasket(it, TreasureTrailClueLevel.MASTER, AttributeKey.MASTER_CLUE_SCROLL, "master", Item(7302))
					}
				} else {
					it.itemBox("Only a key taken from the corpse of a slain foe can unlock the magical barrier on the chest.", BLOODY_KEYS.random())
				}
			} else {
				player.lock()
				yellAtPlayer(player)
				player.message("Maybe I should talk to Nigel before opening his chest..")
				it.delay(1)
			}
			
			player.unlock()
		}
		
		// Ruby of the Fallen
		r.onItemOption1(4670) @Suspendable {
			val targets = getPlayersWithKeys(it.player.world(), it.player)
			
			if (targets.isEmpty()) {
				it.itemBox("You gaze into the gem, but it does not let you see through. It seems as if currently there are no Hunted in the wild.", 4670)
			} else {
				it.itemBox("You gaze into the gem, and it reflects a bright light off at an angle. It looks like the Wilderness contains ${targets.size} Hunted.", 4670)
				
				for (targ in targets) {
					val wildLevel = WildernessLevelIndicator.wildernessLevel(targ.tile())
					
					if (wildLevel >= 1) {
						val angle = Direction.of(Direction.angle(it.player, targ))
						val formatted = when (angle) {
							Direction.NorthEast -> "North-East"
							Direction.NorthWest -> "North-West"
							Direction.SouthEast -> "South-East"
							Direction.SouthWest -> "South-West"
							else -> angle.toString()
						}
						
						it.itemBox("You see a Hunted, named ${targ.name()}, wandering around level $wildLevel Wilderness. The light from the gem seems to originate from the $formatted of here.", 13195)
					} else {
						it.itemBox("You saw a Hunted, named ${targ.name()}, but it seems as if it has already left the Wilderness. The light of the gem is weak.", 13195)
					}
				}
			}
		}
		
		r.onWorldInit {
			it.ctx<World>().timers().set(TimerKey.BLOODCHEST_RECACHE, 5)
		}
		r.onWorldTimer(TimerKey.BLOODCHEST_RECACHE) {
			it.ctx<World>().timers().set(TimerKey.BLOODCHEST_RECACHE, 5)
			playersWithKeys.clear()
			
			val world = it.ctx<World>()
			world.players().forEachKt({ p ->
				if (WildernessLevelIndicator.inWilderness(p.tile())) {
					for (key in BLOODY_KEYS) {
						if (p.inventory().contains(key)) {
							playersWithKeys.add(p)
							break
						}
					}
				}
			})
		}
	}
	
	fun getPlayersWithKeys(world: World, exclude: Player): List<Player> {
		return playersWithKeys.minus(exclude)
	}
	
	@Suspendable fun takeKey(script: Script, player: Player, key: GroundItem) {
		val wildLevel = WildernessLevelIndicator.wildernessLevel(player.tile())
		
		// Skull..
		Skulling.assignSkullState(player)
		
		// Teleblock..
		player.teleblock(248) // Half TB time.
		player.graphic(1237) // A graphic for aesthetics
		
		// Broadcast..
		player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just collected a Blood Key at level $wildLevel Wilderness. Hunters, engage!")
		player.world().players().forEachKt({ other -> other.write(AddMessage("<col=CC1712><img=50> ${player.name()} has just collected a Blood Key at level $wildLevel Wilderness. Hunters, engage!", AddMessage.Type.BROADCAST)) })
		
		// You're now hunted. :)
		player.timers().set(TimerKey.BLOODCHEST_HUNTED, 100) // 60 seconds
//		Cutscene.shakeCamera(player, 5) //TODO look re-enable this when properly supported
		player.executeScript @Suspendable {
			it.clearContext()
			it.delay(10)
//			Cutscene.stopCameraShake(player) //TODO look re-enable this when properly supported
		}
		script.itemBox("RAISE THE HORNS. YOU ARE HUNTED. MAY THE ODDS BE EVER IN YOUR FAVOR.", 13195)
	}
	
	@JvmStatic fun dropKey(killer: Player, wildLvl: Int, totalRisk: Int, tile: Tile) {
		// If you both risk a total of 20K combined, you get increased chance on rarer keys.
		val odds = if (totalRisk > 20000) 2 else 3
		var key = 3606
		
		// Some keys only appear above a certain Wilderness level.
		key = if (wildLvl > 30 && killer.world().rollDie(odds + 3, 1)) { // Between 1/5 and 1/6, depending on risk.
			7302 // Master
		} else if (wildLvl > 20 && killer.world().rollDie(odds + 2, 1)) { // Between 1/4 and 1/5.
			7299 // Elite
		} else if (wildLvl > 10 && killer.world().rollDie(odds + 1, 1)) { // Between 1/3 and 1/4.
			7297 // Hard
		} else if (wildLvl >= 5 && killer.world().rollDie(odds, 1)) { // Between 1/2 and 1/3.
			7297 // Medium
		} else {
			3606 // Easy
		}
		
		// Put the key on the ground.. shiny! :-)
		val groundItem = GroundItem(killer.world(), Item(key), tile, killer.id()).hidden()
		killer.world().spawnGroundItem(groundItem)
		
		killer.message("<col=6a1a18><img=50> A Blood Key falls out of the corpse of your victim.")
	}

	@JvmStatic fun dropKeyFromVolcano(killer: Player, wildLvl: Int, tile: Tile) {
		var key: Int = if (wildLvl > 30 && killer.world().rollDie(5, 1)) {
			7302 // Master
		} else if (wildLvl > 20 && killer.world().rollDie(4, 1)) {
			7299 // Elite
		} else if (wildLvl > 10 && killer.world().rollDie(3, 1)) {
			7297 // Hard
		} else if (wildLvl >= 5 && killer.world().rollDie(2, 1)) {
			7297 // Medium
		} else {
			3606 // Easy
		}

		// Put the key on the ground.. shiny! :-)
		val groundItem = GroundItem(killer.world(), Item(key), tile, killer.id()).hidden()
		killer.world().spawnGroundItem(groundItem)

		killer.message("<col=6a1a18><img=50> A bloody key falls at your feet.")
	}
	
	@JvmStatic fun isKey(key: Int): Boolean = key in BLOODY_KEYS
	
	fun startedBloodySurvivalMinigame(player: Player): Boolean {
		return player.attribOr(AttributeKey.STARTED_BLOODY_SURVIVAL_MINIGAME, false)
	}
	
	@JvmStatic fun hasRewardKey(player: Player): Boolean {
		return player.inventory().hasAny(3606, 3608, 7297, 7299, 7302)
	}
	
	private fun yellAtPlayer(player: Player) {
		player.world().npcs().forEachInAreaKt(player.tile().area(7), { npc ->
			if (npc.id() == NIGEL) {
				npc.face(player)
				npc.sync().shout("${NIGEL_SHOUTS.random()}, ${player.name()}!")
			}
		})
	}
	
	private fun getRewardKey(player: Player): Int {
		BLOODY_KEYS.forEach { key ->
			if (player.inventory().contains(Item(key)))
				return key
		}
		return -1
	}
	
	private fun openRewardCasket(it: Script, rewardLevel: TreasureTrailClueLevel, clueScrollAttribute: AttributeKey, clueScrollType: String, rewardKey: Item) {
		val player = it.player()
		val clueScrollsCompleted = player.attribOr<Int>(clueScrollAttribute, 0)
		val sOrNo = if (clueScrollsCompleted >= 1) "s" else ""
		
		if (player.inventory().remove(Item(rewardKey), false).success()) {
			//Lock the player
			player.lock()
			
			// Generate the clue scroll reward
			when (rewardLevel) {
				TreasureTrailClueLevel.EASY -> EasyRewards.generateEasyReward(player, 2)
				TreasureTrailClueLevel.MEDIUM -> MediumRewards.generateMediumReward(player, 2)
				TreasureTrailClueLevel.HARD -> HardRewards.generateHardReward(player, 2)
				TreasureTrailClueLevel.ELITE -> EliteRewards.generateEliteReward(player, 2)
				TreasureTrailClueLevel.MASTER -> MasterRewards.generateMasterReward(player, 2)
			}
			
			//If it's a master clue, roll for a pet
			if (rewardLevel == TreasureTrailClueLevel.MASTER) {
				if (player.world().rollDie(2500, 1)) {
					
					if (player.world().realm().isPVP || player.world().realm().isOSRune)
						player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received an Olmlet pet from the Blood Chest!")
					
					TreasureTrailRewardCaskets.unlockOlmlet(player)
				}
			}
			
			player.message("<col=3300ff>You have opened ${clueScrollsCompleted + 1} $clueScrollType Blood Chest$sOrNo.</col>")
			
			if (AchievementDiary.diaryHandler(player) is PVPDiaryHandler) {
				AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(it.player() ,26))
			}
			
			//Add the reward to the players inventory, or drop it on the ground
			player.clueScrollReward().forEach { item ->
				if (item != null) {
					if (player.inventory().add(Item(item.id(), item.amount()), false).failed()) {
						val groundItem = GroundItem(player.world(), Item(item.id(), item.amount()), player.tile(), player.id())
						player.world().spawnGroundItem(groundItem)
					}
				}
			}
			
			//Display the interface TODO find the proper interface
//			player.interfaces().sendMain(364)
//			player.write(SetItems(112, 364, 1, player.clueScrollReward()))
			player.putattrib(clueScrollAttribute, clueScrollsCompleted + 1)
			
			//Clear the clue scroll reward and unlock the player
			player.clearattrib(AttributeKey.CLUE_SCROLL_REWARD)
			player.unlock()
		}
	}
}
