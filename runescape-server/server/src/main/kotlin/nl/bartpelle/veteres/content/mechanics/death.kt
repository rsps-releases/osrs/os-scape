package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.clanwars.FFAClanWars
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.events.BloodyVolcano
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterManager
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveGame
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.net.message.game.command.SetPlayerOption
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.ItemsOnDeath
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.util.*

/**
 * Created by Bart on 8/15/2015.
 * Retribution by Jak 12/16/2015
 */
@Suspendable class Death {
	@Suspendable companion object {
		
		@JvmStatic val logger: Logger = LogManager.getLogger(Death)
		
		@JvmStatic fun retrib(player: Player) {
			//Retribution. example: https://www.youtube.com/watch?v=7c6idspnxak
			try {
				if (player.varps().varbit(Varbit.RETRIBUTION) == 1) {
					val pker: Entity? = player.attribOr<Entity>(AttributeKey.KILLER, null) // Person who killed the dead player. Might be a 73 AGS spec pj.
					player.graphic(437);
					val dmg: Int = (player.skills().level(Skills.PRAYER) * 0.25).toInt()
					if (player.varps().varbit(Varbit.MULTIWAY_AREA) == 1) {
						val list = LinkedList<Player>()
						for (p: Player in player.closePlayers(1)) {
							if (!WildernessLevelIndicator.inAttackableArea(p) || p.varps().varbit(Varbit.MULTIWAY_AREA) == 0) {
								//not in the multi area and we were, don't carry over.
								continue;
							}
							if (player.tile().inSqRadius(p.tile(), 1)) {
								list.add(p)
							}
						}
						
						val damagePerPlayer = Math.max(1.0, dmg.toDouble() / Math.max(1, list.size).toDouble()).toInt()
						list.forEach { p ->
							p.hit(player, damagePerPlayer, 0, Hit.Type.PRAYER)
						}
					} else if (player.varps().varbit(Varbit.MULTIWAY_AREA) == 0 && pker != null) {
						if (player.tile().inSqRadius(pker.tile(), 1)) {
							pker.hit(player, dmg, 0, Hit.Type.PRAYER).block(false)
						}
					}
				}
			} catch (e: Exception) {
				logger.error("Death error!", e)
			}
		}
		
		@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
			val player: Player = it.player()
			it.clearContext() // Script continues if player is offline!
			
			player.lockNoDamage() //Lock the player
			
			it.delay(3) // Finish the proper delay after death (2 ticks)
			try {
				Staking.check_double_death(player) // must be checked after damage shows (because of PID you can't do it on the same cycle!)
			} catch (e: Exception) {
				logger.error("Double death check error!", e)
			}
			
			player.stopActions(true)
			
			Death.retrib(player)
			val mostdam: Player? = ItemsOnDeath.identifyMostDamagerKiller(player)
			if (mostdam != null) {
				Death.makePetShoutOnKill(mostdam)
				
				//If the kill happened in Edge PVP instance we apply a brief combat immunity timer to prevent PJing.
				if (PVPAreas.inEdgePvp(player)) {
					mostdam.timers().addOrSet(TimerKey.EDGEPVP_IMMUNITY, 25)
				}
			}
			
			player.animate(836) //Animate the player
			
			it.delay(4) //Set a delay of 4
			player.stopActions(true)
			
			//BH death logic
			val theKiller: Entity? = player.attrib(AttributeKey.KILLER)
			if (theKiller != null && theKiller is Player) {
				BountyHunterManager.onDeath(player, theKiller)
			}
			
			player.clearattrib(AttributeKey.LASTDEATH_VALUE)
			try {
				ItemsOnDeath.droplootToKiller(player, mostdam ?: player)
			} catch (e: Exception) {
				logger.error("Error dropping items and loot!", e)
			}
			
			player.clearattrib(AttributeKey.KILLER) //Clear killer attribute
			player.clearattrib(AttributeKey.TARGET) // Clear last attacked or interacted.
			
			// Close open interface. do this BEFORE MINIGAME HANDLING -> such as arena deaths.
			player.stopActions(true)
			
			// If we died in an instance, clean it up.
			val active = player.world().allocator().active(player.tile())
			var customLogic = false
			
			if (active.isPresent) {
				val instance = active.get()
				
				if (instance.deallocatesOnDeath() && instance.isCreatedFor(player)) {
					player.world().allocator().deallocate(instance)
				}
				
				// Check for a clan war
				if (instance.identifier.isPresent) {
					val identifier = instance.identifier.get()
					
					if (identifier == InstancedMapIdentifier.CLAN_WARS) {
						// Find a clan war, and spawn to the jail.
						val war = ClanWars.get(ClanChat.currentId(player))
						if (war != null) {
							war.jail(player)
							player.message("You have met a glorious death in battle.")
							customLogic = true
						}
					}
				}
			}
			
			val died_under_7_wild = WildernessLevelIndicator.wildernessLevel(player.tile()) <= 7 // Or in edge pvp (not classed as wildy)
			val duel_arena = player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)
			
			// If you die in FFA clan wars, you respawn at the lobby place.
			if (FFAClanWars.inFFAMap(player)) {
				player.teleport(player.world().randomTileAround(Tile(3364, 3168), 3))
				player.message("Oh dear, you are dead!") //Send the death message
			} else if (duel_arena) {
				Staking.on_death(player)
			} else if (player.attrib<InfernoSession>(AttributeKey.INFERNO_SESSION) != null) {
				player.message("Oh dear, you are dead!")
				player.attrib<InfernoSession>(AttributeKey.INFERNO_SESSION).end(false)
			} else if (player.attribOr<Boolean>(AttributeKey.TZHAAR_MINIGAME, false)) {
				FightCaveGame.leaveFightCave(player)
			} else if (player.attribOr<Int>(AttributeKey.JAILED, 0) == 1) {
				player.message("You've died, but you cannot run from your jail sentence!")
				player.teleport(player.tile())
			} else if (active != null && active.isPresent && active.get() != null && active.get().identifier != null
					&& active.get().identifier.isPresent && active.get().identifier.get() == InstancedMapIdentifier.PEST_CONTROL) {
                val spawn = active.get().center().transform(3 - player.world().random(3), 17 + player.world().random(5))

                player.teleport(spawn)
            } else if (MageBankInstance.attackableAreas(player)) {
                player.message("Oh dear, you are dead!")
                player.teleport(MageBankInstance.teleportSafeTile())
			} else if (!customLogic) {
				if (player.world().realm().isPVP || player.world().realm().isOSRune) {
					if (PVPAreas.inPVPArea(player)) {
						player.teleport(PVPAreas.safetileFor(player)) //Teleport the player inside edgeville bank (pvp height)
					} else {
						player.teleport(3094, 3469) //Teleport the player edge coffin spot
					}
				} else if (player.world().realm().isDeadman) {
					player.teleport(Tile(3145, 3510)) // GE corner with ditch / agil shortcut
				} else {
					player.teleport(3212, 3423) //Teleport the player to Varrock square
				}
				
				player.message("Oh dear, you are dead!") //Send the death message
			}
			
			//If the player is transmog'd then we reset the render.
			if (Transmogrify.isTransmogrified(player)) {
				Transmogrify.hardReset(player)
			}

			//If the player is inside the volcano event, remove them from the player list
			if(BloodyVolcano.players.contains(player)) {
				BloodyVolcano.players.remove(player)
			}
			
			//Remove auto-select
			player.varps().varbit(Varbit.AUTOCAST_SELECTED, 0) // Set auto-cast to default; 0
			player.updateWeaponInterface() //Update the weapon interface
			
			//Reset some values
			player.skills().resetStats() //Reset all players stats
			Poison.cure(player) //Cure the player from any poisons
			player.timers().cancel(TimerKey.FROZEN) //Remove frozen timer key
			player.timers().cancel(TimerKey.STUNNED) //Remove stunned timer key
			player.timers().cancel(TimerKey.TELEBLOCK) //Remove teleblock timer key
			player.timers().cancel(TimerKey.TELEBLOCK_IMMUNITY) //Remove the teleblock immunity timer key
			if (!died_under_7_wild && !player.timers().has(TimerKey.RECHARGE_SPECIAL_ATTACK)) {
				player.varps().varp(Varp.SPECIAL_ENERGY, 1000) //Set energy to 100%
				player.timers().register(TimerKey.RECHARGE_SPECIAL_ATTACK, 150) //Set the value of the timer. Currently 1:30m
			}
			player.varps().varp(Varp.SPECIAL_ENABLED, 0) //Disable special attack
			player.timers().cancel(TimerKey.COMBAT_LOGOUT) //Remove combat logout timer key

			//Remove timers
			player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
			player.write(SendWidgetTimer(WidgetTimer.TELEBLOCK, 0))
			player.write(SendWidgetTimer(WidgetTimer.VENGEANCE, 0))

			// Fact: forfiet and death in the duel arena doesn't reset skull related stuff.
			if (!duel_arena) {
				Skulling.unskull(player)
			}
			
			player.clearDamagers() //Clear damagers
			player.clearDamageTimes() // Clear damage time tracker
			player.face(null) // Reset entity facing
			Prayers.disableAllPrayers(player) //Disable all prayers
			if (!player.world().realm().isDeadman) {
				player.write(SetPlayerOption(2, false, "null")) //Remove the player attack option
			} else {
				player.varps().varbit(Varbit.DEADMAN_SKULL_TIMER, 0)
				player.varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, 0)
				player.timers().cancel(TimerKey.DEADMAN_SKULL_TIMER)
			}
			player.putattrib(AttributeKey.RUN_ENERGY, 100.0) //Set the players run energy to 100
			player.graphic(-1) //Set player graphics to -1
			player.hp(100, 0) //Set hitpoints to 100%
			player.animate(-1)  //Set player animation to -1
			player.clearattrib(AttributeKey.MOST_DAM_TRACKER) //Reset most damage done on player
			player.timers().cancel(TimerKey.CHARGE_SPELL) //Removes the spell charge timer from the player
			player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, 0) //Let our players use melee again! : )
			player.clearattrib(AttributeKey.VENOM_TICKS)
			player.varps().varbit(Varbit.VENGENACE_COOLDOWN, 0) //Remove the vengeance cooldown!
			player.clearattrib(AttributeKey.TZHAAR_MINIGAME)
			player.looks().invisible(false)
			player.looks().hide(false)
			
			it.delay(1) //Send delay of 1
			player.looks().update() //Update the players looks
			player.unlock() //Unlock the player
			if (player.bot()) {
				println("Despawning bot ${player.name()}")
				player.logout()
			}
		}
		
		// Messages your pet will shout when you kill someone, if the mechanic is enabled.
		@JvmStatic var SHOUTS = arrayOf("green:gundrilla!!!!11", "TNS tbh", "TNS")
		
		/**
		 * Is the custom mechanic for your pet shouting when you kill someone enabled
		 */
		@JvmStatic var PET_SHOUTING_ENABLED = true
		
		/**
		 * When you kill someone, if you have a follower Pet, they can shout something such as your Clan name.
		 * In future we could customize this and sellout!
		 */
		private fun makePetShoutOnKill(player: Player) {
			// Mechanic enabled?
			if (!PET_SHOUTING_ENABLED) return
			
			// Do we have a pet?
			val pet = player.pet() ?: return
			
			// Have we paid (or are an admin) to have the mechanic?
			if (ItemsOnDeath.hasShoutAbility(player)) {
				
				// Shout something.
				pet.sync().shout(SHOUTS[player.world().random(SHOUTS.size - 1)])
				
				// Make our team mates pets shout something too :)
				val cco = ClanChat.current(player)
				if (cco.isPresent) {
					val cc = cco.get()
					for (friend in cc.members()) {
						friend ?: continue
						
						// Only shout when our team mate is online on the same world.
						if (friend.id != player.id() as Int && friend.world() == player.world().id()) {
							val teamMate = player.world().playerForId(friend.id())?.get() ?: continue
							
							// Must be within the visible viewport.
							if (teamMate.tile().distance(player.tile()) > 14) continue
							val shoutDelay = 1 + player.world().random(2)
							val teamMatePet = teamMate.pet() ?: continue
							teamMatePet.executeScript { s -> s.delay(shoutDelay); s.npc().sync().shout(SHOUTS[player.world().random(SHOUTS.size - 1)]) }
						}
					}
				}
			}
		}
		
		// Add x deaths to your death varp.
		@JvmStatic fun deaths(player: Player, deaths: Int): Int {
			var stage = player.varps().varp(Varp.DEATHS)
			stage += deaths
			player.varps().varp(Varp.DEATHS, stage)
			
			return stage
		}
	}
}

