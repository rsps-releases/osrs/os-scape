package nl.bartpelle.veteres.content.npcs

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/6/2016.
 */

object ShoutingAnimals {
	
	
	val COWS = arrayListOf(2805, 2806, 2807, 2808)
	val DUCKS = arrayListOf(1838, 1839, 2003, 2004)
	val DUCKLINGS = 2002
	val SHEEP = arrayListOf(2794, 2795, 2796, 2800, 2801, 2802, 2697, 2698, 2699, 2789, 2790, 2791)
	
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Cows go moo!
		for (COWS in COWS) {
			r.onNpcSpawn(COWS) @Suspendable {
				val npc: Npc = it.npc()
				fun shout() {
					npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
						s.onInterrupt { shout() }
						while (true) {
							s.delay(npc.world().random(30))
							npc.sync().shout("Moo")
							npc.animate(5854)
							npc.world().spawnSound(npc.tile(), 3044, 0, 7)
						}
					})
				}
				it.onInterrupt { shout() }
				shout()
			}
		}
		
		//Ducks go Quack!
		for (DUCKS in DUCKS) {
			r.onNpcSpawn(DUCKS) @Suspendable {
				val npc: Npc = it.npc()
				fun shout() {
					npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
						s.onInterrupt { shout() }
						while (true) {
							s.delay(npc.world().random(30))
							npc.sync().shout("Quack!")
							npc.world().spawnSound(npc.tile(), 413, 0, 7)
						}
					})
				}
				it.onInterrupt { shout() }
				shout()
			}
		}
		
		//Ducklings go Eep!
		r.onNpcSpawn(DUCKLINGS) @Suspendable {
			val npc: Npc = it.npc()
			fun shout() {
				npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
					s.onInterrupt { shout() }
					while (true) {
						s.delay(npc.world().random(30))
						npc.sync().shout("Eep!")
						npc.world().spawnSound(npc.tile(), 412, 0, 7)
					}
				})
			}
			it.onInterrupt { shout() }
			shout()
		}
		
		//Sheep go Baa!
		for (SHEEP in SHEEP) {
			r.onNpcSpawn(SHEEP) @Suspendable {
				val npc: Npc = it.npc()
				fun shout() {
					npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
						s.onInterrupt { shout() }
						while (true) {
							s.delay(npc.world().random(30))
							npc.sync().shout("Baa!")
							npc.world().spawnSound(npc.tile(), 2053, 0, 7)
						}
					})
				}
				it.onInterrupt { shout() }
				shout()
			}
		}
	}
}