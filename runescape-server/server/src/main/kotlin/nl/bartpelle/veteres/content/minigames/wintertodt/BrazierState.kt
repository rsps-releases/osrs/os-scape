package nl.bartpelle.veteres.content.minigames.wintertodt

/**
 * Created by Bart on 9/10/2016.
 */
enum class BrazierState(val value: Int, val objectId: Int) {
	
	/**
	 * The brazier is currently broken and will not contribute until it is fixed.
	 */
	BROKEN(0, WintertodtObjects.BROKEN_BRAZIER),
	
	/**
	 * Brazier is currently not lit and will not contribute to the Wintertodt.
	 */
	STALE(1, WintertodtObjects.UNLIT_BRAZIER),
	
	/**
	 * The brazier is currently lit and contributes to the Wintertodt.
	 */
	LIT(2, WintertodtObjects.BURNING_BRAZIER)
	
}