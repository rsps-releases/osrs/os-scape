package nl.bartpelle.veteres.content.combat

/**
 * TODO: ONLY THE ENUM IS CURRENTLY DONE, HAVE TO ADD THEM INTO THE COMBAT + CORRECT EFFECTS
 * TODO: Get the correct GFX for opal, jade, pearl, topaz and sapphire bolts.
 */

enum class BoltEffects(val boltId: Int, val hitOnPVP: Int, val hitOnPVM: Int, val GFX: Int) {
	//Lucky Lightning: extra damage
	OPAL_BOLTS(9236, 5, 5, 749),
	
	//Earth's fury: chance of knocking the target to the ground
	JADE_BOLTS(9237, 6, 5, 756),
	
	//Sea curse: chance of striking the target with a mighty bolt of water, dealing extra damage
	PEARL_BOLTS(9238, 6, 6, 750),
	
	//Down to earth: chance of lowering the target's Magic level (Only works on another player)
	TOPAZ_BOLTS(9239, 4, 6, 757),
	
	//Clear mind: chance of lowering the target's Prayer points and giving part of them to the attacker (Only works on another player)
	SAPPHIRE_BOLTS(9240, 4, 0, 751),
	
	//Magical poison: Chance of poisoning the opponent
	EMERALD_BOLTS(9241, 5, 0, 752),
	
	//Blood forfeit: chance of removing 20% of the target's current Hitpoints (this effect costs 10% of the attacker's own current Hitpoints)
	RUBY_BOLTS(9242, 11, 6, 754),
	
	//Armour piercing: the bolts will negate a sizeable portion of the target's Ranged defence, and the special attack raises the bolts' maximum hit
	DIAMOND_BOLTS(9243, 5, 10, 758),
	
	//Dragon's breath: chance of inflicting a Dragonfire hit against the target (does not apply if the target has protection against dragonfire)
	DRAGON_BOLTS(9244, 6, 10, 756),
	
	//Life leech: Chance of doing extra damage and healing the attacker by 25% of the damage dealt (does not work on undead as they have no "life" to leech)
	ONYX_BOLTS(9245, 10, 11, 753),


	// DRAGON BOLTS - DUPLICATE (E)
	DRAGON_OPAL_BOLTS(21932, 5, 5, 749),
	DRAGON_JADE_BOLTS(21934, 6, 5, 756),
	DRAGON_PEARL_BOLTS(21936, 6, 6, 750),
	DRAGON_TOPAZ_BOLTS(21938, 4, 6, 757),
	DRAGON_SAPPHIRE_BOLTS(21940, 4, 0, 751),
	DRAGON_EMERALD_BOLTS(21942, 5, 0, 752),
	DRAGON_RUBY_BOLTS(21944, 11, 6, 754),
	DRAGON_DIAMOND_BOLTS(21946, 5, 10, 758),
	DRAGON_DRAGON_BOLTS(21948, 6, 10, 756),
	DRAGON_ONYX_BOLTS(21950, 10, 11, 753),

	;
}