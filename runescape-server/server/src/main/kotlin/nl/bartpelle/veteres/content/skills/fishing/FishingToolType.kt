package nl.bartpelle.veteres.content.skills.fishing

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import java.util.*
import java.util.stream.Stream

/**
 * All tools pertaining to doing the fishing action should be added here for better unity with the system as a whole.
 * It is a huge plus to have definitions for specific tools on its own class versus merging the fishing spot
 * definition and required item together.
 *
 * @author Mack
 * @see FishSpotType
 */
enum class FishingToolType(animId: Int, levelRequired: Int, toolId: Int, boostMod: Double, fishingSpots: ArrayList<FishSpotType> = arrayListOf(), vararg secondaryItems: Int = intArrayOf()) {
    DRAGON_HARPOON(618, 61, 21028, 1.20, arrayListOf(FishSpotType.HARPOON, FishSpotType.HARPOON_SHARK)),

    NONE(-1, -1, -1, 1.0);

    private val animation = animId
    private val tool = toolId
    private val boost = boostMod
    private val levelReq = levelRequired
    private val usableSpots = fishingSpots
    private val secondarySupplies = secondaryItems

    fun id(): Int {
        return tool
    }

    fun animationId(): Int {
        return animation
    }

    fun levelRequired(): Int {
        return levelReq
    }

    fun secondarySupplies(): IntArray {
        return secondarySupplies
    }

    fun boost(): Double {
        return boost
    }

    companion object {

        fun locateItemFor(player: Player): Optional<FishingToolType> {
            var type: Optional<FishingToolType>
            when (!player.equipment().isEmpty || !player.inventory().isEmpty) {
                true -> {
                    val container = Stream.concat(Arrays.stream(player.equipment().items()), Arrays.stream(player.inventory().items())).filter({item -> item != null})
                    for (item in container) {
                        type = getType(item)
                        if (type.isPresent && type.get() != FishingToolType.NONE) {
                            return type
                        }
                    }
                }
                else -> Optional.empty<FishingToolType>()
            }
            return Optional.empty()
        }

        fun getType(item: Item): Optional<FishingToolType> {
            return Arrays.stream(FishingToolType.values()).filter({toolType -> item.id() == toolType.id()}).findAny()
        }

        fun canUseOnSpot(tool: FishingToolType, spot: FishSpotType): Boolean {
            return (tool.usableSpots.contains(spot))
        }
    }
}