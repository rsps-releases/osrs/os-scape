package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/15/2015.
 */

object StringBow {
	
	enum class Bows(val unstrung: Int, val strung: Int, val lvl: Int, val xp: Double, val animation: Int) {
		SHORT_BOW(50, 841, 5, 5.0, 6678),
		LONG_BOW(48, 839, 10, 10.0, 6684),
		OAK_SHORT_BOW(54, 843, 20, 16.5, 6679),
		OAK_LONG_BOW(56, 845, 25, 25.0, 6685),
		COMPOSITE_BOW(4825, 4827, 30, 45.0, 6686),
		WILLOW_SHORT_BOW(60, 849, 35, 33.3, 6680),
		WILLOW_LONG_BOW(58, 847, 40, 41.5, 6686),
		MAPLE_SHORT_BOW(64, 853, 50, 50.0, 6681),
		MAPLE_LONG_BOW(62, 851, 55, 58.3, 6687),
		YEW_SHORT_BOW(68, 857, 65, 68.5, 6682),
		YEW_LONG_BOW(66, 855, 70, 75.0, 6688),
		MAGIC_SHORT_BOW(72, 861, 80, 83.3, 6683),
		MAGIC_LONG_BOW(70, 859, 85, 91.5, 6689);
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		Bows.values().forEach { bows ->
			val BOW_STRING = 1777
			repo.onItemOnItem(bows.unstrung.toLong(), BOW_STRING.toLong(), s@ @Suspendable {
				//Check if the player has a high enough level to do this.
				if (it.player().skills()[Skills.FLETCHING] < bows.lvl) {
					it.messagebox("You need at least level ${bows.lvl} Fletching to do that.")
					return@s
				}
				//Prompt the player with the # they'd like to make
				var num = 1
				if (it.player().inventory().count(bows.unstrung) > 1) {
					num = it.itemOptions(Item(bows.strung, 150), offsetX = 12)
				}
				while (num-- > 0) {
					//Check if the player's ran out of supplies
					if (bows.unstrung !in it.player().inventory() || BOW_STRING !in it.player().inventory()) {
						break
					}
					//Remove the supplies, give the player the item, animate, send exp, msg, and apply delay.
					it.player().inventory() -= Item(bows.unstrung, 1)
					it.player().inventory() -= Item(BOW_STRING, 1)
					it.player().inventory() += Item(bows.strung, 1)
					it.message("You add a string to the bow.")
					it.player().animate(bows.animation)
					it.addXp(Skills.FLETCHING, bows.xp)
					it.delay(2)
				}
			})
		}
	}
}