package nl.bartpelle.veteres.content.minigames.bounty_hunter

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Mack on 9/16/2017.
 */
object BountyTargetSearch {
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		if (false) {
			r.onWorldInit @Suspendable {
				val world = it.ctx<World>()
				
				while (true) {
					world.players().forEachKt({ player ->
						if (doesPlayerNeedTarget(player)) {
							for (target in BountyHunterManager.potentialTargets) {
								// Don't assign a target if the player has a penalty.
								if (player.timers().has(TimerKey.BOUNTY_HUNTER_PENALTY)) {
									BountyHunterManager.updateWidget(player, BountyHunterManager.LEVEL_RANGE_CHILD)
								} else {
									// Checks if the player meets the criteria and is allowed to receive a target.
									if (BountyHunterManager.meetsCriteria(target, player)) {
										BountyHunterManager.bindTarget(player, target)
										break
									}
								}
							}
						}
					})
					
					// Don't run every tick - once per 8 should do it.
					it.delay(8)
				}
			}
		}
	}
	
	fun doesPlayerNeedTarget(player: Player): Boolean {
		return player.attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false) == false // Is BH enabled?
				&& WildernessLevelIndicator.inWilderness(player.tile()) // Is the player in the Wilderness?
				&& BountyHunterManager.potentialTargets.contains(player) // Is the player a BH target himself?
				&& player.attribOr<Player>(AttributeKey.BOUNTY_HUNTER_TARGET, null) == null // Do we lack a target?
	}
	
}