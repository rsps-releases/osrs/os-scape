package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/9/2015.
 */

object Coffins {
	
	val OPEN_COFFIN = 3577 // Changed around ~#135
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//Close coffin
		repo.onObject(OPEN_COFFIN) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			player.faceObj(obj)
			it.delay(1)
			
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val spawned = MapObj(obj.tile(), 398, obj.type(), obj.rot())
				world.spawnObj(spawned)
				it.delay(300)
				if (spawned.valid(world)) {
					world.removeObj(spawned, false)
					world.spawnObj(obj) // Force re-send the previously existing object
				}
			}
			
			it.message("You close the coffin.")
		}
		
		//Open coffin
		repo.onObject(398) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			player.faceObj(obj)
			it.delay(1)
			
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val openedCoffin = MapObj(obj.tile(), OPEN_COFFIN, obj.type(), obj.rot())
				world.spawnObj(openedCoffin)
				it.delay(300)
				if (openedCoffin.valid(world)) {
					world.removeObj(openedCoffin, false)
					world.spawnObj(obj) // Force re-send the previously existing object
				}
			}
			
			it.message("The coffin creaks open...")
		}
	}
	
}
