package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.*
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.RSFormatter

/**
 * Created by Situations on 12/31/2015.
 */
object PriceChecker {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onButton(387, 19) @Suspendable {
			// price guide open from equipment tab
			it.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
			it.player().interfaces().sendMain(464)
			it.player().interfaces().sendInventory(238)
			it.player().invokeScript(785, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			it.player().invokeScript(600, 1, 1, 15, 30408716)
			it.player().write(InterfaceItem(464, 8, 6512))
			it.player().write(InterfaceText(464, 12, "Total guide price:<br><col=ffffff> 0"))
			it.player().write(InterfaceSettings(464, 2, 0, 27, 1086))
			it.player().invokeScript(149, 15597568, 93, 4, 7, 0, -1, "Add<col=ff9040>", "Add-5<col=ff9040>", "Add-10<col=ff9040>",
					"Add-All<col=ff9040>", "Add-X<col=ff9040>")
			it.player().write(InterfaceSettings(238, 0, 0, 27, 1086))
			it.player().write(SetItems(90, -1, 64212, it.player().attribOr(AttributeKey.PRICE_GUIDE_CONTAINER, ItemContainer(it.player().world(), 28, ItemContainer.Type.REGULAR))))
			it.player().write(SetItems(93, -1, 64209, it.player().inventory()))
		}
		repo.onInterfaceClose(464) {
			val container = it.player().attrib<ItemContainer>(AttributeKey.PRICE_GUIDE_CONTAINER)
			container?.forEach { item ->
				if (item != null)
					it.player().inventory().addOrDrop(item, it.player)
			}
			
			it.player().clearattrib(AttributeKey.PRICE_GUIDE_CONTAINER)
			it.player().interfaces().closeById(238) // Inventory close force
		}
		repo.onButton(464, 2) {
			val container = it.player().attrib<ItemContainer>(AttributeKey.PRICE_GUIDE_CONTAINER) ?: return@onButton
			
			when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
				1 -> remove(it.player(), 1)
				2 -> remove(it.player(), 5)
				3 -> remove(it.player(), 10)
				4 -> remove(it.player(), Int.MAX_VALUE)
				5 -> remove(it.player(), it.inputInteger("Enter amount:"))
				10 -> {
					if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@onButton
					val item = container[it.player()[AttributeKey.BUTTON_SLOT]]
					if (item != null) {
						it.player().message(it.player().world().examineRepository().item(item.id()))
					}
				}
			}
		}
		repo.onButton(464, 5) {
			// Search item
			it.player().write(InvokeScript(750, "Select an item to ask about its price:", 1, -1))
		}
		repo.onButton(464, 10) {
			// Add all
			for (i in 0..27) {
				val itemAt = it.player().inventory()[i] ?: continue // Get item or continue
				offer(it.player(), itemAt.amount(), i)
			}
		}
		repo.onButton(238, 0) {
			when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
				1 -> offer(it.player(), 1)
				2 -> offer(it.player(), 5)
				3 -> offer(it.player(), 10)
				4 -> offer(it.player(), Int.MAX_VALUE)
				5 -> offer(it.player(), it.inputInteger("Enter amount:"))
				10 -> {
					if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@onButton
					val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]]
					if (item != null) {
						it.player().message(it.player().world().examineRepository().item(item.id()))
					}
				}
			}
		}
	}
	
	@JvmStatic fun remove(player: Player, amt: Int, slot: Int? = null) {
		val container = player.attrib<ItemContainer>(AttributeKey.PRICE_GUIDE_CONTAINER) ?: return
		val slot = slot ?: player[AttributeKey.BUTTON_SLOT]
		val item = container[slot] ?: return
		
		val removed = container.remove(Item(item, amt), true, slot).completed()
		player.inventory().add(Item(item, removed), true)
		
		refresh(player, container)
	}
	
	@JvmStatic fun offer(player: Player, amt: Int, slot: Int? = null) {
		val slot = slot ?: player[AttributeKey.BUTTON_SLOT]
		val item = player.inventory()[slot] ?: return
		val container = player.attribOr<ItemContainer>(AttributeKey.PRICE_GUIDE_CONTAINER, ItemContainer(player.world(), 28, ItemContainer.Type.REGULAR))
		
		val removed = player.inventory().remove(Item(item, amt), true, slot).completed()
		container.add(Item(item, removed), true)
		
		player.putattrib(AttributeKey.PRICE_GUIDE_CONTAINER, container)
		
		refresh(player, container)
	}
	
	private fun refresh(player: Player, container: ItemContainer) {
		player.write(SetItems(90, -1, 64212, container))
		player.write(SetItems(93, -1, 64209, player.inventory()))
		
		val prices = Array(container.size()) {
			container[it]?.realPrice(player.world()) ?: 0
		}
		
		var totalPrice = 0
		container.forEach { if (it != null) totalPrice += it.realPrice(player.world()) * it.amount() }
		
		//Set prices
		player.invokeScript(785, *prices)
		player.invokeScript(600, 1, 1, 15, 30408716)
		player.write(InterfaceItem(464, 8, 6512))
		player.write(InterfaceText(464, 12, "Total guide price:<br><col=ffffff> ${RSFormatter.formatNumber(totalPrice)}</col> "))
	}
	
	@JvmStatic fun show(player: Player, item: Int) {
		val item = Item(item, 1)
		
		player.write(InterfaceItem(464, 8, item.id()))
		player.invokeScript(600, 0, 1, 15, 30408716)
		player.write(InterfaceText(464, 12, "${item.name(player.world())}:<br><col=ffffff>${RSFormatter.formatNumber(item.realPrice(player.world()))} ${if (player.world().realm().isPVP) "blood money" else "coins"}</col>"))
	}
}