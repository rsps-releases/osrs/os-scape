package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.ChangeMapMarker
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Jak on 17/10/2016.
 */
object Ectophial {
	
	val ECTOPHIAL_FULL = 4251
	val ECTOPHIAL_EMPTY = 4252
	
	val ECTOFUNCTUS = 16648
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOption1(ECTOPHIAL_FULL) @Suspendable {
			if (Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
				DeadmanMechanics.attemptTeleport(it)
				val target = Tile(3659, 3524)
				val radius = 2
				val player = it.player()
				val used = player.inventory()[player.attrib(AttributeKey.ITEM_SLOT)]
				player.lockNoDamage()
				player.message("You empty the ectoplasm onto the ground around your feet...")
				it.player().animate(878)
				it.player().graphic(1273)
				it.delay(1)
				it.message("...and the world changes around you.")
				it.delay(2)
				it.player().inventory().remove(used, true, player.attrib(AttributeKey.ITEM_SLOT))
				it.player().inventory().add(Item(ECTOPHIAL_EMPTY), false, player.attrib(AttributeKey.ITEM_SLOT))
				if (PVPAreas.inPVPArea(player)) {
					player.teleport(PVPAreas.safetileFor(player)) // Go to inside edge bank, stay in pvp.
				} else {
					player.teleport(player.world().randomTileAround(target, radius))
					// Walk back after
					player.executeScript { s ->
						val ecto = s.player().world().objById(ECTOFUNCTUS, Tile(3658, 3518))
						s.player().walkTo(ecto, PathQueue.StepType.REGULAR)
						s.player().write(ChangeMapMarker(s.player().pathQueue().peekLastTile()))
						val lastTile = s.player().pathQueue().peekLast()?.toTile() ?: s.player().tile()
						s.waitForTile(lastTile)
						if (s.player().inventory().remove(Item(ECTOPHIAL_EMPTY), false).success()) {
							s.player().inventory().add(Item(ECTOPHIAL_FULL), false)
							s.message("You refill the ectophial from the Ectofuntus.")
							s.player().faceTile(ecto.tile())
							s.animate(832)
						}
					}
				}
				player.animate(-1)
				player.graphic(-1)
				player.unlock()
				player.timers().cancel(TimerKey.FROZEN)
				player.timers().cancel(TimerKey.REFREEZE)
				player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
			}
		}
		repo.onItemOnObject(ECTOFUNCTUS) @Suspendable {
			if (it.player().inventory().remove(Item(ECTOPHIAL_EMPTY), false).success()) {
				it.player().inventory().add(Item(ECTOPHIAL_FULL), false)
				it.message("You refill the ectophial from the Ectofuntus.")
			}
		}
	}
	
}