package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 2/23/2016.
 */
object RottenTomatoes {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Crate interaction
		// Rotten apple crate .. shop 29 removed
		r.onObject(3195) { /*it.player().world().shop(29).display(it.player())*/ }
		
		r.onItemOnPlayer(2518) @Suspendable {
			throwTomato(it)
		}
	}
	
	@Suspendable fun throwTomato(it: Script) {
		// Throttle..
		if (it.player().timers().has(TimerKey.TOMATO_TIMER)) {
			return
		}
		
		// Don't do this in the duel arena..
		if (it.player().attribOr(AttributeKey.IN_STAKE, false)) {
			// return
		}
		
		val target = it.target() as Player
		if (target.attribOr(AttributeKey.IN_STAKE, false)) {
			// Remove tomato
			if (it.player().privilege() != Privilege.ADMIN) {
				it.player().inventory().remove(Item(2518), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().timers().register(TimerKey.TOMATO_TIMER, 5)
			}
			
			// Let the magic happen
			it.player().animate(385)
			it.player().graphic(30, 96, 0)
			it.player().faceTile(target.tile())
			
			// And make a projectile + tile graphic :)
			val dist = target.tile().distance(it.player().tile())
			val clientcycles = dist * 8
			val gamecycles = clientcycles / 30
			val delta = (clientcycles + 24) - (gamecycles * 30)
			
			val dest = target.world().randomTileAround(target.tile(), 2)
			it.player().world().spawnProjectile(it.player().tile(), dest, 29, 40, 0, 24, clientcycles, 20, 20)
			it.clearContext()
			it.delay(gamecycles)
			target.world().tileGraphic(31, dest, 0, delta + 1)
		} else {
			it.message("I'm not going to throw fruit at that player, they might hit me!")
		}
	}
	
}