package nl.bartpelle.veteres.content.areas.lumbridge

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.npcs.bosses.wilderness.SkotizoController
import nl.bartpelle.veteres.content.npcs.bosses.wilderness.WildernessEventController
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/6/2016.
 */

object Chests {
	
	// Pair(closedId, openId)
	val CHESTS: Array<Pair<Int, Int>> = arrayOf((Pair(375, 378)))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		CHESTS.forEach { chest ->
			// Register open options
			r.onObject(chest.first) @Suspendable {
				val obj = it.interactionObject()
				it.player().lock()
				it.player().animate(536)
				it.sound(52)
				it.delay(1)
				it.player().world().spawnObj(MapObj(obj.tile(), chest.second, obj.type(), obj.rot()))
				it.player().unlock()
			}
			
			// Register the bank interacting
			r.onObject(chest.second) @Suspendable {
				val obj = it.interactionObject()
				val opt = it.interactionOption()
				
				if (opt == 2) {
					it.message("You cycle the chest but find nothing.")
				} else {
					// Closing
					it.player().lock()
					it.player().animate(536)
					it.sound(51)
					it.delay(1)
					it.player().world().spawnObj(MapObj(obj.tile(), chest.first, obj.type(), obj.rot()))
					it.player().unlock()
				}
			}
			r.onObject(12120) @Suspendable {
				
				if (it.player().inventory().has(19566)) {
					if (it.optionsTitled("Riskfully open the chest?", "Yes, I want to risk it.", "No, I want to keep my key.") == 1) {
						it.player().animate(536)
						it.sound(52)
						it.player.lock()
						it.delay(2)
						it.player.unlock()
						
						val keySlot = it.player().inventory().findFirst(19566).first()
						if (it.player().inventory().remove(Item(19566), true).success()) {
							if (it.player().world().rollDie(10, 1)) {
								val loot = WildernessEventController.randomRareItem(it.player().world().realm())
								it.player().inventory().add(loot, true)
								it.itemBox("As you turn the lock, the key breaks, but the chest opens just before it snaps. Inside the chest you find something mysterious: <col=ff0000>${loot.name(it.player().world())}</col>.", loot.id())
							} else {
								it.player().inventory().add(Item(592), true, keySlot)
								it.messagebox("As you turn the lock, the key breaks. The key tooth is stuck, and your key crumbles.")
							}
						}
					}
				} else {
					it.message("You need a key from Skotizo to open this chest.")
				}
			}
		}
	}
	
}
