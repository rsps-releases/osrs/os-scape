package nl.bartpelle.veteres.content.skills.runecrafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Carl on 2015-08-26.
 * Edited & finished by Situations on 2015-09-12
 */

object RuneConversion {
	
	enum class Altar(val levelReq: Int, val xp: Double, val talisman: Int, val rune: Int, val altarObj: Int, val entranceObj: Int, val entranceTile: Tile, val exitObject: Int,
	                 val exitTile: Tile, val pure: Boolean, val multiplier: Int, val petOdds: Int, val petTransform: Pet? = null) {
		
		AIR(1, 5.0, 1438, 556, 14897, 14989, Tile(2841, 4830), 14841, Tile(2983, 3293), false, 11, 52800, Pet.RIFT_GUARDIAN_AIR),
		MIND(2, 5.5, 1448, 558, 14898, 14990, Tile(2792, 4827), 14842, Tile(2984, 3512), false, 14, 51000, Pet.RIFT_GUARDIAN_MIND),
		WATER(5, 6.0, 1444, 555, 14899, 14991, Tile(2726, 4832), 14843, Tile(3183, 3167), false, 19, 47200, Pet.RIFT_GUARDIAN_WATER),
		EARTH(9, 6.5, 1440, 557, 14900, 14992, Tile(2655, 4830), 14844, Tile(3305, 3472), false, 29, 45200, Pet.RIFT_GUARDIAN_EARTH),
		FIRE(14, 7.0, 1442, 554, 14901, 14993, Tile(2574, 4849), 14845, Tile(3312, 3253), false, 35, 43500, Pet.RIFT_GUARDIAN_FIRE),
		BODY(20, 7.5, 1446, 559, 14902, 14994, Tile(2521, 4834), 14846, Tile(3054, 3443), false, 46, 41800, Pet.RIFT_GUARDIAN_BODY),
		COSMIC(27, 8.0, 1454, 564, 14903, 15608, Tile(2162, 4833), 14847, Tile(2410, 4377), true, 59, 39500, Pet.RIFT_GUARDIAN_COSMIC),
		LAW(54, 9.5, 1458, 563, 14904, 15609, Tile(2464, 4818), 14848, Tile(2860, 3381), true, 200, 38200, Pet.RIFT_GUARDIAN_LAW),
		NATURE(44, 9.0, 1462, 561, 14905, 15610, Tile(2400, 4835), 14892, Tile(2868, 3017), true, 91, 30570, Pet.RIFT_GUARDIAN_NATURE),
		CHAOS(35, 8.5, 1452, 562, 14906, 15611, Tile(2281, 4837), 14893, Tile(3062, 3590), true, 74, 17080, Pet.RIFT_GUARDIAN_CHAOS),
		DEATH(65, 10.0, 1456, 560, 14907, 15612, Tile(2208, 4830), 14894, Tile(1862, 4639), true, 200, 14900, Pet.RIFT_GUARDIAN_DEATH),
		ASTRAL(40, 8.7, -1, 9075, 14911, -1, Tile(2156, 3863), 14895, Tile(2156, 3863), true, 42, 9990, Pet.RIFT_GUARDIAN_ASTRAL);
		
		companion object {
			fun get(talisman: Int): Altar? {
				Altar.values().forEach {
					if (it.talisman == talisman)
						return it
				}
				return null
			}
		}
	}
	
	@ScriptMain @JvmStatic fun rune_conversion(repo: ScriptRepository) {
		Altar.values().forEach { altar ->
			repo.onObject(altar.altarObj) @Suspendable {
				val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
				if (option == 2 && altar.altarObj == 14911) {
					if (it.player().varps().varbit(Varbit.SPELLBOOK) == 0) {
						it.player().varps().varbit(Varbit.SPELLBOOK, 2)
						it.player().animate(645)
						it.player().message("You sense a surge of purity flow through your body!")
					} else if (it.player().varps().varbit(Varbit.SPELLBOOK) == 2) {
						it.player().varps().varbit(Varbit.SPELLBOOK, 0)
						it.player().animate(645)
						it.player().message("You sense a surge of purity flow through your body!")
					} else {
						it.player().varps().varbit(Varbit.SPELLBOOK, 0)
						it.player().animate(645)
						it.player().message("You sense a surge of purity flow through your body!")
					}
				}
				
				if (it.player().skills().xpLevel(Skills.RUNECRAFTING) >= altar.levelReq) {
					var amount = it.player().inventory().count(7936)
					if (!altar.pure)
						amount += it.player().inventory().count(1436)
					
					var msg = "pure"
					if (!altar.pure)
						msg = "rune"
					
					if (amount >= 1) {
						it.player().lock()
						it.animate(791)
						it.player().graphic(186)
						it.delay(4)
						
						if (altar.pure)
							it.player().inventory().remove(Item(7936, amount), true)
						else {
							it.player().inventory().remove(Item(7936, it.player().inventory().count(7936)), true)
							it.player().inventory().remove(Item(1436, it.player().inventory().count(1436)), true)
						}
						var multi = 1
						for (i in IntRange(altar.multiplier, altar.multiplier * 10) step altar.multiplier) {
							if (it.player().skills().xpLevel(Skills.RUNECRAFTING) >= i)
								multi++;
						}
						
						// The Runesmith blessing gives double runes.
						if (BonusContent.isActive(it.player, BlessingGroup.RUNESMITH)) {
							multi *= 2
							it.player.message("The active Runesmith blessing grants you double runes!")
						}
						
						it.player().inventory().add(Item(altar.rune, amount * multi), false)
						it.addXp(Skills.RUNECRAFTING, altar.xp * amount)
						
						// Woo! A pet!
						if (altar.petTransform != null) {
							val odds = (altar.petOdds.toDouble() * it.player().mode().skillPetMod()).toInt()
							if (it.player().world().rollDie(odds, 1)) {
								unlockRiftGuarding(it.player(), altar.petTransform)
							} else {
								val pet = it.player().pet()
								if (pet != null) {
									// Our current pet is a runecrafting pet.
									if (pet.id() >= 7337 && pet.id() <= 7367) {
										PetAI.metamorph(it.player(), altar.petTransform)
									}
								}
							}
						}
						it.player().unlock()
					} else {
						it.messagebox("You do not have any $msg essence to bind.")
					}
				} else {
					it.message("You need a Runecrafting level of " + altar.levelReq + " to infuse these runes.")
				}
			}
		}
		
		Altar.values().forEach { altar ->
			repo.onObject(altar.exitObject, s@ @Suspendable {
				if(it.interactionObject().id() == 14845 && it.player().world().realm().isPVP) {
					it.player().world().server().scriptExecutor().executeScript(it.player(), PVPAreas.showPvpInstanceOptions)
					return@s
				}
				it.player().lock()
				it.message("You step through the portal...")
				it.player().teleport(altar.exitTile)
				it.delay(1)
				it.player().unlock()
			})
		}
		
		Altar.values().forEach { altar ->
			repo.onItemOnObject(altar.entranceObj, @Suspendable {
				val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
				val talisman = Altar.get(item)
				if (talisman != null && altar.talisman == item) {
					it.player().lock()
					it.delay(1)
					it.animate(827)
					it.message("You hold the " + altar.name.toLowerCase() + " talisman towards the mysterous ruins.")
					it.delay(2)
					it.message("You feel a powerful force take hold of you...")
					it.player().lock()
					it.delay(1)
					it.player().teleport(altar.entranceTile)
					it.player().unlock()
				} else {
					val aAn = if (altar == Altar.EARTH || altar == Altar.ASTRAL || altar == Altar.AIR) "an" else "a"
					it.message("You need $aAn " + altar.name.toLowerCase() + " talisman to access the " + altar.name.toLowerCase() + " altar.")
				}
			})
			
			// TODO there's a varp that shows the "enter" option on the object when wearing max cape/tiara - I think. ~Jak
		}
	}
	
	fun unlockRiftGuarding(player: Player, guardian: Pet) {
		if (!PetAI.hasUnlocked(player, guardian)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(guardian.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, guardian, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(guardian.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(guardian.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
}
