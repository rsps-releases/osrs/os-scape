package nl.bartpelle.veteres.content.minigames.inferno

import nl.bartpelle.veteres.content.npcs.inferno.TzKalZuk
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.instance.InstancedMap

/**
 * Created by Mack on 7/25/2017.
 */
object InfernoContext {
	
	/**
	 * The constant value for the tile outside the pool.
	 */
	val INFERNO_OUTSIDE_LOC = Tile(2495, 5111, 0)
	
	/**
	 * The {@link Npc} constant identifier for the 'Nibbler' npc.
	 */
	const val NIBBLER = 7691
	
	/**
	 * The {@link Npc} constant identifier for the 'Blob' npc.
	 */
	const val BLOB = 7693
	
	/**
	 * The {@link Npc} constant identifier for the 'Jal-AkRek-Mej' npc.
	 */
	const val JAL_AKREK_MEJ = 7694
	
	/**
	 * The {@link Npc} constant identifier for the 'Jal-AkRek-Xil' npc.
	 */
	const val JAL_AKREK_XIL = 7695
	
	/**
	 * The {@link Npc} constant identifier for the 'Jal-AkRek-Ket' npc.
	 */
	const val JAL_AKREK_KET = 7696
	
	/**
	 * The {@link Npc} constant identifier for the 'Bat' npc.
	 */
	const val BAT = 7692
	
	/**
	 * The {@link Npc} constant identifier for the 'Jal-ImKot' npc.
	 */
	const val MELEE = 7697
	
	/**
	 * The {@link Npc} constant identifier for the 'Jal-Xil' npc.
	 */
	const val RANGER = 7698
	
	/**
	 * The {@link Npc} constant identifier for the 'Jal-Zek' npc.
	 */
	const val MAGER = 7699
	
	/**
	 * The {@link Npc} constant identifier for the 'JalTok-Jad' npc.
	 */
	const val JALTOK_JAD = 7700
	
	/**
	 * The {@link Npc} constant identifier for the 'Yt-HurKot' npc.
	 */
	const val YT_HURKOT = 7705
	
	/**
	 * The {@link Npc} constant identifier for the 'TzKal-Zuk' npc.
	 */
	const val TZKAL_ZUK = 7706
	
	/**
	 * The {@link Npc} constant identifier for the 'Jal-MajJak' npc.
	 */
	const val JAL_MEJJAK = 7708
	
	/**
	 * The {@link Npc} constant identifier for the 'Ancestral Glyph' npc.
	 */
	const val ANCESTRAL_GLYPH = 7707
	
	/**
	 * The starting point we use to teleport in.
	 */
	val startTile: Tile = Tile(2240, 5312)
	
	/**
	 * The end location where we send the player after the inferno session has ended.
	 */
	val endTile: Tile = Tile(2496, 5113)
	
	/**
	 * The overlay id for displaying TzKal-Zuk's hp on the final wave.
	 */
	const val TZKAL_ZUK_OVERLAY = 596
	
	/**
	 * The {@link Npc} constant identifier for the 'Invisible Pillar' npc.
	 */
	const val PILLAR_INVIS = 7709
	
	/**
	 * The {@link Npc} constant identifier for the 'Visible Pillar' npc.
	 */
	const val PILLAR_VISIBLE = 7710
	
	/**
	 * A flag checking if the npc is classified as a "inferno" npc.
	 */
	@JvmStatic fun isInfernoNpc(npc: Npc): Boolean {
		return(npc.id() == NIBBLER || npc.id() == BLOB || npc.id() == BAT || npc.id() == MELEE
				|| npc.id() == RANGER || npc.id() == MAGER || npc.id() == YT_HURKOT || npc.id() == JALTOK_JAD
				|| npc.id() == TZKAL_ZUK || npc.id() == JAL_MEJJAK
				|| npc.id() == JAL_AKREK_XIL || npc.id() == JAL_AKREK_MEJ || npc.id() == JAL_AKREK_KET || npc.id() == ANCESTRAL_GLYPH)
	}
	
	/**
	 * The npcs we should send respective to the round the player is on.
	 */
	val waveData = arrayListOf(
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 6)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(BLOB, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(BLOB, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 5)),//wave 8
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(MELEE, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(MELEE, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(MELEE, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(MELEE, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2), InfernoWaveEntry(MELEE, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 2)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 5)),
			
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 2), InfernoWaveEntry(RANGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(RANGER, 2)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 5)),
			
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 1),InfernoWaveEntry(MAGER, 1)),
			
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(MELEE, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(MELEE, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1),InfernoWaveEntry(MAGER, 1)),//45
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1),InfernoWaveEntry(MAGER, 1)),//46
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1),InfernoWaveEntry(MAGER, 1)),//47
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2), InfernoWaveEntry(MELEE, 1),InfernoWaveEntry(MAGER, 1)),//48
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 2),InfernoWaveEntry(MAGER, 1)),//49
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),//50
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),//52
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 1), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BAT, 2), InfernoWaveEntry(BLOB, 1), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(BLOB, 2), InfernoWaveEntry(MELEE, 1), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MELEE, 2), InfernoWaveEntry(RANGER, 1),InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(RANGER, 2), InfernoWaveEntry(MAGER, 1)),
			InfernoWave(InfernoWaveEntry(NIBBLER, 3), InfernoWaveEntry(MAGER, 2)),
			InfernoWave(InfernoWaveEntry(JALTOK_JAD, 1)),
			InfernoWave(InfernoWaveEntry(JALTOK_JAD, 3)),
			InfernoWave(InfernoWaveEntry(TZKAL_ZUK, 1))
	)
	
	/**
	 * Gathers the potential spawn tiles into a collection.
	 */
	fun tileSpawns(player: Player): java.util.ArrayList<Tile> {
		
		val map = player.world().allocator().active(player.tile()).orElse(null)
		var area = map ?: return java.util.ArrayList()
		var tiles: java.util.ArrayList<Tile> = java.util.ArrayList()
		
		tiles.add(Tile(area.center().x + 3, area.center().z + 2))
		tiles.add(Tile(area.center().x - 3, area.center().z - 2))
		tiles.add(Tile(area.center().x - 3, area.center().z - 6))
		
		tiles.add(Tile(area.center().x + 5, area.center().z - 4))
		tiles.add(Tile(area.center().x + 7, area.center().z + 4))
		tiles.add(Tile(area.center().x - 7, area.center().z + 6))
		
		return tiles
	}
	
	/**
	 * A flag checking if the session is ready for the next wave.
	 */
	@JvmStatic fun shouldAdvanceWave(player: Player): Boolean {
		
		//Grabs the stashed inferno session, if available.
		val session: InfernoSession = player.attrib<InfernoSession>(AttributeKey.INFERNO_SESSION) ?: return false
		
		return (session.activeNpcs.isEmpty() || session.activeNpcs.size == 0)
	}
	
	/**
	 * Gets the stashed session in the attribute map.
	 */
	@JvmStatic fun get(player: Player): InfernoSession? {
		return (player.attrib<InfernoSession>(AttributeKey.INFERNO_SESSION))
	}
	
	/**
	 * A function returning the state of the active map the player is involved in, if any.
	 */
	@JvmStatic fun activeArea(player: Player): InstancedMap? {
		return (player.world().allocator().active(player.tile())).orElse(null)
	}
	
	/**
	 * A flag checking if the player is in an active session.
	 */
	@JvmStatic fun inSession(player: Player): Boolean {
		if (player.index() == -1 || !player.active()) {
			return false
		}
		return (get(player) != null)
	}
	
	/**
	 * Updates the current hp varbit value for the final boss overlay.
	 * Only updates on hit.
	 */
	@JvmStatic fun updateBossOverlay(npc: Npc, player: Player) {
		player.varps().varbit(5653, npc.hp())
	}
	
	/**
	 * A flag checking if the overlay should be displayed.
	 */
	@JvmStatic fun shouldUpdateOverlay(player: Player, npc: Npc): Boolean {
		if (get(player) == null) {
			return false
		}
		
		return (get(player)!!.wave == 69 && npc.id() == TZKAL_ZUK)
	}
	
	/**
	 * Actions we handle when the player logs out of the active session.
	 */
	@JvmStatic fun canLogout(player: Player): Boolean {
		
		if (get(player) == null) {
			return true
		}
		
		if (!waveEmpty(player) && !player.attribOr<Boolean>(AttributeKey.INFERNO_LOGOUT_WARNING, false)) {
			player.message("<col=FF0000>Attention: Your log out request has been received. You will be able to log out after this wave has finished.")
			player.message("<col=FF0000>You will resume your next wave on log in.")
			player.putattrib(AttributeKey.INFERNO_LOGOUT_WARNING, true)
			player.putattrib(AttributeKey.LOGOUT, false)
			return false
		}
		
		//Set the session to false manually since logging out doesnt call our "end" method.
		get(player)!!.active = false
		
		return true
	}
	
	/**
	 * A flag checking if the current wave is empty allowing log out.
	 */
	@JvmStatic fun waveEmpty(player: Player): Boolean {
		if (get(player) == null) {
			return true
		}
		
		return (get(player)!!.activeNpcs.size == 0 || get(player)!!.activeNpcs.isEmpty())
	}
	
	/**
	 * A class representative of a single entry within a wave collection.
	 */
	data class InfernoWaveEntry(var npcId: Int, var count: Int)
}