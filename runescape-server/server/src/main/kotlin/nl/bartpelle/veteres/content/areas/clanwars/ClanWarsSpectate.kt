package nl.bartpelle.veteres.content.areas.clanwars

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.LocationUtilities
import java.util.*

/**
 * Created by Mack on 9/12/2017.
 */
object ClanWarsSpectate {

	private const val VIEWING_ORB = 26741
    private const val VIEW_ORB_INTER_LAYER = 374
    private const val CENTER_ORB = 11
    private val SPECTATE_VALUES = ClanSpectateInfo.values()
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
        sr.onObject(VIEWING_ORB, {
            if (it.interactionOption() == 1) {
                spectateOrb(it.player())
            }
        })
        sr.onButton(VIEW_ORB_INTER_LAYER, 5, {
            resetViewer(it.player())
        })
        for (btn in arrayOf(11, 12, 13, 14, 15)) {
            sr.onButton(VIEW_ORB_INTER_LAYER, btn, {
                view(it.player(), btn)
            })
        }
	}

    private fun spectateOrb(player: Player) {
        val map = player.world().allocator().active(player.tile())
        if (!map.isPresent) {
            player.message("There is nothing to spectate.")
            return
        }

        player.putattrib(AttributeKey.CACHED_TILE, player.tile())
        view(player, CENTER_ORB)
    }
	
	fun view(player: Player, button: Int) {
        if (!player.looks().invisible()) {
            player.lock()
            player.looks().invisible(true)
            player.looks().hide(true)
            player.interfaces().sendWidgetOn(VIEW_ORB_INTER_LAYER, Interfaces.InterSwitches.INVENTORY_INTER)
        }

        mutateTile(player, button)
	}

    private fun resetViewer(player: Player) {
        player.unlock()
        player.looks().invisible(false)
        player.looks().hide(false)
        player.interfaces().closeById(VIEW_ORB_INTER_LAYER)
        player.teleport(player.attrib(AttributeKey.CACHED_TILE))
    }

    private fun mutateTile(player: Player, button: Int) {
        val map = player.world().allocator().active(player.tile())
        val idx = button - 11
//        val arena = ClanWarsMap[player.varps().varbit(CWSettings.ARENA)]
        val orbLoc = /*ClanSpectateInfo.infoForName(arena.name)*/ClanSpectateInfo.PLATEAU
        val relativeLocation = LocationUtilities.dynamicTileFor(orbLoc.tiles()[idx], ClanWarsMap.PLATEAU.cornerTile, map.get())

        player.teleport(relativeLocation)
    }

    //center, nw, ne, se, sw
	enum class ClanSpectateInfo(private val regions: ArrayList<Int>,  private val tiles: ArrayList<Tile>) {
        PLATEAU(arrayListOf(13133), arrayListOf(Tile(3286, 4958), Tile(3279, 4976), Tile(3306, 4976), Tile(3306, 4951), Tile(3281, 4943)));

        fun regions(): ArrayList<Int> = regions

        fun tiles(): ArrayList<Tile> = tiles

        companion object {

            fun infoForName(name: String): ClanSpectateInfo {
                return Arrays.stream(SPECTATE_VALUES).filter({type -> type.name.equals(name, true)}).findAny().get()
            }
        }
	}
}