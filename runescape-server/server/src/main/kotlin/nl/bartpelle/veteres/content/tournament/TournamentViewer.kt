package nl.bartpelle.veteres.content.tournament

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.net.message.game.command.SendTournamentFighters
import nl.bartpelle.veteres.script.ScriptRepository

object TournamentViewer {

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onObject(26747, s@ @Suspendable {
            val player = it.player()
            if (TournamentConfig.tournamentDisabled) {
                player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently disabled. Please try again later."))
                return@s
            }
            player.interfaces().sendMain(383)
            player.write(SendTournamentFighters("Situations", "Bart"))
        })
    }
}