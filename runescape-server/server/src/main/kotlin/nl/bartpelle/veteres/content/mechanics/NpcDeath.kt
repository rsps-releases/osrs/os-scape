package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import droptables.ScalarLootTable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild.MagicalAnimator
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveMonsters
import nl.bartpelle.veteres.content.npcs.bosses.KrakenBoss
import nl.bartpelle.veteres.content.npcs.bosses.abyssalsire.AbyssalSireChamber
import nl.bartpelle.veteres.content.npcs.godwars.GwdLogic
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.skills.prayer.BoneBurying
import nl.bartpelle.veteres.content.skills.slayer.Slayer
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature
import nl.bartpelle.veteres.content.skills.slayer.SuperiorSlayer
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailClueLevel
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailMonsters
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import java.util.*

/**
 * Created by Bart on 10/6/2015.
 */
object NpcDeath {

	val customDrops = arrayListOf(496, 494, 492, 493, 2042, 2043, 2044)

	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc: Npc = it.ctx()
		it.clearContext()
		
		// Path reset instantly when hitsplat appears killing the npc.
		npc.world().server().scriptExecutor().interruptFor(this)
		npc.pathQueue().clear()
		npc.lockNoDamage()
		
		val killer_id: Optional<Int> = npc.killer()
		val opt_player: Optional<Player> = if (killer_id.isPresent) npc.world().playerForId(killer_id.get()) else Optional.empty()
		// Player that did the most damage.
		val killer: Player? = if (killer_id.isPresent && opt_player.isPresent) opt_player.get() else null
		
		// Person that did the final hit, killing us.
		val killedBy: Entity? = npc.attribOr(AttributeKey.KILLER, null)
		
		// Try to do our death anim
		it.delay(1)
		
		// 1t later facing is reset. Stops npcs looking odd when they reset facing their target the tick they die.
		npc.sync().faceEntity(null)
		
		it.delay(1)
		
		var isBoss = false
		
		//This is for npc v npc death checks.
		if (npc.attribOr<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER, null) != null) {
			npc.attrib<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER).triggerNpcKilledDeath()
		}
		
		if (killer != null) {
			
			// Increment kill. Logging of kills only, does not act as a KC tracker!
			killer.registerNpcKill(npc.id())
			
			AchievementAction.processCategoryAchievement(killer, AchievementCategory.PVM, npc.id())
			
			// Give slayer xp if this is a slayer monster
			if ((npc.combatInfo()?.slayerxp ?: 0) != 0) {
				
				// Check our task. Decrease. Reward. leggo
				val task: Int = killer.attribOr(AttributeKey.SLAYER_TASK_ID, 0)
				var amt: Int = killer.attribOr(AttributeKey.SLAYER_TASK_AMT, 0)
				
				if (killer.attribOr(AttributeKey.WILDERNESS_SLAYER_TASK_ACTIVE, true) && SlayerCreature.lookup(task) != null && SlayerCreature.lookup(task)!!.matches(npc.id()) && !WildernessLevelIndicator.inWilderness(npc.tile())) {
					killer.message("<col=FF0000>You must kill your slayer assignment within the wilderness to receive experience!")
				}
				
				if (!killer.world().realm().isPVP && task > 0 && amt > 0 || killer.world().realm().isPVP && task > 0 && amt > 0 && killer.attribOr(AttributeKey.WILDERNESS_SLAYER_TASK_ACTIVE, false) && WildernessLevelIndicator.inWilderness(npc.tile())) {
					// Resolve taskdef
					val taskdef = SlayerCreature.lookup(task)
					if (taskdef != null && taskdef.matches(npc.id()) && !Skills.disabled(killer, Skills.SLAYER)) {
						
						// Give xp in a new script to avoid blocking when we level up
						killer.executeScript({ it.addXp(Skills.SLAYER, npc.combatInfo().slayerxp) })
						
						// Decrease task number
						killer.putattrib(AttributeKey.SLAYER_TASK_AMT, amt - 1)
						amt -= 1
						
						// Update the slayer interface with the proper assignment amt
						Slayer.displayCurrentAssignment(killer)
						
						// Finished?
						if (amt == 0) {
							// Give points
							val spree: Int = killer.attribOr<Int>(AttributeKey.SLAYER_TASK_SPREE, 0) + 1
							val master = Math.max(1, killer.varps().varbit(Varbit.SLAYER_MASTER))
							if (master == Slayer.TURAEL_ID) {
								killer.colmessage("You have completed your task; return to a Slayer master.", "7F00FF")
							} else {
								var ptsget = when {
									spree % 50 == 0 -> when (master) {
										Slayer.MAZCHNA_ID -> 15
										Slayer.VANNAKA_ID -> 60
										Slayer.CHAELDAR_ID -> 150
										Slayer.NIEVE_ID -> 180
										Slayer.DURADEL_ID -> 225
										Slayer.KRYSTILIA_ID -> 375
										else -> 0
									}
									spree % 10 == 0 -> when (master) {
										Slayer.MAZCHNA_ID -> 5
										Slayer.VANNAKA_ID -> 20
										Slayer.CHAELDAR_ID -> 50
										Slayer.NIEVE_ID -> 60
										Slayer.DURADEL_ID -> 75
										Slayer.KRYSTILIA_ID -> 125
										else -> 0
									}
									else -> when (master) {
										Slayer.MAZCHNA_ID -> 2
										Slayer.VANNAKA_ID -> 4
										Slayer.CHAELDAR_ID -> 10
										Slayer.NIEVE_ID -> 12
										Slayer.DURADEL_ID -> 15
										Slayer.KRYSTILIA_ID -> 25
										else -> 0
									}
								}
								
								// Multiply it because otherwise it won't match the leveling :)
								ptsget *= (killer.mode().slayerPointMultiplier().toDouble() * killer.donationTier().modifiers()[DonatorBoost.SLAYER_PTS.ordinal]).toInt()
								
								if (ptsget > 0) {
									killer.colmessage("You've completed $spree tasks in a row and received $ptsget points; return to a Slayer Master.", "7F00FF")
									killer.varps().varbit(Varbit.SLAYER_POINTS, Math.min(0xFFFF, killer.varps().varbit(Varbit.SLAYER_POINTS) + ptsget))
								}
								
								AchievementAction.processCategoryAchievement(killer, AchievementCategory.SLAYER)
								
								if (killer.attribOr<Boolean>(AttributeKey.WILDERNESS_SLAYER_TASK_ACTIVE, false)) {
									killer.putattrib(AttributeKey.WILDERNESS_SLAYER_TASK_ACTIVE, false)
								}
							}
							
							killer.putattrib(AttributeKey.SLAYER_TASK_SPREE, spree)
						} else {
							// Chance to spawn a superior slayer one if unlocked.
							SuperiorSlayer.trySpawn(killer, taskdef, npc)
						}
					}
				}
			}
			
			//Remove the reanimated NPC ownership for player so they can spawn another! : )
			val isReanimatedMonster = npc.attribOr<Boolean>(AttributeKey.IS_REANIMATED_MONSTER, true)
			val hasReanimatedMonster = killer.attribOr<Boolean>(AttributeKey.HAS_REANIMATED_MONSTER, true)
			if (isReanimatedMonster) {
				if (hasReanimatedMonster)
					killer.clearattrib(AttributeKey.HAS_REANIMATED_MONSTER)
			}
			
			// Check if the dead npc is a barrows brother. Award killcount.
			var isBarrowsBro = false
			val name: String = npc.def().name
			when (name.toLowerCase()) {
			// Slayer NPCs
				"crawling hand" -> killer.putattrib(AttributeKey.KC_CRAWL_HAND, 1 + killer.attribOr<Int>(AttributeKey.KC_CRAWL_HAND, 0))
				"cave bug" -> killer.putattrib(AttributeKey.KC_CAVE_BUG, 1 + killer.attribOr<Int>(AttributeKey.KC_CAVE_BUG, 0))
				"cave crawler" -> killer.putattrib(AttributeKey.KC_CAVE_CRAWLER, 1 + killer.attribOr<Int>(AttributeKey.KC_CAVE_CRAWLER, 0))
				"banshee" -> killer.putattrib(AttributeKey.KC_BANSHEE, 1 + killer.attribOr<Int>(AttributeKey.KC_BANSHEE, 0))
				"cave slime" -> killer.putattrib(AttributeKey.KC_CAVE_SLIME, 1 + killer.attribOr<Int>(AttributeKey.KC_CAVE_SLIME, 0))
				"rockslug" -> killer.putattrib(AttributeKey.KC_ROCKSLUG, 1 + killer.attribOr<Int>(AttributeKey.KC_ROCKSLUG, 0))
				"desert lizard" -> killer.putattrib(AttributeKey.KC_DESERT_LIZARD, 1 + killer.attribOr<Int>(AttributeKey.KC_DESERT_LIZARD, 0))
				"cockatrice" -> killer.putattrib(AttributeKey.KC_COCKATRICE, 1 + killer.attribOr<Int>(AttributeKey.KC_COCKATRICE, 0))
				"pyrefiend" -> killer.putattrib(AttributeKey.KC_PYREFRIEND, 1 + killer.attribOr<Int>(AttributeKey.KC_PYREFRIEND, 0))
				"mogre" -> killer.putattrib(AttributeKey.KC_MOGRE, 1 + killer.attribOr<Int>(AttributeKey.KC_MOGRE, 0))
				"harpie bug swarm" -> killer.putattrib(AttributeKey.KC_HARPIE_BUG, 1 + killer.attribOr<Int>(AttributeKey.KC_HARPIE_BUG, 0))
				"wall beast" -> killer.putattrib(AttributeKey.KC_WALL_BEAST, 1 + killer.attribOr<Int>(AttributeKey.KC_WALL_BEAST, 0))
				"killerwatt" -> killer.putattrib(AttributeKey.KC_KILLERWATT, 1 + killer.attribOr<Int>(AttributeKey.KC_KILLERWATT, 0))
				"molanisk" -> killer.putattrib(AttributeKey.KC_MOLANISK, 1 + killer.attribOr<Int>(AttributeKey.KC_MOLANISK, 0))
				"basilisk" -> killer.putattrib(AttributeKey.KC_BASILISK, 1 + killer.attribOr<Int>(AttributeKey.KC_BASILISK, 0))
				"sea snake" -> killer.putattrib(AttributeKey.KC_SEASNAKE, 1 + killer.attribOr<Int>(AttributeKey.KC_SEASNAKE, 0))
				"terror dog" -> killer.putattrib(AttributeKey.KC_TERRORDOG, 1 + killer.attribOr<Int>(AttributeKey.KC_TERRORDOG, 0))
				"fever spider" -> killer.putattrib(AttributeKey.KC_FEVER_SPIDER, 1 + killer.attribOr<Int>(AttributeKey.KC_FEVER_SPIDER, 0))
				"infernal mage" -> killer.putattrib(AttributeKey.KC_INFERNAL_MAGE, 1 + killer.attribOr<Int>(AttributeKey.KC_INFERNAL_MAGE, 0))
				"brine rat" -> killer.putattrib(AttributeKey.KC_BRINERAT, 1 + killer.attribOr<Int>(AttributeKey.KC_BRINERAT, 0))
				"bloodveld" -> killer.putattrib(AttributeKey.KC_BLOODVELD, 1 + killer.attribOr<Int>(AttributeKey.KC_BLOODVELD, 0))
				"jelly" -> killer.putattrib(AttributeKey.KC_JELLY, 1 + killer.attribOr<Int>(AttributeKey.KC_JELLY, 0))
				"turoth" -> killer.putattrib(AttributeKey.KC_TUROTH, 1 + killer.attribOr<Int>(AttributeKey.KC_TUROTH, 0))
				"zygomite" -> killer.putattrib(AttributeKey.KC_ZYGOMITE, 1 + killer.attribOr<Int>(AttributeKey.KC_ZYGOMITE, 0))
				"cave horror" -> killer.putattrib(AttributeKey.KC_CAVEHORROR, 1 + killer.attribOr<Int>(AttributeKey.KC_CAVEHORROR, 0))
				"aberrant spectre" -> killer.putattrib(AttributeKey.KC_ABERRANT_SPECTRE, 1 + killer.attribOr<Int>(AttributeKey.KC_ABERRANT_SPECTRE, 0))
				"spiritual ranger" -> killer.putattrib(AttributeKey.KC_SPIRITUAL_RANGER, 1 + killer.attribOr<Int>(AttributeKey.KC_SPIRITUAL_RANGER, 0))
				"dust devil" -> killer.putattrib(AttributeKey.KC_DUSTDEVIL, 1 + killer.attribOr<Int>(AttributeKey.KC_DUSTDEVIL, 0))
				"spiritual warrior" -> killer.putattrib(AttributeKey.KC_SPIRITUAL_WARRIOR, 1 + killer.attribOr<Int>(AttributeKey.KC_SPIRITUAL_WARRIOR, 0))
				"kurask" -> killer.putattrib(AttributeKey.KC_KURASK, 1 + killer.attribOr<Int>(AttributeKey.KC_KURASK, 0))
				"skeletal wyvern" -> killer.putattrib(AttributeKey.KC_SKELETAL_WYVERN, 1 + killer.attribOr<Int>(AttributeKey.KC_SKELETAL_WYVERN, 0))
				"gargoyle" -> killer.putattrib(AttributeKey.KC_GARGOYLE, 1 + killer.attribOr<Int>(AttributeKey.KC_GARGOYLE, 0))
				"nechryael" -> killer.putattrib(AttributeKey.KC_NECHRYAEL, 1 + killer.attribOr<Int>(AttributeKey.KC_NECHRYAEL, 0))
				"spiritual mage" -> killer.putattrib(AttributeKey.KC_SPIRITUAL_MAGE, 1 + killer.attribOr<Int>(AttributeKey.KC_SPIRITUAL_MAGE, 0))
				"abyssal demon" -> killer.putattrib(AttributeKey.KC_ABYSSALDEMON, 1 + killer.attribOr<Int>(AttributeKey.KC_ABYSSALDEMON, 0))
				"whirlpool" -> killer.putattrib(AttributeKey.KC_CAVEKRAKEN, 1 + killer.attribOr<Int>(AttributeKey.KC_CAVEKRAKEN, 0))
				"dark beast" -> killer.putattrib(AttributeKey.KC_DARKBEAST, 1 + killer.attribOr<Int>(AttributeKey.KC_DARKBEAST, 0))
				"smoke devil" -> killer.putattrib(AttributeKey.KC_SMOKEDEVIL, 1 + killer.attribOr<Int>(AttributeKey.KC_SMOKEDEVIL, 0))
				"vorkath" -> killer.putattrib(AttributeKey.VORKATH_KC, 1 + killer.attribOr<Int>(AttributeKey.VORKATH_KC, 0))
				"skotizo" -> {
                    isBoss = true
                    killer.putattrib(AttributeKey.SKOTIZO_KC, 1 + killer.attribOr<Int>(AttributeKey.SKOTIZO_KC, 0))
                    if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.SKOTIZO_KC))
                }
                "tekton" -> {
                    isBoss = true
                    killer.putattrib(AttributeKey.TEKTON_KC, 1 + killer.attribOr<Int>(AttributeKey.TEKTON_KC, 0))
                    if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.TEKTON_KC))
                }
                "ancient wyvern" -> {
                    isBoss = true
                    killer.putattrib(AttributeKey.ANCIENT_WYVERN_KC, 1 + killer.attribOr<Int>(AttributeKey.ANCIENT_WYVERN_KC, 0))
                    if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.ANCIENT_WYVERN_KC))
                }
                "battle mage" -> {
                    isBoss = true
                    killer.putattrib(AttributeKey.BATTLE_MAGE_KC, 1 + killer.attribOr<Int>(AttributeKey.BATTLE_MAGE_KC, 0))
                    if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.BATTLE_MAGE_KC))
                }
				"demonic gorilla" -> {
					isBoss = true
					killer.putattrib(AttributeKey.DEMONIC_GORILLA_KC, 1 + killer.attribOr<Int>(AttributeKey.DEMONIC_GORILLA_KC, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.DEMONIC_GORILLA_KC))
				}
			
			// Bosses
				"abyssal sire" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_ABYSSALSIRE, 1 + killer.attribOr<Int>(AttributeKey.KC_ABYSSALSIRE, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_ABYSSALSIRE))
				}
				"callisto" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_CALLISTO, 1 + killer.attribOr<Int>(AttributeKey.KC_CALLISTO, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_CALLISTO))
				}
				"cerberus" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_CERBERUS, 1 + killer.attribOr<Int>(AttributeKey.KC_CERBERUS, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_CERBERUS))
				}
				"chaos elemental" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_CHAOSELE, 1 + killer.attribOr<Int>(AttributeKey.KC_CHAOSELE, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_CHAOSELE))
				}
				"chaos fanatic" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_CHAOS_FANATIC, 1 + killer.attribOr<Int>(AttributeKey.KC_CHAOS_FANATIC, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_CHAOS_FANATIC))
				}
				"commander zilyana" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_ZILYANA, 1 + killer.attribOr<Int>(AttributeKey.KC_ZILYANA, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_ZILYANA))
				}
				"corporeal beast" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_CORPBEAST, 1 + killer.attribOr<Int>(AttributeKey.KC_CORPBEAST, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_CORPBEAST))
				}
				"crazy archaeologist" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_CRAZY_ARCH, 1 + killer.attribOr<Int>(AttributeKey.KC_CRAZY_ARCH, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_CRAZY_ARCH))
				}
				"dagannoth prime" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_PRIME, 1 + killer.attribOr<Int>(AttributeKey.KC_PRIME, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_PRIME))
				}
				"dagannoth rex" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_REX, 1 + killer.attribOr<Int>(AttributeKey.KC_REX, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_REX))
				}
				"dagannoth supreme" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_SUPREME, 1 + killer.attribOr<Int>(AttributeKey.KC_SUPREME, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_SUPREME))
				}
				"general graardor" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_GRAARDOR, 1 + killer.attribOr<Int>(AttributeKey.KC_GRAARDOR, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_GRAARDOR))
				}
				"giant mole" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_GIANTMOLE, 1 + killer.attribOr<Int>(AttributeKey.KC_GIANTMOLE, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_GIANTMOLE))
				}
				"k'ril tsutsaroth" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_KRIL_TSUT, 1 + killer.attribOr<Int>(AttributeKey.KC_KRIL_TSUT, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_KRIL_TSUT))
				}
				"kalphite queen" -> {
					isBoss = true
					if (npc.id() == 6501) { // we don't want the kalphite queen's first stage to effect this
						killer.putattrib(AttributeKey.KC_KQ, 1 + killer.attribOr<Int>(AttributeKey.KC_KQ, 0))
						if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_KQ))
					}
				}
				"king black dragon" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_KBD, 1 + killer.attribOr<Int>(AttributeKey.KC_KBD, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_KBD))
				}
				"kraken" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_KRAKEN, 1 + killer.attribOr<Int>(AttributeKey.KC_KRAKEN, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_KRAKEN))
				}
				"kree'arra" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_KREEARRA, 1 + killer.attribOr<Int>(AttributeKey.KC_KREEARRA, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_KREEARRA))
				}
				"penance queen" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_PENANCEQUEEM, 1 + killer.attribOr<Int>(AttributeKey.KC_PENANCEQUEEM, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_PENANCEQUEEM))
				}
				"scorpia" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_SCORPIA, 1 + killer.attribOr<Int>(AttributeKey.KC_SCORPIA, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_SCORPIA))
				}
				"thermonuclear smoke devil" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_THERMY, 1 + killer.attribOr<Int>(AttributeKey.KC_THERMY, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_THERMY))
				}
				"tztok-jad" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_JAD, 1 + killer.attribOr<Int>(AttributeKey.KC_JAD, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_JAD))
				}
				"venenatis" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_VENENATIS, 1 + killer.attribOr<Int>(AttributeKey.KC_VENENATIS, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_VENENATIS))
				}
				"vet'ion" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_VETION, 1 + killer.attribOr<Int>(AttributeKey.KC_VETION, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_VETION))
				}
				"zulrah" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_ZULRAH, 1 + killer.attribOr<Int>(AttributeKey.KC_ZULRAH, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_ZULRAH))
				}
				"tzkal-zuk" -> {
					isBoss = true
					killer.putattrib(AttributeKey.KC_TZKAL_ZUK, 1 + killer.attribOr<Int>(AttributeKey.KC_TZKAL_ZUK, 0))
					if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", name, killer.attrib(AttributeKey.KC_TZKAL_ZUK))
				}
			}
			when (npc.id()) {
				1673 -> {
					isBarrowsBro = true
					killer.varps().varbit(Varbit.DHAROK, 1)
				}
				1672 -> {
					isBarrowsBro = true
					killer.varps().varbit(Varbit.AHRIM, 1)
				}
				1677 -> {
					isBarrowsBro = true
					killer.varps().varbit(Varbit.VERAC, 1)
				}
				1676 -> {
					isBarrowsBro = true
					killer.varps().varbit(Varbit.TORAG, 1)
				}
				1675 -> {
					isBarrowsBro = true
					killer.varps().varbit(Varbit.KARIL, 1)
				}
				1674 -> {
					isBarrowsBro = true
					killer.varps().varbit(Varbit.GUTHAN, 1)
				}
				6615 -> {
					npc.world().npcs().forEachInAreaKt(Area(3219, 3248, 10329, 10353), { npc ->
						if (npc.id() == 6617) {
							npc.world().unregisterNpc(npc)
						}
					})
				}
				KrakenBoss.KRAKEN_WHIRLPOOL -> { // Kraken boss transmogged KC
					if (npc.sync().transmog() == KrakenBoss.KRAKEN_NPCID) {
						killer.putattrib(AttributeKey.KC_KRAKEN, 1 + killer.attribOr<Int>(AttributeKey.KC_KRAKEN, 0))
						if (!VarbitAttributes.varbiton(killer, 12)) killer.message("Your %s kill count is: <col=FF0000>%d</col>.", "Cave Kraken", killer.attrib(AttributeKey.KC_KRAKEN))
					}
				}
			}
			
			if (isBarrowsBro) {
				killer.clearattrib(AttributeKey.barrowsBroSpawned)
				killer.varps().varbit(Varbit.BARROWS_MONSTER_KC, 1 + killer.varps().varbit(Varbit.BARROWS_MONSTER_KC))
				val newkc: Int = killer.varps().varbit(Varbit.BARROWS_MONSTER_KC)
				//killer.message("Killcount: "+newkc.toString())
				killer.write(SetHintArrow(-1))
			}
			val killerOpp: Entity? = killer.attribOr<Entity>(AttributeKey.LAST_DAMAGER, null);
			if (killer.varps().varbit(Varbit.MULTIWAY_AREA) == 0 && killerOpp != null && killerOpp == npc) { // Last fighting with this dead npc.
				killer.clearattrib(AttributeKey.LAST_WAS_ATTACKED_TIME) // Allow instant aggro from other npcs/players.
			}
			
			var done = booleanArrayOf(false)
			MagicalAnimator.ArmourSets.values().forEach { set ->
				if (!done[0] && set.npc == npc.id()) {
					done[0] = true
					killer.write(SetHintArrow(-1)) // remove hint arrow
				}
			}
		}
		
		npc.animate(npc.combatInfo()?.animations?.death ?: -1)
		
		// Death sound!
		if (killer != null) {
			val sounds = npc.combatInfo()?.sounds?.death
			if (sounds != null && sounds.size > 0) {
				killer.sound(npc.world().random(sounds))
			}
		}
		
		it.delay(npc.combatInfo()?.deathlen ?: 5)
		
		if (killer != null) {
			
			//If our player killed any of the inferno npcs.
			if (InfernoContext.isInfernoNpc(npc) && InfernoContext.inSession(killer)) {
				InfernoContext.get(killer)!!.handleNpcDeath(npc)
			}
			
			//If our player killed a Tz-Kek, spawn it's babies!
			for (fightCaveMonsters in arrayListOf(3116, 3118, 3120, 3121, 3123, 3125, 3127, 3128)) {
				if (npc.id() == fightCaveMonsters) {
					if (killer.isPlayer) {
						val player = killer
						FightCaveMonsters.handleMonsterDeath(npc, player)
					}
				}
			}
			
			if (npc.id() == 101 || npc.id() == 103) {
				when (npc.id()) {
					101 -> npc.sync().transmog(101)
					103 -> npc.sync().transmog(103)
				}
				
				npc.walkRadius(0)
			}
			
			val abyssalSire = 5886
			if (npc.id() == abyssalSire) {
				AbyssalSireChamber.cleanAndResetChamber(npc)
			}
			
			val table = ScalarLootTable.forNPC(npc.id())
			if (table != null && npc.id() != 6500) {
				val tile = npc.tile()
				if(!customDrops.contains(npc.id())) {
					table.guaranteedDrops.forEach {
						if (killer.inventory().has(13116)) {
							for (BONES in intArrayOf(526, 528, 530, 2859, 532, 10976, 10977, 3125, 534, 536, 4812,
									4834, 6812, 6729, 11943)) if (it.convert().id() == BONES) {
								val bones = BoneBurying.Bone.get(it.convert().id())
								killer.executeScript({ it.addXp(Skills.PRAYER, bones!!.xp) })
							}
						} else {
							if (it.min > 0) {
								val dropped = Item(it.id, killer.world().random(it.min..it.max))
								killer.world().spawnGroundItem(GroundItem(killer.world(), dropped, tile, killer.id()))
							} else {
								killer.world().spawnGroundItem(GroundItem(killer.world(), it.convert(), tile, killer.id()))
							}

							notification(killer, it.convert())
						}
					}

					var dropRolls = npc.combatInfo().droprolls

					// During the Untamed blessing, bosses have two drop rolls :)
					if (npc.combatInfo().boss && BonusContent.isActive(killer, BlessingGroup.UNTAMED)) {
						dropRolls *= 2
						killer.message("The active Fable blessing grants you a second drop!")
					}

					for (i in 1..dropRolls) {
						val reward = table.randomItem(killer.world().random(), false, false, false)
						if (reward != null) {
							killer.world().spawnGroundItem(GroundItem(killer.world(), reward, tile, killer.id()))
							notification(killer, reward)
							ServerAnnouncements.tryBroadcastDrop(killer, reward.id())

							// Corp beast drops are displayed to surrounding players.
							if (npc.id() == 319) {
								killer.world().players().forEachInAreaKt(Area(2944, 4352, 3007, 4415), { p ->
									p.message("<col=0B610B>" + killer.name() + " received a drop: " + (if (reward.unnote(killer.world()).amount() == 1)
										reward.name(killer.world())
									else "" + reward.amount() + " x " + reward.unnote(killer.world()).name(killer.world())) + ".")
								})
							}
						}
					}

					// Realism benefit
					if (killer.mode() == GameMode.REALISM && killer.world().rollDie(10, 1)) {
						killer.message("Your endurance in game mode has given you an extra drop!")
						val reward = table.randomItem(killer.world().random(), false, false, false)
						if (reward != null) {
							killer.world().spawnGroundItem(GroundItem(killer.world(), reward, tile, killer.id()))
							notification(killer, reward)
							ServerAnnouncements.tryBroadcastDrop(killer, reward.id())
						}
					}

					val inWilderness: Boolean = killer.tile().x in 2942..3328 && killer.tile().z > 3524 && killer.tile().z < 3968
					val casket = Item(7956)
					val resourceCasket = Item(405)
					val combat = killer.skills().combatLevel()
					val mul: Int = if (killer.mode() == GameMode.CLASSIC) 2 else if (killer.mode() == GameMode.REALISM) 3 else 1
					var chance: Int =
							when {
								combat <= 10 -> 1
								combat <= 20 -> 2
								combat <= 80 -> 3
								combat <= 120 -> 4
								combat > 120 -> 5
								else -> 5
							}
					val regularOdds = if (killer.world().realm().isOSRune) 65 else 100

					chance *= mul

					//If the player is in the wilderness, they have an increased chance at a casket drop
					if (!killer.world().realm().isPVP && (npc.maxHp() > 20 || WildernessLevelIndicator.inWilderness(killer.tile()))) {
						if (inWilderness && killer.world().random(regularOdds - 15) < chance && killer.ironMode() == IronMode.NONE) {
							if (isBoss && killer.world().random(3) == 2) {
								killer.message("<col=0B610B>You have received a boss casket drop!");
								killer.world().spawnGroundItem(GroundItem(killer.world(), Item(6759), tile, killer.id()))
								notification(killer, Item(6759))
							} else {
								killer.message("<col=0B610B>You have received a casket drop!");
								killer.world().spawnGroundItem(GroundItem(killer.world(), casket, tile, killer.id()))
								notification(killer, Item(casket))
							}
						} else if (!inWilderness && killer.world().random(regularOdds) < chance && killer.ironMode() == IronMode.NONE) {
							if (isBoss && killer.world().random(5) == 2) {
								killer.message("<col=0B610B>You have received a boss casket drop!");
								killer.world().spawnGroundItem(GroundItem(killer.world(), Item(6759), tile, killer.id()))
								notification(killer, Item(6759))
							} else {
								killer.message("<col=0B610B>You have received a casket drop!");
								killer.world().spawnGroundItem(GroundItem(killer.world(), casket, tile, killer.id()))
								notification(killer, Item(casket))
							}
						}

						if (inWilderness && killer.world().random(regularOdds) <= chance && killer.ironMode() == IronMode.NONE) {
							killer.message("<col=0B610B>You have received a wilderness casket drop!");
							killer.world().spawnGroundItem(GroundItem(killer.world(), Item(resourceCasket), tile, killer.id()))
							notification(killer, Item(resourceCasket))
						}

						// Wilderness caskets!
						if (npc.maxHp() > 20 && WildernessLevelIndicator.inWilderness(killer.tile())) {
							if (killer.world().rollDie(30, 1)) {
								if (Item(11941) !in killer.inventory() && Item(11941) !in killer.bank()) {
									killer.world().spawnGroundItem(GroundItem(killer.world(), Item(11941), tile, killer.id()))
									notification(killer, Item(11941))
								}
							}
						}
					}

					val clueChance = when (killer.donationTier()) {
						DonationTier.ULTIMATE_DONATOR -> if (killer.world().realm().isRealism) 100 else 50
						DonationTier.GRAND_MASTER_DONATOR -> if (killer.world().realm().isRealism) 120 else 60
						DonationTier.MASTER_DONATOR -> if (killer.world().realm().isRealism) 130 else 64
						DonationTier.LEGENDARY_DONATOR -> if (killer.world().realm().isRealism) 140 else 70
						DonationTier.EXTREME_DONATOR -> if (killer.world().realm().isRealism) 150 else 75
						DonationTier.SUPER_DONATOR -> if (killer.world().realm().isRealism) 160 else 80
						DonationTier.DONATOR -> if (killer.world().realm().isRealism) 170 else 85
						else -> if (killer.world().realm().isRealism) 200 else 90
					}

					//If the monster can drop a clue scroll casket, roll! : )
					if (!killer.world().realm().isPVP && killer.world().rollDie(clueChance, 1)) {
						val clueScrollMonsterLevel = TreasureTrailMonsters.clueScrollForMonster(npc)
						if (clueScrollMonsterLevel != TreasureTrailClueLevel.UNKNOWN) {
							val clueItem: Item? = when (clueScrollMonsterLevel) {
								TreasureTrailClueLevel.EASY -> Item(TreasureTrailRewardCaskets.EASY_CASKET)
								TreasureTrailClueLevel.MEDIUM -> Item(TreasureTrailRewardCaskets.MEDIUM_CASKET)
								TreasureTrailClueLevel.HARD -> Item(TreasureTrailRewardCaskets.HARD_CASKET)
								TreasureTrailClueLevel.ELITE -> Item(TreasureTrailRewardCaskets.ELITE_CASKET)
								else -> null
							}
							if (clueItem != null) {
								killer.world().spawnGroundItem(GroundItem(killer.world(), clueItem, tile, killer.id()))
								notification(killer, clueItem)
								killer.message("<col=0B610B>You have received a clue scroll casket drop!")
							}
						}
					}
				}
				
				// Pets, anyone?! :)
				checkForPet(killer, table)
			}

			// Custom drop tables
			npc.combatInfo()?.scripts?.droptable_?.reward(npc, killer)
		}
		
		// Post-death scripts
		val postDeathScript = npc.combatInfo()?.scripts?.death_
		if (postDeathScript != null) {
			npc.executeScript(postDeathScript)
		}
		
		if (npc.id() == 319) { // Corp beast
			// Reset damage counter
			npc.damagers().forEach { pid ->
				npc.world().playerForId(pid.key).ifPresent { player -> player.varps().varp(Varp.CORP_BEAST_DAMAGE, 0) }
			}
		}
		deathReset(npc)
		if (npc.respawns()) {
			npc.teleport(npc.spawnTile())
			
			npc.hidden(true)
			
			if (npc.combatInfo()?.respawntime ?: 0 > 0) {
				it.delay(npc.combatInfo()?.respawntime!!)
			} else {
				it.delay(50)
			}
			
			GwdLogic.onRespawn(npc)
			respawn(npc)
		} else if (npc.id() != 6500) {
			npc.hidden(true)
			npc.world().unregisterNpc(npc)
		}
	}
	
	
	@JvmStatic fun notification(killer: Player, drop: Item) {
		val loot = drop.unnote(killer.world())

		// Enabled? Untradable buttons are only enabled if the threshold is enabled. Can't have one without the other.
		if (killer.varps().varbit(Varbit.ENABLE_UNTRADABLE_LOOT_NOTIFICATIONS_BUTTONS) != 0) {
			if (!loot.tradable(killer.world())) {
				if (killer.varps().varbit(Varbit.UNTRADABLE_LOOT_NOTIFICATIONS) == 1) {
					killer.message("Untradable drop: ${loot.amount()} x ${loot.name(killer.world())}".col("cc0000"))
				}
			} else if (loot.realPrice(killer.world()) >= killer.varps().varbit(Varbit.LOOT_DROP_THRESHOLD_VALUE)) {
				killer.message("Valuable drop: ${loot.amount()} x ${loot.name(killer.world())} (${loot.realPrice(killer.world()) * loot.amount()} coins)".col("cc0000"))
			}
		}
	}
	
	/**
	 * If you're resetting an Npc as if it were by death but not, for example maybe kraken tentacles which go back down to
	 * the depths when the boss is killed.
	 */
	@JvmStatic fun deathReset(npc: Npc) {
		if (npc.id() != 6500) { // KQ first stage keeps damage onto stage 2!
			npc.clearDamagers() //Clear damagers
			npc.clearDamageTimes()
		}
		npc.clearattrib(AttributeKey.TARGET)
		npc.clearattrib(AttributeKey.LAST_ATTACKED_MAP)
		npc.putattrib(AttributeKey.VENOM_TICKS, 0)
		npc.putattrib(AttributeKey.POISON_TICKS, 0)
	}
	
	@JvmStatic fun respawn(npc: Npc) {
		if (npc.hidden()) { // not respawned yet. we do this check incase it was force-respawned by .. group spawning (gwd)
			deathReset(npc)
			npc.hidden(false)
			npc.hp(npc.maxHp(), 0) // Heal up to full hp
			npc.combatInfo()?.stats = npc.combatInfo()?.originalStats?.clone() // Replenish all stats on this NPC.
			npc.animate(-1) // Reset death animation
			npc.unlock()
		}
	}
	
	fun checkForPet(killer: Player, table: ScalarLootTable): Boolean {
		val pet = table.rollForPet(killer)
		if (pet.isPresent) {
			// Do we already own this pet?
			if (!PetAI.hasUnlocked(killer, pet.get())) {
				// Unlock the varbit. Just do it, rather safe than sorry.
				if (pet.get().varbit > 0) {
					killer.varps().varbit(pet.get().varbit, 1)
				}
				
				// RS tries to add it as follower first. That only works if you don't have one.
				val currentPet = killer.pet()
				if (currentPet == null) {
					killer.message("You have a funny feeling like you're being followed.")
					PetAI.spawnPet(killer, pet.get(), false)
				} else {
					// Sneak it into their inventory. If that fails, fuck you, no pet for you!
					if (killer.inventory().add(Item(pet.get().item), true).success()) {
						killer.message("You feel something weird sneaking into your backpack.")
					} else {
						killer.message("You have a funny feeling like you would have been followed...")
						killer.message("Speak to Probita to claim your pet!")
					}
				}
				
				killer.world().broadcast("<col=844e0d><img=22> ${killer.name()} has unlocked the pet: ${Item(pet.get().item).name(killer.world())}.")
				return true
			}
		}
		return false
	}
}