package nl.bartpelle.veteres.content.minigames.raids

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Mack
 */
object RaidsController {

    private const val XERIC_CHAMBER_DOOR = 29777

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onObject(XERIC_CHAMBER_DOOR, @Suspendable {
            if (it.interactionOption() == 1) {
                enterLobby(it)
            }
        })
    }

    @Suspendable private fun enterLobby(it: Script) {
        val player = it.player()
        val party = partyFor(player)

        if (party == null) {
            it.messagebox("You must be in a raid party to enter the chamber doors.")
            return
        }

        RaidInterfaces.updatePartySessionLayer(player)
    }

    fun partyFor(player: Player): RaidParty? {
        return RaidsResources.partyById(player.attribOr<Int>(AttributeKey.RAID_PARTY, -1))
    }
}