package nl.bartpelle.veteres.content.tournament

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey


object Tournament {

    @Suspendable private fun start(it: Script) {
        val world = it.ctx<World>()
        if(TournamentParticipants.size() <= TournamentConfig.MINIMUM_PARTICIPANTS) {
            world.broadcast(Color.DARK_RED.wrap("<img=79> There wasn't enough players to start the tournament."))
            System.err.println("[TOURNAMENT] There wasn't enough players to start the tournament.")
            world.timers().register(TimerKey.TOURNAMENT_TIMER, 100)
            return
        }

        for(player: Player in TournamentParticipants.participants) {
            player.lock()
            TournamentConfig.TOURNAMENT_MANAGER!!.lock()
            TournamentConfig.TOURNAMENT_MANAGER!!.animate(1818)
            TournamentConfig.TOURNAMENT_MANAGER!!.graphic(343)
            TournamentConfig.TOURNAMENT_MANAGER!!.sync().shout("The tournament is starting - good luck everyone!")
            it.delay(2)
            player.animate(1816)
            player.graphic(342)
            it.delay(2)
            when(TournamentConfig.tournamentType) {
                TournamentType.PVP -> player.teleport(TournamentConfig.PVP_START_AREA.randomTile())
                TournamentType.DMM -> { }
                TournamentType.LMS -> { }
            }
            it.delay(1)
            TournamentConfig.TOURNAMENT_MANAGER!!.unlock()
            it.delay(2)
            player.unlock()
        }

    }

    private fun stop() {

    }

    fun join(player: Player, teleportPlayer: Boolean) {
        if(joinRequirementCheck(player)) {
            TournamentParticipants.addParticipant(player)
            if(teleportPlayer)
                player.teleport(TournamentConfig.WAITING_AREA.randomTile())
            player.message(Color.DARK_RED.wrap("<img=79> You have joined the ${TournamentConfig.tournamentType.name} " +
                    "tournament. Good luck, ${player.name()}."))
        }
    }

    fun leave(player: Player) {
        TournamentParticipants.removeParticipant(player)
    }

    fun joinRequirementCheck(player: Player): Boolean {
        if (TournamentConfig.tournamentDisabled) {
            player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently disabled. Please try again later."))
            return false
        }
        if(!TournamentConfig.inscriptionsOpen) {
            player.message(Color.DARK_RED.wrap("<img=79> Inscriptions are currently closed. Please try again later."))
            return false
        }
        if(TournamentParticipants.containsParticipant(player)) {
            player.message(Color.DARK_RED.wrap("<img=79> You're already participating in the tournament."))
            return false
        }
        return true
    }

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onWorldInit {
            // Easy little toggle for to disable the tournament system
            if(!TournamentConfig.tournamentDisabled)
                it.ctx<World>().timers().register(TimerKey.TOURNAMENT_TIMER, 100)
        }
        r.onWorldTimer(TimerKey.TOURNAMENT_TIMER) {
            System.err.print("Tournament timer executed.")
            TournamentInscription.open(it.ctx<World>())
        }

        r.onWorldTimer(TimerKey.TOURNAMENT_INSCRIPTION_TIMER) {
            System.err.print("Tournament inscription timer executed.")
            TournamentInscription.close(it.ctx<World>())
            start(it)
        }

        r.onNpcSpawn(7316) { TournamentConfig.TOURNAMENT_MANAGER = it.npc()}
    }

}