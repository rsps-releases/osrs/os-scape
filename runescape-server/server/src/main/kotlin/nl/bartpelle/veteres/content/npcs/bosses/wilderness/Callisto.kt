package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.util.CombatStyle


/**
 * Created by Bart on 8/28/2015.
 */
object Callisto {
	
	/**
	 * Main callisto combat script. Callisto can be found at 3291, 3847.
	 */
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackMelee(npc, target, true) && EntityCombat.attackTimerReady(npc)) {
				// At all times, callisto can initiate the heal.
				if (npc.world().rollDie(18, 1)) {
					prepareHeal(npc, target)
				}
				// Determine if we do a special hit, or a regular hit.
				if (npc.world().rollDie(18, 1)) {
					fury(npc, target)
				} else if (npc.world().rollDie(6, 1) && !npc.attribOr<Boolean>(AttributeKey.CALLISTO_ROAR, false)) {
					roar(it)
				} else {
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
						target.hit(npc, EntityCombat.randomHit(npc))
					} else {
						target.hit(npc, 0) // Uh-oh, that's a miss.
					}
					
					npc.animate(npc.attackAnimation())
				}
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@JvmField val healscript: Function1<Script, Unit> = {
		if (it.npc().attribOr(AttributeKey.CALLISTO_DMG_HEAL, false)) {
			val dmg: Hit = it.npc()[AttributeKey.LAST_HIT_DMG]
			it.npc().heal(dmg.damage())
			it.npc().clearattrib(AttributeKey.CALLISTO_DMG_HEAL)
		}
	}
	
	fun fury(npc: Npc, target: Entity) {
		target.hit(npc, npc.world().random(60)).combatStyle(CombatStyle.GENERIC)
		target.message("Callisto's fury sends an almighty shockwave through you.")
		target.stun(4, false)
		target.graphic(245, 124, 0)
		npc.animate(4925)
	}
	
	@Suspendable fun roar(it: Script) {
		val npc = it.npc()
		
		val target = EntityCombat.getTarget(npc)
		
		npc.putattrib(AttributeKey.CALLISTO_ROAR, true)
		if (target is Player) {
			val direction: Direction = Direction.of(target.tile().x - npc.tile().x,
					target.tile().z - npc.tile().z)
			
			val tile: Tile = target.tile().transform(direction.x * 3, direction.y * 3)
			
			val face: FaceDirection = FaceDirection.forTargetTile(npc.tile(), target.tile())
			
			val movement: ForceMovement = ForceMovement(0, 0, direction.x * 3, direction.y * 3, 40, 90, face)
			
			val area: Array<IntArray> = target.world().clipAround(tile, 3)
			
			for (array: IntArray in area) {
				for (value in array) {
					if (value != 0) {
						npc.clearattrib(AttributeKey.CALLISTO_ROAR)
						return
					}
				}
			}
			target.message("Callisto's roar throws you backwards.")
			target.animate(846)
			target.forceMove(movement)
			it.delay(3)
			target.teleport(tile)
			target.hit(npc, 3).combatStyle(CombatStyle.MELEE)
		}
		npc.clearattrib(AttributeKey.CALLISTO_ROAR)
	}
	
	fun prepareHeal(npc: Npc, target: Entity) {
		npc.graphic(157)
		npc.putattrib(AttributeKey.CALLISTO_DMG_HEAL, true)
	}
	
}