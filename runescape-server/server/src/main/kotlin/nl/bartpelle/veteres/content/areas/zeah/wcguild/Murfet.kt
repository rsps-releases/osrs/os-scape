package nl.bartpelle.veteres.content.areas.zeah.wcguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/10/2016.
 */
object Murfet {
	
	const val MURFET = 7237
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MURFET) @Suspendable {
			it.chatPlayer("Hello.", 567)
			it.chatNpc("Hello, how can I help you?", MURFET, 554)
			when (it.options("What's in the cave?", "I was just passing through.")) {
				1 -> {
					it.chatPlayer("What's in the cave?", 554)
					it.chatNpc("Creatures known as ents inhabit the depths of the hills,<br>rumour has it they reside there due to the presence of<br>the ancient redwood trees on the hills.", MURFET, 590)
					it.chatNpc("If you're as skilled a fighter as you are a woodcutter,<br>you may enter the caves. You should find Kai once<br>you are inside, he can give you more information.", MURFET, 590)
				}
				2 -> {
					it.chatPlayer("I was just passing through.", 588)
				}
			}
		}
	}
	
}