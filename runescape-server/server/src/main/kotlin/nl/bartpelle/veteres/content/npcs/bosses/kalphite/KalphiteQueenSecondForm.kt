package nl.bartpelle.veteres.content.npcs.bosses.kalphite

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Jason MacKeigan on 2016-06-29.
 *
 * The purpose of this singleton is to represent the second form of the Kalphite Queen
 * npc.
 */
object KalphiteQueenSecondForm {
	
	@JvmField val script: Function1<Script, Unit> = suspend@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@suspend
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 8) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.canAttackMelee(npc, target) && npc.world().rollDie(4, 1)) {
					attack(npc, target, CombatStyle.MELEE, it)
				} else {
					attack(npc, target, KalphiteQueen.randomizeStyle(), it)
				}
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@suspend
		}
	}
	
	@JvmField val death: Function1<Script, Unit> = suspend@ @Suspendable {
		val npc = it.npc()
		
		val spawn = Npc(6500, npc.world(), npc.tile())
		
		it.clearContext()
		it.delay(60)
		spawn.respawns(false)
		npc.world().registerNpc(spawn)
		npc.animate(6240)
		it.delay(5)
	}
	
	fun attack(npc: Npc, target: Entity, style: CombatStyle, script: Script) {
		val maxHit = npc.combatInfo().maxhit
		
		val animations = KalphiteQueen.animations.get(npc.id()) ?: return
		
		val attackAnimation = KalphiteQueen.animations.get(npc.id())?.get(style) ?: return
		
		npc.animate(attackAnimation)
		
		when (style) {
			CombatStyle.MELEE -> {
				target.hit(npc, if (EntityCombat.attemptHit(npc, target, style)) npc.world().random(maxHit) else 0)
			}
			CombatStyle.RANGE -> {
				npc.world().players().forEachInAreaKt(KalphiteQueen.area, { player ->
					val distance = npc.tile().distance(player.tile())
					
					val delay = Math.max(1, (15 + (distance * 15)) / 30)
					
					npc.world().spawnProjectile(npc, player, 473, 45, 30, 35, distance * 11, 15, 10)
					player.hit(npc, npc.world().random(maxHit), delay).combatStyle(style)
				})
			}
			CombatStyle.MAGIC -> {
				npc.graphic(279)
				npc.world().players().forEachInAreaKt(KalphiteQueen.area, { player ->
					val distance = npc.tile().distance(player.tile())
					
					val delay = Math.max(1, (15 + distance * 15) / 30)
					
					npc.world().spawnProjectile(npc, player, 280, 45, 30, 60, distance * 11, 15, 10)
					player.hit(npc, npc.world().random(maxHit), 2 + delay).combatStyle(style).graphic(281)
				})
			}
		}
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
}