package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Jak on 06/06/2016.
 */
object DragonSquare {
	
	val LEFT: Int = 2366
	val RIGHT: Int = 2368
	val FULL: Int = 1187
	
	@Suspendable @JvmStatic fun on_anvil(it: Script) {
		val player = it.player()
		if (player.inventory().hasAll(LEFT, RIGHT)) {
			player.message("You start to hammer the metal...")
			
			it.player().animate(898)
			it.delay(6)
			
			it.player().inventory()
			if (player.inventory().hasAll(LEFT, RIGHT)) {
				player.inventory().remove(Item(LEFT), true)
				player.inventory().remove(Item(RIGHT), true)
				player.inventory().add(Item(FULL), true)
			}
			it.itemBox("You forge the shield halves together to complete it.", FULL)
		} else {
			it.doubleItemBox("You need both the left and right shield halves to forge a square shield.", LEFT, RIGHT)
		}
	}
}