package nl.bartpelle.veteres.content.skills.farming

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/3/2015.
 */

object Flax {
	
	val FLAX = 14896
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(FLAX) @Suspendable {
			if (it.player().inventory().full()) {
				it.message("You can't carry any more flax.")
			} else {
				it.player().lock()
				it.animate(827)
				it.delay(1)
				it.player().inventory() += 1779
				it.message("You pick some flax.")
				
				// Prepare despawn & respawn
				if (it.player().world().random(6) == 1) {
					val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
					
					it.runGlobal(it.player().world()) @Suspendable { s ->
						s.ctx<World>().removeObj(obj)
						s.delay(10)
						s.ctx<World>().spawnObj(obj)
					}
				}
				
				it.player().unlock()
			}
		}
	}
}