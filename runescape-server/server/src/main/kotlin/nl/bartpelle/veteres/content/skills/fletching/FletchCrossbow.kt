package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/26/2015.
 */

object FletchCrossbow {
	
	enum class Crossbow(val limbs: Int, val stock: Int, val unfcbow: Int, val lvl: Int, val exp: Double, val anim: Int) {
		BRONZE_CROSSBOW(9420, 9440, 9454, 9, 12.0, 4436),
		BLURITE_CROSSBOW(9422, 9442, 9456, 24, 32.0, 4437),
		IRON_CROSSBOW(9423, 9444, 9457, 39, 44.0, 4438),
		STEEL_CROSSBOW(9425, 9446, 9459, 46, 54.0, 4439),
		MITHRIL_CROSSBOW(9427, 9448, 9461, 54, 64.0, 4440),
		ADAMANT_CROSSBOW(9429, 9450, 9463, 61, 82.0, 4441),
		RUNITE_CROSSBOW(9431, 9452, 9465, 69, 100.0, 4442);
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		Crossbow.values().forEach { crossbow ->
			val HAMMER = 2347
			repo.onItemOnItem(crossbow.limbs.toLong(), crossbow.stock.toLong(), s@ @Suspendable {
				//Check if the player has a high enough level to do this.
				if (it.player().skills()[Skills.FLETCHING] < crossbow.lvl) {
					it.messagebox("You need at least level ${crossbow.lvl} Fletching to do that.")
					return@s
				}
				//Check to see if the player has a hammer.
				if (HAMMER !in it.player().inventory()) {
					it.messagebox("You'll need a hammer to do that.")
					return@s
				}
				//Prompt the player with the # they'd like to make
				var num = 1
				if (it.player().inventory().count(crossbow.stock) > 1) {
					num = it.itemOptions(Item(crossbow.unfcbow), offsetX = 12)
				}
				//Check if the player's ran out of supplies
				if (crossbow.stock !in it.player().inventory() || crossbow.limbs !in it.player().inventory()) {
					return@s
				}
				//Remove the supplies, give the player the item, animate, send experience, message, and apply a delay.
				it.player().inventory() -= Item(crossbow.stock, 1)
				it.player().inventory() -= Item(crossbow.limbs, 1)
				it.player().inventory() += Item(crossbow.unfcbow, 1)
				it.message("You attach the stock to the limbs and create an unstrung crossbow.")
				it.player().animate(crossbow.anim)
				it.addXp(Skills.FLETCHING, crossbow.exp)
				it.delay(2)
				
			})
		}
	}
}