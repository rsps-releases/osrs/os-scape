package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jonathan on 6/11/2017.
 */
object WorldMap {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		with(repo) {
			//World map button
			onButton(160, 36, s@ @Suspendable {
				val player = it.player()
				if (player.attribOr(AttributeKey.WORLD_MAP, false)) {
					player.interfaces().closeWorldMap()
				} else {
					
					//Small edit. Need to send script/ISetting prior to the interface overlay so it properly displays players current location
					//as it previously displayed the last tile the player was on (since we were sending the tile hash AFTER the interface display). Msg me @Mack if you need more info.
					player.invokeScript(1749, player.tile().hash30())
					player.write(InterfaceSettings(595, 17, 0, 4, 2))
					player.putattrib(AttributeKey.WORLD_MAP, true)
					
					when (it.interactionOption()) {
						1 -> player.interfaces().sendWorldMap()
						2 -> {
							player.interfaces().sendForMode(player.interfaces().displayMode(false), 3)
							player.interfaces().sendWorldMap()
							player.interfaces().send(594, 165, 28, false)
						}
					}
				}
				
			})
			
			onButton(595, 34, s@ @Suspendable {
				it.player().interfaces().closeWorldMap()
			})
			
			onInterfaceClose(595, s@ @Suspendable {
				it.player().putattrib(AttributeKey.WORLD_MAP, false)
			})
		}
	}
	
}
