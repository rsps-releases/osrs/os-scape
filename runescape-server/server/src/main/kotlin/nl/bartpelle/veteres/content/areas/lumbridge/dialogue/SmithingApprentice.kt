package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/4/2015.
 */

object SmithingApprentice {
	
	val SMITHING_APPRENTICE = 3224
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(SMITHING_APPRENTICE) @Suspendable {
			it.chatPlayer("What do I do after smelting the ore?", 554)
			it.chatNpc("Ahh you'll need to learn to smith. Go to Varrock..", SMITHING_APPRENTICE, 605)
			it.doubleItemBox("In the west of the city you will find these two icons,<br>" +
					"that's where my master, the Smithing Tutor, plies his<br>" +
					"trade. Ask him to teach you how to smith.", Item(5079, 400), Item(5086, 400))
			it.doubleItemBox("Smelt some ore and store it in the bank. Grab a<br>" +
					"hammer from the general store before you go too!", Item(5079, 400), Item(5086, 400))
		}
	}
	
}
