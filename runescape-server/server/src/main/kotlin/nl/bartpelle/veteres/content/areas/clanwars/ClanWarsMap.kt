package nl.bartpelle.veteres.content.areas.clanwars

import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.Tile

/**
 * Created by Bart on 6/1/2016.
 */
enum class ClanWarsMap(val cornerTile: Tile, val maps: IntArray, val startTile1: Tile, val startTile2: Tile, val jail1: Array<Area>, val jail2: Array<Area>, val barrierStart: Tile, val barrierEnd: Tile, val barrierObj: Int) {
	
	WASTELAND(cornerTile = Tile(3264, 4992), maps = intArrayOf(13134, 13135),
			startTile1 = Tile(33, 115),
			startTile2 = Tile(33, 12),
			jail1 = arrayOf(Area(52, 64, 58, 71)),
			jail2 = arrayOf(Area(52, 56, 58, 63)),
			barrierStart = Tile(5, 63),
			barrierEnd = Tile(51, 64),
			barrierObj = 26784),
	
	PLATEAU(cornerTile = Tile(3264, 4928), maps = intArrayOf(13133),
			startTile1 = Tile(28, 59),
			startTile2 = Tile(28, 4),
			jail1 = arrayOf(Area(58, 34, 60, 50)),
			jail2 = arrayOf(Area(58, 13, 60, 29)),
			barrierStart = Tile(4, 32),
			barrierEnd = Tile(53, 31),
			barrierObj = 26784),
	
	SYLVAN_GLADE(cornerTile = Tile(3392, 4992), maps = intArrayOf(13646),
			startTile1 = Tile(31, 58),
			startTile2 = Tile(31, 6),
			jail1 = arrayOf(Area(28, 32, 35, 39)),
			jail2 = arrayOf(Area(28, 24, 35, 31)),
			barrierStart = Tile(4, 31),
			barrierEnd = Tile(57, 31),
			barrierObj = 26788), //barriers spawn on jail
	
	FORSAKEN_QUARRY(cornerTile = Tile(3392, 5056), maps = intArrayOf(13647),
			startTile1 = Tile(31, 55),
			startTile2 = Tile(31, 8),
			jail1 = arrayOf(Area(24, 32, 39, 35)),
			jail2 = arrayOf(Area(24, 28, 39, 31)),
			barrierStart = Tile(8, 31),
			barrierEnd = Tile(55, 31),
			barrierObj = 26786),
	
	TURRETS(cornerTile = Tile(3136, 4992), maps = intArrayOf(12622, 12623),
			startTile1 = Tile(31, 104),
			startTile2 = Tile(31, 23),
			jail1 = arrayOf(Area(0, 64, 7, 118), Area(0, 112, 63, 118), Area(56, 64, 63, 118)),
			jail2 = arrayOf(Area(0, 8, 7, 63), Area(0, 8, 63, 15), Area(56, 8, 63, 63)),
			barrierStart = Tile(8, 63),
			barrierEnd = Tile(55, 64),
			barrierObj = 26790), //barriers spawn on tower
	
	CLAN_CUP(cornerTile = Tile(3136, 4928), maps = intArrayOf(12621),
			startTile1 = Tile(8, 8),
			startTile2 = Tile(55, 8),
			jail1 = arrayOf(Area(3, 3, 32, 6), Area(3, 3, 6, 60), Area(3, 57, 31, 60)),
			jail2 = arrayOf(Area(33, 3, 60, 6), Area(57, 3, 60, 60), Area(32, 57, 60, 60)),
			barrierStart = Tile(8, 33),
			barrierEnd = Tile(55, 33),
			barrierObj = 26784), //barriers spawn on view orb
	
	SOGGY_SWAMP(cornerTile = Tile(3392, 4928), maps = intArrayOf(13645),
			startTile1 = Tile(46, 58),
			startTile2 = Tile(17, 5),
			jail1 = arrayOf(Area(56, 56, 63, 63)),
			jail2 = arrayOf(Area(0, 0, 7, 7)),
			barrierStart = Tile(11, 31),
			barrierEnd = Tile(60, 31),
			barrierObj = 26792),
	
	GHASTLY_SWAMP(cornerTile = Tile(3392, 4928), maps = intArrayOf(13645),
			startTile1 = Tile(46, 58),
			startTile2 = Tile(17, 5),
			jail1 = arrayOf(Area(56, 56, 63, 63)),
			jail2 = arrayOf(Area(0, 0, 7, 7)),
			barrierStart = Tile(11, 31),
			barrierEnd = Tile(60, 31),
			barrierObj = 26792),
	
	NORTHLEACH_QUELL(cornerTile = Tile(3392, 4864), maps = intArrayOf(13644),
			startTile1 = Tile(58, 56),
			startTile2 = Tile(7, 7),
			jail1 = arrayOf(Area(33, 33, 36, 37)),
			jail2 = arrayOf(Area(27, 33, 30, 37)),
			barrierStart = Tile(33, 14),
			barrierEnd = Tile(33, 52),
			barrierObj = 26786),
	
	GRIDLOCK(cornerTile = Tile(3456, 4800), maps = intArrayOf(13899, 13890, 14155, 14156),
			startTile1 = Tile(122, 98),
			startTile2 = Tile(5, 29),
			jail1 = arrayOf(Area(60, 64, 66, 67)),
			jail2 = arrayOf(Area(60, 59, 66, 63)),
			barrierStart = Tile(0, 0),
			barrierEnd = Tile(0, 0),
			barrierObj = 0),
	
	ETHEREAL(cornerTile = Tile(3456, 4800), maps = intArrayOf(13390),
			startTile1 = Tile(28, 59, 3),
			startTile2 = Tile(28, 4, 3),
			jail1 = arrayOf(Area(57, 35, 62, 41)),
			jail2 = arrayOf(Area(57, 27, 62, 33)),
			barrierStart = Tile(3, 23),
			barrierEnd = Tile(54, 24),
			barrierObj = 26784), //TODO
	
	CLASSIC(cornerTile = Tile(3392, 4672), maps = intArrayOf(13641, 13642),
			startTile1 = Tile(32, 116),
			startTile2 = Tile(31, 11),
			jail1 = arrayOf(Area(54, 64, 59, 80)),
			jail2 = arrayOf(Area(54, 47, 59, 63)),
			barrierStart = Tile(3, 63),
			barrierEnd = Tile(53, 63),
			barrierObj = 26784);
	
	companion object {
		
		operator fun get(id: Int) = values()[id]
	}
	
}