package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.graphic
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by bart on 7/19/15.
 */

object EmoteBook {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) { //TODO locking
		repo.onButton(216, 1, s@ @Suspendable {
			// Emote don't happen if you've just been hit.
			if (System.currentTimeMillis() - it.player().attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0L) < 1900) {
				return@s
			}
			// Interrupt what we're doing
			it.player().stopActions(true)
			val slot: Int = it.player().attrib(AttributeKey.BUTTON_SLOT)
			it.onInterrupt { it.animate(-1); it.graphic(-1) }
			
			when (slot) {
				0 -> {
					it.animate(855)
				} // Yes
				1 -> {
					it.animate(856)
				} // No
				2 -> {
					it.animate(858)
				} // Bow
				3 -> {
					it.animate(859)
				} // Angry
				4 -> {
					it.animate(857)
				} // Think
				5 -> {
					it.animate(863)
				} // Wave
				6 -> {
					it.animate(2113)
				} // Shrug
				7 -> {
					it.animate(862)
				} // Cheer
				8 -> {
					it.animate(864)
				} // Beckon
				9 -> {
					it.animate(861)
				} // Laugh
				10 -> {
					it.animate(2109)
				} // Jump for joy
				11 -> {
					it.animate(2111)
				} // Yawn
				12 -> {
					it.animate(866)
				} // Dance
				13 -> {
					it.animate(2106)
				} // Jig
				14 -> {
					it.animate(2107)
				} // Spin
				15 -> {
					it.animate(2108)
				} // Headbang
				16 -> {
					it.animate(860)
				} // Cry
				17 -> {
					it.animate(1374); it.player().graphic(574)
				} // Blow kiss
				18 -> {
					it.animate(2105)
				} // Panic
				19 -> {
					it.animate(2110)
				} // Raspberry
				20 -> {
					it.animate(865)
				} // Clap
				21 -> {
					it.animate(2112)
				} // Salute
				22 -> {
					it.animate(2127)
				} // Goblin bow
				23 -> {
					it.animate(2128)
				} // Goblin salute
				24 -> {
					it.animate(1131)
				} // Glass box
				25 -> {
					it.animate(1130)
				} // Climb rope
				26 -> {
					it.animate(1129)
				} // Lean
				27 -> {
					it.animate(1128)
				} // Glass wall
				28 -> {
					it.animate(4276)
				} // Idea
				29 -> {
					it.animate(1745)
				} // Stomp
				30 -> {
					it.animate(4280)
				} // Flap
				31 -> {
					it.animate(4275)
				} // Slap head
				32 -> {
					it.animate(3544)
				} // Zombie walk
				33 -> {
					it.animate(3543)
				} // Zombie dance
				34 -> {
					it.animate(2836)
				} // Scared
				35 -> {
					it.animate(6111)
				} // Rabbit hop
				36 -> {
					it.animate(2763)
				} // Sit up
				37 -> {
					it.animate(2762)
				} // Push up
				38 -> {
					it.animate(2761)
				} // Star jump
				39 -> {
					it.animate(2764)
				} // Jog
				40 -> {
					it.animate(1708)
				} // Zombie hand
				41 -> {
					it.animate(7131)
				} // Hypermobile drinker
				
				42 -> { //Skillcapes
					val cape: Item? = it.player().equipment().get(EquipSlot.CAPE)
					if (cape == null) {
						it.player().message("You need to be wearing a Skill Cape to perform this emote.")
						return@s
					}
					val capeid = cape.id()
					when (capeid) {
						9747, 9748 -> {
							it.animate(4959); it.player().graphic(823);
						} //Attack cape
						9753, 9754 -> {
							it.animate(4961); it.player().graphic(824);
						} //Defence cape
						9750, 9751 -> {
							it.animate(4981); it.player().graphic(828);
						} //Strength cape
						9768, 9769 -> {
							it.animate(4971); it.player().graphic(833);
						} //Hitpoints cape
						9756, 9757 -> {
							it.animate(4973); it.player().graphic(832);
						} //Ranged cape
						9762, 9763 -> {
							it.animate(4939); it.player().graphic(813);
						} //Magic cape
						9759, 9760 -> {
							it.animate(4979); it.player().graphic(829);
						} //Prayer cape
						9801, 9802 -> {
							it.animate(4955); it.player().graphic(821);
						} //Cooking cape
						9807, 9808 -> {
							it.animate(4957); it.player().graphic(822);
						} //Woodcutting cape
						9783, 9784 -> {
							it.animate(4937); it.player().graphic(812);
						} //Fletching cape
						9798, 9799 -> {
							it.animate(4951); it.player().graphic(819);
						} //Fishing cape
						9804, 9805 -> {
							it.animate(4975); it.player().graphic(8831);
						} //Firemaking cape
						9780, 9781 -> {
							it.animate(4949); it.player().graphic(818);
						} //Crafting cape
						9795, 9796 -> {
							it.animate(4943); it.player().graphic(815);
						} //Smithing cape
						9792, 9793 -> {
							it.animate(4941); it.player().graphic(814);
						} //Mining cape
						9774, 9775 -> {
							it.animate(4969); it.player().graphic(835);
						} //Herblore cape
						9771, 9772 -> {
							it.animate(4977); it.player().graphic(830);
						} //Agility cape
						9777, 9778 -> {
							it.animate(4965); it.player().graphic(826);
						} //Thieving cape
						9786, 9787 -> {
							it.animate(4967); it.player().graphic(1656);
						} //Slayer cape
						9810, 9811 -> {
							it.animate(4963); it.player().graphic(826);
						} //Farming cape
						9765, 9766 -> {
							it.animate(4947); it.player().graphic(817);
						} //Runecrafting cape
						9789, 9790 -> {
							it.animate(4953); it.player().graphic(820);
						} //Construction cape
						9948, 9949 -> {
							it.animate(5158); it.player().graphic(907);
						} //Hunter cape
						9813 -> {
							it.animate(4945); it.player().graphic(816);
						} //Quest cape
						13069 -> {
							it.animate(7121); it.player().graphic(1286)
						}//achievement
						13280, 13329, 13331, 13333, 13335, 13337, 13342, 20760 -> {
							it.animate(7121); it.player().graphic(1286);
						} //Max cape
					}
				}
				43 -> {
					it.animate(-1)
				} // Air guitar
				47 -> {
					val player = it.player()
                    val graphicId = when(player.donationTier()) {
                        DonationTier.MASTER_DONATOR -> 1412
                        DonationTier.GRAND_MASTER_DONATOR -> 1413
                        DonationTier.ULTIMATE_DONATOR -> 1414
                        else -> {
                            it.messagebox("You must be a master tier donator or above to use this emote.")
                            return@s
                        }
                    }

                    it.graphic(graphicId)
                    it.animate(1)
				} //Premier club
			}
			// by adding a delay, we can use the onInterrupt hook to reset anim to -1 when you walk!
			it.delay(10)
			// (nothing happens after the delay ends)
		})
	}
	
}

