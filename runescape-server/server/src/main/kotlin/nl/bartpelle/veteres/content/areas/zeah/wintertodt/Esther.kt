package nl.bartpelle.veteres.content.areas.zeah.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/11/2016.
 */

object Esther {
	
	val ESTHER = 7376
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ESTHER) @Suspendable {
			val TALKED_TO_IGNISIA = it.player().attribOr<Int>(AttributeKey.TALKED_TO_IGNISIA, 0)
			
			if (TALKED_TO_IGNISIA == 1) {
				it.chatNpc("Are you here to fight the cold?", ESTHER, 554)
				it.chatPlayer("Umm... I guess?", 575)
				it.chatNpc("Great! Ask me anything you like about the Bruma<br>Tree, I'm an expert.", ESTHER, 568)
				when (it.options("Tell me about the Wintertodt.", "Tell me about the Bruma tree.", "Tell me about yourself.", "See you later.")) {
					1 -> tell_me_about_wintertodt(it)
					2 -> tell_me_about_the_bruama_tree(it)
					3 -> tell_me_about_yourself(it)
					4 -> see_you_later(it)
				}
			} else {
				it.chatPlayer("Hi.", 567)
				it.chatNpc("Hello.", ESTHER, 588)
				it.chatPlayer("What's happening here?", 554)
				it.chatNpc("You'd best talk to Ignisia, she's in charge around here.", ESTHER, 588)
			}
		}
	}
	
	@Suspendable fun tell_me_about_wintertodt(it: Script) {
		it.chatPlayer("Tell me about the Wintertodt.", 554)
		it.chatNpc("Some would say that ice preserves while fire destroys,<br>but no amount of fire is destroying the Wintertodt," +
				"<br>that's a cold that will freeze the air in your lungs.", ESTHER, 594)
		it.chatNpc("Even the heat generated from burning the Bruma<br>roots only weakens it, and that doesn't last very long<br>nowadays.", ESTHER, 612)
		it.chatPlayer("Looks like an angry snow storm to me...", 588)
		it.chatNpc("What you see in that pit is not the Wintertodt at full<br>strength, it never will be while the Bruma tree still" +
				"<br>burns and the Doors of Dinh remain closed.", ESTHER, 590)
		when (it.options("Tell me about the Bruma tree.", "Tell me about yourself.", "See you later.")) {
			1 -> tell_me_about_the_bruama_tree(it)
			2 -> tell_me_about_yourself(it)
			3 -> see_you_later(it)
		}
	}
	
	@Suspendable fun tell_me_about_the_bruama_tree(it: Script) {
		it.chatPlayer("Tell me about the Bruma tree.", 554)
		it.chatNpc("The Bruma tree is what keeps the Wintertodt weak, the<br>prison was built here by Dinh because of its usefulness" +
				"<br>against the Wintertodt. Some legends say that Xeric is<br>the cause for the doors weakening, or maybe he made", ESTHER, 570)
		it.chatNpc("the Wintertodt stronger? ", ESTHER, 567)
		it.chatPlayer("Is Xeric really such a bad guy?", 554)
		it.chatNpc("It's a known fact that he was interested in the Bruma<br>Tree, for what purpose... there are only rumours.", ESTHER, 593)
		it.chatNpc("Regardless, the Bruma tree definitely contains powerful<br>magic, I've been studying the sap for years and it has" +
				"<br>truly astounding properties.", ESTHER, 590)
		it.chatNpc("There's not many fires that burn green now, are<br>there?", ESTHER, 555)
		it.chatPlayer("What about gnomish fire lighters?", 554)
		it.chatNpc("...", ESTHER, 575)
		when (it.options("Tell me about the Wintertodt.", "Tell me about yourself.", "See you later.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_yourself(it)
			3 -> see_you_later(it)
		}
	}
	
	@Suspendable fun tell_me_about_yourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 554)
		it.chatNpc("Me? I'm a member of the Woodcutting guild, more of<br>" +
				"a researcher though... I'm currently tasked with<br>growing a new Bruma tree.", ESTHER, 569)
		it.chatPlayer("How's that going?", 554)
		it.chatNpc("Poorly. I believe the Bruma tree can only be grown in<br>a freezing cold climate.", ESTHER, 611)
		it.chatPlayer("Well, isn't that here?", 554)
		it.chatNpc("It's just an idea really, for all I know, it could be magic<br>way beyond me, beyond even the Pyromancers.", ESTHER, 576)
		when (it.options("Tell me about the Wintertodt.", "Tell me about the Bruma tree.", "See you later.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_the_bruama_tree(it)
			3 -> see_you_later(it)
		}
	}
	
	@Suspendable fun see_you_later(it: Script) {
		it.chatPlayer("See you later.", 567)
		it.chatNpc("Goodbye!", ESTHER, 567)
	}
	
	
}
