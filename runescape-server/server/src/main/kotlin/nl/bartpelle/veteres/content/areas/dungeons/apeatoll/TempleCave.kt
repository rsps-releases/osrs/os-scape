package nl.bartpelle.veteres.content.areas.dungeons.apeatoll

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/11/2016.
 */
object TempleCave {
	
	const val ZENYTE_SHARD = 19529
	const val CUT_ONYX = 6573
	const val ZENYTE = 19496
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Trapdoor opening
		r.onObject(4879) @Suspendable {
			it.player().lock()
			it.player().animate(827)
			it.delay(1)
			it.interactionObject().replaceWith(4880, it.player().world())
			it.player().unlock()
		}
		
		// Trapdoor closing/entering
		r.onObject(4880) @Suspendable {
			it.player().lock()
			if (it.interactionOption() == 2) {
				it.delay(1)
				it.interactionObject().replaceWith(4879, it.player().world())
			} else {
				it.player().message("You climb down the trapdoor.")
				it.delay(1)
				it.player().teleport(2807, 9201)
			}
			it.player().unlock()
		}
		
		// Trapdoor rope back up
		r.onObject(4881) @Suspendable {
			it.player().lock()
			it.player.animate(828)
			it.delay(1)
			it.player().teleport(2806, 2785)
			it.player().unlock()
		}
		
		// Zenyte forging
		for (i in 4765..4766) {
			r.onItemOnObject(i) @Suspendable {
				val item = it.itemUsedId()
				
				if (item != ZENYTE_SHARD && item != CUT_ONYX) {
					it.player().message("Nothing interesting happens.")
				} else {
					if (Item(ZENYTE_SHARD) in it.player().inventory() && Item(CUT_ONYX) in it.player().inventory()) {
						if (it.optionsTitled("Fuse your onyx and zenyte shard?", "Yes - fuse them.", "Don't fuse any of my gems!") == 1) {
							it.player().inventory() -= Item(ZENYTE_SHARD)
							it.player().inventory() -= Item(CUT_ONYX)
							it.player().inventory() += Item(ZENYTE)
							it.player().message("You reach into the extremely hot flames and fuse the zenyte and onyx together, forming an uncut zenyte.")
						}
					} else {
						it.doubleItemBox("You need a <col=000080>cut onyx<col=000000> to fuse with a <col=000080>" +
								"zenyte shard<col=000000> to<br><col=000000>create an uncut zenyte.", ZENYTE_SHARD, CUT_ONYX)
					}
				}
			}
		}
	}
	
}