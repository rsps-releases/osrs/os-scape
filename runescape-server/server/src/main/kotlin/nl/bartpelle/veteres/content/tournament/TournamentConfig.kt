package nl.bartpelle.veteres.content.tournament

import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player

object TournamentConfig {

    // Tournament areas
    val WAITING_AREA = Area(3077, 3489, 3081, 3496, 0)
    val LEAVE_AREA = Area(3084, 3490, 3487, 3495, 0)

    // PVP tournament areas
    @JvmField var PVP_START_AREA: Area = Area(0)
    @JvmField var PVP_TOURNAMENT_AREA: Area = Area(0)
    @JvmField var ARENAS: ArrayList<Area>? = null
    @JvmField var FINAL_ARENA: Area? = null

    // Tournament configs
    var tournamentDisabled = true
    var inscriptionsOpen = false
    var tournamentInProgress = false
    var tournamentType = TournamentType.PVP
    var TOURNAMENT_MANAGER: Npc? = null
    const val MINIMUM_PARTICIPANTS = 2

    //Tournament rewards


    // Scoreboard
    @JvmField var tournamentWinner: Player? = null

    // Object constants
    const val CRUSHED_SPEARS = 6882
    const val TOURNAMENT_REWARDS = 29087
    const val TOURNAMENT_BOARD = 31846
    const val TOURNAMENT_ORB = 26747


}