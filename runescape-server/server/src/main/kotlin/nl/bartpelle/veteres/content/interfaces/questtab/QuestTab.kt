package nl.bartpelle.veteres.content.interfaces.questtab

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.lumbridge.dialogue.Hans
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.mechanics.EloRating
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.Preset
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.journal.Journal
import nl.bartpelle.veteres.util.journal.toggles.RiskProtectionState

object QuestTab {

	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(259, 4) @Suspendable {
			val slot = it.buttonSlot()
			it.player().journal().select(it.player(), slot)
		}

		r.onButton(259, 8) @Suspendable {
			Journal.MAIN.send(it.player())
		}

		r.onButton(259, 9) @Suspendable {
			Journal.PRESETS.send(it.player())
		}
	}

	class PlayTime(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)
			val time: Int = (it.player().gameTime().toDouble() * 0.6).toInt()
			val days: Int = (time / 86400)
			val hours: Int = (time / 3600) - (days * 24)
			val minutes: Int = (time / 60) - (days * 1440) - (hours * 60)
			val dayText = if (days == 0) "" else " " + days + " day${if (days > 1) "s" else ""},"
			it.chatNpc("You've spent$dayText " + hours + " hour${if (hours > 1) "s" else ""} and " + minutes +
					" minute${if (minutes > 1) "s" else ""} playing OS-Scape.", Hans.HANS, 590)
		}
	}

	class VotePoints(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)
			it.itemBox("You currently have <col=f9fe11><shad>${player.attribOr<Int>(AttributeKey.VOTING_POINTS, 0)}</col></shad> " +
					"voting point${if (player.attribOr<Int>(AttributeKey.VOTING_POINTS, 0) == 1) "" else "s"}. For more points, type ::vote!", 10943)
		}
	}

	class TotalSpent(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)
			if (player.donationTier().ordinal + 1 == 8) {
				it.messagebox("You have currently spent <col=009933>$${player.totalSpent().toInt()}</col>. This is the highest donation tier there is!")
			} else {
				it.messagebox("You have currently spent <col=009933>$${player.totalSpent().toInt()}</col>," +
						" to upgrade to the next donator tier you need to spend an additional " +
						"<col=ff3333>$${Math.ceil(DonationTier.values()[player.donationTier().ordinal + 1].required() - player.totalSpent).toInt()}</col>. " +
						"You also have ${player.credits()} credit(s). These are available at ::store.")
			}
		}
	}

	class CustomPresetOptions(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)
			when(it.optionsTitled("Would you like to create a new custom preset?", "Yes, create a preset.", "No.")) {
				1 -> it.player.presetRepository().createFromState(it.inputString("Enter a name for your preset:"))
			}
		}
	}

	class PresetOld(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)
			QuestTabButtons.presetDialogue(it)
		}
	}

	class PresetInformation(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)
			it.doubleItemBox("Presets are a quick and convenient way to store a loadout and withdraw them later on. " +
					"Simply capture your inventory and equipment, enter a name, and load them when needed.", 1127, 385)
			it.doubleItemBox("By default, you have 5 free preset slots. Each donator rank increases your capacity with 2 slots, up to a total of 15.", 4712, 565)
		}
	}

	class PresetOptions(val player: Player, val preset: Preset) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)

			when (it.optionsTitled("What would you like to do?", "Withdraw this preset", "Delete this preset", "Rename this preset")) {
				1 -> { // Withdraw
					it.player.message("Withdrawing " + preset.name() + "...")
					it.player.stopActions(false)
					preset.apply(it.player)
					it.player.putattrib(AttributeKey.LAST_PRESET, preset.name())
				}
				2 -> { // Delete this preset
					if (it.optionsTitled("Permanently delete it?", "Delete it.", "Nevermind.") == 1) {
						it.player.presetRepository().delete(preset)
					}
				}
				3 -> { // Rename this preset
					it.player.presetRepository().rename(preset, it.inputString("Enter new name:"))
				}
			}
		}
	}

	class PresetEmptyOptions(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			player.stopActions(false)

			if (it.optionsTitled("Capture a new preset?", "Capture my current loadout as preset.", "Never mind.") == 1) {
				it.player.presetRepository().createFromState(it.inputString("Enter a name for your preset:"))
			}
		}
	}

	class RiskedWealth(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			it.player().stopActions(false)
			val mainrisk = it.player().attribOr<Int>(AttributeKey.RISKED_WEALTH, 0)
			val u20risk = it.player().attribOr<Int>(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0)
			val u20text = if (u20risk == 0) "." else "(including <col=ff3333>" + u20risk + "</col> worth of untradables, which you will keep when dying in under 20 wilderness)."
			it.itemBox("You're risking approximately <col=ff3333>" + (mainrisk + u20risk) + "</col> blood money" + u20text + " This value is based on protection prices.", 13307, 200)
		}
	}

	class EloRatingInfo(val player: Player, private val rating: Int, private val color: Color): Function1<Script, Unit> {
		@Suspendable override fun invoke(script: Script) {
            player.stopActions(true)
			// We replace yellow with red in this scenario because yellow isn't legible on this component.
			val colorize = if (color == Color.DARK_YELLOW) Color.RED else Color.GREEN
            val eloStanding = when {
                rating > EloRating.DEFAULT_ELO_RATING -> "above average"
                rating < EloRating.DEFAULT_ELO_RATING -> "below average"
                else -> "average"
            }
			script.messagebox("Your current elo rating is ${colorize.wrap(rating.toString())}. Your rating is ${colorize.wrap(eloStanding)}.")
            script.messagebox("You gain elo while killing a player within the wilderness. You will also lose elo rating if you die.")
            script.messagebox("The amount you gain or lose is dependent on your opponent's combat level and elo rating.")
		}
	}

	class ToggleRiskProtection(val player: Player) : Function1<Script, Unit> {
		@Suspendable override fun invoke(it: Script) {
			it.player().stopActions(false)
			if(player.timers().has(TimerKey.RISK_PROTECTION)) {
				it.itemBox("You can toggle your risk protection in ${player.timers().asMinutesAndSecondsLeft(TimerKey.RISK_PROTECTION)}.", 8792, 100)
				return
			}
			when(it.options("Toggle risk protection", "Forfeit risk protection", "What is Risk Protection?")) {
				1 -> {
					when(it.optionsTitled("What level of risk protection would you like?",
							"10,000+ BM", "25,000+ BM", "50,000+ BM")) {
						1 -> RiskProtectionState.toggleRiskProtection(player, 1)
						2 -> RiskProtectionState.toggleRiskProtection(player, 2)
						3 -> RiskProtectionState.toggleRiskProtection(player, 3)
					}
				}
				2 -> {
					val tier = player.attribOr<Int>(AttributeKey.RISK_PROT_TIER, 3)
					if(tier == 0) {
						player.message("You've already forfeited your risk protection. ")
					} else {
						if (it.optionsTitled("Forfeit your risk protection?", "Yes, let anyone attack me!", "No, I don't want to be ragged.") == 1) {
							player.putattrib(AttributeKey.RISK_PROT_TIER, 0)
							player.timers().extendOrRegister(TimerKey.RISK_PROTECTION, GameCommands.RISK_PROTECTION_EXPIRE_TICKS) // 20 minutes
							player.message(Color.DARK_RED.wrap("You've forfeit your risk protection for %d minutes."), GameCommands.RISK_PROTECTION_EXPIRE_TICKS / 100)
							RiskProtectionState.INSTANCE.send(player)
						}
					}
				}
				3 -> {
					player.sendScroll("Risk Protection",
							"Risk protection is a toggleable feature to prevent raggers",
							"or welfare PVPers from attacking players who are looking for",
							"fights of equal risked wealth. You're able to toggle between",
							"three different protection types:",
							"",
							Color.DARK_RED.wrap("Tier 1 -> Greater than 10,000 blood money"),
							Color.DARK_RED.wrap("Tier 2 -> Greater than 25,000 blood money"),
							Color.DARK_RED.wrap("Tier 3 -> Greater than 50,000 blood money"),
							"",
							"You're unable to attack a player who has higher risk protection",
							"than you have in risked wealth. If you attack a player who has",
							"less wealth than your risk prevention, your risk prevention will",
							"be forfeited for 10 minutes. During this period anybody is able",
							"to attack you regardless of their risked wealth.")
				}
			}
		}
	}


}