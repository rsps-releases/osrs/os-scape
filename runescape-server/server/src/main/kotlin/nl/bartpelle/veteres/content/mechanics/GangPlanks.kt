package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.script.ScriptRepository
import org.apache.logging.log4j.LogManager
import java.io.DataInputStream
import java.io.FileInputStream

/**
 * Created by Jak on 17/10/2016.
 */
object GangPlanks {
	
	private val logger = LogManager.getLogger(GangPlanks::class.java)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		val input = DataInputStream(FileInputStream("data/map/gangplankIds.bin"))
		logger.info("Loaded " + input.available() / 4 + " gangplank ids.")
		while (input.available() > 0) {
			val id = input.readInt()
			if (id in intArrayOf(14315, 14314, 25631, 25629, 25632, 25630)) continue; // skip these custom ones
			
			repo.onObject(id, s@ @Suspendable {
				val obj = it.interactionObject()
				if (it.interactionOption() != 1) return@s
				it.player().lock()
				val start = it.player().tile()
				val dir = if (obj.tile().x < start.x && obj.tile().z == start.z) Direction.West else
					if (obj.tile().x > start.x && obj.tile().z == start.z) Direction.East else
						if (obj.tile().z < start.z && obj.tile().x == start.x) Direction.South else
							Direction.North
				val forceWalkMove = when (dir) {
					Direction.West -> Tile(-1, 0)
					Direction.North -> Tile(0, 1)
					Direction.East -> Tile(1, 0)
					Direction.South -> Tile(0, -1)
					else -> Tile(0, 0)
				}
				val destination = when (dir) {
					Direction.West -> Tile(-3, 0)
					Direction.North -> Tile(0, 3)
					Direction.East -> Tile(3, 0)
					Direction.South -> Tile(0, -3)
					else -> Tile(0, 0)
				}
				it.delay(1)
				it.player().pathQueue().interpolate(it.player().tile().x + forceWalkMove.x, it.player().tile().z + forceWalkMove.z, PathQueue.StepType.FORCED_WALK)
				it.delay(1)
				val onto = it.player().tile().level == 0
				it.player().teleport(start.transform(destination.x, destination.z, if (onto) 1 else -1))
				it.player().unlock()
				if (onto) {
					it.message("You board the ship.")
					it.message("You must speak to one of the sailors before it will set sail.")
				}
			})
		}
		
		repo.onWorldInit @Suspendable {
			val world: World = it.ctx<World>()
			it.delay(1)
			world.spawnObj(MapObj(Tile(2674, 3170, 1), 0, 10, 0), true)
		}
	}
}