package nl.bartpelle.veteres.content.skills.slayer

import com.google.common.collect.ImmutableRangeMap
import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Bart on 11/10/2015.
 */

data class SlayerMaster(val npcId: Int, val defs: Array<SlayerTaskDef>) {
	
	fun randomTask(player: Player): SlayerTaskDef {
		// Grab our last task and exclude it to avoid b2b
		val last: Int = player.attribOr(AttributeKey.SLAYER_TASK_ID, 0)
		
		// Build a map and fill it with possible tasks.
		var tmp = 0
		val rangemap: RangeMap<Int, SlayerTaskDef> = ImmutableRangeMap.builder<Int, SlayerTaskDef>().apply {
			defs.forEach { task ->
				if (task != null && task.creatureUid != last &&
						player.skills().xpLevel(Skills.SLAYER) >= SlayerCreature.lookup(task.creatureUid)!!.req &&
						player.skills().combatLevel() > SlayerCreature.lookup(task.creatureUid)!!.cbreq &&
						!SlayerRewards.isTaskBlocked(player, task)) {
					put(Range.closedOpen(tmp, tmp + task.weighing), task) // Range from where we left off up to the task weight
					tmp += task.weighing
				}
			}
		}.build()
		
		// Grab the span, and use that to pick a random task.
		val range = rangemap.span()
		val rnd = player.world().random(range.upperEndpoint() - 1)
		
		return rangemap.get(rnd)!!
	}
}
