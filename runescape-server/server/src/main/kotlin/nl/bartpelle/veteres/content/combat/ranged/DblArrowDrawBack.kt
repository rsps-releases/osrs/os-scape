package nl.bartpelle.veteres.content.combat.ranged

enum class DblArrowDrawBack(val arrow: Int, val gfx: Int) {
	BRONZE_ARROW(882, 1104),
	IRON_ARROW(884, 1105),
	STEEL_ARROW(886, 1106),
	MITHRIL_ARROW(888, 1107),
	ADAMANT_ARROW(890, 1108),
	RUNITE_ARROW(892, 1109),
	AMETHYST_ARROW(21326, 1383),
	ICE_ARROW(78, 1110),
	DRAGON_ARROW(11212, 1111)
	//ids continue.. cba getting atm. broad and fire arrows (regicide quest?)
}