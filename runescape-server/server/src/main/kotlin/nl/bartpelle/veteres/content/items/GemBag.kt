package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 22/05/2017.
 */
object GemBag {
	
	val GEM_BAG_ID = 12020
	// Unnoted
	val gemIds = intArrayOf(1617, 1619, 1621, 1623, 1631)
	val gemAttributes = arrayOf(AttributeKey.GEMBAG_DIAMOND, AttributeKey.GEMBAG_RUBY, AttributeKey.GEMBAG_EMERALD, AttributeKey.GEMBAG_SAPPHIRE, AttributeKey.GEMBAG_DRAGONSTONE)
	
	@ScriptMain @JvmStatic fun register(repo: ScriptRepository) {
		
		// Fill
		repo.onItemOption1(GEM_BAG_ID) @Suspendable {
			val player = it.player()
			var added = 0
			nigger@ for (i in 0..gemIds.size - 1) {
				for (item in player.inventory().items()) {
					if (item == null || item.id() !in gemIds)
						continue
					if (gemIds[i] == item.id()) {
						val current = player.attribOr<Int>(gemAttributes[i], 0)
						if (current + 1 > 60) {
							player.message("Your gem bag cannot carry anymore ${Item(gemIds[i]).name(player.world()).toLowerCase()}s.")
							continue@nigger
						} else {
							player.putattrib(gemAttributes[i], current + 1)
							player.inventory().remove(item, false)
							added++
						}
					}
				}
			}
			if (added > 0) {
				player.message("You've added $added more gemstones to the bag.")
			} else {
				player.message("You've no gems to add.")
			}
		}
		
		// Check
		repo.onItemOption3(GEM_BAG_ID) {
			val sb = StringBuilder()
			sb.append("The gem bag contains: ")
			for (i in 0..gemIds.size - 1) {
				sb.append("${it.player().attribOr<Int>(gemAttributes[i], 0)} ${Item(gemIds[i]).name(it.player().world()).toLowerCase()}s")
				sb.append("${if (i == gemIds.size - 1) "." else ", "}")
			}
			it.player().message(sb.toString())
		}
		
		// Empty
		repo.onItemOption4(GEM_BAG_ID) {
			val sb = StringBuilder()
			var total = 0
			for (i in 0..gemIds.size - 1) {
				total += it.player().attribOr<Int>(gemAttributes[i], 0)
			}
			if (total == 0) {
				it.player().message("The bag is empty.")
				return@onItemOption4
			}
			var added = 0
			for (i in 0..gemIds.size - 1) {
				if (it.player().inventory().freeSlots() == 0) {
					break
				}
				val count = it.player().attribOr<Int>(gemAttributes[i], 0)
				if (count > 0) {
					val toAdd = Math.min(count, it.player().inventory().freeSlots())
					if (it.player().inventory().add(Item(gemIds[i], toAdd), false).success()) {
						added += toAdd
						it.player().putattrib(gemAttributes[i], count - toAdd)
						if (i > 0 && i != gemIds.size - 1)
							sb.append(", ")
						sb.append("$toAdd ${Item(gemIds[i]).name(it.player().world()).toLowerCase()}s")
					}
				}
			}
			if (added > 0) {
				sb.insert(0, "You remove ")
				sb.append(" from the bag.")
				it.player().message(sb.toString())
			} else {
				it.player().message("The bag is empty.")
			}
		}
	}
}