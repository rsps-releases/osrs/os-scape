package nl.bartpelle.veteres.content.areas.edgeville.osrune

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.text.NumberFormat

/**
 * Created by Situations on 7/18/2016.
 */

object Ilfeen {
	
	const val ILFEEN = 4003
	const val CRYSTAL_SEED = 4207
	const val BOW_COST = 800000
	const val SHIELD_COST = 800000
	const val HALBERD_COST = 1200000
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ILFEEN) @Suspendable {
			if (!it.player().world().realm().isPVP) {
				it.chatPlayer("Hey ya.", 588)
				
				if (it.player().inventory().contains(Item(CRYSTAL_SEED)))
					has_seed(it, ILFEEN) else no_seed(it, ILFEEN)
				
			} else if (it.player().world().realm().isPVP) {
				if (!it.player().inventory().contains(Item(CRYSTAL_SEED))) {
					no_seed(it, ILFEEN)
				} else {
					val opt = it.optionsTitled("Which would you like Elfeen to enchant?", "Crystal bow.", "Crystal shield.", "Crystal halberd", "Never mind.")
					if (opt != 4) {
						val resultStr = when (opt) {
							1 -> "bow"
							2 -> "shield"
							3 -> "halberd"
							else -> "unknown"
						}
						val resItem = when (opt) {
							1 -> 4212
							2 -> 4224
							else -> 13091
						}
						val cost = when (opt) {
							1 -> 10000
							2 -> 3500
							else -> 5500
						}
						if (it.player().inventory().count(13307) < cost) {
							it.chatNpc("You don't have $cost blood money to enchant this crystal, " + it.player().name() + ".", ILFEEN)
						} else if (it.optionsTitled("Pay Ilfeen $cost blood money for a Crystal $resultStr?", "Yes", "No") == 1) {
							val ok = it.player().inventory().remove(Item(CRYSTAL_SEED), false).success() && it.player().inventory().remove(Item(13307, cost), true).success()
							if (ok) {
								it.targetNpc()!!.animate(713)
								it.player().inventory().add(Item(resItem), false)
								when (resItem) {
                                    4212 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_BOW_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_BOW_ENCHANTS, 0))
                                    4224 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_SHIELD_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_SHIELD_ENCHANTS, 0))
                                    13091 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_HALBERD_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_HALBERD_ENCHANTS, 0))
                                }
								it.doubleItemBox("Ilfeen uses an elven enchantment to weave the seed into a $resultStr.", CRYSTAL_SEED, resItem)
								val extra = if (it.player().world().rollDie(20, 1)) "You're one sexy minx." else ""
								it.chatPlayer("Thanks a lot, Ilfeen. $extra")
							}
						}
					}
				}
			}
		}
	}
	
	@JvmStatic @Suspendable fun has_seed(it: Script, npcId: Int) {
		it.chatNpc("Would you like me to enchant your seed for you?", npcId, 590)
		when (it.options("Crystal Bow", "Crystal Shield", "Crystal Halberd")) {
			1 -> exchange_item(it, "Crystal Bow", 4214, BOW_COST, npcId) // Crystal bow Full
			2 -> exchange_item(it, "Crystal Shield", 4225, SHIELD_COST, npcId) // shield Full
			3 -> exchange_item(it, "Crystal Halberd", 13091, HALBERD_COST, npcId) // halberd new
		}
	}
	
	@JvmStatic @Suspendable fun no_seed(it: Script, npcId: Int) {
		if (it.player().world().realm().isPVP) {
			it.chatNpc("You'll need a crystal seed on you if you want me to enchant it, " + it.player().name() + "!", npcId, 588)
		} else {
			it.chatNpc("Talk to me once you have a crystal seed and I'll enchant it into a Crystal bow, shield, or halberd for you.", npcId, 588)
		}
	}
	
	@JvmStatic @Suspendable fun exchange_item(it: Script, name: String, item: Int, cost: Int, npcId: Int) {
		it.chatPlayer("Could you please chant my seed for a $name?")
		it.chatNpc("Absolutely, but it'll cost you ${NumberFormat.getInstance().format(cost)} for me to chant it for you.", npcId, 590)
		when (it.options("Okay", "That's way too much!")) {
			1 -> {
				it.chatPlayer("Okay, here you go..")
				if (it.player().inventory().freeSlots() >= 1) {
					if (it.player().inventory().remove(Item(995, cost), false).success() &&
							it.player().inventory().remove(Item(CRYSTAL_SEED, 1), false).success()) {
						it.player().inventory().add(Item(item), true)
						it.doubleItemBox("You give Ilfeen ${NumberFormat.getInstance().format(cost)} coins and your crystal seed in return for a $name.", Item(995, cost), Item(item))
						when (item) {
                            4214 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_BOW_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_BOW_ENCHANTS, 0))
                            4225 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_SHIELD_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_SHIELD_ENCHANTS, 0))
                            13091 -> it.player().putattrib(AttributeKey.CRYSTAL_SEED_TO_HALBERD_ENCHANTS, 1 + it.player().attribOr<Int>(AttributeKey.CRYSTAL_SEED_TO_HALBERD_ENCHANTS, 0))
                        }
					} else {
						it.chatNpc("You don't have enough gold for me to chant a $name.", npcId, 590)
					}
				} else {
					it.chatNpc("Talk to me once you have an open inventory slot.", npcId, 588)
				}
			}
			2 -> {
				it.chatPlayer("Ohhh.. yeah I don't think I can afford that.")
				it.chatNpc("Suit yourself!", npcId, 590)
			}
		}
	}
}
