package nl.bartpelle.veteres.content.combat.ranged

enum class ThrownaxeDrawback(val arrow: Int, val gfx: Int, val projectile: Int) {
	BRONZE(800, -1, -1),
	IRON(801, -1, -1),
	STEEL(802, -1, -1),
	MITHRIL(803, -1, -1),
	ADAMANT(804, -1, -1),
	RUNE(805, -1, -1),
	DRAGON(20849, 1320, 1319)
}