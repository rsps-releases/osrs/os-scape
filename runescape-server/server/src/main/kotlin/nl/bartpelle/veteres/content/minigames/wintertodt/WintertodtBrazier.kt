package nl.bartpelle.veteres.content.minigames.wintertodt

import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.map.MapObj

/**
 * Created by Bart on 9/10/2016.
 */
data class WintertodtBrazier(val direction: FaceDirection, val tile: Tile, val pyroTile: Tile, val projectileCenter: Tile) {
	
	var pyromancerAlive: Boolean = false
		get() = pyromancer!!.sync().transmog() != 7372
		set
	
	private var state = BrazierState.STALE
	
	var pyromancer: Npc? = null
	
	fun updatePyromancer(world: World) {
		for (n in world.npcs().entriesList) {
			if (n != null && n.id() == 7371 && n.tile().equals(pyroTile)) {
				pyromancer = n
				return
			}
		}
	}
	
	fun getState(): BrazierState = state
	
	fun changeState(s: BrazierState, world: World) {
		state = s
		world.spawnObj(MapObj(tile, s.objectId, 10, 0))
	}
	
	fun doesDamage(): Boolean = pyromancerAlive && state == BrazierState.LIT
	
}