package nl.bartpelle.veteres.content.skills.agility.rooftop

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.skills.agility.UnlockAgilityPet
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 9/5/2015.
 * Finished on 3/12/2016
 */
object AlKharidCourse {
	
	val MARK_SPOTS = arrayOf(Tile(3275, 3186, 3),
			Tile(3267, 3170, 3),
			Tile(3290, 3162, 3),
			Tile(3317, 3161, 1),
			Tile(3317, 3177, 2),
			Tile(3303, 3189, 3))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Wall climb
		r.onObject(10093, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().skills().xpLevel(Skills.AGILITY) < 20) {
				it.message("You need an Agility level of 20 to attempt this.")
			} else {
				it.player().lockNoDamage()
				it.player().faceTile(it.player().tile() - Tile(0, 1))
				it.delay(1)
				it.player().animate(828, 15)
				it.delay(2)
				it.player().teleport(3273, 3192, 3)
				it.animate(-1)
				it.player().unlock()
				it.addXp(Skills.AGILITY, 10.0)
				
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
			}
		})
		
		// Tightrope
		r.onObject(10284, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lockNoDamage()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 6)
			it.player().pathQueue().interpolate(3272, 3172, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(3272, 3173))
			it.player().looks().resetRender()
			it.delay(1)
			it.player().unlock()
			it.addXp(Skills.AGILITY, 30.0)
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
		})
		
		// Cable swing
		r.makeRemoteObject(10355) // Custom walk script
		r.onObject(10355, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().walkTo(3266, 3166, PathQueue.StepType.REGULAR).equals(3266, 3166)) {
				it.waitForTile(Tile(3266, 3166))
				it.player().lock()
				it.player().faceTile(it.player().tile() + Tile(20, 0))
				it.delay(1)
				
				it.message("You begin an almighty run-up...")
				it.animate(1995)
				it.player().forceMove(ForceMovement(3, 0, 30, 0))
				it.delay(1)
				it.message("You gained enough momentum to swing to the other side!")
				it.animate(751)
				it.player().teleport(3269, 3166, 3)
				
				it.player().forceMove(ForceMovement(15, 0, 60, 0))
				it.delay(2)
				it.player().teleport(3284, 3166, 3)
				it.addXp(Skills.AGILITY, 40.0)
				
				it.player().unlock()
				
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
			}
		})
		
		// Teeth-rope :)
		r.onObject(10356, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock();
			it.player().faceTile(it.player().tile() + Tile(1, 0))
			it.delay(1)
			it.player().animate(2586, 10)
			it.delay(1)
			it.player().sound(1933)
			it.player().sound(1934, 15)
			it.player().teleport(3303, 3163, 1)
			it.animate(1601)
			it.delay(1)
			it.animate(1602)
			
			for (i in 1..11) {
				it.player().forceMove(ForceMovement(1, 0, 30, 0))
				it.delay(1)
				it.player().teleport(it.player().tile() + Tile(1, 0))
			}
			
			it.delay(1)
			it.animate(-1)
			it.player().pathQueue().step(3315, 3163, PathQueue.StepType.FORCED_WALK)
			it.addXp(Skills.AGILITY, 40.0)
			
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
		})
		
		// Palm tree swing
		r.makeRemoteObject(10357) // Custom walk script
		r.onObject(10357, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().walkTo(3318, 3165, PathQueue.StepType.REGULAR).equals(3318, 3165)) {
				it.waitForTile(Tile(3318, 3165))
				it.delay(1)
				it.player().lock()
				
				it.player().sound(2468, 10)
				it.animate(2583)
				it.player().forceMove(ForceMovement(0, 0, 0, 4, 35, 49, FaceDirection.NORTH))
				it.delay(1)
				it.player().teleport(3318, 3169, 1)
				it.player().faceTile(Tile(3320, 3169, 1))
				it.delay(1)
				it.player().faceTile(Tile(3320, 3169, 1))
				
				it.player().sound(2459, 35)
				it.animate(1122)
				it.player().forceMove(ForceMovement(0, 0, 0, 1, 34, 42, FaceDirection.NORTH))
				it.delay(1)
				it.player().teleport(Tile(3318, 3170, 1))
				it.delay(1)
				
				
				it.animate(1124)
				it.player().forceMove(ForceMovement(0, 0, -1, 4, 34, 52, FaceDirection.SOUTH))
				it.delay(1)
				
				it.player().sound(2455)
				it.animate(2588)
				it.delay(1)
				it.player().teleport(Tile(3317, 3174, 2))
				it.addXp(Skills.AGILITY, 10.0)
				
				it.player().unlock()
				
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
			}
		})
		
		// Wall climb v2
		r.onObject(10094, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() + Tile(0, 1))
			it.delay(1)
			it.player().animate(828, 15)
			it.delay(2)
			it.player().teleport(3316, 3180, 3)
			it.animate(-1)
			it.addXp(Skills.AGILITY, 5.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
		})
		
		// Last tightrope
		r.onObject(10583, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lockNoDamage()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 6)
			it.player().pathQueue().step(3313, 3186, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().interpolate(3302, 3186, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().step(3302, 3187, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(3302, 3187))
			it.player().looks().resetRender()
			it.delay(1)
			it.player().unlock()
			it.addXp(Skills.AGILITY, 15.0)
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
		})
		
		// Jump down
		r.onObject(10352, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().faceTile(it.player().tile() + Tile(-1, 1))
			it.player().animate(2586)
			it.delay(1)
			it.player().teleport(3299, 3194, 0)
			it.animate(-1)
			it.addXp(Skills.AGILITY, 30.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 40, 20)
			
			// Woo! A pet!
			val odds = (28000.00 * it.player().mode().skillPetMod()).toInt()
			if (it.player().world().rollDie(odds, 1)) {
				UnlockAgilityPet.unlockGiantSquirrel(it.player())
			}
		})
	}
	
}