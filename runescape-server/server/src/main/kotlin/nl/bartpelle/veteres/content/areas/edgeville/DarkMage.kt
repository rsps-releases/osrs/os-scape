package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-01-16.
 */

object DarkMage {
	
	val DARK_MAGE = 2583
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(DARK_MAGE) @Suspendable {
			it.chatPlayer("Hello there.", 588)
			it.chatNpc("Quiet!<br>You must not break my concentration!", DARK_MAGE, 615)
			when (it.options("Why not?", "What are you doing here?", "Ok Sorry.")) {
				1 -> whyNot(it)
				2 -> whatAreYouDoingHere(it)
				3 -> okSorry(it)
			}
		}
		
		r.onNpcOption2(DARK_MAGE) @Suspendable {
			attemptRepairs(it, it.player())
		}
	}
	
	@Suspendable private fun whyNot(it: Script) {
		it.chatPlayer("Why not?", 554)
		it.chatNpc("Well, if my concentration is broken while keeping this gate open, then, if we are luck," +
				"everyone within a one mile radius will either have their heads explode, or will be consumed" +
				"internally by the creatures of the Abyss.", DARK_MAGE, 588)
		it.chatPlayer("Erm...<br>And if we are unlucky?", 575)
		it.chatNpc("If we are unlucky, then the entire universe will begin to fold in upon itself, and all" +
				"reality as we know it will be annihilated in a single stroke.", DARK_MAGE, 588)
		it.chatNpc("So leave me alone!", DARK_MAGE, 614)
		when (it.options("What are you doing here?", "Ok, Sorry.")) {
			1 -> whatAreYouDoingHere(it)
			2 -> okSorry(it)
		}
	}
	
	@Suspendable private fun whatAreYouDoingHere(it: Script) {
		it.chatPlayer("What are you doing here?", 575)
		it.chatNpc("Do you mean what am I doing here in Abyssal space, or are you asking me what I consider" +
				"my ultimate role to be in this voyage what we call life?", DARK_MAGE, 575)
		it.chatPlayer("Um... the first one.")
		it.chatNpc("By remaining here and holding this portal open, I am providing a permanent link between" +
				"normal space and this strange dimension we call Abyssal space.", DARK_MAGE)
		it.chatNpc("As long as this spell remains in effect, we have the capability to teleport into abyssal " +
				"space at will.", DARK_MAGE)
		it.chatNpc("No leave me be!<br>I can afford no distraction in my task!", DARK_MAGE, 614)
		when (it.options("Why not?", "Ok, Sorry.")) {
			1 -> whyNot(it)
			2 -> okSorry(it)
		}
	}
	
	@Suspendable private fun okSorry(it: Script) {
		it.chatPlayer("Ok, Sorry.")
		it.chatNpc("I am attempting to subdue the elemental mechanics of the universe to my will.", DARK_MAGE, 575)
		it.chatNpc("Inane chatter from random idiots is not helping me achieve this!", DARK_MAGE, 575)
	}
	
	@Suspendable private fun attemptRepairs(it: Script, player: Player) {
		it.chatNpc("You don't seem to have any pouches in need of repair.<br>Leave me alone.", DARK_MAGE, 575)
	}
}


