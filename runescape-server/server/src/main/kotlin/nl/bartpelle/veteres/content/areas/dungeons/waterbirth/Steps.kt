package nl.bartpelle.veteres.content.areas.dungeons.waterbirth

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-01-17.
 */

object Steps {
	
	val STEPS = 8966
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(STEPS) @Suspendable {
			it.player().teleport(Tile(2523, 3740))
		}
	}
	
}
