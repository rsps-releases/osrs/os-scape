package nl.bartpelle.veteres.content.areas.dungeons

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2/13/2016.
 */

object DagannothLair {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Climb the ladder down into the boss room.
		r.onObject(10230) {
			it.player().lock();
			it.player().animate(827)
			it.delay(1)
			it.player().teleport(Tile(2900, 4449))
			it.player().unlock()
		}
		
		//Climb the ladder to get out of the bsos room.
		r.onObject(10229) {
			it.player().lock();
			it.player().animate(828)
			it.delay(1)
			it.player().teleport(Tile(1910, 4367))
			it.player().unlock()
		}
		
	}
}