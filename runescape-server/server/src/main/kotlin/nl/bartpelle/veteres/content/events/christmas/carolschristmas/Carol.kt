package nl.bartpelle.veteres.content.events.christmas.carolschristmas

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-15.
 */

object Carol {
	
	val CAROL = 7325
	val BATTERED_TEDDY = 20830
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(CAROL, s@ @Suspendable {
			if (CarolsChristmas.getStage(it.player()) == 22) {
				if (it.player().looks().trans() == 3008) {
					val res = it.player.interfaces().resizable()
					
					it.chatNpc("Eeeeek! A ghost! Please don't hurt Carol.", CAROL, 571)
					it.chatPlayer("Carol... I am the ghost of Christmas future... you must<br>change your ways...", 589)
					it.chatNpc("But Carol doing good, targets are being met.", CAROL, 571)
					it.chatPlayer("You must take care of your workers Carol. Money<br>isn't everything.", 589)
					it.chatNpc("Money is everything to business troll. What happens if<br>Carol not change?", CAROL, 572)
					it.chatPlayer("Your workers are so tired they will wander deeper in<br>to the cave to cycle for somewhere to sleep.", 589)
					it.chatPlayer("Ulina will wander too far and she will not return. How<br>will your targets be met if your workers die Carol?", 611)
					it.chatNpc("Carol not want Ulina to die. That human worker was<br>right. Carol is bad boss and Carol is sorry.", CAROL, 611)
					it.chatPlayer("It is not too late Carol, you can still change things for<br>the better.", 568)
					it.chatNpc("Carol is bad boss. Carol has failed to meet targets. Carol<br>will return to Wiss now...", CAROL, 611)
					it.chatPlayer("No Carol, you must stay and make the presents for<br>Christmas!", 589)
					/*                  it.player().lock()
									  it.player().interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
									  it.player().invokeScript(951)
									  it.delay(2)
									  it.player().invokeScript(948, 0, 0, 0, 255, 50)*/
					CarolsChristmas.setStage(it.player(), 23)
					//Varp 1458 => 34856
					it.player().looks().resetRender()
					it.player().looks().transmog(-1)
					it.messagebox("Carol ignores you and prepares to leave the factory.")
					//it.player().unlock()
					it.chatPlayer("I'd better go and tell Holly the good news.", 567)
				} else {
					it.chatNpc("Don't just stand there, get on with some work!", CAROL, 614)
				}
			} else if (CarolsChristmas.getStage(it.player()) == 20) {
				it.chatPlayer("Carol, I've seen the future! If you don't change your<br>ways Ulina is going to die!", 572)
				it.chatNpc("Hahaha, nice try human... get back to work!", CAROL, 605)
				CarolsChristmas.setStage(it.player(), 21)
			} else if (CarolsChristmas.getStage(it.player()) == 17) {
				for (item in intArrayOf(20802, 20804, 20806, 20816, 20818, 20820, 20824, 20826, 20828)) {
					if (it.player().inventory().has(item)) {
						it.chatPlayer("I have a present for you.", 567)
						it.chatNpc("Oh? Hand it to me then.", CAROL, 588)
						return@s
					}
				}
				
				it.chatNpc("Don't just stand there, get on with some work!", CAROL, 614)
			} else if (CarolsChristmas.getStage(it.player()) == 10) {
				it.chatPlayer("Hello Carol.", 614)
				it.chatNpc("What puny human worker want?", CAROL, 588)
				it.chatPlayer("You've been treating your staff terribly and I'm here<br>to put a stop to it!", 615)
				it.chatNpc("Puny human worker make me laugh. Me has targets to<br>meet, workers must meet targets.", CAROL, 606)
				it.chatPlayer("No. They're overworked and underpaid and this will<br>stop now!", 615)
				it.chatNpc("If puny human worker want to help puny human will<br>go work with them.", CAROL, 606)
				it.chatPlayer("No, you will stop this slavery now!", 610)
				it.chatNpc("Puny human worker will leave now.", CAROL, 605)
				CarolsChristmas.setStage(it.player(), 12)
				it.chatPlayer("I'd better go and speak to Holly.", 575)
			} else {
				if (it.player().inventory().has(BATTERED_TEDDY)) {
					it.chatPlayer("I've found your teddy... Doesn't this remind of what<br>Christmas is about?", 568)
					if (it.player().inventory().remove(Item(BATTERED_TEDDY, 1), false).success()) {
						CarolsChristmas.setStage(it.player(), 16)
						it.itemBox("You hand the teddy to Carol.", BATTERED_TEDDY)
						it.chatNpc("Carol remembers... but that was past. I am business<br>troll now and targets must still be met.", CAROL, 589)
						it.chatPlayer("But think of the people... don't they deserve to feel good<br>about Christmas?", 555)
						it.chatNpc("Targets must be met!", CAROL, 614)
					}
				}
				
				it.chatNpc("Don't just stand there, get on with some work!", CAROL, 614)
			}
		})
		
		r.onItemOnNpc(CAROL, s@ @Suspendable {
			val player = it.player()
			val itemUsed = it.itemUsed()
			val requiredItem = player.attribOr<Int>(AttributeKey.CAROLS_REQUIRED_TOY, 0)
			
			for (item in intArrayOf(20802, 20804, 20806, 20816, 20818, 20820, 20824, 20826, 20828)) {
				if (itemUsed.id() == item) {
					it.chatPlayer("I have a present for you!", 567)
					it.itemBox("You hand Carol your freshly crafted gift.", itemUsed.id())
					
					if (player.inventory().remove(itemUsed, false).success()) {
						if (itemUsed.id() == requiredItem) {
							CarolsChristmas.setStage(it.player(), 18)
							it.chatNpc("This good, Carol likes this!", CAROL, 567)
							it.chatPlayer("Doesn't it feel good to receive gifts? Now will you cut<br>your workers some slack?", 555)
							it.chatNpc("Haha puny human is funny. Get back to work.", CAROL, 605)
						} else {
							it.chatNpc("Stop giving me things I don't like and get back to work!", CAROL, 614)
						}
						
						return@s
					}
				}
			}
		})
	}
	
}
