package nl.bartpelle.veteres.content.mechanics.deadman

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jak on 05/11/2016.
 *
 * Handles the Protected Skills interface and shows you the 10 most vaulable bank items you will lose on death.
 */
object DeadmanProtectionInterface {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		// Examine a lost item from bank
		repo.onButton(226, 16) {
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val lostItems = dmminfo.bankItemsKey.lootcontainer
			if (lostItems != null && it.buttonSlot() <= lostItems.size()) {
				val item = lostItems[it.buttonSlot()]
				it.player().message("You will lose this item from your bank should you die: ${item.amount()} x ${item.definition(it.player().world()).name}.")
			}
		}
		
		// Choosing stats to protect on death
		DmmIKODStats.values().forEach { protectInfo ->
			repo.onButton(226, protectInfo.buttonId) {
				val curvarp = it.player().varps().varp(1295)
				
				// It's on? turn it off.
				if (curvarp.and(protectInfo.bitmask) == protectInfo.bitmask) {
					it.player().varps().varp(1295, it.player().varps().varp(1295).and(protectInfo.bitmask.inv()))
				} else {
					// How many are already enabled?
					val protected = getProtectedStats(curvarp)
					var cbProtected = 0
					var skillsProtected = 0
					protected.forEach { stat ->
						if (stat != null) {
							if (stat.isCombat) cbProtected++ else skillsProtected++
						}
					}
					
					if (protectInfo.isCombat && cbProtected == 2) {
						// it.message("You've already protected 2 combat stats.")
						it.player().varps().sync(1295)
					} else if (!protectInfo.isCombat && skillsProtected == 3) {
						//it.message("You've already protected 3 non-combat stats.")
						it.player().varps().sync(1295)
					} else {
						// Turn it on
						it.player().varps().varp(1295, it.player().varps().varp(1295).xor(protectInfo.bitmask))
					}
				}
				// it.player().debug("Varp 1295 now " + it.player().varps().varp(1295))
			}
		}
		
	}
	
	/**
	 * Returns a List of <DmmIKODStats> enum representing which skulls you're chosen to protected upon dying in Deadman.
	 */
	@JvmStatic fun getProtectedStats(varpValue: Int): ArrayList<DmmIKODStats> {
		val protected = ArrayList<DmmIKODStats>(0)
		DmmIKODStats.values().forEach { info ->
			if (varpValue.and(info.bitmask) == info.bitmask) {
				protected.add(info)
			}
		}
		return protected
	}
}