package nl.bartpelle.veteres.content.npcs.bosses.cerberus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile

/**
 * Created by Jason MacKeigan on 2016-07-11 at 2:56 PM
 */
class CerberusAOEAttackScript(val target: Entity, var ticksRemaining: Int = 7) {
	
	val script: Function1<Script, Unit> = suspend@ @Suspendable {
		val cerberus = it.npc()
		val players = cerberus.closePlayers(20)
		
		val locations = listOf<Tile>(target.tile(), target.world().randomTileAround(target.tile(), 3),
				target.world().randomTileAround(target.tile(), 3))
		
		for (location in locations) {
			cerberus.world().tileGraphic(1246, location, target.tile().level, 0)
		}
		
		while (ticksRemaining-- > 0) {
			for (player in players) {
				for (location in locations) {
					if (player.tile().equals(location.x, location.z)) player.hit(cerberus, 10, 0)
					if (player.tile().nextTo(location)) player.hit(cerberus, 7, 0)
				}
			}
			
			it.delay(2)
		}
		
		locations.forEach { cerberus.world().tileGraphic(1247, it, 0, 0) }
	}
	
}