package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.items.equipment.TomeOfFire
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Situations on 12/1/2015.
 */

object Staves {
	
	const val FIRE_RUNE = 554
	const val WATER_RUNE = 555
	const val AIR_RUNE = 556
	const val EARTH_RUNE = 557
	
	@Suspendable fun checkStaff(player: Player): String {
		val equipment = player.equipment()

		if (TomeOfFire.equipped(player)) return "FIRE"
		if (equipment.hasAny(1405, 1381, 1397)) return "AIR"
		else if (equipment.hasAny(1403, 1383, 1395, 21006)) return "WATER"
		else if (equipment.hasAny(1401, 1387, 1393)) return "FIRE"
		else if (equipment.hasAny(1407, 1385, 1399)) return "EARTH"
		else if (equipment.hasAny(11998, 12000)) return "SMOKE"
		else if (equipment.hasAny(6562, 6563)) return "MUD"
		else if (equipment.hasAny(11787, 11789, 12795, 12796)) return "STEAM"
		else if (equipment.hasAny(3053, 3054)) return "LAVA"
		else if (equipment.hasAny(20736, 20739)) return "DUST"
		else if (equipment.hasAny(20730, 20733)) return "MIST"
		else return "none"
	}
	
	@JvmStatic @Suspendable fun staffRunes(player: Player, rune: Int): Boolean {
		if (player.varps().varp(Varp.INFINITY_RUNES).shr(1) == 1) return true
		val STAFF = checkStaff(player)
		if (rune == AIR_RUNE && STAFF == "AIR") return true
		else if (rune == AIR_RUNE && STAFF == "DUST") return true
		else if (rune == AIR_RUNE && STAFF == "MIST") return true
		else if (rune == WATER_RUNE && STAFF == "WATER") return true
		else if (rune == WATER_RUNE && STAFF == "MIST") return true
		else if (rune == FIRE_RUNE && STAFF == "FIRE") return true
		else if (rune == EARTH_RUNE && STAFF == "EARTH") return true
		else if (rune == EARTH_RUNE && STAFF == "DUST") return true
		else if (rune == AIR_RUNE && STAFF == "SMOKE") return true
		else if (rune == FIRE_RUNE && STAFF == "SMOKE") return true
		else if (rune == WATER_RUNE && STAFF == "STEAM") return true
		else if (rune == FIRE_RUNE && STAFF == "STEAM") return true
		else if (rune == EARTH_RUNE && STAFF == "MUD") return true
		else if (rune == WATER_RUNE && STAFF == "MUD") return true
		else if (rune == FIRE_RUNE && (STAFF == "LAVA")) return true
		else if (rune == EARTH_RUNE && STAFF == "LAVA") return true
		return false
	}
}