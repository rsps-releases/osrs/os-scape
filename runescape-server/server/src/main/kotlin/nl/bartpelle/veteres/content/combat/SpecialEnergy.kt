package nl.bartpelle.veteres.content.combat

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 8/16/2015.
 */

object SpecialEnergy {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onLogin @Suspendable { it.player().timers()[TimerKey.SPECIAL_ENERGY_RECHARGE] = 50 }
		
		repo.onTimer(TimerKey.SPECIAL_ENERGY_RECHARGE) {
			val player = it.player()
			if (player.varps()[Varp.SPECIAL_ENERGY] < 1000) {
				// Restore rate 300 per 30s instead of 100 in safe. QoL for hybrids/edge pkers.
				var safe: Boolean = (!WildernessLevelIndicator.inWilderness(player.tile()) && !PVPAreas.inPVPArea(player)) || (PVPAreas.inPVPArea(player) && WildernessLevelIndicator.inPvpInstanceSafezone(player.tile()))
				if (!player.world().realm().isPVP || player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
					safe = false
				}
				
				// Duel arena!!!
				if (player.attribOr(AttributeKey.IN_STAKE, false)) {
					safe = false;
				}
				
				val set = player.varps()[Varp.SPECIAL_ENERGY] + if (player.world().realm().isDeadman) 150 else if (safe) 300 else 100
				player.varps()[Varp.SPECIAL_ENERGY] = Math.min(1000, set)
			}
			
			player.timers()[TimerKey.SPECIAL_ENERGY_RECHARGE] = if (player.world().realm().isDeadman) 20 else 50
		}
	}
	
}
