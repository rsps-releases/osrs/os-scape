package nl.bartpelle.veteres.content.npcs.godwars.zamorak

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.npcs.godwars.GwdLogic
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Situations on 11/27/2015.
 */

object Kril {
	
	@JvmStatic val ENCAMPMENT = Area(2918, 5318, 2936, 5331)
	@JvmStatic var lastBossDamager: Entity? = null
	
	@JvmStatic fun isMinion(n: Npc): Boolean {
		return n.id() in arrayOf(3129, 3130, 3132)
	}
	
	val QUOTES = arrayOf("Attack them, you dogs!",
			"Forward!",
			"Death to Saradomin's dogs!",
			"Kill them, you cowards!",
			"The Dark One will have their souls!",
			"Zamorak curse them!",
			"Rend them limb from limb!",
			"No retreat!")
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target, 50) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10)) {
				val melee_dist = EntityCombat.canAttackMelee(npc, target, true)
				if (EntityCombat.attackTimerReady(npc)) {
					// Do a quote?
					if (npc.world().rollDie(3, 1)) {
						npc.sync().shout(npc.world().random(QUOTES))
					}
					
					// Attack the player
					if (melee_dist && npc.world().rollDie(2, 1)) {
						// Shall we do the special that slams through?
						if (npc.world().rollDie(5, 1)) {
							npc.sync().shout("YARRRRRRR!") // Overrides previous quote :)
							attackMelee(npc, target, true)
						} else {
							attackMelee(npc, target)
						}
					} else {
						attackMage(npc, target)
					}
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun attackMage(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(3, 3, 0).distance(target.tile())
		npc.world().spawnProjectile(npc, target, 1227, 1, 5, 25, 12 * tileDist, 15, 10)
		val delay = Math.max(1, (20 + (tileDist * 12)) / 30)
		npc.animate(6950)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
			target.hit(npc, npc.world().random(30), delay.toInt()).combatStyle(CombatStyle.MAGIC)
		} else {
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC)
		}
	}
	
	fun attackMelee(npc: Npc, target: Entity, slamThrough: Boolean = false) {
		npc.animate(6948)
		
		if (slamThrough || EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			val hit = target.hit(npc, EntityCombat.randomHit(npc), 1)
			
			// Slamming through prayers is generic
			if (!slamThrough) {
				hit.combatStyle(CombatStyle.GENERIC)
			}
		} else {
			target.hit(npc, 0, 1).combatStyle(CombatStyle.MELEE)
		}
		
		// Slight chance of poison for a fuckton of damage
		if (npc.world().rollDie(10, 1)) {
			target.poison(16)
		}
		
		// Drain effect and message for slam-through
		if (slamThrough) {
			target.message("K'ril Tsutsaroth slams through your protection prayer, leaving you feeling drained.")
			(target as Player).skills().setLevel(Skills.PRAYER, target.skills().level(Skills.PRAYER) / 2)
		}
		// If we're in melee distance it's actually classed as if the target hit us -- has an effect on auto-retal in gwd!
		if (GwdLogic.isBoss(npc.id())) {
			val last_attacked_map = npc.attribOr<HashMap<Entity, Long>>(AttributeKey.LAST_ATTACKED_MAP, HashMap<Entity, Long>())
			last_attacked_map.put(target, System.currentTimeMillis())
			npc.putattrib(AttributeKey.LAST_ATTACKED_MAP, last_attacked_map)
		}
	}
}