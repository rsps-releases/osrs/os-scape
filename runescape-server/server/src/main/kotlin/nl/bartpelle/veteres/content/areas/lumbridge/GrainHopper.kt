package nl.bartpelle.veteres.content.areas.lumbridge

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Situations & Bart on 4/7/2016.
 */

object GrainHopper {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		val GRAIN = 1947
		val HOPPERS = arrayListOf(24961, 2586)
		val HOPPER_CONTROLS = arrayListOf(2607, 24964)
		val HOPPER_CONTROLS_PUSHED = 24967
		val FLOUR_BINS = arrayListOf(1781, 14960)
		val POT = 1931
		val POT_OF_FLOUR = 1933
		
		for (CONTROLS in HOPPER_CONTROLS) {
			r.onObject(CONTROLS) @Suspendable {
				val obj = it.interactionObject()
				val player = it.player()
				if (player.varps()[Varp.FLOUR_BIN] == 30) {
					it.message("The flour bin downstairs is full, I should empty it first.")
				} else {
					player.lock()
					player.animate(3571)
					it.sound(2400)
					if (CONTROLS == 24964) {
						it.runGlobal(player.world()) @Suspendable {
							val world = player.world()
							val old = MapObj(obj.tile(), 24964, obj.type(), obj.rot())
							val spawned = MapObj(obj.tile(), HOPPER_CONTROLS_PUSHED, obj.type(), obj.rot())
							world.removeObj(old)
							world.spawnObj(spawned)
							it.delay(2)
							world.removeObjSpawn(spawned)
							world.spawnObj(old)
						}
					}
					if (player.attribOr(AttributeKey.GRAIN_IN_HOPPER, false)) {
						player.message("You operate the hopper. The grain slides down the chute.")
						player.varps().varp(Varp.FLOUR_BIN, player.varps().varp(Varp.FLOUR_BIN) + 1)
						player.clearattrib(AttributeKey.GRAIN_IN_HOPPER)
					} else {
						player.message("You operate the empty hopper. Nothing interesting happens.")
					}
					it.delay(2)
					player.unlock()
				}
			}
		}
		
		for (HOPPER in HOPPERS) {
			r.onItemOnObject(HOPPER) @Suspendable {
				val item = it.itemUsedId()
				val player = it.player()
				
				if (item == GRAIN) {
					if (player.attribOr<Boolean>(AttributeKey.GRAIN_IN_HOPPER, false) == false) {
						if (player.inventory().remove(Item(GRAIN), false).success()) {
							player.lock()
							it.message("You put grain in the hopper.")
							it.animate(3572)
							it.sound(2567)
							player.putattrib(AttributeKey.GRAIN_IN_HOPPER, true)
							player.unlock()
						}
					} else {
						it.message("There is already grain in the hopper.")
					}
				} else {
					it.message("Nothing interesting happens.")
				}
			}
		}
		
		for (FLOUR_BIN in FLOUR_BINS) {
			r.onObject(FLOUR_BIN) @Suspendable {
				if (it.player().inventory().remove(Item(POT), false).success()) {
					it.player().lock()
					it.player().inventory().add(Item(POT_OF_FLOUR), true)
					it.player().varps().varp(Varp.FLOUR_BIN, it.player().varps().varp(Varp.FLOUR_BIN) - 1)
					it.message("You fill a pot with flour from the bin.")
					it.delay(1)
					it.player().unlock()
				} else {
					it.message("You need an empty pot to hold the flour in.")
				}
			}
		}
		
		for (FLOUR_BIN in FLOUR_BINS) {
			r.onItemOnObject(FLOUR_BIN) @Suspendable {
				val item = it.itemUsedId()
				if (item == POT) {
					if (it.player().inventory().remove(Item(POT), false).success()) {
						it.player().lock()
						it.player().inventory().add(Item(POT_OF_FLOUR), true)
						it.player().varps().varp(Varp.FLOUR_BIN, it.player().varps().varp(Varp.FLOUR_BIN) - 1)
						it.message("You fill a pot with flour from the bin.")
						it.delay(1)
						it.player().unlock()
					} else {
						it.message("You need an empty pot to hold the flour in.")
					}
				} else {
					it.message("Nothing interesting happens.")
				}
			}
		}
	}
}