package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 12/05/2016.
 */
object DragonEquipmentOrnamentKits {
	
	// raw items
	val SQSHIELD = 1187
	val dfull: Int = 11335
	val dchain: Int = 3140
	val legs = 4087
	val skirt = 4585
	
	// 4 kits
	val dsq_kit: Int = 12532
	val dchain_kit = 12534
	val legskirt_kit = 12536
	val dfull_kit = 12538
	
	// 5 trimmed items (legs and skirt share a kit, sexism yakno)
	val dchain_g: Int = 12414
	val dlegs_g: Int = 12415
	val dskirt_g: Int = 12416
	val dfullhelm_g: Int = 12417
	val dsq_gold: Int = 12418
	
	val raw = intArrayOf(SQSHIELD, dfull, dchain, legs, skirt)
	val kits = intArrayOf(dsq_kit, dfull_kit, dchain_kit, legskirt_kit, legskirt_kit)
	val results = intArrayOf(dsq_gold, dfullhelm_g, dchain_g, dlegs_g, dskirt_g)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (i in 0..raw.size - 1) {
			repo.onItemOnItem(raw[i], kits[i]) @Suspendable {
				val player = it.player()
				if (player.inventory().hasAll(raw[i], kits[i])) {
					if (player.inventory().remove(Item(raw[i]), false).success() && player.inventory().remove(Item(kits[i]), false).success()) {
						player.inventory()!!.add(Item(results[i]), false)
						val name: String = player.world().definitions().get(ItemDefinition::class.java, raw[i])!!.name
						it.itemBox("You combine the ornament kit with the $name.", results[i])
					}
				}
			}
		}
		
		for (i in 0..raw.size - 1) {
			repo.onItemOption5(results[i]) @Suspendable {
				val player = it.player()
				if (player.inventory().freeSlots() > 1) {
					if (player.inventory().remove(Item(results[i]), false).success()) {
						player.inventory().add(Item(raw[i]), false)
						player.inventory().add(Item(kits[i]), false)
						val name: String = player.world().definitions().get(ItemDefinition::class.java, results[i])!!.name
						it.doubleItemBox("You dismantle the $name to retrieve the umtrimmed version and the kit.", raw[i], kits[i])
					}
				}
			}
		}
	}
}