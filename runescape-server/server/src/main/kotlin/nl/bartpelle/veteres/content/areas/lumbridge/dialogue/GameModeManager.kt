package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/13/2015.
 */

object GameModeManager {
	
	val MANAGER = 5523
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		/*r.onNpcOption1(MANAGER, s@ @Suspendable {
			if (it.player().world().realm().isOSRune) {
				it.chatNpc("A good day, sir.", MANAGER, 590)
				it.messagebox("He doesn't seem too interested to chat.")
				return@s
			}
			
			it.chatNpc("Hello there, adventurer. How can I help you today?", MANAGER, 590)
			when (it.options("Who are you?", "What game mode am I currently playing?", "Can I change game modes?", "What game modes are there?")) {
				1 -> {
					it.chatPlayer("Who are you?")
					it.chatNpc("I'm here to help players like you change your game mode! Is that something you're interested in?", MANAGER, 590)
					when (it.options("Yes please!", "No thanks.", "What game modes are there?")) {
						1 -> {
							it.chatPlayer("Yes please!")
							getOptions(it)
						}
						2 -> {
							it.chatPlayer("No thanks.")
							it.chatNpc("Okay, let me know if you change your mind!", MANAGER, 590)
						}
						3 -> {
							when (it.options("Tell me about Laid-Back mode.", "Tell me how Classic mode works.", "What is included in Realism?", "<img=2>What exactly is Iron Man mode?")) {
								1 -> {
									it.chatPlayer("Can you tell me what Laid-Back mode offers?")
									it.chatNpc("Ah, yes! This game mode is invented to offer those that are not too eager on training a quick " +
											"experience. They are granted <col=ff0000>1,000x combat</col> experience rates, and <br><col=ff0000>20x skill</col> experience rates.", 3308)
									it.chatPlayer("Are there any penalties or downsides to this?")
									it.chatNpc("Yes, there are indeed. To counter the quick rate of training, it is most fair to give these a <col=ff0000>lower drop rate</col>. This applies to all monsters, and especially to rare drops.", 3308)
									it.chatPlayer("Oh interesting.. thank you.")
								}
								2 -> {
									it.chatPlayer("Could you explain Classic mode to me?")
									it.chatNpc("Very well then, ${it.player().name()}. This is the classic experience most people seek in a private server. It offers you <col=ff0000>250x combat</col> and <col=ff0000>20x skilling</col> experience rates.", 3308)
									it.chatNpc("On top of that, one can always switch back to Laid-Back mode should he not like this mode. Note that you cannot go the other way around!", 3308)
									it.chatPlayer("Are there any negative effects of picking this mode?")
									it.chatNpc("No, there are none. However, you do not get any benefits either as opposed to the Hardened and Realism modes. They might be worth looking into!", 3308)
									it.chatPlayer("Ahh okay, cool, thanks!")
								}
								3 -> {
									it.chatPlayer("If I am looking for a really tough go, does Realism<br>suit me well?")
									it.chatNpc("Correct as can be. Realism is for only those not afraid to spend hours and hours to achieve their goals. It's tricky, and both <col=ff0000>combat and skilling</col> experience rates are stuck at <col=ff0000>10x</col>.", 3308)
									it.chatPlayer("Sounds like the most challenging out there. I assume this is being rewarded?")
									it.chatNpc("You will have a <col=ff0000>higher drop rare</col> and your <col=ff0000>prayer drains much slower in PvM areas</col>.", 3308)
									it.chatNpc("On top of that, Bob in Lumbridge repairs your Barrows items for a highly reduced price depending in your combat level. You also are charged reduced kill count when entering God Wars boss chambers.", 3308)
									it.chatNpc("You are also given a <col=ff0000>higher chance at good rewards when doing clue scrolls</col>. Monsters will also drop <col=ff0000>caskets more commonly</col=ff0000>.", 3308)
									it.chatNpc("On top of this, because your skilling is slightly tougher, you have <col=ff0000>an increased chance to get skilling pets</col> from skilling activities!", 3308)
									it.chatNpc("Should you end up not liking this mode, we give you the option to change back to either Classic or Laid-Back any time. You cannot go the other way!", 3308)
									it.chatPlayer("Oh interesting.. thank you.")
								}
								4 -> {
									it.chatPlayer("What is this Iron Man mode people speak of?")
									it.chatNpc("Iron Man mode is a mode designed for people who want to go on their own. You <col=ff0000>cannot trade</col>. Items dropped by other players <col=ff0000>cannot be picked up</col>. You cannot make use of over/understocked shops.", 3308)
									it.chatNpc("You also do not gain experience in PvP fights and the Falador Party Room is none of your concern anymore. If you like the real restriction, this is your mode.", 3308)
									it.chatPlayer("Oh interesting.. that sounds challenging. Thanks!")
								}
							}
						}
					}
				}
				2 -> {
					it.chatPlayer("What game mode am I currently playing?")
					it.chatNpc("You are currently playing: ${modeName(it)}.", MANAGER, 590)
					it.chatPlayer("Oh, okay! Awesome, thanks!")
				}
				3 -> {
					it.chatPlayer("Can I change game modes?")
					getOptions(it)
				}
				4 -> {
					it.chatPlayer("What game modes are there?")
					it.chatNpc("Well, there are quite a few. Which one would you like to know more about?", MANAGER, 590)
					when (it.options("Tell me about Laid-Back mode.", "Tell me how Classic mode works.", "What is included in Realism?", "<img=2>What exactly is Iron Man mode?")) {
						1 -> {
							it.chatPlayer("Can you tell me what Laid-Back mode offers?")
							it.chatNpc("Ah, yes! This game mode is invented to offer those that are not too eager on training a quick " +
									"experience. They are granted <col=ff0000>1,000x combat</col> experience rates, and <br><col=ff0000>20x skill</col> experience rates.", 3308)
							it.chatPlayer("Are there any penalties or downsides to this?")
							it.chatNpc("Yes, there are indeed. To counter the quick rate of training, it is most fair to give these a <col=ff0000>lower drop rate</col>. This applies to all monsters, and especially to rare drops.", 3308)
							it.chatPlayer("Oh interesting.. thank you.")
						}
						2 -> {
							it.chatPlayer("Could you explain Classic mode to me?")
							it.chatNpc("Very well then, ${it.player().name()}. This is the classic experience most people seek in a private server. It offers you <col=ff0000>250x combat</col> and <col=ff0000>20x skilling</col> experience rates.", 3308)
							it.chatNpc("On top of that, one can always switch back to Laid-Back mode should he not like this mode. Note that you cannot go the other way around!", 3308)
							it.chatPlayer("Are there any negative effects of picking this mode?")
							it.chatNpc("No, there are none. However, you do not get any benefits either as opposed to the Hardened and Realism modes. They might be worth looking into!", 3308)
							it.chatPlayer("Ahh okay, cool, thanks!")
						}
						3 -> {
							it.chatPlayer("If I am looking for a really tough go, does Realism<br>suit me well?")
							it.chatNpc("Correct as can be. Realism is for only those not afraid to spend hours and hours to achieve their goals. It's tricky, and both <col=ff0000>combat and skilling</col> experience rates are stuck at <col=ff0000>10x</col>.", 3308)
							it.chatPlayer("Sounds like the most challenging out there. I assume this is being rewarded?")
							it.chatNpc("You will have a <col=ff0000>higher drop rare</col> and your <col=ff0000>prayer drains much slower in PvM areas</col>.", 3308)
							it.chatNpc("On top of that, Bob in Lumbridge repairs your Barrows items for a highly reduced price depending in your combat level. You also are not charged any kill count when entering God Wars boss chambers.", 3308)
							it.chatNpc("You are also given a <col=ff0000>higher chance at good rewards when doing clue scrolls</col>. Monsters will also drop <col=ff0000>caskets more commonly</col=ff0000>.", 3308)
							it.chatNpc("On top of this, because your skilling is slightly tougher, you have <col=ff0000>an increased chance to get skilling pets</col> from skilling activities!", 3308)
							it.chatNpc("Should you end up not liking this mode, we give you the option to change back to either Classic or Laid-Back any time. You cannot go the other way!", 3308)
							it.chatPlayer("Oh interesting.. thank you.")
						}
						4 -> {
							it.chatPlayer("What is this Iron Man mode people speak of?")
							it.chatNpc("Iron Man mode is a mode designed for people who want to go on their own. You <col=ff0000>cannot trade</col>. Items dropped by other players <col=ff0000>cannot be picked up</col>. You cannot make use of over/understocked shops.", 3308)
							it.chatNpc("You also do not gain experience in PvP fights and the Falador Party Room is none of your concern anymore. If you like the real restriction, this is your mode.", 3308)
							it.chatPlayer("Oh interesting.. that sounds challenging. Thanks!")
						}
					}
				}
			}
		})
		
		r.onNpcOption2(MANAGER, s@ @Suspendable {
			if (it.player().world().realm().isOSRune) {
				it.chatNpc("A good day, sir.", MANAGER, 590)
				it.messagebox("He doesn't seem too interested to chat.")
				return@s
			}
			
			getOptions(it)
		})*/
	}
	
	@Suspendable fun getOptions(it: Script) {
		if (it.player().ironMode() != IronMode.NONE) {
			it.chatNpc("Absolutely! That's what I'm here for. Here are your options, ${it.player().name()}.. just keep in mind that if you do change your game mode <col=ff0000>your iron man will be removed</col>!", MANAGER, 590)
			ironmanOptions(it)
			return
		}
		
		if (it.player().mode() == GameMode.LAID_BACK) {
			it.chatNpc("You are currently playing the easiest game mode available! Unfortunately, if you'd like to play a harder game mode you'll have to create a new account.", MANAGER, 590)
			it.chatPlayer("Oh, okay! Thanks anyways.")
		} else if (it.player().mode() == GameMode.CLASSIC) {
			it.chatNpc("Absolutely! That's what I'm here for. Here are your options, ${it.player().name()}..", MANAGER, 590)
			classicOptions(it)
		} else if (it.player().mode() == GameMode.REALISM) {
			it.chatNpc("Absolutely! That's what I'm here for. Here are your options, ${it.player().name()}..", MANAGER, 590)
			realismOptions(it)
		}
	}
	
	@Suspendable fun classicOptions(it: Script) {
		when (it.options("Laid Back")) {
			1 -> {
				if (it.customOptions("Are you sure you wish to do this?", "Yes, change my game mode to laid back.", "No, I'll stay with my current game mode.") == 1) {
					it.player().mode(GameMode.LAID_BACK)
					it.message("You've changed your game mode to: ${modeName(it)}.")
				}
			}
		}
	}
	
	
	@Suspendable fun realismOptions(it: Script) {
		when (it.options("Laid Back", "Classic")) {
			1 -> {
				if (it.customOptions("Are you sure you wish to do this?", "Yes, change my game mode to laid back.", "No, I'll stay with my current game mode.") == 1) {
					it.player().mode(GameMode.LAID_BACK)
					it.message("You've changed your game mode to: ${modeName(it)}.")
				}
			}
			2 -> {
				if (it.customOptions("Are you sure you wish to do this?", "Yes, change my game mode to classic.", "No, I'll stay with my current game mode.") == 1) {
					it.player().mode(GameMode.CLASSIC)
					it.message("You've changed your game mode to: ${modeName(it)}.")
				}
			}
		}
	}
	
	@Suspendable fun ironmanOptions(it: Script) {
		when (it.options("Laid Back", "Classic")) {
			1 -> {
				if (it.customOptions("Are you sure you wish to do this?", "Yes, change my game mode to laid back.", "No, I'll stay with my current game mode.") == 1) {
					it.player().ironMode(IronMode.NONE)
					it.player().mode(GameMode.LAID_BACK)
					it.message("You've changed your game mode to: ${modeName(it)}, as a result, your ironman has been removed.")
				}
			}
			2 -> {
				if (it.customOptions("Are you sure you wish to do this?", "Yes, change my game mode to classic.", "No, I'll stay with my current game mode.") == 1) {
					it.player().ironMode(IronMode.NONE)
					it.player().mode(GameMode.CLASSIC)
					it.message("You've changed your game mode to: ${modeName(it)}, as a result, your ironman has been removed.")
				}
			}
		}
	}
	
	@Suspendable fun modeName(it: Script): String {
		if (it.player().ironMode() != IronMode.NONE) {
			return "ironman"
		}
		if (it.player().mode() == GameMode.LAID_BACK) {
			return "laid back"
		} else if (it.player().mode() == GameMode.CLASSIC) {
			return "classic"
		} else if (it.player().mode() == GameMode.REALISM) {
			return "realism"
		}
		return "unknown"
	}
	
}
