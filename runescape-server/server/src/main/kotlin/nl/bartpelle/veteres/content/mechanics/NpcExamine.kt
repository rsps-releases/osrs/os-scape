package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.npc.NpcCombatInfo

/**
 * Created by Mack on 11/10/2017.
 */
object NpcExamine {
	
	const val NAME = 3
	const val INTERFACE = 522
	const val STATS = 20
	const val OFFENSIVE_STATS = 22
	const val DEFENSIVE_STATS = 24
	const val OTHER_STATS = 26
	
	@Suspendable fun lookup(script: Script, search: String) {
		val player = script.player()
		var npc: Npc? = null
		
		for (id in 0..player.world().definitions().total(NpcDefinition::class.java)) {
			if (player.world().definitions().get(NpcDefinition::class.java, id) == null)
				continue
			if (search.equals(player.world().definitions().get(NpcDefinition::class.java, id).name, true)) {
				npc = Npc(id, player.world(), player.tile())
				break
			}
		}
		
		if (npc == null) {
			script.chatNpc("Odd... That name doesn't bring up any memory of encountering that monster. Are you sure you've<br> spelled it correctly?", 3306)
			errorOptions(script)
			return
		}
		val monster = npc
		
		player.interfaces().text(INTERFACE, NAME, npc.def().name)
		displayStats(player, monster)
		displayOffensiveStats(player, monster)
		displayDefensiveStats(player, monster)
		displayOtherAttributes(player, monster)
		player.interfaces().sendInventory(INTERFACE)
	}
	
	@Suspendable fun errorOptions(script: Script) {
		when (script.options("Let me try to look up another.", "Nevermind.")) {
			1 -> {
				lookup(script, script.inputString("Enter NPC name:"))
			}
			2 -> {
				script.chatPlayer("Nevermind.")
			}
		}
	}
	
	fun displayStats(player: Player, npc: Npc) {
		val combatInfo: NpcCombatInfo? = npc.combatInfo()
		val lvl = npc.def().combatlevel
		val hp = combatInfo?.stats?.hitpoints ?: 0
		val atk = combatInfo?.stats?.attack ?: 0
		val def = combatInfo?.stats?.defence ?: 0
		val str = combatInfo?.stats?.strength ?: 0
		val mage = combatInfo?.stats?.magic ?: 0
		val range = combatInfo?.stats?.ranged ?: 0
		val max = combatInfo?.maxhit ?: 0
		
		player.interfaces().text(INTERFACE, STATS, "Combat level: $lvl<br>Hitpoints: $hp<br>Attack: $atk<br>Defence: $def<br>Strength: $str<br>Magic: $mage<br>Range: $range<br>Max standard hit: $max")
	}
	
	fun displayOffensiveStats(player: Player, npc: Npc) {
		val speed = npc.combatInfo()?.attackspeed ?: 0
		val atkBonus = npc.combatInfo()?.bonuses?.attack ?: 0
		val strBonus = npc.combatInfo()?.bonuses?.strength ?: 0
		val rangeBonus = npc.combatInfo()?.bonuses?.ranged ?: 0
		val mageBonus = npc.combatInfo()?.bonuses?.magic ?: 0
		
		player.interfaces().text(INTERFACE, OFFENSIVE_STATS, "Attack speed: $speed<br>Attack bonus: $atkBonus<br>Strength bonus: $strBonus<br>Range bonus: $rangeBonus<br>Magic bonus: $mageBonus")
	}
	
	fun displayDefensiveStats(player: Player, npc: Npc) {
		val stabDef = npc.combatInfo()?.bonuses?.stabdefence ?: 0
		val slashDef = npc.combatInfo()?.bonuses?.slashdefence ?: 0
		val crushDef = npc.combatInfo()?.bonuses?.crushdefence ?: 0
		val mageDef = npc.combatInfo()?.bonuses?.magicdefence ?: 0
		val rangeDef = npc.combatInfo()?.bonuses?.rangeddefence ?: 0
		
		player.interfaces().text(INTERFACE, DEFENSIVE_STATS, "Stab bonus: $stabDef<br>Slash bonus: $slashDef<br>Crush bonus: $crushDef<br>Magic bonus: $mageDef<br>Range bonus: $rangeDef")
	}
	
	fun displayOtherAttributes(player: Player, npc: Npc) {
		val slayerLvl = npc.combatInfo()?.slayerlvl ?: 0
		val slayerXp = npc.combatInfo()?.slayerxp ?: 0
		val aggressive = npc.combatInfo()?.aggressive ?: false
		val slayerAssignable = if (slayerLvl > 0) "Yes" else "No"
		val isAggressive = if (aggressive) "Yes" else "No"
		val poisonous = npc.combatInfo()?.poisonous() ?: false
		val isPoisonous = if (poisonous) "Yes" else "No"
		
		player.interfaces().text(INTERFACE, OTHER_STATS, "Slayer task assignable: $slayerAssignable<br>Slayer level required: $slayerLvl<br>Slayer exp reward: $slayerXp<br>Agrressive: $isAggressive<br>Poisonous: $isPoisonous")
	}
}