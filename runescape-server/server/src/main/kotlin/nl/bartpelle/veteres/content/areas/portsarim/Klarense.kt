package nl.bartpelle.veteres.content.areas.portsarim

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 17/10/2016.
 */
object Klarense {
	
	val KLARENSE = 819
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(KLARENSE) @Suspendable {
			// Post dragon slayer quest chat
			it.chatPlayer("Hey, that's my ship!")
			it.chatNpc("No, it's not. It may, to the untrained eye, at first appear to be the Lady Lumbridge, but it is definitely not. It's my new ship. It just happens to look slightly similar, is all.", KLARENSE)
			it.chatPlayer("It has the Lady Lumbridge painted out and 'Klarense's Cruiser' painted over it!")
			it.chatNpc("Nope, you're mistaken. It's my new boat.", KLARENSE)
			it.chatPlayer("I'm going to complain to the staff!")
			it.chatNpc("So you think Tardis Fan will magically appear out of his blue box? I don't think so matey. I'll be seeing you. Bonvoyage.", KLARENSE)
		}
	}
}