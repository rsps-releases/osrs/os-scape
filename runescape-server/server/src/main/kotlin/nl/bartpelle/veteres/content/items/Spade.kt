package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 11/27/2015.
 */
object Spade {
	
	@Suspendable fun crypt(it: Script, t: Tile) {
		it.message("You've broken into a crypt!")
		it.player().lock()
		it.delay(1)
		it.player().teleport(t)
		it.player().unlock()
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(952) @Suspendable {
			it.player().animate(830)
			
			// Barrows my nigger
			if (it.player().tile().distance(Tile(3575, 3297)) < 4) { // Dharok
				crypt(it, Tile(3556, 9718, 3))
			} else if (it.player().tile().distance(Tile(3557, 3298)) < 4) { // Verac
				crypt(it, Tile(3578, 9706, 3))
			} else if (it.player().tile().distance(Tile(3565, 3289)) < 4) { // Ahrim
				crypt(it, Tile(3557, 9703, 3))
			} else if (it.player().tile().distance(Tile(3577, 3282)) < 4) { // Guthan
				crypt(it, Tile(3534, 9704, 3))
			} else if (it.player().tile().distance(Tile(3566, 3276)) < 4) { // Karil
				crypt(it, Tile(3546, 9684, 3))
			} else if (it.player().tile().distance(Tile(3554, 3283)) < 4) { // Torag
				crypt(it, Tile(3568, 9683, 3))
			} else {
				it.message("Nothing interesting happens.")
			}
		}
	}
	
}