package nl.bartpelle.veteres.content.areas.dungeons.taverly.cerberus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 3/7/2016.
 */

object IronWinch {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(23104) @Suspendable {
			val objectTile = it.interactionObject().tile()
			
			val destination = when (objectTile) {
				Tile(1291, 1254) -> Tile(1240, 1226) //West
				Tile(1328, 1254) -> Tile(1368, 1226) //East
				Tile(1307, 1269) -> Tile(1304, 1290) //North
				else -> Tile(0, 0)
			}
			
			val region = when (objectTile) {
				Tile(1291, 1254) -> 4883 //West
				Tile(1328, 1254) -> 5395 //East
				Tile(1307, 1269) -> 5140 //North
				else -> 0
			}
			
			if (it.interactionOption() == 1)
				teleport_player(it, destination)
			else
				peek(it, region)
		}
	}
	
	@Suspendable fun teleport_player(it: Script, tile: Tile) {
		it.player().lock()
		it.delay(2)
		it.player().animate(4506)
		it.delay(2)
		it.player().interfaces().sendWidgetOn(174, Interfaces.InterSwitches.C)
		it.player().invokeScript(951)
		it.delay(2)
		it.player().teleport(tile)
		it.delay(2)
		it.player().invokeScript(948, 0, 0, 0, 255, 50)
		it.player().interfaces().sendWidgetOn(246, Interfaces.InterSwitches.X)
		it.delay(2)
		it.player().unlock()
		it.player().interfaces().closeById(174) // Guess work added (31/8/16) unsure if correct
		it.player().interfaces().closeById(246)
	}
	
	@Suspendable fun peek(it: Script, region: Int) {
		val world = it.player().world()
		val count = LinkedList<Player>()
		
		world.players().forEachKt({ p ->
			if (p.tile().region() == region)
				count += p
		})
		
		if (count.size == 0)
			it.chatNpc("No adventurers are inside the cave.", 5870, 588)
		else
			it.chatNpc("${count.size} adventurer is inside the cave.", 5870, 588)
		
	}
}