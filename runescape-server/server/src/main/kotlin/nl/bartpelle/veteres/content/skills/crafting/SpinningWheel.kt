package nl.bartpelle.veteres.content.skills.crafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/28/2015.
 */

object SpinningWheel {
	
	enum class Items(val before: Int, val after: Int, val lvl: Int, val exp: Double) {
		WOOL(1737, 1759, 1, 2.5),
		FLAX(1779, 1777, 10, 15.0),
		SINEW(9436, 9438, 10, 15.0),
		MAGIC_ROOTS(6051, 6038, 19, 30.0),
		TREE_ROOTS(6049, 6047, 19, 30.0),
		YAK_HAIR(10814, 954, 30, 25.0);
		
		companion object {
			fun get(before: Int): Items? {
				Items.values().forEach { if (it.before == before) return it }
				return null
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		intArrayOf(7132, 25824, 14889).forEach { wheel ->
			r.onObject(wheel) @Suspendable {
				it.player().interfaces().sendMain(459)
			}
			r.onItemOnObject(wheel) @Suspendable {
				val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
				val used = Items.get(item)
				
				if (used != null) {
					if (used.lvl <= it.player().skills().level(Skills.CRAFTING)) {
						spin(it, used.before, used.after, used.exp, 1)
					} else {
						it.message("You need a Crafting level of ${used.lvl} to craft this.")
					}
				}
			}
		}
		
		//Ball of wool - wool 1, 5, 10, x
		r.onButton(459, 100) @Suspendable { spin(it, 1737, 1759, 2.5, 1) }
		r.onButton(459, 99) @Suspendable { spin(it, 1737, 1759, 2.5, 5) }
		r.onButton(459, 98) @Suspendable { spin(it, 1737, 1759, 2.5, 10) }
		r.onButton(459, 97) @Suspendable { spin(it, 1737, 1759, 2.5, amt = it.inputInteger("Enter amount:")) }
		
		//Bow string - flax 1, 5, 10, x
		r.onButton(459, 95) @Suspendable { spin(it, 1779, 1777, 15.0, 1) }
		r.onButton(459, 94) @Suspendable { spin(it, 1779, 1777, 15.0, 5) }
		r.onButton(459, 93) @Suspendable { spin(it, 1779, 1777, 15.0, 10) }
		r.onButton(459, 92) @Suspendable { spin(it, 1779, 1777, 15.0, amt = it.inputInteger("Enter amount:")) }
		
		//Magic amulet string - magic roots 1, 5, 10, x
		r.onButton(459, 107) @Suspendable { spin(it, 6051, 6038, 30.0, 1) }
		r.onButton(459, 106) @Suspendable { spin(it, 6051, 6038, 30.0, 5) }
		r.onButton(459, 105) @Suspendable { spin(it, 6051, 6038, 30.0, 10) }
		r.onButton(459, 104) @Suspendable { spin(it, 6051, 6038, 30.0, amt = it.inputInteger("Enter amount:")) }
		
		//C'bow string - tree roots 1, 5, 10, x
		r.onButton(459, 121) @Suspendable { spin(it, 6049, 6047, 30.0, 1) }
		r.onButton(459, 120) @Suspendable { spin(it, 6049, 6047, 30.0, 5) }
		r.onButton(459, 119) @Suspendable { spin(it, 6049, 6047, 30.0, 10) }
		r.onButton(459, 118) @Suspendable { spin(it, 6049, 6047, 30.0, amt = it.inputInteger("Enter amount:")) }
		
		//C'bow string - sinew 1, 5, 10, x
		r.onButton(459, 114) @Suspendable { spin(it, 9436, 9438, 15.0, 1) }
		r.onButton(459, 113) @Suspendable { spin(it, 9436, 9438, 15.0, 5) }
		r.onButton(459, 112) @Suspendable { spin(it, 9436, 9438, 15.0, 10) }
		r.onButton(459, 111) @Suspendable { spin(it, 9436, 9438, 15.0, amt = it.inputInteger("Enter amount:")) }
		
		//Rope - yak hair 1, 5, 10, x
		r.onButton(459, 128) @Suspendable { spin(it, 10814, 954, 25.0, 1) }
		r.onButton(459, 127) @Suspendable { spin(it, 10814, 954, 25.0, 5) }
		r.onButton(459, 126) @Suspendable { spin(it, 10814, 954, 25.0, 10) }
		r.onButton(459, 125) @Suspendable { spin(it, 10814, 954, 25.0, amt = it.inputInteger("Enter amount:")) }
	}
	
	@Suspendable fun spin(it: Script, before: Int, after: Int, exp: Double, amt: Int) {
		it.player().stopActions(true)
		it.delay(1)
		
		var amt = amt
		var Item = Item(before)
		val name = Item.definition(it.player().world()).name.toLowerCase()
		it.player().interfaces().closeById(459)
		if (!it.player().inventory().contains(Item(before, 1))) {
			it.message("You'll need ${name} to make that.")
			return
		}
		
		while (amt-- > 0) {
			if (it.player().inventory().remove(Item(before), true).failed()) {
				return
			}
			it.player().animate(894)
			it.player().inventory().add(Item(after), true)
			it.addXp(Skills.CRAFTING, exp)
			it.delay(2)
		}
	}
}