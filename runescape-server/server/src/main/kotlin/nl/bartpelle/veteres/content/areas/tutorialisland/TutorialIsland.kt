package nl.bartpelle.veteres.content.areas.tutorialisland

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 10/18/2015.
 */

object TutorialIsland {
	
	operator fun contains(player: Player) = player.tile().region() == 12336 || player.tile().region() == 12592
	
	@ScriptMain @JvmStatic fun registerTutorialIsland(repo: ScriptRepository) {
		/*repo.onLogin {
			if (it.player().tutorialStage() < 100) {
				TutorialIsland.updateState(it.player())
				it.player().interfaces().send(372, 162, 545, true)
				it.player().interfaces().sendMain(269)
			}
		}*/
	}
	
	fun updateState(player: Player) {
		if (player.tutorialStage() == 0) {
			player.writeState("Getting started",
					"To start the tutorial use your left mouse button to click on the",
					"OS-Scape Guide in this room. He is indicated by a flashing",
					"yellow arrow above his head. If you can't see him, use your",
					"keyboard's arrow keys to rotate the view.")
		}
	}
	
	fun Player.writeState(vararg messages: String) {
		var ptr = 0
		for (msg in messages) {
			write(InterfaceText(372, ptr++, msg))
		}
	}
	
	fun Player.tutorialStage(): Int {
		return varps()[Varp.TUTORIAL_STAGE]
	}
}