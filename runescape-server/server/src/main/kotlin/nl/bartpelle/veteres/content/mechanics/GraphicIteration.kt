package nl.bartpelle.veteres.content.mechanics;

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.Entity

class GraphicIteration(entity: Entity, start: Int, iterations: Int) {
	
	val script: Function1<Script, Unit> = suspend@ @Suspendable {
		var graphicToAnimate = start;
		
		while (graphicToAnimate < start + iterations) {
			entity.message("Animating the graphic #$graphicToAnimate.")
			entity.graphic(graphicToAnimate)
			graphicToAnimate++
			it.delay(2)
		}
	}
}