package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Mack on 10/23/2017.
 */
object Transmogrify {

	private const val ENABLED = false

	enum class TransmogItems(id: Int, transmogId: Int, walkable: Boolean) {
		NONE(-1, -1, true),
		BEDSHEET(4285, 3009, true),
		
		;//end
		
		private val item = id
		private val toId = transmogId
		private val mobile = walkable
		
		fun id(): Int {
			return item
		}
		
		fun transmogId(): Int {
			return toId
		}
		
		fun mobile(): Boolean {
			return mobile
		}
	}
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		Arrays.stream(TransmogItems.values()).forEach({ item ->
			sr.onItemEquip(item.id(), script@ {
				if (!canTransmogrify(it.player())) {
					it.message("This doesn't seem like a good time to do this.")
					return@script
				}
				if (isTransmogrified(it.player())) {
					it.message("This doesn't seem like a good idea while I'm like this.")
					return@script
				}
				transmog(it.player(), item)
			})
			sr.onLogin {
				if (it.player().equipment().has(item.id())) {
					transmog(it.player(), item)
				}
			}
			sr.onItemUnequip(item.id(), {
				if (isTransmogrified(it.player())) {
					untransmog(it.player(), item)
				}
			})
		})
	}
	
	/**
	 * A flag to check if the player is currently transmog'd.
	 */
	@JvmStatic fun isTransmogrified(player: Player): Boolean {
		return (player.looks().trans() > -1)
	}
	
	/**
	 * Preconditions to be met prior to allowing the player to transmogrify.
	 */
	@JvmStatic fun canTransmogrify(player: Player): Boolean {
        if (!ENABLED) {
            return false
        }
		if (WildernessLevelIndicator.inAttackableArea(player)) {
			return false
		}
		if (Staking.in_duel(player)) {
			return false
		}
		
		return true
	}
	
	/**
	 * The main function to transmogrifying the player.
	 */
	fun transmog(player: Player, data: TransmogItems) {
        if (!ENABLED) {
            return
        }
		if (!data.mobile()) {
			player.stopActions(true)
		}
		
		player.looks().transmog(data.transmogId())
		player.looks().renderData(player.world().definitions().get(NpcDefinition::class.java, data.transmogId()).renderpairs())
	}
	
	/**
	 * Hard resets the player's render under certain circumstance such as being morphed in a area
	 * they shouldn't or dying whilst morphed.
	 */
	@JvmStatic fun hardReset(player: Player) {
		val transmogType: Optional<TransmogItems> = Arrays.stream(TransmogItems.values()).filter({ type -> player.equipment().has(type.id())}).findAny()
		
		if (transmogType.isPresent) {
			untransmog(player, transmogType.get())
		}
	}
	
	/**
	 * The main function to untransmogrifying the player.
	 */
	fun untransmog(player: Player, data: TransmogItems?) {
		val transmog = data ?: TransmogItems.NONE
		
		if (transmog.mobile()) {
			player.unlock()
		}
		
		player.looks().transmog(-1)
		player.looks().resetRender()
		player.message("You return to your human-like state.")
	}
}