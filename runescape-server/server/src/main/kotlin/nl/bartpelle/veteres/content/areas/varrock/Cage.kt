package nl.bartpelle.veteres.content.areas.varrock

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/9/2016.
 */

object Cage {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(20873) @Suspendable { it.message("You can't unlock the pillory, you'll let all the criminals out!") }
	}
}
