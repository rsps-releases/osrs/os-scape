package nl.bartpelle.veteres.content.areas.falador

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.quests.free.DoricsQuest
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 4/6/2016.
 */

object Doric {
	
	val DORIC = 3893
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(DORIC) @Suspendable {
			when (DoricsQuest.stage(it.player())) {
				0 -> regularDialogue(it)
				10 -> questDialogue(it)
				100 -> postQuestDialogue(it)
			}
		}
	}
	
	@Suspendable fun regularDialogue(it: Script) {
		it.chatNpc("Hello traveller, what brings you to my humble smithy?", DORIC, 554)
		when (it.options("I wanted to use your anvils.", "I want to use your whetstone.",
				"Mind your own business, shortstuff!", "I was just checking out the landscape.", "What do you make here?")) {
			1 -> {
				it.chatPlayer("I wanted to use your anvils.", 588)
				it.chatNpc("My anvils get enough work with my own use. I make<br>pickaxes, and it takes a lot of hard work. If you could<br>get me some more materials, then I could let you use<br>them.", DORIC, 591)
				questStartChatter(it)
			}
			2 -> {
				it.chatPlayer("I wanted to use your whetstone.", 588)
				it.chatNpc("The whetstone is for more advanced smithing, but I<br>could let you use it as well as my anvils if you could<br>get me some more materials.", DORIC, 590)
				questStartChatter(it)
			}
			3 -> {
				it.chatPlayer("Mind your own business, shortstuff!", 614)
				it.chatNpc("How nice to meet someone with such pleasant manners.<br>Do come again when you need to shout at someone<br>smaller than you!", DORIC, 616)
			}
			4 -> {
				it.chatPlayer("I was just checking out the landscape.", 588)
				it.chatNpc("Hope you like it. I do enjoy the solitude of my little<br>home. If you get time, please say hi to my friends in<br>the Dwarven Mine.", DORIC, 569)
				
				when (it.options("Dwarven Mine?", "Will do!")) {
					1 -> {
						it.chatPlayer("Dwarven Mine?", 554)
						it.chatNpc("Yep, the entrance is in the side of Ice Mountain just to<br>the east of here. They're a friendly bunch. Stop in at<br>Nurmof's store and buy one of my pickaxes!", DORIC, 569)
					}
					2 -> {
						it.chatPlayer("Will do!", 567)
					}
				}
			}
			5 -> {
				it.chatPlayer("What do you make here?", 554)
				it.chatNpc("I make pickaxes. I am the best maker of pickaxes in the<br>whole of RuneScape.", DORIC, 568)
				it.chatPlayer("Do you have any to sell?", 554)
				it.chatNpc("Sorry, but I've got a running order with Nurmof.", DORIC, 588)
				
				when (it.options("Who's Nurmof?", "Ah, fair enough.")) {
					1 -> {
						it.chatPlayer("Who's Nurmof?", 554)
						it.chatNpc("Nurmof has a store over in the Dwarven Mine. You<br>can find the entrance on the side of Ice Mountain to<br>the east of here.", DORIC, 569)
					}
					2 -> {
						it.chatPlayer("Ah, fair enough.", 588)
					}
				}
			}
		}
	}
	
	@Suspendable fun questDialogue(it: Script) {
		it.chatNpc("Have you got my materials yet, traveller?", DORIC, 554)
		
		if (DoricsQuest.hasOres(it.player())) {
			it.chatPlayer("I have everything you need.", 567)
			it.chatNpc("Many thanks. Pass them here, please. I can spare you<br>some coins for your trouble, and please use my anvils<br>any time you want.", DORIC, 569)
			it.itemBox("You hand the clay, copper, and iron to Doric.", 436, 400)
			DoricsQuest.takeOresAndComplete(it.player())
		} else {
			it.chatPlayer("Sorry, I don't have them all yet.", 610)
			it.chatNpc("Not to worry, stick at it. Remember, I need 6 clay, 4<br>copper ore, and 2 iron ore.", DORIC, 589)
			informOres(it)
		}
	}
	
	@Suspendable fun questStartChatter(it: Script) {
		when (it.options("Yes, I will get you the materials.", "No, hitting rocks is for the boring people, sorry.")) {
			1 -> {
				it.chatPlayer("Yes, I will get you the materials.", 567)
				it.chatNpc("Clay is what I use more than anything, to make casts.<br>Could you get me 6 clay, 4 copper ore, and 2 iron ore,<br>please? I could pay a little, and let you use my anvils.<br>Take this pickaxe with you just in case you need it.", DORIC, 591)
				it.player().inventory().add(Item(1265), true)
				DoricsQuest.stage(it.player(), 10)
				
				informOres(it)
			}
			
			2 -> {
				it.chatPlayer("No, hitting rocks is for the boring people, sorry.", 562)
				it.chatNpc("That is your choice. Nice to meet you anyway.", DORIC, 575)
			}
		}
	}
	
	@Suspendable fun informOres(it: Script) {
		when (it.options("Where can I find those?", "Certainly, I'll be right back!")) {
			1 -> {
				it.chatPlayer("Where can I find those?", 554)
				it.chatNpc("You'll be able to find all those ores in the rocks just<br>inside the Dwarven Mine. Head east from here and<br>you'll find the entrance in the side of Ice Mountain.", DORIC, 569)
				
				// Below level 15 you get "warned".
				if (it.player().skills().xpLevel(Skills.MINING) < 15) {
					it.chatPlayer("But I'm not a good enough miner to get iron ore.")
					it.chatNpc("Oh well, you could practice mining until you can. Can't beat a bit of mining - it's a useful skill. Failing that, you might be able to find a more experienced adventurer to buy the iron ore off.", DORIC, 569)
				}
			}
			2 -> {
				it.chatPlayer("Certainly, I'll be right back!", 567)
			}
		}
	}
	
	@Suspendable fun postQuestDialogue(it: Script) {
		it.chatNpc("Hello traveller, how is your metalworking coming along?", DORIC, 588)
		it.chatPlayer("Not too bad, Doric.", 588)
		it.chatNpc("Good, the love of metal is a thing close to my heart.", DORIC, 567)
	}
	
}