package nl.bartpelle.veteres.content.skills.agility

import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 9/1/2016.
 */

object UnlockAgilityPet {
	
	fun unlockGiantSquirrel(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.GIANT_SQUIRREL)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(Pet.GIANT_SQUIRREL.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.GIANT_SQUIRREL, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(Pet.GIANT_SQUIRREL.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.GIANT_SQUIRREL.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
}