package nl.bartpelle.veteres.content.areas.draynor

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.events.christmas.carolschristmas.CarolsChristmas
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-20.
 */

//Temp for christmas
object Diango {
	
	val DIANGO = 1308
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(DIANGO) @Suspendable {
			if (CarolsChristmas.getStage(it.player()) == 24) {
				it.chatPlayer("Can you help me? We need to save Christmas!", 554)
				it.chatNpc("That depends... what do you need? A dragon kite?", DIANGO, 588)
				it.chatPlayer("Not exactly. I kind of need you to hire a team of<br>workers from another Christmas Factory.", 589)
				it.chatNpc("WHAT? Why?", DIANGO, 571)
				it.chatPlayer("I ruined Christmas by putting a stop to their slave<br>labour camp, apparently.", 589)
				it.chatNpc("Well... I... In that case well done, I think. And it just<br>so happens I can help, " +
						"I'm short of staff right now so<br>I'll hire everyone I can get.", DIANGO, 573)
				CarolsChristmas.setStage(it.player(), 25)
				it.chatPlayer("That's great! I'll go and tell Holly the good news!", 567)
			} else {
				it.chatNpc("Howdy partner!", DIANGO, 588)
			}
		}
	}
	
}
