package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 08/11/2016.
 */
object GnomeGliders {
	
	enum class GliderSpots(val uid: Int, val endTile: Tile, val btnId: Int) {
		GNOME_TREE(1, Tile(2464, 3501, 3), 4),
		ICE_MOUNTAIN(2, Tile(2850, 3498), 7),
		DIGSITE(3, Tile(3321, 3430), 10),
		DESERT(4, Tile(3284, 3213), 13),
		SHIPYARD(5, Tile(2971, 2969), 16);
		// TODO MM2 and another spot unlocked by quests. Try intervis for components?
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (glider in 6088..6092) {
			repo.onNpcOption2(glider) {
				it.player().varps().varp(153, 0)
				it.player().interfaces().sendMain(138)
			}
		}
		for (spot in GliderSpots.values()) {
			repo.onButton(138, spot.btnId) {
				travel(it, spot)
			}
		}
	}
	
	@Suspendable fun travel(it: Script, targetSpot: GliderSpots) {
		if (it.player().tile().distance(targetSpot.endTile) < 10) {
			it.player().message("You're already there!")
			return
		}
		var fromSpot = GliderSpots.GNOME_TREE
		var dist = 100
		for (spot in GliderSpots.values()) {
			val len = it.player().tile().distance(spot.endTile)
			if (len < dist) {
				dist = len
				fromSpot = spot
			}
		}
		// Must be direct, not between.
		if (fromSpot.uid != GliderSpots.GNOME_TREE.uid && targetSpot.uid != GliderSpots.GNOME_TREE.uid) {
			it.player().message("You can only access other routes via the Gnome Stronghold.")
			it.player().debug("You're currently at: %s (%d)", fromSpot, fromSpot.uid)
			return
		}
		if (fromSpot.uid == GliderSpots.DIGSITE.uid) {
			it.player().message("You're you to travel on a smashed glider?")
			return
		}
		it.player().varps().varp(153, resolveVarpForTravelRoute(fromSpot, targetSpot))
		it.delay(4)
		it.player().teleport(targetSpot.endTile)
		it.player().interfaces().closeMain()
		it.player().varps().varp(153, 0)
	}
	
	/**
	 * Note: when you arrive at the Digsite, it's crashed. You can't travel back.
	 * Gnome tree -> Feldip hills = 10 .. havent found the btn yet .. Feldip hills -> tree is 11
	 *
	 */
	@JvmStatic fun resolveVarpForTravelRoute(fromSpot: GliderSpots, targetSpot: GliderSpots): Int {
		return when (fromSpot) {
			GnomeGliders.GliderSpots.GNOME_TREE -> {
				when (targetSpot) {
					GnomeGliders.GliderSpots.GNOME_TREE -> 0
					GnomeGliders.GliderSpots.ICE_MOUNTAIN -> 1
					GnomeGliders.GliderSpots.DIGSITE -> 3
					GnomeGliders.GliderSpots.DESERT -> 4
					GnomeGliders.GliderSpots.SHIPYARD -> 8
				}
			}
			GnomeGliders.GliderSpots.ICE_MOUNTAIN -> {
				when (targetSpot) {
					GnomeGliders.GliderSpots.GNOME_TREE -> 2
					else -> 0
				}
			}
		// Can't travel from Digsite anywhere
			GnomeGliders.GliderSpots.DESERT -> {
				when (targetSpot) {
					GnomeGliders.GliderSpots.GNOME_TREE -> 5
					else -> 0
				}
			}
			GnomeGliders.GliderSpots.SHIPYARD -> {
				when (targetSpot) {
					GnomeGliders.GliderSpots.GNOME_TREE -> 6
					else -> 0
				}
			}
		// TODO mm2 and other place
			GnomeGliders.GliderSpots.DIGSITE -> return 0
		}
	}
}