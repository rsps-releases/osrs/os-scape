package nl.bartpelle.veteres.content.skills.farming.patch

import co.paralleluniverse.fibers.Suspendable
import com.google.common.collect.ImmutableRangeMap
import com.google.common.collect.Range
import com.google.common.collect.RangeMap
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.skills.farming.Farming
import nl.bartpelle.veteres.content.skills.farming.Farming.farmbit
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import java.util.*

/**
 * Created by Bart on 4/24/2016.
 *
 * Base object for farming allotments, including data.
 */
object Allotments {
	
	const val ALLOTMENT_TIMER = 500
	
	enum class Data(val seed: Item, val product: Int, val level: Int, val diseaseOdds: Double, val plantXp: Double,
	                val harvestXp: Double, val startVal: Int, val endVal: Int, val petOdds: Int, val protector: Flowers.Data? = null) {
		POTATOES(Item(5318, 3), 1942, 1, 0.30, 8.0, 9.0, 6, 10, 49000, Flowers.Data.MARIGOLDS),
		ONIONS(Item(5319, 3), 1957, 5, 0.30, 9.5, 10.5, 13, 17, 37000, Flowers.Data.MARIGOLDS),
		CABBAGES(Item(5324, 3), 1965, 7, 0.25, 10.0, 11.5, 20, 24, 32000, Flowers.Data.ROSEMARY),
		TOMATOES(Item(5322, 3), 1982, 12, 0.20, 12.5, 14.0, 27, 31, 24000, Flowers.Data.MARIGOLDS),
		SWEETCORNS(Item(5320, 3), 5986, 20, 0.17, 17.0, 19.0, 34, 40, 19900),
		STRAWBERRIES(Item(5323, 3), 5504, 31, 0.14, 26.0, 29.0, 43, 49, 14000),
		WATERMELONS(Item(5321, 3), 5982, 47, 0.10, 48.5, 54.5, 52, 60, 9000, Flowers.Data.NASTURTIUM);
		
		fun range(): Range<Int> = Range.closed(startVal, endVal + 2)
	}
	
	// Range lookup map to facilitate looking up the values to their data objects.
	@JvmStatic val RANGELOOKUP: RangeMap<Int, Data> = ImmutableRangeMap.builder<Int, Data>()
			.apply { Data.values().forEach { data -> put(data.range(), data) } }
			.build()
	
	// Seed lookup map
	@JvmStatic val SEEDLOOKUP: Map<Int, Data> = HashMap<Int, Data>().apply {
		Data.values().forEach { data -> put(data.seed.id(), data) }
	}
	
	@Suspendable fun interactAllotment(it: Script, varbit: Farmbit) {
		val varval = it.player().varps().farmbit(varbit)
		val states = AllotmentInfo.decompose(varval)
		val growing = RANGELOOKUP[states.stage]
		
		if (it.interactionOption() == 1) {
			// Weeds?
			if (varval >= 0 && varval <= 2) {
				Farming.rakeWeeds(it, varbit, "allotment")
				return
			}
			
			// Bodied plants?
			if (states.dead) {
				clearDeadCrops(it, varbit)
				return
			}
			
			// Are these grown?
			if (growing != null && states.stage >= growing.endVal && states.stage <= growing.endVal + 2) {
				harvest(it, varbit)
				return
			}
		}
	}
	
	@Suspendable fun clearDeadCrops(it: Script, varbit: Farmbit) {
		if (!it.player().inventory().contains(952)) {
			it.player().message("You need a spade to do that.")
			return
		}
		
		val varval = it.player().varps().farmbit(varbit)
		val states = AllotmentInfo.decompose(varval)
		val growing = RANGELOOKUP[states.stage]
		
		if (growing == null) {
			it.player().message("There aren't any crops in this patch to dig up.")
			return
		}
		
		// Don't dig up healthy crops!
		if (!states.dead) {
			it.chatPlayer("Dig up these healthy plants? Why would I want to do<br>that?", 576)
			return
		}
		
		it.player().message("You start digging the farming patch...")
		it.animate(830)
		it.delay(3)
		
		while (true) {
			it.animate(830)
			it.delay(3)
			
			// Random odds
			if (it.player().world().rollDie(255, 80 + it.player().skills().level(Skills.FARMING))) {
				it.player().varps().farmbit(varbit, 3)
				Farming.setTimer(it.player(), varbit.timer, ALLOTMENT_TIMER)
				break
			}
		}
		
		it.player().message("You have successfully cleared this patch for new crops.")
	}
	
	@Suspendable fun itemOnAllotment(it: Script, varbit: Farmbit) {
		val item = it.itemUsedId()
		val value = it.player().varps().farmbit(varbit)
		val allotmentSeed = SEEDLOOKUP[item]
		val isSeed = Farming.ALL_SEEDS.contains(item)
		
		// Are we trying to cure plants?
		if (item == Farming.PLANT_CURE) {
			curePlants(it, varbit)
			return
		}
		
		// Dig up plants?
		if (item == Farming.SPADE) {
			clearDeadCrops(it, varbit)
			return
		}
		
		// Rake it?
		if (item == Farming.RAKE) {
			Farming.rakeWeeds(it, varbit, "allotment")
			return
		}
		
		// Being a retard with an empty can(not)?
		if (item == Farming.EMPTY_CAN) {
			it.player().message("You need to fill the watering can first.")
			return
		}
		
		// Damn, how I love watering them.
		if (item in Farming.WATERING_CANS) {
			waterPlants(it, varbit, item)
			return
		}
		
		// Is it a seed?
		if (!isSeed) {
			it.player().message("Nothing interesting happens.")
			return
		}
		
		// Can we plant this seed here?
		if (isSeed && allotmentSeed == null) {
			it.player().message("You can't plant a " + Item(item).name(it.player().world()).toLowerCase() + " in this patch.")
			return
		}
		
		// Are there weeds in our patch?
		if (value >= 0 && value < 3) {
			it.player().message("This patch needs weeding first.")
			return
		}
		
		// Is this one already occupied?
		if (value > 3) {
			it.messagebox("You can only plant ${Item(item).name(it.player().world()).toLowerCase()}s in an empty patch.")
			return
		}
		
		// Null seed? Leave.
		if (allotmentSeed == null) {
			return
		}
		
		// Are we a real master farmer? :D
		if (it.player().skills().xpLevel(Skills.FARMING) < allotmentSeed.level) {
			it.messagebox("You must be a Level ${allotmentSeed.level} Farmer to plant those.")
			return
		}
		
		// Do we have enough seeds? You need 3 to plant.
		if (it.player().inventory().count(item) < 3) {
			it.player().message("You need 3 ${Item(item).name(it.player().world()).toLowerCase()}s to grow those.")
			return
		}
		
		// Plant the seeds
		it.player().lock()
		it.player().animate(2291)
		it.delay(3)
		
		var success = false
		if (BonusContent.isActive(it.player, BlessingGroup.NATURES_GIFT) && it.player.world().rollDie(2, 1)) {
			success = true
			it.player.message("The active Nature's Gift blessing lets you conserve your seeds.")
		} else {
			success = it.player().inventory().remove(Item(allotmentSeed.seed, 3), false).success()
		}
		
		if (success) {
			it.player().message("You plant 3 ${Item(item).name(it.player().world()).toLowerCase()}s in the allotment.")
			it.addXp(Skills.FARMING, allotmentSeed.plantXp)
			it.player().varps().farmbit(varbit, allotmentSeed.startVal)
			Farming.setTimer(it.player(), varbit.timer, ALLOTMENT_TIMER)
		}
		
		it.player().unlock()
	}
	
	@Suspendable fun waterPlants(it: Script, varbit: Farmbit, item: Int) {
		val value = it.player().varps().farmbit(varbit)
		val states = AllotmentInfo.decompose(value)
		val growing = RANGELOOKUP[states.stage]
		
		if (growing == null || states.watered || states.diseased || states.dead || states.watered || states.stage >= growing.endVal) {
			it.player().message("This patch doesn't need watering.")
			return
		}
		
		it.player().lock()
		it.player().animate(2293)
		it.delay(4)
		
		if (it.player().inventory().remove(Item(item), true).success()) {
			it.player().inventory().add(Item(if (item == 5333) 5331 else item - 1), true)
			states.watered = true
			it.player().varps().farmbit(varbit, states.compose())
		}
		
		it.player().unlock()
	}
	
	@Suspendable fun curePlants(it: Script, varbit: Farmbit) {
		val value = it.player().varps().farmbit(varbit)
		val states = AllotmentInfo.decompose(value)
		val growing = RANGELOOKUP[states.stage]
		
		if (growing == null) {
			it.player().message("This farming patch is empty.")
			return
		}
		
		if (!states.diseased) {
			it.player().message("This patch doesn't need curing.")
			return
		}
		
		it.player().lock()
		it.player().animate(2288)
		it.player().message("You treat the allotment patch with the plant cure.")
		it.delay(4)
		
		if (it.player().inventory().remove(Item(Farming.PLANT_CURE), true).success()) {
			it.player().inventory().add(Item(229), true)
			states.diseased = false
			it.player().varps().farmbit(varbit, states.compose())
			it.player().message("It is restored to health.")
		}
		
		it.player().unlock()
	}
	
	fun grow(it: Script, varbit: Farmbit) {
		val value = it.player().varps().farmbit(varbit)
		val states = AllotmentInfo.decompose(value)
		val growing = RANGELOOKUP[states.stage]
		
		// Do we need to regrow weeds?
		if (value in 1..3) {
			it.player().varps().farmbit(varbit, value - 1)
			Farming.addTimer(it.player(), varbit.timer, ALLOTMENT_TIMER)
			return
		}
		
		// Do we have a plant to grow or should we grow our weed?
		if (growing != null && states.stage < growing.endVal && !states.dead) {
			// Will it die?
			if (states.diseased) {
				states.dead = true
				it.player().varps().farmbit(varbit, states.compose())
				return // No more growing. It's dead. Get over it :'(
			} else if (states.stage != growing.startVal && willDisease(it.player(), states, growing, varbit)) { // Not dead, but will it disease?
				states.diseased = true
				it.player().varps().farmbit(varbit, states.compose())
			} else { // All good! Grow!
				states.watered = false // Reset all states
				states.stage++
				it.player().varps().farmbit(varbit, states.compose())
			}
			
			// Unless we're fully grown now, resubmit the timer.
			if (states.stage != growing.endVal) {
				Farming.addTimer(it.player(), varbit.timer, ALLOTMENT_TIMER)
			}
		}
	}
	
	@Suspendable fun harvest(it: Script, varbit: Farmbit) {
		val states = AllotmentInfo.decompose(it.player().varps().farmbit(varbit))
		val growing = RANGELOOKUP[states.stage] ?: return
		
		// Cannot harvest with a full inventory on RS.
		if (it.player().inventory().full()) {
			it.player().message("You don't have enough inventory space to do that.")
			return
		}
		
		// Do the heavy work, but only if we have a spade.
		if (!it.player().inventory().contains(952)) {
			it.player().message("You need a spade to do that.")
			return
		}
		
		it.player().message("You begin to harvest the allotment.")
		it.player().lock() // Avoid timers ticking et al
		
		while (true) {
			// Add the item, xp, increase stage and continue.
			it.delay(3)
			it.animate(830)
			it.addXp(Skills.FARMING, growing.harvestXp)
			it.player().inventory().add(Item(growing.product), true)
			
			// Odds are 1/4 to increase a stage, unless the blessing is active.
			if (it.player().world().rollDie(255, if (BonusContent.isActive(it.player, BlessingGroup.NATURES_GIFT)) 25 else 64)) {
				val value = it.player().varps().farmbit(varbit) + 1
				
				// Did we clean the patch?
				if (value > growing.endVal + 2) {
					it.player().varps().farmbit(varbit, 3) // Clear patch
					it.player().unlock()
					it.message("The allotment is now empty.")
					Farming.setTimer(it.player(), varbit.timer, ALLOTMENT_TIMER)
					break
				} else {
					it.player().varps().farmbit(varbit, value)
				}
			}
			
			// Woo! A pet!
			val odds = (growing.petOdds.toDouble() * it.player().mode().skillPetMod()).toInt()
			if (it.player().world().rollDie(odds, 1)) {
				unlockTangleroot(it.player())
			}
			
			// If we're out of inv space, stop the loop.
			if (it.player().inventory().full()) {
				it.player().message("You have run out of inventory space.")
				it.player().unlock()
				break
			}
		}
		
		it.player().unlock()
		
		// You begin to harvest the herb patch.
		// The herb patch is now empty.
		// You treat the allotment with supercompost.
	}
	
	fun willDisease(player: Player, states: AllotmentInfo, allotment: Data, farmbit: Farmbit): Boolean {
		// Protection if the flower is there, ya fele.
		if (allotment.protector != null) {
			val flower = Farmbit.FLOWERS.find { f -> f.attrib == farmbit.attrib }
			
			if (flower != null) {
				val flowerInfo = player.varps().farmbit(flower)
				val status = Flowers.FlowerInfo.decompose(flowerInfo)
				
				// If fully grown, not dead, and not diseased, then we are safe.
				if (!status.dead && !status.diseased && status.stage == allotment.protector.endVal) {
					return false
				}
			}
		}
		
		var odds = allotment.diseaseOdds
		
		// Watering reduces the odds by half
		if (states.watered) {
			odds /= 2
		}
		
		return player.world().randomDouble() <= odds
	}
	
	data class AllotmentInfo(var state: Int, var stage: Int) {
		fun compose(): Int = state.and(0x3).shl(6).or(stage.and(0b111111))
		
		// Holy fuck Kotlin is dope as fuck
		var dead: Boolean
			get() = state == 0x3
			set(v) {
				if (v) state = 0x3 else state = 0x0
			}
		
		var watered: Boolean
			get() = state == 0x1
			set(v) {
				if (v) state = 0x1 else state = 0x0
			}
		
		var diseased: Boolean
			get() = state == 0x2
			set(v) {
				if (v) state = 0x2 else state = 0x0
			}
		
		companion object {
			fun decompose(state: Int): AllotmentInfo = AllotmentInfo(state.shr(6).and(0x3), state.and(0b111111))
		}
	}
	
	fun unlockTangleroot(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.TANGLEROOT)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(Pet.TANGLEROOT.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.TANGLEROOT, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(Pet.TANGLEROOT.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.TANGLEROOT.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
}