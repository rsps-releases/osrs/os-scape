package nl.bartpelle.veteres.content.areas.alkharid.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object SilkTrader {
	
	val SILK_TRADER = 525
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(SILK_TRADER) @Suspendable {
			it.chatNpc("Do you want to buy any fine silks?", SILK_TRADER, 567)
			when (it.options("How much are they?", "No. Silk doesn't suit me.")) {
				1 -> {
					it.chatPlayer("How much are they?", 588)
					it.chatNpc("3 gp.", SILK_TRADER, 567)
					when (it.options("No. That's too much for me.", "Okay, that sounds good.")) {
						1 -> {
							it.chatPlayer("No. That's too much for me.")
							it.chatNpc("2 gp and that's as low as I'll go.", SILK_TRADER, 567)
							it.chatNpc("I'm not selling it for any less. You'll probably go and<br>sell it in Varrock for a profit, anyway.", SILK_TRADER, 567)
							when (it.options("2 gp sounds good.", "No, really. I don't want it.")) {
								1 -> {
									it.chatPlayer("2 gp sounds good.")
									if (it.player().inventory().remove(Item(995, 2), true).success()) {
										it.player().inventory().add(Item(950, 1), true)
										it.itemBox("You buy some silk for 2 gp.", 950)
									} else {
										it.chatPlayer("Oh dear. I don't have enough money.")
									}
								}
								2 -> {
									it.chatPlayer("No, really. I don't want it.")
									it.chatNpc("Okay, but that's the best price you're going to get.", SILK_TRADER, 567)
								}
							}
						}
						2 -> {
							it.chatPlayer("Okay, that sounds good.")
							if (it.player().inventory().remove(Item(995, 3), true).success()) {
								it.player().inventory().add(Item(950, 1), true)
								it.itemBox("You buy some silk for 3 gp.", 950)
							} else {
								it.chatPlayer("Oh dear. I don't have enough money.")
							}
						}
					}
				}
				2 -> {
					it.chatPlayer("No. Silk doesn't suit me.", 588)
				}
			}
		}
	}
}
