package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 23/09/2016.
 */
object ZulrahStaffOfDead {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		/**
		 * CREATION AND DISMANTLING
		 */
		
		//Creating the toxic staff of the dead
		r.onItemOnItem(ZulrahItems.NORM_SOTD, ZulrahItems.MAGIC_FANG) @Suspendable {
			if (it.player().inventory().has(ZulrahItems.NORM_SOTD) && it.player().inventory().has(ZulrahItems.MAGIC_FANG)) {
				
				val staffSlot = ItemOnItem.slotOf(it, ZulrahItems.NORM_SOTD)
				val staff = it.player().inventory()[staffSlot]
				val fangSlot = ItemOnItem.slotOf(it, ZulrahItems.MAGIC_FANG)
				val fang = it.player().inventory()[fangSlot]
				
				it.player().inventory() -= ZulrahItems.NORM_SOTD
				it.player().inventory() -= ZulrahItems.MAGIC_FANG
				it.player().inventory().add(Item(ZulrahItems.TOXIC_SOTD_EMPTY), false, it.itemOnSlot())
				it.animate(3015)
			}
		}
		
		// Dismantle uncharged toxic staff
		r.onItemOption4(ZulrahItems.TOXIC_SOTD_EMPTY, @Suspendable {
			it.doubleItemBox("This staff is <col=FF0000>uncharged</col>. Would you like to remove the magic fang? You can recombine it with the staff again afterwards.", 12902, 12932)
			if (it.options("Yes, remove the fang.", "Never mind.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(ZulrahItems.TOXIC_SOTD_EMPTY) > 0) {
					it.player().inventory() -= ZulrahItems.TOXIC_SOTD_EMPTY
					it.player().inventory() += ZulrahItems.NORM_SOTD
					it.player().inventory() += 12932
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		})
		
		/**
		 * CHARGING AND UNCHARGING
		 */
		
		// Check charges
		r.onItemOption3(ZulrahItems.TOXIC_SOTD_USED) {
			// Check t-SOTD
			check_scales_message(it, it.itemUsed(), 11000)
		}
		
		r.onEquipmentOption(1, ZulrahItems.TOXIC_SOTD_USED) {
			check_scales_message(it, it.player().equipment().get(EquipSlot.WEAPON), 11000)
		}
		
		// Scales on empty
		r.onItemOnItem(ZulrahItems.ZULRAHS_SCALES, ZulrahItems.TOXIC_SOTD_EMPTY) {
			
			val staffSlot = ItemOnItem.slotOf(it, ZulrahItems.TOXIC_SOTD_EMPTY)
			val staff = it.player().inventory()[staffSlot]
			val scalesSlot = ItemOnItem.slotOf(it, ZulrahItems.ZULRAHS_SCALES) // opposite way around
			val scales = it.player().inventory()[scalesSlot]
			
			// If the system is disabled, replace empty with used.
			if (staff.id() == ZulrahItems.TOXIC_SOTD_EMPTY && !ZulrahItems.CHARGING_ENABLED) {
				// Replace empty with a full one with no charges.
				if (it.player().inventory().remove(staff, true).success()) {
					it.player().inventory().add(Item(ZulrahItems.TOXIC_SOTD_USED), true, staffSlot)
					it.message("You don't need to charge this item for it to work.")
				}
				return@onItemOnItem
			}
			if (!ZulrahItems.CHARGING_ENABLED) {
				it.message("You don't need to charge this item for it to work.")
				return@onItemOnItem
			}
			val amtToAdd = Math.min(11000 - staff.property(ItemAttrib.ZULRAH_SCALES), scales.amount())
			if (amtToAdd == 0) {
				it.message("Your toxic staff is already fully charged.")
				return@onItemOnItem
			}
			it.player().inventory() -= staff
			if (it.player().inventory().remove(Item(ZulrahItems.ZULRAHS_SCALES, amtToAdd), false).success()) {
				val chargedStaff = Item(ZulrahItems.TOXIC_SOTD_USED)
				chargedStaff.property(ItemAttrib.ZULRAH_SCALES, staff.property(ItemAttrib.ZULRAH_SCALES) + amtToAdd)
				val newtotal = chargedStaff.property(ItemAttrib.ZULRAH_SCALES)
				it.player().inventory().add(chargedStaff, true, it.itemOnSlot())
				it.message("You add $amtToAdd scales to your toxic staff. It now has $newtotal charges.")
			}
		}
		
		
		// Scales on used (partly full)
		r.onItemOnItem(ZulrahItems.ZULRAHS_SCALES, ZulrahItems.TOXIC_SOTD_USED) {
			if (!ZulrahItems.CHARGING_ENABLED) {
				it.message("You don't need to charge this item for it to work.")
				return@onItemOnItem
			}
			val staffSlot = ItemOnItem.slotOf(it, ZulrahItems.TOXIC_SOTD_USED)
			val staff = it.player().inventory()[staffSlot]
			val scalesSlot = ItemOnItem.slotOf(it, ZulrahItems.ZULRAHS_SCALES) // opposite way around
			val scales = it.player().inventory()[scalesSlot]
			val amtToAdd = Math.min(11000 - staff.property(ItemAttrib.ZULRAH_SCALES), scales.amount())
			if (amtToAdd == 0 && staff.property(ItemAttrib.ZULRAH_SCALES) > 0) {
				it.message("Your toxic staff is already fully charged.")
				return@onItemOnItem
			}
			if (it.player().inventory().remove(Item(ZulrahItems.ZULRAHS_SCALES, amtToAdd), false).success()) {
				val chargedStaff = staff
				chargedStaff.property(ItemAttrib.ZULRAH_SCALES, staff.property(ItemAttrib.ZULRAH_SCALES) + amtToAdd)
				val newtotal = chargedStaff.property(ItemAttrib.ZULRAH_SCALES)
				it.message("You add $amtToAdd scales to your toxic staff. It now has $newtotal charges.")
			}
		}
		
		// 'Uncharge' - convert t-sotd into fang + sotd
		r.onItemOption5(ZulrahItems.TOXIC_SOTD_USED, @Suspendable {
			if (!it.itemUsed().hasProperties()) {
				it.player().inventory().set(it.itemUsedSlot(), Item(ZulrahItems.TOXIC_SOTD_EMPTY))
				it.player().message("You uncharge the toxic staff.")
			} else {
				val spacerequired = if (it.player().inventory().has(ZulrahItems.ZULRAHS_SCALES)) 0 else 1
				if (it.player().inventory().freeSlots() < spacerequired) {
					it.messagebox("You need $spacerequired free inventory slots to do this.")
				} else {
					val scalesToAdd = Math.min(Integer.MAX_VALUE - it.player().inventory().count(ZulrahItems.ZULRAHS_SCALES), it.itemUsed().property(ItemAttrib.ZULRAH_SCALES))
					if (scalesToAdd == 0) { // 2.1b scales held already.
						it.message("You don't have enough room in your inventory to do this.")
					} else {
						val charged = it.itemUsed()
						charged.property(ItemAttrib.ZULRAH_SCALES, charged.property(ItemAttrib.ZULRAH_SCALES) - scalesToAdd)
						if (charged.property(ItemAttrib.ZULRAH_SCALES) == 0) {
							// Give an uncharge blowpipe to the player, as it has nothing in it now.
							it.player().inventory().remove(charged, false, it.itemUsedSlot())
							it.player().inventory().add(Item(ZulrahItems.TOXIC_SOTD_EMPTY), false, it.itemUsedSlot())
						}
						it.player().inventory() += Item(ZulrahItems.ZULRAHS_SCALES, scalesToAdd)
						it.message("You remove the $scalesToAdd zulrah scales from the toxic staff.")
					}
				}
			}
		})
	}
	
	@JvmStatic fun check_scales_message(it: Script, itemUsed: Item, maxScales: Int) {
		val itemname = itemUsed.definition(it.player().world()).name.toLowerCase()
		if (!itemUsed.hasProperties())
			it.player().message("The $itemname is <col=FF0000>uncharged</col>.")
		else {
			val blowpipe = itemUsed.id() == 12926
			if (blowpipe) {
				val darts = if (it.player().world().realm().isPVP) "infinite" else "${itemUsed.property(ItemAttrib.DARTS_COUNT)}"
				it.player().message("Your $itemname has <col=FF0000>${itemUsed.property(ItemAttrib.ZULRAH_SCALES)} scales and $darts darts remaining.")
			} else {
				it.player().message("Your $itemname has <col=FF0000>${((itemUsed.property(ItemAttrib.ZULRAH_SCALES) as Int).toDouble() / maxScales).toFloat() * 100}%</col> charges (${itemUsed.property(ItemAttrib.ZULRAH_SCALES)} scales) remaining.")
			}
		}
	}
}