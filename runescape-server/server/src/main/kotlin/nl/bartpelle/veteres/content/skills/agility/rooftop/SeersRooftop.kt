package nl.bartpelle.veteres.content.skills.agility.rooftop

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.skills.agility.UnlockAgilityPet
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 3/18/2015.
 */
object SeersRooftop {
	
	val MARK_SPOTS = arrayOf(Tile(2725, 3494, 3),
			Tile(2708, 3492, 2),
			Tile(2712, 3478, 2),
			Tile(2702, 3473, 3),
			Tile(2699, 3462, 2))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Wall climb
		r.onObject(11373, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().skills().xpLevel(Skills.AGILITY) < 60) {
				it.message("You need an Agility level of 60 to attempt this.")
			} else {
				it.player().lock()
				it.player().faceTile(it.player().tile() + Tile(0, 1))
				it.delay(1)
				it.player().animate(737, 15)
				it.delay(2)
				it.player().teleport(2729, 3488, 1)
				it.animate(1118)
				it.delay(2)
				it.player().teleport(2729, 3491, 3)
				it.animate(-1)
				it.addXp(Skills.AGILITY, 45.0)
				it.player().unlock()
				
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 60, 20)
			}
		})
		
		// Rooftop jumping
		r.onObject(11374, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().sound(2462, 15)
			it.player().animate(2586, 15)
			it.delay(1)
			it.player().teleport(2719, 3495, 2)
			it.player().animate(2588)
			it.delay(1)
			it.player().sound(2462, 15)
			it.player().animate(2586, 15)
			it.delay(1)
			it.player().teleport(2713, 3494, 2)
			it.player().animate(2588)
			it.delay(1)
			it.addXp(Skills.AGILITY, 20.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 60, 20)
		})
		
		// Tightrope
		r.onObject(11378, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 6)
			it.player().pathQueue().interpolate(2710, 3481, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(2710, 3481))
			it.player().looks().resetRender()
			it.delay(1)
			it.addXp(Skills.AGILITY, 20.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 60, 20)
		})
		
		// Gap jump
		r.onObject(11375, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().faceTile(it.player().tile() - Tile(0, 1))
			it.delay(1)
			it.player().forceMove(ForceMovement(0, 0, 0, -3, 25, 30, FaceDirection.SOUTH))
			it.delay(1)
			it.player().animate(2585)
			it.delay(1)
			it.player().teleport(2710, 3474, 3)
			it.player().forceMove(ForceMovement(0, 0, 0, -2, 17, 26, FaceDirection.SOUTH))
			it.delay(1)
			it.player().teleport(2710, 3472, 3)
			it.addXp(Skills.AGILITY, 35.0)
			putStage(it.player(), 5)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 60, 20)
		})
		
		// Small gap jump
		r.onObject(11376, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().faceTile(it.player().tile() - Tile(0, 1))
			it.player().animate(2586, 15)
			it.player().sound(2462, 15)
			it.delay(1)
			it.player().animate(2588)
			it.player().teleport(2702, 3465, 2)
			it.delay(1)
			it.animate(-1)
			it.addXp(Skills.AGILITY, 15.0)
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 60, 20)
		})
		
		// Final gap jump down
		r.onObject(11377, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.player().faceTile(it.player().tile() + Tile(1, 0))
			it.delay(1)
			it.player().animate(2462, 15)
			it.player().sound(2586, 15)
			it.delay(1)
			it.player().teleport(2704, 3464, 0)
			it.animate(2588)
			it.delay(1)
			it.player().animate(-1)
			
			var stage = it.player().attribOr<Int>(AttributeKey.SEERS_ROOFTOP_COURSE_STATE, 0)
			if (stage == 5) {
				it.player().putattrib(AttributeKey.SEERS_ROOFTOP_COURSE_STATE, 0)
				it.addXp(Skills.AGILITY, 435.0)
			} else {
				it.addXp(Skills.AGILITY, 15.0)
			}
			
			it.player().unlock()
			
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 60, 20)
			
			// Woo! A pet!
			val odds = (13000.00 * it.player().mode().skillPetMod()).toInt()
			if (it.player().world().rollDie(odds, 1)) {
				UnlockAgilityPet.unlockGiantSquirrel(it.player())
			}
		})
	}
	
	private fun putStage(player: Player, stageBit: Int): Int {
		var stage = player.attribOr<Int>(AttributeKey.SEERS_ROOFTOP_COURSE_STATE, 0)
		stage = stage or stageBit
		player.putattrib(AttributeKey.SEERS_ROOFTOP_COURSE_STATE, stage)
		return stage
	}
	
}
