package nl.bartpelle.veteres.content.areas.dungeons

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/11/2015.
 */

object StrongholdOfSecurity {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		intArrayOf(19206, 19207, 17009, 17100, 23653, 23654, 23727, 23728).forEach { door ->
			// Doors
			r.onObject(door) @Suspendable { passDoor(it) }
		}
		r.onObject(20790) { it.player().teleport(1859, 5243) } // Down to stronghold of security
		r.onObject(20784) { it.player().teleport(3081, 3421) } // Up to surface barb village
		r.onObject(19001) { it.player().teleport(3081, 3421) } // Up to surface barb village
		r.onObject(23703) { it.player().teleport(3081, 3421) } // Up to surface barb village
		r.onObject(23732) { it.player().teleport(3081, 3421) } // Up to surface barb village
		r.onObject(23921) { it.player().teleport(3081, 3421) } // Up to surface barb village
		r.onObject(20785) { it.player().teleport(2042, 5245) } // Down to stronghold of security 2
		r.onObject(19003) { it.player().teleport(1903, 5222) } // Up to lv 1
		r.onObject(19004) { it.player().teleport(2123, 5252) } // Up to lv 3
		r.onObject(23706) { it.player().teleport(2358, 5215) } // Up to lv 3
	}
	
	@Suspendable fun passDoor(it: Script) {
		val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
		val atX = obj.tile().x == it.player().tile().x
		val atZ = obj.tile().z == it.player().tile().z
		it.delay(1)
		when (obj.rot()) {
			0 -> {
				if (atX) {
					it.player().teleport(obj.tile().transform(-1, 0, 0))
				} else {
					it.player().teleport(obj.tile())
				}
			}
			1 -> {
				if (atZ) {
					it.player().teleport(obj.tile().transform(0, 1, 0))
				} else {
					it.player().teleport(obj.tile())
				}
			}
			2 -> {
				if (atX) {
					it.player().teleport(obj.tile().transform(1, 0, 0))
				} else {
					it.player().teleport(obj.tile())
				}
			}
			3 -> {
				if (atZ) {
					it.player().teleport(obj.tile().transform(0, -1, 0))
				} else {
					it.player().teleport(obj.tile())
				}
			}
		}
	}
	
}