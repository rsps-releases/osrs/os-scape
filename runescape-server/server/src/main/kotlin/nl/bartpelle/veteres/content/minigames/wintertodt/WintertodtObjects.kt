package nl.bartpelle.veteres.content.minigames.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.areas.zeah.wcguild.WoodcuttingGuild
import nl.bartpelle.veteres.content.minigames.wintertodt.WintertodtGame.MIN_Z_FOR_DAMAGE
import nl.bartpelle.veteres.content.minigames.wintertodt.WintertodtGame.addWintertodtPoints
import nl.bartpelle.veteres.content.skills.firemaking.LogLighting
import nl.bartpelle.veteres.content.skills.woodcutting.Woodcutting
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 9/10/2016.
 */
object WintertodtObjects {
	
	const val HAMMER_CRATE = 29316
	const val KNIFE_CRATE = 29317
	const val AXE_CRATE = 29318
	const val TINDERBOX_CRATE = 29319
	const val POTION_CRATE = 29320
	const val AGILITY_GAP = 29326
	const val SPROUTING_ROOTS = 29315
	const val UNLIT_BRAZIER = 29312
	const val BROKEN_BRAZIER = 29313
	const val BURNING_BRAZIER = 29314
	
	const val POTION_ITEM = 20697
	const val HERB = 20698
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Single-take crates
		r.onObject(HAMMER_CRATE) { takeOne(it, 2347, "a hammer") }
		r.onObject(KNIFE_CRATE) { takeOne(it, 946, "a knife") }
		r.onObject(AXE_CRATE) { takeOne(it, 1351, "an axe") }
		r.onObject(TINDERBOX_CRATE) { takeOne(it, 590, "a tinderbox") }
		
		// Potions can be taken multiple quantities
		r.onObject(POTION_CRATE) {
			val amt = when (it.interactionOption()) {
				2 -> 5
				3 -> 10
				else -> 1
			}
			
			if (it.player.inventory().add(Item(POTION_ITEM, amt), true).completed() > 0) {
				it.player.message("You take the unfinished potion%s from the chest.", if (amt == 1) "" else "s")
			} else {
				it.player.message("You need space in your inventory to take an unfinished potion.")
			}
		}
		
		// Potion crafting
		r.onItemOnItem(HERB, POTION_ITEM) {
			it.player().inventory().remove(Item(HERB), true)
			it.player().inventory().remove(Item(POTION_ITEM), true)
			
			it.player().inventory().add(Item(20699), true)
		}
		
		// Kindling fletching
		r.onItemOnItem(946, 20695) @Suspendable {
			if (it.player.tile().z < MIN_Z_FOR_DAMAGE) { // Avoid people cutting, then safely fletching them.
				it.player().message("Your hands are too cold to fletch here - move closer to the braziers.")
			} else {
				while (it.player.inventory().contains(20695)) {
					it.player.animate(1248)
					it.delay(3)
					
					if (it.player.inventory().remove(Item(20695), true).success()) {
						it.player().inventory().add(Item(20696), true)
						it.player().skills().__addXp(Skills.FLETCHING, 25.0)
						it.player().message("You carefully fletch the root into a bundle of kindling.")
					}
				}
			}
		}
		
		// Agility gaps
		r.onObject(AGILITY_GAP) @Suspendable { jumpGap(it) }
		
		// Herb picking
		r.onObject(SPROUTING_ROOTS) @Suspendable { pickHerb(it) }
		
		// Braziers
		r.makeRemoteObject(UNLIT_BRAZIER)
		r.onObject(UNLIT_BRAZIER) @Suspendable { lightBrazier(it) }
		r.makeRemoteObject(BROKEN_BRAZIER)
		r.onObject(BROKEN_BRAZIER) @Suspendable { repairBrazier(it) }
		r.makeRemoteObject(BURNING_BRAZIER)
		r.onObject(BURNING_BRAZIER) @Suspendable { fuelBrazier(it) }
		
		// Entrance
		r.onObject(29322) @Suspendable {
			if (it.player.world().realm().isPVP) {
				it.messagebox("The door won't budge. It seems frozen.")
			} else if (WintertodtGame.minigameDisabled) {
				it.messagebox("It seems as if the Wintertodt has been killed...")
			} else {
				val res = it.player.interfaces().resizable()
				val enter = it.player.tile().z < 3966
				
				if (enter || it.optionsTitled("Are you sure you want to leave?", "Leave and lose all progress.", "Stay.") == 1) {
					it.player.lock()
					it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
					it.player.invokeScript(951)
					it.delay(2)
					
					if (enter) {
						it.player.teleport(1630, 3982)
					} else {
						it.player.teleport(1630, 3958)
					}
					
					it.player.clearattrib(AttributeKey.WINTERTODT_POINTS)
					WintertodtGame.clearInventoryFor(it.player)
					it.player.interfaces().send(174, if (res) 161 else 548, if (res) 3 else 12, true)
					it.player.invokeScript(948, 0, 0, 0, 255, 50)
				}
				
				it.delay(2)
				it.player.unlock()
			}
		}
		
		// Potion brewing
		r.onItemOnItem(20697, 20698) {
			it.player.inventory().remove(Item(20697), true)
			it.player.inventory().remove(Item(20698), true)
			it.player.inventory().add(Item(20699), true)
		}
		
		// Cutting roots
		r.onObject(29311) @Suspendable {
			cutRoots(it)
		}
		
		// Dropping items
		for (item in intArrayOf(20697, 20699, 20700, 20701, 20702)) {
			r.onItemOption5(item) { removeWithMessage(it, "The vial shatters as it hits the floor.") }
		}
		
		r.onItemOption5(20695) { removeWithMessage(it, "The root shatters as it hits the floor.") }
		r.onItemOption5(20696) { removeWithMessage(it, "The kindling shatters as it hits the floor.") }
		r.onItemOption5(20698) { removeWithMessage(it, "The herb shatters as it hits the floor.") }
	}
	
	fun removeWithMessage(it: Script, message: String) {
		it.player().inventory().set(it.player.attrib<Int>(AttributeKey.ITEM_SLOT), null)
		it.player().message(message)
	}
	
	@Suspendable fun cutRoots(it: Script) {
		val axe = Woodcutting.Woodcutting.findAxe(it.player())
		
		if (!WintertodtGame.state.active) {
			it.player.message("There's no use for bruma roots at this time.")
			return
		}
		
		if (axe == null) {
			it.player.message("You do not have an axe which you have the Woodcutting level to use.")
		} else {
			while (WintertodtGame.state.active && !it.player.inventory().full()) {
				it.player().animate(WoodcuttingGuild.cutAnimationFor(axe))
				it.delay(3)
				
				// 35% chance per 3 ticks to cut a log from the trunk
				if (it.player.world().random(100) <=
						Woodcutting.Woodcutting.chance(it.player.skills()[Skills.WOODCUTTING],
								Woodcutting.Woodcutting.Tree.OAK, axe)) {
					it.addXp(Skills.WOODCUTTING, 11.0)
					it.player().inventory().add(Item(20695), true)
					it.player.message("You get a bruma root.")
				}
			}
		}
	}
	
	fun takeOne(it: Script, item: Int, name: String) {
		if (it.player.inventory().contains(item)) {
			it.player.message("You already have %s.", name)
		} else {
			if (it.player.inventory().add(Item(item), true).success()) {
				it.player.message("You take %s from the chest.", name)
			} else {
				it.player.message("You need space in your inventory to take %s.", name)
			}
		}
	}
	
	@Suspendable fun jumpGap(it: Script) {
		val mapobj = it.interactionObject()
		val fromLeft = it.player.tile().x < mapobj.tile().x
		
		it.delay(1)
		it.player.lock()
		it.player.animate(741, 0)
		it.player.forceMove(ForceMovement(0, 0, if (fromLeft) 2 else -2, 0, 15, 30, if (fromLeft) FaceDirection.EAST else FaceDirection.WEST))
		it.delay(1)
		it.player.teleport(Tile(if (fromLeft) mapobj.tile().x + 1 else mapobj.tile().x - 1, mapobj.tile().z))
		it.addXp(Skills.AGILITY, 9.0)
		it.player.unlock()
	}
	
	@Suspendable fun pickHerb(it: Script) {
		if (!WintertodtGame.state.active) {
			it.player.message("There's no need to do that at this time.")
			return
		}
		
		if (it.player.inventory().full()) {
			it.player.message("You have no space for that.")
			return
		}
		
		it.delay(2)
		
		while (!it.player.inventory().full()) {
			it.player.animate(2282)
			it.delay(3)
			it.player.inventory().add(Item(HERB), true)
			it.player.message("You pick a bruma herb.")
		}
		
		it.player.message("You have no space for that.")
	}
	
	@Suspendable fun lightBrazier(it: Script) {
		val brazier = WintertodtGame.state.brazierTileLookup[it.interactionObject().tile()] ?: return
		it.player.walkTo(it.interactionObject(), PathQueue.StepType.REGULAR)
		it.waitForTile(it.player.pathQueue().peekLastTile())
		
		if (!WintertodtGame.state.active) {
			it.player.message("There's no need to do that at this time.")
			return
		}
		
		if (!it.player.inventory().contains(590)) {
			it.player.message("You need a tinderbox to light that brazier.")
			return
		}
		
		if (!brazier.pyromancerAlive) {
			it.player.message("Heal the Pyromancer before lighting the brazier.")
			return
		}
		
		it.player.animate(733)
		it.delay(3)
		brazier.changeState(BrazierState.LIT, it.player.world())
		it.player.animate(-1)
		it.player.addWintertodtPoints(25)
		it.addXp(Skills.FIREMAKING, (it.player.skills().xpLevel(Skills.FIREMAKING) * 3.0) * LogLighting.pyromancerOutfitBonus(it.player()))
		it.player.message("You light the brazier.")
	}
	
	@Suspendable fun repairBrazier(it: Script) {
		it.player.walkTo(it.interactionObject(), PathQueue.StepType.REGULAR)
		it.waitForTile(it.player.pathQueue().peekLastTile())
		
		if (!it.player.inventory().contains(2347)) {
			it.player.message("You need a hammer to repair that brazier.")
			return
		}
		
		it.player.animate(3676)
		it.delay(3)
		WintertodtGame.state.brazierTileLookup[it.interactionObject().tile()]?.changeState(BrazierState.STALE, it.player.world())
		it.player.animate(-1)
		it.player.message("You fix the brazier.")
		it.player.addWintertodtPoints(25)
	}
	
	@Suspendable fun fuelBrazier(it: Script) {
		it.player.walkTo(it.interactionObject(), PathQueue.StepType.REGULAR)
		it.waitForTile(it.player.pathQueue().peekLastTile())
		
		if (!it.player.inventory().hasAny(20696, 20695)) {
			it.player.message("You have run out of bruma roots.")
			return
		}
		
		while (true) {
			if (!it.interactionObject().valid(it.player.world())) {
				it.player.message("The brazier has gone out.")
				it.player.animate(-1)
				return
			}
			
			if (!it.player.inventory().hasAny(20696, 20695)) {
				it.player.message("You have run out of bruma roots.")
				return
			}
			
			it.player.animate(832)
			it.delay(2)
			it.player.addWintertodtPoints(10)
			
			// Try to burn a kindling first.
			if (!it.player.inventory().remove(Item(20696), true).success()) {
				it.player.inventory().remove(Item(20695), true)
				it.addXp(Skills.FIREMAKING, (it.player.skills().xpLevel(Skills.FIREMAKING) * 1.5) * LogLighting.pyromancerOutfitBonus(it.player()))
			} else {
				it.addXp(Skills.FIREMAKING, (it.player.skills().xpLevel(Skills.FIREMAKING) * 1.9) * LogLighting.pyromancerOutfitBonus(it.player()))
			}
			
			it.delay(2)
		}
	}
	
}