package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 10/12/2016.
 */

object AbyssalSireScion {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val abyssalSireScion = it.npc()
		val targetedPlayer = abyssalSireScion.attribOr<Entity>(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, null)
		
		while (EntityCombat.targetOk(abyssalSireScion, targetedPlayer) && PlayerCombat.canAttack(abyssalSireScion, targetedPlayer)) {
			abyssalSireScion.face(targetedPlayer)
			
			if (EntityCombat.canAttackDistant(abyssalSireScion, targetedPlayer, true, 4) && EntityCombat.attackTimerReady(abyssalSireScion)) {
				
				val randomAttack = abyssalSireScion.world().random(7)
				
				when (randomAttack) {
					1 -> if (EntityCombat.canAttackMelee(abyssalSireScion, targetedPlayer, true))
						meleeAttack(abyssalSireScion, targetedPlayer)
					else rangedAttack(abyssalSireScion, targetedPlayer)
					else -> rangedAttack(abyssalSireScion, targetedPlayer)
				}
			}
			
			EntityCombat.postHitLogic(abyssalSireScion)
			it.delay(1)
		}
		
		//If we no longer pass the combat checks, simply unregister the NPC
		abyssalSireScion.world().unregisterNpc(abyssalSireScion)
	}
	
	fun rangedAttack(abyssalSireSpawn: Npc, targetedPlayer: Entity) {
		val distanceFromPlayer = abyssalSireSpawn.tile().transform(1, 1).distance(targetedPlayer.tile())
		val hitDelay = Math.max(1, (50 + (distanceFromPlayer * 12)) / 30)
		
		abyssalSireSpawn.face(targetedPlayer)
		abyssalSireSpawn.animate(7127)
		abyssalSireSpawn.world().spawnProjectile(abyssalSireSpawn, targetedPlayer, 32, 20, 30, 30, 12 * distanceFromPlayer, 14, 5)
		
		if (EntityCombat.attemptHit(abyssalSireSpawn, targetedPlayer, CombatStyle.RANGE))
			targetedPlayer.hit(abyssalSireSpawn, EntityCombat.randomHit(abyssalSireSpawn), hitDelay.toInt()).combatStyle(CombatStyle.RANGE)
		else
			targetedPlayer.hit(abyssalSireSpawn, 0, hitDelay.toInt())
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSireSpawn)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		
		abyssalSireSpawn.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSireSpawn, abyssalSireSpawn.combatInfo().attackspeed)
	}
	
	fun meleeAttack(abyssalSireSpawn: Npc, targetedPlayer: Entity) {
		if (EntityCombat.attemptHit(abyssalSireSpawn, targetedPlayer, CombatStyle.MELEE))
			targetedPlayer.hit(abyssalSireSpawn, EntityCombat.randomHit(abyssalSireSpawn))
		else
			targetedPlayer.hit(abyssalSireSpawn, 0)
		
		abyssalSireSpawn.face(targetedPlayer)
		abyssalSireSpawn.animate(abyssalSireSpawn.attackAnimation())
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSireSpawn)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		
		abyssalSireSpawn.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSireSpawn, abyssalSireSpawn.combatInfo().attackspeed)
	}
	
}
