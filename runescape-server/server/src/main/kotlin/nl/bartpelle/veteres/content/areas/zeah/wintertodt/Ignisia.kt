package nl.bartpelle.veteres.content.areas.zeah.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/11/2016.
 */

object Ignisia {
	
	val IGNISIA = 7374
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(IGNISIA) @Suspendable {
			val TALKED_TO_IGNISIA = it.player().attribOr<Int>(AttributeKey.TALKED_TO_IGNISIA, 0)
			
			if (TALKED_TO_IGNISIA == 1) {
				it.chatNpc("Welcome back brother. How may I be of assistance?", IGNISIA, 588)
				when (it.options("Tell me about the Wintertodt.", "Tell me about the Pyromancers.", "Tell me about yourself.",
						"What can I do to help?", "I'm fine thanks.")) {
					1 -> tell_me_about_wintertodt(it)
					2 -> tell_me_about_pyromancers(it)
					3 -> tell_me_about_yourself(it)
					4 -> what_can_I_do_to_help(it)
					5 -> im_fine_thanks(it)
				}
			} else {
				it.chatNpc("Welcome brother.", IGNISIA, 588)
				it.chatPlayer("Err... Are we related?", 554)
				it.chatNpc("We are all related brother, for we are all children of the<br>flame.", IGNISIA, 589)
				it.chatPlayer("Okayyy...", 575)
				it.chatPlayer("So what is this place?", 554)
				it.chatNpc("You stand before the Doors of Dinh, behind them lies<br>the prison of the Wintertodt.", IGNISIA, 589)
				it.chatPlayer("What's a Wintertodt?", 554)
				it.chatNpc("We do not know what it is. All we know is that it<br>threatens to snuff out every flame in this land and" +
						"<br>plunge the world into eternal darkness.", IGNISIA, 590)
				it.chatPlayer("Oh dear... We should probably do something about that.", 588)
				it.chatNpc("That is why we are here brother. Ever since the<br>Wintertodt was imprisoned, the Order of the Sacred" +
						"<br>Flame has stood vigil over this place.", IGNISIA, 590)
				it.chatNpc("But alas, despite our best efforts, the Wintertodt is<br>growing in power again.", IGNISIA, 589)
				it.chatPlayer("Well that's just no good. Is there anything I can do to<br>help.", 555)
				it.chatNpc("Our order searches tirelessly for a way to defeat the<br>Wintertodt. Until we discover it, the Wintertodt must be" +
						"<br>held at bay.", IGNISIA, 590)
				it.chatNpc("Pass through the Doors of Dinh to the prison within.<br>You will find the finest of our Order channeling the" +
						"<br>power of the sacred flame into the Wintertodt.", IGNISIA, 590)
				it.chatNpc("Help them keep the holy fires lit so that they can keep<br>the Wintertodt subdued.", IGNISIA, 589)
				it.chatPlayer("How can I do that?", 554)
				it.chatNpc("You will find the roots of the great Bruma tree have<br>crept into the prison. Chop the roots and use them to" +
						"<br>keep the sacred flame alive.", IGNISIA, 590)
				it.chatPlayer("Anything else?", 554)
				it.player().putattrib(AttributeKey.TALKED_TO_IGNISIA, 1)
				it.chatNpc("You can use the herbs of the Bruma tree to make<br>Rejuvination potions. Give these to any of our order" +
						"<br>who fall to the cold of the Wintertodt before they<br>succumb to the darkness.", IGNISIA, 591)
				it.chatNpc("How else may I be of assistance?", IGNISIA, 588)
				when (it.options("Tell me about the Wintertodt.", "Tell me about the Pyromancers.", "Tell me about yourself.", "That's all thanks.")) {
					1 -> tell_me_about_wintertodt(it)
					2 -> tell_me_about_pyromancers(it)
					3 -> tell_me_about_yourself(it)
					4 -> im_fine_thanks(it)
				}
			}
		}
	}
	
	@Suspendable fun tell_me_about_wintertodt(it: Script) {
		it.chatPlayer("Tell me about the Wintertodt.", 554)
		it.chatNpc("Do you feel the winds of winter as they lick across the<br>land? This is the power of the Wintertodt. We know" +
				"<br>not what it is nor from where it came. Only that it<br>wields the power of Winter itself.", IGNISIA, 591)
		it.chatNpc("The ground on which we stand was once full of light,<br>full of life. But then the Wintertodt came. Now the" +
				"<br>fires of these lands burn no more.", IGNISIA, 590)
		it.chatNpc("How else may I be of assistance?", IGNISIA, 588)
		when (it.options("Tell me about the Pyromancers.", "Tell me about yourself.", "What can I do to help?", "I'm fine thanks.")) {
			1 -> tell_me_about_pyromancers(it)
			2 -> tell_me_about_yourself(it)
			3 -> what_can_I_do_to_help(it)
			4 -> im_fine_thanks(it)
		}
	}
	
	@Suspendable fun tell_me_about_pyromancers(it: Script) {
		it.chatPlayer("Tell me about the Pyromancers.", 554)
		it.chatNpc("We are the Order of the Sacred Flame, formed over a<br>thousand years ago when the Wintertodt first" +
				"<br>threatened Great Kourend.", IGNISIA, 590)
		it.chatNpc("We fought against the Wintertodt until we managed to<br>lure it behind Dinh's great doors. Our task did not end" +
				"<br>there though, for we always knew that the Wintertodt<br>would rise again.", IGNISIA, 591)
		it.chatNpc("How else may I be of assistance?", IGNISIA, 588)
		when (it.options("Tell me about the Wintertodt.", "Tell me about yourself.", "What can I do to help?", "I'm fine thanks.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_yourself(it)
			3 -> what_can_I_do_to_help(it)
			4 -> im_fine_thanks(it)
		}
	}
	
	@Suspendable fun tell_me_about_yourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 554)
		it.chatNpc("I am Ignisia, Grand Master of the Order of the Sacred<br>Flame. I have served our Order faithfully since I" +
				"<br>passed the Trials of Fire before the Ascent of Arceuus.", IGNISIA, 590)
		it.chatNpc("How else may I be of assistance?", IGNISIA, 588)
		when (it.options("Tell me about the Wintertodt.", "Tell me about the Pyromancers.", "What can I do to help?", "I'm fine thanks.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_pyromancers(it)
			3 -> what_can_I_do_to_help(it)
			4 -> im_fine_thanks(it)
		}
	}
	
	@Suspendable fun what_can_I_do_to_help(it: Script) {
		it.chatPlayer("What can I do to help?", 554)
		it.chatNpc("We must hold back the Wintertodt until we can find a<br>way to defeat it. Head into the prison and keep the holy" +
				"<br>fires lit so that our Order may keep the evil at bay.", IGNISIA, 590)
		it.chatPlayer("How can I do that?", 554)
		it.chatNpc("You will find the roots of the great Bruma tree have<br>crept into the prison. Chop the roots and use them to" +
				"<br>keep the sacred flame alive.", IGNISIA, 590)
		it.chatPlayer("Anything else?", 554)
		it.chatNpc("You can use the herbs of the Bruma tree to make<br>Rejuvination potions. Give these to any of our order" +
				"<br>who fall to the cold of the Wintertodt before they<br>succumb to the darkness.", IGNISIA, 591)
		it.chatNpc("How else may I be of assistance?", IGNISIA, 588)
		when (it.options("Tell me about the Wintertodt.", "Tell me about the Pyromancers.", "Tell me about yourself.", "I'm fine thanks.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_pyromancers(it)
			3 -> tell_me_about_yourself(it)
			4 -> im_fine_thanks(it)
		}
	}
	
	@Suspendable fun im_fine_thanks(it: Script) {
		it.chatNpc("May the sacred flame guide you brother.", IGNISIA, 588)
	}
	
}
