package nl.bartpelle.veteres.content.areas.dungeons.asgarniaice

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/10/2016.
 */

object IceObjects {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(1738) @Suspendable {
			// Trapdoor down
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().equals(3008, 3150)) {
				it.delay(1)
				it.player().teleport(Tile(3007, 9550))
			}
		}
		r.onObject(10596) @Suspendable { it.delay(1); it.player().teleport(Tile(it.player().tile().x, 9555)) }
		r.onObject(10595) @Suspendable { it.delay(1); it.player().teleport(Tile(it.player().tile().x, 9562)) }
	}
	
}

