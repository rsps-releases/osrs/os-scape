package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.npcs.bosses.KrakenBoss
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle
import java.lang.ref.WeakReference

/**
 * Created by Situations on 4/4/2016.
 */

object EnormousTentacle {
	
	@JvmField public val onhit: Function1<Script, Unit> = s@ @Suspendable {
		if (it.npc().hidden()) return@s
		//We only want the NPC to transmog once
		if (it.npc().sync().transmog() != 5535) {
			it.npc().sync().transmog(5535)
			it.npc().animate(3860)
			it.npc().timers().extendOrRegister(TimerKey.COMBAT_ATTACK, 1)
			it.npc().combatInfo(it.npc().world().combatInfo(5535))
			it.npc().putattrib(AttributeKey.TARGET, WeakReference<Entity>(it.npc().attribOr<Hit>(AttributeKey.LAST_HIT_DMG, null).origin() as Entity))
		}
	}
	
	// When the tentacle dies...
	@JvmField public val death: Function1<Script, Unit> = @Suspendable {
		val npc = it.npc()
		// Reset the transmog to original and revert combatinfo
		npc.sync().transmog(5534)
		npc.combatInfo(npc.world().combatInfo(KrakenBoss.TENTACLE_WHIRLPOOL))
		
		// Wait.. custom respawn hook
		it.delay(12)
		
		// If our boss is being attacked, wake up and fight back!
		val boss = npc.attribOr<Npc>(AttributeKey.BOSS_OWNER, null)
		if (boss != null && boss.isNpc) {
			if (boss.alive()) {
				// transmog and attack boss target if valid
				if (npc.sync().transmog() == 5534) {
					npc.sync().transmog(5535)
					npc.animate(3860)
					npc.timers().extendOrRegister(TimerKey.COMBAT_ATTACK, 1)
					npc.combatInfo(it.npc().world().combatInfo(5535)) // Quickly replace scripts for retaliation before Java finishes processing.
					val target = boss.attribOr<WeakReference<Entity>>(AttributeKey.TARGET, WeakReference<Entity>(null)).get()
					if (target != null) {
						npc.attack(target)
					}
				}
			}
		}
	}
	
	// Combat script.
	@JvmField public val combat: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, false, 15)) {
				if (EntityCombat.attackTimerReady(npc)) {
					val tileDist = npc.tile().distance(target.tile())
					val delay = Math.max(1, (20 + (tileDist * 12)) / 25)
					
					npc.animate(npc.attackAnimation())
					npc.world().spawnProjectile(npc.tile().transform(1, 0), target, 162, 30, 30, 32, 10 * tileDist, 14, 5)
					
					if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
						target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.RANGE)
					} else {
						target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.RANGE) // Uh-oh, that's a miss.
					}
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
}
