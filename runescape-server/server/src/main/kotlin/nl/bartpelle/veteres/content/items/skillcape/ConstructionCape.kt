package nl.bartpelle.veteres.content.items.skillcape

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Jason MacKeigan on 2016-07-05 at 4:01 PM
 */
object ConstructionCape : CapeOfCompletionPerk(intArrayOf(1)) {
	
	override fun option(option: Int): Function1<Script, Unit> = {
		val player = it.player()
		
		when (option) {
			1 -> {
				CapeOfCompletion.boost(Skills.CONSTRUCTION, player)
			}
			2 -> {
				player.message("This feature is unavailable at this time.")
			}
		}
	}
	
}