package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.achievements.pvp.PVPDiaryHandler
import nl.bartpelle.veteres.content.areas.edgeville.dialogue.EmblemTrader
import nl.bartpelle.veteres.content.areas.edgeville.osrune.BMPrices
import nl.bartpelle.veteres.content.areas.edgeville.osrune.ZombieMonk
import nl.bartpelle.veteres.content.examineItemMessage
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.items.Pouches
import nl.bartpelle.veteres.content.items.VotingRewardToken
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.AttributeKey.*
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.Shop
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.services.advertisement.VoteRewardService
import nl.bartpelle.veteres.util.ItemsOnDeath
import nl.bartpelle.veteres.util.Varbit
import org.apache.logging.log4j.LogManager
import java.text.NumberFormat

/**
 * Created by Bart on 9/28/2015.
 */

object ShopInterface {
	
	@JvmStatic val logger = LogManager.getLogger(ShopInterface::class.java)
}

@Suspendable private fun buy(it: Script, player: Player, shop: Shop, requested: Int, itemid: Int, cost: Int) {
	
	var qty = requested
	if (qty > 10) {
		qty = Math.min(qty, shop.stockOf(itemid));
	}
	// May we even?
	if (player.ironMode() != IronMode.NONE) {
		val pvpinfshops = player.world().realm().isPVP && (shop.id() in 1337..1351 || shop.id() in intArrayOf(1353, 1405))
		if (!pvpinfshops && (shop.baseStockOf(itemid) < 1 || shop.stockOf(itemid) > shop.baseStockOf(itemid))) {
			player.message("Iron Men may not buy items that are over-stocked in a shop.")
			return
		}
	}
	
	val itemName = Item(itemid).name(player.world())
	
	if (Pouches.isPouch(itemid)) {//Rc Pouches
		if (player.hasItem(itemid)) {
			player.message("You can only own one $itemName")
			return
		}
		qty = 1
	}
	
	var currency: Int = shop.currency()
	var currencyName: String = if (shop.warriorTokenShop()) "tokens" else "coins"
	
	
	if (!it.player().world().realm().isPVP && shop.currency() > 1) {
		currency = shop.currency()
		currencyName = Item(currency).name(player.world())
		
		if (currency == 11849) {
			currencyName = "Marks of Grace"
		}
	}
	val mastery: Boolean = Equipment.masteryCape(itemid)
	val trimcape: Boolean = itemName.contains("cape(t)")
	var hood = -1
	if (shop.id() == 13 || shop.id() == 48) { // contains skillcapes, check room for hood
		if (mastery) {
			hood = itemid + if (trimcape) 1 else 2
		}
	}
	
	var left = qty
	while (left-- > 0) {
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			return
		}
		
		if (shop.id() == 13) { // contains skillcapes, check room for hood
			if (itemName.contains("cape(t)")) {
				if (player.inventory().freeSlots() < 2) {
					player.message("You need at least two free slots for the cape and hood.")
					return
				}
			}
		}
		
		if (player.inventory().remove(Item(currency, cost), false).failed()) {
			if (player.world().realm().isPVP && shop.id() != 51) {
				player.message("You don't have enough $currencyName. You can buy more at store.os-scape.com.")
			} else {
				player.message("You don't have enough $currencyName.")
			}
			return
		}
		
		if (player.inventory().add(Item(itemid), false).success()) {
			// Mastery cape stock doesn't decrease
			if (!mastery)
				shop.removeStock(itemid, 1)
			
			// Add a skillcape hood too.
			if (hood != -1)
				player.inventory().add(Item(hood), false)
		} else {
			player.inventory().add(Item(currency, cost), true)
			player.message("You don't have enough inventory space.")
			return
		}
	}
}

// Buying from the lost property shop
@Suspendable private fun buyLostItem(script: Script, player: Player, shop: Shop, itemid: Int, cost: Int) {
	// Note: loop and quanity removed. defaults to 1. also fixed currency of blood money
	var currency: Int = 13307
	var currencyName: String = "Blood money"
	
	if (player.inventory().remove(Item(currency, cost), false).failed()) {
		if (player.world().realm().isPVP) {
			player.message("You don't have enough $currencyName. You can buy more at store.os-scape.com.")
		} else {
			player.message("You don't have enough $currencyName.")
		}
		return
	}
	
	if (itemid == 19722 || (itemid >= 8844 && itemid <= 8850)) return // Defender (t) is not in Perdu's shop.
	
	// reduce stored lost items by 1
	for (untradId in ItemsOnDeath.RS_UNTRADABLES_LIST) {
		if (itemid == untradId) {
			if (LostPropertyShop.altarSavedProperty(player, itemid, -1)) {
				if (player.inventory().add(Item(itemid), false).success()) {
					shop.refreshFor(player) // don't reduce stock, there never was any! Only shown when opened.
					break
				} else {
					player.inventory().add(Item(currency, cost), true) // Add it back. Failed. Since the add was non-force, not all items can fit=fail.
					LostPropertyShop.altarSavedProperty(player, itemid, 1) // Add the lost-prop item back, it was reduced befor we bought regardless of success.
					player.message("You don't have enough inventory space.")
					break
				}
			}
		}
	}
}

// Buying an item from the Voting Reward shop
@Suspendable private fun buyVotingReward(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int) {
	var currencyName: String = "Voting Points"
	
	var left = amt
	while (left-- > 0) {
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			break
		}
		
		//Does our player have enough points?
		var points = player.attribOr<Int>(AttributeKey.VOTING_POINTS, 0)
		if (points < cost) {
			if (player.world().realm().isPVP)
				player.message("You don't have enough $currencyName. You can get more by typing ::vote.")
			break
		}
		
		if (player.inventory().add(Item(itemid), false).success()) {
//			shop.removeStock(itemid, 1)
			player.putattrib(AttributeKey.VOTING_POINTS, points - cost)
		} else {
			player.message("You don't have enough inventory space.")
			break
		}
	}
}

// Buying an item from the Voting Reward shop
@Suspendable private fun buyLoyaltyPoint(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int) {
	var currencyName: String = "Loyalty Points"
	
	var left = amt
	while (left-- > 0) {
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			break
		}
		
		//Does our player have enough points?
		val points = player.attribOr<Int>(AttributeKey.LOYALTY_POINTS, 0)
		
		if (points < cost) {
			if (player.world().realm().isPVP)
				player.message("You don't have enough $currencyName.")
			break
		}
		
		if (player.inventory().add(Item(itemid), false).success()) {
//			shop.removeStock(itemid, 1)
			player.putattrib(AttributeKey.LOYALTY_POINTS, points - cost)
		} else {
			player.message("You don't have enough inventory space.")
			break
		}
	}
}

// Buying an item from the Voting Reward shop
@Suspendable private fun buyHalloweenStore(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int) {
	var currencyName: String = "Souls"
	
	var left = amt
	while (left-- > 0) {
		
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			break
		}
		
		//Does our player have enough points?
		val points = player.attribOr<Int>(AttributeKey.HALLOWEEN_SOULS_COLLECTED, 0)
		
		if (points < cost) {
			if (player.world().realm().isPVP) {
				player.message("You don't have enough $currencyName.")
			}
			break
		}
		
		if (player.inventory().add(Item(itemid), false).success()) {
//			shop.removeStock(itemid, 1
			player.putattrib(AttributeKey.HALLOWEEN_SOULS_COLLECTED, points - cost)
		} else {
			player.message("You don't have enough inventory space.")
			break
		}
	}
}

// Buying an item from the Voting Reward shop
@Suspendable private fun buySlayerPoints(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int) {
	var currencyName = "Slayer Points"
	
	var left = amt
	while (left-- > 0) {
		
		if (itemid == 11865 && !player.inventory().has(11864)) {
			player.message("You must have a regular Slayer helm to imbue before purchasing this item.")
			return
		}
		
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			break
		}
		
		//Does our player have enough points?
		val points = player.varps().varbit(Varbit.SLAYER_POINTS)
		
		if (points < cost) {
			if (player.world().realm().isPVP) {
				player.message("You don't have enough $currencyName.")
			}
			break
		}
		
		if (player.inventory().add(Item(itemid), false).success()) {
			if (itemid == 11864 && AchievementDiary.diaryHandler(player) is PVPDiaryHandler) {
				AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(player, 32))
			}
//			shop.removeStock(itemid, 1
			if (itemid == 11865) {
				player.inventory().remove(Item(11864), true)
			}
			player.varps().varbit(Varbit.SLAYER_POINTS, points - cost)
		} else {
			player.message("You don't have enough inventory space.")
			break
		}
	}
}

// Buying an item from the Ensouled Heads shop
@Suspendable private fun buyEnsouledHeads(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int, currencyName: String) {
	val sOrNo = if (amt == 1) "" else "s"
	
	var left = amt
	while (left-- > 0) {
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			return
		}
		
		if (player.inventory().remove(Item(ZombieMonk.getCurrencyID(itemid), cost), false).failed()) {
			if (player.inventory().hasAny(532, 6729, 536, 526)) {
				player.message("This shop only takes noted bones. Use your bones on the Zombie Monk to note them.")
			} else {
				player.message("You don't have enough ${currencyName}.")
			}
			return
		}
		
		if (player.inventory().add(Item(itemid), false).success()) {
			shop.removeStock(itemid, 1)
		} else {
			player.inventory().add(Item(ZombieMonk.getCurrencyID(itemid), cost), true)
			player.message("You don't have enough inventory space.")
			return
		}
	}
}

// Buying an item from the Blood Money PKP Shop
@Suspendable private fun buyShop(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int, currencyName: String, currencyType: Int) {
	
	var left = amt

	if (left > 50000) {
		left = 50000
	}
	
	while (left-- > 0) {
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			return
		}
		
		if (player.inventory().full()) {
			player.message("You don't have enough inventory space to hold this item.")
			return
		}
		
		if (itemid == 11770 || itemid == 11771 || itemid == 11772 || itemid == 11773) {
			val getID = if (itemid == 11770) 6731 else if (itemid == 11771) 6733 else if (itemid == 11772) 6735 else 6737
			val def = player.world().definitions().get(ItemDefinition::class.java, getID)
			
			if (player.inventory().contains(Item(getID))) {
				if (player.inventory().remove(Item(currencyType, cost), false).failed()) {
					player.message("You don't have enough $currencyName.")
				} else {
					player.inventory().remove(Item(getID, 1), true)
					player.inventory().add(Item(itemid, 1), true)
					//shop.removeStock(itemid, 1)
					return
				}
			} else {
				player.message("You need a ${def.name} in order to imbue it.")
				return
			}
		} else {
			if (player.inventory().remove(Item(currencyType, cost), false).failed()) {
				player.message("You don't have enough $currencyName.")
				return
			} else {
				val def = player.world().definitions().get(ItemDefinition::class.java, itemid)
				if(amt > player.inventory().freeSlots() && def.noteModel <= 0 && def.notelink >= 1) {
					player.inventory().add(Item(def.notelink, 1), true)
				} else {
					player.inventory().add(Item(itemid, 1), true)
					//shop.removeStock(itemid, 1)
				}
			}
		}
	}
}

@Suspendable private fun buyAchievementShop(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int, currencyName: String, currencyType: Int) {
	
	var left = amt
	
	if (left > 1000) {
		left = 1000
	}
	
	while (left-- > 0) {
		if (itemid != 13069) {
			player.message("This item is currently unavailable.")
			return
		}
		
		if (player.inventory().full()) {
			player.message("You don't have enough inventory space to hold this item.")
			return
		}
		
		if (!AchievementDiary.diaryHandler(player).completedAllAchievements(player)) {
			player.message("You must have completed all achievement diary tasks to purchase this item.")
			return
		}
		
		if (player.inventory().remove(Item(currencyType, cost), false).failed()) {
			player.message("You don't have enough $currencyName.")
			return
		} else {
			player.inventory().add(Item(itemid, 1), true)
			
			if (player.inventory().addOrDrop(Item(13070), player).failed()) {
				player.message("The item you tried purchasing was dropped on the floor due to lack of inventory space.")
			}
//			shop.removeStock(itemid, 1)
		}
	}
}

// Buying an item from the bounty hunter shop
@Suspendable private fun buyBountyHunterShop(player: Player, shop: Shop, itemid: Int, cost: Int, amt: Int, currencyName: String) {
	var left = amt
	
	if (left > 1000) {
		left = 1000
	}
	
	while (left-- > 0) {
		if (shop.stockOf(itemid) < 1) {
			player.message("The shop has run out of stock.")
			return
		}
		
		if (player.inventory().full()) {
			player.message("You don't have enough inventory space to hold this item.")
			return
		}
		
		if (player.varps().varp(EmblemTrader.BOUNTY_HUNTER_SHOP_VARP) < cost) {
			player.message("You don't have enough $currencyName.")
			return
		} else {
			player.inventory().add(Item(itemid, 1), true)
//			shop.removeStock(itemid, 1)
			player.modifyNumericalAttribute(AttributeKey.BOUNTY_HUNTER_POINTS, -cost, 0)
			
			if (itemid == 12791 && AchievementDiary.diaryHandler(player) is PVPDiaryHandler) {
				AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(player, 38))
			}
			
			player.varps().varp(EmblemTrader.BOUNTY_HUNTER_SHOP_VARP, player.attribOr(AttributeKey.BOUNTY_HUNTER_POINTS, 0))
			return
		}
	}
}


@Suspendable private fun sell(script: Script, player: Player, shop: Shop, qty: Int, itemid: Int, basecost: Int) {
	val cost = basecost
	// May we even?
	if (player.ironMode() != IronMode.NONE) {
		if (shop.stockOf(itemid) < shop.baseStockOf(itemid)) {
			player.message("Iron Men may not take advantage of the pricing when a shop is under-stocked.")
			return
		}
	}
	
	val currency = when (player.world().realm().isPVP) {
		true -> 13307 // Blood money
		false -> if (shop.currency() > 1) shop.currency() else 995 // Coins or custom currency
	}
	
	var left = qty
	while (left-- > 0) {
		if (player.inventory().count(itemid) < 1) {
			return
		}
		player.inventory().remove(Item(itemid), false)
		if (player.inventory().add(Item(currency, cost), false).success()) {
			shop.removeStock(itemid, -1)
		} else {
			player.inventory().add(Item(itemid), true)
			player.message("You don't have enough inventory space.")
			return
		}
	}
}

@ScriptMain fun shopInterface(repo: ScriptRepository) {
	// Shop interface
	repo.onButton(178, 2) {
		//Bounty hunter shop buy
		val shop: Shop = it.player()[SHOP] ?: return@onButton
		val option: Int = it.player().attrib(BUTTON_ACTION)
		val itemId: Int = it.player().attrib(ITEM_ID)
		val player = it.player()
		val item = Item(itemId, 1)
		val def = item.unnote(player.world()).definition(player.world())
		val cost = shop.buyPriceOf(def.id)
		
		val currencyName = "bounty points"
		
		when (option) {
			1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
			2 -> buyBountyHunterShop(player, shop, itemId, cost, 1, currencyName)
			8 -> buyBountyHunterShop(player, shop, itemId, cost, 5, currencyName)
			9 -> buyBountyHunterShop(player, shop, itemId, cost, 10, currencyName)
			10 -> it.examineItemMessage(item)
			else -> it.message("This option is unavailable.")
		}
	}
	repo.onButton(100, 3) {
		// Buy item
		val shop: Shop = it.player()[SHOP] ?: return@onButton
		val player = it.player()
		val option: Int = it.player().attrib(BUTTON_ACTION)
		val itemId: Int = it.player().attrib(ITEM_ID)
		val item = Item(itemId, 1)
		val def = item.unnote(player.world()).definition(player.world())
		val cost = shop.buyPriceOf(def.id)
		val currency = shop.currency()
		
		var currencyName: String = if (shop.warriorTokenShop()) "tokens" else "coins"
		
		if (it.player().world().realm().isPVP) {
			currencyName = when {
                shop.currency() == 6529 -> "Tokkul"
                shop.id() == 69 -> "Voting Points"
                shop.id() == 53 -> "Loyalty Points"
                shop.id() == 54 -> "Souls"
                shop.id() == 10 -> "Slayer Points"
                else -> "Blood money"
            }
		}

		when {
            shop.id() == 69 -> when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
				2 -> buyVotingReward(it.player(), shop, itemId, cost, 1)
				3 -> buyVotingReward(it.player(), shop, itemId, cost, 5)
				4 -> buyVotingReward(it.player(), shop, itemId, cost, it.inputInteger("How many do you wish to buy?"))
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
            shop.id() == 53 -> when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
				2 -> buyLoyaltyPoint(it.player(), shop, itemId, cost, 1)
				3 -> buyLoyaltyPoint(it.player(), shop, itemId, cost, 5)
				4 -> buyLoyaltyPoint(it.player(), shop, itemId, cost, it.inputInteger("How many do you wish to buy?"))
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
            shop.id() == 54 -> when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
				2 -> buyHalloweenStore(it.player(), shop, itemId, cost, 1)
				3 -> buyHalloweenStore(it.player(), shop, itemId, cost, 5)
				4 -> buyHalloweenStore(it.player(), shop, itemId, cost, it.inputInteger("How many do you wish to buy?"))
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
            shop.id() == 55 -> when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
				2 -> buySlayerPoints(it.player(), shop, itemId, cost, 1)
				3 -> buySlayerPoints(it.player(), shop, itemId, cost, 5)
				4 -> buySlayerPoints(it.player(), shop, itemId, cost, it.inputInteger("How many do you wish to buy?"))
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
            shop.id() == 56 -> when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
				2 -> buyAchievementShop(it.player(), shop, itemId, cost, 1, currencyName, currency)
				3 -> buyAchievementShop(it.player(), shop, itemId, cost, 1, currencyName, currency)
				4 -> buyAchievementShop(it.player(), shop, itemId, cost, 1, currencyName, currency)
				5 -> buyAchievementShop(it.player(), shop, itemId, cost, 1, currencyName, currency)
				6 -> buyAchievementShop(it.player(), shop, itemId, cost, 1, currencyName, currency)
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
			shop.id() == 1337 -> when (option) {
				1 -> it.message("${def.name}: is free of charge!")
				2 -> buyShop(it.player(), shop, itemId, 0, 1, currencyName, currency)
				3 -> buyShop(it.player(), shop, itemId, 0, 5, currencyName, currency)
				4 -> buyShop(it.player(), shop, itemId, 0, 10, currencyName, currency)
				5 -> buyShop(it.player(), shop, itemId, 0, 50, currencyName, currency)
				6 -> {
					val amt = it.inputInteger( "How many would you like to buy?")
					buyShop(it.player(), shop, itemId, 0, amt, currencyName, currency)
				}
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
            else -> when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
				2 -> buyShop(it.player(), shop, itemId, cost, 1, currencyName, currency)
				3 -> buyShop(it.player(), shop, itemId, cost, 5, currencyName, currency)
				4 -> buyShop(it.player(), shop, itemId, cost, 10, currencyName, currency)
				5 -> buyShop(it.player(), shop, itemId, cost, 50, currencyName, currency)
				6 -> {
					val amt = it.inputInteger( "How many would you like to buy?")
					buyShop(it.player(), shop, itemId, cost, amt, currencyName, currency)
				}
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
        }
	}
	repo.onButton(300, 2) {
		//regular shops
		// Buy item
		val slot: Int = it.player().attrib(BUTTON_SLOT)
		val option: Int = it.player().attrib(BUTTON_ACTION)
		val itemid: Int = it.player().attrib(ITEM_ID)
		
		val shop: Shop = it.player()[SHOP] ?: return@onButton
		var currencyName: String = if (shop.warriorTokenShop()) "tokens" else "coins"
		
		if (it.player().world().realm().isPVP) {
			currencyName = when {
                shop.currency() == 6529 -> "Tokkul"
                shop.currency() == 1337 -> "Voting Points"
                else -> "Blood money"
            }
		} else if (shop.currency() == 6529) {
			currencyName = "Tokkul"
		} else if (shop.currency() == 12012) {
			currencyName = "golden nuggets"
		} else if (shop.currency() == 11849) {
			currencyName = "Marks of Grace"
		} else if (it.player().world().realm().isOSRune) {
			val ensouledHeadCost = ZombieMonk.getPriceOf(itemid)
			val sOrNo = if (ensouledHeadCost == 1) "" else "s"
			if (shop.currency() == 1337) {
				currencyName = if (itemid == 13510) "dragon bone$sOrNo"
				else if (itemid == 13492) "dagannoth bone$sOrNo"
				else if (itemid == 13474 || itemid == 13477) "big bone$sOrNo"
				else "bone$sOrNo"
			}
			if (shop.currency() == 13307) {
				currencyName = "Blood Money"
			}
		}
		
		if (shop.id() == 1352) {
			// lost property, doesn't use shop stock to obtain item information. generates it below.
			val player = it.player()
			val item = Item(itemid, 1)
			val def = item.unnote(player.world()).definition(player.world())
			val cost = shop.buyPriceOf(def.id)
			
			// Verify item id
			if (LostPropertyShop.getCount(player, itemid) == 0) {
				// INVALID
				return@onButton
			}
			when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
				2 -> buyLostItem(it, player, shop, itemid, cost)
				3 -> buyLostItem(it, player, shop, itemid, cost)
				4 -> buyLostItem(it, player, shop, itemid, cost)
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavalable.")
			}
			return@onButton
		}
		
		val item: Item = (if (slot > shop.stock().size) shop.playerStock()[slot - shop.stock().size - 1] ?: return@onButton else shop.stock()[slot - 1]) ?: return@onButton
		
		if (item.id() < 0) {
			return@onButton
		}
		
		val def = item.unnote(it.player().world()).definition(it.player().world())
		val cost = if (shop.warriorTokenShop()) shop.warriorTokenBuyPriceOf(def.id) else shop.buyPriceOf(def.id)
		val currency = shop.currency()
		
		//it.player().debug("slot %d id %d", slot, def.id)
		if (shop.id() == 13 || shop.id() == 48) { // contains skillcapes, check room for hood
			if (Item(itemid).name(it.player().world()).contains("cape(t)") || Equipment.masteryCape(itemid)) {
				if (it.player().inventory().freeSlots() < 2) {
					it.message("You need at least two free slots for the cape and hood.")
					return@onButton
				}
			}
		}
		if (shop.id() == 48 && itemid == 9756 && it.player().skills().xpLevel(Skills.RANGED) < 99) {
			it.message("You must have mastered this skill before you can buy the mastery cape.")
			return@onButton
		}
		
		// Verify item id
		if (itemid != item.id()) { //This is the issue
			//it.player().debug("Item id mismatch - %d v %d", itemid, item.id())
			return@onButton
		}
		
		// If our player is using the Voting Points exchange shop we..
		if (shop.id() == 1353) {
			val vostingCost = VotingRewardToken.getPriceOf(itemid)
			
			when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(vostingCost)} $currencyName.")
				2 -> buyVotingReward(it.player(), shop, itemid, vostingCost, 1)
				3 -> buyVotingReward(it.player(), shop, itemid, vostingCost, 5)
				4 -> buyVotingReward(it.player(), shop, itemid, vostingCost, 10)
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavalable.")
			}
			return@onButton
		}
		
		// If our player is using the Ensouled head shop we..
		if (shop.id() == 1354) {
			val ensouledHeadCost = ZombieMonk.getPriceOf(itemid)
			
			when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(ensouledHeadCost)} $currencyName.")
				2 -> buyEnsouledHeads(it.player(), shop, itemid, ensouledHeadCost, 1, currencyName)
				3 -> buyEnsouledHeads(it.player(), shop, itemid, ensouledHeadCost, 5, currencyName)
				4 -> buyEnsouledHeads(it.player(), shop, itemid, ensouledHeadCost, 10, currencyName)
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavalable.")
			}
			return@onButton
		}

		// If our player is using the Blood money PKP shop
		if (shop.id() == 1364 || shop.id() == 1365) {
			val bloodMoneyCost = BMPrices.getPriceOf(itemid)
			
			when (option) {
				1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(bloodMoneyCost)} Blood Money.")
				2 -> buyShop(it.player(), shop, itemid, bloodMoneyCost, 1, currencyName, currency)
				3 -> buyShop(it.player(), shop, itemid, bloodMoneyCost, 5, currencyName, currency)
				4 -> buyShop(it.player(), shop, itemid, bloodMoneyCost, 10, currencyName, currency)
				10 -> it.examineItemMessage(item)
				else -> it.message("This option is unavailable.")
			}
			return@onButton
		}
		
		// Else it must be a regular shop so we..
		when (option) {
			1 -> it.message("${def.name}: currently costs ${NumberFormat.getInstance().format(cost)} $currencyName.")
			2 -> buy(it, it.player(), shop, 1, itemid, cost)
			3 -> buy(it, it.player(), shop, 5, itemid, cost)
			4 -> buy(it, it.player(), shop, 10, itemid, cost)
			5 -> buy(it, it.player(), shop, 50, itemid, cost)
			6 -> buy(it, it.player(), shop, 100, itemid, cost)
			10 -> it.examineItemMessage(item)
			else -> it.message("This option is unavalable.")
		}
	}
	
	//Inventory interface
	repo.onButton(301, 0) {
		// Sell item
		val slot: Int = it.player().attrib(BUTTON_SLOT)
		val option: Int = it.player().attrib(BUTTON_ACTION)
		val itemId: Int = it.player().attrib(ITEM_ID)
		
		val shop: Shop = it.player()[SHOP] ?: return@onButton
		
		val item = it.player().inventory()[slot] ?: return@onButton
		val def = item.unnote(it.player().world()).definition(it.player().world())
		val cost = shop.sellPriceOf(def.id)
		
		// Verify item id
		if (itemId != item.id()) {
			return@onButton
		}
		
		if (option == 10) {
			it.examineItemMessage(item)
			return@onButton
		}
		
		if (!shop.canSellBack()) {
			it.player().message("You can't sell items to this shop.")
			return@onButton
		}
		
		if (shop.warriorTokenShop() ||/* shop.id() == 1 || */shop.id() == 51 || shop.id() == 1352 || shop.id() == 1354 || shop.id() == 1364 || shop.id() == 1365 || itemId == 6570) {
			it.player().message("You can't sell items to this shop.")
			return@onButton
		}
		
		// Can we even sell this?
		if ((!shop.generalStore() && !shop.sells(item.unnote(it.player().world()).id())) || !shop.sells(item.unnote(it.player().world()).id()) && (shop.specialPriceOf(itemId) <= 0) || itemId == 995 || itemId == 13307) {
			it.player().message("You can't sell this item to this shop.")
			return@onButton
		}
		
		if (shop.generalStore() && it.player().world().realm().isPVP) {
			it.player().message("You can't sell items to this shop.")
			return@onButton
		}
		
		// On economy worlds, you shouldn't sell untradables to the store.
		if (!it.player().world().realm().isPVP && !Item(itemId).tradable(it.player().world()) && !shop.sells(itemId)) {
			it.player().message("You can't sell this item.")
			return@onButton
		}
		
		val currencyName = when (it.player().world().realm().isPVP) {
			true -> "Blood money"
			false -> if (shop.currency() > 1) {
				if (shop.currency() == 11849) {
					"Marks of Grace"
				} else {
					Item(shop.currency()).name(it.player().world())
				}
			} else {
				"coins"
			}
		}
		
		when (option) {
			1 -> it.message("${def.name}: shop will buy for ${NumberFormat.getInstance().format(cost)} $currencyName.")
			2 -> sell(it, it.player(), shop, 1, itemId, cost)
			3 -> sell(it, it.player(), shop, 5, itemId, cost)
			4 -> sell(it, it.player(), shop, 10, itemId, cost)
			5 -> sell(it, it.player(), shop, 50, itemId, cost)
			10 -> it.examineItemMessage(item)
			else -> it.message("This option is unavalable.")
		}
	}
	
	repo.onInterfaceClose(100, s@ @Suspendable {
		val shop: Shop = it.player()[SHOP] ?: return@s
		shop.playerIdsViewing().remove(it.player().id())
		it.player().interfaces().closeById(301) // Inventory interface
	})
	
	repo.onInterfaceClose(178, s@ @Suspendable {
		val shop: Shop = it.player()[SHOP] ?: return@s
		shop.playerIdsViewing().remove(it.player().id())
		it.player().interfaces().closeById(301) // Inventory interface
	})
	
	// Register timer for restocking
	repo.onWorldInit { it.ctx<World>().timers().register(TimerKey.SHOP_RESTOCK, 100) }
	repo.onWorldTimer(TimerKey.SHOP_RESTOCK) {
		it.ctx<World>().restockShops(false)
		it.ctx<World>().timers().register(TimerKey.SHOP_RESTOCK, 100)
	}
	repo.onWorldInit { it.ctx<World>().timers().register(TimerKey.SHOP_RESTOCK_FAST, 10) }
	repo.onWorldTimer(TimerKey.SHOP_RESTOCK_FAST) {
		it.ctx<World>().restockShops(true)
		it.ctx<World>().timers().register(TimerKey.SHOP_RESTOCK_FAST, 10)
	}
	
	repo.onInterfaceClose(300) {
		it.player().interfaces().closeById(301) // Inventory interface
	}
	
	repo.onWorldInit @Suspendable {
		while (true) {
			it.delay(100)
			try {
				it.ctx<World>().allocator().cleanup()
			} catch (e: Exception) {
				ShopInterface.logger.error("Error during instances cleanup!", e)
			}
		}
	}
	
	repo.onWorldInit {
		// Announcement is only relevant on spawnscape 317\
		it.ctx<World>().timers().register(TimerKey.GLOBAL_ANNOUNCEMENT, 500)
	}
	
	repo.onWorldTimer(TimerKey.GLOBAL_ANNOUNCEMENT) {
		val world = it.ctx<World>()
		
		if (world.realm().isPVP) {
			when (world.random(8)) {
				0 -> world.filterableAnnounce("<col=1e44b3><img=22> Short on Blood money? Why not buy some! Visit ::store to stock up.")
				1 -> world.filterableAnnounce("<col=1e44b3><img=22> Want to try your luck on a Mystery Box? Check out ::store!")
				2 -> world.filterableAnnounce("<col=1e44b3><img=22> Looking for a pet? A boss pet? Buy one at ::store!")
				3 -> world.filterableAnnounce("<col=1e44b3><img=22> Want some FREE Blood money? Vote at ::vote!")
				4 -> world.filterableAnnounce("<col=1e44b3><img=22> Browse a variety of interesting items at ::store!")
				5 -> world.filterableAnnounce("<col=1e44b3><img=22> Don't miss out on free Blood money. Visit ::vote now.")
				6 -> world.filterableAnnounce("<col=1e44b3><img=22> Interested in buying or selling items? Head over to the Grand Exchange now!")
				7 -> world.filterableAnnounce("<col=1e44b3><img=22> Do you feel like your account might be at risk? Setup ::2fa now!")
			}
			
			world.timers().register(TimerKey.GLOBAL_ANNOUNCEMENT, 800)
		} else if (world.realm().isOSRune) {
			when (world.random(5)) {
			// Bart: I have temp disabled this, because they're not true 'yet' ;)
				1 -> world.filterableAnnounce("<col=1e44b3><img=22> Want to try your luck on a Mystery Box? Check out ::store!")
				2 -> world.filterableAnnounce("<col=1e44b3><img=22> Looking for a pet? A boss pet? Buy one at ::store!")
				3 -> world.filterableAnnounce("<col=1e44b3><img=22> Want to become a dicing host? Buy your equipment at ::store!")
				4 -> world.filterableAnnounce("<col=1e44b3><img=22> Need some help? Type ::staff to see a list of online staff members!")
				else -> world.filterableAnnounce("<col=1e44b3><img=22> Short on gold? Why not buy some! Visit ::store to stock up.")
			}
			
			world.timers().register(TimerKey.GLOBAL_ANNOUNCEMENT, 800)
		} else if (!world.realm().isDeadman && !world.server().config().hasPathOrNull("deadman.jaktestserver")) {
			when (world.random(1)) {
				0 -> world.filterableAnnounce("<col=1e44b3><img=22> You can buy various items at our store, ::store!")
				1 -> world.filterableAnnounce("<col=1e44b3><img=22> Want to try your luck on a Mystery Box? Visit ::store.")
			}
			
			world.timers().register(TimerKey.GLOBAL_ANNOUNCEMENT, 800)
		}
	}
	
	repo.onWorldInit {
		val world = it.ctx<World>()
		
		if (world.realm().isPVP)
			world.timers().register(TimerKey.VOTING_ANNOUNCEMENT, 100)
	}
	
	repo.onWorldTimer(TimerKey.VOTING_ANNOUNCEMENT) {
		val world = it.ctx<World>()
		
		if (VoteRewardService.TOTAL_VOTES_CLAIMED >= 10) {
			world.filterableAnnounce("<img=23><col=f9fe11><shad> Another ${VoteRewardService.TOTAL_VOTES_CLAIMED} votes have been claimed, type ::vote to claim yours now!</shad></col>")
			VoteRewardService.TOTAL_VOTES_CLAIMED = 0
		}
		
		world.timers().register(TimerKey.VOTING_ANNOUNCEMENT, 100)
	}
	
}