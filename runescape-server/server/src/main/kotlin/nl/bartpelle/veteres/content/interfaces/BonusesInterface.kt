package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.waitForInterfaceClose
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.EquipmentInfo

/**
 * Created by Bart on 10/2/2015.
 *
 * Handles the bonuses interface and stats panel.
 */

object BonusesInterface {
	
	@JvmStatic @Suspendable fun showEquipmentInfo(it: Script) {
		// Cannot do this while locked.
		if (it.player().locked())
			return
		
		// Has introduced dupes previously..
		it.player().stopActions(false)
		
		it.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
		it.player().interfaces().sendMain(84)
		it.player().interfaces().sendInventory(85)
		it.player().invokeScript(149, 5570560, 93, 4, 7, 1, -1, "Equip", "", "", "", "")
		it.player().interfaces().setting(85, 0, 0, 27, 1180674)
		it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(85) }
		
		BonusInterface.sendBonuses(it.player())
		it.waitForInterfaceClose(84)
		it.player().interfaces().closeMain()
		it.player().interfaces().closeById(85)
	}
	
	object BonusInterface {
		fun plusify(bonus: Int): String {
			return if (bonus < 0) bonus.toString() else "+$bonus"
		}
		
		fun sendBonuses(player: Player) {
			val b = EquipmentInfo.totalBonuses(player, player.world().equipmentInfo())
			val undead: Int = 0
			var slay: Int = 0
			if (player.equipment().get(0) != null) {
				val helmitem: Item = player.equipment().get(0);
				val helmid = helmitem.id()
				val hname: String = helmitem.definition(player.world()).name
				if (helmid == 11864 || helmid == 19647 || helmid == 19643 || helmid == 19639) { // Normal slayer helm
					slay += 15
				} else if (hname.startsWith("Black mask")) {
					slay += 15
				} else if (hname.toLowerCase().contains("slayer helmet (i)")) { // 15% from normal and 15% from imbue
					slay += 30
				}
			}
			// TODO undead - salve amulet
			
			player.write(
					InterfaceText(84, 23, "Stab: ${plusify(b.stab)}"),
					InterfaceText(84, 24, "Slash: ${plusify(b.slash)}"),
					InterfaceText(84, 25, "Crush: ${plusify(b.crush)}"),
					InterfaceText(84, 26, "Magic: ${plusify(b.mage)}"),
					InterfaceText(84, 27, "Range: ${plusify(b.range)}"),
					
					InterfaceText(84, 29, "Stab: ${plusify(b.stabdef)}"),
					InterfaceText(84, 30, "Slash: ${plusify(b.slashdef)}"),
					InterfaceText(84, 31, "Crush: ${plusify(b.crushdef)}"),
					InterfaceText(84, 33, "Range: ${plusify(b.rangedef)}"),
					InterfaceText(84, 32, "Magic: ${plusify(b.magedef)}"),
					
					InterfaceText(84, 35, "Melee strength: ${plusify(b.str)}"),
					InterfaceText(84, 36, "Ranged strength: ${plusify(b.rangestr)}"),
					InterfaceText(84, 37, "Magic damage: ${plusify(b.magestr)}%"),
					
					InterfaceText(84, 38, "Prayer: ${plusify(b.pray)}"),
					InterfaceText(84, 40, "Undead: $undead%"),
					InterfaceText(84, 41, "Slayer: $slay%")
			)
		}
	}
	
	@JvmStatic @ScriptMain fun bonuses_buttons(repo: ScriptRepository) {
		repo.onButton(387, 17) @Suspendable { showEquipmentInfo(it) }
		
		// Unequipping
		repo.onButton(84, 11) @Suspendable { Equipment.unequip(it.player(), EquipSlot.HEAD, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 12) @Suspendable { Equipment.unequip(it.player(), EquipSlot.CAPE, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 13) @Suspendable { Equipment.unequip(it.player(), EquipSlot.AMULET, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 14) @Suspendable { Equipment.unequip(it.player(), EquipSlot.WEAPON, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 15) @Suspendable { Equipment.unequip(it.player(), EquipSlot.BODY, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 16) @Suspendable { Equipment.unequip(it.player(), EquipSlot.SHIELD, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 17) @Suspendable { Equipment.unequip(it.player(), EquipSlot.LEGS, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 18) @Suspendable { Equipment.unequip(it.player(), EquipSlot.HANDS, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 19) @Suspendable { Equipment.unequip(it.player(), EquipSlot.FEET, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 20) @Suspendable { Equipment.unequip(it.player(), EquipSlot.RING, false); BonusInterface.sendBonuses(it.player()) }
		repo.onButton(84, 21) @Suspendable { Equipment.unequip(it.player(), EquipSlot.AMMO, false); BonusInterface.sendBonuses(it.player()) }
		
		// Equipping
		repo.onButton(85, 0, s@ @Suspendable {
			val slot: Int? = it.player()[AttributeKey.BUTTON_SLOT] ?: return@s
			val option: Int = it.player()[AttributeKey.BUTTON_ACTION]
			val item = it.player().inventory()[slot!!] ?: return@s
			
			when (option) {
				1 -> {
					Equipment.equip(it.player(), slot, item, false)
					BonusInterface.sendBonuses(it.player())
				}
				10 -> it.player().message(it.player().world().examineRepository().item(item.id()))
			}
			
		})
	}
	
}
