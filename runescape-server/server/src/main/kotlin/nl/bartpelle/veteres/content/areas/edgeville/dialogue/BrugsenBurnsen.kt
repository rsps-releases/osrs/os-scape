package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by JasonMacKeigan on 2016-07-01.
 *
 * The purpose of this Kotlin file is to represent the dialogue between the
 * non-playable character Brugsen Burnsen and the player to establish a
 * teleport from the edgeville location to the grand exchance location.
 */
object BrugsenBurnsen {
	
	/**
	 * The tile that the non-playable character is standing on in the grand exchange
	 */
	val GRAND_EXCHANCE_LOCATION = Tile(3165, 3474)
	
	/**
	 * The tile that the non-playable character is standing on in edge ville
	 */
	val EDGEVILLE_LOCATION = Tile(3086, 3501)
	
	/**
	 * The numerical value which represents the non-playable character.
	 */
	val BRUGSEN_BURNSEN_ID = 2152
	
	@ScriptMain @JvmStatic fun register(repository: ScriptRepository) {
		repository.onNpcOption1(BRUGSEN_BURNSEN_ID, talk)
	}
	
	val talk: Function1<Script, Unit> = suspend@ @Suspendable {
		val player = it.player()
		
		if (player.tile().inSqRadius(EDGEVILLE_LOCATION, 5)) {
			it.chatNpc("Hello, would you like to go to the Grand Exchange?", BRUGSEN_BURNSEN_ID)
		} else {
			it.chatNpc("Funny, I cannot remember how to teleport players.", BRUGSEN_BURNSEN_ID)
			return@suspend
		}
		val option = it.options("Yes", "No")
		
		if (option == 1) {
			player.teleport(GRAND_EXCHANCE_LOCATION.transform(0, -1))
		}
	}
}