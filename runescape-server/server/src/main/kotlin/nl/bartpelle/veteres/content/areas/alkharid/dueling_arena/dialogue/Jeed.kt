package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Jeed {
	
	val JEED = 3351
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(JEED) @Suspendable {
			val random = it.player().world().random(6)
			
			if (random == 0) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Hi!", JEED, 567)
			}
			if (random == 1) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Why did the skeleton burp?", JEED, 567)
				it.chatPlayer("I don't know?", 575)
				it.chatNpc("'Cause it didn't have the guts to fart!", JEED, 605)
			}
			if (random == 2) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Waaaaassssssuuuuupp?!", JEED, 567)
			}
			if (random == 3) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("What did the skeleton say before it ate?", JEED, 567)
				it.chatPlayer("I don't know?", 575)
				it.chatNpc("Bone-appetit.", JEED, 605)
			}
			if (random == 4) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Well. This beats doing the shopping!", JEED, 567)
			}
			if (random == 5) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("My son just won his first duel!", JEED, 567)
				it.chatPlayer("Congratulations!", 567)
				it.chatNpc("He ripped his opponent in half!", JEED, 567)
				it.chatPlayer("That's gotta hurt!, 571")
				it.chatNpc("He's only 10 as well!", JEED, 567)
				it.chatPlayer("You gotta start 'em young!", 567)
			}
		}
	}
	
}
