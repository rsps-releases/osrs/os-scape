package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Mack on 11/26/2017.
 */
object Toby {
	
	const val ID = 5525
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onNpcOption1(ID, @Suspendable {
			init(it)
		})
	}
	
	@Suspendable fun init(it: Script) {
		it.chatNpc("'Ello, ${it.player().name()}. What can 'er do for ye?", ID)
		when (it.options("Talk about Achievements", "View Shop", "View Achievement Diaries", "Nevermind.")) {
			1 -> {
				talkAchievements(it)
			}
			2 -> {
				openShop(it)
			}
			3 -> {
				AchievementDiary.displayTasks(it)
			}
		}
	}
	
	@Suspendable fun talkAchievements(it: Script) {
		it.chatPlayer("What are 'Achievement Diaries'?")
		it.chatNpc("Achievement diaries are sets of tasks players can do around the server. Each task has a different requirement in order to complete.", ID)
		it.chatNpc("Players can view their current progress on achievement tasks by clicking the green star in their quest journal. Upon displayFormattedProgression of", ID)
		it.itemBox("an achievement you will be awarded a sum of blood money depending on the difficulty of the task. After you've completed all achievements", 13316)
		it.doubleItemBox("you can visit my shop to purchase the mastery cape. The cape can only be worn by those who have fully completed all tasks.", 13069, 13070)
		it.chatNpc("Many achievements take place within the Wilderness where you are vulnerable to losing items so be on the lookout for player killers and other threats.", ID)
		it.chatNpc("Give these tasks your best efforts and good luck!", ID)
		it.chatPlayer("Alright, thanks!")
	}
	
	@Suspendable fun openShop(it: Script) {
		it.player().world().shop(56).display(it.player())
	}
}