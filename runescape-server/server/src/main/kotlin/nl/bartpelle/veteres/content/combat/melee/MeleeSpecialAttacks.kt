package nl.bartpelle.veteres.content.combat.melee

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.items.equipment.TheaterOfBloodItems
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.content.npcs.bosses.abyssalsire.AbyssalSireState
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.*
import java.lang.ref.WeakReference

/**
 * Created by Bart on 3/12/2016.
 */
object MeleeSpecialAttacks {
	
	/**
	Claws statistics:
	Max str (+104 str bonus)
	Peity & aggressive- 41
	Peity & accurate - 40
	Ulimate str + aggressive - 39
	
	Mechanics:
	First hits: max, max/2, max/4, max/4+1 (30-15-7-8)
	1st miss, 2nd hit: max, 0, max/2, max/2 (30-0-15-15)
	1st & 2nd miss, 3rd hits: 0,0, max,max (0-0-30-30)
	First 3 miss, 4th hits: 0, 0, 0, max*(SPECIAL MULTPLIER) (0-0-0-30*m) ... below
	
	if base max is 41 (1.0 dmg multiplier)
	when m=1.15 0-0-0-47 (biggest I've seen on rs in 2011)
	when m=1.5 0-0-0-60 (says on a fansite but can't be possible)
	
	if base max = 32.8 (0.8 multiplier)
	when m=1.15 0-0-0-37
	 */
	@JvmStatic var CLAWS_MULTIPLIER = 0.8
	
	@Suspendable fun doSpecial(it: Script, target: Entity): Boolean {
		val weapon = it.player().equipment()[3] ?: return false
		when (weapon.id()) {
			4153, 12848 -> {
				queueGraniteMaulSpecial(it.player())
				it.player().timers()[TimerKey.COMBAT_ATTACK] = 3
				return true
			}
			1305 -> {
				// Dragon long
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 25))
					return false
				
				it.animate(1058)
				it.player().graphic(248, 92, 0)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var max = CombatFormula.maximumMeleeHit(it.player()) * 1.15
						max *= multi(it.player(), target, CombatStyle.MELEE, false)
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 25 * 10
						return true
					}
				}
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.25)) {
					var max = CombatFormula.maximumMeleeHit(it.player()) * 1.15
					max *= multi(it.player(), target, CombatStyle.MELEE, false)
					val damage = it.player().world().random().nextInt(max.toInt())
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			1215, 1231, 5680, 5698 -> {
				// Dragon dagger spec
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 25))
					return false
				
				it.animate(1062)
				it.player().graphic(252, 92, 0)
				it.player().world().spawnSound(it.player().tile(), 2537, 0, 10)
				
				for (i in 0..1) {
					val delay: Int = if (target.isPlayer) 1 else if (i == 0) 1 else 2
					var max = CombatFormula.maximumMeleeHit(it.player()) * 1.15
					max *= multi(it.player(), target, CombatStyle.MELEE, false)
					
					if (target.isNpc) {
						val npc = target as Npc
						if (npc.id() == 2668) {
							for (i in 0..1) attackTarget(npc, it.player(), max.toInt(), 1)
							it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 25 * 10
							return true
						}
					}
					
					val damage = it.player().world().random(max.toInt())
					if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.35)) {
						val hit: Hit = target.hitpvp(it.player(), damage, delay, CombatStyle.MELEE).block(false).submit().addXp()
						
						// 25% chance to poison every hit!
						if (weapon.id() in intArrayOf(5680, 5698)) { // Ensure we're using poison daggers
							if (it.player().world().rollDie(100, 25)) {
								target.poison(6)
							}
						}
						target.blockHit(hit)
					} else {
						val hit = target.hitpvp(it.player(), 0, delay, CombatStyle.MELEE).block(false).submit()
						target.blockHit(hit)
					}
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			13265, 13267, 13269, 13271 -> {
				// Abyssal dagger special
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(3300)
				it.player().graphic(1283, 0, 0)
				it.sound(2537)
				it.sound(2537) // yes same sound twice on 07
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 0.85
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						for (i in 0..1) attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				val damage = it.player().world().random().nextInt(max.toInt())
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.25)) {
					for (i in 0..1) {
						val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
						
						if (weapon.id() in intArrayOf(13269, 13271)) { // Ensure we're using poison daggers
							// 25% chance to poison every hit!
							if (it.player().world().rollDie(100, 25)) {
								target.poison(6)
							}
						}
						target.blockHit(hit)
					}
				} else {
					val hit1 = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					val hit2 = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit1)
					target.blockHit(hit2)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			11802, 20368 -> {
				// AGS
				
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(7644)
				it.player().graphic(1211)
				it.player().world().spawnSound(it.player().tile(), 3869, 0, 10)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.375 // 125% multiplied by the hidden 110% every godsword possesses
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				val damage = it.player().world().random().nextInt(max.toInt())
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.20)) {
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			11804, 20370 -> {
				// BGS Bandos godsword
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(7642)
				it.player().graphic(1212)
				it.player().world().spawnSound(it.player().tile(), 3869, 0, 10)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.21
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				val damage = it.player().world().random().nextInt(max.toInt())
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.0)) {
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
					
					val skills = intArrayOf(Skills.DEFENCE, Skills.STRENGTH, Skills.PRAYER, Skills.ATTACK, Skills.MAGIC, Skills.RANGED)
					var deductionTotal = damage
					for (i in 0..skills.size) {
						if (deductionTotal <= 0) break
						var take = deductionTotal
						
						// Identify the targets current level in this stat
						val targetCurrentStat = if (target.isPlayer) target.skills().level(skills[i]) else intArrayOf(
								(target as Npc).combatInfo().stats.attack,
								target.combatInfo().stats.defence,
								target.combatInfo().stats.strength,
								0,
								target.combatInfo().stats.ranged,
								0,
								target.combatInfo().stats.magic
						)[i]
						
						if (targetCurrentStat - take < 0) // Cap the amount we can take away to that available.
							take = targetCurrentStat
						
						// Now reduce the stat.
						if (target.isPlayer) {
							target.skills().setLevel(skills[i], target.skills().level(skills[i]) - take)
						} else {
							when (i) {
								0 -> (target as Npc).combatInfo().stats.attack -= take
								1 -> (target as Npc).combatInfo().stats.defence -= take
								2 -> (target as Npc).combatInfo().stats.strength -= take
								4 -> (target as Npc).combatInfo().stats.ranged -= take
								6 -> (target as Npc).combatInfo().stats.magic -= take
							}
							
						}
						deductionTotal -= take
					}
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			11806, 20372 -> {
				// SGS
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(7640)
				it.player().graphic(1209)
				it.player().world().spawnSound(it.player().tile(), 3869, 0, 10)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.10
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				val damage = it.player().world().random().nextInt(max.toInt())
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.1)) {
					// Heal the player & restore prayer and hit the enemy
					it.player().heal(Math.max(10, damage / 2)) // Min heal is 10
					
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
					
					it.player().skills().alterSkill(Skills.PRAYER, Math.max(5, damage / 4)) // Min heal is 5 for prayer
				} else {
					val hit: Hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			11808, 20374 -> {
				// ZGS
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(7638)
				it.player().graphic(1210)
				it.player().world().spawnSound(it.player().tile(), 3869, 0, 10)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.10
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				var damage = it.player().world().random().nextInt(max.toInt())
				
				val success = AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.1)
				if (!success)
					damage = 0
				val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false) // Manual block animation
				if (!success)
					hit.graphic(Graphic(85, 100))
				hit.submit().addXp()
				target.blockHit(hit)
				if (success) {
					target.graphic(369)
					target.freeze(33, target)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			4587, 20000 -> {
				// Dragon scimitar
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 55))
					return false
				it.animate(1872)
				it.player().graphic(347, 100, 0)
				it.sound(2540)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 55 * 10
						return true
					}
				}
				
				val damage = it.player().world().random().nextInt(max.toInt())
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.25)) {
					if (target is Player) {
						val targ = target
						targ.varps().varbit(Varbit.PROTECT_FROM_MAGIC, 0)
						targ.varps().varbit(Varbit.PROTECT_FROM_MELEE, 0)
						targ.varps().varbit(Varbit.PROTECT_FROM_MISSILES, 0)
						targ.varps().varbit(Varbit.RETRIBUTION, 0)
						targ.varps().varbit(Varbit.REDEMPTION, 0)
						targ.timers().register(TimerKey.OVERHEADS_BLOCKED, 9)
						targ.looks().update()
					}
					// You can slash 40s with the d scim spec cos prayers are disabled first if you hit!
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true;
			}
			TheaterOfBloodItems.SCYTHE_OF_VITUR_CHARGED_ID -> {
				val scythe = it.player().equipment()[EquipSlot.WEAPON]
				val charges = scythe.property(ItemAttrib.CHARGES)
				if (charges > 0) {
					val targetSize = target.size().div(2)
                    for (i in 0 until targetSize) {
                        val modifier = if (i == 1) 0.50 else if (i == 2) 0.25 else 1.0
						val maxHit = (CombatFormula.maximumMeleeHit(it.player()).toDouble() * modifier).toInt()
                        val gfxId = when(Direction.of(target.tile().x - it.player().tile().x, target.tile().z - it.player().tile().z)) {
                            Direction.South, Direction.SouthEast -> 1172
                            Direction.North, Direction.NorthWest -> 506
                            Direction.East, Direction.NorthEast -> 478 // TODO look this over in AM
                            else -> 1231
                        }

                        it.player().animate(8056)
                        target.graphic(gfxId, 96, 20)
                        if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.05)) {
							target.hit(it.player(), it.player().world().random(maxHit), 0)
						} else {
							target.hit(it.player(), 0, 0)
						}
                        weapon.modifyProperty(ItemAttrib.CHARGES, -1, 0)
                        if (weapon.property(ItemAttrib.CHARGES) == 0) {
                            if (it.player().equipment().remove(scythe, true).success()) {
                                it.player().equipment().add(Item(TheaterOfBloodItems.SCYTHE_OF_VITUR_UNCHARGED_ID), true, 3)
                                it.player().message("Your scythe has run out of charges!")
                                break
                            }
                        }
                    }
					it.player.timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(scythe.id())
                    return true
				} else {
                    // This shouldn't be reached but we do it just cause YOLO
                    if (it.player().equipment().remove(scythe, true).success()) {
                        it.player().equipment().add(Item(TheaterOfBloodItems.SCYTHE_OF_VITUR_UNCHARGED_ID), true, 3)
                        it.player().message("Your scythe has run out of charges!")
                        return false
                    }
                }

				return false
			}
			1249, 11824 -> {
				// Dragon & zamorakian spear
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						it.player().message("You can't use this special against this Npc.")
						return false
					}
				}
				
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 25))
					return false
				
				// Since this weapon doesn't deal damage, manually extend the in-combat timer.
				target.timers().extendOrRegister(TimerKey.COMBAT_LOGOUT, 20)
				
				val already_speared = target.timers().has(TimerKey.SPEAR)
				
				// Only allow stunning players, stunning npcs is bound to cause issues.
				if (target.isPlayer) {
					
					val dx = it.player().tile().x - target.tile().x
					val dz = it.player().tile().z - target.tile().z
					
					if (already_speared) {
						// If the spear timer is already active, maybe the target was already speared in multi-combat, add to the queue.
						// We don't need to re-lock or re-stun, the affect cycle will do that.
						
						val list = PlayerCombat.pendingSpears(target)
						if (list.size < 5) {
							list.add(Direction.orthogonal(dx, dz).opposite())
						}
						target.putattrib(AttributeKey.SPEAR_MOVES, list)
						
						// Otherwise, if NOT LOCKED (maybe teleporting/pulling a lever?) apply the spear effect.
					} else if (!already_speared && !target.locked()) {

						val attacker = it.player()
						val attackerPid = attacker.pvpPid < target.pvpPid
						target.timers()[TimerKey.SPEAR] = if (attackerPid) 4 else 5
						val dir: Direction? = PlayerCombat.dragonSpearVictim(attacker, target)

						if (attackerPid) {// TODO what happens vs npcs, instant move or not?
							// Attacker has pid, instant move
							target.lockDelayDamage()
							target.pathQueue().clear()
							if (dir != null)
								PlayerCombat.dragonSpearMovement(dir, target)
						} else {
							target.executeScript {
								it.clearContext()
								it.delay(1)
								target.lockDelayDamage()
								target.pathQueue().clear() // victim has pid, but our engine order (scripts before timers) is diff to 07 so we need to let them move in the first attack tick
								if (dir != null)
									PlayerCombat.dragonSpearMovement(dir, target)
							}
						}
					}
				}
				it.animate(1064)
				it.player().graphic(253, 100, 0)
				it.sound(2544)
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = 6
				// We no longer have a target. Spearing someone actually resets cb.
				it.player().putattrib(AttributeKey.ENDCB_POSTSPEC, true)
				return true
			}
			12006 -> {
				// Tent whip
				
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(1658)
				target.graphic(341, 100, 0)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				val damage = it.player().world().random().nextInt(max.toInt())
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.0)) {
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
					
					target.freeze(8, it.player()) // 5 second freeze timer
					if (it.player().world().rollDie(100, 25)) {
						target.poison(4)
					}
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			4151, 12774, 12773 -> {
				// whip
				
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(1658)
				target.graphic(341, 100, 0)
				it.player().world().spawnSound(it.player().tile(), 2713, 0, 10)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				val damage = it.player().world().random().nextInt(max.toInt())
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.25)) {
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
					
					if (target.isPlayer) {
						val targ = target as Player
						val cur: Double = targ.attribOr<Double>(AttributeKey.RUN_ENERGY, 100.0)
						if (cur > 0.0) {
							val drain: Double = cur / 10
							if (drain > 0) {
								targ.setRunningEnergy((cur - drain), true)
								it.player().setRunningEnergy((it.player().attribOr<Double>(AttributeKey.RUN_ENERGY, 100.0) + drain), true)
							}
						}
					}
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}

			12809, 12808 -> {
				// Blessed sara sword
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 65))
					return false

				it.animate(1133)
				it.player().graphic(1213, 100, 0)
				it.player().sound(2537)
				target.graphic(1196, 30, 0)

				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.25
				val damage = it.player().world().random().nextInt(max.toInt())

				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.0)) {
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MAGIC).block(false).submit().addXp()
					target.blockHit(hit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}

				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}

			11838 -> {
				// Sara sword
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 100))
					return false
				
				it.animate(1132)
				it.player().graphic(1213, 100, 0)
				
				val maxHitMelee = it.player().world().random(CombatFormula.maximumMeleeHit(it.player()))
				val maxHitMagic = it.player().world().random(16)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTarget(npc, it.player(), CombatFormula.maximumMeleeHit(it.player()), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 100 * 10
						val hit: Hit = target.hit(it.player(), 16, 1, false)
						hit.submit()
						return true
					}
				}
				
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.0)) {
					val meleeHit: Hit = target.hitpvp(it.player(), maxHitMelee, 1, CombatStyle.MELEE).block(false).submit().addXp()
					val magicHit: Hit = target.hitpvp(it.player(), maxHitMagic, 1, CombatStyle.MAGIC).block(false).submit()
					it.player().skills().__addXp(Skills.MAGIC, (2 * magicHit.damage()).toDouble())
					target.blockHit(meleeHit)
					target.blockHit(magicHit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			11061 -> {
				// Ancient mace
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 100))
					return false
				
				it.animate(6147)
				it.player().graphic(1027)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
						max *= multi(it.player(), target, CombatStyle.MELEE, false)
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 100 * 10
						return true
					}
				}
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				val damage = it.player().world().random().nextInt(max.toInt())
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.0)) {
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
					
					if (target is Player) {
						target.skills().alterSkill(Skills.PRAYER, -hit.damage())
						it.player().skills().alterSkill(Skills.PRAYER, hit.damage())
					}
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			7158 -> {
				// Dragon 2h
				// TODO working offline atm haven't got wiki to hand
				it.animate(3157)
				
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			10887 -> {
				// Barrelchest anchor
				
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50)) {
					return false
				}
				
				val max = CombatFormula.maximumMeleeHit(it.player())
				val damage = it.player().world().random(max)
				val deduceVal = (damage * 0.10).toInt()
				
				it.animate(5870)
				it.player().graphic(1027)
				
				if (target.isPlayer) {
					
					var temp = it.player().world().random(4)
					
					if (temp == 1) {
						temp = 0
					} else if (temp == 2) {
						temp = 1
					} else if (temp == 3) {
						temp = 4
					} else {
						temp = 6
					}
					
					target.skills().alterSkill(temp, -deduceVal)
				}
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.10)) {
					target.hit(it.player(), damage, 0).combatStyle(CombatStyle.MELEE)
				} else {
					target.hit(it.player(), 0, 0).combatStyle(CombatStyle.MELEE)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			1434 -> {
				// Dragon mace
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 25))
					return false
				
				it.animate(1060)
				it.player().graphic(251, 75, 0)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var max = CombatFormula.maximumMeleeHit(it.player()) * 1.5
						max *= multi(it.player(), target, CombatStyle.MELEE, false)
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 25 * 10
						return true
					}
				}
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.25)) {
					var max = CombatFormula.maximumMeleeHit(it.player()) * 1.5
					max *= multi(it.player(), target, CombatStyle.MELEE, false)
					val damage = it.player().world().random().nextInt(max.toInt())
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			13576 -> {
				// Dragon warhammer
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				it.animate(1378)
				it.player().graphic(1292, 0, 0)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var max = CombatFormula.maximumMeleeHit(it.player()) * 1.5
						max *= multi(it.player(), target, CombatStyle.MELEE, false)
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.0)) {
					var max = CombatFormula.maximumMeleeHit(it.player()) * 1.5
					max *= multi(it.player(), target, CombatStyle.MELEE, false)
					val damage = it.player().world().random().nextInt(max.toInt())
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
					
					// Nerf a player's def if it's a player
					if (target is Player) {
						target.skills().alterSkill(Skills.DEFENCE, -(target.skills().level(Skills.DEFENCE) * 0.3).toInt())
					} else if (target is Npc) {
						target.combatInfo().stats.defence = Math.max(0, (target.combatInfo().stats.defence - (target.combatInfo().stats.defence * 0.3)).toInt())
					}
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			13652 -> {
				// Dragon claws :]
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50)) {
					return false
				}
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						attackTargetClaws(npc, it.player(), max)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				
				// Cap maximum damage to 34, aka 68 special attack. We don't want to match the power of AGS.. yet
				val hit = Math.min(max, it.player().world().random().nextInt(max.toInt()).toDouble())
				
				it.player().animate(7514)
				it.player().graphic(1171)
				
				val accuracyRolls = booleanArrayOf(false, false, false, false)
				for (n in 0..3) {
					accuracyRolls[n] = AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.35)
				}
				val damages = intArrayOf(0, 0, 0, 0)
				if (accuracyRolls[0]) { // First accuracy roll hits
					for (n in 1..4) {
						val dmg: Int = when (n) {
							1 -> (hit / 2).toInt()
							2 -> hit.toInt()
							3 -> Math.floor(hit / 4).toInt()
							4 -> (Math.floor(hit / 4) + 1).toInt()
							else -> 1
						}
						damages[n - 1] = dmg
					}
				} else if (accuracyRolls[1]) { // First didnt.. second did
					for (n in 1..4) {
						val dmg: Int = when (n) {
							1 -> hit.toInt()
							2 -> 0
							3 -> (hit / 2).toInt()
							4 -> (hit / 2).toInt()
							else -> 1
						}
						damages[n - 1] = dmg
					}
				} else if (accuracyRolls[2]) { // First two missed.. third hit
					for (n in 1..4) {
						val dmg: Int = when (n) {
							1 -> 0
							2 -> 0
							3 -> hit.toInt()
							4 -> { // Perform as "normal hits"
								// Cap maximum damage to 34, aka 68 special attack. We don't want to match the power of AGS.. yet
								val hit = Math.min(max * CLAWS_MULTIPLIER, it.player().world().random().nextInt(max.toInt()).toDouble())
								hit.toInt()
							}
							else -> 1
						}
						damages[n - 1] = dmg
					}
				} else if (accuracyRolls[3]) { // All first 3 failed, last one hit.. lucky us.
					for (n in 1..4) {
						val dmg: Int = when (n) {
							1, 2, 3 -> 0
							4 -> (hit * 1.1).toInt()
						// RS wiki pre-eoc said 50% dmg bonus.. however even WITH turmoil, max I've ever seen was 47.
							else -> 1
						}
						damages[n - 1] = dmg
					}
				}
				
				for (n in 1..4) {
					val delay = if (n > 2) 2 else 1
					
					if (damages[n - 1] > 0) {
						val hit: Hit = target.hitpvp(it.player(), damages[n - 1], delay, CombatStyle.MELEE).block(false).submit().addXp()
						target.blockHit(hit)
					} else {
						val hit = target.hitpvp(it.player(), 0, delay, CombatStyle.MELEE).block(false).submit()
						target.blockHit(hit)
					}
				}
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			in 13080..13101, 3204 -> {
				// Dragon & Crystal halberd
				
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 30)) {
					return false;
				}
				it.player().animate(1203)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var max = CombatFormula.maximumMeleeHit(it.player()) * 1.1
						max *= multi(it.player(), target, CombatStyle.MELEE, false)
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 30 * 10
						return true
					}
				}
				
				// TODO correct GFX, osrs id is diff from rs3
				/*var gfx: Int
				if(target.tile().z > it.player().tile().z)
					gfx = 283;
				else if(target.tile().z < it.player().tile().z)
					gfx = 282;
				else if(target.tile().x < it.player().tile().x)
					gfx = 285;
				else
					gfx = 284;
				it.player().graphic(gfx)*/
				var damages = if (target.size() > 1) 2 else 1
				while (damages-- > 0) {
					val delay: Int = 1
					var max = CombatFormula.maximumMeleeHit(it.player()) * 1.10
					max *= multi(it.player(), target, CombatStyle.MELEE, false)
					val damage = it.player().world().random().nextInt(max.toInt())
					if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.35)) {
						val hit: Hit = target.hitpvp(it.player(), damage, delay, CombatStyle.MELEE).block(false).submit().addXp()
						target.blockHit(hit)
					} else {
						val hit = target.hitpvp(it.player(), 0, delay, CombatStyle.MELEE).block(false).submit()
						target.blockHit(hit)
					}
				}
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			13263 -> { // Abyssal bludgeon
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50)) {
					return false
				}
				
				it.player().animate(3299)
				it.player().sound(2715, 10)
				it.player().sound(1930, 30)
				target.graphic(1284, 0, 15)
				
				var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
				max *= multi(it.player(), target, CombatStyle.MELEE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var damage: Double = max
						damage *= 1 + (((it.player().skills().xpLevel(Skills.PRAYER) - it.player().skills().level(Skills.PRAYER)) * 0.5) / 100.0)
						
						attackTarget(npc, it.player(), Math.round(damage).toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				var damage: Double = it.player().world().random().nextInt(max.toInt()).toDouble()
				damage *= 1 + (((it.player().skills().xpLevel(Skills.PRAYER) - it.player().skills().level(Skills.PRAYER)) * 0.5) / 100.0)
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.0)) {
					val hit: Hit = target.hitpvp(it.player(), Math.round(damage).toInt(), 1, CombatStyle.MELEE).block(false).submit().addXp()
					target.blockHit(hit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			21009 -> { // Dragon sword
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 40)) {
					return false;
				}
				
				it.animate(7515)
				it.player().graphic(1369, 92, 0)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var max = CombatFormula.maximumMeleeHit(it.player()) * 1.25
						max *= multi(it.player(), target, CombatStyle.MELEE, false)
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 40 * 10
						return true
					}
				}
				
				if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.25)) {
					var max = CombatFormula.maximumMeleeHit(it.player()) * 1.25
					max *= multi(it.player(), target, CombatStyle.MELEE, false)
					val damage = it.player().world().random().nextInt(max.toInt())
					// Generic damage type as such it cannot be protected against by Protection Prayers
					val hit: Hit = target.hitpvp(it.player(), damage, 1, CombatStyle.GENERIC).block(false).submit().addXp(CombatStyle.MELEE)
					target.blockHit(hit)
				} else {
					val hit = target.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
					target.blockHit(hit)
				}
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			21015 -> { // Dinh's Bulwark
				
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50)) {
					return false;
				}
				
				it.player.animate(7511)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
						max *= multi(it.player(), target, CombatStyle.MELEE, false)
						attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				
				val reachable = mutableListOf<Entity>()
				
				if (it.player.varps().varbit(Varbit.MULTIWAY_AREA) == 1) {
					val radius = it.player.bounds().enlarge(5)
					
					// Add npcs & players in bounds
					it.player.world().npcs().forEachInAreaKt(radius, { reachable.add(it) })
					it.player.world().players().forEachInAreaKt(radius, { reachable.add(it) })
					reachable.remove(it.player) // Since we're in bounds too
				} else {
					reachable.add(target)
				}
				
				// Attack whatever's reachable
				for (victim in reachable) {
					if (PlayerCombat.canAttack(it.player, victim, false)) {
						if (AccuracyFormula.doesHit(it.player(), victim, CombatStyle.MELEE, 1.20)) {
							var max = CombatFormula.maximumMeleeHit(it.player()).toDouble()
							max *= multi(it.player(), victim, CombatStyle.MELEE, false)
							val damage = it.player().world().random().nextInt(max.toInt())
							val hit: Hit = victim.hitpvp(it.player(), damage, 1, CombatStyle.MELEE).block(false).submit().addXp()
							victim.blockHit(hit)
						} else {
							val hit = victim.hitpvp(it.player(), 0, 1, CombatStyle.MELEE).block(false).submit()
							victim.blockHit(hit)
						}
					}
				}
				
				it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
				return true
			}
			21742 -> { //Granite hammer
                if (!PlayerCombat.takeSpecialEnergy(it.player(), 60)) {
                    return false
                }

                it.player().animate(1378)

                var max = CombatFormula.maximumMeleeHit(it.player()) * 1.0
                max *= multi(it.player(), target, CombatStyle.MELEE, false)

                if (target.isNpc) {
                    val npc = target as Npc
                    if (npc.id() == 2668) {

                        //Guaranteed 5 damage to the calc'd hit @see wiki
                        attackTarget(npc, it.player(), max.toInt() + 5, 1)
                        it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 60 * 10
                        return true
                    }
                }

                if (AccuracyFormula.doesHit(it.player(), target, CombatStyle.MELEE, 1.25)) {

                    //Guaranteed 5 damage to the calc'd hit @see wiki
                    val damage = it.player().world().random().nextInt(max.toInt()) + 5
	
	                // Generic damage type as such it cannot be protected against by Protection Prayers
                    val hit: Hit = target.hitpvp(it.player(), damage, 0, CombatStyle.GENERIC).block(false).submit().addXp(CombatStyle.MELEE)
                    target.blockHit(hit)
                } else {
	                val hit = target.hitpvp(it.player(), 0, 0, CombatStyle.MELEE).block(false).submit()
                    target.blockHit(hit)
                }

                it.player().timers()[TimerKey.COMBAT_ATTACK] = it.player().world().equipmentInfo().weaponSpeed(it.player().equipment()[3].id())
                return true
            }
		}
		return false
	}
	
	/**
	 * Calculates a Multipler to adjust our max hit. Considers the Slayer helm, your Slayer Target,
	 * the Corp Beast (50% dmg reduction vs non-Spear weapons) etc.
	 */
	@JvmStatic fun multi(player: Player, target: Entity?, style: CombatStyle, ignoreCorp: Boolean): Double {
		var mult: Double = 1.0
		if (target != null) {
			if (Equipment.targetIsSlayerTask(player, target)) {
				val headItem = player.equipment().get(EquipSlot.HEAD)
				// Black mask (0) and (10) and normal slayer helm.
				if (headItem != null) {
					if (IntRange(8901, 8921).contains(headItem.id()) || headItem.id() == 11864 || headItem.id() == 19647 || headItem.id() == 19643 || headItem.id() == 19639) { // black mask / normal slayer helm
						if (style == CombatStyle.MELEE) {
							mult += 0.166 // 16.6%
						}
						// mask/normal helm do not give range/mage bonuses
					} else if (IntRange(11774, 11784).contains(headItem.id())) { // black mask (i) gives 15% mage/range
						if (style == CombatStyle.MELEE) {
							mult += 0.166 // 16.6%
						} else {
							mult += 0.15 // other 2 styles are only 15% (ash confirmed on twitter)
						}
					} else if (intArrayOf(11865, 19641, 19645, 19649).contains(headItem.id())) { // helms (i)
						if (style != CombatStyle.MELEE) {
							mult += 0.3 // 15% (normal helm) + 15% (for the imbue) totalling 30% bonus for magic + range damage
						} else {
							mult += 0.166 // the normal 16.6%
						}
					}
				}
			}
			if (target.isNpc) {
				val npc = target as Npc
				
				//If we're attacking the Abyssal Sire's Respiratory system and it's not disoriented, reduce damage by 95%..
				if (npc.id() == 5914) {
					val combatState = npc.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.STASIS)
					if (combatState == AbyssalSireState.COMBAT) {
						if (npc.world().rollDie(2, 1))
							player.message("You can't deal much damage with those tentacles getting in the way.")
						mult -= 0.9
					}
				}
			}
			
			if (!ignoreCorp && Equipment.corpbeastArmour(player, target, player.world().equipmentInfo().weaponType(player.equipment()[EquipSlot.WEAPON]?.id() ?: -1))) {
				mult -= 0.5 // half the  multipler, meaning damage is 50% less.
			}
		}
		return mult
	}
	
	@JvmStatic fun attackTarget(target: Npc, player: Player, damage: Int, delay: Int) {
		val hit: Hit = target.hit(player, damage, delay, false)
		hit.submit()
		player.timers()[TimerKey.COMBAT_ATTACK] = player.world().equipmentInfo().weaponSpeed(player.equipment()[3].id())
	}
	
	@JvmStatic fun attackTargetClaws(target: Npc, player: Player, hit: Double) {
		player.animate(7514)
		player.graphic(1171)
		
		val damages = intArrayOf(0, 0, 0, 0)
		for (n in 1..4) {
			val dmg: Int = when (n) {
				1 -> (hit / 2).toInt()
				2 -> hit.toInt()
				3 -> Math.floor(hit / 4).toInt()
				4 -> (Math.floor(hit / 4) + 1).toInt()
				else -> 1
			}
			damages[n - 1] = dmg
		}
		
		for (n in 1..4) {
			val delay = if (n > 2) 2 else 1
			
			if (damages[n - 1] > 0) {
				val hit: Hit = target.hitpvp(player, damages[n - 1], delay, CombatStyle.MELEE).block(false).submit().addXp()
				target.blockHit(hit)
			} else {
				val hit = target.hitpvp(player, 0, delay, CombatStyle.MELEE).block(false).submit()
				target.blockHit(hit)
			}
		}
		player.timers()[TimerKey.COMBAT_ATTACK] = player.world().equipmentInfo().weaponSpeed(player.equipment()[3].id())
	}

	@JvmStatic fun queueGraniteMaulSpecial(player: Player) {
		val graniteMaulSpecials = player.attribOr<Int>(AttributeKey.GRANITE_MAUL_SPECIALS, 0)
		player.putattrib(AttributeKey.GRANITE_MAUL_SPECIALS, graniteMaulSpecials + 1)
		player.putattrib(AttributeKey.GRANITE_MAUL_TIMEOUT_TICKS, 5)
	}

	@JvmStatic fun checkGraniteMaul(player: Player) {
		var graniteMaulTimeoutTicks = player.attribOr<Int>(AttributeKey.GRANITE_MAUL_TIMEOUT_TICKS, 0)
		if (graniteMaulTimeoutTicks > 0) {
			player.putattrib(AttributeKey.GRANITE_MAUL_TIMEOUT_TICKS, graniteMaulTimeoutTicks - 1)
			if (player.attribOr<Int>(AttributeKey.GRANITE_MAUL_TIMEOUT_TICKS, 0) == 0) {
				player.putattrib(AttributeKey.GRANITE_MAUL_SPECIALS, 0)
			} else if (graniteMaulTimeoutTicks == 4)
			//1 tick less than 5 because it was subtracted
				autoAttackGraniteMaul(player)
		}
	}

	private fun autoAttackGraniteMaul(player: Player) {
		// Define our target as last entity we attacked
		val ref: WeakReference<Entity> = player.attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: WeakReference<Entity>(null)
		val target: Entity = ref.get() ?: return
		if (player.tile().level != target.tile().level)
			return
		val x = player.tile().x
		val y = player.tile().z
		if (target.size() == 1) {
			val targetX = target.tile().x
			val targetY = target.tile().z
			val diffX = Math.abs(x - targetX)
			val diffY = Math.abs(y - targetY)
			if (diffX + diffY != 1)
				return
		} else {
			val closestPos = Tile.getClosestPosition(player, target)
			val targetX = closestPos.x
			val targetY = closestPos.z
			val diffX = Math.abs(x - targetX)
			val diffY = Math.abs(y - targetY)
			if (diffX > 1 || diffY > 1)
				return
		}
		player.face(target)
	}

	@JvmStatic fun specialGraniteMaul(player: Player): Boolean {
		var graniteMaulSpecials = player.attribOr<Int>(AttributeKey.GRANITE_MAUL_SPECIALS, 0)
		if (graniteMaulSpecials == 0)
			return false

		val wep = player.equipment()[EquipSlot.WEAPON]
		if (wep == null || wep.id() != 4153 && wep.id() != 12848)
			return false

		player.putattrib(AttributeKey.GRANITE_MAUL_SPECIALS, 0)

		player.varps()[Varp.SPECIAL_ENABLED] = 0

		// Define our target as last entity we attacked
		val ref: WeakReference<Entity> = player.attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: WeakReference<Entity>(null)
		val target: Entity = ref.get() ?: return false

		// Check if target is attackable
		if (target.isNpc) {
			if ((target as Npc).combatInfo() == null) {
				player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
				return false
			}
		}

		if (target.dead() || !PlayerCombat.canAttack(player, target) || player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) != 0) {
			player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
			return false
		}

		// Make sure our target can be attacked in this location.
		val wereInAttackable = if (target.isNpc) true else player.attribOr<Boolean>(AttributeKey.ATTACK_OP, false)
		val inattackable: Boolean = if (target.isNpc) true else target.attribOr<Boolean>(AttributeKey.ATTACK_OP, false)
		if (!inattackable || !wereInAttackable) {
			player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
			return false
		}

		var tile = player.tile()
		if (!player.touches(target)) {
			if (player.frozen() || player.stunned()) {
				player.message("I can't reach that!")
				player.sound(154)
				return false
			} else {
				if (target is Npc && target.combatInfo()?.unstacked ?: false) {
					PlayerCombat.moveCloserNoPeek(player, target, tile)
				} else {
					tile = PlayerCombat.moveCloser(player, target, tile)
					if (!player.touches(target, tile)) {
						player.message("Warning: Since the maul's special is an instant attack, it will be wasted when used on a first strike.")
						return false
					}
				}
			}
		}
		player.face(target) // Look in the target's direction. Scary!

		var maxHitDummy = false
		if (target.isNpc) {
			val npc = target as Npc
			if (npc.id() == 2668)
				maxHitDummy = true
		}

		if(graniteMaulSpecials > 2)
			graniteMaulSpecials = 2

		for (i in 0 until graniteMaulSpecials) {
			if (maxHitDummy || takeGMaulSpecial(player, 50)) {

				val max = CombatFormula.maximumMeleeHit(player)
				val didhit: Boolean = AccuracyFormula.doesHit(player, target, CombatStyle.MELEE, 1.05)

				player.interfaces().closeMain()
				if (target.isPlayer) {
					val targ = target as Player
					targ.interfaces().closeMain()
				}
				player.face(target)
				player.animate(1667)
				player.graphic(340, 92, 0)
				player.world().spawnSound(player.tile(), 2715, 0, 10)

				// Make sure they are now classed as in combat.
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				target.putattrib(AttributeKey.LAST_DAMAGER, player)
				player.putattrib(AttributeKey.LAST_ATTACK_TIME, System.currentTimeMillis())
				player.putattrib(AttributeKey.LAST_TARGET, target)
				PlayerCombat.check_spec_and_tele(player, target)

				// Apply the hit, using a max hit prepared when you clicked the spec button, not the cycle after.
				val hit = if (maxHitDummy) max else player.world().random().nextInt(max)
				if (maxHitDummy || didhit) {
					val h: Hit = target.hit(player, hit, 1, false).combatStyle(CombatStyle.MELEE).applyProtection().pidAdjust().submit().addXp()
				} else {
					target.hit(player, 0, 1, false).combatStyle(CombatStyle.MELEE).applyProtection().pidAdjust().submit()
				}
			}
		}
		if(graniteMaulSpecials == 2)
			player.timers().extendOrRegister(TimerKey.COMBAT_ATTACK, 1)
		return true
	}

	private fun takeGMaulSpecial(player: Player, num: Int): Boolean {
		if (VarbitAttributes.varbiton(player, Varbit.INFSPEC)) {
			return true
		}
		if (player.varps()[Varp.SPECIAL_ENERGY] < num * 10) {
			return false
		}
		player.varps()[Varp.SPECIAL_ENERGY] = player.varps()[Varp.SPECIAL_ENERGY] - num * 10
		return true
	}


	/**
	 * Does the actual special attack. Called by spec button and normal melee combat system. Those system
	 * will perform distance checks etc.
	 */
	@JvmStatic fun executeGmaulSpec(it: Script, target: Entity) {
		
		val player = it.player()
		player.varps()[Varp.SPECIAL_ENABLED] = 0
		/*if (PlayerCombat.blockRushing(player, target)) {
			return
		}*/
		
		val max = CombatFormula.maximumMeleeHit(player)
		val didhit: Boolean = AccuracyFormula.doesHit(player, target, CombatStyle.MELEE, 1.05)
		
		var is_maxhit_dummy = false
		if (target.isNpc) {
			val npc = target as Npc
			if (npc.id() == 2668) {
				is_maxhit_dummy = true
			}
		}
		
		if (is_maxhit_dummy || PlayerCombat.takeSpecialEnergy(player, 50)) {
			player.interfaces().closeMain()
			if (target.isPlayer) {
				val targ = target as Player
				targ.interfaces().closeMain()
			}
			player.face(target)
			player.animate(1667)
			player.graphic(340, 92, 0)
			player.world().spawnSound(player.tile(), 2715, 0, 10)
			
			// Make sure they are now classed as in combat.
			target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
			target.putattrib(AttributeKey.LAST_DAMAGER, player)
			player.putattrib(AttributeKey.LAST_ATTACK_TIME, System.currentTimeMillis())
			player.putattrib(AttributeKey.LAST_TARGET, target)
			PlayerCombat.check_spec_and_tele(player, target)
			
			// Apply the hit, using a max hit prepared when you clicked the spec button, not the cycle after.
			val hit = if (is_maxhit_dummy) max else player.world().random().nextInt(max)
			if (is_maxhit_dummy || didhit) {
				// Slightly more accurate
				val h: Hit = target.hit(player, hit, 1, false).combatStyle(CombatStyle.MELEE).queueAlways(true).applyProtection().submit().addXp()
			} else {
				target.hit(player, 0, 1, false).combatStyle(CombatStyle.MELEE).queueAlways(true).applyProtection().submit()
			}
		}
	}
	
}