package nl.bartpelle.veteres.content.events.christmas.antisanta

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 12/23/2015.
 */

object SantasObjects {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Santa's table
		r.onObject(27102) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 60) {
				if (it.player().inventory().count(13346) != 4) {
					it.player().lock()
					it.animate(832)
					it.player().inventory() += 13346
					it.player().message("You take a present from the table.")
					it.delay(1)
					it.player().unlock()
				} else {
					it.chatPlayer("I don't think I need anymore of these.", 588)
				}
			} else {
				it.player().message("You don't need to take anything from the table right now.")
			}
		}
	}
	
}
