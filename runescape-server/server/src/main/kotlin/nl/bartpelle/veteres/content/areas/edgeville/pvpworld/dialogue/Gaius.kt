package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 3/10/2016.
 */

object Gaius {
	
	val GAIUS = 1173
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(GAIUS) {
			// Free shops 1
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(1347).display(it.player())
			} else {
				it.messagebox("This shop is only accessible from the PvP world.")
			}
		}
		r.onNpcOption2(GAIUS) {
			// Free shops 1
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(1348).display(it.player())
			} else {
				it.messagebox("This shop is only accessible from the PvP world.")
			}
		}
		r.onNpcOption3(GAIUS) {
			// Free shops 1
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(1349).display(it.player())
			} else {
				it.messagebox("This shop is only accessible from the PvP world.")
			}
		}
	}
}