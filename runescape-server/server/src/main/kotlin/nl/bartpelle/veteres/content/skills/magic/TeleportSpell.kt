package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.combat.magic.MagicSpell
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */
class TeleportSpell(actionButton: Int, spellTypeName: String, levelRequired: Int, xp: Double, teleportType: TeleportType, startAnim: Int = 714, startGfx: Int, dest: Tile, runesRequired: Array<Item>): MagicSpell {

    val button = actionButton
    private val startAnimation = startAnim
    private val startGraphic = startGfx
    private val spellType = spellTypeName
    private val levelReq = levelRequired
    private val teletype = teleportType
    private val runes = runesRequired
    private val destination = dest
    private val exp = xp

    @Suspendable override fun able(it: Script, alert: Boolean): Boolean {
        val player = it.player()
        if (player.locked()) return false
        if (player.skills().levels()[Skills.MAGIC] < levelReq) {
            if (alert) it.message("You need a Magic level of $levelReq to cast this spell.")
            return false
        }
        if (ClanWars.inInstance(player)) {
            if (alert) {
                player.message("You cannot teleport out of a clan war.")
            }
            return false
        }
        if (Staking.in_duel(player)) {
            if (alert) {
                player.message("You cannot teleport out of a duel.")
            }
            return false
        }
        if (WildernessLevelIndicator.isTutorialIsland(player.tile().region())) { // On tutorial island
            return false
        }
        if (!MagicCombat.has(player, runes(), false)) {
            return false
        }
        if (player.timers().has(TimerKey.TELEBLOCK)) {
            if (alert) {
                player.teleblockMessage()
            }
            return false
        }
        val wildLevel = if (teletype == TeleportType.ABOVE_20_WILD) 30 else 20
        if (WildernessLevelIndicator.wildernessLevel(player.tile()) > wildLevel) {
            if (alert) {
                player.message("A mysterious force blocks your teleport spell!")
                player.message("You can't use this teleport after level $wildLevel wilderness.")
            }
            return false
        }
        if (player.jailed()) {
            if (alert) {
                player.message("You can't leave the jail yet.")
            }
            return false
        }
        if (InfernoContext.inSession(player)) {
            player.message("You can't teleport out of The Inferno.")
            return false
        }
        //Is our player currently in an active Fight Cave?
        val inFightCavesMinigame = player.attribOr<Boolean>(AttributeKey.TZHAAR_MINIGAME, false)
        if (inFightCavesMinigame) {
            player.message("You can't teleport out of the Fight Caves.")
            return false
        }
        if (teletype == TeleportType.HOME_TELEPORT) {
            // Cannot do this in combat, come on. That'd be nigger.
            if (player.timers().has(TimerKey.COMBAT_LOGOUT) || PlayerCombat.inCombat(player)) {
                player.message("You can't cast this spell when in combat.")
                return false
            }
        }
        if (player.world().realm().isPVP && player.timers().has(TimerKey.BLOCK_SPEC_AND_TELE) && player.varps().varbit(Varbit.MULTIWAY_AREA) == 0) {
            player.message("<col=804080>Teleport blocked for ${(player.timers().left(TimerKey.BLOCK_SPEC_AND_TELE) * .6).toInt()} more secs after using spec at the start of a battle.")
            return false
        }
        return true
    }

    @Suspendable override fun execute(it: Script) {
        val player = it.player()

        if (!able(it, true)) return

        player.lock()
        player.stopActions(true)

        val endGfx = when(startGraphic) {
            111 -> -1
            else -> -1
        }
        val endAnimation = when(startAnimation) {
            714 -> 0xFFFF
            else -> 0xFFFF
        }

        val gfxHeight = when(spellType) {
            "modern" -> 92
            else -> 0
        }

        player.animate(startAnimation)
        player.sync().graphic(startGraphic, gfxHeight, 0)

        it.delay(4)

        MagicCombat.has(player, runes(), true)
        player.animate(endAnimation)

        player.teleport(destination.x + it.player().world().random(2), destination.z + it.player().world().random(2))
        player.skills().__addXp(Skills.MAGIC, exp)
        player.unlock()
    }

    override fun runes(): Array<Item> {
        return runes
    }
}