package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */
object Doomsayer {

    private const val ID = 6773

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onNpcOption1(ID, @Suspendable{
            talk(it)
        })
        sr.onNpcOption2(ID, @Suspendable {
            displaySettingMenu(it)
        })
    }

    @Suspendable fun talk(it: Script) {
        it.chatNpc("Hello there, ${it.player().name()}, what can I do for ye?", ID)
        when (it.options("Toggle a game setting", "Nevermind.")) {
            1 -> {
                displaySettingMenu(it)
            }
            2 -> {
                it.chatPlayer("Nevermind.")
            }
        }
    }

    @Suspendable fun displaySettingMenu(it: Script) {
        when (it.options("Toggle HP Overlay", "Nothing.")) {
            1 -> {
                toggle(it, Varbit.HP_OVERLAY_TOGGLED, "HP Overlay")
            }
        }
    }

    @Suspendable fun toggle(it: Script, varbit: Int, name: String = "") {
        val onOrOff = if (on(it.player(), varbit)) "<col=006400>ON</col>" else "<col=FF0000>OFF</col>"
        when (it.optionsTitled("Current status: $onOrOff", "Turn on", "Turn off", "Nevermind.")) {
            1 -> {
                it.player().varps().varbit(varbit, 1)
                it.chatNpc("The $name setting has been toggled on. Come speak to me again if you wish to turn it off.", ID)
            }
            2 -> {
                it.player().varps().varbit(varbit, 0)
                it.chatNpc("The $name setting has been toggled off. Come speak to me again if you wish to turn it on.", ID)
            }
            else -> it.chatPlayer("Nevermind.")
        }
    }

    fun on(player: Player, toggledVarbit: Int): Boolean {
        return (player.varps().varbit(toggledVarbit) == 1)
    }
}