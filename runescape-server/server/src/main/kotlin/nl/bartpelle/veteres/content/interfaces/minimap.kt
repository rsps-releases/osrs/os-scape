package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.Potions
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 8/21/2015.
 */
object minimap {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//Toggle Run on/off
		for (widgetId in intArrayOf(22, 77)) {
			repo.onButton(160, widgetId) {
				if (it.player().attribOr<Double>(AttributeKey.RUN_ENERGY, 0) >= 1.0) {
					it.player().varps()[Varp.RUNNING_ENABLED] = if (it.player().varps()[Varp.RUNNING_ENABLED] == 1) 0 else 1
				} else {
					it.message("You don't have enough energy left to run!")
				}
			}
		}
		
		//Cure Poison
		repo.onButton(160, 4, s@ @Suspendable {
			
			for (id in intArrayOf(2446, 175, 177, 179, 2448, 181, 183, 185, 5943, 5945, 5947, 5949, 5952, 5954, 5956, 5958,
					12913, 12915, 12917, 12919, 12913, 12915, 12917, 12919, 10925, 10927, 10929, 10931)) {
				val found = it.player().inventory().findFirst(id)
				
				if (found.first() != -1) {
					val slot = found.first()
					val pot = Potions.get(found.second().id())
					
					if (pot != null) {
						it.player().putattrib(AttributeKey.ITEM_ID, pot)
						it.player().putattrib(AttributeKey.ITEM_SLOT, slot)
						it.player().putattrib(AttributeKey.FROM_ITEM, it.player().inventory().get(slot))
						Potions.consume(it, pot, found.second().id())
						return@s
					}
				}
			}
			
			// If we reach this, we have nothing to cure us. :(
			it.player().message("You haven't got anything capable of curing your poison.")
		})
	}
	
}