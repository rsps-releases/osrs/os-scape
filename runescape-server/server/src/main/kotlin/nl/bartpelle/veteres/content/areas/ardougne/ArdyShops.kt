package nl.bartpelle.veteres.content.areas.ardougne

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 18/04/2016.
 */

object ArdyShops {
	
	val SILVER_TRADER = 1038
	val BAKER_TRADER = 1040
	val SILK_MERCHANT = 1043
	val FUR_TRADER = 1042
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOnNpc(SILVER_TRADER) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			if (item == 442) {
				offerNote(it, 442, 443)
			}
		}
		
		repo.onNpcOption1(SILVER_TRADER) @Suspendable {
			val cost = 500
			it.chatNpc("I'll buy all your Silver ore for " + cost + " gold each.", SILVER_TRADER)
			if (it.options("Sell all Silver ore for " + cost + " gold each", "No thank you.") == 1) {
				val amt = it.player().inventory().count(442)
				it.player().inventory().remove(Item(442, amt), true)
				it.player().inventory().add(Item(995, amt * cost), true)
				it.itemBox("You get " + amt * cost + " gold in return for your " + amt + " Silver ore.", 995)
			}
		}
		
		repo.onItemOnNpc(BAKER_TRADER) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			
			when (item) {
				1901 -> offerNote(it, 1901, 1902)
				1891 -> offerNote(it, 1891, 1892)
				2389 -> offerNote(it, 2309, 2310)
			}
		}
		
		repo.onItemOnNpc(SILK_MERCHANT) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			
			if (item == 950) {
				offerNote(it, 950, 951)
			}
		}
		
		repo.onNpcOption1(SILK_MERCHANT) @Suspendable {
			it.chatNpc("I'll buy all your silk for 60 gold each.", SILK_MERCHANT)
			if (it.options("Sell all Silk for 60 gold each", "No thank you.") == 1) {
				val amt = it.player().inventory().count(950)
				it.player().inventory().remove(Item(950, amt), true)
				it.player().inventory().add(Item(995, amt * 60), true)
				it.itemBox("You get " + amt * 60 + " gold in return for your " + amt + " Silk.", 995)
			}
		}
		
		repo.onItemOnNpc(FUR_TRADER) @Suspendable {
			// fur trader
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			if (item == 958) {
				offerNote(it, 958, 959)
			}
		}
		
		repo.onNpcOption1(FUR_TRADER) @Suspendable {
			it.chatNpc("I'll buy all your fur for 150 gold each.", FUR_TRADER)
			if (it.options("Sell all Fur for 150 gold each", "No thank you.") == 1) {
				val amt = it.player().inventory().count(958)
				it.player().inventory().remove(Item(958, amt), true)
				it.player().inventory().add(Item(995, amt * 150), true)
				it.itemBox("You get " + amt * 150 + " gold in return for your " + amt + " Fur.", 995)
			}
		}
	}
	
	@Suspendable fun offerNote(it: Script, raw: Int, noted: Int) {
		val player = it.player()
		
		when (it.options("Note one", "Note all")) {
			1 -> {
				player.inventory().remove(Item(raw, 1), false)
				player.inventory().add(Item(noted, 1), false)
			}
			2 -> {
				val amt = player.inventory().count(raw)
				player.inventory().remove(Item(raw, amt), false)
				player.inventory().add(Item(noted, amt), false)
			}
		}
		
		it.itemBox("Your item has been exchanged for notes.", raw)
	}
}