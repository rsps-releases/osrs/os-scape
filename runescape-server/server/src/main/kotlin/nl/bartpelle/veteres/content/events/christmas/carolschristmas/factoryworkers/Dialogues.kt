package nl.bartpelle.veteres.content.events.christmas.carolschristmas.factoryworkers

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.events.christmas.carolschristmas.CarolsChristmas
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-15.
 */

object Dialogues {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (worker in intArrayOf(7496, 7497, 7498, 7499, 7501, 7502, 7503, 7504)) {
			r.onNpcOption1(worker) @Suspendable {
				if (CarolsChristmas.getStage(it.player()) == 17) {
					it.chatPlayer("Do you know anything about Carol's likes and dislikes?<br>I want to make her a present.", 555)
					
					val randomMessage = it.player().world().random(1)
					val toy = it.player().attribOr<Int>(AttributeKey.CAROLS_TOY, 0)
					val toyColor = it.player().attribOr<Int>(AttributeKey.CAROLS_TOY_COLOR, 0)
					
					when (randomMessage) {
						0 -> {
							when (toy) {
								0 -> it.chatNpc("Oh, well, let's just say she's quite averse to round thing.", worker, 589) //Ball
								1 -> it.chatNpc("Given how many times I've been poked, she likes getting her point across.", worker, 589) //Partyhat
								2 -> it.chatNpc("Carol definitely loves to play, whether it's puzzles or<br>sports it keeps her entertained.", worker, 589) //Marionette
							}
							
						}
						1 -> {
							when (toyColor) {
								0 -> it.chatNpc("I'm not really sure... I know she keeps going on about<br>" +
										"the clear blue skies around here. Don't know if that helps though.", worker, 590) //Blue
								1 -> it.chatNpc("I'm not really sure... I know she keeps going on about<br>" +
										"the beautiful red sunset around here. Don't know if that helps though.", worker, 590) //Red
								2 -> it.chatNpc("I'm not really sure... I know she keeps going on about<br>" +
										"the stunning green aura of the swamp. Don't know if<br>that helps though.", worker, 590) //Green
							}
						}
					}
				} else {
					val randomMessage = it.player().world().random(4)
					when (randomMessage) {
						0 -> it.chatNpc("I'm so tired.", worker, 610)
						1 -> it.chatNpc("Please don't distract me, I have so much work to do.", worker, 588)
						2 -> it.chatNpc("We used to love our jobs but Carol has turned in to a<br>" +
								"corporate machine and is only focussed on money these<br>days.", worker, 612)
						3 -> it.chatNpc("I've been on my feet for so long I can't even<br>remember what sleep feels like.", worker, 612)
						4 -> it.chatNpc("I'm sick and tired of listening to this festive music.", worker, 614)
					}
				}
			}
		}
	}
	
}