package nl.bartpelle.veteres.content.events.christmas.antisanta

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 12/22/2015.
 */

object AntiSantasObjects {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Anti-Santa's Machine
		r.onObject(27099) @Suspendable {
			if (it.player().inventory().contains(13352)) {
				if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 30) {
					it.chatPlayer("I'm not going near that again!")
				} else {
					it.player().lock()
					it.delay(1)
					it.animate(1649)
					it.messagebox("You pour the purified tears into the machine..")
					it.player().inventory() -= 13352
					it.messagebox("...the blizzard lets out a howl of sorrow...")
					it.player().varps().varbit(Varbit.XMAS2015_STAGE, 40)
					it.messagebox("...hideous creatures emerge from the frost!")
					it.player().unlock()
				}
			} else {
				it.chatPlayer("I don't think I should be messing around with this...")
			}
		}
		
		//Crate
		r.onObject(27100) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 20) {
				if (!it.player().inventory().contains(13347)) {
					it.animate(832)
					it.player().inventory() += 13347
					it.player().message("You take an empty vial from the crate.")
					
					// Reset the tear states
					it.player().varps().varbit(Varbit.XMAS2015_AUBURY_KID, 0)
					it.player().varps().varbit(Varbit.XMAS2015_EAST_KID, 0)
					it.player().varps().varbit(Varbit.XMAS2015_GENSTORE_KID, 0)
					it.player().varps().varbit(Varbit.XMAS2015_RANGESTORE_KID, 0)
				} else {
					it.player().message("You don't need another vial.")
				}
			} else {
				it.player().message("You don't need to take anything from the crate right now.")
			}
		}
		
		//Table
		r.onObject(27101) @Suspendable {
			if (it.player().varps().varbit(Varbit.XMAS2015_STAGE) == 20) {
				if (it.player().inventory().count(13345) != 4) {
					it.player().lock()
					it.animate(832)
					it.player().inventory() += 13345
					it.player().message("You take an anti-present from the table.")
					it.delay(1)
					it.player().unlock()
				} else {
					it.chatPlayer("I don't think I need anymore of these.", 588)
				}
			} else {
				it.player().message("You don't need to take anything from the table right now.")
			}
		}
	}
	
}
