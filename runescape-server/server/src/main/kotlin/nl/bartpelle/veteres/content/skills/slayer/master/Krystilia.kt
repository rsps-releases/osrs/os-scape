package nl.bartpelle.veteres.content.skills.slayer.master

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.skills.slayer.Slayer
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature
import nl.bartpelle.veteres.content.skills.slayer.SlayerRewards
import nl.bartpelle.veteres.content.skills.slayer.showSlayerRewards
import nl.bartpelle.veteres.content.slayerTaskAmount
import nl.bartpelle.veteres.content.slayerTaskId
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Mack on 9/29/2017.
 */
object Krystilia {
	
	const val KRYSTILIA = 7663
	const val BM_CANCEL_FEE = 2_500
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onNpcOption1(KRYSTILIA, @Suspendable {
			displayOptions(it)
		})
		sr.onNpcOption2(KRYSTILIA, @Suspendable {
			warn(it)
		})
		sr.onNpcOption3(KRYSTILIA, @Suspendable {
			it.player().world().shop(10).display(it.player())
			it.message("You currently have ${it.player().varps().varbit(Varbit.SLAYER_POINTS)} slayer points.")
		})
		sr.onNpcOption4(KRYSTILIA, @Suspendable {
			it.player().interfaces().showSlayerRewards()
		})
	}
	
	@Suspendable fun warn(it: Script) {
		it.chatNpc("Before I assign you anything, I want to make something clear. My tasks have to be done in the wilderness. Only kills inside the wilderness will count.", KRYSTILIA)
		it.chatNpc("If you don't like my tasks, you can come back to me and cancel your task for a fee.", KRYSTILIA)
		
		if (it.options("Yes, I understand I must kill it in the Wilderness.", "No thanks, I don't want tasks from you.") == 1) {
			it.chatPlayer("Yes, I understand I must kill it in the Wilderness.")
			assignTask(it)
		}
	}
	
	@Suspendable fun assignTask(it: Script) {
		val numleft: Int = it.slayerTaskAmount()
		val player = it.player()
		
		if (numleft > 0) {
			it.chatNpc("You're still hunting something. You currently have a task of $numleft ${Slayer.taskName(it.player(), it.slayerTaskId())} remaining.", KRYSTILIA)
			it.chatNpc("Come back to me when you've completed your task for a new one.", KRYSTILIA)
			displayTips(it)
			return
		}
		
		val def = Slayer.master(KRYSTILIA)!!.randomTask(player)
		var task_amt = player.world().random(def.range()).toDouble()
		
		SlayerRewards.multipliable.forEach { creature ->
			if (creature.first == def.creatureUid) {
				if (player.varps().varbit(creature.second) == 1) {
					task_amt *= 1.4
				}
			}
		}
		
		it.player().putattrib(AttributeKey.WILDERNESS_SLAYER_TASK_ACTIVE, true)
		player.varps().varbit(Varbit.SLAYER_MASTER, Slayer.KRYSTILIA_ID)
		player.putattrib(AttributeKey.SLAYER_TASK_ID, def.creatureUid)
		player.putattrib(AttributeKey.SLAYER_TASK_AMT, task_amt.toInt())
		Slayer.displayCurrentAssignment(player)
		
		val task: SlayerCreature = SlayerCreature.lookup(it.slayerTaskId())!!
		val num: Int = it.slayerTaskAmount()
		
		it.chatNpc("Ok, great. Your new task is to kill $num ${Slayer.taskName(it.player(), task.uid)}.", KRYSTILIA)
		displayTips(it)
	}
	
	@Suspendable fun displayTips(it: Script) {
		when (it.options("Got any tips for me?", "Ok, thanks!")) {
			1 -> {
				val tip = Slayer.tipFor(SlayerCreature.lookup(it.slayerTaskId())!!)
				it.chatNpc(tip, KRYSTILIA)
			}
			2 -> {
				it.chatPlayer("Ok, thanks!")
			}
		}
	}
	
	@Suspendable fun displayOptions(it: Script) {
		when (it.options("What is 'Wilderness Slayer'?", "I'd like a task, please.", "Can you cancel my current task for blood money?", "Nothing")) {
			1 -> {
				describeWildernessSlayer(it)
			}
			2 -> {
				warn(it)
			}
			3 -> {
				cancelForBloodMoney(it)
			}
		}
	}
	
	@Suspendable fun cancelForBloodMoney(it: Script) {
		it.chatPlayer("I'd like to cancel my current slayer task with blood money.")
		it.chatNpc("Certainly, just as a reminder, the fee is ${Krystilia.BM_CANCEL_FEE} blood money. This action is irreversible.", KRYSTILIA)
		when (it.optionsTitled("Cancel current slayer task?", "Yes, pay the ${Krystilia.BM_CANCEL_FEE} blood money cancel fee.", "Nevermind, I'll keep my money.")) {
			1 -> {
				if (it.player().inventory().has(Item(13307)) && it.player().inventory().byId(13307).amount() >= BM_CANCEL_FEE) {
					it.player().putattrib(AttributeKey.SLAYER_TASK_ID, 0)
					it.player().putattrib(AttributeKey.SLAYER_TASK_AMT, 0)
					it.player().inventory().remove(Item(13307, BM_CANCEL_FEE), true)
					it.itemBox("You hand over the blood money to Krystilia. She swiftly takes the money and performs her service.", 13315)
					it.chatNpc("There you go. Your current slayer task has been cleared. Come speak to me when you're ready for a new one.", KRYSTILIA)
				} else {
					it.chatNpc("Sorry, but it appears you do not have enough blood money to cover the cancellation fee.", KRYSTILIA)
					it.chatNpc("Come speak to me when you have enough blood money.", KRYSTILIA)
				}
			}
		}
	}
	
	@Suspendable fun describeWildernessSlayer(it: Script) {
		it.chatNpc("Wilderness Slayer is an activity that can be carried out by slaying selective monsters in the wilderness. Every task you complete will be rewarded with 25 slayer points and", KRYSTILIA)
		it.chatNpc("these points can be exchanged within a shop of mine. If you are unable to complete your slayer task or simply wish to change it you can do so by speaking to me. However, there is a fee to this service.", KRYSTILIA)
		it.chatNpc("The point you earn from slayer tasks can be used for various goods in my shop. Lastly, remember, ALL of these tasks are in the wilderness so beware!", KRYSTILIA)
	}
}
