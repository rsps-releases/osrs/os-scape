package nl.bartpelle.veteres.content.minigames.pest_control

/**
 * Created by Jason MacKeigan on 2016-07-20 at 11:30 AM
 *
 * A void knight can be represented as a non-playable character or npc where the parameter
 * for id is the unique id value for that character.
 */
enum class VoidKnight(private val id: Int) {
	NOVICE(2950),
	
	INTERMEDIATE(2951),
	
	VETERAN(2952)
}