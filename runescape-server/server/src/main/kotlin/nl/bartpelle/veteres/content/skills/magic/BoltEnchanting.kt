package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/5/2015.
 */

object BoltEnchanting {
	
	@ScriptMain @JvmStatic fun button(repo: ScriptRepository) {
		//Enchant Crossbow Bolt Interface
		registerSpell(4, repo, 80, 2, 4462, 759, 879, 9236, 10, 9.0, Item(564, 1), Item(556, 2)) //Opal
		registerSpell(7, repo, 80, 3, 4462, 759, 9337, 9240, 10, 17.0, Item(564, 1), Item(555, 1), Item(558, 1)) //Sapphire
		registerSpell(14, repo, 80, 4, 4462, 759, 9335, 9237, 10, 19.0, Item(564, 1), Item(557, 2)) //Jade
		registerSpell(24, repo, 80, 5, 4462, 759, 880, 9238, 10, 29.0, Item(564, 1), Item(556, 2)) //Pearl
		registerSpell(27, repo, 80, 6, 4462, 759, 9338, 9241, 10, 27.0, Item(564, 1), Item(556, 3), Item(561, 1)) //Emerald
		registerSpell(29, repo, 80, 7, 4462, 759, 9336, 9239, 10, 33.0, Item(564, 1), Item(554, 2)) //Red Topaz
		registerSpell(49, repo, 80, 8, 4462, 759, 9339, 9242, 10, 59.9, Item(564, 1), Item(554, 5), Item(565)) //Ruby
		registerSpell(57, repo, 80, 9, 4462, 759, 9340, 9243, 10, 67.0, Item(564, 1), Item(557, 10), Item(563, 2)) //Diamond
		registerSpell(68, repo, 80, 10, 4462, 759, 9341, 9244, 10, 78.0, Item(564, 1), Item(557, 15), Item(566, 1)) //Dragonstone
		registerSpell(87, repo, 80, 11, 4462, 759, 9342, 9245, 10, 97.0, Item(564, 1), Item(554, 20), Item(560, 1)) //Onyx
	}
	
	@Suspendable fun registerSpell(level: Int, repo: ScriptRepository, button: Int, child: Int, animation: Int, gfx: Int, bolt: Int, enchantedBolt: Int, amt: Int, exp: Double, vararg runes: Item) {
		repo.onButton(button, child) @Suspendable { enchant(it, level, animation, gfx, bolt, enchantedBolt, amt, exp, *runes) }
	}
	
	@Suspendable fun enchant(script: Script, level: Int, animation: Int, gfx: Int, bolt: Int, enchantedBolt: Int, amt: Int, exp: Double, vararg runes: Item) {
		
		if (script.player().world().realm().isPVP) {
			script.messagebox("That's a bit pointless is it not...?")
			return
		}
		//Get the amount of bolts in the players inventory
		val invAmt = script.player().inventory().count(bolt)
		
		//Check for level requirements
		if (script.player().skills().xpLevel(Skills.MAGIC) < level) {
			script.player().message("Your magic level is not high enough for this spell.")
			return
		}
		
		//Check for rune requirements
		if (!MagicCombat.has(script.player(), runes.asList().toTypedArray(), true))
			return
		
		if (invAmt >= 10) {
			//Remove the regular amt bolts, add the enchanted
			script.player().inventory() -= Item(bolt, amt)
			script.player().inventory() += Item(enchantedBolt, amt)
		} else {
			//Remove the rest of the players bolts
			script.player().inventory() -= Item(bolt, invAmt)
			script.player().inventory() += Item(enchantedBolt, invAmt)
		}
		
		script.player().interfaces().closeById(80)
		script.player().animate(animation)
		script.player().graphic(gfx)
		script.player().message("The magic of the runes coaxes out the true nature of the gem tips.")
		script.addXp(Skills.MAGIC, if (BonusContent.isActive(script.player, BlessingGroup.ARCANIST)) exp * 2 else exp)
	}
}