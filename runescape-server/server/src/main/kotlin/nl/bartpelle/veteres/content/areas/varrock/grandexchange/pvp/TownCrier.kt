package nl.bartpelle.veteres.content.areas.varrock.grandexchange.pvp

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-01-15.
 */

object TownCrier {
	
	val TOWN_CRIER = 277
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(TOWN_CRIER) @Suspendable {
			it.chatPlayer("Hi!")
			it.chatNpc("Hello there! How can I be of assistance today?", TOWN_CRIER, 554)
			when (it.options("What is this place?", "I don't need any help, thanks though.")) {
				1 -> {
					it.chatPlayer("What is this place?", 577)
					var loc = "Grand Exchange PvP"
					if (PVPAreas.CAMELOT_INSTANCE != null && PVPAreas.CAMELOT_INSTANCE!!.contains(it.targetNpc()))
						loc = "Camelot PvP"
					it.chatNpc("This is $loc, when fighting behind me your protect item prayer doesn't work!", TOWN_CRIER)
					it.chatPlayer("So.. I lose all my items if I fight behind you?")
					it.chatNpc("Yes, that's exactly what I said!", TOWN_CRIER, 554)
				}
				2 -> {
					it.chatPlayer("I don't need any help, thanks though.")
					it.chatNpc("Suit yourself.", TOWN_CRIER)
				}
			}
		}
	}
	
}
