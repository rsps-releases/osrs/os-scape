package nl.bartpelle.veteres.content.interfaces

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 *
 * @author Savions Sw, the legendary.
 * @author Mack - Code modifications. Updated autocast support on Surge Spell addition update.
 *
 */

object SpellSelect {
	
	@JvmStatic val MODERN_CHILDREN = intArrayOf(2, 5, 7, 9, 11, 15, 18, 21, 25, 28, 34, 39, 46, 49, 54, 57, 1, 32, 43, 44, 61, 63, 68, 70, 42)
	@JvmStatic val MODERN_LEVEL_REQS = intArrayOf(1, 5, 9, 13, 17, 23, 29, 35, 41, 47, 53, 59, 62, 65, 70, 75, 1, 50, 60, 60, 81, 85, 90, 95, 60)
	@JvmStatic val MODERN_ITEM_REQS = arrayOf(
			arrayOf(Item(556, 1), Item(558, 1)),
			arrayOf(Item(555, 1), Item(556, 1), Item(558, 1)),
			arrayOf(Item(557, 2), Item(556, 1), Item(558, 1)),
			arrayOf(Item(554, 3), Item(556, 1), Item(558, 1)),
			arrayOf(Item(556, 2), Item(562, 1)),
			arrayOf(Item(555, 2), Item(556, 2), Item(562, 1)),
			arrayOf(Item(556, 2), Item(557, 3), Item(562, 1)),
			arrayOf(Item(554, 3), Item(556, 2), Item(562, 1)),
			arrayOf(Item(556, 3), Item(560, 1)),
			arrayOf(Item(555, 3), Item(556, 3), Item(560, 1)),
			arrayOf(Item(557, 4), Item(556, 3), Item(560, 1)),
			arrayOf(Item(554, 5), Item(556, 4), Item(560, 1)),
			arrayOf(Item(556, 5), Item(565, 1)),
			arrayOf(Item(556, 5), Item(555, 7), Item(565, 1)),
			arrayOf(Item(557, 7), Item(556, 5), Item(565, 1)),
			arrayOf(Item(554, 7), Item(556, 5), Item(565, 1)), // 16th spell ([15])
			arrayOf(Item(557, 2), Item(556, 2), Item(562)), // crumble undead
			arrayOf(Item(560, 1), Item(558, 4)), // [17] magic dart
			arrayOf(Item(554), Item(556, 4), Item(565, 2)), // claws of guth
			arrayOf(Item(554, 4), Item(565, 2), Item(556)), // [19] zammy
            arrayOf(Item(556, 7), Item(21880, 1)), //Wind surge
            arrayOf(Item(555, 10), Item(556, 7), Item(21880, 1)), //Water Surge
            arrayOf(Item(557, 10), Item(556, 7), Item(21880, 1)), //Earth Surge
            arrayOf(Item(554, 10), Item(556, 7), Item(21880, 1)), //Fire Surge
            arrayOf(Item(554, 2), Item(565, 2), Item(556, 4)) //Sara strike
	)
	
	val ANCIENT_CHILDREN = intArrayOf(80, 84, 76, 72, 82, 86, 78, 74, 81, 85, 77, 73, 83, 87, 79, 75)
	val ANCIENT_LEVEL_REQS = intArrayOf(50, 52, 56, 58, 62, 64, 68, 70, 74, 76, 80, 82, 86, 88, 92, 94)
	val ANCIENT_ITEM_REQS = arrayOf(
			arrayOf(Item(562, 2), Item(560, 2), Item(556, 1), Item(554, 1)),
			arrayOf(Item(562, 2), Item(560, 2), Item(556, 1), Item(566, 1)),
			arrayOf(Item(562, 2), Item(560, 2), Item(565, 1)),
			arrayOf(Item(562, 2), Item(560, 2), Item(555, 2)),
			arrayOf(Item(562, 4), Item(560, 2), Item(554, 2), Item(556, 2)),
			arrayOf(Item(562, 4), Item(560, 2), Item(556, 2), Item(566, 2)),
			arrayOf(Item(562, 4), Item(560, 2), Item(565, 2)),
			arrayOf(Item(562, 4), Item(560, 2), Item(555, 4)),
			arrayOf(Item(560, 2), Item(565, 2), Item(554, 2), Item(556, 2)),
			arrayOf(Item(560, 2), Item(565, 2), Item(556, 2), Item(562, 2)),
			arrayOf(Item(560, 2), Item(565, 4)),
			arrayOf(Item(560, 2), Item(565, 2), Item(555, 3)),
			arrayOf(Item(560, 4), Item(565, 2), Item(554, 4), Item(556, 4)),
			arrayOf(Item(560, 4), Item(565, 2), Item(556, 4), Item(566, 3)),
			arrayOf(Item(560, 4), Item(565, 4), Item(566, 1)),
			arrayOf(Item(560, 4), Item(565, 2), Item(555, 6)))
	
	
	@JvmStatic fun reset(player: Player, resetSpell: Boolean, sendTab: Boolean) {
		if (resetSpell) {
			player.varps().varbit(Varbit.AUTOCAST_SELECTED, 0)
		}
		if (sendTab) {
			player.interfaces().sendWidgetOn(593, Interfaces.InterSwitches.COMBAT_TAB)
		}
		player.updateWeaponInterface()
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(593, 20) { open(true, it.player()) }
		r.onButton(593, 24) { open(false, it.player()) }
		
		r.onButton(201, 1) {
			handleAutoButton(it)
		}
		
		r.onLogin { it.player().varps().varbit(Varbit.AUTOCAST_SELECTED, 0) }
	}
	
	fun open(defensive: Boolean, player: Player) {
		if (canCast(player, true)) {
			player.interfaces().sendWidgetOn(201, Interfaces.InterSwitches.COMBAT_TAB)
			val item = player.equipment().get(EquipSlot.WEAPON)
			var id = -1
			var hasancient = false
			if (Equipment.fullAhrim(player) && Equipment.hasAmmyOfDamned(player)) {
				hasancient = true
			}
			
			if (player.varps().varbit(Varbit.SPELLBOOK) == 1 && (item.id() in intArrayOf(6914, 12422, 4675, 11791, 12902, 12904, 21006) || hasancient)) { // Go to ancients
				id = 4675
			} else if (player.varps().varbit(Varbit.SPELLBOOK) == 0) { // If on modern, go to modern (master wand can do BOTH)
				id = -1 // Default
			}
			
			if (item.id() == 1409) { // Ibans staff
				id = 1409
			} else if (item.id() == 22296) {
				id = 22296;
			} else if (item.id() == 4170 || item.id() == 21255) { // slayer staff
				id = 4170
			} else if ((item.id() in intArrayOf(11791, 12902, 12904, 2417)) && player.varps().varbit(Varbit.SPELLBOOK) == 0) { // sotd is modern only
				id = 11791
			} else if (item.id() in intArrayOf(8841, 2416)) {
				id = 8841
			}

			player.varps().varp(664, id)
			player.write(InterfaceSettings(201, 1, 0, 52, 2))
			player.varps().varbit(2668, if (defensive) 1 else 0);
			//276
		}
	}
	
	fun handleAutoButton(it: Script) {
		var slot = it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, 0)
		val player = it.player()
		if (!canCast(player, false)) {
			SpellSelect.reset(player, true, true)
			return
		}
		
		if (slot == 0) {
			SpellSelect.reset(player, true, true)
		} else {
			val book = player.varps().varbit(Varbit.SPELLBOOK)
            val spellId = when(book) {
                0 -> {
                    if (slot < 48) {
                        slot - 1
                    } else {
                        slot - 28
                    }
                }
                else -> slot - 31
            }
			if (spellId < 0 || spellId > 24) {
				player.message("Bad spell selected. (%d, %d)", spellId, slot)
				return
			}
			val defensive = player.varps().varbit(2668) == 1
			
			val requiredItems = if (book == 0) MODERN_ITEM_REQS[spellId] else ANCIENT_ITEM_REQS[spellId]
			val level = if (book == 0) MODERN_LEVEL_REQS[spellId] else ANCIENT_LEVEL_REQS[spellId]
			
			if (!MagicCombat.has(player, requiredItems, false)) {
				return
			}
			if (player.skills().xpLevel(Skills.MAGIC) < level) {
				player.message("Your Magic level is not high enough for this spell.")
				return
			}
			player.varps().varbit(Varbit.AUTOCAST_SELECTED, slot)
			player.interfaces().sendWidgetOn(593, Interfaces.InterSwitches.COMBAT_TAB)
			player.updateWeaponInterface()
		}
	}
	
	fun canCast(player: Player, message: Boolean): Boolean {
		val item = player.equipment().get(EquipSlot.WEAPON) ?: return false
		val book = player.varps().varbit(Varbit.SPELLBOOK)
		
		if (book == 2) {
			if (message)
				player.message("You can't autocast lunar magic.")
			return false
		}
		
		val ancients = book == 1
		
		if ((!ancients && item.id() in intArrayOf(4675)) || (ancients && item.id() !in intArrayOf(4675, 6914, 11791, 12902, 12904, 21006))) {
			if (Equipment.fullAhrim(player) && Equipment.hasAmmyOfDamned(player)) {
				return true
			}
			if (message)
				player.message("You cannot autocast with this staff while being on your current spellbook.")
			return false
		}
		
		return true
	}
}
