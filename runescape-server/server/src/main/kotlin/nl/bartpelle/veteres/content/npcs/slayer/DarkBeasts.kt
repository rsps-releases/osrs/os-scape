package nl.bartpelle.veteres.content.npcs.slayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 4/19/2016.
 */

object DarkBeasts {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 7) && EntityCombat.attackTimerReady(npc)) {
				if (EntityCombat.canAttackMelee(npc, target, true)) { // Melee attack is 100% if in range.
					primary_melee_attack(npc, target)
				} else {
					// Only if out of melee distance.
					primate_magic_attack(npc, target)
				}
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	
	@Suspendable fun primary_melee_attack(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, EntityCombat.randomHit(npc))
		else
			target.hit(npc, 0)
		
		npc.animate(npc.attackAnimation())
	}
	
	@Suspendable fun primate_magic_attack(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
		val delay = Math.max(1, (50 + (tileDist * 12)) / 30)
		
		npc.animate(npc.attackAnimation())
		npc.world().spawnProjectile(npc, target, 130, 40, 30, 50, 12 * tileDist, 14, 5)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC))
			target.hit(npc, EntityCombat.randomHit(npc), delay).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, delay)
		
		target.graphic(131, 80, delay * 26)
	}
}