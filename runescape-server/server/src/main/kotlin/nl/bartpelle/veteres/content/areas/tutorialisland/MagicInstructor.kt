package nl.bartpelle.veteres.content.areas.tutorialisland

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.Realm
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 11/18/2016.
 */
object MagicInstructor {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(3309) @Suspendable {
			it.chatNpc("Hello there, ${it.player.name()}. Are you ready to go to the mainland? Make sure you have " +
					"correctly chosen the Iron Man mode that you'd like to play!", 3309)
			
			val adventure = when (it.player.ironMode()) {
				IronMode.REGULAR -> " as an <img=2>Iron Man"
				IronMode.ULTIMATE -> " as an <img=3>Ultimate Iron Man"
				IronMode.HARDCORE -> " as a <img=10>Hardcore Iron Man"
				else -> ""
			}
			
			if (it.options("Yes! Start my adventure$adventure!", "No, not yet.") == 1) {
				if (it.player.world().realm() == Realm.OSRUNE) {
					it.player.teleport(3095, 3510) // Teleport to Edgeville. Starter given on acc creation!
					RuneScapeGuide.giveIronManSuit(it.player.ironMode(), it.player.inventory()) // Give armour (if any)
				} else if (it.player.world().realm() == Realm.REALISM) {
					if (it.player.ironMode() == IronMode.NONE) {
						RuneScapeGuide.decide(it, it.player.mode()) // Give normal starter pack
					} else {
						RuneScapeGuide.decide(it, it.player.ironMode()) // Give Iron man starter pack + armour
					}
				}
			}
		}
	}
	
}