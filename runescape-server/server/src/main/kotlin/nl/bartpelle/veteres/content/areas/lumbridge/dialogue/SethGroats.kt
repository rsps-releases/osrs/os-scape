package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/17/2015.
 */

object SethGroats {
	
	val SETH_GROATS = 1351
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(SETH_GROATS) @Suspendable {
			it.chatNpc("M'arnin'....going to milk me cowsies!", 1351, 588)
		}
	}
	
}
