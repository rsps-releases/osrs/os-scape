package nl.bartpelle.veteres.content.combat.ranged

enum class BowReqs(val bow: Int, val arrows: IntArray) {
	KBOW(4734, intArrayOf(4740)),
	SHORTBOW(841, intArrayOf(882, 884)),
	LONGBOW(839, intArrayOf(882, 884)),
	
	OAK_SHORT(843, intArrayOf(882, 884)),
	OAKLONG(845, intArrayOf(882, 884)),
	
	WILLOW_SHORT(849, intArrayOf(882, 884, 886)),
	WILLOWLONG(847, intArrayOf(882, 884, 886)),
	
	MAPLE_SHORT(853, intArrayOf(882, 884, 886, 888, 890)),
	MAPLELONG(851, intArrayOf(882, 884, 886, 888, 890)),
	
	YEW_SHORT(857, intArrayOf(882, 884, 886, 888, 890, 892)),
	YEWLONG(855, intArrayOf(882, 884, 886, 888, 890, 892)),
	
	MAGIC_SHORT(861, intArrayOf(882, 884, 886, 888, 890, 892, 4160, 21326)),
	MAGICLONG(859, intArrayOf(882, 884, 886, 888, 890, 892, 4160, 21326)),
	MAGIC_SHORT_I(12788, intArrayOf(882, 884, 886, 888, 890, 892, 4160, 21326)),
	
	SEERCULL(6724, intArrayOf(882, 884, 886, 888, 890, 892, 4160)),
	
	THIRD_AGE_BOW(12424, intArrayOf(882, 884, 886, 888, 890, 892, 11212, 4160, 21326)),
	
	DBOW(11235, intArrayOf(882, 884, 886, 888, 890, 892, 11212, 4160, 21326)),
	DBOW2(12765, intArrayOf(882, 884, 886, 888, 890, 892, 11212, 4160, 21326)), // Cosmetic
	DBOW3(12766, intArrayOf(882, 884, 886, 888, 890, 892, 11212, 4160, 21326)), // Cosmetic
	DBOW4(12767, intArrayOf(882, 884, 886, 888, 890, 892, 11212, 4160, 21326)), // Cosmetic
	DBOW5(12768, intArrayOf(882, 884, 886, 888, 890, 892, 11212, 4160, 21326)), // Cosmetic
	
	TWISTED_BOW(20997, intArrayOf(882, 884, 886, 888, 890, 892, 11212, 4160, 21326)),
}