package nl.bartpelle.veteres.content.items.skillcape

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Jason MacKeigan on 2016-07-04.
 */
object AttackCape : CapeOfCompletionPerk(intArrayOf(1)) {
	
	override fun option(option: Int): Function1<Script, Unit> = {
		val player = it.player()
		
		if (option == 1) {
			CapeOfCompletion.boost(Skills.ATTACK, player)
		}
	}
	
}