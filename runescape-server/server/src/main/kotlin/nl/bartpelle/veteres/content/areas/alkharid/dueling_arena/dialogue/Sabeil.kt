package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Sabeil {
	
	val SABEIL = 3347
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(SABEIL) @Suspendable {
			val random = it.player().world().random(6)
			
			if (random == 0) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("I wouldn't want to be the poor guy that has to clean up<br>after the duels.", SABEIL, 611)
				it.chatPlayer("Me neither.", 571)
			}
			if (random == 1) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Did you know they think this place dates back to the<br>second age?!", SABEIL, 589)
				it.chatPlayer("Really?", 588)
				it.chatNpc("Yeah. The guy at the information kiosk was telling me.", SABEIL, 588)
			}
			if (random == 2) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("My son just won his first duel!", SABEIL, 567)
				it.chatPlayer("Congratulations!", 567)
				it.chatNpc("He ripped his opponent in half!", SABEIL, 567)
				it.chatPlayer("That's gotta hurt!", 571)
				it.chatNpc("He's only 10 as well!", SABEIL, 567)
				it.chatPlayer("You gotta start 'em young!", 567)
			}
			if (random == 3) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Well. This beats doing the shopping!", SABEIL, 567)
			}
			if (random == 4) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Hi!", SABEIL, 567)
			}
			if (random == 5) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Why did the skeleton burp?", SABEIL, 567)
				it.chatPlayer("I don't know?", 575)
				it.chatNpc("'Cause it didn't have the guts to fart!", SABEIL, 605)
			}
		}
	}
	
}
