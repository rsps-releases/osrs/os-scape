package nl.bartpelle.veteres.content.musicareas

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.MusicBook
import nl.bartpelle.veteres.content.red
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.fs.EnumDefinition
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.PlayMusic
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 8/11/2015.
 */

enum class RegionTrack(val song: Int, vararg val regions: Int) {
	YESTERYEAR(177, 12849), // Southern Lumbridge
	HARMONY(6, 12850), // Lumbridge castle: Harmony
	AUTUMN_VOYAGE(9, 12851), // Northern lumbridge
	BOOK_OF_SPELLS(16, 12593), // Lumbridge swamp
	VISION(39, 12337), // Wizard's Tower: Vision
	UNKNOWN_LAND(38, 12338), // Draynor Village
	FLUTE_SALAD(14, 12595), // North-west Lumbridge
	DREAM(15, 12594), // Lumbridge-Draynor
	START(37, 12339), // Northern Draynor
	SPOOKY(36, 12340), // Draynor Manor
	BARBARIANISM(35, 12341), // Barbarian village
	GREATNESS(20, 12596), // River Lum
	EXPANSE(26, 12852), // South Varrock entrance
	GARDEN(25, 12853), // Varrock Square
	SPIRIT(18, 12597), // Varrock West
	FANFARE(46, 11828), // Falador Center
	WORKSHOP(163, 12084), // Falador East
	NIGHTFALL(52, 11827), // South of Falador
	SCAPE_SOFT(175, 11829), // Outside the Falador entrance
	ARRIVAL(45, 11572), // West of Falador
	MILES_AWAY(47, 11571), // Crafting guild
	WANDER(49, 12083), // North of port sarim
	ARABIAN_2(31, 13107), // Al Kharid mine
	ARABIAN(32, 13106), // North Al Kharid
	AL_KHARID(33, 13105), // Al Kharid center
	SEA_SHANTY_2(40, 12082), // Northern Port Sarim
	TOMORROW(418, 12081), // Southern Port Sarim
	LONG_WAY_HOME(148, 11826), // Rimmington mines
	ATTENTION(112, 11825), // Southern Rimmington
	EMPEROR(115, 11570), // West Rimmington
	ARABIAN_CW(32, 13617), // East of clan wars
	DUEL_ARENA(179, 13362), // Duel arena
	SHINE(181, 13363), // Mage training arena
	HORIZON(96, 11573), // South Taverley
	SPLENDOUR(55, 11574), // North Taverley
	PRINCIPALITY(193, 11575), // Burthorpe
	WONDER(129, 11831), // Westmost part of the wilderness ditch
	ALONE(50, 12086), // Edgeville monastery
	INSPIRATION(107, 12087), // Western part of the wilderness ditch
	DANGEROUS(114, 12343), // Western part of the wilderness ditch
	LIGHTNESS(323, 12599), // North of edgeville
	CRYSTAL_SWORD(126, 12855), // North of varrock palace
	MOODY(102, 12600), // WildernesS
	WILDWOOD(109, 12344), // Wilderness
	ARMY_OF_DARKNESS(108, 12088), // Wilderness
	THRALL_OF_THE_SERPENT(500, 8751), // Zul-Andra
	COIL(499, 9007), // Zulrah
	THAT_SULLEN_HALL(504, 5139), // Cerberus hall
	MAWS_JAWS_CLAWS_W(503, 4883, 5140, 5395), // Cerberus fight
	FOREVER(34, 12342), // Edgeville
	ADVENTURE(21, 12854), // Varrock palace
	MEDIEVAL(29, 13109), // East of the varrock eastern gate
	STILL_NIGHT(30, 13108), // Varrock east mining
	DOORWAYS(17, 13110), // Varrock northern lumberyard
	FORBIDDEN(191, 13111), // Wilderness north of varrock sawmill
	PARADE(28, 13366), // Road towards canibis
	MORYTANIA(186, 13622), // Gate to canibis
	TUTORIAL_ISLAND(127, 12336), // Tutorial Island
	MAUSOLEUM(190, 13722), // Tunnel to morytania
	VILLAGE(188, 13878), // Canifis center
	CASTLE_WARS_LOBBY(252, 9776), //Castle wars lobby
	CASTLE_WARS_GAME(251, 9520), //Castle wars lobby
	THE_TERRIBLE_TOWER(265, 13623), // Slayer tower
	FENKENSTRAINS_REFRAIN(269, 13879, 14135), // East of slayer tower
	SHIPWRECKED(271, 14391), // Northern ports of canibis
	DEADLANDS_W(235, 14390, 14134), // East of canibis
	OTHER_SIDE(273, 14647, 14646), // Northernmost port phasmtays
	LULLABY(88, 13365), // Digsite
	VENTURE(10, 13364), // Digsite exam centre
	DWARF_THEME(58, 12085), // Dwarf mine entrance part
	GNOME_VILLAGE(75, 11830), // Goblin village north of falador
	WARRIORS_GUILD(416, 11319), // Warrior's guild
	BEYOND(162, 11418), // Dwarven pass under white wolf mountain
	ICE_MELODY(182, 11318), // White wolf mountain
	CAMELOT(13, 11602), // Camelot
	LIGHTWALK(60, 11061), // Catherby west
	FISHING(41, 11317), // Catherby fishing spots
	BACKGROUND_E(12, 11316, 11060), // Eastern entrana
	MAGICAL_JOURNEY(53, 11805), // Sorcerer's tower
	TRINITY(64, 10804), // Legend's guild
	WONDEROUS(63, 10548), // Ardougne farming patch
	LASTING(93, 10549), // Ranger's guild
	TALKING_FOREST(72, 10550), // McGrubor's woods
	OVERTURE(71, 10806), // SEERS VILLAG EOMG
	MONARCH_WALTZ(70, 10807), // Sinclair Mansion
	BARROWS_MOUND(301, 14131), //Barrows mound
	BARROWS_CRYPT(302, 14231), //Barrows crypt
	LULLABY_(88, 10551), // Path towards rellekka
	SCAPE_CAVE(27, 6298), // Woodcutting Guild ent dungeon
	COUNTRY_JIG(510, 6454), // Woodcutting Guild ent dungeon
	VOLCANIC_VIKINGS(438, 9275), // Neitiznot
	DOORS_OF_DINH(524, 6461), // Wintertodt
	ICE_AND_FIRE(525, 6462), // Wintertodt Minigame Area
	TZHAAR(473, 9552, 10063, 10064, 9807, 9808), // Tzhaar areas
}

fun unlockAndPlay(script: Script, slot: Int) {
	val player = script.player()
	val def = player.world().definitions().get(EnumDefinition::class.java, 812)
	val bitdef = player.world().definitions().get(EnumDefinition::class.java, 819)
	val index = bitdef.getInt(slot) shr 14
	val bit = bitdef.getInt(slot) and 16383
	val varpid = MusicBook.varpForIndex(index)
	val name = def.getString(slot)
	
	if (varpid != -1 && (player.varps()[varpid] and (1 shl bit) == 0)) {
		player.message("You have unlocked a new music track: %s.".red(), name)
		
		var current = player.varps()[varpid]
		current = current or (1 shl bit)
		player.varps()[varpid] = current
	}
	
	val id = MusicBook.resolveId(player.world().server().store(), name)
	player.write(InterfaceText(239, 5, name))
	player.write(PlayMusic(id))
}

@ScriptMain fun script(repo: ScriptRepository) {
	RegionTrack.values().forEach { t ->
		t.regions.forEach { region ->
			repo.onRegionEnter(region) {
				unlockAndPlay(it, t.song)
			}
		}
	}
}