package nl.bartpelle.veteres.content.npcs.misc

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 03/13/2017.
 */

object RangedGuard {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 7)) {
				if (EntityCombat.attackTimerReady(npc)) {
					npc.animate(npc.combatInfo().animations.attack)
					npc.graphic(18, 90, 0)
					npc.world().spawnProjectile(npc.tile(), target, 9, 45, 33, 40, 5 * npc.tile().distance(target.tile()), 11, 105)
					
					if (AccuracyFormula.doesHit(npc, target, CombatStyle.RANGE, 1.0))
						target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(CombatStyle.RANGE)
					else
						target.hit(npc, 0, 1).combatStyle(CombatStyle.RANGE)
					
					
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
}