package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.openInterface
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.waitForInterfaceClose
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 9/18/2015.
 */
fun interrupt_depositbox(s: Script) {
	s.player().interfaces().closeMain();
	s.player().invokeScript(InvokeScript.OPEN_TAB, 3);
	s.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
	s.player().interfaces().sendWidgetOn(149, Interfaces.InterSwitches.INVENTORY_TAB)
	s.player().interfaces().sendWidgetOn(387, Interfaces.InterSwitches.EQUIPMENT_TAB)
}

@Suspendable fun open_deposit_box(script: Script) {
	if (script.player.ironMode() == IronMode.ULTIMATE) {
		script.messagebox("As an Ultimate Iron Man, you cannot use the bank.")
		return
	}
	
	script.openInterface(192)
	
	// Remember where we removed them from
	script.player().interfaces().closeById(149)
	script.player().interfaces().closeById(387)
	script.player().interfaces().setting(192, 2, 0, 27, 1180734)
	
	// Define interrupt script
	script.onInterrupt { interrupt_depositbox(it) }
	
	script.waitForInterfaceClose(192)
	
	// The interface was closed here.. get rid of the stuff
	interrupt_depositbox(script)
}

@ScriptMain fun deposit_objects(repo: ScriptRepository) {
	repo.onObject(11747) @Suspendable { open_deposit_box(it) }
	repo.onObject(6948) @Suspendable { open_deposit_box(it) }
	repo.onObject(29327) @Suspendable { open_deposit_box(it) }
	repo.onObject(26254) @Suspendable { open_deposit_box(it) }
}