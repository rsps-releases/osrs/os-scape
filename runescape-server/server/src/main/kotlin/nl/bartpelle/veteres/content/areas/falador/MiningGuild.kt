package nl.bartpelle.veteres.content.areas.falador

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/21/2016.
 */

object MiningGuild {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(7451) @Suspendable {
			//Climb down the ladders
			val ladder: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val ladder_tile = ladder.tile()
			val dwarves = arrayListOf(3361, 3362, 3363)
			if (it.player().skills().level(Skills.MINING) < 60) {
				it.chatNpc("Sorry, but you need level 60 Mining to get in there.", dwarves[it.player().world().random(2)], 588)
			} else {
				if (ladder_tile == Tile(3019, 3340)) { // North
					it.player().teleport(3019, 9741)
				} else if (ladder_tile == Tile(3019, 3338)) { // South
					it.player().teleport(3019, 9737)
				} else if (ladder_tile == Tile(3020, 3338)) { // East
					it.player().teleport(3021, 9739)
				} else if (ladder_tile == Tile(3020, 3339)) { // West
					it.player().teleport(3017, 9739)
				}
			}
		}
		
		r.onObject(17385) @Suspendable {
			// Climb up the ladders
			val ladder: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val ladder_tile = ladder.tile()
			
			//Mining guild
			if (ladder_tile == Tile(3019, 9740)) { // North
				Ladders.ladderUp(it, Tile(3019, 3341), true)
			} else if (ladder_tile == Tile(3019, 9738)) { // South
				Ladders.ladderUp(it, Tile(3019, 3337), true)
			} else if (ladder_tile == Tile(3020, 9739)) { // East
				Ladders.ladderUp(it, Tile(3021, 3339), true)
			} else if (ladder_tile == Tile(3018, 9739)) { // West
				Ladders.ladderUp(it, Tile(3017, 3339), true)
			} else if (ladder.tile() == Tile(2884, 9797)) {
				Ladders.ladderUp(it, Tile(2884, 3396), true)
			} else if (ladder.tile() == Tile(3209, 9616)) {
				Ladders.ladderUp(it, Tile(3210, 3216), true)
			} else if (ladder.tile().equals(3008, 9550)) { // Asgarnian ice dungeon
				Ladders.ladderUp(it, Tile(3009, 3150), true)
			} else if (ladder_tile == Tile(2842, 9824)) { //Black dragon taverly down to up catherby
				Ladders.ladderUp(it, Tile(2842, 3425), true)
			} else if (ladder_tile == Tile(3096, 9876)) { //Edgeville ladder
				it.player().lock()
				it.player().faceObj(ladder)
				it.animate(828)
				it.delay(1)
				it.player().teleport(3096, 3468)
				it.animate(-1)
				it.player().faceTile(Tile(3095, 3468))
				it.delay(1)
				it.player().unlock()
			} else if (it.interactionObject().tile().equals(3005, 10363)) { // Barb
				Ladders.ladderUp(it, it.player().tile().transform(0, -6400), true)
			} else if (it.interactionObject().tile().equals(2547, 9951)) { // Wildy
				Ladders.ladderUp(it, Tile(2548, 3551), true)
			} else if (ladder.tile().equals(3088, 9971)) {
				Ladders.ladderUp(it, Tile(it.player().tile().x, it.player().tile().z - 6400), true)
			} else if (ladder.tile().equals(2562, 9756)) { // chaos druid tower dungeon to upstairs
				Ladders.ladderUp(it, Tile(2563, 3356), true)
			} else if (ladder.tile().equals(2632, 9694)) { // ardy manhole dungeon for rat pits
				Ladders.ladderUp(it, Tile(2631, 3294), true)
			} else {
				it.player().message("This ladder does not seem to lead anywhere... Odd!")
			}
		}
	}
	
	@Suspendable fun door(it: Script) {
		val door: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		
		if (it.player().skills().level(Skills.MINING) < 60) {
			it.chatNpc("Sorry, but you need level 60 Mining to get in there.", 3361, 588)
		} else {
			it.player().lock()
			
			if (player.tile().z >= 9757) {
				if (!it.player().tile().equals(door.tile().transform(0, 1, 0))) {
					it.player().walkTo(door.tile().transform(0, 1, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(door.tile().transform(0, 1, 0))
				}
				
				it.runGlobal(player.world()) @Suspendable {
					val world = player.world()
					val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
					val spawned = MapObj(Tile(3046, 9757), 7426, door.type(), 2)
					world.removeObj(old, false)
					world.spawnObj(spawned)
					it.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}
				it.player().pathQueue().interpolate(3046, 9756, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(3046, 9756))
				it.delay(1)
				it.player().unlock()
				
			} else {
				if (!it.player().tile().equals(door.tile().transform(0, 0, 0))) {
					it.player().walkTo(door.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(door.tile().transform(0, 0, 0))
				}
				it.runGlobal(player.world()) @Suspendable {
					val world = player.world()
					val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
					val spawned = MapObj(Tile(3046, 9757), 7426, door.type(), 2)
					world.removeObj(old, false)
					world.spawnObj(spawned)
					it.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}
				it.player().pathQueue().interpolate(3046, 9757, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(3046, 9757))
				it.delay(1)
				it.player().unlock()
			}
		}
	}
}