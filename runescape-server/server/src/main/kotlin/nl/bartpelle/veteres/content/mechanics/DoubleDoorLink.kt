package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.veteres.model.Tile

/**
 * Created by Jak on 17/01/2017.
 *
 * Represents a pair of single doors states. Double doors.
 */
class DoubleDoorLink(val door1_state: DoorState, val door2_state: DoorState)

/**
 * Created by Jak on 17/01/2017.
 *
 * Represents a single door, with info about the open and closed states; ID, tile, rotation
 */
class DoorState(val state1_id: Int, val state1_tile: Tile, val state1_rot: Int, val state2_id: Int, val state2_tile: Tile, val state2_rot: Int)