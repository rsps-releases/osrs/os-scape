package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 11/27/2015.
 */
@ScriptMain
fun altar(repo: ScriptRepository) {
    for (altars in intArrayOf(14860, 409, 37990, 2640, 411, 27661, 7812)) {
        repo.onObject(altars) @Suspendable {
            if (it.player().skills().level(5) < it.player().skills().xpLevel(5)) {
                it.player().skills().replenishSkill(5, it.player().skills().xpLevel(5))
                it.animate(645)
                it.player().sound(2674)
            } else {
                it.message("You already have full prayer points.")
            }
        }
    }
}

@ScriptMain
fun register(repo: ScriptRepository) {
    val altars = arrayOf(29149, 29150)
    for (altar in altars) {
        repo.onObject(altar) @Suspendable {
            // Occult altar
            if (it.player().world().realm().isPVP) {
                when (it.options("Modern Spells", "Ancient Magicks", "Lunar Spells")) {
                    1 -> switchToModern(it)
                    2 -> switchToAncient(it)
                    3 -> switchToLunar(it)
                }

            } else if (it.player().world().realm().isOSRune) {
                when (it.interactionOption()) {
                    1 -> switchToAncient(it)
                    2 -> switchToLunar(it)
                    3 -> switchToModern(it)
                    4 -> switchToArceuus(it)
                }
            } else {
                if (it.interactionOption() == 1) {
                    if (it.player().varps().varbit(Varbit.SPELLBOOK) == 0) { // Modern
                        switchToAncient(it)
                    } else {
                        switchToModern(it)
                    }
                }
            }
        }
    }
    repo.onObject(18258) @Suspendable {
        val option = it.interactionOption()
        if (option == 1) {
            if (it.player().skills().level(5) < it.player().skills().xpLevel(5)) {
                it.player().skills().replenishSkill(5, it.player().skills().xpLevel(5))
                it.animate(645)
                it.player().sound(2674)
            } else {
                it.message("You already have full prayer points.")
            }
        }
        if (option == 2) {
            when (it.options("Modern Spells", "Ancient Magicks", "Lunar Spells")) {
                1 -> switchToModern(it)
                2 -> switchToAncient(it)
                3 -> switchToLunar(it)
            }
        }
    }
}

@Suspendable
fun switchToModern(it: Script) {
    it.player().varps().varbit(Varbit.SPELLBOOK, 0)
    it.player().animate(645)
    it.itemBox("Your magic book has been changed to the Regular spellbook.", 1381)
}

@Suspendable
fun switchToAncient(it: Script) {
    it.player().varps().varbit(Varbit.SPELLBOOK, 1)
    it.player().animate(645)
    it.itemBox("Your magic book has been changed to the Ancient spellbook.", 4675)
}

@Suspendable
fun switchToLunar(it: Script) {
    it.player().varps().varbit(Varbit.SPELLBOOK, 2)
    it.player().animate(645)
    it.itemBox("Your magic book has been changed to the Lunar spellbook.", 9084)
}

@Suspendable
fun switchToArceuus(it: Script) {
    it.player().varps().varbit(Varbit.SPELLBOOK, 3)
    it.player().animate(645)
    it.itemBox("Your magic book has been changed to the Arceuus spellbook.", 19677)
}