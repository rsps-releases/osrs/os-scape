package nl.bartpelle.veteres.content.areas.karamja.objects

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/11/2016.
 */

object Rocks {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(11441) @Suspendable {
			it.player().lock()
			it.player().teleport(Tile(2856, 9567))
			it.message("You climb down through the pot hole.")
			it.player().unlock()
		}
	}
}

