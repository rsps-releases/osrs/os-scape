package nl.bartpelle.veteres.content.minigames.pest_control

/**
 * Created by Jason MacKeigan on 2016-07-19 at 2:53 PM
 *
 * The difficulty level of a game. Every difficulty specifies a unique set
 * of characteristics tailored to that level of game.
 */
enum class PestControlDifficulty(val portalHealth: Int, val squire: Int, val voidKnight: Int, val pointsGranted: Int) {
	NOVICE(200, 1771, 2950, 2),
	
	INTERMEDIATE(250, 1772, 2951, 3),
	
	VETERAN(250, 1773, 2952, 4)
}