package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.text.NumberFormat

/**
 * Created by Situations on 5/29/2016.
 */

object VotingRewardToken {
	
	private const val OLD_VOTING_REWARD_TOKEN = 10943
	private const val VOTING_REWARD_TOKEN = 5020
	private const val BLOOD_MONEY = 13307
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(OLD_VOTING_REWARD_TOKEN) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.itemBox("Thank you for voting - please select a reward!", OLD_VOTING_REWARD_TOKEN)
				when (it.options("Voting Points", "1000 Blood Money")) {
					1 -> votingPoints(it, OLD_VOTING_REWARD_TOKEN, 1)
					2 -> bloodMoney(it, OLD_VOTING_REWARD_TOKEN, 1)
				}
			} else {
				it.itemBox("This item can only be used on our PVP world.", OLD_VOTING_REWARD_TOKEN)
			}
		}
		
		r.onItemOption1(VOTING_REWARD_TOKEN) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.itemBox("Thank you for voting - please select a reward!", VOTING_REWARD_TOKEN)
				when (it.options("Voting Points", "1000 Blood Money")) {
					1 -> votingPoints(it, VOTING_REWARD_TOKEN, 1)
					2 -> bloodMoney(it, VOTING_REWARD_TOKEN, 1)
				}
			} else {
				it.itemBox("This item can only be used on our PVP world.", VOTING_REWARD_TOKEN)
			}
		}
		r.onItemOption2(VOTING_REWARD_TOKEN, @Suspendable {
			if (it.player().world().realm().isPVP) {
				val amount = it.player().inventory().count(VOTING_REWARD_TOKEN)
				it.messagebox2("<col=7f0000>Warning!</col><br>You have selected to redeem all your voting tokens! Please proceed with caution!")
				it.itemBox("Thank you for voting - please select a reward!", VOTING_REWARD_TOKEN)
				when (it.options("Voting Points", "1000 Blood Money")) {
					1 -> votingPoints(it, VOTING_REWARD_TOKEN, amount)
					2 -> bloodMoney(it, VOTING_REWARD_TOKEN, amount)
				}
			}
		})
		r.onItemOption3(VOTING_REWARD_TOKEN, @Suspendable {
			if (it.player().world().realm().isPVP) {
				val amount = it.inputInteger("How many voting tokens would you like to redeem?")
				it.itemBox("Thank you for voting - please select a reward!", VOTING_REWARD_TOKEN)
				when (it.options("Voting Points", "1000 Blood Money")) {
					1 -> votingPoints(it, VOTING_REWARD_TOKEN, amount)
					2 -> bloodMoney(it, VOTING_REWARD_TOKEN, amount)
				}
			}
		})
	}
	
	@Suspendable private fun votingPoints(it: Script, item: Int, amount: Int) {
		val has = it.player().inventory().count(VOTING_REWARD_TOKEN)
		var toRemove = amount
		if(toRemove > has)
			toRemove = has

		val votingPoints = it.player().attribOr<Int>(AttributeKey.VOTING_POINTS, 0)
		val s = if (votingPoints == 0) "" else "s"
		
		if (it.player().inventory().remove(Item(item, amount), false).success()) {
			it.player().putattrib(AttributeKey.VOTING_POINTS, votingPoints + toRemove)
			it.messagebox("Your voting points have been increased by $toRemove.<br>You now have" +
					" <col=f9fe11><shad>${it.player().attribOr<Int>(AttributeKey.VOTING_POINTS, 0)}</col></shad> voting point$s.")
		}
	}
	
	@Suspendable private fun bloodMoney(it: Script, item: Int, amount: Int) {
		val has = it.player().inventory().count(VOTING_REWARD_TOKEN)
		var toRemove = amount
		if(toRemove > has)
			toRemove = has
		if (it.player().inventory().remove(Item(item, toRemove), false).success()) {
			val amtToAdd = 1000 * toRemove
			if (it.player().inventory().full()) {
				it.player().bank().add(Item(BLOOD_MONEY, amtToAdd), true)
				it.itemBox("${NumberFormat.getInstance().format(amtToAdd)} Blood Money has been deposited into your bank account.", BLOOD_MONEY, amtToAdd)
			} else {
				it.player().inventory().add(Item(BLOOD_MONEY, amtToAdd), true)
				it.itemBox("${NumberFormat.getInstance().format(amtToAdd)} Blood Money has been added into your inventory.", BLOOD_MONEY, amtToAdd)
			}
		}
	}
	
	@JvmStatic @Suspendable fun getPriceOf(itemid: Int): Int {
		when (itemid) {
			6199 -> return 10
			13655 -> return 50
			13667 -> return 20
			13669 -> return 15
			13671 -> return 40
			13673 -> return 40
			13675 -> return 15
			13677 -> return 15
			8148 -> return 50
			12397 -> return 35
			12393 -> return 20
			12395 -> return 20
			12013 -> return 30
			12014 -> return 25
			12015 -> return 25
			12016 -> return 20
			else -> return 9999
		}
		
		
	}
}
