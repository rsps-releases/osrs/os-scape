package nl.bartpelle.veteres.content.achievements

/**
 * Enum constants used to represent the kind of achievement we are searching for or executing. For example,
 * if we want to scan for any fishing achievements whilst a player is fishing then we call the appropriate key.
 * Created by Mack on 11/22/2017.
 */
enum class AchievementCategory {
	THIEVING,
	MINING,
	FISHING,
	WOODCUTTING,
	HUNTER,
	SLAYER,
	PVM,
	PVP,
	KILLSTREAK,
	BOUNTY_HUNTER,
	CUSTOM,
}