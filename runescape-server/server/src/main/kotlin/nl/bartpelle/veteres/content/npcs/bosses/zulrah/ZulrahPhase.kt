package nl.bartpelle.veteres.content.npcs.bosses.zulrah

/**
 * Created by Bart on 3/6/2016.
 *
 * Represents a single phase, including form and positioning.
 */
data class ZulrahPhase(val form: ZulrahForm, val position: ZulrahPosition, val config: Array<ZulrahConfig> = arrayOf()) {
	
	fun hasConfig(cfg: ZulrahConfig): Boolean {
		return cfg in config
	}
	
}