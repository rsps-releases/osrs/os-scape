package nl.bartpelle.veteres.content.npcs.godwars.armadyl

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 7/4/2016.
 */

object AggressionCheck {
	
	val ARMADYL_PROTECTION_EQUIPMENT = arrayListOf(11785, 11802, 11826, 11828, 11830, 12253,
			12255, 12257, 12259, 12261, 12263, 12470, 12472, 12474, 12476, 12478, 12506, 12508,
			12510, 12512, 12610, 19930)
	
	@JvmField val script: Function1<Entity, Boolean> = s@ @Suspendable { entity -> determine_aggression(entity) }
	
	@Suspendable fun determine_aggression(entity: Entity): Boolean {
		ARMADYL_PROTECTION_EQUIPMENT.forEach { armour ->
			if (entity.equipment().has(armour)) {
				return false
			}
		}
		return true
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		// Set the max return to spawnpoint distance for gwd room npcs
		for (npcId in PlayerCombat.GWD_ROOM_NPCIDS) {
			repo.onNpcSpawn(npcId) {
				it.npc().putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 40)
			}
		}
	}
}
