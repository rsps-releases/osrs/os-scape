package nl.bartpelle.veteres.content.skills.fishing

import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import java.util.*

/**
 * Created by Bart on 12/1/2015.
 */
enum class FishSpotType(val anim: Int, val staticRequiredItem: Int, val missingText: String, val baitItem: Int, val catchables: Array<Fish>, val start: String, val baitMissing: String? = null) {
	
	NET(621, 303, "You need a net to catch these fish.", -1, arrayOf(Fish.SHRIMP, Fish.ANCHOVIES), "You cast out your net..."),
	BAIT(623, 307, "You need a Fishing Rod to Bait these fish.", 313, arrayOf(Fish.SARDINE, Fish.HERRING), "You cast out your line...", "You don't have any Fishing Bait left."),
	BIG_NET(620, 305, "You need a Big Net to catch these fish.", -1, arrayOf(Fish.BASS, Fish.MACKEREL, Fish.COD), "You cast out your net..."),
	CAGE(619, 301, "You need a Lobster Pot to catch Lobsters.", -1, arrayOf(Fish.LOBSTER), "You attempt to catch a Lobster."),
	MONKFISH(621, 303, "You need a net to catch these fish.", -1, arrayOf(Fish.MONKFISH), "You cast out your net..."),
	HARPOON(618, 311, "You need a Harpoon to catch these fish.", -1, arrayOf(Fish.TUNA, Fish.SWORDFISH), "You start to lure the fish."),
	HARPOON_SHARK(618, 311, "You need a Harpoon to catch these fish.", -1, arrayOf(Fish.SHARK), "You start to lure the fish."),
	FLY(623, 309, "You need a Fly Fishing Rod to catch these fish.", 314, arrayOf(Fish.TROUT, Fish.SALMON, Fish.PIKE), "You cast out your line...", "You don't have any feathers left."),
	DARK_CRAB(619, 301, "You need a Lobster Pot and Dark Bait to catch these Dark Crabs.", 11940, arrayOf(Fish.DARK_CRAB), "You attempt to catch a Dark Crab.", "You don't have any Dark Fishing Bait."),
	ANGLERGISH(623, 307, "You need a Fishing Rod to bait these fish.", 13431, arrayOf(Fish.ANGLERFISH, Fish.RAINBOW), "You cast our your line...", "You don't have any Fishing Bait left."),
	INFERNAL_EEL(623, 1585, "You need an Oily Fishing Rod to bait these fish.", 313, arrayOf(Fish.INFERNAL_EEL), "You cast out your line...", "You don't have any Fishing Bait left."),
	;
	
	fun randomFish(world: World, lvl: Int): Fish {
		if (catchables.size == 1)
			return catchables.first()
		val list = if (world.realm().isPVP) catchables.asList() else catchables.asList().filter { fish -> fish.lvl <= lvl }
		Collections.shuffle(list)
		return list.first()
	}
	
	fun bestFish(lvl: Int): Fish {
		if (catchables.size == 1)
			return catchables.first()
		val list = catchables.asList().filter { it.lvl <= lvl }.sortedBy(Fish::lvl)
		return list.last()
	}
	
	fun levelReq(): Int {
		return catchables.minBy { m -> m.lvl }?.lvl ?: 1
	}
}