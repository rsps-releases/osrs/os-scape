package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 2017-01-08.
 */

object CrystalKeyChest {
	
	val CRYSTAL_KEY_CHEST = 172
	
	val CRYSTAL_KEY = 989
	val CRYSTAL_KEY_LOOP = 987
	val CRYSTAL_KEY_TOOTH = 985
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(CRYSTAL_KEY_CHEST, s@ @Suspendable {
			val player = it.player()
			val chest = it.interactionObject()
			
			if (player.inventory().remove(Item(CRYSTAL_KEY, 1), false).success()) {
				player.lock()
				player.message("You unlock the chest with your key.")
				it.sound(51)
				it.player().animate(536)
				
				it.runGlobal(player.world()) @Suspendable {
					val world = player.world()
					val old = MapObj(chest.tile(), CRYSTAL_KEY_CHEST, chest.type(), chest.rot())
					val spawned = MapObj(chest.tile(), 173, chest.type(), chest.rot())
					
					world.removeObj(old)
					world.spawnObj(spawned)
					it.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}
				
				giveReward(player)
				it.delay(1)
				player.unlock()
			} else {
				player.message("You need a crystal key to open this chest.")
			}
		})
		
		r.onItemOnItem(CRYSTAL_KEY_LOOP, CRYSTAL_KEY_TOOTH, s@ @Suspendable {
			val player = it.player()
			
			if (player.inventory().hasAll(CRYSTAL_KEY_LOOP, CRYSTAL_KEY_TOOTH)) {
				player.inventory().remove(Item(CRYSTAL_KEY_LOOP), true)
				player.inventory().remove(Item(CRYSTAL_KEY_TOOTH), true)
				
				player.inventory().add(Item(CRYSTAL_KEY), true)
			} else {
				it.message("Nothing interesting happens.")
			}
		})
	}
	
	enum class Rewards(val chance: Int, val rewards: ArrayList<Item>) {
		FIRST(80, arrayListOf(Item(1631, 1), Item(1969, 1), Item(995, 2000))),
		SECOND(60, arrayListOf(Item(1631, 1))),
		THIRD(50, arrayListOf(Item(1631, 1), Item(371, 5), Item(995, 1000))),
		FOURTH(40, arrayListOf(Item(1631, 1), Item(556, 50), Item(555, 50), Item(557, 50),
				Item(554, 50), Item(559, 50), Item(558, 50), Item(565, 10),
				Item(9075, 10), Item(566, 10))),
		FIFTH(30, arrayListOf(Item(1631, 1), Item(454, 100))),
		SIXTH(20, arrayListOf(Item(1631, 1), Item(1603, 2), Item(1601, 2))),
		SEVENTH(15, arrayListOf(Item(1631, 1), Item(985, 1), Item(995, 750))),
		EIGHT(10, arrayListOf(Item(1631, 1), Item(2363, 3))),
		NINTH(8, arrayListOf(Item(1631, 1), Item(987, 1), Item(995, 750))),
		TENTH(6, arrayListOf(Item(1631, 1), Item(441, 150))),
		ELEVENTH(4, arrayListOf(Item(1631, 1), Item(1183, 1))),
		TWELFTH(0, arrayListOf(Item(1631, 1), Item(1079, 1)));
	}
	
	@Suspendable private fun giveReward(player: Player) {
		val rewards = generateReward(player)
		
		rewards.forEach { item ->
			player.inventory().addOrDrop(item, player)
		}
		
		player.message("You find some treasure in the chest!")
	}
	
	private fun generateReward(player: Player): ArrayList<Item> {
		var randomReward = player.world().random(80)
		
		Rewards.values().forEach { reward ->
			if (reward.chance <= randomReward) {
				return reward.rewards
			}
		}
		
		return Rewards.FIRST.rewards
	}
}
