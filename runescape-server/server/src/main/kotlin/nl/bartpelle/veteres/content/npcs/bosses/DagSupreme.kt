package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Bart on 8/28/2015.
 */

object DagSupreme {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && npc.hp() > 0 && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 8) && EntityCombat.attackTimerReady(npc)) {
				val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
				npc.world().spawnProjectile(npc, target, 475, 30, 30, 20, 12 * tileDist, 14, 5)
				val delay = Math.max(1, (20 + (tileDist * 12)) / 30)
				
				if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
					target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.RANGE)
				} else {
					target.hit(npc, 0, delay.toInt()) // Uh-oh, that's a miss.
				}
				npc.animate(npc.attackAnimation())
				
				// .. and go into sleep mode.
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
}