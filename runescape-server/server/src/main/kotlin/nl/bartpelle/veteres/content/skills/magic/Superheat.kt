package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 3/23/2016.
 */
object Superheat {
	
	enum class BarDefinition(val bar: Int, val level: Int, val xp: Double, val items: Array<Item>, val fail: String = "", val barname: String) {
		BRONZE(2349, 1, 7.5, arrayOf(Item(436), Item(438)), "You need one copper ore and one tin ore to make bronze.", "bronze"),
		SILVER(2355, 20, 13.7, arrayOf(Item(442)), "", "silver"),
		STEEL(2353, 30, 17.5, arrayOf(Item(453, 2), Item(440)), "", "steel"), // No message, 'cos if you don't have coal, you smelt iron.
		IRON(2351, 15, 12.5, arrayOf(Item(440)), "", "iron"), // Iron comes after steel for a reason. Check the code.
		GOLD(2357, 40, 22.5, arrayOf(Item(444)), "", "gold"),
		MITHRIL(2359, 50, 30.0, arrayOf(Item(453, 4), Item(447)), "You need four heaps of coal to smelt mithril.", "mithril"),
		ADAMANT(2361, 70, 37.5, arrayOf(Item(453, 6), Item(449)), "You need six heaps of coal to smelt adamantite.", "adamantite"),
		RUNE(2363, 85, 50.0, arrayOf(Item(453, 8), Item(451)), "You need eight heaps of coal to smelt runite.", "runite");
		
		companion object {
			fun fromOre(player: Player, ore: Int): BarDefinition? {
				// We never accept coal. Ever.
				if (ore == 453) {
					return null
				}
				
				// Try to match up with one of the bars :)
				for (bar in BarDefinition.values()) {
					for (item in bar.items) {
						// You are only matched up against steel, if you're MADE OF STEEL.
						if (ore == item.id() && (bar != STEEL || player.skills()[Skills.SMITHING] >= STEEL.level)) {
							return bar
						}
					}
				}
				
				return null
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onSpellOnItem(218, 26, s@ @Suspendable {
			val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]] ?: return@s
			val runes = arrayOf(Item(554, 4), Item(561, 1))
			
			// This spell uses the same 'stalling' timer as alching.
			while (it.player().timers().has(TimerKey.ALCH_TIMER))
				it.delay(1)
			
			if (!it.player().timers().has(TimerKey.ALCH_TIMER)) {
				it.player().invokeScript(InvokeScript.OPEN_TAB, 6) // Back to mage book
				
				// Do we have a bar match?
				var bardef = BarDefinition.fromOre(it.player(), item.id())
				if (bardef == null) {
					it.player().message("You need to cast superheat item on ore.")
					it.player().graphic(85, 92, 0)
					return@s
				}
				
				
				// Can we smelt it?
				if (it.player().skills()[Skills.MAGIC] < 43) {
					it.message("Your Magic level is not high enough for this spell.")
					it.player().graphic(85, 92, 0)
					return@s
				} else if (it.player().skills()[Skills.SMITHING] < bardef.level) {
					it.message("You need a smithing level of at least ${bardef.level} to smelt ${bardef.barname}.")
					it.player().graphic(85, 92, 0)
					return@s
				}

				// Do we have the required runes?
				if (!MagicCombat.has(it.player(), runes, false)) {
					return@s
				}
				
				// Do we have the ingredients? TODO steel=>iron
				for (req in bardef!!.items) {
					if (req !in it.player().inventory()) {
						if (bardef != BarDefinition.STEEL) {
							// Steel is a special case. We need to attempt iron too.
							it.message(bardef.fail)
							it.player().graphic(85, 92, 0)
							return@s
						} else {
							bardef = BarDefinition.IRON
						}
					}
				}
				
				// Remove the items from the inventory
				for (req in bardef.items) {
					it.player().inventory().remove(req, true) // We already checked above. It's safe.
				}
				MagicCombat.has(it.player(), runes, true)
				
				// Finally, cook the ore. Huehhh.
				it.player().animate(725)
				it.player().graphic(148, 100, 0)
				it.addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) 106.0 else 53.0)
				it.addXp(Skills.SMITHING, bardef.xp)
				it.player().inventory().add(Item(bardef.bar), true) // Always at least 1 free spot. Safe.
				it.player().timers().set(TimerKey.ALCH_TIMER, 3)
			}
		})
	}
	
}
