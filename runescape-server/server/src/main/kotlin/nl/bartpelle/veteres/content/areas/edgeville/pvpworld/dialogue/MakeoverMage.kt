package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey

import nl.bartpelle.veteres.model.entity.player.Looks
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp
import java.util.*

/**
 * Created by Mack on 8/23/17
 */
object MakeoverMage {

	private const val MAKEOVER_MAGE = 1306
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MAKEOVER_MAGE) @Suspendable {
			if (!it.player().world().realm().isRealism) {
				dialogue(it)
			} else {
				it.player().message("This Make-over mage cannot be used on the Realism world.")
			}
		}
		r.onNpcOption3(MAKEOVER_MAGE) @Suspendable {
			if (!it.player().world().realm().isRealism) {
				it.player().interfaces().sendMain(269)
			} else {
				it.player().message("This Make-over mage cannot be used on the Realism world.")
			}
		}
		r.onNpcOption2(MAKEOVER_MAGE, @Suspendable {
            displaySkinToneMenu(it)
        })
	}
	
	@Suspendable fun dialogue(s: Script) {
		s.chatNpc("Hello there, ${s.player().name()}. What can I do for you?", MAKEOVER_MAGE)
		when (s.options("I'd like to set my appearance.", "Show me all unlockable skin modifications.", "Nevermind.")) {
			1 -> {
				s.player().interfaces().sendMain(269)
			}
			2 -> {
                displaySkinToneMenu(s)
			}
			3 -> {
				s.chatPlayer("Nevermind.")
			}
		}
	}
	
	@Suspendable fun displaySkinToneMenu(s: Script) {
		val allTime = s.player().varps().varp(Varp.KILLS) + s.player().attribOr<Int>(AttributeKey.ALLTIME_KILLS, 0)
		when (s.options("Set Midnight Black<col=FF0000>(50 kills required)</col>", "Set Moonlight White<col=FF0000>(100 kills required)</col>", "Set Swamp Green<col=FF0000>(175 kills required)</col>", "More Options...", "Nevermind.")) {
			1 -> {
				if (allTime >= 50) {
					setSkin(s, 9, "Midnight Black")
				} else {
					error(s, 50)
				}
			}
			2 -> {
				if (allTime >= 100) {
					setSkin(s, 10, "Moonlight White")
				} else {
					error(s, 100)
				}
			}
			3 -> {
				if (allTime >= 175) {
					setSkin(s, 8, "Swamp green")
				} else {
					error(s, 175)
				}
			}
			4 -> {
				when (s.options("Set Phasmatys Green<col=FF0000>(250 kills required)</col>", "Set Putrid Purple<col=FF0000>(400 kills required)</col>", "Set Zombie Blue<col=FF0000>(575 kills required)</col>", "Previous...", "Nevermind")) {
                    1 -> {
                        if (allTime >= 250) {
                            setSkin(s, 12, "Phasmatys Green")
                        } else {
                            error(s, 250)
                        }
                    }
                    2 -> {
                        if (allTime >= 400) {
                            setSkin(s, 11, "Putrid Purple")
                        } else {
                            error(s, 400)
                        }
                    }
                    3 -> {
                        if (allTime >= 575) {
                            setSkin(s, 13, "Zombie Blue")
                        } else {
                            error(s, 575)
                        }
                    }
                    4 -> {
                        displaySkinToneMenu(s)
                    }
                    5 -> {
                        s.chatPlayer("Nevermind")
                    }
                }
			}
			5 -> {
				s.chatPlayer("Nevermind.")
			}
		}
	}
	
	@Suspendable fun setSkin(s: Script, skinId: Int, skinName: String) {
		s.player().looks().colors()[4] = skinId
		s.player().looks().update()
        s.player().sync().graphic(1119, 0, 0)
        s.chatNpc("1... 2... and... Poof! The transformation was a success! I have set your skin tone to '<col=8B0000>$skinName</col>'. Come speak to me if you wish to change it again.", MAKEOVER_MAGE)
	}
	
	@Suspendable fun error(s: Script, kc: Int) {
		s.chatNpc("Sorry, but you require a kill count of at least $kc to set this skin modifier.", MAKEOVER_MAGE)
	}
}