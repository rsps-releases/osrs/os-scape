package nl.bartpelle.veteres.content.items.skillcape

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Jason MacKeigan on 2016-07-05 at 10:56 AM
 */
object CookingCape : CapeOfCompletionPerk(intArrayOf(1)) {
	
	override fun option(option: Int): Function1<Script, Unit> = {
		if (option == 1) {
			CapeOfCompletion.boost(Skills.COOKING, it.player())
		}
		
	}
	
}