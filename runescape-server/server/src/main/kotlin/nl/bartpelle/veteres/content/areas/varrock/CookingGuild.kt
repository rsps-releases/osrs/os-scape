package nl.bartpelle.veteres.content.areas.varrock

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.mechanics.Doors
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/9/2016.
 */

object CookingGuild {
	
	val CHEF = 2658
	val CHEFS_HAT = 1949
	val GOLDEN_CHEFS_HAT = 20205
	
	val GUILD_DOOR = 24958
	
	val FIRST_FLOOR_STAIRS = 2608
	val SECOND_FLOOR_STAIRS = 2609
	val THIRD_FLOOR_STAIRS = 2610
	
	@Suspendable fun door(it: Script) {
		val door: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		
		//Requirement checks to enter the cooking guild.
		if (it.player().tile().z <= 3443) {
			if (it.player().skills().level(Skills.COOKING) < 32) {
				it.chatNpc("Sorry. Only the finest chefs are allowed in here. Get<br>your cooking level up to 32 and come back wearing a<br>chef's hat.", CHEF, 560)
			} else if (!it.player().equipment().hasAny(CHEFS_HAT, GOLDEN_CHEFS_HAT)) {
				it.chatNpc("You can't come in here unless you're wearing a chef's<br>hat, or something like that.", CHEF, 589)
			} else {
				if (!it.player().tile().equals(door.tile().transform(0, 0, 0))) {
					it.player().walkTo(door.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(door.tile().transform(0, 0, 0))
				}
				it.player().lock()
				it.runGlobal(player.world()) @Suspendable {
					val world = player.world()
					val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
					val spawned = MapObj(Tile(3143, 3444), 24959, door.type(), 2)
					world.removeObj(old, false)
					world.spawnObj(spawned)
					it.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}
				it.player().pathQueue().interpolate(3143, 3444, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(3143, 3444))
				it.delay(1)
				it.player().unlock()
			}
		} else {
			if (!it.player().tile().equals(door.tile().transform(0, 1, 0))) {
				it.player().walkTo(door.tile().transform(0, 1, 0), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(door.tile().transform(0, 1, 0))
			}
			it.player().lock()
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
				val spawned = MapObj(Tile(3143, 3444), 24959, door.type(), 2)
				world.removeObj(old, false)
				world.spawnObj(spawned)
				it.delay(2)
				world.removeObjSpawn(spawned)
				world.spawnObj(old)
			}
			it.player().pathQueue().interpolate(3143, 3443, PathQueue.StepType.FORCED_WALK)
			it.waitForTile(Tile(3143, 3443))
			it.delay(1)
			it.player().unlock()
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		// This door ID is used elsewhere too!
		r.onObject(GUILD_DOOR) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().equals(3143, 3443)) {
				CookingGuild.door(it)
			} else {
				Doors.attempt_default_door(it)
			}
		}
		//Staircase inside the cooking guild
		r.onObject(FIRST_FLOOR_STAIRS) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().equals(3144, 3447, 0)) {
				Ladders.ladderUp(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level + 1), false)
			}
		}
		r.onObject(SECOND_FLOOR_STAIRS) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().equals(3144, 3447, 1)) {
				val opt = it.interactionOption()
				if (opt == 1) {
					when (it.options("Climb up", "Climb down")) {
						1 -> Ladders.ladderUp(it, Tile(3144, 3446, it.player().tile().level + 1), false)
						2 -> Ladders.ladderDown(it, Tile(3144, 3449, it.player().tile().level - 1), false)
					}
				} else if (opt == 2) {
					Ladders.ladderUp(it, Tile(3144, 3446, it.player().tile().level + 1), false)
				} else if (opt == 3) {
					Ladders.ladderDown(it, Tile(3144, 3449, it.player().tile().level - 1), false)
				}
			}
		}
		r.onObject(THIRD_FLOOR_STAIRS) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().equals(3144, 3447, 2)) {
				Ladders.ladderDown(it, Tile(3144, 3449, it.player().tile().level - 1), false)
			}
		}
	}
}