package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/11/2015.
 */

object Lowe {
	
	val LOWE = 536
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(LOWE) @Suspendable {
			it.chatNpc("Welcome to Lowe's Archery Emporium. Do you want to see my wares?", LOWE, 589)
			when (it.options("Yes, please.", "No, I prefer to bash things close up.")) {
				1 -> {
					if (it.player().ironMode() != IronMode.NONE) {
						it.player().world().shop(16).display(it.player())
					} else {
						it.player().world().shop(7).display(it.player())
					}
				}
				2 -> {
					it.chatPlayer("No, I prefer to bash things close up.")
					it.chatNpc("Humph, philistine.", LOWE)
				}
			}
		}
		r.onNpcOption2(LOWE) {
			if (it.player().ironMode() != IronMode.NONE) {
				it.player().world().shop(16).display(it.player())
			} else {
				it.player().world().shop(7).display(it.player())
			}
		}
	}
	
}
