package nl.bartpelle.veteres.content.interfaces.questtab

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.content.mechanics.deadman.safezones.DmmZones
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Preset
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.Misc
import java.util.*

/**
 * Created by Situations on 12/29/2015.
 */

object QuestTabButtons {
	
	val ECO_HANDLERS_FREE: HashMap<Int, (Script) -> Unit> = HashMap()
	val ECO_HANDLERS_MEMBER: HashMap<Int, (Script) -> Unit> = HashMap()
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {

		if (r.server().world().realm().isRealism) {
			// Eco handlers
			realism_questtab.setupForRealm(r)
		} else {
			// Any other game mode setting
			other_realms_questtab.setupForRealm(r)
		}
	}
	
	@JvmStatic fun displayServerUptime(it: Script) {
		it.player().stopActions(false)
		QuestGuide.clear(it.player())
		
		it.player().interfaces().text(275, 2, "<img=1> Server Information")
		it.player().interfaces().text(275, 4, "<col=800000>Notice:</col> If there's something you'd like to see on")
		it.player().interfaces().text(275, 5, "this list, please request it on our forums.")
		
		it.player().interfaces().text(275, 7, "<col=800000>Server uptime:</col> " + it.player().world().getTime(true))
		
		if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {
			it.player().interfaces().text(275, 8, "<col=800000>[DMM]</col> " + DmmZones.INFO_STRING[0])
			it.player().interfaces().text(275, 9, "<col=800000>[DMM]</col> " + DmmZones.INFO_STRING[1])
		}
		
		QuestGuide.open(it.player())
	}
	
	@JvmStatic fun displayWildernessBreakdown(it: Script) {
		val wildCount = Misc.inWildCount(it.player().world())
		val instanceCount = Misc.inPvpInstanceCount(it.player().world())
		
		it.player().message("Wilderness: <col=FF0000>$wildCount</col>.   Instances: <col=FF0000>$instanceCount</col>.")
		
		if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {
			it.player().stopActions(false)
			QuestGuide.clear(it.player())
			
			it.player().interfaces().text(275, 2, "<img=1> Wilderness breakdown")
			
			it.player().interfaces().text(275, 4, "<col=800000>Level 0-10: </col>" + PVPAreas.range0_10)
			it.player().interfaces().text(275, 5, "<col=800000>Level 11-20: </col>" + PVPAreas.range11_20)
			it.player().interfaces().text(275, 6, "<col=800000>Level 21-30: </col>" + PVPAreas.range21_30)
			it.player().interfaces().text(275, 7, "<col=800000>Level 31-40: </col>" + PVPAreas.range31_40)
			it.player().interfaces().text(275, 8, "<col=800000>Level 41-49: </col>" + PVPAreas.range41_49)
			it.player().interfaces().text(275, 9, "<col=800000>Level 50+: </col>" + PVPAreas.range50plus)
			
			it.player().interfaces().text(275, 11, "<col=800000>Total players inside wilderness:  </col>" + wildCount)
			it.player().interfaces().text(275, 12, "<col=800000>Total players in PVP instances:  </col>" + instanceCount)
			
			QuestGuide.open(it.player())
		}
	}
	
	@Suspendable fun presetDialogue(it: Script) {
		if (GameCommands.inWildBypassable(it.player, "You can't use presets in the wilderness.", true)) {
			return
		}
		if (ClanWars.inInstance(it.player)) {
			it.player.message("You cannot use presets during a clan war.")
			return
		}
		if (Staking.in_duel(it.player)) {
			it.player.message("You cannot use presets during a duel.")
			return
		}
		if (it.player.jailed()) {
			it.player.message("You cannot use a preset when you're in jail.")
			return
		}
		
		if (!it.player().locked()) {
			it.player().stopActions(false)
			
			val list = arrayOf("<br><col=006385>Preset Management", "Capture this preset", "Delete all presets", "Reload last preset", "Preset information", "  <br>", "<col=006385>Presets").toMutableList()
			
			// Build the options
			val array = ArrayList<Preset>()
			it.player.presetRepository().presets().forEach {
				list.add(it.value.name())
				array.add(it.value)
			}
			
			val opt = OptionList.display(it, "Preset Options", list)
			
			when (opt) {
				1 -> { // Capture this preset
					it.player.presetRepository().createFromState(it.inputString("Enter a name for your preset:"))
				}
				2 -> { // Delete all presets
					if (it.optionsTitled("Permanently delete all presets?", "Yes, remove all my presets.", "No, let me keep them.") == 1) {
						it.player.presetRepository().deleteAll()
					}
				}
				3 -> { // Load last preset
					val presetname = it.player.attrib<String?>(AttributeKey.LAST_PRESET)
					if (presetname == null) {
						it.player.message("You don't have a last used preset to spawn.")
					} else {
						for (preset in array) {
							if (preset.name().equals(presetname)) {
								it.player.message("Withdrawing " + preset.name() + "...")
								it.player.stopActions(false)
								preset.apply(it.player)
							}
						}
					}
				}
				4 -> { // Preset information
					it.doubleItemBox("Presets are a quick and convenient way to store a loadout and withdraw them later on. " +
							"Simply capture your inventory and equipment, enter a name, and load them when needed.", 1127, 385)
					it.doubleItemBox("By default, you have 5 free preset slots. Each donator rank increases your capacity with 2 slots, up to a total of 15.", 4712, 565)
				}
				in 7..32 -> { // Preset selecting
					val idx = opt - 7
					if (idx < 0 || idx >= array.size)
						return
					val preset = array[idx]
					
					when (it.optionsTitled("What would you like to do?", "Withdraw this preset", "Delete this preset", "Rename this preset")) {
						1 -> { // Withdraw
							it.player.message("Withdrawing " + preset.name() + "...")
							it.player.stopActions(false)
							preset.apply(it.player)
							it.player.putattrib(AttributeKey.LAST_PRESET, preset.name())
						}
						2 -> { // Delete this preset
							if (it.optionsTitled("Permanently delete it?", "Delete it.", "Nevermind.") == 1) {
								it.player.presetRepository().delete(preset)
							}
						}
						3 -> { // Rename this preset
							it.player.presetRepository().rename(preset, it.inputString("Enter new name:"))
						}
					}
				}
			}
		}
	}
	
}