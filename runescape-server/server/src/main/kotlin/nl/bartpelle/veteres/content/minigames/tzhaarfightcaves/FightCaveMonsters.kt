package nl.bartpelle.veteres.content.minigames.tzhaarfightcaves

import co.paralleluniverse.fibers.Suspendable

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Situations on 9/18/2016.
 */

object FightCaveMonsters {
	
	//Our beautiful monsters
	val TZ_KIH = 3116
	val TZ_KEK = 3118
	val TOK_XIL = 3121
	val YT_MEJKOT = 3123
	val KET_ZEK = 3125
	val TZTOK_JAD = 3127
	
	//Secondary monsters (TZ-Kek second form & TzTok Jad healers)
	val TZ_KEK_MINION = 3120
	val YT_HURKOT = 3128
	
	@Suspendable fun spawnWaveMonsters(player: Player) {
		var currentWave = FightCaveGame.getCurrentWave(player)
		
		while (currentWave >= 63) {
			currentWave -= 63
			spawnMonster(player, TZTOK_JAD)
		}
		while (currentWave >= 31) {
			currentWave -= 31
			spawnMonster(player, KET_ZEK)
		}
		while (currentWave >= 15) {
			currentWave -= 15
			spawnMonster(player, YT_MEJKOT)
		}
		while (currentWave >= 7) {
			currentWave -= 7
			spawnMonster(player, TOK_XIL)
		}
		while (currentWave >= 3) {
			currentWave -= 3
			spawnMonster(player, TZ_KEK)
		}
		while (currentWave > 0) {
			currentWave--
			spawnMonster(player, TZ_KIH)
		}
	}
	
	@Suspendable fun spawnMonster(player: Player, npc: Int) {
		val points = FightCaveGame.spawnTiles(player)
		
		// Would return 0 if we left the instance and npcs spawn a few ticks later
		if (points.size == 0)
			return
		
		val tile = points[player.world().random(points.size - 1)]
		val monster = Npc(npc, player.world(), tile)
		
		monster.walkRadius(200)
		monster.respawns(false)
		monster.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 100)
		player.world().registerNpc(monster)
		monster.attack(player)
		
		if (monster.id() == TZTOK_JAD) {
			player.executeScript @Suspendable {
				it.chatNpc("Look out, here comes TzTok-Jad!", 2180, 615)
			}
		}
	}
	
	@JvmStatic @Suspendable fun handleMonsterDeath(npc: Npc, player: Player) {
		//If the NPC was a TZ Kek, spawn it's babies!
		if (npc.id() == TZ_KEK) {
			for (i in 0..1) {
				val baby = Npc(TZ_KEK_MINION, npc.world(), npc.tile())
				baby.respawns(false)
				baby.walkRadius(200)
				baby.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 100)
				npc.world().registerNpc(baby)
				baby.attack(player)
			}
		}
		
		//If the NPC was Tztok Jad, end the minigame!
		if (npc.id() == TZTOK_JAD) {
			npc.graphic(453)
			FightCaveGame.leaveFightCave(player, true)
		}
		
		//Remove the NPC from the world & array
		npc.world().unregisterNpc(npc)
	}
	
}
