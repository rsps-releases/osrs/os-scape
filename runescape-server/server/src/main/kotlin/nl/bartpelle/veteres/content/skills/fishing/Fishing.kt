package nl.bartpelle.veteres.content.skills.fishing

import co.paralleluniverse.fibers.Suspendable
import com.google.gson.Gson
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.script.ScriptRepository
import java.io.FileReader
import java.lang.ref.WeakReference
import java.util.*

/**
 * Created by Bart on 11/21/2015.
 */
object Fishing {
	
	var fishSpots: Array<FishSpotDef> = arrayOf()
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		val gson = Gson()
		fishSpots = gson.fromJson(FileReader("data/map/fishspots.json"), Array<FishSpotDef>::class.java)
		
		fishSpots.forEach { spot ->
			// Register on world load
			r.onWorldInit {
				createSpot(it.ctx(), spot.spot!!, spot.tiles!!.asList())
			}
			
			// Add interaction
			for (i in 0..spot.spot!!.types.size - 1) {
				when (i) {
					0 -> {
						r.onNpcOption1(spot.spot!!.id) @Suspendable {
							fish(it, spot, spot.spot!!.types[i])
						}
					}
					1 -> {
						r.onNpcOption2(spot.spot!!.id) @Suspendable {
							fish(it, spot, spot.spot!!.types[i])
						}
					}
				}
			}
		}
	}
	
	// After doing ::reloadnpcs
	@JvmStatic fun respawnAllSpots(world: World) {
		val gson = Gson()
		fishSpots = gson.fromJson(FileReader("data/map/fishspots.json"), Array<FishSpotDef>::class.java)
		
		fishSpots.forEach { spot ->
			createSpot(world, spot.spot!!, spot.tiles!!.asList())
		}
		
	}
	
	fun catchChance(player: Player, type: Fish, fishingToolType: FishingToolType): Int {
		val specialToolMod = if (fishingToolType != FishingToolType.NONE) fishingToolType.boost() else 1.0
		val points = 20
		val diff = player.skills()[Skills.FISHING] - type.lvl
		return Math.min(85, (points + diff * specialToolMod).toInt())
	}
	
	@Suspendable fun fish(it: Script, spotDef: FishSpotDef, selectedAction: FishSpotType) {
		//PVP skilling check
		if (Skills.disabled(it.player(), Skills.FISHING)) {
			return
		}
		
		// Level requirement
		if (!it.player().world().realm().isPVP && it.player().skills().level(Skills.FISHING) < selectedAction.levelReq()) {
			it.messagebox("You need to be at least level ${selectedAction.levelReq()} Fishing to catch these fish.")
			return
		}

		//Represents a definition for the found tool being used, if any, to fish.
		val fishingToolDef = FishingToolType.locateItemFor(it.player())
		val overrideTool = (fishingToolDef.isPresent && FishingToolType.canUseOnSpot(fishingToolDef.get(), selectedAction) && it.player().skills().level(Skills.FISHING) >= fishingToolDef.get().levelRequired())

		// Check for the basic item first
		if (!overrideTool && !it.player().inventory().contains(selectedAction.staticRequiredItem)) {
			it.animate(-1)
			it.messagebox(selectedAction.missingText)
			return
		}
		
		// Inventory full?
		if (it.player().inventory().full()) {
			it.animate(-1)
			it.messagebox("You can't carry any more fish.")
			return
		}
		
		// Bait check!
		if (!overrideTool && selectedAction.baitItem != -1 && !it.player().inventory().contains(selectedAction.baitItem)) {
			it.animate(-1)
			it.messagebox(selectedAction.baitMissing ?: "You don't have any bait left.")
			return
		}
		
		it.message(selectedAction.start)
		it.delay(1)
		
		// Rod has an extra one here
		if (selectedAction == FishSpotType.BAIT || selectedAction == FishSpotType.FLY) {
			it.message("You attempt to catch a fish.")
		}
		
		val clearWaterActive = BonusContent.isActive(it.player, BlessingGroup.CLEAR_WATER)
		
		while (true) {
			// Fish spot ok mate?
			val target: Entity? = it.player().attrib<WeakReference<Entity>>(AttributeKey.TARGET).get()
			if (target == null || !target.isNpc || target.dead() || target.finished()) {
				it.animate(-1)
				return
			}
			
			// Check for the basic item first
			if (!overrideTool && !it.player().inventory().contains(selectedAction.staticRequiredItem)) {
				it.animate(-1)
				it.messagebox(selectedAction.missingText)
				return
			}
			
			// Inventory full?
			if (it.player().inventory().full()) {
				it.animate(-1)
				it.messagebox("You can't carry any more fish.")
				return
			}
			
			// Bait check!
			if (!overrideTool && selectedAction.baitItem != -1 && !it.player().inventory().contains(selectedAction.baitItem)) {
				it.animate(-1)
				it.messagebox(selectedAction.baitMissing ?: "You don't have any bait left.")
				return
			}
			
			it.animate(if (overrideTool) fishingToolDef.get().animationId() else selectedAction.anim)
			it.delay(3)
			
			// The Clear Water blessing gives us the best available fish, always.
			val weCatch = if (clearWaterActive)
				selectedAction.bestFish(it.player().skills().level(Skills.FISHING))
			else
				selectedAction.randomFish(it.player().world(), it.player().skills().level(Skills.FISHING))
			
			if (it.player().world().rollDie(100, catchChance(it.player(), weCatch, if (overrideTool) fishingToolDef.get() else FishingToolType.NONE))) {
				it.player().message("You catch ${weCatch.prefix} ${weCatch.fishName}.")
				
				// Do we need to remove bait?
				if (selectedAction.baitItem != -1) {
					it.player().inventory().remove(Item(selectedAction.baitItem), true)
				}
				
				// Chance to kill the spot
				if (!it.player().world().realm().isPVP && it.player().world().rollDie(75, 1)) {
					it.animate(-1)
					val target: Entity? = it.player().attrib<WeakReference<Entity>>(AttributeKey.TARGET).get()
					if (target != null && target.isNpc)
						it.player().world().unregisterNpc(target as Npc) // Finished the npc, stops fishing for others
					
					// Brew a new fishie!
					val newspot = createSpot(it.player().world(), spotDef.spot!!, spotDef.tiles!!.asList())
					if (it.player().attribOr(AttributeKey.DEBUG, false)) {
						it.player().write(SetHintArrow(newspot))
						it.player().message("new spot is " + it.player().tile().distance(newspot.tile()) + " sq away at " + newspot.tile() + " : " + Arrays.toString(newspot.def()!!.options!!))
					}
				}
				
				// Woo! A pet! The reason we do this BEFORE the item is because it's... quite some more valuable :)
				// Rather have a pet than a slimy fishy thing, right?
				val odds = (weCatch.petChance.toDouble() * it.player().mode().skillPetMod()).toInt()
				if (it.player().world().rollDie(odds, 1)) {
					unlockHeron(it.player())
				}

				//Check achievements.
				AchievementAction.processCategoryAchievement(it.player(), AchievementCategory.FISHING, weCatch.item)
				
				if (clearWaterActive && it.player.world().rollDie(2, 1)
						&& it.player.inventory().add(Item(weCatch.item), true).success()) {
					it.player.message("The Clear Water blessing allows you to catch another fish!")
				}

                it.player().inventory().add(Item(weCatch.item), true)
				it.addXp(Skills.FISHING, weCatch.xp)
				
				//Finding blood money while skilling..
				if (it.player().world().realm().isOSRune && WildernessLevelIndicator.inWilderness(it.player().tile())) {
					if (it.player().world().rollDie(5, 1)) {
						if (it.player().inventory().add(Item(13307, 1), false).success()) {
							it.player().message("You find 1 blood money coin inside the water.")
						}
					}
				}
				
				//Finding a casket in the water! Money, money, money..
				if (it.player().world().realm().isOSRune && it.player().world().rollDie(20, 1)) {
					if (it.player().inventory().add(Item(7956, 1), false).success()) {
						it.player().message("You find a casket in the water.")
					}
				}
				
			}
			it.delay(3)
		}
	}
	
	fun unlockHeron(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.HERON)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(Pet.HERON.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.HERON, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(Pet.HERON.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.HERON.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
	fun createSpot(world: World, spot: FishSpot, possible: List<Tile>): Npc {
		Collections.shuffle(possible)
		val npc = Npc(spot.id, world, randomFreeSpotTile(world, possible))
		npc.putattrib(AttributeKey.POSSIBLE_FISH_TILES, possible)
		world.registerNpc(npc)
		return npc
	}
	
	fun randomFreeSpotTile(world: World, tiles: List<Tile>): Tile {
		loop@ for (tile in tiles) {
			// See if it conflicts
			for (npc in world.npcs().stream()) {
				if (npc?.tile()?.equals(tile) ?: false) {
					continue@loop
				}
			}
			
			// Did not conflict yet. Use it!
			return tile
		}
		return tiles.first()
	}
	
	class FishSpotDef {
		var spot: FishSpot? = null
		val tiles: Array<Tile>? = null
	}
	
}