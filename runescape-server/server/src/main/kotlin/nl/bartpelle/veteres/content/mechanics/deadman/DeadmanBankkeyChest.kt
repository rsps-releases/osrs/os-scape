package nl.bartpelle.veteres.content.mechanics.deadman

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import java.util.*

/**
 * Created by Jak on 05/11/2016.
 *
 * Interacting with the Deadman Bank looting Chests, where you take items out of keys you have PKed.
 */
object DeadmanBankkeyChest {
	
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		// Open deadman chest
		repo.onObject(27290) {
			if (!it.player().inventory().hasAny(13302, 13303, 13304, 13305, 13306)) {
				it.message("You need to have a Bank Key to open this chest.")
			} else {
				// Open a tab where we have a key
				for (i in intArrayOf(13302, 13303, 13304, 13305, 13306)) {
					if (it.player().inventory().has(i)) {
						it.player().varps().varbit(Varbit.DEADMAN_ACTIVE_KEY_VIEWED, i - 13302)
						break
					}
				}
				val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
				it.player().interfaces().sendMain(234)
				
				for (i in 0..4) {
					val value: Long = dmminfo.keys[i].value
					if (!it.player().inventory().contains(i + 13302)) {
						it.player().write(InvokeScript(1169, i))
					} else {
						it.player().write(InvokeScript(1169, i, formatValue(value)))
					}
				}
				
				it.player().write(InterfaceSettings(234, 14, 0, 5, 2))
				it.player().write(InterfaceSettings(234, 15, 0, 139, 2046))
				it.player().write(InterfaceText(234, 40, "" + it.player().bank().nextFreeSlot()))
				it.player().write(InterfaceText(234, 42, "800"))
				
				var container = 63744
				var key = 558
				for (i in 0..4) {
					if (it.player().inventory().contains(i + 13302)) {
						it.player().write(SetItems(key++, -1, container--, dmminfo.keys[i].lootcontainer))
					}
				}
			}
		}
		
		repo.onButton(234, 14) {
			// Key being viewed
			val action = it.buttonAction()
			val slot = it.buttonSlot()
			val item = it.itemUsedId()
			if (action == 1 && item == -1) {
				it.player().varps().varbit(Varbit.DEADMAN_ACTIVE_KEY_VIEWED, slot)
			}
		}
		
		repo.onButton(234, 15) {
			// Take items out of key
			val action = it.buttonAction()
			val idx = it.player().varps().varbit(Varbit.DEADMAN_ACTIVE_KEY_VIEWED)
			val slot = it.buttonSlot() - (idx * 28)
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val amount = when (action) {
				1, 6 -> 1
				2, 7 -> 5
				3, 8 -> 10
				4, 9 -> it.inputInteger("How many would you like to ${if (action == 4) "bank" else "take"}?")
				5, 10 -> Integer.MAX_VALUE
				else -> 1
			}
			val asNote = it.player.varps().varbit(Varbit.DEADMAN_WITHDRAW_LOOT_TYPE) == 1
			val key = dmminfo.keys[idx]
			val lootitem: Item = key.lootcontainer[slot] ?: return@onButton
			val rawLoot = Item(lootitem.id(), Math.min(lootitem.amount(), amount))
			val toBank = action in 1..5
			val target = if (toBank) it.player().bank() else it.player().inventory()
			val loot = if ((!asNote && !toBank) || toBank) rawLoot.unnote(it.player().world()) else rawLoot
			val added = target.add(loot, false).completed()
			if (added > 0) {
				key.lootcontainer.remove(Item(rawLoot, added), true)
				key.value -= loot.unnote(it.player().world()).realPrice(it.player().world()) * added
			}
			refreshKey(dmminfo, idx)
			if (key.lootcontainer.freeSlots() == key.lootcontainer.size()) {
				it.player().inventory().remove(Item(keyIdForIdx(idx)), true)
				it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, Math.max(0, it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED) - 1))
				it.player().looks().update()
			}
		}
		
		repo.onButton(234, 26) {
			// Withdraw as Item
			it.player().varps().varbit(Varbit.DEADMAN_WITHDRAW_LOOT_TYPE, 0)
		}
		
		repo.onButton(234, 31) {
			// Withdraw as Note
			it.player().varps().varbit(Varbit.DEADMAN_WITHDRAW_LOOT_TYPE, 1)
		}
		
		repo.onButton(234, 37) {
			// All to inventory
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val idx = it.player().varps().varbit(Varbit.DEADMAN_ACTIVE_KEY_VIEWED)
			val asNote = it.player.varps().varbit(Varbit.DEADMAN_WITHDRAW_LOOT_TYPE) == 1
			val key = dmminfo.keys[idx]
			val toRemove = ArrayList<Item>(0)
			for (loot in key.lootcontainer) {
				loot ?: continue
				val added = it.player().inventory().add(if (asNote) loot.note(it.player().world()) else loot, false).completed()
				if (added > 0)
					toRemove.add(Item(loot.unnote(it.player().world()).id(), added))
			}
			for (looted in toRemove) {
				key.lootcontainer.remove(looted, true)
				key.value -= looted.realPrice(it.player().world()) * looted.amount()
			}
			refreshKey(dmminfo, idx)
			if (key.lootcontainer.freeSlots() != key.lootcontainer.size()) {
				it.player().message("Not all of the key's items could be added to your inventory.")
			} else {
				it.player().inventory().remove(Item(keyIdForIdx(idx)), true)
				it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, Math.max(0, it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED) - 1))
				it.player().looks().update()
			}
		}
		
		repo.onButton(234, 44) {
			// All to bank
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val idx = it.player().varps().varbit(Varbit.DEADMAN_ACTIVE_KEY_VIEWED)
			val key = dmminfo.keys[idx]
			val toRemove = ArrayList<Item>(0)
			for (loot in key.lootcontainer) {
				loot ?: continue
				val added = it.player().bank().add(loot.unnote(it.player().world()), false).completed()
				if (added > 0)
					toRemove.add(Item(loot.id(), added))
			}
			for (looted in toRemove) {
				key.lootcontainer.remove(looted, true)
				key.value -= looted.unnote(it.player().world()).realPrice(it.player().world()) * looted.amount()
			}
			refreshKey(dmminfo, idx)
			if (key.lootcontainer.freeSlots() != key.lootcontainer.size()) {
				it.player().message("Not all of the key's items could be added to your bank.")
			} else {
				it.player().inventory().remove(Item(keyIdForIdx(idx)), true)
				it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, Math.max(0, it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED) - 1))
				it.player().looks().update()
			}
		}
		
		repo.onButton(234, 8) {
			// Destroy key
			val dmminfo = DeadmanMechanics.infoForPlayer(it.player())
			val idx = it.player().varps().varbit(Varbit.DEADMAN_ACTIVE_KEY_VIEWED)
			dmminfo.keys[idx].lootcontainer.empty()
			dmminfo.keys[idx].value = 0L
			refreshKey(dmminfo, idx)
			it.player().inventory().remove(Item(keyIdForIdx(idx)), true)
			it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, Math.max(0, it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED) - 1))
			it.player().looks().update()
		}
	}
	
	/**
	 * The ClientScript requires the value sent as a String.
	 */
	private fun formatValue(value: Long): String {
		val v = value.toDouble()
		var res = value
		var end = ""
		if (value >= 1000000) {
			res = (v / 1000000).toLong()
			end = "m"
		} else if (value >= 1000) {
			res = (v / 1000).toLong()
			end = "k"
		}
		return if (res <= 0L) "" else res.toString() + end
	}
	
	/**
	 * There are 5 in-game items for each of the 5 keys you can hold at once.
	 */
	private fun keyIdForIdx(idx: Int): Int {
		return when (idx) {
			1 -> 13303
			2 -> 13304
			3 -> 13305
			4 -> 13306
			else -> 13302
		}
	}
	
	/**
	 * Send the items of a key onto the loot interface.
	 */
	private fun refreshKey(dmminfo: DeadmanContainers, idx: Int) {
		val containerKey = when (idx) {
			1 -> 559
			2 -> 560
			3 -> 561
			4 -> 562
			else -> 558
		}
		val containerChild = when (idx) {
			1 -> 63743
			2 -> 63742
			3 -> 63741
			4 -> 63740
			else -> 63744
		}
		dmminfo.player.write(InvokeScript(1169, idx, formatValue(dmminfo.keys[idx].value)))
		dmminfo.player.write(SetItems(containerKey, -1, containerChild, dmminfo.keys[idx].lootcontainer))
	}
}