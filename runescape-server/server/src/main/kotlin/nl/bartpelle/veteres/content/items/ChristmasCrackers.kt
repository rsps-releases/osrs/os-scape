package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Sitautions on 4/17/2016.
 */

object ChristmasCrackers {
	
	val cracker = 962
	
	val party_hats: ArrayList<Item> = arrayListOf(Item(1038, 1), Item(1040, 1), Item(1042, 1),
			Item(1044, 1), Item(1046, 1), Item(1048, 1))
	
	val secondary_item: ArrayList<Item> = arrayListOf(Item(1973, 1), Item(1897, 1), Item(1635, 1),
			Item(441, 5), Item(1217, 1), Item(563, 1), Item(1969, 1), Item(950, 1), Item(1718, 1),
			Item(2355, 1))
	
	//TODO - Add correct animation and gfx as well as ensure the player is beside the target before pulling the cracker,.
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		r.onItemOnPlayer(cracker, s@ @Suspendable {
			val target = it.target() as Player
			if (target.interfaces().activeMain() != -1) {
				it.player().message("That player is busy.")
				return@s
			}
			val party_hat_reward = it.player().world().random(party_hats.size - 1)
			val secondary_reward = it.player().world().random(secondary_item.size - 1)
			
			it.messagebox("Warning! Using this item on a player will crack the item open causing it to disappear. Upon cracking it, you and your partner have a 50% chance of receiving the rewards.")
			if (it.optionsTitled("Really crack this cracker?", "Yes, I'd like to crack the christmas cracker!", "No, ") == 1) {
				if (it.player().inventory().freeSlots() < 1) {
					it.messagebox("You require an extra inventory slot to do this.")
				} else if (target.inventory().freeSlots() < 1) {
					it.messagebox("Your partner requires an extra inventory slot to do this.")
				} else if (it.player().inventory().contains(cracker)) {
					it.player().lock()
					it.message("You pull a Christmas cracker...")
					it.animate(451)
					it.player().graphic(176, 82, 0)
					it.delay(1)
					if (it.player().inventory().remove(Item(cracker, 1), true).success()) {
						if (it.player().world().rollDie(3, 1)) {
							it.message("Hey! I got the cracker!")
							target.message("%s%s%s got the prize!", "<col=FF0000>", it.player().name(), "</col>")
							it.player().inventory().add(Item(party_hats[party_hat_reward].id(), party_hats[party_hat_reward].amount()), true)
//							it.player().inventory().add(Item(secondary_item[secondary_reward].id(), secondary_item[secondary_reward].amount()), true)
						} else {
							it.message("%s%s%s got the prize!", "<col=FF0000>", target.name(), "</col>")
							target.message("Hey! I got the cracker from %s%s%s!", "<col=FF0000>", it.player().name(), "</col>")
							target.inventory().add(Item(party_hats[party_hat_reward].id(), party_hats[party_hat_reward].amount()), true)
//							target.inventory().add(Item(secondary_item[secondary_reward].id(), secondary_item[secondary_reward].amount()), true)
						}
					}
					it.player().unlock()
				}
			}
		})
	}
}
