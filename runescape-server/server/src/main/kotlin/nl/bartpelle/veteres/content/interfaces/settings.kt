package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.mechanics.UnifiedSettingsViewer
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Bart on 9/6/2015.
 */
@ScriptMain fun settings(r: ScriptRepository) {
	// Brightness settings
	r.onButton(261, 17) { it.player().varps()[Varp.BRIGHTNESS] = 1 }
	r.onButton(261, 18) { it.player().varps()[Varp.BRIGHTNESS] = 2 }
	r.onButton(261, 19) { it.player().varps()[Varp.BRIGHTNESS] = 3 }
	r.onButton(261, 20) { it.player().varps()[Varp.BRIGHTNESS] = 4 }
	
	r.onButton(261, 24) {
		// Advanced options
		it.player().interfaces().sendMain(60)
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
	}
	
	// Music volume
	r.onButton(261, 27) { it.player().varps()[Varp.MUSIC_VOLUME] = 4 }
	r.onButton(261, 28) { it.player().varps()[Varp.MUSIC_VOLUME] = 3 }
	r.onButton(261, 29) { it.player().varps()[Varp.MUSIC_VOLUME] = 2 }
	r.onButton(261, 30) { it.player().varps()[Varp.MUSIC_VOLUME] = 1 }
	r.onButton(261, 31) { it.player().varps()[Varp.MUSIC_VOLUME] = 0 }
	
	// Sound effects volume
	r.onButton(261, 33) { it.player().varps()[Varp.SFX_VOLUME] = 4 }
	r.onButton(261, 34) { it.player().varps()[Varp.SFX_VOLUME] = 3 }
	r.onButton(261, 35) { it.player().varps()[Varp.SFX_VOLUME] = 2 }
	r.onButton(261, 36) { it.player().varps()[Varp.SFX_VOLUME] = 1 }
	r.onButton(261, 37) { it.player().varps()[Varp.SFX_VOLUME] = 0 }
	
	// Area sound volume
	r.onButton(261, 39) { it.player().varps()[Varp.AREA_VOLUME] = 4 }
	r.onButton(261, 40) { it.player().varps()[Varp.AREA_VOLUME] = 3 }
	r.onButton(261, 41) { it.player().varps()[Varp.AREA_VOLUME] = 2 }
	r.onButton(261, 42) { it.player().varps()[Varp.AREA_VOLUME] = 1 }
	r.onButton(261, 43) { it.player().varps()[Varp.AREA_VOLUME] = 0 }
	
	// Chat effects
	r.onButton(261, 45) {
		it.player().varps()[Varp.CHAT_EFFECTS] = if (it.player().varps()[Varp.CHAT_EFFECTS] == 1) 0 else 1
	}
	
	// Private chat toggle
	r.onButton(261, 47) {
		it.player().varps()[Varp.SPLIT_PRIVATE_CHAT] = if (it.player().varps()[Varp.SPLIT_PRIVATE_CHAT] == 1) 0 else 1
		it.player().write(InvokeScript(83))
	}
	r.onButton(261, 49) {
		it.player().varps().varbit(Varbit.HIDE_PM_WHEN_CHAT_HIDDEN, if (it.player().varps().varbit(Varbit.HIDE_PM_WHEN_CHAT_HIDDEN) == 1) 0 else 1)
		if (!it.player().interfaces().resizable())
			it.message("That option is applicable only in resizable mode with 'Split Private Chat' enabled.")
	}
	r.onButton(261, 51) {
		it.player().varps()[Varp.PROFANITY] = if (it.player().varps()[Varp.PROFANITY] == 1) 0 else 1
	}
	r.onButton(261, 53) {
		if (it.player().world().realm().isPVP) {
			UnifiedSettingsViewer.open(it)
		} else {
			LootPanel.openLootNotificationPanel(it)
		}
	}
	r.onButton(261, 55) {
		it.player().varps().varbit(Varbit.LOGIN_TIMEOUT_HIDDEN, if (it.player().varps().varbit(Varbit.LOGIN_TIMEOUT_HIDDEN) == 1) 0 else 1)
	}
	r.onButton(261, 57) {
		// TODO display name inter
	}
	
	r.onButton(261, 59) {
		it.player().varps()[Varp.ONE_MOUSEBUTTON] = if (it.player().varps()[Varp.ONE_MOUSEBUTTON] == 1) 0 else 1
	}
	r.onButton(261, 61) {
		it.player().varps().varbit(Varbit.MIDDLE_MOUSE, if (it.player().varps().varbit(Varbit.MIDDLE_MOUSE) == 1) 0 else 1)
	}
	r.onButton(261, 63) {
		it.player().varps().varbit(Varbit.FOLLOWER_PRIORITY, if (it.player().varps().varbit(Varbit.FOLLOWER_PRIORITY) == 1) 0 else 1)
	}
	
	//'Set Keybinding' interface.
	r.onButton(261, 65) {
		it.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
		it.player().interfaces().setting(121, 112, 0, 13, 2)
		it.player().interfaces().sendMain(121)
	}
	
	r.onButton(261, 67) {
		it.player().varps().varbit(Varbit.SHIFT_CLICK_DROP, if (it.player().varps().varbit(Varbit.SHIFT_CLICK_DROP) == 1) 0 else 1)
	}
	
	r.onButton(261, 74) {
		it.player().varps()[Varp.ACCEPT_AID] = if (it.player().varps()[Varp.ACCEPT_AID] == 1) 0 else 1
	}
	
	r.onButton(261, 77) {
		if (it.player().attribOr<Double>(AttributeKey.RUN_ENERGY, 0) >= 1.0) {
			it.player().varps()[Varp.RUNNING_ENABLED] = if (it.player().varps()[Varp.RUNNING_ENABLED] == 1) 0 else 1
		} else {
			it.message("You don't have enough energy left to run!")
		}
	}
	
	r.onButton(261, 80, @Suspendable {
		// House options
		it.player().interfaces().sendInventory(370)
		it.player().write(InterfaceText(370, 15, "Number of rooms: 9"))
	})
	
	r.onButton(261, 820) {
		// OSRS bonds.
		it.player().write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
		it.player().interfaces().sendMain(65)
		it.player().write(InvokeScript(733, 0, 0, 0, 0, 0, 0, 0))
	}
	
	// Player attack priority dropdown
	r.onButton(261, 85) {
		it.player().varps().varp(Varp.PLAYER_ATTACK_PRIORITY, it.buttonSlot() - 1)
	}
	
	// Npc attack priority dropdown
	r.onButton(261, 86) {
		it.player().varps().varp(Varp.NPC_ATTACK_PRIORITY, it.buttonSlot() - 1)
	}
	
	// ADVANCED OPTIONS - INTERFACE 60
	r.onButton(60, 6) {
		it.player().varps().varbit(Varbit.SIDEPANELS_OPAQUE, if (it.player().varps().varbit(Varbit.SIDEPANELS_OPAQUE) == 1) 0 else 1)
	}
	r.onButton(60, 8) {
		it.player().varps().varbit(Varbit.SHOW_XP_TILL_LEVEL, if (it.player().varps().varbit(Varbit.SHOW_XP_TILL_LEVEL) == 1) 0 else 1)
	}
	r.onButton(60, 10) {
		// TODO prayer overlay toggle
	}
	r.onButton(60, 12) {
		// TODO special attack description overlay toggle
	}
	r.onButton(60, 16) {
		it.player().varps().varbit(Varbit.DATAORBS_HIDDEN, if (it.player().varps().varbit(Varbit.DATAORBS_HIDDEN) == 1) 0 else 1)
		if (it.player().varps().varbit(Varbit.DATAORBS_HIDDEN) == 0) {
			it.player().interfaces().closeById(160) // Close the old position.
			it.player().interfaces().sendWidgetOn(160, Interfaces.InterSwitches.ORBS)
		} else {
			it.player().interfaces().closeById(160)
		}
	}
	r.onButton(60, 18) { // Box toggle
		it.player().varps().varbit(Varbit.CHATBOX_TRANSPARENT, if (it.player().varps().varbit(Varbit.CHATBOX_TRANSPARENT) == 1) 0 else 1)
	}
	r.onButton(60, 20) { // Tick box toggle
		it.player().varps().varbit(Varbit.CHATBOX_SOLID, if (it.player().varps().varbit(Varbit.CHATBOX_SOLID) == 1) 0 else 1)
	}
	r.onButton(60, 21) { // Box toggle
		it.player().varps().varbit(Varbit.SIDESTONES_ARRANGEMENT, if (it.player().varps().varbit(Varbit.SIDESTONES_ARRANGEMENT) == 1) 0 else 1)
		if (it.player().interfaces().resizable()) {
			it.player().interfaces().sendForMode(if (it.player().varps().varbit(Varbit.SIDESTONES_ARRANGEMENT) == 1) 1 else 2,
					if (it.player().varps().varbit(Varbit.SIDESTONES_ARRANGEMENT) == 1) 2 else 1)
		}
	}
	r.onButton(60, 23) { // Tick box toggle
		it.player().varps().varbit(Varbit.TABS_CAN_BE_CLOSED_BY_HOTKEY, if (it.player().varps().varbit(Varbit.TABS_CAN_BE_CLOSED_BY_HOTKEY) == 1) 0 else 1)
	}
	
	// GAME NOTIFICATIONS - INTERFACE 492
	r.onButton(492, 7) @Suspendable {
		var promptSet = false
		if (it.buttonAction() == 1 && it.player().varps().varbit(Varbit.ENABLE_UNTRADABLE_LOOT_NOTIFICATIONS_BUTTONS) == 1) { // Enabled, and you want to disable
			it.player().varps().varbit(Varbit.ENABLE_UNTRADABLE_LOOT_NOTIFICATIONS_BUTTONS, 0)
		} else if (it.buttonAction() == 2) {
			// Or changing value.
			promptSet = true
		} else if (it.player().varps().varbit(Varbit.LOOT_DROP_THRESHOLD_VALUE) > 0) { // Must be disabled, but a value has previously been set. Just enable.
			it.player().varps().varbit(Varbit.ENABLE_UNTRADABLE_LOOT_NOTIFICATIONS_BUTTONS, 1)
		} else {
			promptSet = true
		}
		if (promptSet) {
			val extra = if (it.player().varps().varbit(Varbit.LOOT_DROP_THRESHOLD_VALUE) == 0) "" else "(${it.player().varps().varbit(Varbit.LOOT_DROP_THRESHOLD_VALUE)} coins)"
			val threshold = it.inputInteger("Set threshold value: $extra")
			it.player().varps().varbit(Varbit.ENABLE_UNTRADABLE_LOOT_NOTIFICATIONS_BUTTONS, if (threshold > 0) 1 else 0)
			it.player().varps().varbit(Varbit.LOOT_DROP_THRESHOLD_VALUE, threshold)
		}
	}
	
	r.onButton(492, 11) {
		it.player().varps().varbit(Varbit.UNTRADABLE_LOOT_NOTIFICATIONS, if (it.buttonSlot() == 0) 0 else 1)
	}
	r.onButton(492, 12) {
		it.player().varps().varbit(Varbit.BOSS_KC_NOTICATION, if (it.buttonSlot() == 0) 0 else 1)
		VarbitAttributes.set(it.player(), 12, if (it.buttonSlot() == 0) 0 else 1)
	}
	
	r.onButton(492, 2) {
		// Close
		it.player().interfaces().closeById(492)
		// Why RS closes these as well idk tbh.
		for (i in intArrayOf(593, 320, 399, 149, 387, 541, 218, 7, 429, 432, 182, 261, 216, 239))
			it.player().interfaces().sendWidgetOn(i, Interfaces.InterSwitches.forWidget(i))
		it.player().updateWeaponInterface() // Strings must be sent again.
	}
	
	r.onButton(492, 14) {
		var promptSet = false
		if (it.buttonAction() == 1 && it.player().varps().varbit(Varbit.DROP_ITEM_WARNING) == 1) {
			it.player().varps().varbit(Varbit.DROP_ITEM_WARNING, 0)
		} else if (it.buttonAction() == 2) { // Change value if enabled
			promptSet = true
		} else if (it.player().varps().varbit(Varbit.DROP_ITEMS_WARNING_VALUE) > 0) {
			it.player().varps().varbit(Varbit.DROP_ITEM_WARNING, 1)
		} else {
			promptSet = true
		}
		if (promptSet) {
			val extra = if (it.player().varps().varbit(Varbit.DROP_ITEMS_WARNING_VALUE) == 0) "" else " (${it.player().varps().varbit(Varbit.DROP_ITEMS_WARNING_VALUE)} coins)"
			val threshold = it.inputInteger("Change threshold value: $extra")
			it.player().varps().varbit(Varbit.DROP_ITEMS_WARNING_VALUE, threshold)
			it.player().varps().varbit(Varbit.DROP_ITEM_WARNING, if (threshold > 1) 1 else 0)
		}
	}
}

object LootPanel {
	fun openLootNotificationPanel(it: Script) {
		// Show notification settings
		for (i in intArrayOf(593, 320, 399, 149, 387, 541, 218, 7, 429, 432, 182, 261, 216, 239)) {
			it.player().interfaces().closeById(i)
		}
		it.player().interfaces().sendWidgetOn(492, Interfaces.InterSwitches.INVENTORY_INTER)
		it.player().interfaces().setting(492, 11, 0, 1, 2)
		it.player().interfaces().setting(492, 12, 0, 1, 2)
	}
}
