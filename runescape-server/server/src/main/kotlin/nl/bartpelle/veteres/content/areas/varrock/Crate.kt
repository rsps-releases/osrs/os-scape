package nl.bartpelle.veteres.content.areas.varrock

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/9/2016.
 */

object Crate {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(20885) { it.player().world().shop(33).display(it.player()) }
	}
}
