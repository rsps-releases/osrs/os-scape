package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.NpcExamine
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Mack on 11/10/2017.
 */
object SurvivalExpert {
	
	const val SURVIVAL_EXPERT = 3306
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onNpcOption1(SURVIVAL_EXPERT, @Suspendable {
			when (it.options("Who are you?", "Look up a monster.")) {
				1 -> {
					it.chatNpc("I am the Survival Expert of Os-Scape. I've traveled around the lands and encountered any creature you could think of.", SURVIVAL_EXPERT)
					it.chatNpc("Many come to me seeking information and advice on various monsters. If you wish to ask<br> me about a specific creature, you may.", SURVIVAL_EXPERT)
					when (it.options("Ok, I'll look up a monster.", "Nothing comes to mind at the moment.")) {
						1 -> {
							NpcExamine.lookup(it, it.inputString("Enter NPC name:"))
						}
						2 -> {
							it.chatPlayer("Nothing comes to mind at the moment.")
							it.chatNpc("Take care, traveler.", SURVIVAL_EXPERT)
						}
					}
				}
				2 -> {
					NpcExamine.lookup(it, it.inputString("Enter NPC name:"))
				}
			}
		})
	}
}