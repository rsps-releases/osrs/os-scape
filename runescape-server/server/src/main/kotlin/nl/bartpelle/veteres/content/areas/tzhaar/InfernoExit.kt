package nl.bartpelle.veteres.content.areas.tzhaar

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Mack on 7/22/2017.
 */
object InfernoExit {

    @JvmStatic @ScriptMain fun register(repo: ScriptRepository)  {
        repo.onObject(30283) s@ {
            
            val session: InfernoSession? = it.player().get<InfernoSession>(AttributeKey.INFERNO_SESSION)
            
            //If their session isn't active and they're somehow within an instance we tp them out to prevent a NPE.
            if (Objects.isNull(session)) {
                it.player().teleport(InfernoContext.INFERNO_OUTSIDE_LOC)
                return@s
            }
            
            when(it.interactionOption()) {
                1 -> {
                    when (it.optionsTitled("<col=FF0000>Really leave?", "Yes - really leave.", "No, I'll stay.")) {
                        1 -> {
                            session!!.end(false)
                        }
                    }
                }
                2 -> {
                    session!!.end(false)
                }
            }
        }
    }
}