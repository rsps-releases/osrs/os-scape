package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-11.
 */

object TowerAdvisor {
	
	val TOWER_ADVISORS = intArrayOf(6061, 6062, 6063, 6064)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (ADVISOR in TOWER_ADVISORS) {
			r.onNpcOption1(ADVISOR) @Suspendable {
				it.chatPlayer("Hello there, what do you do here?")
				it.chatNpc("Hi. We are in charge of this practice area.", ADVISOR)
				it.chatPlayer("This is a practice area?", 575)
				it.chatNpc("Surrounding us are four towers. Each tower contains trained archers of a different level. " +
						"You'll notice it's quite a distance, so you'll need a longbow.", ADVISOR)
				val rangeLevel = it.player().skills().xpLevel(Skills.RANGED)
				if (rangeLevel > 90) {
					it.chatNpc("Looks like you're very skilled, so I advise you to practice on the west tower. That'll " +
							"provide the best challenge for you.", ADVISOR)
				} else if (rangeLevel > 70) {
					it.chatNpc("Looks like you're skilled, so I advise you to practice on the south tower. That'll provide " +
							"the best challenge for you.", ADVISOR)
				} else if (rangeLevel > 50) {
					it.chatNpc("Looks like you're kind of new, so I advise you to practice on the east tower. That'll provide " +
							"the best challenge for you.", ADVISOR)
				} else {
					it.chatNpc("Looks like you're new, so I advise you to practice on the north tower. That'll provide " +
							"the best challenge for you.", ADVISOR)
				}
			}
		}
		
		r.onObject(11662) @Suspendable { Ladders.ladderDown(it, Tile(2667, 3429, 0), true) }
		r.onObject(11661, s@ @Suspendable {
			it.player().message("Nothing interesting happens.")
			return@s
			
			Ladders.ladderUp(it, Tile(2668, 3429, 2), true)
			it.player().world().npcs().forEachInAreaKt(it.player().tile().area(5), { npc ->
				TOWER_ADVISORS.forEach { advisor ->
					if (npc.id() == advisor) {
						when (npc.id()) {
							6061 -> npc.sync().shout("The north tower is occupied, get them!")
							6062 -> npc.sync().shout("The east tower is occupied, get them!")
							6063 -> npc.sync().shout("The south tower is occupied, get them!")
							6064 -> npc.sync().shout("The west tower is occupied, get them!")
						}
					}
				}
			})
		})
	}
}