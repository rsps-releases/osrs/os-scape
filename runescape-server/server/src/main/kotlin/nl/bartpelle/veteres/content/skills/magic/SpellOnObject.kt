package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 10/05/2016.
 */
object SpellOnObject {
	
	val LUNAR_SPELLBOOK = 218 // all spellbooks have same parent
	val FERTILE_SOIL = 120
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onSpellOnObject(LUNAR_SPELLBOOK, FERTILE_SOIL, s@ @Suspendable {
			val player = it.player()
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			player.faceObj(obj)
			player.message("This spell has not been added yet.")
		})
	}
}