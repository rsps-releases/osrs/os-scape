package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 11/9/2015.
 */

object MageArenaLever {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(9706) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			//Check to see if the player is teleblocked
			it.player().faceObj(obj)
			if (!it.player().timers().has(TimerKey.TELEBLOCK)) {
				it.player().lock()
				it.delay(1)
				it.animate(2710)
				it.message("You pull the lever...")
				it.runGlobal(player.world()) @Suspendable {
					it.delay(1)
					val world = player.world()
					val spawned = MapObj(obj.tile(), 9706, obj.type(), obj.rot())//TODO proper
					world.spawnObj(spawned)
					it.delay(5)
					world.removeObjSpawn(spawned)
				}
				it.delay(2)
				it.animate(714)
				it.player().graphic(111, 110, 0)
				it.delay(4)
				player.teleport(player.tile().transform(0, -5))
				it.animate(-1)
				it.player().unlock()
				it.player().varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, 1)
				it.message("...and get teleported into the arena!")
			} else {
				it.player().teleblockMessage()
			}
		}
		
		repo.onObject(9707) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			//Check to see if the player is teleblocked
			if (!it.player().timers().has(TimerKey.TELEBLOCK)) {
				it.player().lock()
				it.player().faceObj(obj)
				it.delay(1)
				it.animate(2710)
				it.message("You pull the lever...")
				it.runGlobal(player.world()) @Suspendable {
					val world = player.world()
					val spawned = MapObj(obj.tile(), 9707, obj.type(), obj.rot())//TODO proper
					world.spawnObj(spawned)
					it.delay(6)
					world.removeObjSpawn(spawned)
				}
				it.delay(2)
				it.animate(714)
				it.player().graphic(111, 110, 0)
				it.delay(4)
				player.teleport(player.tile().transform(0, 5))
				it.animate(-1)
				it.player().unlock()
				it.player().varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, 0)
				it.message("...and get teleported out of the arena!")
			} else {
				it.player().teleblockMessage()
			}
		}
	}
	
}
