package nl.bartpelle.veteres.content.treasuretrails.rewards

import nl.bartpelle.veteres.content.mechanics.ServerAnnouncements
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets.clueScrollReward
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 2016-11-11.
 */

object MediumRewards {
	
	fun generateMediumReward(player: Player, source: Int) {
		val commonRewardAmount = player.world().random(1..4)
		for (i in 1..commonRewardAmount) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = CommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			
			if (player.world().realm().isPVP) {
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, player.world().random(reward.amount)), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
			}
		}
		
		//Roll for good items..
		if (player.world().rollDie(2, 1)) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			if (player.world().realm().isPVP) {
				//Change coins to blood money
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from a medium clue scroll!")
		}
		
		//If we're on the PVP world, automatically give something from the uncommon table..
		if (player.world().realm().isPVP) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from a medium clue scroll!")
			
			//Change coins to blood money
			if (reward.reward.id() == 995)
				reward.reward.id() == 13307
			
			player.clueScrollReward().add(Item(reward.reward), true)
		}
	}
	
	enum class CommonRewards(val probability: Double, val reward: Item, val amount: IntRange = 1..1) {
		BLOOD_MONEY(100.0, Item(13307), 10000..10000),
	}
	
	enum class UncommonRewards(val probability: Double, val reward: Item) {
		MITHRIL_FULL_HELM_T(100.0, Item(12293)),
		MITHRIL_PLATE_BODY_T(100.0, Item(12287)),
		MITHRIL_PLATE_LEGS_T(100.0, Item(12289)),
		MITHRIL_PLATE_SKIRT_T(100.0, Item(12295)),
		MITHRIL_KITE_SHIELD_T(100.0, Item(12291)),
		MITHRIL_FULL_HELM_G(100.0, Item(12283)),
		MITHRIL_PLATE_BODY_G(100.0, Item(12277)),
		MITHRIL_PLATE_LEGS_G(100.0, Item(12279)),
		MITHRIL_PLATE_SKIRT_G(100.0, Item(12285)),
		MITHRIL_KITE_SHIELD_G(100.0, Item(12281)),
		ADAMANT_FULL_HELM_T(100.0, Item(2605)),
		ADAMANT_PLATE_BODY_T(100.0, Item(2599)),
		ADAMANT_PLATE_LEGS_T(100.0, Item(2601)),
		ADAMANT_PLATE_SKIRT_T(100.0, Item(3474)),
		ADAMANT_KITE_SHIELD_T(100.0, Item(2603)),
		ADAMANT_FULL_HELM_G(100.0, Item(2613)),
		ADAMANT_PLATE_BODY_G(100.0, Item(2607)),
		ADAMANT_PLATE_LEGS_G(100.0, Item(2609)),
		ADAMANT_PLATE_SKIRT_G(100.0, Item(3475)),
		ADAMANT_KITE_SHIELD_G(100.0, Item(2611)),
		RED_HEADBAND(80.0, Item(2645)),
		BLACK_HEADBAND(80.0, Item(2647)),
		BROWN_HEADBAND(80.0, Item(2649)),
		WHITE_HEADBAND(80.0, Item(12299)),
		BLUE_HEADBAND(80.0, Item(12301)),
		GOLD_HEADBAND(80.0, Item(12303)),
		PINK_HEADBAND(80.0, Item(12305)),
		GREEN_HEADBAND(80.0, Item(12307)),
		RED_BOATER(70.0, Item(7319)),
		ORANGE_BOATER(70.0, Item(7321)),
		GREEN_BOATER(70.0, Item(7323)),
		BLUE_BOATER(70.0, Item(7325)),
		BLACK_BOATER(70.0, Item(7327)),
		PINK_BOATER(70.0, Item(12309)),
		PURPLE_BOATER(70.0, Item(12311)),
		WHITE_BOATER(70.0, Item(12313)),
		GREEN_D_HIDE_BODY_T(70.0, Item(7372)),
		GREEN_D_HIDE_CHAPS_T(70.0, Item(7380)),
		GREEN_D_HIDE_BODY_G(70.0, Item(7370)),
		GREEN_D_HIDE_CHAPS_G(70.0, Item(7378)),
		BLACK_ELEGANT_SHIRT_(50.0, Item(10400)),
		BLACK_ELEGANT_LEGS(50.0, Item(10502)),
		WHITE_ELEGANT_BLOUSE(50.0, Item(10420)),
		WHITE_ELEGANT_SKIRT(50.0, Item(10422)),
		PURPLE_ELEGANT_SHIRT_(50.0, Item(10416)),
		PURPLE_ELEGANT_LEGS(50.0, Item(10418)),
		PURPLE_ELEGANT_BLOUSE(50.0, Item(10436)),
		PURPLE_ELEGANT_SKIRT(50.0, Item(10438)),
		PINK_ELEGANT_SHIRT_(50.0, Item(12315)),
		PINK_ELEGANT_LEGS(50.0, Item(12317)),
		PINK_ELEGANT_BLOUSE(50.0, Item(12339)),
		PINK_ELEGANT_SKIRT(50.0, Item(12341)),
		GOLD_ELEGANT_SHIRT_(50.0, Item(12347)),
		GOLD_ELEGANT_LEGS(50.0, Item(12349)),
		GOLD_ELEGANT_BLOUSE(50.0, Item(12343)),
		GOLD_ELEGANT_SKIRT(50.0, Item(12345)),
		STRENGTH_AMULET_T(50.0, Item(10364)),
		ADAMANT_CANE(50.0, Item(12377)),
		SARADOMIN_MITRE(40.0, Item(10452)),
		GUTHIX_MITRE(40.0, Item(10454)),
		ZAMORAK_MITRE(40.0, Item(10456)),
		ANCIENT_MITRE(40.0, Item(12203)),
		ARMADYL_MITRE(40.0, Item(12259)),
		BANDOS_MITRE(40.0, Item(12271)),
		SARADOMIN_CLOAK(40.0, Item(10446)),
		GUTHIX_CLOAK(40.0, Item(10448)),
		ZAMORAK_CLOAK(40.0, Item(10450)),
		ANCIENT_CLOAK(40.0, Item(12197)),
		ARMADYL_CLOAK(40.0, Item(12261)),
		BANDOS_CLOAK(40.0, Item(12273)),
		ANCIENT_STOLE(40.0, Item(12201)),
		ARMADYL_STOLE(40.0, Item(12257)),
		BANDOS_STOLE(40.0, Item(12269)),
		ANCIENT_CROZIER(40.0, Item(12199)),
		ARMADYL_CROZIER(40.0, Item(12263)),
		BANDOS_CROZIER(40.0, Item(12275)),
		CAT_MASK(30.0, Item(12361)),
		PENGUIN_MASK(30.0, Item(12428)),
		CRIER_HAT(30.0, Item(12319)),
		CRIER_COAT(30.0, Item(20240)),
		CRIER_BELL(30.0, Item(20243)),
		LEPRECHAUN_HAT(20.0, Item(12359)),
		BLACK_LEPRECHAUN_HAT(20.0, Item(20246)),
		BLACK_UNICORN_MASK(20.0, Item(20266)),
		WHITE_UNICORN_MASK(20.0, Item(20269)),
		CABBAGE_ROUND_SHIELD(15.0, Item(20272)),
		ARCEUUS_BANNER(10.0, Item(20251)),
		HOSIDIUS_BANNER(10.0, Item(20254)),
		LOVAKENGJ_BANNER(10.0, Item(20257)),
		PISCARILIUS_BANNER(10.0, Item(20260)),
		SHAYZIEN_BANNER(10.0, Item(20263)),
		WIZARD_BOOTS(10.0, Item(2579)),
		HOLY_SANDALS(5.0, Item(12598)),
		RANGER_BOOTS(1.0, Item(2577)),
	}
	
}