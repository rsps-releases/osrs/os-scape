package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/11/2015.
 */

object Jatix {
	
	val JATIX = 1174
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(JATIX) @Suspendable {
			it.chatNpc("Would you like to purchase some herblore supplies?", JATIX, 590)
			when (it.options("What do you have?", "No thanks.")) {
				1 -> {
					it.chatPlayer("What do you have?")
					it.player().world().shop(27).display(it.player())
				}
				2 -> {
					it.chatPlayer("No thanks.")
					it.chatNpc("Please do come back!", JATIX, 590)
				}
			}
		}
		r.onNpcOption2(JATIX) @Suspendable {
			it.player().world().shop(27).display(it.player())
		}
	}
	
}
