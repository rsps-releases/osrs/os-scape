package nl.bartpelle.veteres.content.treasuretrails.rewards

import nl.bartpelle.veteres.content.mechanics.ServerAnnouncements
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.content.treasuretrails.TreasureTrailRewardCaskets.clueScrollReward
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 2016-11-11.
 */

object EasyRewards {
	
	fun generateEasyReward(player: Player, source: Int) {
		val commonRewardAmount = player.world().random(1..4)
		for (i in 1..commonRewardAmount) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = CommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			
			if (player.world().realm().isPVP) {
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, player.world().random(reward.amount)), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward, player.world().random(reward.amount)), true)
			}
		}
		
		//Roll for good items..
		if (player.world().rollDie(2, 1)) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			if (player.world().realm().isPVP) {
				//Change coins to blood money
				if (reward.reward.id() == 995) {
					player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
				} else {
					player.clueScrollReward().add(Item(reward.reward), true)
				}
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
			
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from an easy clue scroll!")
		}
		
		//If we're on the PVP world, automatically give something from the uncommon table..
		if (player.world().realm().isPVP) {
			val rnd = player.world().random().nextDouble() * 100.0
			val possible = UncommonRewards.values().filter { e -> rnd <= e.probability }
			val reward = possible.toTypedArray().random()
			
			val aOrAn = if (reward.reward.definition(player.world()).name.toLowerCase()[0] in ServerAnnouncements.VOWELS) "an" else "a"
			
			//Shout our rare reward
			if (reward.probability < 50.0 && source == 2)
				player.world().filterableAnnounce("<col=6a1a18><img=50> ${player.name()} has just received a ${reward.reward.name(player.world())} from the Blood Chest!")
			else if (source == 1 && reward.probability <= 10.0)
				player.world().filterableAnnounce("<col=0052cc>News: ${player.name()} has received $aOrAn ${reward.reward.definition(player.world()).name} from an easy clue scroll!")
			
			//Change coins to blood money
			if (reward.reward.id() == 995) {
				player.clueScrollReward().add(Item(13307, reward.reward.amount()), true)
			} else {
				player.clueScrollReward().add(Item(reward.reward), true)
			}
		}
	}
	
	enum class CommonRewards(val probability: Double, val reward: Item, val amount: IntRange = 1..1) {
		BLOOD_MONEY(100.0, Item(13307), 5000..5000),
	}
	
	enum class UncommonRewards(val probability: Double, val reward: Item) {
		BRONZE_FULL_HELM_T(100.0, Item(12221)),
		BRONZE_PLATE_BODY_T(100.0, Item(12215)),
		BRONZE_PLATE_LEGS_T(100.0, Item(12217)),
		BRONZE_PLATE_SKIRT_T(100.0, Item(12219)),
		BRONZE_KITE_SHIELD_T(100.0, Item(12223)),
		BRONZE_FULL_HELM_G(100.0, Item(12211)),
		BRONZE_PLATE_BODY_G(100.0, Item(12205)),
		BRONZE_PLATE_LEGS_G(100.0, Item(12207)),
		BRONZE_PLATE_SKIRT_G(100.0, Item(12209)),
		BRONZE_KITE_SHIELD_G(100.0, Item(12213)),
		IRON_FULL_HELM_T(100.0, Item(12231)),
		IRON_PLATE_BODY_T(100.0, Item(12225)),
		IRON_PLATE_LEGS_T(100.0, Item(12227)),
		IRON_PLATE_SKIRT_T(100.0, Item(12229)),
		IRON_KITE_SHIELD_T(100.0, Item(12233)),
		IRON_FULL_HELM_G(100.0, Item(12241)),
		IRON_PLATE_BODY_G(100.0, Item(12235)),
		IRON_PLATE_LEGS_G(100.0, Item(12237)),
		IRON_PLATE_SKIRT_G(100.0, Item(12239)),
		IRON_KITE_SHIELD_G(100.0, Item(12243)),
		STEEL_FULL_HELM_T(100.0, Item(20193)),
		STEEL_PLATE_BODY_T(100.0, Item(20184)),
		STEEL_PLATE_LEGS_T(100.0, Item(20187)),
		STEEL_PLATE_SKIRT_T(100.0, Item(20190)),
		STEEL_KITE_SHIELD_T(100.0, Item(20196)),
		STEEL_FULL_HELM_G(100.0, Item(20178)),
		STEEL_PLATE_BODY_G(100.0, Item(20169)),
		STEEL_PLATE_LEGS_G(100.0, Item(20172)),
		STEEL_PLATE_SKIRT_G(100.0, Item(20175)),
		STEEL_KITE_SHIELD_G(100.0, Item(20181)),
		BLACK_FULL_HELM_T(90.0, Item(2587)),
		BLACK_PLATE_BODY_T(90.0, Item(2583)),
		BLACK_PLATE_LEGS_T(90.0, Item(2585)),
		BLACK_PLATE_SKIRT_T(90.0, Item(3472)),
		BLACK_KITE_SHIELD_T(90.0, Item(2589)),
		BLACK_FULL_HELM_G(90.0, Item(2595)),
		BLACK_PLATE_BODY_G(90.0, Item(2591)),
		BLACK_PLATE_LEGS_G(90.0, Item(2593)),
		BLACK_PLATE_SKIRT_G(90.0, Item(3473)),
		BLACK_KITE_SHIELD_G(90.0, Item(2597)),
		BLACK_BERET(90.0, Item(2635)),
		BLUE_BERET(90.0, Item(2633)),
		WHITE_BERET(90.0, Item(2637)),
		RED_BERET(90.0, Item(12247)),
		HIGHWAY_MAN_MASK(90.0, Item(2631)),
		BEANIE(90.0, Item(12245)),
		BLUE_WIZARD_ROBE_TOP_T(80.0, Item(7388)),
		BLUE_WIZARD_ROBE_BOT_T(80.0, Item(7392)),
		BLUE_WIZARD_ROBE_HAT_T(80.0, Item(7396)),
		BLUE_WIZARD_ROBE_TOP_G(80.0, Item(7390)),
		BLUE_WIZARD_ROBE_BOT_G(80.0, Item(7386)),
		BLUE_WIZARD_ROBE_HAT_G(80.0, Item(7394)),
		BLACK_HERALDIC_HELM_H_ONE(80.0, Item(10306)),
		BLACK_HERALDIC_HELM_H_TWO(80.0, Item(10308)),
		BLACK_HERALDIC_HELM_H_THREE(80.0, Item(10310)),
		BLACK_HERALDIC_HELM_H_FOUR(80.0, Item(10312)),
		BLACK_HERALDIC_HELM_H_FIVE(80.0, Item(10314)),
		BLACK_HERALDIC_SHIELD_H_ONE(80.0, Item(7332)),
		BLACK_HERALDIC_SHIELD_H_TWO(80.0, Item(7338)),
		BLACK_HERALDIC_SHIELD_H_THREE(80.0, Item(7344)),
		BLACK_HERALDIC_SHIELD_H_FOUR(80.0, Item(7350)),
		BLACK_HERALDIC_SHIELD_H_FIVE(80.0, Item(7356)),
		BLUE_ELEGANT_SHIRT_(70.0, Item(10408)),
		BLUE_ELEGANT_LEGS(70.0, Item(10410)),
		BLUE_ELEGANT_BLOUSE(70.0, Item(10428)),
		BLUE_ELEGANT_SKIRT(70.0, Item(10430)),
		GREEN_ELEGANT_SHIRT_(70.0, Item(10412)),
		GREEN_ELEGANT_LEGS(70.0, Item(10414)),
		GREEN_ELEGANT_BLOUSE(70.0, Item(10432)),
		GREEN_ELEGANT_SKIRT(70.0, Item(10434)),
		RED_ELEGANT_SHIRT_(70.0, Item(10404)),
		RED_ELEGANT_LEGS(70.0, Item(10406)),
		RED_ELEGANT_BLOUSE(70.0, Item(10424)),
		RED_ELEGANT_SKIRT(70.0, Item(10426)),
		AMULET_OF_MAGIC_T(50.0, Item(10366)),
		BLACK_CANE(50.0, Item(12375)),
		GUTHIX_ROBE_TOP(50.0, Item(10462)),
		GUTHIX_ROBE_LEGS(50.0, Item(10466)),
		SARADOMIN_ROBE_TOP(50.0, Item(10458)),
		SARADOMIN_ROBE_LEGS(50.0, Item(10464)),
		ZAMORAK_ROBE_TOP(50.0, Item(10460)),
		ZAMORAK_ROBE_LEGS(50.0, Item(10468)),
		ANCIENT_ROBE_TOP(50.0, Item(12193)),
		ANCIENT_ROBE_LEGS(50.0, Item(12195)),
		BANDOS_ROBE_TOP(50.0, Item(12265)),
		BANDOS_ROBE_LEGS(50.0, Item(12267)),
		ARMADYL_ROBE_TOP(50.0, Item(12253)),
		ARMADYL_ROBE_LEGS(50.0, Item(12255)),
		IMP_MASK(40.0, Item(12249)),
		GOBLIN_MASK(40.0, Item(12251)),
		TEAM_CAPE_ZERO(30.0, Item(20211)),
		TEAM_CAPE_X(30.0, Item(20214)),
		TEAM_CAPE_I(30.0, Item(20217)),
		WOODEN_SHIELD_G(30.0, Item(20166)),
		GOLDEN_CHEFS_HAT(20.0, Item(20205)),
		GOLDEN_APRON(20.0, Item(20208)),
		MONKS_ROBE_TOP_G(20.0, Item(20199)),
		MONKS_ROBE_BOT_G(20.0, Item(20202)),
		LARGE_SPADE(10.0, Item(20164)),
	}
	
}

