package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 6/12/2016.
 */

object RockHammer {
	
	val GARGOYLES = arrayListOf(412, 413, 1543)
	val ROCK_HAMMER = 4162
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (GARGOYLES in GARGOYLES) {
			r.onItemOnNpc(GARGOYLES) @Suspendable {
				val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
				val npc = it.targetNpc()!!
				
				if (item == ROCK_HAMMER) {
					if (npc!!.hp() < 20) {
						it.player().animate(401)
						it.player().message("You smash the Gargoyle with the rock hammer.")
						npc.hp(0, 0)
					} else {
						it.player().message("This Gargoyle has too much health to kill.")
					}
				}
			}
		}
	}
}
