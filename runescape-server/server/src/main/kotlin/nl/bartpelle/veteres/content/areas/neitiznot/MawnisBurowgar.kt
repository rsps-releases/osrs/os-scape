package nl.bartpelle.veteres.content.areas.neitiznot

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 9/21/2016.
 *
 * Bloke who sells the Helm of Neitiznot. Mainly for Iron men.
 */
object MawnisBurowgar {
	
	const val MAWNIS_INTERACT = 2979
	const val MAWNIS = 1878
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MAWNIS_INTERACT) @Suspendable {
			it.chatNpc("It makes me proud to know that the helm of my<br>ancestors will be worn in battle.", MAWNIS, 589)
			it.chatNpc("I thank you on behalf of all my kinsmen Tonkal.", MAWNIS, 567)
			it.chatPlayer("Ah yes, about that beautiful helmet.", 592)
			it.chatNpc("You mean the priceless heirloom that I gave to you as<br>a sign of my trust and gratitude?", MAWNIS, 555)
			it.chatPlayer("Err yes, that one. I may have mislaid it.", 592)
			it.chatNpc("It's a good job I have alert and loyal men who notice<br>when something like this is left lying around and picks it<br>up.", MAWNIS, 556)
			it.chatNpc("I'm afraid I'm going to have to charge you a<br>50,000GP handling cost.", MAWNIS, 589)
			
			if (it.player.inventory().remove(Item(995, 50000), false).success()) {
				it.player().inventory().addOrDrop(Item(10828), it.player)
				it.chatNpc("Please be more careful with it in future.", MAWNIS, 588)
				it.itemBox("The Burgher hands you his crown.", 10828)
			} else {
				it.chatPlayer("I don't have that much money on me.", 610)
				it.chatNpc("Well, come back when you do.", MAWNIS, 588)
			}
		}
	}
	
}