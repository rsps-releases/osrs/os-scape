package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.content.openInterface
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 12/11/2015.
 */
object ClanChat {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(7, 23) {
			val action = it.buttonAction()
			if (action == 1) {
				//Write the interface strings
				writeChannelName(it.player())
				writeChannelReqs(it.player())
				
				//Display the interface
				it.openInterface(94)
			} else if (action == 2) { // Custom xxx reset ban list!
				val ccoptional = ClanChat.current(it.player())
				ccoptional.ifPresent { cc ->
					if (cc.ownerId() == it.player().id() as Int || it.player().privilege().eligibleTo(Privilege.MODERATOR) || it.player().seniorModerator() || it.player().helper()) {
						val size = cc.tempBanListSize()
						if (size > 0) {
							cc.clearTempBannedList()
							it.message("Clan chat '%s' ban list has been cleared.", cc.name())
						} else {
							it.message("There are no temp banned users to clear.")
						}
					} else {
						it.message("You can't reset the ban list of a clan chat you don't own.")
					}
				}
				if (!ccoptional.isPresent)
					it.message("You need to own a Clan Chat in order to clear your ban list.")
			}
		}
		r.onButton(94, 10) @Suspendable {
			val act = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			when (act) {
				1 -> {
					val name = it.inputString("Enter chat name:")
					if (!name.isEmpty()) {
						it.player().message("Changes will take effect on your clan in the next 60 seconds.")
						it.player().putattrib(AttributeKey.CLAN_NAME, name)
						writeChannelName(it.player())
						ClanChat.broadcastRename(it.player(), name)
					}
				}
				2 -> {
					ClanChat.own(it.player()).ifPresent { c ->
						it.player().message("Changes will take effect on your clan in the next 60 seconds.")
						it.player().putattrib(AttributeKey.CLAN_NAME, "Chat disabled")
						writeChannelName(it.player())
						c.disable()
					}
				}
			}
		}
		
		// Who can enter chat?
		r.onButton(94, 13) @Suspendable {
			val act = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			when (act) {
				1 -> ClanChat.changeReq(it.player(), "joinreq", 0)
				2 -> ClanChat.changeReq(it.player(), "joinreq", 100)
				3 -> ClanChat.changeReq(it.player(), "joinreq", 1) // Recruit
				4 -> ClanChat.changeReq(it.player(), "joinreq", 2) // Corporal
				5 -> ClanChat.changeReq(it.player(), "joinreq", 3) // Sergeant
				6 -> ClanChat.changeReq(it.player(), "joinreq", 4) // Lieutenant
				7 -> ClanChat.changeReq(it.player(), "joinreq", 5) // Captain
				8 -> ClanChat.changeReq(it.player(), "joinreq", 6) // General
			}
		}
		
		// Who can talk on chat?
		r.onButton(94, 16) @Suspendable {
			val act = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			when (act) {
				1 -> ClanChat.changeReq(it.player(), "talkreq", 0)
				2 -> ClanChat.changeReq(it.player(), "talkreq", 100)
				3 -> ClanChat.changeReq(it.player(), "talkreq", 1) // Recruit
				4 -> ClanChat.changeReq(it.player(), "talkreq", 2) // Corporal
				5 -> ClanChat.changeReq(it.player(), "talkreq", 3) // Sergeant
				6 -> ClanChat.changeReq(it.player(), "talkreq", 4) // Lieutenant
				7 -> ClanChat.changeReq(it.player(), "talkreq", 5) // Captain
				8 -> ClanChat.changeReq(it.player(), "talkreq", 6) // General
				9 -> ClanChat.changeReq(it.player(), "talkreq", 127) // Only me
			}
		}
		
		// Who can kick on chat?
		r.onButton(94, 19) @Suspendable {
			val act = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			when (act) {
				4 -> ClanChat.changeReq(it.player(), "kickreq", 2) // Corporal
				5 -> ClanChat.changeReq(it.player(), "kickreq", 3) // Sergeant
				6 -> ClanChat.changeReq(it.player(), "kickreq", 4) // Lieutenant
				7 -> ClanChat.changeReq(it.player(), "kickreq", 5) // Captain
				8 -> ClanChat.changeReq(it.player(), "kickreq", 6) // General
				9 -> ClanChat.changeReq(it.player(), "kickreq", 127) // Only me
			}
		}
		
		//Register the temporary ban list timer when the world launcher
		r.onWorldInit {
			it.ctx<World>().timers().register(TimerKey.CLAN_CHAT_TEMP_BANNED, 20)
		}
		
		//Clear the list when the timer ends
		r.onWorldTimer(TimerKey.CLAN_CHAT_TEMP_BANNED) {
			val world: World = it.ctx()
			
			world.players().forEachKt({ p ->
				ClanChat.own(p).ifPresent { c ->
					if (c.tempBanListSize() > 0) {
						c.clearTempBannedList()
						if ((p.id() as Int) == c.ownerId()) {
							p.message("Your clan chat's ban list has been routinely cleared. It will clear again in two hours.")
						}
					}
				}
			})
			
			world.timers().register(TimerKey.CLAN_CHAT_TEMP_BANNED, 12000)
		}
		
	}
	
	//Write the Clan Chat setup channel name string
	@Suspendable fun writeChannelName(player: Player) {
		val clanName = player.attribOr<String>(AttributeKey.CLAN_NAME, "")
		
		if (clanName != "")
			player.write(InterfaceText(94, 10, clanName.capitalize()))
		else
			player.write(InterfaceText(94, 10, "Chat disabled"))
	}
	
	//Write the Clan Chat setup channel requirement strings
	@JvmStatic @Suspendable fun writeChannelReqs(player: Player) {
		val joinRequirement = player.attribOr<Int>(AttributeKey.CLAN_ENTER, 0)
		val talkRequirement = player.attribOr<Int>(AttributeKey.CLAN_TALK, 0)
		val kickRequirement = player.attribOr<Int>(AttributeKey.CLAN_KICK, 0)
		
		player.write(InterfaceText(94, 13, getNameForRequirement(joinRequirement)))
		player.write(InterfaceText(94, 16, getNameForRequirement(talkRequirement)))
		player.write(InterfaceText(94, 19, getNameForRequirement(kickRequirement)))
	}
	
	fun getNameForRequirement(required: Int): String {
		when (required) {
			0 -> return "Anyone"
			100 -> return "Any friends"
			1 -> return "Recruit+"
			2 -> return "Corporal+"
			3 -> return "Sergeant+"
			4 -> return "Lieutenant+"
			5 -> return "Captain+"
			6 -> return "General+"
			else -> return "Only me"
		}
	}
}