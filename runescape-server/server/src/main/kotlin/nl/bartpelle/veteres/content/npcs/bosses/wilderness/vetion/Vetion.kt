package nl.bartpelle.veteres.content.npcs.bosses.wilderness.vetion

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 9/3/2017.
 */
object Vetion {
	
	val MELEE_ANIM = 5499
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onNpcSpawn(6611, @Suspendable {
			it.npc().putattrib(AttributeKey.VETION_REBORN_ACTIVE, false)
		})
	}
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (npc.attribOr<Boolean>(AttributeKey.VETION_HELLHOUND_SPAWNED, false) == false && EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 6) && EntityCombat.attackTimerReady(npc)) {
				
				if (npc.hp() <= 125 && !npc.hasAttrib(AttributeKey.VETION_HELLHOUND_SPAWNED)) {
					spawnHellhounds(npc, target)
				}
				
				//If the 5 minute timer has expired we revert vetion back to his original form.
				if (!npc.timers().has(TimerKey.VETION_REBORN_TIMER) && npc.sync().transmog() == 6612) {
					npc.sync().transmog(6611)
				}
				
				when (npc.world().random(2)) {
					1 -> {
						doMagic(npc, target)
					}
					else -> {
						if (EntityCombat.canAttackMelee(npc, target)) {
							doMelee(npc, target)
						} else {
							doMagic(npc, target)
						}
					}
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@JvmField val death: Function1<Script, Unit> = s@ @Suspendable {
	
		val vetion = it.npc()
		
		if (vetion.sync().transmog() == 6612) {
			vetion.putattrib(AttributeKey.VETION_REBORN_ACTIVE, false)
		}
		
		vetion.clearattrib(AttributeKey.VETION_HELLHOUND_SPAWNED)
		
		if (vetion.sync().transmog() != 6611) {
			vetion.sync().transmog(6611)
		}
	}
	
	@JvmField val hit: Function1<Script, Unit> = s@ @Suspendable {
		
		val vetion = it.npc()
		val target = EntityCombat.getTarget(vetion)
		
		if ((vetion.hp() == 0 || vetion.dead()) && vetion.attribOr<Boolean>(AttributeKey.VETION_REBORN_ACTIVE, false) == false) {
			spawnVetionReborn(vetion)
			return@s
		}
		
		if (target is Player && vetion.attribOr<Boolean>(AttributeKey.VETION_HELLHOUND_SPAWNED, false) == true) {
			target.message("Vet'ion is immune until the hellhound spawns are killed off.")
		}
	}
	
	fun doMagic(vetion: Npc, target: Entity) {
		
		val toTile = target.tile()
		val tileDist = vetion.tile().distance(target.tile())
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		
		vetion.animate(vetion.attackAnimation())
		
		vetion.world().server().scriptExecutor().executeScript(vetion, @Suspendable {
			
			it.delay(2)
			
			vetion.world().spawnProjectile(vetion.tile() + Tile(1, 1), toTile, 280, 70, 45, delay, 10 * tileDist, 0, 1)
			vetion.world().spawnProjectile(vetion.tile() + Tile(1, 1), toTile.transform(1, 0), 280, 70, 45, delay, 10 * tileDist, 0, 1)
			vetion.world().spawnProjectile(vetion.tile() + Tile(1, 1), toTile.transform(1, 1), 280, 70, 45, delay, 10 * tileDist, 0, 1)
			
			vetion.world().tileGraphic(281, toTile, 0, 10 * tileDist)
			vetion.world().tileGraphic(281, toTile.transform(1, 0), 0, 10 * tileDist)
			vetion.world().tileGraphic(281, toTile.transform(1, 1), 0, 10 * tileDist)
			
			it.delay(1)
			
			if (target.tile() == (toTile) || target.tile() == (toTile.transform(1, 0)) || target.tile() == (toTile.transform(1, 1))) {
				if (EntityCombat.attemptHit(vetion, target, CombatStyle.MAGIC)) {
					target.hit(vetion, target.world().random(vetion.combatInfo().maxhit), delay).combatStyle(CombatStyle.MAGIC)
				} else {
					target.hit(vetion, 0, delay).combatStyle(CombatStyle.MAGIC)
				}
			}
		})
		
		target.putattrib(AttributeKey.LAST_DAMAGER, vetion)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		vetion.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(vetion, vetion.combatInfo().attackspeed)
	}
	
	fun doMelee(vetion: Npc, target: Entity) {
		
		vetion.animate(MELEE_ANIM)
		
		if (EntityCombat.attemptHit(vetion, target, CombatStyle.MELEE)) {
			target.hit(vetion, target.world().random(vetion.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(vetion, 0, 1).combatStyle(CombatStyle.MELEE)
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, vetion)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		vetion.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(vetion, vetion.combatInfo().attackspeed)
	}
	
	fun spawnVetionReborn(vetion: Npc) {
		
		vetion.heal(vetion.combatInfo().stats.hitpoints)
		vetion.sync().transmog(6612)
		vetion.sync().shout("Do it again!!")
		
		vetion.timers().register(TimerKey.VETION_REBORN_TIMER, 500)
		vetion.putattrib(AttributeKey.VETION_REBORN_ACTIVE, true)
	}
	
	fun spawnHellhounds(vetion: Npc, target: Entity) {
	
		vetion.sync().shout("Kill my pets!")
		val minions = mutableListOf<Npc>()
		for (i in 0..1) {
			val minion = VetionMinion(vetion, target, i)
			vetion.executeScript(minion.create)
			minions.add(minion.minion)
		}
		
		vetion.putattrib(AttributeKey.VETION_HELLHOUND_SPAWNED, true)
		vetion.putattrib(AttributeKey.MINION_LIST, minions)
	}
}
