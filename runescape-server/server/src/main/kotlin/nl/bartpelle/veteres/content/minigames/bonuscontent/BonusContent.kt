package nl.bartpelle.veteres.content.minigames.bonuscontent

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Bart on 12/15/2016.
 */
object BonusContent {
	
	@JvmField var activeBlessing = BlessingGroup.NONE
	
	@JvmStatic fun isActive(player: Player, group: BlessingGroup): Boolean = activeBlessing == group
	
	fun cycleBlessing(world: World) {
		val list = BlessingGroup.values().filterNot { it == BlessingGroup.NONE || it == activeBlessing }.toMutableList()
		Collections.shuffle(list)
		activeBlessing = list.first()
		
		val message = "The active blessing has changed to ${activeBlessing.title}. See the quest tab!"
		world.players().forEachKt({ it.write(AddMessage(message, AddMessage.Type.BROADCAST)) })
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit {
			val world = it.ctx<World>()
			
			// Currently, blessings are only enabled on W1/W3.
			if (world.realm().isOSRune || world.realm().isRealism) {
				world.timers().register(TimerKey.NEXT_BLESSING_CYCLE, 6000)
				
				cycleBlessing(world)
			}
		}
		
		r.onWorldTimer(TimerKey.NEXT_BLESSING_CYCLE) {
			it.ctx<World>().timers().register(TimerKey.NEXT_BLESSING_CYCLE, 6000)
			cycleBlessing(it.ctx<World>())
		}
	}
	
}