package nl.bartpelle.veteres.content.npcs.godwars.saradomin

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.model.Entity

/**
 * Created by Situations on 7/4/2016.
 */

object AggressionCheck {
	
	val SARADOMIN_PROTECTION_EQUIPMENT = arrayListOf(542, 544, 1718, 2412, 2415,
			2661, 2663, 2665, 2667, 3479, 3840, 4037, 6762, 8055, 8058, 10384, 10386,
			10388, 10390, 10440, 10446, 10452, 10458, 10464, 10470, 10778, 10784, 10792,
			11806, 11838, 11891, 12598, 12637, 12809, 13331, 13332, 19933)
	
	@JvmField val script: Function1<Entity, Boolean> = s@ @Suspendable { entity -> determine_aggression(entity) }
	
	@Suspendable fun determine_aggression(entity: Entity): Boolean {
		SARADOMIN_PROTECTION_EQUIPMENT.forEach { armour ->
			if (entity.equipment().has(armour)) {
				return false
			}
		}
		return true
	}
}
