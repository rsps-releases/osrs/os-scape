package nl.bartpelle.veteres.content.treasuretrails

import nl.bartpelle.veteres.model.Tile

/**
 * Created by Situations on 2016-11-04.
 */

object TreasureTrailMapClues {
	
	enum class MapsWithCrate(val interfaceID: Int, val crate: Int, val tile: Tile, val level: TreasureTrailClueLevel) { //interface 86 & 314 338
		MCGRUBORS_WOODS(355, 357, Tile(2658, 3488), TreasureTrailClueLevel.MEDIUM),
		CLOCK_TOWER(361, 354, Tile(2565, 3248), TreasureTrailClueLevel.MEDIUM),
		DARK_WARRIORS_FORTRESS(359, 354, Tile(3026, 3628), TreasureTrailClueLevel.HARD),
		OBSERVATORY(358, 18506, Tile(2457, 3182), TreasureTrailClueLevel.HARD),
		//LUMBERYARD(),
		//APE_ATOLL())
	}
	
	
	enum class MapsWithX(val interfaceID: Int, val crate: Int, val tile: Tile, val level: TreasureTrailClueLevel) {
		
		//Maps with an X and buildings
		
		//Maps with an X but no buildings
		
		//Maps with an X and a fishing spot
		
	}
	
}