package nl.bartpelle.veteres.content.npcs

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.areas.edgeville.dialogue.NewWizardPVP
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands

/**
 * Created by Jak on 16/10/2016.
 */
object SpiritTree {
	
	val GE_TREE = 26263
	
	enum class SpiritTrees(val tile: Tile, val destinations: Array<NewWizardPVP.Teleport>) {
		STRONGHOLD(Tile(2460, 3445), buildList(1)),
		GRAND_EXCHANGE(Tile(3184, 3509), buildList(2)),
		VILLAGE(Tile(2543, 3168), buildList(3)),
		BATTLEFIELD(Tile(2555, 3260), buildList(4));
	}
	
	fun buildList(order: Int): Array<NewWizardPVP.Teleport> {
		val village = NewWizardPVP.Teleport("Tree Gnome Village", 2542, 3170, 0)
		val GE = NewWizardPVP.Teleport("Grand Exchange", 3183, 3509, 0)
		val battlefield = NewWizardPVP.Teleport("Battlefield of Khazard", 2555, 3259, 0)
		val stronghold = NewWizardPVP.Teleport("Gnome Stronghold", 2461, 3444, 0)
		val cancel = NewWizardPVP.Teleport("Cancel")
		when (order) {
			1 -> return arrayOf(village, GE, battlefield, cancel)
			2 -> return arrayOf(stronghold, village, battlefield, cancel)
			3 -> return arrayOf(stronghold, GE, battlefield, cancel)
			4 -> return arrayOf(village, GE, stronghold, cancel)
			
		}
		return kotlin.arrayOf()
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (objId in 1293..1295) {
			repo.onObject(objId) @Suspendable {
				val obj = it.interactionObject()
				for (tree in SpiritTrees.values()) {
					if (tree.tile == obj.tile()) {
						val option = it.interactionOption()
						if (option == 2) {
							locations(it, tree)
						} else {
							if (tree == SpiritTrees.GRAND_EXCHANGE || tree == SpiritTrees.BATTLEFIELD) {
								it.chatNpc("Hello gnome friend. Where would you like to go?", 4981)
							} else {
								it.chatNpc("You friend of gnome people, you friend of mine.<br><br>Would you like me to take you somewhere?", 4982)
							}
							locations(it, tree)
						}
					}
				}
			}
		}
	}
	
	@Suspendable private fun locations(it: Script, tree: SpiritTrees) {
		val choice = OptionList.display(it, true, "Spirit Tree Locations", *(tree.destinations.map { it.text }.toTypedArray()))
		if (choice != -1) {
			val destination = tree.destinations[choice]
			if (destination.x > 0) {
				it.player().executeScript { s ->
					teleport(s, destination.x, destination.z, destination.height)
				}
				it.player().executeScript { s ->
					s.itemBox("You place your hands on the dry tough bark of the<br>spirit tree, and feel a surge of energy run through<br>your veins.", 6063, 400)
				}
				// Hide continue and make unclickable
				it.player().write(InterfaceText(193, 2, ""))
				it.player().write(InterfaceSettings(193, 2, -1, -1, 0))
				it.delay(3)
				it.player().write(InterfaceText(193, 2, "Click here to continue"))
				it.player().write(InterfaceSettings(193, 2, -1, -1, 1))
			}
		}
	}
	
	@Suspendable fun teleport(it: Script, x: Int, z: Int, level: Int) {
		it.player.interfaces().closeMain()
		if (GameCommands.pkTeleportOk(it.player(), x, z)) {
			DeadmanMechanics.attemptTeleport(it)
			it.player().lockNoDamage()
			it.player().animate(828, 15)
			it.delay(2)
			it.player().teleport(x, z, 0)
			it.animate(-1)
			it.player().unlock()
		}
	}
}