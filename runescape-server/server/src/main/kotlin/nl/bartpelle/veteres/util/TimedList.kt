package nl.bartpelle.veteres.util

import com.google.gson.annotations.Expose
import java.util.*

class TimedList {

    @Expose private var list: MutableList<Entry>? = null

    fun add(key: String, ms: Long) {
        if (list == null)
            list = ArrayList()
        val e = Entry()
        e.key = key
        e.ms = ms
        list!!.add(e)
    }

    fun contains(key: String, ms: Long, timeoutMinutes: Long): Boolean {
        if (list == null)
            return false
        val timeoutMs = getMinutesToMillis(timeoutMinutes)
        var logged = false
        val it = list!!.iterator()
        while (it.hasNext()) {
            val log = it.next()
            if (ms - log.ms >= timeoutMs)
                it.remove()
            else if (log.key == key)
                logged = true
        }
        if (list!!.isEmpty())
            list = null
        return logged
    }

    private class Entry {
        @Expose var key: String = ""
        @Expose var ms: Long = 0
    }

    fun getMinutesToMillis(minutes: Long): Long {
        return minutes * 60000L
    }

}