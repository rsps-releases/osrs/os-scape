package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/30/2015.
 */

object EstateAgent {
	
	val ESTATE_AGENT: Int = 5419
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ESTATE_AGENT) @Suspendable {
			it.chatNpc("Sorry, we're working on getting this skill done!", ESTATE_AGENT, 567)
			it.chatPlayer("Okay, I'll come back later!")
		}
	}
	
}
