package nl.bartpelle.veteres.content.areas.karamja.objects

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/11/2016.
 */

object ClimbingRope {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(18969) @Suspendable {
			it.player().lock()
			it.message("You climb up the hanging rope...")
			it.delay(2)
			it.player().teleport(Tile(2856, 3167))
			it.message("You appear on the volcano rim.")
			it.player().unlock()
		}
	}
}

