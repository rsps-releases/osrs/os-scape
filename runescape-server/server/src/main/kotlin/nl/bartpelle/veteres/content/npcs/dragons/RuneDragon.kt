package nl.bartpelle.veteres.content.npcs.dragons

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.items.equipment.DragonfireShield
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Mack
 */
object RuneDragon {

    @JvmField
    val script: Function1<Script, Unit> = script@ @Suspendable {
        val dragon = it.npc()
        var target = EntityCombat.getTarget(dragon) ?: return@script

        while (EntityCombat.targetOk(dragon, target) && dragon.hp() > 0 && PlayerCombat.canAttack(dragon, target)) {
            if (EntityCombat.canAttackDistant(dragon, target, true, 6) && EntityCombat.attackTimerReady(dragon)) {
                val tileDist = dragon.tile().distance(target.tile())
                val delay = Math.max(1, (20 + (tileDist) * 12) / 30)
                val rand = dragon.world().random(4)

                when (rand) {
                    1 -> doDragonBreath(it, target, tileDist, delay)
                    2 -> doMagicBlast(it, target, tileDist, delay)
                    3 -> doRangeAttack(it, target, tileDist, delay)
                    else -> {
                        if (EntityCombat.canAttackMelee(dragon, target, false)) {
                            doMelee(dragon, target)
                        } else {
                            when (dragon.world().random(3)) {
                                1 -> doDragonBreath(it, target, tileDist, delay)
                                2 -> doMagicBlast(it, target, tileDist, delay)
                                3 -> doRangeAttack(it, target, tileDist, delay)
                            }
                        }
                    }
                }
                EntityCombat.cooldownForNpc(dragon, target)
            }

            EntityCombat.postHitLogic(dragon)
            it.delay(1)
            target = EntityCombat.refreshTarget(it) ?: break
        }
    }

    @Suspendable private fun doMagicBlast(it: Script, target: Entity, tileDist: Int, delay: Int) {
        val npc = it.npc()

        npc.animate(81)
        it.delay(1)

        npc.world().spawnProjectile(npc.tile().transform(1, 1), target.tile(), 1459, 40, 36, 10 * tileDist, 46, 20, 0)

        if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
            target.sync().graphic(163, 80, 20 * tileDist)
            target.hit(npc, npc.world().random(26), delay).combatStyle(CombatStyle.MAGIC)
        } else {
            target.hit(npc, 0, delay).combatStyle(CombatStyle.MAGIC)
        }
    }

    @Suspendable private fun doRangeAttack(it: Script, target: Entity, tileDist: Int, delay: Int) {
        val npc = it.npc()

        npc.animate(81)
        it.delay(1)

        npc.world().spawnProjectile(npc.tile().transform(1, 1), target.tile(), 16, 20, 20, 41, 40, 18, 255)

        if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
            target.hit(npc, npc.world().random(18), delay).combatStyle(CombatStyle.RANGE)
        } else {
            target.hit(npc, 0, delay).combatStyle(CombatStyle.RANGE)
        }
    }

    @Suspendable private fun doElectricSpell(it: Script, target: Entity, tileDist: Int, delay: Int) {
        val npc = it.npc()

        npc.animate(81)
        it.delay(1)

        npc.world().spawnProjectile(npc.tile().transform(1, 1), target.tile(), 1488, 40, 36, 10 * tileDist, 46, 20, 0)

        npc.world().server().scriptExecutor().executeScript(npc, @Suspendable {
            var ticks = 8
            while (ticks-- > 0) {
                npc.world().spawnProjectile(target.pathQueue().lastStep(), target.tile(), 1488, 40, 36, 10 * tileDist, 46, 20, 0)
            }
        })
    }

    @Suspendable private fun doDragonBreath(it: Script, target: Entity, tileDist: Int, delay: Int) {
        val npc = it.npc()
        var max = 50.0

        npc.animate(81)
        it.delay(1)

        npc.world().spawnProjectile(npc.tile().transform(1, 1), target.tile(), 54, 40, 36, 10 * tileDist, 46, 20, 0)

        if (target is Player) {
            if (DragonfireShield.getTyped(target) != null || target.equipment().hasAt(EquipSlot.SHIELD, 1540)) {
                max *= 0.30
                target.message("Your shield absorbs most of the dragon fire!")
            }
            if (target.attribOr<Int>(AttributeKey.ANTIFIRE_POTION, 0) > 0) {
                max *= 0.30
                target.message("Your potion protects you from the heat of the dragon's breath!")
            }
            if (target.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1) {
                max *= 0.60
                target.message("Your prayer absorbs most of the dragon's breath!")
            }
        }

        if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
            target.hit(npc, target.world().random(50), delay).combatStyle(CombatStyle.MAGIC)
        } else {
            target.hit(npc, 0, delay).combatStyle(CombatStyle.MAGIC)
        }

        if (target is Player && DragonfireShield.canCharge(target)) {
            DragonfireShield.charge(target)
        }
    }

    @Suspendable private fun doMelee(npc: Npc, target: Entity) {
        npc.animate(npc.attackAnimation())

        if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
            target.hit(npc, target.world().random(npc.combatInfo().maxhit), 0).combatStyle(CombatStyle.MELEE)
        } else {
            target.hit(npc, 0, 0).combatStyle(CombatStyle.MELEE)
        }
    }
}