package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.CombatStyle
import java.lang.ref.WeakReference

/**
 * Created by Bart on 10/18/2015.
 */
object RottenPotato {
	
	const val ROTTEN_POTATO = 5733
	
	@ScriptMain @JvmStatic fun registerRottenPotato(r: ScriptRepository) {
		
		r.onItemOnPlayer(ROTTEN_POTATO, s@ @Suspendable {
			// Give you the name and distance to a target
			if (it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				val target: Entity = it.target() ?: return@s
				if (target.isPlayer) {
					it.player().message("Distance to %s (%d) : %d. ", (target as Player).name(), target.id(), it.player().tile().distance(target.tile()))
				} else {
					it.player().message("Distance to %s (%d) : %d. ", (target as Npc).def().name, target.id(), it.player().tile().distance(target.tile()))
				}
			}
		})
		r.onItemOption1(ROTTEN_POTATO) @Suspendable {
			if (it.player().privilege() == Privilege.ADMIN) {
				when (it.optionsTitled("Op1", "Set all stats.", "Wipe inventory.", "Setup POH", "Teleport to player", "Spawn aggressive NPC.")) {
					1 -> {
						val lvl = Math.max(1, Math.min(99, it.inputInteger("Set to what level?")))
						for (i in 0..Skills.SKILL_COUNT - 1) {
							it.player().skills().setXp(i, Skills.levelToXp(lvl).toDouble())
						}
					}
					2 -> {
						for (i in 0..27) {
							if (it.player().inventory()[i]?.id() != ROTTEN_POTATO) {
								it.player().inventory()[i] = null
							}
						}
					}
					4 -> {
					}
				}
			}
		}
		r.onItemOption3(ROTTEN_POTATO) @Suspendable {
			if (it.player().privilege() == Privilege.ADMIN) {
				when (it.optionsTitled("Op3", "Bank menu", "AMEs for all!", "Teleport to RARE!", "Spawn RARE!")) {
					1 -> {
						when (it.optionsTitled("Op3", "Open bank.", "Set PIN to 2468.", "Wipe bank.")) {
							1 -> Bank.open(it.player(), it)
							3 -> it.player().bank().empty()
						}
					}
				}
			}
		}
		r.onItemOption4(ROTTEN_POTATO) @Suspendable {
			if (it.player().privilege() == Privilege.ADMIN) {
				when (it.optionsTitled("Op4", "Keep me logged in.", "Kick me out.", "Kill me.", "Transmogrify me...", "Custom...")) {
					2 -> it.player().logout()
					3 -> it.player().hit(it.player(), it.player().hp()).combatStyle(CombatStyle.GENERIC)
					5 -> {
						when (it.options("Hide tutorial overlay box", "Show tutorial overlay box")) {
							1 -> it.player().interfaces().close(162, 550)
							2 -> it.player().interfaces().send(372, 162, 550, true)
						}
					}
				}
			}
		}
		
		
	}
	
	@JvmField val potatoOnNpc: Function1<Script, Unit> = @Suspendable {
		val npc = it.player().attrib<WeakReference<Npc>>(AttributeKey.TARGET).get()
		it.player().message("Npc index ${npc!!.index()} type ${npc!!.id()} name ${npc!!.def().name}")
		when (it.optionsTitled("Options for NPC ${npc!!.id()}", "Kill NPC.", "Despawn NPC.", "Teleport to me.", "Transmog.", "Replace.")) {
			1 -> {
				if (npc.id() == 6611)
					npc.clearattrib(AttributeKey.VETION_HELLHOUND_SPAWNED)
				npc.hit(it.player, npc.hp())
			}
			2 -> npc.world().unregisterNpc(npc)
			3 -> npc.teleport(it.player.tile())
			4 -> {
				val id = it.inputInteger("Enter NPC ID (or 0 to reset)")
				npc.sync().transmog(if (id == 0) -1 else id)
			}
			5 -> {
				val id = it.inputInteger("Enter NPC ID (or 0 to cancel)")
				if (id > 0) {
					npc.world().unregisterNpc(npc)
					val newNpc = Npc(id, npc.world(), npc.tile())
					npc.world().registerNpc(newNpc)
				}
			}
		}
	}
	
	@JvmStatic @Suspendable fun used_on_object(it: Script) {
		val obj = it.interactionObject()
		val name = obj.definition(it.player().world()).name
		when (it.options("Delete obj '$name'", "Obj on tile count", "Clear object's attributes", "Nvm nigga xx")) {
			1 -> {
				val removeClip = it.optionsTitled("Remove clipping of $name too?", "Yes", "No") == 1
				it.player().world().removeObj(obj, removeClip)
				it.player().message("removed object $name at ${obj.tile().toStringSimple()} and declipped: $removeClip")
			}
			2 -> {
				val type = it.inputInteger("Object type? (0=wall, 4=wall dec, 10=normal)")
				val objsOnTile = it.player().world().allObjsByType(type, obj.tile())
				it.message("Count: %d", objsOnTile.size)
				when (it.options("Print info?", "nvm")) {
					1 -> {
						objsOnTile.forEach { o -> it.message("%s (%d) type:%d rot:%d at %s", o.definition(it.player().world()).name, o.id(), o.type(), o.rot(), o.tile().toStringSimple()) }
					}
				}
			}
			3 -> {
				obj.attribs()?.clear()
				it.messagebox(obj.definition(it.player().world()).name + " - attributes cleared.")
			}
		}
	}
	
}