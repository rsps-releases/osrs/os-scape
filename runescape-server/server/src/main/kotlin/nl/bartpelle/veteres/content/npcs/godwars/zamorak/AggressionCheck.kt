package nl.bartpelle.veteres.content.npcs.godwars.zamorak

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.model.Entity

/**
 * Created by Situations on 7/4/2016.
 */

object AggressionCheck {
	
	val ZAMORAK_PROTECTION_EQUIPMENT = arrayListOf(1033, 1035, 2414, 2417, 2653, 2655,
			2657, 2659, 3478, 4039, 6764, 8056, 8059, 10368, 10370, 10372, 10374,
			10444, 10450, 10456, 10460, 10468, 10474, 10776, 10786, 10790, 11808,
			11824, 11889, 11892, 12638, 13333, 13334, 19936)
	
	@JvmField val script: Function1<Entity, Boolean> = s@ @Suspendable { entity -> determine_aggression(entity) }
	
	@Suspendable fun determine_aggression(entity: Entity): Boolean {
		ZAMORAK_PROTECTION_EQUIPMENT.forEach { armour ->
			if (entity.equipment().has(armour)) {
				return false
			}
		}
		return true
	}
}
