package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.sigmund.Sigmund
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.sigmund.SigmundExchangeConstants
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.items.equipment.ZulrahItems
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterEmblem
import nl.bartpelle.veteres.content.waitForInterfaceClose
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.*
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.services.logging.LoggingService
import nl.bartpelle.veteres.util.Varbit
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.lang.ref.WeakReference
import java.text.NumberFormat

/**
 * Created by Bart on 11/20/2015.
 */

object Trading {
	
	@JvmStatic val logger: Logger = LogManager.getLogger(Trading::class.java)
	
	val SCREEN_ONE = 335
	val SCREEN_TWO = 334
	val INVENTORY_PANE = 336
	
	fun Player.tradingOffer(): ItemContainer {
		val offer = attribOr<ItemContainer>(AttributeKey.TRADING_OFFER, null)
		if (offer != null)
			return offer
		
		val ctr = ItemContainer(world(), 28, ItemContainer.Type.REGULAR)
		putattrib(AttributeKey.TRADING_OFFER, ctr)
		return ctr
	}
	
	fun Player.tradingPartner(): Player? {
		return world().playerForId(attribOr<Int>(AttributeKey.TRADING_PARTNER, -1)).orElse(null)
	}
	
	/**
	 * Aborts a trade, giving the other player a message that the trade has cancelled by this user.
	 */
	@JvmStatic fun Player.abortTrading() {
		if (SigmundExchangeConstants.hasActiveSession(this)) {
			
			stopTrading()
			interfaces().closeById(INVENTORY_PANE) // Interface component
			interfaces().closeMain() // Main trading window
			stopActions(false) //Close any input dialogues
			clearattrib(AttributeKey.SIGMUND_EXCHANGE_SESSION) //clear the session attrib
		} else {
			val partner = tradingPartner() ?: return
			val partnersPartner = partner.tradingPartner()
			
			// Close and refund
			partner.stopTrading()
			stopTrading()
			
			if (partner.interfaces().visible(INVENTORY_PANE)) {
				// Avoid double messages.
				partner.message("Other player declined trade.")
				message("Trade declined.")
			}
			
			clearattrib(AttributeKey.TRADING_PARTNER)
			partner.clearattrib(AttributeKey.TRADING_PARTNER)
			
			interfaces().closeById(INVENTORY_PANE) // Interface component
			partner.interfaces().closeById(INVENTORY_PANE) // Interface component
			
			interfaces().closeMain() // Main trading window
			partner.interfaces().closeMain() // Main trading window
			
			stopActions(false) //Close any input dialogues
			partner.stopActions(false) //Close any input dialogues
			
			// Extra extra security, should never happen after previous patch, but what the hell.
			if (partnersPartner != null) {
				if (partnersPartner != this) {
					logger.error("TRADING #1 : partner-mismatch : [{}, {}, {}]", this.name(), partner.name(), partnersPartner.name())
				}
				partnersPartner.interfaces().closeMain()
			}
		}
	}
	
	/**
	 * Stops a trade. Shows no messages, and is not mutual. Only does logic for this player. Closes everything aswell.
	 */
	fun Player.stopTrading() {
		
		refundOffer()
		
		if (SigmundExchangeConstants.hasActiveSession(this)) {
			SigmundExchangeConstants.getSession(this).playersOffer().empty()
			SigmundExchangeConstants.getSession(this).sigmundsOffer().empty() //Clear sigmund's offers.
		} else {
			tradingOffer().empty()
		}
		
		dupeWatch().exclude()
	}
	
	/**
	 * Clears trade acceptance for both players.
	 */
	fun Player.clearAcceptance(refresh: Boolean = true) {
		if (SigmundExchangeConstants.hasActiveSession(this)) {
			write(SetItems(90, SigmundExchangeConstants.getSession(this).playersOffer()))
			write(SetItems(90, -2, 0, SigmundExchangeConstants.getSession(this).sigmundsOffer()))
			
			
		} else {
			putattrib(AttributeKey.TRADE_ACCEPTED, false)
			
			val partner = tradingPartner() ?: return
			partner.putattrib(AttributeKey.TRADE_ACCEPTED, false)
			
			updateTradingOffer(refresh)
			partner.updateTradingOffer(refresh)
		}
	}
	
	/**
	 * Refund all the offered items back to the player.
	 */
	fun Player.refundOffer() {
		if (SigmundExchangeConstants.hasActiveSession(this)) {
			for (item in SigmundExchangeConstants.getSession(this).playersOffer().items()) {
				if (item != null)
					inventory().add(item, true)
			}
		} else {
			for (item in tradingOffer()) {
				if (item != null)
					inventory().add(item, true) // I dare to say this cannot fail. Bite me.
			}
		}
	}
	
	fun invited(inviter: Player, invited: Player): Boolean {
		if (inviter.timers().has(TimerKey.TRADE_INVITATION_OPEN)) {
			val partnerId: Int = inviter.attribOr(AttributeKey.REQUESTED_TRADE_PARTNER, -1)
			return partnerId == invited.id()
		}
		
		return false
	}
	
	fun initiateTrade(player: Player, partner: Player) {
		if (player.locked() || player.hp() < 1 || partner.locked() || partner.hp() < 1 || player.dead() || partner.dead())
			return
		/*player.timers().extendOrRegister(TimerKey.PACKETLOGGING, 50) // Log packets for 30s
		player.logged(true)
		partner.timers().extendOrRegister(TimerKey.PACKETLOGGING, 50) // Log packets for 30s
		partner.logged(true)*/
		
		player.timers().cancel(TimerKey.TRADE_INVITATION_OPEN) // Avoid reopening a trade from now on.
		player.putattrib(AttributeKey.TRADING_PARTNER, partner.id())
		
		// Reset states
		player.varps().varbit(Varbit.TRADE_MODIFIED_LEFT, 0)
		player.varps().varbit(Varbit.TRADE_MODIFIED_RIGHT, 0)
		player.putattrib(AttributeKey.TRADE_ACCEPTED, false)
		partner.putattrib(AttributeKey.TRADE_ACCEPTED, false)
		
		player.interfaces().sendMain(SCREEN_ONE)
		player.interfaces().sendInventory(INVENTORY_PANE)
		player.write(InterfaceText(SCREEN_ONE, 31, "Trading with: ${partner.name()}"))
		player.write(InterfaceText(SCREEN_ONE, 24, "Your offer: <br>(Value: <col=ffffff>0</col> coins)"))
		player.write(InterfaceText(SCREEN_ONE, 27, "${partner.name()} offers:<br>(Value: <col=ffffff>0</col> coins)"))
		
		// Clear the item containers
		if (player.tradingOffer().occupiedSlots() > 0) {
			logger.error("TRADING #3: {} still had {} items in trading inv on initiate at {}", player.name(), player.tradingOffer().occupiedSlots(), player.tile().toStringSimple())
		}
		if (partner.tradingOffer().occupiedSlots() > 0) {
			logger.error("TRADING #3: {} still had {} items in trading inv on initiate at {}", partner.name(), partner.tradingOffer().occupiedSlots(), partner.tile().toStringSimple())
		}
		player.stopTrading()
		partner.stopTrading()
		
		player.write(SetItems(90, player.tradingOffer()))
		player.write(SetItems(90, -2, 0, partner.tradingOffer()))
		player.invokeScript(149, (INVENTORY_PANE shl 16) or 0, 93, 4, 7, 0, -1, "Offer<col=ff9040>", "Offer-5<col=ff9040>", "Offer-10<col=ff9040>", "Offer-All<col=ff9040>", "Offer-X<col=ff9040>")
		
		player.write(InterfaceSettings(INVENTORY_PANE, 0, 0, 28, 1086))
		
		player.write(InterfaceSettings(SCREEN_ONE, 28, 0, 27, 1024))
		player.write(InterfaceSettings(SCREEN_ONE, 25, 0, 27, 1086))
		
		player.updateTradingOffer()
		
		// Start the watchdog
		player.executeScript(watchdog)
	}
	
	fun fits(player: Player, offer: ItemContainer): Boolean {
		val cpy = player.inventory().copy()
		for (item in offer) {
			if (item != null) {
				if (player.inventory().add(item, false).failed()) {
					player.inventory().restore(cpy)
					player.inventory().clean()
					return false
				}
			}
		}
		player.inventory().restore(cpy)
		player.inventory().clean()
		return true
	}
	
	fun proceedToSecond(player: Player, partner: Player) {
		// Security
		if (!player.interfaces().visible(INVENTORY_PANE) || !partner.interfaces().visible(INVENTORY_PANE)) {
			return
		}
		
		// Check if all would fit.
		if (!fits(player, partner.tradingOffer())) {
			// Screw wasting time (closing like rs)... just tell em straight!
			/*player.interfaces().closeById(INVENTORY_PANE) // Interface component
			partner.interfaces().closeById(INVENTORY_PANE) // Interface component
			player.stopTrading()
			partner.stopTrading()*/
			player.message("You don't have enough inventory space for this trade.")
			partner.message("Other player doesn't have enough inventory space for this trade.")
			return
		}
		
		if (!fits(partner, player.tradingOffer())) {
			// Check opp too - because if we accept (above check is ok) the other person skips the check!
			partner.message("You don't have enough inventory space for this trade.")
			player.message("Other player doesn't have enough inventory space for this trade.")
			return
		}
		
		player.putattrib(AttributeKey.TRADE_ACCEPTED, false)
		partner.putattrib(AttributeKey.TRADE_ACCEPTED, false)
		
		player.interfaces().sendMain(SCREEN_TWO) // Show 2nd screen
		partner.interfaces().sendMain(SCREEN_TWO) // Show 2nd screen
		
		player.write(InterfaceText(SCREEN_TWO, 30, "Trading with:<br>${partner.name()}"))
		partner.write(InterfaceText(SCREEN_TWO, 30, "Trading with:<br>${player.name()}"))
		
		
		player.write(InterfaceText(SCREEN_TWO, 23, "You are about to give:<br>(Value: ${player.getTradeAmount(true, true)})"))
		player.write(InterfaceText(SCREEN_TWO, 24, "In return you will receive:<br>(Value: ${partner.getTradeAmount(true, true)})"))
		
		partner.write(InterfaceText(SCREEN_TWO, 23, "You are about to give:<br>(Value: ${player.getTradeAmount(false, true)})"))
		partner.write(InterfaceText(SCREEN_TWO, 24, "In return you will receive:<br>(Value: ${partner.getTradeAmount(false, true)})"))
		
		
		player.updateTradingOffer()
		partner.updateTradingOffer()
	}
	
	fun complete(player: Player, partner: Player) {
		// Security
		if (!player.interfaces().visible(INVENTORY_PANE) || !partner.interfaces().visible(INVENTORY_PANE)) {
			return
		}
		
		
		for (item in partner.tradingOffer()) {
			if (item != null) {
				val r = player.inventory().add(item, true)
				if (!r.success()) {
					// Just in case...
					val gitem = GroundItem(player.world(), Item(item.id(), r.requested() - r.completed()), player.tile(), player.id())
					player.world().spawnGroundItem(gitem).hidden().lifetime(GroundItem.UNTRADABLE_LIFETIME.toLong())
					player.message("<col=FF0000>%d x %s was dropped to the floor as you could not carry it.", gitem.item().amount(), gitem.item().name(player.world()))
				}
			}
		}
		for (item in player.tradingOffer()) {
			if (item != null) {
				val r = partner.inventory().add(item, true)
				if (!r.success()) {
					// Just in case...
					val gitem = GroundItem(player.world(), Item(item.id(), r.requested() - r.completed()), player.tile(), player.id())
					player.world().spawnGroundItem(gitem).hidden().lifetime(GroundItem.UNTRADABLE_LIFETIME.toLong())
					player.message("<col=FF0000>%d x %s was dropped to the floor as you could not carry it.", gitem.item().amount(), gitem.item().name(player.world()))
				}
			}
		}
		
		player.world().server().service(LoggingService::class.java, true).ifPresent { s ->
			s.logTrade(player, partner, player.tradingOffer(), partner.tradingOffer())
		}
		
		player.tradingOffer().empty()
		partner.tradingOffer().empty()
		player.dupeWatch().exclude()
		partner.dupeWatch().exclude()
		
		// Update our risk value.
		WildernessLevelIndicator.updateCarriedWealthProtection(player)
		WildernessLevelIndicator.updateCarriedWealthProtection(partner)
		
		player.message("Accepted trade.") // TODO trade msg type
		partner.message("Accepted trade.") // TODO trade msg type
		player.clearattrib(AttributeKey.TRADING_PARTNER)
		partner.clearattrib(AttributeKey.TRADING_PARTNER)
		
		// Close as usual.
		player.interfaces().closeById(INVENTORY_PANE)
		player.interfaces().closeMain()
		partner.interfaces().closeById(INVENTORY_PANE)
		partner.interfaces().closeMain()
		player.stopActions(true)
		partner.stopActions(true)
		
	}
	
	/**
	 * Watchdog that is added to both players in the trade that monitors cancellation of the trading screen and closing.
	 */
	val watchdog: Function1<Script, Any> = @Suspendable { s ->
		s.onInterrupt { it.player().abortTrading() }
		s.waitForInterfaceClose(INVENTORY_PANE) // Wait for it to close
	}
	
	fun Player.updateTradingOffer(updateItems: Boolean = true) {
		val partner = tradingPartner() ?: return
		
		if (updateItems) {
			write(SetItems(90, tradingOffer()))
			write(SetItems(90, -2, 0, partner.tradingOffer()))
		}
		
		val txt: Any = if (partner.freeTradingSlots() > 0) partner.freeTradingSlots() else "no"
		write(InterfaceText(SCREEN_ONE, 9, "${partner.name()} has $txt free inventory slots."))
		
		write(InterfaceText(SCREEN_ONE, 24, "Your offer: <br>(Value: ${getTradeAmount(true, true)})"))
		write(InterfaceText(SCREEN_ONE, 27, "${partner.name()} offers:<br>(Value: ${getTradeAmount(false, true)})"))
		
		if (partner.attribOr(AttributeKey.TRADE_ACCEPTED, false)) {
			write(InterfaceText(SCREEN_ONE, 30, "Other player accepted trade."))
			write(InterfaceText(SCREEN_TWO, 4, "Other player has accepted."))
		} else if (attribOr(AttributeKey.TRADE_ACCEPTED, false)) {
			write(InterfaceText(SCREEN_ONE, 30, "Waiting for other player..."))
			write(InterfaceText(SCREEN_TWO, 4, "Waiting for other player..."))
		} else {
			write(InterfaceText(SCREEN_ONE, 30, ""))
			write(InterfaceText(SCREEN_TWO, 4, "Are you sure you want to make this trade?"))
		}
	}
	
	fun Player.freeTradingSlots(): Int {
		return inventory().freeSlots()
	}
	
	fun Player.acceptTrade() {
		if (SigmundExchangeConstants.hasActiveSession(this)) {
			
			if (attribOr<Boolean>(AttributeKey.TRADE_ACCEPTED, false)) {
				putattrib(AttributeKey.TRADE_ACCEPTED, true)
				write(InterfaceText(SCREEN_TWO, 4, "Waiting for other player..."))
			}
			
			if (interfaces().visible(SCREEN_ONE)) {
				Sigmund.displaySecondScreen(this, SigmundExchangeConstants.getSession(this))
			} else {
				Sigmund.completeExchange(this, SigmundExchangeConstants.getSession(this))
			}
		} else {
			val partner = tradingPartner() ?: return
			// Security
			if (!interfaces().visible(INVENTORY_PANE) || !partner.interfaces().visible(INVENTORY_PANE)) {
				return
			}
			
			putattrib(AttributeKey.TRADE_ACCEPTED, true)
			updateTradingOffer()
			partner.updateTradingOffer()
			
			if (partner.attribOr(AttributeKey.TRADE_ACCEPTED, false)) {
				// If the other person has accepted, now we have...
				if (interfaces().visible(SCREEN_ONE)) {
					// First screen
					proceedToSecond(this, partner)
				} else {
					// Second screen
					complete(this, partner) // We're accepting, waiting for our m8 (jus' w8in for a m8) to complete the trade
				}
			}
		}
	}
	
	fun offerItem(player: Player, amount: Int) {
		if (!player.interfaces().visible(SCREEN_ONE)) {
			return
		}
		val partner: Player = player.tradingPartner() ?: return
		
		val old = player.tradingOffer().copy()
		if (player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return
		val item = player.inventory()[player[AttributeKey.BUTTON_SLOT]] ?: return
		val admin = player.privilege() == Privilege.ADMIN
		val tradable = Item(item).tradable(player.world()) && player.world().flagTradable(item)
		
		if (!tradable) {
			if (!admin && !partner.privilege().eligibleTo(Privilege.ADMIN)) {
				player.message("You can't trade this item.")
				return
			} else {
				player.message("<col=FF0000>Note: </col>${if (admin) "As an administrator, you can trade this untradeable item to normal players" else
				"You're allowed to trade this item because you're trading an Administrator"}.")
			}
		}
		
		//Safety check. Making harder for boosters to boost.
		if (WildernessLevelIndicator.inWilderness(player.tile()) && BountyHunterEmblem.isEmblem(item)) {
			player.message("You can't trade emblems while being in the wilderness!")
			return
		}
		
		// Zulrah stuff is tradable by default, but certain attribs make them not.
		if ((item.property(ItemAttrib.DARTS_COUNT) > 0) && !admin) { // Blowpipe
			player.message("You can't trade this item. Unload it first.")
			return
		}
		
		// A non-toxic fully charged trident is tradable with full 2,500 charges. Other scale items are untradable when charged (helm, staff, blowpipe, toxic trident)
		if ((item.property(ItemAttrib.ZULRAH_SCALES) > 0 || item.property(ItemAttrib.CHARGES) > 0) && item.id() != ZulrahItems.TRIDENT_FULL && !admin) {
			player.message("You can't trade this item. Uncharge it first.")
			return
		}
		
		// Cannot trade Blood Keys!
		if (BloodChest.isKey(item.id()) && WildernessLevelIndicator.inWilderness(player.tile())) {
			player.message("The strong, binding power of the key makes it unable to trade it.")
			return
		}
		
		val removed = player.inventory().remove(Item(item.id(), amount), true).completed()
		player.tradingOffer().add(Item(item.id(), removed), true)
		
		player.clearAcceptance()
		//player.write(UpdateItems(90, old, player.tradingOffer().copy()))
		//partner.write(UpdateItems(90, -2, 0, old, player.tradingOffer().copy()))
	}
	
	@JvmStatic fun takeItem(player: Player, amount: Int) {
		if (!player.interfaces().visible(SCREEN_ONE)) {
			return
		}
		
		val partner: Player = player.tradingPartner() ?: return
		
		
		if (player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return
		val item = player.tradingOffer()[player[AttributeKey.BUTTON_SLOT]] ?: return
		val result = player.tradingOffer().remove(Item(item.id(), amount), true)
		val removed = result.completed()
		
		player.inventory().add(Item(item.id(), removed), true)
		player.clearAcceptance()
		
		var mod = false
		for (i in result.effectedSlots()) {
			player.invokeScript(765, 0, i)
			partner.invokeScript(765, 1, i)
			mod = true
		}
		
		if (mod) {
			player.varps().varbit(Varbit.TRADE_MODIFIED_LEFT, 1)
			partner.varps().varbit(Varbit.TRADE_MODIFIED_RIGHT, 1)
		}
	}
	
	fun Player.getTradeAmount(is_player: Boolean, trading: Boolean): String {
		if (world().realm().isPVP)
			return calculate_blood_money(is_player, trading) //Generate a price using our Hashmap.
		else
			return calculate_coins(is_player) //Generate a price using high alchemy prices
	}
	
	fun Player.calculate_blood_money(is_player: Boolean, trading: Boolean): String {
		var blood_money_value = 0
		val partner = tradingPartner()
		
		if (!trading && SigmundExchangeConstants.hasActiveSession(this)) {
			SigmundExchangeConstants.getSession(this).playersOffer().forEach { item ->
				if (item != null) {
					val purchase = Item(item)
					if (this.world().prices().containsKey(purchase.unnote(world()).id())) {
						val cost = this.world().prices()[purchase.unnote(world()).id()]
						
						if ((blood_money_value + cost * item.amount()) >= 2147483647
								|| (blood_money_value + cost * item.amount()) < 0) {
							blood_money_value = 2147483647
						} else {
							blood_money_value += cost * item.amount()
						}
					}
				}
			}
		} else {
			if (is_player) {
				tradingOffer().forEach { item ->
					if (item != null) {
						val purchase = Item(item)
						if (this.world().prices().containsKey(purchase.unnote(world()).id())) {
							val cost = this.world().prices()[purchase.unnote(world()).id()]
							
							if ((blood_money_value + cost * item.amount()) >= 2147483647
									|| (blood_money_value + cost * item.amount()) < 0) {
								blood_money_value = 2147483647
							} else {
								blood_money_value += cost * item.amount()
							}
						}
					}
				}
			} else {
				partner!!.tradingOffer().forEach { item ->
					if (item != null) {
						val purchase = Item(item)
						if (partner.world().prices().containsKey(purchase.unnote(world()).id())) {
							val cost = partner.world().prices()[purchase.unnote(world()).id()]
							
							if ((blood_money_value + cost * item.amount()) >= 2147483647
									|| (blood_money_value + cost * item.amount()) < 0) {
								blood_money_value = 2147483647
							} else {
								blood_money_value += cost * item.amount()
							}
						}
					}
				}
			}
		}
		if (blood_money_value == 2147483647)
			return "<col=ffffff>Lots of</col> Blood Money"
		else
			return "<col=ffffff>${NumberFormat.getInstance().format(blood_money_value)} </col>Blood Money"
	}
	
	fun Player.calculate_coins(is_player: Boolean): String {
		val partner = tradingPartner()
		var trade_value = 0
		
		if (is_player) {
			tradingOffer().forEach { item ->
				if (item != null) {
					val ITEM_DEF = world().definitions().get(ItemDefinition::class.java, item.unnote(world()).id())
					val item_value = this.world().prices()[ITEM_DEF.id]
					if ((trade_value + item_value * item.amount()) >= 2147483647 || (trade_value + item_value * item.amount()) < 0) {
						trade_value = 2147483647
					} else {
						trade_value += item_value * item.amount()
					}
				}
			}
		} else {
			partner!!.tradingOffer().forEach { item ->
				if (item != null) {
					val item_def = world().definitions().get(ItemDefinition::class.java, item.unnote(world()).id())
					val item_value = this.world().prices()[item_def.id]
					
					if ((trade_value + item_value * item.amount()) >= 2147483647
							|| (trade_value + item_value * item.amount()) < 0) {
						trade_value = 2147483647
					} else {
						trade_value += item_value * item.amount()
					}
				}
			}
		}
		if (trade_value == 2147483647)
			return "<col=ffffff>Lots of</col> Coins"
		else
			return "<col=ffffff>${NumberFormat.getInstance().format(trade_value)} </col>Coins"
	}
	
	@JvmStatic fun on_logout(player: Player) {
		player.abortTrading()
		val count: Int = player.tradingOffer().occupiedSlots()
		if (count > 0) {
			player.stopTrading()
			logger.error("TRADING #2: {} still had {} items in trading inv on logout at {}", player.name(), count, player.tile().toStringSimple())
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onPlayerOption4 { tradePlayer(it) }
		
		// Offer buttons
		r.onButton(INVENTORY_PANE, 0) s@ {
			
			val trading = !SigmundExchangeConstants.hasActiveSession(it.player())
			
			when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
				1 -> if (trading) offerItem(it.player(), 1) else Sigmund.offerItem(it.player(), SigmundExchangeConstants.getSession(it.player()), 1)
				2 -> if (trading) offerItem(it.player(), 5) else Sigmund.offerItem(it.player(), SigmundExchangeConstants.getSession(it.player()), 5)
				3 -> if (trading) offerItem(it.player(), 10) else Sigmund.offerItem(it.player(), SigmundExchangeConstants.getSession(it.player()), 10)
				4 -> if (trading) offerItem(it.player(), Int.MAX_VALUE) else Sigmund.offerItem(it.player(), SigmundExchangeConstants.getSession(it.player()), Int.MAX_VALUE)
				5 -> if (trading) offerItem(it.player(), it.inputInteger("Enter amount:")) else Sigmund.offerItem(it.player(), SigmundExchangeConstants.getSession(it.player()), it.inputInteger("Enter amount:"))
				10 -> {
					if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
					val item = it.player().inventory()[it.player()[AttributeKey.BUTTON_SLOT]]
					if (item != null) {
						it.player().message(it.player().world().examineRepository().item(item.id()))
					}
				}
			}
		}
		
		// Withdraw
		r.onButton(SCREEN_ONE, 25) s@ {
			
			val trading = !SigmundExchangeConstants.hasActiveSession(it.player())
			
			when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
				1 -> if (trading) takeItem(it.player(), 1) else Sigmund.removeItem(it.player(), SigmundExchangeConstants.getSession(it.player()), 1)
				2 -> if (trading) takeItem(it.player(), 5) else Sigmund.removeItem(it.player(), SigmundExchangeConstants.getSession(it.player()), 5)
				3 -> if (trading) takeItem(it.player(), 10) else Sigmund.removeItem(it.player(), SigmundExchangeConstants.getSession(it.player()), 10)
				4 -> if (trading) takeItem(it.player(), Int.MAX_VALUE) else Sigmund.removeItem(it.player(), SigmundExchangeConstants.getSession(it.player()), Int.MAX_VALUE)
				5 -> if (trading) takeItem(it.player(), it.inputInteger("Enter amount:")) else Sigmund.removeItem(it.player(), SigmundExchangeConstants.getSession(it.player()), it.inputInteger("Enter amount:"))
				10 -> {
					if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
					val item = it.player().tradingOffer()[it.player()[AttributeKey.BUTTON_SLOT]]
					if (item != null) {
						it.player().message(it.player().world().examineRepository().item(item.id()))
					}
				}
			}
		}
		
		// Examine
		r.onButton(SCREEN_ONE, 28) s@ {
			val partner: Player = it.player().tradingPartner() ?: return@s
			if (it.player().attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return@s
			val item = partner.tradingOffer()[it.player()[AttributeKey.BUTTON_SLOT]]
			if (item != null) {
				it.player().message(it.player().world().examineRepository().item(item.id()))
			}
		}
		
		// Decline
		r.onButton(SCREEN_ONE, 13) {
			it.player().abortTrading()
		}
		r.onButton(SCREEN_TWO, 14) {
			it.player().abortTrading()
		}
		r.onButton(SCREEN_TWO, 32) {
			// X button on screen 2 (screen 1 calls closeInterface packet)
			it.player().abortTrading()
		}
		
		// Accept
		r.onButton(SCREEN_ONE, 10) {
			it.player().acceptTrade()
		}
		r.onButton(SCREEN_TWO, 13) {
			it.player().acceptTrade()
		}
		
		r.onInterfaceClose(SCREEN_ONE) {
			it.player().abortTrading()
		}
		r.onInterfaceClose(SCREEN_TWO) {
			it.player().abortTrading()
		}
		r.onInterfaceClose(INVENTORY_PANE) {
			it.player().abortTrading()
		}
	}
	
	private fun tradePlayer(it: Script) {
		val otherPlayer = it.player().attrib<WeakReference<Player>>(AttributeKey.TARGET).get()
		
		// Ironman? fuck off lol!!
		if (it.player().ironMode() != IronMode.NONE && (otherPlayer == null || !(otherPlayer.privilege() == Privilege.ADMIN || otherPlayer.seniorModerator()))) {
			it.message("You are an Iron Man. You stand alone.")
			return
		}
		
		// Starter trade prevention
		if (!it.player().world().realm().isPVP) {
			if (it.player().gameTime() < 3000 && !it.player().privilege().eligibleTo(Privilege.ADMIN) && !otherPlayer!!.privilege().eligibleTo(Privilege.ADMIN)) {
				it.message("You are restricted from trading until 30 minutes of play time. Only ${Math.ceil((3000.0 - it.player().gameTime()) / 100.0).toInt()} minutes left.")
				return
			}
			if (otherPlayer!!.gameTime() < 3000 && !otherPlayer.privilege().eligibleTo(Privilege.ADMIN) && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				it.message("Your partner is restricted from trading until 30 minutes of play time. Only ${Math.ceil((3000.0 - otherPlayer.gameTime()) / 100.0).toInt()} minutes left.")
				return
			}
		}
		
		if (it.player().jailed() || otherPlayer!!.jailed()) {
			if ((it.player().privilege() == Privilege.PLAYER && !otherPlayer!!.privilege().eligibleTo(Privilege.MODERATOR)) ||
					(otherPlayer!!.privilege() == Privilege.PLAYER && !it.player().privilege().eligibleTo(Privilege.MODERATOR))) {
				it.message("You cannot trade when jailed.")
				return
			}
		}
		
		// Make sure you're not trading yourself.
		if (it.player() == otherPlayer || it.player().id() == otherPlayer!!.id()) {
			return
		}

        //Stop trading in clan wars
		if (ClanWars.inInstance(it.player())) {
            it.message("You can't do that right now.")
            return
        }

        if (ClanWars.inInstance(otherPlayer)) {
            it.message("The other player is busy at the moment.")
            return
        }
		
		// Disallow trading in stakes. Don't want to give your friends food or potions.
		if (it.player().attribOr<Boolean>(AttributeKey.IN_STAKE, false) || otherPlayer.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			it.messagebox("You can't trade during a duel.")
			return
		}
		
		if (otherPlayer.interfaces().activeMain() != -1) {
			it.message("That player is currently busy.")
			return
		}
		
		// If invited, initiate
		if (invited(otherPlayer, it.player())) {
			
			if (!it.player().dead() && !otherPlayer.dead() && !it.player().locked() && !otherPlayer.locked()) {
				it.player().stopActions(true)
				otherPlayer.stopActions(true)
				
				initiateTrade(it.player(), otherPlayer)
				initiateTrade(otherPlayer, it.player())
			}
		} else {
			// If not yet invited, send a request.
			it.player().timers().extendOrRegister(TimerKey.TRADE_INVITATION_OPEN, 50)
			it.player().putattrib(AttributeKey.REQUESTED_TRADE_PARTNER, otherPlayer.id())
			otherPlayer.write(AddMessage("${it.player().name()} wishes to trade with you.", AddMessage.Type.TRADING, it.player().name()))
			it.player().message("Sending trade offer...")
		}
	}
	
}