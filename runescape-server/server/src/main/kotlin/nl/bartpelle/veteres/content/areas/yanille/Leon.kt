package nl.bartpelle.veteres.content.areas.yanille

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 15/06/2016.
 */

object Leon {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption4(1502) @Suspendable {
			it.chatNpc("I've got no business with you sir.", 1502)
			// TODO kebbit bolt making https://i.gyazo.com/f3be3e0d9e1235892a9e5242740e1bc2.png
		}
		repo.onNpcOption1(1502) @Suspendable {
			// Note to self. Nullpointer for Fibre thrown when you use 'it.func()' calls in embedded functions. Define the script as per below and reference that.
			// Functions must also be annotated suspendable.
			val s: Script = it
			@Suspendable fun mainop2() {
				s.chatNpc("I'm afraid with it being a prototype, I've only got a few for my own testing purposes.", 1502)
				s.chatNpc("Of course, for the right price, it might be worth my while to make another prototype", 1502)
				s.chatPlayer("How much?")
				s.chatNpc("To you, 1300 gold pieces.", 1502)
				when (s.options("Ok, here you go.", "I'll give it a miss, thanks.", "Does it use normal crossbow bolts?")) {
					1 -> {
						s.chatPlayer("Ok, here you go.")
						if (!s.player().world().realm().isPVP) {
							if (s.player().inventory().count(995) < 1300) {
								s.chatPlayer("Ok, actually I don't have enough money with me.")
							} else {
								if (s.player().inventory().count(995) - 1300 != 0 && s.player().inventory().freeSlots() == 0) {
									s.chatNpc("It seems you don't have enough inventory space to buy the crossbow.", 1502)
								} else if (s.player().inventory().remove(Item(995, 1300), true).success()) {
									s.player().inventory().add(Item(10156), true)
									s.chatPlayer("Thanks!")
									s.doubleItemBox("You exchange the gold for the odd contraption.", 995, 10156)
								}
								// TODO check for inv space, take coins, give item.
							}
						} else {
							s.chatNpc("Haha gotcha. Check the PK shops in Edgeville to buy my crossbow.", 1502)
						}
					}
					2 -> s.chatNpc("I'll give it a miss, thanks.", 1502)
					3 -> {
						s.chatPlayer("Does it use normal crossbow bolts?")
						s.chatNpc("Ah, I admit as a result of its er...unique construction, it won't take just any old bolts.", 1502)
						s.chatNpc("If you can spplu the materials and a token fee, I'd be happy to make some for you.", 1502)
						s.chatPlayer("What materials do you need?")
						s.chatNpc("Kebbit spikes. Lots of 'em. Not all kebbits have spikes, mind you. You'll be needing to look for prickly kebbits or, even better, razor-backed kebbits \n" +
								"to get material hard enough.", 1502)
						when (s.options("Let's see what you can make then.", "I'll give it a miss, thanks.", "Can't I just make my own?")) {
						// TODO 1 -> trade npc option 2
							2 -> s.chatNpc("I'll give it a miss, thanks.", 1502)
							3 -> {
								s.chatNpc("Can't I just make my own?", 1502)
								s.chatPlayer("Yes, I suppose you could, although you'll need a steady hard with a knife and a chisel.", 1502)
								s.chatPlayer("The bolts have an unusual diameter, but basically you'll just need to be able to carve kebbit spikes into straight shafts.", 1502)
								s.chatPlayer("Great; thanks.")
							}
						}
					}
				}
			}
			
			@Suspendable fun mainop3() {
				s.chatPlayer("What are you holding there?")
				s.chatNpc("This? This is a prototype for a new type of crossbow I've been designing.", 1502)
				s.chatPlayer("It looks pretty weird.")
				s.chatNpc("It's good, isn't it? It includes the bones of various animals in its construction.", 1502)
				s.chatNpc("It's also a fair bit faster than an ardinary crossbow too; it'll take you far less time to reload between shots.", 1502)
				when (s.options("This crazy, it'll never work!", "Could I have a go with it?", "Does it use normal crossbow bolts?")) {
					1 -> {
						s.chatPlayer("This crazy, it'll never work!")
						s.chatNpc("That's what they said about my wind-powered mouse traps, too.", 1502)
						s.chatPlayer("And did they work?")
						s.chatNpc("Well, they only ran into problems because people kept insisting on trying to use them indoors.", 1502)
					}
					2 -> {
						s.chatPlayer("Could I have a go with it?")
						mainop2()
					}
					3 -> mainop3()
				}
			}
			
			@Suspendable fun ops1() {
				when (s.options("What is this place?", "Can I have a go with your crossbow?", "What are you holding there?")) {
					1 -> {
						s.chatPlayer("What is this place?")
						s.chatNpc("This is Aleck's Hunter Emporium. Basically, it's just a shop with a fancy name; you can buy various weapons and traps here.", 1502)
						when (s.options("Can I buy some equipment then?", "Can I have a go with your crossbow?", "What are you holding there?")) {
							1 -> {
								s.chatNpc("Oh, this isn't my shop; there owner is Aleck over there behind the counter.", 1502)
								s.chatNpc("I experiment with my weapon designs. I'm here becaues I've been trying to convince people to back my research and maybe sell some of my products -\n" +
										"like this one I'm holding - in their shops.", 1502)
								s.chatNpc("Aleck doesn't seem to be interested, though.", 1502)
								when (s.options("Can I have a go with your crossbow?", "What are you holding there?")) {
									1 -> {
										s.chatPlayer("Could I have a go with your crossbow?")
										mainop2()
									}
									2 -> mainop3()
								}
							}
							2 -> {
								s.chatPlayer("Could I have a go with your crossbow?")
								mainop2()
							}
							3 -> mainop3()
						}
						
					}
					2 -> {
						s.chatPlayer("Could I have a go with your crossbow?")
						mainop2()
					}
					3 -> mainop3()
				}
			}
			ops1()
		}
	}
}