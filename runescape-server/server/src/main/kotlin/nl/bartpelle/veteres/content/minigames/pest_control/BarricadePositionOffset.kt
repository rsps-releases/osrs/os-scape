package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.veteres.content.TileOffset
import nl.bartpelle.veteres.content.to

/**
 * Created by Jason MacKeigan on 2016-08-30 at 7:39 PM
 */
enum class BarricadePositionOffset(vararg val offsets: TileOffset) {
	WEST(-20 to -2, -20 to 0),
	SOUTH_WEST(-8 to -14, -7 to -14),
	SOUTH_SOUTH_WEST(-19 to -19, -19 to -20),
	SOUTH(1 to -17, 2 to -17),
	SOUTH_SOUTH_EAST(11 to -14, 12 to -14),
	SOUTH_EAST(20 to -19, 20 to -18),
	EAST(20 to -6, 20 to -7),
	NORTH_EAST(17 to 0, 17 to -1)
	;
	
	companion object {
		
		val ALL: List<TileOffset> = BarricadePositionOffset.values().flatMap { it.offsets.asIterable() }
		
	}
}