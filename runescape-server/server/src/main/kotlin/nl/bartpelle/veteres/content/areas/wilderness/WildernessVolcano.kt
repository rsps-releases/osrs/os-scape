package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/25/2016.
 */


object WildernessVolcano {
	
	enum class Shield(val shard_1: Item, val shard_2: Item, val shard_3: Item, val shield: Item, val item_name: String) {
		MALEDICTION_WARD(Item(11931), Item(11932), Item(11933), Item(11924), "Malediction"),
		ODIUM_WARD(Item(11928), Item(11929), Item(11930), Item(11926), "Odium");
	}
	
	val GOLD_RING = 1635
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnObject(26755) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			val forge: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val goblin = Npc(3028, it.player().world(), Tile(it.player().tile().x - 1, it.player().tile().z))
			
			if (item == GOLD_RING) {
				it.player().lock()
				if (!it.player().tile().equals(forge.tile().transform(0, -1, 0))) {
					it.player().walkTo(forge.tile().transform(0, -1, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(forge.tile().transform(0, -1, 0))
				}
				it.player().world().registerNpc(goblin)
				goblin.face(it.player())
				goblin.sync().shout("My Precious!!! NOOOOO!!!")
				it.delay(1)
				goblin.animate(6184)
				goblin.respawns(false)
				it.delay(2)
				it.player().world().unregisterNpc(goblin)
				it.player().unlock()
			} else {
				val shield = Shield.values().find { shield ->
					item == shield.shard_1.id() || item == shield.shard_2.id() || item == shield.shard_3.id()
				}
				if (shield != null) {
					if (shield.shard_1 in it.player().inventory() && shield.shard_2 in it.player().inventory() &&
							shield.shard_3 in it.player().inventory()) {
						it.player().lock()
						if (!it.player().tile().equals(forge.tile().transform(0, -1, 0))) {
							it.player().walkTo(forge.tile().transform(0, -1, 0), PathQueue.StepType.FORCED_WALK)
							it.waitForTile(forge.tile().transform(0, -1, 0))
						}
						it.delay(2)
						it.blankmessagebox("You drop the three shield shards into the mouth of the volcanic<br>chamber of fire.")
						it.animate(4411)
						it.delay(1)
						it.player().forceMove(ForceMovement(0, 0, 0, -1, 45, 126, FaceDirection.NORTH))
						it.delay(1)
						it.player().animate(734, 5)
						it.delay(3)
						it.player().teleport(forge.tile().x, forge.tile().z - 1)
						it.delay(1)
						it.player().inventory() -= shield.shard_1
						it.player().inventory() -= shield.shard_2
						it.player().inventory() -= shield.shard_3
						
						it.player().inventory() += shield.shield
						it.player().message("You forge the shield pieces together in the chambers of fire and are blown back by")
						it.player().message("the intense heat.")
						
						it.player().interfaces().closeById(229)
						it.player().unlock()
					} else {
						it.message("You need all three ${shield.item_name} shards to forge a shield.")
					}
				}
			}
		}
	}
}