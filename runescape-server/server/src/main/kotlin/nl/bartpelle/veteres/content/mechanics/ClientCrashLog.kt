package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.model.entity.Player
import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Jak on 15/12/2016.
 */
object ClientCrashLog {
	
	class CrashEntry(val uuid: Int, val pName: String, val err: String, val posInfo: String, val time: String)
	
	// The log
	@JvmStatic val CRASHENTRY_LIST: ArrayList<CrashEntry> = ArrayList()
	
	// We need a way to reference punishments after adding them to altar the punishment reason at a later time.
	@JvmStatic var INCREMENTING_ENTRY_UUID = 0
	
	
	// Display the log.
	@JvmStatic fun displayLog(player: Player) {
		// Clear the interface
		QuestGuide.clear(player)
		
		// Text to render onto the quest list interface
		val text = ArrayList<String>()
		text.add("<img=1> Crash log (showing ${CRASHENTRY_LIST.size}, caught $INCREMENTING_ENTRY_UUID)")
		text.add("")
		if (CRASHENTRY_LIST.size == 0) {
			text.add("(no crashes!)")
		} else {
			for (i in 0..Math.min(30, CRASHENTRY_LIST.size - 1)) {
				val o = CRASHENTRY_LIST.get(i)
				
				// Write the header
				text.add("" + o.uuid + ". [" + o.time + "] <col=" + PunishmentLog.COL + ">" + o.pName + "</col> crashed at " + o.posInfo + ".")
				
				// Write the exception, if longer than 55 characters then write 55 per line
				if (o.err.length < 55)
					text.add("Detail: " + o.err)
				else {
					val rows = 1 + (o.err.length / 55) // +1 to ensure we get it all, might mess up from rounding
					for (a in 0..rows) {
						text.add(o.err.substring(Math.min(o.err.length, a * 55), Math.min(o.err.length, (a * 55) + 55)))
					}
				}
				text.add("")
			}
		}
		
		// Draw that text
		for (i in 2..2 + text.size - 1) {
			player.interfaces().text(275, i, text[i - 2])
		}
		
		// Display
		QuestGuide.open(player)
	}
	
	@JvmStatic fun render_string(entry: CrashEntry): String {
		return "Entry " + entry.uuid + " at time: " + entry.time + ". User: '" + entry.pName + "' crashed at " + entry.posInfo + ". Message: ' " + entry.err + " '"
	}
	
	@JvmStatic fun dump_crash_log(player: Player?) {
		Thread(Runnable() {
			try {
				val fw = FileWriter("client_crash_log_dump.txt", true)
				synchronized(CRASHENTRY_LIST) {
					CRASHENTRY_LIST.forEach { entry ->
						try {
							fw.append(render_string(entry))
							fw.append("\n")
						} catch (e: Exception) {
							e.printStackTrace()
						}
					}
				}
				fw.close()
			} catch (e: Exception) {
				e.printStackTrace()
			}
		}).start()
		player ?: return
		player.executeScript @Suspendable { it ->
			when (it.optionsTitled("Clear crash log?", "Yes.", "No.")) {
				1 -> {
					val size = CRASHENTRY_LIST.size
					CRASHENTRY_LIST.clear()
					it.messagebox("Crash log cleared of $size entries.")
				}
			}
		}
	}
	
	// Append a punishment
	@JvmStatic fun append(pname: String, err: String, pos: String): Int {
		// Cap the list size, you should be on daily to dump them anyway. Stops using too much mem.
		if (CRASHENTRY_LIST.size > 1000) {
			dump_crash_log(null)
			CRASHENTRY_LIST.clear()
		}
		ClientCrashLog.CRASHENTRY_LIST.add(0,
				CrashEntry(ClientCrashLog.INCREMENTING_ENTRY_UUID++, pname, err, pos, SimpleDateFormat(PunishmentLog.DATETIME_FORMAT).format(Date())))
		
		return ClientCrashLog.INCREMENTING_ENTRY_UUID - 1
	}
}