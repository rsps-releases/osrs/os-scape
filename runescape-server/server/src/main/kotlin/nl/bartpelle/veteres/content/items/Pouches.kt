package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-01-22.
 */

object Pouches {
	
	val SMALL_POUCH = 5509
	val MEDIUM_POUCH = 5510
	val LARGE_POUCH = 5512
	val GIANT_POUCH = 5514
	
	val REGULAR_ESSENCE = 1436
	val PURE_ESSENCE = 7936
	
	val POUCHES = intArrayOf(SMALL_POUCH, MEDIUM_POUCH, LARGE_POUCH, GIANT_POUCH)
	
	@JvmStatic fun isPouch(id: Int) = id == SMALL_POUCH || id == MEDIUM_POUCH || id == LARGE_POUCH || id == GIANT_POUCH
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (POUCH in POUCHES) {
			//Fill
			r.onItemOption1(POUCH, s@ @Suspendable {
				val player = it.player()
				var essenceType = essenceType(player, POUCH)
				
				if (amtOfEssence(player) == 0) {
					player.message("You do not have any essence to fill your pouch with.")
					return@s
				}
				
				if (isPouchFull(player, POUCH)) {
					player.message("Your pouch is full.")
					return@s
				}
				
				if (essenceType == 0) {
					if (player.inventory().has(REGULAR_ESSENCE)) essenceType = REGULAR_ESSENCE
					else if (player.inventory().has(PURE_ESSENCE)) essenceType = PURE_ESSENCE
				}
				
				when (essenceType) {
					REGULAR_ESSENCE -> addEssenceToPouch(it, POUCH, REGULAR_ESSENCE)
					PURE_ESSENCE -> addEssenceToPouch(it, POUCH, PURE_ESSENCE)
					else -> addEssenceToPouch(it, POUCH, PURE_ESSENCE)
				}
			})
			
			//Empty
			r.onItemOption2(POUCH, s@ @Suspendable {
				val player = it.player()
				var amountToAdd = getPouchContents(player, POUCH)
				val essenceToAdd = if (essenceType(player, POUCH) == PURE_ESSENCE) PURE_ESSENCE
				else REGULAR_ESSENCE
				
				if (player.inventory().full()) {
					player.message("You do not have any more free space in your inventory.")
					return@s
				}
				
				if (amountToAdd == 0) {
					player.message("There are no essence in your pouch.")
					player.clearattrib(getAttributeForPureOrNot(POUCH))
					player.clearattrib(getAttributeForPouch(POUCH))
					return@s
				}
				
				if (amountToAdd > player.inventory().freeSlots())
					amountToAdd = player.inventory().freeSlots()
				
				if (amountToAdd == getPouchContents(player, POUCH)) {
					player.clearattrib(getAttributeForPureOrNot(POUCH))
					player.clearattrib(getAttributeForPouch(POUCH))
				} else {
					player.putattrib(getAttributeForPouch(POUCH), getPouchContents(player, POUCH) - amountToAdd)
				}
				
				player.inventory().add(Item(essenceToAdd, amountToAdd), true)
				
			})
			
			
			//Check
			r.onItemOption3(POUCH) @Suspendable {
				val player = it.player()
				val amount = getPouchContents(player, POUCH)
				val pureOrNo = if (essenceType(player, POUCH) == PURE_ESSENCE) "pure" else "normal"
				val isOrAre = if (amount == 1) "is" else "are"
				
				if (amount == 0) player.message("There are no essence in this pouch.")
				else player.message("There $isOrAre $amount $pureOrNo essence in this pouch. ")
			}
			
			r.onItemOnItem(POUCH, REGULAR_ESSENCE, s@ @Suspendable {
				val player = it.player()
				
				if (isPouchFull(player, POUCH)) {
					player.message("You cannot add any more essence to the pouch.")
					return@s
				}
			})
		}
	}
	
	private fun addEssenceToPouch(it: Script, pouch: Int, essenceType: Int) {
		val player = it.player()
		var num = player.inventory().count(essenceType)
		
		while (num-- > 0) {
			if (isPouchFull(player, pouch))
				break
			
			if (player.inventory().remove(Item(essenceType), false).success()) {
				player.putattrib(getAttributeForPouch(pouch), getPouchContents(player, pouch) + 1)
				setEssenceType(player, pouch, essenceType)
			}
		}
	}
	
	private fun getPouchSize(pouch: Int): Int {
		return when (pouch) {
			SMALL_POUCH -> 3
			MEDIUM_POUCH -> 6
			LARGE_POUCH -> 9
			GIANT_POUCH -> 12
			else -> 0
		}
	}
	
	private fun getAttributeForPouch(pouch: Int): AttributeKey {
		return when (pouch) {
			SMALL_POUCH -> AttributeKey.SMALL_POUCH
			MEDIUM_POUCH -> AttributeKey.MEDIUM_POUCH
			LARGE_POUCH -> AttributeKey.LARGE_POUCH
			GIANT_POUCH -> AttributeKey.GIANT_POUCH
			else -> AttributeKey.SMALL_POUCH
		}
	}
	
	private fun getAttributeForPureOrNot(pouch: Int): AttributeKey {
		return when (pouch) {
			SMALL_POUCH -> AttributeKey.STORED_IN_SMALL_POUCH
			MEDIUM_POUCH -> AttributeKey.STORED_IN_MEDIUM_POUCH
			LARGE_POUCH -> AttributeKey.STORED_IN_LARGE_POUCH
			GIANT_POUCH -> AttributeKey.STORED_IN_GIANT_POUCH
			else -> AttributeKey.STORED_IN_SMALL_POUCH
		}
	}
	
	private fun amtOfEssence(player: Player): Int {
		return getAmtOfRuneEssence(player) + getAmtOfPureEssence(player)
	}
	
	private fun isPouchFull(player: Player, pouch: Int): Boolean {
		return getPouchSize(pouch) <= getPouchContents(player, pouch)
	}
	
	private fun getPouchContents(player: Player, pouch: Int): Int {
		return player.attribOr<Int>(getAttributeForPouch(pouch), 0)
	}
	
	private fun getAmtOfRuneEssence(player: Player): Int {
		return player.inventory().count(REGULAR_ESSENCE)
	}
	
	private fun getAmtOfPureEssence(player: Player): Int {
		return player.inventory().count(PURE_ESSENCE)
	}
	
	private fun essenceType(player: Player, pouch: Int): Int {
		return when (pouch) {
			SMALL_POUCH -> player.attribOr(AttributeKey.STORED_IN_SMALL_POUCH, 0)
			MEDIUM_POUCH -> player.attribOr(AttributeKey.STORED_IN_MEDIUM_POUCH, 0)
			LARGE_POUCH -> player.attribOr(AttributeKey.STORED_IN_LARGE_POUCH, 0)
			GIANT_POUCH -> player.attribOr(AttributeKey.STORED_IN_GIANT_POUCH, 0)
			else -> 0
		}
	}
	
	private fun setEssenceType(player: Player, pouch: Int, essence: Int) {
		when (pouch) {
			SMALL_POUCH -> player.putattrib(AttributeKey.STORED_IN_SMALL_POUCH, essence)
			MEDIUM_POUCH -> player.putattrib(AttributeKey.STORED_IN_MEDIUM_POUCH, essence)
			LARGE_POUCH -> player.putattrib(AttributeKey.STORED_IN_LARGE_POUCH, essence)
			GIANT_POUCH -> player.putattrib(AttributeKey.STORED_IN_GIANT_POUCH, essence)
		}
	}
	
}
