package nl.bartpelle.veteres.content.tournament

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.script.ScriptRepository

object TournamentMap {

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onWorldInit {
            setupPVPTournament(it)
        }
    }

    private fun setupPVPTournament(it: Script) {
        TournamentConfig.PVP_TOURNAMENT_AREA = allocateMap(it.ctx<World>(), intArrayOf(14477, 14478, 14733, 14734))

        val map = Tile(TournamentConfig.PVP_TOURNAMENT_AREA.center())
        val arenas = ArrayList<Area>()

        // North west ruins
        arenas.add(Area(map.transform(-46, 29), map.transform(-40, 35)))
        arenas.add(Area(map.transform(-46, 49), map.transform(-40, 55)))
        arenas.add(Area(map.transform(-56, 39), map.transform(-50, 45)))
        arenas.add(Area(map.transform(-36, 39), map.transform(-30, 45)))

        // South west ground level
       arenas.add(Area(map.transform(-52, 7), map.transform(-47, 12)))
       arenas.add(Area(map.transform(-52, 17), map.transform(-47, 22)))
       arenas.add(Area(map.transform(-42, 7), map.transform(-37, 12)))
       arenas.add(Area(map.transform(-42, 17), map.transform(-37, 22)))

        // South west castle second floor
        arenas.add(Area(map.transform(-53, 5), map.transform(-48, 10), 1))
        arenas.add(Area(map.transform(-53, 12), map.transform(-48, 17), 1))
        arenas.add(Area(map.transform(-53, 19), map.transform(-48, 24), 1))
        arenas.add(Area(map.transform(-61, 12), map.transform(-56, 17), 1))
        arenas.add(Area(map.transform(-41, 5), map.transform(-36, 10), 1))
        arenas.add(Area(map.transform(-41, 12), map.transform(-36, 17), 1))
        arenas.add(Area(map.transform(-41, 19), map.transform(-36, 24), 1))
        arenas.add(Area(map.transform(-33, 12), map.transform(-28, 17), 1))

        // South west castle third floor
        arenas.add(Area(map.transform(-55, 4), map.transform(-51, 8), 2))
        arenas.add(Area(map.transform(-55, 9), map.transform(-51, 13), 2))
        arenas.add(Area(map.transform(-55, 16), map.transform(-51, 20), 2))
        arenas.add(Area(map.transform(-55, 21), map.transform(-51, 25), 2))
        arenas.add(Area(map.transform(-50, 4), map.transform(-46, 8), 2))
        arenas.add(Area(map.transform(-50, 9), map.transform(-46, 13), 2))
        arenas.add(Area(map.transform(-50, 16), map.transform(-46, 20), 2))
        arenas.add(Area(map.transform(-50, 21), map.transform(-46, 25), 2))
        arenas.add(Area(map.transform(-43, 4), map.transform(-39, 8), 2))
        arenas.add(Area(map.transform(-43, 9), map.transform(-39, 13), 2))
        arenas.add(Area(map.transform(-43, 16), map.transform(-39, 20), 2))
        arenas.add(Area(map.transform(-43, 21), map.transform(-39, 25), 2))
        arenas.add(Area(map.transform(-38, 4), map.transform(-34, 8), 2))
        arenas.add(Area(map.transform(-38, 9), map.transform(-34, 13), 2))
        arenas.add(Area(map.transform(-38, 16), map.transform(-34, 20), 2))
        arenas.add(Area(map.transform(-38, 21), map.transform(-34, 25), 2))

        // North left castle ground level
        arenas.add(Area(map.transform(-23, 36), map.transform(-18, 41)))
        arenas.add(Area(map.transform(-23, 46), map.transform(-18, 51)))
        arenas.add(Area(map.transform(-13, 36), map.transform(-8, 41)))
        arenas.add(Area(map.transform(-13, 46), map.transform(-8, 51)))

        // North left castle second floor
        arenas.add(Area(map.transform(-25, 35), map.transform(-20, 40), 1))
        arenas.add(Area(map.transform(-25, 47), map.transform(-20, 52), 1))
        arenas.add(Area(map.transform(-18, 27), map.transform(-13, 32), 1))
        arenas.add(Area(map.transform(-18, 35), map.transform(-13, 40), 1))
        arenas.add(Area(map.transform(-18, 35), map.transform(-13, 40), 1))
        arenas.add(Area(map.transform(-18, 47), map.transform(-13, 52), 1))
        arenas.add(Area(map.transform(-18, 55), map.transform(-13, 60), 1))
        arenas.add(Area(map.transform(-11, 35), map.transform(-6, 40), 1))
        arenas.add(Area(map.transform(-11, 47), map.transform(-6, 52), 1))

        // North left third floor
        arenas.add(Area(map.transform(-26, 33), map.transform(-22, 37), 2))
        arenas.add(Area(map.transform(-26, 38), map.transform(-22, 42), 2))
        arenas.add(Area(map.transform(-26, 45), map.transform(-22, 49), 2))
        arenas.add(Area(map.transform(-26, 50), map.transform(-22, 54), 2))
        arenas.add(Area(map.transform(-21, 33), map.transform(-17, 37), 2))
        arenas.add(Area(map.transform(-21, 38), map.transform(-17, 42), 2))
        arenas.add(Area(map.transform(-21, 45), map.transform(-17, 49), 2))
        arenas.add(Area(map.transform(-21, 50), map.transform(-17, 54), 2))
        arenas.add(Area(map.transform(-14, 33), map.transform(-10, 37), 2))
        arenas.add(Area(map.transform(-14, 38), map.transform(-10, 42), 2))
        arenas.add(Area(map.transform(-14, 45), map.transform(-10, 49), 2))
        arenas.add(Area(map.transform(-14, 50), map.transform(-10, 54), 2))
        arenas.add(Area(map.transform(-9, 33), map.transform(-5, 37), 2))
        arenas.add(Area(map.transform(-9, 38), map.transform(-5, 42), 2))
        arenas.add(Area(map.transform(-9, 45), map.transform(-5, 49), 2))
        arenas.add(Area(map.transform(-9, 50), map.transform(-5, 54), 2))

        // North right castle ground level
        arenas.add(Area(map.transform(7, 36), map.transform(12, 41)))
        arenas.add(Area(map.transform(7, 46), map.transform(12, 51)))
        arenas.add(Area(map.transform(17, 36), map.transform(22, 41)))
        arenas.add(Area(map.transform(17, 46), map.transform(22, 51)))

        // North right castle second floor
        arenas.add(Area(map.transform(5, 35), map.transform(10, 40), 1))
        arenas.add(Area(map.transform(5, 47), map.transform(10, 52), 1))
        arenas.add(Area(map.transform(12, 27), map.transform(17, 32), 1))
        arenas.add(Area(map.transform(12, 35), map.transform(17, 40), 1))
        arenas.add(Area(map.transform(12, 47), map.transform(17, 52), 1))
        arenas.add(Area(map.transform(12, 55), map.transform(17, 60), 1))
        arenas.add(Area(map.transform(19, 35), map.transform(24, 40), 1))
        arenas.add(Area(map.transform(19, 47), map.transform(24, 52), 1))

        // North right third floor
        arenas.add(Area(map.transform(4, 33), map.transform(8, 37), 2))
        arenas.add(Area(map.transform(4, 38), map.transform(8, 42), 2))
        arenas.add(Area(map.transform(4, 45), map.transform(8, 49), 2))
        arenas.add(Area(map.transform(4, 50), map.transform(8, 54), 2))
        arenas.add(Area(map.transform(9, 33), map.transform(13, 37), 2))
        arenas.add(Area(map.transform(9, 38), map.transform(13, 42), 2))
        arenas.add(Area(map.transform(9, 45), map.transform(13, 49), 2))
        arenas.add(Area(map.transform(9, 50), map.transform(13, 54), 2))
        arenas.add(Area(map.transform(16, 33), map.transform(20, 37), 2))
        arenas.add(Area(map.transform(16, 38), map.transform(20, 42), 2))
        arenas.add(Area(map.transform(16, 45), map.transform(20, 49), 2))
        arenas.add(Area(map.transform(16, 50), map.transform(20, 54), 2))
        arenas.add(Area(map.transform(21, 33), map.transform(25, 37), 2))
        arenas.add(Area(map.transform(21, 38), map.transform(25, 42), 2))
        arenas.add(Area(map.transform(21, 45), map.transform(25, 49), 2))
        arenas.add(Area(map.transform(21, 50), map.transform(25, 54), 2))

        // South ruins
        arenas.add(Area(map.transform(-19, 2), map.transform(-13, 8)))
        arenas.add(Area(map.transform(-9, 12), map.transform(-3, 18)))
        arenas.add(Area(map.transform(2, 12), map.transform(8, 18)))
        arenas.add(Area(map.transform(-8, 1), map.transform(-1, 7)))
        arenas.add(Area(map.transform(0, 1), map.transform(7, 7)))
        arenas.add(Area(map.transform(12, 2), map.transform(18, 8)))

        // South east ground level
        arenas.add(Area(map.transform(36, 7), map.transform(41, 12)))
        arenas.add(Area(map.transform(36, 17), map.transform(41, 22)))
        arenas.add(Area(map.transform(46, 7), map.transform(51, 12)))
        arenas.add(Area(map.transform(46, 17), map.transform(51, 22)))

        // South east castle second floor
        arenas.add(Area(map.transform(35, 5), map.transform(40, 10), 1))
        arenas.add(Area(map.transform(35, 12), map.transform(40, 17), 1))
        arenas.add(Area(map.transform(35, 19), map.transform(40, 24), 1))
        arenas.add(Area(map.transform(27, 12), map.transform(32, 17), 1))
        arenas.add(Area(map.transform(47, 5), map.transform(52, 10), 1))
        arenas.add(Area(map.transform(47, 12), map.transform(52, 17), 1))
        arenas.add(Area(map.transform(47, 19), map.transform(52, 24), 1))
        arenas.add(Area(map.transform(55, 12), map.transform(60, 17), 1))

        // South east castle third floor
        arenas.add(Area(map.transform(33, 4), map.transform(37, 8), 2))
        arenas.add(Area(map.transform(33, 9), map.transform(37, 13), 2))
        arenas.add(Area(map.transform(33, 16), map.transform(37, 20), 2))
        arenas.add(Area(map.transform(33, 21), map.transform(37, 25), 2))
        arenas.add(Area(map.transform(38, 4), map.transform(42, 8), 2))
        arenas.add(Area(map.transform(38, 9), map.transform(42, 13), 2))
        arenas.add(Area(map.transform(38, 16), map.transform(42, 20), 2))
        arenas.add(Area(map.transform(38, 21), map.transform(42, 25), 2))
        arenas.add(Area(map.transform(45, 4), map.transform(49, 8), 2))
        arenas.add(Area(map.transform(45, 9), map.transform(49, 13), 2))
        arenas.add(Area(map.transform(45, 16), map.transform(49, 20), 2))
        arenas.add(Area(map.transform(45, 21), map.transform(49, 25), 2))
        arenas.add(Area(map.transform(50, 4), map.transform(54, 8), 2))
        arenas.add(Area(map.transform(50, 9), map.transform(54, 13), 2))
        arenas.add(Area(map.transform(50, 16), map.transform(54, 20), 2))
        arenas.add(Area(map.transform(50, 21), map.transform(54, 25), 2))

        // South east ruins
        arenas.add(Area(map.transform(29, 39), map.transform(35, 45)))
        arenas.add(Area(map.transform(39, 29), map.transform(45, 35)))
        arenas.add(Area(map.transform(39, 49), map.transform(45, 55)))
        arenas.add(Area(map.transform(49, 39), map.transform(55, 45)))

        TournamentConfig.ARENAS = arenas
        TournamentConfig.FINAL_ARENA = Area(map.transform(-4, -14), map.transform(3, -7))
    }

    private fun allocateMap(world: World, maps: IntArray): Area {
        val singleMap = maps.first()
        val copyX = singleMap.shr(8) * 64
        val copyZ = singleMap.and(0xFF) * 64

        var size = 64
        var endX = copyX + 64
        var endZ = copyZ + 64

        if (maps.size > 1) {
            size = 128

            val lastRegion = maps.sorted().last()
            val lastX = lastRegion.shr(8) * 64
            val lastZ = lastRegion.and(0xFF) * 64

            endX = lastX + 64
            endZ = lastZ + 64
        }

        val allocated = world.allocator().allocate(size, size, TournamentConfig.LEAVE_AREA.randomTile()).get()
        allocated.setDangerous(false).setMulti(false).persist()
        allocated.setIdentifier(InstancedMapIdentifier.PVP_TOURNAMENT)
        allocated.set(0, 0, Tile(copyX, copyZ), Tile(endX, endZ), 0, true)

        return allocated
    }

}