package nl.bartpelle.veteres.content.areas.tutorialisland

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 11/18/2016.
 */
object IronmanGuides {
	
	val GUIDES = intArrayOf(311, 317, 3369)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (npc in GUIDES) {
			r.onNpcOption1(npc) @Suspendable {
				dialogue(npc, it)
			}
			r.onNpcOption2(npc) @Suspendable {
				claimArmour(npc, it)
			}
		}
		
		// Ironman interface options
		r.onButton(215, 27) { toggle(it.player, IronMode.NONE) }
		r.onButton(215, 15) { toggle(it.player, IronMode.REGULAR) }
		r.onButton(215, 23) { toggle(it.player, IronMode.ULTIMATE) }
		r.onButton(215, 19) { toggle(it.player, IronMode.HARDCORE) }
	}
	
	fun toggle(player: Player, mode: IronMode) {
		if (player in TutorialIsland) {
			player.ironMode(mode)
			player.varps().varbit(Varbit.IRON_MODE, mode.ordinal)
		}
	}
	
	@Suspendable fun dialogue(npc: Int, it: Script) {
		when (it.player.ironMode()) {
			IronMode.NONE -> it.chatNpc("Hello, ${it.player.name()}. We're the Iron Man tutors.<br>What can we do for you?", npc, 568)
			IronMode.REGULAR -> it.chatNpc("Hail, Iron Man!<br>What can we do for you?", npc, 568)
			IronMode.ULTIMATE -> it.chatNpc("Hail, Ultimate Iron Man!<br>What can we do for you?", npc, 568)
			IronMode.HARDCORE -> it.chatNpc("Hail, Hardcore Iron Man!<br>What can we do for you?", npc, 568)
		}
		
		when (it.options("Tell me about Iron Men.", "I'd like to change my Iron Man mode.", "Have you any armour for me, please?", "I'm fine, thanks.")) {
			1 -> {
				it.chatPlayer("Tell me about Iron Men.", 554)
				it.chatNpc("When you play as an <col=7f0000>Iron Man</col>, you do everything<br>for yourself. " +
						"You don't trade with other players, or take<br>their items, or accept their help.", npc, 590)
				it.chatNpc("As an <col=7f0000>Iron Man</col>, you choose to have these restrictions<br>imposed on you, " +
						"so everyone knows you're doing it<br>properly.", npc, 569)
				it.chatNpc("If you think you have what it takes, you can choose to<br>become a " +
						"<col=b10000>Hardcore Iron Man</col>.", npc, 611)
				it.chatNpc("In addition to the standard restrictions, <col=b10000>Hardcore Iron<br><col=b10000>Men</col> " +
						"only have <col=b10000>one life</col>. In the event of a dangerous<br>death, your <col=b10000>" +
						"Hardcore Iron Men</col> status will be<br>downgraded to that of a <col=7f0000>standard Iron Man</col>, and your", npc, 595)
				it.chatNpc("stats will be removed from the <col=b10000>Hardcore Iron Man</col> hiscores.", npc, 592)
				it.chatNpc("For the ultimate challenge, you can choose to become<br>an <col=00007f>Ultimate Iron Man</col>, " +
						"a game mode inspired by the<br>player <col=00007f>IronNoBank</col>.", npc, 612)
				it.chatNpc("In addition to the standard restrictions, <col=00007f>Ultimate Iron<br><col=00007f>Men</col> " +
						"are blocked from using the bank, and they drop all<br>their items when they die.", npc, 594)
				
				if (it.player in TutorialIsland) {
					it.chatNpc("While you're on Tutorial Island, you can switch freely<br>between being a " +
							"<col=7f0000>standard Iron Man</col>, an <col=00007f>Ultimate Iron<br><col=00007f>Man</col>, " +
							"a <col=b10000>Hardcore Iron Man</col> or a normal player.", npc, 590)
					it.chatNpc("Once you've left this island, you'll be able to find us in<br>Lumbridge, but we'll only " +
							"let you switch your<br>restrictions downwards, not upwards.", npc, 594)
					it.chatNpc("So we will let <col=b10000>Hardcore Iron Men</col> or <col=00007f>Ultimate Iron<br>" +
							"<col=00007f>Men</col> downgrade to <col=7f0000>standard Iron Men</col>, and we'll let all<br>" +
							"types of <col=7f0000>Iron Man</col> become normal players.", npc, 590)
				}
			}
			2 -> {
				it.chatPlayer("I'd like to change my Iron Man mode.", 554)
				
				if (it.player in TutorialIsland) {
					it.player.interfaces().sendMain(215)
				} else {
					if (it.player.ironMode() == IronMode.NONE) {
						it.chatNpc("You cannot become an Iron Man after leaving Tutorial Island, sorry.", npc, 554)
					} else {
						it.chatNpc("To have your Iron Man status revoked, please contact customer support.", npc, 554)
					}
				}
			}
			3 -> {
				it.chatPlayer("Have you any armour for me, please?", 554)
				claimArmour(npc, it)
			}
			4 -> it.chatPlayer("I'm fine, thanks.", 562)
		}
	}
	
	@Suspendable fun claimArmour(npc: Int, it: Script) {
		if (it.player.ironMode() == IronMode.NONE) {
			it.chatNpc("You're not an Iron Man.<br>Our armour is only for them.", npc, 611)
			return
		}
		
		if (it.player in TutorialIsland) {
			it.chatNpc("We'll give you your armour once you're off this island.<br>Come and see us in Lumbridge.", npc, 554)
			return
		}
		
		when (it.player.ironMode()) {
			IronMode.REGULAR -> giveSet(it, npc, 12810, 12811, 12812)
			IronMode.ULTIMATE -> giveSet(it, npc, 12813, 12814, 12815)
			IronMode.HARDCORE -> giveSet(it, npc, 20792, 20794, 20796)
		}
	}
	
	@Suspendable fun giveSet(it: Script, npc: Int, vararg parts: Int) {
		var haveAllParts = true
		for (part in parts) {
			if (!it.player.inventory().contains(part) && !it.player.equipment().contains(part) && !it.player.bank().contains(part)) {
				it.player.inventory().addOrDrop(Item(part), it.player)
				haveAllParts = false
			}
		}
		
		if (haveAllParts) {
			it.chatNpc("I think you've already got the whole set", npc, 567)
			return
		}
		
		it.chatNpc("There you go. Wear it with pride.", npc, 567)
	}
	
}