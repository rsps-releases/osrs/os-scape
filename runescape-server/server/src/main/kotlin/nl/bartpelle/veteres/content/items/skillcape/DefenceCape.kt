package nl.bartpelle.veteres.content.items.skillcape

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Jason MacKeigan on 2016-07-04.
 */
object DefenceCape : CapeOfCompletionPerk(intArrayOf(1, 2, 3)) {
	
	override fun option(option: Int): Function1<Script, Unit> = {
		val player = it.player()
		
		when (option) {
			1 -> {
				CapeOfCompletion.boost(Skills.DEFENCE, player)
			}
			2 -> {
				val state = player.attribOr<Int>(AttributeKey.DEFENCE_PERK_TOGGLE, 0)
				
				player.putattrib(AttributeKey.DEFENCE_PERK_TOGGLE, if (state == 0) 1 else 0)
				player.message("The Ring of Life effect is now toggled ${if (state == 0) "on" else "off"}.")
			}
			3 -> {
				player.message("This feature is not available at this current point in time.")
			}
		}
	}
	
}