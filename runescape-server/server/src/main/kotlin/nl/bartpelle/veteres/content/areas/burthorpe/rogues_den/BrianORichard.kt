package nl.bartpelle.veteres.content.areas.burthorpe.rogues_den

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/19/2016.
 */

object BrianORichard {
	
	val BRIAN_O_RICHARD = 3189
	val MYSTIC_JEWEL = 5561
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(BRIAN_O_RICHARD) @Suspendable {
			val DIALOGUE_STARTED = it.player().attribOr<Int>(AttributeKey.BRIAN_O_RICHARD_DIALOGUE, 0)
			if (DIALOGUE_STARTED == 0) {
				it.chatNpc("Hi there, looking for a challenge are you?", BRIAN_O_RICHARD, 567)
				when (it.options("Yes actually, what've you got?", "What is this place?", "No thanks.")) {
					1 -> what_have_you_got(it)
					2 -> what_is_this_place(it)
					3 -> no_thanks(it)
				}
			} else {
				it.chatNpc("Hi again, what can I do for you?", BRIAN_O_RICHARD, 567)
				when (it.options("I want to try the maze again!", "What is this place?", "Nothing thanks")) {
					1 -> {
						it.chatPlayer("I want to try the maze again!", 567)
						if (it.player().inventory().contains(Item(MYSTIC_JEWEL))) {
							it.chatNpc("Great, well you've already got a jewel so away you go!", BRIAN_O_RICHARD, 567)
						} else {
							if (it.player().inventory().add(Item(MYSTIC_JEWEL, 1), false).success()) {
								it.chatNpc("Certainly, here's another jewel, good luck!", BRIAN_O_RICHARD, 567)
							} else {
								it.chatNpc("You need more inventory space before I can give you a jewel!", BRIAN_O_RICHARD, 612)
							}
						}
					}
					2 -> what_is_this_place(it)
					3 -> nothing_thanks(it)
				}
			}
		}
	}
	
	@Suspendable fun what_have_you_got(it: Script) {
		it.chatPlayer("Yes actually, what've you got?", 554)
		
		//Does our player have at least 50 agility?
		if (it.player().skills().level(Skills.AGILITY) < 50) {
			it.chatNpc("Shame, I don't think I have anything for you. Train up<br>your Agility skill to at least 50 and I might be able to<br>help you out.", BRIAN_O_RICHARD, 612)
		} else if (it.player().skills().level(Skills.THIEVING) < 50) {
			it.chatNpc("Shame, I don't think I have anything for you. Train up<br>your Thieving skill to at least 50 and I might be able to<br>help you out.", BRIAN_O_RICHARD, 612)
		} else {
			it.player().putattrib(AttributeKey.BRIAN_O_RICHARD_DIALOGUE, 1)
			it.chatNpc("Aha, I have the perfect thing for you! See if you can<br>get to the centre of my maze," +
					" the further you get the<br>greater the rewards. There's even some special prizes if<br>you make it right to the end.", BRIAN_O_RICHARD, 570)
			when (it.options("Ok that sounds good!", "What is this place?", "Actually I think I'll pass thanks.")) {
				1 -> {
					it.chatPlayer("Ok that sounds good!", 567)
					it.chatNpc("Great! You should take this jewel with you, it'll allow<br>you to get out of the maze at any time. However that's" +
							"<br>all you're allowed to take in with you, no cheating!", BRIAN_O_RICHARD, 569)
					it.chatNpc("Oh one last thing, if you happen to see my harmonica<br>I'd really like to have it back.", BRIAN_O_RICHARD, 593)
				}
				2 -> what_is_this_place(it)
				3 -> I_think_ill_pass(it)
			}
		}
	}
	
	@Suspendable fun what_is_this_place(it: Script) {
		it.chatPlayer("What is this place?", 554)
		it.chatNpc("Ah welcome to my humble home, well actually it belongs<br>to mummsie but she's getting on a bit so I look after" +
				"<br>the place for her.", BRIAN_O_RICHARD, 569)
		it.chatNpc("So are you interested in a challenge?", BRIAN_O_RICHARD, 567)
		when (it.options("Yes actually, what've you got?", "No thanks.")) {
			1 -> what_have_you_got(it)
			2 -> no_thanks(it)
		}
		
	}
	
	@Suspendable fun I_think_ill_pass(it: Script) {
		it.chatPlayer("Actually I think I'll pass thanks.", 588)
	}
	
	@Suspendable fun nothing_thanks(it: Script) {
		it.chatPlayer("Nothing thanks.", 588)
	}
	
	@Suspendable fun no_thanks(it: Script) {
		it.chatPlayer("No thanks.", 588)
	}
}
