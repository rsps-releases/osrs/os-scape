package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/20/2016.
 */

object BasementLadder {
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(10042) @Suspendable { Ladders.ladderDown(it, Tile(2907, 9968), true) }
		r.onObject(9742) @Suspendable { Ladders.ladderUp(it, Tile(2834, 3542), true) }
	}
}
