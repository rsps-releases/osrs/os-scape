package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 13/12/2015.
 */

object MilkCow {
	
	val COW = 8689
	val GILLIE_GROATS = 4628
	val BUCKET = 1925
	val BUCKET_OF_MILK = 1927
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		repo.onItemOnObject(COW) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			if (item == BUCKET) {
				milk_cow(it)
			} else {
				it.message("The cow doesn't want that.")
			}
		}
		
		repo.onObject(COW) @Suspendable {
			val opt = it.interactionOption()
			if (opt == 1) {
				milk_cow(it)
			} else {
				steal_cowbell(it)
			}
		}
	}
	
	@Suspendable fun milk_cow(it: Script) {
		if (it.player().inventory().remove(Item(BUCKET), false).success()) {
			it.player().lock()
			it.player().sound(372, 1, 0)
			it.animate(2305)
			it.delay(2)
			it.player().inventory().add(Item(BUCKET_OF_MILK), true)
			it.player().message("You milk the cow.")
			it.player().unlock()
		} else {
			it.chatNpc("Tee hee! You've never milked a cow before, have you?", GILLIE_GROATS, 605)
			it.chatPlayer("Erm... No. How could you tell?", 554)
			it.chatNpc("Because you're spilling milk all over the floor. What a<br>waste! You need something to hold the milk.", GILLIE_GROATS, 606)
			it.chatPlayer("Ah yes, I really should have guessed that one, shouldn't<br>I?", 589)
			it.chatNpc("You're from the city, aren't you... Try it again with an<br>empty bucket.", GILLIE_GROATS, 606)
			it.chatPlayer("Right, I'll do that.", 588)
		}
	}
	
	@Suspendable fun steal_cowbell(it: Script) {
		if (it.player().world().rollDie(3, 1)) {
			it.player().stun(5, false)
			it.player().sound(521, 1, 20)
			it.player().sound(2727, 1, 0)
			it.player().forceMove(ForceMovement(0, 0, 0, -1, 30, 90, FaceDirection.EAST))
			it.player().animate(734, 15)
			it.delay(3)
			it.player().teleport(it.player().tile().x, it.player().tile().z - 1)
			it.delay(1)
			it.player().message("The cow kicks you and stuns you!")
		} else {
			it.player().lock()
			it.animate(832)
			it.player().sound(2581, 1, 0)
			it.message("You steal a cowbell.")
			it.player().inventory().add(Item(10593), false)
			it.addXp(Skills.THIEVING, 16.0)
			it.player().unlock()
		}
	}
}
