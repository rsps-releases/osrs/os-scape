package nl.bartpelle.veteres.content.skills.slayer.master

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.content.skills.slayer.Slayer
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature
import nl.bartpelle.veteres.content.skills.slayer.SlayerRewards
import nl.bartpelle.veteres.content.skills.slayer.showSlayerRewards
import nl.bartpelle.veteres.content.slayerTaskAmount
import nl.bartpelle.veteres.content.slayerTaskId
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 11/11/2015.
 */
object Turael {
	
	val TURAEL = 401
	
	fun dialogueOptions(player: Player): Array<String> {
		val talked: Boolean = player.attribOr<Int>(AttributeKey.SLAYER_TASK_AMT, 0) > 0 || player.skills().xp()[Skills.SLAYER] > 0
		
		if (talked) {
			return arrayOf("I need another assignment.", "Have you any rewards for me, or anything to trade?",
					"Let's talk about the difficulty of my assignments.", "Er... Nothing...")
		} else {
			return arrayOf("Who are you?", "Have you any rewards for me, or anything to trade?", "Er... Nothing...")
		}
	}
	
	fun assignTask(player: Player) {
		val def = Slayer.master(401)!!.randomTask(player)
		player.varps().varbit(Varbit.SLAYER_MASTER, Slayer.TURAEL_ID)
		player.putattrib(AttributeKey.SLAYER_TASK_ID, def.creatureUid)
		
		var task_amt = player.world().random(def.range()).toDouble()
		SlayerRewards.multipliable.forEach { creature ->
			if (creature.first == def.creatureUid) {
				if (player.varps().varbit(creature.second) == 1) {
					task_amt *= 1.4
				}
			}
		}

		player.putattrib(AttributeKey.SLAYER_TASK_AMT, task_amt.toInt())
		Slayer.displayCurrentAssignment(player)
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(TURAEL, s@ @Suspendable {
			it.chatNpc("'Ello, and what are you after then?", TURAEL, 554)
			
			when (it.optionsS("Select an Option", *dialogueOptions(it.player()))) {
				"Who are you?" -> {
					it.chatPlayer("Who are you?", 554)
					it.chatNpc("I'm one of the elite Slayer Masters.", TURAEL, 567)
					
					when (it.optionsS("Select an Option", "What's a slayer?", "Never heard of you...")) {
						"What's a slayer?" -> {
							it.chatPlayer("What's a slayer?", 554)
							it.chatNpc("Oh dear, what do they teach you in school?", TURAEL, 575)
							it.chatPlayer("Well... er...", 610)
							it.chatNpc("I suppose I'll have to educate you then. A slayer is<br>someone who is trained " +
									"to fight specific creatures. They<br>know these creatures' every weakness and strength. " +
									"As<br>you can guess it makes killing them a lot easier.", TURAEL, 570)
							
							when (it.optionsS("Select an Option", "Wow, can you teach me?", "Sounds useless to me.")) {
								"Wow, can you teach me?" -> {
									it.chatPlayer("Wow, can you teach me?", 567)
									it.chatNpc("Hmmm well I'm not so sure...", TURAEL, 592)
									it.chatPlayer("Pleeeaasssse!", 571)
									it.chatNpc("Oh okay then, you twisted my arm. You'll have to train<br>against specific groups of creatures.", TURAEL, 568)
									it.chatPlayer("Okay, what's first?", 554)
									
									// Prepare your anus
									it.player().inventory() += Item(4155)
									assignTask(it.player())
									
									val task: SlayerCreature = SlayerCreature.lookup(it.slayerTaskId())!!
									val num: Int = it.slayerTaskAmount()
									it.chatNpc("We'll start you off hunting ${Slayer.taskName(it.player(), task.uid)}, you'll need to kill $num<br>of them.", TURAEL, 589)
								}
								"Sounds useless to me." -> {
									it.chatPlayer("Sounds useless to me.")
									it.chatNpc("Suit yourself.", TURAEL, 614)
								}
							}
						}
						
						"Never heard of you..." -> {
							it.chatPlayer("Never heard of you...", 575)
							it.chatNpc("That's because my foe never lives to tell of me. We<br>slayers are a dangerous bunch.", TURAEL, 593)
						}
					}
				}
				
				"Have you any rewards for me, or anything to trade?" -> {
					it.chatPlayer("Have you any rewards for me, or anything to trade?", 554)
					it.chatNpc("I have quite a few rewards you can earn, and a wide<br>variety of Slayer equipment for sale.", TURAEL, 568)
					
					when (it.optionsS("Select an Option", "Look at rewards.", "Look at shop.")) {
						"Look at rewards." -> it.player().interfaces().showSlayerRewards()
						"Look at shop." -> it.player().world().shop(10).display(it.player())
					}
				}
				
				"Er... Nothing..." -> {
					it.chatPlayer("Er... Nothing...")
				}
				
				"I need another assignment." -> {
					it.chatPlayer("I need another assignment.")
					
					// Tell the player if we can go Mazchna-mode
					if (it.player().skills().combatLevel() >= 20) {
						it.chatNpc("You're actually very strong, are you sure you don't<br>want Mazchna in Morytania to assign you a task?", TURAEL, 589)
						
						when (it.options("No that's okay, I'll take a task from you.", "Oh okay then, I'll go talk to Mazchna.")) {
							1 -> {
								it.chatPlayer("No that's okay, I'll take a task from you.", 567)
							}
							2 -> {
								it.chatPlayer("Oh okay then, I'll go talk to Mazchna.", 567)
								return@s
							}
						}
					}
					
					// Time to check our task state. Can we hand out?
					val numleft: Int = it.slayerTaskAmount()
					if (numleft > 0) {
						it.chatNpc("You're still hunting something, come back when you've<br>finished your task.", TURAEL, 576)
						return@s
					}
					
					// Give them a task.
					assignTask(it.player())
					
					val task: SlayerCreature = SlayerCreature.lookup(it.slayerTaskId())!!
					val num: Int = it.slayerTaskAmount()
					it.chatNpc("Excellent, you're doing great. Your new task is to kill $num ${Slayer.taskName(it.player(), task.uid)}.", TURAEL, 589)
					
					when (it.options("Got any tips for me?", "Okay, great!")) {
						1 -> {
							it.chatPlayer("Got any tips for me?", 554)
							it.chatNpc(Slayer.tipFor(task), TURAEL)
							it.chatPlayer("Great, thanks!")
						}
						2 -> {
							it.chatPlayer("Okay, great!")
						}
					}
				}
			}
		})
		
		r.onNpcOption2(TURAEL) { giveTask(it) }
		r.onNpcOption3(TURAEL) { it.player().world().shop(10).display(it.player()) } // Slayer store
		r.onNpcOption4(TURAEL) { it.player().interfaces().showSlayerRewards() } // Reward shop
	}
	
	@Suspendable fun giveTask(it: Script) {
		// Tell the player if we can go Mazchna-mode
		if (it.player().skills().combatLevel() >= 20) {
			it.chatNpc("You're actually very strong, are you sure you don't<br>want Mazchna in Morytania to assign you a task?", TURAEL, 589)
			
			when (it.options("No that's okay, I'll take a task from you.", "Oh okay then, I'll go talk to Mazchna.")) {
				1 -> {
					it.chatPlayer("No that's okay, I'll take a task from you.", 567)
				}
				2 -> {
					it.chatPlayer("Oh okay then, I'll go talk to Mazchna.", 567)
					return
				}
			}
		}
		
		// Time to check our task state. Can we hand out?
		val numleft: Int = it.slayerTaskAmount()
		if (numleft > 0) {
			it.chatNpc("You're still hunting something, come back when you've<br>finished your task.", TURAEL, 576)
			return
		}
		
		// Give them a task.
		assignTask(it.player())
		
		val task: SlayerCreature = SlayerCreature.lookup(it.slayerTaskId())!!
		val num: Int = it.slayerTaskAmount()
		it.chatNpc("Excellent, you're doing great. Your new task is to kill $num ${Slayer.taskName(it.player(), task.uid)}.", TURAEL, 589)
		
		when (it.options("Got any tips for me?", "Okay, great!")) {
			1 -> {
				it.chatPlayer("Got any tips for me?", 554)
				it.chatNpc(Slayer.tipFor(task), TURAEL)
				it.chatPlayer("Great, thanks!")
			}
			2 -> {
				it.chatPlayer("Okay, great!")
			}
		}
	}
	
}