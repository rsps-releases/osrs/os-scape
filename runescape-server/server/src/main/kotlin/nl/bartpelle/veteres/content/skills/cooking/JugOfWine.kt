package nl.bartpelle.veteres.content.skills.cooking

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/16/2016.
 */

object JugOfWine {
	
	val JUG_OF_WINE = 1993
	val JUG = 1935
	val GRAPES = 1987
	val JUG_OF_WATER = 1937
	
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnItem(JUG_OF_WATER, GRAPES, s@ @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.message("Why would you want to do this on the PVP world?")
				return@s
			}
			
			var num = it.player().inventory().count(JUG_OF_WATER)
			
			while (num-- > 0) {
				if (JUG_OF_WATER !in it.player().inventory() || GRAPES !in it.player().inventory()) {
					break
				}
				
				it.player().inventory() -= JUG_OF_WATER
				it.player().inventory() -= GRAPES
				it.player().inventory() += JUG_OF_WINE
				
				it.message("You squeeze the grapes into the jug. The wine begins to ferment.")
				it.addXp(Skills.COOKING, 200.0)
				it.delay(2)
				
			}
			
		})
		
		r.onItemOption4(JUG_OF_WATER) {
			if (it.player().inventory().remove(Item(JUG_OF_WATER, 1), false).success()) {
				it.player().inventory() += JUG
				it.message("You empty the contents of the jug on the floor.")
			}
		}
	}
}
