package nl.bartpelle.veteres.content.areas.edgeville.osrune

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-07-16.
 */

object Shops {
	
	val MELEE_SHOP = 6904
	val RANGE_SHOP = 6906
	val MAGIC_SHOP = 6908
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Melee shop options
		r.onNpcOption1(MELEE_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1355).display(it.player())
		}
		r.onNpcOption2(MELEE_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1356).display(it.player())
		}
		
		//Range shop options
		r.onNpcOption1(RANGE_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1357).display(it.player())
		}
		r.onNpcOption2(RANGE_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1358).display(it.player())
		}
		r.onNpcOption3(RANGE_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1359).display(it.player())
		}
		
		//Magic shop options
		r.onNpcOption1(MAGIC_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1360).display(it.player())
		}
		r.onNpcOption2(MAGIC_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1361).display(it.player())
		}
		r.onNpcOption3(MAGIC_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1362).display(it.player())
		}
		r.onNpcOption4(MAGIC_SHOP) @Suspendable {
			if (it.player().world().realm().isOSRune) it.player().world().shop(1363).display(it.player())
		}
	}
}