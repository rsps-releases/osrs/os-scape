package nl.bartpelle.veteres.util

import java.util.*

/**
 * Created by Jonathan on 2/10/2017.
 */
class WeightedCollection<E> {
	
	private val random: Random = Random()
	private val map: NavigableMap<Double, E> = TreeMap()
	private var total = 0.0
	
	fun add(weight: Double, result: E) {
		if (weight <= 0) return
		total += weight
		map.put(total, result)
	}
	
	fun next(): E = map.ceilingEntry(random.nextDouble() * total).value
	
}