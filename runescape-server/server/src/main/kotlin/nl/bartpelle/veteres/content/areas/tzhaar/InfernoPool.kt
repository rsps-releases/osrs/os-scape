package nl.bartpelle.veteres.content.areas.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.blankmessagebox
import nl.bartpelle.veteres.content.mechanics.cutscene.Cutscene
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Mack on 7/21/2017.
 */
object InfernoPool {
	
	val active = true
	
	/**
	 * The varbit we send when a player unlocks access to the inferno pool by sacrificing a fire cape.
	 * @see TzhaarKetKeh dialogues to see how it's applied.
	 */
	const val JUMP_IN_VARBIT = 5646
	
	/**
	 * The object identifier for the inferno pool
	 */
	const val ID = 30352
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(30352) @Suspendable {
			if (!active) {
				it.chatNpc("Sorry, JalYt. Inferno not available right now.", TzhaarKetKeh.CHATTER)
			} else {
				if (it.player().attribOr<Boolean>(AttributeKey.INFERNAL_SACRIFICE, false)) {
					jump(it)
				} else {
					it.chatNpc("Oy! Get away from there, JalYt. We TzHaar do not trust you to go down there.", TzhaarKetKeh.CHATTER)
				}
			}
		}
	}
	
	private val jumpStart = Tile(2496, 5118)
	private val jumpMovement = ForceMovement(0, 0, 0, 5, 70, 125, FaceDirection.NORTH)
	var infernoWaveOffset = 1
	
	/**
	 * Handles jumping into the pool by sending the appropriate animation + force movement combo.
	 */
	@Suspendable private fun jump(script: Script) {
		val session = InfernoSession(infernoWaveOffset, script.player(), script.player().world())
		
		script.player().lock()
		
		if (script.player().tile().x != jumpStart.x || script.player().tile().z != jumpStart.z) {
			script.player().walkTo(jumpStart, PathQueue.StepType.FORCED_WALK)
			script.delay(3)
		}
		
		script.player().animate(6723)
		script.delay(1)
		
		script.player().forceMove(jumpMovement)
		script.delay(2)
		script.blankmessagebox("You jump into the fiery cauldron of The Inferno; your heart is pulsating.")
		script.delay(3)
		script.blankmessagebox("You fall and fall and feel the temperature rising.")
		script.delay(3)
		session.player().interfaces().sendMain(174)
		Cutscene.fadeToBlack(session.player)
		script.player().teleport(Tile(script.player().tile().x, script.player().tile().z + 6))
		script.player().looks().transmog(10000)
		script.blankmessagebox("Your heart is in your throat...")
		Cutscene.fadeToBlack(script.player())
		script.delay(3)
		script.blankmessagebox("You hit the ground in the centre of The Inferno.")
		script.delay(2)
		script.player().interfaces().close(162, 550)
		script.player().looks().transmog(-1)
		
		if (script.player().world().realm().isPVP) infernoWaveOffset = 67
		
		session.start(infernoWaveOffset, false)
	}
}