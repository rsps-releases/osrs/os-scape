package nl.bartpelle.veteres.content.minigames.bounty_hunter

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer

/**
 * Created by Mack on 9/19/2017.
 */
enum class BountyHunterEmblem(val emblemId: Int = 12746, val previous: Int = -1, val next: Int = -1, val reward: Int = 0) {

	NONE(),
	TIER_ONE(12746, -1, 12748, 50_000),
	TIER_TWO(12748, 12746, 12749, 100_000),
	TIER_THREE(12749, 12748, 12750, 200_000),
	TIER_FOUR(12750, 12749, 12751, 400_000),
	TIER_FIVE(12751, 12750, 12752, 750_000),
	TIER_SIX(12752, 12751, 12753, 1_200_000),
	TIER_SEVEN(12753, 12752, 12754, 1_750_000),
	TIER_EIGHT(12754, 12753, 12755, 2_500_000),
	TIER_NINE(12755, 12754, 12756, 3_500_000),
	TIER_TEN(12756, 12755, -1, 5_000_000)
	;
	
	/**
	 * The emblem id.
	 */
	val id = emblemId
	
	/**
	 * The previous emblem id, if needed, to reference for downgrading.
	 */
	val previousId = previous
	
	/**
	 * The next emblem id, if needed, to reference for upgrading.
	 */
	val nextId = next
	
	/**
	 * The reward, in bounty points, the player receives upon exchanging an emblem.
	 */
	val rewardAmount = reward
	
	/**
	 * Supplies the emblem id.
	 */
	fun id(): Int {
		return emblemId
	}
	
	/**
	 * Supplies the previous emblem id.
	 */
	fun previousEmblem(): Int {
		return previousId
	}
	
	/**
	 * Supplies the next emblem id.
	 */
	fun nextEmblem(): Int {
		return nextId
	}
	
	/**
	 * Supplies the bounty point reward.
	 */
	fun reward(): Int {
		return rewardAmount
	}
	
	/**
	 * Resolves the current enum entry and returns an Item object.
	 */
	fun item(): Item {
		return (Item(id()))
	}
	
	companion object {
		
		/**
		 * The highest entry in this enumeration.
		 */
		val max: BountyHunterEmblem = values()[values().size - 1]
		
		/**
		 * The lowest entry in this enumeration.
		 */
		val lowest = values()[0]
		
		/***
		 * A flag checking if the item is an emblem.
		 */
		@JvmStatic fun isEmblem(item: Item): Boolean {
			return (item.id() == BountyHunterEmblem.TIER_ONE.id() ||  item.id() >= BountyHunterEmblem.TIER_TWO.id() && item.id() <= BountyHunterEmblem.TIER_TEN.id())
		}
		
		/**
		 * Resolves the provided item to a bounty hunter emblem type, if possible.
		 */
		@JvmStatic fun forEmblemType(item: Item): BountyHunterEmblem {
			
			for (emblem in values()) {
				if (item.id() == emblem.id()) {
					return emblem
				}
			}
			
			return NONE
		}
		/**
		 * Resolves the best emblem type in the player's inventory.
		 */
		fun resolveBestEmblem(inventory: ItemContainer): BountyHunterEmblem {
			
			var highestType: BountyHunterEmblem = NONE
			
			values().filter { emblem -> emblem.id() != BountyHunterEmblem.TIER_TEN.id() }.forEach { emblem ->
				if (inventory.contains(Item(emblem.id()))) {
					highestType = emblem
				}
			}
			
			return (highestType)
		}
		
		/**
		 * Resolves the next best emblem based on the Bounty Hunter Emblem argument provided and returns a new Item object.
		 */
		fun next(emblem: BountyHunterEmblem): Item {
			if (emblem == max) {
				return Item(max.id())
			}
			
			return (Item(emblem.nextEmblem()))
		}
		
		/**
		 * Resolves the previous emblem id and returns an Item object.
		 */
		@JvmStatic fun previous(emblem: BountyHunterEmblem): Item {
			if (emblem == lowest) {
				return Item(lowest.id())
			}
			
			return (Item(emblem.previousEmblem()))
		}
		
		/**
		 * Gets the total reward amount based on the amount of emblems the player has in their inventory.
		 */
		fun totalReward(inventory: ItemContainer): Int {
			
			var amt = 0
			
			for (emblem in values()) {
				if (inventory.has(emblem.item())) {
					amt += emblem.reward() * (inventory.count(emblem.id()))
				}
			}
			
			return amt
		}
	}
}