package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

object ItemReclaimingNpc {
	
	// This NPC will later on be used to claim items from a web end. Future feature.
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(3876) @Suspendable {
			it.targetNpc().def().name = "Exchange employee"
			it.chatNpc("No time - long list of recipients.. have to work.. Oh, I forgot something.. right, ${it.player().name()}, I'm too busy as you can see!", 3876)
			it.message("This does not seem to be the right time to have a chat.")
		}
	}
	
}