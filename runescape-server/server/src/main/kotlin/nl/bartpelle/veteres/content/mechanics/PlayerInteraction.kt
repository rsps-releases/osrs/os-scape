package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.util.Varbit
import java.lang.ref.WeakReference

/**
 * Created by Bart on 8/23/2015.
 *
 * Note of usage. Since this Script is used by our executedLater engine function, and itself can deal with multiple types of interaction
 * such as player options 1 through to 8, you must specifically identify which option you are dealing with at the time of invocation.
 * If you attack a player but extremely quickly press the spec bar, the special attack action is parsed instantly and this script
 * will have the wrong information (see: attributekey:interaction_option will be the wrong value.)
 */
class PlayerInteraction(val option: Int) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(it: Script) {
		val weakref = it.player().attrib<WeakReference<Player>>(AttributeKey.TARGET) ?: return
		val p2: Player? = weakref.get()
		val player: Player = it.player()
		
		it.onInterrupt { it.player().face(null) }
		
		if (p2 != null && !p2.dead()) {
			
			if (option == MODOP) { // mod ops included
				it.player().face(null)
				it.player().faceTile(p2.tile())
				if (player.attribOr<Boolean>(AttributeKey.MODOPS, false)) {
					player.world().server().scriptExecutor().executeScript(player, ModeratePlayerOptions.script)
				} else if (player.attribOr<Boolean>(AttributeKey.GIFT_PRAYERS_PLAYER_OP, false)) {
					player.world().server().scriptExecutor().executeScript(player, gift_prayer_script)
				}
				return
			}
			val staff = it.player().privilege().eligibleTo(Privilege.MODERATOR) || it.player().helper()
			if (option == TOURNY && staff) { // mod ops included
				player.world().server().scriptRepository().triggerPlayerOption7(player, p2)
				return
			}
			if (staff && option == ITEM_ON_PLAYER) {
				if (player.attribOr<Int>(AttributeKey.ITEM_ID, -1) == 5733) {
					player.world().server().scriptRepository().triggerItemOnPlayer(player, player.attribOr(AttributeKey.ITEM_ID, -1))
					return
				}
			}
			if (option == ATTACK && p2.looks().hidden() != player.looks().hidden()) { // Stop attacking players if we're invis
				return
			}
			if (option == FOLLOW) { // Following doesn't run to the player, then interact. It instantly interacts to start following.
				player.world().server().scriptExecutor().executeScript(player, PlayerFollowing.script)
				return
			}
			// Attack
			if (option == ATTACK) {
				if (player.attribOr(AttributeKey.ATTACK_OP, false)) {
					if (!player.world().realm().isDeadman) {
						// Validate if the player is in a PVP area.
						if (WildernessLevelIndicator.inAttackableArea(player) && !WildernessLevelIndicator.inAttackableArea(p2)) {
							//player attack
							player.message(if (!PVPAreas.inPVPArea(player)) "Your target is not in the Wilderness." else "Your target is in a Safe Zone.")
							return
						} else if (WildernessLevelIndicator.inAttackableArea(p2) && !WildernessLevelIndicator.inAttackableArea(player)) {
							player.message(if (!PVPAreas.inPVPArea(player)) "You are not in the Wilderness." else "You are in a Safe Zone.")
							return
						}
					}
					player.world().server().scriptExecutor().executeScript(player, PlayerCombat.script)
					return
				}
				// off screen or unregistered
				PlayerCombat.unfreeze_when_out_of_range(player)
			}
			if (option == PELT) {
				// off screen or unregistered
				// PlayerCombat.unfreeze_when_out_of_range(player)
				//player.world().server().scriptExecutor().executeScript(player, Snowball.script)
				return
			}
			
			var reachable: Boolean = false
			var targtile: Tile? = null
			var lastTile: Tile = p2.tile()
			var playerTile: Tile = player.tile()
			player.face(p2)
			
			while (true) {
				// Do we need to recalculate the path?
				if (targtile == null || (lastTile.x != p2.tile().x || lastTile.z != p2.tile().z)) {
					reachable = player.walkTo(p2, PathQueue.StepType.REGULAR)
					targtile = player.pathQueue().peekLast()?.toTile() ?: player.tile()
				}
				
				// Set last tile
				lastTile = p2.tile()
				
				// Are we frozen?
				if (player.frozen()) { // Reset the path found above. The lastTile will now be player.tile, and we CAN reach the object.
					player.message("A magical force stops you from moving.")
					player.sound(154)
					player.pathQueue().clear()
				}
				
				// Have we reached our path?
				if (targtile != null && player.touches(p2, reached(player, p2))) {
					if (player.attribOr(AttributeKey.DEBUG, false)) {
						player.message("Interacting with player ${p2.id()}, option ${player.attrib<Int>(AttributeKey.INTERACTION_OPTION)}.")
					}
					
					if (!Staking.not_in_area(player, p2, "You can't interact with them.") && reachable) {
						if (!player.locked()) {
							when (option) {
								ITEM_ON_PLAYER -> player.world().server().scriptRepository().triggerItemOnPlayer(player, player.attribOr(AttributeKey.ITEM_ID, -1))
								CHALLENGE -> player.world().server().scriptRepository().triggerPlayerOption1(player, p2)
								ATTACK -> player.world().server().scriptRepository().triggerPlayerOption2(player, p2)
								FOLLOW -> player.world().server().scriptRepository().triggerPlayerOption3(player, p2)
								TRADE -> player.world().server().scriptRepository().triggerPlayerOption4(player, p2)
								PELT -> player.world().server().scriptRepository().triggerPlayerOption5(player, p2)
								6 -> player.world().server().scriptRepository().triggerPlayerOption6(player, p2)
								7 -> player.world().server().scriptRepository().triggerPlayerOption7(player, p2)
								8 -> player.world().server().scriptRepository().triggerPlayerOption8(player, p2)
							}
						}
					} else if (!player.frozen()) {
						player.message("I can't reach that!")
					}
					player.face(null)
					player.faceTile(lastTile)
					return
				} else if (player.pathQueue().empty()) {
					player.message("I can't reach that!")
					player.face(null)
					player.faceTile(lastTile)
					return
				} else {
					it.delay(1) // Wait a bit, try next tick
				}
			}
		}
	}
	
	val ITEM_ON_PLAYER = -1
	val CHALLENGE = 1
	val ATTACK = 2
	val FOLLOW = 3
	val TRADE = 4
	val PELT = 5
	val MODOP = 6
	val TOURNY = 7
	// 8 not yet used
	
	fun reached(player: Player, target: Entity): Tile {
		val steps = if (player.pathQueue().running()) 2 else 1
		val otherSteps = if (target.pathQueue().running()) 2 else 1
		
		val otherTile = target.pathQueue().peekAfter(otherSteps)?.toTile() ?: target.tile()
		return player.pathQueue().peekAfter(steps - 1)?.toTile() ?: player.tile()
	}
	
	@JvmField val gift_prayer_script: Function1<Script, Unit> = s@ @Suspendable {
		val ref: WeakReference<Entity> = it.player().attrib(AttributeKey.TARGET);
		val player = it.player()
		
		// Return if we have no ref
		if (ref != null && ref.get() != null) {
			var target: Player = ref.get() as Player
			if (!target.ip().equals(player.ip())) {
				player.message("You can only gift accounts you own prayers you have purchased.")
				return@s
			}
			val opts = arrayListOf<String>("", "", "")
			var idx = 0
			if (player.varps().varbit(Varbit.UNLOCK_PRESERVE) == 1)
				opts[idx++] = "Gift Preserve"
			if (player.varps().varbit(Varbit.UNLOCK_AUGURY) == 1)
				opts[idx++] = "Gift Augury"
			if (player.varps().varbit(Varbit.UNLOCK_RIGOUR) == 1)
				opts[idx++] = "Gift Rigour"
			if (idx == 0)
				player.message("You've no raids prayer unlocked to gift.")
			else if (idx == 1) {
				val varbit = when (opts[0]) {
					"Gift Preserve" -> Varbit.UNLOCK_PRESERVE
					"Gift Augury" -> Varbit.UNLOCK_AUGURY
					"Gift Rigour" -> Varbit.UNLOCK_RIGOUR
					else -> -1
				}
				if (varbit != -1) {
					if (target.varps().varbit(varbit) == 1) {
						player.message("<col=FF0000>${target.name()}</col> already has <col=FF0000>${opts[0].split(" ")[1]}</col> unlocked, so you cannot gift it to them.")
						return@s
					}
					it.messagebox("Are you sure you want to gift <col=FF0000>${opts[0].split(" ")[1]}</col> to <col=FF0000>${target.name()}</col>? You'll lose the prayer and have to purchase it again. ${target.name()}" +
							" will unlock the prayer in return.")
					if (it.optionsTitled("${opts[0]} to ${target.name()}?", "Yes.", "Cancel.") == 1) {
						player.varps().varbit(varbit, 0)
						target.varps().varbit(varbit, 1)
						target.message("${player.name()} has gifted you a raids prayer: <col=FF0000>${opts[0].split(" ")[1]}</col>.")
						player.message("You've gifted ${target.name()} with the raids prayer: <col=FF0000>${opts[0].split(" ")[1]}</col>.")
						it.itemBox("You've gifted <col=FF0000>${target.name()}</col> with the raids prayer: <col=FF0000>${opts[0].split(" ")[1]}</col>.", 21034)
					}
				}
			} else if (idx > 1) {
				val choice: String = it.optionsNullableS("Which prayer are you gifting?", opts[0], opts[1], opts[2])
				val varbit = when (choice) {
					"Gift Preserve" -> Varbit.UNLOCK_PRESERVE
					"Gift Augury" -> Varbit.UNLOCK_AUGURY
					"Gift Rigour" -> Varbit.UNLOCK_RIGOUR
					else -> -1
				}
				if (varbit != -1) {
					if (target.varps().varbit(varbit) == 1) {
						player.message("<col=FF0000>${target.name()}</col> already has <col=FF0000>${opts[0].split(" ")[1]}</col> unlocked, so you cannot gift it to them.")
						return@s
					}
					it.messagebox("Are you sure you want to gift <col=FF0000>${choice.split(" ")[1]}</col> to <col=FF0000>${target.name()}</col>? You'll lose the prayer and have to purchase it again. ${target.name()}" +
							" will unlock the prayer in return.")
					if (it.optionsTitled("${choice} to ${target.name()}?", "Yes.", "Cancel.") == 1) {
						player.varps().varbit(varbit, 0)
						target.varps().varbit(varbit, 1)
						target.message("${player.name()} has gifted you a raids prayer: <col=FF0000>${choice.split(" ")[1]}</col>.")
						player.message("You've gifted ${target.name()} with the raids prayer: <col=FF0000>${choice.split(" ")[1]}</col>.")
						it.itemBox("You've gifted <col=FF0000>${target.name()}</col> with the raids prayer: <col=FF0000>${choice.split(" ")[1]}</col>.", 21034)
					}
				}
			}
		}
	}
	
}
