package nl.bartpelle.veteres.content.skills.slayer

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.red
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Tuple
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 12/9/2016.
 */
object SuperiorSlayer {
	
	fun trySpawn(player: Player, taskDef: SlayerCreature, npc: Npc) {
		// Must unlock the option first.
		if (player.varps().varbit(Varbit.BIGGER_AND_BADDER) == 0) {
			return
		}
		
		val superior = getSuperior(taskDef) ?: return
		
		// A superior task cannot spawn another instance of itself.
		if (superior == npc.id()) {
			return
		}
		
		val odds = if (BonusContent.isActive(player, BlessingGroup.FABLE)) 100 else 200
		if (player.world().rollDie(odds, 1)) {
			val boss = Npc(superior, player.world(), npc.tile())
			boss.world().registerNpc(boss)
			boss.respawns(false)
			boss.putattrib(AttributeKey.OWNING_PLAYER, Tuple(player.id(), player))
			boss.timers().register(TimerKey.SUPERIOR_BOSS_DESPAWN, 200)
			
			player.message("A superior foe has appeared...".red())
		}
	}
	
	fun getSuperior(taskDef: SlayerCreature): Int? = when (taskDef) {
		SlayerCreature.ABYSSAL_DEMON -> 7410
		SlayerCreature.CAVE_HORRORS -> 7401
		SlayerCreature.BLOODVELDS -> 7397
		SlayerCreature.DUST_DEVILS -> 7404
		SlayerCreature.CAVE_CRAWLER -> 7389
		SlayerCreature.CRAWLING_HANDS -> 7388
		SlayerCreature.ROCKSLUG -> 7392
		SlayerCreature.DARK_BEASTS -> 7409
		SlayerCreature.NECHRYAEL -> 7411
		SlayerCreature.SMOKE_DEVILS -> 7406
		SlayerCreature.ABERRANT_SPECRES -> 7402
		SlayerCreature.PYREFIEND -> 7394
		SlayerCreature.JELLIES -> 7399
		else -> null
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onTimer(TimerKey.SUPERIOR_BOSS_DESPAWN) {
			val lastHit = System.currentTimeMillis() - it.npc().attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0.toLong())
			if (lastHit > 10000) { // Ten seconds.
				it.npc().world().unregisterNpc(it.npc())
			}
		}
	}
	
}