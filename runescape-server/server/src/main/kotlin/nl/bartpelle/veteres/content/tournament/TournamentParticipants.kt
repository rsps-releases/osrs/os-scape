package nl.bartpelle.veteres.content.tournament

import nl.bartpelle.veteres.model.entity.Player
import java.util.*

object TournamentParticipants {

    var participants = ArrayList<Player>()

    fun addParticipant(player: Player) {
        participants.add(player)
    }

    fun removeParticipant(player: Player) {
        if(participants.contains(player))
            participants.remove(player)
    }

    fun containsParticipant(player: Player): Boolean {
        return participants.contains(player)
    }

    fun size(): Int {
        return participants.size
    }


}