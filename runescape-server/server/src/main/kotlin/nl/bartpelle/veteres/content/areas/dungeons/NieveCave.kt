package nl.bartpelle.veteres.content.areas.dungeons

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/17/2015.
 */

object NieveCave {
	
	val ROOTS = intArrayOf(26720, 26721)
	val MUD_PILE = 26724
	val THERMO_BOSS_AREA = Area(Tile(2344, 9434), Tile(2376, 9462))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(26710) {
			it.player().teleport(2430, 3424)
		}
		r.onObject(26712) {
			val obj = it.interactionObject()
			if (obj.tile().equals(3749, 5849)) {
				it.player().teleport(2436, 9824)
			}
		}
		r.onObject(26711) {
			val obj = it.interactionObject()
			if (obj.tile().equals(2435, 9824)) {
				it.player().teleport(3748, 5849)
			}
		}
		
		for (ROOT in ROOTS) {
			r.onObject(ROOT) @Suspendable {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				it.delay(1)
				it.player().lock()
				it.player().animate(1603)
				if (it.player().tile().z == obj.tile().z) {
					if (it.player().tile().x > obj.tile().x) {
						it.player().forceMove(ForceMovement(0, 0, -2, 0, 20, 50, 3))
						it.delay(2)
						it.player().teleport(obj.tile().x - 1, obj.tile().z)
					} else {
						it.player().forceMove(ForceMovement(0, 0, 2, 0, 20, 50, 1))
						it.delay(2)
						it.player().teleport(obj.tile().x + 1, obj.tile().z)
					}
				} else {
					if (it.player().tile().z > obj.tile().z) {
						it.player().forceMove(ForceMovement(0, 0, 0, -2, 20, 50, 2))
						it.delay(2)
						it.player().teleport(obj.tile().x, obj.tile().z - 1)
					} else {
						it.player().forceMove(ForceMovement(0, 0, 0, 2, 20, 50, 0))
						it.delay(2)
						it.player().teleport(obj.tile().x, obj.tile().z + 1)
					}
				}
				it.player().unlock()
			}
		}
		
		r.onObject(MUD_PILE) @Suspendable {
			if (it.player().skills().xpLevel(Skills.AGILITY) < 73) {
				it.player().message("You need an Agility level of 73 to use this shortcut.")
			} else {
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				it.delay(1)
				it.player().lock()
				it.player().animate(4435, 10)
				if (it.player().tile().z > obj.tile().z) {
					it.player().forceMove(ForceMovement(0, 0, 0, -2, 20, 50, 2))
					it.delay(2)
					it.player().animate(-1)
					it.player().teleport(obj.tile().x, obj.tile().z - 1)
					it.delay(1)
					it.player().pathQueue().interpolate(obj.tile().x, obj.tile().z - 2, PathQueue.StepType.FORCED_WALK)
					it.delay(1)
					it.player().animate(2586, 15)
					it.delay(1)
					it.player().teleport(obj.tile().x, obj.tile().z - 4)
					it.player().animate(2588)
					it.delay(1)
					it.player().animate(-1)
				} else {
					it.player().forceMove(ForceMovement(0, 0, 0, 2, 20, 50, 0))
					it.delay(2)
					it.player().animate(-1)
					it.player().teleport(obj.tile().x, obj.tile().z + 1)
					it.delay(1)
					it.player().pathQueue().interpolate(obj.tile().x, obj.tile().z + 2, PathQueue.StepType.FORCED_WALK)
					it.delay(1)
					it.player().animate(2586, 15)
					it.delay(1)
					it.player().teleport(obj.tile().x, obj.tile().z + 4)
					it.player().animate(2588)
					it.delay(1)
					it.player().animate(-1)
				}
				it.player().unlock()
			}
		}
		
		// Smoke devil entrance and exit
//		r.onObject(154) { it.player().teleport(3748, 5761) }
		r.onObject(534) { it.player().teleport(3088, 3505) }
		
		// Thermonuclear entrance and exit
		r.onObject(535) {
			if (it.interactionOption() == 1) {
				it.player().teleport(2376, 9452)
			} else if (it.interactionOption() == 2) {
				var count = 0
				val pluralOr = if (count == 1) "" else "s"
				it.player().world().players().forEachInAreaKt((THERMO_BOSS_AREA), { count++ })
				it.messagebox("There are currently $count player$pluralOr in the cave.")
			}
		}
		r.onObject(536) { it.player().teleport(2379, 9452) }
	}
	
}