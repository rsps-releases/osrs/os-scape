package nl.bartpelle.veteres.content.skills.herblore

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/29/2015.
 */

object Cleaning {
	
	enum class Herb(val level: Int, val herbName: String, val grimy: Int, val clean: Int, val xp: Double, val AorAn: String) {
		GUAM(1 /* Level 3, but on rs you do druidic ritual */, "guam leaf", 199, 249, 2.5, "a"),
		ARDIGAL(1, "ardigal", 1527, 1528, 2.5, "an"),
		SITO_FOIL(1, "sito foil", 1529, 1530, 2.5, "a"),
		VOLENCIA_MOSS(1, "volencia moss", 1531, 1532, 2.5, "a"),
		ROGUESPURSE(3, "rogue's purse", 1533, 1534, 2.5, "a"),
		SNAKEWEED(3, "snake weed", 1525, 1526, 2.5, "a"),
		MARRENTILL(5, "marrentill", 201, 251, 5.0, "a"),
		TARROMIN(11, "tarromin", 203, 253, 5.0, "a"),
		HARRALANDER(20, "harralander", 205, 255, 6.3, "a"),
		RANNAR(25, "rannar weed", 207, 257, 7.5, "a"),
		TOADFLAX(30, "toadflax", 3049, 2998, 8.0, "a"),
		IRIT(40, "irit leaf", 209, 259, 8.8, "an"),
		AVANTOE(48, "avantoe", 211, 261, 10.0, "an"),
		KWUARM(54, "kwuarm", 213, 263, 11.3, "a"),
		SNAPDRAGON(59, "snapdragon", 3051, 3000, 11.8, "a"),
		CADANTINE(65, "cadantine", 215, 265, 12.5, "a"),
		LANTADYME(67, "lantadyme", 2485, 2481, 13.1, "a"),
		DRAWFWEED(70, "dwarf weed", 217, 267, 13.8, "a"),
		TORSTOL(75, "torstol", 219, 269, 15.0, "a")
	}
	
	@JvmStatic @ScriptMain fun cleanherb(repo: ScriptRepository) {
		Herb.values().forEach { herb -> repo.onItemOption1(herb.grimy) @Suspendable { clean(it, herb) } }
	}
	
	@Suspendable fun clean(script: Script, herb: Herb) {
		//Does the player have the required level?
		if (script.player().skills()[Skills.HERBLORE] < herb.level) {
			script.player().sound(2277, 0)
			script.player().message("You need level ${herb.level} Herblore to clean ${herb.AorAn} ${herb.herbName}.")
			return
		}
		//Replace the grimy with clean, send message, and add experience.
		script.player().inventory().remove(Item(herb.grimy), true, script.player().attribOr(AttributeKey.ITEM_SLOT, 0))
		script.player().inventory().add(Item(herb.clean), true)
		script.player().sound(intArrayOf(3920, 3921).random(), 0)
		script.player().message("You clean the ${herb.herbName}.")
		script.addXp(Skills.HERBLORE, herb.xp)
	}
}
