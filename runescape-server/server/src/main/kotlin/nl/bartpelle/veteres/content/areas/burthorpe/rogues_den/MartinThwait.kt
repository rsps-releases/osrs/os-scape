package nl.bartpelle.veteres.content.areas.burthorpe.rogues_den

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/19/2016.
 */

object MartinThwait {
	
	val MARTIN_THWAIT = 3193
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MARTIN_THWAIT) @Suspendable {
			it.chatNpc("You know it's sometimes funny how things work out, I<br>lose some gold but find an item, or I lose an item and" +
					"<br>find some gold... no-one ever knows what's gone where<br>ya know.", MARTIN_THWAIT, 595)
			when (it.options("Yeah I know what you mean, found anything recently?", "Can you tell me about your cape?", "Okay... I'll be going now.")) {
				1 -> open_shop(it)
				2 -> {
					it.chatPlayer("Can you tell me about your cape?", 554)
					it.chatNpc("Certainly! Skillcapes are a symbol of achievement. Only<br>people who have mastered a skill and reached level 99" +
							"<br>can get their hands on them and gain the benefits they<br>carry.", MARTIN_THWAIT, 570)
					it.chatNpc("The Cape of Thieving provides a nice boost to your<br>chances of pickpocketing when worn. Is there anything<br>" +
							"else I can help you with?", MARTIN_THWAIT, 590)
					when (it.options("Have you found anything recently?", "No thank you.")) {
						1 -> open_shop(it)
						2 -> it.chatPlayer("No thank you.", 588)
					}
				}
				3 -> it.chatPlayer("Okay... I'll be going now.", 571)
			}
		}
		r.onNpcOption2(MARTIN_THWAIT) @Suspendable { open_shop(it) }
	}
	
	@Suspendable fun open_shop(it: Script) {
		//Does our player have at least 50 agility?
		if (it.player().skills().level(Skills.AGILITY) < 50) {
			it.chatNpc("Sorry, mate. Train up your Agility skill to at least<br>50 and I might be able to help you out.", MARTIN_THWAIT, 593)
		} else if (it.player().skills().level(Skills.THIEVING) < 50) {
			it.chatNpc("Sorry, mate. Train up your Thieving skill to at least<br>50 and I might be able to help you out.", MARTIN_THWAIT, 593)
		} else {
			if (!it.player().world().realm().isPVP)
				it.player().world().shop(39).display(it.player())
		}
	}
}
