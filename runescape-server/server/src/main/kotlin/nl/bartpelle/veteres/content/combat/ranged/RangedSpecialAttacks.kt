package nl.bartpelle.veteres.content.combat.ranged

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.combat.melee.MeleeSpecialAttacks
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.mechanics.MultiwayCombat
import nl.bartpelle.veteres.content.mechanics.Venom
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.WeaponType
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.*

/**
 * Created by Bart on 3/12/2016.
 */
object RangedSpecialAttacks {
	
	@Suspendable fun doRangeSpecial(it: Script, player: Player, target: Entity, tileDist: Int): Boolean {
		val weapon = it.player().equipment()[EquipSlot.WEAPON]?.id() ?: -1
		val ammo = it.player().equipment()[EquipSlot.AMMO]?.id() ?: -1
		
		when (weapon) {
			21902 -> { // Dragon crossbow .. chinchompa carbon copy LUL
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 60))
					return false
				val success = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.0)
				var max = CombatFormula.maximumRangedHit(player, target, true, false, ammo) * 1.0
				max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, false)
				val delay = Math.round(Math.floor(32 / 30.0) + (tileDist.toDouble() * (5 * 0.020) / 0.6)).toInt()
				val damage = player.world().random(max.toInt())

				val hit: Hit = target.hitpvp(player, (damage * 1.2).toInt(), delay, CombatStyle.RANGE).graphic(Graphic(157, 100, 0))
				if (!success)
					hit.adjustDmg(0)
				hit.submit().addXp()

				val targets = mutableListOf<Entity>()
				if (target.isPlayer) {
					it.player().world().players().forEachInAreaKt(target.tile().area(1), { t ->
						if (t != target && t.varps().varbit(Varbit.MULTIWAY_AREA) == 1) {
							targets.add(t)
						}
					})
				} else {
					it.player().world().npcs().forEachInAreaKt(target.tile().area(1), { t ->
						if (t != target && MultiwayCombat.includes(t))
							targets.add(t)
					})
				}

				for (targ: Entity in targets) {
					if (targ == target || targ == player) {
						//dont hit us, or the target we've already hit
						continue
					}
					if (targ.isNpc) {
						val n = targ as Npc

						if (n.id() == InfernoContext.PILLAR_INVIS || n.id() == InfernoContext.PILLAR_VISIBLE) {
							continue
						}
					}
					if (!PlayerCombat.canAttack(player, targ)) { // Validate they're in an attackable location
						continue
					}
					val otherHit: Hit = targ.hitpvp(player, player.world().random((damage * 0.8).toInt()), delay, CombatStyle.RANGE)
					if (!success)
						otherHit.adjustDmg(0)
					otherHit.submit().addXp()

					targ.putattrib(AttributeKey.LAST_DAMAGER, player)
					targ.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					player.putattrib(AttributeKey.LAST_TARGET, targ)
					player.message("other")

					// Exact weapon id not required, just needs to be a form of chinchompa.
					PlayerCombat.blockAnim(targ, PlayerCombat.delayForBlock(21902, WeaponType.CROSSBOW, player.tile().distance(targ.tile())))
				}
				targets.clear()
				player.timers()[TimerKey.COMBAT_ATTACK] = PlayerCombat.rangeAttackSpeed(it.player())
				return true
			}
			20849 -> {
				execute_dragon_thrownaxe_special(it, target)
				return true
			}
			19478, 19481 -> {
				//Heavy ballista & light ballista
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 65))
					return false
				
				player.animate(7222)
				player.world().spawnProjectile(player.tile(), target, PlayerCombat.ballistaProjectileFor(ammo), 38, 36, 49, 3 * tileDist, 1, 120)
				
				val success = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.45)
				var max = CombatFormula.maximumRangedHit(player, target, true, false, ammo) * 1.25
				max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						MeleeSpecialAttacks.attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 65 * 10
						return true
					}
				}
				
				var damage = Math.round(player.world().random(max.toInt()).toDouble()).toInt()
				val delay = Math.round(Math.floor(32 / 30.0) + (tileDist.toDouble() * (5 * 0.020) / 0.6)).toInt()
				target.graphic(344, 120, delay * 30)
				
				if (!success) damage = 0 // We missed!
				
				// Note: ballista is a unique weapon that doesn't have PID applied to it.
				val hit: Hit = target.hit(player, damage, delay, false).combatStyle(CombatStyle.RANGE).applyProtection().submit().addXp()
				
				player.timers()[TimerKey.COMBAT_ATTACK] = PlayerCombat.rangeAttackSpeed(it.player())
				return true
			}
			
			12926 -> {
				// Blowpipe
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 50))
					return false
				
				player.world().spawnProjectile(player.tile(), target, 1043, 35, 36, 67, 3 * tileDist, 15, 105)
				
				var max = CombatFormula.maximumRangedHit(player, target, false, false, -1) * 1.5
				max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						MeleeSpecialAttacks.attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 50 * 10
						return true
					}
				}
				var damage = Math.round(player.world().random(max.toInt()).toDouble()).toInt()
				
				val success = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.0)
				val delay = Math.round(Math.floor(32 / 30.0) + (tileDist.toDouble() * (5 * 0.020) / 0.6)).toInt()
				
				if (!success)
					damage = 0 // We missed
				
				val hit: Hit = target.hitpvp(player, damage, delay, CombatStyle.RANGE).submit().addXp()
				if (hit.damage() > 0) {
					player.heal(damage / 2)
					val venom = Venom.attempt(player, target, CombatStyle.RANGE, true)
					if (venom)
						target.venom(player)
				}
				var cb_delay = if (target.isNpc) 3 else 4
				if (EquipmentInfo.attackStyleFor(player) == AttackStyle.RAPID)
					cb_delay -= 1
				player.timers()[TimerKey.COMBAT_ATTACK] = cb_delay
				return true
			}
			
			861, 12788 -> {
				// Msb and msb (i)
				if (!PlayerCombat.takeSpecialEnergy(it.player(), if (weapon == 861) 55 else 50))
					return false
				
				player.graphic(256, 90, 0)
				player.animate(1074)
				
				player.world().spawnProjectile(player.tile(), target, 249, 45, 33, 28, 5 * tileDist, 11, 105)
				player.world().spawnProjectile(player.tile(), target, 249, 45, 33, 42, 5 * tileDist, 11, 105)
				
				var max = CombatFormula.maximumRangedHit(player, target, true, false, -1) * 1.0
				max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						MeleeSpecialAttacks.attackTarget(npc, it.player(), max.toInt(), 1)
						target.hitpvp(player, max.toInt(), 2, CombatStyle.RANGE).submit()
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + if (weapon == 861) 55 else 50 * 10
						return true
					}
				}
				
				var damage1 = player.world().random(max.toInt())
				var damage2 = player.world().random(max.toInt())
				val delay = Math.round(Math.floor(32 / 30.0) + (tileDist.toDouble() * (5 * 0.020) / 0.6)).toInt()
				
				val success1 = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.0)
				val success2 = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.0)
				
				if (!success1) damage1 = 0
				if (!success2) damage2 = 0
				
				val hit1: Hit = target.hitpvp(player, damage1, delay, CombatStyle.RANGE).submit()
				val hit2: Hit = target.hitpvp(player, damage2, delay, CombatStyle.RANGE).submit()
				PlayerCombat.addCombatXp(it.player(), target, hit1.damage() + hit2.damage(), CombatStyle.RANGE)
				
				player.timers()[TimerKey.COMBAT_ATTACK] = PlayerCombat.rangeAttackSpeed(it.player())
				return true
			}
			
			11785 -> {
				// Acb
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 40))
					return false
				
				player.animate(4230)
				player.world().spawnProjectile(player.tile(), target, 301, 35, 35, 45, 10 * tileDist, 5, 64)//semi accuracte from os-rsps not dumped tho
				
				var max = CombatFormula.maximumRangedHit(player, target, true, false, -1) * 1.0
				max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, false)
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						MeleeSpecialAttacks.attackTarget(npc, it.player(), max.toInt(), 1)
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 40 * 10
						return true
					}
				}
				
				var damage = player.world().random(max.toInt())
				val delay = Math.round(Math.floor(32 / 30.0) + (tileDist.toDouble() * (5 * 0.020) / 0.6)).toInt()
				
				val success = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.0)
				if (!success) damage = 0
				val hit: Hit = target.hitpvp(player, damage, delay, CombatStyle.RANGE).submit().addXp()
				
				player.timers()[TimerKey.COMBAT_ATTACK] = PlayerCombat.rangeAttackSpeed(it.player())
				return true
			}
			
			11235, 12765, 12766, 12767, 12768 -> {
				// dark bow dbow
				if (!PlayerCombat.takeSpecialEnergy(it.player(), 60))
					return false
				
				player.animate(426)
				val ammoId = it.player().equipment()[EquipSlot.AMMO]?.id() ?: -1
				var gfx = 1101
				var gfx2 = 1102 //non drag arrow 2nd arrow has another graphic id
				var endgfx = 1103 // small puff
				var min = 5
				var multiplier = 1.3
				
				if (ammoId == 11212) {
					// dragon arrows
					gfx = 1099 // dragon spec
					gfx2 = 1099 // drag again
					endgfx = 1100 // large puff
					min = 8
					multiplier = 1.5
				}
				val dist: Int = player.tile().distance(target.tile())
				val speed1 = 16 + (dist * 5)
				val speed2 = 25 + (dist * 10)
				
				player.world().spawnProjectile(player.tile(), target, gfx, 40, 36, 40, speed1, 5, 11)//no idea of real values
				player.world().spawnProjectile(player.tile(), target, gfx2, 40, 36, 40, speed2, 25, 11)
				
				var max = CombatFormula.maximumRangedHit(player, target, true, false, -1).toDouble()
				max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, false)
				max *= multiplier
				max = Math.min(48.0, max) // Dark bow is capped on 07. With the new onyx equipment, max is calculated to 49.
				var damage1 = player.world().random(max.toInt())
				var damage2 = player.world().random(max.toInt())
				
				if (target.isNpc) {
					val npc = target as Npc
					if (npc.id() == 2668) {
						MeleeSpecialAttacks.attackTarget(npc, it.player(), max.toInt(), 1)
						target.hitpvp(player, max.toInt(), 2, CombatStyle.RANGE).submit()
						it.player().varps()[Varp.SPECIAL_ENERGY] = it.player().varps()[Varp.SPECIAL_ENERGY] + 60 * 10
						return true
					}
				}
				
				val delay = Math.round(Math.floor(32 / 30.0) + (tileDist.toDouble() * (5 * 0.020) / 0.6)).toInt()
				
				val success1 = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.35)
				val success2 = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.35)
				
				if (!success1) damage1 = 0
				if (!success2) damage2 = 0
				
				// Note: Dark bow first hit does have PID applied, but the delay varies (not always delay-1) depending on dist. It's custom.
				val hit1 = target.hit(player, damage1, delay, false).combatStyle(CombatStyle.RANGE).applyProtection().graphic(Graphic(endgfx, 100)).submit()
				
				// Extra delay which the second arrow has
				var extraDelay = 2
				if (dist <= 5)
					extraDelay -= 1
				
				// The second hit is pid adjusted.
				val hit2 = target.hitpvp(player, damage2, extraDelay + delay, CombatStyle.RANGE).graphic(Graphic(endgfx, 100)).block(false).submit()
				
				// Minimum damages depending on arrow type
				if (hit1.damage() < min) {
					hit1.adjustDmg(min)
					hit1.type(Hit.Type.REGULAR)
				}
				if (hit2.damage() < min) {
					hit2.adjustDmg(min)
					hit2.type(Hit.Type.REGULAR)
				}
				PlayerCombat.addCombatXp(it.player(), target, hit1.damage() + hit2.damage(), CombatStyle.RANGE)
				
				player.timers()[TimerKey.COMBAT_ATTACK] = PlayerCombat.rangeAttackSpeed(it.player())
				return true
			}
		}
		
		return false
	}
	
	@JvmStatic fun attackTarget(target: Npc, player: Player, damage: Int, delay: Int) {
		val hit: Hit = target.hit(player, damage, delay, false)
		hit.submit()
		player.timers()[TimerKey.COMBAT_ATTACK] = player.world().equipmentInfo().weaponSpeed(player.equipment()[3].id())
	}
	
	fun execute_dragon_thrownaxe_special(it: Script, target: Entity) {
		val player = it.player()
		
		/*if (PlayerCombat.blockRushing(player, target)) {
			return
		}*/
		
		var is_maxhit_dummy = false
		if (target.isNpc) {
			val npc = target as Npc
			if (npc.id() == 2668) {
				is_maxhit_dummy = true
			}
		}
		
		if (is_maxhit_dummy || PlayerCombat.takeSpecialEnergy(player, 25)) {
			
			val max = CombatFormula.maximumRangedHit(player, target, false, false, -1) * 1.0
			val didhit: Boolean = is_maxhit_dummy || AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.25)
			
			player.interfaces().closeMain()
			if (target.isPlayer) {
				val targ = target as Player
				targ.interfaces().closeMain()
			}
			player.face(target)
			player.animate(7521)
			player.graphic(1317, 120, 0)
			
			val tileDist = player.tile().distance(target.tile())
			player.world().spawnProjectile(player.tile(), target, 1318, 35, 36, 30, 3 * tileDist, 15, 105)
			
			// Make sure they are now classed as in combat.
			target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
			target.putattrib(AttributeKey.LAST_DAMAGER, player)
			player.putattrib(AttributeKey.LAST_ATTACK_TIME, System.currentTimeMillis())
			player.putattrib(AttributeKey.LAST_TARGET, target)
			PlayerCombat.check_spec_and_tele(player, target)
			
			player.timers()[TimerKey.THROWING_AXE_DELAY] = 1
			it.player().timers().addOrSet(TimerKey.COMBAT_ATTACK, 1) // 1 tick delay before another normal melee
			
			val hit: Int = if (is_maxhit_dummy) max.toInt() else player.world().random().nextInt(max.toInt())
			
			// Delay is always 1 tick - just like other hand thrown items, max distance is 4.
			if (didhit) {
				val h: Hit = target.hit(player, hit, 1).combatStyle(CombatStyle.RANGE).applyProtection().addXp()
			} else {
				target.hit(player, 0, 1).combatStyle(CombatStyle.RANGE)
			}
		}
	}
	
}