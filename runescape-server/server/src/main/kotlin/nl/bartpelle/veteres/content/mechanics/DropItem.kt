package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.combat.FoodItems
import nl.bartpelle.veteres.content.combat.Potions
import nl.bartpelle.veteres.content.items.Pouches
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 25/01/2016.
 */

@ScriptMain fun demake(repo: ScriptRepository) {
	repo.onItemOption5(12436, @Suspendable {
		// Fury (or)
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, revert the ornament to a fury and the kit.", "No, I'll keep my ornament.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12436) > 0) {
					it.player().inventory() -= 12436
					it.player().inventory() += 6585
					it.player().inventory() += 12526
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(12807, @Suspendable {
		// Split cosmetic odium into odium + kit
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Odium ward into the shield and the kit.", "No, I'll keep my ward.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12807) > 0) {
					it.player().inventory() -= 12807
					it.player().inventory() += 12802 // Kit
					it.player().inventory() += 11926 // ward
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(12806, @Suspendable {
		// Split cosmetic malediction into malediction + kit
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Malediction ward into the shield and the kit.", "No, I'll keep my ward.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12806) > 0) {
					it.player().inventory() -= 12806
					it.player().inventory() += 12802 // Kit
					it.player().inventory() += 11924 // ward
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(12848, @Suspendable {
		// Split granite clamp
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Clamp into the maul and the kit.", "No, I'll keep my clamp.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12848) > 0) {
					it.player().inventory() -= 12848
					it.player().inventory() += 4153 // maul
					it.player().inventory() += 12849 // clamp
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOnItem(3188, 12774) {
		// Frozen whip
		if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12774) > 0) {
			it.player().inventory() -= 12774
			it.player().inventory() += 4151 // whip
			it.player().inventory() += 12769 // dye
			it.player().message("You clean the whip, and get the whip and the dye in return.")
			// Do some nice graphics
			it.animate(713)
		}
	}
	
	repo.onItemOnItem(3188, 12773) {
		// Lava whip
		if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12773) > 0) {
			it.player().inventory() -= 12773
			it.player().inventory() += 4151 // whip
			it.player().inventory() += 12771 // dye
			it.player().message("You clean the whip, and get the whip and the dye in return.")
			// Do some nice graphics
			it.animate(713)
		}
	}
	
	repo.onItemOnItem(3188, 12766) {
		// Blue dark bow
		if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12766) > 0) {
			it.player().inventory() -= 12766
			it.player().inventory() += 11235 // whip
			it.player().inventory() += 12757 // dye
			it.player().message("You clean the dark bow, and get the bow and the dye in return.")
			// Do some nice graphics
			it.animate(713)
		}
	}
	
	repo.onItemOnItem(3188, 12765) {
		// Green dark bow
		if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12765) > 0) {
			it.player().inventory() -= 12765
			it.player().inventory() += 11235 // whip
			it.player().inventory() += 12759 // dye
			it.player().message("You clean the dark bow, and get the bow and the dye in return.")
			// Do some nice graphics
			it.animate(713)
		}
	}
	
	repo.onItemOnItem(3188, 12767) {
		// Yellow dark bow
		if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12767) > 0) {
			it.player().inventory() -= 12767
			it.player().inventory() += 11235 // whip
			it.player().inventory() += 12761 // dye
			it.player().message("You clean the dark bow, and get the bow and the dye in return.")
			// Do some nice graphics
			it.animate(713)
		}
	}
	
	repo.onItemOnItem(3188, 12768) {
		// White dark bow
		if (it.player().inventory().freeSlots() >= 2 && it.player().inventory().count(12768) > 0) {
			it.player().inventory() -= 12768
			it.player().inventory() += 11235 // whip
			it.player().inventory() += 12763 // dye
			it.player().message("You clean the dark bow, and get the bow and the dye in return.")
			// Do some nice graphics
			it.animate(713)
		}
	}
	
	repo.onItemOption5(10034) {
		it.itemBox("Poor buggers...", 10034)
		//do nothing. don't release.
	}
	
	repo.onItemOption5(10033) {
		it.itemBox("Poor buggers...", 10033)
		//do nothing. don't release.
	}
	
	repo.onItemOption5(19722, @Suspendable {
		// Dragon defender
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Dragon Defender from the ornament kit.", "No, I'll keep my Defender.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(19722) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 19722
					it.player().inventory() += 12954
					it.player().inventory() += 20143
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(20000, @Suspendable {
		// Dragon scimitar
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Dragon Scimitar from the ornament kit.", "No, I'll keep my Scimitar.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(20000) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 20000
					it.player().inventory() += 4587
					it.player().inventory() += 20002
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})

	repo.onItemOption5(22242, @Suspendable {
		// Dragon platebody
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Dragon platebody from the ornament kit.", "No, I'll keep my Dragon platebody.") == 1) {

				// Apply inv change
				if (it.player().inventory().count(22242) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 22242
					it.player().inventory() += 22236
					it.player().inventory() += 21892
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}

			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})

	repo.onItemOption5(22244, @Suspendable {
		// Dragon kiteshield
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Dragon kiteshield from the ornament kit.", "No, I'll keep my Dragon kiteshield.") == 1) {

				// Apply inv change
				if (it.player().inventory().count(22244) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 22244
					it.player().inventory() += 22239
					it.player().inventory() += 21895
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}

			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})

	repo.onItemOption5(22234, @Suspendable {
		// Dragon boots
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Dragon boots from the ornament kit.", "No, I'll keep my Dragon boots.") == 1) {

				// Apply inv change
				if (it.player().inventory().count(22234) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 22234
					it.player().inventory() += 22231
					it.player().inventory() += 11840
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}

			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})

	repo.onItemOption5(12797, @Suspendable {
		// Dragon pickaxe (orn)
		if (it.customOptions("Dismantle and get the item and kit back?", "Yes.", "No.") == 1) {
			
			// Apply inv change
			if (it.player().inventory().count(12797) > 0 && it.player().inventory().freeSlots() > 0) {
				it.player().inventory() -= 12797
				it.player().inventory() += 11920
				it.player().inventory() += 12800
				// Do some nice graphics
				it.animate(713)
			} else {
				it.player().message("You don't have enough room to do that.")
			}
			
		}
	})
	
	repo.onItemOption5(20366, @Suspendable {
		// Amulet of Torture
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Amulet of Torture from the ornament kit.", "No, I'll keep my Amulet of Torture.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(20366) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 20366
					it.player().inventory() += 19553 // tort
					it.player().inventory() += 20062 // kit
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(19720, @Suspendable {
		// Occult Necklace
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Occult Necklace from the ornament kit.", "No, I'll keep my Occult Necklace.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(19720) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 19720
					it.player().inventory() += 12002
					it.player().inventory() += 20065
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})

	repo.onItemOption5(22249, @Suspendable {
		// Necklace of anguish
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Necklace of anguish from the ornament kit.", "No, I'll keep my Necklace of anguish.") == 1) {

				// Apply inv change
				if (it.player().inventory().count(22249) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 22249
					it.player().inventory() += 19547
					it.player().inventory() += 22246
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}

			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(20368, @Suspendable {
		// Armadyl Godsword
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Armadyl Godsword from the ornament kit.", "No, I'll keep my Armadyl Godsword.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(20368) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 20368
					it.player().inventory() += 11802
					it.player().inventory() += 20068
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(20370, @Suspendable {
		// Bandos Godsword
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Bandos Godsword from the ornament kit.", "No, I'll keep my Bandos Godsword.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(20370) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 20370
					it.player().inventory() += 11804
					it.player().inventory() += 20071
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(20372, @Suspendable {
		// Saradomin Godsword
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Saradomin Godsword from the ornament kit.", "No, I'll keep my Saradomin Godsword.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(20372) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 20372
					it.player().inventory() += 11806
					it.player().inventory() += 20074
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
	
	repo.onItemOption5(20374, @Suspendable {
		// Zammy Godsword
		if (it.player().world().realm().isPVP || it.player().world().realm().isOSRune || it.player().world().realm().isRealism) {
			if (it.customOptions("Are you sure you wish to do this?", "Yes, split the Zamorak Godsword from the ornament kit.", "No, I'll keep my Zamorak Godsword.") == 1) {
				
				// Apply inv change
				if (it.player().inventory().count(20374) > 0 && it.player().inventory().freeSlots() > 0) {
					it.player().inventory() -= 20374
					it.player().inventory() += 11808
					it.player().inventory() += 20077
					// Do some nice graphics
					it.animate(713)
				} else {
					it.player().message("You don't have enough room to do that.")
				}
				
			}
		} else {
			it.messagebox("This option is only available on the PvP server.")
		}
	})
}

object DropItem {
	
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val player = it.player()
		val item = it.itemUsed()
		val slot = it.itemUsedSlot()
		val def = item.definition(player.world())
		if (def != null && def!!.ioptions != null && def!!.ioptions[4] != null && def!!.ioptions[4].equals("destroy", ignoreCase = true) || BloodChest.isKey(item.id()) || Pouches.isPouch(item.id())) {
			DropItem.destroyOption(it, player, slot, Pouches.isPouch(item.id()))
		} else {
			DropItem.dropitem(it, player, slot)
		}
	}
	
	@JvmStatic
	@JvmOverloads
	fun destroyOption(it: Script, player: Player, invslot: Int, destroyIt: Boolean = false) {
		player.stopActions(false)
		player.animate(-1) // Reset animation
		
		if (player.jailed()) {
			player.message("Items cannot be destroyed when jailed.")
			return
		}
		
		val item = player.inventory().get(invslot) ?: return
		
		// Blood keys are permanently destroyed.
		if (BloodChest.isKey(item.id())) {
			if (WildernessLevelIndicator.inWilderness(player.tile())) {
				player.message("This item cannot be dropped while you're inside the wilderness.")
				return
			}
			
			player.executeScript { s ->
				if (s.destroyItem("Are you sure you want to break the key?", "Bloody key (" + BloodChest.KEY_NAMES[item.id()]!! + ")", "Breaking the key destroys it. You cannot get it back.", item)) {
					player.inventory().remove(item, true, invslot)
					player.message("As you break the key in two halves, it dissolves between your fingers and turns into dust.")
				}
			}
			return
		}
		
		player.executeScript { s ->
			val msg = if (destroyIt)
				"Destroying this item permanently destroys it. You cannot get it back."
			else
				"It will be dropped to the ground, you can pick it up."
			
			if (s.destroyItem("Are you sure you want to destroy this item?", item.name(player.world()), msg, item)) {
				dropitem(s, player, invslot, destroyIt)
			}
		}
	}
	
	@JvmStatic
	@JvmOverloads
	@Suspendable fun dropitem(it: Script, player: Player, invslot: Int, destroyIt: Boolean = false) {
		
		player.stopActions(false)
		player.animate(-1) // Reset animation
		
		if (player.jailed()) {
			player.message("Items cannot be dropped when jailed.")
			return
		}
		
		val item = player.inventory().get(invslot) ?: return
		
		if (player.varps().varbit(Varbit.DROP_ITEM_WARNING) == 1) {
			val value = item.realPrice(player.world()) * item.amount()
			if (value >= player.varps().varbit(Varbit.DROP_ITEMS_WARNING_VALUE)) {
				it.itemBox("This item is considered valuable! Are you sure you wish to drop it?", item.id(), item.amount())
				when (it.options("Yes, drop it.", "Drop it and don't remind me about this value again.", "No.")) {
					2 -> {
						player.varps().varbit(Varbit.DROP_ITEMS_WARNING_VALUE, 1 + value)
					}
					
					3 -> return
				}
			}
		}
		
		// Blood money items can't be dropped
		if (player.world().realm().isPVP) {
			val arena = player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)
			val value = player.world().prices().getOrElse(item.id(), 0) * item.amount() // I guess that's one way to enforce types l0l
			
			if (value > 0 && !player.privilege().eligibleTo(Privilege.ADMIN)) {
				if (arena) {
					player.message("You cannot drop items worth Blood money in the Duel Arena.")
					return
				} else if (WildernessLevelIndicator.inAttackableArea(player)) {
					if (item.id() == 13307 && item.amount() > 2000) {
						player.message("You cannot drop over 2000 Blood money in the Wilderness.")
						return
					} else if (value > 100 && item.id() != 13307) {
						player.message("You cannot drop items worth over 100 Blood money in the Wilderness.")
						return
					}
				}
			}
		}
		
		player.sound(2739, 0)
		player.inventory().set(invslot, null)
		
		if (!destroyIt) {
			val ownerid = player.id()
			val floor = GroundItem(player.world(), item, player.tile(), ownerid)
			if (WildernessLevelIndicator.inAttackableArea(player)) {
				
				//Ignore all untradables! You shouldn't be allowing untradables to show instantly.
				if (floor.item().noted(player.world()) || floor.item().definition(player.world()).grandexchange) {
					floor.forceBroadcast(true) //Changes in rs https://github.com/OS-Scape/os-scape-server/issues/805
				}
				
				val f = FoodItems.get(item.id())
				if (f != null) {
					floor.forceBroadcast(false)
					floor.hidden() // Set hidden - stop brew dropping.
				}
				val p by lazy { Potions.get(item.id()) }
				if (!floor.isHidden && p != null) {
					floor.forceBroadcast(false)
					floor.hidden() // Set hidden - stop brew dropping.
				}
			}
			if (player.gameTime() < 3000) {
				floor.forceBroadcast(false)
				floor.hidden()
				player.message("<col=D358F7>Any items you drop will not be visible to others until you've played for 30 minutes.")
			}
			if (floor.item().id() == 668) { // Blurite jail ores
				floor.forceBroadcast(false)
				floor.hidden()
			}
			
			player.world().spawnGroundItem(floor)
		}
	}
}