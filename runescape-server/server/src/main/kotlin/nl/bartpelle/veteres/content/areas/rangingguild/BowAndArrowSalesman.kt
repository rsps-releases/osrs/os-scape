package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-11.
 */

object BowAndArrowSalesman {
	
	val BOW_AND_ARROW_SALESMAN = 6060
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(BOW_AND_ARROW_SALESMAN) @Suspendable {
			it.chatPlayer("Hello.")
			it.chatNpc("A fair day, traveller. Would you like to see my wares?", BOW_AND_ARROW_SALESMAN)
			when (it.options("Yes please", "I'd like to ask you about magic crossbows", "No thanks.")) {
				1 -> {
					it.chatPlayer("Yes please.")
					it.player().world().shop(49).display(it.player())
				}
				2 -> {
					it.chatPlayer("I'd like to ask you about magic crossbows.")
					it.chatNpc("Ahh crossbows. Not exactly what I'd call skilled ranging, but I guess dwarven " +
							"engineering is good, what did you want to know?", BOW_AND_ARROW_SALESMAN)
					it.chatPlayer("The dwarves don't work with magic logs, I'm wondering if you know " +
							"how to make a crossbow out of them?")
					it.chatNpc("I don't... however I will look into it when I get spare time, for now " +
							"you'll have to stick with the dwarven way of doing things.", BOW_AND_ARROW_SALESMAN)
					it.chatPlayer("Ok, perhaps one day in the future I'll get to make magic crossbows.")
				}
				3 -> {
					it.chatPlayer("No thanks.")
					it.chatNpc("Okay good day to you.", BOW_AND_ARROW_SALESMAN)
				}
			}
		}
		r.onNpcOption2(BOW_AND_ARROW_SALESMAN) @Suspendable {
			it.player().world().shop(49).display(it.player())
		}
	}
	
}
