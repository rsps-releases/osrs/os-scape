package nl.bartpelle.veteres.content.skills.crafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.craftingInterface
import nl.bartpelle.veteres.content.itemOptions
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Savions
 * @author Situations
 */

enum class armour(val level: Int, val leatherType: type, val cutleather: Int, val leatherName: String, val exp: Double, val required: Int) {
	
	//Leather items
	LEATHER_GLOVES(1, type.LEATHER, 1059, "a pair of leather gloves", 13.8, 1),
	LEATHER_BOOTS(7, type.LEATHER, 1061, "a pair of leather boots", 16.25, 1),
	LEATHER_COWL(9, type.LEATHER, 1167, "a leather cowl", 18.5, 1),
	LEATHER_VAMBRACES(11, type.LEATHER, 1063, "a pair of leather vambraces", 22.0, 1),
	LEATHER_BODY(14, type.LEATHER, 1129, "a leather body", 25.0, 1),
	LEATHER_CHAPS(18, type.LEATHER, 1095, "a pair of leather chaps", 27.0, 1),
	HARD_LEATHER_BODY(28, type.HARD_LEATHER, 1131, "a hard leather body", 35.0, 1),
	SPIKY_VAMBRACES(32, type.SPIKY_VAMBRANCES, 10077, "a pair of spiky vambraces", 6.0, 1),
	COIF(38, type.LEATHER, 1169, "a coif", 37.0, 1),
	STUDDED_BODY(41, type.LEATHER, 1133, "a studded body", 40.0, 1),
	STUDDED_CHAPS(44, type.LEATHER, 1097, "a pair of studded chaps", 42.0, 1),
	
	//Green dragon leather items
	GREEN_VAMBRACES(57, type.GREEN_DRAGONHIDE, 1065, "a pair of green dragonhide vambraces", 62.0, 1),
	GREEN_CHAPS(60, type.GREEN_DRAGONHIDE, 1099, "a pair of green dragonhide chaps", 124.0, 2),
	GREEN_BODY(63, type.GREEN_DRAGONHIDE, 1135, "a green dragonhide body", 186.0, 3),
	
	//Blue dragon leather items
	BLUE_VAMBRACES(66, type.BLUE_DRAGONHIDE, 2487, "a pair of blue dragonhide vambraces", 70.0, 1),
	BLUE_CHAPS(68, type.BLUE_DRAGONHIDE, 2493, "a pair of blue dragonhide chaps", 140.0, 2),
	BLUE_BODY(71, type.BLUE_DRAGONHIDE, 2499, "a blue dragonhide body", 210.0, 3),
	
	//Red dragon leather items
	RED_VAMBRACES(73, type.RED_DRAGONHIDE, 2489, "a pair of red dragonhide vambraces", 78.0, 1),
	RED_CHAPS(75, type.RED_DRAGONHIDE, 2495, "a pair of red dragonhide chaps", 156.0, 2),
	RED_BODY(77, type.RED_DRAGONHIDE, 2501, "a red dragonhide body", 234.0, 3),
	
	//Black dragon leather items
	BLACK_VAMBRACES(79, type.BLACK_DRAGONHIDE, 2491, "a pair of black dragonhide vambraces", 86.0, 1),
	BLACK_CHAPS(82, type.BLACK_DRAGONHIDE, 2497, "a pair of black dragonhide chaps", 172.0, 2),
	BLACK_BODY(84, type.BLACK_DRAGONHIDE, 2503, "a black dragonhide body", 258.0, 3),
	
	//Snakeskin items
	SNAKESKIN_BOOTS(45, type.SNAKESKIN, 6328, "a pair of snakeskin boots", 30.0, 6),
	SNAKESKIN_VAMBRACES(47, type.SNAKESKIN, 6330, "a pair of snakeskin vambraces", 35.0, 8),
	SNAKESKIN_BANDANA(48, type.SNAKESKIN, 6326, "a snakeskin bandana", 45.0, 5),
	SNAKESKIN_CHAPS(51, type.SNAKESKIN, 6324, "a pair of snakeskin chaps", 50.0, 12),
	SNAKESKIN_BODY(53, type.SNAKESKIN, 6322, "a snakeskin body", 55.0, 15),
	
	//Yak hide
	YAKHIDE_LEGS(43, type.YAK_HIDE, 10822, "a pair of yak-hide legs", 32.0, 1),
	YAKHIDE_BODY(46, type.YAK_HIDE, 10823, "a yak-hide body", 32.0, 2),
	
	//Crab armour
	CRAB_HELMET(15, type.CRAB_HELMET, 7539, "a crab helmet", 32.5, 1),
	CRAB_CLAW(15, type.CRAB_CLAW, 7537, "a crab claw", 32.5, 1)
}

enum class type(val leather: Int, val groupName: String, val leatherName: String) {
	
	LEATHER(1741, "leather armour", "leather"),
	HARD_LEATHER(1743, "hard leather armour", "hard leather"),
	SPIKY_VAMBRANCES(10113, "spiky vambraces", "kebbit claws"),
	CRAB_HELMET(7538, "crab helmets", "fresh crab shell"),
	CRAB_CLAW(7536, "crab claws", "fresh crab claw"),
	GREEN_DRAGONHIDE(1745, "green dragon hide armour", "green dragon leather"),
	BLUE_DRAGONHIDE(2505, "blue dragon hide armour", "blue dragon leather"),
	RED_DRAGONHIDE(2507, "red dragon hide armour", "red dragon leather"),
	BLACK_DRAGONHIDE(2509, "black dragon hide armour", "black dragon leather"),
	SNAKESKIN(6289, "snakeskin armour", "snake skin"),
	YAK_HIDE(10818, "yak hide armour", "yak hide")
}

@ScriptMain fun armour(r: ScriptRepository) {
	
	//Needle on thread
	r.onItemOnItem(1733, 1734, { it.chatPlayer("Perhaps I should use the needle with a piece of leather instead.", 589) })
	
	//Leather crafting (1741)
	r.onItemOnItem(1733, 1741, @Suspendable {
		
		//Hardcoded, please softcode me!
		val child = it.craftingInterface()
		when (child) {
			125 -> handleNormalCrafting(it, Item(armour.LEATHER_BODY.cutleather, 1))
			124 -> handleNormalCrafting(it, Item(armour.LEATHER_BODY.cutleather, 5))
			125 -> handleNormalCrafting(it, Item(armour.LEATHER_BODY.cutleather, it.inputInteger("Enter amount:")))
			114 -> handleNormalCrafting(it, Item(armour.LEATHER_BODY.cutleather, 1000))
			
			127 -> handleNormalCrafting(it, Item(armour.LEATHER_GLOVES.cutleather, 1))
			126 -> handleNormalCrafting(it, Item(armour.LEATHER_GLOVES.cutleather, 5))
			116 -> handleNormalCrafting(it, Item(armour.LEATHER_GLOVES.cutleather, it.inputInteger("Enter amount:")))
			108 -> handleNormalCrafting(it, Item(armour.LEATHER_GLOVES.cutleather, 1000))
			
			129 -> handleNormalCrafting(it, Item(armour.LEATHER_BOOTS.cutleather, 1))
			128 -> handleNormalCrafting(it, Item(armour.LEATHER_BOOTS.cutleather, 5))
			117 -> handleNormalCrafting(it, Item(armour.LEATHER_BOOTS.cutleather, it.inputInteger("Enter amount:")))
			109 -> handleNormalCrafting(it, Item(armour.LEATHER_BOOTS.cutleather, 1000))
			
			131 -> handleNormalCrafting(it, Item(armour.LEATHER_VAMBRACES.cutleather, 1))
			130 -> handleNormalCrafting(it, Item(armour.LEATHER_VAMBRACES.cutleather, 5))
			118 -> handleNormalCrafting(it, Item(armour.LEATHER_VAMBRACES.cutleather, it.inputInteger("Enter amount:")))
			110 -> handleNormalCrafting(it, Item(armour.LEATHER_VAMBRACES.cutleather, 1000))
			
			133 -> handleNormalCrafting(it, Item(armour.LEATHER_CHAPS.cutleather, 1))
			132 -> handleNormalCrafting(it, Item(armour.LEATHER_CHAPS.cutleather, 5))
			119 -> handleNormalCrafting(it, Item(armour.LEATHER_CHAPS.cutleather, it.inputInteger("Enter amount:")))
			111 -> handleNormalCrafting(it, Item(armour.LEATHER_CHAPS.cutleather, 1000))
			
			135 -> handleNormalCrafting(it, Item(armour.COIF.cutleather, 1))
			134 -> handleNormalCrafting(it, Item(armour.COIF.cutleather, 5))
			120 -> handleNormalCrafting(it, Item(armour.COIF.cutleather, it.inputInteger("Enter amount:")))
			112 -> handleNormalCrafting(it, Item(armour.COIF.cutleather, 1000))
			
			137 -> handleNormalCrafting(it, Item(armour.LEATHER_COWL.cutleather, 1))
			136 -> handleNormalCrafting(it, Item(armour.LEATHER_COWL.cutleather, 5))
			121 -> handleNormalCrafting(it, Item(armour.LEATHER_COWL.cutleather, it.inputInteger("Enter amount:")))
			113 -> handleNormalCrafting(it, Item(armour.LEATHER_COWL.cutleather, 1000))
		}
	});
	
	//Single option-crafting
	r.onItemOnItem(1733, 1743, { handleSingleOptionCrafting(it.player(), armour.HARD_LEATHER_BODY, it) })
	r.onItemOnItem(1733, 10113, { handleSingleOptionCrafting(it.player(), armour.SPIKY_VAMBRACES, it) })
	r.onItemOnItem(1733, 7538, { handleSingleOptionCrafting(it.player(), armour.CRAB_HELMET, it) })
	r.onItemOnItem(1733, 7536, { handleSingleOptionCrafting(it.player(), armour.CRAB_CLAW, it) })
	
	//Multiple option-crafting
	r.onItemOnItem(1733, 1745, @Suspendable {
		if (checkLevel(it.player(), 57, type.GREEN_DRAGONHIDE, it))
			handleNormalCrafting(it, it.itemOptions(toItem(armour.GREEN_VAMBRACES), toItem(armour.GREEN_CHAPS), toItem(armour.GREEN_BODY)))
	})
	r.onItemOnItem(1733, 2505, @Suspendable {
		if (checkLevel(it.player(), 66, type.BLUE_DRAGONHIDE, it))
			handleNormalCrafting(it, it.itemOptions(toItem(armour.BLUE_VAMBRACES), toItem(armour.BLUE_CHAPS), toItem(armour.BLUE_BODY)))
	})
	r.onItemOnItem(1733, 2507, @Suspendable {
		if (checkLevel(it.player(), 73, type.RED_DRAGONHIDE, it))
			handleNormalCrafting(it, it.itemOptions(toItem(armour.RED_VAMBRACES), toItem(armour.RED_CHAPS), toItem(armour.RED_BODY)))
	})
	r.onItemOnItem(1733, 2509, @Suspendable {
		if (checkLevel(it.player(), 79, type.BLACK_DRAGONHIDE, it))
			handleNormalCrafting(it, it.itemOptions(toItem(armour.BLACK_VAMBRACES), toItem(armour.BLACK_CHAPS), toItem(armour.BLACK_BODY)))
	})
	//r.onItemOnItem(1733, 6289, {
	//  if (checkLevel(it.player(), 45, type.SNAKESKIN, it))
	//    handleNormalCrafting(it, it.itemOptions(toItem(armour.SNAKESKIN_BOOTS), toItem(armour.SNAKESKIN_VAMBRACES), toItem(armour.SNAKESKIN_CHAPS), toItem(armour.SNAKESKIN_BODY)))
	//});
	r.onItemOnItem(1733, 10818, @Suspendable {
		if (checkLevel(it.player(), 43, type.YAK_HIDE, it))
			handleNormalCrafting(it, it.itemOptions(toItem(armour.YAKHIDE_LEGS), toItem(armour.YAKHIDE_BODY)))
	});
	
	// Snakeskin leather crafting
	r.onItemOnItem(1733, 6289, @Suspendable {
		if (checkLevel(it.player(), 45, type.SNAKESKIN, it))
			handleNormalCrafting(it, it.itemOptions(toItem(armour.SNAKESKIN_BODY), toItem(armour.SNAKESKIN_CHAPS), toItem(armour.SNAKESKIN_VAMBRACES), toItem(armour.SNAKESKIN_BANDANA), toItem(armour.SNAKESKIN_BOOTS)))
	})
}

@Suspendable fun handleSingleOptionCrafting(player: Player, arm: armour, it: Script) {
	if (checkLevel(it.player(), arm.level, arm, it))
		handleNormalCrafting(it, if (player.inventory().count(arm.leatherType.leather) < 2) Item(arm.cutleather, 1) else Item(arm.cutleather, it.itemOptions(toItem(arm))))
}

@Suspendable fun checkLevel(player: Player, level: Int, arm: armour, it: Script): Boolean {
	return checkLevel(player, level, arm.leatherName, it)
}

@Suspendable fun checkLevel(player: Player, level: Int, type: type, it: Script): Boolean {
	return checkLevel(player, level, type.groupName, it)
}

@Suspendable fun checkLevel(player: Player, level: Int, name: String, it: Script): Boolean {
	if (player.skills().level(Skills.CRAFTING) < level) {
		it.messagebox("You need a crafting level of " + level + " to make " + name + ".")
		return false
	}
	return true
}

@Suspendable fun toItem(arm: armour): Item {
	return Item(arm.cutleather, 175)
}

@Suspendable fun handleNormalCrafting(it: Script, itemTo: Item) {
	if (itemTo == null)
		return
	if (it.player().world().realm().isPVP) {
		it.messagebox("That's a bit pointless is it not...?")
		return
	}
	it.player().interfaces().closeMain()
	armour.values().forEach { arm ->
		if (arm.cutleather == itemTo.id()) {
			val player = it.player()
			
			//Lets check if the player has the required crafting level for the specific item & boost has not expired yet.
			if (!checkLevel(player, arm.level, arm.leatherType, it))
				return
			
			//Can the player deliver?
			var amount = itemTo.amount()
			val maxAmount = player.inventory().count(arm.leatherType.leather) / arm.required
			if (maxAmount < amount)
				amount = maxAmount
			if (amount < 1) {
				it.messagebox("You need at least " + arm.required + " pieces of "
						+ arm.leatherType.leatherName + " to make " + arm.leatherName + ".")
				return
			}
			while (amount-- > 0) {
				if (player.inventory().count(1734) < 1) {
					it.messagebox("You need some thread to make anything out of " + arm.leatherType.leatherName + ".")
					return
				}
				if (player.inventory().count(1733) < 1) {
					it.messagebox("You need a needle to work with " + arm.leatherType.leatherName + ".")
					return
				}
				if ((amount + 1) % 5 == 0) {
					player.message("You use up a reel of your thread.")
					player.inventory().remove(Item(1734, 1), true)
				}
				if (player.world().random().nextInt(30) == 0) {
					player.message("Your needle broke.")
					player.inventory().remove(Item(1733, 1), true)
				}
				player.inventory().remove(Item(arm.leatherType.leather, arm.required), true)
				player.inventory().add(Item(arm.cutleather), true)
				player.animate(1249)
				player.message("You make ${arm.leatherName}.")
				it.addXp(Skills.CRAFTING, arm.exp)
				it.delay(2)
			}
		}
	}
}