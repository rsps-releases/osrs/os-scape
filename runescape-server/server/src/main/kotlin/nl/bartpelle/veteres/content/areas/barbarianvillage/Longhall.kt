package nl.bartpelle.veteres.content.areas.barbarianvillage

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 12/6/2015.
 */

object Longhall {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		intArrayOf(11620, 11621).forEach { door ->
			r.onObject(door) { openDoor(it) }
		}
		intArrayOf(11624, 11625).forEach { door ->
			r.onObject(door) { closeDoor(it) }
		}
	}
	
	fun openDoor(s: Script) {
		val world = s.player().world()
		
		world.removeObj(MapObj(Tile(3078, 3435), 11621, 0, 1))
		world.removeObj(MapObj(Tile(3079, 3435), 11620, 0, 1))
		world.spawnObj(MapObj(Tile(3078, 3435), 11625, 0, 0))
		world.spawnObj(MapObj(Tile(3079, 3435), 11624, 0, 2))
	}
	
	fun closeDoor(s: Script) {
		val world = s.player().world()
		
		world.removeObj(MapObj(Tile(3078, 3435), 11625, 0, 0))
		world.removeObj(MapObj(Tile(3079, 3435), 11624, 0, 2))
		world.spawnObj(MapObj(Tile(3078, 3435), 11621, 0, 1))
		world.spawnObj(MapObj(Tile(3079, 3435), 11620, 0, 1))
	}
	
}