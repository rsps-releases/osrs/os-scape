package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Mack
 */
object ImbueScroll {

    @JvmStatic val scrollId = 6808

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onItemOption1(scrollId, @Suspendable {
            read(it)
        })
        for (type in ImbueType.values()) {
            sr.onItemOnItem(scrollId, type.baseId(), @Suspendable {
                imbue(it, type.baseId())
            })
        }
    }

    /**
     * Displays information to the player regarding instructions on imbuing an item.
     */
    private fun read(it: Script) {
        QuestGuide.clear(it.player())
        QuestGuide.addQuestGuideHeader(it.player(), "Scroll of Imbuement")

        var line = 4

        QuestGuide.addScrollLine(it.player(), line++, "<u>About:</u>")
        line++
        QuestGuide.addScrollLine(it.player(), line++, "The Scroll of Imbuement can be fused on selective items")
        QuestGuide.addScrollLine(it.player(), line++, "allowing them to absorb the scroll's enchanted power.")
        QuestGuide.addScrollLine(it.player(), line++, "You will receive an upgraded item with enhanced stats")
        QuestGuide.addScrollLine(it.player(), line++, "and or abilities. Upon using this scroll on an item, ")
        QuestGuide.addScrollLine(it.player(), line++, "the scroll will then dissolve and can not be reversed.")
        QuestGuide.addScrollLine(it.player(), line++, "Below is a list of items that can be imbued and what")
        QuestGuide.addScrollLine(it.player(), line++, "they are transformed into.")
        line++
        QuestGuide.addScrollLine(it.player(), line++, "<u><col=FF0000>Scroll of Imbuement Combination List</col></u>")

        for (type in ImbueType.values()) {
            val baseItem = Item(type.baseId())
            val imbuedItem = Item(type.imbuedId())

            QuestGuide.addScrollLine(it.player(), line++, "<shad><col=ffff00>${baseItem.name(it.player().world())}</col></shad><mdl=${baseItem.id().toLong() shl 32 or 0}> -> " +
                    "<shad><col=ffff00>${imbuedItem.name(it.player().world())}</col></shad> <mdl=${imbuedItem.id().toLong() shl 32 or 0}>")
        }

        QuestGuide.open(it.player())
    }

    /**
     * Performs the imbue action on the specified item argument, if possible.
     */
    @Suspendable fun imbue(it: Script, itemUsed: Int) {
        val imbueType = ImbueType.forId(itemUsed)
        val type = imbueType ?: return

        it.doubleItemBox("You are about to upgrade a <col=8B0000>${it.player().world().definitions().get(ItemDefinition::class.java, imbueType.baseId()).name}</col> into a <col=8B0000>${it.player().world().definitions().get(ItemDefinition::class.java, imbueType.imbuedId()).name}</col>. This process is <col=FF0000>PERMANENT</col> and will result in your Imbuement scroll to fade upon use. Do you wish to continue? ", imbueType.baseId(), imbueType.imbuedId())

        when (it.optionsTitled("Continue with Imbue?", "Yes, I wish to continue with the imbuing.", "No, I'd like to keep my scroll.")) {
            1 -> {
                if (it.player().inventory().remove(Item(scrollId, 1), false).failed())  {
                    return
                }

                it.player().inventory().remove(Item(imbueType.baseId()), true)
                it.player().inventory().add(Item(type.imbuedId(), 1))
                it.message("The scroll fades into dust as the imbuing process finishes.")
            }
        }
    }

    enum class ImbueType(baseId: Int, imbuedId: Int) {
        BERSERKER_RING(6737, 11773),
        WARRIORS_RING(6735, 11772),
        ARCHER_RING(6733, 11771),
        SEERS_RING(6731, 11770),
        TYRANNICAL_RING(12603, 12691),
        RING_OF_THE_GODS(12601, 13202),
        TREASONOUS_RING(12605, 12692),
        RING_OF_SUFFERING(19550, 19710),
        RING_OF_SUFFERING_R(20655, 20657),
        GRANITE_RING(21739, 21752),
        /*SLAYER_HELMET(11864, 11865),
        BLACK_SLAYER_HELMET(19639, 19641),
        GREEN_SLAYER_HELMET(19643, 19645),
        RED_SLAYER_HELMET(19647, 19649),
        PURPLE_SLAYER_HELMET(21264, 21266)*/;

        private val base = baseId
        private val imbued = imbuedId

        fun baseId(): Int {
            return base
        }

        fun imbuedId(): Int {
            return imbued
        }

        companion object {

            fun forId(item: Int): ImbueType? {
                for (type in ImbueType.values()) {
                    if (type.base == item) {
                        return type
                    }
                }

                return null
            }
        }
    }
}