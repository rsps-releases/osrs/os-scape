package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsedId
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.text.NumberFormat
import java.util.*

/**
 * Created by Situations on 5/2/2016.
 */

object TournamentSupplies {
	
	/*@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(100, 3) @Suspendable {
			val item = it.itemUsedId()
			
			when (it.player().attrib<Int>(AttributeKey.INTERACTION_OPTION)) {
				1 -> send_pending_purchase(it, item, 1)
				2 -> send_pending_purchase(it, item, 5)
				3 -> send_pending_purchase(it, item, 10)
				4 -> send_pending_purchase(it, item, Math.min(it.inputInteger("Enter amount:"), 10000))
			}
		}
	}*/
	
	// This is populated on server startup using a onWorldInit script.
	val UNPURCHASABLE = ArrayList<Int>()
	
	@Suspendable fun send_pending_purchase(it: Script, item: Int, attempted_amt: Int) {
		var amt = attempted_amt
		if (it.player().locked()) {
			return
		}
		
		if (!it.player().world().realm().isPVP) {
			return
		}
		
		//To ensure our player doesn't purchase an unwanted item. :^ )
		// By casting to entity we skip the close interface call :P -- Nevermind, fucker! Any button click on this interface closes it! FU jagex
		(it.player() as Entity).stopActions(false)
		
		if (it.player().world().prices().containsKey(item)) {
			val purchase = Item(item)
			val ONE_COST: Long = it.player().world().prices()[purchase.unnote(it.player().world()).id()].toLong()
			var cost = ONE_COST * amt.toLong()
			val purchase_name = purchase.definition(it.player().world()).name
			
			// Adjust price/quantity to free slot count if we cannot note them.
			if (amt > 1 && !Item(item).note(it.player().world()).definition(it.player().world()).stackable()) {
				amt = (it.player().inventory().freeSlots() +
						if (cost > 0 && it.player().inventory().count(13307).toLong() - ((it.player().inventory().freeSlots() + 1) * ONE_COST) == 0L) 1 else 0)
				cost = ONE_COST * amt.toLong()
			}
			
			//Check if the item is unobtainable through shops.
			UNPURCHASABLE.forEach { illegal_item ->
				if (illegal_item == item) {
					it.itemBox("You're unable to purchase this item from the supplies shop.", item)
					return
				}
			}
			if (item == 2572) { // Not coded lol
				it.itemBox("The Ring of Wealth cannot be bought at the moment.", item)
				return
			}
			
			//Ensure our player isn't inside the wilderness
			if (!WildernessLevelIndicator.inWilderness(it.player().tile()) && !WildernessLevelIndicator.inAttackableArea(it.player())) {
				//Ensure out player is at Edgeville
				if (it.player().tile().inArea(3044, 3455, 3114, 3524)) {
					if (cost > 0) {
						// Over max int val
						if (cost >= Int.MAX_VALUE) {
							return
						}
						var freeSlots = it.player().inventory().freeSlots()
						if (it.player().inventory().count(13307).toLong() - cost.toLong() == 0L) // We'll be replacing this slot of gp with the item
							freeSlots++
						if (freeSlots == 0 && it.player().inventory().count(Item(item).note(it.player().world()).id()) == 0) {
							it.message("You don't have enough space to buy that.")
							return
						}
						it.itemBox("Are you sure you'd like to purchase x$amt $purchase_name${if (amt > 1) "s" else ""}" +
								" for <col=009933>${NumberFormat.getInstance().format(cost)}</col> Blood Money?", item)
						when (it.options("Yes", "No")) {
							1 -> {
								if (it.player().inventory().remove(Item(13307, cost.toInt()), false).success()) {
									if (amt > 1) {
										it.player().inventory().add(Item(Item(item).note(it.player().world()).id(), amt), false)
									} else {
										it.player().inventory().add(Item(item, amt), false)
									}
								} else {
									it.itemBox("You need <col=ff0000>${NumberFormat.getInstance().format(cost - it.player().inventory().count(13307))}</col> more Blood Money coins" +
											" before purchasing that.", 13307, zoom = amt)
								}
							}
						}
					} else {
						if (it.player().inventory().freeSlots() == 0 && it.player().inventory().count(Item(item).note(it.player().world()).id()) == 0) {
							it.message("You don't have enough inventory space to hold that.")
							return
						}
						if (amt > 1) {
							it.player().inventory().add(Item(Item(item).note(it.player().world()).id(), amt), false)
						} else {
							it.player().inventory().add(Item(item, amt), false)
						}
					}
				} else {
					it.messagebox("You can only purchase items at Edgeville.")
				}
			} else {
				it.messagebox("You can't purchase items inside the wilderness!")
			}
		}
	}
}