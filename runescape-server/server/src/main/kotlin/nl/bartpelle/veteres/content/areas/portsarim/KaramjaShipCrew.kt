package nl.bartpelle.veteres.content.areas.portsarim

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 17/10/2016.
 */
object KaramjaShipCrew {
	
	val CREW = intArrayOf(2519, 2522, 2523)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (npcId in CREW) {
			repo.onNpcOption1(npcId) @Suspendable {
				it.chatNpc("Do you want to go on a trip to Karamja?", npcId)
				it.chatNpc("The trip will cost you 30 coins.", npcId)
				if (it.options("Yes please.", "No, thank you.") == 1) {
					travelToKaramja(it)
				}
			}
			repo.onNpcOption2(npcId) {
				travelToKaramja(it)
			}
		}
	}
	
	@Suspendable private fun travelToKaramja(it: Script) {
		if (!it.player().inventory().remove(Item(995, 30), false).success()) {
			it.message("You do not have enough money for that.")
			return
		}
		it.message("You board the ship and pay 30 coins.")
		it.player().lock()
		it.player().interfaces().sendMain(299)
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 5)
		it.delay(8)
		it.player().interfaces().closeMain()
		it.player().teleport(Tile(2956, 3143, 1))
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 0)
		it.player().unlock()
		it.messagebox("The ship arrives at Karamja.")
	}
}