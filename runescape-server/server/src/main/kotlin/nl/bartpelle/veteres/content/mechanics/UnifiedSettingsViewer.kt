package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.interfaces.LootPanel
import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.util.Varbit

object UnifiedSettingsViewer {

    @JvmStatic
    fun open(it: Script) {
        //it.player().write(InterfaceText(1337, 0, "Settings panel"))
        NpcDropViewer.clear(it)

        val player = it.player()
        val resultMap = mutableMapOf<String, NpcDefinition>()

        if (!player.interfaces().visible(NpcDropViewer.INTERFACE_ID)) {
            player.interfaces().sendMain(NpcDropViewer.INTERFACE_ID)
        }

        NpcDropViewer.updateInterfaceHeader(player, "Click on a setting to toggle its state.")

        player.putattrib(AttributeKey.LAST_INTER_310_USE_ID, 2)

        val on = "<col=04B431>on"
        val off = "<col=FF0000>off"

        val pkskulls = "Toggle kill streak colored skulls <img=26> <img=27> <img=28> <img=29> <img=30> ${if (VarbitAttributes.varbiton(player, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.varbitid)) on else off}"
        val potionToggleDialogue = "${if (player.attribOr<Boolean>(AttributeKey.GIVE_EMPTY_POTION_VIALS, true)) "<col=FF0000>Remove" else "<col=04B431>Keep"} </col>vials after finishing a potion"
        val toggleHunterStatus = "Toggle bounty hunter ${if (player.attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false)) on else off}"
        val toggleBountyRecord = (if (player.varps().varbit(Varbit.BOUNTY_HUNTER_RECORD_OVERLAY_HIDDEN) == 0) "<col=FF0000>Hide" else "<col=04B431>Show") + " </col>bounty hunter record overlay"
        val kdOverlay = "Toggle Kills/Deaths Overlay ${if (it.player().varps().varbitBool(Varbit.KDR_OVERLAY)) on else off}"
        val hpOverlay = "Toggle HP Overlay ${if (player.varps().varbitBool(Varbit.HP_OVERLAY_TOGGLED)) off else on}"
        val bossKc = "Chat filter: Turn boss killcount messages ${if (!VarbitAttributes.varbiton(it.player(), 12)) "<col=04B431>on" else "<col=FF0000>off"}"
        val yell = "Chat filter: Turn yell broadcasts ${if (!VarbitAttributes.varbiton(it.player(), 13)) on else off}"
        val pkbroadcasts = "Chat filter: Turn PK kill streak broadcasts ${if (it.player().attribOr<Boolean>(AttributeKey.YELL_CHANNEL_DISABLED, false)) off else on}"

        var query = ""
        for (i in arrayListOf(pkskulls, potionToggleDialogue, toggleHunterStatus, toggleBountyRecord, kdOverlay, hpOverlay, bossKc, yell, pkbroadcasts, "Open Loot Notifications Sidepanel"))
            query += NpcDropViewer.addRow(i, true)

        NpcDropViewer.refresh(it, query)
    }

    fun clicked(it: Script) {
        val player = it.player()
        when (it.player().attrib<Int>(AttributeKey.BUTTON_SLOT)) {
            0 -> VarbitAttributes.toggle(player, VarbitAttributes.VarbitInfo.KS_SKULLS_HIDDEN.varbitid)
            1 -> player.putattrib(AttributeKey.GIVE_EMPTY_POTION_VIALS, !player.attribOr<Boolean>(AttributeKey.GIVE_EMPTY_POTION_VIALS, true))
            2 -> player.putattrib(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, !player.attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false))
            3 -> player.varps().varbit(Varbit.BOUNTY_HUNTER_RECORD_OVERLAY_HIDDEN, if (player.varps().varbit(Varbit.BOUNTY_HUNTER_RECORD_OVERLAY_HIDDEN) == 1) 0 else 1)
            4 -> player.varps().varbit(Varbit.KDR_OVERLAY, if (player.varps().varbit(Varbit.KDR_OVERLAY) == 1) 0 else 1)
            5 -> player.varps().varbit(Varbit.HP_OVERLAY_TOGGLED, if (player.varps().varbit(Varbit.HP_OVERLAY_TOGGLED) == 1) 0 else 1)
            6 -> VarbitAttributes.toggle(player, 12)
            7 -> VarbitAttributes.toggle(player, 13)
            8 -> player.putattrib(AttributeKey.YELL_CHANNEL_DISABLED, !player.attribOr<Boolean>(AttributeKey.YELL_CHANNEL_DISABLED, false))
            9 -> LootPanel.openLootNotificationPanel(it)
        }
        open(it)
    }
}