package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Hit
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.hitorigin.PoisonOrigin
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Situations on 10/27/2016.
 */

//7095 = spit out spawn

object AbyssalSirePhaseTwo {
	
	fun initPhaseTwo(abyssalSire: Npc) {
		val abyssalSireState = abyssalSire.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.DISORIENTED)
		val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
		
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.SWITCHING_PHASE)
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_TWO)
		
		//If the tentacles are not currently in a state of disorientation..
		if (abyssalSireState != AbyssalSireState.DISORIENTED) {
			abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
				abyssalSire.lockNoDamage()
				
				AbyssalSireTentacles.lockTentacles(abyssalSireTentacles)
				AbyssalSireTentacles.putCombatState(abyssalSireTentacles, AbyssalSireState.DISORIENTED)
				AbyssalSireTentacles.animateTentacles(abyssalSireTentacles, 7112)
				
				s.delay(2)
				
				AbyssalSireTentacles.transmogTentacles(abyssalSireTentacles, 5913)
				
				s.delay(2)
				
				AbyssalSireTentacles.faceSouth(abyssalSireTentacles)
				abyssalSire.unlock()
			})
		}
		
		//Stand up, walk to the center of the chamber and attack the player!
		abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
			abyssalSire.lockNoDamage()
			abyssalSire.animate(4532)
			abyssalSire.sync().transmog(5889)
			
			s.delay(3)
			
			abyssalSire.pathQueue().interpolate(abyssalSire.tile().x, abyssalSire.tile().z - 12)
			
			s.delay(12)
			
			abyssalSire.sync().transmog(5890)
			abyssalSire.walkRadius(40)
			
			abyssalSire.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 40)
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
			
			abyssalSire.unlock()
		})
		
	}
	
	fun phaseTwoAttack(abyssalSire: Npc, targetedPlayer: Entity) {
		val spawningMinion = abyssalSire.attribOr<Boolean>(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, false)
		val spawningFumes = abyssalSire.attribOr<Boolean>(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, false)
		
		//If the Abyssal Sire isn't currently spawning a minion or poisonous fumes, roll to attack!
		if (!spawningFumes && !spawningMinion) {
			
			if (abyssalSire.world().rollDie(5, 1))
				summonSpawn(abyssalSire, targetedPlayer)
			else if (abyssalSire.world().rollDie(6, 1))
				summonPoisonousFumes(abyssalSire, targetedPlayer)
			else if (abyssalSire.world().rollDie(5, 1))
				tentacleMeleeAttack(abyssalSire, targetedPlayer)
			else if (EntityCombat.canAttackMelee(abyssalSire, targetedPlayer, true))
				basicMeleeAttack(abyssalSire, targetedPlayer)
		}
		
		
		//In case our tentacles aren't facing south..
		val abyssalSireTentacles = AbyssalSireTentacles.getTentacles(abyssalSire)
		AbyssalSireTentacles.faceSouth(abyssalSireTentacles)
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSire)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		abyssalSire.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSire, abyssalSire.combatInfo().attackspeed)
	}
	
	
	fun basicMeleeAttack(abyssalSire: Npc, targetedPlayer: Entity) {
		abyssalSire.face(targetedPlayer)
		
		if (EntityCombat.attemptHit(abyssalSire, targetedPlayer, CombatStyle.MELEE))
			targetedPlayer.hit(abyssalSire, EntityCombat.randomHit(abyssalSire))
		else
			targetedPlayer.hit(abyssalSire, 0)
		
		abyssalSire.animate(5366)
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSire)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		abyssalSire.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSire, abyssalSire.combatInfo().attackspeed)
	}
	
	fun tentacleMeleeAttack(abyssalSire: Npc, targetedPlayer: Entity) {
		abyssalSire.face(targetedPlayer)
		
		if (EntityCombat.attemptHit(abyssalSire, targetedPlayer, CombatStyle.MELEE))
			targetedPlayer.hit(abyssalSire, EntityCombat.randomHit(abyssalSire))
		else
			targetedPlayer.hit(abyssalSire, 0)
		
		abyssalSire.animate(5369)
		
		targetedPlayer.putattrib(AttributeKey.LAST_DAMAGER, abyssalSire)
		targetedPlayer.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		abyssalSire.putattrib(AttributeKey.LAST_TARGET, targetedPlayer)
		EntityCombat.putCombatDelay(abyssalSire, abyssalSire.combatInfo().attackspeed)
	}
	
	
	private fun summonPoisonousFumes(abyssalSire: Npc, targetedPlayer: Entity) {
		val targetsTile = targetedPlayer.tile()
		
		abyssalSire.lockDamageOk()
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, true)
		
		abyssalSire.animate(5367)
		abyssalSire.world().tileGraphic(1275, targetsTile, 0, 0)
		
		abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
			s.delay(2)
			
			for (i in 0..3) {
				if (targetedPlayer.tile().inSqRadius(targetsTile, 1)) {
					targetedPlayer.hit(PoisonOrigin(), targetedPlayer.world().random(IntRange(6, 28)))
							.type(Hit.Type.POISON).block(false)
					if (targetedPlayer.world().rollDie(3, 1))
						targetedPlayer.poison(8)
				}
				
				s.delay(1)
			}
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_FUMES, false)
			abyssalSire.unlock()
		})
	}
	
	private fun summonSpawn(abyssalSire: Npc, targetedPlayer: Entity) {
		val targetTile = abyssalSire.world().randomTileAround(targetedPlayer.tile(), 2)
		val tileDistance = abyssalSire.tile().transform(2, 2, 0).distance(targetTile)
		val abyssalSireSpawn = Npc(5916, abyssalSire.world(), targetTile)
		val abyssalSireSpawns = abyssalSire.attribOr<ArrayList<Npc>>(AttributeKey.ABYSSAL_SIRE_SPAWNS, ArrayList<Npc>())
		
		abyssalSire.world().server().scriptExecutor().executeScript(abyssalSire.world(), @Suspendable { s ->
			abyssalSire.lockDamageOk()
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, true)
			
			abyssalSire.animate(7095)
			abyssalSire.world().spawnProjectile(abyssalSire.tile().transform(2, 2), targetTile, 1274, 90, 0, 80, 16 * tileDistance, 14, 5)
			
			s.delay(Math.max(1, (90 + (tileDistance * 16)) / 30))
			abyssalSireSpawn.respawns(false)
			abyssalSireSpawn.walkRadius(40)
			abyssalSireSpawn.putattrib(AttributeKey.ABYSSAL_SIRE_CHALLENGING_PLAYER, targetedPlayer)
			abyssalSireSpawn.world().registerNpc(abyssalSireSpawn.spawnDirection(6))
			abyssalSireSpawn.animate(4523, 5)
			
			s.delay(2)
			abyssalSireSpawn.attack(targetedPlayer)
			abyssalSireSpawns.add(abyssalSireSpawn)
			
			
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNS, abyssalSireSpawns)
			abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWNING_MINION, false)
			abyssalSireSpawn.putattrib(AttributeKey.ABYSSAL_SIRE_SPAWN_OWNER, abyssalSire)
			abyssalSire.unlock()
		})
	}
	
}
