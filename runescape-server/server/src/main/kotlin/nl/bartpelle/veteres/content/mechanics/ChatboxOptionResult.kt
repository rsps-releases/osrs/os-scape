package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 06/06/2016.
 *
 * Type values:
 * 1 = prompt with "tele to someone in wild?" confirmation then graphics (teleto)
 * 2 = instant graphics (teletome)
 */
class ChatboxOptionResult(val targ: Player, val type: Int) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(it: Script) {
		val lvl = WildernessLevelIndicator.wildernessLevel(targ.tile())
		val loc: String = if (lvl == 0) "dangerous PvP" else "level " + lvl + " wilderness"
		val player = it.player()
		if (type == 2) {
			it.clearContext()
			if (player.privilege().eligibleTo(Privilege.ADMIN)) return // We go unnoticed x
			val ot = player.tile().transform(0, 0)
			player.world().tileGraphic(110, ot, 110, 0)
			player.graphic(110, 110, 0)
			targ.graphic(110, 110, 0)
			
			it.delay(1)
			player.world().tileGraphic(110, ot, 110, 0)
			it.delay(1)
			player.graphic(110, 110, 0)
			it.delay(1)
			targ.graphic(110, 110, 0)
			
			it.delay(1)
			player.world().tileGraphic(110, ot, 110, 0)
			it.delay(1)
			player.graphic(110, 110, 0)
			it.delay(1)
			targ.graphic(110, 110, 0)
		} else if (type == 1 && it.options("Tele to " + targ.name() + " in <col=FF0000>" + loc + "</col>.", "NOPE HAHA") == 1) {
			player.teleport(targ.tile())
			if (player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) != targ.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY)) {
				player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, targ.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY))
			}
			if (player.privilege().eligibleTo(Privilege.ADMIN)) return // We go unnoticed x
			it.clearContext()
			val ot = targ.tile().transform(0, 0)
			player.world().tileGraphic(110, ot, 110, 0)
			player.graphic(110, 110, 0)
			
			it.delay(1)
			player.world().tileGraphic(110, ot, 110, 0)
			it.delay(1)
			player.graphic(110, 110, 0)
			
			it.delay(1)
			player.world().tileGraphic(110, ot, 110, 0)
			it.delay(1)
			player.graphic(110, 110, 0)
		}
	}
	
}