package nl.bartpelle.veteres.content.npcs.pets

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI.petType
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Bart on 3/16/2016.
 */
object PetDialogues {
	
	/**
	 * A simple chain of dialogue in some consecutive order.
	 */
	interface DialogueChain {
		
		/**
		 * Executes the dialogue over the script.
		 */
		fun execute(it: Script)
		
	}
	
	/**
	 * Created by Jason MacKeigan on August 24th at 7:27PM
	 *
	 * The purpose of this enumeration is to store all pet dialogue relating
	 * to each individual pet.
	 */
	enum class PetDialogue(val pets: ArrayList<Pet>) : DialogueChain {
		ABYSSAL_ORPHAN(arrayListOf(Pet.ABYSSAL_ORPHAN)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatNpc("You killed my father.", pet.npc)
				if (it.player().world().random(1) == 1) {
					it.chatPlayer("Yeah, don't take it personally.")
					it.chatNpc("In his dying moment, my father poured his last ounce of strength into my creation. My being is formed from his remains.", pet.npc)
					it.chatNpc("When your own body is consumed to nourish the Nexus, and an army of scions arises from your corpse, I trust you will not take it personally either.", pet.npc)
				} else {
					if (it.player().looks().female()) {
						it.chatPlayer("No, I am your father.")
						it.chatNpc("Human biology may be unfamiliar to me, but nevertheless I doubt that very much.", pet.npc)
					} else {
						it.chatPlayer("No, I am your father.")
						it.chatNpc("No you're not.", pet.npc)
					}
				}
			}
		},
		BABY_MOLE(arrayListOf(Pet.BABY_MOLE)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Hey, Mole. How is life above ground?")
				it.chatNpc("Well, the last time I was above ground, I was having to contend with people throwing snow at some weird yellow duck in my park.", pet.npc)
				it.chatPlayer("Why were they doing that?")
				it.chatNpc("No idea, I didn't stop to ask as an angry mob was closing in on them pretty quickly.", pet.npc)
				it.chatPlayer("Sounds awful.")
				it.chatNpc("Anyway, keep Molin'!", pet.npc)
			}
		},
		HELLPUPPY(arrayListOf(Pet.HELLPUPPY)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..5)) {
					1 -> {
						it.chatPlayer("How many souls have you devoured?")
						it.chatNpc("None.", pet.npc)
						it.chatPlayer("Awww p-")
						it.chatNpc("Yet.", pet.npc)
						it.chatPlayer("Oh.")
					}
					2 -> {
						it.chatPlayer("I wonder if I need to invest in a trowel when I take you out for a walk.")
						it.chatNpc("More like a shovel.", pet.npc)
					}
					3 -> {
						it.chatPlayer("Why were the hot dogs shivering?")
						it.chatNpc("Grrrrr...", pet.npc)
						it.chatPlayer("Because they were served-")
						it.chatNpc("GRRRRRR...", pet.npc)
						it.chatPlayer("-with... chilli?")
					}
					4 -> {
						it.chatPlayer("Hell yeah! Such a cute puppy!")
						it.chatNpc("Silence mortal! Or I'll eat your soul.", pet.npc)
						it.chatPlayer("Would that go well with lemon?")
						it.chatNpc("Grrr...", pet.npc)
					}
					5 -> {
						it.chatPlayer("What a cute puppy, how nice to meet you.")
						it.chatNpc("It'd be nice to meat you too...", pet.npc)
						it.chatPlayer("Urk... nice doggy.")
						it.chatNpc("Grrr...", pet.npc)
					}
				}
			}
		},
		KALPHITE_PRINCESS(arrayListOf(Pet.KALPHITE_PRINCESS, Pet.KALPHITE_PRINCESS_2)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("What is it with your kind and potato cactus?")
				it.chatNpc("Truthfully?", pet.npc)
				it.chatPlayer("Yeah, please.")
				it.chatNpc("Soup. We make a fine soup with it.", pet.npc)
				it.chatPlayer("Kalphites can cook?")
				it.chatNpc("Nah, we just collect it and put it there because we know fools like yourself will come down looking for it then inevitably be killed by my mother.", pet.npc)
				it.chatPlayer("Evidently not, that's how I got you!")
				it.chatNpc("Touche", pet.npc) // é charracter doesnt work  https://i.gyazo.com/9101a6377a23608820e407e47b7693fa.png
			}
		},
		ROCK_GOLEM(arrayListOf(Pet.ROCK_GOLEM)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("So you're made entirely of rocks?")
				it.chatNpc("Not quite, my body is formed mostly of minerals.", pet.npc)
				it.chatPlayer("Aren't minerals just rocks?")
				it.chatNpc("No, rocks are rocks, minerals are minerals. I am formed from minerals.", pet.npc)
				it.chatPlayer("But you're a Rock Golem...")
			}
		},
		HERON(arrayListOf(Pet.HERON)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatNpc("Hop inside my mouth if you want to live!", pet.npc)
				it.chatPlayer("I'm not falling for that...I'm not a fish! I've got more foresight than that.", pet.npc)
			}
		},
		BEAVER(arrayListOf(Pet.BEAVER)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("How much wood could a woodchuck chuck if a woodchuck could chuck wood?")
				it.chatNpc("Approximately 32,768 depending on his Woodcutting level.", pet.npc)
			}
		},
		DARK_CORE(arrayListOf(Pet.PET_DARK_CORE)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Got any sigils for me?")
				it.messagebox("The Core shakes its head.")
				it.chatPlayer("Dammit Core-al!")
				it.chatPlayer("Let's bounce!")
			}
		},
		SNAKELING(arrayListOf(Pet.PET_SNAKELING_BLUE)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Hey little snake!")
				it.chatNpc("Soon, Zulrah shall establish dominion over this plane.", pet.npc)
				it.chatPlayer("Wanna play fetch?")
				it.chatNpc("Submit to the almighty Zulrah.", pet.npc)
				it.chatPlayer("Walkies? Or slidies...?")
				it.chatNpc("Zulrah's wilderness as a God will soon be demonstrated.", pet.npc)
				it.chatPlayer("I give up...")
			}
		},
		CHAOS_ELEMENTAL(arrayListOf(Pet.PET_CHAOS_ELEMENTAL)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Is it true a level 3 skiller caught one of your siblings?")
				it.chatNpc("Yes, they killed my mummy, kidnapped my brother, smiled about it and went to sleep.", pet.npc)
				it.chatPlayer("Aww, well you have me now! I shall call you Squishy and you shall be mine and you shall be my Squishy")
				it.chatPlayer("Come on, Squishy come on, little Squishy!")
			}
		},
		PRINCE_BLACK_DRAGON(arrayListOf(Pet.PRINCE_BLACK_DRAGON)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Shouldn't a prince only have two heads?")
				it.chatNpc("Why is that?", pet.npc)
				it.chatPlayer("Well, a standard Black dragon has one, the King has three so inbetween must have two?")
				it.chatNpc("You're overthinking this.", pet.npc)
			}
		},
		CALLISTO(arrayListOf(Pet.CALLISTO_CUB)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Why the grizzly face?")
				it.chatNpc("You're not funny...", pet.npc)
				it.chatPlayer("You should get in the.... sun more.")
				it.chatNpc("You're really not funny...", pet.npc)
				it.chatPlayer("One second, let me take a picture of you with my.... kodiak camera.")
				it.chatNpc(".....", pet.npc)
				it.chatPlayer("Feeling.... blue.")
				it.chatNpc("If you don't stop, I'm going to leave some... brown... at your feet, human.", pet.npc)
			}
		},
		SCORPIAS_OFFSPRING(arrayListOf(Pet.SCORPIAS_OFFSPRING)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("At night time, if I were to hold ultraviolet light over you, would you glow?")
				it.chatNpc("Two things wrong there, human.", pet.npc)
				it.chatPlayer("Oh?")
				it.chatNpc("One, When has it ever been night time here?", pet.npc)
				it.chatNpc("Two, When have you ever seen ultraviolet light around here?", pet.npc)
				it.chatPlayer("Hm...")
				it.chatNpc("In answer to your question though. Yes I, like every scorpion, would glow.", pet.npc)
			}
		},
		VENENATIS_SPIDERLING(arrayListOf(Pet.VENENATIS_SPIDERLING)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("It's a damn good thing I don't have arachnophobia.")
				it.chatNpc("We're misunderstood. Without us in your house, you'd be infested with flies and other REAL nasties.", pet.npc)
				it.chatPlayer("Thanks for that enlightening fact.")
				it.chatNpc("Everybody gets one.", pet.npc)
			}
		},
		TZREK_JAD(arrayListOf(Pet.TZREK_JAD)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..2)) {
					1 -> {
						it.chatPlayer("Do you miss your people?")
						it.chatNpc("Mej-TzTok-Jad Kot-Kl!", pet.npc)
						it.chatPlayer("No.. I don't think so.")
						it.chatNpc("Jal-Zek Kl?", pet.npc)
						it.chatPlayer("No, no, I wouldn't hurt you.")
					}
					2 -> {
						it.chatPlayer("Are you hungry?")
						it.chatNpc("Kl-Kra!", pet.npc)
						it.chatPlayer("Ooookay...")
					}
				}
			}
		},
		DAGANNOTH_PRIME(arrayListOf(Pet.PET_DAGANNOTH_PRIME)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("So despite there being three kings, you're clearly the leader, right?")
				it.chatNpc("Definitely.", pet.npc)
				it.chatPlayer(" I'm glad I got you as a pet.")
				it.chatNpc("Ugh. Human, I'm not a pet.", pet.npc)
				it.chatPlayer("Stop following me then.")
				it.chatNpc("I can't seem to stop.", pet.npc)
				it.chatPlayer("Pet.")
			}
		},
		DAGANNOTH_REX(arrayListOf(Pet.PET_DAGANNOTH_REX)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Do you have any berserker rings?")
				it.chatNpc("Nope.", pet.npc)
				it.chatPlayer("You sure?")
				it.chatNpc("Yes?", pet.npc)
				it.chatPlayer(" So, if I tipped you upside down and shook you, you'd not drop any berserker rings?")
				it.chatNpc("Nope.", pet.npc)
				it.chatPlayer("What if I endlessly killed your father for weeks on end, would I get one then?")
				it.chatNpc("Been done by someone, nope.", pet.npc)
			}
		},
		DAGANNOTH_SUPREME(arrayListOf(Pet.PET_DAGGANOTH_SUPREME)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Hey, so err... I kind of own you now.")
				it.chatNpc("Tsssk. Next time you enter those caves, human, my father will be having words.", pet.npc)
				it.chatPlayer("Maybe next time I'll add your brothers to my collection.")
			}
		},
		GENERAL_GRAARDOR(arrayListOf(Pet.PET_GENERAL_GRAARDOR)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Not sure this is going to be worth my time but... how are you?")
				it.chatNpc("SFudghoigdfpDSOPGnbSOBNfdbdnopbdnopbddfnopdfpofhdA RRRGGGGH", pet.npc)
				it.chatPlayer("Nope. Not worth it.")
			}
		},
		KRIL_TSUTSAROTH(arrayListOf(Pet.PET_KRIL_TSUTSAROTH)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("How's life in the light?")
				it.chatNpc("Burns slightly.", pet.npc)
				it.chatPlayer("You seem much nicer than your father. He's mean.")
				it.chatNpc(" If you were stuck in a very dark cave for centuries you'd be pretty annoyed too.", pet.npc)
				it.chatPlayer("I guess.")
				it.chatNpc("He's actually quite mellow really.", pet.npc)
				it.chatPlayer("Uh.... Yeah.")
			}
		},
		KREE_ARRA(arrayListOf(Pet.PET_KREEARRA)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Huh... that's odd... I thought that would be big news.")
				it.chatNpc("You thought what would be big news?", pet.npc)
				it.chatPlayer("Well there seems to be an absence of a certain ornithological piece: a headline regarding mass awareness of a certain avian variety. ")
				it.chatNpc("What are you talking about?", pet.npc)
				it.chatPlayer("Oh have you not heard? It was my understanding that everyone had heard....")
				it.chatNpc("Heard wha...... OH NO!!!!?!?!!?!", pet.npc)
				it.chatPlayer("OH WELL THE BIRD, BIRD, BIRD, BIRD BIRD IS THE WORD. OH WELL THE BIRD, BIRD, BIRD, BIRD BIRD IS THE WORD.")
				it.messagebox("There's a slight pause as Kree'Arra Jr. goes stiff.")
			}
		},
		PET_KRAKEN(arrayListOf(Pet.PET_KRAKEN)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("What's Kraken?")
				it.chatNpc("Not heard that one before.", pet.npc)
				it.chatPlayer("How are you actually walking on land?")
				it.chatNpc("We have another leg, just below the center of our body that we use to move across solid surfaces.", pet.npc)
				it.chatPlayer("That's.... interesting.")
			}
		},
		PET_PENANCE_QUEEN(arrayListOf(Pet.PET_PENANCE_QUEEN)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Of all the high gamble rewards I could have won, I won you...")
				it.chatNpc("Keep trying, human. You'll never win that Dragon Chainbody.", pet.npc)
			}
		},
		ZILYANA(arrayListOf(Pet.PET_ZILYANA)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("FIND THE GODSWORD!")
				it.chatNpc("FIND THE GODSWORD!", pet.npc)
				it.messagebox("If the player has a Saradomin godsword")
				it.chatPlayer("I FOUND THE GODSWORD!")
				it.chatNpc("GOOD!!!!!", pet.npc)
			}
		},
		SMOKE_DEVIL(arrayListOf(Pet.PET_SMOKE_DEVIL)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Your kind comes in three different sizes?")
				it.chatNpc("Four, actually.", pet.npc)
				it.chatPlayer("Wow. Whoever created you wasn't very creative. You're just resized versions of one another!")
			}
		},
		VETION_JR(arrayListOf(Pet.VETION_JR_PURPLE, Pet.VETION_JR_ORANGE)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatPlayer("Who is the true lord and king of the lands?")
				it.chatNpc("The mighty heir and lord of the Wilderness.", pet.npc)
				it.chatPlayer("Where is he? Why hasn't he lifted your burden?")
				it.chatNpc("I have not fulfilled my purpose.", pet.npc)
				it.chatPlayer("What is your purpose?")
				it.chatNpc("Not what is, what was. A great war tore this land apart and, for my failings in protecting this land, I carry the burden of its waste.", pet.npc)
			}
		},
		BABY_CHINCHOMPA(arrayListOf(Pet.BABY_CHINCHOMPA_BLACK, Pet.BABY_CHINCHOMPA_YELLOW, Pet.BABY_CHINCHOMPA_GREY, Pet.BABY_CHINCHOMPA_RED)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				if (pet == Pet.BABY_CHINCHOMPA_YELLOW)
					it.chatNpc("Squeaka squeaka!", pet.npc)
				else
					it.chatNpc("Squeak squeak!", pet.npc)
			}
		},
		GIANT_SQUIRREL(arrayListOf(Pet.GIANT_SQUIRREL)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..3)) {
					1 -> {
						it.chatPlayer("So how come you are so agile?")
						it.chatNpc("If you were so nutty about nuts, maybe you would understand the great lengths we go to!", pet.npc)
					}
					2 -> {
						it.chatPlayer("What's up with all that squirrel fur? I guess fleas need a home too.")
						it.chatNpc("You're pushing your luck! Stop it or you'll face my squirrely wrath.", pet.npc)
					}
					3 -> {
						it.chatPlayer("Did you ever notice how big squirrels' teeth are?")
						it.chatNpc("No...", pet.npc)
						it.chatPlayer("You could land a gnome glider on those things!")
						it.chatNpc("Watch it, I'll crush your nuts!", pet.npc)
					}
				}
			}
		},
		TANGLEROOT(arrayListOf(Pet.TANGLEROOT)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..3)) {
					1 -> {
						it.chatPlayer("How are you doing today?")
						it.chatNpc("I am Tangleroot!", pet.npc)
					}
					2 -> {
						it.chatPlayer("I am Tangleroot!")
						it.chatNpc("I am " + it.player().name() + "!", pet.npc)
					}
					3 -> {
						it.chatPlayer("Hello there pretty plant.")
						it.chatNpc("I am Tangleroot!", pet.npc)
					}
				}
			}
		},
		ROCKY(arrayListOf(Pet.ROCKY)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..3)) {
					1 -> {
						it.chatPlayer("*Whistles*")
						it.message("You slip your hand into Rocky's pocket")
						it.chatNpc("OY!! You're going to have to do better than that! Sheesh, what an amateur.", pet.npc)
					}
					2 -> {
						it.chatPlayer("Is there much competition between you raccoons and the magpies?")
						it.chatNpc("Magpies have nothing on us! They're just interested in shinies.", pet.npc)
						it.chatNpc("Us raccoons have a finer taste, we can see the value in anything, whether it shines or not.", pet.npc)
					}
					3 -> {
						it.chatPlayer("Hey Rocky, do you want to commit a bank robbery with me?")
						it.chatNpc("If that is the level you are at, I do not wish to participate in criminal acts with you " + it.player().name() + ".", pet.npc)
						it.chatPlayer("Well what are you interested in stealing?")
						it.chatNpc("The heart of a lovely raccoon called Rodney.", pet.npc)
						it.chatPlayer("I cannot really help you there I'm afraid.")
					}
				}
			}
		},
		RIFT_GUARDIAN(arrayListOf(Pet.RIFT_GUARDIAN_FIRE, Pet.RIFT_GUARDIAN_WATER, Pet.RIFT_GUARDIAN_EARTH,
				Pet.RIFT_GUARDIAN_SOUL, Pet.RIFT_GUARDIAN_NATURE, Pet.RIFT_GUARDIAN_MIND, Pet.RIFT_GUARDIAN_AIR,
				Pet.RIFT_GUARDIAN_ASTRAL, Pet.RIFT_GUARDIAN_BLOOD, Pet.RIFT_GUARDIAN_BODY, Pet.RIFT_GUARDIAN_CHAOS,
				Pet.RIFT_GUARDIAN_COSMIC, Pet.RIFT_GUARDIAN_DEATH, Pet.RIFT_GUARDIAN_LAW)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..3)) {
					1 -> {
						it.chatPlayer("Can you see your own rift?")
						it.chatNpc("No. From time to time I feel it shift and change inside me though. It is an odd feeling.", pet.npc)
					}
					2 -> {
						it.chatPlayer("Where would you like me to take you today Rifty?")
						it.chatNpc("Please do not call me that... we are a species of honour " + it.player().name() + ".", pet.npc)
						it.chatPlayer("Sorry.")
					}
					3 -> {
						it.chatPlayer("Hey! What's that!")
						it.messagebox("You quickly poke your hand through the rift guardian's rift.")
						it.chatNpc("Huh, what?! Where?", pet.npc)
						it.chatPlayer("Not the best guardian it seems.")
					}
				}
			}
		},
		BLOODHOUND(arrayListOf(Pet.BLOODHOUND)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..5)) {
					1 -> {
						it.chatPlayer("How come I can talk to you without an amulet?")
						it.chatNpc("*Woof woof bark!* Elementary, it's due to the influence of the -SQUIRREL-!", pet.npc)
					}
					2 -> {
						it.chatPlayer("Walkies!")
						it.chatNpc("...", pet.npc)
					}
					3 -> {
						it.chatPlayer("Can you help me with this clue?")
						it.chatNpc("*Woof! Bark yip woof!* Sure! Eliminate the impossible first.", pet.npc)
						it.chatPlayer("And then?")
						it.chatNpc("*Bark! Woof bark bark.* Whatever is left, however improbable, must be the answer.", pet.npc)
						it.chatPlayer("So helpful.")
					}
					4 -> {
						it.chatPlayer("I wonder if I could sell you to a vampire to track down dinner.")
						it.chatNpc("*Woof bark bark woof* I have teeth too you know, that joke was not funny.", pet.npc)
					}
					5 -> {
						it.chatPlayer("Hey boy, what's up?")
						it.chatNpc("*Woof! Bark bark woof!* You smell funny.", pet.npc)
						it.chatPlayer("Err... funny strange or funny ha ha?")
						it.chatNpc("*Bark bark woof!* You aren't funny.", pet.npc)
					}
				}
			}
		},
		CHOMPY_CHICK(arrayListOf(Pet.CHOMPY_CHICK)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				it.chatNpc2("*Chirp!*")
			}
		},
		PHOENIX(arrayListOf(Pet.PHOENIX)) {
			@Suspendable override fun execute(it: Script) {
				val pet = it.player().pet()?.petType() ?: return
				when (it.player().world().random(1..4)) {
					1 -> {
						it.chatPlayer("So... The Pyromancers, they're cool, right?")
						it.chatNpc2("We share a common goal..")
						it.chatPlayer("Which is?")
						it.chatNpc("Keeping the cinders burning and preventing the long night from swallowing us all.", pet.npc)
						it.chatPlayer("That sounds scary.")
						it.chatNpc("As long as we remain vigilant and praise the Sun, all will be well.", pet.npc)
					}
					2 -> {
						it.chatNpc2("...")
						it.chatPlayer("What are you staring at?")
						it.chatNpc("The great Sol Supra.", pet.npc)
						it.chatPlayer("Is that me?")
						it.chatNpc("No mortal. The Sun, as you would say.", pet.npc)
						it.chatPlayer("Do you worship it?")
						it.chatNpc("It is wonderous... If only I could be so grossly incandescent.", pet.npc)
					}
					3 -> {
						it.chatPlayer("Who's a pretty birdy?")
						it.messagebox("The Phoenix Gives you a smouldering look.")
					}
					4 -> {
						it.chatNpc2("One day I will burn so hot I'll become Sacred Ash")
						it.chatPlayer("Aww, but you're so rare, where would I find another?")
						it.chatNpc("Do not fret mortal, I will rise from the Sacred Ash greater than ever before.", pet.npc)
						it.chatPlayer("So you're immortal?")
						it.chatNpc("As long as the Sun in the sky gives me strength.", pet.npc)
						it.chatPlayer("...Sky?")
					}
				}
			}
		},
		;
	}
	
	@JvmStatic @ScriptMain fun registerDialogue(repository: ScriptRepository) {
		for (dialogue in PetDialogue.values()) {
			for (petchat in dialogue.pets) {
				repository.onNpcOption1(petchat.npc, @Suspendable { dialogue.execute(it) })
			}
		}
	}
	
}