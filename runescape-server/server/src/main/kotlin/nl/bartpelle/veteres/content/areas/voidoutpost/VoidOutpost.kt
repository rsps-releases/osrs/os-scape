package nl.bartpelle.veteres.content.areas.voidoutpost

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 17/10/2016.
 */
object VoidOutpost {
	
	val TRAVEL_SQUIRE = 1769
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onNpcOption1(TRAVEL_SQUIRE) @Suspendable {
			it.chatNpc("Hi, how can I help you?", TRAVEL_SQUIRE)
			if (it.options("I'd like to go back to Port Sarim please.", "I'm fine thanks.") == 1) {
				it.chatPlayer("I'd like to go back to Port Sarim please.")
				it.chatNpc("Ok, but please come back soon and help us.", TRAVEL_SQUIRE)
				travel(it)
			}
		}
		repo.onNpcOption2(TRAVEL_SQUIRE) @Suspendable {
			travel(it)
		}
	}
	
	@Suspendable private fun travel(it: Script) {
		it.player().lock()
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 15)
		it.player().interfaces().sendMain(299)
		it.player().write(InterfaceText(299, 25, "You sail to Port Sarim."))
		it.delay(11)
		it.player().interfaces().closeMain()
		it.player().varps().varp(Varp.SHIP_TRAVEL_PATH, 0) // reset
		it.player().teleport(Tile(3041, 3198, 1))
		it.player().unlock()
		it.messagebox("The ship arrives at Port Sarim.")
	}
}