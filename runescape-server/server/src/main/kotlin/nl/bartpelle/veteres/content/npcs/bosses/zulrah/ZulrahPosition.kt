package nl.bartpelle.veteres.content.npcs.bosses.zulrah

import nl.bartpelle.veteres.model.Tile

/**
 * Created by Bart on 3/6/2016.
 *
 * Represents a possible tile where Zulrah may find itself positioned.
 */
enum class ZulrahPosition(val tile: Tile, val direction: Tile) {
	
	CENTER(Tile(0, 0), Tile(0, -10)),
	
	WEST(Tile(-10, 0), Tile(10, 0)),
	
	SOUTH(Tile(0, -10), Tile(0, 10)),
	
	EAST(Tile(10, 10), Tile(-10, 0))
	
}