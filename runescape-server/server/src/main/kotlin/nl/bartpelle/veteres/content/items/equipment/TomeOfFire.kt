package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.ItemOnItem
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.content.itemUsedSlot
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 08/04/2017.
 * @author Mack - code modifications.
 */
object TomeOfFire {
	
	val TOME = 20714
	val TOME_EMPTY = 20716
	val BURNT_PAGE = 20718
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (tome in intArrayOf(TOME, TOME_EMPTY)) {
			repo.onItemOnItem(tome, BURNT_PAGE, s@ @Suspendable {

				//Don't allow charging on W2
				if (it.player().world().realm().isPVP) return@s

				val tomeSlot = ItemOnItem.slotOf(it, tome)
				if (tome == TOME_EMPTY) {
					it.player().inventory().set(tomeSlot, Item(TOME))
				}
				val tomeItem = it.player().inventory().get(tomeSlot)
				val pageSlot = ItemOnItem.slotOf(it, BURNT_PAGE)
				val pages = it.player().inventory().get(pageSlot)
				val charges = (pages.amount() * 20)
				if (it.player().inventory().remove(pages, false).success()) {
					tomeItem.property(ItemAttrib.CHARGES, charges + tomeItem.property(ItemAttrib.CHARGES))
					val punc = if (charges > 1) "s" else ""
					it.message("You've added $charges charge$punc to your Tome of Fire.")
				}
			})
		}
		
		repo.onItemOption4(TOME_EMPTY) s@ {
			if (it.player().world().realm().isPVP) return@s
			// Add pages
			it.message("Use any burnt pages you are carrying on the Tome to charge it.")
		}
		repo.onEquipmentOption(2, TOME_EMPTY) s@ {
			if (it.player().world().realm().isPVP) return@s
			it.message("Use any burnt pages you are carrying on the Tome to charge it.")
		}
		repo.onItemOption3(TOME) s@ {
			if (it.player().world().realm().isPVP) {
                it.message("The Tome of Fire is fully charged.")
                return@s
            }

			// Check
			val charges = it.itemUsed().property(ItemAttrib.CHARGES)
			val punc = if (charges > 1) "s" else ""
			it.message("The Tome of Fire has $charges more charge$punc remaining.")
		}
		repo.onEquipmentOption(1, TOME) s@ {
			if (it.player().world().realm().isPVP) {
				it.message("The Tome of Fire is fully charged.")
				return@s
			}

			val charges = it.itemUsed().property(ItemAttrib.CHARGES)
			val punc = if (charges > 1) "s" else ""
			it.message("The Tome of Fire has $charges more charge$punc remaining.")
		}
		repo.onEquipmentOption(2, TOME) s@ {
			if (it.player().world().realm().isPVP) {
				it.message("The Tome does not need to be charged on this world.")
				return@s
			}
			chargeTome(it)
		}
		repo.onItemOption4(TOME) s@ {

			//On W2 we want unlimited charges.
			if (it.player().world().realm().isPVP) {
				it.message("The Tome does not need to be charged on this world.")
				return@s
			}
			chargeTome(it)
		}
	}

	@Suspendable private fun chargeTome(it: Script) {
		// Add/remove pages
		val charges = it.itemUsed().property(ItemAttrib.CHARGES)
		if (charges == 0) {
			it.message("Use any burnt pages you are carrying on the Tome to charge it.")
		} else if (charges < 19) {
			it.message("You have under 20 charges left so no pages can be extracted.")
		} else {
			val remove = Math.min(it.itemUsed().property(ItemAttrib.CHARGES), it.inputInteger("Remove how many charges out of total $charges?"))
			if (remove > 0) {
				val pages = remove / 20
				if (it.player().inventory().add(Item(BURNT_PAGE, pages), false).success()) {
					val newcharges = it.itemUsed().property(ItemAttrib.CHARGES) - pages * 20
					if (newcharges > 0)
						it.itemUsed().property(ItemAttrib.CHARGES, newcharges)
					else
						it.player().inventory().set(it.itemUsedSlot(), Item(TOME_EMPTY))
				} else {
					it.message("You don't have enough inventory space to extract any pages.")
				}
			}
		}
	}

	/**
	 * Checks if the player has the tome of fire equipped with charges to use.
	 */
	@JvmStatic fun equipped(player: Player): Boolean {
		val shield = player.equipment().get(EquipSlot.SHIELD) ?: return false
		return shield.id() == TOME && if (!player.world().realm().isPVP) shield.property(ItemAttrib.CHARGES) > 0 else player.world().realm().isPVP
	}
}