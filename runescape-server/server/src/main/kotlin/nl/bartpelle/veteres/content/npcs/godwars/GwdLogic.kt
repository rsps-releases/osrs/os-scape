package nl.bartpelle.veteres.content.npcs.godwars

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.NpcDeath
import nl.bartpelle.veteres.content.npcs.godwars.armadyl.Kreearra
import nl.bartpelle.veteres.content.npcs.godwars.bandos.Graardor
import nl.bartpelle.veteres.content.npcs.godwars.saradomin.Zilyana
import nl.bartpelle.veteres.content.npcs.godwars.zamorak.Kril
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Jak on 13/04/2017.
 */
object GwdLogic {
	
	
	// Any type of gwd boss - just in the graaardor file srry xxx
	@JvmStatic fun isBoss(i: Int): Boolean {
		return i in arrayOf(2215, 3162, 2205, 3129)
	}
	
	@JvmStatic fun onRespawn(boss: Npc) {
		val minionList: List<Npc> = boss.attribOr<List<Npc>>(AttributeKey.MINION_LIST, null) ?: return
		minionList.forEach { minion ->
			NpcDeath.respawn(minion)
		}
	}
	
	@ScriptMain @JvmStatic fun register(repo: ScriptRepository) {
		repo.onWorldInit {
			var boss: Optional<Npc> = Optional.empty()
			for (a in arrayOf(Kreearra.ENCAMPMENT, Zilyana.ENCAMPMENT, Kril.ENCAMPMENT, Graardor.BANDOS_AREA)) {
				val world = it.ctx<World>()
				// Identify the boss
				world.npcs().forEachInAreaKt(a, { n ->
					if (GwdLogic.isBoss(n.id())) { // Located boss.
						boss = Optional.of(n)
					}
				})
				if (boss.isPresent) {
					// Now identify minions.
					val boss1: Npc = boss.get()
					world.npcs().forEachInAreaKt(a, { n ->
						if (!GwdLogic.isBoss(n.id())) { // non-bosses. must be minions xD
							val minion_list: ArrayList<Npc> = boss1.attribOr(AttributeKey.MINION_LIST, ArrayList<Npc>())
							minion_list.add(n)
							boss1.putattrib(AttributeKey.MINION_LIST, minion_list)
						}
					})
				} else {
					System.out.println("boss missing from gwd... wat")
				}
				boss = Optional.empty()
			}
		}
	}
}