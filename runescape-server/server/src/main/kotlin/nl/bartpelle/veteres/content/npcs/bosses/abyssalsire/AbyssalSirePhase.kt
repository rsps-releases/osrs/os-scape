package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

/**
 * Created by Situations on 10/12/2016.
 */

/**
 * Handles which combat phase the Abyssal Sire is currently in.
 */
enum class AbyssalSirePhase {
	
	PHASE_ONE,
	
	PHASE_TWO,
	
	PHASE_THREE_PART_ONE,
	
	PHASE_THREE_PART_TWO
	
}
