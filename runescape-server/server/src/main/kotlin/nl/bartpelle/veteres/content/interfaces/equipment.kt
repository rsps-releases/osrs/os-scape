package nl.bartpelle.veteres.content.interfaces

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.Mac
import nl.bartpelle.veteres.content.combat.CombatSounds
import nl.bartpelle.veteres.content.items.MaxCape
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelSpecialStakes
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.skills.slayer.Slayer
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.entity.player.WeaponType
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.net.message.game.command.SetPlayerOption
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varp
import java.lang.ref.WeakReference

/**
 * Created by Bart on 8/9/2015.
 */
object Equipment {
	
	@JvmStatic fun hasAmmyOfDamned(player: Player): Boolean {
		return player.equipment().hasAt(EquipSlot.AMULET, 12853) || player.equipment().hasAt(EquipSlot.AMULET, 12851)
	}
	
	@JvmStatic fun fullTorag(player: Player): Boolean {
		return player.equipment().hasAll(4745, 4747, 4749, 4751)
	}
	
	@JvmStatic fun fullAhrim(player: Player): Boolean {
		return player.equipment().hasAll(4708, 4710, 4712, 4714)
	}
	
	@JvmStatic fun fullKaril(player: Player): Boolean {
		return player.equipment().hasAll(4732, 4734, 4736, 4738)
	}
	
	fun unequip(player: Player, slot: Int, interrupt: Boolean = true) {
		if (player.locked())
			return
		
		val at = player.equipment().get(slot) ?: return
		val opt: Int = player.attrib(AttributeKey.BUTTON_ACTION)
		
		// In a duel with the 'No Weapon Switch' option on, blocks swapping weapons AND shields.
		if (slot == EquipSlot.WEAPON || slot == EquipSlot.SHIELD) {
			if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_WEAPON_SWITCH)) {
				player.message("You can't do that within a duel with no weapon switching enabled.")
				return
			}
		}
		
		// Stop actions, except when examining - taking off items doesn't interrupt combat.
		/*if (opt != 10 && interrupt) {
			player.stopActions(false)
		}*/
		
		when (opt) {
			1 -> {
				// This newid can be expanded in the future.
				// Currently converts the maxcape with r-click opts to the corrent inventory one with r-click opts.
				val newid: Int = if (at.id() == 13342) 13280 else at.id()
				val newitem = Item(newid, at.amount()).duplicateProperties(at)
				val res = player.inventory().add(newitem, true)
				val done = res.completed()
				player.equipment().remove(Item(at.id(), done), true)
				player.world().server().scriptRepository().triggerItemUnequip(player, newitem.id())
				
				if (done == at.amount())
					CombatSounds.weapon_equip_sounds(player, slot, at.id())
				
				if (slot == EquipSlot.WEAPON) {
					// Remove auto-select
					SpellSelect.reset(player, true, false)
					
					// Take off the snowball.
					if (newitem.id() == 10501) {
						player.write(SetPlayerOption(5, false, "null"))
					}
				}
				
				if (done != at.amount())
					player.message("You don't have enough free space to do that.")
			}
			10 -> player.message(player.world().examineRepository().item(at.id())) // Examine
			else -> {
				if (player.privilege().eligibleTo(Privilege.ADMIN) && player.attribOr<Boolean>(AttributeKey.DEBUG, false))
					player.message("  Button option %d (slot %d) => Equipment option %d for item: %d", opt, slot, opt - 1, at.id())
				player.stopActions(false)
				player.world().server().scriptRepository().triggerEquipmentOption(player, at.id(), slot, opt - 1)
			}
		}
	}
	
	@JvmStatic fun equip(player: Player, slot: Int, toWear: Item, interrupt: Boolean = true) {
		val info = player.world().equipmentInfo()
		val targetSlot = info.slotFor(toWear.id())
		val type = info.typeFor(toWear.id())
		
		if (targetSlot == -1)
			return
		
		// In a duel with the 'No Weapon Switch' option on, blocks swapping weapons AND shields.
		if (targetSlot == EquipSlot.WEAPON || targetSlot == EquipSlot.SHIELD) {
			if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_WEAPON_SWITCH)) {
				player.message("You can't do that within a duel with no weapon switching enabled.")
				return
			}
		}
		
		// Check if we are already wearing an identical item
		val currentItem = player.equipment().get(player.world().equipmentInfo().slotFor(toWear.id()))
		if (currentItem != null && currentItem.id() == toWear.id() && currentItem.amount() == toWear.amount() && !toWear.definition(player.world()).stackable())//stackable items stack bolts ammo darts etc
			return;
		
		// Requirement testing
		val reqs = player.world().equipmentInfo().requirementsFor(toWear.id())
		if (reqs != null && reqs.size > 0) {
			for (req in reqs) {
				if (player.skills().xpLevel(req.key) < req.value) {
					player.message("You need ${Skills.SKILL_INDEFINITES[req.key]} ${Skills.SKILL_NAMES[req.key]} level of ${req.value} to equip this.")
					return
				}
			}
		}
		
		if ((toWear.id() == AchievementDiary.ACHIEVEMENT_HOOD || toWear.id() == AchievementDiary.ACHIEVEMENT_CAPE) && !AchievementDiary.diaryHandler(player).completedAllAchievements(player)) {
			player.message("You must complete all achievement diary tasks to equip this item.")
			return
		}
		
		if (MAX_CAPES.contains(toWear.id()) && !MaxCape.hasTotalLevel(player)) {
			player.message("You need a Total Level of " + Mac.TOTAL_LEVEL_FOR_MAXED + " to wear this cape.")
			return
		}
		
		// Don't think so buddy xx
		if (Staking.in_duel(player)) {
			if (Staking.equipment_disabled(player, targetSlot)) {
				player.message("That item cannot be worn during this stake.")
				return
			}
			if (targetSlot == 3 && toWear.id() == 11889) {
				player.message("Look whos a dickhead! Me!")
				return
			}
		}
		if (targetSlot == 3 && DuelSpecialStakes.extra_duel_type_rule_broken(player, toWear.id())) {
			player.message("You cannot wear that weapon in this duel.")
			return
		}

		// Trying to equip a weapon that is 2 handed (type 5)
		if (targetSlot == 3 && type == 5 && Staking.in_duel(player)) {
			if (Staking.equipment_disabled(player, EquipSlot.SHIELD)) {
				player.message("Two-handed weapons are disabled for this stake.")
				return
			}
		}
		
		// Begin by setting the used item to null. This is to make it like osrs. Failing is scary but no worries!
		val oldEquip = player.equipment().copy()
		val oldInv = player.inventory().copy()
		
		val toUnequip = player.equipment().get(targetSlot)
		val removed = player.inventory().remove(toWear, true, slot)
		
		// If type is 5 it is a two-handed weapon
		if (type == 5 && player.equipment().hasAt(EquipSlot.SHIELD)) {
			if (player.inventory().add(player.equipment().get(EquipSlot.SHIELD), false, if (!player.equipment().hasAt(3)) slot else 0).failed()) {
				player.message("You don't have enough free space to do that.")
				player.equipment().restore(oldEquip)
				player.inventory().restore(oldInv)
				return
			}
			player.equipment().set(EquipSlot.SHIELD, null)
		}
		
		// If we're trying to equip a shield and we have a 2h weapon equipped, unequip the 2h weapon
		if (targetSlot == EquipSlot.SHIELD && player.equipment().hasAt(EquipSlot.WEAPON)) {
			if (info.typeFor(player.equipment().get(EquipSlot.WEAPON).id()) == 5) {
				// Is this indeed a 2h weapon?
				if (player.inventory().add(player.equipment().get(EquipSlot.WEAPON), false, slot).failed()) {
					player.message("You don't have enough free space to do that.")
					player.equipment().restore(oldEquip)
					player.inventory().restore(oldInv)
					return
				}
				player.equipment().set(EquipSlot.WEAPON, null)
			}
		}
		
		if (targetSlot == EquipSlot.WEAPON) {
			// Weapons interrupt special attack
			player.varps().varp(Varp.SPECIAL_ENABLED, 0)
			
			// Remove auto-select
			SpellSelect.reset(player, true, false)
			val snowball = toWear.id() == 10501
			if (snowball) {
				player.write(SetPlayerOption(5, true, "Pelt"))
			}
			
			if (toWear.id() == 21015) { // Dinh's Bulwark
				player.graphic(1336)
			}
		}
		
		// By default, whatever item we're going to replace will be uneqipped
		// Only circumstance where it is not is arrows, where you can add more to the pile.
		var unequip: Boolean = true
		// Do we already have that same item in that slot?
		if (player.equipment().hasAt(targetSlot, toWear.id())) {
			val newtot = player.equipment().count(toWear.id()) + toWear.amount()
			if (newtot > Integer.MAX_VALUE || newtot < 0) {
				player.message("You don't have enough free space to do that.")
				player.equipment().restore(oldEquip)
				player.inventory().restore(oldInv)
				return
			} else {
				// Dont un equip bolts, instead we'll stack them.
				unequip = false
				// Update the existing worn item total amount to combine the stackable quantities
				player.equipment().set(targetSlot, Item(toWear.id(), newtot))
			}
		}
		
		// Finally, equip the item we had in mind.
		if (unequip) {
			var newitem: Item? = null
			if (toUnequip != null) {
				// On 07 there are two max capes, one has right-click equipment options and the other does not!
				val newid: Int = if (toUnequip.id() == 13342) 13280 else toUnequip.id()
				newitem = Item(newid, toUnequip.amount()).duplicateProperties(toUnequip)
				
				// Take off the snowball. Replace with fight, attack or null.
				if (newitem.id() == 10501) {
					player.write(SetPlayerOption(5, false, "null"))
				}
			}
			if (toUnequip != null) { //TODO for now
				player.world().server().scriptRepository().triggerItemUnequip(player, toUnequip.id())
			}
			val invresult = player.inventory().add(if (newitem == null) toUnequip else newitem, false, slot)
			if (toUnequip != null && invresult.failed()) {
				player.message("You don't have enough free space to do that.")
				player.equipment().restore(oldEquip)
				player.inventory().restore(oldInv)
			} else {
				val newid2: Int = if (toWear.id() == 13280) 13342 else toWear.id()
				val newitem2: Item = Item(newid2, toWear.amount()).duplicateProperties(toWear)
				// When successfully unequipping the pre-existing item in the target slot, wear the new one.
				player.equipment().set(targetSlot, newitem2)
			}
		}

		if (interrupt) {
			val ref: WeakReference<Entity> = player.attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: WeakReference<Entity>(null)
			val wep = player.equipment()[EquipSlot.WEAPON]
			val requestedGmaulSpecs = player.attribOr<Int>(AttributeKey.GRANITE_MAUL_SPECIALS, 0)
			if (wep == null || (wep.id() != 4153 && wep.id() != 12848) || (ref.get() != null) && !player.touches(ref.get()) || (wep.id() == 4153 && requestedGmaulSpecs > 0))
				player.stopActions(false)
		}

		
		player.world().server().scriptRepository().triggerItemEquip(player, toWear.id())
		CombatSounds.weapon_equip_sounds(player, slot, toWear.id())
		player.updateWeaponInterface()
	}
	
	@JvmStatic val MAX_CAPES = intArrayOf(
			13280, 13329, 13331, 13333, 13335, 13337, 13342, 20760,
			MaxCape.INFERNAL_MAX_CAPE, MaxCape.IMBUED_GUTHIX_MAX_CAPE, MaxCape.IMBUED_SARADOMIN_MAX_CAPE, MaxCape.IMBUED_ZAMORAK_MAX_CAPE, 21898)
	
	@JvmStatic fun wearingMaxCape(player: Player): Boolean {
		for (i in MAX_CAPES) {
			if (player.equipment().hasAt(EquipSlot.CAPE, i)) {
				return true
			}
		}
		return false
	}
	
	@JvmStatic val GRACEFUL_CAPES = intArrayOf(11852, 13581, 13593, 13605, 13617, 13629, 13669)
	
	@JvmStatic val GRACEFUL_ITEMS = intArrayOf(11850, 11852, 11854, 11856, 11858, 11860)
	
	@JvmStatic fun wearsFullGraceful(player: Player)
			= player.equipment().hasAllArr(GRACEFUL_ITEMS)
	
	fun targetIsSlayerTask(player: Player, target: Entity): Boolean {
		if (target.isNpc) {
			val type = (target as Npc).id()
			if (type == 2668 || Slayer.creatureMatches(player, type)) { // 2668 is combat dummy, always does max hit.
				return true
			}
		}
		return false
	}
	
	fun corpbeastArmour(player: Player, target: Entity, weaponType: Int): Boolean {
		return target.isNpc && (target as Npc).id() == 319 && (weaponType != WeaponType.SPEAR || player.equipment().hasAt(EquipSlot.WEAPON, 11889))
	}
	
	/**
	 * Checks if the person you're attacking has a charged Serp helm. If so, theres a 1/6 chance you get venomed.
	 */
	@JvmStatic fun checkTargetVenomGear(attacker: Entity, victim: Entity) {
		// NOTE: Jagex has nerfed the helm and it no longer poisons Players
		if (victim.isPlayer && attacker.isNpc) {
			// There is a chance for venom to apply when it has charges. Charge deduction is done on a different time - the same one as Barrows, it decays when in combat @ 733 scales/hour
			if (Equipment.venomHelm(victim) && attacker.world().rollDie(6, 1)) {
				attacker.venom(victim)
			}
		}
	}
	
	@JvmStatic fun venomHelm(target: Entity): Boolean {
		val helm = target.equipment().get(EquipSlot.HEAD) ?: return false
		if (helm.id() == 12931 || helm.id() == 13197 || helm.id() == 13199) {
			if (helm.propertyOr(ItemAttrib.ZULRAH_SCALES, 0) > 0) {
				return true
			}
		}

		return false
	}
	
	fun masteryCape(itemid: Int): Boolean {
		return itemid in 9747..9814 || itemid in 9948..9950 // Hunter cape added at a later time lol
	}
}

@ScriptMain fun unequip(repo: ScriptRepository) {
	repo.onButton(387, 6) { Equipment.unequip(it.player(), EquipSlot.HEAD) }
	repo.onButton(387, 7) { Equipment.unequip(it.player(), EquipSlot.CAPE) }
	repo.onButton(387, 8) { Equipment.unequip(it.player(), EquipSlot.AMULET) }
	repo.onButton(387, 9) { Equipment.unequip(it.player(), EquipSlot.WEAPON) }
	repo.onButton(387, 10) { Equipment.unequip(it.player(), EquipSlot.BODY) }
	repo.onButton(387, 11) { Equipment.unequip(it.player(), EquipSlot.SHIELD) }
	repo.onButton(387, 12) { Equipment.unequip(it.player(), EquipSlot.LEGS) }
	repo.onButton(387, 13) { Equipment.unequip(it.player(), EquipSlot.HANDS) }
	repo.onButton(387, 14) { Equipment.unequip(it.player(), EquipSlot.FEET) }
	repo.onButton(387, 15) { Equipment.unequip(it.player(), EquipSlot.RING) }
	repo.onButton(387, 16) { Equipment.unequip(it.player(), EquipSlot.AMMO) }
}
