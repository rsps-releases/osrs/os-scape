package nl.bartpelle.veteres.content.areas.morytania.slayertower

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/7/2015.
 */

object Doors {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		intArrayOf(2108, 2111).forEach { door ->
			r.onObject(door) { openDoor(it) }
		}
		intArrayOf(2113, 2112).forEach { door ->
			r.onObject(door) { closeDoor(it) }
		}
	}
	
	fun openDoor(s: Script) {//slayer tower enterance
		val world = s.player().world()
		world.removeObj(MapObj(Tile(3429, 3535), 2111, 0, 1))
		world.removeObj(MapObj(Tile(3428, 3535), 2108, 0, 1))
		world.spawnObj(MapObj(Tile(3429, 3536), 2113, 0, 2))
		world.spawnObj(MapObj(Tile(3428, 3536), 2112, 0, 0))
	}
	
	fun closeDoor(s: Script) {//slayer tower enterance
		val world = s.player().world()
		world.spawnObj(MapObj(Tile(3429, 3535), 2111, 0, 1))
		world.spawnObj(MapObj(Tile(3428, 3535), 2108, 0, 1))
		world.removeObj(MapObj(Tile(3429, 3536), 2113, 0, 2))
		world.removeObj(MapObj(Tile(3428, 3536), 2112, 0, 0))
	}
	
}