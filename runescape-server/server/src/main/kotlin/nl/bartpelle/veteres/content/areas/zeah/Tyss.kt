package nl.bartpelle.veteres.content.areas.zeah

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 2016-06-14.
 */

object Tyss {
	
	val TYSS = 7050
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(TYSS) @Suspendable {
			it.chatNpc("Tread carefully in this hallowed place, traveller. The<br>magicks of the Dark Altar are not to be treated lightly", TYSS, 563)
			when (it.options("What is this place?", magicks_option(it), "I'll leave it alone.")) {
				1 -> what_is_this_place(it)
				2 -> magicks_option_selected(it)
				3 -> leave_it_alone(it)
			}
		}
		
		r.onNpcOption2(TYSS) @Suspendable {
			val book = it.player().varps().varbit(Varbit.SPELLBOOK)
			
			if (book == 3) {
				it.player().varps().varbit(Varbit.SPELLBOOK, 0)
				it.chatNpc("The magicks of the Dark Altar have been lifted from<br>your mind.", TYSS, 589)
			} else {
				it.player().varps().varbit(Varbit.SPELLBOOK, 3)
				it.chatNpc("If you find an ensouled cadaver in your travels, you<br>may attempt to reanimate it with the magicks I have<br>placed in your mind.", TYSS, 590)
			}
			
		}
	}
	
	@Suspendable fun what_is_this_place(it: Script) {
		it.chatPlayer("What is this place?", 554)
		it.chatNpc("The Dark Altar is the treasure of the Arceuus House.<br>Our forebears spent their lives - literally, in some cases<br>" +
				"- learning to unlock its secrets and wield its power.", TYSS, 564)
		it.chatNpc("The power of the Dark Altar, reflected in these great<br>crystals, can twist dimensions, warp minds, and even<br>" +
				"bridge the gulf between life and death.", TYSS, 564)
		when (it.options(magicks_option(it), "I'll leave it alone.")) {
			1 -> magicks_option_selected(it)
			2 -> leave_it_alone(it)
		}
	}
	
	@Suspendable fun magicks_option_selected(it: Script) {
		val book = it.player().varps().varbit(Varbit.SPELLBOOK)
		
		if (book == 3) {
			it.chatPlayer("I'd like to stop using your spellbook now.", 554)
			it.player().varps().varbit(Varbit.SPELLBOOK, 0)
			it.chatNpc("Very well, the magicks of the Dark Altar have been<br>lifted from your mind.", TYSS, 589)
		} else {
			it.chatPlayer("Can I try the magicks myself?", 554)
			it.player().varps().varbit(Varbit.SPELLBOOK, 3)
			it.chatNpc("Very well. If you find an ensouled cadaver in your<br>travels, you may attempted to reanimate it with the<br>magicks I have placed in your mind.", TYSS, 590)
			it.chatPlayer("Thanks.", 596)
		}
	}
	
	@Suspendable fun leave_it_alone(it: Script) {
		it.chatPlayer("I'll leave it alone.", 592)
	}
	
	@Suspendable fun magicks_option(it: Script): String {
		val book = it.player().varps().varbit(Varbit.SPELLBOOK)
		
		if (book == 3)
			return "I'd like to stop using your spellbook now."
		else
			return "Can I try the magicks myself?"
	}
}
