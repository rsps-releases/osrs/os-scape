package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/17/2016.
 */

object Ghommal {
	
	val GHOMMAL = 2457
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(GHOMMAL) @Suspendable {
			val attack_lvl = it.player().skills().level(Skills.ATTACK)
			val strength_lvl = it.player().skills().level(Skills.STRENGTH)
			
			//Does our player have the requirements to enter the guild?
			if (attack_lvl + strength_lvl < 130) {
				it.chatNpc("You not pass. You too weedy.", GHOMMAL, 588)
				it.chatPlayer("What? But I'm a warrior!", 571)
				it.chatNpc("Heehee... he say he warrior... I not heard that one<br>for... at leas' 5 minutes!", GHOMMAL, 568)
				it.chatPlayer("Go on, let me in, you know you want to. I could...<br>make it worth your while...", 593)
				it.chatNpc("No! You is not a strong warrior, you not enter till you<br>bigger. Ghommal does not take bribes.", GHOMMAL, 615)
				it.chatPlayer("Why not?", 554)
				it.chatNpc("Ghommal stick to Warrior's Code of Honour. When<br>you a bigger, stronger warrior, you come back.", GHOMMAL, 589)
			} else {
				it.chatNpc("Ghommal welcome you to Warrior Guild!", GHOMMAL, 567)
				it.chatPlayer("Umm.. thank you, I think.", 588)
			}
		}
	}
}
