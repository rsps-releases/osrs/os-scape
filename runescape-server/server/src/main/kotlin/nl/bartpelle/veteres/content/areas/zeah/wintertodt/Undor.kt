package nl.bartpelle.veteres.content.areas.zeah.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/11/2016.
 */

object Undor {
	
	val UDNOR = 7375
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(UDNOR) @Suspendable {
			val TALKED_TO_IGNISIA = it.player().attribOr<Int>(AttributeKey.TALKED_TO_IGNISIA, 0)
			
			if (TALKED_TO_IGNISIA == 1) {
				it.chatNpc("Hello there laddie.", UDNOR, 558)
				when (it.options("Tell me about the Wintertodt.", "Tell me about the Doors of Dinh.", "Tell me about yourself.", "Goodbye.")) {
					1 -> tell_me_about_wintertodt(it)
					2 -> tell_me_about_doors_of_dihn(it)
					3 -> tell_me_about_yourself(it)
					4 -> goodbye(it)
				}
			} else {
				it.chatPlayer("Hello.", 567)
				it.chatNpc("Well hello there laddie.", UDNOR, 567)
				it.chatPlayer("What's all this then?", 554)
				it.chatNpc("Talk to that fiery lass, she'll sort you out.", UDNOR, 567)
			}
			
		}
	}
	
	@Suspendable fun tell_me_about_wintertodt(it: Script) {
		it.chatPlayer("Tell me about the Wintertodt.", 554)
		it.chatNpc("They say it's some kind of powerful fire spirit.", UDNOR, 567)
		it.chatPlayer("Don't you mean ice?", 575)
		it.chatNpc("Aye that as well.", UDNOR, 567)
		it.chatNpc("What it is doesn't bother me, just as long as we keep it<br>behind them doors.", UDNOR, 568)
		when (it.options("Tell me about the Doors of Dinh.", "Tell me about yourself.", "Goodbye.")) {
			1 -> tell_me_about_doors_of_dihn(it)
			2 -> tell_me_about_yourself(it)
			3 -> goodbye(it)
		}
	}
	
	@Suspendable fun tell_me_about_doors_of_dihn(it: Script) {
		it.chatPlayer("Tell me about the Doors of Dinh.", 554)
		it.chatNpc("The Doors were built by Dinh to contain the<br>Wintertodt. This was over a thousand years ago and<br>they still stand today.", UDNOR, 569)
		it.chatPlayer("Who was Dinh?", 554)
		it.chatNpc("Dinh was the greatest smith that the world has ever<br>seen. I am the finest smith around today and even I<br>cannot match his abilities.", UDNOR, 569)
		when (it.options("Tell me about the Wintertodt.", "Tell me about yourself.", "Goodbye.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_yourself(it)
			3 -> goodbye(it)
		}
		
	}
	
	@Suspendable fun tell_me_about_yourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 554)
		it.chatNpc("The name's Undor. You'll struggle to find a dwarf in<br>this land who knows more about metalwork than me.", UDNOR, 568)
		it.chatNpc("One day I hope to be as good as the legendary Dinh<br>himself.", UDNOR, 568)
		it.chatPlayer("What are you doing here?", 554)
		it.chatNpc("They say the Doors are failing and the Wintertodt is<br>trying to escape. I'm here to prove to them that the<br>Doors are fine.", UDNOR, 569)
		it.chatPlayer("And are they?", 554)
		it.chatNpc("Of course they are! They were made by Dinh himself.", UDNOR, 614)
		when (it.options("Tell me about the Wintertodt.", "Tell me about the Doors of Dinh.", "Goodbye.")) {
			1 -> tell_me_about_wintertodt(it)
			2 -> tell_me_about_doors_of_dihn(it)
			3 -> goodbye(it)
		}
	}
	
	@Suspendable fun goodbye(it: Script) {
		it.chatPlayer("Goodbye.", 588)
		it.chatNpc("Until next time laddie.", UDNOR, 567)
	}
}

