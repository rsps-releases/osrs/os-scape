package nl.bartpelle.veteres.content.npcs.inferno.controllers

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.minigames.inferno.InfernoNpcController
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Mack on 8/5/2017.
 */
class JalNibController(nibbler: Npc, session: InfernoSession, offset: Int): InfernoNpcController {
	
	/**
	 * The nibbler npc.
	 */
	val nibbler = nibbler
	
	/**
	 * The current inferno session.
	 */
	val session = session
	
	/**
	 * The current offset value based on how many nibblers are existent to allow for unique tile spawning.
	 */
	var counter = offset
	
	/**
	 * The x-offset we use to modify the z_pos
	 */
	var xPosOff = 0
	
	/**
	 * A flag checking if the nibbler's found a pillar to attack.
	 */
	var found: Boolean = false
	
	override fun onSpawn() {
		
		//Every 3 creeps per column we make a new row
		if (counter % 3 == 0) {
			xPosOff = 1
		}
		
		nibbler.tile(Tile(session.area!!.center().x + xPosOff, session.area!!.center().z + 1 + counter))
		
		for (pillarIndex in 0..session.activePillars.keys.size - 1) {
			if (!session.pillarAt(pillarIndex)!!.dead()) {
				nibbler.attack(session.pillarAt(pillarIndex))
				found = true
				break
			}
		}
		
		//If no pillar found... attack the player.
		if (!found) {
			nibbler.attack(session.player)
		}
	}
	
	override fun onDamage() {
		if (!InfernoContext.inSession(session.player())) {
			session.player().message("I don't think I should be doing this...")
			return
		}
		super.onDamage()
	}
}