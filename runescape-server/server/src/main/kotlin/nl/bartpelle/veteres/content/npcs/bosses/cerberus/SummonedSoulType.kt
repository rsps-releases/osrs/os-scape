package nl.bartpelle.veteres.content.npcs.bosses.cerberus

/**
 * Created by Jason MacKeigan on 2016-07-07 at 12:43 PM
 *
 * Represents the combat style of each mob.
 */
enum class SummonedSoulType(val id: Int) {
	RANGE(5867), MAGIC(5868), MELEE(5869)
}