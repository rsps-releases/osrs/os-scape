package nl.bartpelle.veteres.content.skills.herblore

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemOn
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/21/2015.
 * Source data revamped by Shadowy|Jak on 8/6/2016
 */
object PotionMixing {
	
	
	enum class PotDoses(val one: Int, val two: Int, val three: Int, val four: Int, val emptyItem: Int = 229) {
		ANTIFIRE(2458, 2456, 2454, 2452),
		ZAMMY_BREW(193, 191, 189, 2450),
		SARA_BREW(6691, 6689, 6687, 6685),
		RESTORE(131, 129, 127, 2430),
		PRAYER(143, 141, 139, 2434),
		DEFENCE(137, 135, 133, 2432),
		ATTACK(125, 123, 121, 2428),
		STR(119, 117, 115, 113),
		MAGIC(3046, 3044, 3042, 3040),
		SUPER_MAGIC(11729, 11728, 11727, 11726),
		SUPER_ENERGY(3022, 3020, 3018, 3016),
		ENERGY(3014, 3012, 3010, 3008),
		SUPER_DEF(167, 165, 163, 2442),
		SUPER_RESTORE(3030, 3028, 3026, 3024),
		SUPER_STR(161, 159, 157, 2440),
		RANGING(173, 171, 169, 2444),
		SUPER_ATT(149, 147, 145, 2436),
		ANTIPOISON(179, 177, 175, 2446),
		SUPER_ANTIPOSION(185, 183, 181, 2448),
		ANTIDOTE_P(5949, 5947, 5945, 5943),
		ANTIDOTE_PP(5958, 5956, 5954, 5952),
		COMBAT(9745, 9743, 9741, 9739),
		SUPER_CB(12701, 12699, 12697, 12695),
		AGILITY(3038, 3036, 3034, 3032),
		SANFEW(10931, 10929, 10927, 10925),
		STAMINA(12631, 12629, 12627, 12625),
		EXTENDED_ANTIFIRE(11957, 11955, 11953, 11951),
		ANTI_VENOM_PLUS(12919, 12917, 12915, 12913),
		ANTI_VENOM(12911, 12909, 12907, 12905),
		GUTHIX_REST(4423, 4421, 4419, 4417, 1980);
		
		operator fun get(index: Int): Int {
			if (index == 0) {
				return one
			} else if (index == 1) {
				return two
			} else if (index == 2) {
				return three
			} else {
				return four
			}
		}
		
		fun numDoses(id: Int): Int {
			if (id == one) {
				return 1
			} else if (id == two) {
				return 2
			} else if (id == three) {
				return 3
			} else if (id == four) {
				return 4
			}
			
			return 0
		}
		
		operator fun contains(id: Int): Boolean {
			return three == id || four == id || one == id || two == id
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		reload(r)
	}
	
	// So it can be hot reloaded from JS
	@JvmStatic fun reload(r: ScriptRepository) {
		for (doses in PotionMixing.PotDoses.values()) {
			// One
			r.onItemOnItem(doses.one, doses.one) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.two, doses.emptyItem)
			}
			r.onItemOnItem(doses.one, doses.two) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.three, doses.emptyItem)
			}
			r.onItemOnItem(doses.one, doses.three) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.four, doses.emptyItem)
			}
			r.onItemOnItem(doses.one, doses.four) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.three, doses.two)
			}
			// (2)
			r.onItemOnItem(doses.two, doses.two) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.four, doses.emptyItem)
			}
			r.onItemOnItem(doses.two, doses.three) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.four, doses.one)
			}
			r.onItemOnItem(doses.two, doses.four) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.three, doses.three)
			}
			// (3)
			r.onItemOnItem(doses.three, doses.three) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.four, doses.two)
			}
			// (4) is done by default in above
			// Now splitting potions up
			r.onItemOnItem(doses.emptyItem, doses.two) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.one, doses.one)
			}
			r.onItemOnItem(doses.emptyItem, doses.three) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.two, doses.one)
			}
			r.onItemOnItem(doses.emptyItem, doses.four) {
				combine(it.player(), it.itemUsed().id(), it.itemOn().id(), doses.two, doses.two)
			}
		}
	}
	
	@JvmStatic fun combine(player: Player, used: Int, on: Int, result1: Int, result2: Int) {
		val def = player.world().definitions().get(ItemDefinition::class.java, result1)
		player.inventory().remove(Item(used, 1), true, player.attribOr(AttributeKey.ITEM_SLOT, 0))
		player.inventory().add(Item(result2, 1), true, player.attribOr(AttributeKey.ITEM_SLOT, 0))
		player.inventory().remove(Item(on, 1), true, player.attribOr(AttributeKey.ALT_ITEM_SLOT, 0))
		player.inventory().add(Item(result1, 1), true, player.attribOr(AttributeKey.ALT_ITEM_SLOT, 1))
		val doses: String = if (def.name.contains("1")) "1" else if (def.name.contains("2")) "2" else if (def.name.contains("3")) "3" else if (def.name.contains("4")) "four" else "0"
		player.message("You combined the liquid into $doses doses.")
	}
	
	@JvmStatic fun verifyData(player: Player) {
		
		for (doses in PotionMixing.PotDoses.values()) {
			val n1 = player.world().definitions().get(ItemDefinition::class.java, doses.one)!!.name
			val n2 = player.world().definitions().get(ItemDefinition::class.java, doses.two)!!.name
			val n3 = player.world().definitions().get(ItemDefinition::class.java, doses.three)!!.name
			val n4 = player.world().definitions().get(ItemDefinition::class.java, doses.four)!!.name
			System.out.printf("${doses.name} contains: %s, %s, %s, %s", n1, n2, n3, n4)
			System.out.println()
		}
	}
}