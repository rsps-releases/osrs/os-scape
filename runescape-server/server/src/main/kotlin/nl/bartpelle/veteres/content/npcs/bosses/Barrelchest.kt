package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Nick on 9/20/2016.
 */

object Barrelchest {
	
	val CHANT = "I WILL NOT DIE!"
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackMelee(npc, target, true) && EntityCombat.attackTimerReady(npc)) {
				if (EntityCombat.attackTimerReady(npc) && !npc.attribOr<Boolean>(AttributeKey.BARRELCHEST_JUMP_STATE, false)) {
					val random = npc.world().random(5)
					
					when (random) {
					// 1 -> blockDamage(npc, target)
						2 -> jumpAttack(it, npc, target)
						else -> meleeAttack(npc, target)
					}
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun meleeAttack(npc: Npc, target: Entity) {
		val tile = target.tile()
		
		target.world().players().forEachKt({ player ->
			if (tile.inSqRadius(player.tile(), 2)) {
				if (EntityCombat.attemptHit(npc, player, CombatStyle.MELEE))
					player.hit(npc, EntityCombat.randomHit(npc)) else player.hit(npc, 0)
			}
		})
		
		npc.animate(npc.attackAnimation())
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	@Suspendable fun jumpAttack(it: Script, npc: Npc, target: Entity) {
		
		npc.lockNoDamage()
		it.delay(1)
		it.npc().sync().animation(5895, 5)
		it.delay(2)
		npc.putattrib(AttributeKey.BARRELCHEST_JUMP_STATE, true)
		npc.clearattrib(AttributeKey.BARRELCHEST_JUMP_STATE)
		npc.face(target)
		npc.unlock()
		
		if (target.tile().inSqRadius(npc.tile(), 2)) {
			
			target.executeScript @Suspendable { s ->
				val p = target as Player
				val direction: Direction = Direction.of(p.tile().x - npc.tile().x,
						p.tile().z - npc.tile().z)
				val tile: Tile = p.tile().transform(direction.x * +1, direction.y * +1)
				val face: FaceDirection = FaceDirection.forTargetTile(npc.tile(), p.tile())
				val movement: ForceMovement = ForceMovement(0, 0, direction.x * +1, direction.y * +1, 20, 50, face)
				
				p.lock()
				p.message("Barrelchest lands and the shockwave throws you backwards.")
				p.forceMove(movement)
				p.animate(734)
				s.delay(2)
				p.teleport(tile)
				s.delay(2)
				p.unlock()
				p.hit(p, p.world().random(15))
			}
		}
		
		npc.clearattrib(AttributeKey.BARRELCHEST_JUMP_STATE)
		it.delay(4)
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
}