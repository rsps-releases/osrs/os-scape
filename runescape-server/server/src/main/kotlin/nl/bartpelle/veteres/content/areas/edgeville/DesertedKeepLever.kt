package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 11/9/2015.
 */

object DesertedKeepLever {
	
	val LEVER = 1815
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(LEVER) @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			
			player.faceObj(obj)
			val target = if (it.interactionOption() == 1) Tile(2561, 3311) else Tile(3090, 3475)
			
			if (!player.timers().has(TimerKey.TELEBLOCK)) {
				DeadmanMechanics.attemptTeleport(it)
				player.lock()
				it.delay(1)
				it.animate(2140)
				it.message("You pull the lever...")
				
				it.runGlobal(player.world()) @Suspendable {
					it.delay(1)
					val world = player.world()
					val spawned = MapObj(obj.tile(), 88, obj.type(), obj.rot())
					world.spawnObj(spawned)
					it.delay(5)
					world.removeObjSpawn(spawned)
				}
				
				it.delay(2)
				it.animate(714)
				player.graphic(111, 110, 0)
				it.delay(4)
				player.teleport(target)
				it.animate(-1)
				player.unlock()
				if (!player.world().realm().isDeadman) // So we can tele straight away
					player.clearattrib(AttributeKey.LAST_WAS_ATTACKED_TIME)
				it.message("...And teleport out of the wilderness.")
			} else {
				it.player().teleblockMessage()
			}
		}
	}
	
}
