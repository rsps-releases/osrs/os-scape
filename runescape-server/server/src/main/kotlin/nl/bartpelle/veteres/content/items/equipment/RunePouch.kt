package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 1/24/2016.
 */

object RunePouch {
	
	// Maximum number of runes a stack may hold is 16k.
	val ABS_MAX = 16000
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// View option
		r.onItemOption1(12791) {
			show(it.player())
		}
		
		// Empty option
		r.onItemOption4(12791) {
			for (slot in 0..2) {
				val rune: Item? = runeAt(it.player(), slot)
				if (rune != null) {
					val added = it.player().inventory().add(rune, false)
					if (added.success()) {
						setRuneAt(it.player(), slot, null)
					} else {
						setRuneAt(it.player(), slot, Item(rune.id(), added.requested() - added.completed()))
					}
					if (added.completed() != 0) {
						val name: String = it.player().world().definitions().get(ItemDefinition::class.java, rune.id())!!.name
						it.player().message("%d x %s was removed from your rune pouch", added.completed(), name)
					} else {
						it.player().message("Your inventory is too full to empty these runes.")
					}
				}
			}
		}
		
		for (runetype in Runes.values()) {
			r.onItemOnItem(12791, runetype.item) {
				val rune = it.player().inventory().get(it.player().attrib(AttributeKey.ITEM_SLOT))!!
				val added = add(it.player(), Item(rune.id(), Math.min(ABS_MAX, rune.amount())))
				if (added < 1) {
					it.message("You cannot add any more runes to your pouch.")
				} else {
					it.player().inventory().remove(Item(rune.id(), added), true)
					val name: String = it.player().world().definitions().get(ItemDefinition::class.java, rune.id())!!.name
					it.player().message("You've added %d x $name into the pouch.", added)
				}
			}
		}
		
		r.onButton(190, 4, s@ {
			// Withdraw. button op 1-4 and 10 for 1, 5, all, X, examine
			val slot = it.player().attrib<Int>(AttributeKey.BUTTON_SLOT)
			val action = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			
			var num = when (action) {
				1 -> 1
				2 -> 5
				3 -> Int.MAX_VALUE
				4 -> it.inputInteger("Enter amount:")
				10 -> {
					it.player().message("%s", it.player().world().examineRepository().item(runeAt(it.player(), slot)!!.id()))
					return@s
				}
				else -> return@s
			}
			val rune: Item = runeAt(it.player(), slot) ?: return@s
			num = Math.min(rune.amount(), num)
			if (num < 1) return@s
			
			val added = it.player().inventory().add(Item(rune.id(), num), false)
			if (rune.amount() - added.completed() == 0) {
				setRuneAt(it.player(), slot, null)
			} else {
				setRuneAt(it.player(), slot, Item(rune.id(), rune.amount() - added.completed()))
			}
			if (added.completed() != 0) {
				val name: String = it.player().world().definitions().get(ItemDefinition::class.java, rune.id())!!.name
				it.player().message("%d x %s was removed from your rune pouch", added.completed(), name)
			} else {
				it.player().message("Your inventory is too full to empty these runes.")
			}
		})
		
		// Insert / add to pouch
		r.onButton(190, 8, s@ @Suspendable {
			val slot = it.player().attrib<Int>(AttributeKey.BUTTON_SLOT)
			val action = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			val item = it.player().inventory().get(slot) ?: return@s
			
			var num = when (action) {
				1 -> 1
				2 -> 5
				3 -> Int.MAX_VALUE
				4 -> it.inputInteger("Enter amount:")
				10 -> {
					it.player().message("%s", it.player().world().examineRepository().item(item.id()))
					return@s
				}
				else -> return@s
			}
			
			// Sanitize. Make sure we're not going to dupe here :)
			num = Math.min(it.player().inventory().count(item.id()), Math.min(ABS_MAX, num))
			
			// Only add if num is more than 1!
			if (num < 1)
				return@s
			
			if (item != null) {
				val rune = Runes.get(item.id())
				if (rune == null) {
					it.message("You can only add runes to the rune pouch.")
				} else {
					val added = add(it.player(), Item(rune.item, num))
					if (added < 1) {
						it.message("You cannot add any more runes to your pouch.")
					} else {
						it.player().inventory().remove(Item(rune.item, added), true)
					}
				}
			}
		})
	}
	
	fun add(player: Player, rune: Item): Int {
		// Translate the item to a rune type
		val type = Runes.get(rune.id()) ?: return 0
		val slot = slotOf(player, type)
		
		// Two options: if we have a stack, add it to that. If we don't have a stack, make a new stack.
		if (slot != -1) { // Has in pouch..
			val runeAt = runeAt(player, slot)
			val numAdd = Math.min(rune.amount(), ABS_MAX - runeAt!!.amount()) // Don't exceed 16k!
			
			setRuneAt(player, slot, Item(runeAt.id(), runeAt.amount() + numAdd))
			
			return numAdd
		} else { // Make new stack :)
			val freeSlot = freeSlot(player)
			
			if (freeSlot == -1) { // No free slot means no more runes, hah.
				return 0
			} else {
				// Looks like we have a slot. Let's put it there.
				setRuneAt(player, freeSlot, rune)
				return Math.min(ABS_MAX, rune.amount()) // Can only add 16k max, so safety check here.
			}
		}
		
		return 0
	}
	
	fun show(player: Player) {
		player.interfaces().sendMain(190)
		player.interfaces().setting(190, 4, 0, 2, 1311774)
		player.interfaces().setting(190, 8, 0, 27, 1311774)
	}
	
	fun slotOf(player: Player, rune: Runes): Int {
		var index = 0
		
		for (r in allRunesOf(player)) {
			if (r != null && r.id() == rune.item && r.amount() > 0) {
				return index
			}
			
			index++
		}
		
		return -1
	}
	
	fun freeSlot(player: Player): Int {
		var index = 0
		
		for (r in allRunesOf(player)) {
			if (r == null || r.amount() < 1) {
				return index
			}
			
			index++
		}
		
		return -1
	}
	
	fun remove(player: Player, runeId: Int, amt: Int): Int {
		var idx = -1
		
		for (rune in allRunesOf(player)) {
			idx++
			
			if (rune != null && rune.id() == runeId && rune.amount() > 0) {
				val currentAmt = rune.amount()
				var newitem: Item? = Item(runeId, rune.amount() - amt)
				if (newitem!!.amount() < 0)
					newitem = null
				
				setRuneAt(player, idx, newitem)
				// Return amt available to be removed, or asked removal amt if there are still some left.
				return if (newitem == null) currentAmt else amt
			}
		}
		return 0
	}
	
	fun runeAt(player: Player, slot: Int): Item? {
		val id = player.varps().varbit(slot.toTypeVarbit())
		val amt = player.varps().varbit(slot.toNumVarbit())
		
		// No amount means no rune.
		if (amt < 1) {
			return null
		}
		
		// Translate to a rune item
		return Item(Runes.values()[id - 1].item, amt)
	}
	
	fun setRuneAt(player: Player, slot: Int, rune: Item?) {
		if (rune != null) {
			player.varps().varbit(slot.toTypeVarbit(), Runes.get(rune.id())!!.ordinal + 1)
			player.varps().varbit(slot.toNumVarbit(), Math.min(ABS_MAX, rune.amount()))
		} else {
			player.varps().varbit(slot.toTypeVarbit(), 0)
			player.varps().varbit(slot.toNumVarbit(), 0)
		}
	}
	
	fun numberOf(player: Player, runeId: Int): Int {
		if (player.inventory().count(12791) == 0) { // Don't count runes if you haven't got the pouch on you!
			return 0
		}
		val all = allRunesOf(player)
		for (rune in all) {
			if (rune != null) {
				if (rune.id() == runeId)
					return rune.amount()
			}
		}
		return 0
	}
	
	fun allRunesOf(player: Player): Array<Item?> {
		return arrayOf(runeAt(player, 0), runeAt(player, 1), runeAt(player, 2))
	}
	
	fun Int.toNumVarbit(): Int {
		when (this) {
			0 -> return Varbit.RUNEPOUCH_NUM_LEFT
			1 -> return Varbit.RUNEPOUCH_NUM_MIDDLE
			2 -> return Varbit.RUNEPOUCH_NUM_RIGHT
		}
		
		return 0
	}
	
	fun Int.toTypeVarbit(): Int {
		when (this) {
			0 -> return Varbit.RUNEPOUCH_TYPE_LEFT
			1 -> return Varbit.RUNEPOUCH_TYPE_MIDDLE
			2 -> return Varbit.RUNEPOUCH_TYPE_RIGHT
		}
		
		return 0
	}
	
	enum class Runes(val item: Int) {
		AIR(556), WATER(555), EARTH(557), FIRE(554), MIND(558), CHAOS(562),
		DEATH(560), BLOOD(565), COSMIC(564), NATURE(561), LAW(563), BODY(559),
		SOUL(566), ASTRAL(9075), MIST(324), MUD(4698), DUST(4696), LAVA(4699),
		STEAM(4694), SMOKE(4697), WRATH(21880), NONE(-1);
		
		companion object {
			fun get(rune: Int): Runes? {
				values().forEach {
					if (it.item == rune)
						return it
				}
				
				return null
			}
		}
	}
	
}