package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 17/10/2016.
 */
object FairyRingTransport {
	
	/**
	 * Child id on the inventory interface where you can select a target teleport
	 * Varbit values: 3 varbits for each combination wheel.
	 * Tile - destination
	 * target name - to render on the Travel log
	 */
	enum class FairyRings(val childId: Int, val varbitValues: IntArray = intArrayOf(0, 0, 0), val tile: Tile = Tile(0, 0), val targetName: String) {
		MUDSKIP(13, intArrayOf(0, 0, 3), Tile(2996, 3114), "Asgarnia: Mudskipper Point"),
		ISLANDS_ARDY(15, intArrayOf(0, 0, 2), Tile(2700, 3247), "Islands: South-east of Ardougne"),
		DORGESH_KAAN_CAVE(21, intArrayOf(0, 3, 3), Tile(2735, 5221), "Dungeons: Dorgesh-Kaan cave"),
		KANDARIN_SLAYER(23, intArrayOf(0, 3, 2), Tile(2780, 3613), "Kandarin: Slayer cave near Rellekka"),
		ISLAND_PENG(25, intArrayOf(0, 3, 1), Tile(2500, 3896), "Islands: Penguins"),
		KANDARIN_HUNTER(29, intArrayOf(0, 2, 3), Tile(2319, 3619), "Kandarin: Piscatoris Hunter area"),
		FELDIP(33, intArrayOf(0, 2, 1), Tile(2571, 2956), "Feldip Hills: Jungle Hunter area"),
		LIGHTHOUSE(35, intArrayOf(0, 1, 0), Tile(2503, 3636), "Islands: Lighthouse"),
		MORYTANIA(37, intArrayOf(0, 1, 3), Tile(3597, 3495), "Morytania: Haunted Woods"),
		ABYSS(39, intArrayOf(0, 1, 2), Tile(3059, 4875), "Other Realms: Abyss"),
		MCGRUBOR_WOODS(41, intArrayOf(0, 1, 1), Tile(2644, 3495), "Kandarin: McGrubor's Wood"),
		ISLAND_SALVE(43, intArrayOf(3, 0, 0), Tile(3410, 3324), "Islands: River Salve"),
		DESERT(45, intArrayOf(3, 0, 3), Tile(3251, 3095), "Kharidian Desert: Near the Kalphite Hive"),
		ARDY_ZOO(49, intArrayOf(3, 0, 1), Tile(2635, 3266), "Kandarin: Ardougne Zoo unicorns"),
		FISHER_KING(55, intArrayOf(3, 3, 2), Tile(0, 0), "Other Realms: Fisher King"), // Instanced
		ZUL_ANDRA(57, intArrayOf(3, 3, 1), Tile(2150, 3071), "Islands: Near Zul-Andra"), // Regicide required. Varp to see the fairy ring too I believe
		FELDIP_HILLS(59, intArrayOf(3, 2, 0), Tile(2385, 3035), "Feldip Hills: South of Castle Wars"),
		ENCHANTED_VALLEY(61, intArrayOf(3, 2, 3), Tile(3041, 4532), "Other Realms: Enchanted Valley"), // Instanced
		MORYTANIA_MYRE(63, intArrayOf(3, 2, 2), Tile(3469, 3431), "Morytania: Mort Myre"),
		ZANARIS(65, intArrayOf(3, 2, 1), Tile(2412, 4434), "Other Realms: Zanaris"),
		TZHAAR(67, intArrayOf(3, 1, 0), Tile(2437, 5126), "Dungeons: TzHaar area"),
		LEGENDS_GUILD(71, intArrayOf(3, 1, 2), Tile(2740, 3351), "Kandarin: Legends' Guild"),
		MISCELLANIA(75, intArrayOf(2, 0, 0), Tile(2513, 3884), "Islands: Miscellania"),
		YANILLE(77, intArrayOf(2, 0, 3), Tile(2538, 3127), "Kandarin: North-west of Yanille"),
		ARCEUUS_LIBRARY(81, intArrayOf(2, 0, 1), Tile(0, 0), "Arceuus Library"), // TODO CIS - Great Kourend.
		SINCLAIR_MANSION(87, intArrayOf(2, 3, 2), Tile(2705, 3576), "Kandarin: Sinclair Mansion (east)"),
		COSMIC_PLANE(91, intArrayOf(2, 2, 0), Tile(2075, 4848), "Other Realms: Cosmic Entity's plane"),
		KARAMJA(95, intArrayOf(2, 2, 2), Tile(2801, 3003), "Karamja: Tai Bwo Wannai Village"),
		CANIFIS(97, intArrayOf(2, 2, 1), Tile(3447, 3470), "Morytania: Canifis"),
		ISLAND_DRAYNOR(99, intArrayOf(2, 1, 0), Tile(3082, 3206), "Islands: South of Draynor Village"),
		APE_ATOLL(103, intArrayOf(2, 1, 2), Tile(2740, 2738), "Islands: Ape Atoll"),
		HAZELMERE(105, intArrayOf(2, 1, 1), Tile(2682, 3081), "Islands: Hazelmere's home"),
//		ABBY_NEXUS(107, intArrayOf(1, 0, 0), Tile(3037, 4763), "Other Realms: Abyssal Nexus"),
		//TODO re-enable sire access when it's done properly & drop rates are done.
		// TODO 109 empty string - PROBABLY POH. Is out of order on RS wiki by 1
		GORAK_PLANE(111, intArrayOf(1, 0, 2), Tile(3038, 5348), "Other Realms: Goraks' plane"),
		WIZARD_TOWER(113, intArrayOf(1, 0, 1), Tile(3108, 3149), "Misthalin: Wizards' Tower"),
		TOWER_OF_LIFE(115, intArrayOf(1, 3, 0), Tile(2658, 3230), "Kandarin: Tower of Life"),
		SINCLAIR_WEST(119, intArrayOf(1, 3, 2), Tile(2676, 3587), "Kandarin: Sinclair Mansion (west)"),
		GNOME_GLIDER_KARAMAJA(123, intArrayOf(1, 2, 0), Tile(2900, 3111), "Karamja: Gnome Glider"),
		EDGEVILLE(127, intArrayOf(1, 2, 2), Tile(3129, 3496), "Misthalin: Edgeville"),
		POLAR_HUNTER(129, intArrayOf(1, 2, 1), Tile(2744, 3719), "Kandarin: Polar Hunter area"),
		NARDAH(133, intArrayOf(1, 1, 3), Tile(3423, 3016), "Kharidian Desert: North of Nardah"),
		POISON_WASTE(135, intArrayOf(1, 1, 2), Tile(2213, 3099), "Islands: Poison Waste"),
		HOLLOWS_HIDEOUT(137, intArrayOf(1, 1, 1), Tile(3501, 9821, 3), "Dungeons: Myreque hideout under The Hollows"),
		;
	}
	
	val FAIRY_RING = intArrayOf(29495, 29560)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (ring in FAIRY_RING) {
			repo.onObject(ring) @Suspendable {
				when (it.interactionOption()) {
					1 -> teleport(it, FairyRings.ZANARIS, true)
					2 -> openCombinationPanel(it)
				}
			}
		}
		
		// Left wheel
		repo.onButton(398, 20) {
			// Counter clockwise
			if (left(it) == 0)
				it.player().varps().varbit(Varbit.FAIRY_RING_LEFT, 3)
			else
				it.player().varps().varbit(Varbit.FAIRY_RING_LEFT, left(it) - 1)
		}
		repo.onButton(398, 19) {
			// CLockwise
			if (left(it) == 3)
				it.player().varps().varbit(Varbit.FAIRY_RING_LEFT, 0)
			else
				it.player().varps().varbit(Varbit.FAIRY_RING_LEFT, left(it) + 1)
		}
		
		// Middle wheel
		repo.onButton(398, 22) {
			// Counter clockwise
			if (middle(it) == 0)
				it.player().varps().varbit(Varbit.FAIRY_RING_MIDDLE, 3)
			else
				it.player().varps().varbit(Varbit.FAIRY_RING_MIDDLE, middle(it) - 1)
		}
		repo.onButton(398, 21) {
			// CLockwise
			if (middle(it) == 3)
				it.player().varps().varbit(Varbit.FAIRY_RING_MIDDLE, 0)
			else
				it.player().varps().varbit(Varbit.FAIRY_RING_MIDDLE, middle(it) + 1)
		}
		
		// Right wheel
		repo.onButton(398, 24) {
			// Counter clockwise
			if (right(it) == 0)
				it.player().varps().varbit(Varbit.FAIRY_RING_RIGHT, 3)
			else
				it.player().varps().varbit(Varbit.FAIRY_RING_RIGHT, right(it) - 1)
		}
		repo.onButton(398, 23) {
			// CLockwise
			if (right(it) == 3)
				it.player().varps().varbit(Varbit.FAIRY_RING_RIGHT, 0)
			else
				it.player().varps().varbit(Varbit.FAIRY_RING_RIGHT, right(it) + 1)
		}
		
		// Teleport button
		repo.onButton(398, 26) {
			//it.player().debug("Varbits: %d, %d, %d", left(it), middle(it), right(it))
			val found = booleanArrayOf(false)
			for (entry in FairyRings.values()) {
				if (entry.varbitValues[0] == left(it) && entry.varbitValues[1] == middle(it) && entry.varbitValues[2] == right(it)) {
					teleport(it, entry, false)
					found[0] = true
					break
				}
			}
			if (!found[0]) {
				it.player().interfaces().closeById(398)
				it.messagebox("This combination does not lead anywhere.")
				openCombinationPanel(it)
			}
		}
		
		// Travel log preselections
		for (entry in FairyRings.values()) {
			repo.onButton(381, entry.childId) {
				it.player().varps().varbit(Varbit.FAIRY_RING_LEFT, entry.varbitValues[0])
				it.player().varps().varbit(Varbit.FAIRY_RING_MIDDLE, entry.varbitValues[1])
				it.player().varps().varbit(Varbit.FAIRY_RING_RIGHT, entry.varbitValues[2])
			}
		}
		
		repo.onInterfaceClose(398) {
			it.player().interfaces().closeById(381) // Close inventory too!
			it.player().varps().varbit(Varbit.FAIRY_RING_LEFT, 0)
			it.player().varps().varbit(Varbit.FAIRY_RING_MIDDLE, 0)
			it.player().varps().varbit(Varbit.FAIRY_RING_RIGHT, 0)
		}
	}
	
	// Shave down how much you have to write to obtain varbit values
	fun left(it: Script): Int {
		return it.player().varps().varbit(Varbit.FAIRY_RING_LEFT)
	}
	
	fun middle(it: Script): Int {
		return it.player().varps().varbit(Varbit.FAIRY_RING_MIDDLE)
	}
	
	fun right(it: Script): Int {
		return it.player().varps().varbit(Varbit.FAIRY_RING_RIGHT)
	}
	
	// Teleport to destination!
	@JvmStatic @Suspendable fun teleport(it: Script, entry: FairyRings, fastTele: Boolean) {
		val player = it.player()
		player.interfaces().closeById(398)
		player.interfaces().closeById(381) // Inv
		if (entry == FairyRings.FISHER_KING || entry == FairyRings.ENCHANTED_VALLEY || entry.tile.equals(0, 0)) {
			it.messagebox("This area is not accessible.")
			return
		}
		//Check to see if the player is teleblocked
		if (player.timers().has(TimerKey.TELEBLOCK)) {
			player.teleblockMessage()
			return
		}
		player.world().executeScript @Suspendable { s ->
			player.lock()
			val ring = player.attribOr<MapObj?>(AttributeKey.INTERACTION_OBJECT, null)
			if (ring != null) { // Walk up to it.
				if (!player.tile().equals(ring.tile())) {
					player.pathQueue().interpolate(ring.tile(), PathQueue.StepType.FORCED_WALK)
					s.delay(1)
				}
			}
			s.clearContext()
			s.delay(2)
			player.animate(3265, 30)
			player.graphic(569)
			s.delay(3)
			player.teleport(entry.tile)
			player.animate(3266)
			s.delay(1)
			player.unlock()
			player.pathQueue().clear()
		}
	}
	
	// Show the Travel log and combination interfaces.
	@JvmStatic @Suspendable fun openCombinationPanel(it: Script) {
		// Render the log.
		for (entry in FairyRings.values()) {
			it.player().write(InterfaceText(381, entry.childId, if (entry.targetName.length == 0) "" else "<br>" + entry.targetName))
		}
		it.player().interfaces().sendMain(398)
		it.player().interfaces().sendInventory(381)
	}
}