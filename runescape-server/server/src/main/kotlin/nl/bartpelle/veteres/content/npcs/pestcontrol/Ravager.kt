package nl.bartpelle.veteres.content.npcs.pestcontrol

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.minigames.pest_control.Breakable
import nl.bartpelle.veteres.content.minigames.pest_control.SearchesForBreakable
import nl.bartpelle.veteres.fs.MapDefinition
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Jason MacKeigan on 2016-09-08 at 4:05 PM
 */
object Ravager : SearchesForBreakable {
	
	@Suspendable override fun search(knight: Npc, script: Script, centerOfMap: Tile) {
		val mapDefinition = knight.world().definitions().get(MapDefinition::class.java, centerOfMap.region())
		
		for (breakableDoor in Breakable.DOORS) {
			//val door = PestControlNpc.searchForBreakable(knight, centerOfMap, mapDefinition, 14, breakableDoor, DoorPositionOffset.)
		}
	}
	
}