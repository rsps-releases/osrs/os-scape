package nl.bartpelle.veteres.content.npcs.godwars.saradomin

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.npcs.godwars.GwdLogic
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Bart on 6/11/2016
 */

object Zilyana {
	
	val QUOTES = arrayOf("Death to the enemies of the light!",
			"Slay the evil ones!",
			"Saradomin lend me strength!",
			"By the power of Saradomin!",
			"May Saradomin be my sword!",
			"Good will always triumph!",
			"Forward! Our allies are with us!",
			"Saradomin is with us!",
			"In the name of Saradomin!",
			"Attack! Find the Godsword!")
	
	@JvmStatic val ENCAMPMENT = Area(2888, 5257, 2908, 5276)
	@JvmStatic var lastBossDamager: Entity? = null
	
	@JvmStatic fun isMinion(n: Npc): Boolean {
		return n.id() in arrayOf(2206, 2207, 2208)
	}
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target, 50) && PlayerCombat.canAttack(npc, target)) {
			
			if (npc.timers().has(TimerKey.ZILY_SPEC_COOLDOWN)) {
				// just sit here for a sec lul
			} else {
				val melee_range = EntityCombat.canAttackMelee(npc, target, true)
				//if (EntityCombat.canAttackDistant(npc, target, true, 10))
				if (EntityCombat.attackTimerReady(npc)) {
					// Do a quote?
					if (npc.world().rollDie(3, 1)) {
						npc.sync().shout(npc.world().random(QUOTES))
					}
					
					// Attack the player
					if (melee_range) {
						if (npc.world().rollDie(2, 1)) {
							attackMelee(npc, target)
						} else {
							attackMage(it, npc, target)
						}
					}
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun attackMage(it: Script, npc: Npc, target: Entity) {
		npc.animate(6970)
		npc.timers().extendOrRegister(TimerKey.ZILY_SPEC_COOLDOWN, 7)
		npc.pathQueue().clear()
		
		npc.world().players().forEachInAreaKt(ENCAMPMENT, { p ->
			p.graphic(1221)
			
			for (i in 1..2) {
				if (EntityCombat.attemptHit(npc, p, CombatStyle.MAGIC)) {
					p.hit(npc, npc.world().random(27), 1).combatStyle(CombatStyle.MAGIC)
				} else {
					p.hit(npc, 0, 1).combatStyle(CombatStyle.MAGIC)
				}
			}
		})
	}
	
	fun attackMelee(npc: Npc, target: Entity) {
		npc.animate(6967)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(npc, 0, 1).combatStyle(CombatStyle.MELEE)
		}
		// If we're in melee distance it's actually classed as if the target hit us -- has an effect on auto-retal in gwd!
		if (GwdLogic.isBoss(npc.id())) {
			val last_attacked_map = npc.attribOr<HashMap<Entity, Long>>(AttributeKey.LAST_ATTACKED_MAP, HashMap<Entity, Long>())
			last_attacked_map.put(target, System.currentTimeMillis())
			npc.putattrib(AttributeKey.LAST_ATTACKED_MAP, last_attacked_map)
		}
	}
}