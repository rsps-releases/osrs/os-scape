package nl.bartpelle.veteres.content.areas.portsarim

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 2/10/2016.
 */

object Veos {
	
	val VEOS = 2147
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(VEOS) @Suspendable {
			it.chatPlayer("Hello, who are you?", 554)
			it.chatNpc("I am Veos and I've just arrived in Port Sarim from a<br>distant land.", VEOS, 589)
			
			when (it.optionsS("Select an Option", "Where are you from?", "That's great, can you take me there please?", "Good bye.")) {
				"Where are you from?" -> {
					it.chatPlayer("Where are you from?", 554)
					it.chatNpc("I am from the magnificent city of Great Kourend on<br>the continent of Zeah, to the far west.", VEOS, 555)
					
					talkZeah(it)
				}
				"That's great, can you take me there please?" -> takeMeThere(it)
				"Good bye." -> {
					it.chatPlayer("Good bye.", 588)
				}
			}
		}
		
		r.onNpcOption2(VEOS) @Suspendable {
			travelToZeah(it)
		}
	}
	
	@Suspendable fun talkZeah(it: Script) {
		var opt = it.optionsS("Select an Option", "Tell me more about Great Kourend.", "Tell me more about Zeah",
				"Why have I not heard about a whole new continent before now?", "Why have I never met anyone from Zeah before?", "Good bye.")
		
		while (true) {
			when (opt) {
				"Tell me more about Great Kourend." -> {
					it.chatPlayer("Can you tell me more about Great Kourend?", 554)
					it.chatNpc("Great Kourend is a magnificent city comprising of five<br>districts called Houses. The houses are ruled by the<br>" +
							"Dukes of Arceuus, Lovakengj, Shayzien, Piscarilius and<br>Hosidius.", VEOS, 591)
					it.chatNpc("Since our King died 20 years ago there has been no<br>real ruler and the city suffers for it.", VEOS, 589)
					
					// Restart routine with a new question! :)
					opt = it.optionsS("Select an Option", "That's great, can you take me there please?", "Tell me more about Zeah",
							"Why have I not heard about a whole new continent before now?", "Why have I never met anyone from Zeah before?", "Good bye.")
				}
				
				"Tell me more about Zeah" -> {
					it.chatPlayer("Tell me more about Zeah.", 554)
					it.chatNpc("Zeah is a huge continent far to the west of here full of<br>new adventures and hidden treasures. Sadly, the city of<br>" +
							"Great Kourend is besieged by lizard creatures and it is<br>not possible to get out of the city just yet.", VEOS, 591)
					
					// Restart routine with a new question! :)
					opt = it.optionsS("Select an Option", "That's great, can you take me there please?", "Tell me more about Great Kourend.",
							"Why have I not heard about a whole new continent before now?", "Why have I never met anyone from Zeah before?", "Good bye.")
					
				}
				
				"Why have I not heard about a whole new continent before now?" -> {
					it.chatPlayer("Hang on, I have been adventuring for years. Why<br>have I not heard about a whole new continent before<br>now?", 556)
					it.chatNpc("We have always had a rumour of far flung lands to the<br>east. The Eastern Lands we called them. As an<br>" +
							"explorer I wanted to sail east and bring proof back of<br>my discoveries.", VEOS, 591)
					it.chatPlayer("Why do you want to take proof that you have visited<br>Port Sarim back with you?", 555)
					it.chatNpc("Like most adventurers I seek fame and fortune, if I<br>can take some rare items back I can make my fortune.<br>" +
							"If I can take you back with me, I can have my fame.", VEOS, 569)
					
					// Restart routine with a new question! :)
					opt = it.optionsS("Select an Option", "That's great, can you take me there please?", "Tell me more about Great Kourend.",
							"Tell me more about Zeah", "Why have I never met anyone from Zeah before?", "Good bye.")
				}
				
				"Why have I never met anyone from Zeah before?" -> {
					it.chatPlayer("Why have I never met anyone from Zeah before?", 554)
					it.chatNpc("We have many rumours of explorers visiting this land<br>and stories of people from this land visiting ours.<br>" +
							"Perhaps you have met them but never known.", VEOS, 590)
					it.chatNpc("If your people were more proficient at sailing you may<br>have travelled here more often.", VEOS, 606)
					
					// Restart routine with a new question! :)
					opt = it.optionsS("Select an Option", "That's great, can you take me there please?", "Tell me more about Great Kourend.",
							"Tell me more about Zeah", "Why have I not heard about a whole new continent before now?", "Good bye.")
				}
				
				"That's great, can you take me there please?" -> {
					takeMeThere(it)
					return
				}
				
				"Good bye." -> {
					it.chatPlayer("Good bye.", 588)
					return
				}
			}
		}
	}
	
	@Suspendable fun takeMeThere(it: Script) {
		it.chatPlayer("That's great, can you take me there please?", 554)
		it.chatNpc("Certainly, are you ready to leave now?", VEOS, 567)
		
		when (it.optionsS("Select an Option", "Travel to Great Kourend", "Stay where you are")) {
			"Travel to Great Kourend" -> {
				it.chatPlayer("I am ready let's set sail.", 567)
				it.chatNpc("As you wish, I hope you don't get seasick, it is a long<br>voyage.", VEOS, 568)
				travelToZeah(it)
			}
			"Stay where you are" -> {
				it.chatPlayer("Actually I'd like to stay here.", 588)
				it.chatNpc("As you wish.", VEOS, 567)
			}
		}
	}
	
	@Suspendable fun travelToZeah(it: Script) {
		it.player().lock()
		it.player().interfaces().sendWidgetOn(174, Interfaces.InterSwitches.C)
		it.player().invokeScript(951)
		
		it.delay(4)
		it.message("You board the ship and sail to Great Kourend.")
		it.player().teleport(1824, 3695, 1)
		
		it.player().invokeScript(948, 0, 0, 0, 255, 50)
		it.player().interfaces().sendWidgetOn(246, Interfaces.InterSwitches.X)
		
		it.delay(4)
		it.player().unlock()
		it.player().interfaces().closeById(174) // Guess work added (31/8/16) unsure if correct
		it.player().interfaces().closeById(246)
	}
	
}