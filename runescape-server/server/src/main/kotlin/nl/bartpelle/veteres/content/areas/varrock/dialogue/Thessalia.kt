package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/1/2015.
 */

object Thessalia {
	
	val THESSALIA = 534
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(534) @Suspendable {
			/*it.chatNpc("Do you want to buy any fine clothes?", 534, 589)
			when (it.options("What have you got?", "No, thank you")) {
				1 -> {
					it.chatPlayer("What have you got?")
					when (it.options("Main shop (with trimmed capes)", "Untrimmed achievement capes")) {
						1 -> it.player().world().shop(13).display(it.player())
						2 -> it.player().world().shop(44).display(it.player())
					}
				}
				2 -> {
					it.chatPlayer("No, thank you.")
					it.chatNpc("Well, please return if you change your mind.", 534, 589)
				}
			}*/
		}
		r.onNpcOption2(534) {
			/*when (it.options("Main shop (with trimmed capes)", "Untrimmed achievement capes")) {
				1 -> it.player().world().shop(2).display(it.player())
				2 -> it.player().world().shop(44).display(it.player())
			}*/
		}
		r.onNpcOption3(534) {
			it.player().interfaces().sendMain(269)
		}
	}
	
}
