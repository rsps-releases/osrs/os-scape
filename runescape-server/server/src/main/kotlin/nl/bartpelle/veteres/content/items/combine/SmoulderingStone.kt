package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 9/28/2015.
 */
object SmoulderingStone {
	
	val SMOULDERING_STONE = 13233
	val INFERNAL_PICKAXE = 13243
	val INFERNAL_AXE = 13241
	
	val DRAGON_PICKAXE = 11920
	val DRAGON_AXE = 6739
	
	val DRAGON_BOOTS = 11840
	val RANGER_BOOTS = 2577
	val INFINITY_BOOTS = 6920
	
	val PRIMORDIAL_CRYSTAL = 13231
	val PRIMORDIAL_BOOTS = 13239
	
	val PEGASIAN_CRYSTAL = 13229
	val PEGASIAN_BOOTS = 13237
	
	val ETERNAL_CRYSTAL = 13227
	val ETERNAL_BOOTS = 13235
	
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//Check item charges
		repo.onItemOption3(INFERNAL_PICKAXE) { checkCharges(it, ItemAttrib.CHARGES) }
		
		//Making items
		repo.onItemOnItem(SMOULDERING_STONE, DRAGON_PICKAXE) @Suspendable { makePickaxe(it) }
		repo.onItemOnItem(SMOULDERING_STONE, DRAGON_AXE) @Suspendable { makeAxe(it) }
		repo.onItemOnItem(DRAGON_BOOTS, PRIMORDIAL_CRYSTAL) @Suspendable { makeBoots(it, PRIMORDIAL_BOOTS, DRAGON_BOOTS, PRIMORDIAL_CRYSTAL) }
		repo.onItemOnItem(RANGER_BOOTS, PEGASIAN_CRYSTAL) @Suspendable { makeBoots(it, PEGASIAN_BOOTS, RANGER_BOOTS, PEGASIAN_CRYSTAL) }
		repo.onItemOnItem(INFINITY_BOOTS, ETERNAL_CRYSTAL) @Suspendable { makeBoots(it, ETERNAL_BOOTS, INFINITY_BOOTS, ETERNAL_CRYSTAL) }
	}
	
	@Suspendable private fun makePickaxe(it: Script) {
		// Check requirements
		if (it.player().skills()[Skills.MINING] < 61 || it.player().skills()[Skills.SMITHING] < 85) {
			it.doubleItemBox("You need level 61 Mining and level 85 Smithing to make an infernal pickaxe.", SMOULDERING_STONE, DRAGON_PICKAXE)
			return
		}
		
		it.doubleItemBox("Are you sure you wish to convert your dragon pickaxe into an infernal pickaxe? " +
				"This cannot be reversed. Infernal pickaxes are untradable.", SMOULDERING_STONE, DRAGON_PICKAXE)
		
		if (it.options("Proceed with the infusion.", "Cancel.") == 1) {
			// Do some nice graphics
			it.animate(4513)
			it.player().graphic(1240)
			
			// Apply inv change
			val slot = ItemOnItem.slotOf(it, DRAGON_PICKAXE)
			if (it.player().inventory().remove(Item(DRAGON_PICKAXE), false, slot).success() &&
					it.player().inventory().remove(Item(SMOULDERING_STONE), false, slot).success())
				it.player().inventory().add(Item(INFERNAL_PICKAXE).property(ItemAttrib.CHARGES, 5000), false, slot)
			
			it.itemBox("You infuse the smouldering stone into the pickaxe to make an infernal pickaxe.", INFERNAL_PICKAXE)
		}
	}
	
	@Suspendable private fun makeAxe(it: Script) {
		// Check requirements
		if (it.player().skills()[Skills.WOODCUTTING] < 61 || it.player().skills()[Skills.FIREMAKING] < 85) {
			it.doubleItemBox("You need level 61 Woodcutting and level 85 Firemaking to make an infernal axe.", SMOULDERING_STONE, DRAGON_AXE)
			return
		}
		
		it.doubleItemBox("Are you sure you wish to convert your dragon axe into an infernal axe? " +
				"This cannot be reversed. Infernal axes are untradable.", SMOULDERING_STONE, DRAGON_AXE)
		
		if (it.options("Proceed with the infusion.", "Cancel.") == 1) {
			// Do some nice graphics
			it.animate(4513)
			it.player().graphic(1240)
			
			// Apply inv change
			val slot = ItemOnItem.slotOf(it, DRAGON_AXE)
			if (it.player().inventory().remove(Item(DRAGON_AXE), false, slot).success() &&
					it.player().inventory().remove(Item(SMOULDERING_STONE), false, slot).success())
				it.player().inventory().add(Item(INFERNAL_AXE).property(ItemAttrib.CHARGES, 5000), false, slot)
			
			it.itemBox("You infuse the smouldering stone into the axe to make an infernal pickaxe.", INFERNAL_AXE)
		}
	}
	
	@Suspendable private fun makeBoots(it: Script, result: Int, item1: Int, item2: Int) {
		val item1Name = Item(item1).name(it.player().world())
		val item2Name = Item(item2).name(it.player().world())
		val resultName = Item(result).name(it.player().world())
		
		// Check requirements
		if (it.player().skills().xpLevel(Skills.RUNECRAFTING) < 60 || it.player().skills().xpLevel(Skills.MAGIC) < 60) {
			it.doubleItemBox("You need level 60 Runecrafting and Magic to create $resultName", item1, item2) // no period
			return
		}
		
		it.doubleItemBox("Are you sure you wish to infuse the $item1Name and $item2Name to create $resultName? This can not be reversed.", item1, item2)
		
		if (it.options("Proceed with the infusion.", "Cancel.") == 1) {
			// Do some nice graphics
			if (item1 == DRAGON_BOOTS && item2 == PRIMORDIAL_CRYSTAL) {
				it.animate(4513)
				it.player().graphic(1240)
			} else {
				it.animate(4462)
				it.player().graphic(759)
			}
			
			// Apply inv change
			val slot = ItemOnItem.slotOf(it, item2)
			
			// Make sure they're the same models (fix by Jeramy)
			if (it.player().inventory().remove(Item(item1), false, slot).success() && it.player().inventory().remove(Item(item2), false, slot).success())
				it.player().inventory().add(Item(result), false, slot)
			
			it.addXp(Skills.MAGIC, 200.0)
			it.addXp(Skills.RUNECRAFTING, 200.0)
			it.itemBox("You successfully infuse the $item1Name and $item2Name to create $resultName.", result)
		}
	}
	
	@Suspendable private fun checkCharges(s: Script, attribute: ItemAttrib) {
		val item: Item = s.player().inventory()[s.player()[AttributeKey.ITEM_SLOT]]
		val cur = item.property(attribute)
		s.message("${item.name(s.player().world())}: $cur charges remaining.")
	}
	
}
