package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.util.LocationUtilities

/**
 * Created by Situations on 1/12/2016.
 */

object PirateHut {

	// Option 1 (open)
	@Suspendable fun PirateHutFirst(it: Script) {
		val player = it.player()
        val currX = player.tile().x
        val currZ = player.tile().z
		val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
        val instanced = player in MageBankInstance

        val northDoorTile = Tile(3041, 3959)
        val eastDoorTile = Tile(3044, 3956)
        val westDoorTile = Tile(3038, 3956)

        val eastDoor = obj.tile() == eastDoorTile || instanced && obj.tile() == LocationUtilities.dynamicTileFor(eastDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)
        val westDoor = obj.tile() == westDoorTile || instanced && obj.tile() == LocationUtilities.dynamicTileFor(westDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)
        val northDoor = obj.tile() == northDoorTile || instanced && obj.tile() == LocationUtilities.dynamicTileFor(northDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)

        val rotation = if (westDoor) 1 else if (northDoor) 2 else 3
        val spawnTile = if(westDoor) Tile(3037, 3956) else if(northDoor) Tile(3041, 3960) else Tile(3045, 3956)
        val spawnObj = MapObj(spawnTile, 1539, obj.type(), rotation)
		spawnObj.interactAble(false) // Because it's just a temporary door. Alternatively you could find the alt object-id with no "open" option.
        val x: Int = when(WildernessLevelIndicator.inside_pirates_hideout(player.tile())) {
            true -> {
                when {
                    northDoor -> player.tile().transform(0, 0).x
                    westDoor -> player.tile().transform(-1, 0).x
                    else -> player.tile().transform(1, 0).x
                }
            }
            else -> {
                0
            }
        }
        val z: Int = when(WildernessLevelIndicator.inside_pirates_hideout(player.tile())) {
            true -> {
                when {
                    northDoor -> player.tile().transform(0, 1).z
                    westDoor -> player.tile().transform(0, 0).z
                    else -> player.tile().transform(0, 0).z
                }
            }
            else -> {
                0
            }
        }
		if (!WildernessLevelIndicator.inside_pirates_hideout(it.player().tile())) {
			it.message("The door is locked.")
			return
		} else {
			// Inside
			if (!player.tile().equals(obj.tile())) {
				it.player().pathQueue().interpolate(obj.tile().x, obj.tile().z, PathQueue.StepType.FORCED_WALK)
				it.delay(1)
			}
		}

		it.message("You go through the door.")

		//Replace the object with an open door
		it.runGlobal(player.world()) @Suspendable { s ->
			val world = player.world()
			val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
			val spawned = spawnObj
			world.removeObj(old, false)
			world.spawnObj(spawned)
			s.delay(2)
			world.removeObjSpawn(spawned)
			world.spawnObj(old)
		}
		//Move the player outside of the pirate hut
		it.player().pathQueue().interpolate(x, z, PathQueue.StepType.FORCED_WALK)
	}

	// Option 2 (picklock)
    @Suspendable fun pirateHutSecond(it: Script) {
		val player = it.player()
        val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)

        val northDoorTile = Tile(3041, 3959)
        val eastDoorTile = Tile(3044, 3956)
        val westDoorTile = Tile(3038, 3956)
        val instanced = player in MageBankInstance

        val eastDoor = obj.tile() == eastDoorTile || instanced && obj.tile() == LocationUtilities.dynamicTileFor(eastDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)
        val westDoor = obj.tile() == westDoorTile || instanced && obj.tile() == LocationUtilities.dynamicTileFor(westDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)
        val northDoor = obj.tile() == northDoorTile || instanced && obj.tile() == LocationUtilities.dynamicTileFor(northDoorTile, MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank)

        val rotation = if (westDoor) 1 else if (northDoor) 2 else 3
        val spawnTile = if(westDoor) Tile(3037, 3956) else if(northDoor) Tile(3041, 3960) else Tile(3045, 3956)
        val spawnObj = MapObj(spawnTile, 1539, obj.type(), rotation)
		spawnObj.interactAble(false)

        val x: Int = when(WildernessLevelIndicator.inside_pirates_hideout(player.tile())) {
			true -> {
                when {
                    northDoor -> player.tile().transform(0, 0).x
                    westDoor -> player.tile().transform(-1, 0).x
                    else -> player.tile().transform(1, 0).x
                }
			}
			else -> {
                when {
                    northDoor -> player.tile().transform(0, 0).x
                    westDoor -> player.tile().transform(1, 0).x
                    else -> player.tile().transform(-1, 0).x
                }
			}
		}
		val z: Int = when(WildernessLevelIndicator.inside_pirates_hideout(player.tile())) {
            true -> {
                when {
                    northDoor -> player.tile().transform(0, 1).z
                    westDoor -> player.tile().transform(0, 0).z
                    else -> player.tile().transform(0, 0).z
                }
            }
            else -> {
                when {
                    northDoor -> player.tile().transform(0, -1).z
                    westDoor -> player.tile().transform(0, 0).z
                    else -> player.tile().transform(0, 0).z
                }
            }
        }
		//If the player is inside the hut
		if (WildernessLevelIndicator.inside_pirates_hideout(it.player().tile())) {
			it.message("The door is already unlocked.")
			return
		}
		if (!player.world().realm().isPVP && player.skills().xpLevel(Skills.THIEVING) < 39) {
			player.message("You need a Thieving level of 39 to pick lock this door.")
			return
		}
		//Check if the player has a lockpick
		if (player.inventory().has(1523)) {

			it.message("You attempt to pick the lock.")

			//Create a chance to picklock the door
			if (player.world().random(100) >= 50) {
				it.runGlobal(player.world()) @Suspendable { s ->
					val world = player.world()
					val old = MapObj(obj.tile(), obj.id(), obj.type(), obj.rot())
					val spawned = spawnObj
					world.removeObj(old, false)
					world.spawnObj(spawned)
					s.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}
				//Move the player outside of the pirate hut
				it.player().pathQueue().interpolate(x, z, PathQueue.StepType.FORCED_WALK)

				it.message("You manage to pick the lock.")
				//Add thieving experience for a successful lockpick
                if(!player.world().realm().isPVP)
				it.addXp(Skills.THIEVING, 22.0)
			} else {
				//Send the player a message
				it.message("You fail to pick the lock.")
			}
		} else {
			it.message("You need a lockpick for this lock.")
		}
	}
}