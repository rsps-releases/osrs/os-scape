package nl.bartpelle.veteres.content.skills.agility.rooftop

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.content.skills.agility.UnlockAgilityPet
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 5/19/2016.
 */
object ArdougneRooftop {
	
	val MARK_SPOTS = arrayOf(Tile(2656, 3318, 3))
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Wall climb
		r.onObject(11405, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().skills().level(Skills.AGILITY) >= 90) {
				it.player().lock()
				it.player().faceTile(it.player().tile() + Tile(0, 1))
				it.delay(1)
				it.player().animate(737, 15)
				it.delay(1)
				it.player().teleport(2673, 3298, 1)
				it.player().animate(737)
				it.delay(1)
				it.player().teleport(2673, 3298, 2)
				it.player().animate(737)
				it.delay(1)
				it.player.teleport(2671, 3299, 3)
				it.player.animate(2588)
				it.delay(1)
				it.addXp(Skills.AGILITY, 43.0)
				MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 90)
				it.player().unlock()
			} else {
				it.message("You need at least 90 Agility to attempt this course.")
			}
		})
		
		// Jump down roof
		r.onObject(11406, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().animate(2586, 15)
			it.player.sound(2462, 15)
			it.delay(1)
			it.player.animate(2588)
			it.player().teleport(2667, 3311, 1)
			it.delay(1)
			it.player().animate(2586, 15)
			it.player.sound(2462, 15)
			it.delay(1)
			it.player.animate(2588)
			it.player().teleport(2665, 3315, 1)
			it.delay(1)
			it.player().animate(2583, 15)
			it.delay(1)
			it.player.animate(2588)
			it.player().teleport(2665, 3318, 3)
			it.delay(1)
			it.addXp(Skills.AGILITY, 65.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 90)
			it.player().unlock()
		})
		
		// Plank
		r.onObject(11631, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lock()
			it.player().pathQueue().clear()
			it.player().sound(2495, 0, 4)
			it.player().pathQueue().interpolate(2656, 3318, PathQueue.StepType.FORCED_WALK)
			it.delay(1)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(2656, 3318))
			it.player().looks().resetRender()
			it.delay(1)
			it.addXp(Skills.AGILITY, 50.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 90)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11429, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().faceTile(Tile(2653, 3314))
			it.player().animate(2586, 15)
			it.player.sound(2462, 15)
			it.delay(1)
			it.player.animate(2588)
			it.player().teleport(2653, 3314, 3)
			it.delay(1)
			it.addXp(Skills.AGILITY, 21.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 90)
			it.player().unlock()
		})
		
		// Gap jump
		r.onObject(11430, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().faceTile(Tile(2651, 3309))
			it.player().animate(7133, 15)
			it.player.sound(2462, 15)
			it.delay(1)
			it.player.animate(2588)
			it.player().teleport(2651, 3309, 3)
			it.delay(1)
			it.addXp(Skills.AGILITY, 28.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 90)
			it.player().unlock()
		})
		
		// Steep roof
		r.onObject(11633, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (!it.player().tile().equals(2653, 3300, 3)) return@s // Stop people doing it over and over from wrong side
			it.delay(1)
			it.player().lockNoDamage()
			it.player().animate(753)
			it.player().looks().render(757, 757, 756, 756, 756, 756, -1)
			it.delay(1)
			it.player.sound(2451, 0, 3)
			it.player().pathQueue().clear()
			it.player().pathQueue().interpolate(2654, 3299, PathQueue.StepType.FORCED_WALK)
			it.player().pathQueue().interpolate(2656, 3297, PathQueue.StepType.FORCED_WALK)
			it.waitForTile(Tile(2656, 3297))
			it.delay(1)
			it.player().looks().resetRender()
			it.animate(759)
			it.delay(1)
			it.player().unlock()
			it.addXp(Skills.AGILITY, 57.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 90)
		})
		
		// Gap jump
		r.onObject(11630, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lock()
			it.delay(1)
			it.player().faceTile(it.player.tile() + Tile(1, 0))
			it.delay(1)
			it.player().animate(2586, 15)
			it.player.sound(2462, 15)
			it.delay(1)
			it.player.animate(2588)
			it.player().teleport(2658, 3298, 1)
			it.delay(2)
			it.player.pathQueue().interpolate(2661, 3298, PathQueue.StepType.FORCED_WALK)
			it.waitForTile(Tile(2661, 3298))
			it.delay(2)
			it.player.animate(741)
			it.player().forceMove(ForceMovement(0, 0, 2, -1, 15, 30, FaceDirection.EAST))
			it.delay(1)
			it.player.teleport(2663, 3297, 1)
			it.player.pathQueue().interpolate(2666, 3297, PathQueue.StepType.FORCED_WALK)
			it.waitForTile(Tile(2666, 3297))
			it.delay(3)
			it.player.animate(741)
			it.player().forceMove(ForceMovement(0, 0, 1, 0, 15, 30, FaceDirection.EAST))
			it.delay(1)
			it.player.teleport(2667, 3297, 1)
			it.player.animate(2586)
			it.delay(1)
			it.player.animate(2588)
			it.player.teleport(2668, 3297, 0)
			it.addXp(Skills.AGILITY, 529.0)
			MarksOfGrace.trySpawn(it.player(), MARK_SPOTS, 35, 90)
			it.player().unlock()
			
			// Woo! A pet!
			val odds = (31000.00 * it.player().mode().skillPetMod()).toInt()
			if (it.player().world().rollDie(odds, 1)) {
				UnlockAgilityPet.unlockGiantSquirrel(it.player())
			}
		})
	}
	
}