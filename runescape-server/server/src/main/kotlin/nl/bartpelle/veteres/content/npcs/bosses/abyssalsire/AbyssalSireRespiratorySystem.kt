package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import java.util.*

/**
 * Created by Situations on 10/11/2016.
 */

object AbyssalSireRespiratorySystem {
	
	val RESPIRATORY_SYSTEM = 5914
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcSpawn(RESPIRATORY_SYSTEM) {
			val respiratorySystem = it.npc()
			
			Optional.ofNullable(respiratorySystem.world().objById(26954, respiratorySystem.tile())).ifPresent {
				it.replaceWith(26953, respiratorySystem.world())
			}
		}
	}
	
	@JvmField val deathScript: Function1<Script, Unit> = @Suspendable {
		val respiratorySystem = it.npc()
		val abyssalSire = respiratorySystem.attribOr<Npc>(AttributeKey.OWNING_ABYSSAL_SIRE, 5886)
		val abyssalSireRespiratory = getRespiratorySystem(abyssalSire)
		
		Optional.ofNullable(it.npc().world().objById(26953, it.npc().tile())).ifPresent {
			it.replaceWith(26954, respiratorySystem.world())
		}
		
		abyssalSireRespiratory.remove(respiratorySystem)
		abyssalSire.putattrib(AttributeKey.ABYSSAL_SIRE_RESPIRATORY_SYSTEMS, abyssalSireRespiratory)
	}
	
	@JvmField
	val hitScript: Function1<Script, Unit> = @Suspendable {
		val respiratorySystem = it.npc()
		val abyssalSire = respiratorySystem.attribOr<Npc>(AttributeKey.OWNING_ABYSSAL_SIRE, 5886)
		
		//Start the Abyssal Sire Chamber Reset timer..
		abyssalSire.timers().addOrSet(TimerKey.ABYSSAL_SIRE_RESET_TIMER, 100)
	}
	
	fun putCombatState(respiratorySystem: ArrayList<Npc>, state: AbyssalSireState) {
		respiratorySystem.forEach { r -> r.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, state) }
	}
	
	fun putOwningAbyssalSire(respiratorySystem: ArrayList<Npc>, abyssalSire: Npc) {
		respiratorySystem.forEach { r -> r.putattrib(AttributeKey.OWNING_ABYSSAL_SIRE, abyssalSire) }
	}
	
	fun getRespiratorySystem(abyssalSire: Npc): ArrayList<Npc> =
			abyssalSire.attribOr<ArrayList<Npc>>(AttributeKey.ABYSSAL_SIRE_RESPIRATORY_SYSTEMS, ArrayList<Npc>())
	
	fun attemptToHeal(respiratorySystem: ArrayList<Npc>) {
		respiratorySystem.forEach { r -> if (r.maxHp() > r.hp()) r.heal(r.world().random(2)) }
	}
	
	fun checkRepositorySystem(abyssalSire: Npc) {
		//Check if the respiratory systems are all destroyed..
		val respiratorySize = AbyssalSireRespiratorySystem.getRespiratorySystem(abyssalSire)
		
		//If they are, initiate phase two! :-)
		if (respiratorySize.size == 0)
			AbyssalSirePhaseTwo.initPhaseTwo(abyssalSire)
	}
	
	
}
