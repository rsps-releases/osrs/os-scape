package nl.bartpelle.veteres.content.skills.agility

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.net.message.game.command.AnimateObject
import nl.bartpelle.veteres.net.message.game.command.SetMapBase
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 13/06/2016.
 */
object Barbarian {
	
	val ROPE_SWING = 23131
	val LOG_BALANCE = 23144
	val NET = 20211
	val LEDGE = 23547
	val LEDGE_LADDER = 16682
	val WALL = 1948
	val DUNGEONLADDER_DOWN = 16680
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(ROPE_SWING, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			val obj = it.interactionObject()
			if (it.player().tile().z < 3554) {
				it.player().message("You can't swing from here.")
				return@s
			}
			if (!it.player().tile().equals(2551, 3554)) { // Get in position
				it.player().walkTo(2551, 3554, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2551, 3554))
			}
			it.player().lockDelayDamage()
			it.player().write(SetMapBase(it.player(), obj.tile())) // set base
			it.player().write(AnimateObject(obj, 54)) // make rope pull back
			it.player().animate(751) // swing
			val success = successful(it.player())
			if (success) {
				it.player().forceMove(ForceMovement(0, 0, 0, -5, 30, 50, FaceDirection.SOUTH)) // move
				it.delay(1)
				it.player().write(AnimateObject(obj, 55)) // make the rope go forwards while we swing
				it.player().teleport(it.player().tile().transform(0, -5))
				putStage(it.player(), 1)
				it.addXp(Skills.AGILITY, 22.0)
			} else {
				it.player().forceMove(ForceMovement(0, 0, 0, -3, 30, 50, FaceDirection.SOUTH))
				it.delay(2)
				it.player().teleport(Tile(2552, 9951))
				it.player().animate(766, 5)
				it.delay(1)
				it.player().pathQueue().interpolate(2550, 9950, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2550, 9950))
			}
			it.player().unlock()
			if (!success) {
				it.player().hit(null, it.player().world().random(5..7)).block(false)
			}
		})
		repo.onObject(LOG_BALANCE, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (!it.player().tile().equals(2551, 3546)) { // Get in position
				it.player().walkTo(2551, 3546, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2551, 3546))
			}
			it.delay(1)
			it.player().lockDelayDamage()
			it.message("You walk carefully across the slippery log...")
			it.player().pathQueue().clear()
			val end = Tile(2541, 3546)
			it.player().pathQueue().interpolate(end, PathQueue.StepType.FORCED_WALK)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			val success = successful(it.player())
			if (success) {
				it.waitForTile(end)
				it.player().looks().resetRender()
				putStage(it.player(), 2)
				it.addXp(Skills.AGILITY, 13.7)
				it.message("...You make it safely to the other side.")
			} else {
				it.delay(3)
				it.player().pathQueue().clear()
				it.animate(771)
				it.message("...You loose your footing and fall into the water. Something bites you.")
				it.delay(1)
				it.player().looks().render(772, 772, 772, 772, 772, 772, -1)
				it.player().graphic(68)
				it.player().teleport(it.player().tile().transform(0, 1, 0))
				it.player().hit(null, it.player().world().random(5..7)).block(false)
				it.player().pathQueue().interpolate(2544, 3549, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2544, 3549))
				it.player().looks().resetRender()
			}
			it.player().unlock()
		})
		repo.onObject(NET, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().tile().x < 2539) {
				it.player().message("You can't climb the net from here.")
				return@s
			}
			Ladders.ladderUp(it, it.player().tile().transform(-1, 0, 1), true)
			putStage(it.player(), 4)
			it.addXp(Skills.AGILITY, 8.2)
		})
		repo.onObject(LEDGE, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lockDelayDamage()
			it.player().animate(753)
			it.player().faceTile(Tile(0, it.player().tile().z))
			it.player().pathQueue().clear()
			it.player().pathQueue().interpolate(2533, 3547, PathQueue.StepType.FORCED_WALK)
			it.player().looks().render(756, 756, 756, 756, 756, 756, -1)
			val success = successful(it.player())
			if (success) {
				it.delay(3)
				it.player().pathQueue().clear()
				it.player().pathQueue().interpolate(2532, 3546, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2532, 3546))
				it.player().looks().resetRender()
				putStage(it.player(), 8)
				it.addXp(Skills.AGILITY, 22.0)
			} else {
				it.delay(3)
				it.player().pathQueue().clear()
				it.player().looks().resetRender()
				it.player().animate(766)
				it.delay(1)
				it.player().teleport(2534, 3546, 0)
				it.player().hit(null, it.player().world().random(5..7)).block(false)
				it.player().pathQueue().interpolate(2536, 3546, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2536, 3546))
			}
			it.player().unlock()
		})
		repo.onObject(LEDGE_LADDER, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			Ladders.ladderDown(it, it.player().tile().transform(0, 0, -1), true)
		})
		repo.onObject(WALL, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			val obj = it.interactionObject()
			for (i in arrayOf(Tile(2536, 3553), Tile(2539, 3553), Tile(2542, 3553))) {
				val end = obj.tile().equals(2542, 3553)
				if (obj.tile().equals(i)) {
					if (!it.player().tile().equals(obj.tile().transform(-1, 0, 0))) {
						it.player().walkTo(obj.tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
						it.waitForTile(obj.tile().transform(-1, 0, 0))
					}
					it.player().lockDelayDamage()
					it.player().animate(839)
					it.player().forceMove(ForceMovement(0, 0, 2, 0, 0, 60, FaceDirection.EAST)) // move
					it.delay(1)
					it.player().teleport(it.player().tile().transform(2, 0))
					it.player().unlock()
					it.addXp(Skills.AGILITY, 13.7)
					if (end && it.player().attribOr<Int>(AttributeKey.BARBARIAN_COURSE_STATE, 0) == 15) {
						it.player().putattrib(AttributeKey.BARBARIAN_COURSE_STATE, 0)
						it.addXp(Skills.AGILITY, 46.2)
						
						// Woo! A pet!
						val odds = (22000.00 * it.player().mode().skillPetMod()).toInt()
						if (it.player().world().rollDie(odds, 1)) {
							UnlockAgilityPet.unlockGiantSquirrel(it.player())
						}
						
					}
				}
			}
		})
		repo.onObject(DUNGEONLADDER_DOWN, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			val ladder = it.interactionObject()
			if (ladder.tile() == Tile(2884, 3397)) { // Taverly ldader
				Ladders.ladderDown(it, Tile(2884, 9798), true)
			} else if (ladder.tile().equals(2547, 3551)) { // Barb ladder
				Ladders.ladderDown(it, Tile(2548, 9951), true)
			} else if (ladder.tile().equals(3088, 3571)) { // air obelisk
				Ladders.ladderDown(it, Tile(it.player().tile().x, it.player().tile().z + 6400), true)
			} else {
				it.player().message("This ladder does not seem to lead anywhere... Odd!")
			}
		})
	}
	
	// TODO make formula include exponential success growth for levels over requirement ending at 70
	@JvmStatic fun successful(player: Player): Boolean {
		val req = 35
		val endeffectiveness = 70
		val gap = player.skills().xpLevel(Skills.AGILITY) - req
		val failrate = 0.30
		return Math.random() >= (failrate - ((failrate / (endeffectiveness - req)) * gap))
	}
	
	private fun putStage(player: Player, stageBit: Int): Int {
		var stage = player.attribOr<Int>(AttributeKey.BARBARIAN_COURSE_STATE, 0)
		stage = stage or stageBit
		player.putattrib(AttributeKey.BARBARIAN_COURSE_STATE, stage)
		return stage
	}
}