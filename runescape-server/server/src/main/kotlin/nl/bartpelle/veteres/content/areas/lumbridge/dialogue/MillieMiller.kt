package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/7/2016.
 */

object MillieMiller {
	
	val MILLIE_MILLER = 4627
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(MILLIE_MILLER) @Suspendable {
			it.chatNpc("Hello Adventurer. Welcome to Mill Lane Mill. Can I<br>help you?", MILLIE_MILLER, 568)
			when (it.options("Who are you?", "What is this place?", "How do I mill flour?", "I'm fine, thanks.")) {
				1 -> {
					it.chatPlayer("Who are you?", 554)
					it.chatNpc("I'm Miss Millicent Miller the Miller of Mill Lane Mill.<br>Our family have been milling flour for generations.", MILLIE_MILLER, 568)
					it.chatPlayer("It's a good business to be in. People will always need<br>flour.", 589)
					it.chatPlayer("How do I mill flour?", 554)
					how_to_mill_flour(it)
				}
				2 -> {
					it.chatPlayer("What is this place?", 554)
					it.chatNpc("This is Mill Lane Mill. Millers of the finest flour in<br>RuneScape, and home to the Miller family for many<br>generations", MILLIE_MILLER, 569)
					it.chatNpc("We take grain from the field nearby and mill into flour.", 567)
					it.chatPlayer("How do I mill flour?", 554)
					how_to_mill_flour(it)
				}
				3 -> how_to_mill_flour(it)
				4 -> it.chatPlayer("I'm fine, thanks.", 567)
			}
		}
	}
	
	@Suspendable fun how_to_mill_flour(it: Script) {
		it.chatNpc("Making flour is pretty easy. First of all you need to<br>get some grain. You can pick some from wheat fields.<br>There is one just outside" +
				" the Mill, but there are many<br>others scattered across RuneScape. Feel free to pick", MILLIE_MILLER, 570)
		it.chatNpc("wheat from our field! There always seems to be plenty<br>of wheat there.", MILLIE_MILLER, 568)
		it.chatPlayer("Then I bring my wheat here?", 554)
		it.chatNpc("Yes, or one of the other mills in RuneScape. They all<br>work the same way. Just take your grain to the top<br>floor of the mill (up two ladders," +
				" there are three floors<br>including this one) and then place some grain into the", MILLIE_MILLER, 570)
		it.chatNpc("hopper.", MILLIE_MILLER, 567)
		it.chatNpc("Then you need to start the grinding process by pulling<br>the hopper lever. You can add more grain, but each<br>time you add grain you have to pull" +
				" the hopper lever<br>again.", MILLIE_MILLER, 570)
		it.chatPlayer("So where does the flour go then?", 554)
		it.chatNpc("The flour appears in this room here, you'll need a pot<br>to put the flour into. One pot will hold the flour made<br>by one load of grain", MILLIE_MILLER, 569)
		it.chatNpc("And that's it! You now have some pots of finely ground<br>flour of the highest quality. Ideal for making tasty cakes<br>or delicous bread. I'm not a cook" +
				" so you'll have to ask a<br>cook to find out how to bake things.", MILLIE_MILLER, 570)
		it.chatPlayer("Great! Thanks for your help.", 567)
	}
}
