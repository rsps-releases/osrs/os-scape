package nl.bartpelle.veteres.content.skills.crafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.itemOptions
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 12/16/2015.
 */

object Moulds {
	
	enum class moulds(val mould: Int, val jewellery: Int, val gem: Int, val lvl: Int, val exp: Double, val child: Int) {
		//RINGS
		GOLD_RING(1592, 1635, -1, 5, 15.0, 7),
		SAPPHIRE_RING(1592, 1637, 1607, 20, 40.0, 8),
		EMERALD_RING(1592, 1639, 1605, 27, 55.0, 9),
		RUBY_RING(1592, 1641, 1603, 34, 70.0, 10),
		DIAMOND_RING(1592, 1643, 1601, 43, 85.0, 11),
		DRAGONSTONE_RING(1592, 1645, 1615, 55, 100.0, 12),
		ONYX_RING(1592, 6575, 6573, 67, 115.0, 13),
		ZENYTE_RING(1592, 19538, 19493, 89, 150.0, 14),
		SLAYER_RING(1592, 11866, 4155, 75, 15.0, 15),
		//NECKLACES
		GOLD_NECKLACES(1597, 1654, -1, 6, 20.0, 21),
		SAPPHIRE_NECKLACES(1597, 1656, 1607, 22, 55.0, 22),
		EMERALD_NECKLACES(1597, 1658, 1605, 29, 60.0, 23),
		RUBY_NECKLACES(1597, 1660, 1603, 40, 75.0, 24),
		DIAMOND_NECKLACES(1597, 1662, 1601, 56, 90.0, 25),
		DRAGONSTONE_NECKLACES(1597, 1664, 1615, 72, 105.0, 26),
		ONYX_NECKLACES(1597, 6577, 6573, 82, 120.0, 27),
		ZENYTE_NECKLACE(1597, 19535, 19493, 92, 165.0, 28),
		//AMULETS
		GOLD_AMULETS(1595, 1673, -1, 8, 30.0, 34),
		SAPPHIRE_AMULETS(1595, 1675, 1607, 24, 65.0, 35),
		EMERALD_AMULETS(1595, 1677, 1605, 31, 70.0, 36),
		RUBY_AMULETS(1595, 1679, 1603, 50, 85.0, 37),
		DIAMOND_AMULETS(1595, 1681, 1601, 70, 100.0, 38),
		DRAGONSTONE_AMULETS(1595, 1683, 1615, 80, 150.0, 39),
		ONYX_AMULETS(1595, 6579, 6573, 90, 165.0, 40),
		ZENYTE_AMULETS(1595, 19501, 19493, 98, 200.0, 41),
		//BRACELETS
		GOLD_BRACELET(11065, 11068, -1, 7, 25.0, 47),
		SAPPHIRE_BRACELET(11065, 11071, 1607, 23, 60.0, 48),
		EMERALD_BRACELET(11065, 11076, 1605, 30, 65.0, 49),
		RUBY_BRACELET(11065, 11085, 1603, 42, 80.0, 50),
		DIAMOND_BRACELET(11065, 11092, 1601, 58, 95.0, 51),
		DRAGONSTONE_BRACELET(11065, 11115, 1615, 74, 110.0, 52),
		ONYX_BRACELET(11065, 11130, 6573, 84, 125.0, 53),
		ZENYTE_BRACELET(11065, 19532, 19493, 95, 180.0, 54);
		
		companion object {
			fun get(child: Int): moulds? {
				moulds.values().forEach {
					if (it.child == child)
						return it
				}
				return null
			}
		}
	}
	
	val EMPTY_BUCKET = 3727
	val BUCKET_OF_SAND = 1783
	val SODA_ASH = 1781
	val MOLTEN_GLASS = 1775
	val MOULDS = intArrayOf(1592, 1595, 1597, 11065)
	val GOLD_BAR = 2357
	val FURNACES = intArrayOf(4304, 6189, 11010, 11666, 12100, 12809, 18497, 24009, 26814, 30021, 30510, 36956, 37651, 16469)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		val handle: Function1<Script, Unit> = s@ @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.messagebox("That's a bit pointless is it not...?")
				return@s
			}
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			if (MOULDS.contains(item)) {
				it.player().interfaces().sendMain(446)
			}
			if (item == GOLD_BAR) {
				it.player().message("You need to use a mould on the furnace in order to smelt jewelry.")
			}
			
			//Crafting of molten glass (item -> object)
			if (item == BUCKET_OF_SAND || item == SODA_ASH) {
				if (it.player().inventory().contains(Item(BUCKET_OF_SAND))) {
					if (it.player().inventory().contains(Item(SODA_ASH))) {
						var amt = 1
						if (it.player().inventory().count(SODA_ASH) > 1) {
							amt = it.itemOptions(Item(MOLTEN_GLASS, 175), "How many would you like to make?", offsetX = 9)
						}
						
						while (amt-- > 0) {
							it.animate(899)
							it.delay(2)
							if (it.player().inventory().remove(Item(BUCKET_OF_SAND), true).failed()
									|| it.player().inventory().remove(Item(SODA_ASH), true).failed())
								return@s
							it.player().inventory().add(Item(EMPTY_BUCKET), true)
							it.player().inventory().add(Item(MOLTEN_GLASS), true)
							it.addXp(Skills.CRAFTING, 20.0)
							it.delay(2)
						}
					} else {
						it.messagebox("You need some soda ash to make glass.")
					}
				} else {
					it.messagebox("You need some sand to make glass.")
				}
				
			} else {
				it.message("Nothing interesting happens.")
			}
		}
		for (id in FURNACES)
			repo.onItemOnObject(id, handle)
	}
	
	@Suspendable fun craft(it: Script, jewellery: Int, gem: Int, lvl: Int, exp: Double, amt: Int, mould: Int) {
		var amt = amt
		
		it.player().interfaces().closeById(446)
		
		if (it.player().skills().level(Skills.CRAFTING) < lvl) {
			it.message("You need a Crafting level of $lvl to make this.")
			return
		}
		
		if (it.player().varps().varbit(Varbit.RING_BLING) == 0 && jewellery == 11866) {
			it.message("You have not unlocked the ability to craft this ring.")
			return
		}
		
		while (amt-- > 0 && (gem == -1 || it.player().inventory().contains(gem)) && it.player().inventory().contains(GOLD_BAR) && it.player().inventory().contains(mould)) {
			it.player().animate(899)
			it.delay(3)
			if (gem != -1) {
				if (it.player().inventory().remove(Item(gem), true).failed())
					break
			}
			if (it.player().inventory().remove(Item(GOLD_BAR), true).failed())
				break
			it.player().inventory().add(Item(jewellery), true)
			it.addXp(Skills.CRAFTING, exp)
			it.delay(1)
		}
	}
	
	@JvmStatic @ScriptMain fun interfaceButtons(r: ScriptRepository) {
		moulds.values().forEach { mould ->
			r.onButton(446, mould.child) @Suspendable {
				when (it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)) {
					1 -> {
						craft(it, mould.jewellery, mould.gem, mould.lvl, mould.exp, 1, mould.mould)
					}
					2 -> {
						craft(it, mould.jewellery, mould.gem, mould.lvl, mould.exp, 5, mould.mould)
					}
					3 -> {
						craft(it, mould.jewellery, mould.gem, mould.lvl, mould.exp, 10, mould.mould)
					}
					4 -> {
						craft(it, mould.jewellery, mould.gem, mould.lvl, mould.exp, it.inputInteger("Enter amount:"), mould.mould)
					}
				}
			}
		}
	}
}