package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-11.
 */

object Leatherworker {
	
	val Leatherworker = 6058
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(Leatherworker) @Suspendable {
			it.chatPlayer("Hello.", 567)
			it.chatNpc("Can I help you?", Leatherworker, 567)
			dialogueOption(it)
		}
	}
	
	@Suspendable fun dialogueOption(it: Script) {
		when (it.options("What do you do here?", "No thanks.")) {
			1 -> {
				it.chatPlayer("What do you do here?", 567)
				it.chatNpc("Well, I can cure plan cowhides into pieces of leather ready for crafting.", Leatherworker, 568)
				it.chatNpc("I work with ordinary, hard or dragonhide leather and also snakeskin. What would you like to" +
						" know about?", Leatherworker, 568)
				when (it.options("Ordinary leather.", "Hard leather.", "Snakeskin.", "Dragonhide leather.", "I'm bored of" +
						" leather already.")) {
					1 -> ordinaryLeather(it)
					2 -> hardLeather(it)
					3 -> snakeskin(it)
					4 -> dragonhideLeather(it)
					5 -> boredAlready(it)
				}
			}
			2 -> {
				it.chatPlayer("No thanks.", 588)
				it.chatNpc("Suit yourself.", Leatherworker, 588)
			}
		}
	}
	
	@Suspendable fun ordinaryLeather(it: Script) {
		it.chatPlayer("Ordinary leather.", 588)
		it.chatNpc("I can work one piece of cow hide into ordinary leather. It only costs one piece of gold.", Leatherworker, 568)
		it.chatNpc("Is there anything else I can help you with?", Leatherworker, 567)
		dialogueOption(it)
	}
	
	@Suspendable fun hardLeather(it: Script) {
		it.chatPlayer("Head leather.", 588)
		it.chatNpc("Hard leather is made by specially treating normal leather by wetting and drying it several times.", Leatherworker, 568)
		it.chatNpc("It's then treated with pine oils. It's harder wearing than ordinary leather.", Leatherworker, 568)
		it.chatNpc("Is there anything else I can help you with?", Leatherworker, 567)
		dialogueOption(it)
	}
	
	@Suspendable fun snakeskin(it: Script) {
		it.chatPlayer("Snake skins.", 588)
		it.chatNpc("I can work on piece of snake hide into a snakeskin. It only costs 15 pieces of gold. Snakeskin armour is" +
				"great for intermediate level rangers and provides more defence than hard and studded leather.", Leatherworker, 570)
		it.chatNpc("Is there anything else I can help you with?", Leatherworker, 567)
		dialogueOption(it)
	}
	
	@Suspendable fun dragonhideLeather(it: Script) {
		it.chatPlayer("Dragonhide leather.", 588)
		it.chatNpc("Dragon leather is made from dragonhides. One piece of hide can be worked into one piece of dragon" +
				"leather. It costs 20 coins each piece.", Leatherworker, 569)
		it.chatNpc("Dragon leather is head-wearing and has excellent flexibility. You can make strong and supple armour" +
				"with this.", Leatherworker, 569)
		it.chatNpc("Is there anything else I can help you with?", Leatherworker, 567)
		dialogueOption(it)
	}
	
	@Suspendable fun boredAlready(it: Script) {
		it.chatPlayer("I'm bored of leather already.", 562)
		it.chatNpc("You may be bored now, but an arrow in the backside will soon fix that!", Leatherworker, 589)
	}
}
