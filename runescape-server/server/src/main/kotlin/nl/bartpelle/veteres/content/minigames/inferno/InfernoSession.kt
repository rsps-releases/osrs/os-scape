package nl.bartpelle.veteres.content.minigames.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.achievements.pvp.PVPDiaryHandler
import nl.bartpelle.veteres.content.areas.tzhaar.InfernoLeaderboard
import nl.bartpelle.veteres.content.areas.tzhaar.TzhaarKetKeh
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.npcs.inferno.TzKalZuk
import nl.bartpelle.veteres.content.npcs.inferno.controllers.InfernoPillarController
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.instance.InstancedMap
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.util.Misc
import nl.bartpelle.veteres.util.Varbit
import java.util.*

/**
 * Created by Mack on 7/21/2017.
 */
class InfernoSession(var wave: Int, var player: Player, var world: World) {
    
    /**
     * The flag telling the active state of the session.
     */
    var active = false
    
    /**
     * A flag checking if the session paused.
     */
    var resuming = false
    
    /**
     * A flag checking if we have removed all the pillars. This becomes relevant in wave 67 and on so if for some reason
     * the server hasn't removed them at wave 67 we'll continue to check this flag until it does.
     */
    var deletedPillars = false
    
    /**
     * A collection of active npcs in this wave.
     */
    var activeNpcs: ArrayList<Npc> = ArrayList()
    
    /**
     * The best recorded run time the player has achieved in the inferno.
     */
    var bestRunTime: Long = 0

    /**
     * The amount of tokkul we reward the player with upon the session ending.
     */
    var tokkul = 0
    
    /**
     * Our starting time, in milliseconds.
     */
    var startingTime: Long = 0

    /**
     * The inferno map instance.
     */
    var area: InstancedMap? = null
    
    /**
     * The current wave the player is on. This object allows us to
     * manipulate / display any wave related information we need to.
     */
    var currentWave: Optional<InfernoWave> = Optional.empty()
    
    /**
     * The collection of active pillars within the session.
     */
    val activePillars: HashMap<Npc, MapObj?> = HashMap()
    
    /**
     * The glyph shield for this instance.
     */
    var ancestralGlyph: Npc? = null
    
    /**
     * The wave delay we send to give a buffer time between the waves.
     */
    val delay = if (wave == 69) 12 else 8
    
    /**
     * A flag checking if the player has reached the final boss.
     */
    var finalBoss = false

    /**
     * Starts the inferno session at the desired wave offset.
     */
    fun start(wave: Int, resuming: Boolean) {
        player.clearDamagers() // Fixes #841
    
        //Close the fading.
        player.interfaces().closeById(174)
        
        //officially "starts" the session.
        this.active = true
        
        //Assign our class wave var to the parameter so we can select which wave we wish to start on.
        this.wave = wave
    
        //Assigning the resuming state.
        this.resuming = resuming
        
        //Grab the player's best run time within the map or return zero.
        this.bestRunTime = player.attribOr(AttributeKey.INFERNO_RUNTIME, 0)
        
        //Drop in if not resuming session.
        if (!resuming) {
            player.animate(4367)
            player.sound(2672, 2, 0)
        } else {
            player.message("Your inferno session will resume in 5 seconds.")
        }

        //Stash this session instance into the player's map
        player.putattrib(AttributeKey.INFERNO_SESSION, this)

        //Gets the map info.
        area = world.allocator().allocate(64, 64, Tile(2240, 5312)).get()
        val map = area!!

        //Set our map characteristics
        map.setCreatedFor(player)
        map.isMulti = true
        map.set(0, 0, InfernoContext.startTile, InfernoContext.startTile.transform(64, 64), 0, true)
        map.setEntitiesAgressiveInArea(true)
        map.setDeallocateOnDeath(true)
        map.setDeallocatesOnLogout(true)
        map.setDangerous(false)
	    map.setLargeViewPort(true)
        map.setIdentifier(InstancedMapIdentifier.INFERNO)
        
        spawnPillars(map)
        
        // Put the roof back
        player.varps().varbit(Varbit.INFERNO_BOSS_ROOF, 0)
    
        //Teleport the player and start the session
        player.teleport(map.center())
    
        player.unlock()
        
        startingTime = System.currentTimeMillis()
        
        //Spawns the initial wave calling.
        player.world().server().scriptExecutor().executeScript(player.world(), @Suspendable { s ->
            s.delay(6)
            process(s)
        })
    }
    
    /**
     * Handles our npc death actions.
     */
    @Suspendable fun handleNpcDeath(npc: Npc) {
    
        //Unregister the npc we killed.
        npc.world().unregisterNpc(npc)
    
        //Handle our controller on death actions.
        if (npc.attribOr<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER, null) != null) {
            npc.attrib<InfernoNpcController>(AttributeKey.INFERNO_CONTROLLER).triggerPlayerKilledDeath()
        }
        
        if (activeNpcs.contains(npc)) {
            activeNpcs.remove(npc)
            
            //Increment the wave if we have killed all the npcs.
            if (activeNpcs.size == 0) {
                
                player.message("Wave completed!")
                
                //Increment our wave counter.
                this.wave++
            }
        }
    }
    
    /**
     * The "engine" behind inferno session.
     */
    @Suspendable fun process(s: Script) {
        var cycles = 0
        
        while (active) {
            if (!player.tile().inArea(area)) {
                end(false)
                break
            }
            
            if (wave == 69) {
                if (cycles > 0 && cycles % 400 == 0) {
                    TzKalZuk.spawnRangeAndMager(player, ancestralGlyph)
                }
            }
            
            if (cycles % 10 == 0) {
                if (InfernoContext.shouldAdvanceWave(player)) {
                    if (!finalBoss) {
                        spawnWave()
                    }
                }
            }
            
            s.delay(2)
            cycles++
        }
    }
    
    /**
     * Spawns the next wave according to the wave ptr.
     */
    @Suspendable private fun spawnWave() {
        
        //If not active then let's just not.
        if (!active) {
            return
        }
    
        //Collapse any remaining pillars.
        if ((wave in 67..69) && !deletedPillars) {
            
            var temp: HashMap<Npc, MapObj?> = activePillars
            
            for (pillar in temp.keys) {
            
                clearPillar(pillar)
            }
            temp.clear()
            activePillars.clear()
            deletedPillars = true
        }
        
        //Stash our wave into the saved map.
        player.putattrib(AttributeKey.INFERNO_SAVED_WAVE, wave)
        
        //If the player has the log out requesting pending we stop the next wave from being sent.
        if (player.attribOr(AttributeKey.INFERNO_LOGOUT_WARNING, false)) {
            
            return
        }

        //Increment and get the wave info.
        this.currentWave = Optional.of(InfernoContext.waveData[wave - 1])

        //send the wave message and set the highest round if needed.
        player.message("<col=FF0000>Wave: $wave")
        
       //If the final wave then let's do the cutscene otherwise spawn regularly.
        if (!finalBoss && wave == 69) {
            InfernoCutscene.start(player)
            finalBoss = true
        } else {

            player.world().server().scriptExecutor().executeScript(player.world(), s@ @Suspendable {
        
                if (currentWave.isPresent) {
            
                    it.delay(delay)
            
                    //spawn the wave
                    currentWave.get().spawnWave(player, this)
                }
            })
        }
    }

    /**
     * The actions we should handle upon ending the inferno session.
     */
    fun end(completed: Boolean) {
        
        //switch the session to false to stop our ticking.
        active = false
    
        player.teleport(InfernoContext.endTile)
    
        //Finish up the session by stopping our stopwatch and formatting.
        val time: Long = (System.currentTimeMillis() - startingTime)
        val runText = Misc.formatLongAsHMS(time)
        var bestText = "N/A"
    
        //If they won give them the loot cape.
        if (completed) {
            player.inventory().addOrDrop(Item(21295), player)
            
            if (AchievementDiary.diaryHandler(player) is PVPDiaryHandler) {
                AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(player, 39))
            }
      
            //Reassign best run time if it meets the condition.
            if (time < bestRunTime || bestRunTime == 0L) {
                bestRunTime = time
                player.putattrib(AttributeKey.INFERNO_RUNTIME, bestRunTime)
                
                //Add the entry to the leaderboard repository for display purposes.
               InfernoLeaderboard.save(player)
                player.message("<col=FF0000>New personal best!")
            }
        }
        
        //Call our best text, if available.
        if (player.attribOr<Long>(AttributeKey.INFERNO_RUNTIME, 0L) != 0L) {
            bestText = Misc.formatLongAsHMS(bestRunTime)
        }
        
        player.message("<col=FF0000>You lasted $runText in The Inferno! Best: $bestText")
        
        //If the world is W2 then we just give them the hardcoded final wave tokkul amount. It's all or nothing.
        if (player.world().realm().isPVP && completed) {
            tokkul = 28_704
        } else if (player.world().realm().isOSRune || player.world().realm().isRealism) {
            
            //Calculate our rewards and award the player.
            tokkul = calculateTokkulReward()
        }
    
        //Award the player the tokkul.
        player.inventory().addOrDrop(Item(6529, tokkul), player)
    
        //Finalize the cleanup by clearing any fields we may need to.
        cleanup()
        
        //Send our outro dialogue.
        player.executeScript @Suspendable { s ->
            
            if (completed) {
                s.chatNpc("You are very impressive for a JalYt. You managed to defeat TzKal-Zuk! Please accept this cape as a token of appreciation.", TzhaarKetKeh.CHATTER)
                s.doubleItemBox("This infernal cape is imbued with the strength of TzKal-Zuk. Use the power of this cape wisely JalYt. Here, take some Tokkul too.", 21295, 6529)
            } else if (!player.world().realm().isPVP && wave > 10 && !completed) {
                s.chatNpc("Well done in the Inferno, you can take this Tokkul as a reward.", TzhaarKetKeh.CHATTER)
            } else {
                s.chatNpc("Not a very good attempt JalYt. Better luck next time.", TzhaarKetKeh.CHATTER)
            }
        }
    }
    
    /**
     * Performs any last minute removals that is required to finalizing the session.
     */
    fun cleanup() {
        player.unlock()
    
        //reset any attributes we utilized in the session.
        player.interfaces().closeById(InfernoContext.TZKAL_ZUK_OVERLAY)
        player.clearattrib(AttributeKey.INFERNO_SESSION)
        player.clearattrib(AttributeKey.INFERNO_SAVED_WAVE)
        player.clearattrib(AttributeKey.INFERNO_LOGOUT_WARNING)
    }
    
    /**
     * Spawns our pillars that are able to be attacked by enemy mobs.
     */
    fun spawnPillars(area: InstancedMap?) {
        
        //reset the varbit info
        player.varps().varbit(InfernoPillars.FIRST.varbit(), 0)
        player.varps().varbit(InfernoPillars.SECOND.varbit(), 0)
        player.varps().varbit(InfernoPillars.THIRD.varbit(), 0)
        
        val spawns: ArrayList<Tile> = arrayListOf(Tile(area!!.center().x + 2, area.center().z - 9), Tile(area.center().x + 7, area.center().z + 7), Tile(area.center().x - 15, area.center().z + 4))
        
        for (i in InfernoPillars.FIRST.objectId()..InfernoPillars.THIRD.objectId()) {
            val pillar = Npc(7709, world, spawns[i - InfernoPillars.FIRST.objectId()])
            val obj: MapObj? = MapObj(spawns[(i - InfernoPillars.FIRST.objectId())], i, 10, 0)
            
            player.world().spawnObj(obj)
            player.world().registerNpc(pillar)
            pillar.putattrib(AttributeKey.INFERNO_CONTROLLER, InfernoPillarController(pillar, this))
            activePillars.put(pillar, obj)
        }
    }
    
    /**
     * Removes the pillar under the condition of it dying or
     * if it is transferring to wave 67 which marks the first jad wave.
     */
    fun clearPillar(pillar: Npc) {
    
        val obj: MapObj? = activePillars[pillar]
        
        pillar.animate(7561)
        
        player.world().executeScript @Suspendable { s ->
            
            s.delay(1)
            player.world().removeObj(obj)
            pillar.sync().transmog(7710)
            
            s.delay(2)
            activePillars.remove(pillar)
            player.world().unregisterNpc(pillar)
        }
        
        player.message("<col=FF0000>A pillar has fallen!")
    }
    
    /**
     * Gets the tokkul reward scaling from the wave the player reached.
     */
    fun calculateTokkulReward(): Int {
        var mod = 4
        var base = 104
        
        if (wave < 3) return 0 //stops kids from being cute
        
        return ((wave * mod) * base)
    }
    
    /**
     * Gets the player object from the instance.
     */
    fun player(): Player {
        return player
    }
    
    /**
     * Returns the pillar element at the specified index.
     */
    fun pillarAt(index: Int): Npc? {
        return (activePillars.keys.elementAt(index))
    }
    
    /**
     * Returns the current wave the player is on.
     */
    fun currentWave(): Optional<InfernoWave> {
        return currentWave
    }
}