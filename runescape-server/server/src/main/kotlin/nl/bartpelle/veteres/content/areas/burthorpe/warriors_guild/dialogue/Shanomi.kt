package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/8/2015.
 */

object Shanomi {
	
	val SHANOMI = 2462
	
	val message = arrayListOf("Those things which cannot be seen, perceive them.",
			"Do nothing which is of no use.",
			"Think not dishonestly.",
			"The Way in training is.",
			"Gain and loss between you must distinguish.",
			"Trifles pay attention even to.",
			"Way of the warrior this is.",
			"Acquainted with every art become.",
			"Ways of all professions know you.")
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(SHANOMI) @Suspendable {
			it.chatNpc("Greetings, ${it.player().name()}. Welcome you are in the test of combat.", SHANOMI, 590)
			when (it.options("What do I do here?", "Where do the machines come from?", "What items can I get from here?", "Bye!")) {
				1 -> {
					it.chatPlayer("What do I do here?")
					it.chatNpc("A spare suit of plate armour need you will. Full helm, plate leggings and platebody, yes? Placing it in the centre of the magical machines you will" +
							" be doing. KA-POOF! The armour, it attack most furiously as if", SHANOMI, 590)
					it.chatNpc("alive! Kill it you must, yes.", SHANOMI, 590)
					it.chatPlayer("So I use a full set of armour on the centre plate of the machines and it will animate it? Then I have to kill my our armour... how bizarre!")
					it.chatNpc("Yes. It is as you are saying. For this earn tokens you will. Also gain experience in combat you will. Trained long and here here have I.", SHANOMI, 590)
					it.chatPlayer("You're not from around here are you...?")
					it.chatNpc("It is as you say.", SHANOMI, 590)
					it.chatPlayer("So will I lose my armour?")
					it.chatNpc("Lose armour you will if damaged too much it becomes. Rare this is, but still possible. If kill you the armour does, also lose armour you will.", SHANOMI, 590)
					it.chatPlayer("So, occasionally I might lose a bit because it's being based about and I'll obviously lose it if I die.. that it?")
					it.chatNpc("It is as you say.", SHANOMI, 590)
				}
				2 -> {
					it.chatPlayer("Where do the machines come from?")
					it.chatNpc("Make them I did, with magics.", SHANOMI, 590)
					it.chatPlayer("Magic, in the Warrior's Guild?")
					it.chatNpc("A skilled warrior also am I. Harrallak mistakes does not make. Potential in my invention he sees and opportunity grasps.", SHANOMI, 590)
					it.chatPlayer("I see, so you made the magical machines and Harrallak saw how they could be used in the guild to train warrior's combat... interesting." +
							"Harrallak certainly is an intelligent guy.")
					it.chatNpc("It is as you say.", SHANOMI, 590)
				}
				3 -> {
					if (it.player().world().realm().isPVP) {
						it.player().message("You can't use this shop on this world!")
					} else {
						it.player().world().shop(24).display(it.player())
					}
				}
				4 -> {
					it.chatPlayer("Bye!")
					it.chatNpc("Bye!", SHANOMI, 590)
				}
			}
		}
		
		r.onNpcOption2(SHANOMI) {
			if (it.player().world().realm().isPVP) {
				it.player().message("You can't use this shop on this world!")
			} else {
				it.player().world().shop(24).display(it.player())
			}
		}
		
		r.onNpcSpawn(SHANOMI) @Suspendable {
			val npc: Npc = it.npc()
			fun shoutMessage() {
				npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
					s.onInterrupt { shoutMessage() }
					while (true) {
						s.delay(npc.world().random(5))
						npc.sync().shout(message[npc.world().random(message.size - 1)])
					}
				})
			}
			it.onInterrupt { shoutMessage() }
			shoutMessage()
		}
	}
}

