package nl.bartpelle.veteres.content.areas.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Mack on 7/18/2017.
 */
object VentDoor {
	
	//Our const id to represent the npc we are chatting with.
	const val CHATTER = 2187
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(30266) @Suspendable {
			
			val sacrificed: Boolean = it.player().attribOr(AttributeKey.INFERNAL_ACCESS, false)
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (sacrificed) {
				
				it.delay(1)
				
				if (tileForVent(it.player(), obj).isPresent) {
					
					var tile: Tile = tileForVent(it.player(), obj).get()
					it.player().teleport(tile.x, tile.z)
				}
			} else {
				it.chatNpc("Oy! Get back from there, no JalYt allowed through.", CHATTER)
			}
		}
	}
	
	//An optional returning the tile value we aim to send the player to depending on which
	//entrance they use to get into the inferno.
	fun tileForVent(player: Player, obj: MapObj): Optional<Tile> {
		
		//An offset value calc'ing the player's position in respects to the loc of the vent they're interacting with.
		//This is to reduce having double the statements to account for leaving the area.
		var offset = if (player.tile().z > obj.tile().z) -1 else if (player.tile().x < obj.tile().x) 1 else if (player.tile().z < obj.tile().z) 1 else -1
		
		//Do the tile tings
		if (obj.tile().x == 2495 && obj.tile().z == 5157) {
			return Optional.of(Tile(obj.tile().x + offset, obj.tile().z))
		} else if (obj.tile().x == 2494 && obj.tile().z == 5174) {
			return Optional.of(Tile(obj.tile().x + offset, obj.tile().z))
		} else if (obj.tile().x == 2474 && obj.tile().z == 5137) {
			return Optional.of(Tile(obj.tile().x, obj.tile().z + offset))
		} else if (obj.tile().x == 2457 && obj.tile().z == 5119) {
			return Optional.of(Tile(obj.tile().x, obj.tile().z + offset))
		} else if (obj.tile().x == 2436 && obj.tile().z == 5120) {
			return Optional.of(Tile(obj.tile().x, obj.tile().z + offset))
		}
		
		return Optional.empty()
	}
}