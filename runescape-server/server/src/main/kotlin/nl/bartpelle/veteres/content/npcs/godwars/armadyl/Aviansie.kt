package nl.bartpelle.veteres.content.npcs.godwars.armadyl

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 6/12/2016.
 */

object Aviansie {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 7)) {
				if (EntityCombat.attackTimerReady(npc)) {
					
					npc.animate(get_animation(npc.id()))
					
					if (npc.id() == 3168)
						npc.graphic(1198, 80, 5)
					
					npc.world().spawnProjectile(npc.tile().transform(1, 1, 0), target, get_projectile(npc.id()), 95, 33, 29, 5 * npc.tile().distance(target.tile()), 11, 105)
					
					
					if (npc.id() == 3168) {
						if (AccuracyFormula.doesHit(npc, target, combatType(npc.id()), 1.0)) {
							target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(combatType(npc.id())).graphic(Graphic(160, 80, 0))
						} else {
							target.hit(npc, 0, 1).combatStyle(combatType(npc.id())).graphic(Graphic(85, 90, 0))
						}
						
						
					} else {
						if (AccuracyFormula.doesHit(npc, target, combatType(npc.id()), 1.0)) {
							target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(combatType(npc.id()))
						} else {
							target.hit(npc, 0, 1).combatStyle(combatType(npc.id()))
						}
					}
					
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun get_animation(npc: Int): Int {
		return when (npc) {
			3168 -> 6975
			else -> 6956
		}
	}
	
	@Suspendable fun combatType(npc: Int): CombatStyle {
		return when (npc) {
			3166 -> CombatStyle.MELEE
			3168 -> CombatStyle.MAGIC
			else -> CombatStyle.RANGE
		}
	}
	
	@Suspendable fun get_projectile(npc: Int): Int {
		return when (npc) {
			3167 -> 1192
			3168 -> 159
			3170 -> 1192
			3171 -> 1192
			3172 -> 1192
			3173 -> 1192
			3175 -> 1192
			3178 -> 1192
			3179 -> 1192
			3180 -> 1192
			3181 -> 1192
			3182 -> 1192
			else -> 1193
		}
	}
}
