package nl.bartpelle.veteres.content.skills.agility

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015
 */

object GnomeStronghold {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		//Log Balance
		r.onObject(23145, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.player().lockNoDamage()
			it.message("You walk carefully across the slippery log...")
			it.player().pathQueue().clear()
			it.player().pathQueue().interpolate(2474, 3429, PathQueue.StepType.FORCED_WALK)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(2474, 3429))
			it.player().looks().resetRender()
			it.player().unlock()
			it.message("...You make it safely to the other side.")
			putStage(it.player(), 1)
			it.addXp(Skills.AGILITY, 7.5)
		})
		
		// Obstacle Net
		r.onObject(23134, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lockNoDamage()
			it.message("You climb the netting...")
			it.delay(1)
			it.player().animate(828)
			it.delay(1)
			it.player().teleport(it.player().tile().x, 3424, 1)
			it.player().unlock()
			it.addXp(Skills.AGILITY, 7.5)
		})
		
		// Tree Branch
		r.onObject(23559, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lockNoDamage()
			it.message("You climb the tree...")
			it.delay(1)
			it.player().animate(828)
			it.delay(1)
			it.message("...To the platform above.")
			it.player().teleport(2473, 3420, 2)
			it.player().unlock()
			it.addXp(Skills.AGILITY, 5.0)
		})
		
		//Balancing Rope
		r.onObject(23557, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.delay(1)
			it.message("You carefully cross the tightrope.")
			it.player().lockNoDamage()
			it.player().pathQueue().clear()
			it.player().pathQueue().interpolate(2483, 3420, PathQueue.StepType.FORCED_WALK)
			it.player().looks().render(763, 762, 762, 762, 762, 762, -1)
			it.waitForTile(Tile(2483, 3420))
			it.player().looks().resetRender()
			it.player().unlock()
			it.addXp(Skills.AGILITY, 7.5)
		})
		
		// Tree Branch
		r.onObject(23560, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			it.player().lockNoDamage()
			it.message("You climb down the tree...")
			it.delay(1)
			it.player().animate(828)
			it.delay(1)
			it.player().teleport(2485, 3419, 0)
			it.message("You land on the ground.")
			it.player().unlock()
			putStage(it.player(), 2)
			it.addXp(Skills.AGILITY, 5.0)
		})
		
		// Final nettings
		r.onObject(23135, s@ @Suspendable {
			if (Skills.disabled(it.player(), Skills.AGILITY)) {
				return@s
			}
			if (it.player().tile().z != 3425) {
				it.message("You can not do that from here.")
				return@s
			}
			
			it.player().lockNoDamage()
			it.message("You climb the netting...")
			it.delay(1)
			it.animate(828)
			it.delay(2)
			it.animate(-1)
			
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			it.player().teleport(obj.tile().x, obj.tile().z + 2)
			it.player().unlock()
			putStage(it.player(), 4)
			it.addXp(Skills.AGILITY, 7.5)
		})
		
		// Exit pipes
		for (i in 23138..23139) {
			r.onObject(i, s@ @Suspendable {
				if (Skills.disabled(it.player(), Skills.AGILITY)) {
					return@s
				}
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				
				//Ensure our player is entering the obstacle from the correct side
				if (obj.tile() == Tile(2484, 3435) || obj.tile() == Tile(2487, 3435)) {
					return@s
				}
				
				it.player().lockNoDamage()
				it.delay(1)
				
				// Go to the right spot if we're not there
				if (!it.player().tile().equals(obj.tile().transform(0, -1, 0))) {
					it.player().walkTo(obj.tile().transform(0, -1, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(obj.tile().transform(0, -1, 0))
				}
				
				it.player().animate(749, 30)
				it.player().forceMove(ForceMovement(0, 0, 0, 3, 33, 126, FaceDirection.NORTH))
				it.delay(3)
				it.player().teleport(obj.tile().x, obj.tile().z + 2)
				it.delay(3)
				it.player().pathQueue().interpolate(it.player().tile().x, it.player().tile().z + 1)
				it.delay(1)
				it.player().animate(749, 30)
				it.player().forceMove(ForceMovement(0, 0, 0, 3, 33, 126, FaceDirection.NORTH))
				it.delay(3)
				it.player().teleport(obj.tile().x, obj.tile().z + 6)
				it.player().unlock()
				
				putStage(it.player(), 8)
				var stage = it.player().attribOr<Int>(AttributeKey.GNOME_COURSE_STATE, 0)
				if (stage == 15) {
					it.player().putattrib(AttributeKey.GNOME_COURSE_STATE, 0)
					it.addXp(Skills.AGILITY, 39.0)
					
					// Woo! A pet!
					val odds = (37000.00 * it.player().mode().skillPetMod()).toInt()
					if (it.player().world().rollDie(odds, 1)) {
						UnlockAgilityPet.unlockGiantSquirrel(it.player())
					}
					
				} else {
					it.addXp(Skills.AGILITY, 7.5)
				}
			})
		}
	}
	
	private fun putStage(player: Player, stageBit: Int): Int {
		var stage = player.attribOr<Int>(AttributeKey.GNOME_COURSE_STATE, 0)
		stage = stage or stageBit
		player.putattrib(AttributeKey.GNOME_COURSE_STATE, stage)
		return stage
	}
}