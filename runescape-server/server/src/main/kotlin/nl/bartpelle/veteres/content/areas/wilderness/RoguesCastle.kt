package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementDiary
import nl.bartpelle.veteres.content.achievements.pvp.PVPDiaryHandler
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*


/**
 * Created by Situations on 23/12/2015.
 */

object RoguesCastle {
	
	//Array used to store the economy server chest rewards.
	val eco_rewards: ArrayList<Item> = arrayListOf(Item(1622, 5), //Uncut emerald
			Item(1624, 6), //Uncut sapphire
			Item(995, 1000), //Coins
			Item(360, 15), //Raw Tuna
			Item(593, 25), //Ashes
			Item(591, 3), //Tinderbox
			Item(558, 25), //Mind runes
			Item(1602, 3), //Diamond
			Item(562, 40), //Chaos runes
			Item(560, 30), //Death runes
			Item(554, 30), //Fire runes
			Item(352, 10), //Pike
			Item(454, 13), //Coal
			Item(441, 13), //Iron ore
			Item(386, 10), //Shark
			Item(1616, 3))  //Dragonstone
	
	
	//Array used to store the pvp server chest rewards.
	val pvp_rewards: ArrayList<Item> = arrayListOf(
			Item(13307, 80), //Blood money
			Item(13307, 100), //Blood money
			Item(13307, 120), //Blood money
			Item(443, 4), // Silver ores
			Item(443, 8), // Silver ores
			Item(443, 10), // Silver ores
			Item(1518, 8),  // Maple logs
			Item(1518, 12), // Maple logs
			Item(1518, 10), // Maple logs
			Item(13442, 4), //Anglerfish
			Item(13442, 8), //Anglerfish
			Item(13442, 4), //Anglerfish
			Item(13442, 8), //Anglerfish
			Item(13442, 10) //Anglerfish
	)
	
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Spawn a few extra chests on PvP world
		r.onWorldInit {
			val world = it.ctx<World>()
			
			if (world.realm().isPVP) {
				world.spawnObj(MapObj(Tile(3280, 3940), 26757, 10, 0)) // Rogue's chest
				world.spawnObj(MapObj(Tile(3295, 3936), 26757, 10, 0)) // Rogue's chest
				world.spawnObj(MapObj(Tile(3288, 3938), 26757, 10, 0)) // Rogue's chest
			}
		}
		
		r.onObject(26757, s@ @Suspendable {
			val player = it.player()
			val obj: MapObj = player.attrib(AttributeKey.ORIGINAL_INTERACTION_OBJECT)
			val option: Int = player.attrib(AttributeKey.INTERACTION_OPTION)
			
			// Object ID's for the opened & closed chest
			val opened_chest = 26757
			val closed_chest = 26758
			
			if (option == 1) {
				//Handle the first chest option: "Open".
				player.lock()
				player.faceObj(obj)
				it.animate(537)
				generate_hit(it)
				it.message("You have activated a trap on the chest.")
				player.unlock()
			} else if (option == 2) {
				//Handle the second chest option: "Search for traps".
				if (!player.world().realm().isPVP && player.skills().level(Skills.THIEVING) < 84) {
					player.faceObj(obj)
					player.message("You need a Thieving level of 84 to successfully loot this chest.")
				} else {
					// On the PvP world, only one player can access a chest at a time.
					if (obj.attrib<Any?>(AttributeKey.OWNING_PLAYER) != null) {
						it.message("Someone else is opening that.")
						return@s
					}
					
					obj.putattrib(AttributeKey.OWNING_PLAYER, player.id())
					
					// Else we must be high enough thieving to crack the chest!
					it.player().lock()
					it.message("You cycle the chest for traps.")
					it.message("You find a trap on the chest.")
					it.delay(2)
					it.message("You disable the trap")
					it.animate(535)
					it.addXp(Skills.THIEVING, 100.0)
					
					if (AchievementDiary.diaryHandler(it.player()) is PVPDiaryHandler) {
						AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(it.player(), 0))
						AchievementAction.process(player, AchievementDiary.diaryHandler(player).achievementById(it.player(), 11))
					}
					
					// Grabs a reward from our array lists
					val eco_reward = player.world().random(eco_rewards.size - 1)
					val pvp_reward = player.world().random(pvp_rewards.size - 1)
					
					// If our player is on the PVP world, give them the appropriate reward.
					if (player.world().realm().isPVP) {
						val def = it.player().world().definitions().get(ItemDefinition::class.java, pvp_rewards[pvp_reward].unnote(it.player().world()).id())
						player.inventory() += Item(pvp_rewards[pvp_reward].id(), pvp_rewards[pvp_reward].amount())
						player.message("You find some ${def.name} inside.")
					} else {
						// Else we must be on the economy server..
						val def = it.player().world().definitions().get(ItemDefinition::class.java, eco_rewards[eco_reward].unnote(it.player().world()).id())
						player.inventory() += Item(eco_rewards[eco_reward].id(), eco_rewards[eco_reward].amount())
						player.message("You find some ${def.name} inside.")
					}
					
					// For every level 135 Rogue inside the Rogues Castle we..
					player.world().npcs().forEachKt({ npc ->
						if (WildernessLevelIndicator.inside_rouges_castle(npc.tile()) && npc.id() == 6603) {
							npc.sync().shout("Someone's stealing from us, get them!!")
							npc.attack(player)
						}
					})
					
					// Handle replacing the chest with an open chest, then back to the original object..
					it.runGlobal(player.world()) @Suspendable {
						val world = player.world()
						val old = MapObj(obj.tile(), opened_chest, obj.type(), obj.rot())
						val spawned = MapObj(obj.tile(), closed_chest, obj.type(), obj.rot())
						world.removeObj(old)
						world.spawnObj(spawned)
						obj.clearattrib(AttributeKey.OWNING_PLAYER)
						it.delay(60)
						world.removeObjSpawn(spawned)
						world.spawnObj(old)
					}
					
					obj.clearattrib(AttributeKey.OWNING_PLAYER)
					it.player().unlock()
				}
			}
		})
	}
	
	fun generate_hit(it: Script) {
		val player = it.player()
		val current_hp = player.hp()
		
		//Generate the hit, and apply it to the player.
		if (current_hp >= 90) {
			player.hit(player, 17)
		} else if (current_hp >= 80) {
			player.hit(player, 15)
		} else if (current_hp >= 70) {
			player.hit(player, 14)
		} else if (current_hp >= 60) {
			player.hit(player, 12)
		} else if (current_hp >= 50) {
			player.hit(player, 11)
		} else if (current_hp >= 40) {
			player.hit(player, 9)
		} else if (current_hp >= 30) {
			player.hit(player, 7)
		} else if (current_hp >= 20) {
			player.hit(player, 6)
		} else if (current_hp >= 10) {
			player.hit(player, 5)
		} else if (current_hp >= 7) {
			player.hit(player, 4)
		} else if (current_hp >= 3) {
			player.hit(player, 3)
		} else player.hit(player, 1)
	}
}