package nl.bartpelle.veteres.content.tournament

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.script.ScriptRepository

object TournamentManager {

    @JvmStatic @ScriptMain fun register(r: ScriptRepository) {
        r.onNpcOption1(7316, s@ @Suspendable {
            val player = it.player()
            if (TournamentConfig.tournamentDisabled) {
                player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently in development. Please try again later."))
                return@s
            }
        })
        r.onNpcOption2(7316, s@ @Suspendable {
            val player = it.player()
            if (TournamentConfig.tournamentDisabled) {
                player.message(Color.DARK_RED.wrap("<img=79> The tournament is currently in development. Please try again later."))
                return@s
            }
        })
    }

}