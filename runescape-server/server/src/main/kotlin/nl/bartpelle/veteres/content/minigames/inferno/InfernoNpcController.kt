package nl.bartpelle.veteres.content.minigames.inferno

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Mack on 8/3/2017.
 */
interface InfernoNpcController {
	
	/**
	 * Actions we may need to check on a per-attack basis.
	 */
	fun tick(it: Script) {}
	
	/**
	 * Created this function to allow us to manipulate certain on spawn actions and
	 * I can predefine specific spawn points for npcs that need to follow a certain trend.
	 */
	fun onSpawn()
	
	/**
	 * This method is specifically for triggering if the player has killed the npc. Otherwise, add any code into the triggerNpcKilledDeath.
	 */
	fun triggerPlayerKilledDeath() {}
	
	/**
	 * This method is for any npc killed trigger deaths. I.e if a glyph was killed by jad or any other mobs it triggers this.
	 */
	fun triggerNpcKilledDeath() {}
	
	/**
	 * The actions we handle on damage dealt.
	 */
	fun onDamage() {}
}