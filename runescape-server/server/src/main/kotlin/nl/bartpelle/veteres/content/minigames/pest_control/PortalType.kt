package nl.bartpelle.veteres.content.minigames.pest_control

/**
 * Created by Jason MacKeigan on 2016-07-12 at 4:01 PM
 *
 * Represents each type of individual portal in the pest control mini game.
 */
enum class PortalType(val interfaceOrder: Int, val shieldedNpcId: Int, val shieldlessNpcId: Int, val xOffset: Int,
                      val yOffset: Int, val shieldComponent: Int) {
	PURPLE(0, 1743, 1747, -28, -1, 19),
	BLUE(1, 1744, 1748, 24, -4, 21),
	YELLOW(2, 1745, 1749, 13, -22, 23),
	RED(3, 1746, 1750, -11, -23, 25);
	
	companion object {
		
		/**
		 * A mapping of all shielded non-playable characters for their respective portal type.
		 */
		val IDS = PortalType.values().map { it.shieldedNpcId }
		
	}
}