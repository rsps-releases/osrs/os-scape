package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.GameCommands

/**
 * Created by Jak on 20/09/2016.
 * Opens a chatbox prompt to either enter a typed custom reason OR choose from a selection
 * of reasons that a punishment was issued for.
 */
class SetPunishmentReason(val punishmentUuid: Int) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(it: Script) {
		// Find by ID
		val punishment: PunishmentLog.Punishment? = PunishmentLog.forUuid(punishmentUuid)
		if (punishment == null) {
			it.message("Invalid punishment uuid - " + punishmentUuid)
			return
		}
		// Custom or from selection?
		when (it.optionsTitled("Update punishment $punishmentUuid reason...", "From list...", "Type custom...")) {
			1 -> {
				// From selection
				val opt = OptionList.display(it, false, "Select a Reason", *(PunishmentLog.reasonsList.map { it.reason }.toTypedArray()))
				if (opt != -1) {
					val selection = PunishmentLog.reasonsList[opt].reason
					PunishmentLog.updateReason(punishment, selection)
					it.message("Punishment " + punishmentUuid + " reason updated to: " + punishment.reason)
					it.player().world().playerByName(punishment.offenderName).ifPresent { op -> op.message("<col=00FFFF>Jail reason: ${punishment.reason}.") }
					if (it.player().interfaces().activeMain() == 275) { // Refresh our screen
						GameCommands.process(it.player(), "modlog")
					}
					// We can be attacked & interrupted again now.
					it.player().timers().cancel(TimerKey.COMBAT_IMMUNITY)
				}
			}
			2 -> {
				// Custom reason
				PunishmentLog.updateReason(punishment, it.inputString("What is the reason? Other staff can see this."))
				it.message("Punishment " + punishmentUuid + " reason updated to: " + punishment.reason)
				if (it.player().interfaces().activeMain() == 275) { // Refresh our screen
					GameCommands.process(it.player(), "modlog")
				}
				// We can be attacked & interrupted again now.
				it.player().timers().cancel(TimerKey.COMBAT_IMMUNITY)
			}
		}
	}
}