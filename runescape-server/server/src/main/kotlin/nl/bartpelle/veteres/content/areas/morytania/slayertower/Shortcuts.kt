package nl.bartpelle.veteres.content.areas.morytania.slayertower

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/7/2015.
 */

object Shortcuts {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(16537) @Suspendable {
			val player = it.player()
			val obj: MapObj = player.attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3422 && obj.tile().z == 3550) {
				if (!player.equipment().contains(Item(4168))) {
					it.messagebox("A foul stench seems to be seeping down from the floor above... it could be dangerous up there without a nosepeg.")
					when (it.customOptions("Go up anyway?", "Yes", "No")) {
						1 -> {
							Ladders.ladderUp(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level + 1), true)
							it.addXp(Skills.AGILITY, 4.0)
						}
						2 -> {
							it.messagebox("You decide to save your nose, and self, from almost certain death.")
						}
					}
				} else {
					Ladders.ladderUp(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level + 1), true)
					it.addXp(Skills.AGILITY, 4.0)
				}
			} else if (obj.tile().x == 3447 && obj.tile().z == 3576 && it.player().skills().level(Skills.AGILITY) >= 71) {
				Ladders.ladderUp(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level + 1), true)
				it.addXp(Skills.AGILITY, 4.0)
			} else {
				it.message("You need an Agility level of 71 to negotiate this obstacle.")
			}
		}
		r.onObject(16538) @Suspendable {
			val player = it.player()
			val obj: MapObj = player.attrib(AttributeKey.INTERACTION_OBJECT)
			if (obj.tile().x == 3422 && obj.tile().z == 3550) {
				Ladders.ladderDown(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level - 1), true)
				it.player().hit(it.player(), it.player().world().random(5))
			} else if (obj.tile().x == 3447 && obj.tile().z == 3576 && it.player().skills().level(Skills.AGILITY) >= 71) {
				Ladders.ladderDown(it, Tile(it.player().tile().x, it.player().tile().z, it.player().tile().level - 1), true)
				it.player().hit(it.player(), it.player().world().random(5))
			} else {
				it.message("You need an Agility level of 71 to negotiate this obstacle.")
			}
		}
	}
}