package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Bart on 2/23/2016.
 */

object DuelArenaScoreboard {
	
	@JvmStatic val FIGHT_HISTORY = LinkedList<String>()
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Fill up array with empty strings..
		FIGHT_HISTORY.clear()
		for (x in 1..50) {
			FIGHT_HISTORY.add("")
		}
		r.onObject(3192) { display(it.player()) }
	}
	
	@JvmStatic fun registerResult(winner: Player, loser: Player) {
		FIGHT_HISTORY.offer("${winner.name()} (${winner.skills().combatLevel()}) beat ${loser.name()} (${loser.skills().combatLevel()})")
		trimHistory()
	}
	
	fun display(player: Player) {
		// Send the history
		var x = 71
		for (str in FIGHT_HISTORY) {
			player.write(InterfaceText(108, x--, str))
		}
		
		// Send our wins/losses
		player.write(InterfaceText(108, 16, "My Wins: ${player.attribOr<Int>(AttributeKey.STAKES_WON, 0)}"))
		player.write(InterfaceText(108, 17, "My Losses: ${player.attribOr<Int>(AttributeKey.STAKES_LOST, 0)}"))
		
		player.interfaces().sendMain(108)
	}
	
	fun trimHistory() {
		while (FIGHT_HISTORY.size > 50) {
			FIGHT_HISTORY.poll()
		}
	}
	
}