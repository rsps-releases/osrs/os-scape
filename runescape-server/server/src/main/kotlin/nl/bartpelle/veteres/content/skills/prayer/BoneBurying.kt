package nl.bartpelle.veteres.content.skills.prayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.content.itemOptions
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Carl on 2015-08-12.
 */

object BoneBurying {
	
	enum class Bone(val itemId: Int, val xp: Double) {
		REGULAR_BONES(526, 4.5),
		BURNT_BONES(528, 4.5),
		BAT_BONES(530, 4.5),
		WOLF_BONES(2859, 4.5),
		BIG_BONES(532, 15.0),
		LONG_BONE(10976, 15.0),
		CURVED_BONE(10977, 15.0),
		JOGRE_BONE(3125, 15.0),
		BABYDRAGON_BONES(534, 30.0),
		DRAGON_BONES(536, 72.0),
		ZOGRE_BONES(4812, 22.5),
		OURG_BONES(4834, 140.0),
		WYVERN_BONES(6812, 72.0),
		DAGANNOTH_BONES(6729, 125.0),
		LAVA_DRAGON_BONES(11943, 85.0),
		SUPERIOR_DRAGON_BONES(22124, 150.0);
		
		companion object {
			fun get(itemId: Int): Bone? {
				Bone.values().forEach {
					if (it.itemId == itemId)
						return it
				}
				return null
			}
		}
	}
	
	@ScriptMain @JvmStatic fun bone_burying(repo: ScriptRepository) {
		Bone.values().forEach { bone -> repo.onItemOption1(bone.itemId) @Suspendable { bury(it, bone) } }
	}
	
	@JvmStatic @Suspendable fun bury(it: Script, bone: Bone) {
		val player = it.player()
		it.clearContext()
		if (player.timers().has(TimerKey.BONE_BURYING))
			return
		player.pathQueue().clear()
		player.timers().extendOrRegister(TimerKey.BONE_BURYING, 2)
		player.inventory().remove(Item(bone.itemId), true, player.attribOr(AttributeKey.ITEM_SLOT, 0))
		player.animate(827)
		player.message("You dig a hole in the ground...")
		
		var xp = bone.xp
		
		// Lava drag isle check
		if (bone.itemId == 11943 && player.tile().inArea(3172, 3799, 3232, 3857)) {
			xp *= 4
		}
		var mes = "You bury the bones."
		// Necromancer Blessing
		if (BonusContent.isActive(player, BlessingGroup.NECROMANCER)) {
			xp *= 2
			mes = "You bury the bones with the Necromancer's blessing."
		}
		
		player.skills().__addXp(Skills.PRAYER, xp)
		it.delay(1)
		player.message(mes)
	}
	
	@ScriptMain @JvmStatic fun bonesOnAltar(r: ScriptRepository) {
		intArrayOf(14860, 409, 37990, 2640, 411, 27661, 7812).forEach { altar ->
			r.onItemOnObject(altar, @Suspendable {
				val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				val bones = Bone.get(item)
				
				if (bones != null) {
					startBonesOnAltar(it, bones, obj)
				}
			})
		}
	}
	
	@Suspendable fun startBonesOnAltar(it: Script, bones: Bone, obj: MapObj) {
		var amt = 1
		if (it.player().inventory().count(bones.itemId) > 1) {
			amt = it.itemOptions(Item(bones.itemId, 175), "How many would you like to use?", offsetX = 9)
		}
		
		while (amt-- > 0 && obj.valid(it.player().world())) {
			if (it.player().inventory().remove(Item(bones.itemId), true).failed()) {
				it.message("You have ran out of bones.")
				return
			}
			it.player().animate(896)
			it.player().world().tileGraphic(624, obj.tile(), 0, 0)
			if (it.player().world().objById(13213, Tile(3095, 3506)) != null &&
					it.player().world().objById(13213, Tile(3098, 3506)) != null) { // Gilded altar locations
				it.message("The gods are very pleased with your offerings.")
				it.addXp(Skills.PRAYER, bones.xp * 3)
			} else {
				it.message("The gods are pleased with your offerings.")
				it.addXp(Skills.PRAYER, bones.xp * 2)
			}
			it.delay(3)
		}
	}
}