package nl.bartpelle.veteres.content.areas.zeah.wcguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/10/2016.
 */
object Berry {
	
	const val BERRY = 7235
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(BERRY) @Suspendable {
			it.chatPlayer("Hello.", 567)
			it.chatNpc("Good day, adventurer. If you have any questions, I'm<br>sure Lars will be more than " +
					"happy to answer them. He's<br>in the building to the south.", BERRY, 569)
			it.chatNpc("Also don't forget to visit my sister's axe shop. She works<br>in the same building as Lars.", BERRY, 568)
			it.chatPlayer("Thanks!", 567)
		}
	}
	
}