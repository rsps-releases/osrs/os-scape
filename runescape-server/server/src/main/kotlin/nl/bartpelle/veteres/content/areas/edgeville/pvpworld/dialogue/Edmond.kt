package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/29/2015.
 */

object Edmond {
	
	val EDMOND = 4256
	val UNTRADABLE_SHOP = 4
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(EDMOND) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(UNTRADABLE_SHOP).display(it.player())
			} else {
				it.player().message("You can only view this shop in the PK server.")
			}
		}
	}
}