package nl.bartpelle.veteres.content.minigames

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.ChangeMapVisibility
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.SettingsBuilder
import nl.bartpelle.veteres.util.Tuple
import nl.bartpelle.veteres.util.Varbit
import java.util.*

/**
 * Created by Bart on 11/27/2015. <-- created literally the class name noob
 * @author Jak write 99.9% ty
 **/

object Barrows {
	
	val possibles = arrayOf(20720, 20770, 20772, 20721, 20771, 20722)
	val lootItemIds = arrayOf(560, 565, 562, 558, 4740, 995)
	val lootItemAmts = arrayOf(180, 80, 270, 450, 120, 8000)
	val barrowsItemIds = arrayOf(4708, 4710, 4712, 4714, 4716, 4718, 4720,
			4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745,
			4747, 4749, 4751, 4753, 4755, 4757, 12853, 4759, 20543)
	
	@JvmStatic fun barrowsBrotherKc(player: Player): Int {
		return player.varps().varbit(Varbit.AHRIM) +
				player.varps().varbit(Varbit.DHAROK) +
				player.varps().varbit(Varbit.GUTHAN) +
				player.varps().varbit(Varbit.KARIL) +
				player.varps().varbit(Varbit.TORAG) +
				player.varps().varbit(Varbit.VERAC)
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Make it dark. Thou shalt not see.. a brother..
		val darkRegions = intArrayOf(14231)
		darkRegions.forEach { region ->
			r.onRegionEnter(region) {
				//When we enter a cave..
				it.player().write(ChangeMapVisibility(2))
				it.player().interfaces().send(24, it.player().interfaces().activeRoot(), it.player().interfaces().barrowsComponent(), true)
				//it.player().message("Killcount: " + it.player().varps().varbit(Varbit.BARROWS_MONSTER_KC))
			}
			r.onRegionExit(region) {
				//And when we leave the cave..
				it.player().write(ChangeMapVisibility(0))
				it.player().interfaces().closeById(24)
				
				var npc = it.player().attribOr<Npc>(AttributeKey.barrowsBroSpawned, null)
				if (npc != null) {
					//TODO despawn barrows brother IF NOT ALREADY DEAD.
					if (!npc.dead() && npc.hp() > 0) {
						npc.stopActions(true)
						it.player().clearattrib(AttributeKey.barrowsBroSpawned)
						npc.lockNoDamage()
						npc.hidden(true)
						npc.world().unregisterNpc(npc)
						it.player().write(SetHintArrow(null, 1))
					}
				}
			}
		}
		
		// Register stairways to heaven.
		val stairs = arrayOf(
				Pair(20668, Tile(3575, 3297, 0)), // Dharok
				Pair(20667, Tile(3565, 3289, 0)), // Ahrim
				Pair(20672, Tile(3557, 3298, 0)), // Verac
				Pair(20669, Tile(3577, 3282, 0)), // Guthan
				Pair(20670, Tile(3566, 3276, 0)), // Karil
				Pair(20671, Tile(3554, 3283, 0))  // Torag
		)
		stairs.forEach { stair ->
			r.onObject(stair.first) {
				it.player().teleport(stair.second)
			}
		}
		
		r.onObject(20973) @Suspendable {
			// final looting chest at the bottom crypt maze
			val killedAll = it.player().varps().varbit(Varbit.DHAROK) == 1
					&& it.player().varps().varbit(Varbit.TORAG) == 1
					&& it.player().varps().varbit(Varbit.AHRIM) == 1
					&& it.player().varps().varbit(Varbit.KARIL) == 1
					&& it.player().varps().varbit(Varbit.VERAC) == 1
					&& it.player().varps().varbit(Varbit.GUTHAN) == 1
			if (!killedAll && it.player().attribOr<Npc>(AttributeKey.barrowsBroSpawned, null) == null
					&& Barrows.barrowsBrotherKc(it.player()) >= 5) {
				//spawn the last bro
				var targ: Int = it.player().attribOr<Int>(AttributeKey.FINAL_BARROWS_BRO_COFFINID, 0)
				var broId: Int = 0
				
				when (targ) {
					20720 -> {
						broId = 1673
					} //Dharock
					20770 -> {
						broId = 1672
					} //Ahrim
					20772 -> {
						broId = 1677
					} //Verac
					20721 -> {
						broId = 1676
					} //Torag
					20771 -> {
						broId = 1675
					} //Verac
					20722 -> {
						broId = 1674
					} //Guthan
				}
				if (broId != 0) {
					val tile = Tile(3548 + it.player().world().random().nextInt(6), 9691 + it.player().world().random().nextInt(1), it.player().tile().level)
					val npc = Npc(broId, it.player().world(), tile)
					
					it.player().world().registerNpc(npc)
					npc.putattrib(AttributeKey.OWNING_PLAYER, Tuple(it.player().id(), it.player()))
					it.player().putattrib(AttributeKey.barrowsBroSpawned, npc)
					it.player().write(SetHintArrow(npc, 1))
					npc.respawns(false)
					npc.face(it.player())
					npc.sync().shout("How dare you disturb my rest!")
					npc.attack(it.player())
				} else {
					it.player().message("Unrecognised brother from coffin")
				}
			} else {
				if (Barrows.barrowsBrotherKc(it.player()) < 6) {
					it.player().message("You need to have killed all 6 brothers to loot the chest.")
				} else {
					it.player().varps().varbit(Varbit.BARROWS_MONSTER_KC, 0)
					it.player().varps().varbit(Varbit.AHRIM, 0)
					it.player().varps().varbit(Varbit.DHAROK, 0)
					it.player().varps().varbit(Varbit.GUTHAN, 0)
					it.player().varps().varbit(Varbit.KARIL, 0)
					it.player().varps().varbit(Varbit.TORAG, 0)
					it.player().varps().varbit(Varbit.VERAC, 0)
					it.player().clearattrib(AttributeKey.FINAL_BARROWS_BRO_COFFINID)
					it.player().message("Congratulations.")
					it.player().putattrib(AttributeKey.BARROWS_CHESTS_OPENED, 1 + it.player().attribOr<Int>(AttributeKey.BARROWS_CHESTS_OPENED, 0));
					it.player().message("Your Barrows chest count is: <col=FF0000>" + it.player().attribOr(AttributeKey.BARROWS_CHESTS_OPENED, 0) + "</col>.");
					
					//Generate three items as a reward from the chest..
					giveloot(it)
				}
			}
		}
		
		//DHAROK, AHRIM, VERAC, TORAG, KARIL,GUTHAN
		var tile: Tile? = null
		var broId: Int = 0
		for (coffinId in possibles) @Suspendable {
			r.onObject(coffinId, s@ @Suspendable {
				var targ: Int = it.player().attribOr<Int>(AttributeKey.FINAL_BARROWS_BRO_COFFINID, 0)
				if (targ == 0) {
					targ = setChest(it.player().world().random())
					it.player().putattrib(AttributeKey.FINAL_BARROWS_BRO_COFFINID, targ)
				}
				
				if (targ == coffinId) @Suspendable {
					when (it.options("Yes, I'm not afraid!", "No way")) {
						1 -> @Suspendable {
							if (barrowsBrotherKc(it.player()) < 5) {
								it.messagebox("You must have killed 5 brothers to enter the final crypt!")
							} else {
								it.player().teleport(Tile(3551, 9699))
							}
						}
						2 -> {
							it.player().interfaces().close(162, 550) // close chatbox
						}
					}
				} else {
					val npc2: Npc? = it.player().attribOr<Npc>(AttributeKey.barrowsBroSpawned, null)
					if (npc2 != null) {
						it.player().message("This sarcophagus has already been raided!")
					} else {
						when (coffinId) {
							20720 -> {
								//DHAROK
								tile = Tile(3552 + it.player().world().random().nextInt(4), 9716,
										it.player().tile().level)
								broId = 1673
								if (it.player().varps().varbit(Varbit.DHAROK) == 1) {
									it.player().message("You've already raided this coffin.")
									return@s
								}
							}
							20770 -> {
								//ah
								tile = Tile(3552 + it.player().world().random().nextInt(6), 9701,
										it.player().tile().level)
								broId = 1672
								if (it.player().varps().varbit(Varbit.AHRIM) == 1) {
									it.player().message("You've already raided this coffin.")
									return@s
								}
							}
							20772 -> {
								//ver
								tile = Tile(3576, 9708 - it.player().world().random().nextInt(4),
										it.player().tile().level)
								broId = 1677
								if (it.player().varps().varbit(Varbit.VERAC) == 1) {
									it.player().message("You've already raided this coffin.")
									return@s
								}
							}
							20721 -> {
								//tor
								tile = Tile(3567 + it.player().world().random().nextInt(5), 9684,
										it.player().tile().level)
								broId = 1676
								if (it.player().varps().varbit(Varbit.TORAG) == 1) {
									it.player().message("You've already raided this coffin.")
									return@s
								}
							}
							20771 -> {
								//ka
								tile = Tile(3548, 9681 + it.player().world().random().nextInt(5),
										it.player().tile().level)
								broId = 1675
								if (it.player().varps().varbit(Varbit.KARIL) == 1) {
									it.player().message("You've already raided this coffin.")
									return@s
								}
							}
							20722 -> {
								//GUTHAN
								tile = Tile(3536, 9701 + it.player().world().random().nextInt(5),
										it.player().tile().level)
								broId = 1674
								if (it.player().varps().varbit(Varbit.GUTHAN) == 1) {
									it.player().message("You don't find anything.")
									return@s
								}
							}
							else -> {
								it.player().message("unsupported coffin")
							}
						}
						val npc = Npc(broId, it.player().world(), tile)
						it.player().world().registerNpc(npc)
						npc.putattrib(AttributeKey.OWNING_PLAYER, Tuple(it.player().id(), it.player()))
						it.player().putattrib(AttributeKey.barrowsBroSpawned, npc)
						it.player().write(SetHintArrow(npc, 1))
						npc.respawns(false)
						npc.face(it.player())
						npc.sync().shout("How dare you disturb my rest!")
						it.player().message("You don't find anything.")
						npc.attack(it.player())
					}
				}
			})
		}
	}
	
	@JvmStatic fun testloot(p: Player) {
		p.world().server().scriptExecutor().executeScript(p, @Suspendable {
			giveloot(it)
		})
	}
	
	private fun giveloot(it: Script) {
		var gotBarrow: Boolean = false
		
		if (BonusContent.isActive(it.player, BlessingGroup.TOMB_RAIDER)) {
			it.player.message("The active Tomb Raider blessing grants you increased loot chance!")
		}
		
		var totalLoot = ItemContainer(it.player().world(), 10, ItemContainer.Type.FULL_STACKING)
		
		for (i in 1..3) {
			val rand: Random = it.player().world().random()
			val idx: Int = rand.nextInt(lootItemIds.size)
			var multiplier = if (it.player().world().realm().isOSRune && lootItemAmts[idx] > 600) 2 else 1
			
			// The Tomb Raider blessing grants increased 'junk' amounts, e.g more runes.
			if (BonusContent.isActive(it.player, BlessingGroup.TOMB_RAIDER)) {
				multiplier++;
			}
			
			var shitItem: Item = Item(lootItemIds[idx], rand.nextInt(lootItemAmts[idx]) * multiplier)
			
			if (it.player().world().realm().isPVP && shitItem.id() == 995) { // Give blood money instead of coins
				shitItem = Item(13307, it.player().world().random(300..450))
			}
			
			val res2: ItemContainer.Result = it.player().inventory().add(shitItem, false)
			val rStr: String = res2.failed().toString()
			if (res2.failed()) {
				it.player().world().spawnGroundItem(GroundItem(it.player().world(), shitItem, it.player().tile(), it.player().id()))
			}
			
			totalLoot.add(shitItem, true)
			
			var chance = 33 // 1/33 for any peice after killing 6 brothers
			if (it.player().world().realm().isRealism) {
				chance = if (it.player().mode() == GameMode.REALISM) 26 else if (it.player().mode() == GameMode.LAID_BACK) 45 else 33
			} else if (it.player().world().realm().isPVP) {
				chance = 4
			} else if (it.player().world().realm().isOSRune) {
				chance = 16
			}
			
			// The Tomb Raider blessing grants one increased chance (33%) on barrows items.
			if (BonusContent.isActive(it.player, BlessingGroup.TOMB_RAIDER)) {
				chance = (chance.toDouble() * 0.66).toInt()
			}
			
			val possibleBarrows = if (it.player().world().realm().isPVP) arrayOf(4708, 4710, 4712, 4714, 4716, 4718, 4720,
					4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745,
					4747, 4749, 4751, 4753, 4755, 4757, 12853, 4759) else barrowsItemIds
			
			if (!gotBarrow && rand.nextInt(chance) == 0) { // out of the 3 possible loots, only 1 can be a barrow. there IS however
				// a chance that this barrows loot will give the player ANOTHER
				gotBarrow = true
				val idx: Int = rand.nextInt(possibleBarrows.size)
				val shitItem: Item = Item(possibleBarrows[idx], 1)
				val res2: ItemContainer.Result = it.player().inventory().add(shitItem, false)
				if (res2.failed()) {
					it.player().world().spawnGroundItem(GroundItem(
							it.player().world(),
							shitItem,
							it.player().tile(),
							it.player().id()))
				}
				
				totalLoot.add(shitItem, true)
				
				var doubleChance = 100
				if (it.player().world().realm().isRealism) {
					if (it.player().mode() == GameMode.REALISM)
						doubleChance = 50 // 1/50 w1 realism
				} else {
					doubleChance = 50 // 1/50 on w2 w3
				}
				
				if (rand.nextInt(doubleChance) == 0) {
					val idx: Int = rand.nextInt(possibleBarrows.size)
					val shitItem: Item = Item(possibleBarrows[idx], 1)
					val res2: ItemContainer.Result = it.player().inventory().add(shitItem, false)
					if (res2.failed()) {
						it.player().world().spawnGroundItem(GroundItem(
								it.player().world(),
								shitItem,
								it.player().tile(),
								it.player().id()))
					}
					
					totalLoot.add(shitItem, true)
				}
			}
		}
		
		// Display the loot interface
		it.player().interfaces().sendMain(155)
		it.player().write(SetItems(141, totalLoot))
		it.player().interfaces().setting(155, 3, 0, 10, SettingsBuilder().option(9))
	}
	
	/**
	 * Select a random coffin ID to be the enterance to the chest.
	 */
	fun setChest(r: Random): Int {
		return possibles[r.nextInt(6)]
	}
	
}