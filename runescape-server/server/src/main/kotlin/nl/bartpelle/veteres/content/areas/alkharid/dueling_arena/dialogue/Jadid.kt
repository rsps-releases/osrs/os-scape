package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Jadid {
	
	val JADID = 3348
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(JADID) @Suspendable {
			val random = it.player().world().random(7)
			
			if (random == 0) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Can't you see I'm watching the duels?", JADID, 614)
				it.chatPlayer("I'm sorry!", 571)
			}
			if (random == 1) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Knock knock!", JADID, 567)
				it.chatPlayer("Who's there?", 588)
				it.chatNpc("Boo.", JADID, 567)
				it.chatPlayer("Boo who?", 575)
				it.chatNpc("Don't cry, it's just me!", JADID, 605)
			}
			if (random == 2) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("My favourite fighter is Mubariz!", JADID, 567)
				it.chatPlayer("The guy at the information kiosk?", 575)
				it.chatNpc("Yeah! He rocks!", JADID, 567)
				it.chatPlayer("Takes all sorts, I guess.", 567)
			}
			if (random == 3) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Hmph.", JADID, 610)
			}
			if (random == 4) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Why did the skeleton burp?", JADID, 567)
				it.chatPlayer("I don't know?", 575)
				it.chatNpc("'Cause it didn't have the guts to fart!", JADID, 605)
			}
			if (random == 5) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("Ooh. This is exciting!", JADID, 567)
				it.chatPlayer("Yup!", 567)
			}
			if (random == 6) {
				it.chatPlayer("Hi!", 567)
				it.chatNpc("How can you make a very lively hankerchief?", JADID, 567)
				it.chatPlayer("I don't know?", 575)
				it.chatNpc("Put a little boogey in it.", JADID, 605)
			}
		}
	}
}