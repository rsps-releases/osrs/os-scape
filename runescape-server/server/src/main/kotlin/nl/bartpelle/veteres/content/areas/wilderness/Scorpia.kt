package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/11/2016.
 */

object Scorpia {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(26762) @Suspendable {
			//Cavern
			val player = it.player()
			val obj: MapObj = player.attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (obj.tile() == Tile(3231, 3936)) //South
				teleportPlayer(it, 3233, 10332)
			else if (obj.tile() == Tile(3231, 3951)) //West
				teleportPlayer(it, 3232, 10351)
			else if (obj.tile() == Tile(3241, 3949)) //East
				teleportPlayer(it, 3243, 10351)
		}
		
		r.onObject(26763) @Suspendable {
			val player = it.player()
			val obj: MapObj = player.attrib(AttributeKey.INTERACTION_OBJECT)
			
			if (obj.tile() == Tile(3233, 10331)) //South
				teleportPlayer(it, 3233, 3938)
			else if (obj.tile() == Tile(3232, 10352)) //West
				teleportPlayer(it, 3233, 3950)
			else if (obj.tile() == Tile(3243, 10352)) //East
				teleportPlayer(it, 3242, 3948)
		}
	}
	
	@Suspendable fun teleportPlayer(it: Script, x: Int, z: Int) {
		val player = it.player()
		
		player.lock()
		it.animate(2796)
		it.delay(2)
		it.animate(-1)
		player.teleport(x, z)
		player.unlock()
	}
}
