package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 8/25/2015.
 */

object TeleportTabs {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) { // tele tabs teletab teleport tabs
		//Normal
		repo.onItemOption1(8007) @Suspendable { tele(it, it.player(), Tile(3212, 3423), 2) } // Varrock
		repo.onItemOption1(8008) @Suspendable { tele(it, it.player(), Tile(3222, 3218), 2) } // Lumbridge
		repo.onItemOption1(8009) @Suspendable { tele(it, it.player(), Tile(2965, 3380), 3) } // Falador
		repo.onItemOption1(8010) @Suspendable { tele(it, it.player(), Tile(2757, 3479), 2) } // Camelot
		repo.onItemOption1(8011) @Suspendable { tele(it, it.player(), Tile(2662, 3307), 2) } // Ardougne
		repo.onItemOption1(8012) @Suspendable { tele(it, it.player(), Tile(2549, 3114, 2), 2) } // Watchtower
		repo.onItemOption1(8013) @Suspendable { tele(it, it.player(), Tile(3087, 3499), 2) } // Player-owned house
		
		//Ancient Magicks
		repo.onItemOption4(12775) @Suspendable { tele(it, it.player(), Tile(3288, 3886), 2) } // Annakarl
		repo.onItemOption4(12776) @Suspendable { tele(it, it.player(), Tile(3161, 3667), 2) } // Carrallangar
		repo.onItemOption4(12777) @Suspendable { tele(it, it.player(), Tile(2966, 3696), 2) } // Dareeyak
		repo.onItemOption4(12778) @Suspendable { tele(it, it.player(), Tile(2972, 3872), 2) } // Ghorrock
		repo.onItemOption1(12779) @Suspendable { tele(it, it.player(), Tile(3497, 3487), 2) } // Kharyrll
		repo.onItemOption1(12780) @Suspendable { tele(it, it.player(), Tile(3014, 3500), 2) } // Lassar
		repo.onItemOption1(12781) @Suspendable { tele(it, it.player(), Tile(3097, 9882), 2) } // Paddewwa
		repo.onItemOption1(12782) @Suspendable { tele(it, it.player(), Tile(3349, 3346), 3) } // Senntisten
		
		//Arceuus
		repo.onItemOption1(19613) @Suspendable { tele(it, it.player(), Tile(3240, 3195, 0), 2) } //Lumbridge Graveyard teleport
		repo.onItemOption1(19615) @Suspendable { tele(it, it.player(), Tile(3109, 3350, 0), 2) } //Draynor Manor teleport
		repo.onItemOption1(19617) @Suspendable { tele(it, it.player(), Tile(2981, 3510, 0), 2) } //Mind Altar teleport
		repo.onItemOption1(19619) @Suspendable { tele(it, it.player(), Tile(3431, 3460, 0), 2) } //Salve Graveyard teleport
		repo.onItemOption1(19621) @Suspendable { tele(it, it.player(), Tile(3549, 3529, 0), 2) } //Fenkenstrain's teleport
		repo.onItemOption1(19623) @Suspendable { tele(it, it.player(), Tile(3502, 3292, 0), 2) } //West Ardougne teleport
		repo.onItemOption1(19625) @Suspendable { tele(it, it.player(), Tile(3212, 3424, 0), 2) } //Harmony Island teleport TODO: COORDS
		repo.onItemOption1(19627) @Suspendable { tele(it, it.player(), Tile(2979, 3763, 0), 2) } //Cementery teleport
		repo.onItemOption1(19629) @Suspendable { tele(it, it.player(), Tile(3564, 3312, 0), 2) } //Barrows teleport
		repo.onItemOption1(19631) @Suspendable { tele(it, it.player(), Tile(3212, 3424, 0), 2) } //Ape Atoll teleport TODO: COORDS
		
		//Redirected
		repo.onItemOption1(11741) @Suspendable { tele(it, it.player(), Tile(2954, 3224), 1) } // Rimmington
		repo.onItemOption1(11742) @Suspendable { tele(it, it.player(), Tile(2894, 3465), 1) } // Taverley
		repo.onItemOption1(11743) @Suspendable { tele(it, it.player(), Tile(3340, 3004), 1) } // Pollnivneach
		repo.onItemOption1(11744) @Suspendable { tele(it, it.player(), Tile(2670, 3632), 1) } // Rellekka
		repo.onItemOption1(11745) @Suspendable { tele(it, it.player(), Tile(2758, 3178), 1) } // Brimhaven
		repo.onItemOption1(11746) @Suspendable { tele(it, it.player(), Tile(2544, 3095), 1) } // Yanille
		repo.onItemOption1(11747) @Suspendable { tele(it, it.player(), Tile(2910, 3612), 1) } // Trollheim
	}
	
	
	@Suspendable fun tele(it: Script, player: Player, target: Tile, radius: Int) {
		if (!Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
			return
		}
		
		if (!WildernessLevelIndicator.inWilderness(player.tile()) && WildernessLevelIndicator.inWilderness(target)) {
			if (it.optionsTitled("Teleport into the <col=FF0000>wilderness</col>?", "Yes.", "Heck no!") != 1) {
				return
			}
		}
		DeadmanMechanics.attemptTeleport(it)
		
		val used = player.inventory()[player.attrib(AttributeKey.ITEM_SLOT)]
		player.lockNoDamage()
		player.sound(965, 15)
		player.animate(4069, 16)
		it.delay(2)
		player.inventory().remove(Item(used.id(), 1), true)
		player.graphic(678)
		player.animate(4071)
		it.delay(2)
		if (PVPAreas.inPVPArea(player)) {
			player.teleport(PVPAreas.safetileFor(player)) // Go to inside edge bank, stay in pvp.
		} else {
			player.teleport(player.world().randomTileAround(target, radius))
		}
		player.animate(-1)
		player.graphic(-1)
		player.unlock()
		player.timers().cancel(TimerKey.FROZEN)
		player.timers().cancel(TimerKey.REFREEZE)
		player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
	}
}