package nl.bartpelle.veteres.content.events.christmas.carolschristmas.factoryworkers

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-15.
 */

object PartyHatToys {
	
	val RED_PARTY_HAT = 7511
	val BLUE_PARTY_HAT = 7512
	val GREEN_PARTY_HAT = 7513
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishSamsToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(GREEN_PARTY_HAT, world, Tile(3172, 5335))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3172, 5347)
						s.delay(13)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			finishSamsToy()
			it.onInterrupt { finishSamsToy() }
		}
		
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishCaveGoblinsToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(RED_PARTY_HAT, world, Tile(3172, 5333))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3172, 5347)
						s.delay(14)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			
			finishCaveGoblinsToy()
			it.onInterrupt { finishCaveGoblinsToy() }
		}
		
		r.onWorldInit @Suspendable {
			val world = it.ctx<World>()
			
			it.delay(1)
			fun finishPalumsToy() {
				world.server().scriptExecutor().executeLater(world, @Suspendable { s ->
					while (true) {
						val toy = Npc(BLUE_PARTY_HAT, world, Tile(3169, 5333))
						
						world.registerNpc(toy)
						s.delay(2)
						toy.pathQueue().interpolate(3169, 5339)
						s.delay(6)
						toy.pathQueue().interpolate(3172, 5339)
						s.delay(3)
						toy.pathQueue().interpolate(3172, 5347)
						s.delay(8)
						world.unregisterNpc(toy)
						s.delay(1)
					}
				})
			}
			
			finishPalumsToy()
			it.onInterrupt { finishPalumsToy() }
		}
		
		
	}
}