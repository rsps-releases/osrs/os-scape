package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.Help
import nl.bartpelle.veteres.content.interfaces.OptionList
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2/13/2016.
 */

object Crier {
	
	val CRIER = 276
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcSpawn(CRIER) @Suspendable {
			it.onInterrupt { shout(it) }
			shout(it)
		}
		r.onNpcOption1(CRIER) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.player().world().server().scriptExecutor().executeScript(it.player(), OptionList.DisplayOptions("OS-Scape Help", Help.SCROLL_OPTIONS))
			}
		}
		r.onNpcOption2(CRIER) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.chatNpc("Would you like to view the latest updates?", CRIER)
				if (it.options("Yes.", "No.") == 1) {
					it.player().write(AddMessage("https://forum.os-scape.com/index.php?forums/server-updates.4/", AddMessage.Type.URL))
				}
			}
		}
	}
	
	@Suspendable private fun shout(it: Script) {
		val npc: Npc = it.npc()
		
		if (npc.world().realm().isPVP) {
			npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
				s.onInterrupt { shout(s) }
				while (true) {
					val random = s.npc().world().random(5)
					var message = ""
					when (random) {
						1 -> message = "Want a quick run down of how our PvP server works? Right click me!"
						2 -> message = "Your quest tab contains important information, be sure to take a look!"
						3 -> message = "For a list of our available commands, simply type ::commands!"
						4 -> message = "Low on blood money? Get 1k for a minute of your time by typing ::vote!"
						else -> "You can purchase items to quick-start your adventure. Simly type ::store!"
					}
					npc.sync().shout(message)
					s.delay(1)
					npc.sync().shout(message)
					s.delay(1)
					npc.sync().shout(message)
					s.delay(1)
					npc.sync().shout(message)
					s.delay(4)
				}
			})
		}
	}
}