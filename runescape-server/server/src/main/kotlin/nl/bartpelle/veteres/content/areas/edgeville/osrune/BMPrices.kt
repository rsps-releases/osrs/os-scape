package nl.bartpelle.veteres.content.areas.edgeville.osrune

import co.paralleluniverse.fibers.Suspendable

/**
 * Created by Situations on 7/20/2016.
 * This is for World 3 - where Blood Money is a PKP currency
 */

object BMPrices {
	
	@JvmStatic @Suspendable fun getPriceOf(itemid: Int): Int {
		when (itemid) {
			4207 -> return 250 //Crystal seed
			12791 -> return 500 //Rune pouch
			12759 -> return 125 //Green Dbow paint
			12761 -> return 125 //Yellow Dbow paint
			12763 -> return 125 //White Dbow paint
			12757 -> return 125 //Blue Dbow paint
			12771 -> return 150 //Volcanic whip mix
			12769 -> return 150 //Frozen whip mix
			12849 -> return 150 //Granite clamp
			12526 -> return 200 // Fury ornament kit
			12528 -> return 200 // Dark infinity colour kit
			12530 -> return 200 // Light infinity colour kit
			12532 -> return 100 // Dragon sq shield ornament kit
			12534 -> return 150 // Dragon chainbody ornament kit
			12536 -> return 150 // Dragon plate/skirt ornament kit
			12538 -> return 200 // Dragon full helm ornament kit
			20143 -> return 200 // Dragon defender ornament kit
			20062 -> return 250 // Amulet of troture ornament kit
			20065 -> return 250 // Occult ornament kit
			12802 -> return 250 // Ward upgrade kit
			20002 -> return 150 // Dragon scimitar ornament kit
			20068 -> return 200 // Armadyl godsword ornament kit
			20071 -> return 250 // Bandos godsword ornament kit
			20074 -> return 250 // Saradomin godsword ornament kit
			20077 -> return 150 // Zamorak godsword ornament kit
			10548 -> return 100 // Fighter hat
			10551 -> return 100 // Fighter torso
			6570 -> return 150// Fire Cape
			7458 -> return 30// Adamant Gloves
			7462 -> return 50 // Barrows Gloves
			3842 -> return 50// Unholy Book
			12798 -> return 50 // Steam staff kit
			12800 -> return 50 // Dragon pickaxe kit
			12954 -> return 50 // Dragon Defender
			8839 -> return 300// Void Knight Top
			8840 -> return 300// Void Knight Robe
			8841 -> return 50// Void Knight Mace
			8842 -> return 50// Void Knight Gloves
			11663 -> return 250 // Void Mage Helm
			11664 -> return 250 // Void Ranger Helm
			11665 -> return 250 // Void Melee Helm
			13072 -> return 400 // Elite Void Top
			13073 -> return 400 // Elite Void Robe
			10499 -> return 30 // Ava's Accumulator
			11770 -> return 400 //  Seers ring (i)
			11771 -> return 350 // Archers ring(i)
			11772 -> return 300 // Warriors ring (i
			11773 -> return 500 // Berserker ring (i))
			13124 -> return 750 // Ardy cloak 4
			else -> return -1
		}
	}
}
