package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.sigmund

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.mechanics.Trading
import nl.bartpelle.veteres.content.mechanics.Trading.clearAcceptance
import nl.bartpelle.veteres.content.quests.QuestGuide
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.DonatorBoost
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import java.text.NumberFormat

/**
 * Created by Mack on 10/10/2017.
 */
object Sigmund {

	/**
	 * The npc's id.
	 */
	const val ID = 3894

	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		SigmundExchangeConstants.load()
		sr.onNpcOption2(ID, @Suspendable { exchange(it) })
		sr.onNpcOption3(ID, @Suspendable { viewBuyableGoods(it.player()) })
	}

	/**
	 * Displays all of Sigmund's dialogue options.
	 */
	@Suspendable fun displayOptions(it: Script) {
		when (it.options("Who are you?", "What goods do you currently buy?", "I'd like to sell you some of my goods.", "Nothing.")) {
			1 -> {
				it.chatNpc("I am Sigmund the Merchant, a collector of sorts. I seek valuable resources that can be harvested from the wilderness. Fish, logs, ores and so forth all peak my interest.", ID)
				it.chatNpc("Come back to me if you have anything of my interest.", ID)
			}
			2 -> {
				viewBuyableGoods(it.player())
			}
			3 -> {
				exchange(it)
			}
		}
	}

	/**
	 * Sets up the exchange session.
	 */
	fun exchange(it: Script) {
		val player = it.player()
		val exchange = SigmundExchangeContainer(player)

		//Stash the session to our attribute for referencing.
		player.putattrib(AttributeKey.SIGMUND_EXCHANGE_SESSION, exchange)

		setupOfferInterface(player, exchange.playersOffer(), exchange.sigmundsOffer())
	}

	/**
	 * Sends the approrpriate masks & scripts to enable trade screen interaction. We can't fully adopt the {@link Trading#initiateTrade} method as it requires a player-to-player
	 * interaction while this is just player-to-npc.
	 */
	fun setupOfferInterface(player: Player, offer: ItemContainer, reward: ItemContainer) {

		player.varps().varbit(Varbit.TRADE_MODIFIED_LEFT, 0)
		player.interfaces().sendMain(Trading.SCREEN_ONE)
		player.interfaces().sendInventory(Trading.INVENTORY_PANE)
		player.write(InterfaceText(Trading.SCREEN_ONE, 31, "Trading with: Sigmund the Merchant"))
		player.write(InterfaceText(Trading.SCREEN_ONE, 24, "Your offer: <br>(Value: <col=ffffff>0</col> Blood Money)"))
		player.write(InterfaceText(Trading.SCREEN_ONE, 27, "Sigmund offers:<br>(Value: <col=ffffff>0</col> Blood Money)"))

		player.write(InterfaceText(Trading.SCREEN_ONE, 9, "Sigmund has 27 free inventory slots."))

		player.write(SetItems(90, offer))
		player.write(SetItems(90, -2, 0, reward))
		player.invokeScript(149, (Trading.INVENTORY_PANE shl 16) or 0, 93, 4, 7, 0, -1, "Offer<col=ff9040>", "Offer-5<col=ff9040>", "Offer-10<col=ff9040>", "Offer-All<col=ff9040>", "Offer-X<col=ff9040>")

		player.write(InterfaceSettings(Trading.INVENTORY_PANE, 0, 0, 28, 1086))

		player.write(InterfaceSettings(Trading.SCREEN_ONE, 28, 0, 27, 1024))
		player.write(InterfaceSettings(Trading.SCREEN_ONE, 25, 0, 27, 1086))
	}

	/**
	 * Displays the confirmation screen in the Sigmund exchange.
	 */
	fun displaySecondScreen(player: Player, session: SigmundExchangeContainer) {
		// Security
		if (!player.interfaces().visible(Trading.INVENTORY_PANE)) {
			return
		}

		//Reset accept status
		player.putattrib(AttributeKey.TRADE_ACCEPTED, false)

		//Send the second screen and set it up with text required.
		player.interfaces().sendMain(Trading.SCREEN_TWO) // Show 2nd screen
		player.write(InterfaceText(Trading.SCREEN_TWO, 30, "Trading with:<br>Sigmund the Merchant"))
		player.write(InterfaceText(Trading.SCREEN_TWO, 23, "You are about to give:<br>"))
		player.write(InterfaceText(Trading.SCREEN_TWO, 24, "In return you will receive:<br>"))
		player.write(InterfaceText(Trading.SCREEN_TWO, 4, "Other player has accepted."))
	}

	/**
	 * Completes the transaction.
	 */
	fun completeExchange(player: Player, session: SigmundExchangeContainer) {

		//Give the BM.
		for (item in session.sigmundsOffer().items()) {
			player.inventory().add(item)
		}

		session.playersOffer().empty()
		session.sigmundsOffer().empty()

		//Clear the item containers on the interface.
		player.write(SetItems(90, session.playersOffer()))
		player.write(SetItems(90, -2, 0, session.sigmundsOffer()))

		player.interfaces().closeById(Trading.INVENTORY_PANE) // Interface component
		player.interfaces().closeMain() // Main trading window
		player.stopActions(false) //Close any input dialogues
		player.clearattrib(AttributeKey.SIGMUND_EXCHANGE_SESSION) //clear the session attrib

		//Reset text on child ids.
		player.write(InterfaceText(Trading.SCREEN_ONE, 30, ""))
		player.write(InterfaceText(Trading.SCREEN_TWO, 4, ""))

		player.message("Trade complete.")
	}

	/**
	 * Custom offer logic, similar to tradings, but oriented towards the Sigmund exchange.
	 */
	fun offerItem(player: Player, session: SigmundExchangeContainer, amount: Int) {
		val item = player.inventory()[player[AttributeKey.BUTTON_SLOT]] ?: return

		// Safety check to prevent people from selling anything not permitted.
		val bloodMoneyPrice = SigmundExchangeConstants.sigmundPrices[item.unnote(player.world()).id()] ?: 0
		if (bloodMoneyPrice <= 0) {
			player.message("He doesn't seem very interested in buying this item. Try another.")
			return
		}

		val removed = player.inventory().remove(Item(item.id(), amount), true).completed()
		val boost = ((bloodMoneyPrice * player.donationTier().modifiers()[DonatorBoost.SIGMUND.ordinal]) - bloodMoneyPrice).toInt()

		session.playersOffer().add(Item(item.id(), removed), true)
		session.sigmundsOffer().add(Item(13307, (bloodMoneyPrice + boost) * removed))

		player.clearAcceptance()

		player.write(InterfaceText(Trading.SCREEN_ONE, 30, "Other player accepted trade."))
	}

	/**
	 * Removes an item from the player's offer container and updates the interface.
	 * Sigmund's offer container is updated accordingly.
	 */
	fun removeItem(player: Player, session: SigmundExchangeContainer, amount: Int) {
		if (player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return
		val item = session.playersOffer()[player[AttributeKey.BUTTON_SLOT]] ?: return
		val result = session.playersOffer().remove(Item(item.id(), amount), true)
		val removed = result.completed()
		val bloodMoneyPrice = SigmundExchangeConstants.sigmundPrices[item.unnote(player.world()).id()] ?: return
		val boost = ((bloodMoneyPrice * player.donationTier().modifiers()[DonatorBoost.SIGMUND.ordinal]) - bloodMoneyPrice).toInt()

		session.sigmundsOffer().remove(Item(13307, (bloodMoneyPrice + boost) * removed), true)
		player.inventory().add(Item(item.id(), removed), true)

		player.clearAcceptance()

		player.write(InterfaceText(Trading.SCREEN_ONE, 30, "Other player accepted trade."))
	}

	/**
	 * A function to display the list of goods Sigmund will currently buy from a player.
	 */
	@JvmStatic fun viewBuyableGoods(player: Player) {
		var ptr = 7

		QuestGuide.clear(player)
		player.interfaces().text(275, 2, "Sigmund the Merchant")
		SigmundExchangeConstants.sigmundPrices.forEach({ k, v ->
			val cost = v
			val delta = ((cost * player.donationTier().modifiers()[DonatorBoost.SIGMUND.ordinal]) - cost).toInt()

			player.interfaces().text(275, 5, "Below is a list of items Sigmund is interested in purchasing.")
			player.interfaces().text(275, ptr++, "-> " + Item(k).definition(player.world()).name + " <img=40> ${NumberFormat.getInstance().format(cost)} ${if(delta != 0)" + <col=8B0000>(${NumberFormat.getInstance().format(delta)} BM boost)" else ""}")
		})

		player.interfaces().sendMain(275)
	}
}