package nl.bartpelle.veteres.content.areas.dungeons.fremennikslayer

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/10/2016.
 */

object SlayercaveEntrance {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(2123) @Suspendable {
			it.delay(1)
			it.player().teleport(Tile(2808, 10002))
		}
	}
	
}

