package nl.bartpelle.veteres.content.npcs.bosses.abyssalsire

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 10/11/2016.
 */

object AbyssalSireTentacles {
	
	val ABYSSAL_SIRE_TENTACLES = intArrayOf(5909, 5911, 5910)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (tentacles in ABYSSAL_SIRE_TENTACLES) {
			r.onNpcSpawn(tentacles) @Suspendable { idleScript(it.npc()) }
		}
	}
	
	@Suspendable fun idleScript(tentacle: Npc) {
		tentacle.world().server().scriptExecutor().executeLater(tentacle, @Suspendable { s ->
			var tentacleState = tentacle.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.STASIS)
			
			while (tentacleState == AbyssalSireState.STASIS) {
				tentacle.faceTile(Tile(tentacle.tile().x + 4, tentacle.tile().z))
				tentacleState = tentacle.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.STASIS)
				s.delay(1)
			}
		})
	}
	
	fun getTentacles(abyssalSire: Npc): ArrayList<Npc> {
		return abyssalSire.attribOr<ArrayList<Npc>>(AttributeKey.ABYSSAL_SIRE_TENTACLES, ArrayList<Npc>())
	}
	
	fun lockTentacles(tentacles: ArrayList<Npc>) {
		tentacles.forEach { t -> t.lock() }
	}
	
	fun unlockTentacles(tentacles: ArrayList<Npc>) {
		tentacles.forEach { t -> t.unlock() }
	}
	
	fun animateTentacles(tentacles: ArrayList<Npc>, animID: Int) {
		tentacles.forEach { t -> t.animate(animID) }
	}
	
	fun transmogTentacles(tentacles: ArrayList<Npc>, transmogID: Int) {
		tentacles.forEach { t -> t.sync().transmog(transmogID) }
	}
	
	fun putCombatState(tentacles: ArrayList<Npc>, state: AbyssalSireState) {
		tentacles.forEach { t -> t.putattrib(AttributeKey.ABYSSAL_SIRE_STATE, state) }
	}
	
	fun facePlayer(tentacles: ArrayList<Npc>, target: Entity) {
		tentacles.forEach { t -> t.faceTile(target.tile()) }
	}
	
	fun faceSouth(tentacles: ArrayList<Npc>) {
		tentacles.forEach { t -> t.faceTile(Tile(t.tile().x + 4, t.tile().z)) }
	}
	
	fun removeClipping(tentacles: ArrayList<Npc>) {
		for (i in 1..tentacles.size) {
			tentacles.forEach { tentacle ->
				when (i) {
					1, 4 -> for (i in 0..2) tentacle.world().removeObj(MapObj(Tile(tentacle.tile().x + 4, tentacle.tile().z + 9 - i), 4451, 10, 3), true)
					3, 6 -> for (i in 0..2) tentacle.world().removeObj(MapObj(Tile(tentacle.tile().x + 4, tentacle.tile().z - i), 4451, 10, 3), true)
				}
			}
		}
	}
	
	fun attemptToHitPlayer(it: Script, tentacles: ArrayList<Npc>, target: Entity) {
		tentacles.forEach { t ->
			it.runGlobal(target.world()) @Suspendable {
				val tentacleX = t.tile().x + 4
				val tentacleZ = t.tile().z + 4
				
				if (target.tile().inSqRadius(Tile(tentacleX, tentacleZ), 4)) {
					target.hit(target, t.world().random(30))
					t.animate(7109)
				}
			}
		}
	}
	
}
