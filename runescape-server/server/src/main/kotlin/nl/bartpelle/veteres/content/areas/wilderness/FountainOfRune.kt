package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/16/2015.
 */

object FountainOfRune {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnObject(26782) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			for (uncharged in intArrayOf(1704, 2572, 12785)) {
				if (item == uncharged) {
					it.player().lock()
					it.message("You hold the jewellery against the fountain..")
					it.delay(1)
					it.animate(832)
					
					val amount = it.player().inventory().count(uncharged)
					it.player().inventory().remove(Item(uncharged, amount), true)
					var replacement = when (uncharged) {
						2572 -> 11980
						12785 -> 20786
						else -> 11978 // glory (5)
					}
					
					if (uncharged == 1704 && it.player().world().rollDie(5000, 1)) {
						replacement = 19707
					}
					it.player().inventory().add(Item(replacement, amount), true)
					it.itemBox("You feel a power emanating from the fountain as it recharges your jewellery.", replacement)
					
					it.player().unlock()
				} else {
					it.message("The fountain can't recharge that.")
				}
			}
		}
	}
	
}
