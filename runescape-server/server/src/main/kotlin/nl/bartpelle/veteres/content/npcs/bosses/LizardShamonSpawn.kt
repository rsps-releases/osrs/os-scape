package nl.bartpelle.veteres.content.npcs.bosses

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc


/**
 * Created by Situations on 3/17/2016.
 */

class LizardShamonSpawn(val target: Entity) {
	
	
	@JvmField val suicide: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		
		while (EntityCombat.targetOk(npc, target)) {
			if (npc.tile().inArea(1416, 3693, 1453, 3726)) {
				if (EntityCombat.canAttackMelee(npc, target, true)) {
					do_we_explode(it, target, npc)
				} else {
					do_we_explode(it, target, npc)
				}
			} else {
				npc.world().unregisterNpc(npc)
			}
		}
		npc.world().unregisterNpc(npc)
	}
	
	@Suspendable fun do_we_explode(it: Script, target: Entity, npc: Npc) {
		npc.face(target)
		if (npc.world().rollDie(4, 1)) {
			blow_me_up(it, target, npc)
		}
		it.delay(1)
	}
	
	@Suspendable fun blow_me_up(it: Script, target: Entity, npc: Npc) {
		it.delay(1 + npc.world().random(2))
		npc.animate(7159)
		it.delay(2)
		npc.hidden(true)
		npc.world().tileGraphic(1295, npc.tile(), 1, 0)
		
		it.runGlobal(target.world()) @Suspendable {
			target.world().players().forEachKt({ p ->
				if (p.tile().inSqRadius(npc.tile(), 1))
					p.hit(target, target.world().random(10))
			})
		}
		
		it.delay(2)
		npc.world().unregisterNpc(npc)
		
	}
}
