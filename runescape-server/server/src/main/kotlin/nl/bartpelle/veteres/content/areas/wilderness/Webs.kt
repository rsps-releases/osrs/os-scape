package nl.bartpelle.veteres.content.areas.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.EquipmentInfo

/**
 * Created by Situations on 11/9/2015.
 */

object Webs {

	const val KNIFE = 946
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(733) @Suspendable {
			val player = it.player()
			val weaponId = player.equipment()[EquipSlot.WEAPON]?.id() ?: -1;
			val wepName = (Item(weaponId).definition(player.world())?.name ?: "").toLowerCase()
			val hasSharpEdge = "sword" in wepName || "dagger" in wepName || "axe" in wepName || "whip" in wepName
					|| "scimitar" in wepName || "dead" in wepName || "tent" in wepName || "claw" in wepName

			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			it.player().faceObj(obj)
			if (player.inventory().has(KNIFE)) {
				it.animate(911)
				slashWeb(it, obj)
			} else if (hasSharpEdge) {
				it.animate(EquipmentInfo.attackAnimationFor(player))
				slashWeb(it, obj)
			} else {
				it.player().message("Only a sharp blade can cut through this sticky web.")
			}
		}

		repo.onItemOnObject(733) @Suspendable {
			val itemUsed = it.itemUsed()
			val wepName = itemUsed.definition(it.player().world())?.name ?: ""
			val hasSharpEdge = "sword" in wepName || "dagger" in wepName || "axe" in wepName || "whip" in wepName
					|| "scimitar" in wepName || "dead" in wepName || "tent" in wepName || "claw" in wepName

			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			it.player().faceObj(obj)
			if (itemUsed.id() == KNIFE) {
				it.animate(911)
				slashWeb(it, obj)
			} else if (hasSharpEdge) {
				it.animate(EquipmentInfo.attackAnimationFor(it.player(), itemUsed.id()))
				slashWeb(it, obj)
			} else {
				it.player().message("Only a sharp blade can cut through this sticky web.")
			}
		}
	}
	
	@Suspendable fun slashWeb(it: Script, obj: MapObj) {
		val player = it.player()
		
		if (player.world().random(100) >= 50) {
			it.message("You slash the web apart.")
			player.lockDamageOk()
			it.delay(1)
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val old = MapObj(obj.tile(), 733, obj.type(), obj.rot())
				val spawned = MapObj(obj.tile(), 734, obj.type(), obj.rot())
				
				world.removeObj(old)
				world.spawnObj(spawned)
				it.delay(100)
				world.removeObjSpawn(spawned)
				world.spawnObj(old)
			}
			it.delay(1)
			it.player().unlock()
		} else {
			it.message("You fail to cut through it.")
		}
	}
	
}
