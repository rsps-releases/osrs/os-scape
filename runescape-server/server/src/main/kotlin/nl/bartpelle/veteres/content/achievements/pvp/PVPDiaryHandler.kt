package nl.bartpelle.veteres.content.achievements.pvp

import nl.bartpelle.veteres.content.achievements.Achievement
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.achievements.AchievementDiaryHandler
import java.util.stream.Collectors

/**
 * @author Mack
 */
class PVPDiaryHandler : AchievementDiaryHandler {

    /**
     * The set of achievements for this handler.
     */
    private var achievements: MutableSet<Achievement> = mutableSetOf()

    /**
     * Returns all of the achievements matching the specified key type into a new set.
     */
    override fun achievementsByKey(key: AchievementCategory): Set<Achievement> {
        return achievements().stream().filter({a -> a.type() == key}).collect(Collectors.toSet())
    }

    /**
     * Initializes the set of achievements, if empty, otherwise returns the existing collection.
     */
    override fun achievements(): MutableSet<Achievement> {
        if (achievements.isEmpty()) {
            for (difficulty in PVPAchievements.values()) {
                achievements.addAll(difficulty.achievements)
            }
        }
        return achievements
    }

    /**
     * A function that flags if the specified achievement is able to be executed outside of the wilderness
     * within the world.
     */
    fun exception(id: Int): Boolean {
        return arrayOf(26, 32, 33, 38, 39).contains(id)
    }
}