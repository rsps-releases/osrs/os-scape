package nl.bartpelle.veteres.content.areas.ardougne

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 01/11/2016.
 *
 * Manhole 4Head
 */
object ArdougneManhole {
	
	val MANHOLE = 881
	val EXIT_STAIRS = 18354
	val RUIN_ENTERANCE = 18270
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(MANHOLE) @Suspendable {
			Ladders.ladderDown(it, Tile(2632, 9695), true)
		}
		
		repo.onObject(EXIT_STAIRS) @Suspendable {
			if (it.interactionObject().tile().equals(2696, 9682)) {
				it.player().teleport(Tile(2697, 3283))
			}
		}
		
		repo.onObject(RUIN_ENTERANCE) @Suspendable {
			if (it.interactionObject().tile().equals(2696, 3283)) {
				it.player().teleport(Tile(2696, 9683))
			}
		}
	}
}