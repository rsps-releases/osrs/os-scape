package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.Poison
import nl.bartpelle.veteres.content.mechanics.Venom
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.Varp
import java.lang.ref.WeakReference

/**
 * Created by Situations on 1/6/2016.
 * Modified by Jak to support pvp instance visiting and leaving. 24/1/2016 dd:mm:yyyy cos im normal y0
 */

object SurgeonGeneralTafani {
	
	val TAFANI = 3343
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(TAFANI) @Suspendable {
			heal(it)

			val ref = it.player().attrib<WeakReference<Npc>>(AttributeKey.TARGET)
			val npc = ref.get()
			if (npc != null) {
				npc.animate(881)
				it.chatNpc("There you go, you should be all set. Stay safe out there now.", TAFANI)
			}
		}

		r.onObject(23709) @Suspendable {
			heal(it)
		}
	}

	@Suspendable @JvmStatic fun heal(it: Script) {
		it.player().hp(Math.max(it.player().skills().level(Skills.HITPOINTS), it.player().skills().xpLevel(Skills.HITPOINTS)), 20) //Set hitpoints to 100%
		Poison.cure(it.player()) //Cure the player from any poisons
		Venom.cure(2, it.player())
		it.player().skills().replenishSkill(5, it.player().skills().xpLevel(5)) //Set the players prayer level to full
		it.player().skills().replenishStatsToNorm()
		it.player().putattrib(AttributeKey.RUN_ENERGY, 100.0) //Set the players run energy to 100
		
		if (it.player().timers().has(TimerKey.RECHARGE_SPECIAL_ATTACK)) {
			it.message("You can recharge your special energy again in ${it.player().timers().asMinutesAndSecondsLeft(TimerKey.RECHARGE_SPECIAL_ATTACK)}.")
		} else {
			val donor = it.player().donationTier() != DonationTier.NONE
			it.player().varps().varp(Varp.SPECIAL_ENERGY, 1000) //Set energy to 100%
			it.player().timers().register(TimerKey.RECHARGE_SPECIAL_ATTACK, if (donor) 145 else 150) //Donors get slightly less cd timer than normal.
		}
		
		it.player().graphic(436, 48, 0)
		it.player().sound(958)
	}
	
	@Suspendable @JvmStatic fun tafaniop1(it: Script) {
		if (GameCommands.PVP1_OFF || it.player().world().realm().isOSRune) {
			normaloptions(it)
		} else {
			val goingToPvp: Boolean = !PVPAreas.inPVPArea(it.player())
			when (it.optionsTitled("What can Tafani help with?", if (!goingToPvp) "I'd like to leave." else "PvP instances", "Heal me!")) {
				1 -> {
					if (goingToPvp) {
						it.player().world().server().scriptExecutor().executeScript(it.player(), PVPAreas.showPvpInstanceOptions)
					} else {
						Teleports.basicTeleport(it, Tile(3092, 3500))
						it.player().timers().cancel(TimerKey.FROZEN)
						it.player().timers().cancel(TimerKey.REFREEZE)
						it.player().write(UpdateStateCustom(1)) // normal world
					}
				}
				2 -> {
					normaloptions(it)
				}
			}
		}
	}
	
	@Suspendable fun normaloptions(it: Script) {
		it.chatNpc("Wow, you look like you've been taking a beating! Want me to patch you up?", TAFANI, 590)
		when (it.options("Yes, I'd love that.", "No thank you.")) {
			1 -> {
				it.chatPlayer("Yes, I'd love that.")
				healPlayer(it)
			}
			2 -> {
				it.chatPlayer("No thank you.")
				it.chatNpc("Okay.. if you say so.", TAFANI, 590)
			}
		}
	}
	
	@Suspendable fun healPlayer(it: Script) {
		
		it.player().hp(Math.max(it.player().skills().level(Skills.HITPOINTS), it.player().skills().xpLevel(Skills.HITPOINTS)), 20) //Set hitpoints to 100%
		Poison.cure(it.player()) //Cure the player from any poisons
		Venom.cure(2, it.player())
		it.player().skills().replenishSkill(5, it.player().skills().xpLevel(5)) //Set the players prayer level to full
		it.player().skills().replenishStatsToNorm()
		it.player().putattrib(AttributeKey.RUN_ENERGY, 100.0) //Set the players run energy to 100
		
		//Send the player a message letting them know the... outcum. ; )
		if (it.player().world().rollDie(10, 1) && it.player().world().realm().isPVP) {
			it.player().message("The nurse grabs your cock steadily and beats it until you feel replenished.")
		} else {
			it.player().message("Tafani heals you, cures your poisons, and recharges your prayer.")
		}
	}
	
	@Suspendable fun refillSpecial(it: Script) {
		if (it.player().timers().has(TimerKey.RECHARGE_SPECIAL_ATTACK)) {
			it.message("Tafani will only restore your special every couple of minutes.")
		} else {
			it.message("Tafani recharges your special attack.")
			it.player().varps().varp(Varp.SPECIAL_ENERGY, 1000) //Set energy to 100%
			it.player().varps().varp(Varp.SPECIAL_ENABLED, 0) //Disable special attack
			it.player().timers().register(TimerKey.RECHARGE_SPECIAL_ATTACK, 150) //Set the value of the timer. Currently 1:30m
		}
	}
}