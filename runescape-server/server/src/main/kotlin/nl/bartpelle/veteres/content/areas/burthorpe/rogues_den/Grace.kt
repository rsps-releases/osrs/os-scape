package nl.bartpelle.veteres.content.areas.burthorpe.rogues_den

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 5/16/2016.
 */
object Grace {
	
	const val GRACE = 5919
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		r.onNpcOption1(GRACE) @Suspendable {
			it.chatNpc("What can I do for you?", GRACE, 554)
			when (it.optionsTitled("What would you like to say?", "I don't know. What can you do for me?", "Can I see what you're selling?", "I'm alright, thanks.")) {
				1 -> {
					it.chatPlayer("I don't know. What can you do for me?", 554)
					it.chatNpc("A good question indeed! I'm selling special clothing for<br>Agility enthusiasts.", GRACE, 606)
					it.chatNpc("Sometimes, when you're exploring the hidden rooftops of<br>our cities, you'll find one of my Marks.", GRACE, 589)
					it.chatNpc("Once you've got enough Marks, I'll exchange them for<br>my Graceful clothing. So, does that interest you?", GRACE, 568)
					
					when (it.optionsTitled("What would you like to say?", "Can I see what you're selling?", "I'm alright, thanks.")) {
						1 -> {
							it.chatPlayer("Can I see what you're selling?", 554)
							it.player().world().shop(38).display(it.player())
						}
						2 -> it.chatPlayer("I'm alright, thanks.", 562)
					}
				}
				2 -> {
					it.chatPlayer("Can I see what you're selling?", 554)
					it.player().world().shop(38).display(it.player())
				}
				3 -> it.chatPlayer("I'm alright, thanks.", 562)
			}
		}
		
		r.onNpcOption2(GRACE) {
			it.player().world().shop(38).display(it.player())
		}
	}
	
	
}