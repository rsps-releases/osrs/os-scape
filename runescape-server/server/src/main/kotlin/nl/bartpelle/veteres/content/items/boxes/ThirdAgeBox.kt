package nl.bartpelle.veteres.content.items.boxes

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 1/25/2016.
 */

object ThirdAgeBox {
	
	val third_age_box = 13438 //third age box item ID
	
	//Array list to hold common items received by the mysterious box. (Items around 0-1500 BM)
	val items: ArrayList<Item> = arrayListOf(Item(10330, 1), //3rd age range top
			Item(10332, 1), //3rd age range legs
			Item(10334, 1), //3rd age range coif
			Item(10336, 1), //3rd age range vambraces
			Item(10338, 1), //3rd age robe top
			Item(10340, 1), //3rd age robe
			Item(10342, 1), //3rd age mage hat
			Item(10344, 1), //3rd age amulet
			Item(10346, 1), //3rd age platelegs
			Item(10348, 1), //3rd age platebody
			Item(10350, 1), //3rd age full helmet
			Item(10352, 1)) //3rd age kite shield
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(third_age_box) @Suspendable {
			it.itemBox("Opening this box gives you a random third age item. Are you sure you want to open the box?", items[it.player().world().random(items.size - 1)].id())
			if (it.optionsTitled("Open the Third Age box?", "Yes.", "No.") == 1) {
				//Size of the reward array list
				val reward = it.player().world().random(items.size - 1)
				
				//If chance is less than 2 we give them a very rare reward..
				val def = it.player().world().definitions().get(ItemDefinition::class.java, items[reward].unnote(it.player().world()).id())
				it.player().inventory().remove(Item(third_age_box), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(Item(items[reward].id(), items[reward].amount()), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().message("You open the mysterious box to find a ${def.name}.")
			}
		}
	}
}
