package nl.bartpelle.veteres.content.areas.revenantcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.script.ScriptRepository

object RevenantCaves {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Cave entrance
		r.onObject(31555) {
			it.player().lockDamageOk()
			it.animate(2796)
			it.delay(2)
			it.animate(-1)
			it.player().teleport(3196, 10056)
			it.player().filterableMessage("You enter the cave.")
			it.player().unlock()
		}
		// Cave exit
		r.onObject(31557) {
			it.player().lockDamageOk()
			it.animate(2796)
			it.delay(2)
			it.animate(-1)
			it.player().teleport(3075, 3653)
			it.player().filterableMessage("You exit the cave.")
			it.player().unlock()
		}
		// Northern cave exit
		r.onObject(31558) {
			it.player().lockDamageOk()
			it.animate(2796)
			it.delay(2)
			it.animate(-1)
			it.player().teleport(3126, 3832)
			it.player().filterableMessage("You exit the cave.")
			it.player().unlock()
		}
		// Northern cave entrance
		r.onObject(31556) {
			it.player().lockDamageOk()
			it.animate(2796)
			it.delay(2)
			it.animate(-1)
			it.player().teleport(3241, 10234)
			it.player().filterableMessage("You enter the cave.")
			it.player().unlock()
		}
		
		r.makeRemoteObject(31561)
		r.onObject(31561, this::doGapJump)
	}
	
	@Suspendable fun doGapJump(it: Script) {
		val obj = it.interactionObject()
		val dest = it.player().walkTo(obj.tile().x, obj.tile().z, PathQueue.StepType.REGULAR)
		it.waitForTile(dest)
		
		if (obj.tile().distance(it.player().tile()) <= 2) {
			val delta = obj.tile() - it.player().tile()
			println("$delta")
			
			it.player().lockDelayDamage()
			it.player().faceTile(it.player().tile() + Tile(delta.x * 2, delta.z * 2))
			it.delay(1)
			
			it.player().forceMove(ForceMovement(0, 0, delta.x, delta.z, 30, 45, FaceDirection.forTargetTile(delta.x, delta.z)))
			it.player().animate(741, 15)
			it.delay(2)
			it.player().teleport(it.player().tile() + Tile(delta.x, delta.z))
			it.player().forceMove(ForceMovement(0, 0, delta.x, delta.z, 15, 28, FaceDirection.forTargetTile(delta.x, delta.z)))
			it.player().animate(741, 0)
			it.delay(2)
			it.player().teleport(it.player().tile() + Tile(delta.x, delta.z))
			
			it.player().message("You make it across the jump without any problems.")
			it.player().unlock()
		} else {
			it.message("You can't reach that.")
		}
	}
	
}