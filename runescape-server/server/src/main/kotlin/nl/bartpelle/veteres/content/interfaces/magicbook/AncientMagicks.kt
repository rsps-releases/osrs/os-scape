package nl.bartpelle.veteres.content.interfaces.magicbook

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterManager
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 3/20/2016.
 */

object AncientMagicks {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Teleports
		r.onButton(218, 97, s@ @Suspendable { attempt_home_teleport(it) }) //Home teleport
		r.onButton(218, 88) @Suspendable { cast_spell(it, 1, 54, Tile(3097, 9882, 0), 35.0, false, Item(556, 1), Item(554, 1), Item(563, 2)) } //Paddewwa teleport
		r.onButton(218, 89) @Suspendable { cast_spell(it, 1, 60, Tile(3349, 3346, 0), 35.0, false, Item(566, 1), Item(563, 2)) } //Senntisten teleport
		r.onButton(218, 90) @Suspendable { cast_spell(it, 1, 66, Tile(3497, 3487, 0), 35.0, false, Item(565, 1), Item(563, 2)) } //Kharyrll teleport
		r.onButton(218, 91) @Suspendable { cast_spell(it, 1, 72, Tile(3014, 3500, 0), 35.0, false, Item(555, 4), Item(563, 2)) } //Lassar teleport
		r.onButton(218, 92) @Suspendable { cast_spell(it, 1, 78, Tile(2966, 3696, 0), 35.0, false, Item(556, 2), Item(554, 3), Item(563, 2)) } //Dareeyak Teleport
		r.onButton(218, 93) @Suspendable { cast_spell(it, 1, 84, Tile(3163, 3664, 0), 35.0, false, Item(566, 2), Item(563, 2)) } //Carrallangar teleport
		r.onButton(218, 94) @Suspendable { cast_spell(it, 1, 90, Tile(3288, 3886, 0), 35.0, false, Item(565, 2), Item(563, 2)) } //Annakarl teleport
		r.onButton(218, 95) @Suspendable { cast_spell(it, 1, 96, Tile(2972, 3873, 0), 35.0, false, Item(555, 8), Item(563, 2)) } //Varrock teleport
		
		//Bounty teleport to target
		r.onButton(218, 92, s@ @Suspendable {
			
			if (!it.player().attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TARGET_TELEPORT_UNLOCKED, false)) {
				it.player().message("You have not unlocked this spell yet.")
				return@s
			}
			
			if (!WildernessLevelIndicator.inWilderness(it.player().tile())) {
				it.player().message("You must be in the Wilderness to use this spell.")
				return@s
			}
			
			if (it.player().attribOr<Player>(AttributeKey.BOUNTY_HUNTER_TARGET, null) != null) {
				
				if (!WildernessLevelIndicator.inWilderness(BountyHunterManager.getTargetFor(it.player())!!.tile())) {
					it.player().message("Your target is currently not in the Wilderness.")
					return@s
				}
				
				cast_spell(it,0,  86, it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).tile(), 45.0, false, Item(563, 1), Item(560, 1), Item(562, 1))
				it.player().varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY))
			} else {
				it.player().message("You currently have no target to teleport to!")
			}
		})
		
		//Combat spells
		register_spell(50, r, 218, 80, 183, 184, 1978, -1, 12, -1, Graphic(385), 227, 14, 2, 30.0, "Smoke rush", Item(562, 2), Item(560, 2), Item(556, 1), Item(554, 1)) // Smoke rush TODO poison
		register_spell(52, r, 218, 84, 178, 179, 1978, -1, 12, -1, Graphic(379), 227, 15, 2, 51.0, "Shadow rush", Item(562, 2), Item(560, 2), Item(556, 1), Item(566, 1)) // Shadow rush
		register_spell(56, r, 218, 76, 106, 102, 1978, -1, 12, -1, Graphic(373), 227, 16, 2, 33.0, "Blood rush", Item(562, 2), Item(560, 2), Item(565, 1)) // Blood rush
		register_spell(58, r, 218, 72, 171, 170, 1978, 360, 12, -1, Graphic(361), 227, 17, 2, 34.0, "Ice rush", Item(562, 2), Item(560, 2), Item(555, 2)) // Ice rush
		register_spell(62, r, 218, 82, 183, 184, 1979, -1, 12, -1, Graphic(381), 227, 19, 2, 36.0, "Smoke burst", Item(562, 4), Item(560, 2), Item(554, 2), Item(556, 2)) // Smoke burst TODO FIX GFX ANIM
		register_spell(64, r, 218, 86, 178, 179, 1979, -1, 12, -1, Graphic(382), 227, 19, 2, 37.0, "Shadow burst", Item(562, 4), Item(560, 2), Item(556, 2), Item(566, 2)) // Shadow burst
		register_spell(68, r, 218, 78, 106, 102, 1979, -1, 12, -1, Graphic(376), 227, 21, 2, 39.0, "Blood burst", Item(562, 4), Item(560, 2), Item(565, 2)) // Blood burst
		register_spell(70, r, 218, 74, 171, 173, 1979, -1, 12, -1, Graphic(363), 227, 22, 2, 40.0, "Ice burst", Item(562, 4), Item(560, 2), Item(555, 4)) // Ice burst
		register_spell(74, r, 218, 81, 183, 184, 1978, -1, 12, -1, Graphic(387), 227, 23, 2, 42.0, "Smoke blitz", Item(560, 2), Item(565, 2), Item(554, 2), Item(556, 2)) // Smoke blitz
		register_spell(76, r, 218, 85, 178, 179, 1978, -1, 12, -1, Graphic(381), 227, 24, 2, 43.0, "Shadow blitz", Item(560, 2), Item(565, 2), Item(556, 2), Item(566, 2)) // Shadow blitz
		register_spell(80, r, 218, 77, 106, 107, 1978, -1, 12, -1, Graphic(375), 227, 25, 2, 45.0, "Blood blitz", Item(560, 2), Item(565, 4)) // Blood blitz
		register_spell(82, r, 218, 73, 171, 169, 1978, -1, 12, 366, Graphic(367), 227, 26, 2, 46.0, "Ice blitz", Item(560, 2), Item(565, 2), Item(555, 3)) // Ice blitz
		register_spell(86, r, 218, 83, 183, 184, 1979, -1, 12, -1, Graphic(391), 227, 27, 2, 48.0, "Smoke barrage", Item(560, 4), Item(565, 2), Item(554, 4), Item(556, 4)) // Smoke barrage
		register_spell(88, r, 218, 87, 178, 179, 1979, -1, 12, -1, Graphic(383), 227, 28, 2, 49.0, "Shadow barrage", Item(560, 4), Item(565, 2), Item(556, 4), Item(566, 3)) // Shadow barrage
		register_spell(92, r, 218, 79, 106, 102, 1979, -1, 12, -1, Graphic(377), 227, 29, 2, 51.0, "Blood barrage", Item(560, 4), Item(565, 4), Item(566, 1)) // Blood barrage
		register_spell(94, r, 218, 75, 171, 168, 1979, 366, 12, -1, Graphic(369), 227, 30, 2, 52.0, "Ice barrage", Item(560, 4), Item(565, 2), Item(555, 6)) // Ice barrage
	}
}
