package nl.bartpelle.veteres.content.areas.dungeons.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/11/2016.
 */

object CaveEntrance {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(11836) @Suspendable {
			it.player().lock()
			it.player().teleport(Tile(2862, 9572))
			it.player().unlock()
		}
	}
}
