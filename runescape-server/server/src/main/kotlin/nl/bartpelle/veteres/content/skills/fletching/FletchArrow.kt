package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/15/2015.
 */

object FletchArrow {
	
	enum class Arrows(val item1: Int, val item2: Int, val outcome: Int, val xp: Double, val lvlreq: Int, val itemName: String, val shortName: String) {
		HEADLESS(52, 314, 53, 1.0, 1, "headless arrows", ""),
		BRONZE_ARROW(53, 39, 882, 1.3, 1, "bronze arrows", "bronze"),
		IRON_ARROW(53, 40, 884, 2.5, 15, "iron arrows", "iron"),
		STEEL_ARROW(53, 41, 886, 5.0, 30, "steel arrows", "steel"),
		MITHRIL_ARROW(53, 42, 888, 7.5, 45, "mithril arrows", "mithril"),
		BROAD_ARROW(53, 11874, 4160, 10.0, 52, "broad arrows", "broad"),
		ADAMANT_ARROW(53, 43, 890, 10.0, 60, "adamant arrows", "adamant"),
		RUNE_ARROW(53, 44, 892, 12.5, 75, "rune arrows", "rune"),
		AMETHYST_ARROW(53, 21350, 21326, 202.5, 82, "amethyst arrows", "amethyst"),
		DRAGON_ARROW(53, 11237, 11212, 15.0, 90, "dragon arrows", "dragon")
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		Arrows.values().forEach { arrow ->
			repo.onItemOnItem(arrow.item1.toLong(), arrow.item2.toLong(), s@ @Suspendable {
				//Check if the player has a high enough level to do this.
				if (it.player().skills()[Skills.FLETCHING] < arrow.lvlreq) {
					it.messagebox("You need a Fletching level of ${arrow.lvlreq} or above to make ${arrow.itemName}.")
					return@s
				}
				
				//Prompt the player with the # they'd like to make
				var num = 1
				if (it.player().inventory().count(arrow.item1) > 15) {
					num = it.itemOptions(Item(arrow.outcome, 150), offsetX = 12)
				}
				while (num-- > 0) {
					//Check if the player's ran out of supplies
					if (arrow.item1 !in it.player().inventory() || arrow.item2 !in it.player().inventory()) {
						break
					}
					
					val max = Math.min(it.player().inventory().count(arrow.item1), it.player().inventory().count(arrow.item2))
					
					if (arrow.item1 == 52 && arrow.item2 == 314) {
						it.player().inventory() -= Item(arrow.item1, max)
						it.player().inventory() -= Item(arrow.item2, max)
						it.player().inventory() += Item(arrow.outcome, max)
						it.player().message("You attach feathers to $max arrow shafts.")
						it.addXp(Skills.FLETCHING, arrow.xp * (max.toDouble() / 15.0))
						it.delay(2)
					} else {
						//Remove the supplies, give the player the item, animate, send exp, msg, and apply delay.
						it.player().inventory() -= Item(arrow.item1, max)
						it.player().inventory() -= Item(arrow.item2, max)
						it.player().inventory() += Item(arrow.outcome, max)
						it.player().message("You attach arrow heads to $max arrow shafts.")
						it.player().message("You attach ${arrow.shortName} heads to some of your arrows.")
						it.addXp(Skills.FLETCHING, arrow.xp * (max.toDouble() / 15.0))
						it.delay(2)
					}
				}
			})
		}
	}
}
