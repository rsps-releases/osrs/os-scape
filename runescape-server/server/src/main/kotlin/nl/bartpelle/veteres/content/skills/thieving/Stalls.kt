package nl.bartpelle.veteres.content.skills.thieving

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Sitautions on 10/28/2015
 */

object Stalls {
	
	class Stalls {
		//Storage for the stall data we currently aren't using
		enum class unused(level: Int, respawn: Int, experience: Int, name: String) {
			VEGETABLE_STALL(2, 2, 10, "Vegetable Stall"),
			CRAFTING_STALL(5, 7, 16, "Crafting Stall"),
			MONKEY_FOOD_STALL(5, 7, 16, "Monkey Food Stall"),
			MONKEY_GENERAL_STALL(5, 7, 16, "Monkey General Stall"),
			WINE_STALL(22, 16, 27, "Wine Stall"),
			SEED_STALL(27, 18, 10, "Seed Stall"),
			FISH_STALL(42, 25, 42, "Fish Stall"),
			CROSSBOW_STALL(49, 25, 52, "Crossbow Stall"),
			MAGIC_STALL(65, 80, 100, "Magic Stall"),
			SCIMITAR_STALL(65, 80, 100, "Scimitar Stall"),
		}
		
		//Data for the stalls we have supported in game
		enum class data(val level: Int, val respawn: Int, val experience: Double, val petOdds: Int, val stallname: String, val obj: Int, val robj: Int, val rewards: ArrayList<Item>) {
			BAKERS_STALL(5, 2, 16.0, 49000, "baker's stall", 11730, 634, arrayListOf(Item(1891, 1), Item(2309, 1), Item(1901, 1), Item(995, 150))),
			SILK_STALL(20, 5, 24.0, 42000, "silk stall", 11429, 634, arrayListOf(Item(950, 1), Item(995, 250))),
			TEA_STALL(5, 7, 24.0, 35000, "tea Stall", 635, 634, arrayListOf(Item(712, 1))),
			FUR_STALL(35, 6, 36.0, 22000, "fur stall", 11732, 634, arrayListOf(Item(958, 1), Item(995, 500))),
			SILVER_STALL(50, 7, 54.0, 19000, "silver stall", 11734, 634, arrayListOf(Item(442, 1), Item(995, 800))),
			SPICE_STALL(65, 8, 81.0, 13000, "spice stall", 11733, 634, arrayListOf(Item(2007, 1), Item(995, 1000))),
			GEM_STALL(75, 10, 160.0, 8500, "gem stall", 11731, 634, arrayListOf(Item(1617, 1), Item(1619, 1), Item(1621, 1), Item(1623, 1), Item(995, 1250)));
		}
		
		companion object {
			@Suspendable fun thieve(it: Script, stall: data, robj: Int) {
				val player = it.player()
				val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
				
				//Does the player have the required level?
				if (player.skills()[Skills.THIEVING] < stall.level) {
					player.sound(2277, 0)
					it.messagebox("You need to be level ${stall.level} to steal from the ${stall.stallname}.")
					return
				}
				
				//Is the players inventory full?
				if (player.inventory().full()) {
					player.sound(2277, 0)
					it.messagebox("Your inventory is too full to hold any more.")
					return
				}
				
				//Send the player a message, and make them do an animation
				it.message("You attempt to steal from the ${stall.stallname}..")
				player.lock()
				player.animate(832)
				it.delay(1)
				
				//Repalce the world object to the temp one
				it.runGlobal(player.world()) @Suspendable { s ->
					val world = player.world()
					val spawned = MapObj(obj.tile(), robj, obj.type(), obj.rot())
					spawned.putattrib(AttributeKey.MAPOBJ_UUID, MapObj.INCREMENTING_MAPOBJ_UUID++)
					world.spawnObj(spawned)
					s.delay(stall.respawn)
					// By comparing this unique uuid we won't despawn multiple times if multiple stalls were spawned by players
					// thieving at the same time
					if (spawned.valid(world, true)) {
						world.removeObjSpawn(spawned)
					}
				}
				
				// Woo! A pet!
				val odds = (stall.petOdds.toDouble() * player.mode().skillPetMod()).toInt()
				if (player.world().rollDie(odds, 1)) {
					unlockRaccoon(player)
				}
				
				//Give the player experience and a reward
				val randomId = player.world().random(stall.rewards.size - 1)
				player.inventory() += Item(stall.rewards[randomId].id(), stall.rewards[randomId].amount())
				it.addXp(Skills.THIEVING, stall.experience)
				player.unlock()
			}
		}
	}
	
	fun unlockRaccoon(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.ROCKY)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(Pet.ROCKY.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.ROCKY, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(Pet.ROCKY.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.ROCKY.item).name(player.world())}.")
		} else {
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
	@JvmStatic @ScriptMain fun register_stalls(r: ScriptRepository) {
		r.onObject(11730) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.THIEVING)) {
				Stalls.thieve(it, Stalls.data.BAKERS_STALL, Stalls.data.BAKERS_STALL.robj)
			}
		}
		r.onObject(11729) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.THIEVING)) {
				Stalls.thieve(it, Stalls.data.SILK_STALL, Stalls.data.SILK_STALL.robj)
			}
		}
		r.onObject(635) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.THIEVING)) {
				Stalls.thieve(it, Stalls.data.TEA_STALL, Stalls.data.TEA_STALL.robj)
			}
		}
		r.onObject(11732) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.THIEVING)) {
				Stalls.thieve(it, Stalls.data.FUR_STALL, Stalls.data.FUR_STALL.robj)
			}
		}
		r.onObject(11734) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.THIEVING)) {
				Stalls.thieve(it, Stalls.data.SILVER_STALL, Stalls.data.SILVER_STALL.robj)
			}
		}
		r.onObject(11733) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.THIEVING)) {
				Stalls.thieve(it, Stalls.data.SPICE_STALL, Stalls.data.SPICE_STALL.robj)
			}
		}
		r.onObject(11731) @Suspendable {
			if (!Skills.disabled(it.player(), Skills.THIEVING)) {
				Stalls.thieve(it, Stalls.data.GEM_STALL, Stalls.data.GEM_STALL.robj)
			}
		}
	}
	
}
