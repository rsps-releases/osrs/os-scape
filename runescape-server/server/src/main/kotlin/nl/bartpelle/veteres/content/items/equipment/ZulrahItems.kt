package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/29/2016.
 */

object ZulrahItems {
	
	/**
	 * Uses a onWorldInit hook .. enabled for all but world 2.
	 */
	@JvmStatic var CHARGING_ENABLED = false
	
	/**
	 * W2 doesn't need them full stop, but for the eco works there is an issue of supply. Everybody
	 * is used to dragon-dart powered blowpipe. Without a source of income for the highest powered darts,
	 * people then require rune+adamant smithing levels to get the highest tier. We can address this after 2017.6
	 */
	@JvmStatic var BLOWPIPE_REQUIRES_DARTS = false
	
	val TANZANITE_FANG = 12922
	val EMPTY_TOXIC_BLOWPIPE = 12924
	val USED_TOXIC_BLOWPIPE = 12926
	val ZULRAHS_SCALES = 12934
	
	val JAR_OF_SWAMP = 12936
	
	val SERPENTINE_VISAGE = 12927
	val SERPENTINE_HELM_UNCHARGED = 12929
	val SERPENTINE_HELM = 12931
	
	val TANZANITE_MUTAGEN = 13200
	val MAGMA_MUTAGEN = 13201
	
	val TANZANITE_HELM_UNCHARGED = 13196
	val TANZANITE_HELM = 13197
	
	val MAGMA_HELM_UNCHARGED = 13198
	val MAGMA_HELM = 13199
	
	val MAGIC_FANG = 12932
	val NORM_SOTD = 11791
	val TOXIC_SOTD_USED = 12904
	val TOXIC_SOTD_EMPTY = 12902
	
	val TRIDENT_FULL = 11905
	val TRIDENT_USED = 11907
	val TRIDENT_EMPTY = 11908
	
	val TOXIC_TRIDENT_USED = 12899
	val TOXIC_TRIDENT_EMPTY = 12900
	
	val NORM_TRIDENTS = arrayListOf(TRIDENT_FULL, TRIDENT_USED, TRIDENT_EMPTY)
	
	val CHISEL = 1755
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Opening the jar of swamp..
		r.onItemOption1(JAR_OF_SWAMP) @Suspendable { it.itemBox("I'm not opening that, it smells funny!", JAR_OF_SWAMP) }
		
		for (fang in intArrayOf(MAGIC_FANG, TANZANITE_FANG, SERPENTINE_VISAGE)) {
			r.onItemOption4(fang) @Suspendable {
				val item = it.itemUsed()
				val name = item.name(it.player().world()).toLowerCase()
				it.doubleItemBox("Dismantling your $name will destory it and give you 20,000 zulrah scales in return. Are you sure?", Item(fang), Item(ZULRAHS_SCALES, 20000))
				if (it.optionsTitled("Dismantle the $name?", "Yes", "No") == 1) {
					if (it.player().inventory().remove(Item(fang), false).success()) {
						it.player().inventory() += Item(ZULRAHS_SCALES, 20000)
					}
				}
			}
		}
		
	}
}
