package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-11.
 */

object ArmourSalesman {
	
	val ARMOUR_SALESMAN = 6059
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(ARMOUR_SALESMAN) @Suspendable {
			it.chatPlayer("Good day to you.", 567)
			it.chatNpc("And to you. Can I help you?", ARMOUR_SALESMAN, 567)
			when (it.options("I'd like to see what you sell.", "I've seen enough, thanks.")) {
				1 -> {
					it.chatPlayer("I'd like to see what you sell.", 567)
					it.chatNpc("Indeed, cast your eyes on my wares, adventurer.", ARMOUR_SALESMAN, 567)
					it.player().world().shop(48).display(it.player())
				}
				2 -> {
					it.chatPlayer("I've seen enough, thanks.", 588)
					it.chatNpc("Very good, adventurer.", ARMOUR_SALESMAN, 588)
				}
			}
		}
		r.onNpcOption2(ARMOUR_SALESMAN) @Suspendable {
			it.player().world().shop(48).display(it.player())
		}
	}
}
