package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.veteres.content.TileOffset
import nl.bartpelle.veteres.content.to

/**
 * Created by Jason MacKeigan on 2016-09-03 at 12:56 AM
 */
enum class BottomBarricadePositionOffset(vararg val offsets: TileOffset) {
	WEST(-20 to -4),
	SOUTH_WEST(-9 to -14),
	SOUTH_SOUTH_WEST(-19 to -21),
	SOUTH(3 to -17),
	SOUTH_SOUTH_EAST(10 to -14),
	SOUTH_EAST(20 to -18),
	EAST(20 to -8),
	NORTH_EAST(17 to -2)
	;
	
	companion object {
		
		val ALL = BottomBarricadePositionOffset.values().flatMap { it.offsets.asIterable() }
	}
}