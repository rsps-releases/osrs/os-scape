package nl.bartpelle.veteres.content.items.skillcape

import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import java.util.concurrent.TimeUnit

/**
 * Created by Jason MacKeigan on 2016-07-05 at 11:31 AM
 */
object FletchingCape : CapeOfCompletionPerk(intArrayOf(1, 2)) {
	
	override fun option(option: Int): Function1<Script, Unit> = {
		val player = it.player()
		
		when (option) {
			1 -> {
				CapeOfCompletion.boost(Skills.FLETCHING, player)
			}
			2 -> {
				if (player.inventory().freeSlots() < 2) {
					player.message("You need two free slots to do this.")
				} else {
					resetIfPossible(player)
					
					val amount = player.attribOr<Int>(AttributeKey.FLETCHING_PERK_SEARCH_AMOUNT, 0)
					
					val currentTime = System.currentTimeMillis()
					
					val timeOfSearch = player.attribOr<Long>(AttributeKey.FLETCHING_PERK_SEARCH_TIME, 0L)
					
					if (amount < 3) {
						player.putattrib(AttributeKey.FLETCHING_PERK_SEARCH_AMOUNT, amount + 1)
						player.inventory().add(Item(9418), false)
						player.inventory().add(Item(9174), false)
						player.message("You cycle and find a mithril grappling hook and a bronze crossbow.")
						
						if (amount + 1 == 3) {
							player.putattrib(AttributeKey.FLETCHING_PERK_SEARCH_TIME, currentTime)
						}
					} else {
						player.message("You need to wait roughly ${24 - TimeUnit.MILLISECONDS.toHours(currentTime - timeOfSearch)} hours to cycle again.")
					}
				}
			}
		}
	}
	
	/**
	 * A function that when referenced will determine if the player requires
	 * the attribute to be set to some default value if a certain amount of
	 * time has passed.
	 */
	private fun resetIfPossible(player: Player) {
		val amount = player.attribOr<Int>(AttributeKey.FLETCHING_PERK_SEARCH_AMOUNT, 0)
		
		val currentTime = System.currentTimeMillis()
		
		val timeOfSearch = player.attribOr<Long>(AttributeKey.FLETCHING_PERK_SEARCH_TIME, 0L)
		
		if (amount == 3 && currentTime - timeOfSearch > TimeUnit.HOURS.toMillis(24)) {
			player.putattrib(AttributeKey.FLETCHING_PERK_SEARCH_AMOUNT, 0)
		}
	}
}