package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 7/27/2017.
 */
object JalAkRekMej {
	
	@JvmField val script: Function1<Script, Unit> =  s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 4) && EntityCombat.attackTimerReady(npc)) {
				
				attack(npc, target)
				
				//Send to cooldown.
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun attack(npc: Npc, target: Entity) {
		npc.animate(npc.attackAnimation())
		
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc.tile() + Tile(1, 1), target, 1381, 10, 34, 45, 12 * tileDist, 0, 1)
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
			target.hit(npc, npc.world().random(18), 1).delay(delay).combatStyle(CombatStyle.MAGIC)
		} else {
			target.hit(npc, 0, 1)
		}
	}
}