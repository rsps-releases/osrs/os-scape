package nl.bartpelle.veteres.content.areas.lumbridge

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/5/2016.
 */

object Signpost {
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(18493) @Suspendable {
			val post: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			//Lumbridge Castle
			if (post.tile() == Tile(3235, 3228)) {
				it.player().invokeScript(143, 280, 0)
				it.player().invokeScript(InvokeScript.SETVARCS, 10786175, 200)
				it.player().interfaces().sendMain(135)
				it.player().write(
						InterfaceText(135, 12, "West to the Lumbridge Castle and Draynor Village. Beware the goblins!"),
						InterfaceText(135, 9, "South to the swamps of Lumbridge."),
						InterfaceText(135, 3, "Head north towards Fred's farm and the windmill."),
						InterfaceText(135, 8, "Cross the bridge and head east to Al Kharid or north to Varrock."))
			}
			
			//North of Lumbridge
			if (post.tile() == Tile(3261, 3230)) {
				it.player().invokeScript(143, 280, 0)
				it.player().invokeScript(InvokeScript.SETVARCS, 10786175, 200)
				it.player().interfaces().sendMain(135)
				it.player().write(
						InterfaceText(135, 12, "West to Lumbridge."),
						InterfaceText(135, 9, "The River Lum lies to the south."),
						InterfaceText(135, 3, "North to farms and Varrock."),
						InterfaceText(135, 8, "East to Al Kharid - toll gate; bring some money."))
			}
			
			//Draynor village
			if (post.tile() == Tile(3107, 3296)) {
				it.player().invokeScript(143, 280, 0)
				it.player().invokeScript(InvokeScript.SETVARCS, 10786175, 200)
				it.player().interfaces().sendMain(135)
				it.player().write(
						InterfaceText(135, 12, "West to Port Sarim, Falador and Rimmington."),
						InterfaceText(135, 9, "South to Draynor Village and the Wizards' Tower."),
						InterfaceText(135, 3, "North to Draynor Manor."),
						InterfaceText(135, 8, "East to Lumbridge."))
			}
			
			//Varrock south fork
			if (post.tile() == Tile(3268, 3332)) {
				it.player().invokeScript(143, 280, 0)
				it.player().invokeScript(InvokeScript.SETVARCS, 10786175, 200)
				it.player().interfaces().sendMain(135)
				it.player().write(
						InterfaceText(135, 12, "West to Champions' Guild and Varrock south gate."),
						InterfaceText(135, 9, "South through farms to Al Kharid and Lumbridge."),
						InterfaceText(135, 3, "Sheep lay this way."),
						InterfaceText(135, 8, "East to Al Kharid mine and follow the path north to Varrock east gate."))
			}
		}
	}
}
