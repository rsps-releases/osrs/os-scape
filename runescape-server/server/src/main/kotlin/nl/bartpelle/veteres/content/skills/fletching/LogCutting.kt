package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/15/2015.
 */

object LogCutting {
	
	enum class Logs(val id: Int, val items: Array<Item>) {
		NORMAL_LOG(1511, arrayOf(Item(52, 15), Item(50, 1), Item(48, 1), Item(9440, 1))),
		OAK_LOG(1521, arrayOf(Item(54, 1), Item(56, 1), Item(9442, 1))),
		WILLOW_LOG(1519, arrayOf(Item(60, 1), Item(58, 1), Item(9444, 1))),
		TEAK_LOG(1517, arrayOf(Item(9446, 1))),
		MAPLE_LOG(1517, arrayOf(Item(64, 1), Item(62, 1), Item(9448, 1))),
		YEW_LOG(1515, arrayOf(Item(68, 1), Item(66, 1), Item(9452, 1))),
		MAGIC_LOG(1513, arrayOf(Item(72, 1), Item(70, 1)))
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		Logs.values().forEach { log ->
			val KNIFE = 946
			repo.onItemOnItem(KNIFE.toLong(), log.id.toLong(), s@ @Suspendable {
				
				//Log constants
				val NORMAL = 1511
				val OAK = 1521
				val WILLOW = 1519
				val MAPLE = 1517
				val YEW = 1515
				val MAGIC = 1513
				
				//Regular log
				if (log.id == NORMAL) {
					val option1: Int = log.items[0].id()
					val option2: Int = log.items[1].id()
					val option3: Int = log.items[2].id()
					val option4: Int = log.items[3].id()
					
					val strings = arrayOf("15 Arrows", "15 Javelins", "Short Bow", "Long Bow", "Crossbow Stock")
					val item = it.itemOptions(Item(option1, 175), Item(19584, 175), Item(option2, 175), Item(option3, 175), Item(option4, 175), strings = strings)
					var amt: Int = item.amount()
					
					while (amt-- > 0) {
						if (NORMAL !in it.player().inventory() || KNIFE !in it.player().inventory()) {
							break
						}
						if (item.id() == option1) {
							it.player().inventory() -= NORMAL
							it.player().inventory() += Item(item.id(), 15)
							it.addXp(Skills.FLETCHING, 5.0)
							it.message("You carefully cut the wood into 15 arrow shafts.")
						} else if (item.id() == option2) {
							if (it.player().skills()[Skills.FLETCHING] < 5) {
								it.messagebox("You need a Fletching level of 5 or above to make a Shortbow.")
								return@s
							} else {
								it.player().inventory() -= NORMAL
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 5.0)
								it.message("You carefully cut the wood into a shortbow.")
							}
						} else if (item.id() == option3) {
							if (it.player().skills()[Skills.FLETCHING] < 10) {
								it.messagebox("You need a Fletching level of 10 or above to make a Longbow.")
								return@s
							} else {
								it.player().inventory() -= NORMAL
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 10.0)
								it.message("You carefully cut the wood into a longbow.")
							}
						} else if (item.id() == option4) {
							if (it.player().skills()[Skills.FLETCHING] < 9) {
								it.messagebox("You need a Fletching level of 9 or above to make a Wooden stock.")
								return@s
							} else {
								it.player().inventory() -= NORMAL
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 6.0)
								it.message("You carefully cut the wood into a crossbow stock.")
							}
						} else if (item.id() == 19584) {
							if (it.player().skills()[Skills.FLETCHING] < 3) {
								it.messagebox("You need a Fletching level of 3 or above to fletch Javelin shafts.")
								return@s
							} else {
								it.player().inventory() -= NORMAL
								it.player().inventory() += Item(19584, 15)
								it.addXp(Skills.FLETCHING, 5.0)
							}
						}
						
						it.animate(1248)
						it.delay(2)
					}
				}
				
				//Oak log
				if (log.id == OAK) {
					val option1: Int = log.items[0].id()
					val option2: Int = log.items[1].id()
					val option3: Int = log.items[2].id()
					val item = it.itemOptions(Item(option1, 175), Item(option2, 175), Item(option3, 175))
					var amt: Int = item.amount()
					
					while (amt-- > 0) {
						if (OAK !in it.player().inventory() || KNIFE !in it.player().inventory()) {
							break
						}
						if (item.id() == option1) {
							if (it.player().skills()[Skills.FLETCHING] < 20) {
								it.messagebox("You need a Fletching level of 20 or above to make an Oak Shortbow.")
								return@s
							} else {
								it.player().inventory() -= OAK
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 16.5)
								it.message("You carefully cut the wood into a shortbow.")
							}
						} else if (item.id() == option2) {
							if (it.player().skills()[Skills.FLETCHING] < 25) {
								it.messagebox("You need a Fletching level of 25 or above to make an Oak Longbow.")
								return@s
							} else {
								it.player().inventory() -= OAK
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 16.5)
								it.message("You carefully cut the wood into a longbow.")
							}
						} else if (item.id() == option3) {
							if (it.player().skills()[Skills.FLETCHING] < 24) {
								it.messagebox("You need a Fletching level of 24 or above to make an Oak Stock.")
								return@s
							} else {
								it.player().inventory() -= OAK
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 16.0)
								it.message("You carefully cut the wood into a crossbow stock.")
							}
						}
						it.animate(1248)
						it.delay(2)
					}
				}
				//Willow log
				if (log.id == WILLOW) {
					val option1: Int = log.items[0].id()
					val option2: Int = log.items[1].id()
					val option3: Int = log.items[2].id()
					val item = it.itemOptions(Item(option1, 175), Item(option2, 175), Item(option3, 175))
					var amt: Int = item.amount()
					
					while (amt-- > 0) {
						if (WILLOW !in it.player().inventory() || KNIFE !in it.player().inventory()) {
							break
						}
						if (item.id() == option1) {
							if (it.player().skills()[Skills.FLETCHING] < 35) {
								it.messagebox("You need a Fletching level of 35 or above to make a Willow Shortbow.")
								return@s
							} else {
								it.player().inventory() -= WILLOW
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 33.3)
								it.message("You carefully cut the wood into a shortbow.")
							}
						} else if (item.id() == option2) {
							if (it.player().skills()[Skills.FLETCHING] < 40) {
								it.messagebox("You need a Fletching level of 40 or above to make a Willow Longbow.")
								return@s
							} else {
								it.player().inventory() -= WILLOW
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 41.5)
								it.message("You carefully cut the wood into a longbow.")
							}
						} else if (item.id() == option3) {
							if (it.player().skills()[Skills.FLETCHING] < 39) {
								it.messagebox("You need a Fletching level of 39 or above to make a Willow Stock.")
								return@s
							} else {
								it.player().inventory() -= WILLOW
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 22.0)
								it.message("You carefully cut the wood into a crossbow stock.")
							}
						}
						it.animate(1248)
						it.delay(2)
					}
				}
				//Maple log
				if (log.id == MAPLE) {
					val option1: Int = log.items[0].id()
					val option2: Int = log.items[1].id()
					val option3: Int = log.items[2].id()
					val item = it.itemOptions(Item(option1, 175), Item(option2, 175), Item(option3, 175))
					var amt: Int = item.amount()
					
					while (amt-- > 0) {
						if (MAPLE !in it.player().inventory() || KNIFE !in it.player().inventory()) {
							break
						}
						if (item.id() == option1) {
							if (it.player().skills()[Skills.FLETCHING] < 50) {
								it.messagebox("You need a Fletching level of 50 or above to make a Maple Shortbow.")
								return@s
							} else {
								it.player().inventory() -= MAPLE
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 50.0)
								it.message("You carefully cut the wood into a shortbow.")
							}
						} else if (item.id() == option2) {
							if (it.player().skills()[Skills.FLETCHING] < 55) {
								it.messagebox("You need a Fletching level of 55 or above to make a Maple Longbow.")
								return@s
							} else {
								it.player().inventory() -= MAPLE
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 55.0)
								it.message("You carefully cut the wood into a longbow.")
							}
						} else if (item.id() == option3) {
							if (it.player().skills()[Skills.FLETCHING] < 54) {
								it.messagebox("You need a Fletching level of 54 or above to make a Maple Stock.")
								return@s
							} else {
								it.player().inventory() -= MAPLE
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 32.0)
								it.message("You carefully cut the wood into a crossbow stock.")
							}
						}
						it.animate(1248)
						it.delay(2)
					}
				}
				//Yew log
				if (log.id == YEW) {
					val option1: Int = log.items[0].id()
					val option2: Int = log.items[1].id()
					val option3: Int = log.items[2].id()
					val item = it.itemOptions(Item(option1, 175), Item(option2, 175), Item(option3, 175))
					var amt: Int = item.amount()
					
					while (amt-- > 0) {
						if (YEW !in it.player().inventory() || KNIFE !in it.player().inventory()) {
							break
						}
						if (item.id() == option1) {
							if (it.player().skills()[Skills.FLETCHING] < 65) {
								it.messagebox("You need a Fletching level of 65 or above to make a Yew Shortbow.")
								return@s
							} else {
								it.player().inventory() -= YEW
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 67.5)
								it.message("You carefully cut the wood into a shortbow.")
							}
						} else if (item.id() == option2) {
							if (it.player().skills()[Skills.FLETCHING] < 70) {
								it.messagebox("You need a Fletching level of 70 or above to make a Yew Longbow.")
								return@s
							} else {
								it.player().inventory() -= YEW
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 75.0)
								it.message("You carefully cut the wood into a longbow.")
							}
						} else if (item.id() == option3) {
							if (it.player().skills()[Skills.FLETCHING] < 69) {
								it.messagebox("You need a Fletching level of 69 or above to make a Yew Stock.")
								return@s
							} else {
								it.player().inventory() -= YEW
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 32.0)
								it.message("You carefully cut the wood into a crossbow stock.")
							}
						}
						it.animate(1248)
						it.delay(2)
					}
				}
				//Magic log
				if (log.id == MAGIC) {
					val option1: Int = log.items[0].id()
					val option2: Int = log.items[1].id()
					val item = it.itemOptions(Item(option1, 175), Item(option2, 175))
					var amt: Int = item.amount()
					
					while (amt-- > 0) {
						if (MAGIC !in it.player().inventory() || KNIFE !in it.player().inventory()) {
							break
						}
						if (item.id() == option1) {
							if (it.player().skills()[Skills.FLETCHING] < 80) {
								it.messagebox("You need a Fletching level of 80 or above to make a Magic Shortbow.")
								return@s
							} else {
								it.player().inventory() -= MAGIC
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 83.3)
								it.message("You carefully cut the wood into a shortbow.")
							}
						} else if (item.id() == option2) {
							if (it.player().skills()[Skills.FLETCHING] < 85) {
								it.messagebox("You need a Fletching level of 85 or above to make a Magic Longbow.")
								return@s
							} else {
								it.player().inventory() -= MAGIC
								it.player().inventory() += item.id()
								it.addXp(Skills.FLETCHING, 91.5)
								it.message("You carefully cut the wood into a longbow.")
							}
						}
						it.animate(1248)
						it.delay(2)
					}
				}
			})
		}
	}
}