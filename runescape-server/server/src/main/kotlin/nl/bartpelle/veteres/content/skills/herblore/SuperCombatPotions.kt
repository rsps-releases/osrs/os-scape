package nl.bartpelle.veteres.content.skills.herblore

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.itemOptions
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/30/2016.
 */

object SuperCombatPotions {
	
	val SUPER_COMBAT_POTION = 12695
	val SUPER_STRENGTH_POTION = 2440
	val SUPER_ATTACK_POTION = 2436
	val SUPER_DEFENCE_POTION = 2442
	val TORSTOL = 269
	val VIAL = 229
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnItem(SUPER_STRENGTH_POTION, SUPER_ATTACK_POTION) @Suspendable { makePotion(it) }
		r.onItemOnItem(SUPER_STRENGTH_POTION, SUPER_DEFENCE_POTION) @Suspendable { makePotion(it) }
		r.onItemOnItem(SUPER_STRENGTH_POTION, TORSTOL) @Suspendable { makePotion(it) }
		r.onItemOnItem(SUPER_DEFENCE_POTION, SUPER_ATTACK_POTION) @Suspendable { makePotion(it) }
		r.onItemOnItem(SUPER_DEFENCE_POTION, TORSTOL) @Suspendable { makePotion(it) }
		r.onItemOnItem(SUPER_ATTACK_POTION, TORSTOL) @Suspendable { makePotion(it) }
	}
	
	@Suspendable fun makePotion(it: Script) {
		if (it.player().skills().xpLevel(Skills.HERBLORE) < 90) {
			it.messagebox("You need a Herblore level of at least 90 to make this potion.")
			return
		}
		
		if (it.player().world().realm().isPVP) {
			it.messagebox("Why would you want to train Herblore on a PVP server?")
			return
		}
		
		if (!it.player().inventory().hasAll(SUPER_STRENGTH_POTION, SUPER_ATTACK_POTION, SUPER_DEFENCE_POTION, TORSTOL)) {
			it.message("You need a super strength potion, super attack potion, super defence potion, and torsol to create a Super combat potion.")
			return
		}
		
		var amount = 1
		if (it.player().inventory().count(TORSTOL) > 1) {
			amount = it.itemOptions(Item(SUPER_COMBAT_POTION, 150), offsetX = 25)
		}
		
		while (amount-- > 0) {
			if (!it.player().inventory().hasAll(SUPER_STRENGTH_POTION, SUPER_ATTACK_POTION, SUPER_DEFENCE_POTION, TORSTOL)) {
				return
			}
			it.player().inventory().remove(Item(SUPER_STRENGTH_POTION), true)
			it.player().inventory().remove(Item(SUPER_ATTACK_POTION), true)
			it.player().inventory().remove(Item(SUPER_DEFENCE_POTION), true)
			it.player().inventory().remove(Item(TORSTOL), true)
			
			it.player().inventory().add(Item(VIAL), true)
			it.player().inventory().add(Item(VIAL), true)
			it.player().inventory().add(Item(SUPER_COMBAT_POTION), true)
			
			it.player().sound(2608, 0)
			it.player().animate(363)
			
			it.addXp(Skills.HERBLORE, 150.0)
			it.delay(2)
		}
	}
}
