package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/15/2015.
 */

object FletchBolts {
	
	enum class FletchableBolt(val item1: Int, val item2: Int, val outcome: Int, val xp: Double, val lvlreq: Int, val itemName: String, val shortName: String) {
		BRONZE_ARROW(314, 9375, 877, 5.0, 9, "bronze bolts", "bronze"),
		IRON_ARROW(314, 9377, 9140, 15.0, 39, "iron bolts", "iron"),
		STEEL_ARROW(314, 9378, 9141, 35.0, 46, "steel bolts", "steel"),
		MITHRIL_ARROW(314, 9379, 9142, 50.0, 54, "mithril bolts", "mithril"),
		BROAD_ARROW(314, 11876, 11875, 30.0, 55, "broad bolts", "broad"),
		ADAMANT_ARROW(314, 9380, 9143, 70.0, 61, "adamant bolts", "adamant"),
		RUNE_ARROW(314, 9381, 9144, 100.0, 69, "rune bolts", "rune")
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		FletchableBolt.values().forEach { bolt ->
			repo.onItemOnItem(bolt.item1.toLong(), bolt.item2.toLong(), s@ @Suspendable {
				//Check if the player has a high enough level to do this.
				if (it.player().skills()[Skills.FLETCHING] < bolt.lvlreq) {
					it.messagebox("You need a Fletching level of ${bolt.lvlreq} or above to make ${bolt.itemName}.")
					return@s
				}
				
				//Prompt the player with the # they'd like to make
				var num = 1
				if (it.player().inventory().count(bolt.item1) > 10) {
					num = it.itemOptions(Item(bolt.outcome, 150), offsetX = 12)
				}
				
				// You can't 100% afk this. Wouldn't be nice :)
				if (num > 15)
					num = 15
				
				while (num-- > 0) {
					//Check if the player's ran out of supplies
					if (bolt.item1 !in it.player().inventory() || bolt.item2 !in it.player().inventory()) {
						break
					}
					
					var max = Math.min(it.player().inventory().count(bolt.item1), it.player().inventory().count(bolt.item2))
					if (max > 10)
						max = 10
					
					it.player().inventory() -= Item(bolt.item1, max)
					it.player().inventory() -= Item(bolt.item2, max)
					it.player().inventory() += Item(bolt.outcome, max)
					it.player().message("You attach feathers to $max bolts.")
					it.addXp(Skills.FLETCHING, bolt.xp * (max / 10.0).toDouble())
					it.delay(2)
				}
			})
		}
	}
}