package nl.bartpelle.veteres.content.combat

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Carl on 2015-08-13.
 */

object FoodItems {
	
	enum class Food(val itemId: Int, val heal: Int, val replacement: Int = -1, val effect: Boolean = false) {
		SHRIMP(315, 3),
		COOKED_CHICKEN(2140, 4),
		SARDINE(325, 4),
		COOKED_MEAT(2142, 4),
		BREAD(2309, 5),
		HERRING(347, 5),
		MACKEREL(355, 6),
		TROUT(333, 7),
		PIKE(351, 8),
		COD(339, 7),
		CURRY(2011, 19),
		SALMON(329, 9),
		TUNA(361, 10),
		PEACHES(6883, 8),
		BANANAS(1963, 2),
		CAKE(1891, 4, 1893),
		TWO_THIRDS_CAKE(1893, 4, 1895),
		ONE_THIRD_CAKE(1895, 4),
		CHOCOLATE_CAKE(1897, 5, 1899),
		TWO_THIRDS_CHOCOLATE_CAKE(1899, 5, 1901),
		ONE_THIRD_CHOCOLATE_CAKE(1901, 5),
		LOBSTER(379, 12),
		BASS(365, 13),
		SWORDFISH(373, 14),
		MONKFISH(7946, 16),
		EDIBLE_SEAWEED(403, 4),
		STRANGE_FRUIT(464, 0, -1, true),
		PURPLE_SWEETS(10476, 3, -1, true),
		PLAIN_PIZZA(2289, 7, 2291),
		HALF_PLAIN_PIZZA(2291, 7),
		MEAT_PIZZA(2293, 8, 2295),
		HALF_MEAT_PIZZA(2295, 8),
		ANCHOVY_PIZZA(2297, 9, 2299),
		HALF_ANCHOVY_PIZZA(2299, 9),
		PINEAPPLE_PIZZA(2301, 11, 2303),
		HALF_PINEAPPLE_PIZZA(2303, 11),
		SHARK(385, 20),
		SEA_TURTLE(397, 21),
		MANTA_RAY(391, 22),
		TUNA_POTATO(7060, 22),
		CHEESE_POTATO(6705, 16),
		DARK_CRAB(11936, 22),
		REDBERRY_PIE(2325, 5, 2333),
		HALF_REDBERRY_PIE(2333, 5, 2313),
		MEAT_PIE(2327, 6, 2331),
		HALF_MEAT_PIE(2331, 6, 2313),
		APPLE_PIE(2323, 7, 2335),
		HALF_APPLE_PIE(2335, 7, 2313),
		GARDEN_PIE(7178, 6, 7180),
		HALF_GARDEN_PIE(7180, 6, 2313),
		FISH_PIE(7188, 6, 7190),
		HALF_FISH_PIE(7190, 6, 2313),
		ADMIRAL_PIE(7198, 8, 7200),
		HALF_ADMIRAL_PIE(7200, 8, 2313),
		WILD_PIE(7208, 11, 7210),
		HALF_WILD_PIE(7210, 11, 2313),
		SUMMER_PIE(7218, 11, 7220),
		HALF_SUMMER_PIE(7220, 11, 2313),
		KARAMBWAN(3144, 18, -1, true),
		ANGLERFISH(13441, 22),
		FRIED_MUSHROOMS(7082, 5, 1923),
		CHILLI_POTATO(7054, 14),
		MUSHROOM_POTATO(7058, 20),
		CUP_OF_TEA(712, 2, 1980, true),
		JUG_OF_WINE(1993, 11, 1935, true),
		BEER(1917, 1, 1919, true),
		POTATO(1942, 1, -1, true),
		ONION(1957, 1, -1, true),
		CABBAGE(1965, 1, -1, true),
		UGTHANKI_KEBAB(1883, 19, -1, true),
		UGTHANKI_KEBAB_2(1885, 19, -1, true),
		DWARVEN_STOUT(1913, 1, 1919, true),
		DWARVEN_STOUT4(5777, 1, 5775, true),
		DWARVEN_STOUT3(5775, 1, 5773, true),
		DWARVEN_STOUT2(5773, 1, 5771, true),
		DWARVEN_STOUT1(5771, 1, 1919, true),
		DWARVEN_STOUT_M(5747, 1, 1919, true),
		DWARVEN_STOUT_M4(5857, 1, 5855, true),
		DWARVEN_STOUT_M3(5855, 1, 5853, true),
		DWARVEN_STOUT_M2(5853, 1, 5851, true),
		DWARVEN_STOUT_M1(5851, 1, 1919, true),
	}
	
	@JvmStatic fun get(raw: Int): Food? {
		Food.values().forEach {
			if (it.itemId == raw)
				return it
		}
		
		return null
	}
	
	@JvmStatic @ScriptMain fun food(repo: ScriptRepository) {
		Food.values().forEach { food -> repo.onItemOption1(food.itemId) @Suspendable { eat(it, food) } }
	}
	
	@Suspendable fun eat(it: Script, food: Food) {
		val player = it.player()
		// Check timers and other things. Also karambwan ya fele me.
		if (food == Food.KARAMBWAN) {
			if (player.timers().has(TimerKey.KARAMBWAN) || player.dead() || player.hp() < 1)
				return
		} else {
			if (player.timers().has(TimerKey.FOOD) || player.dead() || player.hp() < 1)
				return
		}
		
		if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_FOOD)) {
			player.message("Food is disabled for this duel.")
			return
		}
		
		//Need to keep the "inInstance" calling otherwise we'll be applying restrictions to players
		//who aren't even in the event which is a big no no - Mack
		if (ClanWars.checkRule(player, CWSettings.FOOD)) {
			player.message("Food is disabled for this clan war.")
			return
		}
		
		if (player.world().realm().isDeadman && player.timers().has(TimerKey.DEADMAN_BANK_COOLDOWN))
			return
		
		val foodItem = Item(food.itemId)
		val healed = player.hp() < player.maxHp()
		val name = foodItem.definition(player.world()).name.toLowerCase()
		
		val slot: Int = player.attribOr(AttributeKey.ITEM_SLOT, 0)
		player.inventory().remove(foodItem, true, slot)
		if (food.replacement > -1)
			player.inventory().add(Item(food.replacement), true, slot)
		
		val fullpizza = food == Food.PLAIN_PIZZA || food == Food.MEAT_PIZZA || food == Food.ANCHOVY_PIZZA || food == Food.PINEAPPLE_PIZZA
		val halfpizza = food == Food.HALF_PLAIN_PIZZA || food == Food.HALF_MEAT_PIZZA || food == Food.HALF_ANCHOVY_PIZZA || food == Food.HALF_PINEAPPLE_PIZZA
		
		player.timers().extendOrRegister(TimerKey.FOOD, if (fullpizza) 1 else if (halfpizza) 2 else 3)
		player.timers().extendOrRegister(TimerKey.COMBAT_ATTACK, 5)
		
		
		// My old code which people hate but was soooo accurate :(
		//if(player.timers().has(TimerKey.COMBAT_ATTACK)) {
		//    player.timers().addOrSet(TimerKey.COMBAT_ATTACK, 2)
		//} else {
		//    player.timers().addOrSet(TimerKey.COMBAT_ATTACK, 3)
		//}
		
		if (food == Food.ANGLERFISH) {
			player.heal(food.heal, 22)
		} else if (food == Food.PURPLE_SWEETS) {
			player.heal(player.world().random(food.heal), 0)
		} else {
			player.heal(food.heal, 0)
		}
		
		val eatAnim = if (4084 == it.player().equipment()[3]?.id() ?: -1) 1469 else 829
		player.animate(eatAnim)
		player.sound(2393)
		
		
		if (!food.effect) {
			if (fullpizza) {
				it.message("You eat half of the $name.")
			} else if (halfpizza) {
				it.message("You eat the remaining $name.")
			} else {
				it.message("You eat the $name.")
			}
			
			if (healed)
				it.message("It heals some health.")
		} else {
			if (food == Food.BEER) {
				it.message("You drink the beer. You feel slightly reinvigorated...")
				it.message("...and slightly dizzy too.")
			} else if (food == Food.CUP_OF_TEA) {
				it.message("You drunk the cup of tea.")
				player.sync().shout("Aaah, nothing like a nice cuppa tea!")
			} else if (food == Food.JUG_OF_WINE) {
				it.message("You drink the wine.")
				it.message("It makes you feel a bit dizzy.")
			} else if (food == Food.UGTHANKI_KEBAB || food == Food.UGTHANKI_KEBAB_2) {
				val random = player.world().random(4)
				when (random) {
					1 -> player.sync().shout("Lovely!")
					2 -> player.sync().shout("Scrummy!")
					3 -> player.sync().shout("Delicious!")
					else -> player.sync().shout("Yum!")
				}
				it.message("You eat the kebab.")
			} else if (food == Food.KARAMBWAN) {
				player.timers().register(TimerKey.KARAMBWAN, 3) // Register karambwan timer too
				player.timers().register(TimerKey.POTION, 3) // Register the potion timer (karambwan blocks pots)
				it.message("You eat the Karambwan.")
				
				if (healed)
					it.message("It heals some health.")
			} else if (food == Food.CABBAGE) {
				it.message("You eat the cabbage. Yuck!")
			} else if (food == Food.POTATO) {
				it.message("You eat the potato. Yuck!")
				if (healed)
					it.message("It heals some health anyway.")
			} else if (food == Food.ONION) {
				it.message("You eat the onion.")
				it.message("It's always sad to see a grown man cry.")
			} else if (food == Food.PURPLE_SWEETS) {
				val RUN_ENERGY = player.attribOr<Double>(AttributeKey.RUN_ENERGY, 0)
				player.setRunningEnergy(RUN_ENERGY + 10, true)
				it.message("You eat the sweets.")
				it.message("The sugary goodness heals some energy.")
			} else if (food.name.contains("Dwarven")) {
				// TODO correct message
				player.skills().alterSkill(Skills.MINING, 1)
				player.skills().alterSkill(Skills.SMITHING, 1)
				player.skills().alterSkill(Skills.ATTACK, -3)
				player.skills().alterSkill(Skills.STRENGTH, -3)
				player.heal(1)
			} else if (food == Food.STRANGE_FRUIT) {
				val RUN_ENERGY = player.attribOr<Double>(AttributeKey.RUN_ENERGY, 0)
				player.setRunningEnergy(RUN_ENERGY + 30, true)
				it.message("You eat the strange fruit, tasty!")
				it.message("It's restores some run energy.")
			}
		}
	}
	
}
