package nl.bartpelle.veteres.content.items.tools

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/27/2015.
 */
object ItemPacks {
	
	enum class Pack(val id: Int, val rune: Int) {
		// Runes
		FIRE_RUNES(12734, 554),
		WATER_RUNES(12730, 555),
		AIR_RUNES(12728, 556),
		EARTH_RUNES(12732, 557),
		MIND_RUNES(12736, 558),
		CHAOS_RUNES(12738, 562),
		
		// Farming
		SACKS(13252, 5419),
		BASKETS(13254, 5377),
		POT(13250, 5357), // <-- Insert weed meme here
		
		// Slayer supplies
		BROAD_ARROWHEADS(11885, 11874),
		UNFINISHED_BROAD_BOLTS(11887, 11876),
		
		// Fishing
		BAIT(11883, 313),
		FEATHERS(11881, 314),
		
		// Herblore
		VIAL(11877, 230),
		VIAL_OF_WATER(11879, 228),
		EMPTY_JUGS(20742, 1936),
		AMYLASE(12641, 12640)
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		Pack.values().forEach { pack ->
			r.onItemOption1(pack.id) {
				val slot: Int = it.player()[AttributeKey.ITEM_SLOT]
				it.player().inventory().remove(Item(pack.id), true, slot)
				it.player().inventory() += Item(pack.rune, 100)
			}
		}
	}
	
}