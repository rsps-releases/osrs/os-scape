package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.FaceDirection
import nl.bartpelle.veteres.model.ForceMovement
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Tuple

/**
 * Created by Situations on 11/17/2015.
 */

object MagicalAnimator {
	
	enum class ArmourSets(val npc: Int, val helm: Item, val legs: Item, val body: Item) {
		BRONZE(2450, Item(1075), Item(1117), Item(1155)),
		IRON(2451, Item(1067), Item(1115), Item(1153)),
		STEEL(2452, Item(1069), Item(1119), Item(1157)),
		BLACK(2453, Item(1077), Item(1125), Item(1165)),
		MITHRIL(2454, Item(1071), Item(1121), Item(1159)),
		ADAMANT(2455, Item(1073), Item(1123), Item(1161)),
		RUNE(2456, Item(1079), Item(1127), Item(1163));
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOnObject(23955) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			
			// Resolve the armour
			val sets = ArmourSets.values().find { sets ->
				item == sets.helm.id() || item == sets.body.id() || item == sets.legs.id()
			}
			
			// Got one? Or na
			if (sets != null) {
				if (sets.helm in it.player().inventory() && sets.body in it.player().inventory() && sets.legs in it.player().inventory()) {
					// if (set == 0) {
					val spawnTile = Tile(obj.tile().x, obj.tile().z, obj.tile().level)
					
					it.delay(1)
					it.player().lock()
					it.blankmessagebox("You place your armour on the platform where it disappears....")
					it.delay(1)
					it.animate(827)
					it.delay(3)
					
					// Remove all the parts
					it.player().inventory() -= sets.helm
					it.player().inventory() -= sets.body
					it.player().inventory() -= sets.legs
					
					it.delay(2)
					it.blankmessagebox2("The animator hums, something appears to be working. You stand back...")
					it.delay(2)
					it.player().forceMove(ForceMovement(0, 0, 0, +2, 45, 126, FaceDirection.SOUTH))
					it.delay(1)
					
					it.player().animate(820, 5)
					it.delay(3)
					
					it.player().teleport(obj.tile().x, obj.tile().z + 3)
					it.delay(1)
					
					val npc = Npc(sets.npc, it.player().world(), spawnTile)
					npc.putattrib(AttributeKey.OWNING_PLAYER, Tuple(it.player().id(), it.player()))
					
					it.player().world().registerNpc(npc)
					npc.faceTile(npc.tile().transform(1, 0, 0))
					npc.spawnDirection(6)
					npc.animate(4166, 1)
					npc.sync().shout("I'm ALIVE!")
					it.player().write(SetHintArrow(npc))
					it.delay(1)
					
					//Close the players dialogue box & proceed with the rest
					it.player().interfaces().closeById(229)
					it.player().unlock()
					val player = it.player()
					it.clearContext()
					
					it.delay(2)
					npc.face(player)
					it.delay(1)
					npc.respawns(false)
					npc.attack(player)
				} else {
					//If the player doesn't have a full set of armour, send this message instead of starting the sequence
					it.messagebox("You need a platebody, legs and full helm of the same type to activate<br>the armour animator.")
				}
			} else {
				it.chatNpc("Plain full plate armour in those machines only you may<br>use. When other items used, bad things happen, yes!", 2462, 572)
				it.chatPlayer("Ok, so I can only use plain full plate armour, no<br>trimmed or other stuff.", 589)
				it.chatNpc("It is as you say.", 2462, 567)
			}
		}
	}
	
}
