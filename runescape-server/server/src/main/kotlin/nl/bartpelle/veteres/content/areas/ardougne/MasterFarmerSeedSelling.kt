package nl.bartpelle.veteres.content.areas.ardougne

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 18/04/2016.
 */

object MasterFarmerSeedSelling {
	
	val SEEDS = IntRange(5095, 5106) + IntRange(5280, 5324)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOnNpc(3257) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			SEEDS.forEach { seed ->
				if (seed == item) {
					it.chatNpc("I'll buy all of those seeds for 10 gold each.", 3257)
					if (it.options("Sell seeds for 10 gold each", "No thank you.") == 1) {
						val amt = it.player().inventory().count(item)
						it.player().inventory().remove(Item(item, amt), true)
						it.player().inventory().add(Item(995, amt * 10), true)
						it.itemBox("You get " + amt * 10 + " gold in return for your " + amt + " seeds.", 995)
					}
				}
			}
		}
	}
}