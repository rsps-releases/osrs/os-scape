package nl.bartpelle.veteres.content.mechanics

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.Login
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.services.serializers.pg.PgSqlPlayerSerializer
import nl.bartpelle.veteres.util.RSFormatter
import java.sql.Timestamp
import java.time.Instant

/**
 * Created by Bart on 12/8/2015.
 */
class MuteTimeSpecifier(val name: String) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(s: Script) {
		when (s.optionsTitled("What do you want to do?", "Set mute length.", "View mute length.", "Nothing.")) {
			1 -> {
				val len = s.inputString("Enter mute length (2 minutes, 1 day, until november, christmas 2016):")
				val date = RSFormatter.parseDate(len)
				
				if (date != null) {
					val duration = 2000 + (date.time - Timestamp.from(Instant.now()).time)
					s.message("<col=7F00FF>$name's</col> mute adjusted; expires in <col=7F00FF>${Login.duration(duration)}</col> on <col=7F00FF>$date.")
					s.player().world().server().service(PgSqlPlayerSerializer::class.java, true).ifPresent({ s ->
						s.mutePlayer(name, date.time)
					})
					s.player().world().playerByName(name).ifPresent({ other ->
						other.muted(Timestamp(date.time))
						other.message("Your mute has been adjusted, it expires in <col=7F00FF>${Login.duration(duration)}.")
					})
				} else {
					s.message("Unknown time specified.".col("FF0000"))
				}
			}
			2 -> {
				s.player().world().playerByName(name).ifPresent({ other ->
					val duration = 2000 + (other.mutedUntil.time - Timestamp.from(Instant.now()).time)
					s.player().message("<col=7F00FF>$name's</col> mute expires in <col=7F00FF>${Login.duration(duration)}.")
				})
			}
		}
	}
	
}