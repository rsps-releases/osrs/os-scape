package nl.bartpelle.veteres.content.skills.magic.lunar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.skills.magic.ProductionSpell
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item

/**
 * @author Mack
 */
class StringJewelrySpell: ProductionSpell(80) {

    private val UNSTRUNG_DATA = UnstrungJewellery.values()

    @Suspendable override fun able(it: Script, alert: Boolean): Boolean {
        val amulet = amulet(it.player())

        if (amulet == null) {
            if (alert) it.messagebox("You do not have a piece of unstrung jewelry to perform this spell.")
            return false
        }

        if (it.player().inventory().add(Item(amulet.strung()), false).failed()) {
            if (alert) it.messagebox("You don't have enough space in your inventory to carry anymore jewelry!")
            return false
        }

        return super.able(it, true)
    }

    @Suspendable override fun execute(it: Script) {
        val player = it.player()

        while (true) {
            if (!able(it, false)) break
            val amulet = amulet(player) ?: break

            player.inventory().remove(amulet.unstrung(), false)
            MagicCombat.has(player, runes(), true)

            player.animate(4413)
            player.graphic(729)

            player.skills().__addXp(Skills.CRAFTING, 4.0)
            player.skills().__addXp(Skills.MAGIC, 83.0)
            it.delay(4)
        }
    }

    override fun runes(): Array<Item> {
        return arrayOf(Item(9075, 2), Item(557, 10), Item(555, 5))
    }

    private fun amulet(player: Player): UnstrungJewellery? {
        for (i in 0..player.inventory().size()) {
            if (player.inventory()[i] == null) continue
            for (unstrungType in UNSTRUNG_DATA) {
                if (player.inventory()[i].id() == unstrungType.unstrung()) return unstrungType
            }
        }
        return null
    }

    enum class UnstrungJewellery(unstrungId: Int, strungId: Int) {
        GOLD_AMULET(1673, 1692),
        SAPPHIRE_AMULET(1675, 1694),
        EMERALD_AMULET(1677, 1696),
        RUBY_AMULET(1679, 1698),
        DIAMOND_AMULET(1681, 1700),
        DRAGONSTONE_AMULET(1683, 1702);

        private val unstrung = unstrungId
        private val strung = strungId

        fun unstrung(): Int {
            return unstrung
        }

        fun strung(): Int {
            return strung
        }
    }
}