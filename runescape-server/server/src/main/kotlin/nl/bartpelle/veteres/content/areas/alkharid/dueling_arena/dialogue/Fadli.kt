package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/28/2015.
 */

object Fadli {
	
	val FADLI = 3340
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(FADLI) @Suspendable {
			it.chatPlayer("Hi!", 567)
			it.chatNpc("What?", FADLI, 562)
			when (it.options("What do you do?", "What is this place?", "I'd like to access my bank, please.", "I'd like to collect items.",
					"Do you watch any matches?")) {
				1 -> {
					it.chatPlayer("What do you do?", 588)
					it.chatNpc("You can store your stuff here if you want. You can<br>dump anything you don't want to carry whilst you're<br>" +
							"fighting duels and then pick it up again on the way out.", FADLI, 564)
					it.chatNpc("To be honest I'm wasted here.", FADLI, 562)
					it.chatNpc("I should be winning duels in an arena! I'm the best<br>warrior in Al Kharid!", FADLI, 615)
					it.chatPlayer("Easy, tiger!", 571)
				}
				2 -> {
					it.chatPlayer("What is this place?", 575)
					it.chatNpc("Isn't it obvious?", FADLI, 614)
					it.chatNpc("This is the Duel Arena...duh!", FADLI, 588)
				}
				3 -> {
					it.chatPlayer("I'd like to access my bank, please.", 588)
					it.chatNpc("Sure.", FADLI, 562)
					Bank.open(it.player(), it)
				}
				4 -> {
					it.chatPlayer("I'd like to collect items.", 567)
					it.chatNpc("Ehhh... nah.", FADLI, 562)
				}
				5 -> {
					it.chatPlayer("Do you watch any matches?", 588)
					it.chatNpc("When I can.", FADLI, 588)
					it.chatNpc("Most aren't any good so I throw rotten fruit at them!", FADLI, 567)
					it.chatPlayer("Heh. Can I buy some?", 567)
					//it.chatNpc("Nope.", Fadli, 562 )
					if (!it.player().world().realm().isPVP) {
						it.player().world().shop(29).display(it.player())
					}
				}
			}
		}
		r.onNpcOption2(FADLI) {
			Bank.open(it.player(), it)
		}
		r.onNpcOption3(FADLI) {
			it.message("This feature is currently unavailable.")
		}
		r.onNpcOption4(FADLI) {
			//it.chatNpc("Sorry, I'm not interested.", Fadli, 562)
			if (!it.player().world().realm().isPVP) {
				it.player().world().shop(29).display(it.player())
			}
		}
	}
	
}
