package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 3/15/2016.
 */

object ChaosElemental {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10)) {
				if (EntityCombat.attackTimerReady(npc)) {
					
					val random = npc.world().random(7)
					npc.animate(npc.attackAnimation())
					when (random) {
						1 -> teleport_attack(it, npc, target)
						2 -> disarming_attack(it, npc, target)
						else -> primary_attack(it, npc, target)
					}
					
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, 4)
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun disarming_attack(it: Script, npc: Npc, target: Entity) {
		attack_logic(it, npc, target, 550, 551, 552)
		if (target.inventory().add(target.equipment().get(EquipSlot.WEAPON), false).success()) {
			target.equipment().set(EquipSlot.WEAPON, null)
		}
	}
	
	@Suspendable fun teleport_attack(it: Script, npc: Npc, target: Entity) {
		val random = target.world().random(5)
		attack_logic(it, npc, target, 553, 554, 555)
		target.teleport(Tile(target.tile().x - random, target.tile().z - random))
	}
	
	@Suspendable fun primary_attack(it: Script, npc: Npc, target: Entity) {
		val combat_style = attack_style(it)
		
		attack_logic(it, npc, target, 556, 557, 558)
		
		if (AccuracyFormula.doesHit(npc, target, combat_style, 1.0))
			target.hit(npc, EntityCombat.randomHit(npc), 0).combatStyle(combat_style)
		else
			target.hit(npc, 0, 0).combatStyle(combat_style)
	}
	
	@Suspendable fun attack_logic(it: Script, npc: Npc, target: Entity, initial_graphic: Int, projectile: Int, end_graphic: Int) {
		val tileDist = npc.tile().distance(target.tile())
		val time = Math.max(1, (20 + tileDist * 12) / 30) + 1
		npc.graphic(initial_graphic, 80, 0)
		npc.world().spawnProjectile(npc, target, projectile, 40, 25, 35, 12 * tileDist, 15, 10)
		it.delay(time)
		target.graphic(end_graphic, 75, 0)
	}
	
	fun attack_style(it: Script): CombatStyle {
		val random = it.npc().world().random(10)
		if (random > 7)
			return CombatStyle.MAGIC
		else
			return CombatStyle.RANGE
	}
}
