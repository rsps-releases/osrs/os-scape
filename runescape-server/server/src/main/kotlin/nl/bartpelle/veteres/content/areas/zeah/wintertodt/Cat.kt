package nl.bartpelle.veteres.content.areas.zeah.wintertodt

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/11/2016.
 */

object Cat {
	
	val CAT = 7380
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(CAT) @Suspendable {
			it.chatNpc("Meow.", CAT, 588)
		}
	}
}