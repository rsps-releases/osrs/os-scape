package nl.bartpelle.veteres.content.skills.mining

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/27/2015.
 */

object RuneEssenceMining {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(7471) @Suspendable { mineEssence(it) }
		r.onObject(7479) @Suspendable {
			it.player().lockNoDamage()
			it.player().graphic(110, 124, 30)
			it.player().message("You step through the portal.")
			it.delay(2)
			it.player().teleport(3253, 3401)
			it.player().unlock()
		}
	}
	
	fun power(player: Player, pick: Mining.Pickaxe): Int {
		val points = ((player.skills().level(Skills.MINING) - 1) + 1 + pick.points.toDouble())
		return Math.min(80, points.toInt())
	}
	
	fun ticks(power: Int): Int {
		return 2 + when (power) {
			in 0..14 -> 3 // Total of 5 (3.0s)
			in 15..29 -> 2 // Total of 4
			in 30..50 -> 1 // Total of 3
			else -> 0 // Total of 2 (1.2s)
		}
	}
	
	@Suspendable fun mineEssence(s: Script) {
		val pick = Mining.findPickaxe(s.player())
		if (pick == null) {
			s.player().sound(2277, 0)
			s.messagebox("You need a pickaxe to mine this rock. You do not have a pickaxe<br>which you have the Mining level to use.")
			return
		}
		
		if (s.player().inventory().full()) {
			s.player().sound(2277, 0)
			s.message("Your inventory is too full to hold any more rune stones.")
			s.animate(-1)
			return
		}
		
		s.delay(1)
		s.message("You swing your pick at the rock.")
		
		while (true) {
			if (s.player().inventory().full()) {
				s.player().sound(2277, 0)
				s.message("Your inventory is too full to hold any more rune stones.")
				s.animate(-1)
				return
			}
			
			s.animate(pick.anim)
			s.delay(ticks(power(s.player(), pick)))
			
			s.player().inventory() += if (s.player().skills().level(Skills.MINING) >= 30) 7936 else 1436
			s.addXp(Skills.MINING, 5.0)
		}
	}
}