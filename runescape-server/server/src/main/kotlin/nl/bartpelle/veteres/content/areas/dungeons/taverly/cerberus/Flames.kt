package nl.bartpelle.veteres.content.areas.dungeons.taverly.cerberus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.npcs.bosses.cerberus.CerberusRegion
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/7/2016.
 */

object Flames {
	
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(23105) @Suspendable {
			//Crossing the flames (Both interaction options)
			if (it.interactionOption() == 1) {
				if (it.optionsTitled("Do you wish to pass through the flames?", "Yes - I know I'll get hurt.", "No way!") == 1) {
					move_through_flames(it)
				}
			} else {
				move_through_flames(it)
			}
		}
	}
	
	@Suspendable fun move_through_flames(it: Script) {
		val x = it.player().tile().x
		val z = it.player().tile().z
		val player = it.player()
		val region = player.tile().region()
		val world = player.world()
		val cerberusRegion = CerberusRegion.valueOfRegion(region)
		
		if (z < it.interactionObject().tile().z) {
			player.lockNoAttack()
			player.face(null)
			player.pathQueue().interpolate(x, z + 2, PathQueue.StepType.FORCED_WALK)
			it.player().hit(it.player(), 5, 1)
			it.waitForTile(Tile(x, z + 2))
			player.unlock()
		} else {
			player.lockNoAttack()
			player.face(null)
			player.pathQueue().interpolate(x, z - 2, PathQueue.StepType.FORCED_WALK)
			it.player().hit(it.player(), 5, 1)
			it.waitForTile(Tile(x, z - 2))
			player.unlock()
		}
	}
	
	@Suspendable fun spawn_flames(p: Player, first_tile: Tile, second_tile: Tile, third_tile: Tile) {
		p.world().spawnObj(MapObj(first_tile, 23105, 10, 3), true)
		p.world().spawnObj(MapObj(second_tile, 23105, 10, 3), true)
		p.world().spawnObj(MapObj(third_tile, 23105, 10, 3), true)
	}
	
	@Suspendable fun remove_flames(p: Player, first_tile: Tile, second_tile: Tile, third_tile: Tile) {
		p.world().removeObj(MapObj(first_tile, 23105, 10, 3), true)
		p.world().removeObj(MapObj(second_tile, 23105, 10, 3), true)
		p.world().removeObj(MapObj(third_tile, 23105, 10, 3), true)
	}
	
}
