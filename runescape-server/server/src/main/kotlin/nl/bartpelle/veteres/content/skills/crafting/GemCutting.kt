package nl.bartpelle.veteres.content.skills.crafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 10/30/2015.
 */


object GemCutting {
	
	val CHISEL = 1755
	
	enum class gems(val level: Int, val uncut: Int, val cut: Int, val cutName: String, val exp: Double, val anim: Int, val plural: String) {
		OPAL(1, 1625, 1609, "Opal", 15.0, 886, "Opals"),
		JADE(13, 1627, 1611, "Jade", 20.0, 886, "Jades"),
		RED_TOPAZ(16, 1629, 1613, "Red topaz", 25.0, 887, "Red topazes"),
		SAPPHIRE(20, 1623, 1607, "Sapphire", 50.0, 888, "Sapphires"),
		EMERALD(27, 1621, 1605, "Emerald", 67.5, 889, "Emeralds"),
		RUBY(34, 1619, 1603, "Ruby", 85.0, 887, "Rubies"),
		DIAMOND(43, 1617, 1601, "Diamond", 107.5, 886, "Diamonds"),
		DRAGONSTONE(55, 1631, 1615, "Dragonstone", 137.5, 885, "Dragonstones"),
		ONYX(67, 6571, 6573, "Onyx", 167.5, 2717, "Onyxes"),
		ZENYTE(89, 19496, 19493, "zenyte", 200.0, 7185, "zenyte")
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//For each gem in the players inventory do the following..
		gems.values().forEach { gem ->
			if (gem.uncut != -1) {
				//If the player uses an uncut gem with a chisel we..
				repo.onItemOnItem(gem.uncut.toLong(), CHISEL.toLong(), s@ @Suspendable {
					if (it.player().world().realm().isPVP) {
						it.messagebox("That's a bit pointless is it not...?")
						return@s
					}
					//Check if the player has the required level to cut the gem.
					if (it.player().skills()[Skills.CRAFTING] < gem.level) {
						it.messagebox("You need a Crafting level of ${gem.level} to cut ${gem.plural}.")
						return@s
					}
					
					//Prompt the player with the # they'd like to cut
					var num = 1
					if (it.player().inventory().count(gem.uncut) > 1) {
						// By default, 1, if we have more, ask how many.
						num = it.itemOptions(Item(gem.cut, 150), offsetX = 12)
					}
					
					while (num-- > 0) {
						//Check if the player still has an uncut gem and chisel.
						if (gem.uncut !in it.player().inventory() || CHISEL !in it.player().inventory()) {
							break
						}
						
						//Remove the uncut, add a cut, animate, send message, add experience and set delay.
						it.player().inventory() -= gem.uncut
						it.player().inventory() += gem.cut
						it.player().animate(gem.anim)
						it.message("You cut the ${gem.cutName}.")
						it.addXp(Skills.CRAFTING, gem.exp)
						it.delay(2)
					}
				})
			}
		}
	}
}