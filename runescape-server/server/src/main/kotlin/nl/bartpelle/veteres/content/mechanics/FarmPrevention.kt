package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.clanwars.FFAClanWars
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.player.Privilege
import java.util.*

/**
 * Created by Mack on 10/30/2017.
 */
object FarmPrevention {
	
	private const val KILL_HISTORY_MAX_SIZE = 3
	
	@JvmStatic fun valid(entity: Entity, target: Player): Boolean {
		if (!entity.isPlayer || !target.isPlayer) {
			return false
		}
		if (FFAClanWars.inFFAMap(entity) || ClanWars.inInstance(entity) || (entity.attribOr<Boolean>(AttributeKey.IN_STAKE, false))) {
			return false
		}
		
		val player = entity as Player
		val list = player.attribOr<String>(AttributeKey.KILLED_PLAYERS, "")
		
		if (!player.privilege().eligibleTo(Privilege.ADMIN) && player.ip() == target.ip()) {
			return false
		}
		
		if (list.isEmpty()) {
			log(player, target.id(), list)
			return true
		}
		
		if (!list.isEmpty() && contains(player, target)) {
			player.message("You have recently killed this player therefore you will not receive any bonus rewards.")
			return false
		}
		
		log(player, target.id(), list)
		return true
	}
	
	fun log(player: Player, targetUid: Any, list: String) {
		val array = ArrayList<Int>()
		var formatted = ""
		
		if (!list.isEmpty()) {
			for (id in list.split(",")) {
				array.add(Integer.parseInt(id.trim()))
			}
		}
		
		if (!array.contains(targetUid)) {
			targetUid as Int
			array.add(targetUid)
		}
		
		val toRemove = array.size - KILL_HISTORY_MAX_SIZE
		if (toRemove > 0) {
			for (i in 0..toRemove - 1) {
				array.removeAt(0)
			}
		}
		
		formatted = array.toString().replace("[", "").replace("]", "").trim()
		player.putattrib(AttributeKey.KILLED_PLAYERS, formatted)
	}
	
	fun historyFor(player: Player): ArrayList<Int> {
		if (player.attribOr<String>(AttributeKey.KILLED_PLAYERS, "").isEmpty()) {
			return ArrayList()
		}
		
		val container = player.attrib<String>(AttributeKey.KILLED_PLAYERS).split(",")
		val toReturn = arrayListOf<Int>()
		
		for (id in container) {
			toReturn.add(Integer.parseInt(id.trim()))
		}
		
		return toReturn
	}
	
	fun contains(player: Player, target: Player): Boolean {
		val id = target.id() as Int
		return (historyFor(player).contains(id))
	}
}