package nl.bartpelle.veteres.content.minigames.pest_control

/**
 * Created by Jason MacKeigan on 2016-09-02 at 12:15 AM
 *
 * Represents an enumeration of breakable objects where each chronological value
 * represents the successor object that is spawned upon the destruction or
 * explosion of the breakable object. All elements must contain at least
 * three elements, otherwise the program will fail to execute.
 */
enum class Breakable(vararg val stages: Int) {
	BARRICADE(14224, 14227, 14230),
	TOP_CORNER_BARRICADE(14225, 14228, 14231),
	BOTTOM_CORNER_BARRICADE(14226, 14229, 14232),
	SECOND_DOOR(14235, 14239, 14243, 14247),
	THIRD_DOOR(14233, 14237, 14242, 14245)
	;
	
	fun after(current: Int): Int {
		val indexOfCurrent = stages.indexOf(current)
		
		if (indexOfCurrent == -1) {
			return -1
		}
		val nextIndex = indexOfCurrent + 1
		
		if (nextIndex > stages.size - 1) {
			return -1
		}
		return stages[nextIndex]
	}
	
	fun before(current: Int): Int {
		val indexOfCurrent = stages.indexOf(current)
		
		if (indexOfCurrent == -1) {
			return -1
		}
		val nextIndex = indexOfCurrent - 1
		
		if (nextIndex < 0) {
			return -1
		}
		return stages[nextIndex]
	}
	
	val allButFirst = stages.copyOfRange(1, stages.size)
	
	companion object {
		
		fun after(breakable: Breakable, current: Int): Int {
			return breakable.after(current)
		}
		
		val DOORS = arrayOf(SECOND_DOOR, THIRD_DOOR)
	}
	
	init {
		if (stages.size < 3) {
			throw RuntimeException("Fatal exception, Breakable.kt object contains less than 3 arguments. See class for more")
		}
	}
}