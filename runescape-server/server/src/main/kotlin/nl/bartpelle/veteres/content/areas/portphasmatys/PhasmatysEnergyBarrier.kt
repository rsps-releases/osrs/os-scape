package nl.bartpelle.veteres.content.areas.portphasmatys

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.script.ScriptRepository
import java.lang.ref.WeakReference

/**
 * Created by Jak on 17/10/2016.
 */
object PhasmatysEnergyBarrier {
	
	var EAST_GUARD: Npc? = null
	var WEST_GUARD: Npc? = null
	
	val GATE = 16105
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		repo.onNpcSpawn(3007) {
			if (it.npc().tile().equals(3661, 3508)) {
				EAST_GUARD = it.npc()
			} else if (it.npc().tile().equals(3658, 3508)) {
				WEST_GUARD = it.npc()
			}
		}
		
		repo.onObject(GATE) @Suspendable {
			val obj = it.interactionObject()
			val option = it.interactionOption()
			val player = it.player()
			if (option == 1) {
				if (player.tile().z >= 3508) { // We're outside
					if (EAST_GUARD != null && WEST_GUARD != null) {
						var chatTo = EAST_GUARD
						if (player.tile().distance(WEST_GUARD!!.tile()) < player.tile().distance(EAST_GUARD!!.tile()))
							chatTo = WEST_GUARD
						player.putattrib(AttributeKey.TARGET, WeakReference<Entity>(chatTo))
						guardChat(it)
					}
				} else { // inside
					it.player().lock()
					it.player().pathQueue().clear()
					it.player().pathQueue().interpolate(3660, 3509, PathQueue.StepType.FORCED_WALK)
					it.waitForTile(Tile(3660, 3509))
					it.player().unlock()
				}
			} else if (option == 4) {
				if (player.tile().z >= 3508) { // We're outside
					it.player().lock()
					it.player().message("All visitors to Port Phasmatys must pay a toll charge of 2 Ectotokens. However, you have done the ghosts of our town a" +
							" service that surpasses all value, so you may pass without charge.")
					it.player().pathQueue().clear()
					it.player().pathQueue().interpolate(3660, 3508, PathQueue.StepType.FORCED_WALK)
					it.player().pathQueue().interpolate(3660, 3507, PathQueue.StepType.FORCED_WALK)
					it.waitForTile(Tile(3660, 3507))
					it.player().unlock()
				} else { // inside
					it.player().lock()
					it.player().pathQueue().clear()
					it.player().pathQueue().interpolate(3660, 3509, PathQueue.StepType.FORCED_WALK)
					it.waitForTile(Tile(3660, 3509))
					it.player().unlock()
				}
			}
		}
	}
	
	@Suspendable fun guardChat(it: Script) {
		it.chatNpc("All visitors to Port Phasmatys must pay a toll charge of 2 Ectotokens. However, you have done the ghosts of our town a" +
				" service that surpasses all value, so you may pass without charge.", it.targetNpc()!!.id())
		it.player().lock()
		it.player().pathQueue().clear()
		it.player().pathQueue().interpolate(3660, 3508, PathQueue.StepType.FORCED_WALK)
		it.player().pathQueue().interpolate(3660, 3507, PathQueue.StepType.FORCED_WALK)
		it.waitForTile(Tile(3660, 3507))
		it.player().unlock()
	}
}