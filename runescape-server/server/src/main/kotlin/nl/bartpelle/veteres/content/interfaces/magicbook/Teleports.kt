package nl.bartpelle.veteres.content.interfaces.magicbook

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.DonationTier
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.GameCommands
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Carl on 2015-08-12.
 */

object Teleports {
	
	/**
	 * Determines if the player is able to teleport. The inform parameter
	 * allows us to inform the player of the reason they cannot teleport
	 * if we so wish to.
	 */
	@JvmStatic fun canTeleport(player: Player, inform: Boolean, teletype: TeleportType): Boolean {
		if (ClanWars.inInstance(player)) {
			if (inform) {
				player.message("You cannot teleport out of a clan war.")
			}
			return false
		}
		if (Staking.in_duel(player)) {
			if (inform) {
				player.message("You cannot teleport out of a duel.")
			}
			return false
		}
		if (WildernessLevelIndicator.isTutorialIsland(player.tile().region())) { // On tutorial island
			return false
		}
		if (player.timers().has(TimerKey.TELEBLOCK)) {
			if (inform) {
				player.teleblockMessage()
			}
			return false
		}
		val wildLevel = if (teletype == TeleportType.ABOVE_20_WILD) 30 else 20
		if (WildernessLevelIndicator.wildernessLevel(player.tile()) > wildLevel) {
			if (inform) {
				player.message("A mysterious force blocks your teleport spell!")
				player.message("You can't use this teleport after level $wildLevel wilderness.")
			}
			return false
		}
		if (player.jailed()) {
			if (inform) {
				player.message("You can't leave the jail yet.")
			}
			return false
		}
		if (InfernoContext.inSession(player)) {
			player.message("You can't teleport out of The Inferno.")
			return false
		}
		//Is our player currently in an active Fight Cave?
		val inFightCavesMinigame = player.attribOr<Boolean>(AttributeKey.TZHAAR_MINIGAME, false)
		if (inFightCavesMinigame) {
			player.message("You can't teleport out of the Fight Caves.")
			return false
		}
		if (teletype == TeleportType.HOME_TELEPORT) {
			// Cannot do this in combat, come on. That'd be nigger.
			if (player.timers().has(TimerKey.COMBAT_LOGOUT) || PlayerCombat.inCombat(player)) {
				player.message("You can't cast this spell when in combat.")
				return false
			}
		}
		if (player.world().realm().isPVP && player.timers().has(TimerKey.BLOCK_SPEC_AND_TELE) && player.varps().varbit(Varbit.MULTIWAY_AREA) == 0) {
			player.message("<col=804080>Teleport blocked for ${(player.timers().left(TimerKey.BLOCK_SPEC_AND_TELE) * .6).toInt()} more secs after using spec at the start of a battle.")
			return false
		}
		if (player.looks().invisible()) {
			player.looks().invisible(false)
		}

		return true
	}
	
	@JvmStatic @Suspendable fun rolTeleport(player: Player): Boolean {
		// rol ringoflife ring of life
		player.stopActions(true)
		if (!canTeleport(player, true, TeleportType.GENERIC)) {
			return false
		}
		return true
	}
	
	
	@JvmField val ringOfLifeTeleport: Function1<Script, Unit> = s@ @Suspendable {
		val player = it.player()
		it.player().lockNoDamage()
		it.player().animate(714)
		it.player().graphic(111, 92, 0)
		it.delay(3)
		if (player.world().realm().isPVP) {
			if (PVPAreas.inPVPArea(player)) {
				player.teleport(PVPAreas.safetileFor(player)) //Teleport the player inside edgeville bank (pvp height)
			} else {
				player.teleport(3094, 3469) //Teleport the player edge coffin spot
			}
		} else {
			player.teleport(3212, 3423) //Teleport the player to Varrock square
		}
		it.animate(-1)
		it.player().graphic(-1)
		it.player().unlock()
	}
	
	@JvmStatic @Suspendable fun basicTeleport(player: Player, tile: Tile) {
		player.executeScript { basicTeleport(it, tile) }
	}
	
	@JvmStatic @Suspendable fun basicTeleport(it: Script, tile: Tile) {
		basicTeleport(it, tile, 714, Graphic(111, 92))
	}
	
	@JvmStatic @Suspendable fun basicTeleport(it: Script, tile: Tile, anim: Int, gfx: Graphic) {
		DeadmanMechanics.attemptTeleport(it)
		it.player().lockNoDamage()
		it.player().animate(anim)
		it.player().graphic(gfx.id(), gfx.height(), gfx.delay())
		it.player().sound(200)
		it.delay(3)
		it.player().teleport(tile)
		it.player().animate(-1)
		it.player().graphic(-1)
		it.player().unlock()
	}
	
	/**
	 * For uninterruptable scripts (BOTS ONLY!)
	 */
	@JvmStatic @Suspendable fun teleportContextless(it: Script, player: Player, tile: Tile, anim: Int, gfx: Graphic) {
		player.lockNoDamage()
		player.animate(anim)
		player.graphic(gfx.id(), gfx.height(), gfx.delay())
		player.sound(200)
		it.delay(3)
		player.teleport(tile)
		player.animate(-1)
		player.graphic(-1)
		player.unlock()
	}
	
}

class TeleportNoReqs(val targetTile: Tile) : Function1<Script, Unit> {
	override fun invoke(it: Script) {
		Teleports.basicTeleport(it, targetTile)
	}
}

data class autocast_definition(val level: Int, val cast_sound: Int, val hit_sound: Int, val animation: Int, val projectile: Int, val speed: Int, val startgfx: Int, val gfx: Graphic,
                               val splash_sound: Int, val maxhit: Int, val xp: Double, val runes: Array<out Item>, val spellbook: Int, val name: String) {
	
}

@Suspendable fun register_spell(level: Int, repo: ScriptRepository, button: Int, child: Int, cast_sound: Int, hit_sound: Int, animation: Int, projectile: Int, speed: Int, startgfx: Int, gfx: Graphic, splash_sound: Int, maxhit: Int, spellbook: Int, xp: Double, name: String, vararg runes: Item) {
	PlayerCombat.AUTOCAST_REPO.put(child, autocast_definition(level, cast_sound, hit_sound, animation, projectile, speed, startgfx, gfx, splash_sound, maxhit, xp, runes, spellbook, name))
	repo.onSpellOnEntity(button, child) @Suspendable {
		PlayerCombat.spellOnPlayer(it, cast_sound, hit_sound, animation, projectile, speed, startgfx, gfx, splash_sound, maxhit, spellbook, level, xp, name, *runes)
	}
}

@Suspendable fun attempt_home_teleport(it: Script) {
	//If the player is locked or dead
	if (it.player().locked() || it.player().dead() || it.player().hp() <= 0)
		return
	
	//Stop the players actions
	it.player().stopActions(true)
	
	//If our player is on OSRune, give them the option to teleport to home or the market place
	if (it.player().world().realm().isOSRune) {
		cast_home_teleport(it, 3088 + it.player().world().random(1), 3502 + it.player().world().random(1))
		return
	}
	
	// If the player is on the PVPWorld world, send their ass to edge!
	if (it.player().world().realm().isPVP) {
		cast_home_teleport(it, 3088, 3505)
		return
	}
	
	if (it.player().world().realm().isDeadman) {
		cast_home_teleport(it, 3145, 3510)
		return
	}
	
	cast_home_teleport(it, 3211 + it.player().world().random(2), 3422 + it.player().world().random(2))
}

@Suspendable fun cast_home_teleport(it: Script, x: Int, z: Int) {
	if (it.player().locked()) {
		return
	}
	
	if (!Teleports.canTeleport(it.player(), true, TeleportType.HOME_TELEPORT)) {
		return
	}
	
	it.onInterrupt { it.player().graphic(-1) }
	it.player().stopActions(true)
	
	DeadmanMechanics.attemptTeleport(it)
	val donatorByIcon = it.player().calculateBaseIcon() >= 15 && it.player().calculateBaseIcon() <= 21
	// Donators get an instant home teleport, which also looks wicked!
	if (donatorByIcon || it.player().donationTier() != DonationTier.NONE || !it.player.world().realm().isRealism || it.player().privilege().eligibleTo(Privilege.ADMIN)) {
		it.player().lockNoDamage()
		it.delay(1)
		it.player().graphic(1296)
		it.player().animate(3865)
		it.delay(3)
		it.player().teleport(x, z, 0)
		it.animate(-1)
		it.player().unlock()
	} else {
		it.animate(4847)
		it.player().graphic(800)
		it.delay(7)
		if (!player_in_combat(it)) {
			it.animate(4850)
			it.delay(5)
			if (!player_in_combat(it)) {
				it.animate(4853)
				it.player().graphic(802)
				it.delay(3)
				if (!player_in_combat(it)) {
					it.animate(4855)
					it.player().graphic(803, -1, 0)
					it.delay(3)
					if (!player_in_combat(it)) {
						it.player().graphic(804)
						it.delay(1)
						if (!player_in_combat(it)) {
							it.animate(4857)
							it.delay(3)
							if (!player_in_combat(it)) {
								it.player().teleport(x, z, 0)
								
								// I mean, we could be a little bit nigger...
								it.player().message("<col=ff0000>If you were a premium member, your teleport would be instant! Check ::store.")
							}
						}
					}
				}
			}
		}
	}
}

//Boolean to see if the player is in combat or not.
fun player_in_combat(it: Script): Boolean {
	val myLastTime = System.currentTimeMillis() - it.player().attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0.toLong())
	return myLastTime < 4500
}

@Suspendable fun cast_spell(it: Script, spellbook: Int, level: Int, targetTile: Tile, xp: Double, unsafe: Boolean, vararg runes: Item, shout: String = "") {
	val player: Player = it.player()
	if (it.player().locked()) {
		return
	}
	
	it.player().stopActions(true)
	
	//Check for level requirements
	if (it.player().skills().xpLevel(Skills.MAGIC) < level) {
		it.player().message("Your Magic level is not high enough for this spell.")
		return
	}
	
	
	/*if (player.privilege() != Privilege.ADMIN && player.world().realm().isPVP && !GameCommands.wildernessTeleportAntiragOk(targetTile.x, targetTile.z, player, true)) {
		return
	}*/

	if (!Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
		return
	}

	// Check if we have all the runes
	if (!MagicCombat.has(it.player(), runes.toList().toTypedArray(), false))
		return
	
	if (player.privilege() != Privilege.ADMIN) {
		if (WildernessLevelIndicator.inWilderness(targetTile) && player.inventory().count(6685) > GameCommands.brewCap) {
			player.message("" + player.inventory().count(6685) + " brews is a little excessive don't you think?")
			return
		}
	} else {
		player.message("As an admin you bypass pk-tele restrictions.")
	}
	
	if (unsafe) {
		if (it.optionsTitled("Teleport into the wilderness?", "Yes", "No") != 1) {
			return
		}
	}
	DeadmanMechanics.attemptTeleport(it)

	MagicCombat.has(it.player(), runes.toList().toTypedArray(), true)
	
	if (!shout.isNullOrEmpty()) {
		it.player().sync().shout(shout)
	}
	it.player().world().spawnSound(it.player().tile(), 197, 0, 7)
	it.player().skills().__addXp(Skills.MAGIC, if (BonusContent.isActive(it.player, BlessingGroup.ARCANIST)) xp * 2 else xp) // Must be the safe variant, other one blocks
	it.player().lockNoDamage()
	if (spellbook == 0) {
		//Modern spells
		it.player().animate(714)
		it.player().graphic(111, 92, 0)
	} else if (spellbook == 1) {
		//Ancient spells
		it.player().animate(1979)
		it.player().graphic(392)
	} else if (spellbook == 3) {
		//Arceuus spells
		it.player().graphic(1296)
		it.player().animate(3865)
	}
	
	it.delay(3)
	it.player().teleport(it.player().world().randomTileAround(targetTile, 1))
	it.animate(-1)
	it.player().graphic(-1)
	it.player().unlock()
}
