package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.net.message.game.command.WorldHop
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.services.worldlist.WorldListServer

object LogoutTab {

	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onButton(182, 8) @Suspendable {
			// Logout button
			DeadmanMechanics.attemptLogout(it)
			it.player().putattrib(AttributeKey.LOGOUT, true)
		}

		repo.onButton(182, 3) {
			// "World switcher" button on logout tab
			it.player().interfaces().sendWidgetOn(69, Interfaces.InterSwitches.LOGOUT_TAB)
			it.player().interfaces().setting(548, 31, -1, -1, 2)
			it.player().interfaces().setting(69, 14, 0, 420, 6)
		}

		repo.onButton(69, 3) {
			// 'X' button on world switcher
			it.player().interfaces().sendWidgetOn(182, Interfaces.InterSwitches.LOGOUT_TAB)
			it.player().interfaces().setting(548, 31, -1, -1, 2)
		}

		repo.onButton(69, 19) @Suspendable {
			DeadmanMechanics.attemptLogout(it)
			it.player().putattrib(AttributeKey.LOGOUT, true)
		}

		repo.onButton(69, 9) {
			it.player().varps().varbit(4596, if (it.player().varps().varbit(4596) == 2) 3 else 2)
		}
		repo.onButton(69, 10) {
			it.player().varps().varbit(4596, if (it.player().varps().varbit(4596) == 1) 0 else 1)
		}
		repo.onButton(69, 11) {
			it.player().varps().varbit(4596, if (it.player().varps().varbit(4596) == 4) 5 else 4)
		}
		repo.onButton(69, 12) {
			it.player().varps().varbit(4596, if (it.player().varps().varbit(4596) == 8) 9 else 8)
		}
		repo.onButton(69, 13) {
			it.player().varps().varbit(4596, if (it.player().varps().varbit(4596) == 6) 7 else 6)
		}
		repo.onButton(69, 17) @Suspendable {
			val action = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			if (action == 2)
				it.player().varps().varbit(4597, 0)
			else
				it.player.world().server().service(WorldListServer::class.java, true).ifPresent { wls ->
					wls.worlds[it.player().varps().varbit(4597)]?.apply {
						DeadmanMechanics.attemptLogout(it)
						it.player().putattrib(AttributeKey.WORLDHOP_LOGOUTMSG, WorldHop(ip, id, flags))
						it.player().putattrib(AttributeKey.LOGOUT, true)
					}
				}
		}
		repo.onButton(69, 18) @Suspendable {
			val action = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			if (action == 2)
				it.player().varps().varbit(4598, 0)
			else
				it.player.world().server().service(WorldListServer::class.java, true).ifPresent { wls ->
					wls.worlds[it.player().varps().varbit(4598)]?.apply {
						DeadmanMechanics.attemptLogout(it)
						it.player().putattrib(AttributeKey.WORLDHOP_LOGOUTMSG, WorldHop(ip, id, flags))
						it.player().putattrib(AttributeKey.LOGOUT, true)
					}
				}
		}

		// World hopping
		repo.onButton(69, 14) @Suspendable {
			val slot = it.buttonSlot()
			val action = it.player().attrib<Int>(AttributeKey.BUTTON_ACTION)
			if (it.player().world().server().isDevServer) {
				DeadmanMechanics.attemptLogout(it)
				it.player().putattrib(AttributeKey.WORLDHOP_LOGOUTMSG, WorldHop("localhost", 2, 1029))
				it.player().putattrib(AttributeKey.LOGOUT, true)
			}
			//it.player().debug("slot %d", slot)
			else if (action == 1) {
				it.player.world().server().service(WorldListServer::class.java, true).ifPresent { wls ->
					wls.worlds[slot]?.apply {
						DeadmanMechanics.attemptLogout(it)
						it.player().putattrib(AttributeKey.WORLDHOP_LOGOUTMSG, WorldHop(ip, id, flags))
						it.player().putattrib(AttributeKey.LOGOUT, true)
					}
				}

			} else {
				if (it.player().varps().varbit(4597) == 0) {
					it.player().varps().varbit(4597, slot) // put in slot 1
				} else {
					it.player().varps().varbit(4598, slot) // slot 2
				}
			}
		}

	}
}