package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/13/2016.
 */

object AppreciationLetter {
	
	val APPRECIATION_LETTER = 7966
	val MYSTERY_BOX = 6199
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onItemOption1(APPRECIATION_LETTER) @Suspendable {
			if (it.player().inventory().remove(Item(APPRECIATION_LETTER), false).success()) {
				it.player().inventory().add(Item(MYSTERY_BOX), true)
				it.itemBox("Thank you for being such an amazing, loyal player! Please take this mystery box as a thank you from the OS-Scape team.", MYSTERY_BOX)
			}
		}
	}
	
}