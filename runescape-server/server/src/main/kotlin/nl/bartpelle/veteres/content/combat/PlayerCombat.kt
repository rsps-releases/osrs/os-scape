package nl.bartpelle.veteres.content.combat

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.clanwars.FFAClanWars
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator.updateCarriedWealthProtection
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.content.combat.melee.MeleeSpecialAttacks
import nl.bartpelle.veteres.content.combat.ranged.*
import nl.bartpelle.veteres.content.events.tournament.EdgevilleTourny
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.interfaces.SpellSelect
import nl.bartpelle.veteres.content.interfaces.magicbook.autocast_definition
import nl.bartpelle.veteres.content.items.equipment.BarrowsSets
import nl.bartpelle.veteres.content.items.equipment.TheaterOfBloodItems
import nl.bartpelle.veteres.content.items.equipment.ZulrahItems
import nl.bartpelle.veteres.content.items.skillcape.CapeOfCompletion
import nl.bartpelle.veteres.content.mechanics.MultiwayCombat
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.content.mechanics.Venom
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelAntiScams
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.minigames.inferno.InfernoContext
import nl.bartpelle.veteres.content.npcs.bosses.KrakenBoss
import nl.bartpelle.veteres.content.npcs.bosses.abyssalsire.AbyssalSire
import nl.bartpelle.veteres.content.npcs.bosses.abyssalsire.AbyssalSireState
import nl.bartpelle.veteres.content.npcs.godwars.armadyl.Kreearra
import nl.bartpelle.veteres.content.npcs.godwars.bandos.Graardor
import nl.bartpelle.veteres.content.npcs.godwars.saradomin.Zilyana
import nl.bartpelle.veteres.content.npcs.godwars.zamorak.Kril
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature
import nl.bartpelle.veteres.fs.Color
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.*
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.model.map.steroids.PathRouteFinder
import nl.bartpelle.veteres.model.map.steroids.RangeStepSupplier
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.*
import nl.bartpelle.veteres.util.journal.main.Risk
import nl.bartpelle.veteres.util.journal.toggles.RiskProtectionState
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.lang.ref.WeakReference
import java.text.NumberFormat
import java.util.*

/**
 * Created by Bart on 8/12/2015.
 */

object PlayerCombat {
	
	val logger: Logger = LogManager.getLogger(PlayerCombat)
	
	var AUTOCAST_REPO: HashMap<Int, autocast_definition> = HashMap()


	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//Autocasting
		repo.onButton(593, 3) { it.player().varps()[43] = 0; resetCb(it.player()) }
		repo.onButton(593, 7) { it.player().varps()[43] = 1; resetCb(it.player()) }
		repo.onButton(593, 11) { it.player().varps()[43] = 2; resetCb(it.player()) }
		repo.onButton(593, 15) { it.player().varps()[43] = 3; resetCb(it.player()) }
		
		// Autoretaliate
		repo.onButton(593, 27) { it.player().varps()[Varp.AUTO_RETALIATE_DISABLED] = if (it.player().varps()[Varp.AUTO_RETALIATE_DISABLED] == 1) 0 else 1 }

		repo.onTimer(TimerKey.RISK_PROTECTION) {
			RiskProtectionState.monitorRiskProtection(it.player())
		}
		
		repo.onTimer(TimerKey.COMBAT_IMMUNITY) {
			it.message("Your combat immunity has worn off.")
		}
		
		repo.onTimer(TimerKey.TELEBLOCK) {
			it.player().timers().cancel(TimerKey.BLOCK_SPEC_AND_TELE)
			// No message for fading; only when leaving the wilderness do you get the fade message.
		}
	}
	
	private fun resetCb(player: Player) { // Just the spell
		SpellSelect.reset(player, true, true)
	}
	
	@JvmStatic fun unfreeze_when_out_of_range(entity: Entity) {
		val freezer = entity.attrib<Entity>(AttributeKey.FROZEN_BY) ?: return
		if (freezer.tile().distance(entity.tile()) >= 13 || freezer.index() == -1) {//off screen or unregistered
			entity.timers().cancel(TimerKey.FROZEN) //Remove frozen timer key
			entity.timers().cancel(TimerKey.REFREEZE) //Remove frozen timer key
			if(entity.isPlayer) {
				val player = entity as Player
				player.write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
			}
			//unfreeze as freezer is out of distance. Alt: If freezer logged off, we get unfrozen.
		}
	}
	
	@JvmStatic fun getTarget(entity: Entity): Entity? {
		return entity.attribOr<WeakReference<Entity?>>(AttributeKey.TARGET, WeakReference<Entity>(null)).get()
	}
	
	@JvmStatic fun setTarget(entity: Entity, other: Entity) {
		entity.putattrib(AttributeKey.TARGET, WeakReference<Entity>(other))
	}
	
	@JvmStatic fun clearTarget(entity: Entity) {
		entity.clearattrib(AttributeKey.TARGET)
	}

	/**
	 * Finds the last person that attacked the given Entity
	 */
	@JvmStatic fun lastAttacker(entity: Entity): Entity? {
		return entity.attribOr(AttributeKey.LAST_DAMAGER, null)
	}

	/**
	 * Finds the last entity the given Entity attacked.
	 */
	@JvmStatic fun lastTarget(entity: Entity): Entity? {
		return entity.attribOr(AttributeKey.LAST_TARGET, null)
	}
	
	/**
	 * @return True to break combat loop in PlayerCombat -- since for autocasting this is embedded into the loop of PlayerCombat - a True return means you want to break that loop too!
	 */
	@Suspendable fun spellOnPlayer(script: Script, cast_sound: Int, hit_sound: Int, animation: Int, projectile: Int, speed: Int, startgfx: Int, gfx: Graphic,
	                               splash_sound: Int, maxhit: Int, spellbook: Int, level: Int, xp: Double, name: String, vararg runes: Item): Boolean {
		val ref: WeakReference<Entity> = script.player()[AttributeKey.TARGET] ?: return false
		
		var target: Entity? = ref.get()
		val player: Player = script.player()
		
		if (target != null) {
			player.face(target)
			if (target.isNpc) {
				val npc = target as Npc
				//if(npc.combatInfo().stats != null)
				//    player.debug("Npc stats: %s", npc.combatInfo().stats)
			}
		}
		
		player.pathQueue().clear()
		val prev_tick_autocast: Int = player.varps().varbit(Varbit.AUTOCAST_SELECTED)
		
		
		// You don't need to check target here, because the doMagicSpell method does it after all these initial checks.
		while (target != null && !target.dead() && !player.dead()) {
			val autocast_changed = prev_tick_autocast != player.varps().varbit(Varbit.AUTOCAST_SELECTED)
			if (autocast_changed) break // if we stop auto
			val tile = player.tile()
			
			if (target is Player && !target.world().realm().isDeadman) {
				if (!WildernessLevelIndicator.inAttackableArea(player)) {
					player.message(if (!PVPAreas.inPVPArea(player)) "You are not in the Wilderness." else "You are in a Safe Zone.")
					break
				}
				
				if (!WildernessLevelIndicator.inAttackableArea(target)) {
					player.message(if (!PVPAreas.inPVPArea(player)) "Your target is not in the Wilderness." else "Your target is in a Safe Zone.")
					break
				}
			}
			
			// On 07, the player gets unfrozen when the freezer is at least X tiles away
			PlayerCombat.unfreeze_when_out_of_range(player)
			
			var dist = 0
			if (player.sync().secondaryStep() != -1 && target.sync().secondaryStep() != -1)
				dist = 3
			else if (player.sync().secondaryStep() != -1)
				dist = 1
			
			//player.debug("dist=%d  my 1st=%d   opp first=%d%n", dist, player.sync().secondaryStep(), target.sync().secondaryStep())
			
			val attackRange = 10 + dist
			val inrange = player.tile().distance(target.tile()) <= attackRange
			
			// Projectile path finder good?
			val supplier = RangeStepSupplier(player, target, attackRange)
			var reached = supplier.reached(player.world(), target)
			
			// When you cast you stop moving
			if (!reached && !player.frozen() && !player.stunned()) {
				player.walkTo(target, PathQueue.StepType.REGULAR)
				player.pathQueue().trimToSize(if (player.pathQueue().running()) 2 else 1)
			}
			
			// Reassess after walkTo + trimToSize
			reached = supplier.reached(player.world(), target)
			
			// Combat stops if you're in range but projectile won't reach.
			if (!reached && inrange && player.frozen()) {
				player.message("I can't reach that!")
				player.sound(154)
				// Just stop root combat loop, since it's embedded by autocasting in the PlayerCombat loop - not this spellOnPlayer loop
				// This is because spellOnPlayer direct call is magicOnPlayer packet - while the primary PC loop is AttackPlayer (click on player rather than use spell on)
				// Notably, stopactions doesn't interrupt the playercombat script.. fuck knows why! Have to use method return to break the root loop.
				return true
			} else if (reached) {
				// Recheck
				if (!player.timers().has(TimerKey.COMBAT_ATTACK)) {
					player.pathQueue().trimToSize(if (player.pathQueue().running() || target.pathQueue().running()) 2 else 1)
					
					val casted = MagicCombat.doMagicSpell(player, target, tile, cast_sound, hit_sound, animation, projectile, speed, startgfx, gfx, splash_sound, maxhit, spellbook, level, xp, name, *runes)
					if (casted) {
						player.timers()[TimerKey.IN_COMBAT] = 5
						target.putattrib(AttributeKey.LAST_DAMAGER, player)
						target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
						player.putattrib(AttributeKey.LAST_ATTACK_TIME, System.currentTimeMillis())
						player.putattrib(AttributeKey.LAST_TARGET, target)
						if (target.isPlayer) {
							val targ = target as Player
							targ.interfaces().closeMain()
							targ.interfaces().close(162, 550) // Close chatbox
						}
						player.interfaces().closeMain()
						player.interfaces().close(162, 550) // Close chatbox
					}
					
					if (target != null && target.isNpc) {
						val npc = target as Npc
						if (npc.id() == 5886)
							AbyssalSire.awakenAbyssalSire(npc, player)
					}
					
					break
				}
			}
			script.delay(1)
			target = ref.get()
		}
		return false
	}
	
	fun correctArrows(bow1: Int, arrow: Int): Boolean {
		var ok: Boolean = false
		BowReqs.values().forEach { bow ->
			if (bow1 == bow.bow) {
				bow.arrows.forEach { arrow2 ->
					if (arrow == arrow2) {
						ok = true
					}
				}
			}
		}
		return ok
	}
	
	fun BoltGFX(boltId: Int): Int {
		BoltEffects.values().forEach { bolt ->
			if (boltId == bolt.boltId) {
				return bolt.GFX
			}
		}
		return 0
	}
	
	fun BoltSpecialChance(boltId: Int, player: Boolean): Int {
		//If we're bolting a player or an NPC.
		BoltEffects.values().forEach { bolt ->
			if (boltId == bolt.boltId) {
				return if (player) bolt.hitOnPVP else bolt.hitOnPVM
			}
		}
		return 0
	}
	
	val SNOWBALL = 10501
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val ref: WeakReference<Entity> = it.player().attrib<WeakReference<Entity>>(AttributeKey.TARGET) ?: WeakReference<Entity>(null)
		var target: Entity? = ref.get() ?: null
		val player: Player = it.player()
		
		// Stuff that happens the first time we attack
		player.interfaces().closeMain()
		player.interfaces().close(162, 550) // Close chatbox
		
		if (player.varps().varbit(Varbit.AUTOCAST_SELECTED) != 0) {
			if (!player.timers().has(TimerKey.COMBAT_ATTACK)) { // There is a 1-tick delay before you start autocasting on RS
				player.timers().addOrSet(TimerKey.COMBAT_ATTACK, 1)
			}
		}
		if (target != null) {
			player.face(target)
			if (!canAttack(player, target)) {
				player.debug("<col=FF0000>Can't attack mate. Tick %s", player.world().cycleCount())
				return@s
			}
		}
		
		while (target != null && !target.dead() && !player.dead() && canAttack(player, target)) {

			if(MeleeSpecialAttacks.specialGraniteMaul(player)) {
				it.delay(1)
				continue
			}

			// So interestingly being stunned by a spear doesn't stop combat.
			if (player.timers().has(TimerKey.SPEAR)) {
				it.delay(1)
				target = ref.get()
				continue
			}
			
			PlayerCombat.unfreeze_when_out_of_range(player)
			// Begin by clearing the path queue
			player.pathQueue().clear()
			
			player.face(target)
			player.varps().varp(Varp.ATTACK_PRIORITY_PID, target.index())
			
			var tile = player.tile() // Grab base move tile
			
			// Establish some stuff...
			val weaponId = player.equipment()[EquipSlot.WEAPON]?.id() ?: -1
			val ammoId = player.equipment()[EquipSlot.AMMO]?.id() ?: -1
			val weaponType = player.world().equipmentInfo().weaponType(weaponId)
			val wepName = Item(weaponId).definition(player.world())?.name ?: ""
			val ammoName = Item(ammoId).definition(player.world())?.name ?: ""
			
			// You can't attack anyone who's NOT in the correct zone.
			if (target.isPlayer && weaponId != SNOWBALL && !target.world().realm().isDeadman && (!WildernessLevelIndicator.inAttackableArea(player)
					|| !WildernessLevelIndicator.inAttackableArea(target as Player))) {
				player.debug("<col=FF0000>Bad pvp zones.")
				break
			}
			
			// Put combat timer
			player.timers()[TimerKey.IN_COMBAT] = 5
			
			// Range, mage or melee?
			if (weaponType == WeaponType.BOW || weaponType == WeaponType.CROSSBOW || weaponType == WeaponType.THROWN || weaponType == WeaponType.CHINCHOMPA) {
				val attackRange = attackRange(player, weaponId)
				val inrange = player.tile().distance(target.tile()) <= attackRange
				val supplier = RangeStepSupplier(player, target, attackRange)
				
				// When launching an attack our path is cropped/stopped.
				if (!supplier.reached(player.world()) && !player.frozen() && !player.stunned()) {
					player.walkTo(target, PathQueue.StepType.REGULAR)
					player.pathQueue().trimToSize(if (player.pathQueue().running()) 2 else 1)
				}
				
				val reached = supplier.reached(player.world())
				// Reached is the projectile path finder. inrange is just strictly distance. We could be chasing (running)!
				if (!reached && inrange && player.frozen()) {
					player.message("I can't reach that!")
					player.sound(154)
					break
				}
				// Can we shoot?
				else if (reached) {
					
					val drag_thrownaxe_spec = (weaponId == 20849 && player.varps()[Varp.SPECIAL_ENABLED] == 1)
					/*if (drag_thrownaxe_spec && PlayerCombat.blockRushing(player, target)) {
						player.varps()[Varp.SPECIAL_ENABLED] = 0
						break
					}*/
					
					// Wait for cool down
					if (!player.timers().has(TimerKey.COMBAT_ATTACK) || drag_thrownaxe_spec) {
						
						val inpvpinstances = WildernessLevelIndicator.inPvpInstanceAttackable(player)
						
						// If you're in the mage arena, where it is magic only.
						if (player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) == 1) {
							player.message("You can only use magic inside the arena!")
							break
						}
						
						// We're ready to attack again.
						if (target.isPlayer) {
							if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_RANGED)) {
								player.message("Range is disabled for this duel.")
								break
							}
							if (ClanWars.checkRule(player, CWSettings.RANGE)) {
								player.message("Range is disabled for this clan war.")
								break
							}
							if (player.varps()[Varp.SPECIAL_ENABLED] != 0) {
								if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_SPK_ATK)) {
									player.message("Special attacks are disabled for this duel.")
									break
								}
								if (ClanWars.checkRule(player, CWSettings.SPECIAL_ATTACKS, 2)) {
									player.message("Special attacks are disabled for this clan war.")
									break
								}
							}
						}
						
						
						// We're ready to attack again.
						// Do we even have ammo?
						val crystalBow = (weaponId in 4212..4223)
						val blowpipe = weaponId == 12926 || weaponId == 12924
						if (!crystalBow && ((weaponType == WeaponType.BOW || weaponType == WeaponType.CROSSBOW) && ammoName == "")) {
							player.message("There's no ammo left in your quiver.")
							break
						}
						if (PlayerCombat.darkbow(weaponId) && ammoId != -1 && player.equipment().get(EquipSlot.AMMO).amount() < 2) {
							//d bow shoots 2 arrows. doubt accumulator affects ability to shoot
							player.message("You don't have enough ammo.")
							break
						}
						if (blowpipe) {
							if (!player.world().realm().isPVP && ZulrahItems.BLOWPIPE_REQUIRES_DARTS && player.equipment()[3].property(ItemAttrib.DARTS_COUNT) < 1) {
								player.message("Your blowpipe <col=FF0000>has no darts</col>.")
								break
							}
							if (weaponId == 12924 || (weaponId == 12926 && player.equipment()[EquipSlot.WEAPON].property(ItemAttrib.ZULRAH_SCALES) == 0)) {
								player.message("You have to load your Blowpipe with scales to shoot it.")
								break
							}
						}
						
						// Check if we use the right type of ammo first
						if (weaponType == WeaponType.CROSSBOW) {
							if (weaponId == 4734 || (weaponId in 4934..4938)) {
								// Karil's cbow
								if (ammoId != 4740) {
									player.message("You can't use that ammo with your crossbow.")
									break
								}
							} else if (weaponId == 19478 || weaponId == 19481) { // Light & heavy ballista
								if (ammoId !in 825..830 && ammoId !in 19484..19490) {
									player.message("You can't use that ammo with your Ballista.")
									break
								}
							} else if (weaponId == 10156 && ammoId !in 10158..10159) {
								player.message("You can't use that ammo with your crossbow.")
								break
							} else if ((ammoId in 21924 .. 21973 || ammoId == 21905) && weaponId !in intArrayOf(11785, 21902, 21012)) {
								player.message("Dragon bolts can only be fired from an Armadyl, Dragon or Dragon Hunter crossbow.")
								break
							} else if (" bolts" !in ammoName) {
								player.message("You can't use that ammo with your crossbow.")
								break
							}
						}
						
						if (!crystalBow && (weaponType == WeaponType.BOW && " arrow" !in ammoName)) {
							player.message("You can't use that ammo with your bow.")
							break
						}
						
						if (wepName.replace(" (i)", "").endsWith("bow", true) && !crystalBow && !wepName.startsWith("kari", true) && !wepName.contains("cross", true)) {
							// bolt ammo
							if (!correctArrows(weaponId, ammoId)) {
								val lit: String = if ((ammoName.contains("rack") || ammoName.contains("arrow")) && !ammoName.contains("arrows")) "s" else ""
								player.message("You can't use $ammoName$lit with a $wepName.")
								break
							}
						}
						
						if (target.isPlayer && !player.privilege().eligibleTo(Privilege.ADMIN)) {
							// restrictions
							if (GameCommands.rangeRestrictedZoneEnabled && player.world().realm().isPVP) {
								if (WildernessLevelIndicator.inRestrictedRangeZone(target.tile())) {
									player.message("This player is standing in a range-restricted zone.")
									break
								}
							}
							
							if (GameCommands.rangeZoneNeedsSameSpellbook && player.world().realm().isPVP) {
								if (WildernessLevelIndicator.inRestrictedRangeZone(target.tile())) {
									if (player.varps().varbit(Varbit.SPELLBOOK) != target.varps().varbit(Varbit.SPELLBOOK)) {
										player.message("This player is standing in a restricted zone - you must be on the same spellbook to fight.")
										break
									}
								}
							}
							
							if (GameCommands.westsAntiragEnabled && player.world().realm().isPVP) {
								if (WildernessLevelIndicator.at_west_dragons(player.tile())) {
									player.message("<col=9A2EFE>You're standing in a range-restricted zone.")
									break
								}
								if (WildernessLevelIndicator.at_west_dragons(target.tile())) {
									player.message("<col=9A2EFE>Your target is in a range-restricted zone.")
									break
								}
							}
							if (inpvpinstances) {
								val map = player.world().allocator().active(player.tile()).get()
								val map_id: InstancedMapIdentifier? = if (map.identifier.isPresent) map.identifier.get() else null
								if (map_id == InstancedMapIdentifier.GE_PVP) {
									val defenceCalc = player.skills().xpLevel(Skills.DEFENCE) - target.skills().xpLevel(Skills.DEFENCE)
									val defenceLevelDifferent = defenceCalc <= -10 || defenceCalc >= 10
									if (defenceLevelDifferent) {
										player.message("<col=9A2EFE>You and your opponents Defence levels must be within 10 levels of each other to use Range.")
										break
									}
								} else if (player.skills().combatLevel() > 100) {
									val in_edge = map_id == InstancedMapIdentifier.EDGE_PVP
									if (in_edge || map_id == InstancedMapIdentifier.CANIFIS_PVP) {
										player.message("With over 100 combat, range <col=FF0000>cannot</col> be used in the ${if (in_edge) "Edgeville" else "Canifis"} PvP instance.")
										break
									}
								}
							}
						}
						
						// End of combat checks, start attacking...
						val chins: Boolean = weaponType == WeaponType.CHINCHOMPA
						val ballista: Boolean = weaponId == 19478 || weaponId == 19481
						
						it.player().world().spawnSound(it.player().tile(), CombatSounds.weapon_attack_sounds(it), 0, 10)
						player.animate(EquipmentInfo.attackAnimationFor(player))
						
						val tileDist = tile.distance(target.tile())
						var cyclesPerTile = 5
						var baseDelay = if (chins) 20 else if (ballista) 30 else 41
						var startHeight = 40
						var endHeight = 36
						var curve = 15
						var graphic = -1 // 228
						var addRangeStr = false
						
						if (crystalBow) {
							addRangeStr = true
							player.graphic(250, 96, 0)
							graphic = 249
							if (weaponId == 4212) {
								player.equipment().remove(Item(4212), true, EquipSlot.WEAPON)
								player.equipment().add(Item(4214), true, EquipSlot.WEAPON)
								player.message("Your crystal bow has degraded!")
							}
						}
						
						if (chins) {
							graphic = if (weaponId == 10033) 908 else
								if (weaponId == 10034) 909 else 909 // TODO black chins gfx
						}
						
						// Bows need special love.. projectile and graphic :D
						if (weaponType == WeaponType.BOW && !crystalBow) {
							addRangeStr = true
							
							val db = ArrowDrawBack.values().find({ it.arrow == ammoId })
							// Find the gfx and do it : -)
							if (db != null) {
								if (PlayerCombat.darkbow(weaponId)) {
									val db2 = DblArrowDrawBack.values().find({ it.arrow == ammoId })
									if (db2 != null) {
										player.graphic(db2.gfx, 96, 0)
										graphic = db.projectile
									}
								} else {
									player.graphic(db.gfx, 96, 0)
									graphic = db.projectile
								}
							}
						}
						
						// Knives are not your de-facto stuff either
						if (weaponType == WeaponType.THROWN) {
							// Is this a knife? There are more than only knives that people throw.. think.. asparagus. uwat? darts, thrownaxes, javelins
							val drawback = KnifeDrawback.values().find({ it.arrow == weaponId })
							if (drawback != null) {
								player.graphic(drawback.gfx, 96, 0)
								graphic = drawback.projectile
							} else {
								val db2 = DartDrawback.values().find({ it.dart == weaponId })
								if (db2 != null) {
									player.graphic(db2.gfx, 96, 0)
									graphic = db2.projectile
								} else {
									val db3 = ThrownaxeDrawback.values().find({ it.arrow == weaponId })
									if (db3 != null) {
										player.graphic(db3.gfx, 96, 0)
										graphic = db3.projectile
									}
								}
							}
						}
						
						// Crossbows are the other type of special needs
						if (weaponType == WeaponType.CROSSBOW) {
							addRangeStr = true
							cyclesPerTile = 3
							baseDelay = if (ballista) 80 else 41
							startHeight = 40
							endHeight = if (ballista) 30 else 36
							curve = 5
							graphic = if (weaponId == 19481 || weaponId == 19478) ballistaProjectileFor(ammoId) else 27
						}
						
						target.putattrib(AttributeKey.LAST_DAMAGER, player)
						target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
						player.putattrib(AttributeKey.LAST_ATTACK_TIME, System.currentTimeMillis())
						player.putattrib(AttributeKey.LAST_TARGET, target)
						
						if (target.isPlayer) {
							val targ = target as Player
							targ.interfaces().closeMain()
							targ.interfaces().close(162, 550) // Close chatbox
						}
						player.interfaces().closeMain()
						player.interfaces().close(162, 550) // Close chatbox
						Equipment.checkTargetVenomGear(player, target)
						blockAnim(target, delayForBlock(weaponId, weaponType, tileDist))
						
						val not_spec = player.varps()[Varp.SPECIAL_ENABLED] == 0
						val did_spec = !not_spec && RangedSpecialAttacks.doRangeSpecial(it, player, target, tileDist)
						var targ_is_dummy = false
						
						if (not_spec || !did_spec) {
							if (graphic != -1)
								player.world().spawnProjectile(player.tile(), target, graphic, startHeight, endHeight, baseDelay, cyclesPerTile * tileDist, curve, 11)
							
							//val ballistaDelay = if(ballista) -1 else 0
							val hitdelay = calcHitDelay(weaponId, weaponType, tileDist)
							// Math.ceil(Math.floor(baseDelay / 30.0) + (tileDist.toDouble() * (cyclesPerTile.toDouble() * 0.020) / 0.6) + ballistaDelay)
							
							if (target.isNpc) {
								val npc = target as Npc
								if (npc.id() == 2668) {
									targ_is_dummy = true
									val boltSpec = wepName.contains("crossbow") && ammoName.contains("(e)")
									attackTarget(npc, player, weaponId, CombatStyle.RANGE, ((CombatFormula.maximumRangedHit(player, target, addRangeStr, boltSpec, ammoId) * 1.0) * MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, boltSpec && (ammoId == 9242 || ammoId == 21944))).toInt(), 2)
									if (boltSpec) target.graphic(BoltGFX(ammoId))
									continue
								}
							}
							
							val critBonuses = EquipmentInfo.criticalBonuses(player)
							val success = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, getPvpRangeAccuracyModifier(critBonuses)) || player.hasAttrib(AttributeKey.ALWAYS_HIT)
							val venom = Venom.attempt(player, target, CombatStyle.RANGE, success)
							if (venom)
								target.venom(player)
							
							if (success) {
								
								val can_bolt_spec: Boolean = wepName.contains("crossbow") && ammoName.contains("(e)")
								val boltspecial: Boolean = can_bolt_spec && if (player.world().random(100) < BoltSpecialChance(ammoId, true)) true else false
								
								var max = CombatFormula.maximumRangedHit(player, target, addRangeStr, boltspecial, ammoId) * 1.0
								max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.RANGE, boltspecial && (ammoId == 9242 || ammoId == 21944))
								
								if (target.isPlayer && RANGE_DPS_WHEEL_ENABLED) {
									val upperMax = max
									val lowWheelMax = if (upperMax >= 35) 33.0 else if (upperMax >= 33) 30.0 else if (upperMax >= 29) 26.0 else if (upperMax > 19)
										(upperMax - 2.0) else upperMax

									// Apply the cap / lower the DPS most of the time, then grant access to higher hits in circumstances
									// Where you should be taking people's heads off.. as the combat triangle would dictate.
									// let's go with a 50% base chance of being capped, then reduce that when we're powerful.
									val targetCritBonuses = EquipmentInfo.criticalBonuses(target as Player)
									val power = 0
									// TODO power needs to follow a formula... should probably be based off of how much "stronger" you are than
									// TODO your opponents defence, plus some randomness needs to be used otherwise there'd be no luck potential.
									// TODO Luck needs to player a larger part when against strong gear when two people are evenly matched
									// TODO For pures, this can be ignored, you'll hit through their shit defence always.
									// TODO This is mainly for rag/mystic vs max mage, to make magic gear slightly most absorbent. OMG ABSORPTION!
									if (player.world().rollDie(100, 50 + power)) {
										max = lowWheelMax
									}
								}
								
								val damage = player.attribOr<Int>(AttributeKey.ALWAYS_HIT, player.world().random(max.toInt()))
								val hit: Hit = target.hitpvp(player, damage, hitdelay, CombatStyle.RANGE)
								if (chins) {
									hit.graphic(Graphic(157, 100, 0))
									multi_target_chinchompas(it, target, hit, hitdelay, success)
								}
								hit.submit().addXp()
								if (Equipment.hasAmmyOfDamned(player) && Equipment.fullKaril(player) && player.world().rollDie(100, 25)) {
									// Second hit deals 50% of first hit. First hit will have protection prayers accounted for.
									val dam2: Hit = target.hit(player, hit.damage() / 2, hitdelay, false).combatStyle(CombatStyle.RANGE).pidAdjust().submit().addXp()
								}
								
								//If the bolt is a special attack we...
								if (boltspecial) {
									if (ammoId == 9245) {
										//If the bolt used is onyx (e) we...
										player.hp(player.hp() + hit.damage() / 4, 0) //Heal the player for 25% of the damage dealt.
									}
									target.graphic(BoltGFX(ammoId))
								}
								
							} else {
								// We missed.
								val hit: Hit = target.hitpvp(player, 0, hitdelay, CombatStyle.RANGE)
								
								if (chins) {
									hit.graphic(Graphic(157, 100, 0))
									multi_target_chinchompas(it, target, hit, hitdelay, success)
								}
								hit.submit()
							}
							
							if (PlayerCombat.darkbow(weaponId)) {
								// dark bow 2nd arrow
								player.world().spawnProjectile(player.tile(), target, graphic, startHeight, endHeight, 10 + baseDelay, 30 + (cyclesPerTile * tileDist), curve, 105)
								val success2 = AccuracyFormula.doesHit(player, target, CombatStyle.RANGE, 1.0)
								// At range 3 the hits hit the same time. may indicate mathematical rounding in calc.
								val secondArrowDelay = hitdelay + if (tileDist == 3) 0 else if (tileDist in intArrayOf(7, 8, 10)) 2 else 1
								if (success2) {
									val max2 = CombatFormula.maximumRangedHit(player, target, addRangeStr, false, -1)
									val second_damage = player.world().random(max2)
									val hit2: Hit = target.hitpvp(player, second_damage, secondArrowDelay, CombatStyle.RANGE).submit().addXp()
								} else {
									target.hitpvp(player, 0, secondArrowDelay, CombatStyle.RANGE).submit()
								}
							}
							
							var attackSpeed = player.world().equipmentInfo().weaponSpeed(weaponId)
							
							// Toxic blow pipe is faster on Npcs
							if (blowpipe && target is Npc)
								attackSpeed = 3
							
							// When firing rapid, one tick is subtracted
							if (EquipmentInfo.attackStyleFor(player) == AttackStyle.RAPID)
								attackSpeed -= 1
							
							player.timers()[TimerKey.COMBAT_ATTACK] = attackSpeed
							player.timers()[TimerKey.IN_COMBAT] = attackSpeed
						}
						
						// Remove the ammo :)
						if (!crystalBow && !blowpipe && !targ_is_dummy) {
							val equipSlot = if (chins || weaponId == 6522) EquipSlot.WEAPON else
								if (KnifeDrawback.values().any { it.arrow == weaponId }) EquipSlot.WEAPON else
									if (DartDrawback.values().any { it.dart == weaponId }) EquipSlot.WEAPON else
										if (JavelinDrawback.values().any { it.arrow == weaponId }) EquipSlot.WEAPON else
											if (ThrownaxeDrawback.values().any { it.arrow == weaponId }) EquipSlot.WEAPON else
												EquipSlot.AMMO
							val ammo: Item? = player.equipment()[equipSlot]
							
							// chins & javelins always lost. darts and ammo slot can be saved!
							val remove: Boolean = if (chins || ballista) true else PlayerCombat.notAvas(player)
							
							if (remove && ammo != null) {
								val ammoToRemove = if (PlayerCombat.darkbow(weaponId)) 2 else 1 // dark bow removes 2 arrows


								// Chance you can get an arrow back, instead of it being lost forever.
								val cape = player.equipment()[EquipSlot.CAPE]?.id() ?: -1

								// |------save------|---break---|-drop-|
								// As per wiki, normal avas is 72% save, 20% break, leaving 8% to go to the floor.
								// New avas is 80% save, 20% break, 0% floor
								val saveChance = if (cape in intArrayOf(21898, 22109)) 0.8001 else if (wearingAvasEffect(player)) 0.7201 else 0.0
								val roll = Math.random()

								if (saveChance == 0.0 || roll >= saveChance) {
									player.equipment()[equipSlot] = Item(ammo.id(), ammo.amount() - ammoToRemove)

									// These items do not drop ammo to the floor if lost.
									if (!chins && !ballista && ammo.id() != 4740 && roll >= saveChance + 0.2) {
										// 8% chance to drop if normal ava, otherwise will never drop (roll cannot be over 1.0)
										player.world().spawnGroundItem(GroundItem(player.world(), Item(ammo.id(), 1), target.tile(), player.id()))
										if (PlayerCombat.darkbow(weaponId)) {// support dropping 2nd arrow as well
											player.world().spawnGroundItem(GroundItem(player.world(), Item(ammo.id(), 1), target.tile(), player.id()))
										}
									}
								}
							}
						}
						
						//Check for skulling context.
						Skulling.skull(player, target)
						
						// After every attack, reset the special mode.
						player.varps()[Varp.SPECIAL_ENABLED] = 0
						if (did_spec) {
							PlayerCombat.check_spec_and_tele(player, target)
						}
						
						if (target != null && target.isNpc) {
							val npc = target as Npc
							if (npc.id() == 5886) AbyssalSire.awakenAbyssalSire(npc, player)
						}
						
					}
				}
			} else if ((player.equipment().hasAt(EquipSlot.WEAPON, 11905) || player.equipment().hasAt(EquipSlot.WEAPON, 11907)) && (target.isNpc || player.privilege().eligibleTo(Privilege.ADMIN))) {
				spellOnPlayer(it, -1, -1, 1167, 1252, 30, 1251, Graphic(1253, 90), 227, 28, 1, 75, 0.toDouble(), "trident")
				
				player.timers()[TimerKey.COMBAT_ATTACK] = 4
				player.timers()[TimerKey.IN_COMBAT] = 5
			} else if (player.equipment().hasAt(EquipSlot.WEAPON, 12899) && (target.isNpc || player.privilege().eligibleTo(Privilege.ADMIN))) {
				spellOnPlayer(it, -1, -1, 1167, 1040, 30, 665, Graphic(1042, 90), 227, 28, 1, 75, 0.toDouble(), "trident")

				player.timers()[TimerKey.COMBAT_ATTACK] = 4
				player.timers()[TimerKey.IN_COMBAT] = 5
			} else if (player.equipment().hasAt(EquipSlot.WEAPON, TheaterOfBloodItems.SANGUINESTI_CHARGED_ID) && (target.isNpc || player.privilege().eligibleTo(Privilege.ADMIN))) {
				spellOnPlayer(it, -1, -1, 1167, 1539, 30, 1540, Graphic(1541, 90), 227, 32, 1, 75, 0.toDouble(), "trident")

				player.timers()[TimerKey.COMBAT_ATTACK] = 4
				player.timers()[TimerKey.IN_COMBAT] = 5
			} else if (player.varps().varbit(Varbit.AUTOCAST_SELECTED) != 0) {
				var varp = player.varps().varbit(Varbit.AUTOCAST_SELECTED)
				val book = player.varps().varbit(Varbit.SPELLBOOK)

				player.debug("[Autocast Debug] spell=%d, book=%d", varp, book)
				
				val spell = when (book) {
					1 -> {
						if (varp - 31 >= SpellSelect.ANCIENT_CHILDREN.size || varp - 31 < 0) {
							player.message("Unsupported autocast spell selected.")
							SpellSelect.reset(player, true, true)
							null
						} else AUTOCAST_REPO[SpellSelect.ANCIENT_CHILDREN[varp - 31]]
					} // Ancient spellbook autocasting
					0 -> {
                        varp -= if (varp >= 48) 28 else 1

						if (varp >= SpellSelect.MODERN_CHILDREN.size || varp - 1 < 0) {
							player.debug("Unsupported autocast spell selected [spellId=$varp].")
							SpellSelect.reset(player, true, true)
							null
						} else {
							val spellid = SpellSelect.MODERN_CHILDREN[varp]
							//player.debug("Selected spell %s", spellid)
							AUTOCAST_REPO[spellid]
						}
					} // Modern spellbook autocasting
					2 -> {
						player.debug("Unsupported autocast spell selected.")
						null
					} // Lunars not supported
					else -> {
						player.debug("Unsupported autocast spell selected.")
						null
					} // New resurection zeah spellbook? Idk
				}
				
				if (spell != null) {
					val stop = spellOnPlayer(it, spell.cast_sound, spell.hit_sound, spell.animation, spell.projectile, spell.speed, spell.startgfx, spell.gfx,
							spell.splash_sound, spell.maxhit, spell.spellbook, spell.level, spell.xp, spell.name, *spell.runes)
					if (stop) {
						break
					}
				} else {
					// Spell is null.
					player.debug("<col=FF0000>Autocasting.. what exactly?</col> %s %s", varp, book)
				}
			} else if (player.equipment().hasAny(11908, 12900)) {
				player.message("You must charge your trident before you can use it. Use the <col=FF0000>check</col> option.")
				break
			} else if (target.isPlayer && player.equipment().hasAny(11905, 11907, 12899) && !player.privilege().eligibleTo(Privilege.ADMIN)
					&& !ClanWars.inInstance(player) && ClanWars.checkRule(player, CWSettings.ALLOW_TRIDENT)) {
				player.message("You can't use tridents against players.")
				break
			} else {
				// If you're in the mage arena, where it is magic only.
				if (player.varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY) == 1) {
					player.message("You can only use magic inside the arena!")
					break
				}
				
				// Dummy distance, just make sure they're on screen.
				val attackRange = 10
				val inrange = player.tile().distance(target.tile()) <= attackRange
				
				// Projectile path finder good?
				val supplier = RangeStepSupplier(player, target, attackRange)
				var reached = supplier.reached(player.world(), target)
				
				// Melee range? Using the tile we pathed too in the above code, providing we're not frozen.
				val touches = player.touches(target, tile)
				
				if (!touches && player.frozen()) {
					// Ok, so in this scenario, the projectile path finder can't figure a path. They're behind something. Tree hugging. CANCEL COMBAT!
					if (inrange && !reached) {
						player.message("I can't reach that!")
						player.sound(154)
						break
					} else {
						// No issues with the projectile path finder. Combat DOES NOT cancel, you just can't reach them this cycle.
						player.message("A magical force stops you from moving.")
						player.sound(154)
					}
				}
				
				var stuck = false
				
				// Ok seems like we're melee.
				if (!touches) {
					if (!player.frozen() && !player.stunned()) {
						// If the target is an NPC and is a non-stacking one, we must ensure we don't add the step yet.
						// Not frozen? Try to move closer.
						if (target is Npc && target.combatInfo()?.unstacked ?: false) {
							moveCloserNoPeek(player, target, tile)
						} else {
							tile = moveCloser(player, target, tile)
						}
					} else {
						// When frozen, don't attempt to move closer at all!
						stuck = true
					}
				}
				
				// Do we finally touch the enemy now?
				if (!stuck && player.touches(target, tile)) {
					// Check if we may deal a blow :)
					
					val gmaul_spec = (weaponId in intArrayOf(4153, 12848) && player.varps()[Varp.SPECIAL_ENABLED] == 1)
					/*if (gmaul_spec && PlayerCombat.blockRushing(player, target)) {
						player.varps()[Varp.SPECIAL_ENABLED] = 0
						break
					}*/
					
					if (!player.timers().has(TimerKey.COMBAT_ATTACK) || gmaul_spec) {
						if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_MELEE)) {
							player.message("Melee is disabled for this duel.")
							break
						}
						if (ClanWars.checkRule(player, CWSettings.MELEE)) {
							player.message("Melee is disabled for this clan war.")
							break
						}
						if (player.varps()[Varp.SPECIAL_ENABLED] != 0) {
							if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_SPK_ATK)) {
								player.message("Special attacks are disabled for this duel.")
								break
							}
							if (ClanWars.checkRule(player, CWSettings.SPECIAL_ATTACKS, 2)) {
								player.message("Special attacks are disabled for this clan war.")
								break
							}
						}
						
						//Att acking Aviansie with melee.
						if (target.isNpc) {
							val npctarg: Npc = target as Npc
							val id = npctarg.id()
							
							if (id == 3166 || id == 3167 || id == 3168 || id == 3169 || id == 3170 || id == 3171
									|| id == 3172 || id == 3173 || id == 3174 || id == 3175 || id == 3176 || id == 3177
									|| id == 3178 || id == 3179 || id == 3180 || id == 3181 || id == 3182 || id == 3183) {
								player.message("The Aviansie is flying too high for you to attack using melee.")
								break
							} else if (id >= 3162 && id <= 3165) {
								player.message("It's flying too high for you to attack using melee.")
								break
							}
						}
						
						if (weaponId == 21015) { // Dinh's Bulwark can only be used with the first attack style.
							if (player.varps().varp(Varp.ATTACK_STYLE) != 0) {
								player.message("Your bulwark gets in the way.")
								break
							}
						}
						
						// Checks done, start attacking.
						Equipment.checkTargetVenomGear(player, target)
						target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
						target.putattrib(AttributeKey.LAST_DAMAGER, player)
						player.putattrib(AttributeKey.LAST_ATTACK_TIME, System.currentTimeMillis())
						player.putattrib(AttributeKey.LAST_TARGET, target)
						if (target.isPlayer) {
							val targ = target as Player
							targ.interfaces().closeMain()
							targ.interfaces().close(162, 550) // Close chatbox
						}
						player.interfaces().closeMain()
						player.interfaces().close(162, 550) // Close chatbox
						if (weaponId == 13091) {
							player.equipment().remove(Item(13091), true, EquipSlot.WEAPON)
							player.equipment().add(Item(13092), true, EquipSlot.WEAPON)
							player.message("Your crystal halberd has degraded!")
						}
						if (weaponId == 13080) {
							player.equipment().remove(Item(13080), true, EquipSlot.WEAPON)
							player.equipment().add(Item(13081), true, EquipSlot.WEAPON)
							player.message("Your crystal halberd has degraded!")
						}

						val chargedWeaponSpec = weaponId in arrayOf(TheaterOfBloodItems.SCYTHE_OF_VITUR_CHARGED_ID)
						val not_spec = player.varps()[Varp.SPECIAL_ENABLED] == 0 && !chargedWeaponSpec
						val did_spec = !not_spec && MeleeSpecialAttacks.doSpecial(it, target)

						if (not_spec || !did_spec) {
							var success = AccuracyFormula.doesHit(player, target, CombatStyle.MELEE, 1.0) || player.hasAttrib(AttributeKey.ALWAYS_HIT)
							var veracEffect: Boolean = false
							
							// Verac mate
							if (BarrowsSets.hasVerac(player) && player.world().rollDie(100, 25)) {
								success = true
								veracEffect = true
								target.graphic(1041, 92, 0)
							}
							
							if (target.isNpc) {
								val npc = target as Npc
								if (npc.id() == 2668) {
									attackTarget(npc, player, weaponId, CombatStyle.MELEE, CombatFormula.maximumMeleeHit(player), 1)
									continue
								}
							}
							
							if (success) {
								var max = CombatFormula.maximumMeleeHit(player).toDouble()
								max *= MeleeSpecialAttacks.multi(player, target, CombatStyle.MELEE, false)
								
								val damage = player.attribOr<Int>(AttributeKey.ALWAYS_HIT, player.world().random(max.toInt()))
								
								// Use hit() rather than hitpvp() because veracEffect means protection prayers do not always apply.
								val hit: Hit = target.hit(player, damage, 1, false).combatStyle(CombatStyle.MELEE).pidAdjust().block(false)
								if (!veracEffect) {
									hit.applyProtection() // Verac doesn't take prayer into consideration!
								}
								// Finalize hit.
								hit.submit().addXp()
								
								// Do block anim
								target.blockHit(hit)
								
								// Pls heal if guthan ty!
								if (BarrowsSets.hasGuthan(player) && player.world().rollDie(100, 25)) {
									// Ammy of damned allows healing 10HP above base HP level
									player.heal(damage, if (Equipment.hasAmmyOfDamned(player)) 10 else 0)
									target.graphic(398, 92, 0)
								}
								
								// Abyssal tent and dds have 25% chance to poison. Poison strength varies.
								if (weaponId == 12006 && player.world().rollDie(100, 25)) {
									target.poison(4)
								} else if ((weaponId == 5698 || weaponId == 13271) && player.world().rollDie(100, 25)) {
									target.poison(6)
								}
							} else {
								val hit = target.hitpvp(player, 0, 1, CombatStyle.MELEE).block(false).submit() // Uh-oh, that's a miss.
								target.blockHit(hit)
							}
							it.animate(EquipmentInfo.attackAnimationFor(player))
							it.player().world().spawnSound(it.player().tile(), CombatSounds.weapon_attack_sounds(it), 0, 10)
							player.timers()[TimerKey.COMBAT_ATTACK] = player.world().equipmentInfo().weaponSpeed(weaponId)
							player.timers()[TimerKey.IN_COMBAT] = player.world().equipmentInfo().weaponSpeed(weaponId)
						}
						
						//Check for skulling context.
						Skulling.skull(player, target)
						
						// After every attack, reset the special mode.
						player.varps()[Varp.SPECIAL_ENABLED] = 0
						if (did_spec) {
							PlayerCombat.check_spec_and_tele(player, target)
						}
						
					}
				}
			}

			if (it.player().attribOr<Boolean>(AttributeKey.ENDCB_POSTSPEC, false)) {
				it.player().clearattrib(AttributeKey.ENDCB_POSTSPEC)
				break
			}
			//player.message("%s attacking Targ %s [ded %s | me %s] cycle %d", player.name(), (target as Player).name(), target.dead(), player.dead(), player.world().gameCycles)
			//System.out.println(String.format("%s attacking Targ %s [ded %s | me %s] cycle %d", player.name(), (target as Player).name(), target.dead(), player.dead(), player.world().gameCycles))
			it.delay(1)
			target = ref.get()
		}
		
		// At the end of combat, face nobody. Not even yourself.
		it.delay(1)
		player.face(null)
	}
	
	/**
	 * Upon using a special attack, block teleporting for up to 12 seconds after that battle started.
	 */
	fun check_spec_and_tele(player: Player, target: Entity) {
		if (player.world().realm().isPVP && target.isPlayer) {
			if (VarbitAttributes.varbiton(target as Player, 19) && WildernessLevelIndicator.wildernessLevel(target.tile()) >= 8) {
				val historyMap: MutableMap<Int, Int> = player.attribOr<MutableMap<Int, Int>>(AttributeKey.PVP_WILDY_AGGRESSION_TRACKER, HashMap<Int, Int>())
				val agroTick = historyMap.getOrDefault(target.id() as Int, player.world().cycleCount())
				val dif = player.world().cycleCount() - agroTick
				if (dif < 20) {
					player.timers().extendOrRegister(TimerKey.BLOCK_SPEC_AND_TELE, 20 - dif)
				}
			}
		}
	}
	
	@JvmStatic fun wearingAvasEffect(player: Player): Boolean {
		val cape: Int = player.equipment()[EquipSlot.CAPE]?.id() ?: -1
		return cape in intArrayOf(10498, 10499, 13337, 9756, 9757, CapeOfCompletion.RANGED.trimmed,
				CapeOfCompletion.RANGED.untrimmed, 22109, 21898)
	}
	
	@JvmStatic fun notAvas(player: Player): Boolean {
		val cape = player.equipment().get(EquipSlot.CAPE)
		return Math.random() <= 0.2 || cape == null || !wearingAvasEffect(player)
	}
	
	private fun multi_target_chinchompas(it: Script, primary_target: Entity, mainhit: Hit, delay: Int, success: Boolean) {
		val player = it.player()
		val targets = mutableListOf<Entity>()
		if (primary_target.isPlayer) {
			it.player().world().players().forEachInAreaKt(primary_target.tile().area(1), { t ->
				if (t.varps().varbit(Varbit.MULTIWAY_AREA) == 1) {
					targets.add(t)
				}
			})
		} else {
			it.player().world().npcs().forEachInAreaKt(primary_target.tile().area(1), { t ->
				if (MultiwayCombat.includes(t)) targets.add(t)
			})
		}
		
		for (targ: Entity in targets) {
			if (targ == primary_target || targ == player) {
				//dont hit us, or the target we've already hit
				continue
			}
			if (targ.isNpc) {
				val n = targ as Npc
				
				if (n.id() == InfernoContext.PILLAR_INVIS || n.id() == InfernoContext.PILLAR_VISIBLE) {
					continue
				}
			}
			if (!canAttack(player, targ)) { // Validate they're in an attackable location
				continue
			}
			val hit: Hit = targ.hitpvp(player, player.world().random(mainhit.damage()), delay, CombatStyle.RANGE).graphic(Graphic(157, 100, 0))
			if (!success)
				hit.adjustDmg(0)
			hit.submit().addXp()
			
			targ.putattrib(AttributeKey.LAST_DAMAGER, player)
			targ.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
			player.putattrib(AttributeKey.LAST_TARGET, targ)
			targ.graphic(-1)
			
			Equipment.checkTargetVenomGear(player, targ)
			// Exact weapon id not required, just needs to be a form of chinchompa.
			blockAnim(targ, delayForBlock(10033, WeaponType.THROWN, player.tile().distance(targ.tile())))
		}
		targets.clear()
	}
	
	// The hitmark delay without pid. If pid, it gets adjusted elsewhere.
	// https://i.gyazo.com/d84b1fb9969e3166ff5abf2978e77b4d.png
	@JvmStatic fun calcHitDelay(weaponId: Int, weaponType: Int, dist: Int): Int {
		if (ballista(weaponId))
			return if (dist <= 4) 2 else 3
		if (weaponId == 12926 || weaponType == WeaponType.CHINCHOMPA)   // Blowpipe / chins longer range throwning weps
			return if (dist <= 5) 2 else 3
		return when (weaponType) {
			WeaponType.THROWN -> 2 // darts / knives with max dist 4
			else -> if (dist <= 2) 2 else if (dist <= 8) 3 else 4 // shortbow (and darkbow), longbow, karils xbow, crystal bow, crossbows
		}
	}
	
	@JvmStatic fun blockAnim(target: Entity, delayForBlock: Int) {
		//System.out.printf("Delay: %d%n", delayForBlock)
		if (target.isNpc) {
			if (!(target as Npc).sync().hasFlag(NpcSyncInfo.Flag.ANIMATION.value)) {
				if (target.combatInfo() != null) {
					if (target.combatInfo().animations.block > 0)
						target.animate(target.combatInfo().animations.block, delayForBlock)
				} else {
					target.animate(424, delayForBlock)
				}
			}
		} else if (target.isPlayer) {
			if (!(target as Player).sync().hasFlag(PlayerSyncInfo.Flag.ANIMATION.value)) {
				target.animate(EquipmentInfo.blockAnimationFor(target), delayForBlock)
			}
		}
	}
	
	@JvmStatic fun delayForBlock(weaponId: Int, weaponType: Int, dist: Int): Int {
		var delay = when (weaponType) { // Initial delay
			WeaponType.THROWN, WeaponType.CHINCHOMPA -> 32
			WeaponType.BOW, WeaponType.CROSSBOW -> 46
			else -> 46
		}
		val dbow = darkbow(weaponId)
		if (dbow)
			delay = 55
		val pt = if (dbow) 10 else if (ballista(weaponId)) 3 else 5 // delay per tile between opponents
		return delay + (Math.max(1, dist) * pt) // dist -1 cos initial delay is 1*pt larger than it should be
	}
	
	@JvmStatic fun attackTarget(target: Npc, player: Player, weaponId: Int, combat: CombatStyle, damage: Int, delay: Int) {
		val damage = player.attribOr<Int>(AttributeKey.ALWAYS_HIT, damage)
		val hit: Hit = target.hit(player, damage, delay, false).combatStyle(combat).pidAdjust().block(true)
		hit.submit()
		player.animate(EquipmentInfo.attackAnimationFor(player))
		player.timers()[TimerKey.COMBAT_ATTACK] = player.world().equipmentInfo().weaponSpeed(weaponId)
		player.timers()[TimerKey.IN_COMBAT] = player.world().equipmentInfo().weaponSpeed(weaponId)
		player.varps()[Varp.SPECIAL_ENABLED] = 0
	}
	
	@JvmStatic fun ballista(weaponId: Int): Boolean {
		return weaponId in intArrayOf(19478, 19481)
	}
	
	// Feature enabled?
	@JvmStatic var CUSTOM_RANGE_ACCURACY_MODIFIER_ENABLED = true
	
	// At what value of range attack should the negatively affecting mechanic apply
	@JvmStatic var BONUS_THRESHOLD = 0
	
	// Read within methods for info
	fun getPvpRangeAccuracyModifier(critBonuses: EquipmentInfo.Bonuses): Double {
		if (!CUSTOM_RANGE_ACCURACY_MODIFIER_ENABLED) {
			return 1.0
		}
		val rangeOffensive = critBonuses.range
		// A negative offensive bonus from the most important items means you're not very clever
		// you're using the wrong combat style gear which is negating your accuracy.
		return if (rangeOffensive < BONUS_THRESHOLD) 0.75 else 1.0
	}
	
	// TODO unfinished mechanic @Jak
	@JvmStatic var RANGE_DPS_WHEEL_ENABLED = false
	
	@JvmStatic fun darkbow(itemid: Int): Boolean {
		return itemid == 11235 || (itemid >= 12765 && itemid <= 12768)
	}

	@JvmStatic fun dragonSpearVictim(attacker: Entity, target: Entity): Direction? {
		var start = attacker.tile()
		if (start.equals(target.tile())) {
			start = attacker.pathQueue().peekNextTile()
			attacker.message("same tile so new tile: %s", start)
		}
		val dx = start.x - target.tile().x
		val dz = start.z - target.tile().z
		if (dx == 0 && dz == 0) // no more available
			return null
		// Move the player in a certain direction.
		val finder = PathRouteFinder(target)
		val dir = Direction.diagonal(dx, dz).opposite()
		if (finder.legalProjectileStep(target.world(), target.tile(), dir)) {
			return dir
		}
		return null
	}

	@JvmStatic fun dragonSpearMovement(dir: Direction, victim: Entity) {
		victim.graphic(254, 100, 0)
		//victim.message("Speared in dir $dir to ${dir.x} and ${dir.y}")
		victim.putattrib(AttributeKey.IGNORE_FREEZE_MOVE, true)
		victim.walkTo(victim.tile().transform(dir.x, dir.y, 0), PathQueue.StepType.FORCED_WALK)
		victim.clearattrib(AttributeKey.IGNORE_FREEZE_MOVE)
	}


	fun attackRange(player: Player, weaponId: Int): Int {
		val long_range = EquipmentInfo.attackStyleFor(player) == AttackStyle.LONG_RANGE
		
		val composite_bows = intArrayOf(10280, 10282, 10284, 4827)
		val long_bows = intArrayOf(839, 845, 847, 851, 855, 859)
		val cystal_bows = IntRange(4214, 4223)
		
		var attackRange = 7 //Shortbows & Regular Crossbows
		
		if (composite_bows.contains(weaponId))
			attackRange = 10
		if (long_bows.contains(weaponId))
			attackRange = 9
		if (cystal_bows.contains(weaponId))
			attackRange = 10
		if (weaponId == 20997) //twisted bow
			attackRange = 13
		if (weaponId == 11785)
			attackRange = 8 //armadyl crossbow
		if (IntRange(5654, 5667).contains(weaponId) || IntRange(863, 876).contains(weaponId))
			attackRange = 4 //knifes
		if (IntRange(806, 817).contains(weaponId) || IntRange(5628, 5641).contains(weaponId) || weaponId == 3093 || weaponId == 3094)
			attackRange = 3 //darts
		if (weaponId == 4212)
			attackRange = 13 //crystal bow
		if (PlayerCombat.darkbow(weaponId))
			attackRange = 9 //dark bow
		if (weaponId == 10033 || weaponId == 10034 || weaponId == 11959)
			attackRange = 9 //chinchompas
		if (IntRange(825, 836).contains(weaponId) || IntRange(5642, 5653).contains(weaponId))
			attackRange = 5 //javelins
		if (IntRange(800, 805).contains(weaponId) || weaponId == 20849)
			attackRange = 4 //thrown axes
		if (weaponId == 6724)
			attackRange = 8 //seercull bow
		if (weaponId == 8880)
			attackRange = 6 //dorgeshuun c'bow
		if (weaponId == 4827)
			attackRange = 5 //comp ogre bow
		
		if (attackRange > 1 && EquipmentInfo.attackStyleFor(player) == AttackStyle.LONG_RANGE) // Check if selected style is long range, increasing the distance.
			attackRange += 2
		
		if (attackRange > 10 && weaponId != 20997) // Maximum distance of a distance-based attack is 10 tiles
			attackRange = 10
		return attackRange
	}
	
	/**
	 * Returns delay before you can attack again.
	 */
	fun rangeAttackSpeed(player: Player): Int {
		val weapon: Item = player.equipment()[3] ?: return 4
		val weaponId = weapon.id()
		var speed = player.world().equipmentInfo().weaponSpeed(weaponId)
		if (EquipmentInfo.attackStyleFor(player) == AttackStyle.RAPID)
			speed -= 1
		return speed
	}
	
	
	fun attemptSkull(player: Player, target: Entity) {
		/*// Vs players only..
		if (target !is Player)
			return
		// You can't skull in stakes
		if (player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			return
		}
		// Skulling does not work in FFA clan wars.
		if (FFAClanWars.inFFAMap(player)) {
			return
		}
		
		// No skulls in Clan Wars
		if (ClanWars.inInstance(player)) {
			return
		}
		trackPvpAggression(player, target as Player)
		
		val mySkullHistoryMap: MutableMap<Int, Long> = getFreshSkullmap(player)
		val oppSkullHistoryMap: MutableMap<Int, Long> = getFreshSkullmap(target)
		val skull_removed_time: Long = java.lang.Long.parseLong(player.attribOr<String>(AttributeKey.SKULL_REMOVED_TIME, "0"))
		val should_reskull_because_skull_removed = target.id() as Int in mySkullHistoryMap && skull_removed_time > mySkullHistoryMap.get(target.id() as Int) as Long
		
		*//*if (player.attribOr(AttributeKey.DEBUG, false))
			player.debug("<col=FF0000>Skulling info v1: %s, %s, %s, me died:%ss, last hit targ:%ss", player.id() in oppSkullHistoryMap, player.attribOr<Int>(AttributeKey.SKULL, 0),
					target.id() as Int in mySkullHistoryMap, skull_removed_time/36000, mySkullHistoryMap.getOrDefault(target.id() as Int, 0L)/36000)*//*
		
		// We're not skulled, but opp has us in their map. they skulled.
		if (player.id() in oppSkullHistoryMap && player.attribOr<Int>(AttributeKey.SKULL, 0) == 0) {
			
			// If you died after you skulled on them originally, you'll reskull.
			if (!should_reskull_because_skull_removed) {
				// Your attack on that person IS still recorded though.
				// This can be tested as such p2 skulls on you, p2 skulls disappear, you won't instantly reskull because you've hit them previously within 20mins.
				mySkullHistoryMap.put(target.id() as Int, System.currentTimeMillis())
				//player.debug("not skullin")
				return
			}
		}
		
		// On Deadman, you don't skull if the other person is skulled (reguardless of who attacked who) HOWEVER, DO store that you hit them.
		// This means target skull can disappear, you won't suddenly skull. Two people can actually bank fight in safezones forever.
		if (player.world().realm().isDeadman && target.attribOr<Int>(AttributeKey.SKULL, 0) != 0) {
			// Your attack on that person IS still recorded though.
			mySkullHistoryMap.put(target.id() as Int, System.currentTimeMillis())
			return
		}
		
		
		// We're now going to skull.
		val we_attacked_time = mySkullHistoryMap.getOrElse(target.id() as Int, { 0L })
		val target_last_died = java.lang.Long.parseLong(target.attribOr<String>(AttributeKey.LAST_TIME_DEATH, "0"))
		val target_has_died_since_we_last_attacked = we_attacked_time != 0L && we_attacked_time < target_last_died
		
		// Debug
		//if (player.attribOr(AttributeKey.DEBUG, false))
		//    player.debug("<col=FF0000>Skulling info: Last hit '%s' @ %d me died:%ss  Died: %ds ~ died since: %s", target.name(), we_attacked_time, skull_removed_time/3600000, target_last_died/3600000, target_has_died_since_we_last_attacked)
		
		// We've not hit this person before. They haven't hit us.(checked above) Skull reset to disappear 20 minutes.
		if (should_reskull_because_skull_removed || !mySkullHistoryMap.containsKey(target.id() as Int) || target_has_died_since_we_last_attacked) {
			player.putattrib(AttributeKey.SKULL, 200)
			player.looks().update()
			player.debug("Skulled!")
			
			if (player.world().realm().isDeadman) {
				player.varps().varbit(Varbit.DEADMAN_SKULL_TIMER, Math.min(30, player.varps().varbit(Varbit.DEADMAN_SKULL_TIMER) + 30)) // Set max as 15 mins
				if (!player.timers().has(TimerKey.DEADMAN_SKULL_TIMER)) {
					player.timers().extendOrRegister(TimerKey.DEADMAN_SKULL_TIMER, 50)
				}
				player.message("Your Deadman Skull timer has been <col=FF0000>reset</col> to 15 minutes for skulling.")
				DeadmanMechanics.triggerGuards(player)
			}
		}
		// Record when we last hit the target player.
		mySkullHistoryMap.put(target.id() as Int, System.currentTimeMillis())*/
	}
	
	/**
	 * Records the game tick of when you attacked that target. Used for anti-rag mechanics.
	 * Should always be called for any PvP attack - sits next to skulling code nicely.
	 */
	fun trackPvpAggression(player: Player, target: Player) {
		if (player.world().realm().isPVP) {
			val historyMap: MutableMap<Int, Int> = player.attribOr<MutableMap<Int, Int>>(AttributeKey.PVP_WILDY_AGGRESSION_TRACKER, HashMap<Int, Int>())
			val targetEnterWildTick = target.attribOr<Int>(AttributeKey.INWILD, 0)
			val lastAgroToTarget = historyMap.getOrDefault(target.id() as Int, -1)
			// Either not tracked yet or they left the wildy and re-entered, so we'll start tracking this new battle from after they returned to the wild.
			if (lastAgroToTarget == -1 || (lastAgroToTarget != -1 && targetEnterWildTick > lastAgroToTarget)) {
				historyMap.put(target.id() as Int, player.world().cycleCount())
				player.debug("Recorded battle start vs ${target.name()} on tick ${player.world().cycleCount()}.")
			}
			player.putattrib(AttributeKey.PVP_WILDY_AGGRESSION_TRACKER, historyMap)
		}
	}
	
	fun moveCloser(player: Player, target: Entity, tile: Tile): Tile {
		val steps = if (player.pathQueue().running()) 2 else 1
		val otherSteps = if (target.pathQueue().running()) 2 else 1
		
		val otherTile = target.pathQueue().peekAfter(otherSteps)?.toTile() ?: target.tile()
		player.stepTowards(target, otherTile, 25)
		return player.pathQueue().peekAfter(steps - 1)?.toTile() ?: tile
	}
	
	fun moveCloserNoPeek(player: Player, target: Entity, tile: Tile): Tile {
		val steps = if (player.pathQueue().running()) 2 else 1
		
		val otherTile = target.tile()
		player.stepTowards(target, otherTile, 25)
		return player.pathQueue().peekAfter(steps - 1)?.toTile() ?: tile
	}
	
	fun takeSpecialEnergy(player: Player, num: Int): Boolean {
		if (VarbitAttributes.varbiton(player, Varbit.INFSPEC)) {
			//infspec enabled
			return true
		}
		if (player.varps()[Varp.SPECIAL_ENERGY] < num * 10) {
			player.message("You don't have enough power left.")
			return false
		}
		player.varps()[Varp.SPECIAL_ENERGY] = player.varps()[Varp.SPECIAL_ENERGY] - num * 10
		return true
	}
	
	fun ballistaProjectileFor(ammo: Int): Int {
		return when (ammo) {
			825 -> 200 // Bronze
			826 -> 201 // Iron
			827 -> 202 // Steel
			828 -> 203 // Mithril
			829 -> 204 // Adamant
			830 -> 205 // Rune
			19484 -> 1301 // Dragon
			else -> 1301
		}
	}
	
	@JvmStatic fun canAttack(entity: Entity, other: Entity): Boolean {
		return canAttack(entity, other, true)
	}
	
	// Does not stop combat, only sends a message if an entity is unable to attack another entity. Reasons include slayer level, invisibility, and single-combat restrictions.
	@JvmStatic fun canAttack(entity: Entity, other: Entity, message: Boolean = true): Boolean {
		if (entity.index() == -1 || other.index() == -1) { // Target logged off.
			return false
		}
		if (entity.stunned()) {
			// Calling stun interrupts combat, but this will force stop it too.
			return false
		}
		if (other.tile().level != entity.tile().level) {
			return false
		}
		if (other.isNpc()) {
			val npc = other as Npc
			if (npc.hidden() || (entity.isPlayer && npc.id() == 7707) || npc.locked() && npc.id() != 5886 && npc.id() != 2668)
				return false
			if (npc.id() == 2668) // you can always attack combat dummies
				return true
		}
		
		var wep = -1
		if (entity.isPlayer) {
			wep = (entity as Player).equipment()[3]?.id() ?: -1
		}
		if (other.isPlayer) {
			// Can't attack invis
			val them = other as Player
			if (them.looks().hidden()) {
				return false
			}
			
			// This check for being in an attackable zone has no message because this check is done elsewhere
			// Which gives a messages in those other places.
			// This check if for multi-target attacks like barrage/burst/chins where extra targets have to be checked
			// For combat validity.
			if (entity.isPlayer && !WildernessLevelIndicator.inAttackableArea(them)) {
				// Do NOT send a message here.
				if (entity.world().realm().isDeadman) {
					if (WildernessLevelIndicator.isTutorialIsland(entity.tile().region())) {
						if (message)
							entity.message("The guards wouldn't be very pleased if you did that here.")
						
						return false
					}
				} else {
					return false
				}
			}
			
			// Kraken attacking Players.
			if (entity.isNpc) {
				if ((entity as Npc).id() in intArrayOf(KrakenBoss.KRAKEN_NPCID, KrakenBoss.TENTACLE_NPCID, KrakenBoss.TENTACLE_NPCID, KrakenBoss.TENTACLE_WHIRLPOOL)) {
					return true
				}
			}
			
			// Disable attacking your own people in Clan Wars.
			if (ClanWars.inInstance(entity) && ClanWars.inInstance(other)) {
				if (ClanChat.currentId(entity as Player) == ClanChat.currentId(other)) {
					if (message)
						entity.message("That player is on your team!")
					
					return false
				}
			}
		} else if (other.isNpc) {
			if ((other as Npc).combatInfo() == null) {
				if (message)
					entity.message("Without combat attributes this monster is unattackable.")
				
				return false
			} else if (other.combatInfo().unattackable) {
				if (message)
					entity.message("You cannot attack this monster.")
				
				return false
			}
			
			//If we're attacking the Abyssal Sire and it's currently disoriented..
			if (other.id() == 5886) {
				val combatState = other.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.DISORIENTED)
				
				if (combatState == AbyssalSireState.DISORIENTED) {
					if (message)
						entity.message("The sire is disoriented. Maybe you can do something useful while it's unable to control the tentacles.")
					
					return false
				}
			}
			
		}
		
		val targetLastVictim = other.attribOr<Entity>(AttributeKey.LAST_TARGET, null)
		
		// The last time our target was attacked
		val targetLastAttackedTime = System.currentTimeMillis() - other.attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0.toLong())
		
		if (entity.isPlayer && other.isNpc) {
			val oppNpc = other as Npc
			if (!entity.world().realm().isPVP && oppNpc.combatInfo() != null) {
				val slayerReq = Math.max(SlayerCreature.slayerReq(other.id()), other.combatInfo().slayerlvl)
				if (slayerReq > (entity as Player).skills().level(Skills.SLAYER)) {
					if (message)
						entity.message("You need a slayer level of $slayerReq to harm this NPC.")
					
					return false
				}
			}
			
			if (wep == 10501) {
				entity.message("You can only pelt other players with a snowball.")
				return false
			} else if (other.id() == 5914) {
				val respiratoryState = other.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.STASIS)
				
				if (respiratoryState == AbyssalSireState.STASIS) {
					if (message)
						entity.message("I can't reach that!")
					return false
				}
			}
			
			// The kraken boss already has a focus. Multi doesn't matter now.
			if (other.id() == KrakenBoss.KRAKEN_WHIRLPOOL && other.sync().transmog() == KrakenBoss.KRAKEN_NPCID) {
				if (other.attribOr<WeakReference<Entity>>(AttributeKey.TARGET, WeakReference(null)).get() != entity && targetLastAttackedTime < 10000L) {
					if (message)
						entity.message("The Kraken already has a target.")
					return false
				}
			}
		}
		
		// Last person to hit our target.
		val targetLastAttacker = other.attrib<Entity>(AttributeKey.LAST_DAMAGER)
		
		// Last time we were attacked
		val myLastAttackedTime = System.currentTimeMillis() - entity.attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0.toLong())
		
		// Last person to hit us.
		val myLastAttacker = entity.attrib<Entity>(AttributeKey.LAST_DAMAGER)
		
		// Last time our target attacked something.
		val targetLastAttackTime = System.currentTimeMillis() - other.attribOr<Long>(AttributeKey.LAST_ATTACK_TIME, 0.toLong())
		
		// Our current wilderness level. Some mechanic apply only to 10+ wild (not to the edge rune noob pkers)
		val wildLvl = WildernessLevelIndicator.wildernessLevel(entity.tile())
		
		// Our risked amount
		val risked = if (entity.isNpc) 0 else (entity.attribOr<Int>(AttributeKey.RISKED_WEALTH, 0) +
				if (wildLvl >= 20) entity.attribOr<Int>(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0) else 0)
		
		// Opponent risked amount
		val oppRisked = if (other.isNpc) 0 else (other.attribOr<Int>(AttributeKey.RISKED_WEALTH, 0) +
				if (wildLvl >= 20) other.attribOr<Int>(AttributeKey.RISKED_WEALTH_UNTRADBLES_PROTECTED_UNDER20, 0) else 0)
		
		val inpvpinstances = WildernessLevelIndicator.inPvpInstanceAttackable(entity)
		var edge_or_canifis_pvp: Boolean = false
		var instance_key: InstancedMapIdentifier? = null
		entity.world().allocator().active(entity.tile()).ifPresent { map -> map.identifier.ifPresent { id -> instance_key = id } }
		
		if (instance_key == InstancedMapIdentifier.CANIFIS_PVP || instance_key == InstancedMapIdentifier.EDGE_PVP)
			edge_or_canifis_pvp = true

		val me_edgepk: Boolean = entity.world().realm().isPVP && GameCommands.edgeDitch10secondPjTimerEnabled && WildernessLevelIndicator.inside_extended_pj_timer_zone(entity.tile())
		val targ_edgepk: Boolean = entity.world().realm().isPVP && GameCommands.edgeDitch10secondPjTimerEnabled && WildernessLevelIndicator.inside_extended_pj_timer_zone(other.tile())

		val me_westsrag: Boolean = entity.world().realm().isPVP && GameCommands.westsAntiragEnabled && WildernessLevelIndicator.at_west_dragons(entity.tile())
		val targ_westsrag: Boolean = entity.world().realm().isPVP && GameCommands.westsAntiragEnabled && WildernessLevelIndicator.at_west_dragons(other.tile())

		var myTimeToPj: Long = if (me_edgepk || me_westsrag || (edge_or_canifis_pvp || instance_key == InstancedMapIdentifier.GE_PVP)) 10_000L else 4500L
		val areaPjTimer = pjTimerForArena(instance_key)
		if (areaPjTimer != 4500L)
			myTimeToPj = areaPjTimer
		var targTimeToPj: Long = if (targ_edgepk || targ_westsrag || (edge_or_canifis_pvp || instance_key == InstancedMapIdentifier.GE_PVP)) 10_000L else 4500L
		if (areaPjTimer != 4500L)
			targTimeToPj = areaPjTimer
		
		if (entity.isPlayer && other.isPlayer) {
			
			// Staking security
			if (Staking.not_in_area(entity, other, "You can't attack them.")) {
				return false
			}
			if (Staking.stake_not_started(entity, other)) {
				entity.message("The fight hasn't started yet!")
				return false
			}
			if (ClanWars.inInstance(entity) && ClanWars.inInstance(other)) {
				val war = ClanWars.get(ClanChat.currentId(entity as Player))
				if (war != null && !war.started()) {
					entity.message("The war hasn't started yet!")
					return false
				}
			}
			if (DuelAntiScams.bug_abused_rules(entity, other)) {
				return false
			}
			
			// PvP security
			if (inpvpinstances) {
				
				// In instances like Edge and canifis, people might rag each other like void vs pures, this make it pure vs pure only for <120 combats.
				if (GameCommands.DEFENCE_REQUIRED_ENABLED && edge_or_canifis_pvp && (entity.skills().combatLevel() < 120 && entity.skills().xpLevel(Skills.DEFENCE) != other.skills().xpLevel(Skills.DEFENCE))) {
					entity.message("When under 120 combat, your Defence level must be the same as your opponent.")
					return false
				}
				// There is a list of users who are allowed to attack other people in the tournament zone, if not, they can only spectate.
				if (EdgevilleTourny.insideTournament(entity) && !EdgevilleTourny.INVITED.contains((entity as Player).id())) {
					entity.message("You're not allowed to fight in here.")
					return false
				}
				
				if (!MultiwayCombat.includes(entity) && entity.varps().varbit(Varbit.MULTIWAY_AREA) == 0 && (edge_or_canifis_pvp || instance_key == InstancedMapIdentifier.GE_PVP)) {
					// The PvP world PJ mechanic. Stops you pjing AT ALL if someone is in combat, rather than the usual mechanic which is
					// you can PJ if they haven't been hit for approx 4s
					if ((targetLastAttackTime < targTimeToPj && entity != targetLastVictim) || targetLastAttackedTime < targTimeToPj && entity != targetLastAttacker) {
						entity.message("That player is fighting someone.")
						return false
					}
				}
			}
		}
		
		//If the NPC isn't visible we should no longer be attacking them.
		if (entity.isNpc) {
			val npc = entity as Npc
			if (npc.hidden()) {
				return false
			}
			if (other.isPlayer) {
				if ((other as Player).interfaces().activeMain() == 155) {
					// When viewing the barrows loot interface, NPCs are not aggressive. Interesting eh.
					return false
				}
			}
		}
		
		val inCwFFA = FFAClanWars.inFFAWilderness(entity)
		val inArena = Staking.in_duel(entity)
		val inClanWar = ClanWars.inInstance(entity)
		val opponentIsKeyHolder = other in BloodChest.playersWithKeys
		
		// Level checks only apply to PvP
		if (other.isPlayer && entity.isPlayer) {
			// Is the player deep enough in the wilderness?
			// FFA Clan wars does not make any checks for levels. Free for all :)
			if (!inCwFFA && !inClanWar && !inArena && !entity.world().realm().isDeadman && !opponentIsKeyHolder) {
				
				val oppWithinLvl: Boolean = entity.skills().combatLevel() >= getLowestLevel(other, entity) &&
						entity.skills().combatLevel() <= getHighestLevel(other, entity)
				
				if (!oppWithinLvl) {
					if (message)
						entity.message(if (!PVPAreas.inPVPArea(entity)) "Your level difference is too great! You need to move deeper into the Wilderness." else "Your level difference is too great.")
					return false
				} else {
					val withinLvl: Boolean = (other.skills().combatLevel() >= getLowestLevel(entity, other) &&
							other.skills().combatLevel() <= getHighestLevel(entity, other))
					if (!withinLvl) {
						if (message)
							entity.message(if (!PVPAreas.inPVPArea(entity)) "Your level difference is too great! You need to move deeper into the Wilderness." else "Your level difference is too great.")
						return false
					}
				}
			}
		}
		if (PVPAreas.inEdgePvp(other)) {
			if (other.timers().has(TimerKey.EDGEPVP_IMMUNITY)) {
				if (entity.isPlayer) {
					if (message) {
						entity.message("That player is temporarily <col=FF0000>immune</col> to attacks.")
					}
				}
				return false
			}
		}
		if (PVPAreas.inEdgePvp(entity) && entity.isPlayer && entity.timers().has(TimerKey.EDGEPVP_IMMUNITY)) {
			if (message) {
				entity.message("You're temporarily <col=FF0000>immune</col> to combat - during this period you can't fight.")
			}
			return false
		}
		if (other.timers().has(TimerKey.COMBAT_IMMUNITY)) {
			if (entity.isPlayer) {
				//if (!(entity as Player).privilege().eligibleTo(Privilege.ADMIN)) { // Used as a mechanic in GE PVP instance
				if (message)
					entity.message("That player is temporarily <col=FF0000>immune</col> to attacks.")
				return false
				//}
			} else {
				return false
			}
		}
		if (entity.timers().has(TimerKey.COMBAT_IMMUNITY) && entity.isPlayer && !(entity as Player).privilege().eligibleTo(Privilege.ADMIN)) {
			if (message)
				entity.message("You're temporarily <col=FF0000>immune</col> to combat - during this period you can't fight. To cancel this, type ::modpk")
			return false
		}
		
		if (other.isPlayer) {
			if (other.varps().varbit(Varbit.MULTIWAY_AREA) == 1 && MultiwayCombat.includes(other)) {
				if (!(other.tile().region() == 12190 && other.tile().level == 3)) // Height 3 in wildy gwd is single-way combat.
					return true
			}
		} else if (MultiwayCombat.includes(other)) {
			return true
		}
		
		// Note: DO NOT ADD anymore possibly false-returning statements below this multi check. Multi will have priority if true!

		val isOpponentDead = myLastAttacker == null || myLastAttacker.dead()
		
		if (myLastAttackedTime < myTimeToPj && myLastAttacker != null && myLastAttacker !== other && !isOpponentDead) {
			// Multiway check bro!
			if (entity.isPlayer) {
				if (entity.varps().varbit(Varbit.MULTIWAY_AREA) != 1 && !MultiwayCombat.includes(other) && !opponentIsKeyHolder) {
					if (message)
						entity.message("You're already under attack.")
					return false
				}
			} else {
				if (!MultiwayCombat.includes(entity) && !opponentIsKeyHolder) {
					if (message)
						entity.message("I'm already under attack.")
					return false
				}
			}
		}

		if (targetLastAttackedTime < targTimeToPj && targetLastAttacker != null && targetLastAttacker !== entity) {
			// Multiway check bro!
			if (other.isPlayer) {
				if (other.varps().varbit(Varbit.MULTIWAY_AREA) != 1 && !MultiwayCombat.includes(other) && !opponentIsKeyHolder) {
					if (message)
						entity.message("Someone else is already fighting your opponent.")
					return false
				}
			} else {
				if (!MultiwayCombat.includes(other)) {
					if (message)
						entity.message("Someone else is fighting that.")
					return false
				}
			}
		}

		// Update our carried wealth
		if(entity.isPlayer && other.isPlayer) {
			updateCarriedWealthProtection(entity as Player)
			Risk.INSTANCE.send(entity)
		}

		// Check to ensure risk is not less than risk protection
		if (entity is Player) {
			RiskProtectionState.monitorRiskProtection(entity as Player)
		}
		
		// The following check is OK to be after the multi-check - because it only applies in single-way combat.
		if (entity.world().realm().isPVP && entity.isPlayer && other.isPlayer &&
				wildLvl > 0 && !inArena && !inCwFFA && !inClanWar
				&& GameCommands.RISK_PROTECTION_ENABLED
				&& !other.tile().inArea(Area(3030, 3523, 3114, 3554))) {

			// Threshold for who can be attacked
			val targetProtection = RiskProtectionState.protectionValue(other as Player)
			val playerProtection = RiskProtectionState.protectionValue(entity as Player)

			// Does our players target have protection value?
			if(risked < targetProtection) {
				entity.message(Color.DARK_RED.wrap("This player is currently under risk protection. You must risk an additional" +
						" ${ NumberFormat.getInstance().format(targetProtection - risked)} blood money in order to attack them."))
				return false
			}

			// If our players target is risking less than our players risk protection
			if(playerProtection > oppRisked) {
				entity.putattrib(AttributeKey.RISK_PROT_TIER, 0)
				entity.timers().extendOrRegister(TimerKey.RISK_PROTECTION, GameCommands.RISK_PROTECTION_EXPIRE_TICKS) // 20 minutes
				entity.message(Color.DARK_RED.wrap("By attacking this player you forfeit your risk protection for %d minutes."), GameCommands.RISK_PROTECTION_EXPIRE_TICKS / 100)
				RiskProtectionState.INSTANCE.send(entity)
			}
		}
		return true
	}

	fun pjTimerForArena(key: InstancedMapIdentifier?): Long {
		return when (key) {
			null -> 4500L
			InstancedMapIdentifier.TOURNY_PLATEAU -> EdgevilleTourny.PJ_TIMER_LENGTH[1]
			InstancedMapIdentifier.TOURNY_GLADE -> EdgevilleTourny.PJ_TIMER_LENGTH[2]
			InstancedMapIdentifier.TOURNY_EDGE -> EdgevilleTourny.PJ_TIMER_LENGTH[0]
			InstancedMapIdentifier.TOURNY_TURRENTS -> EdgevilleTourny.PJ_TIMER_LENGTH[3]
			else -> 4500L
		}
	}
	
	@JvmStatic val GWD_ROOMS = arrayOf(Kreearra.ENCAMPMENT, Zilyana.ENCAMPMENT, Kril.ENCAMPMENT, Graardor.BANDOS_AREA)
	
	@JvmStatic fun bothInFixedRoom(entity: Entity, other: Entity): Boolean {
		for (area in GWD_ROOMS) {
			if (area.contains(entity) && !area.contains(other) || !area.contains(entity) && area.contains(other)) {
				return false
			}
		}
		return true
	}
	
	// No longer used
	@JvmStatic fun inFixedRoom(entity: Entity): Boolean {
		// Only worth checking if we're in a GWD region id. Performance update per 8/13/2016.
		val regionId = entity.tile().region()
		if (regionId == 11602 || regionId == 11603 || regionId == 11346 || regionId == 11347)
			return false
		
		for (area in GWD_ROOMS) {
			if (area.contains(entity)) {
				return true
			}
		}
		return false
	}
	
	val GWD_ROOM_NPCIDS = intArrayOf(3165, 3163, 3164, 3162,
			2215, 2216, 2217, 2218,
			3129, 3130, 3132, 3131,
			2206, 2207, 2208, 2205)
	
	@JvmStatic fun gwdRoomNpc(npc: Npc): Boolean {
		return npc.id() in GWD_ROOM_NPCIDS
	}
	
	fun getLowestLevel(entity: Entity, target: Entity): Int {
		val combat: Int = entity.skills().combatLevel()
		val wilderness: Int = WildernessLevelIndicator.wildernessLevel(entity.tile())
		var min: Int = combat - wilderness
		if (PVPAreas.inPVPArea(entity) && PVPAreas.inPVPArea(target)) {
			min = combat - 15 // 15 of our own level.
		}
		if (min < 3) {
			min = 3
		}
		return min
	}
	
	fun getHighestLevel(entity: Entity, target: Entity): Int {
		val combat: Int = entity.skills().combatLevel()
		val wilderness: Int = WildernessLevelIndicator.wildernessLevel(entity.tile())
		var max: Int = combat + wilderness
		if (PVPAreas.inPVPArea(entity) && PVPAreas.inPVPArea(target)) {
			max = combat + 15 // 15 of our own level.
		}
		if (max > 126) {
			max = 126
		}
		return max
	}
	
	@JvmStatic fun addCombatXp(player: Player, target: Entity, hit: Int, style: CombatStyle) {
		addCombatXp(player, target, hit, style, EquipmentInfo.xpModeFor(player))
	}
	
	fun addCombatXp(player: Player, target: Entity, hitDamage: Int, style: CombatStyle, mode: AttackMode) {
		var hit = hitDamage
		// Rather than putting hooks into every style of attacking a target, centralize it and stop giving XP
		// when certain styles can't damage an opponent.
		
		if (target.isNpc) {
			val ntarg = target as Npc
			val id = ntarg.id()
			if (id == 5534) hit = 0 // Can't damage tentacles much
			if (id == 496) {
				if (ntarg.sync().transmog() != 494) // Whirlpool as kraken
					hit = 0
				else if (style != CombatStyle.MAGIC)
					hit = 0
			}
			if (id == 319) { // Corp beast
				player.varps().varp(Varp.CORP_BEAST_DAMAGE, player.varps().varp(Varp.CORP_BEAST_DAMAGE) + hitDamage)
			}
		}
		
		when (style) {
			CombatStyle.MELEE -> {
				when (mode) {
					AttackMode.ATTACK -> {
						player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
						player.skills().__addXp(Skills.ATTACK, (hit * 4).toDouble(), target !is Player)
					}
					AttackMode.STRENGTH -> {
						player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
						player.skills().__addXp(Skills.STRENGTH, (hit * 4).toDouble(), target !is Player)
					}
					AttackMode.DEFENCE -> {
						player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
						player.skills().__addXp(Skills.DEFENCE, (hit * 4).toDouble(), target !is Player)
					}
					AttackMode.SHARED -> {
						val xp: Double = (hit * 4).toDouble()
						player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
						player.skills().__addXp(Skills.ATTACK, xp / 3, target !is Player)
						player.skills().__addXp(Skills.STRENGTH, xp / 3, target !is Player)
						player.skills().__addXp(Skills.DEFENCE, xp / 3, target !is Player)
					}
				}
			}
			
			CombatStyle.RANGE -> {
				when (mode) {
					AttackMode.RANGED -> {
						player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
						player.skills().__addXp(Skills.RANGED, (hit * 4).toDouble(), target !is Player)
					}
					AttackMode.SHARED -> {
						player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
						player.skills().__addXp(Skills.RANGED, (hit * 2).toDouble(), target !is Player)
						player.skills().__addXp(Skills.DEFENCE, (hit * 2).toDouble(), target !is Player)
					}
				}
			}
			
			CombatStyle.MAGIC -> {
				when (mode) {
					AttackMode.MAGIC -> {
						if (player.varps().varbit(2668) == 0 || player.varps().varbit(Varbit.AUTOCAST_SELECTED) == 0) {
							// Accurate? Or normal autocast? aka non defensive
							player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
							player.skills().__addXp(Skills.MAGIC, (hit * 2).toDouble(), target !is Player)
						} else {
							// Defensive autocast...
							player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
							player.skills().__addXp(Skills.MAGIC, (hit + (hit / 3)).toDouble(), target !is Player)
							player.skills().__addXp(Skills.DEFENCE, hit.toDouble(), target !is Player)
						}
					}
					AttackMode.SHARED -> {
						player.skills().__addXp(Skills.HITPOINTS, (hit + (hit / 3)).toDouble(), target !is Player)
						player.skills().__addXp(Skills.MAGIC, (hit * 2).toDouble(), target !is Player)
						player.skills().__addXp(Skills.DEFENCE, (hit * 2).toDouble(), target !is Player)
					}
				}
			}
		}
	}
	
	// Was the given entity attacked or has attacked someone within the last 10 seconds?
	@JvmStatic fun inCombat(entity: Entity): Boolean {
		val lastAttacked = System.currentTimeMillis() - entity.attribOr<Long>(AttributeKey.LAST_WAS_ATTACKED_TIME, 0L)
		val lastAttack = System.currentTimeMillis() - entity.attribOr<Long>(AttributeKey.LAST_ATTACK_TIME, 0L)
		return entity.timers().has(TimerKey.COMBAT_LOGOUT) || lastAttack < 10000L || lastAttacked < 10000L
	}
	
	fun blockRushing(player: Player, target: Entity): Boolean {
		if (target.isPlayer && player.world().realm().isPVP) {
			if (target.varps().varbit(Varbit.MULTIWAY_AREA) == 0 && VarbitAttributes.varbiton(target as Player, 18) && WildernessLevelIndicator.wildernessLevel(target.tile()) >= 8) {
				val historyMap: MutableMap<Int, Int> = player.attribOr<MutableMap<Int, Int>>(AttributeKey.PVP_WILDY_AGGRESSION_TRACKER, HashMap<Int, Int>())
				val agroTick = historyMap.getOrDefault(target.id() as Int, player.world().cycleCount())
				if (player.world().cycleCount() - agroTick < 20) {
					player.message("<col=804080>Target protected; you must be at least 12 seconds into a battle to use instant special attacks.")
					return true
				}
			}
		}
		return false
	}

    @JvmStatic fun pendingSpears(p: Entity): Queue<Direction> {
		return p.attribOr<Queue<Direction>>(AttributeKey.SPEAR_MOVES, LinkedList<Direction>())
	}
}
