package nl.bartpelle.veteres.content.skills.fletching

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/15/2015.
 */

object FletchDarts {
	
	enum class Darts(val tip: Int, val outcome: Int, val lvlreq: Int, val xp: Double, val itemName: String) {
		BRONZE_DART(819, 806, 1, 1.8, "bronze darts"),
		IRON_DART(820, 807, 22, 3.8, "iron darts"),
		STEEL_DART(821, 808, 37, 7.5, "steel darts"),
		MITHRIL_ARROW(822, 809, 52, 11.2, "mithril darts"),
		ADAMANT_ARROW(823, 810, 67, 15.0, "adamant darts"),
		RUNE_ARROW(824, 811, 81, 18.8, "rune darts"),
		DRAGON_ARROW(11232, 11230, 95, 25.0, "dragon darts")
	}
	
	val FEATHER = 314
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		for (darts in Darts.values()) {
			repo.onItemOnItem(darts.tip.toLong(), FEATHER.toLong(), s@ @Suspendable {
				makedart(darts, it)
			})
		}
	}
	
	fun makedart(darts: Darts, it: Script) {
		//Check if the player has a high enough level to do this.
		if (it.player().skills()[Skills.FLETCHING] < darts.lvlreq) {
			it.messagebox("You need a Fletching level of ${darts.lvlreq} or above to make ${darts.itemName}.")
			return
		}
		var supplyCount = Math.min(it.player().inventory().count(darts.tip), it.player().inventory().count(FEATHER))
		var toMake = Math.min(supplyCount, 10)
		it.player().inventory() -= Item(darts.tip, toMake)
		it.player().inventory() -= Item(FEATHER, toMake)
		it.player().inventory() += Item(darts.outcome, toMake)
		it.addXp(Skills.FLETCHING, darts.xp)
		it.player().message("You make $toMake " + darts.itemName + ".")
	}
}
