package nl.bartpelle.veteres.content.npcs.inferno

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Mack on 7/26/2017.
 */
object JalAk {

	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 6) && EntityCombat.attackTimerReady(npc)) {
				
				if (target.isPlayer) {
					val player: Player = target as Player
					
					if (player.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1) {
						range(npc, target)
					} else if (player.varps().varbit(Varbit.PROTECT_FROM_MISSILES) == 1) {
						magic(npc, target)
					} else {
						
						if (EntityCombat.canAttackMelee(npc, target) && npc.world().rollDie(3, 1)) {
							melee(npc, target)
						} else when(npc.world().random(2)) {
							1 -> {
								range(npc, target)
							}
							else -> {
								magic(npc, target)
							}
						}
					}
				}
				
				//Send to cooldown.
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun melee(npc: Npc, target: Entity) {
		
		npc.animate(npc.attackAnimation())
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			target.hit(npc, npc.world().random(20), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(npc, 0, 1)
		}
	}
	
	fun range(npc: Npc, target: Entity) {
		
		npc.animate(npc.attackAnimation())
		
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc.tile() + Tile(1, 1), target, 1380, 20, 34, 45, 12 * tileDist, 0, 1)
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
			target.hit(npc, npc.world().random(29), delay).combatStyle(CombatStyle.RANGE)
		} else {
			target.hit(npc, 0, delay)
		}
	}
	
	fun magic(npc: Npc, target: Entity) {
		
		npc.animate(npc.attackAnimation())
		
		val tileDist = npc.tile().distance(target.tile())
		npc.world().spawnProjectile(npc.tile() + Tile(1, 1), target, 1378, 20, 34, 45, 12 * tileDist, 0, 1)
		val delay = Math.max(1, (30 + tileDist * 12) / 30)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
			target.hit(npc, npc.world().random(29), 1).delay(delay).combatStyle(CombatStyle.MAGIC)
		} else {
			target.hit(npc, 0, 1)
		}
	}
}