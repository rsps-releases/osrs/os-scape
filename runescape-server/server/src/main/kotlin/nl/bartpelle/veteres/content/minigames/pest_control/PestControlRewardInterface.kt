package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import java.util.*

/**
 * Created by Jason MacKeigan on 2016-08-02 at 7:34 PM
 *
 * Each reward represents a single section of the void knight reward option interface.
 * Each reward can have a group of singular rewards.
 */
object PestControlRewardInterface {
	
	/**
	 * Opens the reward interface for the interacting player.
	 */
	fun open(interacting: Player) {
		updatePointComponent(interacting)
		updateExperienceSectionComponent(interacting)
		interacting.interfaces().sendMain(PestControl.REWARD_INTERFACE)
	}
	
	/**
	 * Updates the attribute for the selected reward, and visually updates
	 * the aspect of the interface for displaying the name of the reward.
	 */
	fun select(interacting: Player, section: Section, reward: Reward) {
		interacting.putattrib(AttributeKey.PEST_CONTROL_REWARD_SELECTED, Optional.of(reward))
		updateNameComponent(interacting, section)
	}
	
	/**
	 * Attempts to confirm the selection of the reward from the interface.
	 */
	fun confirm(interacting: Player, reward: Reward) {
		if (reward.appendable(interacting)) {
			try {
				deduct(interacting, reward.pointsRequired())
				reward.append(interacting)
				updatePointComponent(interacting)
			} catch (iae: IllegalArgumentException) {
				interacting.message("Unable to confirm purchase at this time, come back later.")
			}
		}
	}
	
	/**
	 * Visually updates the point component of the interface to be that of the players current pest
	 * control points, or otherwise zero.
	 */
	private fun updatePointComponent(interacting: Player) {
		interacting.interfaces().text(PestControl.REWARD_INTERFACE, 150, interacting.attribOr<Int>(
				AttributeKey.PEST_CONTROL_POINTS, 0).toString())
	}
	
	/**
	 * Visually updates the aspect of the interface for experience rewards.
	 */
	private fun updateExperienceSectionComponent(interacting: Player) {
		for (section in Section.values()) {
			val firstReward = section.rewards.first()
			
			if (firstReward is ExperienceReward) {
				interacting.interfaces().text(PestControl.REWARD_INTERFACE, section.widgetHeader,
						if (interacting.skills().level(firstReward.skillId) < 25) {
							"<col=FF0000>You must reach level 25 first."
						} else {
							"<col=00CC00>${section.nameFormatted()} - ${ExperienceReward.experienceFor(
									firstReward.skillExperience, interacting.skills().level(firstReward.skillId))} xp"
						}
				)
			}
		}
	}
	
	/**
	 * Deducts the amount of points from the player.
	 */
	private fun deduct(recipient: Player, points: Int) {
		val current = recipient.attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0)
		
		if (current <= 0)
			return
		
		recipient.modifyNumericalAttribute(AttributeKey.PEST_CONTROL_POINTS, -points, Integer(0))
	}
	
	/**
	 * Visually updates the name component of the interface that displays what section of
	 * the interface is currently selected.
	 */
	fun updateNameComponent(interacting: Player, section: Section) {
		interacting.interfaces().text(PestControl.REWARD_INTERFACE, 149, section.nameFormatted())
	}
	
	/**
	 * An individual section of the interface whether it be pertaining to experience,
	 * packets, or void items.
	 */
	enum class Section(val widgetHeader: Int, vararg val rewards: Reward) {
		ATTACK_XP(62, ExperienceReward(Skills.ATTACK, 1, 86, ExperienceBase.ATTACK),
				ExperienceReward(Skills.ATTACK, 10, 101, ExperienceBase.ATTACK),
				ExperienceReward(Skills.ATTACK, 100, 108, ExperienceBase.ATTACK)),
		
		STRENGTH_XP(63, ExperienceReward(Skills.STRENGTH, 1, 87, ExperienceBase.STRENGTH),
				ExperienceReward(Skills.STRENGTH, 10, 102, ExperienceBase.STRENGTH),
				ExperienceReward(Skills.STRENGTH, 100, 109, ExperienceBase.STRENGTH)),
		
		DEFENCE_XP(64, ExperienceReward(Skills.DEFENCE, 1, 88, ExperienceBase.DEFENCE),
				ExperienceReward(Skills.DEFENCE, 10, 103, ExperienceBase.DEFENCE),
				ExperienceReward(Skills.DEFENCE, 100, 110, ExperienceBase.DEFENCE)),
		
		RANGED_XP(65, ExperienceReward(Skills.RANGED, 1, 89, ExperienceBase.RANGE),
				ExperienceReward(Skills.RANGED, 10, 104, ExperienceBase.RANGE),
				ExperienceReward(Skills.RANGED, 100, 111, ExperienceBase.RANGE)),
		
		MAGIC_XP(66, ExperienceReward(Skills.MAGIC, 1, 90, ExperienceBase.MAGIC),
				ExperienceReward(Skills.MAGIC, 10, 105, ExperienceBase.MAGIC),
				ExperienceReward(Skills.MAGIC, 100, 112, ExperienceBase.MAGIC)),
		
		HITPOINT_XP(67, ExperienceReward(Skills.HITPOINTS, 1, 91, ExperienceBase.HITPOINTS),
				ExperienceReward(Skills.HITPOINTS, 10, 106, ExperienceBase.HITPOINTS),
				ExperienceReward(Skills.HITPOINTS, 100, 113, ExperienceBase.HITPOINTS)),
		
		PRAYER_XP(68, ExperienceReward(Skills.PRAYER, 1, 92, ExperienceBase.PRAYER),
				ExperienceReward(Skills.PRAYER, 10, 107, ExperienceBase.PRAYER),
				ExperienceReward(Skills.PRAYER, 100, 114, ExperienceBase.PRAYER)),
		
		HERB_PACK(-1, PackReward(arrayOf(Item(200, 10), Item(202, 10), Item(204, 10), Item(206, 10), Item(208, 10),
				Item(210, 10)), arrayOf(Item(212, 8), Item(214, 8), Item(216, 8), Item(218, 5), Item(220, 3)), 97, 30)),
		
		MINERAL_PACK(-1, PackReward(arrayOf(Item(437, 25), Item(439, 25), Item(441, 20), Item(443, 15), Item(445, 15),
				Item(448, 12)), arrayOf(Item(450, 10), Item(452, 5)), 98, 15)),
		
		SEED_PACK(-1, PackReward(arrayOf(Item(5291, 20), Item(5292, 20), Item(5293, 20), Item(5294, 15), Item(5295, 10),
				Item(5296, 8), Item(5297, 8), Item(5298, 6)), arrayOf(Item(5299, 5), Item(5300, 4), Item(5301, 4),
				Item(5302, 4), Item(5303, 4), Item(5304, 3)), 100, 15)),
		
		VOID_KNIGHT_MACE(-1, ItemReward(Item(8841), 250, 93)),
		
		VOID_KNIGHT_TOP(-1, ItemReward(Item(8839), 250, 94)),
		
		VOID_KNIGHT_ROBES(-1, ItemReward(Item(8840), 250, 95)),
		
		VOID_KNIGHT_GLOVES(-1, ItemReward(Item(8842), 150, 96)),
		
		VOID_KNIGHT_MAGE_HELM(-1, ItemReward(Item(11665), 200, 119)),
		
		VOID_KNIGHT_RANGER_HELM(-1, ItemReward(Item(11664), 200, 120)),
		
		VOID_KNIGHT_MELEE_HELM(-1, ItemReward(Item(11663), 200, 121))
		;
		
		fun nameFormatted(): String = name.replace("_", " ").toLowerCase().capitalize()
	}
	
	/**
	 * The base experience obtained from the particular skill.
	 */
	enum class ExperienceBase(val base: Int) {
		ATTACK(35),
		STRENGTH(35),
		DEFENCE(35),
		HITPOINTS(35),
		RANGE(32),
		MAGIC(32),
		PRAYER(18)
	}
	
	/**
	 * A reward obtained from the pest control interface. The interface currently
	 * consists of experience and item based rewards.
	 */
	interface Reward {
		
		/**
		 * The button that must be interacted with to trigger this reward.
		 */
		fun buttonId(): Int
		
		/**
		 * The amount of points required for this individual reward.
		 */
		fun pointsRequired(): Int
		
		/**
		 * Determines whether or not the player, in this case the recipient, can
		 * have the reward appended to them.
		 */
		fun appendable(recipient: Player): Boolean
		
		/**
		 * Appends an item to the recipient whether it be an item or some experience.
		 */
		fun append(recipient: Player)
		
	}
	
	/**
	 * Represents an abstract reward that has a given button id as all
	 * rewards do.
	 */
	abstract class AbstractReward(val buttonId: Int, val pointsRequired: Int) : Reward {
		
		/**
		 * The button that must be clicked to trigger this reward.
		 */
		override fun buttonId(): Int = buttonId
		
		/**
		 * The points required for this reward.
		 */
		override fun pointsRequired(): Int = pointsRequired
		
	}
	
	/**
	 * A type of reward that appends experience to the recipient whether it be
	 * attack, defence, range, magic, hitpoints, or prayer.
	 */
	class ExperienceReward(val skillId: Int, pointsRequired: Int, widgetId: Int, val skillExperience: ExperienceBase) :
			AbstractReward(widgetId, pointsRequired) {
		
		/**
		 * Determines if the player has a level of at least 25 in the skill they're
		 * selecting to which if they don't, they will be given a message stating so.
		 */
		override fun appendable(recipient: Player): Boolean {
			val level = recipient.skills().level(skillId)
			
			if (level < 25) {
				recipient.message("The Void Knights will not offer training in skills in which you have " +
						"a level of level than 25.")
				return false
			}
			if (recipient.attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0) < pointsRequired()) {
				recipient.message("You don't have the pest control points required to do this.")
				return false
			}
			return true
		}
		
		/**
		 * Appends the experience to the player given the formula (BASE_XP * FLOOR(LEVEL ^ 2 / 600))
		 * where base is either 18 for prayer, 32 for magic or range, and 35 for everything else.
		 * Level is the currently level of the player in the selected skill.
		 */
		override fun append(recipient: Player) {
			val level = recipient.skills().level(skillId)
			
			val experience = experienceFor(skillExperience, level)
			
			val experienceForPoints = experience * if (pointsRequired() == 100) 110 else pointsRequired()
			
			recipient.skills().__addXp(skillId, Math.max(0.0, experienceForPoints.toDouble()))
		}
		
		companion object {
			
			/**
			 * Calculates the experience received for the given base experience gained
			 * with the level of the player.
			 */
			fun experienceFor(base: ExperienceBase, level: Int) = base.base * Math.floor(Math.pow(
					level.toDouble(), 2.0) / 600)
		}
		
	}
	
	/**
	 * A type of reward that is given in the form of an item whether it be a skill related
	 * or void knight related.s
	 */
	class ItemReward(val item: Item, pointsRequired: Int, widgetId: Int) : AbstractReward(widgetId, pointsRequired) {
		
		/**
		 * Determines whether or not the player has the requirements
		 * to purchase this item.
		 */
		override fun appendable(recipient: Player): Boolean {
			if (recipient.inventory().freeSlots() < item.amount()) {
				recipient.message("You need at least ${item.amount()} free slots to do this.")
				return false
			}
			if (recipient.attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0) < pointsRequired()) {
				recipient.message("You need at least ${pointsRequired()} points to do this.")
				return false
			}
			return true
		}
		
		/**
		 * Appends the item reward to the recipient and duducts the points from the
		 * players.
		 */
		override fun append(recipient: Player) {
			recipient.inventory().add(item, true)
		}
	}
	
	class PackReward(val common: Array<Item>, val rare: Array<Item>, widgetId: Int, pointsRequired: Int) : AbstractReward(
			widgetId, pointsRequired) {
		
		/**
		 * The probability or chance that we will be appended a rare.
		 */
		val CHANCE_FOR_RARE = 10
		
		/**
		 * Appends an item to the recipient whether it be an item or some experience.
		 */
		override fun append(recipient: Player) {
			val random = Random()
			
			for (iteration in 1..5) {
				val item = if (random.nextInt(99) < CHANCE_FOR_RARE) rare.random() else common.random()
				
				recipient.inventory().add(Item(item.id(), 1 + random.nextInt(item.amount() - 1)), true)
			}
		}
		
		/**
		 * Determines whether or not the player, in this case the recipient, can
		 * have the reward appended to them.
		 */
		override fun appendable(recipient: Player): Boolean {
			if (recipient.inventory().freeSlots() < 5) {
				recipient.message("You need at least five (5) free slots to do this.")
				return false
			}
			if (recipient.attribOr<Int>(AttributeKey.PEST_CONTROL_POINTS, 0) < pointsRequired()) {
				recipient.message("You need at least ${pointsRequired()} points to do this.")
				return false
			}
			return true
		}
		
	}
	
}