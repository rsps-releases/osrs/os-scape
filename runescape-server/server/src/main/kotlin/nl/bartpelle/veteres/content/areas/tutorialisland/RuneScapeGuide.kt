package nl.bartpelle.veteres.content.areas.tutorialisland

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.doubleItemBox
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/18/2015.
 */

object RuneScapeGuide {
	
	fun decide(it: Script, mode: GameMode) {
		it.player().mode(mode)
		it.player().teleport(3212, 3423) // 3212, 3423 = varrock center square
		it.player().stopActions(true)
		
		// 150k
		it.player().inventory().add(Item(995, 50000), true) // 50k
		
		// Set of iron
		it.player().inventory().add(Item(1153), true) // Helm
		it.player().inventory().add(Item(1115), true) // Body
		it.player().inventory().add(Item(1067), true) // Legs
		it.player().inventory().add(Item(1191), true) // Shield
		it.player().inventory().add(Item(1323), true) // Scimitar
		
		it.player().inventory().add(Item(380, 40), true) // 40x lobster
		it.player().inventory().add(Item(3105), true) // Climb boots
		it.player().inventory().add(Item(1731), true) // Amulet of power
		it.player().inventory().add(Item(1169), true) // Coif
		it.player().inventory().add(Item(1129), true) // Leather body
		it.player().inventory().add(Item(1095), true) // Leather chaps
		it.player().inventory().add(Item(841), true) // Shortbow
		it.player().inventory().add(Item(882, 150), true) // Bronze arrows
		
		it.player().inventory().add(Item(556, 200), true) // Air rune
		it.player().inventory().add(Item(558, 200), true) // Mind rune
		it.player().inventory().add(Item(554, 150), true) // Fire rune
		it.player().inventory().add(Item(555, 150), true) // Water rune
		it.player().inventory().add(Item(557, 150), true) // Earth rune
		it.player().inventory().add(Item(562, 35), true) // Chaos rune
		it.player().inventory().add(Item(563, 25), true) // Law rune
		
		if (it.player().world().realm().isRealism || it.player().world().realm().isOSRune)
			ClanChat.join(it.player(), "help eco")
	}
	
	fun decide(it: Script, mode: IronMode) {
		it.player().ironMode(mode)
		it.player().teleport(3222, 3219)
		it.player().stopActions(true)
		
		// Iron man starter pack
		val inv = it.player.inventory()
		inv.add(Item(1351), true) // Bronze axe
		inv.add(Item(590), true) // Tinderbox
		inv.add(Item(303), true) // Fishing net
		inv.add(Item(315), true) // Shrimps
		inv.add(Item(2309), true) // Bread
		inv.add(Item(1265), true) // Bronze pick
		inv.add(Item(1205), true) // Bronze dagger
		inv.add(Item(1277), true) // Bronze sword
		inv.add(Item(1171), true) // Wooden shield
		inv.add(Item(841), true) // Shortbow
		inv.add(Item(882, 25), true) // Bronze arrows
		inv.add(Item(558, 15), true) // Mind rune
		inv.add(Item(555, 6), true) // Water rune
		inv.add(Item(557, 4), true) // Earth rune
		inv.add(Item(559, 2), true) // Body rune
		inv.add(Item(995, 25), true) // 25gp
		
		giveIronManSuit(it.player.ironMode(), it.player.inventory())
		
		if (it.player().world().realm().isRealism || it.player().world().realm().isOSRune)
			ClanChat.join(it.player(), "help eco")
	}
	
	fun giveIronManSuit(mode: IronMode, inv: ItemContainer) {
		when (mode) {
			IronMode.REGULAR -> {
				inv.add(Item(12810), true)
				inv.add(Item(12811), true)
				inv.add(Item(12812), true)
			}
			IronMode.ULTIMATE -> {
				inv.add(Item(12813), true)
				inv.add(Item(12814), true)
				inv.add(Item(12815), true)
			}
			IronMode.HARDCORE -> {
				inv.add(Item(20792), true)
				inv.add(Item(20794), true)
				inv.add(Item(20796), true)
			}
		}
	}
	
	@JvmStatic @ScriptMain fun registerRuneScapeGuide(repo: ScriptRepository) {
		repo.onNpcOption1(3308, s@ @Suspendable {
			if (it.player().world().realm().isOSRune) {
				showOsruneGuide(it)
			} else if (it.player().world().realm().isDeadman) {
				it.chatNpc("Welcome to the Deadman development world! I'll give you options to leave in a second. When you leave, use ::master for stats and ::home to go" +
						" to a Safe zone. ", 3308)
				it.chatNpc("You can spawn items via ::cycle [item name] then ::item [item ID, amount].", 3308)
				it.chatNpc("You can also ::copy [player name] to get someones equipment and inventory. The Deadman server has no Public release date and is weeks away" +
						" from being finished. ", 3308)
				it.chatNpc("Ask Jak for the Titanpad link for pitch your design ideas. Use ::barrage to get ancients. Enjoy!", 3308)
				when (it.optionsTitled("Where would you like to begin?", "Lumbridge", "Falador", "Varrock", "Ardougne", "Gnome Stronghold")) {
					1 -> it.player().teleport(Tile(3222, 3222))
					2 -> it.player().teleport(Tile(2965, 3383))
					3 -> it.player().teleport(Tile(3217, 3428))
					4 -> it.player().teleport(Tile(2661, 3305))
					5 -> it.player().teleport(Tile(2461, 3435))
				}
			} else if (it.player().world().realm().isRealism) {
				sendRealismGuide(it)
			}
		})
	}
	
	@Suspendable fun sendRealismGuide(it: Script) {
		it.chatNpc("Greetings! I see you are a new arrival to this land. My<br>job is to welcome all new visitors. So welcome!", 3308, 568)
		it.chatNpc("Before you can go on your adventure, I would like to ask you how much of an adventurer you really are. We offer you a selection of '<col=ff0000>Game Modes</col>', as we call them. They determine the speed and ease of this world.", 3308, 568)
		
		while (true) {
			when (it.options("Tell me about Laid-Back mode.", "Tell me how Classic mode works.", /*"What's this Hardened mode?", */"What is included in Realism?")) {
				1 -> {
					it.chatPlayer("Can you tell me what Laid-Back mode offers?")
					it.chatNpc("Ah, yes! This game mode is invented to offer those that are not too eager on training a quick " +
							"experience. They are granted <col=ff0000>1,000x combat</col> experience rates, and <br><col=ff0000>20x skill</col> experience rates.", 3308)
					it.chatPlayer("Are there any penalties or downsides to this?")
					it.chatNpc("Yes, there are indeed. To counter the quick rate of training, it is most fair to give these a <col=ff0000>lower drop rate</col>. This applies to all monsters, and especially to rare drops.", 3308)
					it.chatNpc("Do you want to use this game mode?", 3308)
					
					if (it.options("Yes, the Laid-Back mode is mine!", "No, let me browse some more.") == 1) {
						it.player.mode(GameMode.LAID_BACK)
						it.player.teleport(3140, 3087)
						return
					}
				}
				2 -> {
					it.chatPlayer("Could you explain Classic mode to me?")
					it.chatNpc("Very well then, ${it.player().name()}. This is the classic experience most people seek in a private server. It offers you <col=ff0000>250x combat</col> and <col=ff0000>20x skilling</col> experience rates.", 3308)
					it.chatNpc("On top of that, one can always switch back to Laid-Back mode should he not like this mode. Note that you cannot go the other way around!", 3308)
					it.chatPlayer("Are there any negative effects of picking this mode?")
					it.chatNpc("No, there are none. However, you do not get any benefits either as opposed to the Hardened and Realism modes. They might be worth looking into!", 3308)
					
					if (it.options("Yes, the Classic mode is mine!", "No, let me browse some more.") == 1) {
						it.player.mode(GameMode.CLASSIC)
						it.player.teleport(3140, 3087)
						return
					}
				}
				3 -> {
					it.chatPlayer("If I am looking for a really tough go, does Realism<br>suit me well?")
					it.chatNpc("Correct as can be. Realism is for only those not afraid to spend hours and hours to achieve their goals. It's tricky, and both <col=ff0000>combat and skilling</col> experience rates are stuck at <col=ff0000>10x</col>.", 3308)
					it.chatPlayer("Sounds like the most challenging out there. I assume this is being rewarded?")/*
                    it.chatNpc("Yes, that is indeed true. You inherit all the benefits from Hardened, but increased and strengthened. Let me sum them up for you.", 3308)*/
					it.chatNpc("You will have a <col=ff0000>higher drop rare</col> and your <col=ff0000>prayer drains much slower in PvM areas</col>.", 3308)
					it.chatNpc("On top of that, Bob in Lumbridge repairs your Barrows items for a highly reduced price depending in your combat level. You also are not charged any kill count when entering God Wars boss chambers.", 3308)
					it.chatNpc("You are also given a <col=ff0000>higher chance at good rewards when doing clue scrolls</col>. Monsters will also drop <col=ff0000>caskets more commonly</col=ff0000>.", 3308)
					it.chatNpc("On top of this, because your skilling is slightly tougher, you have <col=ff0000>an increased chance to get skilling pets</col> from skilling activities!", 3308)
					it.chatNpc("Should you end up not liking this mode, we give you the option to change back to either Classic or Laid-Back any time. You cannot go the other way!", 3308)
					
					if (it.options("Yes, the Realism mode is mine!", "No, let me browse some more.") == 1) {
						it.player.mode(GameMode.REALISM)
						it.player.teleport(3140, 3087)
						return
					}
				}
			}
		}
	}
	
	@Suspendable fun showOsruneGuide(it: Script) {
		it.chatNpc("Hello ${it.player.name()}! First and foremost, welcome to<br>OS-Scape's Economy realm. I'll be your guide.", 3308, 568)
		it.chatNpc("Before we start, I'd like to ask you if you're interested in a short run-down of how everything works, or just want to play right away.", 3308, 568)
		
		when (it.optionsTitled("Do you want a tutorial?", "Show the short game tutorial!", "No, thanks. I'll be on my own.")) {
			1 -> {
				it.chatPlayer("Please show me around, as I'm fairly new.")
				it.chatNpc("Very well, ${it.player.name()}. Let's get started with the basics. First of all, are you happy with how you look? I mean... Over here, of course. In the game.", 3308, 568)
				
				if (it.options("I like my appearance.", "I'd rather change something first.") == 2) {
					it.player().interfaces().sendMain(269)
					return
				}
				
				it.chatNpc("Splendid, I do think you look fantastic like that. Let's go on and look at where people trade!", 3308, 568)
				it.player.lock()
				it.player.teleport(3095, 3510)
				it.chatNpc("This is the store hut, the main marketplace of OS-Scape. People buy and sell all kinds of goods here. We encourage you to do your trade here to show others you're interested in doing business.", 3308, 568)
				it.doubleItemBox("You can buy a lot of items from the stores found around this place, yet some are only obtainable through players. Find a partner to trade with to get the more powerful and delicate items!", Item(995, 1000000), Item(4151, 1))
				//it.chatNpc("This is the Grand Exchange, the main marketplace of OS-Scape. People buy and sell all kinds of goods here. We encourage you to do your trade here to show others you're interested in doing business.", 3308, 568)
				//it.doubleItemBox("At the Grand Exchange, one can put up buy and sell offers to exchange goods to other players in a safe and global matter. Simply post an offer and wait for another player to complete it, either partially or fully.", Item(995, 1000000), Item(4151, 1))
				
				it.player.teleport(3140, 3087)
				it.player.unlock()
			}
			2 -> {
				it.chatPlayer("I'll just be on my own. I'm rather familiar, so I'll manage.")
				
				//it.player.teleport(3095, 3510)
				it.player.teleport(3140, 3087)
			}
		}
	}
	
}
