package nl.bartpelle.veteres.content.areas.dungeons.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/9/2015.
 */

object OddLookingWall {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		intArrayOf(7432, 7434).forEach { n ->
			r.onObject(n) @Suspendable {
				if (it.player().tile().level == 0) {
					val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
					val player = it.player()
					
					it.player().lock()
					
					// Go to the right spot if we're not there
					if (!it.player().tile().equals(obj.tile().transform(-1, 0, 0)) && player.tile().x <= 3093) {
						it.player().walkTo(obj.tile(), PathQueue.StepType.FORCED_WALK)
						it.waitForTile(obj.tile())
					}
					
					it.runGlobal(player.world()) @Suspendable {
						val world = player.world()
						val door1 = MapObj(Tile(3094, 9895), 7434, 0, 0)
						val door2 = MapObj(Tile(3094, 9896), 7432, 0, 0)
						val spawned = MapObj(Tile(3093, 9895), 7435, 0, 3)
						val spawned2 = MapObj(Tile(3093, 9896), 7433, 0, 1)
						
						world.removeObj(door1, false)
						world.removeObj(door2, false)
						world.spawnObj(spawned)
						world.spawnObj(spawned2)
						
						it.delay(2)
						
						world.removeObj(spawned)
						world.removeObj(spawned2)
						world.spawnObj(door1)
						world.spawnObj(door2)
					}
					
					//Force the player to move to the tile
					if (player.tile().x <= 3093) {
						it.player().pathQueue().interpolate(3094, it.player().tile().z, PathQueue.StepType.FORCED_WALK)
					} else if (player.tile().x >= 3094) {
						it.player().pathQueue().interpolate(3093, it.player().tile().z, PathQueue.StepType.FORCED_WALK)
					}
					
					it.player().unlock()
				}
			}
		}
	}
	
}
