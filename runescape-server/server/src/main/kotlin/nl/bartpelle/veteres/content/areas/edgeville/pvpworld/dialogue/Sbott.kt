package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.items.VotingRewardToken
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands

/**
 * Created by Situations on 5/29/2016.
 */

object Sbott {
	
	val SBOTT = 6526
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		/*r.onNpcOption1(SBOTT) @Suspendable {
			it.chatNpc("Hi ${it.player().name()}, how can I help you?", SBOTT, 588)
			when (it.options("What is voting?", "Can you open the webpage?", "How many points do I have?", "What do I get for voting?")) {
				1 -> what_is_voting(it)
				2 -> open_voting_page(it)
				3 -> how_many_points(it)
				4 -> what_do_I_get(it)
			}
		}
		
		r.onNpcOption2(SBOTT) @Suspendable { view_shop(it) }
		r.onNpcOption3(SBOTT) @Suspendable { open_voting_page(it) }*/
	}
	
	@Suspendable fun what_is_voting(it: Script) {
		/*it.chatPlayer("What exactly is voting?", 588)
		it.chatNpc("Voting is the quickest way to grow our community. Each time you vote for OS-Scape on a toplist, you make it easier for players such as yourself to notice and try out OS-Scape.", SBOTT, 567)
		it.chatNpc("It is extremely important that we're highly ranked on the RSPS toplists so we've implemented incentive to give you a reason to vote other than the communities growth!", SBOTT, 588)
		when (it.options("Tell me more about the reward.", "Can you open the webpage?", "Can I view your shop?", "Thanks.")) {
			1 -> what_do_I_get(it)
			2 -> open_voting_page(it)
			3 -> view_shop(it)
			4 -> it.chatPlayer("Thanks.")
		}*/
	}
	
	@Suspendable fun what_do_I_get(it: Script) {
		/*it.chatPlayer("What do I get for voting?", 588)
		it.chatNpc("Besides the large impact you'll have on OS-Scape's growth, you'll be awarded a Voting Reward Token which can be redeemed for a few different items. Some of the items you can get are..", SBOTT)
		it.itemBox("A Voting Point, which can be used to purchase tons of amazing items from my Point Exchange shop...", VotingRewardToken.VOTING_REWARD_TOKEN)
		it.itemBox("...and 750 Blood Money, which is the in-game currency for OS-Scape PVP.", VotingRewardToken.BLOOD_MONEY, 750)
		it.chatNpc("You can also trade your Voting Reward Token to other players for a potentially large profit.", SBOTT, 567)
		when (it.options("Can you open the voting webpage?", "What is voting?", "How many points do I have?")) {
			1 -> open_voting_page(it)
			2 -> what_is_voting(it)
			3 -> how_many_points(it)
		}*/
	}
	
	@Suspendable fun how_many_points(it: Script) {
		/*val voting_points = it.player().attribOr<Int>(AttributeKey.VOTING_POINTS, 0)
		val s = if (voting_points == 1) "" else "s"
		
		it.chatPlayer("How many voting points do I currently have?", 588)
		it.chatNpc("You currently have $voting_points point$s.", SBOTT, 567)
		it.chatPlayer("Oh, okay.. thanks!", 567)*/
	}
	
	@Suspendable fun open_voting_page(it: Script) {
		/*it.chatPlayer("Can you open the webpage so I can vote, please?")
		it.chatNpc("Sure, here you are!", SBOTT, 567)
		GameCommands.process(it.player(), "vote")*/
	}
	
	@Suspendable fun view_shop(it: Script) {
		/*if (it.player().world().realm().isPVP) {
			val voting_points = it.player().attribOr<Int>(AttributeKey.VOTING_POINTS, 0)
			val s = if (voting_points == 1) "" else "s"
			it.player().message("You currently have $voting_points voting point$s.")
			it.player().world().shop(69).display(it.player())
		} else {
			it.chatNpc("You can only view this shop on the PVP world!", SBOTT, 567)
		}*/
	}
}
