package nl.bartpelle.veteres.content.npcs.misc

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Shadowy|Jak on Feb 1st 2016.
 */

object DarkWizard {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		val GRAPHIC = if (npc.id() == 5087) 93 else if (npc.id() == 5086) 93 else 96
		val PROJECTILE = if (npc.id() == 5087) 94 else if (npc.id() == 5086) 94 else 97
		val HIT_GRAPHIC = if (npc.id() == 5087) 95 else if (npc.id() == 5086) 95 else 98
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 5) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.attackTimerReady(npc)) {
					npc.graphic(GRAPHIC, 100, 0)
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					
					val tileDist = npc.tile().distance(target.tile())
					val delay = (1 + Math.floor(tileDist.toDouble()) / 2.0).toInt()
					
					npc.world().spawnProjectile(npc.tile(), target, PROJECTILE, 40, 20, 50, 20 + (2 * tileDist), 10, 10)
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MAGIC)) {
						target.hit(npc, EntityCombat.randomHit(npc)).graphic(Graphic(92, 124, 0)).delay(delay)
					} else {
						target.hit(npc, 0).graphic(Graphic(HIT_GRAPHIC, 124)).delay(delay)
					}
					npc.animate(1162)
					EntityCombat.putCombatDelay(npc, 5)
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
}


