package nl.bartpelle.veteres.content.skills.firemaking

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Bart on 10/5/2015.
 */

object LogLighting {
	
	enum class LightableLog(val id: Int, val xp: Double, val req: Int, val barb_req: Int, val lifetime: Int) {
		LOGS(1511, 40.0, 1, 21, 200),
		ACHEY(2862, 40.0, 1, 21, 200),
		OAK(1521, 60.0, 15, 35, 233),
		WILLOW(1519, 90.0, 30, 50, 284),
		TEAK(6333, 105.0, 35, 55, 316),
		MAPLE(1517, 135.0, 45, 65, 350),
		MAHOGANY(6332, 157.5, 50, 70, 400),
		YEW(1515, 202.5, 60, 80, 500),
		MAGIC(1513, 303.8, 75, 95, 550),
		REDWOOD(19669, 350.0, 90, 99, 600);
		
		// Die roll  /100
		fun lightChance(player: Player, catching: Boolean): Int {
			val points: Int = 40
			val diff = Math.min(6, player.skills()[Skills.FIREMAKING] - req) // 6 points max
			return Math.min(100, points + diff * (if (catching) 15 else 10))
		}
		
		companion object {
			fun logForId(id: Int): LightableLog? {
				for (log in values()) {
					if (log.id == id)
						return log
				}
				return null;
			}
		}
	}
	
	enum class LightingAnimation(val item: Int, val anim: Int) {
		TRAININGBOW(9705, 6713),
		SHORTBOW(841, 6714),
		LONGBOW(839, 6714),
		OAK_SHORTBOW(843, 6715),
		OAK_LONGBOW(845, 6715),
		WILLOW_SHORTBOW(849, 6716),
		WILLOW_LONGBOW(847, 6716),
		MAPLE_SHORTBOW(853, 6717),
		MAPLE_LONGBOW(851, 6717),
		YEW_SHORTBOW(857, 6718),
		YEW_LONGBOW(855, 6718),
		MAGIC_SHORTBOW(861, 6719),
		MAGIC_LONGBOW(859, 6719),
		SEERCULL(6724, 6720);
	}
	
	val FIRELIGHTERS = intArrayOf(7329, 7330, 7331, 10326, 10327)
	
	@JvmStatic @ScriptMain fun logLighting(r: ScriptRepository) {
		for (i in FIRELIGHTERS) {
			r.onItemOnItem(20275, i) {
				// Uncharged firelighter
				it.player().inventory() -= 20275
				val lighters = if (it.itemUsed().id() == 20275) it.itemOn() else it.itemUsed()
				val slot = ItemOnItem.slotOf(it, lighters.id())
				
				it.player().inventory().remove(lighters, false, slot)
				val charged = Item(20278) // Add the new charged one
				if (it.player().inventory().add(charged, false).success()) {
					charged.property(ItemAttrib.CHARGES, lighters.amount())
					charged.property(ItemAttrib.LIGHTER_ID, lighters.id()) // Set item id assosiated with this firelighter - there are 5 types.
					it.message("You've added %d x %s to your gnomish firelighter.", lighters.amount(), lighters.name(it.player().world()))
					
					// Confirm properties were added
					val reducedLighter = Item(charged.property(ItemAttrib.LIGHTER_ID), charged.property(ItemAttrib.CHARGES))
					it.player().debug("This gnomish firelighter contains %d x %s", reducedLighter.amount(), reducedLighter.name(it.player().world()))
				}
			}
		}
		
		r.onItemOption5(20278) @Suspendable {
			// Remove charges from charged firelighter
			val box = it.player().inventory()[it.itemUsedSlot()]
			
			it.player().debug("has charges: %s from %s at slot %d.", box.hasProperties(), box, it.itemUsedSlot())
			val lighters = Item(box.property(ItemAttrib.LIGHTER_ID), box.property(ItemAttrib.CHARGES)) // TODO how the heck is this failing?
			
			it.messagebox("Do you wish to remove the charges from the firelighter?")
			if (it.optionsTitled("Remove firelighter charges?", "Yes.", "No.") == 1) {
				if (it.player().inventory().freeSlots() < 2) {
					it.message("You need two free spaces to do this.")
				} else {
					if (box.id() == 20278) {
						it.player().inventory().add(lighters, false) // give back firelighters
						it.player().inventory() -= 20278 // remove charged
						it.player().inventory() += 20275 // give uncharged
					}
				}
			}
		}
		r.onItemOption4(20278) {
			// Check charges on firelighter
			val box = it.itemUsed()
			if (box.id() == 20278) {
				val lighters = Item(box.property(ItemAttrib.LIGHTER_ID), box.property(ItemAttrib.CHARGES))
				it.message("This gnomish firelighter contains %d x %s", lighters.amount(), lighters.name(it.player().world()))
			}
		}
		r.onItemOption4(20275) {
			// Check empty firelighter
			it.message("This gnomish firelighter has no charges.")
		}
		
		r.onInvitemOnGrounditem(590, s@ @Suspendable {
			val spawnedItem = it.interactionGrounditem()
			
			var log: LightableLog? = null
			for (log1 in LightableLog.values()) {
				if (spawnedItem.item().id() == log1.id) {
					log = log1
				}
			}
			log ?: return@s // wrong item on gitem
			
			if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
				// Stop people spamming fires.
				it.messagebox("You can't use this skill on the PvP server.")
				return@s
			}
			if (it.player().attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
				it.message("You can't light a fire in here.")
				return@s
			}
			// Check level requirement
			if (it.player().skills()[Skills.FIREMAKING] < log.req) {
				val itemname = spawnedItem.item().definition(it.player().world()).name.toLowerCase()
				it.player().message("You need a Firemaking level of ${log.req} to burn $itemname.")
				return@s
			}
			
			// Check tile
			if (it.player().world().objByType(10, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
					|| it.player().world().objByType(11, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
					|| (it.player().world().floorAt(it.player().tile()) and 0x4) != 0) {
				it.message("You can't light a fire here.")
				return@s
			}
			var targTile = it.player().tile().transform(-1, 0, 0)
			var legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(-1, 0), 1)
			if (!legal) {
				targTile = it.player().tile().transform(1, 0, 0)
				legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(1, 0), 1)
				if (!legal) {
					return@s // No valid move to go!
				}
			}
			
			val catch = it.player().timers().has(TimerKey.FIRE_CATCHING)
			it.delay(1)
			
			if (catch && it.player().world().random(100) <= log.lightChance(it.player(), true)) { // Success
				// msg after removing log
			} else {
				it.player().animate(733)
				it.player().message("You attempt to light the logs.")
				it.delay(3)
				
				while (it.player().world().random(100) > log.lightChance(it.player(), true) && spawnedItem.valid(it.player().world())) {
					it.player().animate(733)
					it.delay(4)
				}
				if (!spawnedItem.valid(it.player().world()))
					it.message("The logs you were trying to light have disappeared.")
			}
			
			// Remove the logs
			if (!it.player().world().removeGroundItem(spawnedItem)) { // Invalid!
				return@s
			}
			
			if (BonusContent.isActive(it.player, BlessingGroup.PYROMANIAC) && it.player.world().rollDie(2, 1)) {
				it.player.inventory().add(Item(spawnedItem.item().id()), true)
				it.player().message("The active Pyromaniac blessing lets you light some kindling instead.")
			} else {
				it.player().message("The fire catches and the logs begin to burn.")
			}
			
			// Set our three tick timer to catch the fire (like on RS :))
			it.player().timers()[TimerKey.FIRE_CATCHING] = 3
			
			// Spawn a fire that dies after a minute and walk away
			val fire = makeFire(it.player(), log.lifetime)
			it.player().animate(-1)
			it.player().walkTo(targTile, PathQueue.StepType.FORCED_WALK)
			it.player().lock()
			it.delay(1)
			it.player().unlock()
			it.player().faceObj(fire)
			
			// Give us some xp now, because.. dialogue.
			it.addXp(Skills.FIREMAKING, log.xp * pyromancerOutfitBonus(it.player()))
		})
		
		for (log in LightableLog.values()) {
			
			r.onItemOnItem(590, log.id.toLong(), s@ @Suspendable {
				val logItem = Item(log.id)
				
				if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
					// Stop people spamming fires.
					it.messagebox("You can't use this skill on the PvP server.")
					return@s
				}
				if (it.player().attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
					it.message("You can't light a fire in here.")
					return@s
				}
				// Check level requirement
				if (it.player().skills()[Skills.FIREMAKING] < log.req) {
					val itemname = logItem.name(it.player().world()).toLowerCase()
					it.player().message("You need a Firemaking level of ${log.req} to burn $itemname.")
					return@s
				}
				
				// Check tile
				if (it.player().world().objByType(10, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
						|| it.player().world().objByType(11, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
						|| (it.player().world().floorAt(it.player().tile()) and 0x4) != 0) {
					it.message("You can't light a fire here.")
					return@s
				}
				var targTile = it.player().tile().transform(-1, 0, 0)
				var legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(-1, 0), 1)
				if (!legal) {
					targTile = it.player().tile().transform(1, 0, 0)
					legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(1, 0), 1)
					if (!legal) {
						return@s // No valid move to go!
					}
				}
				
				val catch = it.player().timers().has(TimerKey.FIRE_CATCHING)
				val slot: Int = ItemOnItem.slotOf(it, log.id)
				it.delay(1)
				
				// Drop the logs
				it.player().inventory().remove(logItem, true, slot)
				val spawnedItem = it.player().world().spawnGroundItem(GroundItem(it.player().world(), logItem, it.player().tile(), it.player().id()))
				
				if (catch && it.player().world().random(100) <= log.lightChance(it.player(), true)) {
					if (BonusContent.isActive(it.player, BlessingGroup.PYROMANIAC) && it.player.world().rollDie(2, 1)) {
						it.player.inventory().add(Item(spawnedItem.item().id()), true)
						it.player().message("The active Pyromaniac blessing lets you light some kindling instead.")
					} else {
						it.player().message("The fire catches and the logs begin to burn.")
					}
				} else {
					it.player().animate(733)
					it.player().message("You attempt to light the logs.")
					it.delay(3)
					
					while (it.player().world().random(100) > log.lightChance(it.player(), true)) {
						it.player().animate(733)
						it.delay(4)
					}
					
					if (BonusContent.isActive(it.player, BlessingGroup.PYROMANIAC) && it.player.world().rollDie(2, 1)) {
						it.player.inventory().add(Item(spawnedItem.item().id()), true)
						it.player().message("The active Pyromaniac blessing lets you light some kindling instead.")
					} else {
						it.player().message("The fire catches and the logs begin to burn.")
					}
				}
				
				// Remove the logs
				it.player().world().removeGroundItem(spawnedItem)
				
				// Set our three tick timer to catch the fire (like on RS :))
				it.player().timers()[TimerKey.FIRE_CATCHING] = 3
				
				// Spawn a fire that dies after a minute and walk away
				val fire = makeFire(it.player(), log.lifetime)
				it.player().animate(-1)
				it.player().walkTo(targTile, PathQueue.StepType.FORCED_WALK)
				it.player().lock()
				it.delay(1)
				it.player().unlock()
				it.player().faceObj(fire)
				
				// Give us some xp now, because.. dialogue.
				it.addXp(Skills.FIREMAKING, log.xp * pyromancerOutfitBonus(it.player()))
			})
			
			for (bows in LightingAnimation.values()) {
				r.onItemOnItem(bows.item.toLong(), log.id.toLong(), s@ @Suspendable {
					val logItem = Item(log.id)
					
					if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
						// Stop people spamming fires.
						it.messagebox("You can't use this skill on the PvP server.")
						return@s
					}
					if (it.player().attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
						it.message("You can't light a fire in here.")
						return@s
					}
					// Check level requirement
					if (it.player().skills()[Skills.FIREMAKING] < log.barb_req) {
						val itemname = logItem.name(it.player().world()).toLowerCase()
						it.player().message("You need a Firemaking level of ${log.barb_req} to burn $itemname in a barbarian fashion.")
						return@s
					}
					
					// Check tile
					if (it.player().world().objByType(10, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
							|| it.player().world().objByType(11, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
							|| (it.player().world().floorAt(it.player().tile()) and 0x4) != 0) {
						it.message("You can't light a fire here.")
						return@s
					}
					var targTile = it.player().tile().transform(-1, 0, 0)
					var legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(-1, 0), 1)
					if (!legal) {
						targTile = it.player().tile().transform(1, 0, 0)
						legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(1, 0), 1)
						if (!legal) {
							return@s // No valid move to go!
						}
					}
					
					val catch = it.player().timers().has(TimerKey.FIRE_CATCHING)
					val slot: Int = ItemOnItem.slotOf(it, log.id)
					it.delay(1)
					
					// Drop the logs
					it.player().inventory().remove(logItem, true, slot)
					val spawnedItem = it.player().world().spawnGroundItem(GroundItem(it.player().world(), logItem, it.player().tile(), it.player().id()))
					
					if (catch && it.player().world().random(100) <= log.lightChance(it.player(), true)) {
						if (BonusContent.isActive(it.player, BlessingGroup.PYROMANIAC) && it.player.world().rollDie(2, 1)) {
							it.player.inventory().add(Item(spawnedItem.item().id()), true)
							it.player().message("The active Pyromaniac blessing lets you light some kindling instead.")
						} else {
							it.player().message("The fire catches and the logs begin to burn.")
						}
					} else {
						it.player().animate(getLighter(it))
						it.player().message("You attempt to light the logs.")
						it.delay(3)
						
						while (it.player().world().random(100) > log.lightChance(it.player(), true)) {
							it.player().animate(getLighter(it))
							it.delay(4)
						}
						
						if (BonusContent.isActive(it.player, BlessingGroup.PYROMANIAC) && it.player.world().rollDie(2, 1)) {
							it.player.inventory().add(Item(spawnedItem.item().id()), true)
							it.player().message("The active Pyromaniac blessing lets you light some kindling instead.")
						} else {
							it.player().message("The fire catches and the logs begin to burn.")
						}
					}
					
					// Remove the logs
					it.player().world().removeGroundItem(spawnedItem)
					
					// Set our three tick timer to catch the fire (like on RS :))
					it.player().timers()[TimerKey.FIRE_CATCHING] = 3
					
					// Spawn a fire that dies after a minute and walk away
					val fire = makeFire(it.player(), log.lifetime)
					it.player().animate(-1)
					it.player().walkTo(targTile, PathQueue.StepType.FORCED_WALK)
					it.player().lock()
					it.delay(1)
					it.player().unlock()
					it.player().faceObj(fire)
					
					// Give us some xp now, because.. dialogue.
					it.addXp(Skills.FIREMAKING, log.xp * pyromancerOutfitBonus(it.player()))
				})
			}
			
			r.onGroundItemOption2(log.id, s@ @Suspendable {
				val logItem = Item(log.id)
				val logs: GroundItem? = it.player().attrib(AttributeKey.INTERACTED_GROUNDITEM)
				
				
				if (getLighter(it) == -1) {
					it.player().message("You do not have a suitable lighting method, and the logs won't light themselves.")
					return@s
				}
				
				if (it.player().world().realm().isPVP && !it.player().privilege().eligibleTo(Privilege.ADMIN)) {
					// Stop people spamming fires.
					it.messagebox("You can't use this skill on the PvP server.")
					return@s
				}
				if (it.player().attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
					it.message("You can't light a fire in here.")
					return@s
				}
				// Check level requirement
				if (it.player().skills()[Skills.FIREMAKING] < log.req) {
					val itemname = logItem.name(it.player().world()).toLowerCase()
					it.player().message("You need a Firemaking level of ${log.req} to burn $itemname.")
					return@s
				}
				
				// Check tile
				if (it.player().world().objByType(10, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
						|| it.player().world().objByType(11, it.player().tile().x, it.player().tile().z, it.player().tile().level) != null
						|| (it.player().world().floorAt(it.player().tile()) and 0x4) != 0) {
					it.message("You can't light a fire here.")
					return@s
				}
				var targTile = it.player().tile().transform(-1, 0, 0)
				var legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(-1, 0), 1)
				if (!legal) {
					targTile = it.player().tile().transform(1, 0, 0)
					legal = nl.bartpelle.veteres.model.map.steroids.RouteFinder.isLegal(it.player().world(), it.player().tile(), Direction.orthogonal(1, 0), 1)
					if (!legal) {
						return@s // No valid move to go!
					}
				}
				
				val catch = it.player().timers().has(TimerKey.FIRE_CATCHING)
				it.delay(1)
				
				if (catch && it.player().world().random(100) <= log.lightChance(it.player(), true)) {
					if (BonusContent.isActive(it.player, BlessingGroup.PYROMANIAC) && it.player.world().rollDie(2, 1)) {
						it.player.inventory().add(Item(logs!!.item().id()), true)
						it.player().message("The active Pyromaniac blessing lets you light some kindling instead.")
					} else {
						it.player().message("The fire catches and the logs begin to burn.")
					}
				} else {
					it.player().animate(getLighter(it))
					it.player().message("You attempt to light the logs.")
					it.delay(3)
					
					while (it.player().world().random(100) > log.lightChance(it.player(), true)) {
						it.player().animate(getLighter(it))
						it.delay(4)
					}
					
					if (BonusContent.isActive(it.player, BlessingGroup.PYROMANIAC) && it.player.world().rollDie(2, 1)) {
						it.player.inventory().add(Item(logs!!.item().id()), true)
						it.player().message("The active Pyromaniac blessing lets you light some kindling instead.")
					} else {
						it.player().message("The fire catches and the logs begin to burn.")
					}
				}
				
				// Remove the logs
				it.player().world().removeGroundItem(logs)
				
				// Set our three tick timer to catch the fire (like on RS :))
				it.player().timers()[TimerKey.FIRE_CATCHING] = 3
				
				// Spawn a fire that dies after a minute and walk away
				val fire = makeFire(it.player(), log.lifetime)
				it.player().animate(-1)
				it.player().walkTo(targTile, PathQueue.StepType.FORCED_WALK)
				it.player().lock()
				it.delay(1)
				it.player().unlock()
				it.player().faceObj(fire)
				
				// Give us some xp now, because.. dialogue.
				it.addXp(Skills.FIREMAKING, log.xp * pyromancerOutfitBonus(it.player()))
			})
		}
	}
	
	fun pyromancerOutfitBonus(player: Player): Double {
		var bonus = 1.0
		
		val hat = player.equipment()[EquipSlot.HEAD]?.id() ?: -1
		val top = player.equipment()[EquipSlot.BODY]?.id() ?: -1
		val legs = player.equipment()[EquipSlot.LEGS]?.id() ?: -1
		val boots = player.equipment()[EquipSlot.FEET]?.id() ?: -1
		
		if (hat == 20708)
			bonus += 0.4
		if (top == 20704)
			bonus += 0.8
		if (legs == 20706)
			bonus += 0.6
		if (boots == 20710)
			bonus += 0.2
		
		//If we've got the whole set, it's an additional 0.5% exp bonus
		if (bonus >= 3.0)
			bonus += 0.5
		
		return bonus
	}
	
	@Suspendable fun makeFire(player: Player, life: Int): MapObj {
		val obj: MapObj = MapObj(player.tile(), 26185, 10, 0)
		val world = player.world()
		player.world().server().scriptExecutor().executeScript(world, @Suspendable { s ->
			s.ctx<World>().spawnObj(obj)
			s.delay(life + s.ctx<World>().random(15))
			s.ctx<World>().removeObjSpawn(obj)
			s.delay(1)
			world.spawnGroundItem(GroundItem(world, Item(592), obj.tile(), null))
		})
		return obj
	}
	
	fun getLighter(it: Script): Int {
		LightingAnimation.values().forEach { lighter ->
			if (it.player().inventory().has(lighter.item)) {
				return lighter.anim
			}
		}
		return -1
	}
}