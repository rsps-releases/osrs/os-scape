package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.interactionOption
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.content.exchange.ExchangeType
import nl.bartpelle.veteres.model.entity.player.content.exchange.GrandExchange
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import java.util.*

/**
 * Created by Bart on 10/28/2016.
 */
object GrandExchangeButtons {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		val exchangeBooths = arrayOf(10061, 590)
		for (id in exchangeBooths) {
			r.onObject(id) @Suspendable {
				val player = it.player()
				if (GrandExchange.enabled) {
					if (it.interactionOption() == 1) {
						it.player.grandExchange().display()
					} else if (it.interactionOption() == 3) {
						it.player().grandExchange().openCollectionBox()
					}
				} else {
					player.message("The Grand Exchange is not available at the moment.")
				}
			}
		}

		
		// Back button
		r.onButton(465, 4) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			it.player.grandExchange().display()
		}
		
		// Component buttons
		for (slot in 7..14) {
			r.onButton(465, slot) {
				val player = it.player()
				if (!GrandExchange.enabled) {
					player.message("The Grand Exchange is not available at the moment.")
					return@onButton
				}
				if (it.buttonSlot() == 2 && it.buttonAction() == 2) {
					it.player.grandExchange().abortOffer(slot - 7)
				} else if (it.buttonSlot() == 3) {
					it.player.grandExchange().showBuy(slot - 7)
				} else {
					it.player.grandExchange().showSell(slot - 7)
				}
			}
		}
		
		// Abort
		r.onButton(465, 22) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			it.player().grandExchange().abortOffer()
		}
		
		// Claim GE
		r.onButton(465, 23) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			val slot: Int = player.attrib<Int>(AttributeKey.BUTTON_SLOT) - 2
			val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
			val clickedId: Int = player.attrib(AttributeKey.ITEM_ID)
			val geslot = player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT) - 1
			
			claimItem(player, geslot, slot, option, clickedId, false)
		}
		
		r.onButton(465, 6) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
			
			for (i in 0..player.grandExchange().offers().size - 1)
				for (j in 0..1)
					claimItem(player, i, j, option + 1, -1, false)
		}
		
		// Claim collection box
		for (button in 5..12) {
			r.onButton(402, button) {
				val player = it.player()
				if (!GrandExchange.enabled) {
					player.message("The Grand Exchange is not available at the moment.")
					return@onButton
				}
				val slot: Int = player.attrib<Int>(AttributeKey.BUTTON_SLOT) - 3
				val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
				val clickedId: Int = player.attrib(AttributeKey.ITEM_ID)
				
				claimItem(player, button - 5, slot, option, clickedId, true)
			}
		}
		
		for (button in 3..4) {
			r.onButton(402, button) {
				val player = it.player()
				if (!GrandExchange.enabled) {
					player.message("The Grand Exchange is not available at the moment.")
					return@onButton
				}
				for (i in 0..player.grandExchange().offers().size - 1) claimItem(player, i, i, button - 1, -1, true)
			}
		}
		
		
		// History
		r.onButton(465, 3) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			it.player().grandExchange().openHistory()
		}
		
		
		// Back to exchange (from history)
		r.onButton(383, 2) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			it.player().grandExchange().display()
		}
		
		r.onButton(383, 3) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			var realSlot = player.attrib<Int>(AttributeKey.BUTTON_SLOT) ?: return@onButton
			realSlot /= 6
			
			val item = player.grandExchange().getHistoryItem(realSlot)
			if (item != null) {
				when (player.attrib<Int>(AttributeKey.BUTTON_ACTION)) {
					1 -> {
						val slot = player.grandExchange().checkSlot()
						if (slot > -1) {
							player.grandExchange().display()
							player.grandExchange().showBuyOffer(item.id())
						}
					}
					10 -> player.message(player.world().examineRepository().item(item.id()))
				}
			}
		}
		
		// Confirm
		r.onButton(465, 27) {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@onButton
			}
			val itemid = player.varps().varp(1151)
			val amount = player.varps().varbit(Varbit.GRAND_EXCHANGE_AMOUNT)
			val price = player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE)
			val slot = player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT) - 1
			when (player.attribOr<ExchangeType>(AttributeKey.GRANDEXCHANGE_MODE, ExchangeType.NULL)) {
				ExchangeType.SELL -> player.grandExchange().confirmSellOffer(slot, Item(itemid, amount), price)
				ExchangeType.BUY -> player.grandExchange().confirmBuyOffer(slot, Item(itemid, amount), price)
			}
		}
		
		r.onButton(467, 0) s@ {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@s
			}
			val slot: Int = player.attrib(AttributeKey.BUTTON_SLOT)
			val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
			val item = if (slot >= player.inventory().items().size) return@s else player.inventory()[slot] ?: return@s
			when (option) {
				1 -> player.grandExchange().showSellOffer(item)
				10 -> player.message(player.world().examineRepository().item(item.id()))
			}
		}
		
		r.onButton(465, 24, s@ @Suspendable {
			val player = it.player()
			if (!GrandExchange.enabled) {
				player.message("The Grand Exchange is not available at the moment.")
				return@s
			}
			val slot: Int = player.attrib(AttributeKey.BUTTON_SLOT)
			val buying = player.attribOr<ExchangeType>(AttributeKey.GRANDEXCHANGE_MODE, ExchangeType.NULL) == ExchangeType.BUY
			
			val itemid = player.varps().varp(1151)
			
			val invCount by lazy { player.inventory().count(itemid, Item(itemid, 1).note(player.world()).id()) }
			
			if (slot == 0) {
				player.invokeScript(750, "What would you like to buy?", 1, -1)
			} else if (slot in 1..7) {
				val inc: Long = when (slot) {
					1 -> -1
					2, 3 -> 1
					4 -> 10
					5 -> 100
					6 -> if (buying) 1000 else invCount.toLong()
					7 -> if (buying) it.inputInteger("How many do you wish to buy?").toLong() else it.inputInteger("How many do you wish to sell?").toLong()
					else -> return@s
				}
				val current = player.varps().varbit(Varbit.GRAND_EXCHANGE_AMOUNT).toLong()
				
				val realAmount = Math.min(Integer.MAX_VALUE.toLong(), current + inc).toInt()
				
				player.varps().varbit(Varbit.GRAND_EXCHANGE_AMOUNT, Math.min(if (buying) Integer.MAX_VALUE else invCount, if ((current == 1L && inc > 1) || slot == 7 || (slot == 6 && !buying)) inc.toInt() else realAmount))
			} else {
				val inc: Long = when (slot) {
					8 -> -1
					9 -> 1
					10 -> 5
					11 -> Item(itemid).realPrice(player.world()).toLong() //Get regular ge price
					12 -> it.inputInteger("Set a price for each item:").toLong()
					13 -> 5
					else -> return@s
				}
				
				val current: Long = player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE).toLong()
				val newAmt: Long = when (slot) {
					11, 12 -> inc
					10, 13 -> {
						var remain: Long = (current / 20.0).toLong()
						if (remain <= 0) remain = 1
						if (slot == 10)
							current - remain
						else
							current + remain
					}
					else -> current + inc
				}
				
				if (current == 0L) {
					player.message("You can't trade that item on the Grand Exchange.")
					player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE, 0)
					return@s
				}
				
				val realAmount = Math.min(Integer.MAX_VALUE.toLong(), newAmt).toInt()
				player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE, Math.max(1, realAmount))
			}
		})
		
		r.onInterfaceClose(465) {
			val player = it.player()
			player.varps().varbit(Varbit.GRAND_EXCHANGE_SLOT, -1)
			player.clearattrib(AttributeKey.GRANDEXCHANGE_MODE)
			player.varps().varbit(Varbit.GRAND_EXCHANGE_AMOUNT, 0)
			player.varps().varbit(Varbit.GRAND_EXCHANGE_PRICE, 0)
			player.varps().varp(1151, -1)
			player.invokeScript(299, 1, 1) // Closes the chatbox
			player.interfaces().closeMain();
			player.interfaces().closeById(467)
		}
	}
	
	@Suspendable fun claimItem(player: Player, geslot: Int, slot: Int, option: Int, item: Int, collectionBox: Boolean): Boolean {
		if (!GrandExchange.enabled) {
			player.message("The Grand Exchange is not available at the moment.")
			return false
		}
		if (geslot >= 0 && geslot < player.grandExchange().offers().size) {
			val offer by lazy { player.grandExchange().offers()[geslot] }
			if (offer == null || offer.id() == -1L || (offer.coins() <= 0 && offer.items() <= 0)) {
				return true //No items to remove, but lets not break the loop :)
			}
			when (option) {
				in 1..3 -> {
					val bank = option == 3
					
					var itemToAdd = offer.collectItems().get(slot)
					
					if (itemToAdd != null) {
						val notesOption = if (itemToAdd.amount() > 1 && itemToAdd.noteable(player.world())) 1 else 2
						
						if (!itemToAdd.stackable(player.world()) && option == notesOption)
							itemToAdd = itemToAdd.note(player.world())
						
						val result = if (bank) player.bank().add(itemToAdd, false) else player.inventory().add(itemToAdd, false)
						if (result.completed() > 0) {
							offer.collectItems().remove(Item(itemToAdd.unnote(player.world()).id(), result.completed()), false)
							
							if (itemToAdd.id() == player.currency()) offer.coins(offer.coins() - result.completed())
							else offer.items(offer.items() - result.completed())
							
							if (offer.collectItems().isEmpty && (offer.cancelled() || offer.completed() >= offer.requested())) {
								player.grandExchange().offers()[geslot] = null
								if (!collectionBox)
									player.grandExchange().display()
							}
							player.grandExchange().updateOffers()
						}
						if (!result.success()) {
							player.message("Your ${if (bank) "bank" else "inventory"} is too full.")
							return false
						}
					}
				}
				10 -> {
					player.message(player.world().examineRepository().item(item))
					return false
				}
			}
		}
		return false
	}
	
}