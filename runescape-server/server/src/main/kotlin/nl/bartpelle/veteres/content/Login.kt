package nl.bartpelle.veteres.content

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.interfaces.magicbook.TeleportNoReqs
import nl.bartpelle.veteres.content.items.MaxCape
import nl.bartpelle.veteres.content.lottery.Lottery
import nl.bartpelle.veteres.content.mechanics.MultiwayCombat
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.content.minigames.inferno.InfernoSession
import nl.bartpelle.veteres.content.minigames.tzhaarfightcaves.FightCaveGame
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.tutorial.Tutorial
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.player.ClanChat
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.net.message.game.command.InterfaceVisibility
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.net.message.game.command.UpdateStateCustom
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp
import java.sql.Timestamp
import java.time.Instant

/**
 * Created by Bart on 7/9/2015.
 */

object Login {
	
	@JvmStatic var SHOULD_REPORT_GENERIC_CRASHES = false
	
	@JvmStatic @ScriptMain fun login(repo: ScriptRepository) {
		repo.onLogin(@Suspendable {
			it.player().varps().varp(111, 9) // Unlock "Travel" on spirit tree
			it.player().varps().varbit(598, 2) // Unlock "Travel" on spirit tree
			it.player().varps().varp(150, 160) // Unlock "Travel" on spirit tree
			it.player().varps().varbit(4470, 1) // Unlock "Edgeville" option on Deserted Keep Lever
			it.player().varps().varp(Varp.CANOE_STATION, 0) // Resets on logout
			it.player().varps().varp(675, 0) // Resets on logout canoe #2?
			it.player().varps().varbit(Varbit.LOCK_CAMERA, 0)
			
			//Unlock all the emotes
			it.player().varps().varp(313, 16777215)
			it.player().varps().varbit(Varbit.UNLOCK_GOBLIN_BOW_AND_SALUTE_EMOTE, 7)
			it.player().varps().varbit(Varbit.UNLOCK_FLAP_EMOTE, 1)
			it.player().varps().varbit(Varbit.UNLOCK_SLAP_HEAD_EMOTE, 1)
			it.player().varps().varbit(Varbit.UNLOCK_IDEA_EMOTE, 1)
			it.player().varps().varbit(Varbit.UNLOCK_STAMP_EMOTE, 1)
			
			//If we're not on the realism world, set blocked task unlocks
			if (!it.player().world().realm().isRealism) {
				it.player().varps().varp(Varp.QUEST_POINTS, 250)
				it.player().varps().varbit(Varbit.UNLOCK_BLOCK_TASK_SIX, 1)
			}
			
			it.message("Welcome to OS-Scape. On here, you die.")
			
			if (!it.player().world().realm().isPVP && World.xpMultiplier > 0) {
				it.message("There is currently a ${World.xpMultiplier}x exp multiplier active!")
			}
			
			if (it.player().muted()) {
				val duration = 2000 + (it.player().mutedUntil.time - Timestamp.from(Instant.now()).time)
				it.message("You are muted for a further <col=7F00FF>${Login.duration(duration)}.")
			}
			
			it.player().invokeScript(1350)
			it.player().write(InvokeScript(389, it.player().skills().combatLevel()))

			
			if (Lottery.enabled() && Lottery.config?.winningsFor(it.player().id() as Int) ?: 0 > 0) {
				it.message("You have unclaimed lottery winnings! Please claim them from the financial advisor at Edgeville.")
			}
			
			// Attempt to rejoin CC
			if (it.player().attribOr<Int>(AttributeKey.CLAN_CHANNEL, -1) > 0) {
				ClanChat.join(it.player(), it.player().attribOr<Int>(AttributeKey.CLAN_CHANNEL, -1))
			} else {
				ClanChat.join(it.player(), "help") // Join help channel by default
			}

			//QuestTab.clearList(it.player())
			//it.player().questTab().update(it.player())
			it.player().journal().send(it.player())
			
			//Set the players energy level
			val energy = it.player().attribOr<Double>(AttributeKey.RUN_ENERGY, 100.0) // Full run for new accs.
			it.player().setRunningEnergy(energy, true)


			if (it.player().world().realm().isOSRune && it.player().varps().varp(Varp.TUTORIAL_STAGE) == 0) {
                it.player().world().server().scriptExecutor().executeScript(it.player(), Tutorial.script)
			} else {
                // Two-factor preach
                if (it.player().twofactorKey() == null || it.player().twofactorKey().isEmpty()) {
                    if (it.player().world().realm().isPVP) {
                        it.message("<col=ff0000>Your account is not secure. No refunds on hacking will be provided without two-factor!")
                        it.message("<col=ff0000>To get 15% extra Blood money per kill, set up your authenticator through ::2fa.")
                    } else {
                        it.message("<col=ff0000>Your account is not secure. Please set up two-factor authenticating through ::2fa and keep hackers at distance.")
                    }
                }
			}
			
			if (!it.player().world().realm().isPVP) {
				if (Equipment.wearingMaxCape(it.player())) {
					if (!MaxCape.hasTotalLevel(it.player())) {
						Bank.deposit(Int.MAX_VALUE, it.player(), null, it.player().equipment().get(EquipSlot.CAPE)!!, it.player().equipment())
						it.player().message("<col=FF0000>The total level required for the Max Cape has changed! Your worn cape has been banked.")
					}
				}
			}
			it.player().pathQueue().setDefaultLastStep()
			// Force fix any remaining bugged accounts
			if (it.player().varps().varbit(Varbit.MULTIWAY_AREA) == 1 && !MultiwayCombat.includes(it.player().tile())) {
				it.player().varps().varbit(Varbit.MULTIWAY_AREA, 0)
			}
			if (it.player().jailed() && it.player().tile().region() != 13103) { // Safety since it was possible to escape.
				it.player().world().server().scriptExecutor().executeScript(it.player(), TeleportNoReqs(Tile(3290, 3017)))
				it.player().message("You have been jailed. Mine ores and give to Irena to escape.")
			}
			
			// Spawn our pet on login
			PetAI.spawnOnLogin(it.player())
			
			// Show the wilderness overlay on login if Deadman.
			if (it.player().world().realm().isDeadman) {
				it.player().world().announce("<col=9C29C1>" + it.player().name() + " has just logged in!</col>")
				it.player().interfaces().sendWidgetOn(90, Interfaces.InterSwitches.T)
				val showCross = it.player().attribOr<Int>(AttributeKey.GENERAL_VARBIT1, 0) as Int shr 22 and 3 == 1
				it.player().write(InterfaceVisibility(Interfaces.SKULL, WildernessLevelIndicator.CROSSED_OUT_SKULL_CHILD_ID, showCross))
			}
			
			// Update the client state for if we're hiding/showing killstreak skulls (cosmetic optional)
			val off = VarbitAttributes.varbiton(it.player(), Varbit.KS_SKULLS_HIDDEN)
			it.player().write(UpdateStateCustom.skullToggle(!off))
			
			it.player().write(UpdateStateCustom.setErrorReportState(SHOULD_REPORT_GENERIC_CRASHES))
			
			val infernoWave = it.player().attribOr<Int>(AttributeKey.INFERNO_SAVED_WAVE, 0)
			if (infernoWave != 0) {
				val session = InfernoSession(infernoWave, it.player(), it.player().world())
				session.start(infernoWave, true)
			}
			
			//If our player was doing the Fight Caves Minigame, continue where they left off..
			val tzhaarWave = it.player().attribOr<Int>(AttributeKey.TZHAAR_FIGHT_CAVES_WAVE, 0)
			if (tzhaarWave != 0) {
				FightCaveGame.initiateFightCaves(it, true)
			}
			
			if (it.player().world().realm().isPVP) {
				it.itemBox("Get a headstart - collect your FREE <col=ff0000>Blood money</col>! For only two minutes of your time, you too can get a lot of free <col=ff0000>Blood money</col>! Visit ::vote and collect your rewards now.", 13307, 175)
			}
		})
	}
	
	@JvmStatic fun duration(duration: Long): String {
		var time = duration / 1000 // Seconds
		if (time < 60)
			return "$time second${if (time > 1) "s" else ""}"
		else {
			time = duration / 1000 / 60 // Minutes
			if (time < 60)
				return "$time minute${if (time > 1) "s" else ""}"
			else {
				time = duration / 1000 / 60 / 60 // Hours
				if (time < 24)
					return "$time hour${if (time > 1) "s" else ""}"
				else {
					time = duration / 1000 / 60 / 60 / 24 // Days
					return "$time day${if (time > 1) "s" else ""}"
				}
			}
		}
	}
}