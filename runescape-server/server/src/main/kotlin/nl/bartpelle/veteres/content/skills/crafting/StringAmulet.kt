package nl.bartpelle.veteres.content.skills.crafting

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/21/2015.
 */


object StringAmulet {
	
	enum class amulet(val unf: Int, val fin: Int, val lvl: Int, val exp: Double) {
		GOLD(1673, 1692, 8, 4.0),
		SAPPHIRE(1675, 1694, 24, 4.0),
		EMERALD(1677, 1696, 31, 4.0),
		RUBY(1679, 1698, 50, 4.0),
		DIAMOND(1681, 1700, 70, 4.0),
		DRAGONSTONE(1683, 1702, 80, 4.0),
		ONYX(6579, 6581, 90, 4.0),
		ZENYTE(19501, 19541, 98, 4.0);
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//For each amulet(u) in the players inventory do the following..
		amulet.values().forEach { amulet ->
			if (amulet.unf != -1) {
				val BALL_OF_WOOL = 1759
				
				//If the player uses an amulet(u) with a ball of wool we
				repo.onItemOnItem(amulet.unf.toLong(), BALL_OF_WOOL.toLong(), s@ @Suspendable {
					if (it.player().world().realm().isPVP) {
						it.messagebox("That's a bit pointless is it not...?")
						return@s
					}
					//Check if the player has the required level to string the amulet
					if (it.player().skills()[Skills.CRAFTING] < amulet.lvl) {
						it.messagebox("You need a Crafting level of ${amulet.lvl} to string this amulet.")
						return@s
					}
					
					//Prompt the player with the # they'd like to cut
					var num = 1
					if (it.player().inventory().count(amulet.unf) > 1) {
						// By default, 1, if we have more, ask how many.
						num = it.itemOptions(Item(amulet.fin, 150), offsetX = 0)
					}
					
					while (num-- > 0) {
						//Check if the player still has an amulet(u) and ball of wool
						if (amulet.unf !in it.player().inventory() || BALL_OF_WOOL !in it.player().inventory()) {
							break
						}
						
						//Remove the amulet(u), add a finished amulet, send message, add experience and set delay.
						it.player().inventory() -= amulet.unf
						it.player().inventory() -= BALL_OF_WOOL
						it.player().inventory() += amulet.fin
						it.message("You put some string on your amulet.")
						it.addXp(Skills.CRAFTING, amulet.exp)
						it.delay(2)
					}
				})
			}
		}
	}
}