package nl.bartpelle.veteres.content.areas.burthorpe.rogues_den

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 5/16/2016.
 */
object RoguesDenEntrance {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(7257) { it.player().teleport(3061, 4985, 1) }
		r.onObject(7258) { it.player().teleport(2906, 3537) }
	}
	
}