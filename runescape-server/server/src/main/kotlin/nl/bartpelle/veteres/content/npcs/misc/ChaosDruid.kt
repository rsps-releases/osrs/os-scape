package nl.bartpelle.veteres.content.npcs.misc

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 11/13/2015.
 */

object ChaosDruid {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		val target = EntityCombat.getTarget(it) ?: return@s
		
		//Determine if we cast the spell or attack the target
		if (npc.world().rollDie(2, 1)) {
			//Cast the spell on the target
			spell(npc, target)
		} else {
			//Else we attack the target with a melee attack
			if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
				//We hit the player!
				target.hit(npc, EntityCombat.randomHit(npc))
			} else {
				//Uh-oh, that's a miss.
				target.hit(npc, 0)
			}
			//Animate the NPC
			npc.animate(npc.attackAnimation())
		}
		//Entity combat delay
		EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
	}
	
	//Cast the spell on the target
	fun spell(npc: Npc, target: Entity) {
		val tileDist = npc.tile().transform(3, 3, 0).distance(target.tile())
		val delay = Math.max(1, 20 + tileDist * 12 / 30)
		val player = target as Player
		
		//Determine of we successfully cast the spell on the target or not
		if (npc.world().rollDie(2, 1)) {
			//We succeed! Send the player a message
			target.message("You feel weakened.")
			//Remove the players attack level by 1
			player.skills().alterSkill(Skills.ATTACK, -1.toInt())
			//Send the graphic on the target
			target.graphic(181, 124, delay)
		} else {
			//Else we splash, and cast the splash graphic on the target
			target.graphic(85, 124, delay)
		}
		//Animate the NPC with the spell casting animation regardless
		npc.animate(710)
	}
}


