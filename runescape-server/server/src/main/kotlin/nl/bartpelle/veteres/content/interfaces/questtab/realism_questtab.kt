package nl.bartpelle.veteres.content.interfaces.questtab

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 09/11/2016.
 */
object realism_questtab {
	
	fun setupForRealm(r: ScriptRepository) {
		r.onButton(Interfaces.QUEST_TAB, 2) {
			// Minigames
			if (it.player().privilege().eligibleTo(Privilege.ADMIN))
				when (it.options("Uptime", "Wildy count")) {
					1 -> QuestTabButtons.displayServerUptime(it)
					2 -> QuestTabButtons.displayWildernessBreakdown(it) // (admins)
				}
			
		}
		r.onButton(Interfaces.QUEST_TAB, 7) @Suspendable {
			// Realism hook
			QuestTabButtons.ECO_HANDLERS_FREE[it.buttonSlot()]?.invoke(it)
		}
		r.onButton(Interfaces.QUEST_TAB, 8) @Suspendable {
			QuestTabButtons.ECO_HANDLERS_MEMBER[it.buttonSlot()]?.invoke(it)
		}
	}
}