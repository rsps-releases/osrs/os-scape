package nl.bartpelle.veteres.content.items.combine

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking
import nl.bartpelle.veteres.content.minusAssign
import nl.bartpelle.veteres.content.plusAssign
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 22/02/2016.
 */
object GodswordParts {
	
	val SHARD1: Int = 11818
	val SHARD2: Int = 11820
	val SHARD3: Int = 11822
	val SHARD1_AND_2: Int = 11794
	val SHARD1_AND_3: Int = 11796
	val SHARD2_AND_3: Int = 11800
	val BLADE: Int = 11798
	val SARA_HILT: Int = 11814
	val ARMA_HILT: Int = 11810
	val ZAMMY_HILT: Int = 11816
	val BANDOS_HILT: Int = 11812
	val AGS: Int = 11802
	val BGS: Int = 11804
	val SGS: Int = 11806
	val ZGS: Int = 11808
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onItemOnItem(SHARD1, SHARD2) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().has(SHARD1) && player.inventory().has(SHARD2)) {
				player.inventory() -= SHARD1
				player.inventory() -= SHARD2
				player.inventory() += SHARD1_AND_2
			}
		}
		
		repo.onItemOnItem(SHARD1, SHARD3) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(SHARD1, SHARD3)) {
				player.inventory() -= SHARD1
				player.inventory() -= SHARD3
				player.inventory() += SHARD1_AND_3
			}
		}
		
		repo.onItemOnItem(SHARD2, SHARD3) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(SHARD2, SHARD3)) {
				player.inventory() -= SHARD2
				player.inventory() -= SHARD3
				player.inventory() += SHARD2_AND_3
			}
		}
		
		repo.onItemOnItem(SHARD1, SHARD2_AND_3) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(SHARD1, SHARD2_AND_3)) {
				player.inventory() -= SHARD1
				player.inventory() -= SHARD2_AND_3
				player.inventory() += BLADE
			}
		}
		
		repo.onItemOnItem(SHARD2, SHARD1_AND_3) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(SHARD2, SHARD1_AND_3)) {
				player.inventory() -= SHARD2
				player.inventory() -= SHARD1_AND_3
				player.inventory() += BLADE
			}
		}
		
		repo.onItemOnItem(SHARD3, SHARD1_AND_2) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(SHARD3, SHARD1_AND_2)) {
				player.inventory() -= SHARD3
				player.inventory() -= SHARD1_AND_2
				player.inventory() += BLADE
			}
		}
		
		repo.onItemOnItem(BLADE, SARA_HILT) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(BLADE, SARA_HILT)) {
				player.inventory() -= BLADE
				player.inventory() -= SARA_HILT
				player.inventory() += SGS
			}
		}
		
		repo.onItemOnItem(BLADE, ARMA_HILT) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(BLADE, ARMA_HILT)) {
				player.inventory() -= BLADE
				player.inventory() -= ARMA_HILT
				player.inventory() += AGS
			}
		}
		
		repo.onItemOnItem(BLADE, ZAMMY_HILT) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(BLADE, ZAMMY_HILT)) {
				player.inventory() -= BLADE
				player.inventory() -= ZAMMY_HILT
				player.inventory() += ZGS
			}
		}
		
		repo.onItemOnItem(BLADE, BANDOS_HILT) @Suspendable {
			val player = it.player()
			if (Staking.screen_closed(player) && player.inventory().hasAll(BLADE, BANDOS_HILT)) {
				player.inventory() -= BLADE
				player.inventory() -= BANDOS_HILT
				player.inventory() += BGS
			}
		}
		
		repo.onItemOption3(AGS, s@ @Suspendable {
			val player = it.player()
			if (player.world().realm().isPVP) {
				player.message("You can't disable Godswords on the PvP world.")
				return@s
			}
			if (Staking.screen_closed(player) && player.inventory().has(AGS) && player.inventory().freeSlots() >= 2) {
				player.inventory() -= AGS
				player.inventory() += ARMA_HILT
				player.inventory() += BLADE
			}
		})
		
		repo.onItemOption3(BGS, s@ @Suspendable {
			val player = it.player()
			if (player.world().realm().isPVP) {
				player.message("You can't disable Godswords on the PvP world.")
				return@s
			}
			if (Staking.screen_closed(player) && player.inventory().has(BGS) && player.inventory().freeSlots() >= 2) {
				player.inventory() -= BGS
				player.inventory() += BANDOS_HILT
				player.inventory() += BLADE
			}
		})
		
		repo.onItemOption3(SGS, s@ @Suspendable {
			val player = it.player()
			if (player.world().realm().isPVP) {
				player.message("You can't disable Godswords on the PvP world.")
				return@s
			}
			if (Staking.screen_closed(player) && player.inventory().has(SGS) && player.inventory().freeSlots() >= 2) {
				player.inventory() -= SGS
				player.inventory() += SARA_HILT
				player.inventory() += BLADE
			}
		})
		
		repo.onItemOption3(ZGS, s@ @Suspendable {
			val player = it.player()
			if (player.world().realm().isPVP) {
				player.message("You can't disable Godswords on the PvP world.")
				return@s
			}
			if (Staking.screen_closed(player) && player.inventory().has(ZGS) && player.inventory().freeSlots() >= 2) {
				player.inventory() -= ZGS
				player.inventory() += ZAMMY_HILT
				player.inventory() += BLADE
			}
		})
	}
}