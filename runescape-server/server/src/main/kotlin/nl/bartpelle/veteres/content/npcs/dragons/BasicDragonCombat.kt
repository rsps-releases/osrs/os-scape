package nl.bartpelle.veteres.content.npcs.dragons

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.items.equipment.DragonfireShield
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * @author Savions Sw, the not so legendary.
 * with a good ol' remake by the legend himself: Situations.
 */

object BasicDragonCombat {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackMelee(npc, target, true) && EntityCombat.attackTimerReady(npc)) {
				if (target.world().rollDie(2, 1))
					breath_fire(npc, target)
				else
					regular_hit(npc, target)
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed + 1)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	
	@Suspendable fun regular_hit(npc: Npc, target: Entity) {
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, EntityCombat.randomHit(npc))
		else
			target.hit(npc, 0)
		
		npc.animate(npc.attackAnimation())
	}
	
	@Suspendable fun breath_fire(npc: Npc, target: Entity) {
		var max = 50.0
		val antifire_charges = target.attribOr<Int>(AttributeKey.ANTIFIRE_POTION, 0)
		val hasShield = false
		val hasPotion = false

		if (max > 0 && antifire_charges > 1000) {
			target.message("Your super antifire potion protects you completely from the heat of the dragon's breath!")
			max = 0.0
		}
		
		//Does our player have an anti-dragon shield?
		if (max > 0 && (target.equipment().hasAt(EquipSlot.SHIELD, 11283) || target.equipment().hasAt(EquipSlot.SHIELD, 11284) ||
				target.equipment().hasAt(EquipSlot.SHIELD, 1540))) {
			target.message("Your shield absorbs most of the dragon fire!")
			max *= 0.3
		}
		
		//Has our player recently consumed an antifire potion?
		if (max > 0 && antifire_charges > 0) {
			target.message("Your potion protects you from the heat of the dragon's breath!")
			max *= 0.3
		}
		
		//Is our player using protect from magic?
		if (max > 0 && target.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1) {
			target.message("Your prayer absorbs most of the dragon's breath!")
			max *= 0.6
		}
		
		if (hasShield && hasPotion) {
			max == 0.0
		}
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE))
			target.hit(npc, target.world().random(max.toInt())).combatStyle(CombatStyle.MAGIC)
		else
			target.hit(npc, 0, 1)

		npc.animate(81)
		npc.graphic(1, 100, 0)

		if (target is Player && DragonfireShield.canCharge(target)) {
			DragonfireShield.charge(target)
		}
	}
}