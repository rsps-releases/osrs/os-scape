package nl.bartpelle.veteres.content.treasuretrails

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemUsedSlot
import nl.bartpelle.veteres.content.widgetHash
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations & Jak on 2016-11-04.
 */

object TreasureTrailPuzzleBoxes {
	/**
	 * TODO: Need to add saving and checks to not scramble if a puzzle is in progress.
	 * TODO: Need to also add the "hint" hover to show the solved puzzle.
	 * TODO: Need to add a check to see if the player has completed the puzzle.
	 * TODO: Need to make the "empty" area random instead of always at the bottom right.
	 * TODO: Need to find the correct messages sent to the player for solving, wrong piece, etc..
	 */
	
	enum class SliderMovePossibilities {
		UP, DOWN, LEFT, RIGHT, NONE
	}
	
	val PUZZLE_INTERFACE = 363
	
	val CASTLE_PUZZLE = 2800
	val castlePuzzlePieces = arrayListOf(2749, 2750, 2751, 2752, 2753, 2754, 2755, 2756, 2757, 2758, 2759, 2760,
			2761, 2762, 2763, 2764, 2765, 2766, 2767, 2768, 2769, 2770, 2771, 2772)
	
	val TREE_PUZZLE = 3565
	val treePuzzlePieces = arrayListOf(3619, 3620, 3621, 3622, 3623, 3624, 3625, 3626, 3627, 3628, 3629, 3630, 3631,
			3632, 3633, 3634, 3635, 3636, 3637, 3638, 3639, 3640, 3641, 3642)
	
	val TROLL_PUZZLE = 3571
	val trollPuzzlePieces = arrayListOf(3643, 3644, 3645, 3646, 3647, 3648, 3649, 3650, 3651, 3652, 3653, 3654, 3655,
			3656, 3657, 3658, 3659, 3660, 3661, 3662, 3663, 3664, 3665, 3666)
	
	val ZULRAH_PUZZLE = 20280
	val zulrahPuzzlePieces = arrayListOf(20283, 20284, 20285, 20286, 20287, 20288, 20289, 20290, 20291, 20292, 20293,
			20294, 20295, 20296, 20297, 20298, 20299, 20300, 20301, 20302, 20303, 20304, 20305, 20306)
	
	val CERBERUS_PUZZLE = 20281
	val cerberusPuzzlePieces = arrayListOf(20307, 20308, 20309, 20310, 20311, 20312, 20313, 20314, 20315, 20316, 20317,
			20318, 20319, 20320, 20321, 20322, 20323, 20324, 20325, 20326, 20327, 20328, 20329, 20330)
	
	val GNOME_CHILD_PUZZLE = 20282
	val gnomeChildPuzzlePieces = arrayListOf(20331, 20332, 20333, 20334, 20335, 20336, 20337, 20338, 20339, 20340,
			20341, 20342, 20343, 20344, 20345, 20346, 20347, 20348, 20349, 20350, 20351, 20352, 20353, 20354)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Handle opening the puzzle boxes..
		r.onItemOption1(CASTLE_PUZZLE) @Suspendable { openPuzzleBox(it.player(), CASTLE_PUZZLE) }
		r.onItemOption1(TREE_PUZZLE) @Suspendable { openPuzzleBox(it.player(), TREE_PUZZLE) }
		r.onItemOption1(TROLL_PUZZLE) @Suspendable { openPuzzleBox(it.player(), TROLL_PUZZLE) }
		r.onItemOption1(ZULRAH_PUZZLE) @Suspendable { openPuzzleBox(it.player(), ZULRAH_PUZZLE) }
		r.onItemOption1(CERBERUS_PUZZLE) @Suspendable { openPuzzleBox(it.player(), CERBERUS_PUZZLE) }
		r.onItemOption1(GNOME_CHILD_PUZZLE) @Suspendable { openPuzzleBox(it.player(), GNOME_CHILD_PUZZLE) }
		
		//Handle clicking the puzzle box pieces..
		for (slider in castlePuzzlePieces) {
			r.onItemOption5(slider) { moveSlider(it) }
		}
		for (slider in treePuzzlePieces) {
			r.onItemOption5(slider) { moveSlider(it) }
		}
		for (slider in trollPuzzlePieces) {
			r.onItemOption5(slider) { moveSlider(it) }
		}
		for (slider in zulrahPuzzlePieces) {
			r.onItemOption5(slider) { moveSlider(it) }
		}
		for (slider in cerberusPuzzlePieces) {
			r.onItemOption5(slider) { moveSlider(it) }
		}
		for (slider in gnomeChildPuzzlePieces) {
			r.onItemOption5(slider) { moveSlider(it) }
		}
	}
	
	private fun moveSlider(it: Script) {
		//Is this a valid puzzle?
		val interfaceHash = it.widgetHash()
		if (interfaceHash != (363 shl 16) + 1) {
			it.player().debug("Bad puzzle interface")
			return
		}
		
		//Is the slot clicked an available slot?
		val slot = it.itemUsedSlot()
		if (slot < 0 || slot > 24)
			return
		
		val direction = findAvailableMoves(it, slot)
		val puzzlePieces = it.player().treasureTrailPuzzlePieces()
		val puzzle = puzzlePieces.get(slot)
		
		when (direction) {
			SliderMovePossibilities.UP -> movePuzzlePiece(puzzle, puzzlePieces, slot - 5)
			SliderMovePossibilities.DOWN -> movePuzzlePiece(puzzle, puzzlePieces, slot + 5)
			SliderMovePossibilities.LEFT -> movePuzzlePiece(puzzle, puzzlePieces, slot - 1)
			SliderMovePossibilities.RIGHT -> movePuzzlePiece(puzzle, puzzlePieces, slot + 1)
			SliderMovePossibilities.NONE -> {
				if (it.player().world().rollDie(3, 1))
					it.message("You can't move the slider in that direction.")
			}
		}
		
		it.player().write(SetItems(112, 363, 1, it.player().treasureTrailPuzzlePieces()))
	}
	
	private fun movePuzzlePiece(puzzle: Item, puzzlePieces: ItemContainer, toMove: Int) {
		puzzlePieces.remove(puzzle, true)
		puzzlePieces.add(puzzle, true, toMove)
	}
	
	private fun findAvailableMoves(it: Script, slot: Int): SliderMovePossibilities {
		if (slot < 0 || slot > 24)
			return SliderMovePossibilities.NONE
		
		val puzzle = it.player().treasureTrailPuzzlePieces()
		val puzzlePieces = puzzle.items()
		
		if (slot > 0 && puzzlePieces[slot - 1] == null)
			return SliderMovePossibilities.LEFT
		if (slot < puzzle.size() - 1 && puzzlePieces[slot + 1] == null)
			return SliderMovePossibilities.RIGHT
		if (slot >= 5 && puzzlePieces[slot - 5] == null)
			return SliderMovePossibilities.UP
		if (slot < puzzle.size() - 5 && puzzlePieces[slot + 5] == null)
			return SliderMovePossibilities.DOWN
		
		return SliderMovePossibilities.NONE
	}
	
	private fun openPuzzleBox(player: Player, puzzle: Int) {
		player.interfaces().sendMain(PUZZLE_INTERFACE)
		scramblePuzzlePieces(player, getPiecesForPuzzle(puzzle))
		player.write(SetItems(112, 363, 1, player.treasureTrailPuzzlePieces()))
	}
	
	private fun getPiecesForPuzzle(puzzle: Int): ArrayList<Int> {
		when (puzzle) {
			CASTLE_PUZZLE -> return castlePuzzlePieces
			TREE_PUZZLE -> return treePuzzlePieces
			TROLL_PUZZLE -> return trollPuzzlePieces
			ZULRAH_PUZZLE -> return zulrahPuzzlePieces
			CERBERUS_PUZZLE -> return cerberusPuzzlePieces
			GNOME_CHILD_PUZZLE -> return gnomeChildPuzzlePieces
			
			else -> return castlePuzzlePieces
		}
	}
	
	private fun scramblePuzzlePieces(player: Player, puzzlePieces: ArrayList<Int>) {
		val puzzlePieces: List<Int> = puzzlePieces.toList()
		Collections.shuffle(puzzlePieces)
		
		val puzzle = ItemContainer(player.world(), 25, ItemContainer.Type.REGULAR)
		for (i in 0..puzzlePieces.size - 1) {
			puzzle.add(Item(puzzlePieces[i]), true)
		}
		
		player.putattrib(AttributeKey.TREASURE_TRAIL_PUZZLE, puzzle)
	}
	
	fun Player.treasureTrailPuzzlePieces(): ItemContainer {
		val offer = attribOr<ItemContainer>(AttributeKey.TREASURE_TRAIL_PUZZLE, null)
		if (offer != null)
			return offer
		
		val container = ItemContainer(world(), 25, ItemContainer.Type.REGULAR)
		
		putattrib(AttributeKey.TREASURE_TRAIL_PUZZLE, container)
		return container
	}
	
}
