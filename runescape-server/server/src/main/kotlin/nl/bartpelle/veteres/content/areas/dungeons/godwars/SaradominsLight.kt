package nl.bartpelle.veteres.content.areas.dungeons.godwars

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 6/11/2016.
 */

object SaradominsLight {
	
	const val LIGHT = 13256
	
	@ScriptMain @JvmStatic fun register(r: ScriptRepository) {
		r.onItemOption1(LIGHT) @Suspendable {
			if (it.player().varps().varbit(Varbit.SARADOMINS_LIGHT) == 1) {
				it.itemBox("You have already been enlightened by the light of<br>Saradomin. Perhaps you could give it to someone else.", LIGHT)
			} else {
				it.itemBox("As you commune with the holy star, you feel the light<br>" +
						"of Saradomin preparing to fill your mind and enlighten<br>" +
						"your vision. If you submit, it will help you see through<br>the darkness of Zamorak's evil.", LIGHT)
				
				when (it.optionsTitled("Submit to the light of Saradomin?", "Yes, let the light in.", "No, cancel.")) {
					1 -> {
						it.player().inventory().remove(Item(LIGHT), true)
						it.player().invokeScript(1068)
						it.player().interfaces().send(193, 162, 550, false)
						it.player().varps().varbit(Varbit.SARADOMINS_LIGHT, 1)
						it.itemBox("You submit to the light of Saradomin.<br>Zamorak's darkness will henceforth have no effect on<br>you.", LIGHT)
					}
				}
			}
		}
	}
	
}