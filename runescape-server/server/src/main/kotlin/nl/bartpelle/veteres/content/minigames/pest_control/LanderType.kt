package nl.bartpelle.veteres.content.minigames.pest_control

import nl.bartpelle.veteres.model.Tile

/**
 * Created by Jason MacKeigan on 2016-07-12 at 4:08 PM
 *
 * Each element of the enumeration represents a type of lander in the pest control mini game.
 * Each type has a combat level requirement.
 */
enum class LanderType(val difficulty: PestControlDifficulty, val combatRequirement: Int, val gankplankIn: Int,
                      val gankplankOut: Int, val inside: Tile, val outside: Tile) {
	NOVICE(PestControlDifficulty.NOVICE, 40, 14315, 14314, Tile(2660, 2639), Tile(2657, 2639)),
	
	INTERMEDIATE(PestControlDifficulty.INTERMEDIATE, 70, 25631, 25629, Tile(2641, 2644), Tile(2644, 2644)),
	
	VETERAN(PestControlDifficulty.VETERAN, 100, 25632, 25630, Tile(2635, 2653), Tile(2638, 2653))
}