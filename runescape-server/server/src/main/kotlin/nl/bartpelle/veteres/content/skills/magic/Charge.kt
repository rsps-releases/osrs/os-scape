package nl.bartpelle.veteres.content.skills.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.addXp
import nl.bartpelle.veteres.content.combat.magic.MagicCombat
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 3/26/2016.
 */

object Charge {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onButton(218, 60, s@ @Suspendable {
			
			val runes = arrayOf(Item(554, 3), Item(565, 3), Item(556, 3))
			
			if (it.player().skills().level(Skills.MAGIC) >= 80) {
				if (it.player().timers().has(TimerKey.CHARGE_SPELL)) {
					it.player().message("You can't recast that yet, your current Charge is too strong.")
				} else {
					if (!MagicCombat.has(it.player(), runes, true)) {
						return@s
					}
					it.player().timers().register(TimerKey.CHARGE_SPELL, 200)
					it.player().message("You feel charged with magic power.")
					it.player().animate(811)
					it.player().graphic(111, 130, 3)
					it.addXp(Skills.MAGIC, 180.0)
				}
			} else {
				it.message("You need a Magic level of 80 to cast this spell.")
			}
		})
	}
}