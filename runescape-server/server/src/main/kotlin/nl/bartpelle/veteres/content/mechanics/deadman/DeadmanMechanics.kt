package nl.bartpelle.veteres.content.mechanics.deadman

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.destroyItem
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.content.itemUsedSlot
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.content.mechanics.deadman.safezones.DmmZones
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.CombatStyle
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 03/11/2016.
 *
 * Handle other general DMM mechanics not in other classes.
 * Spawn world objects on world init, destroy carried keys, spawn guards when a player enters a Safe zone when skulled.
 */
object DeadmanMechanics {
	
	// Item ID of DMM keys.
	val KEYS = intArrayOf(13302, 13303, 13304, 13305, 13306)
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		// Spawn some objects. Bank chests.
		repo.onWorldInit {
			val world = it.ctx<World>()
			if (world.realm().isDeadman) {
				world.spawnObj(MapObj(Tile(3147, 3510), 27290, 10, 1))
			}
		}
		
		// Destory option on bank key
		for (key in KEYS) {
			repo.onItemOption5(key) @Suspendable {
				val key = it.itemUsed()
				val slot = it.itemUsedSlot()
				it.player().stopActions(false)
				if (it.destroyItem("Are you sure you want to destroy this key?", "Bank key", "Your right to access the associated bank chest will be forfeit.", key)) {
					DeadmanMechanics.waitBeforeDestroying(it)
					if (it.player().inventory().remove(key, true, slot).success()) {
						it.message("Poof! It's gone!")
						it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, Math.max(0, it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED) - 1))
						it.player().looks().update()
					}
				}
			}
		}
		
		// Every 30s of the deadman skull timer
		repo.onTimer(TimerKey.DEADMAN_SKULL_TIMER) {
			it.player().varps().varbit(Varbit.DEADMAN_SKULL_TIMER, it.player().varps().varbit(Varbit.DEADMAN_SKULL_TIMER) - 1)
			val cyclesLeft = it.player().varps().varbit(Varbit.DEADMAN_SKULL_TIMER)
			if (cyclesLeft == 0) {
				// Was the last cycle.. clear attrib.
				Skulling.unskull(it.player())
				it.message("Your Skull timer has <col=FF0000>finished</col> and you can enter Guarded zones.")
			} else {
				it.player().timers().extendOrRegister(TimerKey.DEADMAN_SKULL_TIMER, 50) // 30 seconds, 50 ticks.
			}
		}
		
	}
	
	/**
	 * Grab the DeadmanContainer instance which itself links together the multiple ItemContainers used in Deadman mode per player.
	 */
	@JvmStatic fun infoForPlayer(player: Player): DeadmanContainers {
		val info = player.attribOr<DeadmanContainers>(AttributeKey.DEADMAN_INFO, DeadmanContainers(player))
		player.putattrib(AttributeKey.DEADMAN_INFO, info)
		return info
	}
	
	/**
	 * When picking up a key from the floor, obtain the BankKey and items that are linked with it.
	 */
	fun pickupKey(it: Script, item: GroundItem) {
		val count = when (item.item().id()) {
			13302 -> 1
			13303 -> 2
			13304 -> 3
			13305 -> 4
			13306 -> 5
			else -> 1
		}
		it.player().varps().varbit(Varbit.DEADMAN_KEYS_CARRIED, count)
		it.player().looks().update()
		val dmminfo = infoForPlayer(it.player())
		dmminfo.keys[count - 1] = item.dmm_key()
		item.linkDmmBankKey(null)
	}
	
	// When you logout, wait 7 seconds before logging
	@JvmStatic @Suspendable fun attemptLogout(it: Script) {
		waitForAction(it, "logout")
	}
	
	// When you attempt to teleport, wait 7 seconds before doing so
	@JvmStatic @Suspendable fun attemptTeleport(it: Script) {
		waitForAction(it, "teleport")
	}
	
	// Wait 7s before allowing key to be destroyed - Source: http://puu.sh/s81CV/cd8782dd3a.jpg
	@JvmStatic @Suspendable fun waitBeforeDestroying(it: Script) {
		waitForAction(it, "destroy this key", title = "Trying to destroy your winnings?")
	}
	
	/**
	 * Run a clever ClientScript that tells the player they must wait X seconds before doing a given action.
	 */
	private fun waitForAction(it: Script, action: String = "teleport", title: String = "Running away? Not so fast!") {
		if (it.player().world().realm().isDeadman) {
			//it.player().debug("dmm tele %s %d", it.player().world().realm().isDeadman, it.player().attribOr<Int>(AttributeKey.SKULL, 0))
			if (Skulling.skulled(it.player()) || PlayerCombat.inCombat(it.player())) {
				it.player().interfaces().send(228, 162, 550, false)
				it.player().write(InvokeScript(1149, "before you can $action...", 11, title, 1))
				it.onInterrupt @Suspendable { s ->
					s.player().interfaces().close(162, 550)
					s.player().interfaces().closeById(94) // Destroy item interface, if opened.
				}
				it.delay(11)
				it.player().interfaces().close(162, 550) // Chatbox, if opened.
				it.player().interfaces().closeById(94) // Destroy item interface, if opened.
				// Has to be called manually because fire_logout is processed before scripts. This means it'll miss a cycle.
				if (action.equals("logout")) {
					it.player().fire_logout()
				}
			}
		}
	}
	
	/**
	 * Trigger the guards which will absoutely destroy you when you enter a Safe zone.
	 * While you are frozen the same tick you enter, your walking queue is reset 1 tick later.. like barrages.
	 * This means, if you're super fast, you can run backwards out of a Safezone. In this situation, the guards will stop attacking you
	 * but you are permantly frozen, until _another person_ freeze you, in which case their timer overwrites the Frozen TimerKey.. allowing you to escape!
	 */
	@JvmStatic @Suspendable fun triggerGuards(player: Player) {
		if (!player.world().realm().isDeadman) // world realm
			return
		if (player.varps().varbit(Varbit.DEADMAN_SKULL_TIMER) == 0) // unskulled
			return
		if (player.attribOr<Int>(AttributeKey.GENERAL_VARBIT1, 0) as Int shr 22 and 3 == 1) // safe zone overlay tracker
			return
		if (!DmmZones.DMM_ZONE_CHECK_ENABLED) // not enabled anyway
			return
		
		player.world().executeScript @Suspendable { it ->
			val guard = Npc(typeForZone(player.tile()), player.world(), findValidTile(player.tile()))
			player.world().registerNpc(guard)
			player.message("<col=7F00FF>You've been frozen by the Guards!")
			guard.sync().shout("Stop right there!!")
			player.freeze(1000000, guard)
			player.graphic(369)
			guard.animate(1819) // no weapon teleblock emote
			guard.face(player)
			if (guard.tile().equals(player.tile())) {
				// move out from under them
				guard.walkTo(player.tile().transform(0, -1), PathQueue.StepType.FORCED_WALK)
				it.delay(1)
			}
			it.delay(3)
			// Now guards do other attacks .. if player alive and not back (Escaped) to Deadman dangerous zones
			var varp = player.attribOr<Int>(AttributeKey.GENERAL_VARBIT1, 0) as Int shr 22 and 3
			var next = player.world().random(3..6)
			while (player.alive() && varp != 1) {
				if (next-- == 0) {
					player.hit(guard, player.world().random(79)).combatStyle(CombatStyle.GENERIC).block(false)
					guard.animate(395)
					player.graphic(369)
					next = player.world().random(3..6)
				}
				it.delay(1)
				varp = player.attribOr<Int>(AttributeKey.GENERAL_VARBIT1, 0) as Int shr 22 and 3
			}
			// died or escaped
			player.timers().cancel(TimerKey.FROZEN)
			player.timers().cancel(TimerKey.REFREEZE)
			if (player.dead()) {
				guard.sync().shout("You've just become another OS-Scape Deadman, ${player.name()}!")
			} else {
				guard.sync().shout("Lucky escape " + player.name() + "!")
			}
			it.delay(5) // 15s
			player.world().unregisterNpc(guard)
		}
	}
	
	// A random tile for a safe zone Guard to spawn on.
	private fun findValidTile(tile: Tile): Tile {
		// TODO check 3x3 validity and choose one at random
		return tile
	}
	
	@JvmStatic var VARROCK = Area(3200, 3200, 3600, 3600)
	
	/**
	 * TODO the different npc ids depending which Safe zone you 'accidentally' fly head first into.
	 */
	private fun typeForZone(tile: Tile): Int {
		// 6698 ghost guard port phyas
		// 6699 desert guard the southern city sopham
		// 6700 rellekka guard shortsword
		// 6702 relleka longsword
		if (VARROCK.contains(tile)) {
			return 6701
		}
		return 6701
	}
}