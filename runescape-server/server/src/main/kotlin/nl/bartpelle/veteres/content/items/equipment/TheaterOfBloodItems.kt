package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.content.itemUsedSlot
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * @author Heaven
 */
object TheaterOfBloodItems {

    private const val MAX_CHARGE_COUNT = 20000
    const val SANGUINESTI_UNCHARGED_ID = 22481
    const val SANGUINESTI_CHARGED_ID = 22323
    const val SCYTHE_OF_VITUR_CHARGED_ID = 22325
    const val SCYTHE_OF_VITUR_UNCHARGED_ID = 22486

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        for (itemId in arrayOf(SANGUINESTI_CHARGED_ID, SCYTHE_OF_VITUR_CHARGED_ID)) {
            sr.onItemOption3(itemId) {
                checkItemCharges(it)
            }
            sr.onEquipmentOption(1, itemId) {
                checkItemCharges(it)
            }
        }
        sr.onItemOption4(SANGUINESTI_CHARGED_ID) @Suspendable {
            charge(it)
        }
        sr.onItemOption3(SANGUINESTI_UNCHARGED_ID) @Suspendable {
            charge(it)
        }
        sr.onItemOption5(SANGUINESTI_CHARGED_ID) @Suspendable {
            uncharge(it)
        }
        sr.onItemOption4(SCYTHE_OF_VITUR_CHARGED_ID) @Suspendable {
            uncharge(it)
        }
        sr.onItemOption4(SCYTHE_OF_VITUR_UNCHARGED_ID) @Suspendable {
            charge(it)
        }
    }

    @Suspendable
    private fun charge(it: Script, usedOn: Boolean = false) {
        if (!usedOn) {
            val item = it.itemUsed()
            val itemSlot = it.itemUsedSlot()
            val requestedAmount = it.inputInteger("How many charges do you want to apply? (Up to 20,000)")
            val currentCharges = item.property(ItemAttrib.CHARGES)
            val bloodRuneCount = it.player().inventory().count(565)
            val bloodMoneyCount = it.player().inventory().count(13307)
            val maxSupplyAmount = Math.min(MAX_CHARGE_COUNT - currentCharges, Math.min(bloodRuneCount, bloodMoneyCount))
            val charges = if (requestedAmount < maxSupplyAmount) requestedAmount else maxSupplyAmount
            if (charges == 0) {
                it.message("You need at least 1 blood rune and 1 blood money to charge this item.")
                return
            }

            // Remove the cost required to charge the item.
            it.player().inventory().remove(Item(565, charges), true)
            it.player().inventory().remove(Item(13307, charges), true)

            // If we're charging the uncharged item then let's replace it with a charged one, otherwise we add the charges to existing.
            if (item.id() == SANGUINESTI_UNCHARGED_ID && it.player().inventory().remove(item, true, itemSlot).success()) {
                it.player().inventory().add(Item(SANGUINESTI_CHARGED_ID).property(ItemAttrib.CHARGES, charges), true, itemSlot)
            } else if (item.id() == SCYTHE_OF_VITUR_UNCHARGED_ID && it.player().inventory().remove(item, true, itemSlot).success()) {
                it.player().inventory().add(Item(SCYTHE_OF_VITUR_CHARGED_ID).property(ItemAttrib.CHARGES, charges), true, itemSlot)
            } else {
                item.property(ItemAttrib.CHARGES, currentCharges + charges)
            }

            if (item.id() == SCYTHE_OF_VITUR_CHARGED_ID || item.id() == SCYTHE_OF_VITUR_UNCHARGED_ID) {
                it.itemBox("You apply $charges charges to your scythe of vitur.", SCYTHE_OF_VITUR_CHARGED_ID)
            } else {
                it.itemBox("You apply $charges charges to your sanguinesti staff.", SANGUINESTI_CHARGED_ID)
            }
        }
    }

    @Suspendable private fun uncharge(it: Script) {
        val item = it.itemUsed()
        val itemSlot = it.itemUsedSlot()
        val charges = item.property(ItemAttrib.CHARGES)
        if (charges == 0) {
            if (item.id() == SCYTHE_OF_VITUR_UNCHARGED_ID || item.id() == SCYTHE_OF_VITUR_CHARGED_ID) {
                it.itemBox("Your scythe contains no charges.", SCYTHE_OF_VITUR_CHARGED_ID)
            } else {
                it.itemBox("Your staff contains no charges.", SANGUINESTI_CHARGED_ID)
            }
            return
        }

        if (it.optionsTitled("<col=FF0000>Uncharge this item for all its charges? (Regaining $charges blood money)", "Proceed.", "Cancel.") == 1) {
            if (!it.player().inventory().roomFor(Item(13307)) || !it.player().inventory().roomFor(Item(565))) {
                it.messagebox("You do not have enough space in your inventory to uncharge your item.")
                return
            }

            val toAdd = if (item.id() == SCYTHE_OF_VITUR_CHARGED_ID) SCYTHE_OF_VITUR_UNCHARGED_ID else SANGUINESTI_UNCHARGED_ID
            if (it.player().inventory().remove(item, true, itemSlot).success()) {
                it.player().inventory().add(Item(toAdd), true, itemSlot)
                it.player().inventory().add(Item(13307, charges))
                it.player().inventory().add(Item(565, charges))
                item.property(ItemAttrib.CHARGES, 0)
                val itemName = if (toAdd == SCYTHE_OF_VITUR_UNCHARGED_ID) "scythe of vitur" else "sanguinesti staff"
                it.itemBox("You uncharged your $itemName, regaining $charges blood runes and blood money in the process.", toAdd)
            }
        }
    }

    @Suspendable
    private fun checkItemCharges(it: Script) {
        val player = it.player()
        val item = it.itemUsed()
        val charges = item.property(ItemAttrib.CHARGES)
        val itemType = if (item.id() == SCYTHE_OF_VITUR_CHARGED_ID) "scythe" else "staff"
        val castOr = if (item.id() == SCYTHE_OF_VITUR_CHARGED_ID) "attacks" else "casts"
        player.message("Your $itemType has <col=FF0000>${(charges.toDouble() / MAX_CHARGE_COUNT) * 100.0}%</col> charges ($charges $castOr) remaining.")
    }

    @JvmStatic fun justiciarSet(player: Player): Boolean {
        val helmId = player.equipment()[EquipSlot.HEAD]?.id() ?: -1
        val chestId = player.equipment()[EquipSlot.BODY]?.id() ?: -1
        val legsId = player.equipment()[EquipSlot.LEGS]?.id() ?: -1
        if (helmId != 22326 || chestId != 22327 || legsId != 22328) {
            return false
        }

        return true
    }
}