package nl.bartpelle.veteres.content.npcs.fightcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 9/18/2016.
 */

object YtMejkot {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackMelee(npc, target, true) && EntityCombat.attackTimerReady(npc)) {
				
				if (npc.world().rollDie(3, 1)) {
					attemptHeal(npc)
				} else {
					if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
						target.hit(npc, EntityCombat.randomHit(npc))
					} else {
						target.hit(npc, 0) // Uh-oh, that's a miss.
					}
				}
				
				npc.animate(npc.attackAnimation())
				
				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun attemptHeal(npc: Npc) {
		val currentHP = npc.hp()
		
		//If our monster is below 50% hitpoints we heal ourselves and those around us.
		if (currentHP < 80) {
			npc.heal(npc.world().random(10))
			npc.graphic(444, 150, 5)
			npc.animate(2639)
			npc.world().npcs().forEachInAreaKt(npc.bounds(3), { monster ->
				monster.heal(npc.world().random(10))
			})
			
		}
	}
}