package nl.bartpelle.veteres.content.npcs.bosses.cerberus

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Jason MacKeigan on 2016-07-07 at 1:28 PM
 *
 * Represents the attack made from cerberus on a particular Entity target. The
 * npcs parameter is the local group of three individual summoner soul non-
 * playable characters that will be doing the actual attacking.
 */
class SummonedSoulAttackScript(val target: Entity,
                               val npcs: Collection<Npc>,
                               var positionInOrder: SummonedSoulOrder = SummonedSoulOrder.FIRST,
                               var state: State = State.IDLE) {
	
	// the order that the npcs will attack in
	val orderedNpcs = mutableMapOf<SummonedSoulOrder, Npc>()
	
	// the original location for each npc
	val originalLocation = mutableMapOf<Npc, Tile>()
	
	// the destination of each npc
	val destinations = mutableMapOf<Npc, Tile>()
	
	init {
		npcs.forEach { originalLocation.put(it, it.tile()) }
		
		shuffleDestinations()
	}
	
	val script: Function1<Script, Unit> = suspend@ @Suspendable {
		val cerberus = it.npc()
		it.clearContext()
		
		while (state != State.COMPLETE) {
			when (state) {
				State.IDLE -> {
					npcs.forEach { setVisible(npcs) }
					
					destinations.forEach {
						it.key.pathQueue().clear()
						it.key.pathQueue().interpolate(it.value, PathQueue.StepType.FORCED_WALK)
					}
					state = State.WALKING_TO
				}
				
				State.WALKING_TO -> {
					checkWalkingLocation(destinations, State.ATTACKING)
				}
				
				State.ATTACKING -> {
					val currentNpc = orderedNpcs.get(positionInOrder) ?: return@suspend
					val nextOrder = positionInOrder.next()
					
					attack(currentNpc, target)
					it.delay(2)
					
					if (nextOrder == SummonedSoulOrder.THIRD && positionInOrder == SummonedSoulOrder.THIRD) {
						npcs.forEach {
							val destination = originalLocation.get(it)!!
							
							it.pathQueue().clear()
							it.pathQueue().interpolate(destination, PathQueue.StepType.FORCED_WALK)
							destinations.put(it, destination)
						}
						state = State.WALKING_AWAY
					} else {
						positionInOrder = nextOrder
					}
				}
				
				State.WALKING_AWAY -> {
					checkWalkingLocation(originalLocation, State.COMPLETE)
					if (state == State.COMPLETE) {
						it.delay(1)
						npcs.forEach { setInvisible(npcs) }
						it.delay(7)
						cerberus.putattrib(AttributeKey.CERBERUS_SUMMONED_SOUL_ATTACK_UNAVAILABLE, false)
					}
				}
			}
			
			it.delay(1)
		}
	}
	
	private fun setInvisible(npcs: Collection<Npc>) {
		npcs.forEach { n -> n.hidden(true) }
	}
	
	private fun setVisible(npcs: Collection<Npc>) {
		npcs.forEach { n -> n.hidden(false) }
	}
	
	/**
	 * A means of attacking the target with the summoned souls respective
	 * style of attack.
	 */
	private fun attack(npc: Npc, target: Entity) {
		val graphic = graphics.get(npc.id()) ?: return
		
		val style = styles.get(npc.id()) ?: return
		
		npc.faceTile(target.tile())
		npc.animate(npc.attackAnimation())
		val tileDist = npc.tile().transform(0, 0, 0).distance(target.tile())
		npc.world().spawnProjectile(npc, target, graphic, 1, 5, 25, 12 * tileDist, 15, 10)
		if (target.hit(npc, 30, 2).combatStyle(style).applyProtection().damage() < 30) {
			target.skills().alterSkill(Skills.PRAYER, -30)
		}
	}
	
	/**
	 * A means of randomly selecting a destination for each npc dynamically and populating
	 * the ordered list of npcs.
	 */
	private fun shuffleDestinations() {
		val destinations = mutableMapOf<Npc, Tile>()
		
		// store each of the possible end destinations in a mapping for each npc
		npcs.forEach { destinations.put(it, it.tile().transform(0, -SummonedSoulAttackScript.WALKING_DISTANCE)) }
		
		// create a mutable ArrayList containing each of the destination values
		val shuffledDestinations = ArrayList(destinations.values)
		
		// shuffle the mutable ArrayList
		Collections.shuffle(shuffledDestinations)
		
		// obtain an iterator from the shuffled destinations
		val iterator = shuffledDestinations.iterator()
		
		// a mapping of each tile to npc for later usage
		val tilesToNpcs = mutableMapOf<Tile, Npc>()
		
		npcs.forEach {
			val tile = iterator.next()
			
			// populate the tile to npcs mapping
			tilesToNpcs.put(tile, it)
			
			// in no specific order we store the next destination in a mapping
			this.destinations.put(it, tile)
		}
		
		// the destinations ordered by their respective x values
		val orderedDestinations = this.destinations.values.sortedBy { it.x }
		
		// populate the ordered npcs mapping with the order and the npcs for the tile
		for ((index, value) in orderedDestinations.withIndex()) {
			orderedNpcs.put(SummonedSoulOrder.ordered[index], tilesToNpcs.get(value)!!)
		}
	}
	
	/**
	 * A function used to determine if all of the non-playable characters in this
	 * attack have all met their desired walk destinations. If they have, the
	 * state is progress to that of indicated parameter.
	 */
	private fun checkWalkingLocation(locations: Map<Npc, Tile>, nextState: State) {
		var count = 0
		
		for (npc in npcs) {
			val destination = locations.get(npc)!!
			
			if (npc.tile().equals(destination.x, destination.z)) {
				count++
			}
		}
		
		if (count == 3) {
			state = nextState
		}
	}
	
	/**
	 * An enumeration of elements that represent the potential state that the
	 * attack can be in.
	 */
	enum class State {
		IDLE, WALKING_TO, ATTACKING, WALKING_AWAY, COMPLETE
	}
	
	companion object {
		
		// the distance that each summoned soul must walk to and from
		const val WALKING_DISTANCE = 7
		
		// a mapping of all non-playable character id values to their attack graphics
		val graphics = mapOf<Int, Int>(5869 to 1248, 5868 to 100, 5867 to 124)
		
		// a mapping of all non-playable character id values to their combat styles of attack
		val styles = mapOf<Int, CombatStyle>(5869 to CombatStyle.MELEE, 5868 to CombatStyle.MAGIC,
				5867 to CombatStyle.RANGE)
	}
	
}