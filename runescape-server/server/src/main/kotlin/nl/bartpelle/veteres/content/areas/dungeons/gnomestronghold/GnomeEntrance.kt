package nl.bartpelle.veteres.content.areas.dungeons.gnomestronghold

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/10/2016.
 */

object GnomeEntrance {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(26709) @Suspendable {
			it.animate(2796)
			it.delay(2)
			it.player().teleport(Tile(2444, 9825))
		}
		4
		r.makeRemoteObject(26713) // Stepping stone cave krakens
		
		r.onObject(26713) @Suspendable {
			// Stepping stone
			if (it.player().tile().equals(2456, 9838)) {
				it.player().teleport(2457, 9830)
			} else if (it.player().tile().equals(2457, 9830)) {
				it.player().teleport(2456, 9838)
			}
		}
	}
	
}
