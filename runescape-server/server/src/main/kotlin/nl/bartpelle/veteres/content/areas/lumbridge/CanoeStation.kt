package nl.bartpelle.veteres.content.areas.lumbridge

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.content.areas.instances.PVPAreas
import nl.bartpelle.veteres.content.skills.woodcutting.Woodcutting
import nl.bartpelle.veteres.content.sound
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Interfaces
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.net.message.game.command.*
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 5/20/2016.
 * Finished by Jak 11/08/2016
 */

object CanoeStation {
	
	val MAKE_CANOE_WIDGET = 52
	val SELECT_DESINATION_WIDGET = 57
	val ANIM_CHOP = 3285
	val ANIM_TREE_FALL = 3304
	val ANIM_PUSH_OFF = 3301
	val NO_EXAMINE_STATE = 9
	val CHOPPED_DOWN_STATE = 10
	
	enum class CanoeInfo(val uid: Int, val objId: Int, val varbit: Int, val travelBtnIds: IntArray, val endPos: Tile, val endMsg: String,
	                     val chopTile: Tile, val shapeTile: Tile, val faceBoatTile: Tile, val sinkTile: Tile, val wcReq: Int) {
		LUMBY(1, 12163, 1839, intArrayOf(32, 11), Tile(3240, 3241), "in Lumbridge", Tile(2, 0), Tile(2, 2), Tile(-5, 0), Tile(3238, 3240), 12),
		CHAMPIONS_GUILD(2, 12164, 1840, intArrayOf(13, 33), Tile(3199, 3343), "at the Champion's Guild", Tile(4, 2), Tile(2, 2), Tile(0, -5), Tile(3204, 3337), 12),
		BARB_VILLAGE(3, 12165, 1841, intArrayOf(34, 15), Tile(3109, 3415), "in Barbarian village", Tile(2, 0), Tile(2, 2), Tile(-5, 0), Tile(3107, 3414), 12),
		EDGEVILLE(4, 12166, 1842, intArrayOf(38, 39), Tile(3128, 3503), "in Edgeville", Tile(2, 0), Tile(2, 2), Tile(-5, 0), Tile(3127, 3503), 12),
		WILDY_CHINS(5, -1, 1843, intArrayOf(35), Tile(3144, 3798), "in Deep Wilderness", Tile(0, 0), Tile(0, 0), Tile(0, 0), Tile(0, 0), 1);
		
		companion object {
			@JvmStatic fun infoForObject(objId: Int): CanoeInfo? {
				for (info in CanoeInfo.values()) {
					if (info.objId == objId) {
						return info
					}
				}
				return null
			}
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		for (child in intArrayOf(8, 24, 6, 25, 3, 26, 13, 27)) {
			r.onButton(MAKE_CANOE_WIDGET, child) @Suspendable {
				// Make log / canoe / raft etc
				shapeBoat(it, if (child in intArrayOf(8, 24, 36)) 1 else if (child in intArrayOf(6, 25, 37)) 2 else if (child in intArrayOf(3, 26, 38)) 3 else 4)
			}
		}
		for (info in CanoeInfo.values()) {
			r.onObject(info.objId) @Suspendable {
				interactWithStation(it, info)
			}
			for (child in info.travelBtnIds) {
				r.onButton(SELECT_DESINATION_WIDGET, child) {
					travelToDesination(it, info)
				}
			}
		}
	}
	
	@JvmStatic @Suspendable fun shapeBoat(it: Script, boatType: Int) {
		val obj = it.interactionObject()
		val info = CanoeInfo.infoForObject(obj.id()) ?: return
		it.player().lockDelayDamage()
		it.player().interfaces().closeMain()
		it.player().faceTile(it.player().tile().transform(info.faceBoatTile.x, info.faceBoatTile.z))
		var loops = 10
		while (loops-- > 0) {
			it.delay(1)
			it.player().animate(ANIM_CHOP)
		}
		it.player().animate(-1)
		it.player().varps().varbit(info.varbit, boatType)
		it.player().unlock()
	}
	
	@Suspendable private fun travelToDesination(it: Script, info: CanoeInfo) {
		it.player().lockNoDamage()
		it.player().interfaces().closeMain()
		
		// Fade out
		it.player().interfaces().sendWidgetOn(174, Interfaces.InterSwitches.C)
		it.player().write(InvokeScript(951))
		
		// Wait 3 seconds (2, teleport, 1, see below)
		it.delay(2)
		
		// testswimming(it.player()) // TODO this looks pretty complicated. Rotated instance, missing xteas,
		// similar to how POH has single chunks blocked together. PLUS cutscene usage to change orentation of camera
		it.delay(1)
		
		// TODO enable when cutscene enable
		// All tabs closed except cc, friends ignores logout and music
		/*for (sidebar in intArrayOf(593, 320, 399, 149, 387, 149, 387, 271, 218, 261, 216))
			it.player().interfaces().closeById(sidebar)
		it.player().write(ChangeMapVisibility(2))

		it.player().interfaces().sendWidgetOn(174, Interfaces.InterSwitches.C)
		it.player().write(InvokeScript(948, 0, 0, 0, 255, 50)) // fade back in
		it.player().animate(3302) // Swim animation ALWAYS faces East. Map orentation needs to change! :)

		// Swim for 10s
		it.delay(10)

		it.player().interfaces().sendWidgetOn(174, Interfaces.InterSwitches.C)
		it.player().write(InvokeScript(951)) // fade out
		it.delay(3)*/
		
		it.player().interfaces().sendWidgetOn(174, Interfaces.InterSwitches.C)
		it.player().write(InvokeScript(948, 0, 0, 0, 255, 50)) // fade back in
		
		// Respawn and restore tabs
		it.player().varps().varbit(Varbit.LOCK_CAMERA, 0)
		it.player().animate(-1)
		it.player().teleport(info.endPos)
		it.player().message("You arrive ${info.endMsg}. <br>Your canoe sinks into the water after the hard journey.")
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.COMBAT_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.SKILLS_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.QUEST_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.INVENTORY_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.EQUIPMENT_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.PRAYER_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.MAGIC_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.SETTINGS_TAB)
		it.player().interfaces().sendDefaultInterlink(Interfaces.InterSwitches.EMOTES_TAB)
		it.player().write(ChangeMapVisibility(0))
		it.player().updateWeaponInterface()
		val varp = it.player().varps().varbit(info.varbit) // Identify canoe type used
		it.player().varps().varbit(info.varbit, 0) // Reset
		val sinkId = when (varp) {
			11 -> 12159
			12 -> 12160
			13 -> 12161
			14 -> 12162
			else -> 12162
		}
		it.player().unlock()
		val ship = it.player().world().spawnObj(MapObj(info.sinkTile, sinkId, 10, 1), false)
		it.delay(2)
		it.player().world().removeObj(ship)
	}
	
	@Suspendable private fun interactWithStation(it: Script, info: CanoeInfo) {
		val obj = it.interactionObject()
		val varbit = it.player().varps().varbit(info.varbit)
		
		//If our player requests to chop-down the canoe station we..
		if (varbit == 0) {
			it.player().lock()
			
			val chopTile = obj.tile().transform(info.chopTile.x, info.chopTile.z)
			if (!it.player().tile().equals(chopTile)) {
				it.player().walkTo(chopTile, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(chopTile)
			}
			
			it.player().faceTile(it.player().tile().transform(info.faceBoatTile.x, info.faceBoatTile.z))
			
			if (it.player().skills().level(Skills.WOODCUTTING) < info.wcReq) {
				it.messagebox("You must have at least level ${info.wcReq} Woodcutting to start making canoes.")
			} else {
				val axe: Woodcutting.Woodcutting.Hatchet? = Woodcutting.Woodcutting.findAxe(it.player())
				
				//Does our player have an axe?
				if (axe == null) {
					it.player().sound(2277, 0)
					it.message("You do not have an axe which you have the Woodcutting level to use.")
				} else {
					it.player().animate(axe.anim)
					it.delay(2)
					
					it.player().animate(-1)
					it.player().write(SetMapBase(it.player(), obj.tile())) // set base
					it.player().write(AnimateObject(obj, ANIM_TREE_FALL))
					
					it.player().varps().varbit(info.varbit, NO_EXAMINE_STATE) // Removes 'cut' option on tree
					it.delay(1)
					it.sound(2734)
					it.player().varps().varbit(info.varbit, CHOPPED_DOWN_STATE) // Tree fallen down
				}
			}
			it.player().unlock()
			
			//If our player requests to shape the canoe we..
		} else if (varbit == CHOPPED_DOWN_STATE) { // When Tree down
			
			it.player().lock()
			
			val shapeTile = obj.tile().transform(info.shapeTile.x, info.shapeTile.z)
			if (!it.player().tile().equals(shapeTile)) {
				it.player().walkTo(shapeTile, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(shapeTile)
			}
			it.player().faceTile(it.player().tile().transform(info.faceBoatTile.x, info.faceBoatTile.z))
			it.delay(1)
			// Open options to cut into
			it.player().invokeScript(InvokeScript.SETVARCS, 3612928, 0)
			it.player().write(InterfaceVisibility(52, 6, false))
			it.player().write(InterfaceVisibility(52, 32, true))
			it.player().write(InterfaceVisibility(52, 3, false))
			it.player().write(InterfaceVisibility(52, 35, true))
			it.player().write(InterfaceVisibility(52, 13, false))
			it.player().write(InterfaceVisibility(52, 29, true))
			it.player().interfaces().sendMain(52)
			it.player().invokeScript(InvokeScript.SETVARCS, -1, -1)
			it.player().unlock()
			
		} else if (varbit in 1..4) { // Canoe built on stand
			it.player().lockDelayDamage()
			it.animate(ANIM_PUSH_OFF)
			it.player().faceTile(it.interactionObject().tile().transform(info.faceBoatTile.x, info.faceBoatTile.z))
			it.player().varps().varbit(info.varbit, it.player().varps().varbit(info.varbit) + 4)
			it.player().write(SetMapBase(it.player(), obj.tile())) // set base
			it.player().write(AnimateObject(obj, ANIM_TREE_FALL))
			it.delay(2)
			it.player().varps().varbit(info.varbit, it.player().varps().varbit(info.varbit) + 6)
			it.player().unlock()
			
		} else if (varbit in 11..14) { // Paddle to destination
			it.player().faceTile(it.interactionObject().tile().transform(info.faceBoatTile.x, info.faceBoatTile.z))
			it.player().write(InterfaceVisibility(57, 50, false))
			it.player().write(InterfaceVisibility(57, 11, false))
			it.player().write(InterfaceVisibility(57, 13, false))
			it.player().write(InterfaceVisibility(57, 15, false))
			it.player().write(InterfaceVisibility(57, 38, false))
			it.player().write(InterfaceVisibility(57, 31, false))
			it.player().interfaces().sendMain(57)
		}
	}
	
	@JvmStatic fun testswimming(player: Player) {
		val corner = Tile.regionToTile(7238)
		val swimInstance = player.world().allocator().allocate(64, 64, corner).get()
		swimInstance.setCreatedFor(player)
		swimInstance.set(0, 0, corner, corner.transform(64, 64), 2, true)
		val realBoatTile = Tile(1817, 4515)
		val instanceBoatTile = PVPAreas.resolveBasic(realBoatTile, corner, swimInstance)
		val boatRotated = instanceBoatTile.transform(5, 1)
		player.teleport(boatRotated)
		player.faceTile(boatRotated.transform(0, -2))
		player.message("Tele %s face %s", boatRotated, boatRotated.transform(0, -2))
		player.varps().varbit(Varbit.LOCK_CAMERA, 1)
	}
}