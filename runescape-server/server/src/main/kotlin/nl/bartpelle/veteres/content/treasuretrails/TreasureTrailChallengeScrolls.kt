package nl.bartpelle.veteres.content.treasuretrails

/**
 * Created by Situations on 2016-11-07.
 */

object TreasureTrailChallengeScrolls {
	
	enum class Challenges(npc: Int, challenge: String, answer: Int, enabled: Boolean = false) {
		AWOWOGEI(3396, "If I have 303 bananas, and share them between 31 friends evenly, only handing out full bananas. " +
				"How many will I have left over?", 24),
		ZOO_KEEPER(3113, "How many animals are in the Ardougne Zoo?", 40),
		BROTHER_TRANQUILITY(550, "If I have 49 bottles of rum to share between 7 pirates, how many would each pirate get?", 7),
		CAPN_IZZY_NO_BEARDS_PARROT(5789, "How many Banana Trees are there in the plantation?", 33),
		KING_RONALD(1399, "How many bookcases are there in the Varrock palace library?", 24),
		ELUNED(889, "A question on elven crystal math. I have 5 and 3 crystals, large and small respectively. A large " +
				"crystal is worth 10,000 coins and a small is worth but 1,000. How much are all my crystals worth?", 53000),
		GABOOTY(6424, "How many buildings in the village?", 11),
		GALLOW(6775, "How many vine patches can you find in this vinery?", 12),
		COOK(225, "How many cannons does Lumbridge Castle have?", 9),
		CAROLINE(5067, "How many fishermen are there on the fishing platform?", 11),
		DUNSTAN(4105, "How much smithing experience does one receive for smelting a blurite bar?", 8),
		BARAEK(533, "How many stalls are there in Varrock Square?", 5),
		BOLKOY(4965, "How many flowers are in the clearing below this platform?", 13),
		GNOME_COACH(3142, "How many gnomes on the Gnome ball field have red patches on their uniforms?", 6),
		RECRUITER(4262, "How many houses have a cross on the door?", 20),
		DOCKMASTER(6966, "What is the cube root of 125?", 5),
		DREZEL(3488, "Please solve this for x: 7x - 28 = 21", 7),
		BRUNDT(3926, "How many people are waiting for the next bard to perform?", 4),
		EDMOND(2202, "How many pigeon cages are there around the back of Jerico's house?", 3),
		FATHER_AERECK(921, "How many gravestones are in the church graveyard?", 19),
		ORACLE(821, "If x is 15 and y is 3, what is 3x + y?", 48),
		KARIM(529, "I have 16 kebabs, I eat one myself and share the rest equally between 3 friends. How many do they " +
				"have each?", 5),
		KAYLEE(1316, "How many chairs are there in the Rising Sun?", 18),
		MANDRITH(6599, "How many scorpions live under the pit?", 28),
		MINER_MAGNUS(3654, "How many coal rocks are around here?", 8),
		NIEVE(6797, "How many farming patches are there in Gnome stronghold?", 2),
		HAZELMERE(1422, "What is 19 to the power of 3?", 6859),
		GNOME_BALL_REFEREE(3157, "What is 57 x 89 + 23?", 5096),
		CAPTAIN_TOBIAS(3644, "How many ships are there docked at Port Sarim currently?", 6),
		STRANGE_OLD_MAN(1671, "One pipe fills a barrel in 1 hour while another pipe can fill the same barrel in 2 hours.<br/>" +
				"How many minutes will it take to fill the take if both pipes are used?", 40),
		TARIA(2675, "How many buildings are there in Rimmington?", 7),
		MARTIN_THWAIT(3193, "How many natural fires burn in the Rogue's Den?", 2),
		HICKTON(1044, "How many ranges are there in the Catherby?", 2),
		OTTO_GODBLESSED(2914, "How many types of dragon are there beneath the whirlpool's cavern?", 2),
		WISE_OLD_MAN(2108, "How many bookcases are in the Wise Old Man's house?", 28),
		PROSPECTOR_PERCY(6562, "During a party, everyone shook hands with everyone else. There were 66 handshakes. How many" +
				" people were at the party?", 12),
		SIR_KAY(3521, "How many fountains are there within the grounds of Camelot castle", 6),
		BROTHER_OMAD(4244, "What is the next number? 12, 13, 15, 17, 111, 113, 117, 119, 123....?", 129),
		SIGLI_THE_HUNTSMAN(3294, "What is the combined slayer requirement of every monster in the slayer cave?", 302),
		CAM_THE_CAMEL(5952, "How many items can carry water in RuneScape?", 6),
		DOMINIC_ONION(1120, "How many reward points does a herb box cost?", 9500),
		EOHRIC(4103, "	King Arthur and Merlin sit down at the Round Table with 8 knights. How many degrees does each get?", 36),
		ONEIROMANCER(3835, "How many Suqah inhabit Lunar isle?", 25),
		LISSE_ISAAKSON(1888, "How many arctic logs are required to make a large fremennik round shield?", 2),
		ORONWEN(1478, "What is the minimum amount of quest points required to reach Lletya?", 20),
		DOOMSAYER(6773, "What is 40 divided by 1/2 plus 15?", 95),
		MARISI(6921, "How many districts form the city of Great Kourend?", 5),
		OLD_CRONE(2996, "What is the combined combat level of each species that live in Slayer tower?", 619),
		AMBASSADOR_ALVIJAR(5117, "Double the miles before the initial Dorgeshuun veteran.", 2505),
		ADAM(311, "How many snakeskins are needed in order to craft 44 boots, 29 vambraces, and 34 bandanas?", 666),
		BROTHER_KOJO(3606, "On a clock, how many times a day does the minute hand and the hour hand overlap?", 22),
		KING_PERCIVAL(4058, "How many cannons are on this here castle?", 5),
		PROFESSOR_GRACKLEBONE(7048, "How many round tables can be found on this floor of the library?", 9),
		TRAIBORN(5081, "How many air runes would I need to cast 630 wind waves?", 3150),
		FAIRY_GODFATHER(1840, "There are 3 inputs and 4 letters on each ring How many total individual fairy ring codes are" +
				" possible?", 64),
		SPIRIT_TREE(4981, "What is the next number in the sequence? 1, 11, 21, 1211, 111221, 312211", 13112221),
		EVIL_DAVE(901, "What is 333 multiplied by 2?", 666),
		CAPTAIN_KHALED(6971, "How many fishing cranes can you find around here?", 5),
		NURSE_WOONED(6873, "How many injured soldiers are in the tents?", 52),
		SQUIRE(1759, "White knights are superior to black knights. 2 white knights can handle 3 black knights. How many knights" +
				" do we need for an army of 981 black knights?", 654),
		ALI_THE_KEBAB_SELLER(3536, "How many coins would you need to purchase 133 kebabs from me?", 399),
		CLERRIS(7040, "If I have 1000 blood runes, and cast 131 ice barrage spells, how many blood runes do I have left?", 738),
		HORPHIS(7046, "On a scale of 1-10, how helpful is Logosia?", 1),
		FLAX_KEEPER(5522, "If I have 1014 flax, and I spin a third of them into bowstring, how many flax do I have left?", 676),
		DARK_MAGE(2583, "How many rifts are found here in the abyss?", 13),
		ARETHA(7042, "32 - 5x = 22, what is x?", 2),
		WEIRD_OLD_MAN(954, "SIX LEGS! All of them have six! There are 25 of them! How many legs?", 150),
		CHARLIE_THE_TRAMP(5209, "How many coins would I have if I had 0 coins and attempted to buy 3 loaves of bread?", 0)
	}
	
}