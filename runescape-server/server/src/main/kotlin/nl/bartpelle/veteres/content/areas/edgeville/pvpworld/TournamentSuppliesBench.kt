package nl.bartpelle.veteres.content.areas.edgeville.pvpworld

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.net.message.game.command.AddMessage
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/11/2016.
 */

object TournamentSuppliesBench {
	
	val TOURNAMENT_SUPPLIES_BENCH = 7811
	val TOURNAMENT_STATUE = 26073
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(TOURNAMENT_SUPPLIES_BENCH) @Suspendable {
			if (it.player().world().realm().isPVP) {
				it.player().world().shop(1337).display(it.player())
			}
		}
		r.onObject(TOURNAMENT_STATUE) @Suspendable {
			it.itemBox("Thanks for supporting OS-Scape. Amazing things are coming.", 744)
			when(it.optionsTitled("View the poll results?", "Yes, view the poll results.", "No, I'm okay.")) {
				1 -> it.player().write(AddMessage("https://forum.os-scape.com/index.php?/topic/6223-server-direction-and-poll/", AddMessage.Type.URL))
			}
		}
	}
}
