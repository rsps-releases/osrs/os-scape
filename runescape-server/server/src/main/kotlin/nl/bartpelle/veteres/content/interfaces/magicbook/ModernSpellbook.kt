package nl.bartpelle.veteres.content.interfaces.magicbook

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterManager
import nl.bartpelle.veteres.content.openInterface
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Situations on 3/20/2016.
 */

object ModernSpellbook {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Teleports
		r.onButton(218, 1, s@ @Suspendable { attempt_home_teleport(it) }) //Home teleport
		r.onButton(218, 16) @Suspendable {
			//Varrock teleport
			val action = it.buttonAction()
			if (action == 1) {
				cast_spell(it, 0, 25, Tile(3212, 3424, 0), 35.0, false, Item(556, 3), Item(554, 1), Item(563, 1))
			} else {
				it.message("This feature cannot be used yet.")
			}
		}
		r.onButton(218, 19) @Suspendable { cast_spell(it, 0, 31, Tile(3222, 3219, 0), 41.0, false, Item(556, 3), Item(557, 1), Item(563, 1)) } //Lumbridge teleport
		r.onButton(218, 22) @Suspendable { cast_spell(it, 0, 37, Tile(2965, 3383, 0), 48.0, false, Item(556, 3), Item(555, 1), Item(563, 1)) } //Falador teleport
		r.onButton(218, 27) @Suspendable {
			//Camelot teleport
			val btn = it.buttonAction()
			if (btn == 1) {
				cast_spell(it, 0, 45, Tile(2757, 3478, 0), 55.5, false, Item(556, 5), Item(563, 1))
			} else {
				it.message("This feature cannot be used yet.")
			}
			// 2 = seers
			// 10 = configure
		}
		r.onButton(218, 33) @Suspendable { cast_spell(it, 0, 51, Tile(2662, 3306, 0), 61.0, false, Item(555, 2), Item(563, 2)) } //Ardougne teleport
		r.onButton(218, 38) @Suspendable {
			//Watchtower teleport
			val btn = it.buttonAction()
			if (btn == 1) {
				cast_spell(it, 0, 58, Tile(2547, 3113, 2), 68.0, false, Item(563, 2), Item(557, 2))
			} else {
				it.message("This feature cannot be used yet.")
			}
		}
		r.onButton(218, 45) @Suspendable { cast_spell(it, 0, 61, Tile(2891, 3679, 0), 68.0, false, Item(563, 2), Item(554, 2)) } //Trollhiem teleport
		r.onButton(218, 48) @Suspendable { cast_spell(it, 0, 64, Tile(2796, 2791, 0), 74.0, false, Item(554, 2), Item(555, 2), Item(563, 2)/*, Item(563)*/) } //Ape atoll
		r.onButton(218, 53) @Suspendable { cast_spell(it, 0, 69, Tile(1639, 3672, 0), 82.0, false, Item(563, 2), Item(566, 2), Item(555, 4), Item(554, 5), shout = "Thadior Agrula Elevo") } //Kourend Teleport
		
		//Bounty teleport to target
		r.onButton(218, 63, s@ @Suspendable {
			
			if (!it.player().attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TARGET_TELEPORT_UNLOCKED, false)) {
				it.player().message("You have not unlocked this spell yet.")
				return@s
			}
			
			if (!WildernessLevelIndicator.inWilderness(it.player().tile())) {
				it.player().message("You must be in the Wilderness to use this spell.")
				return@s
			}
			
			if (it.player().attribOr<Player>(AttributeKey.BOUNTY_HUNTER_TARGET, null) != null) {
				
				if (!WildernessLevelIndicator.inWilderness(BountyHunterManager.getTargetFor(it.player())!!.tile())) {
					it.player().message("Your target is currently not in the Wilderness.")
					return@s
				}
				
				cast_spell(it,0,  86, it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).tile(), 45.0, false, Item(563, 1), Item(560, 1), Item(562, 1))
				it.player().varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY, it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).varps().varbit(Varbit.MAGEBANK_MAGIC_ONLY))
			} else {
				it.player().message("You currently have no target to teleport to!")
			}
		})
		
		//Combat spells
		register_spell(1, r, 218, 2, 220, 221, 1162, 91, 12, 90, Graphic(92, 80), 227, 2, 1, 5.5, "Wind strike", Item(556, 1), Item(558, 1)) // Wind Strike
		register_spell(5, r, 218, 5, 211, 212, 1162, 94, 12, 93, Graphic(95, 80), 227, 4, 1, 7.5, "Water strike", Item(555, 1), Item(556, 1), Item(558, 1)) // Water Strike
		register_spell(9, r, 218, 7, 132, 133, 1162, 97, 12, 96, Graphic(98, 80), 227, 6, 1, 9.5, "Earth strike", Item(557, 2), Item(556, 1), Item(558, 1)) // Earth Strike
		register_spell(13, r, 218, 9, 160, 161, 1162, 100, 12, 99, Graphic(101, 80), 227, 8, 1, 11.5, "Fire strike", Item(554, 3), Item(556, 1), Item(558, 1)) // Fire Strike
		register_spell(17, r, 218, 11, 218, 219, 1162, 118, 12, 117, Graphic(119, 80), 227, 9, 1, 13.5, "Wind Bolt", Item(556, 2), Item(562, 1)) // Wind Bolt
		register_spell(20, r, 218, 13, 151, 153, 1161, 178, 12, 177, Graphic(181, 80), 227, 0, 1, 30.0, "Bind", Item(555, 3), Item(557, 3), Item(561, 2)) // Bind
		register_spell(23, r, 218, 15, 209, 210, 1162, 121, 12, 120, Graphic(122, 80), 227, 10, 1, 16.5, "Water Bolt", Item(555, 2), Item(556, 2), Item(562, 1)) // Water Bolt
		register_spell(29, r, 218, 18, 130, 131, 1162, 124, 12, 123, Graphic(125, 80), 227, 11, 1, 19.5, "Earth Bolt", Item(557, 3), Item(556, 2), Item(562, 1)) // Earth Bolt
		register_spell(35, r, 218, 21, 157, 158, 1162, 127, 12, 126, Graphic(128, 80), 227, 12, 1, 21.5, "Fire Bolt", Item(554, 4), Item(556, 2), Item(562, 1)) // Fire Bolt
		register_spell(41, r, 218, 25, 216, 217, 1162, 133, 12, 132, Graphic(134, 80), 227, 13, 1, 25.5, "Wind Blast", Item(556, 3), Item(560, 1)) // Wind Blast
		register_spell(47, r, 218, 28, 207, 208, 1162, 136, 12, 135, Graphic(137, 80), 227, 14, 1, 28.5, "Water Blast", Item(555, 3), Item(556, 3), Item(560, 1)) // Water Blast
		register_spell(50, r, 218, 32, 1718, 1576, 1576, 328, 12, -1, Graphic(329, 80), 227, 10, 1, 30.0, "Magic dart", Item(558, 4), Item(560, 1)) // Magic dart
		register_spell(50, r, 218, 31, 151, 153, 1161, 178, 12, 177, Graphic(180, 80), 227, 2, 1, 60.0, "Snare", Item(555, 4), Item(557, 4), Item(561, 3)) // Snare
		register_spell(53, r, 218, 34, 128, 129, 1162, 139, 12, 138, Graphic(140, 80), 227, 15, 1, 31.5, "Earth Blast", Item(557, 4), Item(556, 3), Item(560, 1)) // Earth Blast
		register_spell(59, r, 218, 39, 155, 156, 1162, 130, 12, 129, Graphic(131, 80), 227, 16, 1, 34.5, "Fire Blast", Item(554, 5), Item(556, 4), Item(560, 1)) // Fire Blast
		register_spell(60, r, 218, 42, -1, 1659, 811, -1, 12, -1, Graphic(76, 80), 1656, 20, 1, 35.0, "Saradomin Strike", Item(565, 2), Item(554, 2), Item(556, 4)) //Saradomin Strike
		register_spell(60, r, 218, 43, -1, 1653, 811, -1, 12, -1, Graphic(77, 80), 1652, 20, 1, 35.0, "Claws of Guthix", Item(565, 2), Item(554, 1), Item(556, 4)) //Claws of Guthix
		register_spell(60, r, 218, 44, -1, 1655, 811, -1, 12, -1, Graphic(78, 0), 1654, 20, 1, 35.0, "Flames of Zamorak", Item(565, 2), Item(554, 4), Item(556, 1)) //Flames of Zamorak
		register_spell(62, r, 218, 46, 222, 223, 1167, 159, 12, 158, Graphic(160, 80), 227, 17, 1, 36.0, "Wind Wave", Item(556, 5), Item(565, 1)) // Wind Wave
		register_spell(65, r, 218, 49, 213, 214, 1167, 162, 12, 161, Graphic(163, 80), 227, 18, 1, 37.5, "Water Wave", Item(555, 7), Item(556, 5), Item(565, 1)) // Water Wave
		register_spell(70, r, 218, 54, 134, 135, 1167, 165, 12, 164, Graphic(166, 80), 227, 19, 1, 40.0, "Earth Wave", Item(557, 7), Item(556, 5), Item(565, 1)) // Earth Wave
		register_spell(75, r, 218, 57, 162, 163, 1167, 156, 12, 155, Graphic(157, 80), 227, 20, 1, 42.5, "Fire Wave", Item(554, 7), Item(556, 5), Item(565, 1)) // Fire Wave
		register_spell(79, r, 218, 58, 151, 153, 1161, 178, 12, 177, Graphic(179, 80), 227, 3, 1, 89.0, "Entangle", Item(555, 5), Item(557, 5), Item(561, 4)) // Entangle
		register_spell(85, r, 218, 64, -1, -1, 1820, 1299, 9, -1, Graphic(345, 80), 227, 0, 1, 84.0, "Teleblock", Item(562, 1), Item(563, 1), Item(560, 1)) //Tele Block
		register_spell(81, r, 218, 61, -1, -1, 7855, 1456, 12, 1455, Graphic(1457, 80), 227, 21, 1, 44.0, "Wind Surge", Item(556, 7), Item(21880, 1))//wind surge
        register_spell(85, r, 218, 63, -1, -1, 7855, 1459, 12, 1458, Graphic(1460, 80), 227, 22, 1, 46.0, "Water Surge", Item(555, 10), Item(556, 7), Item(21880, 1))//water surge
        register_spell(90, r, 218, 68, -1, -1, 7855, 1462, 12, 1461, Graphic(1463, 80), 227, 23, 1, 48.0, "Earth Surge", Item(557, 10), Item(556, 7), Item(21880, 1))//Earth surge
        register_spell(95, r, 218, 70, -1, -1, 7855, 1465, 12, 1464, Graphic(1466, 80), 227, 24, 1, 51.0, "Fire Surge", Item(554, 10), Item(556, 7), Item(21880, 1))//Fire surge

		//Teleother
		r.onButton(218, 56) {} //Lumbridge
		r.onButton(218, 61) {} //Falador
		r.onButton(218, 65) {} //Camelot
		
		
		//Misc
		r.onButton(218, 4) { it.openInterface(80) } //Enchant Crossbow Bolt
		
	}
}