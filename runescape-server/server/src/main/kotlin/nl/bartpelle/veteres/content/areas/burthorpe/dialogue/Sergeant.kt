package nl.bartpelle.veteres.content.areas.burthorpe.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 5/17/2016.
 */

object Sergeant {
	
	val messages = arrayListOf("My daughter can hit harder than that!", "Well done, soldier!",
			"Work it!", "Push it!", "You're not out for a Sunday stroll, soldier!")
	val ANIMATIONS = arrayListOf(422, 423, 425)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		//Sergeants
		r.onNpcOption1(4084) { it.message("The Sergeant is busy training the soldiers.") }
		r.onNpcOption1(4085) { it.message("The Sergeant is busy training the soldiers.") }
		
		//Training the group of soldiers
		r.onNpcSpawn(4084) {
			val npc: Npc = it.npc()
			val soldier_list = LinkedList<Npc>()
			
			it.delay(1)
			npc.world().npcs().forEachInAreaKt(Area(2890, 3537, 2896, 3539), { soldier -> soldier_list.add(soldier) })
			
			fun trainTheSoldiers() {
				npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
					s.onInterrupt { trainTheSoldiers() }
					while (true) {
						val animation = ANIMATIONS[npc.world().random(2)]
						
						s.delay(8)
						npc.animate(animation)
						s.delay(2)
						
						for (soldier in soldier_list) {
							soldier.animate(animation)
						}
						
						s.delay(1)
					}
				})
			}
			it.onInterrupt { trainTheSoldiers() }
			trainTheSoldiers()
		}
		
		//Training soldiers at punching bag
		r.onNpcSpawn(4085) {
			val npc: Npc = it.npc()
			
			val north_soldier = LinkedList<Npc>()
			val south_soldier = LinkedList<Npc>()
			
			it.delay(1)
			npc.world().npcs().forEachInAreaKt(Area(2899, 3530, 3901, 3534), { soldier ->
				if (soldier.tile() == Tile(2900, 3533)) {
					north_soldier.add(soldier)
				} else if (soldier.tile() == Tile(2900, 3531)) {
					south_soldier.add(soldier)
				}
			})
			
			fun yellAtSoldier() {
				npc.world().server().scriptExecutor().executeLater(npc, @Suspendable { s ->
					s.onInterrupt { yellAtSoldier() }
					while (true) {
						val soldier = if (npc.world().rollDie(2, 1)) north_soldier else south_soldier
						
						s.delay(npc.world().random(10..15))
						npc.lock()
						soldier.first().lock()
						npc.face(soldier.first())
						s.delay(2)
						npc.sync().shout(messages[npc.world().random(4)])
						s.delay(1)
						soldier.first().sync().shout("Yes sir!")
						soldier.first().animate(855)
						s.delay(2)
						npc.unlock()
						soldier.first().unlock()
					}
				})
			}
			it.onInterrupt { yellAtSoldier() }
			yellAtSoldier()
		}
	}
}
