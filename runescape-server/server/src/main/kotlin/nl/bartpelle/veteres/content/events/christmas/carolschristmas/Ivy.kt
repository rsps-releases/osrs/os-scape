package nl.bartpelle.veteres.content.events.christmas.carolschristmas

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-14.
 */

object Ivy {
	
	val IVY = 7494
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(IVY) @Suspendable {
			if (CarolsChristmas.getStage(it.player()) == 26) {
				it.chatNpc("Thanks for your help! I can't wait to work with Diango<br>once I'm fully rested!", IVY, 568)
			} else {
				it.chatNpc("I'm so tired...", IVY, 610)
			}
		}
	}
	
}
