package nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.content.waitForTile
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.PathQueue
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.MapObj

/**
 * Created by Situations on 5/17/2016.
 */

object GuildEntrance {
	
	@Suspendable fun door(it: Script) {
		val door: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
		val player = it.player()
		
		val attack_lvl = it.player().skills().level(Skills.ATTACK)
		val strength_lvl = it.player().skills().level(Skills.STRENGTH)
		
		//Let's currently disable the required stats as we have pures and what not looking to get their untradables.
		val require_stats = false
		
		if (it.player().tile().x >= 2877) {
			if (attack_lvl + strength_lvl < 130 && require_stats) {
				it.messagebox3("You are not a high enough level to enter the guild. Work on your<br>combat skills some more. You need to have a combined attack and<br>strength level of at least 130.")
			} else {
				if (!it.player().tile().equals(door.tile().transform(0, 0, 0))) {
					it.player().walkTo(door.tile().transform(0, 0, 0), PathQueue.StepType.FORCED_WALK)
					it.waitForTile(door.tile().transform(0, 0, 0))
				}
				it.player().lock()
				it.runGlobal(player.world()) @Suspendable {
					val world = player.world()
					val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
					val spawned = MapObj(Tile(2876, 3546), 24319, door.type(), 1)
					world.removeObj(old, false)
					world.spawnObj(spawned)
					it.delay(2)
					world.removeObjSpawn(spawned)
					world.spawnObj(old)
				}
				it.player().pathQueue().interpolate(2876, 3546, PathQueue.StepType.FORCED_WALK)
				it.waitForTile(Tile(2876, 3546))
				it.delay(1)
				it.player().unlock()
			}
		} else {
			if (!it.player().tile().equals(door.tile().transform(-1, 0, 0))) {
				it.player().walkTo(door.tile().transform(-1, 0, 0), PathQueue.StepType.FORCED_WALK)
				it.waitForTile(door.tile().transform(-1, 0, 0))
			}
			it.player().lock()
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val old = MapObj(door.tile(), door.id(), door.type(), door.rot())
				val spawned = MapObj(Tile(2876, 3546), 24319, door.type(), 1)
				world.removeObj(old, false)
				world.spawnObj(spawned)
				it.delay(2)
				world.removeObjSpawn(spawned)
				world.spawnObj(old)
			}
			it.player().pathQueue().interpolate(2877, 3546, PathQueue.StepType.FORCED_WALK)
			it.waitForTile(Tile(2877, 3546))
			it.delay(1)
			it.player().unlock()
		}
	}
}
