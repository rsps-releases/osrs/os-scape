package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.runGlobal
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 11/9/2015.
 */

object Trapdoor {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		//Open trapdoor
		repo.onObject(1579, s@ @Suspendable {
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			it.player().faceObj(obj)
			it.delay(1)
			it.runGlobal(player.world()) @Suspendable {
				val world = player.world()
				val spawned = MapObj(obj.tile(), 1581, obj.type(), obj.rot())
				world.spawnObj(spawned)
				it.delay(300)
				world.removeObjSpawn(spawned)
			}
			it.message("The trapdoor opens...")
		})
		
		//Climb-down Trapdoor
		repo.onObject(1581) @Suspendable {
			val option: Int = it.player().attrib(AttributeKey.INTERACTION_OPTION)
			
			when (option) {
				1 -> {
					val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
					val player = it.player()
					
					it.player().faceObj(obj)
					it.delay(1)
					player.teleport(3096, 9867)
					it.message("You climb down through the trapdoor...")
				}
				2 -> {
					val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
					val player = it.player()
					
					it.player().faceObj(obj)
					it.delay(1)
					it.runGlobal(player.world()) @Suspendable {
						val world = player.world()
						val spawned = MapObj(obj.tile(), 1579, obj.type(), obj.rot())
						world.spawnObj(spawned)
					}
					it.message("You close the trapdoor.")
				}
			}
		}
	}
	
}
