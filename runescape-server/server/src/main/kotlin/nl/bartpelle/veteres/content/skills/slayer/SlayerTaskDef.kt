package nl.bartpelle.veteres.content.skills.slayer

/**
 * Created by Bart on 11/10/2015.
 */


data class SlayerTaskDef(val weighing: Int, val min: Int, val max: Int, val creatureUid: Int) {
	fun range(): IntRange = IntRange(min, max)
}