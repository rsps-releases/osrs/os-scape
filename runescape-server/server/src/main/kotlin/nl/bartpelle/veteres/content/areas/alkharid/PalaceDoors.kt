package nl.bartpelle.veteres.content.areas.alkharid

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interactionObject
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 18/07/2016.
 */

object PalaceDoors {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(1511, s@ @Suspendable {
			// Left/West double door
			val obj = it.interactionObject()
			val closed: Boolean = obj.rot() == 0
			val obj2: MapObj = it.player().world().objById(1513, obj.tile().transform(1, 0, 0)) ?: return@s
			
			it.player().world().removeObj(obj)
			it.player().world().spawnObj(MapObj(obj.tile(), obj.id(), obj.type(), if (closed) 3 else 0))
			it.player().world().removeObj(obj2)
			it.player().world().spawnObj(MapObj(obj2.tile(), obj2.id(), obj2.type(), if (closed) 3 else 2))
		})
		
		repo.onObject(1513, s@ @Suspendable {
			//obj rot 3 = east/right side double door
			val obj = it.interactionObject()
			val closed: Boolean = obj.rot() == 2
			val obj2: MapObj = it.player().world().objById(1511, obj.tile().transform(-1, 0, 0)) ?: return@s
			
			it.player().world().removeObj(obj)
			it.player().world().spawnObj(MapObj(obj.tile(), obj.id(), obj.type(), if (closed) 3 else 2))
			it.player().world().removeObj(obj2)
			it.player().world().spawnObj(MapObj(obj2.tile(), obj2.id(), obj2.type(), if (closed) 3 else 0))
		})
	}
	
}