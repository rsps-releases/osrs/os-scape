package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 12/16/2015.
 */

object BobBarter {
	
	val BOB_BARTER = 5449
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(BOB_BARTER) @Suspendable {
			it.chatNpc("Hello, chum, fancy buyin' some designer jewellery? They've come all the way from Ardougne! Most pukka!", BOB_BARTER, 590)
			it.chatPlayer("Erm, no. I'm all set, thanks.")
			it.chatNpc("Okay, chum, so what can I do for you? I can tell you the very latest herb & potion prices, or perhaps I could help you decant your potions.", BOB_BARTER, 590)
			when (it.options("Who are you?", "Can you show me the prices for herbs and potions?", "Can you decant things for me?", "I'll leave you to it.")) {
				1 -> {
					it.chatPlayer("Who are you?")
					it.chatNpc("Why I'm Bob! Your friendly seller of smashin' goods!", BOB_BARTER, 590)
					it.chatPlayer("So what do you have to sell?")
					it.chatNpc("Oh, not much at the moment. Cuz, ya know, business being so well and cushie.", BOB_BARTER, 590)
					it.chatPlayer("You don't really look like you're being so successful.")
					it.chatNpc("You plonka! It's all a show, innit! If I let people knows I'm in good business they'll want a share of the moolah!", BOB_BARTER, 590)
					it.chatPlayer("You conveniently have a response for everything.")
					if (it.player().looks().female())
						it.chatNpc("That's the Ardougne way, my darlin'.", BOB_BARTER, 590)
					else
						it.chatNpc("That's the Ardougne way, my good sir.", BOB_BARTER, 590)
				}
				2 -> {
					it.chatPlayer("Can you show me the prices for herbs and potions?")
					it.chatNpc("Sorry, that feature is currently unavailable.", BOB_BARTER, 590)
					it.chatPlayer("I'll leave you to it.")
				}
				3 -> {
					it.chatPlayer("Can you decant things for me?")
					if (!canDecant(it.player())) {
						it.chatNpc("I don't think you've got anything that I can combine!", BOB_BARTER, 590)
					} else {
						decantation(it.player())
						it.chatNpc("There, all done!", BOB_BARTER, 590)
					}
				}
				4 -> {
					it.chatPlayer("I'll leave you to it.")
				}
			}
		}
		r.onNpcOption2(BOB_BARTER) @Suspendable {
			it.chatPlayer("Can you show me the prices for herbs and potions?")
			it.chatNpc("Sorry, that feature is currently unavailable.", BOB_BARTER, 590)
			it.chatPlayer("I'll leave you to it.")
		}
		r.onNpcOption3(BOB_BARTER) @Suspendable {
			it.chatPlayer("Can you decant things for me?")
			if (!canDecant(it.player())) {
				it.chatNpc("I don't think you've got anything that I can combine!", BOB_BARTER, 590)
			} else {
				decantation(it.player())
				it.chatNpc("There, all done!", BOB_BARTER, 590)
			}
		}
	}
	
	
	enum class potions(val one: Int, val two: Int, val three: Int, val four: Int, val emptyItem: Int = 230) {
		ATTACK(125, 123, 121, 2428),
		NOTED_ATTACK(126, 124, 122, 2429),
		SUPER_ATTACK(149, 147, 145, 2436),
		NOTED_SUPER_ATTACK(150, 148, 146, 2437),
		STRENGTH(119, 117, 115, 113),
		NOTED_STRENGTH(120, 118, 116, 114),
		SUPER_STRENGTH(161, 159, 157, 2440),
		NOTED_SUPER_STRENGTH(162, 160, 158, 2441),
		DEFENCE(137, 135, 133, 2432),
		NOTED_DEFENCE(138, 136, 134, 2433),
		SUPER_DEFENCE(167, 165, 163, 2442),
		NOTED_SUPER_DEFENCE(168, 166, 164, 2443),
		RANGING(173, 171, 169, 2444),
		NOTED_RANGING(174, 172, 170, 2445),
		MAGIC(3046, 3044, 3042, 3040),
		NOTED_MAGIC(3047, 3045, 3043, 3041),
		COMBAT(9745, 9743, 9741, 9739),
		NOTED_COMBAT(9746, 9744, 9742, 9740),
		SUPER_COMBAT(12701, 12699, 12697, 12695),
		NOTED_SUPER_COMBAT(12702, 12700, 12698, 12696),
		PRAYER(143, 141, 139, 2434),
		NOTED_PRAYER(144, 142, 140, 2435),
		RESTORE(131, 129, 127, 2430),
		NOTED_RESTORE(132, 130, 128, 2431),
		SUPER_RESTORE(3030, 3028, 3026, 3024),
		NOTED_SUPER_RESTORE(3031, 3029, 3027, 3025),
		ANTI_FIRE(2458, 2456, 2454, 2452),
		NOTED_ANTI_FIRE(2459, 2457, 2455, 2453),
		ZAMORAK_BREW(193, 191, 189, 2450),
		NOTED_ZAMORAK_BREW(194, 192, 190, 2451),
		SARADOMIN_BREW(6691, 6689, 6687, 6685),
		NOTED_SARADOMIN_BREW(6692, 6690, 6688, 6686),
		ENERGY(3014, 3012, 3010, 3008),
		NOTED_ENERGY(3015, 3013, 3011, 3009),
		SUPER_ENERGY(3022, 3020, 3018, 3016),
		NOTED_SUPER_ENERGY(3023, 3021, 3019, 3017),
		ANTIPOSION(179, 177, 175, 2446),
		NOTED_ANTIPOSION(180, 178, 176, 2447),
		SUPER_ANTIPOISON(185, 183, 181, 2448),
		NOTED_SUPER_ANTIPOISON(186, 184, 182, 2449),
		RELICYMS_BALM(4848, 4846, 4844, 4842),
		NOTED_RELICYMS_BALM(4849, 4847, 4845, 4843),
		AGILITY(3038, 3036, 3034, 3032),
		NOTED_AGILITY(3039, 3037, 3035, 3033),
		FISHING(155, 153, 151, 2438),
		NOTED_FISHING(156, 154, 152, 2439),
		STAMINA(12631, 12629, 12627, 12625),
		NOTED_STAMINA(12632, 12630, 12628, 12626),
		SANFEW(10931, 10929, 10927, 10925),
		NTOED_SANFEW(10932, 10930, 10928, 10926),
		GUTHIX_REST(4423, 4421, 4419, 4417, 1980);
	}
	
	@Suspendable fun canDecant(player: Player): Boolean {
		potions.values().forEach() { potion ->
			if (player.inventory().has(potion.one) || player.inventory().has(potion.two) || player.inventory().has(potion.three) || player.inventory().has(potion.four)) {
				return true
			}
		}
		return false
	}
	
	@Suspendable fun decantation(player: Player) {
		potions.values().forEach() { potion ->
			
			val four = potion.four
			val three = potion.three
			val two = potion.two
			val one = potion.one
			
			var totalDoses = 0
			var leftOver = 0
			var emptyPotions = 0
			
			if (player.inventory().has(three)) {
				totalDoses += (3 * player.inventory().count(potion.three))
				emptyPotions += player.inventory().count(potion.three)
				player.inventory().remove(Item(three, player.inventory().count(potion.three)), true)
			}
			if (player.inventory().has(two)) {
				totalDoses += (2 * player.inventory().count(potion.two))
				emptyPotions += player.inventory().count(potion.two)
				player.inventory().remove(Item(two, player.inventory().count(potion.two)), true)
			}
			if (player.inventory().has(one)) {
				totalDoses += (1 * player.inventory().count(potion.one))
				emptyPotions += player.inventory().count(potion.one)
				player.inventory().remove(Item(one, player.inventory().count(potion.one)), true)
			}
			if (totalDoses > 0)
				if (totalDoses >= 4)
					player.inventory().add(Item(four, totalDoses / 4), true)
				else if (totalDoses == 3)
					player.inventory().add(Item(three, 1), true)
				else if (totalDoses == 2)
					player.inventory().add(Item(two, 1), true)
				else if (totalDoses == 1)
					player.inventory().add(Item(one, 1), true)
			if ((totalDoses % 4) != 0) {
				if (player.inventory().count(one) == 1 || player.inventory().count(two) == 1 || player.inventory().count(three) == 1) {
					return
				}
				emptyPotions -= 1
				leftOver = totalDoses % 4;
				if (leftOver == 3)
					player.inventory().add(Item(three, 1), true)
				else if (leftOver == 2)
					player.inventory().add(Item(two, 1), true)
				else if (leftOver == 1)
					player.inventory().add(Item(one, 1), true)
			}
			emptyPotions -= (totalDoses / 4)
			player.inventory().add(Item(potion.emptyItem, emptyPotions), true)
		}
	}
	
}
