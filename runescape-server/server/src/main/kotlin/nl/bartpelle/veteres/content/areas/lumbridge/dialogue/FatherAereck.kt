package nl.bartpelle.veteres.content.areas.lumbridge.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/20/2016.
 */

object FatherAereck {
	
	val FATHER_AERECK = 921
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(FATHER_AERECK) @Suspendable {
			it.chatNpc("Welcome to the church of holy Saradomin.", FATHER_AERECK, 567)
			when (it.options("Who's Saradomin?", "Nice place you've got here."/*, "I'm looking for a quest!"*/)) {
				1 -> {
					it.chatPlayer("Who's Saradomin?", 554)
					it.chatNpc("Surely you have heard of the god, Saradomin?", FATHER_AERECK, 571)
					it.chatNpc("He who creates the forces of goodness and purity in<br>this world? I cannot believe your ignorance!", FATHER_AERECK, 589)
					it.chatNpc("This is the god with more followers than any other! ...At<br>least in this part of the world.", FATHER_AERECK, 589)
					it.chatNpc("He who created this world along with his brothers<br>Guthix and Zamorak?", FATHER_AERECK, 589)
					when (it.options("Oh, THAT Saradomin...", "Oh, sorry. I'm not from this world.")) {
						1 -> {
							it.chatPlayer("Oh, THAT Saradomin...", 588)
							it.chatNpc("There... is only one Saradomin...", FATHER_AERECK, 575)
							it.chatPlayer("Yeah... I, uh, thought you said something else.", 588)
						}
						2 -> {
							it.chatPlayer("Oh, sorry. I'm not from this world.", 588)
							it.chatNpc("...", FATHER_AERECK, 571)
							it.chatNpc("That's... strange.", FATHER_AERECK, 588)
							it.chatNpc("I thought things not from this world were all... You<br>know. Slime and tentacles.", FATHER_AERECK, 589)
							when (it.options("You don't understand. This is a computer game!", "I am - do you like my disguise?")) {
								1 -> {
									it.chatPlayer("You don't understand. This is a computer game!", 588)
									it.chatNpc("I... beg your pardon?", FATHER_AERECK, 575)
									it.chatPlayer("Never mind.", 588)
								}
								2 -> {
									it.chatPlayer("I am - do you like my disguise?", 567)
									it.chatNpc("Aargh! Avaunt foul creature from another dimension!<br>Avaunt! Begone in the name of Saradomin!", FATHER_AERECK, 572)
									it.chatPlayer("Ok, ok, I was only joking...", 567)
								}
							}
						}
					}
				}
				2 -> {
					it.chatPlayer("Nice place you've got here.", 567)
					it.chatNpc("It is, isn't it? It was built over 230 years ago.", FATHER_AERECK, 567)
				}
			}
		}
	}
}
