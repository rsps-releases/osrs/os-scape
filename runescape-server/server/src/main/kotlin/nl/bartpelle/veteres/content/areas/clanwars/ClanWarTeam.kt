package nl.bartpelle.veteres.content.areas.clanwars

import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Jonathan on 2/25/2017.
 */
class ClanWarTeam {
	
	var participants = mutableSetOf<Player>() // Participants active in team 1
	
	var jail = mutableSetOf<Player>() // Participants jailed
	
	var kills = 0
	
	var kingPoints = 0
	
	var oddskullPoints = 0
	
	fun size() = participants.size
	
	fun isEmpty() = size() <= 0
	
	infix operator fun plusAssign(p: Player) {
		participants.add(p)
	}
	
	infix operator fun minusAssign(p: Player) {
		participants.remove(p)
	}
	
	override fun toString() = "ClanWarTeam(participants=$participants, jail=$jail, kills=$kills, kingPoints=$kingPoints, oddskullPoints=$oddskullPoints)"
	
}