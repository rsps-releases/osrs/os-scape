package nl.bartpelle.veteres.content.npcs.misc

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 8/23/2016.
 */


object ElderChaosDruid {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 5) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.attackTimerReady(npc)) {
					//Check to see if we're able to teleport the player beside us.
					if (npc.world().rollDie(5, 1) && npc.tile().distance(target.tile()) > 3 && npc.tile().distance(target.tile()) < 6 &&
							!target.timers().has(TimerKey.ELDER_CHAOS_DRUID_TELEPORT)) {
						target.timers().addOrSet(TimerKey.ELDER_CHAOS_DRUID_TELEPORT, 5)
						target.teleport(npc.world().randomTileAround(npc.tile(), 1))
						target.graphic(409, 0, 4)
						npc.sync().shout("You dare run from us!")
					} else {
						airWave(npc, target)
					}
					
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	@Suspendable fun airWave(npc: Npc, target: Entity) {
		npc.animate(npc.attackAnimation())
		npc.graphic(158, 92, 7)
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
		
		val tileDist = npc.tile().distance(target.tile())
		val delay = Math.max(1, (60 + (tileDist * 12)) / 30)
		
		npc.world().spawnProjectile(npc.tile(), target, 159, 43, 31, 51, 12 * tileDist, 16, 64)
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0)) {
			target.hit(npc, EntityCombat.randomHit(npc), delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(160, 90, 0)) // Cannot protect from this.
		} else {
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(85, 124, 0)) // Cannot protect from this.
		}
	}
}