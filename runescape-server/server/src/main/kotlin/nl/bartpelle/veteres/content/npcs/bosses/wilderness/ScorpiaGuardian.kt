package nl.bartpelle.veteres.content.npcs.bosses.wilderness

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.model.entity.Npc


/**
 * Created by Situations on 3/17/2016.
 */

class ScorpiaGuardian(val scorpia: Npc) {
	
	@JvmField val healscript: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		
		while (EntityCombat.targetOk(npc, scorpia)) { // no can attack needed. no combat.
			npc.face(scorpia)
			
			if (EntityCombat.canAttackMelee(npc, scorpia, true)) {
				scorpia.heal(3)
				it.delay(8)
			} else {
				it.delay(1)
			}
		}
	}
}

