package nl.bartpelle.veteres.content.areas.burthorpe.rogues_den

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.content.mechanics.bankobjects
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 5/19/2016.
 */

object EmeraldBenedict {
	
	val EMERALD_BENEDICT = 3194
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(EMERALD_BENEDICT) @Suspendable {
			it.chatNpc("Got anything you don't want to lose?", EMERALD_BENEDICT, 592)
			when (it.options("Yes actually, can you help?", "Yes, but can you show me my PIN settings?",
					"Yes, but I'd like to collect items now.", "Yes thanks, and I'll keep hold of it too.")) {
				1 -> Bank.open(it.player(), it)
				2 -> it.chatNpc("We don't have bank pin support.", EMERALD_BENEDICT, 612)
				3 -> it.chatNpc("We don't have grand exchange support.", EMERALD_BENEDICT, 612)
				4 -> it.chatPlayer("Yes thanks, and I'll keep hold of it too.", 571)
			}
		}
		r.onNpcOption2(EMERALD_BENEDICT) @Suspendable { Bank.open(it.player(), it) }
		r.onNpcOption3(EMERALD_BENEDICT) @Suspendable { it.chatNpc("We don't have grand exchange support.", EMERALD_BENEDICT, 612) }
		r.onItemOnNpc(EMERALD_BENEDICT, s@ @Suspendable {
			val player = it.player()
			val itemid: Int = player.attrib(AttributeKey.ITEM_ID)
			val slot: Int = player.attrib(AttributeKey.ITEM_SLOT)
			val def: ItemDefinition = player.world().definitions().get(ItemDefinition::class.java, itemid) ?: return@s
			
			bankobjects.noteLogic(it, itemid, slot, def)
		})
	}
}
