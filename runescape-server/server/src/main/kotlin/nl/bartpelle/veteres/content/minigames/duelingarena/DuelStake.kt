package nl.bartpelle.veteres.content.minigames.duelingarena

import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking.duelOffer
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking.duelPartner
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking.refreshMyStake
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 10/1/2016.
 */

object DuelStake {
	
	// Take an item out of the stake screen
	fun takeItem(player: Player, amount: Int) {
		val partner = player.duelPartner() ?: null
		
		if (partner == null) {
			player.message("You're not staking anybody so you can't withdraw items.")
			return
		}
		if (!player.interfaces().visible(481)) {
			player.message("You're not in a stake to withdraw items.")
			return
		}
		if (!partner.interfaces().visible(481)) {
			player.message("Your partner isn't staking so you can't withdraw items.")
			return
		}
		if (Staking.in_duel(player) || Staking.in_duel(partner)) {
			player.message("You can't take out items during a stake.")
			return
		}
		
		if (player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return
		
		val item = player.duelOffer()[player[AttributeKey.BUTTON_SLOT]] ?: return
		val result = player.duelOffer().remove(Item(item.id(), amount), true)
		val removed = result.completed()
		
		for (i in result.effectedSlots())
			partner.invokeScript(1450, 31522844, i, 31522889)
		
		player.invokeScript(1450, 31522844, -1, 31522889)
		
		player.inventory().add(Item(item.id(), removed), true)
		
		partner.message("Duel Stake removal: $removed x ${item.unnote(player.world()).definition(player.world()).name} removed!")
		
		player.refreshMyStake()
		
		player.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		partner.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		
		arrayListOf(player, partner).forEach { staker ->
			staker.write(InterfaceText(481, 17, "")) // Stake value
			staker.write(InterfaceText(481, 25, "")) // Stake value
			itemAddedOrRemovedWarning(staker)
		}
	}
	
	// Put an item into the stake screen
	fun offerItem(player: Player, amount: Int) {
		val partner = player.duelPartner() ?: null
		
		if (partner == null) {
			player.message("You're not staking anybody.")
			return
		}
		
		if (!player.interfaces().visible(481)) {
			player.message("You're not in a stake.")
			return
		}
		
		if (!partner.interfaces().visible(481)) {
			player.message("Your partner isn't staking.")
			return
		}
		
		if (Staking.in_duel(player) || Staking.in_duel(partner)) {
			player.message("You can't modify items during a stake.")
			return
		}
		
		if (partner.ironMode() != IronMode.NONE) {
			player.message("Your partner is an Iron man, and cannot receive stakes.")
			return
		}
		
		if (player.attribOr<Int>(AttributeKey.BUTTON_SLOT, -1) == -1) return
		
		val old = player.duelOffer().copy()
		val item = player.inventory()[player[AttributeKey.BUTTON_SLOT]] ?: return
		val tradable = Item(item).tradable(player.world()) && player.world().flagTradable(item)
		val admin = player.privilege().eligibleTo(Privilege.ADMIN)
		
		if (!tradable) {
			if (!admin) {
				player.message("You can't stake this item.")
				return
			} else {
				player.message("Note: As an admin you are able to stake untradable items.")
			}
		}
		
		val result = player.inventory().remove(Item(item.id(), amount), true)
		val removed = result.completed()
		
		for (i in result.effectedSlots())
			partner.invokeScript(1450, 31522844, i, 31522889)
		player.invokeScript(1450, 31522844, -1, 31522889)
		
		partner.message("Duel Stake addition: $removed x ${item.unnote(player.world()).definition(player.world()).name} added!")
		
		player.duelOffer().add(Item(item.id(), removed), true)
		player.refreshMyStake()
		
		player.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		partner.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		
		arrayListOf(player, partner).forEach { staker ->
			staker.write(InterfaceText(481, 17, "")) // Stake value
			staker.write(InterfaceText(481, 25, "")) // Stake value
			itemAddedOrRemovedWarning(staker)
		}
		
		
		//player.write(UpdateItems(134, old, player.duelOffer().copy())) // Update ours
		//partner.write(UpdateItems(134, -2, 60937, old, player.duelOffer().copy())) // Update our offer for our partner
		
	}
	
	fun itemAddedOrRemovedWarning(player: Player) {
		val partner = player.duelPartner() ?: return
		player.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		partner.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		
		val updated = player.varps().varp(Varp.STAKE_SETTINGS)
		partner.varps().varp(Varp.STAKE_SETTINGS, updated)
		
		arrayOf(player, partner).forEach { staker ->
			staker.invokeScript(1441, 31588458, 5)
		}
		
		// Delay before you can accept.
		player.timers().extendOrRegister(TimerKey.DUEL_ACCEPT_DELAY, 6)
		partner.timers().extendOrRegister(TimerKey.DUEL_ACCEPT_DELAY, 6)
	}
	
}