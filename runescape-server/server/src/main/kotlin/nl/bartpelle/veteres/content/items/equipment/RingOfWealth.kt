package nl.bartpelle.veteres.content.items.equipment

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.content.skills.slayer.SlayerGem
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 08/04/2017.
 */
object RingOfWealth {
	
	val ROW = 2572
	val ROW5 = 11980
	val ROW4 = 11982
	val ROW3 = 11984
	val ROW2 = 11986
	val ROW1 = 11988
	val ROW_I = 12785
	val ROW_I5 = 20786
	val ROW_I4 = 20787
	val ROW_I3 = 20788
	val ROW_I2 = 20789
	val ROW_I1 = 20790
	
	@ScriptMain @JvmStatic fun register(repo: ScriptRepository) {
		for (ring in intArrayOf(ROW, ROW1, ROW2, ROW3, ROW4, ROW5, ROW_I, ROW_I1, ROW_I2, ROW_I3, ROW_I4, ROW_I5)) {
			repo.onItemOption3(ring, s@ @Suspendable {
				when (it.options("View boss log.", "Toggle coin collection ${if (VarbitAttributes.varbiton(it.player(), Varbit.ROW_COIN_COLLECTION_OFF)) "on" else "off"}.")) {
					1 -> SlayerGem.sendLog(it.player())
					2 -> {
						toggle_collection(it)
					}
				}
			})
			repo.onItemOption4(ring, s@ @Suspendable {
				doteleports(it, ring)
			})
			repo.onEquipmentOption(1, ring) {
				doteleports(it, ring)
			}
			repo.onEquipmentOption(5, ring) {
				SlayerGem.sendLog(it.player())
			}
			repo.onEquipmentOption(6, ring) {
				toggle_collection(it)
			}
		}
		for (ring in intArrayOf(ROW1, ROW2, ROW3, ROW4, ROW5, ROW_I1, ROW_I2, ROW_I3, ROW_I4, ROW_I5)) {
			repo.onEquipmentOption(1, ring) { tele(it, Tile(2564, 3847)) }
			repo.onEquipmentOption(2, ring) { tele(it, Tile(3164, 3464)) }
			repo.onEquipmentOption(3, ring) { tele(it, Tile(3002, 3377)) }
			repo.onEquipmentOption(4, ring) { tele(it, Tile(2838, 10127)) }
		}
	}
	
	@Suspendable private fun doteleports(it: Script, ring: Int) {
		if (ring == ROW || ring == ROW_I) {
			it.message("You will need to recharge your ring at the Fountain of Rune before you can use it again.")
		} else {
			val target = when (it.options("Miscellania", "Grand Exchange entrance", "Falador Park", "Dondakan (Kaldagrim)")) {
				1 -> Tile(2564, 3847)
				2 -> Tile(3164, 3464)
				3 -> Tile(3002, 3377)
				4 -> Tile(2838, 10127)
				else -> it.player().tile()
			}
			if (Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
				Teleports.basicTeleport(it, target, 3872, Graphic(1237, 92))
			}
		}
	}
	
	fun tele(it: Script, tile: Tile) {
		if (Teleports.canTeleport(it.player(), true, TeleportType.GENERIC)) {
			Teleports.basicTeleport(it, tile, 3872, Graphic(1237, 92))
		}
	}
	
	private fun toggle_collection(it: Script) {
		VarbitAttributes.toggle(it.player(), 17)
		it.message("The Ring of Wealths' coin collection feature is now: ${if (VarbitAttributes.varbiton(it.player(), Varbit.ROW_COIN_COLLECTION_OFF)) "off" else "on"}.")
	}
	
}