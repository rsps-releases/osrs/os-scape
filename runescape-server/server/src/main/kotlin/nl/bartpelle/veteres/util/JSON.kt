package nl.bartpelle.veteres.util

import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken

/**
 * Created by Jonathan on 2/12/2017.
 */
object JSON {
	
	private val parser = JsonParser()
	private val gson = Gson()
	
	@JvmStatic fun parse(json: String) = parser.parse(json)
	
	private val mapType = object : TypeToken<Map<Int, Long>>() {
	}.type
	
	@JvmStatic fun parseMap(json: String): Map<Int, Long> = gson.fromJson(json, mapType)
	
	@JvmStatic fun toJson(src: Any) = gson.toJson(src)
	
}