package nl.bartpelle.veteres.content.areas.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.npcs.pets.Pet
import nl.bartpelle.veteres.content.npcs.pets.PetAI
import nl.bartpelle.veteres.content.npcs.pets.PetAI.pet
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Mack on 7/17/2017.
 */
object TzhaarKetKeh {
	
	//This is our npc constant representing the npc the player is chatting with.
	const val CHATTER = 7690
	
	//The infernal cape item id constant.
	const val INFERNAL_CAPE = 21295
	
	//The fire cape item id constant.
	const val FIRE_CAPE = 6570
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(CHATTER) @Suspendable {
			it.chatNpc("Yes, JalYt-Mej-Xo-${it.player().name()}?", CHATTER)
			when (it.options("What is this place?", "What happened here?", "Can I go down there?", "View best run times.", "Nevermind.")) {
				1 -> {
					placeDescrDialogue(it)
				}
				2 -> {
					whatHappenedDialogue(it)
				}
				3 -> {
					goDownDialogue(it)
				}
				4 -> {
					viewBestRunTimes(it.player())
				}
			}
		}
		r.onNpcOption2(CHATTER) @Suspendable {
			it.chatPlayer("I have a spare infernal cape, perhaps I could offer it back to you for a littlun to take with me?")
			if (!it.player().inventory().contains(INFERNAL_CAPE)) {
				it.chatNpc("Sorry JalYt, I don't think you do have a cape on you.", CHATTER)
			} else {
				it.chatNpc("Maybe. Hand it over and if you make lucky TokKul toss, it can be your JalYt.", CHATTER)
				it.chatNpc("But do know, you do not get cape back.", CHATTER)
				when (it.optionsTitled("<col=ff0000>Sacrifice infernal cape for a chance at Jal-Nib-Rek?", "Yes, I accept that I won't get my cape back.", "No, I want to keep my cape.")) {
					1 -> {
						it.player().inventory().remove(INFERNAL_CAPE, true)
						val chance = 1
						if (it.player().world().rollDie(100, chance)) {
							unlockPet(it.player())
							it.chatNpc("Lucky TokKul toss played in your favor, JalYt. Take this Jal-Rek-Nib as your prize.", CHATTER)
						} else {
							it.chatNpc("No Jal-Nib-Rek for you.", CHATTER)
						}
					}
					2 -> {
						it.chatPlayer("Sorry, I want to keep my cape.")
					}
				}
			}
		}
		r.onNpcOption3(CHATTER) @Suspendable {
			viewBestRunTimes(it.player())
		}
		r.onItemOnNpc(CHATTER) @Suspendable {
			val item: Int = it.player().attrib(AttributeKey.ITEM_ID)
			if (item == FIRE_CAPE) {
				goDownDialogue(it)
			}
		}
	}
	
	//A set of dialogues for easy access to the "What is the place?" dialogue option.
	@Suspendable fun placeDescrDialogue(it: Script) {
		it.chatPlayer("What is this place?")
		it.chatNpc("This is our birthing pool. TzHaar are born from eggs incubated in this large pool here.", CHATTER)
		it.chatNpc("When hatched, we retain memories. Memories and knowledge passed on from ancestors who returned to the lava.", CHATTER)
		it.chatPlayer("Something doesn't seem right... There's a large opening.")
		when (it.options("What happened here?", "Can I go down there?", "Nevermind")) {
			1 -> {
				whatHappenedDialogue(it)
			}
			2 -> {
				goDownDialogue(it)
			}
		}
	}
	
	//A set of dialogues for easy access to the "What happened here?" dialogue
	@Suspendable fun whatHappenedDialogue(it: Script) {
		it.chatPlayer("What happened here?")
		it.chatNpc("We went too far... way too far, we need the memories. JalYt would not understand.", CHATTER)
		it.chatPlayer("Please help me to understand then.")
		it.chatNpc("We TzHaar have special ability. When incubated in lava, a hatched TzHaar retains knowledge and memories of ancestors who returned to the lava.", CHATTER)
		it.chatNpc("Not all memories though, those about earliest ancestors were not being retained. We experiment.", CHATTER)
		it.chatNpc("We increased the depth at which the eggs are incubated. Over time this worked, newly hatched TzHaar had more memories of lost history!", CHATTER)
		it.chatPlayer("That sounds fantastic though, uncovering the past to determine where your species originated?")
		it.chatNpc("Yes, but you JalYt do not understand, we kept pushing it. We wanted answers, but too deep we went.", CHATTER)
		it.chatNpc("Eventually the pool collapsed into a sink hole, huge inferno there. This was big ancient incubation chamber.", CHATTER)
		it.chatNpc("We hatched eggs in there and it was not a good decision. They were so different, bigger, stronger and fought each other for dominance.", CHATTER)
		it.chatNpc("Instead of knowledge, we now have a prison of dangerous TzHaar creatures... and they're not under our control.", CHATTER)
		it.chatNpc("One grew incredibly strong, powerful, he controls those creatures in the prison and TzKal-Zuk, as we call him is hungry.", CHATTER)
		it.chatNpc("Hungry for memories, memories beyond the Inferno, which only TzHaar on surface can provide.", CHATTER)
		it.chatPlayer("Do you know what he's planning? What are you doing?")
		it.chatNpc("We can only guess one day he will want to scale up onto the surface himself. We are keeping him at bay by offering sacrifices for him to feast on.", CHATTER)
		it.chatPlayer("Is there no way to take him out?")
		it.chatNpc("We hoped you, with your fancy JalYt weaponry would be our best hope. You proved your commitment by sacrificing your fire cape.", CHATTER)
		when (it.options("What is this place?", "Can I go down there?", "Nevermind")) {
			1 -> {
				placeDescrDialogue(it)
			}
			2 -> {
				goDownDialogue(it)
			}
		}
	}
	
	//A set of dialogues for easy access to the "Can I go down there?" option.
	@Suspendable fun goDownDialogue(it: Script) {
		it.chatPlayer("Can I go do there?")
		if (it.player().attribOr<Boolean>(AttributeKey.INFERNAL_SACRIFICE, false)) {
			it.chatNpc("You can head into The Inferno anytime. We trust you to help us JalYt-Ket-Xo-${it.player().name()}.", CHATTER)
		} else {
			it.chatNpc("JalYt, we need help, but this is extremely dangerous. TzHaar not strong enough to take control back.", CHATTER)
			it.chatNpc("We are worried about trusting JalYt to take on this task for us.", CHATTER)
			it.chatPlayer("Let me prove it.")
			it.chatNpc("I have one idea, if you could sacrifice your fire cape to me, I can take your word.", CHATTER)
			if (it.player().equipment().has(FIRE_CAPE) && !it.player().inventory().has(FIRE_CAPE)) {
				it.chatPlayer("Let me just take it off first.")
				return
			}
			if (!it.player().inventory().has(FIRE_CAPE)) {
				it.chatNpc("Sorry JalYt, I don't think you do have a cape on you.", CHATTER)
				return
			}
			when (it.options("Yes, I want to sacrifice my cape.", "No, I want to keep it!")) {
				1 -> {
					it.chatPlayer("Okay, I'd like to sacrifice my fire cape.")
					it.chatNpc("Very well, just hand it over.", CHATTER)
					when (it.optionsTitled("Really sacrifice your Fire cape?", "No, I want to keep it!", "Yes, hand it over.")) {
						1 -> {
							it.chatPlayer("Sorry, I want to keep it.")
						}
						2 -> {
							it.itemBox("You hand over your cape to TzHaar-Ket-Keh.", 6570)
							it.chatNpc("Give it your best shot, JalYt-Ket-${it.player().name()}.", CHATTER)
							it.player().inventory().remove(6570, true)
							it.player().putattrib(AttributeKey.INFERNAL_SACRIFICE, true)
							it.player().varps().varbit(InfernoPool.JUMP_IN_VARBIT, 2)
						}
					}
				}
			}
		}
		it.chatPlayer("I'll do my best.")
	}
	
	fun unlockPet(player: Player) {
		if (!PetAI.hasUnlocked(player, Pet.JAL_NIB_REK)) {
			// Unlock the varbit. Just do it, rather safe than sorry.
			player.varps().varbit(Pet.JAL_NIB_REK.varbit, 1)
			
			// RS tries to add it as follower first. That only works if you don't have one.
			val currentPet = player.pet()
			if (currentPet == null) {
				player.message("You have a funny feeling like you're being followed.")
				PetAI.spawnPet(player, Pet.JAL_NIB_REK, false)
			} else {
				// Sneak it into their inventory. If that fails, fuck you, no pet for you!
				if (player.inventory().add(Item(Pet.JAL_NIB_REK.item), true).success()) {
					player.message("You feel something weird sneaking into your backpack.")
				} else {
					player.message("Speak to Probita to claim your pet!")
				}
			}
			
			player.world().broadcast("<col=844e0d><img=22> ${player.name()} has unlocked the pet: ${Item(Pet.JAL_NIB_REK.item).name(player.world())}.")
		} else {
			
			//Already unlocked!
			player.message("You have a funny feeling like you would have been followed...")
		}
	}
	
	/**
	 * Opens up the leaderboard.
	 */
	fun viewBestRunTimes(player: Player) {
		InfernoLeaderboard.display(player)
	}
}