package nl.bartpelle.veteres.content.areas.desert.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.content.itemUsed
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 19/01/2016. #Gundrilla
 */

object Jail {
	
	val IRENA = 4640
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		repo.onItemOnNpc(IRENA) @Suspendable {
			if (it.itemUsed()!!.id() == 668) { // Blurite
				offerOres(it)
			}
		}
		
		repo.onRegionExit(13103) {
			// Make sure blurite removed when
			for (i in it.player().inventory().items()) { // Clear blurite ores.
				if (i == null) continue
				if (i!!.id() == 668) {
					it.player().inventory().remove(i, true)
				}
			}
			it.player().message("<col=FF0000>Any blurite you were carrying was taken by Irena.")
		}
		
		// Talk to the NPC that gives a pickaxe, and you give ores to to escape the jail.
		repo.onNpcOption1(IRENA) @Suspendable {
			when (it.options("Pickaxe", "Give ores")) {
				1 -> {
					// Make sure people can mine their way out.
					if (!it.player().inventory().has(1265)) {
						it.player().inventory().add(Item(1265, 1), false)
						it.itemBox("Irena hands you a pickaxe.", 1265) // bronze axe, not a freaking rune pickaxe on eco LOL
					} else {
						it.chatNpc("You've got a pickaxe on you!", IRENA)
					}
				}
				2 -> {
					// Give ores to the NPC. Remove from inv and add to total ores mined.
					offerOres(it)
				}
			}
		}
	}
	
	@JvmStatic @Suspendable fun offerOres(it: Script) {
		it.player().putattrib(AttributeKey.JAIL_ORES_MINED, it.player().attribOr<Int>(AttributeKey.JAIL_ORES_MINED, 0) + Math.max(0, it.player().inventory().count(668)))
		val amountOfOresMined: Int = it.player().attribOr<Int>(AttributeKey.JAIL_ORES_MINED, 0)
		
		if (amountOfOresMined >= it.player().attribOr<Int>(AttributeKey.JAIL_ORES_TO_ESCAPE, 0)) {
			// You're free!
			it.itemBox("You can now leave! Don't break the rules ye? Sick.", 668)
			it.player().putattrib(AttributeKey.JAILED, 0)
			it.player().putattrib(AttributeKey.JAIL_ORES_MINED, 0)
		} else {
			// Tell the player how many more ores they are required to mine to exit.
			it.player().inventory().remove(Item(668, amountOfOresMined), true)
			it.itemBox("You try giving her ores. " + Math.max(0, it.player().attribOr<Int>(AttributeKey.JAIL_ORES_TO_ESCAPE, 0) - amountOfOresMined) + " left...", 668)
		}
	}
	
}
