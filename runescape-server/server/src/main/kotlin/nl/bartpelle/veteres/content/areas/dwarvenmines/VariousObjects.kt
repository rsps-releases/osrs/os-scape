package nl.bartpelle.veteres.content.areas.dwarvenmines

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.instances.MageBankInstance
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.mechanics.Ladders
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.LocationUtilities

/**
 * Created by Bart on 10/5/2015.
 */

object VariousObjects {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onObject(11867) @Suspendable { Ladders.ladderDown(it, Tile(3018, 9850), true) }// Ladder down @ mines
		r.onObject(17387) @Suspendable { Ladders.ladderUp(it, Tile(3018, 3450), true) }// Ladder up from mines
		
		r.onObject(23969) @Suspendable {
			// Stairs up from mines
			val obj: MapObj = it.player()[AttributeKey.INTERACTION_OBJECT]
			if (obj.tile().x == 3059 && obj.tile().z == 9776) {
				it.player().teleport(Tile(3061, 3376))
			}
		}
		
		//THIS LADDER IS IN FALLY AND WILDY. CHECK COORDS OF OBJECT.
		r.onObject(16664) @Suspendable {
			val obj: MapObj = it.player()[AttributeKey.INTERACTION_OBJECT]
			if (obj.tile().x == 3058 && obj.tile().z == 3376) {
				//fally down
				it.player().teleport(Tile(3058, 9776))
			}
			if (obj.tile().x == 3044 && obj.tile().z == 3924) {
				//mb stairs down
				it.player().teleport(Tile(3044, 10322))
			}

            if (MageBankInstance.mageBank!!.contains(it.player())) {
                it.player().teleport(LocationUtilities.dynamicTileFor(Tile(3044, 10322), MageBankInstance.mageBankDungeon))
            }
			
			// Yanile dungeon down
			if (obj.tile().equals(2603, 3078)) {
				it.player().teleport(2602, 9479)
			}
		}
		
		//THIS LADDER IS IN FALLY AND WILDY. CHECK COORDS OF OBJECT.
		r.onObject(16665) @Suspendable {
			val obj: MapObj = it.player()[AttributeKey.INTERACTION_OBJECT]

            if (MageBankInstance.mageBankDungeon!!.contains(it.player())) {
                it.player().teleport(LocationUtilities.dynamicTileFor(Tile(3044, 3927), MageBankInstance.mageBankWildTileCorner, MageBankInstance.mageBank))
            }

			if (obj.tile().x == 3044 && obj.tile().z == 10324) {
				//mb stairs up
				it.player().teleport(Tile(3044, 3927))
			}
			
			// Yanile dungeon up
			if (obj.tile().equals(2603, 9478)) {
				it.player().teleport(2603, 3078)
			}
		}
		
		r.onObject(24057) {
			val obj: MapObj = it.player()[AttributeKey.INTERACTION_OBJECT]
			obj.replaceWith(MapObj(Tile(3061, 3375), 24058, obj.type(), 2), it.player().world())
		}
		r.onObject(24058) {
			val obj: MapObj = it.player()[AttributeKey.INTERACTION_OBJECT]
			obj.replaceWith(MapObj(Tile(3061, 3374), 24057, obj.type(), 1), it.player().world())
		}
	}
	
}
