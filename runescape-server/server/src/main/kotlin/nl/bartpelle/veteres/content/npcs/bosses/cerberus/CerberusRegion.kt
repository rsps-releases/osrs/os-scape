package nl.bartpelle.veteres.content.npcs.bosses.cerberus

import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.Tile
import java.util.*

/**
 * Created by Jason MacKeigan on 2016-09-19 at 6:58 PM
 */
enum class CerberusRegion(val id: Int, val area: Area) {
	WEST(4883, Area(1228, 1243, 1252, 1257)),
	EAST(5395, Area(1356, 1243, 1380, 1257)),
	NORTH(5140, Area(1292, 1307, 1316, 1321));
	
	val spawn = Tile(area.x1() + 12, area.z1() + 7)
	
	val flames = arrayOf(
			Tile(spawn.x - 1, spawn.z - 8),
			Tile(spawn.x, spawn.z - 8),
			Tile(spawn.x + 1, spawn.z - 8)
	)
	
	companion object {
		
		fun valueOfRegion(id: Int): Optional<CerberusRegion> {
			return Optional.ofNullable(CerberusRegion.values().find { it.id == id })
		}
		
	}
}