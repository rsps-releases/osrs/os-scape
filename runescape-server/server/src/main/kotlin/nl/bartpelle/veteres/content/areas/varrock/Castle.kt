package nl.bartpelle.veteres.content.areas.varrock

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.script.ScriptRepository


/**
 * Created by Situations on 12/22/2015.
 */

object Castle {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onObject(11807) @Suspendable {
			it.player().lock()
			it.delay(1)
			it.player().teleport(Tile(it.player().tile().x, 3476, 1))
			it.player().unlock()
		}
		repo.onObject(11799) @Suspendable {
			it.player().lock()
			it.delay(1)
			it.player().teleport(Tile(it.player().tile().x, 3472, 0))
			it.player().unlock()
		}
	}
	
}