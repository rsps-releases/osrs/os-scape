package nl.bartpelle.veteres.content.areas.edgeville.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.script.ScriptRepository

object PrisonPete {
	
	@ScriptMain @JvmStatic fun register(repository: ScriptRepository) {
		repository.onNpcOption1(368, @Suspendable {
			it.chatNpc("I got hacked too many times because I didn't setup 2-factor authentication and now they won't let me play!", 368, 598)
			if (it.player().twofactorKey() != null) {
				it.chatPlayer("Well it's a good thing I have 2FA setup.", 605)
			} else {
				it.chatPlayer("...maybe I should setup 2FA.", 596)
			}
		})
	}
}