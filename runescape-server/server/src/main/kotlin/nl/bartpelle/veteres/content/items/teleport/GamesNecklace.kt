package nl.bartpelle.veteres.content.items.teleport

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.col
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.interfaces.magicbook.Teleports
import nl.bartpelle.veteres.content.mechanics.TeleportType
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.WidgetTimer
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.SendWidgetTimer
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 3/12/2016.
 */

object GamesNecklace {
	
	enum class GamesNecklace(val id: Int, val charges: Int, val result: Int) {
		EIGHT(3853, 8, 3855),
		SEVEN(3855, 7, 3857),
		SIX(3857, 6, 3859),
		FIVE(3859, 5, 3861),
		FOUR(3861, 4, 3863),
		THREE(3863, 3, 3865),
		TWO(3865, 2, 3867),
		ONE(3867, 1, -1);
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		GamesNecklace.values().forEach { necklace ->
			r.onItemOption4(necklace.id) @Suspendable { rub(necklace.id, it, necklace.result, necklace.charges) }
			r.onEquipmentOption(1, necklace.id) { doMagic(necklace.id, Tile(2898, 3552, 0), it, necklace.result, necklace.charges, true) }
			r.onEquipmentOption(2, necklace.id) { doMagic(necklace.id, Tile(2519, 3572, 0), it, necklace.result, necklace.charges, true) }
			r.onEquipmentOption(3, necklace.id) { doMagic(necklace.id, Tile(2946, 4384, 2), it, necklace.result, necklace.charges, true) }
			//r.onEquipmentOption(4, necklace.id) { doMagic(necklace.id, Tile(3293, 3163, 0), it, necklace.result, necklace.charges, true) }
			r.onEquipmentOption(5, necklace.id) { doMagic(necklace.id, it.player().world().randomTileAround(Tile(1627, 3941, 0), 2), it, necklace.result, necklace.charges, true) }
		}
	}
	
	@Suspendable fun rub(usedId: Int, script: Script, result: Int, currentCharges: Int) {
		script.message("You rub the necklace...")
		
		when (script.optionsTitled("Where would you like to teleport to?", "Burthorpe.", "Barbarian Outpost.", "Corporeal Beast.", "<str>Tears of Guthix.</str>", "Wintertodt Camp.")) {
			1 -> doMagic(usedId, Tile(2898, 3552, 0), script, result, currentCharges)
			2 -> doMagic(usedId, Tile(2519, 3572, 0), script, result, currentCharges)
			3 -> doMagic(usedId, Tile(2968, 4383, 2), script, result, currentCharges)
		//4 -> doMagic(usedId, Tile(3293, 3163, 0), script, result, currentCharges) //TODO: Need to add Tears of Guthix teleport
			5 -> doMagic(usedId, script.player().world().randomTileAround(Tile(1627, 3941, 0), 2), script, result, currentCharges)
		}
	}
	
	@Suspendable fun doMagic(usedId: Int, targetTile: Tile, it: Script, result: Int, currentCharges: Int, equipment: Boolean = false) {
		if (!Teleports.canTeleport(it.player(), true, TeleportType.ABOVE_20_WILD)) {
			return
		}
		DeadmanMechanics.attemptTeleport(it)
		
		val slot: Int = it.player()[AttributeKey.ITEM_SLOT]
		Teleports.basicTeleport(it, targetTile)
		
		if (currentCharges == 1)
			it.message("Your games necklace crumbles to dust.".col("7F00FF"))
		else if (currentCharges == 2)
			it.message("Your games necklace has one use left.".col("7F00FF"))
		else
			it.message("Your games necklace has ${numToStr(currentCharges - 1)} uses left.".col("7F00FF"))
		
		if (equipment) {
			if (usedId == 3867) {
				it.player().equipment().remove(Item(3867), true, EquipSlot.AMULET)
				it.player().unlock()
				return
			}
			if (it.player().equipment().remove(Item(usedId), true, EquipSlot.AMULET).success())
				it.player().equipment().add(Item(result), true, EquipSlot.AMULET)
		} else {
			if (usedId == 3867) {
				it.player().inventory().remove(Item(3867), true, EquipSlot.AMULET)
				it.player().unlock()
				return
			}
			if (it.player().inventory().remove(Item(usedId), true, slot).success())
				it.player().inventory().add(Item(result), true, slot)
		}
		
		it.player().timers().cancel(TimerKey.FROZEN)
		it.player().timers().cancel(TimerKey.REFREEZE)
		it.player().write(SendWidgetTimer(WidgetTimer.BARRAGE, 0))
	}
	
	fun numToStr(num: Int): String = when (num) {
		2 -> "two"
		3 -> "three"
		4 -> "four"
		5 -> "five"
		6 -> "six"
		7 -> "seven"
		else -> "?"
	}
	
}
