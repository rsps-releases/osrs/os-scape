package nl.bartpelle.veteres.content.items.boxes

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.dawnguard.DataStore
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.items.equipment.ZulrahItems
import nl.bartpelle.veteres.content.random
import nl.bartpelle.veteres.fs.DefinitionRepository
import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.script.ScriptRepository
import java.util.*

/**
 * Created by Situations on 1/25/2016.
 */

object MysteryBoxPvP {
	
	val MYSTERY_BOX = 6199 //mysterious box item ID
	
	val LOW = arrayOf(Item(4708, 1), //Ahrim's hood
			Item(4712, 1), //Ahrim's robetop
			Item(4714, 1), //Ahrim's robeskirt
			Item(4710, 1), //Ahrim's staff
			Item(4716, 1), //Dharok's helm
			Item(4720, 1), //Dharok's platebody
			Item(4722, 1), //Dharok's platelegs
			Item(4718, 1), //Dharok's greataxe
			Item(4724, 1), //Guthan's helm
			Item(4728, 1), //Guthan's platebody
			Item(4730, 1), //Guthan's chainskirt
			Item(4726, 1), //Guthan's warspear
			Item(4745, 1), //Torag's helm
			Item(4749, 1), //Torag's platebody
			Item(4751, 1), //Torag's platelegs
			Item(4747, 1), //Torag's hammers
			Item(4753, 1), //Verac's helm
			Item(4757, 1), //Verac's brassard
			Item(4759, 1), //Verac's plateskirt
			Item(4755, 1), //Verac's flail
			Item(4732, 1), //Karil's coif
			Item(4736, 1), //Karil's leathertop
			Item(4738, 1), //Karil's leatherskirt
			Item(4734, 1))    //Karil's crossbow
	
	val MEDIUM_LOW = arrayOf(Item(13307, 1500), //1500 Blood money
			Item(13307, 2000), //2000 Blood money
			Item(13307, 2500), //2500 Blood money
			Item(13442, 100), //100 Anglerfish
			Item(13442, 50), //50 Anglerfish
			Item(11235, 1), //Dark bow
			Item(12829, 1), //Spirit shield
			Item(4151, 1), //Abyssal whip
			Item(4153, 1), //Granite maul
			Item(6920, 1), //Infinity boots
			Item(11128, 1))   //Berseker necklace
	
	val MEDIUM = arrayOf(Item(13307, 5000), //5k Blood money
			Item(13307, 6000), //6k Blood money
			Item(13307, 8000), //8k Blood money
			Item(12696, 250), //250 Super combat potions
			Item(13442, 250), //250 Anglerfish
			Item(12696, 500), //500 Super combat potions
			Item(13442, 500), //500 Anglerfish
			Item(12006, 1), //Abyssal tentacle
			Item(12002, 1), //Occult necklace
			Item(6585, 1), //Amulet of fury
			Item(11283, 1), //Dragonfire shield
			Item(12904, 1), //Toxic staff of the dead
			Item(11791, 1), //Staff of the dead
			Item(4224, 1), //New crystal shield
			Item(12831, 1), //Blessed spirit shield
			Item(11926, 1), //Odium ward
			Item(11924, 1)) //Malediction ward
	
	val MEDIUM_HIGH = arrayOf(Item(12931, 1), //Serpentine helm
			Item(13227, 1), //Eternal crystal
			Item(13229, 1), //Pegasian crystal
			Item(13231, 1), //Primordial crystal
			Item(11828, 1), //Armadyl chestplate
			Item(11830, 1), //Armadyl chainskirt
			Item(11826, 1), //Armadyl helmet
			Item(11834, 1), //Bandos tassets
			Item(11832, 1), //Bandos chestplate
			Item(11808, 1), //Zamorak godsword
			Item(11806, 1), //Saradomin godsword
			Item(11804, 1), //Bandos godsword
			Item(6737, 1), //Berserker ring
			Item(6735, 1), //Warriors ring
			Item(6733, 1), //Archers ring
			Item(6731, 1), //Seers ring
			Item(11773, 1), //Berserker ring (i)
			Item(11772, 1), //Warriors ring (i)
			Item(11771, 1), //Archers ring (i)
			Item(11770, 1), //Seers ring (i))
			Item(20517, 1), //Elder chaos top
			Item(20520, 1), //Elder chaos bottom
			Item(20595, 1))   //Elder chaos hood
	
	val HIGH_LOW = arrayOf(Item(13307, 30000), //30k Blood money
			Item(13307, 90000), //90k Blood money
			Item(13307, 45000), //45k Blood money
			Item(12696, 1000), //1000 Super combat potions
			Item(13442, 1000), //1000 Anglerfish
			Item(12696, 1500), //1500 Super combat potions
			Item(13442, 1500), //1500 Anglerfish
			Item(11806, 1), //Armadyl godsword
			Item(12825, 1), //Arcane spirit shield
			Item(12821, 1), //Spectral spirit shield
			Item(13271, 1), //Abyssal dagger
			Item(12791, 1), //Rune pouch
			Item(11785, 1), //Armadyl crossbow
			Item(13576, 1))   //Dragon warhammer
	
	val HIGH = arrayOf(Item(13307, 75000)) // Dragon claws
	
	val RARE = arrayOf(Item(13307, 150000), //150k Blood money
			Item(13307, 200000), //200k Blood money
			Item(12696, 5000), //5000 Super combat potions
			Item(13442, 5000), //5000 Anglerfish
			Item(12696, 10000), //10000 Super combat potions
			Item(13442, 10000), //10000 Anglerfish
			Item(1053, 1), //Green halloween mask
			Item(1055, 1), //Blue halloween mask
			Item(1057, 1), //Red halloween mask
			Item(1050, 1))   //Santa hat
	
	val EXTREME = arrayOf(Item(12817), // Elysian spirit shield
			Item(1050), // Santa hat
			Item(12422), // 3rd age wand
			Item(12424), // 3rd age bow
			Item(12426), // 3rd age sword
			Item(12437), // 3rd age cloak
			Item(10330), // 3rd age range top
			Item(10332), // 3rd age range legs
			Item(10334), // 3rd age range coif
			Item(10336), // 3rd age range vanbraces
			Item(10338), // 3rd age robe top
			Item(10340), // 3rd age robe
			Item(10342), // 3rd age mage hat
			Item(10344), // 3rd age amulet
			Item(10346), // 3rd age platelegs
			Item(10348), // 3rd age platebody
			Item(10350), // 3rd age fullhelm
			Item(10352), // 3rd age kiteshield
			Item(20014), // 3rd age pickaxe
			Item(20011), //3rd age axe
			Item(13652), // Dragon claws
			Item(13442, 10000), //10000 Anglerfish
			Item(1038, 1), //Red party hat
			Item(1040, 1), //Yellow party hat
			Item(1042, 1), //Blue party hat
			Item(1044, 1), //Green party hat
			Item(1046, 1), //Purple party hat
			Item(1048, 1))   //White  party hat
	
	/**
	 * Handle when we click the item
	 */
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		if (r.server().world().realm().isPVP) {
			r.onItemOption1(MYSTERY_BOX) @Suspendable {
				val items = randomTable { x -> it.player().world().rollDie(x, 1) }
				
				val item = items.random()
				if (items === EXTREME || items === RARE || items === HIGH || items === HIGH_LOW) {
					it.player().world().broadcast("<col=1e44b3><img=22> " + it.player().name() + " got a " + item.unnote(it.player().world()).definition(it.player().world()).name + " from a Mystery Box!")
				}
				
				it.player().message("You open the Mystery Box and find... " + item.unnote(it.player().world()).definition(it.player().world()).name + " x " + item.amount() + "!")
				it.player().inventory().remove(Item(MYSTERY_BOX), true, it.player().attribOr(AttributeKey.ITEM_SLOT, 0))
				it.player().inventory().add(item, true)
				if (item.id() == ZulrahItems.TOXIC_SOTD_USED) {
					// World 2 reward only, only world that drops toxic item as an item instead of parts.
					item.property(ItemAttrib.ZULRAH_SCALES, 11000)
				}
				if (item.id() == ZulrahItems.SERPENTINE_HELM) {
					// World 2 reward only, only world that drops toxic item as an item instead of parts.
					item.property(ItemAttrib.ZULRAH_SCALES, 16000)
				}
			}
			
		}
	}
	
	fun randomTable(dieFunc: Function1<Int, Boolean>): Array<Item> {
		val items: Array<Item>
		
		if (dieFunc(350)) {
			items = EXTREME
		} else if (dieFunc(150)) {
			items = HIGH
		} else if (dieFunc(50)) {
			items = HIGH_LOW
		} else if (dieFunc(20)) {
			items = MEDIUM_HIGH
		} else if (dieFunc(4)) {
			items = MEDIUM
		} else {
			items = MEDIUM_LOW
		}
		
		return items
	}
	
	@JvmStatic fun main(args: Array<String>) {
		val rolls = 28
		val results = mutableMapOf<Int, Int>()
		val random = Random()
		
		for (i in 1..rolls) {
			val item = randomTable { random.nextInt(it + 1) < 1 } .random()
			
			results.compute(item.id(), { k, v -> if (v == null) item.amount() else (v + item.amount()) })
		}
		
		val store = DataStore("data/filestore")
		val repo = DefinitionRepository(store, true)
		
		results.forEach { k, v ->
			val def = repo.get(ItemDefinition::class.java, Item(k).unnote(repo).id())
			println(def.name + ": " + v)
		}
	}
	
}
