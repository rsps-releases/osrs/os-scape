package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.buttonAction
import nl.bartpelle.veteres.content.mechanics.VarbitAttributes
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Jak on 24/08/2016.
 */
object GameMsgFilter {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		repo.onButton(162, 7) @Suspendable {
			val button = it.buttonAction()
			if (button == 2) { // Filter on/off
				it.player().varps().varbit(Varbit.GAME_FILTER, if (it.player().varps().varbit(Varbit.GAME_FILTER) == 1) 0 else 1)
			} else if (button == 3) { // Customize filter
				val one = "Boss killcount messages: " + (if (!VarbitAttributes.varbiton(it.player(), 12)) "<col=04B431>on" else "<col=FF0000>off") + "."
				val two = "PK killstreak broadcasts: " + (if (!VarbitAttributes.varbiton(it.player(), 13)) "<col=04B431>on" else "<col=FF0000>off") + "."
				val three = "Yell channel: " + (if (it.player().attribOr<Boolean>(AttributeKey.YELL_CHANNEL_DISABLED, false)) "<col=FF0000>off" else "<col=04B431>on") + "."
				
				if (!it.player().world().realm().isPVP) {
					val three = "Levelup & rare drop broadcasts (coming soon): " + (if (!VarbitAttributes.varbiton(it.player(), 14)) "<col=04B431>on" else "<col=FF0000>off") + "."
					when (it.optionsTitled("Modify filter settings.", one, two, three)) {
						1 -> {
							VarbitAttributes.toggle(it.player(), 12)
							it.player().varps().varbit(Varbit.BOSS_KC_NOTICATION, if (VarbitAttributes.varbiton(it.player(), 12)) 1 else 0)
							it.message("Boss kc now: ${if (VarbitAttributes.varbiton(it.player(), 12)) "filtered (hidden)" else "unfiltered (visible)"}")
						}
						2 -> VarbitAttributes.toggle(it.player(), 13)
						3 -> VarbitAttributes.toggle(it.player(), 14)
					}
				} else {
					when (it.optionsTitled("Modify filter settings.", one, two, three)) {
						1 -> {
							VarbitAttributes.toggle(it.player(), 12)
							it.player().varps().varbit(Varbit.BOSS_KC_NOTICATION, if (VarbitAttributes.varbiton(it.player(), 12)) 1 else 0)
							it.message("Boss kc now: ${if (VarbitAttributes.varbiton(it.player(), 12)) "filtered (hidden)" else "unfiltered (visible)"}")
						}
						2 -> VarbitAttributes.toggle(it.player(), 13)
						
						3 -> {
							val status = !it.player().attribOr<Boolean>(AttributeKey.YELL_CHANNEL_DISABLED, false)
							it.player().putattrib(AttributeKey.YELL_CHANNEL_DISABLED, status)
							it.player().message("Yell channel is now "+ (if (!status) "<col=04B431>enabled." else "<col=FF0000>disabled."))
						}
					}
				}
			}
			
		}
	}
}