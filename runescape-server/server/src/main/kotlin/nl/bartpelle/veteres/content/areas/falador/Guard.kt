package nl.bartpelle.veteres.content.areas.falador

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/6/2016.
 */

object Guard {
	
	val GUARD = 6561
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(GUARD) @Suspendable {
			it.chatNpc("Halt! Are you here to sabotage the Motherlode Mine?", GUARD, 614)
			when (it.options("What's the Motherlode mine?", "No, I'm not,", "What would you do if I said 'yes'?")) {
				1 -> whats_the_mine(it)
				2 -> no_im_not(it)
				3 -> what_would_you_do(it)
			}
		}
	}
	
	@Suspendable fun whats_the_mine(it: Script) {
		it.chatPlayer("What's the Motherlode Mine?", 554)
		it.chatNpc("Prospector Percy discovered unusual mineral veins in<br>this cave. Now he's built a machine to let us extract<br>useful ores.", GUARD, 590)
		it.chatNpc("Percy calls it the Motherlode. I don't know why, and<br>I'm not asking - that's between him and his mother.", GUARD, 576)
		it.chatNpc("Dwarves don't normally let humans claim areas of the<br>mine like this, but Percy's machine is really useful, so<br>we struck a deal.", GUARD, 594)
	}
	
	@Suspendable fun no_im_not(it: Script) {
		it.chatPlayer("No, I'm not.", 614)
		it.chatNpc("That's a relief. Have a nice day.", GUARD, 567)
	}
	
	@Suspendable fun what_would_you_do(it: Script) {
		it.chatPlayer("What would you do if I said 'yes'?", 592)
		it.chatNpc("I'd ask you not to.", GUARD, 575)
	}
}
