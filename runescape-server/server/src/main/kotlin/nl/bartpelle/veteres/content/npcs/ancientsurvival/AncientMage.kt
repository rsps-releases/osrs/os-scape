package nl.bartpelle.veteres.content.npcs.ancientsurvival

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Graphic
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.map.steroids.Direction
import nl.bartpelle.veteres.util.AccuracyFormula
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 2016-09-07.
 */


object AncientMage {
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10) && EntityCombat.attackTimerReady(npc)) {
				
				if (EntityCombat.attackTimerReady(npc)) {
					val randomAttack = npc.world().random(8)
					
					
					when (randomAttack) {
						1, 2 -> Barrage(npc, target)
						3, 4 -> Blitz(npc, target)
						5, 6 -> Burst(npc, target)
						7, 8 -> Rush(npc, target)
						else -> Teleport(npc, target)
					}
					
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	/**
	 * Handles the Ancient Mage's teleport spell
	 */
	@Suspendable fun Teleport(npc: Npc, target: Entity) {
		npc.executeScript @Suspendable { s ->
			val direction: Direction = Direction.of(target.tile().x + npc.tile().x,
					target.tile().z + npc.tile().z)
			val tile: Tile = target.tile().transform(direction.x * +3, direction.y * +3)
			
			val secondDirection: Direction = Direction.of(target.tile().x - npc.tile().x,
					target.tile().z - npc.tile().z)
			val secondTile: Tile = target.tile().transform(direction.x * -3, direction.y * -3)
			
			npc.lock()
			npc.sync().shout("Seventhior Distine Molenko!")
			target.message("The Ancient Wizard teleports away.")
			s.delay(1)
			npc.animate(714)
			npc.graphic(111, 92, 0)
			s.delay(3)
			npc.animate(-1)
			if (npc.world().rollDie(2, 1))
				npc.teleport(tile)
			else
				npc.teleport(secondTile)
			npc.graphic(409, 0, 4)
			npc.unlock()
			s.delay(1)
			npc.attack(target)
		}
	}
	
	/**
	 * Handles the Ancient Mage's barrage attack style
	 */
	@Suspendable fun Barrage(npc: Npc, target: Entity) {
		val attackType = npc.world().random(3)
		val attackGraphic = arrayListOf(391, 383, 377, 369)
		val attackMaxHit = arrayListOf(27, 28, 29, 30)
		val tileDist = npc.tile().distance(target.tile())
		val delay = Math.max(1, (60 + (tileDist * 12)) / 30)
		
		npc.animate(1979)
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0)) {
			val randomHit = npc.world().random(attackMaxHit[attackType])
			target.hit(npc, randomHit, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(attackGraphic[attackType]))
			SpellEffect(npc, target, delay, attackType, "Barrage", randomHit)
		} else {
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(85, 124, 0))
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
	}
	
	/**
	 * Handles the Ancient Mage's blitz attack style
	 */
	@Suspendable fun Blitz(npc: Npc, target: Entity) {
		val attackType = npc.world().random(3)
		val npcGraphic = arrayListOf(-1, -1, -1, 366)
		val attackGraphic = arrayListOf(387, 381, 375, 367)
		val attackProjectile = arrayListOf(384, 380, 374, -1)
		val attackMaxHit = arrayListOf(22, 23, 24, 25)
		val tileDist = npc.tile().distance(target.tile())
		val delay = Math.max(1, (60 + (tileDist * 12)) / 30)
		
		npc.animate(1978)
		if (npcGraphic[attackType] != -1)
			npc.graphic(npcGraphic[attackType])
		npc.world().spawnProjectile(npc.tile(), target, attackProjectile[attackType], 43, 31, 51, 12 * tileDist, 16, 64)
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0)) {
			val randomHit = npc.world().random(attackMaxHit[attackType])
			target.hit(npc, randomHit, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(attackGraphic[attackType]))
			SpellEffect(npc, target, delay, attackType, "Blitz", randomHit)
		} else {
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(85, 124, 0))
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
	}
	
	/**
	 * Handles the Ancient Mage's burst attack style
	 */
	@Suspendable fun Burst(npc: Npc, target: Entity) {
		val attackType = npc.world().random(3)
		val attackGraphic = arrayListOf(381, 382, 376, 363)
		val attackMaxHit = arrayListOf(18, 19, 20, 21)
		val tileDist = npc.tile().distance(target.tile())
		val delay = Math.max(1, (60 + (tileDist * 12)) / 30)
		
		npc.animate(1979)
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0)) {
			val randomHit = npc.world().random(attackMaxHit[attackType])
			target.hit(npc, randomHit, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(attackGraphic[attackType]))
			SpellEffect(npc, target, delay, attackType, "Burst", randomHit)
		} else {
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(85, 124, 0))
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
	}
	
	/**
	 * Handles the Ancient Mage's rush attack style
	 */
	@Suspendable fun Rush(npc: Npc, target: Entity) {
		val attackType = npc.world().random(3)
		val attackGraphic = arrayListOf(385, 379, 373, 361)
		val attackProjectile = arrayListOf(384, 378, 372, 360)
		val attackMaxHit = arrayListOf(14, 15, 16, 17)
		val tileDist = npc.tile().distance(target.tile())
		val delay = Math.max(1, (60 + (tileDist * 12)) / 30)
		
		npc.animate(1978)
		npc.world().spawnProjectile(npc.tile(), target, attackProjectile[attackType], 43, 31, 51, 12 * tileDist, 16, 64)
		
		if (AccuracyFormula.doesHit(npc, target, CombatStyle.MAGIC, 1.0)) {
			val randomHit = npc.world().random(attackMaxHit[attackType])
			target.hit(npc, randomHit, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(attackGraphic[attackType]))
			SpellEffect(npc, target, delay, attackType, "Rush", randomHit)
		} else {
			target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.MAGIC).graphic(Graphic(85, 124, 0))
		}
		
		target.putattrib(AttributeKey.LAST_DAMAGER, npc)
		target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
		npc.putattrib(AttributeKey.LAST_TARGET, target)
	}
	
	/**
	 * Handles all the spell effects -> Smoke, Shadow, Blood, and Ice spells.
	 */
	@Suspendable fun SpellEffect(npc: Npc, target: Entity, delay: Int, attackType: Int, attackName: String, randomHit: Int) {
		npc.executeScript @Suspendable { s ->
			s.delay(delay)
			when (attackType) {
				0 -> { // Smoke - poisons the tartget starting at 2, 2, 4, 4 damage
					if (npc.world().rollDie(2, 1)) {
						val weak = attackName.equals("Rush") || attackName.equals("Burst")
						if (target.poison(if (weak) 2 else 4, false))
							target.message("The Ancient Wizard poisions you.")
					}
				}
				1 -> { // Shadow - targets attack lowered by 10%, 10%, 15%, 15%
					val attackLevel = target.skills().xpLevel(Skills.ATTACK)
					val currentAttackLevel = target.skills().level(Skills.ATTACK)
					val attackReductionAmount = if (attackName.equals("Rush") || attackName.equals("Burst")) .10 else .15
					val newAttackLevel = (attackLevel - (attackLevel * attackReductionAmount))
					
					if (newAttackLevel < currentAttackLevel && npc.world().rollDie(2, 1)) {
						target.skills().setLevel(Skills.ATTACK, newAttackLevel.toInt())
						target.message("The Ancient Wizard weakens your attack level.")
					}
				}
				2 -> { //Blood - caster is healed for 25% of total damage dealt to enemies
					if (npc.world().rollDie(2, 1)) {
						target.heal(randomHit / 4) // Heal for 25% with blood barrage
						target.message("The Ancient Wizard steals some of your hitpoints.")
					}
				}
				3 -> { //Ice - binds the target for 5, 10, 15, 20 seconds
					val timeFrozen = arrayListOf(5, 10, 15, 20)
					target.freeze(timeFrozen[attackType], npc)
				}
			}
		}
	}
}