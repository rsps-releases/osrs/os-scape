package nl.bartpelle.veteres.content.areas.instances

import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.events.tournament.EdgevilleTourny
import nl.bartpelle.veteres.content.mechanics.cutscene.Cutscene
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.instance.InstancedMap
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.LocationUtilities

/**
 * @author Simplemind
 */
object MageBankInstance {

    @JvmStatic var mageBank: InstancedMap? = null
    @JvmStatic var mageBankSafe: InstancedMap? = null
    @JvmStatic var mageBankDungeon: InstancedMap? = null
    @JvmStatic val mageBankWildTileCorner = Tile(3008, 3904)

    private const val SHIP_LADDER_UP = 272
    private const val SHIP_LADDER_DOWN = 273
    private val mageBankSafeTile = Tile(2539, 4716)
    private val mageBankSafeMapCorner = Tile(2496, 4672)

    @JvmStatic @ScriptMain
    fun register(sr: ScriptRepository) {
        sr.onWorldInit {
            setupMageBankEnvironment(it)
        }
        sr.onObject(SHIP_LADDER_UP, {
            it.player().animate(828)
            it.player().teleport(it.player().tile().transform(0, 0, 1))
            it.player().message("You climb up the ladder.")
        })
        sr.onObject(SHIP_LADDER_DOWN, {
            it.player().animate(827)
            it.player().teleport(it.player().tile().transform(0, 0, -1))
            it.player().message("You climb down the ladder.")
        })
    }

    @JvmStatic
    fun attackableAreas(player: Entity): Boolean {
        mageBank ?: return false
        mageBankDungeon ?: return false

        return mageBank!!.contains(player) || mageBankDungeon!!.contains(player)
    }

    @JvmStatic
    fun teleportSafeTile(): Tile {
        return LocationUtilities.dynamicTileFor(mageBankSafeTile, mageBankSafe)
    }

    @JvmStatic
    fun inDungeon(entity: Entity): Boolean {
        return mageBankDungeon?.contains(entity) ?: return false
    }

    operator fun contains(entity: Entity): Boolean {
        return (mageBank?.contains(entity) ?: return false || mageBankDungeon?.contains(entity) ?: return false)
    }

    private fun setupMageBankEnvironment(it: Script) {
        val world = it.ctx<World>()

        setupMageBankSafe(world)
        setupMageBank(world)
        setupMageBankDungeon(world)
    }

    private fun setupMageBankDungeon(world: World) {
        val map = world.allocator().allocate(1, Tile(3088, 3504)).get()
        val startTile = Tile(3008, 10304)

        map.set(0, 0, startTile, startTile.transform(64, 64), 0, true)
        map.setDangerous(true)
        map.isMulti = false
        map.setPersists(true)

        mageBankDungeon = map

        val npcs: ArrayList<Npc> = ArrayList()
        npcs.addAll(LocationUtilities.copyNpcSpawnsFrom(world, Tile(3043, 10320), 64))

        npcs.stream().forEach({original ->
            val cloned = Npc(original.id(), world, LocationUtilities.dynamicTileFor(original.spawnTile(), mageBankDungeon))

            cloned.walkRadius(original.walkRadius())
            world.registerNpc(cloned)
        })
    }

    private fun setupMageBankSafe(world: World) {
        val map = world.allocator().allocate(1, Tile(3088, 3504)).get()

        map.set(0, 0, mageBankSafeMapCorner, mageBankSafeMapCorner.transform(64, 64), 0, true)
        map.setPersists(true)

        mageBankSafe = map

        val banker = Npc(1600, world, LocationUtilities.dynamicTileFor(Tile(2533, 4717), mageBankSafe))
        banker.spawnDirection(Cutscene.Direction.NORTH.ordinal)
        world.registerNpc(banker)

        EdgevilleTourny.ENTER[4] = teleportSafeTile()
    }

    private fun setupMageBank(world: World) {
        val map = world.allocator().allocate(3, Tile(3088, 3504)).get()

        map.set(0, 0, mageBankWildTileCorner, mageBankWildTileCorner.transform(192, 192), 0, true)
        map.setDangerous(true)
        map.isMulti = false
        map.setPersists(true)

        mageBank = map

        val npcs: ArrayList<Npc> = ArrayList()

        npcs.addAll(LocationUtilities.copyNpcSpawnsFrom(world, Tile(3078, 3955), 12))
        npcs.addAll(LocationUtilities.copyNpcSpawnsFrom(world, Tile(3191, 3960), 6))
        npcs.addAll(LocationUtilities.copyNpcSpawnsFrom(world, Tile(3041, 3956), 6))
        npcs.addAll(LocationUtilities.copyNpcSpawnsFrom(world, Tile(3186, 3918), 12))
        npcs.addAll(LocationUtilities.copyNpcSpawnsFrom(world, Tile(3175, 3953), 8))

        for (original in npcs) {
            val spawnTile = LocationUtilities.dynamicTileFor(original.spawnTile(), mageBankWildTileCorner, mageBank)
            val cloned = Npc(original.id(), world, spawnTile)
            cloned.walkRadius(original.walkRadius())
            world.registerNpc(cloned)
        }

        EdgevilleTourny.DEATH[4] = teleportSafeTile()
        EdgevilleTourny.EVENT_INSTANCES[4] = mageBank
    }
}
