package nl.bartpelle.veteres.content.mechanics

import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player

/**
 * @author Heaven
 */
object EloRating {

    const val DEFAULT_ELO_RATING = 1300
    private const val MAX_ELO_RATING = 4000

    @JvmStatic fun modify(player: Player, target: Player) {
        var currentEloRating = player.attribOr<Int>(AttributeKey.ELO_RATING, DEFAULT_ELO_RATING)
        var targetEloRating = target.attribOr<Int>(AttributeKey.ELO_RATING, DEFAULT_ELO_RATING)
        val gain = if (target.skills().combatLevel() < player.skills().combatLevel() || targetEloRating < currentEloRating) {
            player.world().random(IntRange(10, 15))
        } else  {
            player.world().random(IntRange(20, 25))
        }

        if (currentEloRating + gain > MAX_ELO_RATING) {
            currentEloRating = MAX_ELO_RATING
        } else {
            currentEloRating += gain
        }

        if (targetEloRating - gain < 0) {
            targetEloRating = 0
        } else {
            targetEloRating -= gain
        }

        player.putattrib(AttributeKey.ELO_RATING, currentEloRating)
        target.putattrib(AttributeKey.ELO_RATING, targetEloRating)
        player.message("<col=FF0000>You have gained +$gain to your elo rating.")
        target.message("<col=FF0000>You have lost -$gain to your elo rating.")
    }
}