package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.items.LootingBag
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.AddReceivedPrivateMessage
import nl.bartpelle.veteres.net.message.game.command.InterfaceSettings
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.InvokeScript
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 8/23/2015.
 */

object Bank {
	
	@JvmStatic
	@JvmOverloads
	fun deposit(num: Int, player: Player, slot: Int?, item: Item?, from: ItemContainer = player.inventory(), targetTab: Int? = null, fromTab: Int? = null) {
		if (item != null) {
			var from = from
			
			val inInv = from.count(item.id())
			
			val toadd = if (item.definition(player.world()).noteModel == 0)
				Item(item, Math.min(inInv, num))
			else
				Item(item.definition(player.world()).notelink, Math.min(inInv, num)) // Params do not exist on paramable items
			
			// Break open tabs
			val insertInto: ItemContainer
			val openTabId = targetTab ?: player.varps().varbit(Varbit.BANK_SELECTED_TAB)
			var tabs: Array<ItemContainer?> = arrayOf()
			
			val forceTabs = targetTab != null && fromTab != null
			
			if (!forceTabs && (player.bank().contains(toadd.id()) || openTabId == 0 || player.bank().contains(toadd.definition(player.world()).placeheld))) {
				insertInto = player.bank()
			} else {
				tabs = buildTabs(player)
				val targetTab = Math.max(0, Math.min(10, openTabId))
				insertInto = tabs[targetTab]!!
			}
			
			var specifySlot = slot != null
			if (tabs.isNotEmpty() && from == player.bank() && fromTab != null) {
				from = tabs[fromTab]!!
				specifySlot = false
			}
			val stacks = item.definition(player.world()).stackable()
			
			if (player.bank().full() && ((!stacks && insertInto != player.bank()) || (stacks && !insertInto.contains(item.id())))) {
				player.message("Bank full. You've used all ${player.bank().size()} slots.")
				return
			}
			var failed: Boolean = false
			
			if (stacks || num == 1) {
				// Stackable is the easiest. Remove & add.
				val result = insertInto.add(toadd, false).completed()
				
				if (specifySlot && ((from[slot!!]?.id() ?: -1) != item.id() || (from[slot]?.amount() ?: 0) < result)) {
					specifySlot = false
				}
				
				if (result > 0) {
					if (specifySlot) {
						if (!from.remove(Item(item.id(), result), true, slot!!).success()) {
							warn(player)
						}
					} else {
						if (!from.remove(Item(item.id(), result), true).success()) {
							warn(player)
						}
					}
				} else {
					player.message("Bank full. You can't deposit any more of this item.")
					failed = true
				}
			} else {
				// Non-stackable works different due to item attribs. Must be iterated.
				var remaining = num
				for (i in 0..from.size() - 1) {
					if (remaining < 1)
						break
					
					val itemAt = from[i] ?: continue // Get item or continue
					
					// Is this a candidate?
					if (itemAt.id() == item.id()) {
						val result = insertInto.add(itemAt, false).completed()
						if (specifySlot && ((from[i]?.id() ?: -1) != item.id() || (from[i]?.amount() ?: 0) < result)) {
							specifySlot = false
						}
						if (result > 0) {
							if (specifySlot) {
								if (!from.remove(Item(item.id(), result), true, i).success()) {
									warn(player)
								}
							} else {
								if (!from.remove(Item(item.id(), result), true).success()) {
									warn(player)
								}
							}
							remaining--
						} else {
							player.message("Bank full. No more of this item can be deposited.")
							failed = true
						}
					}
				}
			}
			
			// Unify tabs again
			if (!failed && insertInto != player.bank()) {
				meltTabs(player, tabs)
			}
		}
	}
	
	private fun warn(player: Player) {
		player.world().players().forEachKt({ p ->
			if (p != null && p.privilege().eligibleTo(Privilege.MODERATOR)) {
				p.write(AddReceivedPrivateMessage("DupeWatch", 2, 0, "WARNING: apparent user duping: " + player.name() + "; see chatbox!"))
				p.message("<col=ff0000>[DUPEWATCH] Bank warning, ${player.name()} added items to their bank without losing any items!")
			}
		})
		
	}
	
	@JvmStatic fun depositInventory(player: Player) {
		for (i in 0..27) {
			val itemAt = player.inventory()[i] ?: continue // Get item or continue
			deposit(itemAt.amount(), player, i, itemAt)
		}
	}
	
	@JvmStatic fun depositEquipment(player: Player) {
		for (i in 0..13) {
			val itemAt = player.equipment()[i] ?: continue // Get item or continue
			deposit(itemAt.amount(), player, i, itemAt, player.equipment())
		}
	}
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		// Item depositing
		repo.onButton(15, 3, s@ @Suspendable {
			val player = it.player()
			val slot: Int = player.attrib(AttributeKey.BUTTON_SLOT)
			val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
			val item = player.inventory()[slot] ?: return@s
			
			//Examining
			if (option == 10) {
				if (item.amount() < 100000)
					player.message(player.world().examineRepository().item(item.id()))
				else
					player.message("%d x %s.", item.amount(), item.definition(player.world()).name)
				return@s
			}
			
			val num = when (option) {
				1 -> {
					if (item.id() == 11941) {
						it.player().interfaces().setting(81, 7, 0, 27, 1024)
					}
					return@s
				}
				2 -> 1
				3 -> 5
				4 -> 10
				5 -> Math.max(1, player.varps().varbit(Varbit.LAST_BANK_X))
				6 -> {
					val num = it.inputInteger("Enter amount:");
					player.varps().varbit(Varbit.LAST_BANK_X, num);
					num
				}
				7 -> player.inventory().count(item.id()) // All items
				else -> 1
			}
			
			deposit(num, player, slot, item)
		})
		
		// Deposit inventory - bank inventory bank all
		repo.onButton(12, 41) {
			depositInventory(it.player())
		}
		
		// Deposit equipment bank - bank equipment bank all
		repo.onButton(12, 43) {
			depositEquipment(it.player())
			SpellSelect.reset(it.player(), true, false)
		}
		
		//Incinerator confirm
		repo.onButton(12, 46) {
			if (it.buttonAction() == 1) {
				val slot: Int = it.player().attrib(AttributeKey.BUTTON_SLOT)
				if (it.player().bank().get(slot) != null) {
					val item = it.player().bank().get(slot - 1)
					it.player().bank().remove(item, true)
					it.player().message("You have successfully incinerated <col=FF0000>x${item.amount()} ${item.name(it.player().world())}</col>.")
				}
			}
		}
		
		// Withdrawing
		repo.onButton(12, 23, s@ @Suspendable {
			val player = it.player()
			val slot: Int = player.attrib(AttributeKey.BUTTON_SLOT)
			val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
			val clickedId: Int = player.attrib(AttributeKey.ITEM_ID)
			val item = if (slot >= player.bank().items().size) return@s else player.bank()[slot] ?: return@s
			val noted = player.varps().varbit(Varbit.BANK_NOTE) == 1
			var placehold = false
			
			if (item != null && item.id() == clickedId) {
				//Examining
				if (option == 10) {
					if (item.amount() < 100000)
						player.message(player.world().examineRepository().item(item.id()))
					else
						player.message("%d x %s.", item.amount(), item.definition(player.world()).name)
					return@s
				}
				
				val num = when (option) {
					1 -> 1
					2 -> 5
					3 -> 10
					4 -> Math.max(1, it.player().varps().varbit(Varbit.LAST_BANK_X))
					5 -> {
						val num = it.inputInteger("Enter amount:"); it.player().varps().varbit(Varbit.LAST_BANK_X, num); num
					}
					6 -> player.bank().countSlot(slot, item.id()) // All items
					7 -> player.bank().countSlot(slot, item.id()) - 1 // All items except one
					8 -> {
						placehold = true
						player.bank().countSlot(slot, item.id())
					} // All items (placeholder)
					else -> 1
				}
				
				// If we're withdrawing a placeholder item, clear it.
				if (Item(item).definition(player.world()).pheld14401 > 0) {
					player.bank().remove(Item(item), true, slot)
					return@s
				}
				
				if (num != 0) {
					// Check if we would withdraw at least 1
					val numinbank = player.bank().countSlot(slot, item.id())
					val numtake = Math.min(num, numinbank)
					
					// Withdrawing one of these is dangerous..
					if (numtake < 1 || Item(item).definition(player.world()).pheld14401 > 0) {
						return@s
					}
					
					var itemToAdd = Item(item, numtake)
					if (noted)
						itemToAdd = itemToAdd.note(player.world())
					
					val firstresult = player.bank().findFirst(item.id())
					val result = player.inventory().add(itemToAdd, true)
					player.bank().remove(Item(item, result.completed()), true, slot)
					
					// Placehold this?
					if ((placehold || player.varps().varbit(Varbit.BANK_PLACEHOLDERS) == 1) && player.bank().get(firstresult.first()) == null) {
						// Does that item have a placeholder?
						val placeholderTemplate = Item(item).definition(player.world()).pheld14401
						val placeholderId = Item(item).definition(player.world()).placeheld
						
						if (placeholderTemplate == -1 && placeholderId > 0 && !item.hasProperties()) {
							player.bank().set(firstresult.first(), Item(placeholderId))
						}
					}
					
					// Extra coolness for notes
					if (noted && item.id() == itemToAdd.id()) {
						player.message("This item cannot be withdrawn as a note.")
					}
					if (item.id() == 11283 && player.varps().varbit(Varbit.BANK_PLACEHOLDERS) == 1 && player.bank().get(firstresult.first()) == null) {
						// People wonder why it doesn't have a placeholder. Tell em'
						if (!player.world().realm().isPVP)
							player.message("This dragonfire shield is technically charged with dragonfire, and therefore has no placeholder.")
					}
					
					// Show a message if we couldn't withdraw all
					if (result.requested() != result.completed()) {
						if (result.completed() == 0) {
							player.message("You don't have enough inventory space.")
						} else {
							player.message("You don't have enough inventory space to withdraw that many.")
						}
					}
				}
				meltTabs(player, buildTabs(player))
			}
		})
		
		// Deposit equipment deposit box
		repo.onButton(192, 5) {
			if (it.player.ironMode() == IronMode.ULTIMATE) {
				it.messagebox("As an Ultimate Iron Man, you cannot use the bank.")
			} else {
				depositEquipment(it.player())
			}
			
			SpellSelect.reset(it.player(), true, false)
		}
		
		// Deposit inventory deposit box
		repo.onButton(192, 3) {
			if (it.player.ironMode() == IronMode.ULTIMATE) {
				it.messagebox("As an Ultimate Iron Man, you cannot use the bank.")
			} else {
				depositInventory(it.player())
			}
		}
		
		// Deposit looting deposit box
		repo.onButton(192, 7) {
			if (it.player.ironMode() == IronMode.ULTIMATE) {
				it.messagebox("As an Ultimate Iron Man, you cannot use the bank.")
			} else {
				LootingBag.depositLootingBag(it.player())
			}
		}
		
		// Item depositing deposit box
		repo.onButton(192, 2, s@ @Suspendable {
			val player = it.player()
			val slot: Int = player.attrib(AttributeKey.BUTTON_SLOT)
			val option: Int = player.attrib(AttributeKey.BUTTON_ACTION)
			val item = if (slot > player.inventory().items().size) return@s else player.inventory()[slot] ?: return@s
			
			if (item != null) {
				//Examining
				if (option == 10) {
					player.message(player.world().examineRepository().item(item.id()))
					return@s
				}
				
				val num = when (option) {
					1 -> 1
					2 -> 5
					3 -> 10
					4 -> player.inventory().count(item.id()) // All items
					5 -> {
						val num = it.inputInteger("Enter amount:");
						it.player().varps().varbit(Varbit.LAST_BANK_X, num);
						num
					}
					else -> 1
				}
				
				deposit(num, it.player(), slot, item)
			}
		})
		
		// Closing
		repo.onInterfaceClose(12) {
			it.player().interfaces().closeById(15)
			it.player().invokeScript(101) // Close input chatbox. Param is 11 on RS, but this makes it close on oss... and osrsps
			
			meltTabs(it.player(), buildTabs(it.player()))
			it.player().interfaces().close(162, 550)
			
			// If we were viewing a dead tab, set it to 0
			val viewing = it.player().varps().varbit(Varbit.BANK_SELECTED_TAB)
			if (viewing > 0 && tabSize(it.player(), viewing - 1) < 1) {
				it.player().varps().varbit(Varbit.BANK_SELECTED_TAB, 0)
			}
			if (it.player().world().realm().isDeadman) {
				it.player().timers().extendOrRegister(TimerKey.DEADMAN_BANK_COOLDOWN, 5)
			}
			
			it.player().stopActions(false)
		}
		
		// Placeholders
		repo.onButton(12, 37) {
			it.player().varps().varbit(Varbit.BANK_PLACEHOLDERS, it.player().varps().varbit(Varbit.BANK_PLACEHOLDERS).inv())
		}
		// Withdraw as item
		repo.onButton(12, 32) {
			it.player().varps().varbit(Varbit.BANK_NOTE, 0)
		}
		// Withdraw as note
		repo.onButton(12, 34) {
			it.player().varps().varbit(Varbit.BANK_NOTE, 1)
		}
		
		// Bank swap
		repo.onButton(12, 27) {
			it.player().varps().varbit(Varbit.BANK_INSERT, 0)
		}
		
		// Insert
		repo.onButton(12, 29) {
			it.player().varps().varbit(Varbit.BANK_INSERT, 1)
		}
		
		// Enable incinerator
		repo.onButton(12, 50) {
			val selectedIncinerator = it.player().varps().varbit(Varbit.BANK_INCINERATOR)
			
			when (selectedIncinerator) {
				1 -> it.player().varps().varbit(Varbit.BANK_INCINERATOR, 0)
				else -> it.player().varps().varbit(Varbit.BANK_INCINERATOR, 1)
			}
		}
		
		// Deposit worn items button hidden toggle
		repo.onButton(12, 51) {
			it.player().varps().varbit(Varbit.BANK_DEPOSIT_WORN_ITEMS_BTN, if (it.player().varps().varbit(Varbit.BANK_DEPOSIT_WORN_ITEMS_BTN) == 0) 1 else 0)
		}
		
		// Collapsing tabs and switching
		repo.onButton(12, 21) {
			val option: Int = it.player().attrib(AttributeKey.BUTTON_ACTION)
			if (option == 6) {
				Bank.collapseTab(it.player(), it.buttonSlot() - 10)
			} else {
				if (tabSize(it.player(), it.buttonSlot() - 11) > 0 || it.buttonSlot() - 10 == 0) {
					it.player().varps().varbit(Varbit.BANK_SELECTED_TAB, it.buttonSlot() - 10)
				} else {
					it.player().message("To create a new tab, drag items from your bank onto this tab.")
				}
			}
		}
		
		// Remove all placeholders
		repo.onButton(12, 52) {
			val bank = it.player().bank()
			for (i in 0..799) {
				val item = bank[i] ?: continue
				
				if (item.definition(it.player().world()).pheld14401 > 0) {
					bank[i] = null
				}
			}
		}
		
		// Tab display settings. Roman numeral, number, item.
		repo.onButton(12, 49) {
			val subbutton = it.buttonSlot()
			it.player().varps().varbit(4170, subbutton)
		}
		
		repo.onButton(15, 18) {
			it.player().varps().varbit(Varbit.BANK_YOUR_LOOT, 0)
		}
	}
	
	@JvmStatic @Suspendable fun open(p: Player, it: Script?) {
		if (p.ironMode() == IronMode.ULTIMATE) {
			if (it != null) {
				it.messagebox("As an Ultimate Iron Man, you cannot use the bank.")
			} else {
				p.message("As an Ultimate Iron Man, you cannot use the bank.")
			}
			return
		}
		p.write(InvokeScript(InvokeScript.SETVARCS, -1, -2147483648))

        if (p.interfaces().resizable()) p.invokeScript(917, -1, -2)

		p.interfaces().sendMain(12, false)
		p.interfaces().sendInventory(15)

		p.write(InterfaceSettings(12, 23, 0, 799, 1312254))
		p.write(InterfaceSettings(12, 23, 809, 817, 2))
		p.write(InterfaceSettings(12, 23, 818, 827, 1048576))
		p.write(InterfaceSettings(12, 21, 10, 10, 1048578))
		p.write(InterfaceSettings(12, 21, 11, 19, 1179714))
		p.write(InterfaceSettings(15, 3, 0, 27, 1181438))
		p.write(InterfaceSettings(15, 14, 0, 27, 1054))
		p.write(InterfaceSettings(12, 46, 1, 800, 2))
		p.write(InterfaceSettings(12, 49, 0, 3, 2))
		p.write(InterfaceText(12, 18, "800"))
		p.varps().varbit(Varbit.BANK_YOUR_LOOT, 0)
		
		p.bank().makeDirty()
		if (p.world().realm().isDeadman) {
			p.timers().extendOrRegister(TimerKey.DEADMAN_BANK_COOLDOWN, 5)
		}

        p.invokeScript(1495, "The total amount of items you have in this bank over the max capacity.", 786439, 786474)
		
		if (it != null) {
			it.onInterrupt { it.player().interfaces().closeById(12); it.player().interfaces().closeById(15); }
			it.waitForInterfaceClose(12)
		}
	}
	
	@JvmStatic fun buildTabs(p: Player): Array<ItemContainer?> {
		val array = arrayOfNulls<ItemContainer>(10)
		
		var pos = 0
		val raw = p.bank().items() // Faster access
		
		// First calc tabs
		for (i in 0..8) {
			val tabsize = p.varps().varbit(4171 + i)
			val crafted = arrayOfNulls<Item>(800)
			
			// Only makes sense to do this if there's items in the tab
			if (tabsize > 0) {
				System.arraycopy(raw, pos, crafted, 0, tabsize)
				pos += tabsize
			}
			
			// Put the tab in the list
			array[i + 1] = ItemContainer(p.world(), crafted, ItemContainer.Type.FULL_STACKING)
			//array[i + 1]!!.pack()
		}
		
		// Put the rest in the main array
		val main = arrayOfNulls<Item>(800)
		System.arraycopy(raw, pos, main, 0, 800 - pos)
		array[0] = ItemContainer(p.world(), main, ItemContainer.Type.FULL_STACKING)
		array[0]!!.pack()
		
		return array
	}
	
	@JvmStatic fun meltTabs(p: Player, tabs: Array<ItemContainer?>) {
		val crafted = arrayOfNulls<Item>(800)
		var pos = 0
		
		// Join the contents of all the tabs and send varbits appropriately
		var tabvar = 1
		for (i in 1..9) {
			if (tabs[i] != null) {
				tabs[i]!!.pack()
				val tabsize = tabs[i]!!.nextFreeSlot()
				if (tabsize > 0) {
					p.varps().varbit(4170 + tabvar++, tabsize)
					
					// Only put items in it if the tab's not empty
					System.arraycopy(tabs[i]!!.items(), 0, crafted, pos, tabsize)
					pos += tabsize
				}
			}
		}
		
		// Collapse other tabs
		if (tabvar < 10) {
			for (i in tabvar..9) {
				p.varps().varbit(4170 + i, 0)
			}
		}
		
		tabs[0]!!.pack() //Lets just pack everything until we can fix the logic behind nextFreeSlot() TODO
		
		// Finally, add up the 'remainder'
		System.arraycopy(tabs[0]!!.items(), 0, crafted, pos, tabs[0]!!.nextFreeSlot())
		p.bank().items(crafted)
	}
	
	@JvmStatic fun tabStart(p: Player, tab: Int): Int {
		var cur = 0
		for (i in 0..tab) {
			if (i == tab) {
				return cur
			}
			
			cur += p.varps().varbit(4171 + i)
		}
		
		return cur
	}
	
	@JvmStatic fun tabSize(p: Player, tab: Int): Int {
		return p.varps().varbit(4171 + tab)
	}
	
	@JvmStatic fun dragToSlot(p: Player, slot: Int, target: Int) {
		// Find our woeshoem or return
		val itemAt = p.bank()[slot] ?: return
		
		p.bank()[slot] = null
		
		val nano = System.nanoTime()
		val tabs = buildTabs(p)
		tabs[target - 10]!!.add(itemAt, true)
		meltTabs(p, tabs)
		val taken = System.nanoTime() - nano
	}
	
	fun collapseTab(p: Player, tab: Int) {
		val tabs = buildTabs(p)
		for (item in tabs[tab]!!.items()) {
			if (item != null) {
				tabs[0]!!.add(item, true)
			}
		}
		tabs[tab] = null
		meltTabs(p, tabs)
	}
	
	@JvmStatic fun findInTab(p: Player, item: Item): Int {
		val tabs = buildTabs(p)
		
		for (i in 0..tabs.size - 1) {
			if (tabs[i]?.contains(item) ?: false) {
				return i
			}
		}
		return -1
	}
}