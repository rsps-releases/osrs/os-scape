package nl.bartpelle.veteres.content.areas.rangingguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain


import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2017-03-11.
 */

object Guard {
	
	val GUARD = 6056
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(GUARD) @Suspendable {
			it.chatPlayer("Hello there.", 567)
			it.chatNpc("Greetings, traveller. Enjoy your time at the Ranging Guild.", GUARD, 589)
		}
	}
	
}
