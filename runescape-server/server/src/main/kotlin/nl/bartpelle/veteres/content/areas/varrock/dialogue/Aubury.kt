package nl.bartpelle.veteres.content.areas.varrock.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.mechanics.deadman.DeadmanMechanics
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 10/27/2015.
 */

object Aubury {
	
	val AUBURY = 637
	
	fun teleportToMines(player: Player) {
		if (player.world().realm().isPVP) {
			player.message("That's pretty pointless actually.")
		} else {
			player.teleport(2911, 4830, 0)
		}
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(AUBURY) @Suspendable {
			it.chatNpc("Do you want to buy some runes?", AUBURY, 567)
			when (it.options("Yes please!", "Oh, it's a rune shop. No thank you, then.", "Can you teleport me to the Rune Essence?")) {
				1 -> {
					if (it.player().ironMode() != IronMode.NONE) {
						it.player().world().shop(17).display(it.player())
					} else {
						it.player().world().shop(8).display(it.player())
					}
				}
				2 -> {
					it.chatPlayer("Oh, it's a rune shop. No thank you, then.")
					it.chatNpc("Well, if you find someone who does want runes, please<br>send them my way.", AUBURY, 568)
				}
				3 -> {
					it.chatPlayer("Can you teleport me to the Rune Essence?", 554)
					DeadmanMechanics.attemptTeleport(it)
					it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
					it.player().graphic(110, 124, 100)
					it.player().lockNoDamage()
					it.delay(3)
					teleportToMines(it.player())
					it.player().unlock()
				}
			}
		}
		
		r.onNpcOption2(AUBURY) @Suspendable {
			if (it.player().ironMode() != IronMode.NONE) {
				it.player().world().shop(17).display(it.player())
			} else {
				it.player().world().shop(8).display(it.player())
			}
		}
		
		r.onNpcOption3(AUBURY) @Suspendable {
			DeadmanMechanics.attemptTeleport(it)
			it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
			it.player().graphic(110, 124, 100)
			it.player().lockNoDamage()
			it.delay(3)
			teleportToMines(it.player())
			it.player().unlock()
		}
	}
}