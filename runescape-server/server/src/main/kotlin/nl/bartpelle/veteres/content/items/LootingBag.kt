package nl.bartpelle.veteres.content.items

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.*
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.interfaces.Bank
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceText
import nl.bartpelle.veteres.net.message.game.command.SetItems
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 9/12/2016.
 */

object LootingBag {
	
	val LOOTING_BAG = 11941
	val OPEN_LOOTING_BAG = 22586
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		for (id in arrayOf(LOOTING_BAG, OPEN_LOOTING_BAG)) {
			r.onItemOption1(id) {
				toggleLootingBag(it, id)
			}

			//Check the contents of the looting bag
			r.onItemOption2(id) @Suspendable {
				val itemPrices = getLootingBagPrices(it.player())

				it.player().interfaces().sendInventory(81)
				it.player().interfaces().setting(81, 5, 0, 27, 1024)
				it.player().invokeScript(495, "Looting bag", 0)
				it.player().write(SetItems(516, it.player().lootingBag()))
				it.player().write(InterfaceText(81, 6, "Value: - $itemPrices"))
				it.player().invokeScript(1235, 516, itemPrices)

				it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(81) }
				it.waitForInterfaceClose(81)
			}

			//Deposit some goodies into the looting bag
			r.onItemOption3(id) @Suspendable {
				val receivedWarning = it.player().attribOr<Int>(AttributeKey.RECEIVED_LOOTING_BAG_WARNING, 0) == 1

				if (receivedWarning) {
					val itemPrices = getInventoryItemPrices(it.player())

					it.player().interfaces().sendInventory(81)
					it.player().interfaces().setting(81, 5, 0, 27, 542)
					it.player().invokeScript(495, "Add to bag", 1)
					it.player().write(InterfaceText(81, 6, "Value: -"))
					it.player().invokeScript(1235, 93, *itemPrices)
				} else {
					it.messagebox("Items can be withdrawn from the bag <col=7f0000>only at a bank</col>.<br>The bag and its contents are <col=7f0000>always lost on death</col>.")

					if (it.optionsTitled("Do you still wish to store items in the bag?", "Yes, I understand how it works.", "No, cancel.") == 1) {
						it.player().putattrib(AttributeKey.RECEIVED_LOOTING_BAG_WARNING, 1)
						it.player().interfaces().sendInventory(81)
						it.player().interfaces().setting(81, 5, 0, 27, 542)
						it.player().invokeScript(495, 1, "Add to bag")
					}
				}

				it.onInterrupt { it.player().interfaces().closeMain(); it.player().interfaces().closeById(81) }
				it.waitForInterfaceClose(81)
			}

			// Avoid dropping the loot bag.
			r.onItemOption5(id) {
				it.message("Dropping the bag would damage it!")
			}

			// Item on looting bag!
			r.onItemOnItem(id, Int.MAX_VALUE, s@ @Suspendable {
				val item = (if (it.itemUsedId() == LOOTING_BAG || it.itemUsedId() == OPEN_LOOTING_BAG)
					it.player().inventory().get(it.itemOnSlot())
				else it.player.inventory()[it.itemUsedSlot()]) ?: return@s

				val opt = it.customOptions("How many do you want to deposit?", "One", "Five", "All", "X")

				val amt = when (opt) {
					1 -> 1
					2 -> 5
					3 -> Int.MAX_VALUE
					4 -> it.inputInteger("Enter amount:")
					else -> return@s
				}

				deposit(it, item, amt, null)
			})
		}
		
		//Handles adding the items into the bag
		r.onButton(81, 5) {
			val slotSelected: Int = it.player().attrib(AttributeKey.BUTTON_SLOT)
			val optionSelected: Int = it.player().attrib(AttributeKey.BUTTON_ACTION)
			val itemID: Int = it.player().attrib(AttributeKey.ITEM_ID)
			
			// Examining can happen without the item being there, for reasons.
			if (optionSelected == 10) {
				it.player.message(it.player.world().examineRepository().item(itemID))
				return@onButton
			}
			
			val item = it.player().inventory().get(slotSelected) ?: return@onButton
			
			// Client must be out of sync?
			if (itemID != item.id()) {
				return@onButton
			}
			
			val amt = when (optionSelected) {
				1 -> 1
				2 -> 5
				3 -> Int.MAX_VALUE
				4 -> it.inputInteger("Enter amount:")
				9 -> {
					it.player().message(it.player().world().examineRepository().item(item.id()))
					return@onButton
				}
				else -> return@onButton
			}
			
			deposit(it, item, amt, null)
		}
		
		// Deposit all our loot from the Looting Bag into the bank
		r.onButton(15, 5) {
			depositLootingBag(it.player())
		}
		
		// Deposit single
		r.onButton(15, 10) {
			if (it.buttonSlot() !in 0..27) return@onButton
			val item = it.player.lootingBag()[it.buttonSlot()]
			val amt = when (it.interactionOption()) {
				1 -> 1
				2 -> 5
				3 -> Int.MAX_VALUE
				4 -> it.inputInteger("Enter amount:")
				10 -> {
					it.player().message(it.player().world().examineRepository().item(item.id()))
					return@onButton
				}
				else -> return@onButton
			}
			
			Bank.deposit(amt, it.player, it.buttonSlot(), item, it.player.lootingBag())
		}
	}
	
	fun deposit(it: Script, item: Item, amount: Int, groundItem: GroundItem?): Boolean {
		// Is the player trying to store a looting bag inside a looting bag?
		if (item.id() == LOOTING_BAG || item.id() == OPEN_LOOTING_BAG) {
			it.player().message("You may be surprised to learn that bagception is not permitted.")
			return false
		}
		
		// Is the player inside the wilderness?
		if (!WildernessLevelIndicator.inWilderness(it.player().tile())) {
			it.player().message("You can't put items in the looting bag unless you're in the Wilderness.")
			return false
		}
		
		// Is the item trade-able?
		if (!item.rawtradable(it.player().world())) {
			it.player().message("Only tradeable items can be put in the bag.")
			return false
		}

		return depositItemsIntoBag(it, item.id(), amount, groundItem)
	}
	
	@JvmStatic fun depositLootingBag(player: Player) {
		for (i in 0..27) {
			val itemAt = player.lootingBag()[i] ?: continue // Get item or continue
			Bank.deposit(itemAt.amount(), player, i, itemAt, player.lootingBag())
		}
	}
	
	fun depositItemsIntoBag(it: Script, itemID: Int, amtToStore: Int, groundItem: GroundItem?): Boolean {
		var requestedAmount = amtToStore
		val maxAmount = groundItem?.item()?.amount() ?: it.player().inventory().count(itemID)
		
		//Does our player have enough of the item to store that they requested?
		if (maxAmount < amtToStore)
			requestedAmount = maxAmount
		
		val result = it.player().lootingBag().add(Item(itemID, requestedAmount), true)
		if (groundItem == null) {
            it.player().inventory().remove(Item(itemID, result.completed()), true)
        }
		
		if (result.completed() == 0) {
			it.player().message("Not enough space in your looting bag.")
			return false
		}

		return true
	}
	
	@Suspendable fun getInventoryItemPrices(player: Player): Array<Any> {
		var prices = emptyArray<Any>()
		
		if (player.world().realm().isPVP) {
			player.inventory().forEach { item ->
				if (item != null && player.world().prices().containsKey(item.unnote(player.world()).id())) {
					val cost = player.world().prices()[item.unnote(player.world()).id()]
					if (!item.rawtradable(player.world())) prices += -1
					else prices += cost
				} else {
					prices += 1
				}
			}
		} else {
			player.inventory().forEach { item ->
				if (item != null) {
					prices += 0
				} else {
					prices += -1
				}
			}
		}
		
		return prices
	}
	
	@Suspendable fun getLootingBagPrices(player: Player): Int {
		var prices = 0
		
		if (player.world().realm().isPVP) {
			player.lootingBag().forEach { item ->
				if (item != null) {
					if (player.world().prices().containsKey(item.unnote(player.world()).id())) {
						val cost = player.world().prices()[item.unnote(player.world()).id()]
						if (!item.rawtradable(player.world())) prices += -1
						else prices += cost
					} else {
						prices += -1
					}
				}
			}
		} else {
			player.lootingBag().forEach { item ->
				if (item != null) {
					prices += 0
				} else {
					prices += -1
				}
			}
		}
		
		return prices
	}

	private fun toggleLootingBag(it: Script, lootbagId: Int) {
		val player = it.player()
		val slot = it.itemUsedSlot()
		val newId = if (lootbagId == LOOTING_BAG) OPEN_LOOTING_BAG else LOOTING_BAG
		if (player.inventory().remove(Item(lootbagId), true, slot).success()) {
			player.inventory().add(Item(newId), true, slot)
		}
	}

	fun lootbagOpen(player: Player): Boolean {
		return (player.inventory().has(OPEN_LOOTING_BAG))
	}
}
