package nl.bartpelle.veteres.content.areas.zeah.wcguild

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Bart on 6/10/2016.
 */
object Lars {
	
	const val LARS = 7236
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(LARS) @Suspendable {
			it.chatNpc("Welcome to the woodcutting guild. How may I help<br>you?", LARS, 568)
			
			when (it.options("Tell me about the guild.", "Tell me about yourself.", "I'm just passing by.")) {
				1 -> aboutTheGuild(it)
				2 -> aboutYourself(it)
				3 -> justPassingBy(it)
			}
		}
	}
	
	@Suspendable fun aboutTheGuild(it: Script) {
		it.chatPlayer("Tell me about the guild.", 554)
		it.chatNpc("I founded the woodcutting guild when I retired from<br>the Shayzien military. I spotted the ancient " +
				"redwood<br>trees in the hills many years ago from the Shayzien<br>camp, and decided to construct the guild around them.", LARS, 570)
		it.chatNpc("You can find them to the West, just follow the path.", LARS, 567)
		it.chatNpc("However, if you're skilled in combat, or simply afraid of<br>heights, the ent cavern may be more suited for you.<br>" +
				"The ents dwell deep beneath the ancient redwoods, and<br>can be accessed through the cave to the North-West.", LARS, 591)
		it.chatNpc("Is there anything else I can help you with?", LARS, 554)
		
		when (it.options("Tell me about yourself.", "That's all, thanks.")) {
			1 -> aboutYourself(it)
			2 -> it.chatPlayer("That's all, thanks.", 567)
		}
	}
	
	@Suspendable fun aboutYourself(it: Script) {
		it.chatPlayer("Tell me about yourself.", 554)
		it.chatNpc("My name is Lars, and I am the founder of the<br>woodcutting guild. I originally grew up in a Shayzien<br>camp, and was raised on the battlefield.", LARS, 569)
		it.chatNpc("As I grew older, I found peace in the Hosidius house,<br>and the joys of skilled labour. I turned in my battleaxe<br>for a hatchet, and set up a guild for those also talented<br>in the skill of woodcutting.", LARS, 570)
		
		it.chatNpc("Is there anything else I can help you with?", LARS, 554)
		
		when (it.options("Tell me about the guild.", "That's all, thanks.")) {
			1 -> aboutTheGuild(it)
			2 -> it.chatPlayer("That's all, thanks.", 567)
		}
		
	}
	
	@Suspendable fun justPassingBy(it: Script) {
		it.chatPlayer("I'm just passing by.", 588)
		it.chatNpc("Sure, let me know if there's anything I can do for you.", LARS, 567)
	}
	
}