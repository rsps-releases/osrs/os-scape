package nl.bartpelle.veteres.content.interfaces

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.Teleports.CATEGORIES
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.content.areas.wilderness.CommandTeleportPrompt
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.buttonSlot
import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.net.message.game.command.SendTeleports
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.GameCommands

object TeleportInterface {

    @JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
        //Interface options
        repo.onButton(583, 19, @Suspendable { selectCategory(it.player(), it.buttonSlot()) })
        repo.onButton(583, 22, @Suspendable { selectSubcategory(it.player(), it.buttonSlot()) })
        repo.onButton(583, 63, @Suspendable { selectTeleport(it, it.player(), it.buttonSlot()) })

        //Our beautiful wizard
        repo.onNpcOption1(4159) @Suspendable {
            val player = it.player()
            val teleportCategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_CATEGORY_INDEX, 0)
            val teleportSubcategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, 0)

            player.write(SendTeleports("OS-Scape Teleports",
                    teleportCategoryIndex, CATEGORIES,
                    teleportSubcategoryIndex, CATEGORIES[teleportCategoryIndex].subcategories,
                    CATEGORIES[teleportCategoryIndex].subcategories[teleportSubcategoryIndex].teleports))
            player.interfaces().sendMain(583)
        }

        repo.onNpcOption2(4159) @Suspendable {
            val previousTeleport = it.player().attribOr<Tile>(AttributeKey.PREVIOUS_TELEPORT, null)

            if (previousTeleport == null) {
                it.messagebox("You haven't teleport anywhere yet.")
            } else {
                teleport(it, previousTeleport.x, previousTeleport.z, previousTeleport.level, false)
            }
        }
    }

    private fun selectCategory(player: Player, index: Int) {
        val teleportCategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_CATEGORY_INDEX, 0)
        val teleportSubcategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, 0)

        if (teleportCategoryIndex == index && teleportSubcategoryIndex == 0)
            return
        if (index < 0 || index >= CATEGORIES.size)
            return
        if (player.interfaces().visible(583))
            player.interfaces().closeMain()
        player.putattrib(AttributeKey.TELEPORT_CATEGORY_INDEX, index)
        player.putattrib(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, 0)
        player.write(SendTeleports(null,
                player.attribOr<Int>(AttributeKey.TELEPORT_CATEGORY_INDEX, 0), null,
                player.attribOr<Int>(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, 0), CATEGORIES[index].subcategories,
                CATEGORIES[index].subcategories[0].teleports))

        player.interfaces().sendMain(583)

    }

    private fun selectSubcategory(player: Player, index: Int) {
        val teleportCategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_CATEGORY_INDEX, 0)
        val teleportSubcategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, 0)
        if (teleportSubcategoryIndex == index)
            return
        val cat = CATEGORIES[teleportCategoryIndex]
        if (index < 0 || index >= cat.subcategories.size)
            return
        if (player.interfaces().visible(583))
            player.interfaces().closeMain()
        player.putattrib(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, index)
        player.write(SendTeleports(null,
                player.attribOr<Int>(AttributeKey.TELEPORT_CATEGORY_INDEX, 0), null,
                player.attribOr<Int>(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, 0), null,
                cat.subcategories[index].teleports))
        player.interfaces().sendMain(583)
    }

    private fun selectTeleport(it: Script, player: Player, index: Int) {
        val teleportCategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_CATEGORY_INDEX, 0)
        val teleportSubcategoryIndex = player.attribOr<Int>(AttributeKey.TELEPORT_SUBCATEGORY_INDEX, 0)

        if (teleportCategoryIndex == -1)
            return
        val teleports = CATEGORIES[teleportCategoryIndex].subcategories[teleportSubcategoryIndex].teleports
        if (index < 0 || index >= teleports.size)
            return
        val teleport = teleports[index]
        teleport(it, teleport.x, teleport.y, teleport.z)
    }


    @Suspendable fun teleport(it: Script, x: Int, z: Int, level: Int, quick_cast: Boolean = false) {
        it.player.interfaces().closeMain()
        it.player().putattrib(AttributeKey.PREVIOUS_TELEPORT, Tile(x, z, level))
        if (GameCommands.pkTeleportOk(it.player(), x, z)) {
            if (!WildernessLevelIndicator.inWilderness(it.player().tile()) && WildernessLevelIndicator.inWilderness(Tile(x, z, level))) {
                // Show enter wilderness conformation.
                if (!CommandTeleportPrompt.wantsToEnterWild(it)) {
                    return
                }
            }
            if (quick_cast) {
                it.player().lockNoDamage()
                it.delay(1)
                it.player().graphic(1296)
                it.player().animate(3865)
                it.delay(3)
                it.player().teleport(x, z, 0)
                it.animate(-1)
                it.player().unlock()
            } else {
                it.targetNpc()!!.sync().faceEntity(it.player())
                it.targetNpc()!!.sync().shout("Seventhior Distine Molenko!")
                it.targetNpc()!!.sync().animation(1818, 1)
                it.targetNpc()!!.sync().graphic(343, 100, 1)
                it.delay(1)
                it.player().graphic(1296)
                it.player().animate(3865)
                it.player().lockNoDamage()
                it.delay(3)
                it.player().teleport(x, z, level)
                it.animate(-1)
                it.player().unlock()
            }
        }
    }
}