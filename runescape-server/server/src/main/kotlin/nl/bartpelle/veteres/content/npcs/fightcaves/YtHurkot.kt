package nl.bartpelle.veteres.content.npcs.fightcaves

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Situations on 9/19/2016.
 * Edited by Mack on 8/8/2017.
 */

object YtHurkotCombat {
	@JvmField
	val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s

		while (PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackMelee(npc, target, true) && EntityCombat.attackTimerReady(npc)) {

				if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
					target.hit(npc, EntityCombat.randomHit(npc))
				} else {
					target.hit(npc, 0)
				}

				npc.animate(npc.attackAnimation())

				target.putattrib(AttributeKey.LAST_DAMAGER, npc)
				target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
				npc.putattrib(AttributeKey.LAST_TARGET, target)
				EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
			}

			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
}

class YtHurkot(val tztokJad: Npc) {
	
	@JvmField val healJad: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var cycles = 0
		
		while (PlayerCombat.canAttack(npc, tztokJad) && npc.attribOr<Entity?>(AttributeKey.LAST_DAMAGER, null) == null) {
			if (EntityCombat.canAttackMelee(npc, tztokJad, true)) {
				npc.face(tztokJad)
				if (cycles % 4 == 0) {
					tztokJad.graphic(444, 250, 5)
					tztokJad.heal(npc.world().random(10))
					npc.animate(2639)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			cycles++
		}
	}
}