package nl.bartpelle.veteres.content.lottery

import nl.bartpelle.veteres.GameServer
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.util.JSON
import java.sql.Date
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.OffsetTime
import java.time.ZoneOffset
import java.time.temporal.TemporalAdjusters

/**
 * Created by Jonathan on 2/12/2017.
 */
data class LotteryInstance(var worldId: Int, var currency: Int, var depositModifier: Int, var depositModExpiry: Date?,
                           var withdrawModifier: Int, var withdrawModExpiry: Date?, var enddate: Long, var server: GameServer) {
	
	internal val currencyName by lazy { Item(currency).name(server.world()) }
	internal val tokensName = if (currency == 13204) "platinum" else "blood"
	
	
	internal val timeOffset: Long get() = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.SATURDAY)).atTime(OffsetTime.of(20, 0, 0, 0, ZoneOffset.of("+0000"))).toInstant().toEpochMilli() //FFS Oracle u serious?
	
	internal var entrants = mutableMapOf<Int, Long>()
	
	internal val winners = mutableMapOf<Int, Long>()
	
	fun toJson() = JSON.toJson(Lottery.config!!.entrants)!!
	
	fun amountFor(id: Int) = entrants[id] ?: 0
	
	fun winningsFor(id: Int) = winners[id] ?: 0
	
	fun clearAndReset() {
		entrants.clear()
		enddate = timeOffset
	}
	
}