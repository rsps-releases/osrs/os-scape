package nl.bartpelle.veteres.content.combat.magic

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.areas.clanwars.CWSettings
import nl.bartpelle.veteres.content.areas.clanwars.ClanWars
import nl.bartpelle.veteres.content.areas.edgeville.dialogue.EmblemTrader
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.interfaces.Equipment
import nl.bartpelle.veteres.content.items.equipment.RunePouch
import nl.bartpelle.veteres.content.items.equipment.TheaterOfBloodItems
import nl.bartpelle.veteres.content.items.equipment.ZulrahItems
import nl.bartpelle.veteres.content.mechanics.MultiwayCombat
import nl.bartpelle.veteres.content.mechanics.Prayers
import nl.bartpelle.veteres.content.mechanics.Skulling
import nl.bartpelle.veteres.content.mechanics.Venom
import nl.bartpelle.veteres.content.minigames.duelingarena.DuelOptions
import nl.bartpelle.veteres.content.npcs.bosses.abyssalsire.AbyssalSirePhase
import nl.bartpelle.veteres.content.npcs.bosses.abyssalsire.AbyssalSirePhaseOne
import nl.bartpelle.veteres.content.npcs.bosses.abyssalsire.AbyssalSireState
import nl.bartpelle.veteres.content.set
import nl.bartpelle.veteres.content.skills.magic.CombinationRunes
import nl.bartpelle.veteres.content.skills.magic.Staves
import nl.bartpelle.veteres.model.*
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.instance.InstancedMapIdentifier
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.*
import java.util.stream.Collectors

/**
 * Created by Bart on 3/12/2016.
 */
object MagicCombat {
	
	@Suspendable fun doMagicSpell(player: Player, target: Entity, tile: Tile, cast_sound: Int, hit_sound: Int, animation: Int, projectile: Int, speed: Int, startgfx: Int, gfx: Graphic, splash_sound: Int, maxhit: Int, spellbook: Int, level: Int, xp: Double, name: String, vararg runes: Item): Boolean {
		
		
		if (!PlayerCombat.canAttack(player, target)) {
			return false
		}
		if (DuelOptions.ruleToggledOn(player, DuelOptions.NO_MAGIC)) {
			player.message("Magic is disabled for this duel.")
			return false
		}
		
		val spellBook = player.varps().varbit(Varbit.SPELLBOOK)
		if (spellBook != 0 && ClanWars.checkRule(player, CWSettings.MAGIC, 1)) {
			player.message("Non normal magic spellbooks have been disabled for this clan war.")
			return false
		}
		
		if (name != "Bind" && name != "Snare" && name != "Entangle" && ClanWars.checkRule(player, CWSettings.MAGIC, 2)) {
			player.message("All non-binding spells have been disabled for this clan war.")
			return false
		}
		
		if (ClanWars.checkRule(player, CWSettings.MAGIC, 3)) {
			player.message("Magic is disabled for this duel.")
			return false
		}
		
		// See if it's exclusively owned
		val ownerLink = target.attribOr<Tuple<Int, Player>>(AttributeKey.OWNING_PLAYER, Tuple(-1, null))
		if (ownerLink.first() != null && ownerLink.first() >= 0 && ownerLink.first() != player.id()) {
			player.message("They don't seem interested in fighting you.")
			return false
		}
		
		//Check for level requirements
		if (player.skills().level(Skills.MAGIC) < level && !player.privilege().eligibleTo(Privilege.ADMIN)) {
			player.message("Your Magic level is not high enough for this spell.")
			return false
		}
		
		//Check to see if your slayer level is high enough
		if (target.isNpc) {
			val other = target as Npc
			if (other.combatInfo() == null) {
				player.message("You can't attack this npc.")
				return false
			}
			val slayerReq = other.combatInfo()?.slayerlvl ?: 0
			if (!player.world().realm().isPVP && slayerReq > player.skills().level(Skills.SLAYER)) {
				player.message("You need a slayer level of $slayerReq to harm this NPC.")
				return false
			}
		}
		
		if (gfx.id() == 345 && target.isPlayer) {
			// Don't allow teleblock or range when attacking someone in the 'wests' area.
			if (GameCommands.westsAntiragEnabled) {
				if (WildernessLevelIndicator.at_west_dragons(player.tile())) {
					player.message("<col=9A2EFE>You can't use teleblock in this zone.")
					return false
				}
				if (WildernessLevelIndicator.at_west_dragons(target.tile())) {
					player.message("<col=9A2EFE>Your target is in a teleblock-restricted zone.")
					return false
				}
			}
			val active = player.world().allocator().active(player.tile())
			if (active.isPresent) {
				val instance_key = active.get().getIdentifier().orElse(null)
				// If edge PvP is enabled, and the target is in that area.
				if (instance_key == InstancedMapIdentifier.GE_PVP || instance_key == InstancedMapIdentifier.EDGE_PVP || instance_key == InstancedMapIdentifier.CANIFIS_PVP) {
					player.message("<col=9A2EFE>You can't use Teleblock in this PvP instance.")
					return false
				}
			}
		}

		
		//Teleblock
		val tileDist = tile.distance(target.tile())
		if (gfx.id() == 345) {
			if (target.isNpc) {
				player.message("You can only cast that on other player.")
				return false
			}
			if (target.timers().has(TimerKey.TELEBLOCK)) {
				player.message("That player is already being affected by this spell.")
				return false
			}
			if (target.timers().has(TimerKey.TELEBLOCK_IMMUNITY)) {
				player.message("That player is currently immune to this spell.")
				return false
			}
		}
		if (gfx.id() == 179 || gfx.id() == 180 || gfx.id() == 181) {
			if (target.frozen()) {
				player.message("That player is already effected by this spell.")
				return false
			}
		}
		
		//Godspells
		if (name.equals("Saradomin Strike")) {
			if (!player.equipment().hasAny(2412, EmblemTrader.IMBUED_SARADOMIN_CAPE, 21776)) {
				player.message("You need a Saradomin cape to cast Saradomin Strike.")
				return false
			}
			if (!player.equipment().hasAny(2415, 22296)) {
				player.message("You need a Saradomin Staff or Staff of Light to cast Saradomin Strike.")
				return false
			}
		}
		
		if (name.equals("Claws of Guthix")) {
			if (!player.equipment().hasAny(2413, EmblemTrader.IMBUED_GUTHIX_CAPE, 21784)) {
				player.message("You need a Guthix cape to cast Claws of Guthix.")
				return false
			}
			if (!player.equipment().hasAny(2416, 8841)) {
				player.message("You need a Guthix Staff or a Void Knight Mace to cast Claws of Guthix.")
				return false
			}
		}
		
		if (name.equals("Flames of Zamorak")) {
			if (!player.equipment().hasAny(2414, 13333, EmblemTrader.IMBUED_ZAMORAK_CAPE, 21780)) {
				player.message("You need a Zamorak cape to cast Flames of Zamorak.")
				return false
			}
			if (!player.equipment().hasAny(2417, 11791, 12902, 12904)) {
				player.message("You need a Zamorak Staff or a Staff of the Dead to cast Flames of Zamorak.")
				return false
			}
		}
		
		player.timers()[TimerKey.IN_COMBAT] = 5
		
		val weapon = player.equipment().get(3)?.id() ?: -1
		//Kodai wand has 15% change of negating rune cost
		// Now remove runes. Also, apply the staff of the dead effect here (1/8 chance to not take runes).
		val freeCast = (weapon in intArrayOf(11791, 12902, 12904) && player.world().rollDie(8, 1)) || (weapon == 21006 && Math.random() < 0.15)
		
		// There is no message sent when the cost is discarded.
		if (!freeCast) {
			if (!MagicCombat.has(player, runes.asList().toTypedArray(), true))
				return false
		}
		
		player.animate(animation)
		player.world().spawnSound(player.tile(), cast_sound, 0, 15)
		player.graphic(startgfx, 90, 1)
		target.graphic(-1)//reset barrage block, see lemon pk vid 1
		val success = AccuracyFormula.doesHit(player, target, CombatStyle.MAGIC, 1.0) || player.hasAttrib(AttributeKey.ALWAYS_HIT)
		
		val wepid = player.equipment()[3]?.id() ?: -1
		val TRIDENT_SPELL = name.toLowerCase().contains("trident") && (wepid in intArrayOf(11905, 11907, 11908, 12899, TheaterOfBloodItems.SANGUINESTI_CHARGED_ID))
		
		if (TRIDENT_SPELL) {
			player.world().spawnProjectile(player.tile(), target, projectile, 10, 0, 45, (9 * tileDist), 10, 10)
			
			if (wepid in intArrayOf(11905, 11907, TheaterOfBloodItems.SANGUINESTI_CHARGED_ID)) { // used and full normal trident. toxic trident charges degrade when venom is checked.
				val wep = player.equipment()[3]!!
                val notTrident = wepid == TheaterOfBloodItems.SANGUINESTI_CHARGED_ID
				val newcharges = wep.property(ItemAttrib.CHARGES) - 1
				wep.property(ItemAttrib.CHARGES, newcharges)
				
				if (newcharges < 1) {
                    val staffName = if (wepid == TheaterOfBloodItems.SANGUINESTI_CHARGED_ID) "sanguinesti staff" else "trident"
                    val replacementId = if (wepid == TheaterOfBloodItems.SANGUINESTI_CHARGED_ID) TheaterOfBloodItems.SANGUINESTI_UNCHARGED_ID else ZulrahItems.TRIDENT_EMPTY
					player.message("Your $staffName has become <col=FF0000>uncharged</col> as it has run out of charges.")
					player.equipment().set(EquipSlot.WEAPON, Item(replacementId))
				} else if (newcharges == 2499 && !notTrident) {
					player.message("Your trident has become <col=FF0000>untradable</col> as you've used a charge.")
					val used = Item(ZulrahItems.TRIDENT_USED).duplicateProperties(wep)
					player.equipment().set(EquipSlot.WEAPON, used)
				}
			}
			
		} else if (gfx.id() == 345) {
			if (success)
				player.world().spawnProjectile(player.tile(), target, projectile, 40, 20, 45, (9 * tileDist), 10, 10)
			else
				player.world().spawnProjectile(player.tile(), target, 1300, 40, 20, 45, (9 * tileDist), 10, 10)
		} else {
			if (spellbook == 1)
				player.world().spawnProjectile(player.tile(), target, projectile, 40, 20, 50, speed * tileDist, 10, 10)
			if (projectile > 0 && !target.pathQueue().empty() && spellbook == 2)
				player.world().spawnProjectile(player.tile(), target, projectile, 100, 0, 36, speed * tileDist, 50, 80)
		}
		
		if (name.startsWith("Shadow")) {
			if (target.isNpc) {
				val npc = target as Npc
				//If the NPC is abyssal sire, attempt to disorient..
				if (npc.id() == 5886) {
					val combatPhase = npc.attribOr<AbyssalSirePhase>(AttributeKey.ABYSSAL_SIRE_PHASE, AbyssalSirePhase.PHASE_ONE)
					val combatState = npc.attribOr<AbyssalSireState>(AttributeKey.ABYSSAL_SIRE_STATE, AbyssalSireState.COMBAT)
					
					if (!npc.timers().has(TimerKey.ABYSSAL_SIRE_DISORIENTATION) && combatPhase == AbyssalSirePhase.PHASE_ONE
							&& combatState == AbyssalSireState.COMBAT) {
						if (name.contains("rush")) AbyssalSirePhaseOne.attemptToDisorient(player, npc, 25)
						else if (name.contains("burst")) AbyssalSirePhaseOne.attemptToDisorient(player, npc, 50)
						else if (name.contains("blitz")) AbyssalSirePhaseOne.attemptToDisorient(player, npc, 75)
						else if (name.contains("barrage")) AbyssalSirePhaseOne.disorientAbyssalSire(player, npc)
					}
				}
			}
		}
		
		var delay = 1.5 + Math.floor(tileDist.toDouble()) / 2.0
		
		if (TRIDENT_SPELL)
			delay -= 1 // 1 less tick to make it faster
		
		delay = Math.min(Math.max(1.0, delay), 5.0) // Always a minimum delay before impact.
		
		player.timers()[TimerKey.COMBAT_ATTACK] = 5
		if (target.isPlayer) {
			val targ = target as Player
			targ.interfaces().closeMain()
			targ.interfaces().close(162, 550) // Close chatbox
		}
		
		var sound_delay = 10
		
		if (splash_sound != 227) {
			sound_delay = 6
		}
		
		val venom = Venom.attempt(player, target, CombatStyle.MAGIC, success)
		val dummy = target.isNpc && (target as Npc).id() == 2668
		val adjustedMax = CombatFormula.modifyMagicDamage(player, maxhit, name)
		
		val damage = if (dummy) adjustedMax else player.attribOr<Int>(AttributeKey.ALWAYS_HIT, player.world().random(adjustedMax))
		
		if (venom)
			target.venom(player)
		
		if (success) {
			var tbTime = 495
			val tb_delay = if (tileDist <= 2) 1 else if (tileDist <= 5) 2 else 3
			if (gfx.id() == 345) {
				target.timers().extendOrRegister(TimerKey.COMBAT_LOGOUT, 20)
				target.graphic(gfx.id(), 0, tb_delay * 30)//a tick in graphic format/maths is 30.
				
				val half: Boolean = target.varps().varbit(Varbit.PROTECT_FROM_MAGIC) == 1
				if (half) {
					tbTime = 248 //Half teleblock
					/*if (target.varps().varbit(Varbit.PIETY) == 1) {
						tbTime = 150 + target.world().random(50)//1:30 - 2:00 mins
					}*/
				}
				// After investigating on RS - the teleblock lands instantly even from 11 tiles away.
				target.teleblock(tbTime)
				
				// Add base XP - 82xp if a half, 85 is full!
				player.skills().__addXp(Skills.MAGIC, if (half) 82.0 else 85.0, target !is Player)
				Skulling.skull(player, target)
				
				return true
			}
			
			val hit: Hit = target.hitpvp(player, damage, delay.toInt(), CombatStyle.MAGIC).graphic(Graphic(gfx.id(), gfx.height())).submit()
			val actualDamage: Int = hit.damage()
			//delay ^^ 10ms or something idk what % of a gametick but slight delay
			
			// extend the delay by 1 cycle which the pid skipped
			if (hit.hasPidAdjusted) {
				hit.graphic(Graphic(hit.graphic().id(), hit.graphic().height(), 30))
			}
			
			player.skills().__addXp(Skills.MAGIC, xp, target !is Player) // Add base XP first, then damage exp later.
			
			if (TRIDENT_SPELL && target.isNpc) {
				// Trident has attack modes for magic only OR defensive-magic
				PlayerCombat.addCombatXp(player, target, actualDamage, CombatStyle.MAGIC)
			} else {
				// Spells need the attack mode to be specified - otherwise it will default to the mode of our
				// current worn weapon
				PlayerCombat.addCombatXp(player, target, actualDamage, CombatStyle.MAGIC, AttackMode.MAGIC)
			}
			
			player.varps().varp(Varp.ATTACK_PRIORITY_PID, target.index())
			player.world().spawnSound(target.tile(), hit_sound, delay.toInt() * 3 * sound_delay, 10)
			
			// SPELL EFFECTS
			if (name.startsWith("Smoke")) {
				if (player.world().rollDie(100, 25)) {
					target.poison(if (name.contains("rush") || name.contains("burst")) 2 else 4)
				}
			} else if (name.startsWith("Shadow")) {
				val change = if (name.contains("rush") || name.contains("burst")) .1 else .15
				if (target.isPlayer) {
					if (target.skills().level(Skills.ATTACK) > target.skills().xpLevel(Skills.ATTACK) - (target.skills().level(Skills.ATTACK) * change)) {
						target.skills().alterSkill(Skills.ATTACK, -(target.skills().level(Skills.ATTACK) * change).toInt())
					}
				}
			} else if (gfx.id() == 369) { // ice barrage, 20s freeze
				target.freeze(33, player)
			} else if (gfx.id() == 377 || gfx.id() == 373 || gfx.id() == 376 || gfx.id() == 375) {
				player.heal(actualDamage / 4) // Heal for 25% with blood barrage
				
			} else if (gfx.id() == 361 || gfx.id() == 181) { // ice rush (5s)
				target.freeze(8, player)
			} else if (gfx.id() == 363 || gfx.id() == 180) { // ice burst (10s)
				target.freeze(16, player)
				//} else if ((gfx == 381 || gfx == 383) && target.isPlayer) {//shadow barrage
				//    target.skills().alterSkill(Skills.ATTACK, -(target.skills().level(Skills.ATTACK) * 0.15).toInt(), false)
				
			} else if (gfx.id() == 367 || gfx.id() == 179) { // ice blitz and entangle
				target.freeze(25, player) // 15 second freeze timer
				
			} else if (gfx.id() == 78) { // Flames of zammy
				if (target.isPlayer) {
					if (target.skills().level(Skills.MAGIC) > target.skills().xpLevel(Skills.MAGIC) * 0.95) {
						target.skills().setLevel(Skills.MAGIC, (target.skills().xpLevel(Skills.MAGIC) * 0.95).toInt())
					}
				}
			} else if (gfx.id() == 77) { // Claws of guthix
				if (target.isPlayer) {
					if (target.skills().level(Skills.DEFENCE) > target.skills().xpLevel(Skills.DEFENCE) * 0.9) {
						target.skills().setLevel(Skills.DEFENCE, (target.skills().xpLevel(Skills.DEFENCE) * 0.9).toInt())
					}
				} else if (target.isNpc) {
					val npctarg: Npc = target as Npc
					if (npctarg.combatInfo() != null && npctarg.combatInfo().bonuses != null) {
						val cur = npctarg.combatInfo().bonuses
						val raw = player.world().combatInfo(npctarg.id()).bonuses
						cur.slashdefence = (raw.slashdefence * 0.9).toInt()
						cur.stabdefence = (raw.stabdefence * 0.9).toInt()
						cur.crushdefence = (raw.crushdefence * 0.9).toInt()
						cur.magicdefence = (raw.magicdefence * 0.9).toInt()
						cur.rangeddefence = (raw.rangeddefence * 0.9).toInt()
					}
				}
			} else if (gfx.id() == 76) { // Sara strike
				if (target.isPlayer) {
					if (target.skills().level(Skills.PRAYER) > 1) {
						val to: Int = Math.max(0, target.skills().level(Skills.PRAYER) - 1)
						target.skills().setLevel(Skills.PRAYER, to)
						if (to == 0) {
							Prayers.disableAllPrayers(target as Player)
							target.message("You have run out of prayer points, you must recharge at an altar.")
						}
					}
				}
			}
		} else {
			
			player.skills().__addXp(Skills.MAGIC, xp, target !is Player) // Add base XP
			player.world().spawnSound(target.tile(), splash_sound, delay.toInt() * 3 * sound_delay, 10)
			
			if (gfx.id() == 345) {
				// Hit type REGULAR is required here for the splash GFX to work properly.
				target.hitpvp(player, 0, delay.toInt() - 1, CombatStyle.MAGIC).type(Hit.Type.REGULAR).block(false).graphic(Graphic(85, 90, 30)).submit()
				target.timers().extendOrRegister(TimerKey.COMBAT_LOGOUT, 20)
				Skulling.skull(player, target)
				return true
			}
			val hit = target.hitpvp(player, 0, delay.toInt(), CombatStyle.MAGIC).graphic(Graphic(85, 90)).submit()
			
			// extend the delay by 1 cycle which the pid skipped
			if (hit.hasPidAdjusted) {
				hit.graphic(Graphic(hit.graphic().id(), hit.graphic().height(), 30))
			}
		}
		if (name.endsWith("barrage") || name.endsWith("burst")) {
			if (MultiwayCombat.includes(target) && !(ClanWars.inInstance(target) && target.isPlayer && ClanWars.checkRule(target as Player, CWSettings.SINGLE_SPELLS))) {
				multispell(player, target, tile, animation, projectile, speed, startgfx, gfx, maxhit, spellbook, level, xp, name, delay, venom, *runes)
			}
		}
		
		//Set the skulling context.
		Skulling.skull(player, target)
		
		Equipment.checkTargetVenomGear(player, target)
		return true
	}

	@JvmStatic fun deductRunesOrCombo(player: Player, runes: Array<Item>) {
		val removing = ItemContainer(player.world(), runes, ItemContainer.Type.REGULAR)
		// Normal try delete
		runes.forEach { rune ->
			deleteRune(player, rune, removing)
		}
		// Still runes pending removal .. we must be holding combo runes which qualified.
		if (removing.items().filter { i -> i != null }.count() > 0) {
			//player.message("deduct: remaining to delete: ${Arrays.toString(removing.items().filter { i -> i != null }.toTypedArray())}")
			removing.items().filter { i -> i != null }.forEach { rune ->
				// Elemental?
				var deleted = 0
				if (rune.id() in 554 .. 558) {
					val possibilities = CombinationRunes.COMBO_RUNES.entries.stream().filter { combo -> combo.value.elementals[0] == rune.id() || combo.value.elementals[1] == rune.id() }.map { combo -> combo.key }.collect(Collectors.toList())
					possibilities.forEach { combo ->
						// We're looping multiple runes .. only delete how much we actually need to. So keep track of deleted count.
						if (deleted < rune.amount())
							deleted += deleteRune(player, Item(combo, rune.amount()), removing)
					}
				}
			}
		}
	}

	private fun deleteRune(player: Player, rune: Item, removing: ItemContainer): Int {
		// Try inv
		var deleted = 0
		val removed = player.inventory().remove(rune, true)
		deleted += removed.completed()
		if (removed.completed() > 0)
			removing.remove(Item(rune.id(), removed.completed()), false)

		// Try to remove from the rune pouch if we didn't remove all.
		if (removed.completed() < removed.requested()) {
			val leftover = removed.requested() - removed.completed()
			val rpRemoved = RunePouch.remove(player, rune.id(), leftover)
			removing.remove(Item(rune.id(), rpRemoved), false)
			deleted += rpRemoved
		}
		//player.message("Deleting rune $rune .. successful: $deleted")
		return deleted
	}

	@JvmStatic fun has(player: Player, runes: Array<Item>, delete: Boolean): Boolean {
		val reqs = ItemContainer(player.world(), runes, ItemContainer.Type.REGULAR)
		// Remove based on if we have infinite from wearing equipment.
		runes.filter { r -> r != null }.forEach { r -> if (Staves.staffRunes(player, r.id())) reqs.remove(Item(r.id(), Integer.MAX_VALUE), true) }

		// The staff we're wearing took care of all reqs
		if (reqs.items().filter { i -> i != null }.count() == 0)
			return true

		// Find uses where elemental runes take the FULL COMBO RUNE (for example if a spell takes BOTH air + water in a mud rune)
		val combos = CombinationRunes.findComboRuneUses(player, reqs.items())
		// This container will have the runes still required after combination runes have eliminated some.
		val postComboReqs = ItemContainer(player.world(), combos.remainingRequired, ItemContainer.Type.REGULAR)

		// The staff we're wearing took care of all reqs
		if (postComboReqs.items().filter { i -> i != null }.count() == 0 && !delete) {
			// Only return true if not deleting, otherwise deleting code is below
			return true
		}

		// Iterate over the remaining required runes
		postComboReqs.items().filter { i -> i != null }.forEach { rune ->
			if (realOrComboRuneElementalCount(player, rune.id()) < rune.amount()) {
				player.message("You do not have enough " + rune.name(player.world()) + "s to cast this spell.")
				return false
			}
		}

		// At this point all rune requirements will have been met.
		if (delete) {
			// Delete combo runes
			combos.usedComboRunes.forEach { used ->
				if (used.runepouch)
					RunePouch.remove(player, used.id, used.amt)
				else
					player.inventory().remove(Item(used.id, used.amt), false)
			}
			// Delete remaining required runes using the old method. This removes from inv/RP.
			deductRunesOrCombo(player, postComboReqs.items().filter { i -> i != null }.toTypedArray())
		}
		return true
	}

	private fun realOrComboRuneElementalCount(player: Player, elementalId: Int): Int {
		// Possibilities = types of runes acceptable as equivilent of this elemental rune. Mud would account for water + earth
		val possibilities = mutableListOf(elementalId)
		CombinationRunes.COMBO_RUNES.entries.stream().filter { combo -> combo.value.elementals[0] == elementalId || combo.value.elementals[1] == elementalId }.forEach { combo -> possibilities.add(combo.key)}
		return possibilities.sumBy { RunePouch.numberOf(player, it) + player.inventory().count(it) }
	}

	@Suspendable fun multispell(player: Player, focus: Entity, tile: Tile, animation: Int, projectile: Int, speed: Int,
	                            startgfx: Int, gfx: Graphic, maxhit: Int, spellbook: Int, level: Int, xp: Double, name: String, delay: Double, venom: Boolean, vararg runes: Item): Boolean {
		// Targets are either close npcs or players, not both. Depends on primary focus target.
		val targets = mutableListOf<Entity>()
		if (focus.isPlayer) {
			for (p2: Player in focus.closePlayers(1)) {
				if (p2 == focus || p2 == player) {
					//dont hit us, or the target we've already hit
					continue
				}
				if (p2.varps().varbit(Varbit.MULTIWAY_AREA) == 0 || !WildernessLevelIndicator.inAttackableArea(p2) || !MultiwayCombat.includes(p2)) {
					//not in the multi area and we were, don't carry over.
					continue
				}
				if (!PlayerCombat.canAttack(player, p2)) {
					continue
				}
				targets.add(p2)
			}
		} else if (focus.isNpc) {
			for (npc: Npc in focus.closeNpcs(1)) {
				if (npc == focus || npc == player) {
					//dont hit us, or the target we've already hit
					continue
				}
				
				//Inferno checks
				if (npc.id() == 7710 || npc.id() == 7709) {
					continue
				}
				
				if (!MultiwayCombat.includes(npc)) {
					//not in the multi area and we were, don't carry over.
					continue
				}
				if (!PlayerCombat.canAttack(player, npc)) {
					continue
				}
				targets.add(npc)
			}
		}
		for (target in targets) {
			val hit = player.attribOr<Int>(AttributeKey.ALWAYS_HIT, player.world().random(CombatFormula.modifyMagicDamage(player, maxhit, name)))
			
			target.putattrib(AttributeKey.LAST_DAMAGER, player)
			target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
			player.putattrib(AttributeKey.LAST_TARGET, target)
			
			val tileDist = tile.distance(target.tile())
			player.animate(animation)
			player.graphic(startgfx, 90, 1)
			target.graphic(-1)//reset barrage block, see lemon pk vid 1
			
			if (spellbook == 1)
				player.world().spawnProjectile(player.tile(), target, projectile, 40, 20, 50, speed * tileDist, 10, 10)
			
			if (projectile > 0 && !target.pathQueue().empty() && spellbook == 2)
				player.world().spawnProjectile(player.tile(), target, projectile, 100, 0, 36, speed * tileDist, 50, 80)
			
			player.timers()[TimerKey.COMBAT_ATTACK] = 5
			val success = AccuracyFormula.doesHit(player, target, CombatStyle.MAGIC, 1.0) || player.hasAttrib(AttributeKey.ALWAYS_HIT)
			
			if (target.isPlayer) {
				val ptarg = target as Player
				ptarg.interfaces().closeMain()
				ptarg.interfaces().close(162, 550) // Close chatbox
			}
			
			if (success) {
				if (venom && player.world().rollDie(4, 1))
					target.venom(player)
				
				if (spellbook == 1)
					target.hitpvp(player, hit, delay.toInt(), CombatStyle.MAGIC).graphic(Graphic(gfx.id(), 80)).submit()
				if (spellbook == 2)
					target.hitpvp(player, hit, delay.toInt(), CombatStyle.MAGIC).graphic(Graphic(gfx.id(), gfx.height(), 10)).submit()
				//^^ delay 10ms or something idk what % of a gametick but slight delay
				
				PlayerCombat.addCombatXp(player, target, hit, CombatStyle.MAGIC, AttackMode.MAGIC)
				
				if (gfx.id() == 369)//ice barrage
					target.freeze(33, player) // 20 second freeze timer
				else if (gfx.id() == 377 || gfx.id() == 373 || gfx.id() == 376 || gfx.id() == 375)
					player.heal(hit / 4) // Heal for 25% with blood barrage
				
				//else if ((gfx == 379 || gfx == 382) && target.isPlayer) // smoke barrage TODO whys this commented
				//    target.skills().alterSkill(Skills.ATTACK, -(target.skills().level(Skills.ATTACK) * 0.1).toInt(), false)
				
				else if (gfx.id() == 361 || gfx.id() == 181)
					target.freeze(8, player)
				else if (gfx.id() == 363 || gfx.id() == 180)
					target.freeze(16, player)
				//else if ((gfx == 381 || gfx == 383) && target.isPlayer) //shadow barrage TODO why this commented
				//    target.skills().alterSkill(Skills.ATTACK, -(target.skills().level(Skills.ATTACK) * 0.15).toInt(), false)
				else if (gfx.id() == 367 || gfx.id() == 179) {
					// ice blitz and entangle
					target.freeze(25, player) //15 second freeze timer
				}
			} else {
				player.skills().__addXp(Skills.MAGIC, xp, target !is Player) // Add base XP
				target.hitpvp(player, 0, delay.toInt(), CombatStyle.MAGIC).graphic(Graphic(85, 90)).submit()
			}
		}
		return true
	}
	
}