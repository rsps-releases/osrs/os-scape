package nl.bartpelle.veteres.content.skills.slayer.master

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.skills.slayer.Slayer
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature
import nl.bartpelle.veteres.content.skills.slayer.SlayerRewards
import nl.bartpelle.veteres.content.skills.slayer.showSlayerRewards
import nl.bartpelle.veteres.content.slayerTaskAmount
import nl.bartpelle.veteres.content.slayerTaskId
import nl.bartpelle.veteres.fs.NpcDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 11/11/2015.
 */
object Nieve {
	
	val NIEVE = 490
	
	fun assignTask(player: Player) {
		val def = Slayer.master(NIEVE)!!.randomTask(player)
		player.varps().varbit(Varbit.SLAYER_MASTER, Slayer.NIEVE_ID)
		player.putattrib(AttributeKey.SLAYER_TASK_ID, def.creatureUid)
		
		var task_amt = player.world().random(def.range()).toDouble()
		SlayerRewards.multipliable.forEach { creature ->
			if (creature.first == def.creatureUid) {
				if (player.varps().varbit(creature.second) == 1) {
					task_amt *= 1.4
				}
			}
		}

		player.putattrib(AttributeKey.SLAYER_TASK_AMT, task_amt.toInt())
		Slayer.displayCurrentAssignment(player)
	}
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(NIEVE, s@ @Suspendable {
			if (it.player().skills().combatLevel() < 85) {
				it.message("You need a combat level of 85 to talk to Nieve.")
				return@s
			}
			it.player().world().definitions().get(NpcDefinition::class.java, NIEVE).name = "Nieve"
			it.chatNpc("'Ello, and what are you after then?", NIEVE, 554)
			
			when (it.optionsS("Select an Option", "I need another assignment.", "Have you any rewards for me, or anything to trade?",
					"Let's talk about the difficulty of my assignments.", "Er... Nothing...")) {
				"Have you any rewards for me, or anything to trade?" -> {
					it.chatPlayer("Have you any rewards for me, or anything to trade?", 554)
					it.chatNpc("I have quite a few rewards you can earn, and a wide<br>variety of Slayer equipment for sale.", NIEVE, 568)
					
					when (it.optionsS("Select an Option", "Look at rewards.", "Look at shop.")) {
						"Look at rewards." -> it.player().interfaces().showSlayerRewards()
						"Look at shop." -> it.player().world().shop(10).display(it.player())
					}
				}
				
				"Er... Nothing..." -> {
					it.chatPlayer("Er... Nothing...")
				}
				
				"I need another assignment." -> {
					it.chatPlayer("I need another assignment.")
					
					// Tell the player if we can go Vannaka-mode
					/* if (it.player().skills().combatLevel() >= 40) {
						 it.chatNpc("You're actually very strong, are you sure you don't<br>want Vannaka under Edgeville to assign you a task?", VANNAKA, 589)
 
						 when (it.options("No that's okay, I'll take a task from you.", "Oh okay then, I'll go talk to Vannaka.")) {
							 1 -> it.chatPlayer("No that's okay, I'll take a task from you.", 567)
							 2 -> {
								 it.chatPlayer("Oh okay then, I'll go talk to Vannaka.",567)
								 return@s
							 }
						 }
					 }*/
					
					giveTask(it)
				}
			}
		})
		
		r.onNpcOption2(NIEVE) { giveTask(it) } // Quick tasking
		r.onNpcOption3(NIEVE) { it.player().world().shop(10).display(it.player()) } // Slayer store
		r.onNpcOption4(NIEVE) { it.player().interfaces().showSlayerRewards() } // Reward shop
	}
	
	@Suspendable fun giveTask(it: Script) {
		it.player().world().definitions().get(NpcDefinition::class.java, NIEVE).name = "Nieve"
		
		// Time to check our task state. Can we hand out?
		val numleft: Int = it.slayerTaskAmount()
		if (numleft > 0) {
			it.chatNpc("You're still hunting something, come back when you've<br>finished your task.", NIEVE, 576)
			return
		}
		
		//Is the player 85 combat?
		if (it.player().skills().combatLevel() < 85) {
			it.message("You need a combat level of 85 to talk to Nieve.")
			return
		}
		
		// Give them a task.
		assignTask(it.player())
		
		val task: SlayerCreature = SlayerCreature.lookup(it.slayerTaskId())!!
		val num: Int = it.slayerTaskAmount()
		it.chatNpc("Excellent, you're doing great. Your new task is to kill $num ${Slayer.taskName(it.player(), task.uid)}.", NIEVE, 589)
		
		when (it.options("Got any tips for me?", "Okay, great!")) {
			1 -> {
				it.chatPlayer("Got any tips for me?", 554)
				it.chatNpc(Slayer.tipFor(task), NIEVE)
				it.chatPlayer("Great, thanks!")
			}
			2 -> {
				it.chatPlayer("Okay, great!")
			}
		}
	}
	
}