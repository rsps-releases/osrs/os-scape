package nl.bartpelle.veteres.content.items

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 4/10/2016.
 */

object EmptyItem {
	
	val EMPTY_BUCKET = 1925
	val BUCKETS_OF_STUFF = arrayListOf(1783, 1927, 1929, 4286, 4687, 4693, 7622, 7624, 7626, 9659)
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		
		for (BUCKET in BUCKETS_OF_STUFF) {
			r.onItemOption4(BUCKET) {
				if (it.player().inventory().remove(Item(BUCKET), false).success()) {
					it.player().inventory().add(Item(EMPTY_BUCKET), true)
					it.message("You empty the contents of the bucket on the floor.")
				}
			}
		}
	}
}

