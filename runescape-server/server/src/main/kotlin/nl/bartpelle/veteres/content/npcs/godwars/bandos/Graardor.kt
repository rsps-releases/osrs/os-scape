package nl.bartpelle.veteres.content.npcs.godwars.bandos

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.content.npcs.godwars.GwdLogic
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle
import java.util.*

/**
 * Created by Situations on 11/27/2015.
 */

object Graardor {
	
	
	@JvmStatic val BANDOS_AREA = Area(2863, 5350, 2877, 5370)
	@JvmStatic var lastBossDamager: Entity? = null
	
	@JvmStatic fun isMinion(n: Npc): Boolean {
		return n.id() in arrayOf(2216, 2217, 2218)
	}
	
	val QUOTES = arrayOf("Death to our enemies!",
			"Brargh!",
			"Break their bones!",
			"For the glory of Bandos!",
			"Split their skulls!",
			"We feast on the bones of our enemies tonight!",
			"CHAAARGE!",
			"Crush them underfoot!",
			"All glory to Bandos!",
			"GRRRAAAAAR!",
			"FOR THE GLORY OF THE BIG HIGH WAR GOD!")
	
	@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {
		val npc = it.npc()
		var target = EntityCombat.getTarget(it) ?: return@s
		
		while (EntityCombat.targetOk(npc, target, 50) && PlayerCombat.canAttack(npc, target)) {
			if (EntityCombat.canAttackDistant(npc, target, true, 10)) {
				val canmelee = EntityCombat.canAttackMelee(npc, target, true)
				
				if (EntityCombat.attackTimerReady(npc)) {
					// Do a quote?
					if (npc.world().rollDie(2, 1)) {
						npc.sync().shout(npc.world().random(QUOTES))
					}
					
					// Attack the player
					if (!canmelee || npc.world().rollDie(3, 1)) {
						attackMage(npc, target)
					} else {
						attackMelee(npc, target)
					}
					
					// .. and go into sleep mode.
					target.putattrib(AttributeKey.LAST_DAMAGER, npc)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					npc.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(npc, npc.combatInfo().attackspeed)
				}
			}
			
			EntityCombat.postHitLogic(npc)
			it.delay(1)
			target = EntityCombat.refreshTarget(it) ?: return@s
		}
	}
	
	fun attackMage(npc: Npc, p: Entity) {
		npc.animate(7021)
		
		npc.world().players().forEachInAreaKt(BANDOS_AREA, { target ->
			val tileDist = npc.tile().transform(1, 1, 0).distance(target.tile())
			npc.world().spawnProjectile(npc, target, 1202, 1, 5, 25, 12 * tileDist, 15, 10)
			val delay = Math.max(1, (20 + (tileDist * 12)) / 30)
			
			if (EntityCombat.attemptHit(npc, target, CombatStyle.RANGE)) {
				target.hit(npc, npc.world().random(35), delay.toInt()).combatStyle(CombatStyle.RANGE)
			} else {
				target.hit(npc, 0, delay.toInt()).combatStyle(CombatStyle.RANGE)
			}
		})
	}
	
	fun attackMelee(npc: Npc, target: Entity) {
		npc.animate(7018)
		
		if (EntityCombat.attemptHit(npc, target, CombatStyle.MELEE)) {
			val hit = target.hit(npc, EntityCombat.randomHit(npc), 1).combatStyle(CombatStyle.MELEE)
		} else {
			target.hit(npc, 0, 1).combatStyle(CombatStyle.MELEE)
		}
		
		// If we're in melee distance it's actually classed as if the target hit us -- has an effect on auto-retal in gwd!
		if (GwdLogic.isBoss(npc.id())) {
			val last_attacked_map = npc.attribOr<HashMap<Entity, Long>>(AttributeKey.LAST_ATTACKED_MAP, HashMap<Entity, Long>())
			last_attacked_map.put(target, System.currentTimeMillis())
			npc.putattrib(AttributeKey.LAST_ATTACKED_MAP, last_attacked_map)
		}
	}
}