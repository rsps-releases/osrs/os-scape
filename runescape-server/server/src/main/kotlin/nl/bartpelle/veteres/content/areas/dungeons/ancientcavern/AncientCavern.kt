package nl.bartpelle.veteres.content.areas.dungeons.ancientcavern

import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.player
import nl.bartpelle.veteres.script.ScriptRepository

object AncientCavern {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		// Whirlpool
		r.onObject(25274) {
			it.message("You dive into the swirling maelstrom of the whirlpool.")
			it.message("You are swirled beneath the water, the darkness and pressure are overwhelming.")
			it.message("Mythical forces guide you into a cavern below the whirlpool.")
			it.player.teleport(1763, 5365, 1)
		}
		
		// Log
		r.onObject(25216) {
			it.player.message("You jump on the log and dislodge it. You guide your makeshift vessel through the caves to an unknown destination.")
			it.player.message("You find yourself on the banks of the river, far below the lake.")
			it.player.teleport(2531, 3445)
		}
		
		r.onObject(25336) { it.player.teleport(1768, 5366, 1) }
		r.onObject(25338) { it.player.teleport(1772, 5366, 0) }
		r.onObject(25337) { it.player.message("The path is far too slippery to climb. How inconvenient.") }
		
		r.onObject(25339) { it.player.teleport(1768, 5366, 1) }
		r.onObject(25340) { it.player.teleport(1778, 5346, 0) }
	}
	
}
