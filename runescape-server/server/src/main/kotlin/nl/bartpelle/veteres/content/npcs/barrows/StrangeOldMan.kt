package nl.bartpelle.veteres.content.npcs.barrows

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Jak on 20/04/2016.
 */
object StrangeOldMan {
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(1671) @Suspendable {
			if (it.player().inventory().freeSlots() >= 1) {
				it.player().inventory().add(Item(952, 1), true)
				it.itemBox("The man hands you a spade.", 952)
			} else {
				it.chatNpc("hmhmh...", 1671)
			}
		}
	}
}