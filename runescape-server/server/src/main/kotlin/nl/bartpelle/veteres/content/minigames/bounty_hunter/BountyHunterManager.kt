package nl.bartpelle.veteres.content.minigames.bounty_hunter

import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.achievements.AchievementAction
import nl.bartpelle.veteres.content.achievements.AchievementCategory
import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.content.mechanics.FarmPrevention
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.GroundItem
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.net.message.game.command.InterfaceVisibility
import nl.bartpelle.veteres.net.message.game.command.SetHintArrow
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Mack on 9/16/2017.
 */
object BountyHunterManager {
	
	/**
	 * A flag checking if the bounty hunter activity is active.
	 */
	@JvmStatic var active = false
	
	/**
	 * The varbit ID to be sent when a player with a target leaves the wilderness.
	 * NOTE: inter 90,46 visibility must be flagged true to appear.
	 */
	const val SAFETY_TIMER_VARBIT = 877
	
	/**
	 * The amount of game ticks the safety timer begins at.
	 */
	const val SAFETY_TIMER_PENALTY_TICKS = 200
	
	/**
	 * The bounty hunter interface ID.
	 */
	const val MAIN = 90
	
	/**
	 * The target name widget.
	 */
	const val TARGET_NAME_CHILD = 37
	
	/**
	 * The level range widget for displaying wilderness level area your target is in.
	 */
	const val LEVEL_RANGE_CHILD = 38
	
	/**
	 * The varbit id to send a specific wealth indicator on the interface.
	 */
	const val WEALTH_VARBIT = 1538
	
	/**
	 * The varbit id to display the highest emblem the target has.
	 */
	const val EMBLEM_VARBIT = 4162
	
	/**
	 * The max amount of times a player is allowed to skip
	 */
	const val MAX_SKIP_ALLOWANCE = 5
	
	/**
	 * The collection of players who are in the wilderness and could potentially be assigned as a target.
	 */
	var potentialTargets: ArrayList<Player> = ArrayList()
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onLogin {
			if (it.player().timers().has(TimerKey.BOUNTY_HUNTER_PENALTY)) {
				it.player().timers().cancel(TimerKey.BOUNTY_HUNTER_PENALTY)
			}
		}
		sr.onButton(MAIN, 40, {
			if (hasTarget(it.player())) {
				if (it.optionsTitled("<col=FF0000>Really skip your target?", "Yes, skip my current target.", "Nevermind.") == 1) {
					
					if (it.player().timers().has(TimerKey.BOUNTY_HUNTER_SKIP_COOLDOWN)) {
						it.player().message("You may only skip a target once every minute. Wait a little longer...")
						return@onButton
					}
					
					val target = it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET)
					
					unassignTargetFor(it.player(), true, false)
					unassignTargetFor(target, false, false)
				}
			} else {
				it.player().message("You currently have no target in which you can skip.")
			}
		})
		sr.onTimer(TimerKey.BOUNTY_HUNTER_INTERFACE_UPDATE, {
		
			if (hasTarget(it.player())) {
				displayInformationFor(it.player())
				it.player().timers().register(TimerKey.BOUNTY_HUNTER_INTERFACE_UPDATE, 15)
			}
		})
		sr.onTimer(TimerKey.BOUNTY_HUNTER_SKIP_RESET, {
			it.player().clearattrib(AttributeKey.BOUNTY_HUNTER_TARGET_SKIPS)
		})
		sr.onTimer(TimerKey.BOUNTY_HUNTER_SAFETY_TIMER, {
			
			if (hasTarget(it.player())) {
				
				val target = it.player().attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET)
				
				it.player().message("<col=FF0000>You did not return to the wilderness in time therefore you lost your current target.")
				unassignTargetFor(it.player(), false, false)
				unassignTargetFor(target, false, true)
			}
		})
	}
	
	/**
	 * A flag checking if the player can be added to the collection of potential targets.
	 */
	@JvmStatic fun canAdd(player: Player): Boolean {
		
		return (!potentialTargets.contains(player))
	}
	
	/**
	 * Adds the player to the collection.
	 */
	@JvmStatic fun addToCollection(player: Player) {
		
		potentialTargets.add(player)
		
		//Upon entering, if they have no target, let's refresh this interface.
		if (!hasTarget(player)) {
			hardResetInterface(player)
		} else {
			updateSafetyTimerFor(player, true)
		}
	}
	
	/**
	 * A flag checking if the player can be removed from the bounty hunter selection.
	 */
	@JvmStatic fun canRemove(player: Player): Boolean {
		return (potentialTargets.contains(player))
	}
	
	/**
	 * A flag checking if the player has an active target.
	 */
	@JvmStatic fun hasTarget(player: Player): Boolean {
		return (player.attribOr<Player>(AttributeKey.BOUNTY_HUNTER_TARGET, null) != null)
	}
	
	/**
	 * Supplies the target {@link Player} object for the specified argument. This is not a null-safe approach so you must verify the player has a
	 * target prior to calling this.
	 */
	@JvmStatic fun getTargetFor(player: Player): Player? {
		return (player.attrib(AttributeKey.BOUNTY_HUNTER_TARGET))
	}
	
	/**
	 * A flag checking if the specified player argument matches the player's current target, if available.
	 */
	@JvmStatic fun isTarget(player: Player, target: Player): Boolean {
		return (player.attribOr<Player>(AttributeKey.BOUNTY_HUNTER_TARGET, null) != null) && (player.attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET) == target)
	}
	
	/**
	 * A flag checking if the player has a penalty active.
	 */
	@JvmStatic fun isUnderPenalty(player: Player): Boolean {
		return (player.timers().has(TimerKey.BOUNTY_HUNTER_PENALTY))
	}
	
	/**
	 * Removes the player from the bounty hunter selection.
	 */
	@JvmStatic fun remove(player: Player, logout: Boolean) {
		
		potentialTargets.remove(player)
		
		if (hasTarget(player)) {
			updateSafetyTimerFor(player, false)
		}
		
		if (logout && hasTarget(player)) {
			unassignTargetFor(getTargetFor(player)!!, false, true)
			unassignTargetFor(player, true, false)
		}
	}
	
	/**
	 * Preconditions needing to be met in order to be a target.
	 */
	@JvmStatic fun meetsCriteria(player: Player, potential: Player): Boolean {
		if (player.bot() || potential.bot())
			return false
		if (potential.attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false) == true || player.attribOr<Boolean>(AttributeKey.BOUNTY_HUNTER_TOGGLED_OFF, false) == true) {
			
			return false
		}
		if ((WildernessLevelIndicator.wildernessLevel(player.tile()) > 10 && WildernessLevelIndicator.wildernessLevel(potential.tile()) < 10) || (WildernessLevelIndicator.wildernessLevel(potential.tile()) > 10 && WildernessLevelIndicator.wildernessLevel(player.tile()) < 10)) {
			
			return false
		}
		if (player == potential || player.name() == potential.name()) {
			
			return false
		}
		if (potential.dead() || player.dead()) {
			
			return false
		}
		if (!player.privilege().eligibleTo(Privilege.ADMIN) && FarmPrevention.contains(player, potential)) {
			return false
		}
		if (potential.timers().has(TimerKey.BOUNTY_HUNTER_TARGET_DELAY) || player.timers().has(TimerKey.BOUNTY_HUNTER_TARGET_DELAY)) {
			
			return false
		}
		if (potential.timers().has(TimerKey.BOUNTY_HUNTER_SKIP_COOLDOWN) || player.timers().has(TimerKey.BOUNTY_HUNTER_SKIP_COOLDOWN)) {
			
			return false
		}
		if (player.attribOr<String>(AttributeKey.BOUNTY_HUNTER_LAST_SKIPPED_TARGET, "") == potential.name() || potential.attribOr<String>(AttributeKey.BOUNTY_HUNTER_LAST_SKIPPED_TARGET, "") == player.name()) {
			
			return false
		}
		if (hasTarget(potential) || hasTarget(player)) {
			
			return false
		}
		
		return (player.skills().combatLevel() + WildernessLevelIndicator.wildernessLevel(player.tile()) >= (potential.skills().combatLevel()) && (player.skills().combatLevel() - WildernessLevelIndicator.wildernessLevel(player.tile()) <= potential.skills().combatLevel()))
	}
	
	/**
	 * Begins or resumes the safety timer a player is allowed while having a target.
	 */
	private fun updateSafetyTimerFor(player: Player, pause: Boolean) {
		val visible = !WildernessLevelIndicator.inWilderness(player.tile())
		var ticks = if (player.varps().varbit(SAFETY_TIMER_VARBIT) > 0) player.varps().varbit(SAFETY_TIMER_VARBIT) else SAFETY_TIMER_PENALTY_TICKS
		
		if (pause) {
			player.varps().varbit(SAFETY_TIMER_VARBIT, player.timers().left(TimerKey.BOUNTY_HUNTER_SAFETY_TIMER))
			player.timers().cancel(TimerKey.BOUNTY_HUNTER_SAFETY_TIMER)
		}
		
		if (visible) {
			player.varps().varbit(SAFETY_TIMER_VARBIT, ticks)
			player.timers().register(TimerKey.BOUNTY_HUNTER_SAFETY_TIMER, ticks)
		}
		
		player.write(InterfaceVisibility(90, 46, visible))
	}
	
	/**
	 * Binds a target to the specified player argument.
	 */
	fun bindTarget(player: Player, target: Player) {
		player.putattrib(AttributeKey.BOUNTY_HUNTER_TARGET, target)
		target.putattrib(AttributeKey.BOUNTY_HUNTER_TARGET, player)
		
		player.write(SetHintArrow(target))
		target.write(SetHintArrow(player))
		
		displayInformationFor(player)
		displayInformationFor(target)
		
		player.message("<col=FF0000>You were assigned a target: ${target.name()}")
		target.message("<col=FF0000>You were assigned a target: ${player.name()}")
		
		player.timers().register(TimerKey.BOUNTY_HUNTER_INTERFACE_UPDATE, 15)
		target.timers().register(TimerKey.BOUNTY_HUNTER_INTERFACE_UPDATE, 15)
	}
	
	/**
	 * Performs the necessary removal of information if a player decides to skip,
	 * leaves or kills their current target.
	 */
	fun unassignTargetFor(player: Player, penalize: Boolean, announce: Boolean) {
		
		val target = player.attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET)
		
		if (announce) {
			val skipOrNot = (if (penalize) "skipped" else "lost")
			player.message("You have $skipOrNot your target current target. You will receive a new one shortly.")
		}
		
		if (penalize) {
			
			//Start our BH skip reset timer. In osrs, apparently, after X minutes the counter for number of targets you skipped gets reset.
			/*if (player.timers().has(TimerKey.BOUNTY_HUNTER_SKIP_RESET)) {
				player.timers().register(TimerKey.BOUNTY_HUNTER_SKIP_RESET, 1_500)
			}*/
			
			if (!player.timers().has(TimerKey.BOUNTY_HUNTER_SKIP_COOLDOWN)) {
				player.timers().register(TimerKey.BOUNTY_HUNTER_SKIP_COOLDOWN, 100)
			}
			
			player.putattrib(AttributeKey.BOUNTY_HUNTER_LAST_SKIPPED_TARGET, target.name())
			player.varps().varbit(SAFETY_TIMER_VARBIT, 0)
			
//			player.modifyNumericalAttribute(AttributeKey.BOUNTY_HUNTER_TARGET_SKIPS, 1, 0)
			
			/*if (player.attribOr<Int>(AttributeKey.BOUNTY_HUNTER_TARGET_SKIPS, 0) >= MAX_SKIP_ALLOWANCE) {
				applyPenalty(player)
			}*/
		}
		
		player.timers().cancel(TimerKey.BOUNTY_HUNTER_INTERFACE_UPDATE)
		player.putattrib(AttributeKey.BOUNTY_HUNTER_TARGET, null)
		player.write(SetHintArrow(player.attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET)))
		
		//Add a small delay in between finding targets.
		player.timers().register(TimerKey.BOUNTY_HUNTER_TARGET_DELAY, 5)
		
		hardResetInterface(player)
	}
	
	/**
	 * Actions we manage on kill within the bounty hunter context.
	 */
	@JvmStatic fun onKill(killer: Player, dead: Player) {
		
		val enabled = false

        val emblem = BountyHunterEmblem.resolveBestEmblem(killer.inventory())

        if (enabled) {

			val rolledEmblem = killer.world().random(4) == 1

			//If we rolled an emblem, drop it.
			if (rolledEmblem) {
				val emblemGround: GroundItem = GroundItem(killer.world(), BountyHunterEmblem.TIER_ONE.item(), dead.tile(), killer.id())
				killer.world().spawnGroundItem(emblemGround)
			}
		}

		//If we have an emblem to upgrade then we will continue.
		if (emblem != BountyHunterEmblem.NONE) {
			
			val emblemItem = Item(emblem.id())
			
			killer.inventory().remove(emblemItem, true)
			killer.inventory().add(BountyHunterEmblem.next(emblem), true)
			killer.message("<col=FF0000>An emblem of yours has been upgraded!")
		}
		
		AchievementAction.processCategoryAchievement(killer, AchievementCategory.BOUNTY_HUNTER)
		
		killer.timers().addOrSet(TimerKey.BOUNTY_HUNTER_TARGET_DELAY, 30)
	}
	
	/**
	 * Actions we handle on death.
	 */
	@JvmStatic fun onDeath(dead: Player, killer: Player) {
		
		var announce = true
		
		//If the target killed us then we reward them.
		if (killer.isPlayer && killer != dead && isTarget(dead, killer) && !FarmPrevention.contains(killer, dead)) {
			announce = false
			BountyHunterManager.onKill(killer, dead)
		}
		
		//Add the buffer timer
		dead.timers().addOrSet(TimerKey.BOUNTY_HUNTER_TARGET_DELAY, 30)
		
		//If they have a target, unbind them.
		if (getTargetFor(dead) != null) {
			unassignTargetFor(getTargetFor(dead)!!, false, announce)
			unassignTargetFor(dead, false, false)
		}
	}
	
	/**
	 * Updates a specific widget on the BH interface.
	 */
	fun updateWidget(player: Player, vararg args: Any) {
		
		val child = args[0]
		
		if (child == TARGET_NAME_CHILD) {
			if (hasTarget(player)) {
				player.interfaces().text(MAIN, TARGET_NAME_CHILD, player.attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).name())
			} else if (isUnderPenalty(player)) {
				player.interfaces().text(MAIN, TARGET_NAME_CHILD, "<col=FF0000>     ---")
			} else {
				player.interfaces().text(MAIN, TARGET_NAME_CHILD, "None")
			}
		}
		
		if (child == WEALTH_VARBIT) {
			
			val value = args[1]
			
			if (value is Int) {
				
				var temp = value
				
				if (temp < 0) temp = 0 else if (temp > 5) temp = 5
			
				//If the varbit value is different from the cached value we add the new value.
				if (player.varps().varbit(WEALTH_VARBIT) != temp) {
					player.varps().varbit(WEALTH_VARBIT, temp)
				}
			}
		}
		
		if (child == LEVEL_RANGE_CHILD) {
			if (hasTarget(player)) {
				
				var distColor = "<col=00ff00>"
				var levelRangeChild = ""
				
				if (!WildernessLevelIndicator.inWilderness(getTargetFor(player)!!.tile())) {
					levelRangeChild = "Safe Zone"
				} else {
					
					val dist = player.tile().distance(player.attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).tile())
					
					if (dist in 15..60) {
						distColor = "<col=CD853F>"
					} else if (dist > 60) {
						distColor = "<col=0174DF>"
					}
					
					levelRangeChild = resolveWildRange(player.attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET))
				}
				
				player.interfaces().text(MAIN, LEVEL_RANGE_CHILD, "$distColor $levelRangeChild, Cmb ${player.attrib<Player>(AttributeKey.BOUNTY_HUNTER_TARGET).skills().combatLevel()}")
			} else {
				player.interfaces().text(MAIN, LEVEL_RANGE_CHILD, "")
			}
		}
		
		if (child == EMBLEM_VARBIT) {
			
			val value = args[1]
			
			if (value is Int) {
				
				var temp = value
				
				if (temp < 0) temp = 0 else if (temp > 10) temp = 10
				
				if (player.varps().varbit(EMBLEM_VARBIT) != temp) {
					player.varps().varbit(EMBLEM_VARBIT, temp)
				}
			}
		}
	}
	
	/**
	 * Refreshes all widgets on the interface, if needed.
	 */
	fun displayInformationFor(player: Player) {
		val wealthIcon = resolveWealthRisk(getTargetFor(player)!!)
		val emblemIcon = resolveHighestEmblem(getTargetFor(player)!!)
		
		updateWidget(player, TARGET_NAME_CHILD)
		updateWidget(player, WEALTH_VARBIT, wealthIcon)
		updateWidget(player, EMBLEM_VARBIT, emblemIcon)
		updateWidget(player, LEVEL_RANGE_CHILD)
	}
	
	/**
	 * Performs a hard reset of displaying the interface.
	 */
	fun hardResetInterface(player: Player) {
		updateWidget(player, TARGET_NAME_CHILD)
		updateWidget(player, WEALTH_VARBIT, 0)
		updateWidget(player, EMBLEM_VARBIT, 0)
		updateWidget(player, LEVEL_RANGE_CHILD)
	}
	
	/**
	 * Applies the penalty to the player excluding them from receiving a target.
	 * Sets the appropriate timers and then updates the widget.
	 */
	@Deprecated("Bounty hunter penalty not desired anymore therefore we just apply a 1 minute cooldown to skipping targets.")
	fun applyPenalty(player: Player) {
		
		player.timers().register(TimerKey.BOUNTY_HUNTER_PENALTY, 1_500)
		player.putattrib(AttributeKey.BOUNTY_HUNTER_TARGET_SKIPS, 0)
		updateWidget(player, LEVEL_RANGE_CHILD)
		
		player.message("<col=FF0000>You have skipped a target too many times and are now placed under a temporary penalty.")
	}
	
	/**
	 * Resolves the player's risk value and returns the proper ordinal.
	 */
	fun resolveWealthRisk(player: Player): Int {
		val inventoryTotal = player.inventory().valueOf()
		val equipmentTotal = player.equipment().valueOf()
		val riskVal = (inventoryTotal + equipmentTotal)
		
		if (riskVal in 5_000..10_000) {
			
			return 2
		} else if (riskVal in 10_001..20_000) {
			
			return 3
		} else if (riskVal in 20_001..40_000) {
			
			return 4
		} else if (riskVal >= 40_001) {
			
			return 5
		}
		
		return 1
	}
	
	/**
	 * Resolves the target's wilderness level range.
	 */
	fun resolveWildRange(p: Player): String {
		
		var currentLvl = WildernessLevelIndicator.wildernessLevel(p.tile())
		var min = currentLvl - 3
		
		if (min < 1) min = 1
		
		var max = currentLvl + 3
		
		if (max > 56) max = 56
		
		return ("Lvl $min-$max")
	}
	
	/**
	 * Resolves the player's best emblem then returns the varbit value to send.
	 */
	fun resolveHighestEmblem(p: Player): Int {
		val emblem = BountyHunterEmblem.resolveBestEmblem(p.inventory())
		if (emblem != BountyHunterEmblem.NONE) {
			
			return emblem.ordinal
		}
		
		return 0
	}
}