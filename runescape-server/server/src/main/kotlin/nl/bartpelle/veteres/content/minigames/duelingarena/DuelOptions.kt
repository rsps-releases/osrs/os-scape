package nl.bartpelle.veteres.content.minigames.duelingarena

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.get
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking.duelOffer
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking.duelPartner
import nl.bartpelle.veteres.content.minigames.duelingarena.Staking.extra_options_apply
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.EquipSlot
import nl.bartpelle.veteres.model.entity.player.Skills
import nl.bartpelle.veteres.model.item.ItemContainer
import nl.bartpelle.veteres.net.message.game.command.*
import nl.bartpelle.veteres.script.TimerKey
import nl.bartpelle.veteres.util.Varbit
import nl.bartpelle.veteres.util.Varp

/**
 * Created by Jak on 10/1/2016.
 */

object DuelOptions {
	
	@JvmField val NO_FORFEIT = 0
	@JvmField val NO_MOVEMENT = 1
	@JvmField val NO_WEAPON_SWITCH = 2
	@JvmField val SHOW_INVENTORIES = 3
	@JvmField val NO_RANGED = 4
	@JvmField val NO_MELEE = 5
	@JvmField val NO_MAGIC = 6
	@JvmField val NO_DRINKS = 7
	@JvmField val NO_FOOD = 8
	@JvmField val NO_PRAYER = 9
	@JvmField val OBSTACLES = 10
	@JvmField val FUN_WEAPONS = 12
	@JvmField val NO_SPK_ATK = 13
	
	@JvmField val EQUIPMENT_BUTTONS = arrayOf(69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79)
	
	@JvmField val EQUIPMENT_VARBITS = arrayOf(Varbit.HELM_SLOT, Varbit.CAPE_SLOT, Varbit.AMULET_SLOT,
			Varbit.WEAPON_SLOT, Varbit.BODY_SLOT, Varbit.SHIELD_SLOT, Varbit.LEGS_SLOT,
			Varbit.GLOVES_SLOT, Varbit.BOOTS_SLOT, Varbit.RING_SLOT, Varbit.AMMO_SLOT)
	
	@JvmField val SLOTS_TO_VARP_MAP = arrayOf(EquipSlot.HEAD, EquipSlot.CAPE, EquipSlot.AMULET,
			EquipSlot.WEAPON, EquipSlot.BODY, EquipSlot.SHIELD, EquipSlot.LEGS, EquipSlot.HANDS, EquipSlot.FEET, EquipSlot.RING, EquipSlot.AMMO)
	
	
	//Toggling dueling options..
	@JvmStatic @Suspendable fun toggleOptions(it: Script, button: Int) {
		val player = it.player()
		val partner = player.duelPartner() ?: return
		
		if (!player.interfaces().visible(482) || !partner.interfaces().visible(482) || Staking.in_duel(player) || Staking.in_duel(partner)) {
			return
		}
		
		var shift = 0
		
		when (button) {
			37, 50 -> shift = NO_FORFEIT
			38, 51 -> shift = NO_MOVEMENT
			39, 52 -> shift = NO_WEAPON_SWITCH
			40, 53 -> shift = SHOW_INVENTORIES
			41, 54 -> shift = NO_RANGED
			42, 55 -> shift = NO_MELEE
			43, 56 -> shift = NO_MAGIC
			44, 57 -> shift = NO_DRINKS
			45, 58 -> shift = NO_FOOD
			46, 59 -> shift = NO_PRAYER
			47, 60 -> shift = OBSTACLES
			48, 61 -> shift = FUN_WEAPONS
			49, 62 -> shift = NO_SPK_ATK
		}
		
		//Additional rule checks
		when (shift) {
			NO_RANGED -> {
				if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_WEAPON_SWITCH)) {
					player.message("You can't restrict attack types and have no weapon switching.")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				} else if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MELEE) &&
						DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC)) {
					player.message("You can't have no melee, no magic, no ranged, how would you fight?")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				}
			}
			NO_MAGIC -> {
				if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_WEAPON_SWITCH)) {
					player.message("You can't restrict attack types and have no weapon switching.")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				} else if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED) &&
						DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MELEE)) {
					player.message("You can't have no melee, no magic, no ranged, how would you fight?")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				}
			}
			NO_MELEE -> {
				if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_WEAPON_SWITCH)) {
					player.message("You can't restrict attack types and have no weapon switching.")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				} else if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED) &&
						DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC)) {
					player.message("You can't have no melee, no magic, no ranged, how would you fight?")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				}
			}
			FUN_WEAPONS -> {
				player.message("The Fun Weapons rule is currently unavailable.")
				player.varps().sync(Varp.STAKE_SETTINGS)
				return
			}
			NO_MOVEMENT -> {
				if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.OBSTACLES)) {
					player.message("You can't have no movement in an arena with obstacles.")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				}
			}
			OBSTACLES -> {
				if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MOVEMENT)) {
					player.message("You can't have obstacles if you want no movement.")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				}
			}
			NO_WEAPON_SWITCH -> {
				if (DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MELEE) ||
						DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_MAGIC) ||
						DuelOptions.ruleToggledOnNotInArena(player, DuelOptions.NO_RANGED)) {
					player.message("You can't restrict attack types and have no weapon switching.")
					player.varps().sync(Varp.STAKE_SETTINGS)
					return
				}
			}
		}
		
		val relativeValue = Math.pow(2.0, shift.toDouble()).toInt()
		val current = player.varps().varp(Varp.STAKE_SETTINGS)
		
		if (current and relativeValue == relativeValue)
			player.varps().varp(Varp.STAKE_SETTINGS, current.xor(relativeValue))
		else
			player.varps().varp(Varp.STAKE_SETTINGS, current.or(relativeValue))
		
		optionChangedWarning(player, button)
	}
	
	//Toggling dueling equipment restrictions..
	@JvmStatic @Suspendable fun toggleArmour(it: Script, button: Int) {
		val player = it.player()
		val partner = player.duelPartner() ?: return
		
		if (!player.interfaces().visible(482) || !partner.interfaces().visible(482) || Staking.in_duel(player) || Staking.in_duel(partner)) {
			player.interfaces().closeMain()
			return
		}
		
		for (i in 0..EQUIPMENT_BUTTONS.size - 1) {
			if (EQUIPMENT_BUTTONS[i] == button) {
				val equipmentVarpID: Int = EQUIPMENT_VARBITS[i]
				val current = player.varps().varbit(equipmentVarpID)
				
				player.varps().varbit(equipmentVarpID, if (current == 1) 0 else 1)
				partner.varps().varbit(equipmentVarpID, player.varps().varbit(equipmentVarpID))
			}
		}
		
		optionChangedWarning(player, button)
	}
	
	//Handles when an options has been changed..
	@JvmStatic @Suspendable fun optionChangedWarning(player: Player, button: Int) {
		val partner = player.duelPartner() ?: return
		player.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		partner.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		
		val updated = player.varps().varp(Varp.STAKE_SETTINGS)
		partner.varps().varp(Varp.STAKE_SETTINGS, updated)
		
		arrayOf(player, partner).forEach { staker ->
			//Do the current options match our players previous duel?
			val last = staker.attribOr<Int>(AttributeKey.PREVIOUS_DUEL_SETTINGS, 0)
			if (staker.varps().varp(Varp.STAKE_SETTINGS) == last) {
				staker.write(InterfaceVisibility(482, 117, false))
			} else {
				staker.write(InterfaceVisibility(482, 117, true))
			}
			
			//Do the current options match our players preset?
			val preset = staker.attribOr<Int>(AttributeKey.SAVED_DUEL_SETTINGS, 0)
			if (staker.varps().varp(Varp.STAKE_SETTINGS) == preset) {
				staker.write(InterfaceVisibility(482, 116, false))
			} else {
				staker.write(InterfaceVisibility(482, 116, true))
			}
			
			staker.invokeScript(1441, 31588458, 5)
		}
		
		// Delay before you can accept.
		player.timers().extendOrRegister(TimerKey.DUEL_ACCEPT_DELAY, 6)
		partner.timers().extendOrRegister(TimerKey.DUEL_ACCEPT_DELAY, 6)
		
		// Flash exclamation mark
		// Only values 51-60 should be applied. Values in the 40s range creates the graphical bug. See issue #866
		partner.write(InvokeScript(968, 482.shl(16) + (if (button < 50) button + 13 else button), -1))
		
		// Notify opponent..
		partner.message("Duel Option change - ${toggledMessage(button, player)}")
		partner.write(InterfaceText(482, 107, "<col=ff0000>An option has changed - check before accepting!"))
		player.write(InterfaceText(482, 107, ""))
	}
	
	fun toggledMessage(button: Int, player: Player): String {
		when (button) {
			37, 50 -> return "Forfeit ${toggledOnOrOffRules(NO_FORFEIT, player)}"
			38, 51 -> return "No Movement ${toggledOnOrOffRules(NO_MOVEMENT, player)}"
			39, 52 -> return "No Weapon Switching ${toggledOnOrOffRules(NO_WEAPON_SWITCH, player)}"
			40, 53 -> return "Show Inventories ${toggledOnOrOffRules(SHOW_INVENTORIES, player)}"
			41, 54 -> return "No Ranged ${toggledOnOrOffRules(NO_RANGED, player)}"
			42, 55 -> return "No Melee ${toggledOnOrOffRules(NO_MELEE, player)}"
			43, 56 -> return "No Magic ${toggledOnOrOffRules(NO_MAGIC, player)}"
			44, 57 -> return "No Drinks ${toggledOnOrOffRules(NO_DRINKS, player)}"
			45, 58 -> return "No Food ${toggledOnOrOffRules(NO_FOOD, player)}"
			46, 59 -> return "No Prayer ${toggledOnOrOffRules(NO_PRAYER, player)}"
			47, 60 -> return "Obstacles ${toggledOnOrOffRules(OBSTACLES, player)}"
			48, 61 -> return "Fun weapons ${toggledOnOrOffRules(FUN_WEAPONS, player)}"
			49, 62 -> return "No Special attacks ${toggledOnOrOffRules(NO_SPK_ATK, player)}"
			69 -> return "Disable Head Slot ${toggledOnOrOffArmour(player, button)}"
			70 -> return "Disable Back Slot ${toggledOnOrOffArmour(player, button)}"
			71 -> return "Disable Neck Slot ${toggledOnOrOffArmour(player, button)}"
			72 -> return "Disable Right hand Slot ${toggledOnOrOffArmour(player, button)}"
			73 -> return "Disable Torso Slot ${toggledOnOrOffArmour(player, button)}"
			74 -> return "Disable Left hand slot ${toggledOnOrOffArmour(player, button)}"
			75 -> return "Disable Leg Slot ${toggledOnOrOffArmour(player, button)}"
			76 -> return "Disable Hand Slot ${toggledOnOrOffArmour(player, button)}"
			77 -> return "Disable Feet Slot ${toggledOnOrOffArmour(player, button)}"
			78 -> return "Disable Ring Slot ${toggledOnOrOffArmour(player, button)}"
			79 -> return "Disable Ammo Slot ${toggledOnOrOffArmour(player, button)}"
			111, 113 -> return "Opponent's last duel options loaded!"
			112, 115 -> return "Opponent's preset options loaded!"
			else -> return ""
		}
	}
	
	fun toggledOnOrOffRules(shift: Int, player: Player): String {
		val current = player.varps().varp(Varp.STAKE_SETTINGS)
		val relativeValue = Math.pow(2.0, shift.toDouble()).toInt()
		
		if (current and relativeValue == relativeValue)
			return "ON!"
		else return "OFF!"
	}
	
	fun toggledOnOrOffArmour(player: Player, button: Int): String {
		var string = ""
		
		for (i in 0..EQUIPMENT_BUTTONS.size - 1) {
			// If the button clicked matches that from this array, use that index to grab the varpbit ID from the 2nd array.
			if (EQUIPMENT_BUTTONS[i] == button) {
				val equipmentVarpID: Int = EQUIPMENT_VARBITS[i]
				val current = player.varps().varbit(equipmentVarpID)
				
				val partner = player.duelPartner()
				
				if ((button == 72 || button == 74) && current == 1) {
					player.message("Beware: You won't be able to use two-handed weapons such as bows.")
					partner!!.message("Beware: You won't be able to use two-handed weapons such as bows.")
				}
				
				if (current == 1)
					string = "ON!"
				else
					string = "OFF!"
			}
		}
		return string;
	}
	
	// Use as Stake.rule_on(player, Stake.NO_MELEE)
	@JvmStatic fun ruleToggledOn(player: Player, shift: Int): Boolean {
		// Double check we're in the arena!
		if (!player.attribOr<Boolean>(AttributeKey.IN_STAKE, false)) {
			return false
		}
		return ruleToggledOnNotInArena(player, shift)
	}
	
	// Used STRICTLY ONLY for pre-starting duel checks for SELECTION STAKES (whip/dds only) NOT FOR GENERAL RULE CHECKS MID FIGHT
	@JvmStatic fun ruleToggledOnNotInArena(player: Player, shift: Int): Boolean {
		val current = player.varps().varp(Varp.STAKE_SETTINGS)
		val relativeValue = Math.pow(2.0, shift.toDouble()).toInt()
		return current and relativeValue == relativeValue
	}
	
	// Open the duel screen with a partner.
	fun initiateDuel(player: Player, partner: Player) {
		if (player.locked() || player.hp() < 1 || partner.locked() || partner.hp() < 1 || player.dead()
				|| partner.dead() || player == partner || player.id() == partner.id()
				|| Staking.in_duel(player) || Staking.in_duel(partner)) {
			player.message("You can't start a stake right now.")
			return
		}
		
		player.timers().cancel(TimerKey.DUEL_INVITATION_OPEN) // Avoid reopening a duel from now on.
		player.putattrib(AttributeKey.DUEL_PARTNER, partner.id())
		
		// States
		player.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		partner.putattrib(AttributeKey.DUEL_ACCEPTED, false)
		
		player.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, -2)
		partner.putattrib(AttributeKey.DUEL_EXTRA_TYPE_SELECTION, -2)
		
		player.clearattrib(AttributeKey.DUEL_ANTISCAM_WARRNING_STAGE)
		partner.clearattrib(AttributeKey.DUEL_ANTISCAM_WARRNING_STAGE)
		
		// Reset duel settings
		player.varps().varp(286, 0)
		partner.varps().varp(286, 0)
		
		// Clear the item containers
		Staking.returnStakedItems(player)
		Staking.returnStakedItems(partner)
		
		// 07 stuff
		player.interfaces().sendMain(482) // main
		// player.interfaces().sendInventory(109) // inventory
		
		player.write(InvokeScript(209, partner.name())) // opp name
		
		player.write(InterfaceText(482, 35, "Dueling with: ${partner.name()}")) // opp name
		player.write(InterfaceText(482, 34, "${lowerLevelWarning(player, partner)}Combat level: ${partner.skills().combatLevel()}")) // cb level
		
		//Attack levels
		player.interfaces().text(482, 8, "${partner.skills().level(Skills.ATTACK)}")
		player.interfaces().text(482, 9, "${partner.skills().xpLevel(Skills.ATTACK)}")
		
		//Strengh levels
		player.interfaces().text(482, 12, "${partner.skills().level(Skills.STRENGTH)}")
		player.interfaces().text(482, 13, "${partner.skills().xpLevel(Skills.STRENGTH)}")
		
		//Defence levels
		player.interfaces().text(482, 16, "${partner.skills().level(Skills.DEFENCE)}")
		player.interfaces().text(482, 17, "${partner.skills().xpLevel(Skills.DEFENCE)}")
		
		//Hitpoints levels
		player.interfaces().text(482, 20, "${partner.skills().level(Skills.HITPOINTS)}")
		player.interfaces().text(482, 21, "${partner.skills().xpLevel(Skills.HITPOINTS)}")
		
		//Prayer levels
		player.interfaces().text(482, 24, "${partner.skills().level(Skills.PRAYER)}")
		player.interfaces().text(482, 25, "${partner.skills().xpLevel(Skills.PRAYER)}")
		
		//Range levels
		player.interfaces().text(482, 28, "${partner.skills().level(Skills.RANGED)}")
		player.interfaces().text(482, 29, "${partner.skills().xpLevel(Skills.RANGED)}")
		
		//Magic levels
		player.interfaces().text(482, 32, "${partner.skills().level(Skills.MAGIC)}")
		player.interfaces().text(482, 33, "${partner.skills().xpLevel(Skills.MAGIC)}")
		
		player.interfaces().text(482, 107, "") // Accept status
	}
	
	fun lowerLevelWarning(player: Player, partner: Player): String {
		val myCombat = player.skills().combatLevel()
		val partnerCombat = partner.skills().combatLevel()
		
		val levelDifference = myCombat - partnerCombat
		
		if (levelDifference < -9) {
			return "<col=ff0000>"
		} else if (levelDifference < -6) {
			return "<col=ff3000>"
		} else if (levelDifference < -3) {
			return "<col=ff7000>"
		} else if (levelDifference < 0) {
			return "<col=ffb000>"
		} else if (levelDifference > 9) {
			return "<col=ff00>"
		} else if (levelDifference > 6) {
			return "<col=40ff00>"
		} else if (levelDifference > 3) {
			return "<col=80ff00>"
		} else if (levelDifference > 0) {
			return "<col=c0ff00>"
		} else {
			return "<col=ffff00>"
		}
	}
	
	@JvmStatic fun advanceToSecondScreen(player: Player) {
		val partner = player.duelPartner() ?: return
		
		//Do our player currently have the duel options interface open?
		if (!player.interfaces().visible(482) || !partner.interfaces().visible(482)) {
			return
		}
		
		player.clearattrib(AttributeKey.DUEL_ACCEPTED)
		partner.clearattrib(AttributeKey.DUEL_ACCEPTED)
		
		arrayOf(player, partner).forEach { staker ->
			val partner = staker.duelPartner()!!
			
			staker.write(CloseInterface(staker.interfaces().activeRoot(), staker.interfaces().mainComponent()))
			staker.write(CloseInterface(staker.interfaces().activeRoot(), staker.interfaces().inventoryComponent()))
			
			staker.write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
			
			staker.interfaces().sendMain(481)
			staker.interfaces().sendInventory(109)
			
			staker.interfaces().text(481, 21, "${partner.name()}'s stake:")
			
			staker.write(InvokeScript(149, 7143424, 93, 4, 7, 0, -1, "Stake 1", "Stake 5", "Stake 10", "Stake All", "Stake X"))
			staker.write(InterfaceSettings(109, 0, 0, 27, 1086))
			
			staker.write(InvokeScript(149, 31522834, 134, 4, 7, 0, 31522835, "Remove 1", "Remove 5", "Remove 10", "Remove All", "Remove X"))
			staker.write(InterfaceSettings(481, 18, 0, 27, 1086))
			
			staker.write(InvokeScript(158, 31522842, 134, 4, 7, 0, 31522843, "", "", "", "", "", 1))
			staker.write(InterfaceSettings(481, 26, 0, 27, 1024))
			
			
			staker.write(SetItems(134, -1, 64168, staker.duelOffer())) // Ours
			staker.write(SetItems(134, -2, 60937, partner.duelOffer())) // Opponents
			staker.write(SetItems(93, -1, 64209, staker.inventory())) // Our inventory
			
			staker.write(InvokeScript(158, 31522842, 134, 4, 7, 0, 31522843, "", "", "", "", "", 1))
			staker.write(InvokeScript(1449, 31522842))
			
			if (DuelOptions.ruleToggledOnNotInArena(staker, DuelOptions.SHOW_INVENTORIES)) {
				staker.write(InterfaceVisibility(481, 10, false))
				staker.write(InterfaceVisibility(481, 29, false))
				staker.write(InterfaceVisibility(481, 30, true))
				staker.write(InterfaceVisibility(481, 6, true))
				
				//staker.write(InvokeScript(158, 31522845, 93, 4, 7, 0, -1, "", "", "", "", "", 0))
				staker.write(InterfaceSettings(481, 29, 0, 27, 1024))
//				staker.invokeScript(1447)
				staker.write(InterfaceSettings(481, 30, 0, 13, 1024))
				
				updatePartnersInventories(staker)
			} else {
				staker.write(InterfaceVisibility(481, 10, true))
				staker.write(InterfaceVisibility(481, 6, false))
			}
		}
	}
	
	@JvmStatic fun advanceToThirdScreen(player: Player) {
		val partner = player.duelPartner() ?: return
		
		//Do our player currently have the duel options interface open?
		if (!player.interfaces().visible(481) || !partner.interfaces().visible(481)) {
			return
		}
		
		player.clearattrib(AttributeKey.DUEL_ACCEPTED)
		partner.clearattrib(AttributeKey.DUEL_ACCEPTED)
		
		val selected_duel_applies = extra_options_apply(player)
		
		arrayOf(player, partner).forEach { staker ->
			val partner = staker.duelPartner()!!
			staker.write(CloseInterface(staker.interfaces().activeRoot(), staker.interfaces().inventoryComponent()))
			
			staker.write(InvokeScript(InvokeScript.SETVARCS, -1, -1))
			
			staker.interfaces().sendMain(476)
			
			if (selected_duel_applies) {
				staker.message("<col=7F00FF>This duel qualifies for a selection type-duel! You can choose normal / dds+whip / dds+tent after accepting!")
			}
			
			val otherSkills = partner.skills()
			staker.write(InterfaceVisibility(476, 77, false))
			staker.interfaces().text(476, 72,
					"${partner.name()}<br>Combat level: ${lowerLevelWarning(staker, partner)}</col>" +
							"<br>Attack: ${otherSkills[Skills.ATTACK]}/${otherSkills.xpLevel(Skills.ATTACK)}<br>" +
							"Defence: ${otherSkills[Skills.DEFENCE]}/${otherSkills.xpLevel(Skills.DEFENCE)}<br>" +
							"Strength: ${otherSkills[Skills.STRENGTH]}/${otherSkills.xpLevel(Skills.STRENGTH)}<br>" +
							"Hitpoints: ${otherSkills[Skills.HITPOINTS]}/${otherSkills.xpLevel(Skills.HITPOINTS)}<br>" +
							"Prayer: ${otherSkills[Skills.PRAYER]}/${otherSkills.xpLevel(Skills.PRAYER)}<br>" +
							"Ranged: ${otherSkills[Skills.RANGED]}/${otherSkills.xpLevel(Skills.RANGED)}<br>" +
							"Magic: ${otherSkills[Skills.MAGIC]}/${otherSkills.xpLevel(Skills.MAGIC)}"
			)
			
			staker.write(SetItems(-1, 64168, 134, staker.duelOffer()))
			staker.write(SetItems(-2, 60937, 134, partner.duelOffer()))
		}
	}
	
	fun updatePartnersInventories(staker: Player) {
		val partner = staker.duelPartner()!!
		staker.invokeScript(1452, *inventoryToArray(partner.inventory()))
//		staker.invokeScript(1447, *inventoryToArray(partner.equipment()))
		partner.invokeScript(1452, *inventoryToArray(staker.inventory()))
//		partner.invokeScript(1447, *inventoryToArray(staker.equipment()))
	}
	
	fun inventoryToArray(itemContainer: ItemContainer): Array<Int?> {
		val arr = kotlin.arrayOfNulls<Int>(itemContainer.size())
		
		for (i in 0..arr.size - 1) {
			arr[i] = itemContainer[i]?.id() ?: -1
		}
		
		return arr
	}
	
}
