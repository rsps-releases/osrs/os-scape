package nl.bartpelle.veteres.content.areas.tzhaar

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.contains
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Mack on 8/10/2017.
 */
object TzHaarKet {
	
	const val KET = 2187
	
	@JvmStatic @ScriptMain fun register(sr: ScriptRepository) {
		sr.onNpcOption1(KET, @Suspendable {
			it.chatNpc("You need help JalYt-Mej-Xo-${it.player().name()}?", KET)
			
			when (it.options("How about a max stack of gold?", if (!it.player().attribOr<Boolean>(AttributeKey.INFERNAL_ACCESS, false)) "I have this fire cape that I got from defeating TzTok-Jad." else "What are you guarding?", "No I'm fine thanks.")) {
				1 -> {
					it.chatPlayer("How about a max stack of gold?")
					it.chatNpc("Do not waste me time JalYt.", KET)
					it.chatPlayer("Don't be such a stubborn TzHaar thing...")
					it.chatNpc("What did JalYt say?", KET)
					it.chatPlayer("What? Oh, nothing!")
				}
				2 -> {
					if (!it.player().attribOr<Boolean>(AttributeKey.INFERNAL_ACCESS, false)) unlock_city(it) else what_are_you_guarding(it)
				}
				3 -> {
					it.chatPlayer("No thanks I'm fine.")
				}
			}
		})
	}
	
	@Suspendable fun what_are_you_guarding(script: Script) {
		
		script.chatPlayer("What are you guarding?")
		script.chatNpc("Ah, trusted and most capable JalYt, this is the city of Mor Ul Rek. It has grand central Inferno, our birthing pool. Go! See for yourself JalYt-Mej-Xo-${script.player().name()}.", KET)
		
		when (script.options("Thanks, Goodbye.", "Thanks, JalYt-Mej-Xo-${script.player().name()} over and out.")) {
			1 -> {
				script.chatPlayer("Thanks, Goodbye.")
			}
			2 -> {
				script.chatPlayer("Thanks, JalYt-Mej-Xo-${script.player().name()} over and out.")
				script.chatNpc("Haha funny JalYt try to speak language, why do you put that accent on when attempting it?", KET)
				script.chatPlayer("I don't know... maybe it helps you to understand what I'm saying. See you around.")
			}
		}
	}
	
	@Suspendable fun unlock_city(script: Script) {
		script.chatPlayer("I have this fire cape that I got from defeating TzTok-Jad.")
		
		if (script.player().inventory().contains(6570)) {
			script.itemBox("You hold out your fire cape and show it to TzHaar-Ket.", 6570)
			script.chatNpc("That is most impressive JalYt-Ket-${script.player().name()}.", KET)
			script.chatPlayer("Can't I go through now?")
			script.chatNpc("I suppose so, I'll grant you access to Mor Ul Rek. The guards will open the gates for you, you are first JalYt to pass these gates!", KET)
			script.player().putattrib(AttributeKey.INFERNAL_ACCESS, true)
		} else {
			script.chatNpc("Do not waste me time JalYt. You do not have cape.", KET)
		}
	}
}