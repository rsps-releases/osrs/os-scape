package nl.bartpelle.veteres.content.treasuretrails

import nl.bartpelle.veteres.model.entity.Npc

/**
 * Created by Situations on 2016-11-20.
 */

object TreasureTrailMonsters {
	
	val EASY_MONSTERS = arrayListOf("Man", "H.A.M. Member", "Minotaur", "Woman", "Goblin", "Al Kharid warrior",
			"Rock Crab", "Sand Crab", "Barbarian", "Cave slime", "Banshee", "Icefiend", "Thug", "Farmer", "Ocga")
	
	val MEDIUM_MONSTERS = arrayListOf("Abyssal Leechs", "Abyssal Walker", "Basilisk", "Black Guard",
			"Black Guard Berserker", "Catablepon", "Cockatrice", "Dagannoth", "Giant rock crab", "Giant Sea Snake",
			"Guard", "Harpie bug swarm", "Ice warrior", "Ice giant", "Jogre", "Jungle horror",
			"King Sand Crab", "Leech", "Mammoth", "Market Guard", "Molanisk", "Mummy", "Paladin",
			"Pyrefiend", "Sea Snake Hatchling", "Sea Snake Young", "Skeleton", "Skeleton hero",
			"Speedy Keith", "Tribesman", "Vampyre", "Vampyre Juvinate", "Wallasalki", "Werewolf")
	
	val HARD_MONSTERS = arrayListOf("Aberrant spectre", "Abyssal demon", "Ankou", "Black dragon", "Black demon",
			"Bloodveld", "Blue dragon", "Bronze dragon", "Brutal green dragon", "Cave horror", "Cave kraken",
			"Chaos Elemental", "Chaos Fanatic", "Commander Zilyana", "Cyclops", "Dagannoth Prime", "Dagannoth Rex",
			"Dagannoth Supreme", "Dark beast", "Demonic Gorilla", "Elf warrior", "Gargoyle", "General Graardor",
			"Greater demon", "Green dragon", "Hellhound", "Iron dragon", "Jelly", "Kalphite Queen", "King Black Dragon",
			"Kree'arra", "K'ril Tsutsaroth", "Kurask", "Mithril dragon", "Nechryael", "Red dragon", "Spiritual mage",
			"Spiritual ranger", "Spiritual warrior", "Steel dragon", "Suqah", "Terror Dog", "Tortured Gorilla",
			"Turoth", "Tyras guard", "Waterfiend")
	
	val ELITE_MONSTERS = arrayListOf(
			"Abyssal demon", "Abyssal Sire", "Black dragon", "Barrows chest", "Callisto", "Cave kraken", "Cerberus",
			"Chaos Elemental", "Dagannoth Prime", "Dagannoth Rex", "Dagannoth Supreme", "Dark beast", "Demonic Gorilla",
			"General Graardor", "Giant Mole", "Kalphite queen", "King black dragon", "Kraken", "Kree'arra", "K'ril Tsutsaroth",
			"Lizardman shaman", "Lava dragon", "Mithril dragon", "Salarin the Twisted", "Skeletal Wyvern", "Smoke devil",
			"Steel dragon", "Thermonuclear smoke devil", "Tortured Gorilla", "Venenatis", "Vet'ion", "Zilyana", "Zulrah")
	
	fun clueScrollForMonster(npc: Npc): TreasureTrailClueLevel {
		EASY_MONSTERS.forEach { easyMonster ->
			if (npc.def().name == easyMonster) {
				return TreasureTrailClueLevel.EASY
			}
		}
		MEDIUM_MONSTERS.forEach { mediumMonster ->
			if (npc.def().name == mediumMonster) {
				return TreasureTrailClueLevel.MEDIUM
			}
		}
		HARD_MONSTERS.forEach { hardMonster ->
			if (npc.def().name == hardMonster) {
				return TreasureTrailClueLevel.HARD
			}
		}
		ELITE_MONSTERS.forEach { eliteMonster ->
			if (npc.def().name == eliteMonster) {
				return TreasureTrailClueLevel.ELITE
			}
		}
		
		return TreasureTrailClueLevel.UNKNOWN
	}
	
	
}