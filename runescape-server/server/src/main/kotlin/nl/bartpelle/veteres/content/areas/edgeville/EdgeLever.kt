package nl.bartpelle.veteres.content.areas.edgeville

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.animate
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.player.Privilege
import nl.bartpelle.veteres.model.map.MapObj
import nl.bartpelle.veteres.script.ScriptRepository
import nl.bartpelle.veteres.script.TimerKey

/**
 * Created by Situations on 11/9/2015.
 */
object EdgeLever {
	
	@JvmStatic @ScriptMain fun register(repo: ScriptRepository) {
		
		repo.onObject(26761, s@ @Suspendable {
			// EDGE TO DESERTED KEEP (54 wild)
			val obj: MapObj = it.player().attrib(AttributeKey.INTERACTION_OBJECT)
			val player = it.player()
			it.player().faceObj(obj)
			
			if (it.player().timers().has(TimerKey.TELEBLOCK)) {
				it.player().teleblockMessage()
				return@s
			}
			if (player.privilege() != Privilege.ADMIN) {
				if (player.inventory().count(6685) > 18) {
					player.message("" + player.inventory().count(6685) + " brews is a little excessive don't you think?")
					return@s
				}
			} else {
				player.message("As an admin you bypass pk-tele restrictions.")
			}
			it.player().lockNoDamage()
			it.delay(1)
			it.animate(2140)
			it.message("You pull the lever...")
			it.player().world().server().scriptExecutor().executeScript(player.world(), @Suspendable { s ->
				s.delay(1)
				val world = player.world()
				val uplever: MapObj? = world.objByType(4, obj.tile())
				if (uplever != null) {
					val spawned = MapObj(obj.tile(), 88, obj.type(), obj.rot())
					world.spawnObj(spawned)
					s.delay(5)
					world.removeObjSpawn(spawned)
					world.spawnObj(obj)
					// Possible bug identified: guessing by not re-spawning the default region object (the 'up' lever) by force sometimes
					// players will be out of sync.. the up removal of the down lever doesn't then re-send a pre-existing cache object back in that position.
				}
			})
			it.delay(2)
			it.animate(714)
			it.player().graphic(111, 110, 0)
			it.delay(4)
			player.teleport(3154, 3924)
			it.animate(-1)
			it.player().unlock()
			it.message("...And teleport into the wilderness.")
		})
	}
	
}
