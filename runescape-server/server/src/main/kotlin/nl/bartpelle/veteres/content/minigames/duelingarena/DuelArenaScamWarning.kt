package nl.bartpelle.veteres.content.minigames.duelingarena

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.blankmessagebox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player

/**
 * Created by Jak on 06/09/2016.
 */
class DuelArenaScamWarning(var player: Player, var armCount: Int, var rulesCount: Int) : Function1<Script, Unit> {
	
	@Suspendable override fun invoke(it: Script) {
		it.messagebox3("This stake has " + armCount + " armour slots disabled and " + rulesCount + " rules. It <col=FF0000>does not</col> meet" +
				" the requirements for a custom stake (such as tentacle only). Are you sure you want to do this stake?")
		when (it.options("Yes, I did these settings on purpose.", "No, I want to check the rules again.")) {
			1 -> {
				val partner: Player? = Staking.partnerFor(player)
				if (partner == null) {
					player.message("Unable to find partner.")
					player.interfaces().close(162, 550) // chatbox close
					player.interfaces().closeMain()
				} else {
					val stage = partner.attribOr<Int>(AttributeKey.DUEL_ANTISCAM_WARRNING_STAGE, -1)
					if (stage == 1) {
						player.interfaces().close(162, 550) // chatbox close
						partner.interfaces().close(162, 550) // chatbox close
						Staking.start_duel(player, partner)
					} else if (stage == -1) { // They haven't chosen yet.
						player.putattrib(AttributeKey.DUEL_ANTISCAM_WARRNING_STAGE, 1)
						it.blankmessagebox("Waiting for opponent...")
					} else {
						player.message(partner.name() + " didn't want to start this stake.")
						player.interfaces().close(162, 550) // chatbox close
						player.interfaces().closeMain()
					}
				}
			}
			2 -> {
				val partner: Player? = Staking.partnerFor(player)
				player.interfaces().closeMain()
				if (partner != null) {
					partner.interfaces().closeMain()
				}
			}
		}
	}
}