package nl.bartpelle.veteres.content.npcs.bosses.wilderness.vetion

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.content.combat.EntityCombat
import nl.bartpelle.veteres.content.combat.PlayerCombat
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.Entity
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.util.CombatStyle

/**
 * Created by Mack on 9/6/2017.
 */

class VetionMinion(vetion: Npc, targetEntity: Entity, counter: Int) {

	companion object {
		@JvmField val script: Function1<Script, Unit> = s@ @Suspendable {

			val minion = it.npc()
			var target = EntityCombat.getTarget(it) ?: return@s

			while(EntityCombat.targetOk(minion, target) && PlayerCombat.canAttack(minion, target)) {
				if (EntityCombat.attackTimerReady(minion) && EntityCombat.canAttackMelee(minion, target, true)) {

					minion.animate(minion.attackAnimation())

					if (EntityCombat.attemptHit(minion, target, CombatStyle.MELEE)) {
						target.hit(minion, minion.world().random(minion.combatInfo().maxhit), 1).combatStyle(CombatStyle.MELEE)
					} else {
						target.hit(minion, 0, 1).combatStyle(CombatStyle.MELEE)
					}

					target.putattrib(AttributeKey.LAST_DAMAGER, minion)
					target.putattrib(AttributeKey.LAST_WAS_ATTACKED_TIME, System.currentTimeMillis())
					minion.putattrib(AttributeKey.LAST_TARGET, target)
					EntityCombat.putCombatDelay(minion, minion.combatInfo().attackspeed)
				}

				EntityCombat.postHitLogic(minion)
				it.delay(1)
				target = EntityCombat.refreshTarget(it) ?: return@s
			}
		}

		@JvmField val death: Function1<Script, Unit> = s@ @Suspendable {

			val vetion = it.npc().attribOr<Npc>(AttributeKey.BOSS_OWNER, null)
			if (vetion != null) {
				//Check for any minions.
				val minList = vetion.attribOr<ArrayList<Npc>>(AttributeKey.MINION_LIST, null)
				if (minList != null) {
					minList.remove(it.npc())
					// All minions dead? Enable damage on vetion again
					if (minList.size == 0) {
						vetion.putattrib(AttributeKey.VETION_HELLHOUND_SPAWNED, false)
					}
				}
			}
		}
	}
	
	/**
	 * The current target inherited from the Vetion boss.
	 */
	var target = targetEntity
	
	/**
	 * Our generated minion spawn.
	 */
	val minion = Npc(6613, vetion.world(), vetion.tile())
	
	val create: Function1<Script, Unit> = s@ @Suspendable {

		minion.putattrib(AttributeKey.BOSS_OWNER, vetion)
		minion.tile(vetion.tile().transform(minion.world().random(3), -1))
		minion.walkRadius(8)
		minion.respawns(false)
		minion.putattrib(AttributeKey.MAX_DISTANCE_FROM_SPAWN, 6)
		vetion.world().registerNpc(minion)
		
		minion.attack(target)
	}
}