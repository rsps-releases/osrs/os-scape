package nl.bartpelle.veteres.content.areas.edgeville.pvpworld.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain
import nl.bartpelle.veteres.content.areas.edgeville.pvpworld.BloodChest
import nl.bartpelle.veteres.content.itemBox
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 2016-12-11.
 */

object Nigel {
	
	val NIGEL = 6093
	val THE_BLOOD_CHEST_BOOK = 9717
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(NIGEL, s@ @Suspendable {
			val player = it.player()
			
			//Has our player started the tutorial?
			if (BloodChest.startedBloodySurvivalMinigame(player)) {
				if (BloodChest.hasRewardKey(player)) {
					it.chatPlayer("Hey, I have a key! What should I do with it?", 588)
					it.chatNpc("Were you listening when I told you? Open the Blood Chest!", NIGEL, 614)
				} else {
					it.chatNpc("Welcome back, ${player.name()}! Did you come to listen to stories of my past?", NIGEL, 588)
					dialogueOptions(it)
				}
			} else {
				tutorialDialogue(it)
			}
			
		})
	}
	
	@Suspendable fun dialogueOptions(it: Script) {
		if (!hasBloodChestBook(it.player())) {
			when (it.options("Yes!", "I lost my book.", "I'll be leaving now.")) {
				1 -> yes(it)
				2 -> lostBook(it)
				3 -> leavingNow(it)
			}
		} else {
			when (it.options("Yes!", "I'll be leaving now.")) {
				1 -> yes(it)
				2 -> leavingNow(it)
			}
		}
	}
	
	@Suspendable fun yes(it: Script) {
		it.chatPlayer("Yes, I'd love to hear a story!", 588)
		it.chatNpc("I'm writing a book about them! You'll have to wait and buy a copy. Did you need anything else?", NIGEL, 588)
		if (!hasBloodChestBook(it.player())) {
			when (it.options("I lost my book.", "I'll be leaving now.")) {
				1 -> lostBook(it)
				2 -> leavingNow(it)
			}
		} else {
			leavingNow(it)
		}
	}
	
	@Suspendable fun lostBook(it: Script) {
		it.chatPlayer("I lost my book.", 614)
		if (it.player().inventory().add(Item(THE_BLOOD_CHEST_BOOK, 1), false).success()) {
			it.chatNpc("Well, it's a good thing I made copies! Here, take another.", NIGEL, 588)
		} else {
			it.chatNpc("Doesn't look like you have room!", NIGEL, 588)
		}
	}
	
	@Suspendable fun leavingNow(it: Script) {
		it.chatPlayer("I'll be leaving now..", 588)
		it.chatNpc("Okay, get out there and get some keys! Best of luck!", NIGEL, 588)
	}
	
	
	@Suspendable fun tutorialDialogue(it: Script) {
		val player = it.player()
		it.chatNpc("Come to Nigel's Blood Emporium! Located just East of Edgev-", NIGEL, 588)
		it.chatNpc("Oh, you're already here! Welcome, welcome, it's good to see you, ${player.name()}.", NIGEL, 588)
		it.chatPlayer("Hello there! It's good to see you... wait, how did you know my name?", 588)
		it.chatNpc("Not important! What is important is that I have a challenge for you. A challenge that involves blood, gore, viscera, conquest, but most importantly... treasure!", NIGEL, 588)
		it.chatNpc("You see, I used to be quite the warrior in my time. Many a foe kneeled before me! I'm remembering it now in perfect clarity.", NIGEL, 588)
		it.chatNpc("The cheering of the spectators, the wind in my hair, the blood on my face, and the feeling of their key in my hand.", NIGEL, 588)
		it.chatPlayer("Their key?", 588)
		it.chatNpc("Their key! You see, beside me lays a chest that takes such a key in exchange for a reward!", NIGEL, 588)
		it.chatPlayer("A reward? I like the sound of that! How do I go about getting one of these keys?", 588)
		it.chatNpc("Oh, that's the easy part! North of me lays a ditch, and beyond that is the wilderness!", NIGEL, 588)
		it.chatNpc("Inside of that wilderness lays some people, and inside of them lays their key. Not... always though.", NIGEL, 588)
		it.chatPlayer("What do you mean by 'not always'?", 588)
		it.chatNpc("Well, you see, not everybody has a key! I've always found that the more they were carrying, the better the chance they'd have one.", NIGEL, 588)
		it.chatNpc("I also noticed they'd seem to drop more frequently when I was risking some of my best armor and weapons myself! The strange things they are.", NIGEL, 588)
		it.chatPlayer("I see. So I have to kill people in the wilderness and the more they're wearing, and the more I'm wearing, the better the chance?", 588)
		it.chatNpc("Exactly! Also... there's one more thing you should know...", NIGEL, 588)
		it.chatPlayer("And that is...?", 588)
		it.chatNpc("Well, each key is cursed. When you pick up a key, everybody else is made aware. You should have seen some of the looks I'd get back in the day!", NIGEL, 588)
		it.chatNpc("But anyways, everybody is made aware that you have a key and for some reason the keys prevent you from teleporting.", NIGEL, 588)
		it.chatPlayer("They what?", 588)
		it.chatNpc("They also make you skulled, and attackable by more than one player. So, in short, everybody knows you have one and you've no easy escape. Be wary in picking one up.", NIGEL, 588)
		it.chatPlayer("Okay, I understand.", 588)
		it.chatNpc("Good! So go get a key for yourself, come back here, and then use it on that chest over there and claim your reward.", NIGEL, 588)
		it.chatNpc("I'm far too old to be doing it myself anymore... Ahh, how it used to be back in the day...", NIGEL, 588)
		player.putattrib(AttributeKey.STARTED_BLOODY_SURVIVAL_MINIGAME, true)
		it.chatNpc("Oh, by the way.. take this in case you forget.", NIGEL, 588)
		
		if (player.inventory().add(Item(THE_BLOOD_CHEST_BOOK, 1), false).success()) {
			it.itemBox("Nigel hands you a book.", THE_BLOOD_CHEST_BOOK)
		} else {
			it.chatNpc("Looks like your inventory is full, talk to me once you have space!", NIGEL, 588)
		}
	}
	
	private fun hasBloodChestBook(player: Player): Boolean {
		if (player.inventory().has(THE_BLOOD_CHEST_BOOK)) return true
		else if (player.bank().has(THE_BLOOD_CHEST_BOOK)) return true
		
		return false
	}
	
}
