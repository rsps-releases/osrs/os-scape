package nl.bartpelle.veteres.content.areas.alkharid.dueling_arena.dialogue

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.skript.Script
import nl.bartpelle.skript.ScriptMain

import nl.bartpelle.veteres.script.ScriptRepository

/**
 * Created by Situations on 3/4/2015.
 */

object Gee {
	
	val GEE = 3261
	
	@JvmStatic @ScriptMain fun register(r: ScriptRepository) {
		r.onNpcOption1(GEE) @Suspendable {
			val random = it.player().world().random(3)
			
			when (random) {
				0 -> {
					it.chatNpc("Hello there, can I help you?", GEE, 567)
					when (it.options("Where am I?", "How are you today?", "Are there any quests I can do here?", "Where can I get a haircut like yours?")) {
						1 -> gee_where_am_I_option(it)
						2 -> gee_how_are_you_option(it)
						3 -> gee_quests_options(it)
						4 -> gee_haircut_option(it)
					}
				}
				1 -> {
					it.chatNpc("Hello there, can I help you?", GEE, 567)
					when (it.options("What's up?", "Are there any quests I can do here?", "Can I buy your stick?")) {
						1 -> gee_whats_up_option(it)
						2 -> gee_quests_options(it)
						3 -> gee_buy_stick_option(it)
					}
				}
				2 -> {
					it.chatNpc("Hello there, can I help you?", GEE, 567)
					when (it.options("Where am I?", "How are you today?", "Are there any quests I can do here?", "Where can I get a haircut like yours?")) {
						1 -> gee_where_am_I_option(it)
						2 -> gee_how_are_you_option(it)
						3 -> gee_quests_options(it)
						4 -> gee_haircut_option(it)
					}
				}
				3 -> {
					it.chatNpc("Hello there, can I help you?", GEE, 567)
					when (it.options("Where am I?", "How are you today?", "Are there any quests I can do here?", "Your show lace is untied.")) {
						1 -> gee_where_am_I_option(it)
						2 -> gee_how_are_you_option(it)
						3 -> gee_quests_options(it)
						4 -> gee_shoe_lace_option(it)
					}
				}
			}
		}
	}
	
	@Suspendable fun gee_whats_up_option(it: Script) {
		it.chatPlayer("What's up?", 554)
		it.chatNpc("I assume the sky is up..", GEE, 554)
		it.chatPlayer("You assume?", 554)
		it.chatNpc("Yeah, unfortunately I don't seem to be able to look up.", GEE, 554)
	}
	
	@Suspendable fun gee_where_am_I_option(it: Script) {
		it.chatPlayer("Where am I?", 554)
		it.chatNpc("This is the town of Lumbridge my friend.", GEE, 605)
		when (it.options("How are you today?", "Do you know of any quests I can do?", "Your show lace is untied.")) {
			1 -> gee_how_are_you_option(it)
			2 -> gee_quests_options(it)
			3 -> gee_shoe_lace_option(it)
			4 -> gee_haircut_option(it)
		}
	}
	
	@Suspendable fun gee_how_are_you_option(it: Script) {
		it.chatPlayer("How are you today?", 567)
		it.chatNpc("Aye, not too bad thank you. Lovely weather in<br>OS-Scape this fine day.", GEE, 568)
		it.chatPlayer("Weather?", 605)
		it.chatNpc("Yes weather, you know.", GEE, 605)
		it.chatNpc("The state or condition of the atmosphere at a time and<br>place, with respect to variables such as temperature,<br>" +
				"moisture, wind velocity, and barometric pressure.", GEE, 556)
		it.chatPlayer("...", 554)
		it.chatNpc("Not just a pretty face eh? Ha ha ha.", GEE, 605)
	}
	
	@Suspendable fun gee_buy_stick_option(it: Script) {
		it.chatPlayer("Can I buy your stick?", 554)
		it.chatNpc("It's not a stick! I'll have you know it's a very powerful<br>staff!", GEE, 615)
		it.chatPlayer("Really? Show me what it can do!", 554)
		it.chatNpc("Um..It's a bit low on power at the moment..", GEE, 610)
		it.chatPlayer("It's a stick isn't it?", 605)
		it.chatNpc("...Ok it's a stick.. But only while I save up for a staff.<br>Zaff in Varrock square sells them in his shop.", GEE, 611)
		it.chatPlayer("Well good luck with that.", 605)
	}
	
	@Suspendable fun gee_haircut_option(it: Script) {
		it.chatPlayer("Where can I get a haircut like yours?", 554)
		it.chatNpc("Yes, it does look like you need a hairdresser.", GEE, 567)
		it.chatPlayer("Oh thanks!", 614)
		it.chatNpc("No problem. The hairdresser in Falador will probably be<br>able to sort you out.", GEE, 606)
		it.chatPlayer("The Lumbridge general store sells useful maps if you<br>don't know the way.", 568)
	}
	
	@Suspendable fun gee_shoe_lace_option(it: Script) {
		it.chatPlayer("Your shoe lace is untied.", 605)
		it.chatNpc("No it's not!", GEE, 614)
		it.chatPlayer("No you're right. I have nothing to back that up.", 605)
		it.chatNpc("Fool! Leave me alone!", GEE, 614)
	}
	
	@Suspendable fun gee_quests_options(it: Script) {
		it.chatPlayer("Do you know of any quests I can do?", 554)
		it.chatNpc("What kind of quest are you looking for?", GEE, 554)
		when (it.options("I fancy a bit of a fight, anything dangerous?", "Something easy please, I'm new here.", "I'm a thinker rather than fighter, anything skill oriented?",
				"I want to do all kinds of things, do you know of anything like that?", "Maybe another time.")) {
			1 -> {
				it.chatPlayer("I fancy a bit of a fight, anything dangerous?", 567)
				it.chatNpc("Hmm.. dangerous you say? What sort of creatures are<br>you looking to fight?", GEE, 555)
				when (it.optionsTitled("Tell me about...", "Big scary demons!", "Vampires!", "Small.. something small would be good.", "Maybe another time.")) {
					1 -> {
						it.chatPlayer("Big scary demons!", 567)
						it.chatNpc("You are a brave soul indeed.", GEE, 605)
						it.chatNpc("Now that you mention it, I heard a rumour about a<br>gypsy in Varrock who is rambling about some kind of<br>greater evil.. sounds demon-like if " +
								"you ask me.", GEE, 556)
						it.chatNpc("Perhaps you could check it out if you are as brave as<br>you say?", GEE, 555)
						it.chatPlayer("Thanks for the tip, perhaps I will.", 567)
					}
					2 -> {
						it.chatPlayer("Vampires!", 567)
						it.chatNpc("Ha ha. I personally don't believe in such things.<br>However, there is a man in Draynor Village who has<br>been scaring the village folk with stories" +
								" of vampires.", GEE, 607)
						it.chatNpc("He's named Morgan and can be found in one of the<br>village houses. Perhaps you could see what the matter<br>is?", GEE, 569)
						it.chatPlayer("Thanks for the tip, perhaps I will.", 567)
					}
					3 -> {
						it.chatPlayer("Small.. something small would be good.", 567)
						it.chatNpc("Small? Small isn't really that dangerous though is it?", GEE, 554)
						it.chatPlayer("Yes it can be! There could be anything from an evil<br>chicken to a poisonous spider. They attack in numbers<br>you know!", 616)
						it.chatNpc("Yes ok, point taken. Speaking of small monsters, I hear<br>old Wizard Mizgog in the wizards' tower has just had<br>all his beads taken by a gang of " +
								"mischievous imps.", GEE, 569)
						it.chatNpc("Sounds like it could be a quest for you?", GEE, 567)
						it.chatPlayer("Perhaps, thanks for the tip.", 567)
					}
					4 -> {
						it.chatPlayer("Maybe another time.", 567)
					}
				}
			}
			2 -> {
				it.chatPlayer("Something easy please, I'm new here.", 567)
				it.chatNpc("I can tell you about plenty of small easy tasks.", GEE, 567)
				it.chatNpc("The Lumbridge cook has been having problems, the<br>Duke is confused over some strange rocks and on top<br>of all that, poor lad Romeo in Varrock has girlfriend<br>" +
						"problems.", GEE, 557)
				when (it.optionsTitled("Tell me about..", "The Lumbridge cook.", "The Duke's strange stones.", "Romeo and his girlfriend.", "Maybe another time.")) {
					1 -> {
						it.chatPlayer("Tell me about the Lumbridge cook.", 554)
						it.chatNpc("It's funny really, the cook would forget his head if it<br>wasn't screwed on. This time he forgot to get<br>ingredients for the Duke's birthday cake. ", GEE, 607)
						it.chatNpc("Perhaps you could help him? You will probably find him<br>in the Lumbridge Castle kitchen.", GEE, 555)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 567)
					}
					2 -> {
						it.chatPlayer("Tell me about the Duke's strange stones.", 567)
						it.chatNpc("Well the Duke of Lumbridge has found a strange stone<br>that no one seems to understand. Perhaps you could<br>help him? You can probably find him upstairs in<br>" +
								"Lumbridge Castle.", GEE, 570)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 567)
					}
					3 -> {
						it.chatPlayer("Tell me about Romeo and his girlfriend please.", 567)
						it.chatNpc("Romeo in Varrock needs help with finding his beloved<br>Juliet, you may be able to help him out. ", GEE, 568)
						it.chatNpc("Unless of course you manage to find Juliet first in<br>which case she has probably lost Romeo.", GEE, 606)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 567)
					}
					4 -> {
						it.chatPlayer("Maybe another time.", 567)
					}
				}
			}
			3 -> {
				it.chatPlayer("I'm a thinker rather than fighter, anything skill<br>orientated?", 568)
				it.chatNpc("Skills play a big part when you want to progress in<br>knowledge throughout OS-Scape. I know of a few skill-<br>related quests that can get you started.", GEE, 556)
				it.chatNpc("You may be able to help out Fred the farmer who is in<br>need of someones crafting expertise.", GEE, 568)
				it.chatNpc("Or, there's always Doric the dwarf who needs an<br>errand running for him?", GEE, 568)
				when (it.optionsTitled("Tell me about..", "Fred the farmer.", "Doric the dwarf.", "Maybe another time.")) {
					1 -> {
						it.chatPlayer("Tell me about Fred the farmer please.", 567)
						it.chatNpc("You can find Fred next to the field of sheep in<br>Lumbridge. Perhaps you should go and speak with him.", GEE, 555)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 567)
					}
					2 -> {
						it.chatPlayer("Tell me about Doric the dwarf.", 567)
						it.chatNpc("Doric the dwarf is located north of Falador. He might<br>be able to help you with smithing. You should speak to<br>him. He may let you use his anvils.", GEE, 569)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 567)
					}
					3 -> {
						it.chatPlayer("Maybe another time.", 567)
					}
				}
			}
			4 -> {
				it.chatPlayer("I want to do all kinds of things, do you know of<br>anything like that?", 568)
				it.chatNpc("Of course I do. OS-Scape is a huge place you know,<br>now let me think...", GEE, 568)
				it.chatNpc("Hetty the witch in Rimmington might be able to offer<br>help in the ways of magical abilities..", GEE, 568)
				it.chatNpc("Also, pirates are currently docked in Port Sarim,<br>Where pirates are, treasure is never far away...", GEE, 568)
				it.chatNpc("Or you could go help out Ernest who got lost in<br>Draynor Manor, spooky place that.", GEE, 568)
				when (it.optionsTitled("Tell me about..", "Hetty the Witch", "Pirate's treasure.", "Earnest and Draynor Manor.", "Maybe another time.")) {
					1 -> {
						it.chatPlayer("Tell me about Hetty the witch.", 567)
						it.chatNpc("Hetty the witch can be found in Rimmington, south of<br>Falador. She's currently working on some new potions.<br>Perhaps you could give her a hand? She " +
								"might be able<br>to offer help with your magical abilities.", GEE, 570)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 569)
					}
					2 -> {
						it.chatPlayer("Tell me about Pirate's Treasure.", 567)
						it.chatNpc("RedBeard Frank in Port Sarim's bar, the Rusty<br>Anchor might be able to tell you about the rumored<br>treasure that is buried somewhere in OS-Scape.", GEE, 569)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 569)
					}
					3 -> {
						it.chatPlayer("Tell me about Ernest please.", 567)
						it.chatNpc("The best place to start would be at the gate to<br>Draynor Manor. There you will find Veronica who will<br>be able to tell you more.", GEE, 569)
						it.chatNpc("I suggest you tread carefully in that place; it's haunted.", GEE, 567)
						it.chatPlayer("Perhaps, thanks for the suggestion.", 569)
					}
					4 -> {
						it.chatPlayer("Maybe another time.", 567)
					}
				}
			}
			5 -> {
				it.chatPlayer("Maybe another time.", 567)
			}
		}
	}
	
}
