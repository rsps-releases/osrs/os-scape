import co.paralleluniverse.fibers.Suspendable
import com.typesafe.config.ConfigFactory
import nl.bartpelle.skript.Script
import nl.bartpelle.veteres.GameServer
import nl.bartpelle.veteres.model.Tile
import nl.bartpelle.veteres.model.entity.Player
import java.io.File
import kotlin.concurrent.thread

object ScriptTest {
	
	@Throws(Exception::class)
	@JvmStatic
	fun main(args: Array<String>) {
		val c = ConfigFactory.systemProperties().withFallback(ConfigFactory.parseFileAnySyntax(File("server.conf")))
		val s = GameServer(c, File(c.getString("server.filestore")))
		s.start()
		
		val p = Player(null, "", s.world(), Tile(3222, 3222), null, null)
		s.world().registerPlayer(p)
		p.executeScript(script)
		
		thread {
			while (true) {
				println("Loop")
				Thread.sleep(5000)
				println("Stop")
				p.stopActions(true)
				Thread.sleep(5000)
				println("Go")
				p.executeScript(script)
			}
		}.start()
	}
	
	@JvmField
	val script: Function1<Script, Unit> = s@ @Suspendable {
		val id = it.player().world().random(1000)
		while (true) {
			println("Script Loop $id")
			it.delay(1)
		}
	}
}


