package droptables

import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.Skills

/**
 * Created by Situations on 4/5/2016.
 */

class ReanimatedMinion : Droptable {
	
	override fun reward(killed: Npc, killer: Player) {
		if (killed.id() == 7018) //Goblin
			killer.skills().__addXp(Skills.PRAYER, 130.0)
		if (killed.id() == 7019) //Monkey
			killer.skills().__addXp(Skills.PRAYER, 182.0)
		if (killed.id() == 7020) //Imp
			killer.skills().__addXp(Skills.PRAYER, 286.0)
		if (killed.id() == 7021) //Minotaur
			killer.skills().__addXp(Skills.PRAYER, 364.0)
		if (killed.id() == 7022) //Scorpion
			killer.skills().__addXp(Skills.PRAYER, 454.0)
		if (killed.id() == 7023) //Bears
			killer.skills().__addXp(Skills.PRAYER, 480.0)
		if (killed.id() == 7024) //Unicorn
			killer.skills().__addXp(Skills.PRAYER, 494.0)
		if (killed.id() == 7025) //Dog
			killer.skills().__addXp(Skills.PRAYER, 520.0)
		if (killed.id() == 7026) //Chaos Druid
			killer.skills().__addXp(Skills.PRAYER, 584.0)
		if (killed.id() == 7027) //Giant
			killer.skills().__addXp(Skills.PRAYER, 650.0)
		if (killed.id() == 7028) //Ogre
			killer.skills().__addXp(Skills.PRAYER, 716.0)
		if (killed.id() == 7029) //Elf
			killer.skills().__addXp(Skills.PRAYER, 754.0)
		if (killed.id() == 7030) //Troll
			killer.skills().__addXp(Skills.PRAYER, 780.0)
		if (killed.id() == 7031) //Horror
			killer.skills().__addXp(Skills.PRAYER, 832.0)
		if (killed.id() == 7032) //Kalphite
			killer.skills().__addXp(Skills.PRAYER, 884.0)
		if (killed.id() == 7033) //Dagannoth
			killer.skills().__addXp(Skills.PRAYER, 936.0)
		if (killed.id() == 7034) //Bloodveld
			killer.skills().__addXp(Skills.PRAYER, 1040.0)
		if (killed.id() == 7035) //TzHaar
			killer.skills().__addXp(Skills.PRAYER, 1104.0)
		if (killed.id() == 7036) //Demon
			killer.skills().__addXp(Skills.PRAYER, 1170.0)
		if (killed.id() == 7037) //Aviansie
			killer.skills().__addXp(Skills.PRAYER, 1234.0)
		if (killed.id() == 7038) //Abyssal Creature
			killer.skills().__addXp(Skills.PRAYER, 1300.0)
		if (killed.id() == 7039) //Dragon
			killer.skills().__addXp(Skills.PRAYER, 1560.0)
	}
}