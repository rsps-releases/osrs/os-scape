package droptables

import nl.bartpelle.veteres.content.areas.burthorpe.warriors_guild.MagicalAnimator
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Bart on 10/6/2015.
 */
class AnimatedArmourDrops : Droptable {
	
	val GUILD_TOKEN = 8851
	
	override fun reward(npc: Npc, player: Player) {
		if (npc.id() == MagicalAnimator.ArmourSets.BRONZE.npc) {
			// setSpawned(player, 0)
			//if (player.world().rollDie(100, 75))
			drop(npc, player, MagicalAnimator.ArmourSets.BRONZE.helm)
			//if (player.world().rollDie(100, 75))
			drop(npc, player, MagicalAnimator.ArmourSets.BRONZE.legs)
			//if (player.world().rollDie(100, 75))
			drop(npc, player, MagicalAnimator.ArmourSets.BRONZE.body)
			drop(npc, player, Item(GUILD_TOKEN, 5))
		} else if (npc.id() == MagicalAnimator.ArmourSets.IRON.npc) {
			//  setSpawned(player, 0)
			//if (player.world().rollDie(100, 85))
			drop(npc, player, MagicalAnimator.ArmourSets.IRON.helm)
			//if (player.world().rollDie(100, 85))
			drop(npc, player, MagicalAnimator.ArmourSets.IRON.legs)
			//if (player.world().rollDie(100, 85))
			drop(npc, player, MagicalAnimator.ArmourSets.IRON.body)
			drop(npc, player, Item(GUILD_TOKEN, 10))
		} else if (npc.id() == MagicalAnimator.ArmourSets.STEEL.npc) {
			//setSpawned(player, 0)
			//if (player.world().rollDie(100, 90))
			drop(npc, player, MagicalAnimator.ArmourSets.STEEL.helm)
			//if (player.world().rollDie(100, 90))
			drop(npc, player, MagicalAnimator.ArmourSets.STEEL.legs)
			//if (player.world().rollDie(100, 90))
			drop(npc, player, MagicalAnimator.ArmourSets.STEEL.body)
			drop(npc, player, Item(GUILD_TOKEN, 15))
		} else if (npc.id() == MagicalAnimator.ArmourSets.BLACK.npc) {
			//  setSpawned(player, 0)
			//if (player.world().rollDie(100, 92))
			drop(npc, player, MagicalAnimator.ArmourSets.BLACK.helm)
			//if (player.world().rollDie(100, 92))
			drop(npc, player, MagicalAnimator.ArmourSets.BLACK.legs)
			//if (player.world().rollDie(100, 92))
			drop(npc, player, MagicalAnimator.ArmourSets.BLACK.body)
			drop(npc, player, Item(GUILD_TOKEN, 20))
		} else if (npc.id() == MagicalAnimator.ArmourSets.MITHRIL.npc) {
			// setSpawned(player, 0)
			//if (player.world().rollDie(100, 96))
			drop(npc, player, MagicalAnimator.ArmourSets.MITHRIL.helm)
			//if (player.world().rollDie(100, 96))
			drop(npc, player, MagicalAnimator.ArmourSets.MITHRIL.legs)
			//if (player.world().rollDie(100, 96))
			drop(npc, player, MagicalAnimator.ArmourSets.MITHRIL.body)
			drop(npc, player, Item(GUILD_TOKEN, 25))
		} else if (npc.id() == MagicalAnimator.ArmourSets.ADAMANT.npc) {
			// setSpawned(player, 0)
			//if (player.world().rollDie(100, 98))
			drop(npc, player, MagicalAnimator.ArmourSets.ADAMANT.helm)
			//if (player.world().rollDie(100, 98))
			drop(npc, player, MagicalAnimator.ArmourSets.ADAMANT.legs)
			//if (player.world().rollDie(100, 98))
			drop(npc, player, MagicalAnimator.ArmourSets.ADAMANT.body)
			drop(npc, player, Item(GUILD_TOKEN, 30))
		} else if (npc.id() == MagicalAnimator.ArmourSets.RUNE.npc) {
			// setSpawned(player, 0)
			//if (player.world().rollDie(100, 99))
			drop(npc, player, MagicalAnimator.ArmourSets.RUNE.helm)
			//if (player.world().rollDie(100, 99))
			drop(npc, player, MagicalAnimator.ArmourSets.RUNE.legs)
			//if (player.world().rollDie(100, 99))
			drop(npc, player, MagicalAnimator.ArmourSets.RUNE.body)
			drop(npc, player, Item(GUILD_TOKEN, 40))
		}
	}
	
	//fun setSpawned(player: Player, stageBit: Int): Int {
	//    player.putattrib(AttributeKey.ARMOUR_SET, stageBit)
	//   return stageBit
	//}
	
}