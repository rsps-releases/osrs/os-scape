package droptables

import nl.bartpelle.veteres.content.minigames.bounty_hunter.BountyHunterEmblem
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

class RevenantDropTable : Droptable {
	
	val REVENANT_ETHER = 21820
	
	val REVENANT_IMP = 7881
	val REVENANT_GOBLIN = 7931
	val REVENANT_PYREFIEND = 7932
	val REVENANT_HOBGOBLIN = 7933
	val REVENANT_CYCLOPS = 7934
	val REVENANT_HELLHOUND = 7935
	val REVENANT_DEMON = 7936
	val REVENANT_ORK = 7937
	val REVENANT_DARK_BEAST = 7938
	val REVENANT_KNIGHT = 7939
	val REVENANT_DRAGON = 7940
	
	
	override fun reward(killed: Npc, killer: Player) {
		// Every revenant drops some ether...
		drop(killed, killer, Item(REVENANT_ETHER, killer.world().random(1..10)))
		
		// Revenants all have a slightly different loot table:
		when (killed.id()) {
			REVENANT_IMP -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_ONE, BountyHunterEmblem.TIER_ONE, 30, 3606, 50, 50)
			REVENANT_GOBLIN -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_ONE, BountyHunterEmblem.TIER_TWO, 30, 3606, 50, 75)
			REVENANT_PYREFIEND -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_ONE, BountyHunterEmblem.TIER_TWO, 30, 7297, 50, 100)
			REVENANT_HOBGOBLIN -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_ONE, BountyHunterEmblem.TIER_TWO, 30, 7297, 50, 125)
			REVENANT_CYCLOPS -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_TWO, BountyHunterEmblem.TIER_FOUR, 30, 7297, 50, 150)
			REVENANT_HELLHOUND -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_TWO, BountyHunterEmblem.TIER_FOUR, 30, 7297, 50, 150)
			REVENANT_DEMON -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_FOUR, BountyHunterEmblem.TIER_SIX, 50, 7297, 70, 200)
			REVENANT_ORK -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_FOUR, BountyHunterEmblem.TIER_SIX, 50, 7297, 70, 250)
			REVENANT_DARK_BEAST -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_FIVE, BountyHunterEmblem.TIER_EIGHT, 50, 7297, 70, 400)
			REVENANT_KNIGHT -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_FIVE, BountyHunterEmblem.TIER_EIGHT, 50, 7299, 70, 400)
			REVENANT_DRAGON -> dropRevenantTable(killed, killer, BountyHunterEmblem.TIER_SIX, BountyHunterEmblem.TIER_TEN, 50, 7299, 70, 500)
		}
	}
	
	fun dropRevenantTable(n: Npc, p: Player, minTier: BountyHunterEmblem, maxTier: BountyHunterEmblem, emblemOdds: Int, bloodKey: Int, keyOdds: Int, bm: Int) {
		if (p.world().rollDie(keyOdds, 1)) {
			drop(n, p, Item(p.world().random(minTier.id..maxTier.id), 1))
		} else if (p.world().rollDie(emblemOdds, 1)) {
			drop(n, p, Item(p.world().random(minTier.id..maxTier.id), 1))
		} else if (p.world().rollDie(5, 1)) {
			drop(n, p, Item(13307, bm))
		}
	}
	
}