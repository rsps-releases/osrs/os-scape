package droptables


import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.content.skills.slayer.SlayerCreature
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

class SuperiorSlayerDroptable : Droptable {
	
	val IMBUED_HEART = 20724
	val MIST_STAFF = 20733
	val DUST_STAFF = 20739
	
	override fun reward(killed: Npc, killer: Player) {
		val req = SlayerCreature.slayerReq(killed.id())
		var odds = (200.0 - ((Math.pow((req + 55).toDouble(), 2.0)) / 125.0))
		
		if (BonusContent.isActive(killer, BlessingGroup.FABLE)) {
			odds *= 0.85
		}
		
		if (killer.world().rollDie(odds.toInt(), 1)) {
			// Odds for the heart are 1/8
			if (killer.world().rollDie(8, 1)) {
				drop(killed, killer, Item(IMBUED_HEART))
			} else {
				// Staves are 7/8 (7/16 each)
				if (killer.world().random().nextBoolean()) {
					drop(killed, killer, Item(MIST_STAFF))
				} else {
					drop(killed, killer, Item(DUST_STAFF))
				}
			}
		}
	}
}