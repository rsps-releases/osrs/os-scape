package droptables

import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Bart on 10/6/2015.
 */
class GuardDropTable : Droptable {
	
	override fun reward(killed: Npc, killer: Player) {
		val random = killer.world().random()
		
		drop(killed, killer, Item(526)) // Spawn bones
		
		if (random.nextInt(128) == 0) {
			// Drop clue!!
		} else {
			val table = random.nextInt(22)
			when (table) {
				0, 1 -> drop(killed, killer, Item(440)) // Iron ore
				in 2..6 -> { // Weaponry
					
				}
				in 7..12 -> { // Runes
					when (random.nextInt(19)) {//upto 16
						in 0..2 -> drop(killed, killer, Item(556, 6)) // Air rune
						in 3..5 -> drop(killed, killer, Item(557, 3)) // Earth rune
						in 6..8 -> drop(killed, killer, Item(554, 2)) // Fire rune
						12 -> drop(killed, killer, Item(562, random.nextInt(2) + 1)) // Chaos rune
						13 -> drop(killed, killer, Item(561)) // Nature rune
						16 -> drop(killed, killer, Item(565)) // Blood rune
						17, 18 -> drop(killed, killer, Item(1446)) // Body talisman
					}
				}
				in 13..17 -> { // Seeds
					when (random.nextInt(17)) {//upto 16
						in 0..2 -> drop(killed, killer, Item(5318)) // Potato seed
						in 3..5 -> drop(killed, killer, Item(5319)) // Onion seed
						in 6..8 -> drop(killed, killer, Item(5324)) // Cabbage seed
						in 9..11 -> drop(killed, killer, Item(5322)) // Tomato seed
						12, 13 -> drop(killed, killer, Item(5320)) // Sweetcorn seed
						14, 15 -> drop(killed, killer, Item(5323)) // Strawberry seed
						16 -> drop(killed, killer, Item(5321)) // Watermelon seed
					}
				}
				else -> {
					if (random.nextInt(10) < 2)
						drop(killed, killer, Item(1947)) // Grain
					else if (!killer.world().realm().isPVP)
						drop(killed, killer, Item(995, random.nextInt(30) + 1)) // Coins
				}
			}
		}
	}
	
}