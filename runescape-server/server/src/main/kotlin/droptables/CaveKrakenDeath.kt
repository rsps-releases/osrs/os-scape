package droptables

import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.entity.player.IronMode
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Bart on 10/6/2015.
 */
class CaveKrakenDeath : Droptable {
	
	override fun reward(killed: Npc, killer: Player) {
		val table = ScalarLootTable.forNPC(493)
		if (table != null) {
			var reward = table.randomItem(killer.world().random(), false, false, false)
			if (reward != null) {
				drop(killer.tile(), killer, reward)
			}

			drop(killer.tile(), killer, Item(526))

			table.guaranteedDrops.forEach {
				drop(killer.tile(), killer, Item(it.id, killer.world().random(it.min..it.max)))
			}
			
			// Realism benefit
			if (killer.mode() == GameMode.REALISM && killer.world().rollDie(11, 1)) {
				killer.message("Your endurance in game mode has given you an extra drop!")
				reward = table.randomItem(killer.world().random(), false, false, false)
				if (reward != null) {
					drop(killer.tile(), killer, reward)
				}
			}
			
			val inWilderness: Boolean = killer.tile().x in 2942..3328 && killer.tile().z > 3524 && killer.tile().z < 3968
			val casket = 7956
			val resourceCasket = 405
			val combat = killer.skills().combatLevel()
			val mul: Int = if (killer.mode() == GameMode.CLASSIC) 2 else if (killer.mode() == GameMode.REALISM) 3 else 1
			var chance: Int =
					when {
						combat <= 10 -> 1
						combat <= 20 -> 2
						combat <= 80 -> 3
						combat <= 120 -> 4
						combat > 120 -> 5
						else -> 5
					}
			val regularOdds = if (killer.world().realm().isOSRune) 65 else 100
			
			chance *= mul
			
			//Do we get a casket drop?
			if (!killer.world().realm().isPVP) {
				if (inWilderness && killer.world().random(regularOdds - 15) < chance && killer.ironMode() == IronMode.NONE) {
					killer.message("<col=0B610B>You have received a casket drop!")
					drop(killer.tile(), killer, Item(casket))
				} else if (!inWilderness && killer.world().random(regularOdds) < chance && killer.ironMode() == IronMode.NONE) {
					killer.message("<col=0B610B>You have received a casket drop!")
					drop(killer.tile(), killer, Item(casket))
				}
				
				if (inWilderness && killer.world().random(regularOdds) <= chance && killer.ironMode() == IronMode.NONE) {
					killer.message("<col=0B610B>You have received a wilderness casket drop!")
					drop(killer.tile(), killer, Item(resourceCasket))
				}
			}
		}
		killed.sync().transmog(-1)
	}
	
}