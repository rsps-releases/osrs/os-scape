package droptables

import nl.bartpelle.veteres.fs.ItemDefinition
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Situations on 6/3/2015.
 */

class WarriorsGuildCyclops : Droptable {
	
	override fun reward(killed: Npc, killer: Player) {
		
		val defender = killer.attribOr<Int>(AttributeKey.WARRIORS_GUILD_CYCLOPS_ROOM_DEFENDER, 0)
		val chance = if (defender == 12954) 70 else 30
		
		if (killer.world().rollDie(chance, 1)) {
			val def = killer.world().definitions().get(ItemDefinition::class.java, defender)
			val aOrAn = if (defender == 8845) "an" else if (defender == 8849) "an" else "a"
			
			drop(killed.tile(), killer, Item(defender))
			killer.message("<col=804080>The Cyclops drops $aOrAn ${def.name}. Be sure to show this to Kamfreena to unlock the next tier Defender!")
		}
	}
}