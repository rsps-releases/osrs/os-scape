package droptables

import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.util.Varbit

/**
 * Created by Bart on 10/6/2015.
 */
class GWDSaradominMinion : Droptable {
	
	override fun reward(killed: Npc, killer: Player) {
		val current = killer.varps().varbit(Varbit.GWD_SARADOMIN_KC)
		if (current < 2000) {
			killer.varps().varbit(Varbit.GWD_SARADOMIN_KC, current + 1)
		}
	}
	
}