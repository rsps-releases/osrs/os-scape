package droptables

import nl.bartpelle.veteres.content.mechanics.NpcDeath
import nl.bartpelle.veteres.content.mechanics.ServerAnnouncements
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Bart on 10/6/2015.
 */
class ZulrahDeath : Droptable {
	
	override fun reward(killed: Npc, killer: Player) {
		val table = ScalarLootTable.forNPC(2042)
		if (table != null) {
			drop(killer.tile(), killer, Item(12934, 100 + killer.world().random(200)))
			drop(killer.tile(), killer, Item(12938, 1 + killer.world().random(1)))
			drop(killer.tile(), killer, Item(13307, 70 + killer.world().random(80)))
			
			NpcDeath.checkForPet(killer, table)
			
			if (killer.world().rollDie(50, 1) && !killer.world().realm().isPVP) {
				drop(killer.tile(), killer, Item(20543, 1))
				killer.message("<col=0B610B>You have received a clue scroll casket drop!")
			}
			
			val rolls = if (BonusContent.isActive(killer, BlessingGroup.UNTAMED)) 4 else 2
			for (i in 1..rolls) {
				val reward = table.randomItem(killer.world().random(), false, false, false)
				if (reward != null) {
					drop(killer.tile(), killer, reward)
					ServerAnnouncements.tryBroadcastDrop(killer, reward.id())
				}
			}
			
			// Realism benefit
			if (killer.mode() == GameMode.REALISM && killer.world().rollDie(7, 1)) {
				killer.message("Your endurance in game mode has given you an extra drop!")
				var reward = table.randomItem(killer.world().random(), false, false, false)
				if (reward != null) {
					drop(killer.tile(), killer, reward)
					ServerAnnouncements.tryBroadcastDrop(killer, reward.id())
				}
			}
		}
	}
	
}
