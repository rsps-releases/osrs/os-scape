package droptables

import nl.bartpelle.veteres.content.areas.wilderness.WildernessLevelIndicator
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.entity.player.GameMode
import nl.bartpelle.veteres.model.item.Item

/**
 * Created by Bart on 10/6/2015.
 */
class WildyAnkous : Droptable {
	
	override fun reward(killed: Npc, killer: Player) {
		if (WildernessLevelIndicator.inWilderness(killed.tile())) {
			val chance = when (killer.mode()) {
				GameMode.REALISM -> 3;
				GameMode.CLASSIC -> 2;
				else -> 1
			}
			if (killer.world().rollDie(chance, 20)) {
				drop(killed, killer, Item(11940, killer.world().random(40..60)))
			}
		}
	}
	
}