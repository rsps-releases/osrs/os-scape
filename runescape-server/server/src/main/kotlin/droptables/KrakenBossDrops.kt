package droptables

import nl.bartpelle.veteres.content.items.equipment.ZulrahItems
import nl.bartpelle.veteres.content.mechanics.NpcDeath
import nl.bartpelle.veteres.content.minigames.bonuscontent.BlessingGroup
import nl.bartpelle.veteres.content.minigames.bonuscontent.BonusContent
import nl.bartpelle.veteres.model.Area
import nl.bartpelle.veteres.model.World
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.model.item.Item
import nl.bartpelle.veteres.model.item.ItemAttrib
import java.util.*

/**
 * Created by Jak on 20/10/2016.
 */
class KrakenBossDrops : Droptable {
	
	override fun reward(npc: Npc, killer: Player) {
		val table = ScalarLootTable.forNPC(494)
		if (table != null) {
			val loots = ArrayList<String>()
			table.guaranteedDrops.forEach {
				drop(killer.tile(), killer, Item(it.id, killer.world().random(it.min..it.max)))
				loots.add(lootname(it.convert(), killer.world(), killer.name()))
			}
			
			val rolls = if (BonusContent.isActive(killer, BlessingGroup.UNTAMED)) 2 else 1
			for (i in 1..rolls) {
				var reward = table.randomItem(killer.world().random(), false, false, false)
				if (reward != null) {
					if (reward.id() == ZulrahItems.TRIDENT_FULL) {
						reward.property(ItemAttrib.CHARGES, 2500)
					} else if (reward.id() == ZulrahItems.TOXIC_TRIDENT_USED) {
						reward.property(ItemAttrib.CHARGES, 2500)
					}
					drop(killer.tile(), killer, reward)
					loots.add(lootname(reward, killer.world(), killer.name()))
				}
			}
			
			val room = Area(killer.tile().regionCorner(), killer.tile().regionCorner().transform(64, 64))
			// Broadcast to everyone in the normal kraken room.
			killer.world().players().forEachInAreaKt(room, { other ->
				for (lootmsg in loots)
					other.message(lootmsg)
			})
			NpcDeath.checkForPet(killer, table)
		}
	}
	
	private fun lootname(convert: Item, world: World, looterName: String): String {
		return String.format("<col=0B610B>%s received a drop: %s.",
				looterName,
				if (convert.unnote(world).amount() == 1)
					convert.name(world)
				else
					"" + convert.amount() + " x " + convert.unnote(world).name(world))
	}
}