package droptables

import co.paralleluniverse.fibers.Suspendable
import nl.bartpelle.veteres.content.clearContext
import nl.bartpelle.veteres.model.AttributeKey
import nl.bartpelle.veteres.model.entity.Npc
import nl.bartpelle.veteres.model.entity.Player
import nl.bartpelle.veteres.util.Tuple

/**
 * Created by Situations on 4/6/2016.
 */

class Ent : Droptable {
	override fun reward(killed: Npc, killer: Player) {
		val trunk = Npc(6595, killed.world(), killed.tile())
		trunk.world().registerNpc(trunk)
		trunk.putattrib(AttributeKey.OWNING_PLAYER, Tuple(killer.id(), killer))
		
		trunk.executeScript @Suspendable {
			it.clearContext()
			it.delay(100) // Lives for exactly a minute
			trunk.world().unregisterNpc(trunk)
		}
	}
}