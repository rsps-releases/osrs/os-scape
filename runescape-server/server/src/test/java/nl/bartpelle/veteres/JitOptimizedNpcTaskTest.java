package nl.bartpelle.veteres;

import io.netty.buffer.Unpooled;
import nl.bartpelle.veteres.io.RSBuffer;
import org.junit.Test;

/**
 * Created by Bart on 8/17/2016.
 */
public class JitOptimizedNpcTaskTest {
	
	private static final int NPC_INDEX = 10914, NPC_DIR = 2, NPC_UPDATE = 1,
			NPC_ID = 7185, NPC_TILE = 1, NPC_DX = -3, NPC_DZ = 4;
	
	@Test
	public void verifyEqual() {
		RSBuffer buffer = new RSBuffer(Unpooled.buffer(8));
		
		long start = System.nanoTime();
		for (int i = 0; i < 1000000; i++) {
			buffer.get().writerIndex(0);
			buffer.startBitMode();
			buffer.writeBits(15, NPC_INDEX);
			buffer.writeBits(3, NPC_DIR); // Direction to face
			buffer.writeBits(1, NPC_UPDATE); // Update
			buffer.writeBits(14, NPC_ID); // npc id
			buffer.writeBits(1, NPC_TILE); // Clear tile queue
			buffer.writeBits(5, NPC_DX);
			buffer.writeBits(5, NPC_DZ);
			buffer.endBitMode();
		}
		System.out.println("Buffer mode: " + (System.nanoTime() - start));
		
		long result = buffer.get().getLong(0);
		System.out.println(Long.toBinaryString(result));
		
		start = System.nanoTime();
		long computed = 0L;
		for (int i = 0; i < 1000000; i++) {
			computed |= ((long) NPC_INDEX) << 29L;
			computed |= ((long) NPC_DIR) << 26L;
			computed |= ((long) NPC_UPDATE) << 25L;
			computed |= ((long) NPC_ID) << 11L;
			computed |= ((long) NPC_TILE) << 10L;
			computed |= ((long) (NPC_DX & 0b11111)) << 5L;
			computed |= ((long) (NPC_DZ & 0b11111)) << 0L;
		}
		System.out.println("Banging mode: " + (System.nanoTime() - start));
		
		System.out.println(Long.toBinaryString(computed << 20L));
	}
	
}
